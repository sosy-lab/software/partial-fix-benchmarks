# Partial-Fix Benchmarks

This project contains benchmark tasks for automatic program repair of partial fixes.
The benchmark tasks reflect the following scenario:

- Bug found
- Bugfix committed, but doesn't fix
- Proper bugfix committed

Thus, each benchmark task is a triple of commit hashes h:
```math
(h_{original}, h_{fix1}, h_{fix2})
```

## Scripts

We provide scripts for querying Github for benchmarks that fulfill our criteria ([see below](Initial-Criteria)).

### Requirements
The scripts are written in python 3.8.

Python module requirements are listed in [`requirements.txt`](requirements.txt)
These can be installed, for example, with `pip install -r requirements.txt`.

### How to use the scripts
Please add your Github-API key to the config file in config/partial-fix.cfg.
For this go to `Settings/Developer settings/Personal access token` and generate a new one. The token does not need any rights!
See the official [GitHub documentation](https://docs.github.com/en/free-pro-team@latest/github/authenticating-to-github/creating-a-personal-access-token) for more details. 

Now open a terminal, cd to src/ and execute github_search.py. 
The script will create a new folder `benchmarks` containing all commits matching the selected strategy stored as .json-file.

There are three optional arguments:
- `-f` or `--files`: Add one or more paths to files which contain repositories separated by a line break
- `-r` or `--repositories`: Add one or more repositories
- `-b` or `--benchmarks`: Specify the output folder
- `-s` or `--strategy`: Select one of our three strategies: `reopen`, `status` or `score`
- `-u` or `--update`: This does not take any arguments, instead, if activated, the code will run on the existing benchmark set only

Repositories can either be passed as github.com url (`https://github.com/user/reponame`) or just by using `user/reponame`.
Without custom configurations the script stores all issues matching the `score strategy` to `../benchmarks`. 
By default we search the top-rated repositories, starting with the repository with the most stars.
## Initial Criteria

Benchmark tasks should fulfill the following criteria:

- Real-world programs (or parts of it)
- Executable test cases available that expose defect and demonstrate fix
- C programming language
- Language constructs supported by [Angelix](http://angelix.io/) and [Klee](https://klee.github.io/)
- Makefile-based projects (for Angelix)
- LLVM bitcode can be generated (for Klee)

Criteria on defects in programs:

- Defect statements are only over primitive data types: int, long, char, enum, struct, integer arrays, pointers
- Defect classes: defects are allowed in conditions and/or assignments
- Multiple defect statements are allowed, but no chunks of code that constitute a single defect together

Criteria on recurring fix:
- Recurring fixes in a single method
- Recurring fixes modify same code lines or code lines that reach each other according to control-flow

## Related benchmark statements

| Author   | Language  | Domain                             | Available |
|:-----    |:--------  |:--------------------------------   |:--------- |
|Seo and Kim [1]| C/C++|Firefox 3.6|No|
|Gao et al. [2]| Java|Android Projects|Yes|
|Guo et al. [3]|Java|Bugzilla Databases|No: broken link|
|Park et al. [4]|Java|Eclipse JDT core, SWT; Mozilla|Yes (bug ids)|

[1]: Seo H, Kim S. Predicting recurring crash stacks[C]//2012 Proceedings of the 27th
IEEE/ACM International Conference on Automated Software Engineering. IEEE,
2012: 180-189.  
[2]: Gao Q, Zhang H, Wang J, et al. Fixing recurring crash bugs via analyzing q&a
sites (T)[C]//2015 30th IEEE/ACM International Conference on Automated Software
Engineering (ASE). IEEE, 2015: 307-318.  
[3]: Gu Z, Barr E T, Hamilton D J, et al. Has the bug really been fixed?[C]//2010 ACM/
IEEE 32nd International Conference on Software Engineering. IEEE, 2010, 1: 55-64.  
[4]: Park J, Kim M, Ray B, et al. An empirical study of supplementary bug fixes[C]//
2012 9th IEEE Working Conference on Mining Software Repositories (MSR). IEEE,
2012: 40-49.  