diff --git a/doc/man/man5/keepalived.conf.5 b/doc/man/man5/keepalived.conf.5
index 79f6a9f6d..2d64df3ba 100644
--- a/doc/man/man5/keepalived.conf.5
+++ b/doc/man/man5/keepalived.conf.5
@@ -135,6 +135,12 @@ and
 # allows ipsets to be used with a namespace on kernels prior to 3.13.
 \fBnamespace_with_ipsets\fR
 
+# change the behavior of `net_namespace` parameter in order to do all ipvs
+# operations within the mentioned namespace, but keep the process in the
+# default namespace. It allows to easily split the VIP traffic on a given
+# namespace and keep the healthchecks traffic with the root namespace
+\fBnamespace_with_ipvs\fR
+
 # If multiple instances of keepalived are run in the same namespace,
 # this will create pid files with NAME as part of the file names,
 # in /var/run/keepalived.
diff --git a/keepalived/check/libipvs.c b/keepalived/check/libipvs.c
index 3da43fa4c..31bb2689b 100644
--- a/keepalived/check/libipvs.c
+++ b/keepalived/check/libipvs.c
@@ -53,6 +53,10 @@
 #include "logger.h"
 #include "utils.h"
 
+#if HAVE_DECL_CLONE_NEWNET
+#include "namespaces.h"
+#endif
+
 
 typedef struct ipvs_servicedest_s {
 	struct ip_vs_service_user	svc;
@@ -252,7 +256,7 @@ static int ipvs_nl_send_message(struct nl_msg *msg, nl_recvmsg_msg_cb_t func, vo
 		return -1;
 	}
 
-	if (genl_connect(sock) < 0)
+	if (nl_ipvs_connect(sock) < 0)
 		goto fail_genl;
 
 	family = genl_ctrl_resolve(sock, IPVS_GENL_NAME);
@@ -353,7 +357,7 @@ int ipvs_init(void)
 	try_nl = false;
 #endif
 
-	if ((sockfd = socket(AF_INET, SOCK_RAW | SOCK_CLOEXEC, IPPROTO_RAW)) == -1)
+	if ((sockfd = my_socketat(ipvs_namespace, AF_INET, SOCK_RAW | SOCK_CLOEXEC, IPPROTO_RAW)) == -1)
 		return -1;
 
 #if !HAVE_DECL_SOCK_CLOEXEC
diff --git a/keepalived/core/global_parser.c b/keepalived/core/global_parser.c
index 9ef758909..44d3389bd 100644
--- a/keepalived/core/global_parser.c
+++ b/keepalived/core/global_parser.c
@@ -1434,6 +1434,15 @@ namespace_ipsets_handler(const vector_t *strvec)
 
 	global_data->namespace_with_ipsets = true;
 }
+
+static void
+namespace_ipvs_handler(const vector_t *strvec)
+{
+	if (!strvec)
+		return;
+
+	global_data->namespace_with_ipvs = true;
+}
 #endif
 
 #ifdef _WITH_DBUS_
@@ -1951,6 +1960,7 @@ init_global_keywords(bool global_active)
 #if HAVE_DECL_CLONE_NEWNET
 	install_keyword_root("net_namespace", &net_namespace_handler, global_active);
 	install_keyword_root("namespace_with_ipsets", &namespace_ipsets_handler, global_active);
+	install_keyword_root("namespace_with_ipvs", &namespace_ipvs_handler, global_active);
 #endif
 	install_keyword_root("use_pid_dir", &use_pid_dir_handler, global_active);
 	install_keyword_root("instance", &instance_handler, global_active);
diff --git a/keepalived/core/main.c b/keepalived/core/main.c
index 7dbfa3a8c..28a04730c 100644
--- a/keepalived/core/main.c
+++ b/keepalived/core/main.c
@@ -2257,7 +2257,11 @@ keepalived_main(int argc, char **argv)
 
 #if HAVE_DECL_CLONE_NEWNET
 	if (global_data->network_namespace) {
-		if (global_data->network_namespace && !set_namespaces(global_data->network_namespace)) {
+		if (global_data->namespace_with_ipvs && init_ipvs_namespaces(global_data->network_namespace) < 0) {
+			log_message(LOG_ERR, "Unable to set network namespace for ipvs %s - exiting", global_data->network_namespace);
+			goto end;
+		}
+		else if (!set_namespaces(global_data->network_namespace)) {
 			log_message(LOG_ERR, "Unable to set network namespace %s - exiting", global_data->network_namespace);
 			goto end;
 		}
@@ -2421,8 +2425,12 @@ keepalived_main(int argc, char **argv)
 	}
 
 #if HAVE_DECL_CLONE_NEWNET
-	if (global_data && global_data->network_namespace)
-		clear_namespaces();
+	if (global_data && global_data->network_namespace) {
+		if (global_data->namespace_with_ipvs)
+			close_ipvs_namespaces();
+		else
+			clear_namespaces();
+	}
 #endif
 
 	if (use_pid_dir)
diff --git a/keepalived/core/namespaces.c b/keepalived/core/namespaces.c
index 09ebfff17..1afe4a270 100644
--- a/keepalived/core/namespaces.c
+++ b/keepalived/core/namespaces.c
@@ -163,6 +163,16 @@
 #include <stdio.h>
 #include <sys/mount.h>
 #include <stdbool.h>
+#include <limits.h>
+#include <sys/types.h>
+#include <sys/socket.h>
+
+#ifdef LIBIPVS_USE_NL
+#include <netlink/socket.h>
+#include <netlink/genl/genl.h>
+#endif
+
+static int default_namespace = -1;
 
 #ifndef HAVE_SETNS
 //#include "linux/unistd.h"
@@ -300,3 +310,87 @@ clear_namespaces(void)
 {
 	unmount_run();
 }
+
+/* get default namespace file descriptor to be able to place fd in a given
+ * namespace
+ */
+static int init_default_namespace(void)
+{
+	char netns_path[PATH_MAX];
+	pid_t pid;
+
+	pid = getpid();
+	if (sprintf(netns_path, "/proc/%d/ns/net", pid) < 0)
+		return -1;
+
+	default_namespace = open(netns_path, O_RDONLY | O_CLOEXEC);
+	return default_namespace;
+}
+
+/* opens a socket in the <nsfd> namespace with the parameters <domain>,
+ * <type> and <protocol> and returns the FD or -1 in case of error (check errno).
+ */
+int my_socketat(int nsfd, int domain, int type, int protocol)
+{
+	int sock;
+
+	if (default_namespace >= 0 && nsfd && setns(nsfd, CLONE_NEWNET) == -1)
+		return -1;
+
+	sock = socket(domain, type, protocol);
+
+	if (default_namespace >= 0 && nsfd && setns(default_namespace, CLONE_NEWNET) == -1) {
+		if (sock >= 0)
+			close(sock);
+		return -1;
+	}
+	return sock;
+}
+
+/* opens namespace for ipvs communication */
+static int open_ipvs_namespace(const char *ns_name)
+{
+	char netns_path[PATH_MAX];
+
+	if (sprintf(netns_path, "/var/run/netns/%s", ns_name) < 0)
+		return -1;
+
+	ipvs_namespace = open(netns_path, O_RDONLY | O_CLOEXEC);
+	return ipvs_namespace;
+}
+
+/* init default and ipvs namespaces */
+int init_ipvs_namespaces(const char *ns_name)
+{
+	if (init_default_namespace() < 0)
+		return -1;
+	if (open_ipvs_namespace(ns_name) < 0)
+		return -1;
+	return 0;
+}
+
+/* close default and ipvs namespaces */
+int close_ipvs_namespaces(void)
+{
+	int ret = 0;
+
+	ret = close(default_namespace);
+	if (ret)
+		return ret;
+	ret = close(ipvs_namespace);
+	return ret;
+}
+
+/* netlink connect within ipvs namespace */
+int nl_ipvs_connect(struct nl_sock *sock)
+{
+	if (default_namespace >= 0 && ipvs_namespace && setns(ipvs_namespace, CLONE_NEWNET) == -1)
+		return -1;
+
+	if (genl_connect(sock) < 0)
+		return -1;
+
+	if (default_namespace >= 0 && ipvs_namespace && setns(default_namespace, CLONE_NEWNET) == -1)
+		return -1;
+	return 0;
+}
diff --git a/keepalived/include/global_data.h b/keepalived/include/global_data.h
index 07c4e22d0..4b361488d 100644
--- a/keepalived/include/global_data.h
+++ b/keepalived/include/global_data.h
@@ -96,6 +96,7 @@ typedef struct _data {
 #if HAVE_DECL_CLONE_NEWNET
 	const char			*network_namespace;	/* network namespace name */
 	bool				namespace_with_ipsets;	/* override for namespaces with ipsets on Linux < 3.13 */
+	bool				namespace_with_ipvs;	/* only move ipvs in network namespace */
 #endif
 	const char			*local_name;
 	const char			*instance_name;		/* keepalived instance name */
diff --git a/keepalived/include/namespaces.h b/keepalived/include/namespaces.h
index 09baf8ee6..0892227c5 100644
--- a/keepalived/include/namespaces.h
+++ b/keepalived/include/namespaces.h
@@ -24,9 +24,17 @@
 #define _NAMESPACE_H_
 
 #include <stdbool.h>
+#include <netlink/socket.h>
 
 extern void free_dirname(void);
 extern bool set_namespaces(const char*);
 extern void clear_namespaces(void);
+extern int my_socketat(int, int, int, int);
+
+/* ipvs namespaces */
+extern int init_ipvs_namespaces(const char *);
+extern int close_ipvs_namespaces(void);
+extern int nl_ipvs_connect(struct nl_sock *sock);
+int ipvs_namespace = -1;
 
 #endif
