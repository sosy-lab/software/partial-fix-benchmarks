Rename sb_rand() to sb_rand_default().
Fixes #27: Provide portable reentrant PRNG

Replaced system-provided PRNG with xoroshiro128+. Minor cleanups in
random numbers API.
Fixes #27: Provide portable reentrant PRNG

Replaced system-provided PRNG with xoroshiro128+. Minor cleanups in
random numbers API.
