persistently substitute invalid job attributes with default values - not only in add_job
@vliaskov The preferred way to fix this is to pass a valid username in the first place. Personally I would have preferred to simply reject any print request containing an invalid username, as the current workaround is to treat the request as anonymous unless "strict conformance" is enabled on the server.

[master 092db1e29] Additional changes for the scheduler to substitute default values for invalid job attributes when running in "relaxed conformance" mode (Issue #5229)
Thank you @michaelrsweet it works for me as well with this commit.
Reopening to bring this into 2.2.x as well (since it appears the changes were not backported... :/)

[branch-2.2 aefefc353] Backport changes for attribute substitution (Issue #5229)
