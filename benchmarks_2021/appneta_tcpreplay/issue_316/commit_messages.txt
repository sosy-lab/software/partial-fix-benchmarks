Merge pull request #321 from GabrielGanne/l7-fuzzing-doc

improve fuzzing documentation
Bug #316 Back out fuzzing test

The option `--fuzz-seed` feeds the system's random seed. Although this produces consistent results between runs on a given platform, results will be inconsistent on other platforms. Therefore a test cannot be produced tht is multi-platform.

Alternately a separate random number generated could be implemented, bypassing the system. This would ensure consistency across platforms.
Merge pull request #332 from appneta/Bug_#316_fuzz_test_fails_osx

Bug #316 fuzz test fails osx
Bug #316 - add platform-consistent random number generator
Bug #316 reintroduce tests which work multi-platform
Bug #316 - fix big-endian tests
Bug #316 - fix little-endian test
Merge pull request #337 from appneta/Bug_#316_new_pseudo_random_generator

Bug #316 new pseudo random generator
Merge branch 'Release_4.2' into coverity_scan

* Release_4.2: (27 commits)
  Release 4.2 - fix merge issue - update version
  Feature #302 - update change log
  fuzzing  ignores layers 2, 3, and 4 (#341)
  Enhancement #334 - Fix warnings on debug (#339)
  Enhancement #334 - modernize configure.ac (#338)
  Bug #316 - fix little-endian test
  Bug #316 - fix big-endian tests
  Bug #316 reintroduce tests which work multi-platform
  Bug #316 - add platform-consistent random number generator
  Remove -O3 from CFLAGS
  Replace deprecated AM_CONFIG_HEADER
  Bug #316 Back out fuzzing test
  Bug #329 fix -l0 print logic
  even more Coverity fixes
  more Coverity fixes
  Coverity fixes
  Release 4.2beta3: bump version
  Release 4.2.0beta2 - update CHANGELOG (#326)
  add tcpreplay_opts.h to list of BUILT_SOURCES
  Bug #210 avoid overflow with -M option
  ...
