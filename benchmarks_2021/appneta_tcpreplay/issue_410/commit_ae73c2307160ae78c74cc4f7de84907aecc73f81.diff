diff --git a/.travis.yml b/.travis.yml
index acf7b828..1da59f2d 100644
--- a/.travis.yml
+++ b/.travis.yml
@@ -9,24 +9,24 @@ addons:
 
 matrix:
   include:
-    - os: osx
-      osx_image: xcode8.2
-      compiler: clang
-      before_install:
-        - brew update
-        - brew install autogen
     - os: linux
       dist: trusty
       compiler: gcc
+      addons:
+        apt:
+          packages:
+          - libpcap-dev
+          - autogen
     - os: linux
       dist: trusty
       compiler: clang
-      env: CI_BUILD_PREFIX=scan-build-3.5
+      env: CI_BUILD_PREFIX=scan-build-3.9
       addons:
         apt:
           packages:
           - libpcap-dev
-          - clang-3.5
+          - clang-3.9
+          - autogen
 
 script:
   - autoreconf -iv > build.log 2>&1 || (cat build.log && exit 1)
diff --git a/configure.ac b/configure.ac
index 4039bf31..fa9eef70 100644
--- a/configure.ac
+++ b/configure.ac
@@ -4,7 +4,7 @@ dnl $Id$
 AC_PREREQ([2.69])
 
 dnl Set version info here!
-AC_INIT([tcpreplay],[4.2.7-beta1],
+AC_INIT([tcpreplay],[4.3.0-beta1],
     [https://github.com/appneta/tcpreplay/issues],
     [tcpreplay],
     [http://tcpreplay.sourceforge.net/])
@@ -15,7 +15,8 @@ AM_MAINTAINER_MODE
 AM_WITH_DMALLOC
 
 dnl People building from GitHub need the same version of Autogen as I'm using
-dnl or specify --disable-local-libopts
+dnl or specify --disable-local-libopts to force using the locally-installed
+dnl copy of libopts rather than the source in the `./liopts/` directory.
 MAINTAINER_AUTOGEN_VERSION=5.18.4
 
 AC_CONFIG_MACRO_DIR([m4])
@@ -171,21 +172,41 @@ AC_PATH_PROG(depmod, depmod, /sbin/depmod, $PATH:/sbin)
 AC_PATH_PROG(insmod, insmod, /sbin/insmod, $PATH:/sbin)
 AC_PATH_PROG(rmmod, rmmod, /sbin/rmmod, $PATH:/sbin)
 
+
+dnl tcpreplay has (so far) been relying on leading-edge autogen.
+dnl Therefore, by default:
+dnl - use the version we ship with
+dnl - do not install it
+dnl - build a static copy (AC_DISABLE_SHARED - implicitly done earlier)
+case "${enable_local_libopts+set}" in
+ set) ;;
+ *) enable_local_libopts=yes ;;
+esac
+
+case "${enable_libopts_install+set}" in
+ set) ;;
+ *) enable_libopts_install=no ;;
+esac
+
 dnl check autogen version
 AUTOGEN_VERSION="unknown - man pages will not be built"
 if test -n "${AUTOGEN}" ; then
-    AC_MSG_CHECKING(for autogen version >= 5.18.x)
-    ${AUTOGEN} -v >autogen.version
-    AUTOGEN_VERSION=$(cat autogen.version | ${SED} 's|.* \([[0-9\.]]\{5,\}\).*|\1|')
+    AC_MSG_CHECKING(for autogen version >= ${MAINTAINER_AUTOGEN_VERSION})
+    AUTOGEN_VERSION=$(${AUTOGEN} -v | ${CUT} -d' ' -f 4)
     AUTOGEN_MAJOR=$(echo ${AUTOGEN_VERSION} | ${CUT} -d '.' -f 1)
     AUTOGEN_MINOR=$(echo ${AUTOGEN_VERSION} | ${CUT} -d '.' -f 2)
-    if ( test ${AUTOGEN_MAJOR} -eq 5 && test ${AUTOGEN_MINOR} -lt 18 ) || test ${AUTOGEN_MAJOR} -lt 5 ; then
+    AUTOGEN_BUILD=$(echo ${AUTOGEN_VERSION} | ${CUT} -d '.' -f 3)
+    if (test ${AUTOGEN_MAJOR} -eq 5 && test ${AUTOGEN_MINOR} -eq 18 && test ${AUTOGEN_BUILD} -lt 4) || 
+            (test ${AUTOGEN_MAJOR} -eq 5 && test ${AUTOGEN_MINOR} -lt 18) ||
+            test ${AUTOGEN_MAJOR} -lt 5 ; then
         AC_MSG_RESULT(no)
-        AC_MSG_WARN([${AUTOGEN} is too old (${AUTOGEN_VERSION}) for building from source code. Please upgrade to 5.18.4)])
+        if test "x$enable_local_libopts" == "xno"; then
+            AC_MSG_ERROR([${AUTOGEN} is too old (${AUTOGEN_VERSION}) for building from source code. Upgrade to 5.18.4 or higher])
+        fi
+        AUTOGEN_VERSION="${AUTOGEN_VERSION} - downlevel"
     else
         AC_MSG_RESULT(yes)
     fi
-    rm -f autogen.version
 
     dnl Compare the installed version with the maintainer version if building from GitHub and not using system libopts
     if test ! -f src/tcpreplay_opts.c && test "x$enable_local_libopts" = "xyes" ; then
@@ -195,7 +216,7 @@ if test -n "${AUTOGEN}" ; then
     fi
 else
     if test ! -f src/tcpreplay_opts.c ; then
-        AC_MSG_ERROR([Please install GNU autogen $MAINTAINER_AUTOGEN_VERSION if you are building from GitHub. To avoid this message download source from http://tcpreplay.appneta.com/wiki/installation.html])
+        AC_MSG_ERROR([Please install GNU autogen $MAINTAINER_AUTOGEN_VERSION or higher if you are building from GitHub. To avoid this message download source from http://tcpreplay.appneta.com/wiki/installation.html])
     fi
 fi
 AC_DEFINE([AUTOGEN_VERSION], [${AUTOGEN_VERSION}], [What version of autogen is installed on this system])
@@ -239,18 +260,26 @@ AC_CHECK_TYPE(u_int32_t, uint32_t)
 AC_CHECK_TYPE(u_int64_t, uint64_t)
 
 dnl OS X SDK 10.11 throws lots of unnecessary macro warnings
+wno_format=""
+wno_macro_redefined=""
 case $host in
     *-apple-darwin*)
         AC_MSG_CHECKING(for $CC -Wno-macro-redefined)
         OLD_CFLAGS=$CFLAGS
-        CFLAGS="$CFLAGS -Wno-macro-redefined"
-        wno_macro_redefined=""
+        CFLAGS="$CFLAGS -Wno-macro-redefined -Werror"
         AC_COMPILE_IFELSE([AC_LANG_SOURCE([[#include <stdlib.h>
             int main(int argc, char *argv[]) { return(0); }]])],
             [ AC_MSG_RESULT(yes)
               wno_macro_redefined="-Wno-macro-redefined" ],
             [ AC_MSG_RESULT(no) ])
         CFLAGS="$OLD_CFLAGS $wno_macro_redefined"
+        CFLAGS="$CFLAGS -Wno-format -Werror"
+        AC_COMPILE_IFELSE([AC_LANG_SOURCE([[#include <stdlib.h>
+            int main(int argc, char *argv[]) { return(0); }]])],
+        [ AC_MSG_RESULT(yes)
+          wno_format="-Wno-format" ],
+        [ AC_MSG_RESULT(no) ])
+        CFLAGS="$OLD_CFLAGS $wno_format"
     ;;
 esac
 
@@ -352,7 +381,7 @@ AC_ARG_ENABLE(debug,
     AC_HELP_STRING([--enable-debug], [Enable debugging code and support for the -d option]),
     [ if test x$enableval = xyes; then
         debug=yes
-        CFLAGS="${USER_CFLAGS} -g -O0 -std=gnu99 -Wall $wextra $wfatal_errors $wno_variadic_macros $wno_format_contains_nul"
+        CFLAGS="${USER_CFLAGS} -g -O0 -std=gnu99 -Wall $wextra $wfatal_errors $wno_variadic_macros $wno_format_contains_nul $wno_format"
 # We may also want to add:
 #         -Wformat-security -Wswitch-default -Wunused-paramter -Wpadded"
         debug_flag=DEBUG
@@ -847,6 +876,9 @@ AC_RUN_IFELSE([AC_LANG_PROGRAM([[
  * returns zero if version >= 0.9.6
  * or one otherwise
  */
+    if (strncmp(pcap_lib_version(), PCAP_TEST, 3) >= 0)
+        exit(0);
+
     if (strncmp(pcap_lib_version(), PCAP_TEST, 5) >= 0) {
         printf("%s ", pcap_lib_version());
         exit(0);
@@ -898,6 +930,8 @@ extern const char pcap_version[[]]; /* double up since autoconf escapes things *
  * returns zero if version >= 0.7.2
  * or one otherwise
  */
+    if (strncmp(pcap_lib_version(), PCAP_TEST, 3) >= 0)
+        exit(0);
 
     if (strncmp(pcap_version, PCAP_TEST, 5) >= 0)
             exit(0);
@@ -1697,7 +1731,7 @@ AM_CONDITIONAL([ENABLE_TCPDUMP], test "$tcpdump_path" != "no" -a x$have_pcap_dum
 if test x$tcpdump_path != xno -a x$have_pcap_dump_fopen = xyes ; then
     AC_DEFINE([ENABLE_VERBOSE], [1], [Do we have tcpdump and pcap_dump_fopen()?])
 else
-    AC_MSG_WARN([Your version of libpcap is too old for --verbose support])
+    AC_MSG_WARN([Your version of libpcap does not support --verbose])
 fi
 
 dnl No 'make test' when cross compile
@@ -1802,21 +1836,6 @@ else
     AC_MSG_RESULT(no)
 fi
 
-dnl tcpreplay has (so far) been relying on leading-edge autogen.
-dnl Therefore, by default:
-dnl - use the version we ship with
-dnl - do not install it
-dnl - build a static copy (AC_DISABLE_SHARED - implicitly done earlier)
-case "${enable_local_libopts+set}" in
- set) ;;
- *) enable_local_libopts=yes ;;
-esac
-
-case "${enable_libopts_install+set}" in
- set) ;;
- *) enable_libopts_install=no ;;
-esac
-
 LIBOPTS_CHECK(libopts)
 
 
diff --git a/docs/CHANGELOG b/docs/CHANGELOG
index 2bcd1b05..511b747e 100644
--- a/docs/CHANGELOG
+++ b/docs/CHANGELOG
@@ -1,4 +1,10 @@
-05/27/2017 Version 4.2.7-beta1
+01/18/2018 Version 4.3.0 beta1
+    - Travis CI build fails due to new build images (#432)
+    - Unable to build with libpcap 1.8.1 (#430)
+    - heap-buffer-overflow in get_l2protocol (#410)
+    - heap-buffer-overflow in packet2tree (#409)
+    - heap-buffer-overflow in get_l2len (#408)
+    - heap-buffer-overflow in flow_decode (#407)
     - Fix sleep calculations when using multiple pcap files (#399)
 
 05/10/2017 Version 4.2.6
diff --git a/src/common/flows.c b/src/common/flows.c
index 6a9eaffc..7e2fe1fe 100644
--- a/src/common/flows.c
+++ b/src/common/flows.c
@@ -185,12 +185,18 @@ flow_entry_type_t flow_decode(flow_hash_table_t *fht, const struct pcap_pkthdr *
     switch (datalink) {
     case DLT_LINUX_SLL:
         l2_len = 16;
+        if (pkthdr->caplen < l2_len)
+            return FLOW_ENTRY_INVALID;
+
         sll_hdr = (sll_hdr_t *)pktdata;
         ether_type = sll_hdr->sll_protocol;
         break;
 
     case DLT_PPP_SERIAL:
         l2_len = 4;
+        if (pkthdr->caplen < l2_len)
+            return FLOW_ENTRY_INVALID;
+
         ppp = (struct tcpr_pppserial_hdr *)pktdata;
         if (ntohs(ppp->protocol) == 0x0021)
             ether_type = htons(ETHERTYPE_IP);
@@ -200,6 +206,9 @@ flow_entry_type_t flow_decode(flow_hash_table_t *fht, const struct pcap_pkthdr *
 
     case DLT_C_HDLC:
         l2_len = 4;
+        if (pkthdr->caplen < l2_len)
+            return FLOW_ENTRY_INVALID;
+
         hdlc_hdr = (hdlc_hdr_t *)pktdata;
         ether_type = hdlc_hdr->protocol;
         break;
@@ -212,6 +221,9 @@ flow_entry_type_t flow_decode(flow_hash_table_t *fht, const struct pcap_pkthdr *
         break;
 
     case DLT_JUNIPER_ETHER:
+        if (pkthdr->caplen < 5)
+            return FLOW_ENTRY_INVALID;
+
         if (memcmp(pktdata, "MGC", 3))
             warnx("No Magic Number found: %s (0x%x)",
                  pcap_datalink_val_to_description(datalink), datalink);
@@ -222,11 +234,15 @@ flow_entry_type_t flow_decode(flow_hash_table_t *fht, const struct pcap_pkthdr *
         } else
             l2_len = 4; /* no header extensions */
 
+        /* fall through */
+    case DLT_EN10MB:
+        /* set l2_len if we did not fell through */
+        if (l2_len == 0)
+            l2_len = sizeof(eth_hdr_t);
+
         if (pkthdr->caplen < l2_len)
             return FLOW_ENTRY_INVALID;
 
-        /* fall through */
-    case DLT_EN10MB:
         ether_type = ntohs(((eth_hdr_t*)(pktdata + l2_len))->ether_type);
 
         while (ether_type == ETHERTYPE_VLAN) {
diff --git a/src/common/get.c b/src/common/get.c
index 11a9b347..87baa786 100644
--- a/src/common/get.c
+++ b/src/common/get.c
@@ -86,8 +86,11 @@ get_l2protocol(const u_char *pktdata, const int datalen, const int datalink)
     uint16_t eth_hdr_offset = 0;
     struct tcpr_pppserial_hdr *ppp;
 
-    assert(pktdata);
-    assert(datalen);
+    if (!pktdata || !datalen) {
+        errx(-1, "invalid l2 parameters: pktdata=0x%p len=%d",
+                pktdata, datalen);
+        return 0;
+    }
 
     switch (datalink) {
     case DLT_RAW:
@@ -105,37 +108,46 @@ get_l2protocol(const u_char *pktdata, const int datalen, const int datalink)
         if ((pktdata[3] & 0x80) == 0x80) {
             eth_hdr_offset = ntohs(*((uint16_t*)&pktdata[4]));
             eth_hdr_offset += 6;
-        } else
+        } else {
             eth_hdr_offset = 4; /* no header extensions */
+        }
         /* fall through */
     case DLT_EN10MB:
-        eth_hdr = (eth_hdr_t *)(pktdata + eth_hdr_offset);
-        ether_type = ntohs(eth_hdr->ether_type);
-        switch (ether_type) {
-        case ETHERTYPE_VLAN: /* 802.1q */
-            vlan_hdr = (vlan_hdr_t *)pktdata;
-            return ntohs(vlan_hdr->vlan_len);
-        default:
-            return ether_type; /* yes, return it in host byte order */
+        if (datalen >= (sizeof(eth_hdr_t) + eth_hdr_offset)) {
+            eth_hdr = (eth_hdr_t *)(pktdata + eth_hdr_offset);
+            ether_type = ntohs(eth_hdr->ether_type);
+            switch (ether_type) {
+            case ETHERTYPE_VLAN: /* 802.1q */
+                vlan_hdr = (vlan_hdr_t *)pktdata;
+                return ntohs(vlan_hdr->vlan_len);
+            default:
+                return ether_type; /* yes, return it in host byte order */
+            }
         }
         break;
 
     case DLT_PPP_SERIAL:
-        ppp = (struct tcpr_pppserial_hdr *)pktdata;
-        if (ntohs(ppp->protocol) == 0x0021)
-            return htons(ETHERTYPE_IP);
-        else
-            return ppp->protocol;
+        if (datalen >= sizeof(struct tcpr_pppserial_hdr)) {
+            ppp = (struct tcpr_pppserial_hdr *)pktdata;
+            if (ntohs(ppp->protocol) == 0x0021)
+                return htons(ETHERTYPE_IP);
+            else
+                return ppp->protocol;
+        }
         break;
 
     case DLT_C_HDLC:
-        hdlc_hdr = (hdlc_hdr_t *)pktdata;
-        return hdlc_hdr->protocol;
+        if (datalen >= sizeof(hdlc_hdr_t)) {
+            hdlc_hdr = (hdlc_hdr_t *)pktdata;
+            return hdlc_hdr->protocol;
+        }
         break;
 
     case DLT_LINUX_SLL:
-        sll_hdr = (sll_hdr_t *)pktdata;
-        return sll_hdr->sll_protocol;
+        if (datalen >= sizeof(sll_hdr_t)) {
+            sll_hdr = (sll_hdr_t *)pktdata;
+            return sll_hdr->sll_protocol;
+        }
         break;
 
     default:
@@ -164,45 +176,58 @@ get_l2len(const u_char *pktdata, const int datalen, const int datalink)
     switch (datalink) {
     case DLT_RAW:
         /* pktdata IS the ip header! */
-        return 0;
         break;
 
     case DLT_JUNIPER_ETHER:
         l2_len = 24;
         /* fall through */
     case DLT_EN10MB:
-        ether_type = ntohs(((eth_hdr_t*)(pktdata + l2_len))->ether_type);
+        if (datalen >= sizeof(eth_hdr_t) + l2_len) {
+            ether_type = ntohs(((eth_hdr_t*)(pktdata + l2_len))->ether_type);
+
+            while (ether_type == ETHERTYPE_VLAN) {
+                vlan_hdr = (vlan_hdr_t *)(pktdata + l2_len);
+                ether_type = ntohs(vlan_hdr->vlan_len);
+                l2_len += 4;
+                if (datalen < sizeof(vlan_hdr_t) + l2_len) {
+                    l2_len = -1;
+                    break;
+                }
+            }
 
-        while (ether_type == ETHERTYPE_VLAN) {
-            vlan_hdr = (vlan_hdr_t *)(pktdata + l2_len);
-            ether_type = ntohs(vlan_hdr->vlan_len);
-            l2_len += 4;
+            l2_len += sizeof(eth_hdr_t);
         }
 
-        l2_len += sizeof(eth_hdr_t);
+        if (datalen < l2_len)
+            l2_len = -1;
 
-        return l2_len;
         break;
 
     case DLT_PPP_SERIAL:
-        return 4;
+        if (datalen >= 4) {
+            l2_len = 4;
+        }
         break;
 
     case DLT_C_HDLC:
-        return CISCO_HDLC_LEN;
+        if (datalen >= CISCO_HDLC_LEN) {
+            l2_len = CISCO_HDLC_LEN;
+        }
         break;
 
     case DLT_LINUX_SLL:
-        return SLL_HDR_LEN;
+        if (datalen >= SLL_HDR_LEN) {
+            l2_len = SLL_HDR_LEN;
+        }
         break;
 
     default:
         errx(-1, "Unable to process unsupported DLT type: %s (0x%x)", 
              pcap_datalink_val_to_description(datalink), datalink);
-        break;
+        return -1; /* we shouldn't get here */
     }
 
-    return -1; /* we shouldn't get here */
+    return l2_len;
 }
 
 /**
@@ -229,7 +254,7 @@ get_ipv4(const u_char *pktdata, int datalen, int datalink, u_char **newbuff)
     l2_len = get_l2len(pktdata, datalen, datalink);
 
     /* sanity... datalen must be > l2_len + IP header len*/
-    if (l2_len + TCPR_IPV4_H > datalen) {
+    if (l2_len < 0 || l2_len + TCPR_IPV4_H > datalen) {
         dbg(1, "get_ipv4(): Layer 2 len > total packet len, hence no IP header");
         return NULL;
     }
@@ -291,7 +316,7 @@ get_ipv6(const u_char *pktdata, int datalen, int datalink, u_char **newbuff)
     l2_len = get_l2len(pktdata, datalen, datalink);
 
     /* sanity... datalen must be > l2_len + IP header len*/
-    if (l2_len + TCPR_IPV6_H > datalen) {
+    if (l2_len < 0 || l2_len + TCPR_IPV6_H > datalen) {
         dbg(1, "get_ipv6(): Layer 2 len > total packet len, hence no IPv6 header");
         return NULL;
     }
@@ -360,10 +385,15 @@ get_layer4_v6(const ipv6_hdr_t *ip6_hdr, const int len)
     struct tcpr_ipv6_ext_hdr_base *next, *exthdr;
     uint8_t proto;
     uint32_t maxlen;
+    int min_len;
 
     assert(ip6_hdr);
 
-    /* jump to the end of the IPv6 header */ 
+    min_len = TCPR_IPV6_H + sizeof(struct tcpr_ipv6_ext_hdr_base);
+    if (len < min_len)
+        return NULL;
+
+    /* jump to the end of the IPv6 header */
     next = (struct tcpr_ipv6_ext_hdr_base *)((u_char *)ip6_hdr + TCPR_IPV6_H);
     proto = ip6_hdr->ip_nh;
 
@@ -374,7 +404,7 @@ get_layer4_v6(const ipv6_hdr_t *ip6_hdr, const int len)
         /* recurse due to v6-in-v6, need to recast next as an IPv6 Header */
         case TCPR_IPV6_NH_IPV6:
             dbg(3, "recursing due to v6-in-v6");
-            return get_layer4_v6((ipv6_hdr_t *)next, len);
+            return get_layer4_v6((ipv6_hdr_t *)next, len - min_len);
             break;
 
         /* loop again */
diff --git a/src/common/tcpdump.c b/src/common/tcpdump.c
index db7d314f..5db66534 100644
--- a/src/common/tcpdump.c
+++ b/src/common/tcpdump.c
@@ -307,7 +307,7 @@ tcpdump_fill_in_options(char *opt)
     char *token = NULL;
 
     /* zero out our options_vec for execv() */
-    memset(options_vec, '\0', OPTIONS_VEC_SIZE);
+    memset(options_vec, '\0', sizeof(options_vec));
     
     /* first arg should be the binary (by convention) */
     options_vec[0] = TCPDUMP_BINARY;
diff --git a/src/defines.h.in b/src/defines.h.in
index c1aeccc5..2f6a9d1e 100644
--- a/src/defines.h.in
+++ b/src/defines.h.in
@@ -226,6 +226,7 @@ typedef enum tcpprep_mode_e {
 #endif
 
 /* converts a 64bit int to network byte order */
+#if !defined ntohll && !defined htonll
 #ifndef HAVE_NTOHLL
 #ifdef WORDS_BIGENDIAN
 #define ntohll(x) (x)
@@ -236,7 +237,8 @@ typedef enum tcpprep_mode_e {
                      (unsigned int)ntohl(((int)(x >> 32))))
 #define htonll(x) ntohll(x)
 #endif /* WORDS_BIGENDIAN */
-#endif
+#endif /* HAVE_NTOHLL */
+#endif /* !ntohll && !htonll */
 
 #define DEBUG_INFO   1          /* informational only, lessthan 1 line per packet */
 #define DEBUG_BASIC  2          /* limited debugging, one line per packet */
diff --git a/src/tcpedit/parse_args.c b/src/tcpedit/parse_args.c
index 59647168..be174f81 100644
--- a/src/tcpedit/parse_args.c
+++ b/src/tcpedit/parse_args.c
@@ -50,7 +50,7 @@ tcpedit_post_args(tcpedit_t *tcpedit) {
         char **list = (char**)STACKLST_OPT(PNAT);
         int first = 1;
 
-        tcpedit->rewrite_ip ++;
+        tcpedit->rewrite_ip = true;
 
         do {
             char *p = *list++;
@@ -74,7 +74,7 @@ tcpedit_post_args(tcpedit_t *tcpedit) {
     
     /* --srcipmap */
     if (HAVE_OPT(SRCIPMAP)) {
-        tcpedit->rewrite_ip ++;
+        tcpedit->rewrite_ip = true;
         if (! parse_cidr_map(&tcpedit->srcipmap, OPT_ARG(SRCIPMAP))) {
             tcpedit_seterr(tcpedit, 
                 "Unable to parse --srcipmap=%s", OPT_ARG(SRCIPMAP));
@@ -84,7 +84,7 @@ tcpedit_post_args(tcpedit_t *tcpedit) {
 
     /* --dstipmap */
     if (HAVE_OPT(DSTIPMAP)) {
-        tcpedit->rewrite_ip ++;
+        tcpedit->rewrite_ip = true;
         if (! parse_cidr_map(&tcpedit->dstipmap, OPT_ARG(DSTIPMAP))) {
             tcpedit_seterr(tcpedit, 
                 "Unable to parse --dstipmap=%s", OPT_ARG(DSTIPMAP));
diff --git a/src/tcpedit/plugins/dlt_en10mb/en10mb.c b/src/tcpedit/plugins/dlt_en10mb/en10mb.c
index b773b312..9c95d9a0 100644
--- a/src/tcpedit/plugins/dlt_en10mb/en10mb.c
+++ b/src/tcpedit/plugins/dlt_en10mb/en10mb.c
@@ -372,7 +372,7 @@ dlt_en10mb_decode(tcpeditdlt_t *ctx, const u_char *packet, const int pktlen)
     
     assert(ctx);
     assert(packet);
-    if (pktlen < 14)
+    if (pktlen < TCPR_802_3_H)
         return TCPEDIT_ERROR;
 
     /* get our src & dst address */
@@ -386,6 +386,9 @@ dlt_en10mb_decode(tcpeditdlt_t *ctx, const u_char *packet, const int pktlen)
     /* get the L3 protocol type  & L2 len*/
     switch (ntohs(eth->ether_type)) {
         case ETHERTYPE_VLAN:
+            if (pktlen < TCPR_802_1Q_H)
+                    return TCPEDIT_ERROR;
+
             vlan = (struct tcpr_802_1q_hdr *)packet;
             ctx->proto = vlan->vlan_len;
             
@@ -402,7 +405,6 @@ dlt_en10mb_decode(tcpeditdlt_t *ctx, const u_char *packet, const int pktlen)
         default:
             ctx->proto = eth->ether_type;
             ctx->l2len = TCPR_802_3_H;
-            break;
     }
 
     return TCPEDIT_OK; /* success */
@@ -426,7 +428,7 @@ dlt_en10mb_encode(tcpeditdlt_t *ctx, u_char *packet, int pktlen, tcpr_dir_t dir)
     assert(ctx);
     assert(packet);
 
-    if (pktlen < 14) {
+    if (pktlen < TCPR_802_1Q_H) {
         tcpedit_seterr(ctx->tcpedit, 
                 "Unable to process packet #" COUNTER_SPEC " since it is less then 14 bytes.", 
                 ctx->tcpedit->runtime.packetnum);
@@ -454,6 +456,13 @@ dlt_en10mb_encode(tcpeditdlt_t *ctx, u_char *packet, int pktlen, tcpr_dir_t dir)
         newl2len = config->vlan == TCPEDIT_VLAN_ADD ? TCPR_802_1Q_H : TCPR_802_3_H;
     }
 
+    if (pktlen < newl2len) {
+        tcpedit_seterr(ctx->tcpedit,
+                "Unable to process packet #" COUNTER_SPEC " since its new length less then %d bytes.",
+                ctx->tcpedit->runtime.packetnum, newl2len);
+        return TCPEDIT_ERROR;
+    }
+
     /* Make space for our new L2 header */
     if (newl2len != ctx->l2len)
         memmove(packet + newl2len, packet + ctx->l2len, pktlen - ctx->l2len);
diff --git a/src/tcpedit/portmap.c b/src/tcpedit/portmap.c
index e1da1883..20c738d4 100644
--- a/src/tcpedit/portmap.c
+++ b/src/tcpedit/portmap.c
@@ -346,13 +346,13 @@ rewrite_ports(tcpedit_t *tcpedit, u_char protocol, u_char *layer4)
 
             udp_hdr->uh_sport = newport;
         }
-
     }
+
     return 0;
 }
 
 int
-rewrite_ipv4_ports(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr)
+rewrite_ipv4_ports(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr, const int len)
 {
     assert(tcpedit);
     u_char *l4;
@@ -360,15 +360,16 @@ rewrite_ipv4_ports(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr)
     if (*ip_hdr == NULL) {
         return 0;
     } else if ((*ip_hdr)->ip_p == IPPROTO_TCP || (*ip_hdr)->ip_p == IPPROTO_UDP) {
-        l4 = get_layer4_v4(*ip_hdr, 65536);
-        return rewrite_ports(tcpedit, (*ip_hdr)->ip_p, l4);
+        l4 = get_layer4_v4(*ip_hdr, len);
+        if (l4)
+            return rewrite_ports(tcpedit, (*ip_hdr)->ip_p, l4);
     }
 
     return 0;
 }
 
 int
-rewrite_ipv6_ports(tcpedit_t *tcpedit, ipv6_hdr_t **ip6_hdr)
+rewrite_ipv6_ports(tcpedit_t *tcpedit, ipv6_hdr_t **ip6_hdr, const int len)
 {
     assert(tcpedit);
     u_char *l4;
@@ -376,8 +377,10 @@ rewrite_ipv6_ports(tcpedit_t *tcpedit, ipv6_hdr_t **ip6_hdr)
     if (*ip6_hdr == NULL) {
         return 0;
     } else if ((*ip6_hdr)->ip_nh == IPPROTO_TCP || (*ip6_hdr)->ip_nh == IPPROTO_UDP) {
-        l4 = get_layer4_v6(*ip6_hdr, 65535);
-        return rewrite_ports(tcpedit, (*ip6_hdr)->ip_nh, l4);
+        l4 = get_layer4_v6(*ip6_hdr, len);
+        if (l4)
+            return rewrite_ports(tcpedit, (*ip6_hdr)->ip_nh, l4);
     }
+
     return 0;
 }
diff --git a/src/tcpedit/portmap.h b/src/tcpedit/portmap.h
index 5c910a43..4b583b6b 100644
--- a/src/tcpedit/portmap.h
+++ b/src/tcpedit/portmap.h
@@ -28,7 +28,7 @@ int parse_portmap(tcpedit_portmap_t **portmapdata, const char *ourstr);
 void free_portmap(tcpedit_portmap_t *portmap);
 void print_portmap(tcpedit_portmap_t *portmap);
 long map_port(tcpedit_portmap_t *portmap , long port);
-int rewrite_ipv4_ports(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr);
-int rewrite_ipv6_ports(tcpedit_t *tcpedit, ipv6_hdr_t **ip_hdr);
+int rewrite_ipv4_ports(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr, const int len);
+int rewrite_ipv6_ports(tcpedit_t *tcpedit, ipv6_hdr_t **ip_hdr, const int len);
 
 #endif
diff --git a/src/tcpedit/tcpedit.c b/src/tcpedit/tcpedit.c
index 28abac97..20ffcc84 100644
--- a/src/tcpedit/tcpedit.c
+++ b/src/tcpedit/tcpedit.c
@@ -90,28 +90,33 @@ tcpedit_packet(tcpedit_t *tcpedit, struct pcap_pkthdr **pkthdr,
     int l2len = 0, l2proto, retval = 0, dst_dlt, src_dlt, pktlen, lendiff;
     int ipflags = 0, tclass = 0;
     int needtorecalc = 0;           /* did the packet change? if so, checksum */
-    u_char *packet = *pktdata;
+    u_char *packet;
+
     assert(tcpedit);
     assert(pkthdr);
     assert(*pkthdr);
     assert(pktdata);
-    assert(packet);
+    assert(*pktdata);
     assert(tcpedit->validated);
 
+    packet = *pktdata;
 
     tcpedit->runtime.packetnum++;
+
     dbgx(3, "packet " COUNTER_SPEC " caplen %d", 
             tcpedit->runtime.packetnum, (*pkthdr)->caplen);
 
-
     /*
      * remove the Ethernet FCS (checksum)?
      * note that this feature requires the end user to be smart and
      * only set this flag IFF the pcap has the FCS.  If not, then they
      * just removed 2 bytes of ACTUAL PACKET DATA.  Sucks to be them.
      */
-    if (tcpedit->efcs > 0) {
-        (*pkthdr)->caplen -= 4;
+    if (tcpedit->efcs > 0 &&(*pkthdr)->len > 4) {
+        if ((*pkthdr)->caplen == (*pkthdr)->len) {
+            (*pkthdr)->caplen -= 4;
+        }
+
         (*pkthdr)->len -= 4;
     }
 
@@ -179,7 +184,7 @@ tcpedit_packet(tcpedit_t *tcpedit, struct pcap_pkthdr **pkthdr,
 
         /* rewrite TCP/UDP ports */
         if (tcpedit->portmap != NULL) {
-            if ((retval = rewrite_ipv4_ports(tcpedit, &ip_hdr)) < 0)
+            if ((retval = rewrite_ipv4_ports(tcpedit, &ip_hdr, (*pkthdr)->caplen)) < 0)
                 return TCPEDIT_ERROR;
         }
     }
@@ -216,7 +221,7 @@ tcpedit_packet(tcpedit_t *tcpedit, struct pcap_pkthdr **pkthdr,
 
         /* rewrite TCP/UDP ports */
         if (tcpedit->portmap != NULL) {
-            if ((retval = rewrite_ipv6_ports(tcpedit, &ip6_hdr)) < 0)
+            if ((retval = rewrite_ipv6_ports(tcpedit, &ip6_hdr, (*pkthdr)->caplen)) < 0)
                 return TCPEDIT_ERROR;
         }
     }
diff --git a/src/tcpliveplay.c b/src/tcpliveplay.c
index 27bc54be..4763f885 100644
--- a/src/tcpliveplay.c
+++ b/src/tcpliveplay.c
@@ -1186,7 +1186,7 @@ do_checksum_liveplay(u_int8_t *data, int proto, int len) {
     ipv4_hdr *ipv4;
     tcp_hdr *tcp;
     int ip_hl;
-    volatile int sum;   // <-- volatile works around a PPC g++ bug
+    volatile int sum = 0;   // <-- volatile works around a PPC g++ bug
 
     sum;
     ipv4 = NULL;
diff --git a/src/tcpprep.c b/src/tcpprep.c
index 638b2998..798b8456 100644
--- a/src/tcpprep.c
+++ b/src/tcpprep.c
@@ -290,7 +290,6 @@ check_ipv4_regex(const unsigned long ip)
     } else {
         return 0;
     }
-
 }
 
 static int
@@ -355,9 +354,6 @@ process_raw_packets(pcap_t * pcap)
          * it as a non-IP packet, UNLESS we're in MAC mode, in which case
          * we should let the MAC matcher below handle it
          */
-
-        eth_hdr = (eth_hdr_t *)pktdata;
-
         if (options->mode != MAC_MODE) {
             dbg(3, "Looking for IPv4/v6 header in non-MAC mode");
             
@@ -368,17 +364,12 @@ process_raw_packets(pcap_t * pcap)
             if ((ip_hdr = (ipv4_hdr_t *)get_ipv4(pktdata, pkthdr.caplen, 
                     pcap_datalink(pcap), &buffptr))) {
                 dbg(2, "Packet is IPv4");
-                    
-            } 
-            
-            /* then look for IPv6 */
-            else if ((ip6_hdr = (ipv6_hdr_t *)get_ipv6(pktdata, pkthdr.caplen,
+            } else if ((ip6_hdr = (ipv6_hdr_t *)get_ipv6(pktdata, pkthdr.caplen,
                     pcap_datalink(pcap), &buffptr))) {
+                /* IPv6 */
                 dbg(2, "Packet is IPv6");    
-            } 
-            
-            /* we're something else... */
-            else {
+            } else {
+                /* we're something else... */
                 dbg(2, "Packet isn't IPv4/v6");
 
                 /* we don't want to cache these packets twice */
@@ -392,6 +383,10 @@ process_raw_packets(pcap_t * pcap)
             }
     
             l2len = get_l2len(pktdata, pkthdr.caplen, pcap_datalink(pcap));
+            if (l2len < 0) {
+                /* go to next packet */
+                continue;
+            }
 
             /* look for include or exclude CIDR match */
             if (options->xX.cidr != NULL) {
@@ -442,6 +437,12 @@ process_raw_packets(pcap_t * pcap)
 
         case MAC_MODE:
             dbg(2, "processing mac mode...");
+            if (pkthdr.caplen < sizeof(*eth_hdr)) {
+                dbg(2, "capture length too short for mac mode processing");
+                break;
+            }
+
+            eth_hdr = (eth_hdr_t *)pktdata;
             direction = macinstring(options->maclist, (u_char *)eth_hdr->ether_shost);
 
             /* reverse direction? */
@@ -456,15 +457,15 @@ process_raw_packets(pcap_t * pcap)
             /* first run through in auto mode: create tree */
             if (options->automode != FIRST_MODE) {
                 if (ip_hdr) {
-                    add_tree_ipv4(ip_hdr->ip_src.s_addr, pktdata);
+                    add_tree_ipv4(ip_hdr->ip_src.s_addr, pktdata, pkthdr.caplen);
                 } else if (ip6_hdr) {
-                    add_tree_ipv6(&ip6_hdr->ip_src, pktdata);
+                    add_tree_ipv6(&ip6_hdr->ip_src, pktdata, pkthdr.caplen);
                 }
             } else {
                 if (ip_hdr) {
-                    add_tree_first_ipv4(pktdata);
+                    add_tree_first_ipv4(pktdata, pkthdr.caplen);
                 } else if (ip6_hdr) {
-                    add_tree_first_ipv6(pktdata);
+                    add_tree_first_ipv6(pktdata, pkthdr.caplen);
                 }
             }  
             break;
diff --git a/src/tree.c b/src/tree.c
index 3b1286f2..4aa0be00 100644
--- a/src/tree.c
+++ b/src/tree.c
@@ -41,7 +41,7 @@ extern int debug;
 char tree_print_buff[TREEPRINTBUFFLEN]; 
 
 static tcpr_tree_t *new_tree();
-static tcpr_tree_t *packet2tree(const u_char *);
+static tcpr_tree_t *packet2tree(const u_char *, const int);
 #ifdef DEBUG        /* prevent compile warnings */
 static char *tree_print(tcpr_data_tree_t *);
 static char *tree_printnode(const char *, const tcpr_tree_t *);
@@ -370,12 +370,18 @@ check_ip6_tree(const int mode, const struct tcpr_in6_addr *addr)
  * client, if the DST IP doesn't exist in the TREE, we add it as a server
  */
 void
-add_tree_first_ipv4(const u_char *data)
+add_tree_first_ipv4(const u_char *data, const int len)
 {
-    tcpr_tree_t *newnode = NULL, *findnode;
+    tcpr_tree_t *newnode, *findnode;
     ipv4_hdr_t ip_hdr;
     
     assert(data);
+
+    if (len < (TCPR_ETH_H + TCPR_IPV4_H)) {
+        errx(-1, "Capture length %d too small for IPv4 parsing", len);
+        return;
+    }
+
     /* 
      * first add/find the source IP/client 
      */
@@ -418,12 +424,18 @@ add_tree_first_ipv4(const u_char *data)
 }
 
 void
-add_tree_first_ipv6(const u_char *data)
+add_tree_first_ipv6(const u_char *data, const int len)
 {
-    tcpr_tree_t *newnode = NULL, *findnode;
+    tcpr_tree_t *newnode, *findnode;
     ipv6_hdr_t ip6_hdr;
 
     assert(data);
+
+    if (len < (TCPR_ETH_H + TCPR_IPV6_H)) {
+        errx(-1, "Capture length %d too small for IPv6 parsing", len);
+        return;
+    }
+
     /*
      * first add/find the source IP/client
      */
@@ -516,43 +528,47 @@ add_tree_node(tcpr_tree_t *newnode)
  * - the way the host acted the first time we saw it (client or server)
  */
 void
-add_tree_ipv4(const unsigned long ip, const u_char * data)
+add_tree_ipv4(const unsigned long ip, const u_char * data, const int len)
 {
-    tcpr_tree_t *newnode = NULL;
+    tcpr_tree_t *newnode;
     assert(data);
 
-    newnode = packet2tree(data);
+    newnode = packet2tree(data, len);
+    if (newnode) {
+        assert(ip == newnode->u.ip);
 
-    assert(ip == newnode->u.ip);
+        if (newnode->type == DIR_UNKNOWN) {
+            /* couldn't figure out if packet was client or server */
 
-    if (newnode->type == DIR_UNKNOWN) {
-        /* couldn't figure out if packet was client or server */
+            dbgx(2, "%s (%lu) unknown client/server",
+                    get_addr2name4(newnode->u.ip, RESOLVE), newnode->u.ip);
 
-        dbgx(2, "%s (%lu) unknown client/server",
-            get_addr2name4(newnode->u.ip, RESOLVE), newnode->u.ip);
+        }
 
+        add_tree_node(newnode);
     }
-    add_tree_node(newnode);
 }
 
 void
-add_tree_ipv6(const struct tcpr_in6_addr * addr, const u_char * data)
+add_tree_ipv6(const struct tcpr_in6_addr * addr, const u_char * data,
+        const int len)
 {
-    tcpr_tree_t *newnode = NULL;
+    tcpr_tree_t *newnode;
     assert(data);
 
-    newnode = packet2tree(data);
+    newnode = packet2tree(data, len);
+    if (newnode) {
+        assert(ipv6_cmp(addr, &newnode->u.ip6) == 0);
 
-    assert(ipv6_cmp(addr, &newnode->u.ip6) == 0);
+        if (newnode->type == DIR_UNKNOWN) {
+            /* couldn't figure out if packet was client or server */
 
-    if (newnode->type == DIR_UNKNOWN) {
-        /* couldn't figure out if packet was client or server */
+            dbgx(2, "%s unknown client/server",
+                    get_addr2name6(&newnode->u.ip6, RESOLVE));
+        }
 
-        dbgx(2, "%s unknown client/server",
-            get_addr2name6(&newnode->u.ip6, RESOLVE));
+        add_tree_node(newnode);
     }
-
-    add_tree_node(newnode);
 }
 
 /**
@@ -679,8 +695,8 @@ new_tree()
  * if it's an undefined packet, we return -1 for the type
  * the u_char * data should be the data that is passed by pcap_dispatch()
  */
-tcpr_tree_t *
-packet2tree(const u_char * data)
+static tcpr_tree_t *
+packet2tree(const u_char * data, const int len)
 {
     tcpr_tree_t *node = NULL;
     eth_hdr_t *eth_hdr = NULL;
@@ -697,6 +713,11 @@ packet2tree(const u_char * data)
     char srcip[INET6_ADDRSTRLEN];
 #endif
 
+    if (len < sizeof(*eth_hdr)) {
+        errx(-1, "packet capture length %d too small to process", len);
+        return NULL;
+    }
+
     node = new_tree();
 
     eth_hdr = (eth_hdr_t *) (data);
@@ -714,6 +735,13 @@ packet2tree(const u_char * data)
     }
 
     if (ether_type == htons(ETHERTYPE_IP)) {
+        if (len < (TCPR_ETH_H + hl + TCPR_IPV4_H)) {
+            safe_free(node);
+            errx(-1, "packet capture length %d too small for IPv4 processing",
+                    len);
+            return NULL;
+        }
+
         memcpy(&ip_hdr, (data + TCPR_ETH_H + hl), TCPR_IPV4_H);
 
         node->family = AF_INET;
@@ -726,6 +754,13 @@ packet2tree(const u_char * data)
                     RESOLVE), 16);
 #endif
     } else if (ether_type == htons(ETHERTYPE_IP6)) {
+        if (len < (TCPR_ETH_H + hl + TCPR_IPV6_H)) {
+            safe_free(node);
+            errx(-1, "packet capture length %d too small for IPv6 processing",
+                    len);
+            return NULL;
+        }
+
         memcpy(&ip6_hdr, (data + TCPR_ETH_H + hl), TCPR_IPV6_H);
 
         node->family = AF_INET6;
diff --git a/src/tree.h b/src/tree.h
index f3e8c46d..f7478e77 100644
--- a/src/tree.h
+++ b/src/tree.h
@@ -53,10 +53,10 @@ typedef struct tcpr_buildcidr_s {
 
 #define DNS_QUERY_FLAG 0x8000
 
-void add_tree_ipv4(const unsigned long, const u_char *);
-void add_tree_ipv6(const struct tcpr_in6_addr *, const u_char *);
-void add_tree_first_ipv4(const u_char *);
-void add_tree_first_ipv6(const u_char *);
+void add_tree_ipv4(const unsigned long, const u_char *, const int);
+void add_tree_ipv6(const struct tcpr_in6_addr *, const u_char *, const int);
+void add_tree_first_ipv4(const u_char *, const int);
+void add_tree_first_ipv6(const u_char *, const int);
 tcpr_dir_t check_ip_tree(const int, const unsigned long);
 tcpr_dir_t check_ip6_tree(const int, const struct tcpr_in6_addr *);
 int process_tree();
