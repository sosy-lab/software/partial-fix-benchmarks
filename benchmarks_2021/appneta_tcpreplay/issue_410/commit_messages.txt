fix some small gcc 7 warnings (#403)

* fix -Wbool-operation warnings found by gcc-7

Signed-off-by: Gabriel Ganne <gabriel.ganne@enea.com>

* fix -Wmemset-elt-size warning found by gcc-7

Signed-off-by: Gabriel Ganne <gabriel.ganne@enea.com>

* fix -Wuninitialized warning found by gcc-7

Signed-off-by: Gabriel Ganne <gabriel.ganne@enea.com>
fix heap-buffer-overflow in flow_decode()

for all datalinks, check whether caplen > l2 header len
this should fix issues #407 #408 #409 #410

Signed-off-by: Gabriel Ganne <gabriel.ganne@enea.com>
fix heap-buffer-overflow in flow_decode()

for all datalinks, check whether caplen > l2 header len
this should fix issues #407 #408 #409 #410

Signed-off-by: Gabriel Ganne <gabriel.ganne@enea.com>
#407 #408 #409 #410 create branch for downstream PR
#410 check packet length in get_l2protocol
Merge pull request #438 from appneta/Bug_#410_heap_buffer_overflow_get_l2protocol

Bug #410 heap buffer overflow get_l2protocol
Merge branch '4.3' into PR_#397_multi_pcap_sleep_calc

* 4.3:
  #409 check packet length in packet2tree functions (#440)
  #408 check packet length in get_l2len
  #410 check packet length in get_l2protocol
  #432 restore config.h.in
  #407 #408 #409 #410 create branch for downstream PR
  Bug #432 Attempt to use local libopts to fix Travis CI (#433)
  fix heap-buffer-overflow in flow_decode()
  Bug #430 - properly parse libpcap version numbers
  fix some small gcc 7 warnings (#403)
  fix -Wmemset-elt-size warning found by gcc-7
  fix -Wbool-operation warnings found by gcc-7

Conflicts:
	configure.ac
	docs/CHANGELOG
