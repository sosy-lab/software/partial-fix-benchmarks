with multiplier option, only first file can be sent and hang
fixed in PR #508 
Reopening to fix regression. The `--mbps` option is not working properly when using `--loop` option.

Example
```
tcpreplay -i ens33 -l100  -M 1 ping_reply.pcap  
Actual: 400 packets (39200 bytes) sent in 0.025010 seconds
Rated: 1567373.0 Bps, 12.53 Mbps, 15993.60 pps
Statistics for network device: ens33
	Successful packets:        400
	Failed packets:            0
	Truncated packets:         0
	Retried packets (ENOBUFS): 0
	Retried packets (EAGAIN):  0
```
PR #519 fixes `--mbps` and `--pps` options while not affecting `--multiplier` option.
This still isn't working for me.

I have a pcap which sends a SYN packet every 10 microseconds.  It contains 12200 packets.

When I set --multiplier=0.0000002 and replay the pcap, a few packets are sent and then 100 seconds later at the end of the replay, the remaining 12000~ are sent in under a second.  This multiplier is not slowing down the packet sending in my pcap.

Despite this, tcpreplay output says it was sending 116pps which is incorrect.


Reopening to investigate. I suspect that 0.0000002 is rounded down to 0 and is causing problems.
Fixed in PR #446