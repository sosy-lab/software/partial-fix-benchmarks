4.3 - revert travis updates from merge
Bug #484 CVE-2018-17582 Check for corrupt PCAP files

* Check for packets that are larger than 262144 bytes
* Check for capture lengths that are greater than packet length

Example of a corrupt PCAP file ...

sudo src/tcpreplay -i ens33 --unique-ip -t --loop 4 get_next_paket_01
safe_pcap_next ERROR: Invalid packet length in send_packets.c:get_next_packet() line 1054: 8388670 is greater than maximum 262144
Merge pull request #491 from appneta/Bug_#484_heap_overflow_get_next_packet

Bug #484 CVE-2018-17582 Check for corrupt PCAP files
Bug #486 CVE-2018-17974 realloc memory if packet size increases

Also added check for packet size > cap len, although this may
be never be hit since #484
Merge branch 'Enhancement_#493_codacy_fixes' into coverity_scan

* Enhancement_#493_codacy_fixes: (26 commits)
  Enhancement #493 - fixes for Codacy identified issues
  Bug #486 Enforce max snaplen rather than doing realloc
  Bug #486 CVE-2018-17974 realloc memory if packet size increases
  Bug #484 CVE-2018-17582 Check for corrupt PCAP files
  4.3 - revert travis updates from merge
  Remove dead code
  resolve possible null pointer dereference
  travis-ci: add autogen package
  Bug #461 build warnings (#462)
  #412 fix gcc 6.3 compiler warning
  #421 fix ms to ns conversion
  Bug #423 remove commented code
  Bug #423 Remove limit for tcpprep -S
  Bug #398 Rewrite of tcpdump.c (#457)
  Bug #402 memset dlt radiotap get 80211 (#454)
  #404 fix check_list return values (#453)
  #406 fix zero-length IP headers
  #416 apply STDIN restore to all programs
  #416 fix compile issue introduced by downstream PR
  #416 update CHANGELOG [ci skip]
  ...
Bug #486 CVE-2018-17974 realloc memory if packet size increases (#492)

* Bug #486 CVE-2018-17974 realloc memory if packet size increases

Also added check for packet size > cap len, although this may
be never be hit since #484

* Bug #486 Enforce max snaplen rather than doing realloc

* increase MAX_SNAPLEN from 65535 to 262144
* increase MAXPACKET from 65549 to 262158
* exit on buffer overflow for adding VLAN tag (as opposed to realloc)
Bug #485 Heap overflow fixed in #484

Getting the following error message when attempting to reproduce bug:

tcpreplay -i ens33 --unique-ip -t --loop 4 fast_edit_package_02
safe_pcap_next ERROR: Invalid packet length in send_packets.c:get_next_packet() line 1054: packet length 28 is less than capture length 60
Merge pull request #499 from appneta/Bug_#485_heap_overflow_fast_edit_packet

Bug #485 Heap overflow fixed in #484
Merge branch '4.3' into Bug_#418_2nd_packet_no_delay

* 4.3: (22 commits)
  Bug #418 don't ignore 2nd packet timing
  Bug #411 allow TAP on all platforms
  Bug #174 ensure --with-testnic does not affect replay
  Bug #406 change packet length to network order
  Bug #413 fix manpage typos
  Bug #485 Heap overflow fixed in #484
  Enhancement_#482 update CHANGELOG/CREDITS
  Enhancement_#482 test Makefile merge error fixup
  Enhancement_#482 test Makefile cleanup
  Bug #489 free after memcpy
  Bug #488 heap overflow csum replace4 (#496)
  Bug #486 CVE-2018-17974 realloc memory if packet size increases (#492)
  Enhancement #493 - fixes for Codacy identified issues
  Bug #486 Enforce max snaplen rather than doing realloc
  Bug #486 CVE-2018-17974 realloc memory if packet size increases
  Bug #484 CVE-2018-17582 Check for corrupt PCAP files
  4.3 - revert travis updates from merge
  Simplify plugin Makefiles
  allow out-of-tree build
  Remove dead code
  ...
Merge branch '4.3' into Enhancement_#425_edit_tcp_seq-ack_numbers_staging

* 4.3: (36 commits)
  Enhancement #506 disable C99 and fix warnings (#507)
  Bug #418 don't ignore 2nd packet timing
  Bug #411 allow TAP on all platforms
  Bug #174 ensure --with-testnic does not affect replay
  Bug #406 change packet length to network order
  Bug #413 fix manpage typos
  Bug #485 Heap overflow fixed in #484
  Enhancement_#482 update CHANGELOG/CREDITS
  Enhancement_#482 test Makefile merge error fixup
  Enhancement_#482 test Makefile cleanup
  Bug #489 free after memcpy
  Bug #488 heap overflow csum replace4 (#496)
  Bug #486 CVE-2018-17974 realloc memory if packet size increases (#492)
  Enhancement #493 - fixes for Codacy identified issues
  Bug #486 Enforce max snaplen rather than doing realloc
  Bug #486 CVE-2018-17974 realloc memory if packet size increases
  Bug #484 CVE-2018-17582 Check for corrupt PCAP files
  4.3 - revert travis updates from merge
  Simplify plugin Makefiles
  allow out-of-tree build
  ...
