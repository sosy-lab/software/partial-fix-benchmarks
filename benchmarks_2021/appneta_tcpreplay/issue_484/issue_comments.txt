Heap overflow in get_next_packet()
https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2018-17582 was assigned for this issue
I don't think the analysis is correct. This has nothing to do with null destination pointer (`safe_malloc()` checks return value of `malloc()` and exits on allocation failure) and most likely not with the destination at all.

IMHO the actual problem here is a buffer overflow on *read* because of unrealistic value of `pktlen` (taken from `pkthdr->len`) which is 8388670 while the data actually present is much shorter (65535). The same problem could probably happen with real life pcap files taken with tcpdump `-s` option. We should probably check if `pkthdr->caplen` is shorter than `pkthdr->len` and either drop the packet with a warning or bail out completely.

After all, it does not make much sense to replay packets if we only have leading parts of them. We might perhaps fill the missing part with zeros or random content but that would mostly result in incorrect checksums.
I agree with @mkubecek in that the original analysis is incorrect. There is a check for the condition where `dst` is non-zero.

That said, there can be additional checks for cases where `libpcap` returns invalid data. Specifically, need to add checks for packets greater than maximum packet size (65535) and for capture lengths greater than packet length.

By adding checks to all calls to `pcap_next()`, can fix several potential heap overflows such as #477. 
fixed in PR #491 