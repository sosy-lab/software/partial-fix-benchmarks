CALL and JMP with rel16 argument must mask RIP with 0xffff
is this true? note that Capstone converts all target addresses to absolute addresses.

Yes, and it's the absolute addresses which appear wrong. Just see the Operation section of Intel's Instruction Set Reference for `JMP` and `CALL` instructions. There they explicitly write for e.g. near jumps (not to be confused with short ones!):

```
...
IF OperandSize = 16
    THEN (* OperandSize = 16 *)
        EIP<-tempEIP AND 0000FFFFH
    ELSE (* OperandSize = 64 *)
        RIP<-tempRIP;
FI;
...
```

fixed. please confirm, thanks.

Not fixed for `0xfc26: E9 35 64` in 16 bit mode. It's `jmp 0x605e`, but disassembled as `jmp 0x1605e`.

can you confirm now with the latest fix?

No, now `0xfff6: 66 E9 35 64 93 53` in 16-bit mode and `0x9123fff1: E9 35 64 93 53` in 32 bit mode as well as `0x649123fff1: E9 35 64 93 53` in 64 bit mode are broken. They shouldn't mask resulting RIP.

just pushed a fix. can you check all your tests again now?

The following still doesn't work:
- `0xffe1: 66 e8 35 64 93 53 call 0x5394641c` in 16 bit mode. Capstone loses carry from bit 15 to bit 16, reporting target address of `0x5393641c`.
- `0x649123ffe1: 66 e8 35 64 call 0x641a` in 64 bit mode. I'd expect immediate operand size to be 16 bit, but Capstone says 32 bit. (Note that is an unsupported instruction in 64 bit mode, according to Intel docs, but we still want some consistency in output, don't we?)
- `0x649123ffe1: 66 e9 35 64 jmp 0x641a` in 64 bit mode. Same as above
- `0xffe1: 66 e9 35 64 93 53 jmp 0x5394641c` in 16 bit mode. Capstone says `jmp 0x641c`.

`0xfff6: 66 E9 35 64 93 53 in 16-bit mode`     <-- what is the expected output here?

```
jmp 0x53946431
```

`Not fixed for 0xfc26: E9 35 64 in 16 bit mode. It's jmp 0x605e, but disassembled as jmp 0x1605e.`

are you sure with the above? `E9 35 64` is **JMP rel32**, so the output should be `jmp 0x1605e` because there is no need to mask `0xffff`. 

`0xffe1: 66 e8 35 64 93 53 call 0x5394641c in 16 bit mode. Capstone loses carry from bit 15 to bit 16, reporting target address of 0x5393641c.`

can you confirm the expected output of above code?

It's 16-bit mode, and no `66` prefix is used. Thus `OperandSize==16` (it's to previous comment).

so `0xffe1: 66 e8 35 64 93 53` = **call 0x641c**.

how about another the code above regarding "JMP rel32" ?

> can you confirm the expected output of above code?

Yes, the expected output seems to be correct. What exactly makes you doubt?

`0xffe1: 66 e8 35 64 93 53` in 16-bit mode has `66` prefix, thus operand size is 32 bits. Thus this is `jmp rel32` and follows rules for such jumps, i.e. no masking.
`E9 35 64` in 16 bit mode has no operand size override, thus operand size is 16 bits. Thus masking must be applied to target address.

@10110111 is right. That is, Prefix 0x66 switches between 16-bit and 32-bit to be opposite to the default operand size in CS segment/selector.

Same for 0x67 but to be opposite to the default address size in the segment/selector associated to the memory operand of the instruction. With no segment/selector prefix, it is _DS_ by default or _SS_ for implicit stack memory operands (_xSP_, _xBP_) or stack instructions.

can you test the latest code in "master" branch?

to keep track of bugs being fixed, i added a new script `suite/regress.py`, so feel free to add any bugs you find in this file, then send a pull-request.

thanks

The disassembly strings now seem OK, but why are these cases reported as having 32 bit operand?

```
Intel64: 0x649123ffe1: 66 e8 35 64      call 0x641a
Intel64: 0x649123ffe1: 66 e9 35 64      jmp  0x641a
```

It seems they should have 16 bit one.

this issue with the size of immediate operands has been thoroughly fixed in the "next" branch. so if you really need this, please consider using the code from the "next" branch instead. (fyi, "next" branch code is quite stable)

will fix this in the "master" branch when i have more time. 

Just tried with the `next` branch, but the two cases mentioned above are still the same.

Also, should I even try to test `master` in the future? Or is it going to be dropped soon?

fixed in the latest commit in the "next" branch.

the "next" branch will be released as v4.0 in the future, while "master" branch is for 3.x series. some less-important bugfixes will be ported back to "master" branch when i have more free time.

thanks.

OK, seems to finally be fixed in `next`.
