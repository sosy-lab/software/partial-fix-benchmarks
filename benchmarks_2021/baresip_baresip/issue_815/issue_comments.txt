Remote Control: Missing explicit UA and call selection for commands
yes I think it is a good idea to explicitly specify the UA account.

we currently have the concept of a "current" UA, but perhaps it is time to get rid of it ?

Alfred E. Heggestad writes:

> yes I think it is a good idea to explicitly specify the UA account.
> 
> we currently have the concept of a "current" UA, but perhaps it is
> time to get rid of it ?

My baresip app is making one uag_current_set() call, but I don't
anymore remember what its purpose is.  Perhaps none.

getting rid of "current" UA is a big task but not impossible :)

the current UA is mostly needed when making an outgoing call.

some ideas:

1. when the number of UAs is 1, this UA will be used for all operations

2. when using multiple UAs we can add a `default` flag to indicate the default UA.

3. when dialing to a domain, the target domain can be matched against the domain
of all UAs. if a match is found that UA will be used.

here is the function that has to be removed:

```
struct ua   *uag_current(void);
```

try to grep for this in the code 
It is not only the "current" UA but also the "current" call (or line-id) of the UA :-)

I think there is no need to remove the "current"-concept in general.
But some optional specific command parameters could help a lot.
So maybe adding an optional `aor` parameter to the `dial` command and a `id` parameter to the `hangup` command for example would help a lot. If they are specified they should override the "current" selection.

So for my example from above this would mean something like this:
`/dial sip:+4912345678901 sip:123456789@192.168.0.10` and
`/hangup sip:123456789@192.168.0.10 04ce3b08af9e91254b5d16cae000`

In my case all UAs are on the same domain so they can not be distinguished just by the domain part. The whole AOR has to match.
what you can try is to run the `uafind` command in front of e.g. dial

```
  /uafind ..                   Find User-Agent <aor>
  /dial ..            d ..     Dial
```

```
/uafind sip:123456789@192.168.0.10
/dial sip:+4912345678901 
```

if you run the application yourself, you should be in control of which
user interfaces are enabled. for example you can disable stdio.so.
the incoming commands from MQTT should be executed in sequence,
as they are processed in the main thread. there should not be
any race conditions.

we could add explicit UA to some commands but we have to find out
on which level. The command system is split into 2 logical blocks:

1. Raw commands
2. Commands via a protocol (e.g. MQTT)

if we add aor= and callid= to 1.) then it can be re-used also by e.g. stdio.so
if we add it to 2.) then it cannot be reused by stdio.so




> what you can try is to run the `uafind` command in front of e.g. dial
> ```
> /uafind sip:123456789@192.168.0.10
> /dial sip:+4912345678901 
> ```
This is exactly what I do at the moment but even this MAY be disturbed by a background job which rolls through all UAs (32 in my testcase here with MADI audiointerface) and requests the list of active calls the same way.

> we could add explicit UA to some commands but we have to find out
> on which level. The command system is split into 2 logical blocks:
> 
>     1. Raw commands
> 
>     2. Commands via a protocol (e.g. MQTT)
> 
> 
> if we add aor= and callid= to 1.) then it can be re-used also by e.g. stdio.so
> if we add it to 2.) then it cannot be reused by stdio.so

Anyway it is important that the published callids are unique across all UAs and that they match in lists (pull) and events (push). Currently there is a concept of line numbers per UA in lists and unique(?) ids in events and log.

I think the "current" implementation is ok for stdio.so as simple human interface but the remote control interface might have other requirements as it may be the base for advanced UIs or automation in any way.
here is a list of UI/Ctrl modules:

- stdio
- cons
- http
- mqtt
- ctrl_tcp

each of these should/could have its own `struct ui`

we could find a way to transport the ua/call context from
ui module via ui/command to the actual command handler.
here is a WIP patch:

```patch
diff --git a/modules/debug_cmd/debug_cmd.c b/modules/debug_cmd/debug_cmd.c
index 136e0cb..3a993a2 100644
--- a/modules/debug_cmd/debug_cmd.c
+++ b/modules/debug_cmd/debug_cmd.c
@@ -70,10 +70,11 @@ static int cmd_config_print(struct re_printf *pf, void *unused)
 }
 
 
-static int cmd_ua_debug(struct re_printf *pf, void *unused)
+static int cmd_ua_debug(struct re_printf *pf, void *arg)
 {
-	const struct ua *ua = uag_current();
-	(void)unused;
+	struct cmd_arg *carg = arg;
+	const char *aor = carg->data;
+	const struct ua *ua = uag_find_aor(aor);
 
 	if (ua)
 		return ua_debug(pf, ua);
diff --git a/modules/mqtt/subscribe.c b/modules/mqtt/subscribe.c
index 340eb63..5d12297 100644
--- a/modules/mqtt/subscribe.c
+++ b/modules/mqtt/subscribe.c
@@ -24,6 +24,7 @@ static void handle_command(struct mqtt *mqtt, const struct pl *msg)
 	struct re_printf pf = {print_handler, resp};
 	struct odict *od = NULL;
 	const struct odict_entry *oe_cmd, *oe_prm, *oe_tok;
+	const char *aor;
 	char buf[256], resp_topic[256];
 	int err;
 
@@ -42,6 +43,8 @@ static void handle_command(struct mqtt *mqtt, const struct pl *msg)
 		goto out;
 	}
 
+	aor = odict_string(od, "accountaor");
+
 	debug("mqtt: handle_command:  cmd='%s', token='%s'\n",
 	      oe_cmd ? oe_cmd->u.str : "",
 	      oe_tok ? oe_tok->u.str : "");
@@ -55,7 +58,7 @@ static void handle_command(struct mqtt *mqtt, const struct pl *msg)
 	err = cmd_process_long(baresip_commands(),
 			       buf,
 			       str_len(buf),
-			       &pf, NULL);
+			       &pf, (void *)aor);
 	if (err) {
 		warning("mqtt: error processing command (%m)\n", err);
 	}
```

1. the JSON element `accountaor` is extracted from the mqtt message
2. the accountaor string is passed to the command system via "data" (opaque)
3. the example command "cmd_ua_debug" use the aor to find the correct UA.

something similar can also be done for callid.

I also have a different patch that adds a new optional parameter to
selected commands, to use that specific useragent.

the syntax is something like this:

`/dial ua=sip:user@example.com 1234`

with this option we have 2 different options that we can try out:

1. add an optional `aor` parameter to all commands in MQTT.
this will not affect the other UI/CTRL modules

2. add an optional `aor` parameter to selected commands that is relevant for UA.
this will affect all UI/CTRL modules, incl. stdio and mqtt.


I would suggest to add the optional `accountaor` (and `id`) parameters to all "software" remote control paths with priority. I think this is only JSON MQTT and JSON ctrl_tcp (netstring) for the moment (more could be added as module in future). But if it is easier to add it to all paths/modules then add it to all paths.

As the `uafind` works fine as a workaround for an `accountaor` parameter the more urgent thing is to add the `id` to all call-related commands, especially `hangup` as there is no workaround.
is there a consensus for implementing option 1.) ?

NOTE: both ua and call will be added. we are now trying to establish how in
the baresip framework it will be solved, possibly affecting a lot of code.
the chosen solution will also be used for the long term future, we will not
revisit this problem again in 6 months.

Maybe we should ask @jmillan and @oej for this ?
> is there a consensus for implementing option 1.) ?

As long as this includes both MQTT and ctrl_tcp, yes.
I’m not sure about adding aor in command. 
/uafind is cleaner and faster when you have to dial multiple extensions. 
Like this:
/uafind sip:123456789@192.168.0.10
/dial 100
/dial 101
/dial 102

We don’t need to call uag_find_aor each time.
Maybe we just need to implement /callfind ID ?
@GGGO I agree with you.

Just to clarify, the aor parameter is optional and if missing the current UA will be used.

here is my patch for reference:

```patch
diff --git a/modules/debug_cmd/debug_cmd.c b/modules/debug_cmd/debug_cmd.c
index 136e0cb..3f72cdd 100644
--- a/modules/debug_cmd/debug_cmd.c
+++ b/modules/debug_cmd/debug_cmd.c
@@ -70,10 +70,51 @@ static int cmd_config_print(struct re_printf *pf, void *unused)
 }
 
 
-static int cmd_ua_debug(struct re_printf *pf, void *unused)
+static int split_params(struct ua **uap, struct cmd_arg *carg)
 {
-	const struct ua *ua = uag_current();
-	(void)unused;
+	static char buf[256];      /* NOTE */
+	struct pl pl_ua, pl_prm;
+	struct ua *ua;
+	char aor[256];
+	int err;
+
+	re_printf("input prm: '%s'\n", carg->prm);
+
+	if (0 == re_regex(carg->prm, str_len(carg->prm),
+			  "ua=[^ ]+[ \t]*[^]+", &pl_ua, NULL, &pl_prm)) {
+
+		re_printf("ua: '%r'\n", &pl_ua);
+
+		err  = pl_strcpy(&pl_ua, aor, sizeof(aor));
+		err |= pl_strcpy(&pl_prm, buf, sizeof(buf));
+		if (err)
+			return err;
+
+		ua = uag_find_aor(aor);
+		if (!ua) {
+			warning("ua not found (%s)\n", aor);
+			return ENOENT;
+		}
+
+		*uap = ua;
+		carg->prm = buf;
+	}
+
+	return 0;
+}
+
+
+static int cmd_ua_debug(struct re_printf *pf, void *arg)
+{
+	struct cmd_arg *carg = arg;
+	struct ua *ua = uag_current();
+	int err;
+
+	err = split_params(&ua, carg);
+	if (err)
+		return err;
+
+	info("filtered param: '%s'\n", carg->prm);
 
 	if (ua)
 		return ua_debug(pf, ua);
```

If I understand correctly, split_params will be called each time we call /dial 100, right ?
I don't know about the re_regex performance but I would prefer to not have split_params implemented in commands.
I think this kind of logic should stay in ui/ctrl module.
ref: https://github.com/alfredh/baresip/issues/51#issuecomment-152301084
it looks like there is now consensus for solution 1.) -- i.e. add ua/call lookup
in the ui/ctrl module.

that means, no changes to the command syntax.

I added a new command to lookup call from <callid>:

```
/callfind 123456
```

can you please test this patch:

```patch
diff --git a/modules/mqtt/subscribe.c b/modules/mqtt/subscribe.c
index 340eb63..9de703a 100644
--- a/modules/mqtt/subscribe.c
+++ b/modules/mqtt/subscribe.c
@@ -25,6 +25,7 @@ static void handle_command(struct mqtt *mqtt, const struct pl *msg)
 	struct odict *od = NULL;
 	const struct odict_entry *oe_cmd, *oe_prm, *oe_tok;
 	char buf[256], resp_topic[256];
+	const char *aor, *callid;
 	int err;
 
 	err = json_decode_odict(&od, 32, msg->p, msg->l, 16);
@@ -42,6 +43,31 @@ static void handle_command(struct mqtt *mqtt, const struct pl *msg)
 		goto out;
 	}
 
+	aor    = odict_string(od, "accountaor");
+	callid = odict_string(od, "callid");
+
+	if (aor) {
+		struct ua *ua = uag_find_aor(aor);
+		if (!ua) {
+			warning("mqtt: ua not found (%s)\n", aor);
+			goto out;
+		}
+
+		uag_current_set(ua);
+
+		if (callid) {
+			struct call *call;
+
+			call = call_find_id(ua_calls(ua), callid);
+			if (!call) {
+				warning("mqtt: call not found (%s)\n", callid);
+				goto out;
+			}
+
+			call_set_current(ua_calls(ua), call);
+		}
+	}
+
 	debug("mqtt: handle_command:  cmd='%s', token='%s'\n",
 	      oe_cmd ? oe_cmd->u.str : "",
 	      oe_tok ? oe_tok->u.str : "");
```

@premultiply can you test the patch please, I don't have mqtt.
@alfredh This works like a charm for me.
Tried it with `dial` and `hangup` commands from our web GUI in development.

What may be a bit misleading is that the call-id is named only `id` in outgoing event and needs to be named `callid` in incoming command.
pushed to master.

thanks for good input and testing.