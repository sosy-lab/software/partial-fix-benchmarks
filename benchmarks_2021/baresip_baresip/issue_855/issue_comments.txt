Incorrect suggested ptime and buffer value in outgoing SDP
Apart from that wouldn‘t it be better to set the jitter buffer time in ms and let baresip calculate the resulting frame count?
Because the jitter does not care about frame count but about time.
Definitely agree, the possibility to set the jitter buffer in ms instead of frames and let Baresip calculate the resulting frame count used would make a bunch of scenarios and use cases a lot more controlled. For example incoming calls where ptime is being mirrored etc.
if you have two or more codecs with different ptime's which value should be used ?

example:

```
l16:   5ms
pcma: 20ms
ilbc: 30ms
```

the `a=ptime` SDP attribute should now contain ?

hint: the selected encoder is decided after the SDP Offer/Answer exchange is complete.


EBU recommends to set ptime:4 as default if no better value is available at this moment.

Maybe another way would be to already shrink the fmtp list to only the selected codec as answerer (to SDP INVITE) and set ptime to this codecs value.
A more complex solution:
https://tools.ietf.org/id/draft-garcia-mmusic-multiple-ptimes-problem-03.html

A snippet from this document:
> ptime/maxptime algorithm
> 
> Instead of indicating a 'ptime/maxptime' on a per-codec basis as done in many different proposals, this draft proposes to make use of the 'ptime/maxptime' as a common parameter coming from different sources:
> ptime(s), ptime(d), ptime(i) and maxptime(s), maxptime(d), maxptime(i).
> 
> In function of the available information for the 'ptime' and 'maxptime', the packetization time which will be used for the transmission "pt" is based on following algorithm.
> 
> 1. Determine codec to be used, e.g. G723 based on local info or the optional network info.
> 2. Determine coding data rate, e.g. 6.4 kbps based on local info or the optional network info.
> 3. Based on the codec, the frame size in ms is known: fc = frame size of the codec.
> 4. Determine the MTU size which can be used. Based on this value, the codec frame size and datarate, a 'maxptime' related to the codec "mc" can be calculated.
> 5. Check the ptime(s, d, i) and maxptime(s, d, i, mc). Take the maximum value from the available set of ptime(s, d, i) which is lower or equal than the minimum value in the set maxptime(s, d, i, mc).
> 6. Normalize this 'ptime' value to the integer multiple of the frame size lower or equal to this 'ptime' value and lower or equal to the "mc" but not lower then the codec frame size.
> 
> Remark:
> It's up to a local policy of the device, to determine which 'ptime/maxptime' sources it will use in its calculation, e.g. it is possible to disallow the treatment of the 'ptime' indicated by the other side. This can easily be done by including/excluding the 'ptime/maxptime' values from the vectors used in the calculation.
> 
> The formula to calculate the packetization time for the transmission of voice packets in the RTP payload data has following input parameters.
> 
> 1. The packetization time made available from different sources. When no value is known, the frame size of the voice codec is used.
> 2. The maximum packetization time values made available from different sources. When no value is known, the frame size of the voice codec is used.
> 3. The frame size of the codec.
> 4. The packetization time corresponding with the selected codec, frame size, frame datarate and the network MTU. This packetization time has to be larger or equal to the frame size. At least one frame size should fit in the MTU!
> 
> The function has one output parameter: the packetization time which has to be used for the transmission: "pt". It is the frame size of the codec multiplied by the number of frames which have to be placed in the RTP payload based on the provided 'ptime' and 'maxptime' values. In the formula, the maximum packetization time related to the MTU is added to the vector which contains one or more packetization time values. The minimum value out of this set is determined. For the 'ptime' set "p" which contains one or more values, the values of the 'ptime' which is higher as the minimum value of the 'maxptime' set "mp" is replaced by this value. Then the maximum value out of this set is determined and used to calculate the amount of voice frames which can be included with that packetization time.
> 
> Some examples are provided. The first example is related to the G723 with a frame size of 30 ms. When the receiver has indicated a 'ptime' of 20 ms in the SDP, the RTP will be sent with one voice frame of 30 ms.
> In another example, a G711 codec with a default 'ptime' of 20 ms and an indicated 'ptime' of 60 ms, 3 speech frames of 20 ms can be transmitted in one RTP packet towards the receiver which has indicated his ability to receive RTP packets with 60 ms packetization time.
> 
> This "pt" is used to allocate the PCM buffer size where the voice samples from the synchronous network interface are stored before being passed in RTP packets towards the packet oriented network.
> 
> When the 'ptime' and 'maxptime' are lower as the frame size of the codec, no packetization time for the transmission can be determined. An invalid value (=0) is indicated by the algorithm. In that case, the sender has to select another codec with a voice frame size which is lower or equal to the 'ptime' or 'maxptime'. 
Or just do not set ptime in SDP if not known and/or only return mirrored ptime value in SDP from peer (if set).
As far as I know, the only working solution today is to set the ebuacip:plength parameter for each codec. This only works if both parties understand the parameters suggested in this document.
https://tech.ebu.ch/docs/tech/tech3368.pdf

a=ebuacip:plength 98 20
a=ebuacip:plength 111 4
a=ebuacip:plength 110 5

Baresip has some support, perhaps we should try to implement full support?

Skickat från min iPhone

24 nov. 2019 kl. 12:26 skrev premultiply <notifications@github.com>:

﻿

Or just do not set if not known and/or only mirror ptime value from SDP peer (if set).

—
You are receiving this because you are subscribed to this thread.
Reply to this email directly, view it on GitHub<https://github.com/alfredh/baresip/issues/855?email_source=notifications&email_token=AAZ36DPK36VOSZLKICFMJCTQVJQGNA5CNFSM4JP7FBFKYY3PNVWWK3TUL52HS4DFVREXG43VMVBW63LNMVXHJKTDN5WW2ZLOORPWSZGOEFAJCUQ#issuecomment-557879634>, or unsubscribe<https://github.com/notifications/unsubscribe-auth/AAZ36DLV3P6ZKJI6PFFYLILQVJQGNANCNFSM4JP7FBFA>.

Hi,
According to tech3368, your example Alfred would set ptime according to this:

_When sending an SDP, the plength attribute should be signalled for each format, but a ptime attribute should also be included. The value to select for the ptime attribute shall be any value between and including, the minimum or maximum values of the plength attributes: minimum plength value <= ptime <= maximum plength value_
(3.5.2 Signaling, page 13)
https://tech.ebu.ch/docs/tech/tech3368.pdf

So, your example might be tricky to solve without using the ebuacip:plentgh.
If ebuacip:plength is not used, I guess a=ptime:5 would be the most correct here as it is the value for the intended/preferred codec. Also because, in any case, if a=ptime is left out, or out of range (fragmentation will occur) in an incoming SDP, default value for the used codec will be used by the caller, and the callee.

I also agree with premultiply, for l16 `a=ptime:4` seems to be the default value, so Baresip should signal that per default instead of 5ms when using l16
(3.1.4 16bit PCM, page 12)
https://tech.ebu.ch/docs/tech/tech3326.pdf

In this issue however, important is the suggested jitter buffer value `ebuacip:jbdef` which seems to be incorrectly calculated when a call is set up with l16 - today it seems to be calculated from the default ptime and not calculated by using the signaled value, which makes the callee to use/set an unintented value in ms for its jitter buffer
By the way this is how AVT handles the multiple ptime issue:

![grafik](https://user-images.githubusercontent.com/4681172/69639223-712c8480-105c-11ea-8970-f462f85bdb0d.png)

EDIT: ... and also Orban does the same as AVT here. Multiple ptime values.
this suggestion from Ola can be added:

```
a=ebuacip:plength 98 20
a=ebuacip:plength 111 4
a=ebuacip:plength 110 5
```

we just have to iterate over all the audio codecs,
and add this SDP attribute for each of them.

also multiple ptime attributes can be added.
it is difficult to say how legacy SIP equipment will handle this..

if you want the ptime value for L16 changed to 4ms, could one of your please
send a separate PR for that ?

I have made a fix for this, the SDP offer will now look like this:

```
v=0
o=- 2863325213 519350340 IN IP4 10.0.1.8
s=-
c=IN IP4 10.0.1.8
t=0 0
a=tool:baresip 0.6.5
m=audio 17442 RTP/AVP 0 8 96 10 97 98 99 100 11 101 102 103 104
a=rtpmap:0 PCMU/8000
a=rtpmap:8 PCMA/8000
a=rtpmap:96 L16/48000/2
a=rtpmap:10 L16/44100/2
a=rtpmap:97 L16/32000/2
a=rtpmap:98 L16/16000/2
a=rtpmap:99 L16/8000/2
a=rtpmap:100 L16/48000
a=rtpmap:11 L16/44100
a=rtpmap:101 L16/32000
a=rtpmap:102 L16/16000
a=rtpmap:103 L16/8000
a=rtpmap:104 telephone-event/8000
a=fmtp:104 0-15
a=sendrecv
a=label:1
a=rtcp-rsize
a=ssrc:4120941070 cname:sip:aeh@iptel.org
a=ptime:4
a=ebuacip:version 0
a=ebuacip:jb 0
a=ebuacip:jbdef 0 auto 8-16
a=ebuacip:qosrec 46
```

i.e. if an audio codec has ptime set, it will override the account-specific ptime.

please test it and see that it is working for you :)


I consider the issue fixed for now, please re-open if it does not work.
Informationsklass: Sveriges Radio Intern

Thank you Alfred! We will have time to verify and check this during the day. Yes, will re-open if anthing comes up.

Jim

Från: Alfred E. Heggestad <notifications@github.com>
Skickat: den 4 december 2019 12:42
Till: alfredh/baresip <baresip@noreply.github.com>
Kopia: Jim Eld <jim.eld@sr.se>; Author <author@noreply.github.com>
Ämne: Re: [alfredh/baresip] Incorrect suggested ptime and buffer value in outgoing SDP (#855)


I consider the issue fixed for now, please re-open if it does not work.

-
You are receiving this because you authored the thread.
Reply to this email directly, view it on GitHub<https://github.com/alfredh/baresip/issues/855?email_source=notifications&email_token=AH45CSKMS7IWXE6V3HZYV43QW6JR7A5CNFSM4JP7FBFKYY3PNVWWK3TUL52HS4DFVREXG43VMVBW63LNMVXHJKTDN5WW2ZLOORPWSZGOEF4XKCQ#issuecomment-561607946>, or unsubscribe<https://github.com/notifications/unsubscribe-auth/AH45CSNS5DN4DPONBN6Y4ADQW6JR7ANCNFSM4JP7FBFA>.

After having verified this solution, I think we are very close, **but** I found one "showstopper" as it seems.

Baresip is the caller

Loading only one module (l16) and setting buffer in config to 9-10 frames (40ms)
**Result**: Good! a=ptime in SDP is 4, and ebuacip:jbdef is 40 - Call is established correctly!

Loading only one module (opus) and setting buffer in config to 9-10 frames (200ms)
**Result**: Good! a=ptime in SDP is 20, and ebuacip:jbdef is 200 - Call is established correctly!

Loading two modules (opus + l16) and setting buffer in config to 9-10 frames (200ms if opus)
**Result**: Fail! a=ptime in SDP is 4, eventhough opus is the first/intended codec - also Baresip gives the following error message:
_audio: opus encode error: 384 samples (Protocol error)
opus: encode error: invalid argument_
_Call is established incorrectly...No RTP sent to callee...callee ends call after timeout due to RTP timeout_

Expected result, a=ptime SHOULD be 20 and not 4 in this scenario as OPUS is the first announced/the intended codec

I don't seem to figure out how to re-open this issue, should I create a new issue and refer to this issue?
#869 
I have added `minptime` SDP attribute and also EBU-ACIP `plength`:

```
v=0.
o=- 1352140388 1041489085 IN IP4 10.0.1.8.
s=-.
c=IN IP4 10.0.1.8.
t=0 0.
a=tool:baresip 0.6.5.
m=audio 42138 RTP/AVP 96 97 10 98 99 100 101 11 102 103 104 105.
a=rtpmap:96 opus/48000/2.
a=fmtp:96 stereo=1;sprop-stereo=1;maxaveragebitrate=28000.
a=rtpmap:97 L16/48000/2.
a=rtpmap:10 L16/44100/2.
a=rtpmap:98 L16/32000/2.
a=rtpmap:99 L16/16000/2.
a=rtpmap:100 L16/8000/2.
a=rtpmap:101 L16/48000.
a=rtpmap:11 L16/44100.
a=rtpmap:102 L16/32000.
a=rtpmap:103 L16/16000.
a=rtpmap:104 L16/8000.
a=rtpmap:105 telephone-event/8000.
a=fmtp:105 0-15.
a=sendrecv.
a=label:1.
a=rtcp-rsize.
a=ssrc:4256103495 cname:sip:alfredh@sip.antisip.com.
a=minptime:4.
a=ptime:20.
a=ebuacip:version 0.
a=ebuacip:jb 0.
a=ebuacip:jbdef 0 auto 8-16.
a=ebuacip:qosrec 46.
a=ebuacip:plength 97 4.
a=ebuacip:plength 10 4.
a=ebuacip:plength 98 10.
a=ebuacip:plength 99 20.
a=ebuacip:plength 100 20.
a=ebuacip:plength 101 10.
a=ebuacip:plength 11 10.
a=ebuacip:plength 102 20.
a=ebuacip:plength 103 20.
a=ebuacip:plength 104 20.
```

Please note that the `ptime` attribute is only a hint to the callee, about what
the caller *wants* to receive. The callee must take its own decision about which
value to use, based on remote capabilities and local capabilities.

Hi,

As I can see from our tests this works OK and as intended.

When loading two modules/codecs with one that differs from baresip default ptime (eg l16 and opus) correct ptime is being used in the actual call by both caller and callee , and ebuacip:plenght is signaled per codec in SDP

IP-audio codecs in this test:
Baresip -> Prodys Quantum
Baresip -> Prodys Prontonet
Baresip -> UMAC (mandozzi)

L16/4800/2 = ptime 4ms
OPUS/48000/2 = ptime 20ms


Baresip is the caller - default ptime=20ms (Baresip default)

**Test 1**
Loading only one module (l16) and setting jitter buffer in config to 9-10 frames
Result: OK!
ebuacip:jbdef is 40 (ms) - ptime in call is 4 (ms) (rx/tx)

**Test 2**
Loading only one module (opus) and setting jitter buffer in config to 9-10 frames
Result: OK! 
ebuacip:jbdef is 200 (ms) - ptime in call is 20 (ms) (rx/tx)

**Test 3**
Loading two modules (l16 + opus) and setting buffer in config to 9-10 frames
Result: OK! L16 is preferred codec and is being used
ebuacip:jbdef is 40 (ms) - ptime in call is 4 (ms) (rx/tx)

**Test 4**
Loading two modules (opus + l16) and setting buffer in config to 9-10 frames
Result: OK! OPUS is preferred codec and is being used
ebuacip:jbdef is 200 (ms) - ptime in call is 20 (ms) (rx/tx)

In my opinion, and for our use cases, this works and I suggest that this issue can be closed .


------------------

**However, +1 for issue #869 by premultiply.**

If jitter buffer where to be set in ms in config instead of frames we would have a more consistent behaviour.

Suggested jitter buffer in SDP and used jitter buffer (caller/callee) would be the same in this case, no matter what codec used or signaled in the SDP.
Many thanks for testing :)
