feat: Support case-sensitive identifier

Add `case_sensitive_ident` and `case_compat_type_func` GUC variables
to support case-sensitive identifier.

Closes #205
feat: Create labels on demand in a query

If a label which does not exist is used in CREATE or MERGE clauses,
the label will be created on demand.

Closes #204
feat: Create labels on demand in a query

If a label which does not exist is used in CREATE or MERGE clauses,
the label will be created on demand.

Closes #204
feat: Create labels on demand in a query

If a label which does not exist is used in CREATE or MERGE clauses,
the label will be created on demand.

Closes #204
