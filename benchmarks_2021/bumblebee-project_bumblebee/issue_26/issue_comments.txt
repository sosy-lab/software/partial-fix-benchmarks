Support driver-specific conffiles?
Why not having "sub-configuration-files" so to switch between driver configurations you need to change only a line in the main bumblebee.conf file?
Is the same idea made "permanent" or at least persistent

An "include relative/to/conf/dir" line? Doesn't that make it unnecessary complicated?

Mhhh I was proposing something like: 
- Read the main configuration file (the relevant part is the DRIVER option)
- Have a per-driver configuration file to be read after (that's what I called sub-configuration-files)

That is the same idea, but how should the file be named? `$sysconfdir/bumblebee/bumblebee-DRIVER.conf`?

The order of precedence:
1. commandline options (most important)
2. driver config file `bumblebee-DRIVER.conf`
3. main config file `bumblebee.conf`

All options are probably allowed to be overriden in the driver config file, except for DRIVER. The file is optional.

I'm making those conf files. The issue is which options to set as "driver-specific". So far I got:
- kernel module
- x.conf file
- lib_path
- module_path

Now, what do we do with power management method? It can be considered to be driver-specific as nouveau has two options (of course we can use bbswitch for both and rely on auto-detection).

PM method is driver-specific. Are you allowing all settings to be overridden?

I'm thinking on changing the entire configuration files to a single file of 'ini' style, with sections, using Glib. Will update soon.

Last commits are WIP but the old behavior is still used. So it's safe to use so far. Eventually I'll break it all with the new behavior and configuration file :P

Let's get this ready before release. It should be backwards compatible, i.e. all options without a group ("[common]" or "[main]"?) should be read by both optirun and the server. I have not fully studied the code, but can it be made with the sections `[bumblebeed]` and `[optirun]`?

Since we are changing the whole code base I don't think the configuration should be kept backwards compatible. Besides as we are hardcoding the default configuration, It should not be a problem to work without configuration at all. I'm concerned about the "false by default" behaviour. Maybe it can be implemented gradually.

Currently, `[nvidia]` and `[nouveau]` are config groups. Shouldn't this be `[driver-DRIVER]` instead? Or even `[Driver DRIVER]`? (just like `[Desktop Entry]`)

And I hitted another bug, `--driver` overrides `DRIVER/KernelDriver`, not `bumblebeed/Driver`. This is because driver override (and detection) takes place after the config has loaded. I'll try to fix that.

Ok, I'm going to take this approach:
1. load --verbose and --config
2. load program section settings
3. load --driver
4. perform autodetection of driver
5. load driver section
6. load remaining cmd line options
7. detect PM method

Because of the current design, autodetection only works if the module **file** name is either nvidia, nvidia-current or nouveau. In other cases, autodetection will fail.

I'm thinking in using Glib data types and allocation methods for the configuration structure. The GString structure and related methods are easier and more intuitive for using and managing strings. Besides, shouldn't `bb_config` structure became a pointer when being managed? I think is a better approach and maybe should improve code.

I don't understand this order:
- load --driver
- perform autodetection of driver
- load driver section

Won't be better:
- perform autodetection of driver
- load --driver
- load driver section

Nope, the auto-detection completes the module name too if it's not set. If you do it the other way round, a driver is detected (say nouveau) and the module is set to "nouveau". When --driver override takes place, only the driver is set (say nvidia). Then you've a broken configuration.

As for using glib everywhere, gchar = char, so it hasn't caused any issues so far. Be aware which glib function to use for deallocating memory: the g_keyfile_load_new (or sth) can be freed with g_keyfile_free, but not g_free.
While you're at it, please replace set_string_value completely and use strdup in combination with free_and_set_value (or a glib func at your choice)

Acknowledge. I was looking into it. I'll do my research/playarounds/experiments on my own repository or in a different branch. But as it's a major change I should postpone it to 3.1

Even open a different issue. If this feature is complete we can close and call it done. The Glib part would be done later

I'd say that this feature is pretty complete, glib stuff is a separate thing, open a new issue for that.
