Remove desktop size struct
Make saved game read/write endian-agnostic

Fixes #32
Make test suite endian-proof

Issue #32
Make char fields explicitly signed

Fixes #32
