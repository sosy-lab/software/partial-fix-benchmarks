Inactive transparency regression in 0d67243 causes chromium omnibar transparency
In fact, this is not a regression, it's the fix of a bug. The behavior is caused by the change of client window detection, I suppose. We once used `WM_STATE` to identify a client window, but if a window doesn't have a WM frame it usually doesn't have a `WM_STATE`. This is true for notification windows and popup windows, I guess. And they usually have a `WM_CLASS`, so for detecting client windows `WM_CLASS` might be more accurate than `WM_STATE`.

You think 8724101 does not have this problem because you have `--mark-wmwin-focused`. This marks a window focused if it does not have a client window, and as compton was detecting client window with `WM_STATE`, and the omnibar dropdown window doesn't have a window manager frame or `WM_STATE`, it's considered a WM window and is therefore considered focused. `0d67243` corrected the client window detecting, so the problem shows up.

Now, some of Chromium's issues become a problem again. Its omnibar dropdown window has `_NET_WM_WINDOW_TYPE_NORMAL`, and no `WM_TRANSIENT_FOR`. Only a `WM_CLIENT_LEADER`, but hey, that thing is for session management! (Firefox is doing a lot more better, `_NET_WM_WINDOW_TYPE_UTILITY` and `WM_TRANSIENT_FOR` for its location bar dropdown window.) How the hell could you expect me to identify a `_NET_WM_WINDOW_TYPE_NORMAL` window as a dropdown window and give it correct status? `WM_PROTOCOLS`? Well, if it's in any sense reliable... Or should we just think a window without a WM frame unconditionally a WM window? Thoughts?

> We once used WM_STATE to identify a client window, but if a window doesn't have a WM frame it usually doesn't have a WM_STATE. This is true for notification windows and popup windows, I guess. And they usually have a WM_CLASS, so for detecting client windows WM_CLASS might be more accurate than WM_STATE.

I see. I probably should have read the commit message better. Hmm, ok. I'd still like to keep my eyes open for any other windows that may be affected. Chromium's dropdown isn't a big deal really. If WM_CLASS ends up being more reliable for most windows, it's probably worthwhile.

Check if my commit fix the problem for you, chjj. Use `--mark-ovredir-focused` or `mark-ovredir-focused = true;` to turn it on. I'm pretty worried that it would have unpredictable side effects, as my understanding of override-redirect is extremely limited. Well, and I don't have many other choices. It would make more sense to let a transient window share the focused state with its parent, yet Chromium's location bar dropdown window does not have `WM_TRANSIENT_FOR`, so... Let's see how well this works, anyway.

Huh, so this issue report is 4 months ago... I will close it if I don't get further replies, @chjj .
