remove DEH_BEXPtrInit(), DEH_PointerInit() already does the same
Revert "increase DEFAULT_RAM to 64 MiB"

This reverts commit b24fd9e532119a3a94910c1a6da3dad22ad550d6.

This was a red herring. The game is actually playable with as less as
5 MB of heap memory. However, there is an issue with the memory region
occupied be the DEHACKED patch being Z_Free()'d when the lumpinfo[]
array has been realloc()'d in the mean time. This is simply less
likely to happen when more heap memory is provided in the first place.

See https://github.com/chocolate-doom/chocolate-doom/issues/442
Fix crash caused by adding a new WAD file.

This fixes #442, a crash caused by adding a new WAD file after a
lump has been loaded (and cached) from a previous WAD. This
manifested when playing using the Freedoom IWADs and also loading
a PWAD at the same time. The Freedoom IWADs have DEHACKED lumps
that are loaded from within the IWAD file.

The ultimate cause (thanks to Fabian Greffrath for uncovering it)
is that lumpinfo is realloc()ed after each new WAD load to store
the lumpinfo_t structures from the new WAD. If a lump has been read
and cached from a previous WAD file, it may end up having an invalid
'user' pointer that points to somewhere in the old lumpinfo[] array,
not the new one.

I think this bug was masked because realloc() will often not move
data if the previous location can simply be extended. The bug was
discovered when loading BTSX as a PWAD, probably because it's a
large WAD that contains a lot of lumps, and forced a move during
realloc.
wad: Change type of lumpinfo[] array.

Originally lumpinfo[] was an array of lumpinfo_t structs. However,
this is problematic because the array must be realloc()ed to
increase its size with each load of a new WAD. If data from one of
the WADs has already been cached in memory, the zone memory system's
user pointer can end up pointing at freed memory.

Therefore, any modification to lumpinfo[] had to be carefully
controlled - see previous commit d5b6bb70919f that applied an
attempted fix. However, this fix did not fix loading using the
merging command line parameters, which change lumpinfo[] in far more
complicated ways.

Instead, this fixes the issue in a more fundamental way. lumpinfo[]
is now an array of pointers to lumpinfo_t structs, disconnecting
their memory locations from their location in the lumpinfo[] array.
It's now possible to change or rearrange the array in any desired
way without the zone user pointers getting out of sync.

This also includes a minor change to define a lumpindex_t type for
representing lumpinfo[] indices, to make the WAD API more literate.

Thanks to Fabian Greffrath for identifying that the bug was still
present when using the merge parameters. This fixes #442 (again).
