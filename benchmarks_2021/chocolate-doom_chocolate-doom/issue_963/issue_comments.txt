Memento Mori's music WAD MMMUS.WAD plays only the title track but no level soundtracks when a fluidsynth soundfont is used
This is most probably because the SDL2_Mixer libraries shipped with the daily Chocolate Doom snapshots are built without fluidsynth support. If you use the DLL package that I sent you some days ago, you should be able to reproduce the issues with SF2 and SF3 sound font loading that we recently discussed.
I extracted the DLL package to Choco, nothing changed....
Does it help if you set the variable in a separate line?
```
set SDL_SOUNDFONTS=TimGM6mb.sf2 
chocolate-doom
```
@fabiangreffrath In fact I do set it in a separate line, I dunno why GitHub put all the code in line :)
Thanks! I have applied some more fixes from our experiments over at Crispy to the Choco code base. Could you please test again with tomorrow's snapshot?
>Could you please test again with tomorrow's snapshot?

Yes, certainly!
@fabiangreffrath 
>Recent versions of SDL_Mixer allow for rendering MIDI songs using
the fluidsynth backend and recent versions of fluidsynth allow for
using soundfonts in SF3 format (which contain OGG compressed samples).

Should I replace all the DLLs with those from the snapshot?
Please use the dlls i sent you. 
I 'm using those DLLs but with the new EXEs MIDI still plays instead of the soundfonts.
*sigh* wellcome to dll hell...

Are these definitely the same DLLs that you use to play music with soundfonts with Crispy? I am especially asking about SDL2_mixer.dll and fluidsynth.dll with their respective dependencies.

Please do not even try soundfonts in SF3 format, they are known-broken because of the early timeout in the midiproc code.


>Are these definitely the same DLLs that you use to play music with soundfonts with Crispy? I am especially asking about SDL2_mixer.dll and fluidsynth.dll with their respective dependencies.

There's no such DLL as fluidsynth.dll in that pack for Crispy. And yes, I'm using DLLs from that pack for sure.
> fluidsynth.dll

Sorry, but there is `libfluidsynth-1.dll`, right?
>Sorry, but there is libfluidsynth-1.dll, right?

I had to copy it over from Crispy Doom installation, because this archive
[crispy-doom-libs.zip](https://github.com/chocolate-doom/chocolate-doom/files/1554941/crispy-doom-libs.zip) doesn't contain it.
Aha! I got it working with DLLs from 
[crispy-doom-full_5.0+git6c1e8b.zip](https://github.com/chocolate-doom/chocolate-doom/files/1554951/crispy-doom-full_5.0.git6c1e8b.zip)

Tested the MMMUS.WAD with Choco, the issue of level soundtracks not played after the starting one persists.

Phew, thanks for checking again. Yes, this is the library set should should work with Choco to load soundfonts. Why did you close the bug if the result is that the music does not change between the title screen and the first map?
My analysis so far: We are facing a racing condition if an external library such as fluidsynth is used by SDL2_mixer to render MIDI music, especially if this library has to load additional external resources such as said soundfont.

The code in midiproc (and the glue in `src/i_sdlmusic.c` and `i_midipipe.c`) currently assumes (1) that it is possible to instantly remove the temporary music file after `Mix_LoadMUS()` has been called on it (and returned) and (2) that it is possible to instantly create a new temporary music file of the same name once `Mix_HaltMusic()` has been called (I have fixed this to also call `Mix_FreeMusic()` in my latest commit.

So, why is this a problem? If we tell SDL2_mixer to render a MIDI file with fluidsynth and a given soundfont, then the temporary music file will remain opened as long as the fluidsynth library plays the music. It will thus not be possible to `remove()` it right after the call to `Mix_LoadMUS()`. This is not yet a problem in itself, because we can simply override the file once we have called `Mix_FreeMusic()` *and wait until it returns*. The latter, however, does not happen!

While `I_SDL_StopSong()` calls `I_MidiPipe_StopSong()`, which is supposed to trigger the right thing in the midiproc process, there is currently no way to wait until the unregistering of the song has actually  been finished by the midiproc process. So, if we send another signal with `I_MidiPipe_RegisterSong()` to start a new song immediately after calling `I_MidiPipe_StopSong()`, without a chance to wait until this has finished, we will end up playing the same song again. This is what we see happening here.

A possible counter-measure would be to add an ACK package for the `StopSong` call and only return after this package has been received back, but my attempts [1] to add this have somehow failed so far due to failing conenctions to the server -- and my patience with this entire issue is currently approaching an end. 

@AlexMax could you probably also have a look at this, please?

[1] https://github.com/fabiangreffrath/crispy-doom/commit/a8fd660d4046efe5d57db64ac0815799ac28c393
@fabiangreffrath Hi there.  You guys are several steps ahead of me, and the attempted solution you linked is likely what I would have done in your shoes.

What do you mean by "failing connections to the server"?  Seems like if that was going on, more messages would be getting dropped or the subprocess might have become wedged in some other fashion on some other message.

Unfortunately, I don't have a buildable Chocolate Doom on my system right now, and in past experience it usually takes at least one evening of anguish for me to get things set up properly.
Tested the latest daily build with DLLs from https://github.com/chocolate-doom/chocolate-doom/files/1554951/crispy-doom-full_5.0.git6c1e8b.zip
The issue #963 persists.
Maybe the SDL DLLs should be compiled freshly with fluidsynth library?
@Zodomaniac So this means songs are still not properly stopped and started and you hear the same song over and over again?
No, the problem is that when I launch Memento Mori with the music WAD MMMUS.WAD with fluidsynth soundfont, only the title track is played but the level soundtracks are not played at all.
@fabiangreffrath Can you reproduce my case?
Nope, haven't booted into Windows for ages...
@fragglet then maybe you could investigate?
For those who want to feel @Zodomaniac's pain, apply the following patch from Crispy to enable playback of OGG/FLAC/MP3-encoded music lumps:
```
--- a/src/i_sdlmusic.c
+++ b/src/i_sdlmusic.c
@@ -406,9 +406,15 @@ static void *I_SDL_RegisterSong(void *data, int len)
     // MUS files begin with "MUS"
     // Reject anything which doesnt have this signature
 
-    filename = M_TempFile("doom.mid");
+    filename = M_TempFile("doom"); // [crispy] generic filename
 
+    // [crispy] Reverse Choco's logic from "if (MIDI)" to "if (not MUS)"
+    // MUS is the only format that requires conversion,
+    // let SDL_Mixer figure out the others
+/*
     if (IsMid(data, len) && len < MAXMIDLENGTH)
+*/
+    if (len < 4 || memcmp(data, "MUS\x1a", 4)) // [crispy] MUS_HEADER_MAGIC
     {
         M_WriteFile(filename, data, len);
     }
```
Then, make sure you have "Native MIDI" selected as your music backend and load a music replacement PWAD like e.g. `-file DoomMetalVol5.wad`.
@fabiangreffrath have you posted this in the right issue? This issue is about problems with fluidsynth.
The underlying issue appears to be the same, though. 
That's where I described the pain that @fabiangreffrath mentioned (will check if it's gone after the recent commit)
https://github.com/fabiangreffrath/crispy-doom/issues/446
And to feel my pain with MM and fluidsynth, use the daily build with DLLs replaced by ones from the archive (containing DLLs, the soundfont and launching BAT-file) [DLLs.zip](https://github.com/chocolate-doom/chocolate-doom/files/3550998/DLLs.zip)



The above commit eliminated repeating the same song in every level, but from Memento Mori's music WAD MMMUS.WAD still only the intro track is played, but no level soundtracks when a fluidsynth soundfont is used:
```
@echo off
set SDL_SOUNDFONTS=TimGM6mb.sf2
start chocolate-doom -merge mm.wad mmmus.wad
exit
```
Removing midiproc from the folder resolves the problem, which shows there's still some bug lurking in it.
This may be a time-out issue. Could you please take care that `DEBUGOUT` is defined as `puts()` in `src/i_midipipe.c`, rebuild, try again and report what is printed to the console?
I modified midipipe.c
```
//#if defined(_DEBUG)
#define DEBUGOUT(s) puts(s)
//#else
//#define DEBUGOUT(s)
//#endif
```
but nothing is printed to Windows CMD when I launch the BAT-file and the track starting error on level setup occurs.
You'll have to start it from an MSYS shell. 
```
export SDL_SOUNDFONTS=TimGM6mb.sf2
cd src
./chocolate-doom -merge mm.wad mmmus.wad
```
Here's what I get:
```
Emulating the behavior of the 'Doom 1.9' executable.
HU_Init: Setting up heads up display.
ST_Init: Init status bar.
I_MidiPipe_RegisterSong succeeded
I_MidiPipe_PlaySong succeeded
I_MidiPipe_StopSong failed
I_MidiPipe_UnregisterSong failed
I_MidiPipe_RegisterSong failed
I_MidiPipe_ShutdownServer failed
Error loading midi: Could not communicate with midiproc.
```
This reflects the fact that the intro song plays, but when the demo starts, the level soundtrack fails to play.
I wonder how it fails to *stop* the song...?
Really puzzling.

In fact, it properly stops playing the title song, but somehow fails to recognize that all went well. Also, if you start with `-warp 1` then the song for MAP01 plays and if you then type IDCLEV02, it changes to the second map's song without any problems. Maybe this has to do with the title song? But on the other hand, this seems to be related to midiproc.exe, all works as expected without it.
Okay, I think I found it: The title track causes the fluidsynth backend to emit a warning
```
fluidsynth: warning: Instrument not found on channel 9 [bank=128 prog=127], substituted [bank=128 prog=0]
```
This warning is printed to `stderr` which is the same handle that is used for the inter-process communication. @AlexMax Is there a way to clean the buffer before sending a new package?
@fabiangreffrath Hey there.  That's some pretty amazing detective work in figuring that out, hats off to you.  Also, major boo to fluidsynth for polluting standard channels.

The answer is "I don't know".  However, while we're throwing ideas around how about these ideas?

1. Perhaps it would be a better idea to set up non-standard handles for doing IPC?  I don't know the exact feasibility of something like this, but that's what I would first think to do.
2. Is it possible to silence these warnings somehow?  Like a flag or environment variable?
3. What if - instead of breaking horrifically - the pipe reader tried to detect when it saw a nonsense message and simply scanned for the ending CRLF and discarded everything in between?
Okay, after looking at this problem a while, I think I have come up with a potential strategy for fixing it.  Instead of attaching to standard streams - we can have the child process inherit parents handles through a mechanism similar to [this](https://stackoverflow.com/questions/7420508/how-to-pass-handle-to-child-process).  I'll try an implementation myself in the next few days.

For what it's worth, I don't think fluidsynth is writing to stderr.  While messing with this bug, I tried to purposefully pollute stderr with bad data, but could never get anything to go wrong, while polluting stdout resulted in brokenness.

I also wonder if it might be a good idea to have some sort of command line option that would turn on midipipe debug output, so if heaven forbid we come across another problem, we might be able to get some assistance debugging the problem.
Yes, let's please use something less fragile than stdin/stdout for IPC. 
Can't we somehow redirect stdout for all processes called by midiproc to e.g. `NUL:` but not for midiproc itself? Maybe with something like `freopen("NUL:", "w", stdout)`?

Apart from this, I like your idea to scan the buffer for a `\n` character if its content is unexpected and try again from the next character.
@AlexMax would be cool if you can fix it soon, so that it makes into the hotfix release of Crispy Doom eliminating some vicious bugs.
My apologies, I've been distracted by a personal project.  I'll figure something out this weekend.
I've submitted a pull request #1195 that will hopefully nip this problem in the bud.  I now pass the input and ouput handles to the subprocess using handle inheritance.  The actual handle ID's have to be communicated to the subprocess with additional parameters to midiproc, which makes this a breaking change, but since midiproc is version-locked to the version of Chocolate Doom used to run it, I don't anticipate it will be a problem.
@AlexMax Thanks a lot! Looking forward to trying this.
@AlexMax Thanks for resolving this issue, the problem is gone and tracks from MMMUS.WAD play properly! :)