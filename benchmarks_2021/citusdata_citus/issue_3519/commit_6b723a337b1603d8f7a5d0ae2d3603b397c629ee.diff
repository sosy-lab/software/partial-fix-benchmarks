diff --git a/src/backend/distributed/executor/adaptive_executor.c b/src/backend/distributed/executor/adaptive_executor.c
index ae2ae34b7c6..d778c2d1fb7 100644
--- a/src/backend/distributed/executor/adaptive_executor.c
+++ b/src/backend/distributed/executor/adaptive_executor.c
@@ -143,6 +143,7 @@
 #include "distributed/local_executor.h"
 #include "distributed/multi_client_executor.h"
 #include "distributed/multi_executor.h"
+#include "distributed/multi_explain.h"
 #include "distributed/multi_partitioning_utils.h"
 #include "distributed/multi_physical_planner.h"
 #include "distributed/multi_resowner.h"
@@ -694,6 +695,16 @@ AdaptiveExecutor(CitusScanState *scanState)
 	TupleDestination *defaultTupleDest =
 		CreateTupleStoreTupleDest(scanState->tuplestorestate, tupleDescriptor);
 
+	if (RequestedForExplainAnalyze(scanState))
+	{
+		/*
+		 * Worker's saved EXPLAIN ANALYZE is deleted at transaction end, make sure
+		 * we use a coordinated transaction so we can fetch it.
+		 */
+		UseCoordinatedTransaction();
+		taskList = ExplainAnalyzeTaskList(taskList, defaultTupleDest, tupleDescriptor);
+	}
+
 	bool hasDependentJobs = HasDependentJobs(job);
 	if (hasDependentJobs)
 	{
@@ -3155,13 +3166,25 @@ TransactionStateMachine(WorkerSession *session)
 				TaskPlacementExecution *placementExecution = session->currentTask;
 				ShardCommandExecution *shardCommandExecution =
 					placementExecution->shardCommandExecution;
-				bool storeRows = shardCommandExecution->expectResults;
+				Task *task = shardCommandExecution->task;
+
+				/*
+				 * In EXPLAIN ANALYZE we need to store results except for multiple placements,
+				 * regardless of query type. In other cases, doing the same doesn't seem to have
+				 * a drawback.
+				 */
+				bool storeRows = true;
 
 				if (shardCommandExecution->gotResults)
 				{
 					/* already received results from another replica */
 					storeRows = false;
 				}
+				else if (task->partiallyLocalOrRemote)
+				{
+					/* already received results from local execution */
+					storeRows = false;
+				}
 
 				bool fetchDone = ReceiveResults(session, storeRows);
 				if (!fetchDone)
diff --git a/src/backend/distributed/planner/multi_explain.c b/src/backend/distributed/planner/multi_explain.c
index 7d5ffc71b14..a9caabd5d62 100644
--- a/src/backend/distributed/planner/multi_explain.c
+++ b/src/backend/distributed/planner/multi_explain.c
@@ -11,6 +11,7 @@
 #include "libpq-fe.h"
 #include "miscadmin.h"
 
+#include "access/htup_details.h"
 #include "access/xact.h"
 #include "catalog/namespace.h"
 #include "catalog/pg_class.h"
@@ -41,6 +42,7 @@
 #include "distributed/remote_commands.h"
 #include "distributed/recursive_planning.h"
 #include "distributed/placement_connection.h"
+#include "distributed/tuple_destination.h"
 #include "distributed/tuplestore.h"
 #include "distributed/listutils.h"
 #include "distributed/worker_protocol.h"
@@ -73,6 +75,22 @@ bool ExplainAllTasks = false;
  */
 static char *SavedExplainPlan = NULL;
 
+/* struct to save explain flags */
+typedef struct
+{
+	bool verbose;
+	bool costs;
+	bool buffers;
+	bool timing;
+	bool summary;
+	ExplainFormat format;
+} ExplainOptions;
+
+
+/* EXPLAIN flags of current distributed explain */
+static ExplainOptions CurrentDistributedQueryExplainOptions = {
+	0, 0, 0, 0, 0, EXPLAIN_FORMAT_TEXT
+};
 
 /* Result for a single remote EXPLAIN command */
 typedef struct RemoteExplainPlan
@@ -82,6 +100,15 @@ typedef struct RemoteExplainPlan
 } RemoteExplainPlan;
 
 
+typedef struct ExplainAnalyzeDestination
+{
+	TupleDestination pub;
+	Task *originalTask;
+	TupleDestination *originalTaskDestination;
+	TupleDesc lastSavedExplainAnalyzeTupDesc;
+} ExplainAnalyzeDestination;
+
+
 /* Explain functions for distributed queries */
 static void ExplainSubPlans(DistributedPlan *distributedPlan, ExplainState *es);
 static void ExplainJob(Job *job, ExplainState *es);
@@ -93,6 +120,7 @@ static void ExplainTask(Task *task, int placementIndex, List *explainOutputList,
 static void ExplainTaskPlacement(ShardPlacement *taskPlacement, List *explainOutputList,
 								 ExplainState *es);
 static StringInfo BuildRemoteExplainQuery(char *queryString, ExplainState *es);
+static const char * ExplainFormatStr(ExplainFormat format);
 static void ExplainWorkerPlan(PlannedStmt *plannedStmt, DestReceiver *dest,
 							  ExplainState *es,
 							  const char *queryString, ParamListInfo params,
@@ -102,6 +130,15 @@ static bool ExtractFieldBoolean(Datum jsonbDoc, const char *fieldName, bool defa
 static ExplainFormat ExtractFieldExplainFormat(Datum jsonbDoc, const char *fieldName,
 											   ExplainFormat defaultValue);
 static bool ExtractFieldJsonbDatum(Datum jsonbDoc, const char *fieldName, Datum *result);
+static TupleDestination * CreateExplainAnlyzeDestination(Task *task,
+														 TupleDestination *taskDest);
+static void ExplainAnalyzeDestPutTuple(TupleDestination *self, Task *task,
+									   int placementIndex, int queryNumber,
+									   HeapTuple heapTuple);
+static TupleDesc ExplainAnalyzeDestTupleDescForQuery(TupleDestination *self, int
+													 queryNumber);
+static char * WrapQueryForExplainAnalyze(const char *queryString, TupleDesc tupleDesc);
+static List * SplitString(StringInfo str, char delimiter);
 
 /* Static Explain functions copied from explain.c */
 static void ExplainOneQuery(Query *query, int cursorOptions,
@@ -421,6 +458,30 @@ RemoteExplain(Task *task, ExplainState *es)
 
 	RemoteExplainPlan *remotePlan = (RemoteExplainPlan *) palloc0(
 		sizeof(RemoteExplainPlan));
+
+	if (es->analyze)
+	{
+		Assert(task->fetchedExplainAnalyzePlan);
+
+		/*
+		 * Similar to postgres' ExplainQuery(), we split by newline only for
+		 * text format.
+		 */
+		if (es->format == EXPLAIN_FORMAT_TEXT)
+		{
+			remotePlan->explainOutputList = SplitString(task->fetchedExplainAnalyzePlan,
+														'\n');
+		}
+		else
+		{
+			remotePlan->explainOutputList = list_make1(task->fetchedExplainAnalyzePlan);
+		}
+
+		remotePlan->placementIndex = task->fetchExplainAnalyzePlacementIndex;
+
+		return remotePlan;
+	}
+
 	StringInfo explainQuery = BuildRemoteExplainQuery(TaskQueryStringForAllPlacements(
 														  task),
 													  es);
@@ -615,49 +676,53 @@ static StringInfo
 BuildRemoteExplainQuery(char *queryString, ExplainState *es)
 {
 	StringInfo explainQuery = makeStringInfo();
-	char *formatStr = NULL;
+	const char *formatStr = ExplainFormatStr(es->format);
 
-	switch (es->format)
+	appendStringInfo(explainQuery,
+					 "EXPLAIN (ANALYZE %s, VERBOSE %s, "
+					 "COSTS %s, BUFFERS %s, TIMING %s, SUMMARY %s, "
+					 "FORMAT %s) %s",
+					 es->analyze ? "TRUE" : "FALSE",
+					 es->verbose ? "TRUE" : "FALSE",
+					 es->costs ? "TRUE" : "FALSE",
+					 es->buffers ? "TRUE" : "FALSE",
+					 es->timing ? "TRUE" : "FALSE",
+					 es->summary ? "TRUE" : "FALSE",
+					 formatStr,
+					 queryString);
+
+	return explainQuery;
+}
+
+
+/*
+ * ExplainFormatStr converts the given explain format to string.
+ */
+static const char *
+ExplainFormatStr(ExplainFormat format)
+{
+	switch (format)
 	{
 		case EXPLAIN_FORMAT_XML:
 		{
-			formatStr = "XML";
-			break;
+			return "XML";
 		}
 
 		case EXPLAIN_FORMAT_JSON:
 		{
-			formatStr = "JSON";
-			break;
+			return "JSON";
 		}
 
 		case EXPLAIN_FORMAT_YAML:
 		{
-			formatStr = "YAML";
-			break;
+			return "YAML";
 		}
 
 		default:
 		{
-			formatStr = "TEXT";
-			break;
+			return "TEXT";
 		}
 	}
-
-	appendStringInfo(explainQuery,
-					 "EXPLAIN (ANALYZE %s, VERBOSE %s, "
-					 "COSTS %s, BUFFERS %s, TIMING %s, SUMMARY %s, "
-					 "FORMAT %s) %s",
-					 es->analyze ? "TRUE" : "FALSE",
-					 es->verbose ? "TRUE" : "FALSE",
-					 es->costs ? "TRUE" : "FALSE",
-					 es->buffers ? "TRUE" : "FALSE",
-					 es->timing ? "TRUE" : "FALSE",
-					 es->summary ? "TRUE" : "FALSE",
-					 formatStr,
-					 queryString);
-
-	return explainQuery;
 }
 
 
@@ -875,6 +940,280 @@ ExtractFieldJsonbDatum(Datum jsonbDoc, const char *fieldName, Datum *result)
 }
 
 
+/*
+ * CitusExplainOneQuery is the executor hook that is called when
+ * postgres wants to explain a query.
+ */
+void
+CitusExplainOneQuery(Query *query, int cursorOptions, IntoClause *into,
+					 ExplainState *es, const char *queryString, ParamListInfo params,
+					 QueryEnvironment *queryEnv)
+{
+	/* save the flags of current EXPLAIN command */
+	CurrentDistributedQueryExplainOptions.costs = es->costs;
+	CurrentDistributedQueryExplainOptions.buffers = es->buffers;
+	CurrentDistributedQueryExplainOptions.verbose = es->verbose;
+	CurrentDistributedQueryExplainOptions.summary = es->summary;
+	CurrentDistributedQueryExplainOptions.timing = es->timing;
+	CurrentDistributedQueryExplainOptions.format = es->format;
+
+	/* rest is copied from ExplainOneQuery() */
+	instr_time planstart,
+			   planduration;
+
+	INSTR_TIME_SET_CURRENT(planstart);
+
+	/* plan the query */
+	PlannedStmt *plan = pg_plan_query(query, cursorOptions, params);
+
+	INSTR_TIME_SET_CURRENT(planduration);
+	INSTR_TIME_SUBTRACT(planduration, planstart);
+
+	/* run it (if needed) and produce output */
+	ExplainOnePlan(plan, into, es, queryString, params, queryEnv,
+				   &planduration);
+}
+
+
+/*
+ * CreateExplainAnlyzeDestination creates a destination suitable for collecting
+ * explain analyze output from workers.
+ */
+static TupleDestination *
+CreateExplainAnlyzeDestination(Task *task, TupleDestination *taskDest)
+{
+	ExplainAnalyzeDestination *tupleDestination = palloc0(
+		sizeof(ExplainAnalyzeDestination));
+	tupleDestination->originalTask = task;
+	tupleDestination->originalTaskDestination = taskDest;
+
+#if PG_VERSION_NUM >= PG_VERSION_12
+	TupleDesc lastSavedExplainAnalyzeTupDesc = CreateTemplateTupleDesc(1);
+#else
+	TupleDesc lastSavedExplainAnalyzeTupDesc = CreateTemplateTupleDesc(1, false);
+#endif
+
+	TupleDescInitEntry(lastSavedExplainAnalyzeTupDesc, 1, "explain analyze", TEXTOID, 0,
+					   0);
+	tupleDestination->lastSavedExplainAnalyzeTupDesc = lastSavedExplainAnalyzeTupDesc;
+
+	tupleDestination->pub.putTuple = ExplainAnalyzeDestPutTuple;
+	tupleDestination->pub.tupleDescForQuery = ExplainAnalyzeDestTupleDescForQuery;
+
+	return (TupleDestination *) tupleDestination;
+}
+
+
+/*
+ * ExplainAnalyzeDestPutTuple implements TupleDestination->putTuple
+ * for ExplainAnalyzeDestination.
+ */
+static void
+ExplainAnalyzeDestPutTuple(TupleDestination *self, Task *task,
+						   int placementIndex, int queryNumber,
+						   HeapTuple heapTuple)
+{
+	ExplainAnalyzeDestination *tupleDestination = (ExplainAnalyzeDestination *) self;
+	if (queryNumber == 0)
+	{
+		TupleDestination *originalTupDest = tupleDestination->originalTaskDestination;
+		originalTupDest->putTuple(originalTupDest, task, placementIndex, 0, heapTuple);
+	}
+	else if (queryNumber == 1)
+	{
+		bool isNull = false;
+		TupleDesc tupDesc = tupleDestination->lastSavedExplainAnalyzeTupDesc;
+		Datum explainAnalyze = heap_getattr(heapTuple, 1, tupDesc, &isNull);
+
+		if (isNull)
+		{
+			ereport(WARNING, (errmsg(
+								  "received null explain analyze output from worker")));
+			return;
+		}
+
+		StringInfo fetchedExplainAnalyzePlan = makeStringInfo();
+		appendStringInfoString(fetchedExplainAnalyzePlan, TextDatumGetCString(
+								   explainAnalyze));
+
+		tupleDestination->originalTask->fetchedExplainAnalyzePlan =
+			fetchedExplainAnalyzePlan;
+		tupleDestination->originalTask->fetchExplainAnalyzePlacementIndex =
+			placementIndex;
+	}
+	else
+	{
+		ereport(ERROR, (errmsg("cannot get EXPLAIN ANALYZE of multiple queries"),
+						errdetail("while receiving tuples for query %d", queryNumber)));
+	}
+}
+
+
+/*
+ * ExplainAnalyzeDestTupleDescForQuery implements TupleDestination->tupleDescForQuery
+ * for ExplainAnalyzeDestination.
+ */
+static TupleDesc
+ExplainAnalyzeDestTupleDescForQuery(TupleDestination *self, int queryNumber)
+{
+	ExplainAnalyzeDestination *tupleDestination = (ExplainAnalyzeDestination *) self;
+	if (queryNumber == 0)
+	{
+		TupleDestination *originalTupDest = tupleDestination->originalTaskDestination;
+		return originalTupDest->tupleDescForQuery(originalTupDest, 0);
+	}
+	else if (queryNumber == 1)
+	{
+		return tupleDestination->lastSavedExplainAnalyzeTupDesc;
+	}
+
+	ereport(ERROR, (errmsg("cannot get EXPLAIN ANALYZE of multiple queries"),
+					errdetail("while requesting for tuple descriptor of query %d",
+							  queryNumber)));
+	return NULL;
+}
+
+
+/*
+ * RequestedForExplainAnalyze returns true if we should get the EXPLAIN ANALYZE
+ * output for the given custom scan node.
+ */
+bool
+RequestedForExplainAnalyze(CitusScanState *node)
+{
+	return (node->customScanState.ss.ps.state->es_instrument != 0);
+}
+
+
+/*
+ * ExplainAnalyzeTaskList returns a task list suitable for explain analyze. After executing
+ * these tasks, fetchedExplainAnalyzePlan of originalTaskList should be populated.
+ */
+List *
+ExplainAnalyzeTaskList(List *originalTaskList,
+					   TupleDestination *defaultTupleDest,
+					   TupleDesc tupleDesc)
+{
+	List *explainAnalyzeTaskList = NIL;
+	Task *originalTask = NULL;
+
+	foreach_ptr(originalTask, originalTaskList)
+	{
+		if (originalTask->queryCount != 1)
+		{
+			ereport(ERROR, (errmsg("cannot get EXPLAIN ANALYZE of multiple queries")));
+		}
+
+		Task *explainAnalyzeTask = copyObject(originalTask);
+		const char *queryString = TaskQueryStringForAllPlacements(explainAnalyzeTask);
+		char *wrappedQuery = WrapQueryForExplainAnalyze(queryString, tupleDesc);
+		char *fetchQuery = "SELECT worker_last_saved_explain_analyze()";
+		SetTaskQueryStringList(explainAnalyzeTask, list_make2(wrappedQuery, fetchQuery));
+
+		TupleDestination *originalTaskDest = originalTask->tupleDest ?
+											 originalTask->tupleDest :
+											 defaultTupleDest;
+
+		explainAnalyzeTask->tupleDest =
+			CreateExplainAnlyzeDestination(originalTask, originalTaskDest);
+
+		explainAnalyzeTaskList = lappend(explainAnalyzeTaskList, explainAnalyzeTask);
+	}
+
+	return explainAnalyzeTaskList;
+}
+
+
+/*
+ * WrapQueryForExplainAnalyze wraps a query into a worker_save_query_explain_analyze()
+ * call so we can fetch its explain analyze after its execution.
+ */
+static char *
+WrapQueryForExplainAnalyze(const char *queryString, TupleDesc tupleDesc)
+{
+	StringInfo columnDef = makeStringInfo();
+	for (int columnIndex = 0; columnIndex < tupleDesc->natts; columnIndex++)
+	{
+		if (columnIndex != 0)
+		{
+			appendStringInfoString(columnDef, ", ");
+		}
+
+		Form_pg_attribute attr = &tupleDesc->attrs[columnIndex];
+		char *attrType = format_type_with_typemod(attr->atttypid, attr->atttypmod);
+
+		appendStringInfo(columnDef, "field_%d %s", columnIndex, attrType);
+	}
+
+	/*
+	 * column definition cannot be empty, so create a dummy column definition for
+	 * queries with no results.
+	 */
+	if (tupleDesc->natts == 0)
+	{
+		appendStringInfo(columnDef, "dummy_field int");
+	}
+
+	StringInfo explainOptions = makeStringInfo();
+	appendStringInfo(explainOptions, "{\"verbose\": %s, \"costs\": %s, \"buffers\": %s, "
+									 "\"timing\": %s, \"summary\": %s, \"format\": \"%s\"}",
+					 CurrentDistributedQueryExplainOptions.verbose ? "true" : "false",
+					 CurrentDistributedQueryExplainOptions.costs ? "true" : "false",
+					 CurrentDistributedQueryExplainOptions.buffers ? "true" : "false",
+					 CurrentDistributedQueryExplainOptions.timing ? "true" : "false",
+					 CurrentDistributedQueryExplainOptions.summary ? "true" : "false",
+					 ExplainFormatStr(CurrentDistributedQueryExplainOptions.format));
+
+	StringInfo wrappedQuery = makeStringInfo();
+	appendStringInfo(wrappedQuery,
+					 "SELECT * FROM worker_save_query_explain_analyze(%s, %s) AS (%s)",
+					 quote_literal_cstr(queryString),
+					 quote_literal_cstr(explainOptions->data),
+					 columnDef->data);
+
+	return wrappedQuery->data;
+}
+
+
+/*
+ * SplitString splits the given string by the given delimiter.
+ *
+ * Why not use strtok_s()? Its signature and semantics are difficult to understand.
+ *
+ * Why not use strchr() (similar to do_text_output_multiline)? Although not banned,
+ * it isn't safe if by any chance str is not null-terminated.
+ */
+static List *
+SplitString(StringInfo str, char delimiter)
+{
+	if (str->len == 0)
+	{
+		return NIL;
+	}
+
+	List *tokenList = NIL;
+	StringInfo token = makeStringInfo();
+
+	for (size_t index = 0; index < str->len; index++)
+	{
+		if (str->data[index] == delimiter)
+		{
+			tokenList = lappend(tokenList, token);
+			token = makeStringInfo();
+		}
+		else
+		{
+			appendStringInfoChar(token, str->data[index]);
+		}
+	}
+
+	/* append last token */
+	tokenList = lappend(tokenList, token);
+
+	return tokenList;
+}
+
+
 /* below are private functions copied from explain.c */
 
 
diff --git a/src/backend/distributed/shared_library_init.c b/src/backend/distributed/shared_library_init.c
index b5b85ba6802..22a5cab397d 100644
--- a/src/backend/distributed/shared_library_init.c
+++ b/src/backend/distributed/shared_library_init.c
@@ -221,7 +221,8 @@ _PG_init(void)
 	 * duties. For simplicity insist that all hooks are previously unused.
 	 */
 	if (planner_hook != NULL || ProcessUtility_hook != NULL ||
-		ExecutorStart_hook != NULL || ExecutorRun_hook != NULL)
+		ExecutorStart_hook != NULL || ExecutorRun_hook != NULL ||
+		ExplainOneQuery_hook != NULL)
 	{
 		ereport(ERROR, (errmsg("Citus has to be loaded first"),
 						errhint("Place citus at the beginning of "
@@ -267,6 +268,7 @@ _PG_init(void)
 	set_join_pathlist_hook = multi_join_restriction_hook;
 	ExecutorStart_hook = CitusExecutorStart;
 	ExecutorRun_hook = CitusExecutorRun;
+	ExplainOneQuery_hook = CitusExplainOneQuery;
 
 	/* register hook for error messages */
 	emit_log_hook = multi_log_hook;
diff --git a/src/backend/distributed/transaction/transaction_management.c b/src/backend/distributed/transaction/transaction_management.c
index 418ecfce218..e9ebcffb82e 100644
--- a/src/backend/distributed/transaction/transaction_management.c
+++ b/src/backend/distributed/transaction/transaction_management.c
@@ -370,6 +370,7 @@ CoordinatedTransactionCallback(XactEvent event, void *arg)
 			RemoveIntermediateResultsDirectory();
 
 			UnSetDistributedTransactionId();
+			FreeSavedExplainPlan();
 			break;
 		}
 
diff --git a/src/include/distributed/multi_explain.h b/src/include/distributed/multi_explain.h
index 2fe71f04a29..efe76bad63e 100644
--- a/src/include/distributed/multi_explain.h
+++ b/src/include/distributed/multi_explain.h
@@ -11,11 +11,20 @@
 #define MULTI_EXPLAIN_H
 
 #include "executor/executor.h"
+#include "tuple_destination.h"
 
 /* Config variables managed via guc.c to explain distributed query plans */
 extern bool ExplainDistributedQueries;
 extern bool ExplainAllTasks;
 
 extern void FreeSavedExplainPlan(void);
+extern void CitusExplainOneQuery(Query *query, int cursorOptions, IntoClause *into,
+								 ExplainState *es, const char *queryString, ParamListInfo
+								 params,
+								 QueryEnvironment *queryEnv);
+extern List * ExplainAnalyzeTaskList(List *originalTaskList,
+									 TupleDestination *defaultTupleDest, TupleDesc
+									 tupleDesc);
+extern bool RequestedForExplainAnalyze(CitusScanState *node);
 
 #endif /* MULTI_EXPLAIN_H */
diff --git a/src/include/distributed/multi_physical_planner.h b/src/include/distributed/multi_physical_planner.h
index 78f13fd3cee..5658731b10b 100644
--- a/src/include/distributed/multi_physical_planner.h
+++ b/src/include/distributed/multi_physical_planner.h
@@ -337,6 +337,9 @@ typedef struct Task
 	 * NULL, in which case executor might use a default destination.
 	 */
 	struct TupleDestination *tupleDest;
+
+	StringInfo fetchedExplainAnalyzePlan;
+	int fetchExplainAnalyzePlacementIndex;
 } Task;
 
 
