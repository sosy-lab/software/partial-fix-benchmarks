SSM版本实践
我觉得短字符串比较效率是 lua 程序的热点，不易修改。而回收时机问题未必严重。因为临时字符串的问题看起来并没有比过去更严重。产生一个临时字符串依然需要等待产生它的 vm 收集。这是 lua gc 原有的问题，并不是新问题。 lua 5.4 也会有所改进。
是不是 ssm 过大导致插入效率变低？那个统计看了吗？平均 slot 的长度，方差？
使用了最新ssm版本重新测试，没有出现之前的回收效率问题，可以确认是bug引起的
1000机器人压测每分钟平均新增100w字符串，回收100w字符串，基本持平，可以证明这些基本上是临时字符串了
variance为0.5

测试了sharedshrstr和ssm版本
* sharedshrstr内存占用居然要少一点，估计与回收快有关系，
* sharedshrstr的cpu比ssm版本高10%，分别是165%，155%
  分析是段字符串比较的消耗和内存回收速度导致的差异

```
#define eqshrstr(a,b)	check_exp((a)->tt == LUA_TSHRSTR, (a) == (b)?1:((isshared(a)||isshared(b))?luaS_eqshrstr(a, b):0))
```
这个函数的有额外消耗在于2个字符串中有shared字符串，才需要比较内存。否则和原来是一致
对于访问sharetable的表是减缓了速度。对于非shared字符串和原来是一样的消耗

还有就是这个实现比较简单

短字符串做 key 的时候，因为有 interning 优化才做到了 O(1) 的访问速度，而这个在 lua 中是个热点。字符串共享不仅仅用于共享表，更主要的用途是共享 proto ，这才是重点。而所有源码中的短字符串常量本质上都是共享的。
我打算把 ssm 分支合并到主干了。
我已经把 ssm rebase 到 master top ，可以看看还可能有什么问题，然后就合并到主干了。
了解了，现在是看ssm版本么？感觉漏掉了很多提交啊
```
$ git diff ssm ssmhong
diff --git a/3rd/lua/lfunc.c b/3rd/lua/lfunc.c
index eaf100b..20dfc15 100644
--- a/3rd/lua/lfunc.c
+++ b/3rd/lua/lfunc.c
@@ -157,7 +157,7 @@ void luaF_shareproto (Proto *f) {
     return;
   MAKESHARED(f->source);
   for (i = 0; i < f->sizek; i++) {
-    if (ttype(&f->k[i]) == LUA_TSTRING)
+    if (ttnov(&f->k[i]) == LUA_TSTRING)
       MAKESHARED(tsvalue(&f->k[i]));
   }
   for (i = 0; i < f->sizeupvalues; i++)
diff --git a/3rd/lua/lgc.c b/3rd/lua/lgc.c
index 235ee89..6370db7 100644
--- a/3rd/lua/lgc.c
+++ b/3rd/lua/lgc.c
@@ -65,7 +65,7 @@
 */
 #define maskcolors     (~(bitmask(BLACKBIT) | WHITEBITS))
 #define makewhite(g,x) \
- (x->marked = cast_byte((x->marked & maskcolors) | luaC_white(g)))
+ (x->marked = cast_byte((x->marked & maskcolors) | (x->marked & bitmask(SHAREBIT)) | luaC_white(g)))

 #define white2gray(x)  resetbits(x->marked, WHITEBITS)
 #define black2gray(x)  resetbit(x->marked, BLACKBIT)
@@ -756,7 +756,7 @@ static GCObject **sweeplist (lua_State *L, GCObject **p, lu_mem count) {
       freeobj(L, curr);  /* erase 'curr' */
     }
     else {  /* change mark to 'white' */
-      curr->marked = cast_byte((marked & maskcolors) | white);
+      curr->marked = cast_byte((marked & maskcolors) | (marked & bitmask(SHAREBIT)) |white);
       p = &curr->next;  /* go to next element */
     }
   }
diff --git a/3rd/lua/lstring.c b/3rd/lua/lstring.c
index 7ad062f..3a646d0 100644
--- a/3rd/lua/lstring.c
+++ b/3rd/lua/lstring.c
@@ -230,9 +230,10 @@ struct shrmap {

 static struct shrmap SSM;

-#define ADD_SREF(ts) ATOM_INC(&((ts)->u.ref))
-#define DEC_SREF(ts) ATOM_DEC(&((ts)->u.ref))
+#define ADD_SREF(ts) do {if(ATOM_INC(&((ts)->u.ref))==1) ATOM_DEC(&SSM.garbage);}while(0)
+#define DEC_SREF(ts) do {if(ATOM_DEC(&((ts)->u.ref))==0) ATOM_INC(&SSM.garbage);}while(0)
 #define ZERO_SREF(ts) ((ts)->u.ref == 0)
+#define FREE_SREF(ts) do {free(ts);ATOM_DEC(&SSM.total);ATOM_DEC(&SSM.garbage);}while(0)

 static struct ssm_ref *
 newref(int size) {
@@ -335,8 +336,11 @@ markref(struct ssm_ref *r, TString *s, int changeref) {
        unsigned int h = s->hash;
        int slot = lmod(h, r->hsize);
        TString * hs = r->hash[slot];
-       if (hs == s)
+       if (hs == s){
+               if (changeref)
+                       DEC_SREF(s);
                return;
+       }
        ++r->nuse;
        if (r->nuse >= r->hsize && r->hsize <= MAX_INT/2) {
                expand_ref(r, changeref);
@@ -346,6 +350,8 @@ markref(struct ssm_ref *r, TString *s, int changeref) {
        if (hs != NULL) {
                if (hs == s) {
                        --r->nuse;
+                       if (changeref)
+                               DEC_SREF(s);
:


```
呀，难道我操作失误了？我刚才 pull & rebase & push -f 了。

麻烦你帮忙做一下 merge ，然后提个 pr 吧。

( https://github.com/hongling0/skynet/tree/ssm_top )
提交到哪个分支
ssm分支提示 Can’t automatically merge. Don’t worry, you can still create the pull request.
直接到ssm分支吗？
我刚才利用你的 github 仓库的 ssm 分支抢修了一下，你看看现在是不是对的？
```

$ git diff ssm ssmhong
diff --git a/3rd/lua/lgc.c b/3rd/lua/lgc.c
index dc09793..6370db7 100644
--- a/3rd/lua/lgc.c
+++ b/3rd/lua/lgc.c
@@ -1086,7 +1086,6 @@ static lu_mem singlestep (lua_State *L) {
         return (n * GCFINALIZECOST);
       }
       else {  /* emergency mode or no more finalizers */
-        luaS_collect(g, 0);  /* send short strings set to gc thread */
         g->gcstate = GCSpause;  /* finish collection */
         return 0;
       }
diff --git a/3rd/lua/lua.h b/3rd/lua/lua.h
index a944404..675bc0f 100644

```
rebase 的时候好像有一个 commits 被 rebase 了两次。 https://github.com/cloudwu/skynet/commits/ssm/3rd/lua/lgc.c 

这里  "rewrite SSM and clonefunction" 这个似乎多产生了一条提交，分别是  https://github.com/cloudwu/skynet/commit/133026ef18191d3954ae0af461481a8c788c0edb#diff-03dfba5e3489856eedd3551a4c83b2f6 和  https://github.com/cloudwu/skynet/commit/3c9f4525af1d00c643d43f63677641ea48952508#diff-03dfba5e3489856eedd3551a4c83b2f6

前一个似乎是自动 rebase 的一个错误。我把它去掉。
我用 rebase -i 重新整理一下，删除了多出来的提交。
以后我还是少用 rebase , 太考验 git 功力了。
我合并的时候，lstring.c有个冲突，解决了冲突之后就好了。
```
lua_checksig_

#define LUA_CACHELIB
LUAMOD_API int (luaopen_cache) (lua_State *L);
```
这些代码被删掉了
不太明白。什么被删掉了？相对之前的版本吗？
对了一遍 ssm 和 master 的 diff ，感觉没有漏掉什么了。
https://github.com/cloudwu/skynet/pull/1030  我打算端午节后合并。
@hongling0 还有一个问题需要你也帮忙做一些测试（或是 review 一下）。

我认为 https://github.com/cloudwu/skynet/blob/ssm/3rd/lua/lgc.c#L749 此处的
 `| bitmask(SHAREBIT);  /* shared object never dead */` 

是多余的。因为共享来的对象是在别的 vm 创建出来的，不应该出现在当前 vm 的任何链表上，所以也不会被 sweeplist 清理掉。这里检查 SHAREBIT 就是多余的。

而这个检查还会导致真正需要关闭 sharetable 的母体的时候，无法正确的 free 这个 object 。

你能否帮忙测试一下，去掉这个 `| bitmask(SHAREBIT)` 会不会有问题。

如果更严格一点，在 sweeplist 中，不应该遍历到有 sharebit 的对象。(标注 sharebit 的 vm 应该关闭了 gc ）
当时测试我加了lua_assert的定义，发现有使用sharetable的虚拟机里面有报checkliveness的错误，为了排除错误，我增加这个Revert SHAREBIT 的提交，使创建sharetable的虚拟机不会因为gc导致丢失sharebit位。没有考虑到关闭该虚拟机的这个问题。

不gc的话是可以去掉上面的代码。

我考虑一下，去掉会导致 gc 的风险。所以我换了一种改法： https://github.com/cloudwu/skynet/commit/ca4f549f0da2828b5d96fab688da39a32859c58a
@cloudwu 我把行数看成了759
749行的代码是你之前的修改，这个代码是可以去掉的。简单测试了下是正常的。

重新定义了lua_assert测试发现几个问题！

- 由于 在 (fafc4cad34c45c99cd494608b00339af9bdbc67c) 把LUA_TSHRSTR的shared属性去掉了，现在LUA_TSHRSTR相关的checkliveness，luaC_barrier里面的lua_assert都不会通过

1. 修改checkliveness和luaC_barrier宏，排除LUA_TSHRSTR之后可以解决，但是不确定是否有还有其他地方需要修改

2. 尝试把LUA_TSHRSTR恢复shared属性，但是需要修改luaS_clearcache避免字符串引用计数错误 ，这样也能解决断言问题

> (fafc4cad34c45c99cd494608b00339af9bdbc67c) 是为在luaS_clearcache避免字符串引用计数错误而提交的
> 这个问题虽然影响不大（断言代码正常情况下是定义为空的，后续有有足够的判断去避免出问题），但是对于使用lua_assert做调试不太友好，个人倾向方案2来解决这个问题

- Revert SHAREBIT的提交并不能解决标注sharebit 的vm的gc问题，因为标记流程不会标记到共享表中的LUA_TSHRSTR，这些字符串在gc中就会被回收掉。
目前是正常的，因为sharetable没有开gc。

- lua_assert检测到 close_state中 gettotalbytes(g)==sizeof(LG)失败，暂时没有找到原因


749 如果去掉，会因为 shared object 并没有置黑，如果在母体中发生 gc ，就会导致不正确的清理行为。所以我觉得应该保留。但在 close 阶段要保证清理掉。

sharebit 不适合还原。因为多个 sharedtable 可能共享相同的 long string 。在没有引用计数时，必须通过扫描整个 vm 才能确保正确。

我不是很明白为什么短字符串的 shared bit 和 `luaS_clearcache` 有关联，可否解释一下。
> 以前测试的时候发现，有概率从skynet.getenv获得的字符串为nil。

g->strcache里面的字符串是直接拷贝地址的，没有增加引用计数。
```
void luaS_clearcache (global_State *g) {
  int i, j;
  for (i = 0; i < STRCACHE_N; i++)
    for (j = 0; j < STRCACHE_M; j++) {
    if (!isshared(g->strcache[i][j]) && iswhite(g->strcache[i][j]))  /* will entry be collected? */
      g->strcache[i][j] = g->memerrmsg;  /* replace it with something fixed */
    }
}
```
LUA_TSHRSTR是shared的话就不会被清理掉了，会导致后面使用luaS_new的时候复活这个字符串
```
TString *luaS_new (lua_State *L, const char *str) {
  unsigned int i = point2uint(str) % STRCACHE_N;  /* hash */
  int j;
  TString **p = G(L)->strcache[i];
  for (j = 0; j < STRCACHE_M; j++) {
    if (strcmp(str, getstr(p[j])) == 0)  /* hit? */
      return p[j];  /* that is it */
  }
  /* normal route */
  for (j = STRCACHE_M - 1; j > 0; j--)
    p[j] = p[j - 1];  /* move out last element */
  /* new element is first in the list */
  p[0] = luaS_newlstr(L, str, strlen(str));
  return p[0];
}
```
我明白了，其实所有的 shared string 都可以直接在 `luaS_clearcache` 阶段从 cache 中清掉。

因为，在 newstring 时，从 cache 中复活一个共享字符串的意义不大。如果它是短字符串，现在没有 O(1) 的办法知道它是否还在当前 vm 中。目前是用一个标记集合转交给独立的 gc 线程去统计的，white flag 在这里不具备检查的意义。

如果它是长字符串，本身存在于 cache  中的机会就非常罕见了，因为共享长字符串一般都是从外部 vm 过来，并非当前 vm new 出来的。

针对 short string 的 shared bit 我已经做了修改。`close_state` 的问题，还需要一起看一下。
关于共享短字符串在母体中不被 mark ，开启母体的 gc 后会被减引用的问题。我认为这样修改就可以了：https://github.com/cloudwu/skynet/commit/b45384cc9f9e78c10cfbc607dcbe11cdcf86dd1c

当然，目前母体是不开 gc 的。
https://github.com/cloudwu/skynet/commit/24b333a5e96aeb1fb3ec6f0f304ac9d514551184   `luaS_fix` 是个内部 api ，所以加了一个公开 api `lua_sharestring` 来做这件事。
> close_state中 gettotalbytes(g)==sizeof(LG)失败

应该是之前没有清理 shared bit 的对象导致的泄露，就是最近的 patch 想解决的问题。现在应该没有了？ @hongling0 
我测试了最新版本，在make_matrix调用了fullgc也貌似ok，close_state中 gettotalbytes(g)==sizeof(LG)断言失败的问题也没有了。

我还有几个需要确认的地方
- luaF_shareproto 应该把Proto本身标记shared！以便于其他vm不会通过Luaclosure标记到它（测试发现Proto对象会出现在其他vm的gray链表中）

- 对于shared function 的支持貌似不对！
1. sharetable中LClosure没有标记shared
2. reallymarkobject没有对LUA_TLCL排除shared

- 对于checkliveness需要排除LUA_TSHRSTR。因为的marked标记是WHITEBITS，非shared的LUA_TSHRSTR必然是isdead，修改如下
```
#define checkliveness(L,obj) \
	lua_longassert(!iscollectable(obj) || ttisshrstring(obj) \
		(righttt(obj) && (L == NULL || !isdead(G(L),gcvalue(obj)))))
```
- luaC_barrier_函数中的lua_assert遇到非shared的LUA_TSHRSTR的isdead判断也能不能通过

- luaC_barrier_中的makewhite会修改共享对象和LUA_TSHRSTR的marked值

我今天对 lua 5.4 alpha 尝试做了修改， https://github.com/cloudwu/lua/tree/skynet?files=1

目前还没做完。但我反思了之前的修改，的确有点问题。如果 proto 出现在别的 vm 链表上肯定是 bug 。

shared 的检测应该在 reallymarkobject 和 barrier 上做就够了。这样就能阻止 gc mark 触碰shared 对象。上面的 bug 可能是没有在 barrier 检查的后果。


btw, 5.4 的思路是先对短字符串比较做点修改。但是之后还是考虑把  ssm 搬过去。顺便比较两个方案。但 5.4 的 gc 要复杂的多，需要再研究一下。
我修复了几个 bug 。我认为 proto 被串到其它 vm 中，是因为漏掉 mark shared 造成的，加上后不应出现这种情况。

无 upvalue 的 lua function 目前可以被共享，相应的  reallymarkobject 也处理了。

现在所有的 shortstring 一律被标记为 shared, 这样，checkliveness 我认为就不必修改。因为 !isdead 一定为真（因为有 shared 标记）`luaC_barrier_` 应该也没有问题了。

5.3 和 5.4 的不同在于 shortstring 在 ssm 中，依赖 gc 来 mark 。所以不能像 5.4 做的修改那样，简单的在 barrier 检查 shared bit 简单阻断。

ps. 由于周末放假不在公司，所以我只改了代码没有环境测试。还是拜托 @hongling0 帮忙看看，今天晚上的修改暂时放在 bugfix 分支上。



一点想法：如果母体一定会关闭 gc , 那么 shared bit 这个东西未必有存在的意义。因为我们只要把所有共享的对象都置为黑色就够了。

如果对象不在当前 vm 中，那么当前的 gc 过程就没有机会改变其颜色，黑色的节点也可以阻止 gc 深入遍历它。

因为共享表不可写，所以也不会触发 barrierback 改写颜色。 这样，只需要单独在 table 结构的 flag 上标记它不可改变就够了。
测试了下，目前没有发现什么问题！
btw：jemalloc有5.2.0版本了( https://github.com/jemalloc/jemalloc/releases/tag/5.2.0) 
已经更新 jemalloc 5.2.0 

@lvzixun  给 sharetable 加了热更新 https://github.com/cloudwu/skynet/pull/1033 ，已经合并。也可以测试一下。
@hongling0 我还有一点疑问。之前， Proto 漏了 share bit 的标记。

在 `traverseLclosure` 的第一行，`markobjectN(g, cl->p);  /* mark its prototype */` 这里会 mark 这个 proto 。markobjectN 会调用 markobject ，

```C
#define markobject(g,t)	{ if (iswhite(t)) reallymarkobject(g, obj2gco(t)); }
```

除非这里的 cl->p 的 iswhite 没有通过，否则 reallymarkobject 就会检查 shared bit ，如果漏了，它会把 proto 调用挂在当前 vm 的 gray 链表上。最终 close 的时候，所有链表上的对象都会被 free 。

共享的 proto 对象如果被 free ，会引起崩溃。

不知道为何你之前的测试从未发生？
为了避免掩盖问题，我把 Proto 结构中的 l_G 去掉了。因为有了 shared bit 后，这个东西就多余了。shared bit 可以阻止外部 vm 标记共享的 Proto 。之前则是靠比较 l_G 和当前的 g 是否一致来判断是外部的 Proto 还是本地的。

https://github.com/cloudwu/skynet/commit/9ff54b7c1ffa797e4e8c52f3ec6735979cb8cdd6

@hongling0 我暂时放在 bugfix 分支，你可以看看。btw，这样也进一步减少了对官方 lua 的修改。不过最关键想了解的还是：是什么导致了之前这个问题没有显现出来？
我猜了下你看对不对
free对象走的是 g->allgc链表
```
g->sweepgc = sweeplist(L, &g->allgc, 1);
```
递归遍历走的是对象的gclist链表
```
#define linkgclist(o,p)	((o)->gclist = (p), (p) = obj2gco(o))
```
他们走的是不同的路径，所以不会free错对象

但是多线程操作gclist链表没有崩溃这个还是不好理解
- 是不是因为其特殊的遍历顺序导致他不出异常呢
- 或许遍历到失效的对象（其他线程修改了这个对象的gclist指针之后又free这个指针指向的对象）的概率太低
的确不会 free。但我觉得 bug 会把同一个 proto 对象，在不同 vm 间移动，（多线程不会错）导致一个 vm 的 gray 链表断掉（把其它 vm 的 list 串过来，部分 gray 对象无法 mark 。这样可能导致 gc 清理一些不该清的对象（因为没有mark )。
我倾向于出错的概率很低，因为多线程测试比较难覆盖到这点。gc 并不是常见的流程，多个线程并发，gc 碰在一起，且刚好在同一个阶段的机会很少。

不过你前面说，有 proto 被串在其它 vm 里，我觉得观察到这种现象后，如果继续跑 gc 流程，几乎一定会出错。因为两个 vm 共享了一个 gray list ，几乎一定会一起 mark 链表上的相同对象。

所以建议再测试一下去掉 l_G 的版本，如果还有问题，暴露的机会大一些。
我觉得有下面几点可以考虑下，先不考虑竞争冲突（进入gray列表的的对象的gclist指针在一定时间内没有被修改）
- 只有有gclist的对象才会进入gray list，Closure，Table，lua_State, Proto这些。
- gclist队列实际是栈的用法，弹出栈顶的对象进行遍历标记，遍历到上面的对象的话就入栈。
- 之前只有Proto是共享的。通过Proto能关联的对象只有子Proto和const K对象和其他字符串对象。不会扩散到其他对象（扩散也没有问题，只要这些对象的gclist指针在一段时间没有被修改）。
- Proto所在的虚拟机是关闭gc的(很难触发)，标记错了也不会触发回收，而且这些虚拟机最终也不会关闭，就不容易发现问题
- 把Proto关联的对象遍历完之后，gay列表会回到这个Proto入栈之前的状态。

所以入栈并不一定导致出错，只要不会有2个线程同时标记到有关联的proto结构就没有问题。

我今天想到一个方法即保证短字符串比较的性能，又可以去掉 ssm 。https://blog.codingnow.com/2019/06/string_comparison.html
https://github.com/cloudwu/skynet/tree/stringid   @hongling0  我今天改了一版，把 ssm 去掉了，给 TString 加了个 id 用于做比较。应该和你之前提交过的那版类似。你看看 ？
简单测试了下，性能上达到了设计目的。

> 我今天想到一个方法即保证短字符串比较的性能，又可以去掉 ssm 。https://blog.codingnow.com/2019/06/string_comparison.html

cpu和ssm之前的版本，ssm版本差异不大。
后面再详细跑一下。
我已经把 stringid 分支合并到主干，ssm 特性已经完全去掉了。