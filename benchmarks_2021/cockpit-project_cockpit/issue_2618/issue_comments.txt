assertion failed (batch->refs > 0)
http://files.cockpit-project.org/hubbot/916d026378af93cd9f2383fa4d992fdebd125956_f22_x86-64/hubbot.html

Another case of this. Now with more debugging info:

```
Unexpected journal message 'BATCH 22081 (refs -2098759536)'
Unexpected journal message '**'
Unexpected journal message 'cockpit-bridge:ERROR:src/bridge/cockpitdbuscache.c:290:_batch_unref: assertion failed: (batch->refs > 0)'
```

http://files.cockpit-project.org/hubbot/24937605c565d24c93fc3f97022acd8faa21a851_f22_x86-64/hubbot.html

Same backtrace:

```
Aug 27 09:59:36 m16 systemd-coredump[1102]: Process 1059 (cockpit-bridge) of user 1000 dumped core.

                                            Stack trace of thread 1059:
                                            #0  0x00007f7d14a1f9c8 raise (libc.so.6)
                                            #1  0x00007f7d14a2165a abort (libc.so.6)
                                            #2  0x00007f7d158872a5 g_assertion_message (libglib-2.0.so.0)
                                            #3  0x00007f7d1588733a g_assertion_message_expr (libglib-2.0.so.0)
                                            #4  0x000056418211bd51 _batch_unref (cockpit-bridge)
                                            #5  0x000056418211cd5b on_get_all_reply (cockpit-bridge)
                                            #6  0x00007f7d15e17ba7 g_simple_async_result_complete (libgio-2.0.so.0)
                                            #7  0x00007f7d15e748cc g_dbus_connection_call_done (libgio-2.0.so.0)
                                            #8  0x00007f7d15e17ba7 g_simple_async_result_complete (libgio-2.0.so.0)
                                            #9  0x00007f7d15e17c09 complete_in_idle_cb (libgio-2.0.so.0)
                                            #10 0x00007f7d15860a8a g_main_context_dispatch (libglib-2.0.so.0)
                                            #11 0x00007f7d15860e20 g_main_context_iterate.isra.29 (libglib-2.0.so.0)
                                            #12 0x00007f7d15860ecc g_main_context_iteration (libglib-2.0.so.0)
                                            #13 0x00005641821033cc run_bridge (cockpit-bridge)
                                            #14 0x00007f7d14a0b700 __libc_start_main (libc.so.6)
                                            #15 0x00005641821039d9 _start (cockpit-bridge)
```

As suspected, memory corruption:

```

OK
Unexpected journal message '*** Error in `cockpit-bridge': corrupted double-linked list: 0x000055a14af35d00 ***'
Unexpected journal message '======= Backtrace: ========='
Unexpected journal message '/lib64/libc.so.6(+0x77a8d)[0x7fe98b20aa8d]'
Unexpected journal message '/lib64/libc.so.6(+0x7e793)[0x7fe98b211793]'
Unexpected journal message '/lib64/libc.so.6(+0x80bfd)[0x7fe98b213bfd]'
Unexpected journal message '/lib64/libc.so.6(__libc_malloc+0x6e)[0x7fe98b21646e]'
Unexpected journal message '/lib64/libc.so.6(realloc+0x2d8)[0x7fe98b2179d8]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_realloc+0x39)[0x7fe98c00e749]'
Unexpected journal message '/lib64/libglib-2.0.so.0(+0x1d429)[0x7fe98bfdc429]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_array_set_size+0x54)[0x7fe98bfdcd44]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_byte_array_set_size+0x9)[0x7fe98bfdd8c9]'
Unexpected journal message 'cockpit-bridge(+0x2cbc6)[0x55a149f67bc6]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_main_context_dispatch+0x15a)[0x7fe98c008a8a]'
Unexpected journal message '/lib64/libglib-2.0.so.0(+0x49e20)[0x7fe98c008e20]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_main_context_iteration+0x2c)[0x7fe98c008ecc]'
Unexpected journal message 'cockpit-bridge(+0x24829)[0x55a149f5f829]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_object_unref+0xfc)[0x7fe98c30da5c]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit_valist+0xc7d)[0x7fe98c322ced]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit+0x8f)[0x7fe98c32329f]'
Unexpected journal message 'cockpit-bridge(+0x11c3d)[0x55a149f4cc3d]'
Unexpected journal message '/lib64/libffi.so.6(ffi_call_unix64+0x4c)[0x7fe98af90db0]'
Unexpected journal message '/lib64/libffi.so.6(ffi_call+0x2f8)[0x7fe98af90818]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_cclosure_marshal_generic+0x1f9)[0x7fe98c3094f9]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_closure_invoke+0x145)[0x7fe98c308cd5]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(+0x21539)[0x7fe98c31a539]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit_valist+0xa72)[0x7fe98c322ae2]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit+0x8f)[0x7fe98c32329f]'
Unexpected journal message 'cockpit-bridge(+0x31ba1)[0x55a149f6cba1]'
Unexpected journal message '/lib64/libffi.so.6(ffi_call_unix64+0x4c)[0x7fe98af90db0]'
Unexpected journal message '/lib64/libffi.so.6(ffi_call+0x2f8)[0x7fe98af90818]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_cclosure_marshal_generic+0x1f9)[0x7fe98c3094f9]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_closure_invoke+0x145)[0x7fe98c308cd5]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(+0x21d5f)[0x7fe98c31ad5f]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit_valist+0xa72)[0x7fe98c322ae2]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit+0x8f)[0x7fe98c32329f]'
Unexpected journal message 'cockpit-bridge(+0x317cd)[0x55a149f6c7cd]'
Unexpected journal message 'cockpit-bridge(+0x2eec7)[0x55a149f69ec7]'
Unexpected journal message '/lib64/libffi.so.6(ffi_call_unix64+0x4c)[0x7fe98af90db0]'
Unexpected journal message '/lib64/libffi.so.6(ffi_call+0x2f8)[0x7fe98af90818]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_cclosure_marshal_generic_va+0x3b5)[0x7fe98c309a25]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(+0xff04)[0x7fe98c308f04]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit_valist+0xc0d)[0x7fe98c322c7d]'
Unexpected journal message '/lib64/libgobject-2.0.so.0(g_signal_emit+0x8f)[0x7fe98c32329f]'
Unexpected journal message 'cockpit-bridge(+0x2cc33)[0x55a149f67c33]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_main_context_dispatch+0x15a)[0x7fe98c008a8a]'
Unexpected journal message '/lib64/libglib-2.0.so.0(+0x49e20)[0x7fe98c008e20]'
Unexpected journal message '/lib64/libglib-2.0.so.0(g_main_context_iteration+0x2c)[0x7fe98c008ecc]'
Unexpected journal message 'cockpit-bridge(+0x103cc)[0x55a149f4b3cc]'
Unexpected journal message '/lib64/libc.so.6(__libc_start_main+0xf0)[0x7fe98b1b3700]'
Unexpected journal message 'cockpit-bridge(+0x109d9)[0x55a149f4b9d9]'
Unexpected journal message '======= Memory map: ========'
Unexpected journal message '55a149f3b000-55a149f84000 r-xp 00000000 fc:03 8727129                    /usr/bin/cockpit-bridge'
Unexpected journal message '55a14a184000-55a14a186000 r--p 00049000 fc:03 8727129                    /usr/bin/cockpit-bridge'
Unexpected journal message '55a14a186000-55a14a187000 rw-p 0004b000 fc:03 8727129                    /usr/bin/cockpit-bridge'
Unexpected journal message '55a14aeca000-55a14af48000 rw-p 00000000 00:00 0                          [heap]'
Unexpected journal message '7fe970000000-7fe970022000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe970022000-7fe974000000 ---p 00000000 00:00 0'
Unexpected journal message '7fe978000000-7fe978022000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe978022000-7fe97c000000 ---p 00000000 00:00 0'
Unexpected journal message '7fe97c000000-7fe97c022000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe97c022000-7fe980000000 ---p 00000000 00:00 0'
Unexpected journal message '7fe980000000-7fe980021000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe980021000-7fe984000000 ---p 00000000 00:00 0'
Unexpected journal message '7fe9865c6000-7fe9865c7000 ---p 00000000 00:00 0'
Unexpected journal message '7fe9865c7000-7fe986dc7000 rw-p 00000000 00:00 0                          [stack:1079]'
Unexpected journal message '7fe986dc7000-7fe986dc8000 ---p 00000000 00:00 0'
Unexpected journal message '7fe986dc8000-7fe9875c8000 rw-p 00000000 00:00 0                          [stack:1078]'
Unexpected journal message '7fe9875c8000-7fe9875c9000 ---p 00000000 00:00 0'
Unexpected journal message '7fe9875c9000-7fe987dc9000 rw-p 00000000 00:00 0                          [stack:1077]'
Unexpected journal message '7fe987dc9000-7fe987dca000 ---p 00000000 00:00 0'
Unexpected journal message '7fe987dca000-7fe9885ca000 rw-p 00000000 00:00 0                          [stack:1073]'
Unexpected journal message '7fe9885ca000-7fe9885d6000 r-xp 00000000 fc:03 17320609                   /usr/lib64/libnss_files-2.21.so'
Unexpected journal message '7fe9885d6000-7fe9887d5000 ---p 0000c000 fc:03 17320609                   /usr/lib64/libnss_files-2.21.so'
Unexpected journal message '7fe9887d5000-7fe9887d6000 r--p 0000b000 fc:03 17320609                   /usr/lib64/libnss_files-2.21.so'
Unexpected journal message '7fe9887d6000-7fe9887d7000 rw-p 0000c000 fc:03 17320609                   /usr/lib64/libnss_files-2.21.so'
Unexpected journal message '7fe9887d7000-7fe9887e6000 r-xp 00000000 fc:03 16899846                   /usr/lib64/libbz2.so.1.0.6'
Unexpected journal message '7fe9887e6000-7fe9889e5000 ---p 0000f000 fc:03 16899846                   /usr/lib64/libbz2.so.1.0.6'
Unexpected journal message '7fe9889e5000-7fe9889e6000 r--p 0000e000 fc:03 16899846                   /usr/lib64/libbz2.so.1.0.6'
Unexpected journal message '7fe9889e6000-7fe9889e7000 rw-p 0000f000 fc:03 16899846                   /usr/lib64/libbz2.so.1.0.6'
Unexpected journal message '7fe9889e7000-7fe9889fc000 r-xp 00000000 fc:03 16900894                   /usr/lib64/libelf-0.163.so'
Unexpected journal message '7fe9889fc000-7fe988bfb000 ---p 00015000 fc:03 16900894                   /usr/lib64/libelf-0.163.so'
Unexpected journal message '7fe988bfb000-7fe988bfc000 r--p 00014000 fc:03 16900894                   /usr/lib64/libelf-0.163.so'
Unexpected journal message '7fe988bfc000-7fe988bfd000 rw-p 00015000 fc:03 16900894                   /usr/lib64/libelf-0.163.so'
Unexpected journal message '7fe988bfd000-7fe988c01000 r-xp 00000000 fc:03 16900161                   /usr/lib64/libattr.so.1.1.0'
Unexpected journal message '7fe988c01000-7fe988e00000 ---p 00004000 fc:03 16900161                   /usr/lib64/libattr.so.1.1.0'
Unexpected journal message '7fe988e00000-7fe988e01000 r--p 00003000 fc:03 16900161                   /usr/lib64/libattr.so.1.1.0'
Unexpected journal message '7fe988e01000-7fe988e02000 rw-p 00004000 fc:03 16900161                   /usr/lib64/libattr.so.1.1.0'
Unexpected journal message '7fe988e02000-7fe988e70000 r-xp 00000000 fc:03 16899818                   /usr/lib64/libpcre.so.1.2.5'
Unexpected journal message '7fe988e70000-7fe989070000 ---p 0006e000 fc:03 16899818                   /usr/lib64/libpcre.so.1.2.5'
Unexpected journal message '7fe989070000-7fe989071000 r--p 0006e000 fc:03 16899818                   /usr/lib64/libpcre.so.1.2.5'
Unexpected journal message '7fe989071000-7fe989072000 rw-p 0006f000 fc:03 16899818                   /usr/lib64/libpcre.so.1.2.5'
Unexpected journal message '7fe989072000-7fe989098000 r-xp 00000000 fc:03 16899913                   /usr/lib64/libexpat.so.1.6.0'
Unexpected journal message '7fe989098000-7fe989298000 ---p 00026000 fc:03 16899913                   /usr/lib64/libexpat.so.1.6.0'
Unexpected journal message '7fe989298000-7fe98929b000 r--p 00026000 fc:03 16899913                   /usr/lib64/libexpat.so.1.6.0'
Unexpected journal message '7fe98929b000-7fe98929c000 rw-p 00029000 fc:03 16899913                   /usr/lib64/libexpat.so.1.6.0'
Unexpected journal message '7fe98929c000-7fe9892b2000 r-xp 00000000 fc:03 18175180                   /usr/lib64/libgcc_s-5.1.1-20150618.so.1'
Unexpected journal message '7fe9892b2000-7fe9894b1000 ---p 00016000 fc:03 18175180                   /usr/lib64/libgcc_s-5.1.1-20150618.so.1'
Unexpected journal message '7fe9894b1000-7fe9894b2000 r--p 00015000 fc:03 18175180                   /usr/lib64/libgcc_s-5.1.1-20150618.so.1'
Unexpected journal message '7fe9894b2000-7fe9894b3000 rw-p 00016000 fc:03 18175180                   /usr/lib64/libgcc_s-5.1.1-20150618.so.1'
Unexpected journal message '7fe9894b3000-7fe9894f9000 r-xp 00000000 fc:03 16900375                   /usr/lib64/libdw-0.163.so'
Unexpected journal message '7fe9894f9000-7fe9896f9000 ---p 00046000 fc:03 16900375                   /usr/lib64/libdw-0.163.so'
Unexpected journal message '7fe9896f9000-7fe9896fb000 r--p 00046000 fc:03 16900375                   /usr/lib64/libdw-0.163.so'
Unexpected journal message '7fe9896fb000-7fe9896fc000 rw-p 00048000 fc:03 16900375                   /usr/lib64/libdw-0.163.so'
Unexpected journal message '7fe9896fc000-7fe98970d000 r-xp 00000000 fc:03 16899924                   /usr/lib64/libgpg-error.so.0.13.0'
Unexpected journal message '7fe98970d000-7fe98990c000 ---p 00011000 fc:03 16899924                   /usr/lib64/libgpg-error.so.0.13.0'
Unexpected journal message '7fe98990c000-7fe98990d000 r--p 00010000 fc:03 16899924                   /usr/lib64/libgpg-error.so.0.13.0'
Unexpected journal message '7fe98990d000-7fe98990e000 rw-p 00011000 fc:03 16899924                   /usr/lib64/libgpg-error.so.0.13.0'
Unexpected journal message '7fe98990e000-7fe9899e9000 r-xp 00000000 fc:03 16900200                   /usr/lib64/libgcrypt.so.20.0.3'
Unexpected journal message '7fe9899e9000-7fe989be9000 ---p 000db000 fc:03 16900200                   /usr/lib64/libgcrypt.so.20.0.3'
Unexpected journal message '7fe989be9000-7fe989bea000 r--p 000db000 fc:03 16900200                   /usr/lib64/libgcrypt.so.20.0.3'
Unexpected journal message '7fe989bea000-7fe989bf3000 rw-p 000dc000 fc:03 16900200                   /usr/lib64/libgcrypt.so.20.0.3'
Unexpected journal message '7fe989bf3000-7fe989c18000 r-xp 00000000 fc:03 16899839                   /usr/lib64/liblzma.so.5.2.0'
Unexpected journal message '7fe989c18000-7fe989e17000 ---p 00025000 fc:03 16899839                   /usr/lib64/liblzma.so.5.2.0'
Unexpected journal message '7fe989e17000-7fe989e18000 r--p 00024000 fc:03 16899839                   /usr/lib64/liblzma.so.5.2.0'
Unexpected journal message '7fe989e18000-7fe989e19000 rw-p 00025000 fc:03 16899839                   /usr/lib64/liblzma.so.5.2.0'
Unexpected journal message '7fe989e19000-7fe989e20000 r-xp 00000000 fc:03 17320624                   /usr/lib64/librt-2.21.so'
Unexpected journal message '7fe989e20000-7fe98a01f000 ---p 00007000 fc:03 17320624                   /usr/lib64/librt-2.21.so'
Unexpected journal message '7fe98a01f000-7fe98a020000 r--p 00006000 fc:03 17320624                   /usr/lib64/librt-2.21.so'
Unexpected journal message '7fe98a020000-7fe98a021000 rw-p 00007000 fc:03 17320624                   /usr/lib64/librt-2.21.so'
Unexpected journal message '7fe98a021000-7fe98a128000 r-xp 00000000 fc:03 17320594                   /usr/lib64/libm-2.21.so'
Unexpected journal message '7fe98a128000-7fe98a327000 ---p 00107000 fc:03 17320594                   /usr/lib64/libm-2.21.so'
Unexpected journal message '7fe98a327000-7fe98a328000 r--p 00106000 fc:03 17320594                   /usr/lib64/libm-2.21.so'
Unexpected journal message '7fe98a328000-7fe98a329000 rw-p 00107000 fc:03 17320594                   /usr/lib64/libm-2.21.so'
Unexpected journal message '7fe98a329000-7fe98a32d000 r-xp 00000000 fc:03 16900166                   /usr/lib64/libcap.so.2.24'
Unexpected journal message '7fe98a32d000-7fe98a52c000 ---p 00004000 fc:03 16900166                   /usr/lib64/libcap.so.2.24'
Unexpected journal message '7fe98a52c000-7fe98a52d000 r--p 00003000 fc:03 16900166                   /usr/lib64/libcap.so.2.24'
Unexpected journal message '7fe98a52d000-7fe98a52e000 rw-p 00004000 fc:03 16900166                   /usr/lib64/libcap.so.2.24'
Unexpected journal message '7fe98a52e000-7fe98a545000 r-xp 00000000 fc:03 17320621                   /usr/lib64/libresolv-2.21.so'
Unexpected journal message '7fe98a545000-7fe98a745000 ---p 00017000 fc:03 17320621                   /usr/lib64/libresolv-2.21.so'
Unexpected journal message '7fe98a745000-7fe98a746000 r--p 00017000 fc:03 17320621                   /usr/lib64/libresolv-2.21.so'
Unexpected journal message '7fe98a746000-7fe98a747000 rw-p 00018000 fc:03 17320621                   /usr/lib64/libresolv-2.21.so'
Unexpected journal message '7fe98a747000-7fe98a749000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98a749000-7fe98a76a000 r-xp 00000000 fc:03 16829057                   /usr/lib64/libselinux.so.1'
Unexpected journal message '7fe98a76a000-7fe98a969000 ---p 00021000 fc:03 16829057                   /usr/lib64/libselinux.so.1'
Unexpected journal message '7fe98a969000-7fe98a96a000 r--p 00020000 fc:03 16829057                   /usr/lib64/libselinux.so.1'
Unexpected journal message '7fe98a96a000-7fe98a96b000 rw-p 00021000 fc:03 16829057                   /usr/lib64/libselinux.so.1'
Unexpected journal message '7fe98a96b000-7fe98a96d000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98a96d000-7fe98a982000 r-xp 00000000 fc:03 16899835                   /usr/lib64/libz.so.1.2.8'
Unexpected journal message '7fe98a982000-7fe98ab81000 ---p 00015000 fc:03 16899835                   /usr/lib64/libz.so.1.2.8'
Unexpected journal message '7fe98ab81000-7fe98ab82000 r--p 00014000 fc:03 16899835                   /usr/lib64/libz.so.1.2.8'
Unexpected journal message '7fe98ab82000-7fe98ab83000 rw-p 00015000 fc:03 16899835                   /usr/lib64/libz.so.1.2.8'
Unexpected journal message '7fe98ab83000-7fe98ab86000 r-xp 00000000 fc:03 17320592                   /usr/lib64/libdl-2.21.so'
Unexpected journal message '7fe98ab86000-7fe98ad85000 ---p 00003000 fc:03 17320592                   /usr/lib64/libdl-2.21.so'
Unexpected journal message '7fe98ad85000-7fe98ad86000 r--p 00002000 fc:03 17320592                   /usr/lib64/libdl-2.21.so'
Unexpected journal message '7fe98ad86000-7fe98ad87000 rw-p 00003000 fc:03 17320592                   /usr/lib64/libdl-2.21.so'
Unexpected journal message '7fe98ad87000-7fe98ad8a000 r-xp 00000000 fc:03 16902340                   /usr/lib64/libgmodule-2.0.so.0.4400.1'
Unexpected journal message '7fe98ad8a000-7fe98af89000 ---p 00003000 fc:03 16902340                   /usr/lib64/libgmodule-2.0.so.0.4400.1'
Unexpected journal message '7fe98af89000-7fe98af8a000 r--p 00002000 fc:03 16902340                   /usr/lib64/libgmodule-2.0.so.0.4400.1'
Unexpected journal message '7fe98af8a000-7fe98af8b000 rw-p 00003000 fc:03 16902340                   /usr/lib64/libgmodule-2.0.so.0.4400.1'
Unexpected journal message '7fe98af8b000-7fe98af92000 r-xp 00000000 fc:03 16900199                   /usr/lib64/libffi.so.6.0.2'
Unexpected journal message '7fe98af92000-7fe98b191000 ---p 00007000 fc:03 16900199                   /usr/lib64/libffi.so.6.0.2'
Unexpected journal message '7fe98b191000-7fe98b192000 r--p 00006000 fc:03 16900199                   /usr/lib64/libffi.so.6.0.2'
Unexpected journal message '7fe98b192000-7fe98b193000 rw-p 00007000 fc:03 16900199                   /usr/lib64/libffi.so.6.0.2'
Unexpected journal message '7fe98b193000-7fe98b34a000 r-xp 00000000 fc:03 17320585                   /usr/lib64/libc-2.21.so'
Unexpected journal message '7fe98b34a000-7fe98b549000 ---p 001b7000 fc:03 17320585                   /usr/lib64/libc-2.21.so'
Unexpected journal message '7fe98b549000-7fe98b54d000 r--p 001b6000 fc:03 17320585                   /usr/lib64/libc-2.21.so'
Unexpected journal message '7fe98b54d000-7fe98b54f000 rw-p 001ba000 fc:03 17320585                   /usr/lib64/libc-2.21.so'
Unexpected journal message '7fe98b54f000-7fe98b553000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98b553000-7fe98b56a000 r-xp 00000000 fc:03 17320619                   /usr/lib64/libpthread-2.21.so'
Unexpected journal message '7fe98b56a000-7fe98b769000 ---p 00017000 fc:03 17320619                   /usr/lib64/libpthread-2.21.so'
Unexpected journal message '7fe98b769000-7fe98b76a000 r--p 00016000 fc:03 17320619                   /usr/lib64/libpthread-2.21.so'
Unexpected journal message '7fe98b76a000-7fe98b76b000 rw-p 00017000 fc:03 17320619                   /usr/lib64/libpthread-2.21.so'
Unexpected journal message '7fe98b76b000-7fe98b76f000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98b76f000-7fe98b78a000 r-xp 00000000 fc:03 17090974                   /usr/lib64/libpolkit-gobject-1.so.0.0.0'
Unexpected journal message '7fe98b78a000-7fe98b98a000 ---p 0001b000 fc:03 17090974                   /usr/lib64/libpolkit-gobject-1.so.0.0.0'
Unexpected journal message '7fe98b98a000-7fe98b98b000 r--p 0001b000 fc:03 17090974                   /usr/lib64/libpolkit-gobject-1.so.0.0.0'
Unexpected journal message '7fe98b98b000-7fe98b98c000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98b98c000-7fe98b995000 r-xp 00000000 fc:03 17090916                   /usr/lib64/libpolkit-agent-1.so.0.0.0'
Unexpected journal message '7fe98b995000-7fe98bb95000 ---p 00009000 fc:03 17090916                   /usr/lib64/libpolkit-agent-1.so.0.0.0'
Unexpected journal message '7fe98bb95000-7fe98bb96000 r--p 00009000 fc:03 17090916                   /usr/lib64/libpolkit-agent-1.so.0.0.0'
Unexpected journal message '7fe98bb96000-7fe98bb97000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98bb97000-7fe98bb99000 r-xp 00000000 fc:03 17320630                   /usr/lib64/libutil-2.21.so'
Unexpected journal message '7fe98bb99000-7fe98bd98000 ---p 00002000 fc:03 17320630                   /usr/lib64/libutil-2.21.so'
Unexpected journal message '7fe98bd98000-7fe98bd99000 r--p 00001000 fc:03 17320630                   /usr/lib64/libutil-2.21.so'
Unexpected journal message '7fe98bd99000-7fe98bd9a000 rw-p 00002000 fc:03 17320630                   /usr/lib64/libutil-2.21.so'
Unexpected journal message '7fe98bd9a000-7fe98bdbe000 r-xp 00000000 fc:03 19170067                   /usr/lib64/libjson-glib-1.0.so.0.0.4'
Unexpected journal message '7fe98bdbe000-7fe98bfbd000 ---p 00024000 fc:03 19170067                   /usr/lib64/libjson-glib-1.0.so.0.0.4'
Unexpected journal message '7fe98bfbd000-7fe98bfbe000 r--p 00023000 fc:03 19170067                   /usr/lib64/libjson-glib-1.0.so.0.0.4'
Unexpected journal message '7fe98bfbe000-7fe98bfbf000 rw-p 00024000 fc:03 19170067                   /usr/lib64/libjson-glib-1.0.so.0.0.4'
Unexpected journal message '7fe98bfbf000-7fe98c0f6000 r-xp 00000000 fc:03 16902338                   /usr/lib64/libglib-2.0.so.0.4400.1'
Unexpected journal message '7fe98c0f6000-7fe98c2f6000 ---p 00137000 fc:03 16902338                   /usr/lib64/libglib-2.0.so.0.4400.1'
Unexpected journal message '7fe98c2f6000-7fe98c2f7000 r--p 00137000 fc:03 16902338                   /usr/lib64/libglib-2.0.so.0.4400.1'
Unexpected journal message '7fe98c2f7000-7fe98c2f8000 rw-p 00138000 fc:03 16902338                   /usr/lib64/libglib-2.0.so.0.4400.1'
Unexpected journal message '7fe98c2f8000-7fe98c2f9000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98c2f9000-7fe98c349000 r-xp 00000000 fc:03 16902342                   /usr/lib64/libgobject-2.0.so.0.4400.1'
Unexpected journal message '7fe98c349000-7fe98c549000 ---p 00050000 fc:03 16902342                   /usr/lib64/libgobject-2.0.so.0.4400.1'
Unexpected journal message '7fe98c549000-7fe98c54a000 r--p 00050000 fc:03 16902342                   /usr/lib64/libgobject-2.0.so.0.4400.1'
Unexpected journal message '7fe98c54a000-7fe98c54b000 rw-p 00051000 fc:03 16902342                   /usr/lib64/libgobject-2.0.so.0.4400.1'
Unexpected journal message '7fe98c54b000-7fe98c6c1000 r-xp 00000000 fc:03 16902336                   /usr/lib64/libgio-2.0.so.0.4400.1'
Unexpected journal message '7fe98c6c1000-7fe98c8c1000 ---p 00176000 fc:03 16902336                   /usr/lib64/libgio-2.0.so.0.4400.1'
Unexpected journal message '7fe98c8c1000-7fe98c8c5000 r--p 00176000 fc:03 16902336                   /usr/lib64/libgio-2.0.so.0.4400.1'
Unexpected journal message '7fe98c8c5000-7fe98c8c7000 rw-p 0017a000 fc:03 16902336                   /usr/lib64/libgio-2.0.so.0.4400.1'
Unexpected journal message '7fe98c8c7000-7fe98c8c9000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98c8c9000-7fe98c8ea000 r-xp 00000000 fc:03 17320577                   /usr/lib64/ld-2.21.so'
Unexpected journal message '7fe98caa6000-7fe98cab4000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98cab4000-7fe98cadc000 r-xp 00000000 fc:03 16902175                   /usr/lib64/libsystemd.so.0.6.0'
Unexpected journal message '7fe98cadc000-7fe98cadd000 ---p 00028000 fc:03 16902175                   /usr/lib64/libsystemd.so.0.6.0'
Unexpected journal message '7fe98cadd000-7fe98cade000 r--p 00028000 fc:03 16902175                   /usr/lib64/libsystemd.so.0.6.0'
Unexpected journal message '7fe98cade000-7fe98cadf000 rw-p 00029000 fc:03 16902175                   /usr/lib64/libsystemd.so.0.6.0'
Unexpected journal message '7fe98cadf000-7fe98cae1000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98cae1000-7fe98cae8000 r--s 00000000 fc:03 17329337                   /usr/lib64/gconv/gconv-modules.cache'
Unexpected journal message '7fe98cae8000-7fe98cae9000 rw-p 00000000 00:00 0'
Unexpected journal message '7fe98cae9000-7fe98caea000 r--p 00020000 fc:03 17320577                   /usr/lib64/ld-2.21.so'
Unexpected journal message '7fe98caea000-7fe98caeb000 rw-p 00021000 fc:03 17320577                   /usr/lib64/ld-2.21.so'
Unexpected journal message '7fe98caeb000-7fe98caec000 rw-p 00000000 00:00 0'
Unexpected journal message '7ffc3f3cc000-7ffc3f3ed000 rw-p 00000000 00:00 0                          [stack]'
Unexpected journal message '7ffc3f3f7000-7ffc3f3f9000 r--p 00000000 00:00 0                          [vvar]'
Unexpected journal message '7ffc3f3f9000-7ffc3f3fb000 r-xp 00000000 00:00 0                          [vdso]'
Unexpected journal message 'ffffffffff600000-ffffffffff601000 r-xp 00000000 00:00 0                  [vsyscall]'
Journal database copied to check-multi-machine-key-testBasic-10.111.111.14-FAIL.journal
Journal database copied to check-multi-machine-key-testBasic-10.111.111.17-FAIL.journal
> transport closed, dropped message:  
{"problem":"cancelled","command":"close","channel":"1:1!18"}
> transport closed, dropped message:  
{"command":"close","channel":"1:1!21"}
> transport closed, dropped message:  
{"command":"close","channel":"1:1!22"}
> transport closed, dropped message:  
{"command":"close","channel":"1:1!26"}
> transport closed, dropped message:  
{"command":"kill","group":"localhost/shell/shell"}
> problem retrieving real time metrics data: authentication-failed
> problem retrieving real time metrics data: authentication-failed
> problem retrieving real time metrics data: authentication-failed
> problem retrieving real time metrics data: authentication-failed
> transport closed, dropped message:  
{"command":"kill","group":"10.111.111.17/shell/shell"}
> transport closed, dropped message:  
{"command":"kill","group":"localhost/shell/shell"}
Warning: Permanently added '10.111.111.14' (ECDSA) to the list of known hosts.
Warning: Permanently added '10.111.111.17' (ECDSA) to the list of known hosts.
E
======================================================================
ERROR: testBasic (__main__.TestMultiMachineKeyAuth)
----------------------------------------------------------------------
Traceback (most recent call last):
  File "./check-multi-machine-key", line 102, in tearDown
    self.check_journal_messages(self.machine2)
  File "/home/hubbot/hubbot/7222b64cd4f6ed7c253c8ecf8dd1aee509eaf9ed_f22_x86-64/test/testlib.py", line 557, in check_journal_messages
    raise Error("There were unexpected journal messages")
Error: There were unexpected journal messages

----------------------------------------------------------------------
Ran 1 test in 72.750s
```

http://files.cockpit-project.org/hubbot/7222b64cd4f6ed7c253c8ecf8dd1aee509eaf9ed_f22_x86-64/hubbot.html

Another instance, different negative refs number `BATCH 21869 (refs -254661376)`

http://files.cockpit-project.org/hubbot/423960e4906aee811cebc8e3e9b37a2f9a5e828e_f22_x86-64/hubbot.html

Another instance, different negative refs number `BATCH 22072 (refs -139202368)`

http://files.cockpit-project.org/hubbot/e33bd2dedb53a5c69108ab81078449795d0c29b3_f22_x86-64.1/hubbot.html

Possible fix in abe53bf3f0dd68c16dada7b414283f724878d97f
