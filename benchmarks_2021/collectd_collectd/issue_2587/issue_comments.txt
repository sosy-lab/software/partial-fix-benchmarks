tail plugin: wrong metric name reported to graphite for latency distribution
@PhantomPhreak , thanks for reporting this!

The cause of this issue is here:

https://github.com/collectd/collectd/commit/f46d76e976ed76d2d8fa297918ecd308680d7ae9#diff-61f9572676242d09a9165f3c0e9749be
https://github.com/collectd/collectd/pull/2535
https://github.com/collectd/collectd/issues/2423

@octo, please care about this regression.
@rubenk , can you please fix 'snprintf' format so both GCC 7 will be happy and backward compatibility will be retained?

We need to revert change of `src/utils_tail_match.c` by f46d76e976ed76d2d8fa297918ecd308680d7ae9 commit and do the fix in a different way. Can you please do this?
@rpv-tomsk I'm sorry, but I don't understand what you want me to do?
Ruben, have you a GCC 7 to check code changes?

Commit f46d76e976ed76d2d8fa297918ecd308680d7ae9  has change:
```
-      snprintf(vl.type_instance, sizeof(vl.type_instance), "%s-%.0f",
+      snprintf(vl.type_instance, sizeof(vl.type_instance), "%.117s-%.2f",
```

That changes format of type instance to fix 'gcc 7 format-truncation errors'.
Unfortunately that changes format of a number, now it adds two digits after the radix character.
Example of  format change result:
```
# perl -e 'print sprintf("%.0f\n", 2);'
2
# perl -e 'print sprintf("%.2f\n", 2);'
2.00
```

Can you please fix this change so that will output integer value (as it was before the change) _and_ also with GCC 7.0 complains fixed? 

Maybe that will be enough to change "%.2f" to "%.0f", but I'm unsure and unable to verify.

Thanks.


We have a Fedora 26 builder now which has GCC 7 btw.

I just checked, and the change that you propose doesn't generate a truncation warning. It was the `%s`, not the `%.0f` that generated it.
Ok, will do PR.
Thanks!
@PhantomPhreak 
Hi, Pavel. 

Can you please confirm what #2611 fixes the issue?
@rpv-tomsk how can i check it? I checked collectd [v5.8.0.94.g204b551-1~jessie](https://pkg.ci.collectd.org/deb/dists/jessie/master/binary-amd64/), and it's still have this issue.
Unfortunately, there is a failure in new packages build.
I will try to inform you when build succeedes.
Hope, this issue was solved. If not, please inform us.

Thanks.