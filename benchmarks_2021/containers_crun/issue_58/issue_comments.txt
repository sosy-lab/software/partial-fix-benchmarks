Add container state to crun list?
runc list
```
ID          PID         STATUS      BUNDLE      CREATED     OWNER
```

I've opened a PR here: https://github.com/giuseppe/crun/pull/61

Does it work fine for you?
Looks like there is a single whitespace before container state?
```
/ # crun list
NAME PID       STATUS   BUNDLE PATH                            
rngd 993        running /containers/services/rngd  
```

Stopped / killed container shouldn't have the (old) PID?
```
/ # crun list
NAME PID       STATUS   BUNDLE PATH                            
rngd 993        stopped /containers/services/rngd 
```

runc changes it to PID 0 if no process is running.
```
/ # runc list
ID             PID         STATUS      BUNDLE                            CREATED                          OWNER
rngd           0           stopped     /containers/services/rngd         2019-07-26T13:06:22.596123385Z   #0
```
Thanks for the eagle eye check :smile:

I've pushed a new version, does it look better?
PID and whitespace is fixed 👍 
if you are fine with the change as it is, I will merge it
Yes, looks good. Tested with two containers (running, stopped).