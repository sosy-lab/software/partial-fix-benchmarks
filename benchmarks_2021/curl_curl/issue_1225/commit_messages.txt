VC: remove the makefile.vc6 build infra

The winbuild/ build files is now the single MSVC makefile build choice.

Closes #1215
telnet: fix windows compiler warnings

Thumbs-up-by: Jay Satiro

Closes #1225
telnet: fix windows compiler warnings

Thumbs-up-by: Jay Satiro

Closes #1225
telnet: (win32) fix read callback return variable

telnet.c(1427,21): warning: comparison of constant 268435456 with
expression of type 'CURLcode' is always false

telnet.c(1433,21): warning: comparison of constant 268435457 with
expression of type 'CURLcode' is always false

Reported-by: Gisle Vanem
Bug: https://github.com/curl/curl/issues/1225#issuecomment-290340890
telnet: (win32) fix read callback return variable

telnet.c(1427,21): warning: comparison of constant 268435456 with
expression of type 'CURLcode' is always false

telnet.c(1433,21): warning: comparison of constant 268435457 with
expression of type 'CURLcode' is always false

Reviewed-by: Jay Satiro
Reported-by: Gisle Vanem
Bug: https://github.com/curl/curl/issues/1225#issuecomment-290340890

Closes #1374
