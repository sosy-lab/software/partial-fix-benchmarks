brotli: data at the end of content can be lost

Decoding loop implementation did not concern the case when all
received data is consumed by Brotli decoder and the size of decoded
data internally hold by Brotli decoder is greater than CURL_MAX_WRITE_SIZE.
For content with unencoded length greater than CURL_MAX_WRITE_SIZE this
can result in the loss of data at the end of content.

Closes #2194
progress.c: Fixed download/Upload speed is not accurate.
Closes https://github.com/curl/curl/issues/2200
progress.c: Fixed download/Upload speed is not accurate.
Closes https://github.com/curl/curl/issues/2200
progress: calculate transfer speed on milliseconds if possible

to increase accuracy for quick transfers

Bug #2200
progress: calculate transfer speed on milliseconds if possible

to increase accuracy for quick transfers

Fixes #2200
progress: calculate transfer speed on milliseconds if possible

to increase accuracy for quick transfers

Fixes #2200
Closes #2206
