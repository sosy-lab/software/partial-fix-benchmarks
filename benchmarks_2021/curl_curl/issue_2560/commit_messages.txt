contributors.sh: use "on github", not at
http2: use the correct function pointer typedef

Fixes gcc-8 picky compiler warnings
Reported-by: Rikard Falkeborn
Bug: #2560
http2: use the correct function pointer typedef

Fixes gcc-8 picky compiler warnings
Reported-by: Rikard Falkeborn
Bug: #2560
gcc: disable picky gcc-8 function pointer warnings in two places

Reported-by: Rikard Falkeborn
Bug: #2560
http2: use the correct function pointer typedef

Fixes gcc-8 picky compiler warnings
Reported-by: Rikard Falkeborn
Bug: #2560
Closes #2568
gcc: disable picky gcc-8 function pointer warnings in two places

Reported-by: Rikard Falkeborn
Bug: #2560
gcc: disable picky gcc-8 function pointer warnings in two places

Reported-by: Rikard Falkeborn
Bug: #2560
Closes #2569
