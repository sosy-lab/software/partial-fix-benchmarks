travis: remove curl before a normal build

on Linux. To make sure the test suite runs with its newly build tool and
doesn't require an external one present.

Bug: #3198
Closes #3200
url: a short host name + port is not a scheme

The function identifying a leading "scheme" part of the URL considered a few
letters ending with a colon to be a scheme, making something like "short:80"
to become an unknown scheme instead of a short host name and a port number.

Extended test 1560 to verify.

Reported-by: Hagai Auro
Fixes #3220
url: a short host name + port is not a scheme

The function identifying a leading "scheme" part of the URL considered a few
letters ending with a colon to be a scheme, making something like "short:80"
to become an unknown scheme instead of a short host name and a port number.

Extended test 1560 to verify.

Reported-by: Hagai Auro
Fixes #3220
url: a short host name + port is not a scheme

The function identifying a leading "scheme" part of the URL considered a few
letters ending with a colon to be a scheme, making something like "short:80"
to become an unknown scheme instead of a short host name and a port number.

Extended test 1560 to verify.

Reported-by: Hagai Auro
Fixes #3220
Closes #3223
url: a short host name + port is not a scheme

The function identifying a leading "scheme" part of the URL considered a few
letters ending with a colon to be a scheme, making something like "short:80"
to become an unknown scheme instead of a short host name and a port number.

Extended test 1560 to verify.

Also fix test203 to use posix_pwd to make it get the correct path on windows

Reported-by: Hagai Auro
Fixes #3220
Fixes #3233
Closes #3223
Closes #3235
url: a short host name + port is not a scheme

The function identifying a leading "scheme" part of the URL considered a few
letters ending with a colon to be a scheme, making something like "short:80"
to become an unknown scheme instead of a short host name and a port number.

Extended test 1560 to verify.

Also fix test203 to use posix_pwd to make it get the correct path on windows

Reported-by: Hagai Auro
Fixes #3220
Fixes #3233
Closes #3223
Closes #3235
url: a short host name + port is not a scheme

The function identifying a leading "scheme" part of the URL considered a
few letters ending with a colon to be a scheme, making something like
"short:80" to become an unknown scheme instead of a short host name and
a port number.

Extended test 1560 to verify.

Also fixed test203 to use file_pwd to make it get the correct path on
windows. Removed test 2070 since it was a duplicate of 203.

Assisted-by: Marcel Raad
Reported-by: Hagai Auro
Fixes #3220
Fixes #3233
Closes #3223
Closes #3235
