unexpected bad/illegal url format error. A short host + port is treated as a scheme!
And curiously a `curl boing` will try to resolve `boing` with  recursing over a dot less name.
Ack, a regression from  46e1640. PR coming up.
commit d9abebc7ee2 reverted the fix temporarily since it broke tests on windows. #3235 should ideally fix it "proper".