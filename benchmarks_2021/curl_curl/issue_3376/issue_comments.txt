mbedTLS backend should use CURLOPT_SSL_VERIFYHOST to control CN checking
@rosenqui can you check if my fix works for you?
I should have time to try it out later today.

Thanks for the quick fix!
Fix looks good - I commented in #3382 
This looks to be the simplest solution - same code as before with a check of `verifyhost` first.
```
  ret = mbedtls_ssl_get_verify_result(&BACKEND->ssl);

  if(!SSL_CONN_CONFIG(verifyhost))
    ret &= ~MBEDTLS_X509_BADCERT_CN_MISMATCH;   /* Ignore hostname errors if verifyhost is disabled */
  if(ret && SSL_CONN_CONFIG(verifypeer)) {
    if(ret & MBEDTLS_X509_BADCERT_EXPIRED)
      failf(data, "Cert verify failed: BADCERT_EXPIRED");

    if(ret & MBEDTLS_X509_BADCERT_REVOKED) {
      failf(data, "Cert verify failed: BADCERT_REVOKED");
      return CURLE_SSL_CACERT;
    }

    if (ret & MBEDTLS_X509_BADCERT_CN_MISMATCH)
      failf(data, "Cert verify failed: BADCERT_CN_MISMATCH");

    if(ret & MBEDTLS_X509_BADCERT_NOT_TRUSTED)
      failf(data, "Cert verify failed: BADCERT_NOT_TRUSTED");

    return CURLE_PEER_FAILED_VERIFICATION;
  }

  peercert = mbedtls_ssl_get_peer_cert(&BACKEND->ssl);
```

Turn off the `MBEDTLS_X509_BADCERT_CN_MISMATCH` error bit if `verifyhost` is disabled, but otherwise do the same as before. We don't want to fail if `verifyhost` is on and `verifypeer` is off since the latter is the master kill switch for validation.
Thanks, like #3390 ?