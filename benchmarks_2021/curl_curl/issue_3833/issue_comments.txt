os400: Definition not found for symbol 'Curl_vsetopt'
Thanks, yes that seems to be accurate. How about changing that function call to something this:

~~~diff
--- a/packages/OS400/ccsidcurl.c
+++ b/packages/OS400/ccsidcurl.c
@@ -1303,13 +1303,16 @@ curl_easy_setopt_ccsid(CURL * curl, CURLoption tag, ...)
     data->set.str[STRING_COPYPOSTFIELDS] = s;   /* Give to library. */
     break;
 
   case CURLOPT_ERRORBUFFER:                     /* This is an output buffer. */
   default:
-    result = Curl_vsetopt(data, tag, arg);
+  {
+    long val = va_arg(arg, long);
+    result = curl_easy_setopt(curl, tag, val);
     break;
-    }
+  }
+  }
 
   va_end(arg);
   return result;
 }

Thanks for the quick response, yes, your proposed change builds fine and I now have a clean 7.64.1 build.
Thanks for confirming!
This fix may build, but it surely won't run properly: while longs are 32-bits, pointers are 128-bits !
In addition, alignment is important and pointers must be handled as such or else they would lose their addressing capabilities.
I already faced this (https://github.com/curl/curl/commit/3b548ffde9f0ea85dd320ae6af23a2e3fdbb6d29) and use of `Curl_vsetopt`() is the easiest way to avoid locally dispatching on tag argument `CURLOPTTYPE_*` to determine the proper type and proper calling sequence.
I suppose one way to fix this is to pass a pointer to curl_easy_setopt using va_arg(arg, char *) instead of the long value. curl_easy_setopt_ccsid() should ensure that callers are specifying a suitable tag with additional switch statements, the default case should return CURLE_UNKNOWN_OPTION.

I will try this approach against 7.65.1.
There are 2 ways to fix it:

1. Reinstate `Curl_vsetopt()` as it was before. This is the easiest, more efficient and elegant change. Since Daniel already made static this function twice, I was expecting a comment to go/nogo this way.
2. Dispatch on the `CURLOPTTYPE_*` part of the option, branching to an appropriate call to a`curl_easy_setopt()` call for the given parameter type. Feasible but has no advantage on the previous solution.
> Reinstate Curl_vsetopt() as it was before

Reverting the lib/setopt.c part of 05b100aee should probably be enough. I wasn't aware of this use of the function when I made it static - and I would propose that we add a comment about it this time so that we can avoid doing this exact same dance again in a future! :smile: 
> Reinstate Curl_vsetopt() as it was before

I can do it... with a comment  :-)