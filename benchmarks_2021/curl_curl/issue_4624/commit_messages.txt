include: make CURLE_HTTP3 use a new error code

To avoid potential issues with error code reuse.

Reported-by: Christoph M. Becker
Assisted-by: Dan Fandrich
Fixes #4601
Closes #4627
openssl: Revert to less sensitivity for SYSCALL errors

- Disable the extra sensitivity except in debug builds (--enable-debug).

- Improve SYSCALL error message logic in ossl_send and ossl_recv so that
  "No error" / "Success" socket error text isn't shown on SYSCALL error.

Prior to this change 0ab38f5 (precedes 7.67.0) increased the sensitivity
of OpenSSL's SSL_ERROR_SYSCALL error so that abrupt server closures were
also considered errors. For example, a server that does not send a known
protocol termination point (eg HTTP content length or chunked encoding)
_and_ does not send a TLS termination point (close_notify alert) would
cause an error if it closed the connection.

To be clear that behavior made it into release build 7.67.0
unintentionally. So far there is just one user report due to it.

Ultimately the idea is a good one, and other SSL backends may already
behave similarly (such as Windows native OS SSL Schannel). However much
more of our user base is using OpenSSL and there is a mass of legacy
users in that space, so I think that behavior should be partially
reverted and then rolled out slowly.

This commit changes the behavior so that the increased sensitivity is
disabled in all curl builds except curl debug builds (DEBUGBUILD). If
after a period of time there are no major issues then it can be enabled
in dev and release builds with the newest OpenSSL (1.1.1+).

Bug: https://github.com/curl/curl/issues/4409#issuecomment-555955794
Reported-by: Bjoern Franke

Fixes https://github.com/curl/curl/issues/4624
Closes https://github.com/curl/curl/pull/4623
openssl: Revert to less sensitivity for SYSCALL errors

- Disable the extra sensitivity except in debug builds (--enable-debug).

- Improve SYSCALL error message logic in ossl_send and ossl_recv so that
  "No error" / "Success" socket error text isn't shown on SYSCALL error.

Prior to this change 0ab38f5 (precedes 7.67.0) increased the sensitivity
of OpenSSL's SSL_ERROR_SYSCALL error so that abrupt server closures were
also considered errors. For example, a server that does not send a known
protocol termination point (eg HTTP content length or chunked encoding)
_and_ does not send a TLS termination point (close_notify alert) would
cause an error if it closed the connection.

To be clear that behavior made it into release build 7.67.0
unintentionally. Several users have reported it as an issue.

Ultimately the idea is a good one, since it can help prevent against a
truncation attack. Other SSL backends may already behave similarly (such
as Windows native OS SSL Schannel). However much more of our user base
is using OpenSSL and there is a mass of legacy users in that space, so I
think that behavior should be partially reverted and then rolled out
slowly.

This commit changes the behavior so that the increased sensitivity is
disabled in all curl builds except curl debug builds (DEBUGBUILD). If
after a period of time there are no major issues then it can be enabled
in dev and release builds with the newest OpenSSL (1.1.1+), since users
using the newest OpenSSL are the least likely to have legacy problems.

Bug: https://github.com/curl/curl/issues/4409#issuecomment-555955794
Reported-by: Bjoern Franke

Fixes https://github.com/curl/curl/issues/4624
Closes https://github.com/curl/curl/pull/4623
