smbserver: fix Python version specific ConfigParser import

Follow up to ee63837 and 8c7c4a6
Fixes #5077
connect: happy eyeballs cleanup

Make sure each separate index in connn->tempaddr[] is used for a fixed
family (and only that family) during the connection process.

Fixes #5083
Fixes #4954
connect: happy eyeballs cleanup

Make sure each separate index in connn->tempaddr[] is used for a fixed
family (and only that family) during the connection process.

Fixes #5083
Fixes #4954
connect: happy eyeballs cleanup

Make sure each separate index in connn->tempaddr[] is used for a fixed
family (and only that family) during the connection process.

If family one takes a long time and family two fails immediately, the
previous logic could misbehave and retry the same family two address
repeatedly.

Reported-by: Paul Vixie
Reported-by: Jay Satiro
Fixes #5083
Fixes #4954
Closes #5089
