CMake: fix test for warning suppressions

GCC doesn't warn for unknown `-Wno-` options, except if there are other
warnings or errors [0]. This was problematic with `CURL_WERROR` as that
warning-as-error cannot be suppressed. Notably, this always happened
with `-Wno-pedantic-ms-format` when not targeting Windows. So test for
the positive form of the warning instead, which should always result in
a diagnostic if unknown.

[0] https://gcc.gnu.org/onlinedocs/gcc/Warning-Options.html

Closes https://github.com/curl/curl/pull/5714
WIN32: stop forcing narrow-character API

Except where the results are only used for character output or with
hardcoded string literals.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes
WIN32: stop forcing narrow-character API

Except where the results are only used for character output or with
hardcoded string literals.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes
WIN32: stop forcing narrow-character API

Except where the results are only used for character output.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes
WIN32: stop forcing narrow-character API

Except where the results are only used for character output.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes https://github.com/curl/curl/pull/5718
WIN32: stop forcing narrow-character API

Except where the results are only used for character output.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes https://github.com/curl/curl/pull/5718
WIN32: stop forcing narrow-character API

Except where the results are only used for character output.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes https://github.com/curl/curl/pull/5718
WIN32: stop forcing narrow-character API

Except where the results are only used for character output.
getenv is not touched because it's part of the public API, and having
it return UTF-8 instead of ANSI would be a breaking change.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes https://github.com/curl/curl/pull/5718
WIN32: stop forcing narrow-character API

Except where the results are only used for character output.
getenv is not touched because it's part of the public API, and having
it return UTF-8 instead of ANSI would be a breaking change.

Fixes https://github.com/curl/curl/issues/5658
Fixes https://github.com/curl/curl/issues/5712
Closes https://github.com/curl/curl/pull/5718
