FTP download fails using libcurl when TCP_FASTOPEN enabled
With the current version (7.74), compiled with `-g -Og`, the same run fails earlier:
```
 curlftp --tcp-fastopen "ftp://xmlsoft.org/libxslt/libxslt-1.1.33.tar.gz"
*   Trying 91.121.203.120:21...
* Connected to xmlsoft.org () port 21 (#0)
< 220 (vsFTPd 2.2.2)
* Send failure: Operation already in progress
* Closing connection 0
curl told us 55
```
I don't see how TCP Fast Open will help for an FTP transfer so I would recommend you don't enable that, as a rather safe work-around.
I think we will disable that in our downstream library wrapper. See `Julia`: https://github.com/JuliaLang/Downloads.jl/pull/76
That was wrong, the commit only fixed #6262, this should rather be fixed by #6265 ...