"schannel: failed to retrieve ALPN result" when running on Windows 7
I figure this means we shouldn't try to use ALPN on anything before 8.1?

Have you tried to add a check that can switch off `conn->bits.tls_enable_alpn` if the windows version isn't recent enough?

[According to MS](https://technet.microsoft.com/en-us/library/hh831771%28v=ws.11%29.aspx), ALPN has been introduced with Windows 8.1. It is not available in Windows 7, so it should be disabled on these systems.

We have not tried to do this depending on the Windows version. What we're trying right now is to set `CURLOPT_SSL_ENABLE_ALPN` to 0. We'll report the outcome.

> we're trying right now is to set `CURLOPT_SSL_ENABLE_ALPN` to 0

Yeah, that sounds like a functional work-around. Another one would be to rebuild libcurl with a TLS backend that supports ALPN on win 7 as well, but if you can live without APLN I guess your approach is the easier one.

We should still get a fix for this done.

Confirm  everything above. I've got a minimal test case running that shows the following result

| Operating system | unpatched | patched |
| --- | --- | --- |
| Windows Server 2012 R2 | ok | ok |
| Windows 7 | fail | ok |

where patched means adding the following in our application code:

```
#ifdef _WIN32
    // Disable ALPN which is not supported on Windows < 8.1
    // https://msdn.microsoft.com/en-us/library/windows/desktop/aa379340%28v=vs.85%29.aspx
    curlEasy_->add<CURLOPT_SSL_ENABLE_ALPN>(0);
#endif
```

all builds are done using MSVS 2015 on Windows Server 2012 R2.

---

Given we have a nice workaround now, the question is weather or not [this compile time feature check](https://github.com/curl/curl/blob/125827e60ee98a6d42567a2418e50ab738ba2a5a/lib/vtls/schannel.c#L66) is sufficient. This causes the default behavior that code compiled on Windows 8+ does not run on Windows 7 (and below).

I'd like to make clear that we use Windows Server 2012 **R2**, which is based on Windows 8.1 and which is the oldest version that supports ALPN. Windows Server 2012 (without R2) is based on Windows 8 and does **not** support ALPN.

> Given we have a nice workaround now, the question is weather or not this compile time feature check is sufficient.

It is not. There should be a runtime check in the schannel TLS code that disables ALPN if it is running on Windows < 8.1. I could have a shot at implementing this in the next few days, but I'm not sure that it will be today or tomorrow. @bagder Do you intend to release a bug fix version when this is done?

> Do you intend to release a bug fix version when this is done?

Sure, once we have a bug fix it'll get merged and should be included in the pending next release.

My apologies for the regression here. I was fairly certain I had tested this on Windows 7 when I implemented #724, but apparently did not.
