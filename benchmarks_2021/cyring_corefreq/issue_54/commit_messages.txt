Intel chipset 955 : DIMM Geometry [UNTESTED]
Temperature sensor of family 17h [Experimental issue #54] / Remove code of issue #55
Ryzen thermal adjustment for issue #54
Stress Ryzen up to the XFR frequency (issue #54)
Change for a Ryzen Boost and XFR frequencies table (issue #54)
Issue #54 : Ryzen Power Unit, Core and Pkg Energy, Voltage Core
Issue #54 : Try MSR PStateStat as an index of PStateDef to read Processor current VID
Issue #54 : Try the energy accumulators in cumulative mode.
Issue #54 : Debug the RAPL counters.
Issue #54 : Rollback the Ryzen RAPL counters code.
RAPL: Power window time unit is scaled to the monitoring interval / AMD 17h: L3 cache size in 512 KB units (issue #54) / AMD 17h: Adjust some boosted ratios / AMD 0Fh: remove the experimental flag.
Issue #54 : fix maxCoreCount
AMD family 17h: added the Instructions Retired Performance Counter and changed for the read-only APERF, MPERF registers (issue #54)
AMD : provide a structure to manage the fixed performance counters (issue #54)
AMD SVM bit: Secure Virtual Machine (issue #54)
With AMD, rename "HTT" to "SMT" and some "Turbo" to "Boost" (issue #54)
Zen: Allow CPB toggle. Fix IOMMU Base Address. (issue #54)
Zen: Re-compute Boost ratios after toggling CPB (issue #54)
Zen: Fixed the ratios refresh after toggling CPB (issue #54)
Intel: Reverted the Ratio_Unlock state (issue #62) / Zen: Reset Boost ratios (issue #54)
Fix assembly instructions macros (issue #54)
Compute notification (issue #54) / Rename to Turbo Activation
Fix to compute ratios after toggling CPB (issue #54)
Fix Ryzen Core temperature regression (issue #54)
Ryzen: make use of RDTSCP (issue #54)
Base Clock: defaults to auto; can be switched back to reference frequency (issue #77)
InitThermal: Fix the max limit temperature (issue #54)
Frequency View: clean garbage up (issue #54)
InitThermal: Fix the minimum limit temperature (issue #54)
[Experimental][AMD] Test misc Zen SMU registers (issue #54)
[Experimental][AMD] Dump in hexa misc Zen SMU registers (issue #54)
