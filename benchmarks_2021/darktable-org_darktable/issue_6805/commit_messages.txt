Corrections to pt_BR (user manual)
fix unaligned-store SSE crash

On some systems, _mm_store_ps segfaults for addresses which are not
16-byte aligned.  When the image width is not a multiple of four, some
of the writes for the frame are unaligned, so use the slower
_mm_storeu_ps to avoid the crash.

Hopefully fixes #6805.  (Untested because _mm_store_ps does not
segfault here.)
borders: fix unaligned-load SSE crash

On some systems, unaligned SSE loads segfault.  The color to be
written to the image border is passed in an unaligned array, so use
_mm_loadu_ps instead of _mm_load_ps.  (Since we do the load exactly
once per image, the speed difference doesn't matter.)

Hopefully fixes #6805 (_mm_load_ps does not crash here).
borders: fix unaligned-load SSE crash

On some systems, unaligned SSE loads segfault.  The color to be
written to the image border is passed in an unaligned array, so use
_mm_loadu_ps instead of _mm_load_ps.  (Since we do the load exactly
once per image, the speed difference doesn't matter.)

Hopefully fixes #6805 (_mm_load_ps does not crash here).
borders: use _mm_storeu_ps as we could access non aligned memory.

Attempt to fix #6805
