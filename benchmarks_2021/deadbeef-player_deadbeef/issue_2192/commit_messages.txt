gtkui: fix handling repeat/shuffle menu item changes (fixes #2308)
streamer: move streamer_reset to after output->stop (fixes #2192)
alsa: pause/unpause device when setting format (fixes #2192)
