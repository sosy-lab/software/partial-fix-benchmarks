mp3parser: improve heuristics used to detect freeformat mp3 files (fixes #2392)
cueutil: skip unknown cuesheet fields (fixes #2389)
cueutil: fix handling linebreak when ignoring unsupported cuesheet fields (fixes #2389)
cueutil: fix handling linebreak when ignoring unsupported cuesheet fields (fixes #2389)
