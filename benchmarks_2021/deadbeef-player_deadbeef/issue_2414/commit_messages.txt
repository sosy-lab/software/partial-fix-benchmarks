Revert "vtx: do not include sys/mman.h"

This reverts commit 0f3daf54d1b273dab2c34ab790d8f4be056e0194.
gtkui: fix autoresizing columns when deadbeef starts maximized (fixes #2414)
gtkui: implemented a more robust method of detecting when gtk widget has been realized and resized ot its final size and maximization state (fixes #2414)
[pull] master from DeaDBeeF-Player:master (#4)

* cocoaui: sort items in medialib groups

* cocoaui: improve medialib right click track selection

* medialib: update modified tracks when mtime is higher than scan time

* cocoaui: fix window title formatting in pre-bigsur macOS (fixes #2433)

* fixed a few thread safety issues

* cocoaui: medialib outline view styling: make it look right in big sur beta 5

* cocoaui: fix medialib random crash on startup

* medialib: added support for multiple toplevel music folders

* fix tests

* cocoaui: sort medialib tracks accounting for disc nr

* cocoaui: added medialib configure button

* cocoaui: medialib configuration UI WIP

* cocoaui: cleanup

* cocoaui: eliminate the need to install libjansson to build medialib plugin

* compile fix

* cocoaui: medialib folder list editing

* coreaudio: prevent sleep while playback is active (fixes #2434)

* coreaudio: use kIOPMAssertionTypeNoIdleSleep instead of kIOPMAssertionTypeNoDisplaySleep, to allow display sleep while music is playing (fixes #2434)

* coreaudio: avoid sleep prevention code from being executed multiple times, just in case

* Revert "streamer: workaround for double-sending DB_EV_SONGCHANGED"

This reverts commit d63a53bf70f09bc8534f6394580221c0af1babeb.

* cocoaui: fix group titles clipping (#2436)

* cocoaui: fix crash after clicking empty space in column header, causing array write at index -1

* cocoaui: use path instead of uri when changing medialib settings

* medialib: thread safe configuration access

* cocoaui: medialib search

* cocoaui: added a comment for -[MediaLibraryOutlineView validateProposedFirstResponder:forEvent:]

* medialib: ensure playlist is not null before clearing, when medialib paths list is empty

* medialib: fix applying medialib configuration when no previous configuration existed

* medialib: fix crash caused by under-retained track references

* junklib: fix bug in shift_jis detection, which caused memory corruption

* medialib: music files in the top level music folder will now show up with folder filter

* cocoaui: remove the apply/reset buttons from medialib preferences

* cocoaui: report artist/album/title to the NowPlayingInfoCenter (fixes #2401)

* cocoaui: report artist/album/title to the NowPlayingInfoCenter (fixes #2401)

* junklib: fix bug in shift_jis detection, which caused memory corruption

* medialib: remove ddb_playlist_t access from medialib API

* cocoaui: reimplemented medialib drag-drop functionality without involving the internal medialib playlist

* medialib: new plugin API, which allows multiple sources / instances (WIP)

* medialib: new plugin type for supporting multiple media sources

* medialib: fix types / names

* deadbeef.h: media source plugin api level limit

* gtkui: move files to trash

* mp4p update

* travis: cancel windows build if make fails

* mp4: cleanup old text fields before writing new ones

* mp4: fixed writing total discs

* mp4: fix writing disc number

* added clang building support using docker

* travis: build on ubuntu bionic + clang

* enable building medialib plugin for linux

* artwork: convert to libdispatch

* artwork: moved image loading/scaling routines to separate files

* cocoaui: fix libjansson install path

* medialib: add build steps for automake/autoconf build

* medialib: add jannson flags to build

* lastfm: convert to libdispatch

* cocoaui: improved playlist focus handling

* osx: gtkui project settings fixups

* gtkui: fix assigning hotkeys with shift (fixes #2437)

* gtkui: align hotkey code changes, and make hotkeys assignments work again (fixes #2437)

* gtkui: make playlist min width 250px, to prevent drawing/scrolling problems (fixes #1385)

* gtkui: fix crash / memory leak after shrinking gtkbox (fixes #2297)

* gtkui: fix drawing playlist column separators on newer GTK themes

* notify: convert to libdispatch (fixes #1077)

* premake5-win: add medialib

* premake5: use clang

* travis: add clang for windows build

* premake/travis: dispatch windows support

* travis: add build messages for windows

* gtkui: fix pixmaps and document paths in xcode project

* travis: remove gcc from windows build

* travis: enforce clang on windows builds

* cocoaui: disable experimental/WIP features for master branch

* switch from gnu99 to c99

* xcodeconfig folder rename

* jansson xcconfig delete

* switch to clang in apgcc wrapper

* cleanup 18.04 docker build scripts

* added 14.04 clang docker build scripts

* switch travis config back to 14.04

* added clang requirement to configure.ac

* README update

* README.md update

* README.md update

* exclude medialib from build on master

* 14.04 docker test script to include libdispatch

* apbuild as submodule, ship libdispatch+deps in portable build

* submodule fix

* ffmpeg: cleanup unused enum

* tf: include band field in %artist% mapping

* aac: close the file if it is mp4 but not aac

This prevents leaking a file descriptor for each ALAC file.

* alac: re-use the same fd for mp4reader, don't open a duplicate

This prevents a FD leak for each played ALAC file.

* gtkui: fix file filter for the GtkFileChooserNative implementation

* osx: remove jansson xcconfig reference

* dumb: added some extra error recilience to s3m parser, to make certain seemingly corrupt s3m files to load and play, although it's not clear whether they're playing correctly (fixes #2463)

* cocoaui: covermanager slight cleanup and modernization

* main: Make socket path use XDG_RUNTIME_DIR if set again

This was accidentally reverted by commit 61c3bc88732ff1dd21a3e8fef706dede0d01df17
On Windows XDG_RUNTIME_DIR is unset to it will simply use the dbconfdir path.

* mp4p update: memory leak fix (fixes #2474)

* cocoaui/artwork: fix memory leaks

* cocoaui: fix logger autoscroll when the log window shows for the first time with enough initial content to cause scrolling

* gtkui: Fix warning when there is no log action

* gtkui: fix logwindow/widget scroll detection

This fixes issue where scroll changes in response
to resizing the log window. Hooking the value-changed
signal of the GtkAdjustment is better as it only
triggers when users directly scroll.

* aac: update copyright notice

* lastfm: fix a few reference counting bugs

* disable medialib API for 1.8.5

* 1.8.5-beta1 + ChangeLog

* Changelog update

* portaudio: convert to libdispatch

* portaudio: fix playback of non-existent tracks

* README.md update

* premake5: add pkgconfig_cflags

* premake5: refactor build script

* premake5: fix windows build

* premake5: disable medialib by default

* travis: add yasm for windows build

* vtx: do not include sys/mman.h

* libwin: add shn workarounds

* premake5: add plugins, many different changes

* travis: update premake invokation

* README.md update

* windows_postbuild: fix removal of *.lib files

* premake5: fix for linux

* Replace premake5.lua with premake5-win.lua

* premake5: cleanup, disable plugins for windows

* Revert "vtx: do not include sys/mman.h"

This reverts commit 0f3daf54d1b273dab2c34ab790d8f4be056e0194.

* gtkui: fix autoresizing columns when deadbeef starts maximized (fixes #2414)

* gtkui: implemented a more robust method of detecting when gtk widget has been realized and resized ot its final size and maximization state (fixes #2414)

* osx: gtk version update

* mp4tagutil: fix a bug which caused calculating negative padding size, when moov is already located at the end of file (fixes #2474)

* gme: fix weird typedefs

* updated docker scripts

* gtkui: fix playlist not drawing content on startup (fixes #2484)

* New vscode debug config for autotools builds

* Add two  generated files to .gitignore

* gtkui: add parent menu for widgets

* Make data key a constant

* gtkui: Update titlebar based on TF update interval

* Make sure to cancel timer on shutdown

* Changelog update

* deadbeef.h: removed obsolete wiki link

* streamer: fix album shuffle after last album track issue (fixes #2471)

* 1.8.5-beta2 + changelog

* streamer: fix shuffle bug repeating the same tracks when more than 2 tracks queue up (fixes #2438)

* Changelog update

* osx: delete xcode workspace, since it's not really used, only referencing a single project

* travis: use Xcode12.2

* translations update for 1.8

* docker: added --progress plain to be able to read the logs

* 1.8.5-rc1

* about box copyright year update

* 1.8.5 release

* devel version

* include libdispatch & deps in the package builds

enable package upload for devel builds

* devel version

* include libdispatch & deps in the package builds

* fix resource paths in non-portable builds (fixes #2499)

* 1.8.6-rc1

* fix resource paths in non-portable builds (fixes #2499)

* devel version

* translations update

* gtkui: fix truncating of long group titles (fixes #2436)

* gtkui: fix truncating of long group titles (fixes #2436)

* update translations

* 1.8.6 release

* pulse: error handling instead of assert

* include libdispatch & deps in the package builds

* shellexec: cleanup legacy config support, rename the config property name to not contain WIP (#829)

* aac: instant bitrate support (fixes #1561)

* aac: fix mp4 chapter functionality

* devel version

* aac: instant bitrate support (fixes #1561)

* aac: fix mp4 chapter functionality

* gtkui: delete unused old code

* delete unused commented out code

* vfs_curl: added connection timeout of 10 seconds (fixes #1094)

* cocoaui: new preferences icons

* cocoaui: fix toolbar appearance (selectable items)

* hotkeys: initialize after all plugins has loaded / connected (fixes #1150)

* hotkeys: fix the missing action_sort_by_title, fix warnings

* junklib: do not discard id3v2.3-specific frames, when they can be converted to TXXX; also convert such TXXX frames correctly when writing id3v2.4 (fixes #2304)

* docker: fix parameter typo

* gtkui: refactor delete from disk code to be reusable

* cocoaui: added delete from disk

* premake: add libdeletefromdisk

* premake: fix lib parse

* premake: don't put static libs in output build

* mp4: fix crash caused by encountering unsupporter metadata fields; fix writing the padding after moov is moved to the end of file (fix #2509)

* disable travis-ci

* mac github action

* linux github action

* windows github action

* enable uploading artifacts from github actions

* added github workflow build status badges

* updated README; added discord link

* cocoaui: fix deallocating MainWindowController; correctly capture track list for the menu actions

* cocoaui: reference cycle fixing

* cocoaui: fix cleaning up PlaylistViewController

* deletefromdisk: rename DeleteFromDiskTrackList to a more reusability-friendly UtilTrackList

* fixed typos

* better github action job names, for discord webhook

* ID3v2 frame sizes are before unsynchronisation

* junklib: fixes id3v2.3 unsynchronized tag reading support without breaking id3v2.4

* alac: handle invalid ALAC box data with samplerate=0

* alac: fix handling alac files with 48 byte ASC

* main: save resume state immediately after every song finished or playback is stopped

* alsa: move hwparams setting to the playback thread

* streamer: remove the deferred setformat hack

* alsa: setformat error handling

* pulse: handle setformat called from streamer_read on playback thread

* alsa, pulse: _setformat_requested cleanup

* osx: gtk3 xcconfig update

* gtkui: capture selected track list when the context menu opens (fixes #2432)

* osx: fix gtk icon loading

* osx: build FLAC without DEBUG=1, to prevent asserts in debug builds

* wma: double-free crash fix for wma+cue

* cocoaui: fix crash when pinned group is not updated after groups are rebuilt

* osx: reordered targets alphabetically

* adplug: fix or suppress warnings when building in Xcode

* m3u: fix warnings and add tests for m3u loader

* osx: shut up wildmidi warnings

* osx: fix mp3/mpg123/libmad warnings

* osx: deps update (libzip warnings)

* osx: shutup ogg/vorbis/opus warnings

* osx: ebur128 warnings fix

* osx: shut up most of the sc68 warnings

* osx: separate libwavpack into separate target, and fix warnings

* osx: shut up libsamplerate warnings

* osx: shut up psf warnings

* osx: update gtk3 xcconfig

* osx: remove libmp4ff

* osx: shut up gme warnings

* osx: shut up jansson warnings

* osx: shut up wma warnings

* osx: fix a few warnings, don't set deployment target explicitly

* osx: shut up vtx warnings

* osx: shut up libvorbis warnings

* osx: shut up tta warnings

* osx: shut up libsndfile warnings

* osx: shut up libsidplay2 warnings

* osx: sid warnings fix

* osx: libsamplerate compile fix

* osx: fix deployment target for tests

Co-authored-by: Alexey Yakovenko <wakeroid@gmail.com>
Co-authored-by: John Doe <kpcee@mailbox.com>
Co-authored-by: Jakub Wasylków <kuba_160@protonmail.com>
Co-authored-by: Toad King <toadking@toadking.com>
Co-authored-by: Thomas Jepp <tom@tomjepp.co.uk>
Co-authored-by: Nicolai Syvertsen <saivert@saivert.com>
Co-authored-by: freddii <freddii@users.noreply.github.com>
Co-authored-by: Johannes <johannes.wikner@gmail.com>
chore: Merge with latest main branch (#5)

* chore: Remove .travis.yml

* [pull] master from DeaDBeeF-Player:master (#4)

* cocoaui: sort items in medialib groups

* cocoaui: improve medialib right click track selection

* medialib: update modified tracks when mtime is higher than scan time

* cocoaui: fix window title formatting in pre-bigsur macOS (fixes #2433)

* fixed a few thread safety issues

* cocoaui: medialib outline view styling: make it look right in big sur beta 5

* cocoaui: fix medialib random crash on startup

* medialib: added support for multiple toplevel music folders

* fix tests

* cocoaui: sort medialib tracks accounting for disc nr

* cocoaui: added medialib configure button

* cocoaui: medialib configuration UI WIP

* cocoaui: cleanup

* cocoaui: eliminate the need to install libjansson to build medialib plugin

* compile fix

* cocoaui: medialib folder list editing

* coreaudio: prevent sleep while playback is active (fixes #2434)

* coreaudio: use kIOPMAssertionTypeNoIdleSleep instead of kIOPMAssertionTypeNoDisplaySleep, to allow display sleep while music is playing (fixes #2434)

* coreaudio: avoid sleep prevention code from being executed multiple times, just in case

* Revert "streamer: workaround for double-sending DB_EV_SONGCHANGED"

This reverts commit d63a53bf70f09bc8534f6394580221c0af1babeb.

* cocoaui: fix group titles clipping (#2436)

* cocoaui: fix crash after clicking empty space in column header, causing array write at index -1

* cocoaui: use path instead of uri when changing medialib settings

* medialib: thread safe configuration access

* cocoaui: medialib search

* cocoaui: added a comment for -[MediaLibraryOutlineView validateProposedFirstResponder:forEvent:]

* medialib: ensure playlist is not null before clearing, when medialib paths list is empty

* medialib: fix applying medialib configuration when no previous configuration existed

* medialib: fix crash caused by under-retained track references

* junklib: fix bug in shift_jis detection, which caused memory corruption

* medialib: music files in the top level music folder will now show up with folder filter

* cocoaui: remove the apply/reset buttons from medialib preferences

* cocoaui: report artist/album/title to the NowPlayingInfoCenter (fixes #2401)

* cocoaui: report artist/album/title to the NowPlayingInfoCenter (fixes #2401)

* junklib: fix bug in shift_jis detection, which caused memory corruption

* medialib: remove ddb_playlist_t access from medialib API

* cocoaui: reimplemented medialib drag-drop functionality without involving the internal medialib playlist

* medialib: new plugin API, which allows multiple sources / instances (WIP)

* medialib: new plugin type for supporting multiple media sources

* medialib: fix types / names

* deadbeef.h: media source plugin api level limit

* gtkui: move files to trash

* mp4p update

* travis: cancel windows build if make fails

* mp4: cleanup old text fields before writing new ones

* mp4: fixed writing total discs

* mp4: fix writing disc number

* added clang building support using docker

* travis: build on ubuntu bionic + clang

* enable building medialib plugin for linux

* artwork: convert to libdispatch

* artwork: moved image loading/scaling routines to separate files

* cocoaui: fix libjansson install path

* medialib: add build steps for automake/autoconf build

* medialib: add jannson flags to build

* lastfm: convert to libdispatch

* cocoaui: improved playlist focus handling

* osx: gtkui project settings fixups

* gtkui: fix assigning hotkeys with shift (fixes #2437)

* gtkui: align hotkey code changes, and make hotkeys assignments work again (fixes #2437)

* gtkui: make playlist min width 250px, to prevent drawing/scrolling problems (fixes #1385)

* gtkui: fix crash / memory leak after shrinking gtkbox (fixes #2297)

* gtkui: fix drawing playlist column separators on newer GTK themes

* notify: convert to libdispatch (fixes #1077)

* premake5-win: add medialib

* premake5: use clang

* travis: add clang for windows build

* premake/travis: dispatch windows support

* travis: add build messages for windows

* gtkui: fix pixmaps and document paths in xcode project

* travis: remove gcc from windows build

* travis: enforce clang on windows builds

* cocoaui: disable experimental/WIP features for master branch

* switch from gnu99 to c99

* xcodeconfig folder rename

* jansson xcconfig delete

* switch to clang in apgcc wrapper

* cleanup 18.04 docker build scripts

* added 14.04 clang docker build scripts

* switch travis config back to 14.04

* added clang requirement to configure.ac

* README update

* README.md update

* README.md update

* exclude medialib from build on master

* 14.04 docker test script to include libdispatch

* apbuild as submodule, ship libdispatch+deps in portable build

* submodule fix

* ffmpeg: cleanup unused enum

* tf: include band field in %artist% mapping

* aac: close the file if it is mp4 but not aac

This prevents leaking a file descriptor for each ALAC file.

* alac: re-use the same fd for mp4reader, don't open a duplicate

This prevents a FD leak for each played ALAC file.

* gtkui: fix file filter for the GtkFileChooserNative implementation

* osx: remove jansson xcconfig reference

* dumb: added some extra error recilience to s3m parser, to make certain seemingly corrupt s3m files to load and play, although it's not clear whether they're playing correctly (fixes #2463)

* cocoaui: covermanager slight cleanup and modernization

* main: Make socket path use XDG_RUNTIME_DIR if set again

This was accidentally reverted by commit 61c3bc88732ff1dd21a3e8fef706dede0d01df17
On Windows XDG_RUNTIME_DIR is unset to it will simply use the dbconfdir path.

* mp4p update: memory leak fix (fixes #2474)

* cocoaui/artwork: fix memory leaks

* cocoaui: fix logger autoscroll when the log window shows for the first time with enough initial content to cause scrolling

* gtkui: Fix warning when there is no log action

* gtkui: fix logwindow/widget scroll detection

This fixes issue where scroll changes in response
to resizing the log window. Hooking the value-changed
signal of the GtkAdjustment is better as it only
triggers when users directly scroll.

* aac: update copyright notice

* lastfm: fix a few reference counting bugs

* disable medialib API for 1.8.5

* 1.8.5-beta1 + ChangeLog

* Changelog update

* portaudio: convert to libdispatch

* portaudio: fix playback of non-existent tracks

* README.md update

* premake5: add pkgconfig_cflags

* premake5: refactor build script

* premake5: fix windows build

* premake5: disable medialib by default

* travis: add yasm for windows build

* vtx: do not include sys/mman.h

* libwin: add shn workarounds

* premake5: add plugins, many different changes

* travis: update premake invokation

* README.md update

* windows_postbuild: fix removal of *.lib files

* premake5: fix for linux

* Replace premake5.lua with premake5-win.lua

* premake5: cleanup, disable plugins for windows

* Revert "vtx: do not include sys/mman.h"

This reverts commit 0f3daf54d1b273dab2c34ab790d8f4be056e0194.

* gtkui: fix autoresizing columns when deadbeef starts maximized (fixes #2414)

* gtkui: implemented a more robust method of detecting when gtk widget has been realized and resized ot its final size and maximization state (fixes #2414)

* osx: gtk version update

* mp4tagutil: fix a bug which caused calculating negative padding size, when moov is already located at the end of file (fixes #2474)

* gme: fix weird typedefs

* updated docker scripts

* gtkui: fix playlist not drawing content on startup (fixes #2484)

* New vscode debug config for autotools builds

* Add two  generated files to .gitignore

* gtkui: add parent menu for widgets

* Make data key a constant

* gtkui: Update titlebar based on TF update interval

* Make sure to cancel timer on shutdown

* Changelog update

* deadbeef.h: removed obsolete wiki link

* streamer: fix album shuffle after last album track issue (fixes #2471)

* 1.8.5-beta2 + changelog

* streamer: fix shuffle bug repeating the same tracks when more than 2 tracks queue up (fixes #2438)

* Changelog update

* osx: delete xcode workspace, since it's not really used, only referencing a single project

* travis: use Xcode12.2

* translations update for 1.8

* docker: added --progress plain to be able to read the logs

* 1.8.5-rc1

* about box copyright year update

* 1.8.5 release

* devel version

* include libdispatch & deps in the package builds

enable package upload for devel builds

* devel version

* include libdispatch & deps in the package builds

* fix resource paths in non-portable builds (fixes #2499)

* 1.8.6-rc1

* fix resource paths in non-portable builds (fixes #2499)

* devel version

* translations update

* gtkui: fix truncating of long group titles (fixes #2436)

* gtkui: fix truncating of long group titles (fixes #2436)

* update translations

* 1.8.6 release

* pulse: error handling instead of assert

* include libdispatch & deps in the package builds

* shellexec: cleanup legacy config support, rename the config property name to not contain WIP (#829)

* aac: instant bitrate support (fixes #1561)

* aac: fix mp4 chapter functionality

* devel version

* aac: instant bitrate support (fixes #1561)

* aac: fix mp4 chapter functionality

* gtkui: delete unused old code

* delete unused commented out code

* vfs_curl: added connection timeout of 10 seconds (fixes #1094)

* cocoaui: new preferences icons

* cocoaui: fix toolbar appearance (selectable items)

* hotkeys: initialize after all plugins has loaded / connected (fixes #1150)

* hotkeys: fix the missing action_sort_by_title, fix warnings

* junklib: do not discard id3v2.3-specific frames, when they can be converted to TXXX; also convert such TXXX frames correctly when writing id3v2.4 (fixes #2304)

* docker: fix parameter typo

* gtkui: refactor delete from disk code to be reusable

* cocoaui: added delete from disk

* premake: add libdeletefromdisk

* premake: fix lib parse

* premake: don't put static libs in output build

* mp4: fix crash caused by encountering unsupporter metadata fields; fix writing the padding after moov is moved to the end of file (fix #2509)

* disable travis-ci

* mac github action

* linux github action

* windows github action

* enable uploading artifacts from github actions

* added github workflow build status badges

* updated README; added discord link

* cocoaui: fix deallocating MainWindowController; correctly capture track list for the menu actions

* cocoaui: reference cycle fixing

* cocoaui: fix cleaning up PlaylistViewController

* deletefromdisk: rename DeleteFromDiskTrackList to a more reusability-friendly UtilTrackList

* fixed typos

* better github action job names, for discord webhook

* ID3v2 frame sizes are before unsynchronisation

* junklib: fixes id3v2.3 unsynchronized tag reading support without breaking id3v2.4

* alac: handle invalid ALAC box data with samplerate=0

* alac: fix handling alac files with 48 byte ASC

* main: save resume state immediately after every song finished or playback is stopped

* alsa: move hwparams setting to the playback thread

* streamer: remove the deferred setformat hack

* alsa: setformat error handling

* pulse: handle setformat called from streamer_read on playback thread

* alsa, pulse: _setformat_requested cleanup

* osx: gtk3 xcconfig update

* gtkui: capture selected track list when the context menu opens (fixes #2432)

* osx: fix gtk icon loading

* osx: build FLAC without DEBUG=1, to prevent asserts in debug builds

* wma: double-free crash fix for wma+cue

* cocoaui: fix crash when pinned group is not updated after groups are rebuilt

* osx: reordered targets alphabetically

* adplug: fix or suppress warnings when building in Xcode

* m3u: fix warnings and add tests for m3u loader

* osx: shut up wildmidi warnings

* osx: fix mp3/mpg123/libmad warnings

* osx: deps update (libzip warnings)

* osx: shutup ogg/vorbis/opus warnings

* osx: ebur128 warnings fix

* osx: shut up most of the sc68 warnings

* osx: separate libwavpack into separate target, and fix warnings

* osx: shut up libsamplerate warnings

* osx: shut up psf warnings

* osx: update gtk3 xcconfig

* osx: remove libmp4ff

* osx: shut up gme warnings

* osx: shut up jansson warnings

* osx: shut up wma warnings

* osx: fix a few warnings, don't set deployment target explicitly

* osx: shut up vtx warnings

* osx: shut up libvorbis warnings

* osx: shut up tta warnings

* osx: shut up libsndfile warnings

* osx: shut up libsidplay2 warnings

* osx: sid warnings fix

* osx: libsamplerate compile fix

* osx: fix deployment target for tests

Co-authored-by: Alexey Yakovenko <wakeroid@gmail.com>
Co-authored-by: John Doe <kpcee@mailbox.com>
Co-authored-by: Jakub Wasylków <kuba_160@protonmail.com>
Co-authored-by: Toad King <toadking@toadking.com>
Co-authored-by: Thomas Jepp <tom@tomjepp.co.uk>
Co-authored-by: Nicolai Syvertsen <saivert@saivert.com>
Co-authored-by: freddii <freddii@users.noreply.github.com>
Co-authored-by: Johannes <johannes.wikner@gmail.com>

* Restyle chore: Merge with latest main branch (#6)

* Restyled by astyle

* Restyled by clang-format

* Restyled by jq

* Restyled by prettier-markdown

* Restyled by shellharden

* Restyled by shfmt

* Restyled by whitespace

Co-authored-by: Restyled.io <commits@restyled.io>

* Restyle chore: Merge with latest main branch (#7)

* Restyled by astyle

* Restyled by clang-format

* Restyled by shellharden

Co-authored-by: Restyled.io <commits@restyled.io>

* Restyle chore: Merge with latest main branch (#8)

* Restyled by astyle

* Restyled by clang-format

Co-authored-by: Restyled.io <commits@restyled.io>

* Restyle chore: Merge with latest main branch (#9)

* Restyled by astyle

* Restyled by clang-format

Co-authored-by: Restyled.io <commits@restyled.io>

* Restyle chore: Merge with latest main branch (#10)

* Restyled by astyle

* Restyled by clang-format

Co-authored-by: Restyled.io <commits@restyled.io>

* Restyle chore: Merge with latest main branch (#11)

* Restyled by astyle

* Restyled by clang-format

Co-authored-by: Restyled.io <commits@restyled.io>

Co-authored-by: pull[bot] <39814207+pull[bot]@users.noreply.github.com>
Co-authored-by: Alexey Yakovenko <wakeroid@gmail.com>
Co-authored-by: John Doe <kpcee@mailbox.com>
Co-authored-by: Jakub Wasylków <kuba_160@protonmail.com>
Co-authored-by: Toad King <toadking@toadking.com>
Co-authored-by: Thomas Jepp <tom@tomjepp.co.uk>
Co-authored-by: Nicolai Syvertsen <saivert@saivert.com>
Co-authored-by: freddii <freddii@users.noreply.github.com>
Co-authored-by: Johannes <johannes.wikner@gmail.com>
Co-authored-by: restyled-io[bot] <32688539+restyled-io[bot]@users.noreply.github.com>
Co-authored-by: Restyled.io <commits@restyled.io>
