Remove dead code

closes #458
Fix ~/.fehbg creation when using --randomize or directory arguments

Closes #456

Note that --randomize and directory names are not saved in fehbg. It only
contains the command line needed to recreate the wallpaper setup which
resulted from its invocation, i.e., after randomization etc. have been
applied. This is intentional.
Do not omit filenames in ~/.fehbg when using --no-xinerama (#456)
Release v3.1.3

Closes #456
