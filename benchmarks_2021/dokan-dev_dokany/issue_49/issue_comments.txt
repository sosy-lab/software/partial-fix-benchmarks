Fix trailing backslash in path of GetFileInformation()/OpenDirectory() call during SetAccessControl()
This should make the work.
(I was not able to reproduce the **"\Dir\"** to test).

@Liryna The changes are wrong.
Among other things it breaks directory rename when the directory is located at root drive.

Reverted the 'empty filename change' only which was a wrong fix and cause the new bug, with commit https://github.com/dokan-dev/dokany/commit/4496617e47dabf2123020e7732ddc15264005260.
