diff --git a/core/win32/ntdll.c b/core/win32/ntdll.c
index dc41b54315..2ee1d6f42b 100644
--- a/core/win32/ntdll.c
+++ b/core/win32/ntdll.c
@@ -4674,8 +4674,8 @@ our_create_thread_ex(HANDLE hProcess, bool target_64bit, void *start_addr,
     info.teb.buffer = &teb;
     res = NT_RAW_SYSCALL(CreateThreadEx, &hthread, THREAD_ALL_ACCESS, &oa, hProcess,
                          (LPTHREAD_START_ROUTINE)convert_data_to_function(start_addr),
-                         thread_arg, !!suspended, 0, stack_commit, stack_reserve,
-                         &info);
+                         thread_arg, suspended ? TRUE : FALSE, 0,
+                         stack_commit, stack_reserve, &info);
     if (!NT_SUCCESS(res)) {
         NTPRINT("create_thread_ex: failed to create thread: %x\n", res);
         return INVALID_HANDLE_VALUE;
diff --git a/libutil/detach.c b/libutil/detach.c
index 2441a2b698..be0021bbeb 100644
--- a/libutil/detach.c
+++ b/libutil/detach.c
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2011-2013 Google, Inc.  All rights reserved.
+ * Copyright (c) 2011-2018 Google, Inc.  All rights reserved.
  * Copyright (c) 2003-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -59,6 +59,18 @@ typedef NTSTATUS (NTAPI *NtCreateThreadType) (OUT PHANDLE ThreadHandle,
                                               IN PUSER_STACK UserStack,
                                               IN BOOLEAN CreateSuspended);
 NtCreateThreadType NtCreateThread = NULL;
+typedef NTSTATUS (NTAPI *NtCreateThreadExType) (OUT PHANDLE ThreadHandle,
+                                                IN ACCESS_MASK DesiredAccess,
+                                                IN POBJECT_ATTRIBUTES ObjectAttributes,
+                                                IN HANDLE ProcessHandle,
+                                                IN LPTHREAD_START_ROUTINE StartAddress,
+                                                IN LPVOID StartParameter,
+                                                IN BOOL CreateSuspended,
+                                                IN uint StackZeroBits,
+                                                IN SIZE_T StackCommitSize,
+                                                IN SIZE_T StackReserveSize,
+                                                INOUT create_thread_info_t *thread_info);
+NtCreateThreadExType NtCreateThreadEx = NULL;
 
 #ifdef X64
 typedef unsigned __int64 ptr_uint_t;
@@ -175,16 +187,15 @@ get_kernel_thread_start_thunk()
  *     possible, could try to share. */
 /* NOTE - stack_reserve and stack commit must be multiples of PAGE_SIZE and reserve
  *     should be at least 5 pages larger then commit */
-/* NOTE - if !target_api, target thread routine can't exit by by returning, instead it
- *     must terminate itself.
+/* NOTE - if !target_api && < win8, target thread routine can't exit by returning:
+ *    instead it must terminate itself.
  * NOTE - caller or target thread routine is responsible for informing csrss (if
  *    necessary) and freeing the the thread stack (caller is informed of the stack
  *    via OUT remote_stack arg).
- * If arg_buf is non-null then the arg_buf_size bytes of arg_buf are copied onto the
- * new thread's stack and pointer to them is passed as the argument to the start
- * routine instead of arg.
- * FIXME - on Vista we could be using NtCreateThreadEx (like all the api routines do),
- *    but this still works fine and that would complicate argument handling.
+ * If arg_buf is non-null then the arg_buf_size bytes of arg_buf are copied onto
+ * the new thread's stack (< win8) or into a new alloc (>= win8) and pointer to
+ * them is passed as the argument to the start routine instead of arg.  For >= win8,
+ * the arg_buf copy must be freed with NtFreeVirtualMemory by the caller.
  */
 static HANDLE
 nt_create_thread(HANDLE hProcess, PTHREAD_START_ROUTINE start_addr,
@@ -210,11 +221,22 @@ nt_create_thread(HANDLE hProcess, PTHREAD_START_ROUTINE start_addr,
     DO_ASSERT(!target_64bit); /* Not supported. */
 #endif
 
-    if (NtCreateThread == NULL) {
-        /* FIXME - on Vista we could instead use NtCreateThreadEx which is what all
-         * the api routines use and requires less setup. But it also uses structs that
-         * we haven't fully reversed or found good documentation for. */
-        /* Don't need a lock, writing the pointer (4 [8 on 64-bit] byte write) is atomic. */
+    get_platform(&platform);
+    DO_ASSERT(platform != 0);
+
+    if (platform >= PLATFORM_WIN_8) {
+        /* NtCreateThread is not supported to we have to use NtCreateThreadEx,
+         * which simplifies the stack but complicates arg_buf and uses undocumented
+         * structures.
+         */
+        if (NtCreateThreadEx == NULL) {
+            NtCreateThreadEx = (NtCreateThreadExType)
+                GetProcAddress(GetModuleHandle(L"ntdll.dll"), "NtCreateThreadEx");
+            if (NtCreateThreadEx == NULL)
+                goto error;
+        }
+    } else if (NtCreateThread == NULL) {
+        /* Don't need a lock, writing the pointer (4 [8 on x64] byte write) is atomic. */
         NtCreateThread = (NtCreateThreadType)
             GetProcAddress(GetModuleHandle(L"ntdll.dll"), "NtCreateThread");
         if (NtCreateThread == NULL)
@@ -267,131 +289,165 @@ nt_create_thread(HANDLE hProcess, PTHREAD_START_ROUTINE start_addr,
 
     InitializeObjectAttributes(&oa, NULL, OBJ_CASE_INSENSITIVE, NULL, sd);
 
-    stack.ExpandableStackBottom =
-        VirtualAllocEx(hProcess, NULL,
-                       /* we leave the top page MEM_FREE: we could reserve it instead
-                        * and then adjust our get_stack_bounds() core assert */
-                       stack_reserve - PAGE_SIZE,
-                       MEM_RESERVE, PAGE_READWRITE);
-    if (remote_stack != NULL)
-        *remote_stack = stack.ExpandableStackBottom;
-    if (stack.ExpandableStackBottom == NULL)
-        goto error;
-
-    /* We provide non-committed boundary page on each side of the stack just to
-     * be safe (note we will get a stack overflow exception if stack grows to
-     * 3rd to last page of this region (xpsp2)). */
-    stack.ExpandableStackBottom =
-        ((byte *)stack.ExpandableStackBottom) + PAGE_SIZE;
-    stack.ExpandableStackBase =
-        ((byte *)stack.ExpandableStackBottom) + stack_reserve - (2*PAGE_SIZE);
-    /* PR 252008: WOW64's initial APC uses the stack base, ignoring CONTEXT.Esp,
-     * so we put an extra page in place for the nudge arg.  It should be
-     * freed w/ no problems since the Bottom's region is freed.
-     * An alternative is a separate allocation and setting NUDGE_FREE_ARG,
-     * but the caller is the one who knows the structure of the arg.
-     */
-    if (wow64)
-        stack.ExpandableStackBase = ((byte*)stack.ExpandableStackBase) - PAGE_SIZE;
-
-    stack.ExpandableStackLimit =
-        ((byte *)stack.ExpandableStackBase) - stack_commit;
-    num_commit_bytes = stack_commit + PAGE_SIZE;
-    p = ((byte *)stack.ExpandableStackBase) - num_commit_bytes;
-    if (wow64)
-        num_commit_bytes += PAGE_SIZE;
-    p = VirtualAllocEx(hProcess, p, num_commit_bytes, MEM_COMMIT, PAGE_READWRITE);
-    if (p == NULL)
-        goto error;
-    if (!VirtualProtectEx(hProcess, p, PAGE_SIZE, PAGE_READWRITE|PAGE_GUARD, &old_prot))
-        goto error;
-
-    get_platform(&platform);
-    DO_ASSERT(platform != 0);
-
-    /* set the context: initialize with our own */
-    context.ContextFlags = CONTEXT_FULL;
-    GetThreadContext(GetCurrentThread(), &context);
-
-    /* set esp */
-    context.CXT_XSP = (ptr_uint_t)stack.ExpandableStackBase;
-    /* On vista, the kernel sets esp a random number of bytes in from the base of
-     * the stack as part of the stack ASLR.  RtlUserThreadStart assumes that
-     * esp will be set at least 8 bytes into the region by writing to esp+4 and
-     * esp+8. So we need to move up by at least 8 bytes.  The smallest offset
-     * I've seen is 56 bytes so we'll go with that as there could be another
-     * place assuming there's room above esp. We do it irregardless of whether we
-     * target api or not in case it's relied on elsewhere. */
-#define VISTA_THREAD_STACK_PAD 56
-    if (platform >= PLATFORM_VISTA) {
-        context.CXT_XSP -= VISTA_THREAD_STACK_PAD;
-    }
-    /* Anticipating x64 we align to 16 bytes everywhere */
-#define STACK_ALIGNMENT 16
-    /* write the argument buffer to the stack */
-    if (arg_buf != NULL) {
-        SIZE_T written;
-        if (wow64) { /* PR 252008: see above */
-            thread_arg = (void *)
-                (((byte *)stack.ExpandableStackBase) + PAGE_SIZE - arg_buf_size);
-        } else {
-            context.CXT_XSP -= ALIGN_FORWARD(arg_buf_size, STACK_ALIGNMENT);
-            thread_arg = (void *)context.CXT_XSP;
+    if (platform >= PLATFORM_WIN_8) {
+        create_thread_info_t info = {0};
+        CLIENT_ID cid;
+        TEB *teb;
+        if (arg_buf != NULL) {
+            SIZE_T written;
+            void *arg_copy = VirtualAllocEx(hProcess, NULL, arg_buf_size,
+                                            MEM_COMMIT, PAGE_READWRITE);
+            if (arg_copy == NULL)
+                goto error;
+            if (!WriteProcessMemory(hProcess, arg_copy, arg_buf, arg_buf_size,
+                                    &written) ||
+                written != arg_buf_size) {
+                goto error;
+            }
+            thread_arg = arg_copy;
         }
-        if (!WriteProcessMemory(hProcess, thread_arg, arg_buf, arg_buf_size, &written) ||
-            written != arg_buf_size) {
+        info.struct_size = sizeof(info);
+        info.client_id.flags = THREAD_INFO_ELEMENT_CLIENT_ID |
+            THREAD_INFO_ELEMENT_UNKNOWN_2;
+        info.client_id.buffer_size = sizeof(cid);
+        info.client_id.buffer = &cid;
+        /* We get STATUS_INVALID_PARAMETER unless we also ask for teb. */
+        info.teb.flags = THREAD_INFO_ELEMENT_TEB | THREAD_INFO_ELEMENT_UNKNOWN_2;
+        info.teb.buffer_size = sizeof(TEB*);
+        info.teb.buffer = &teb;
+        if (!NT_SUCCESS(NtCreateThreadEx(&hThread, THREAD_ALL_ACCESS, &oa,
+                                         hProcess, start_addr, thread_arg,
+                                         suspended ? TRUE : FALSE, 0, 0, 0, &info))) {
             goto error;
         }
+        if (tid != NULL)
+            *tid = (ptr_uint_t)cid.UniqueThread;
+    } else {
+        stack.ExpandableStackBottom =
+            VirtualAllocEx(hProcess, NULL,
+                           /* we leave the top page MEM_FREE: we could reserve it instead
+                            * and then adjust our get_stack_bounds() core assert */
+                           stack_reserve - PAGE_SIZE,
+                           MEM_RESERVE, PAGE_READWRITE);
+        if (remote_stack != NULL)
+            *remote_stack = stack.ExpandableStackBottom;
+        if (stack.ExpandableStackBottom == NULL)
+            goto error;
+
+        /* We provide non-committed boundary page on each side of the stack just to
+         * be safe (note we will get a stack overflow exception if stack grows to
+         * 3rd to last page of this region (xpsp2)). */
+        stack.ExpandableStackBottom =
+            ((byte *)stack.ExpandableStackBottom) + PAGE_SIZE;
+        stack.ExpandableStackBase =
+            ((byte *)stack.ExpandableStackBottom) + stack_reserve - (2*PAGE_SIZE);
+        /* PR 252008: WOW64's initial APC uses the stack base, ignoring CONTEXT.Esp,
+         * so we put an extra page in place for the nudge arg.  It should be
+         * freed w/ no problems since the Bottom's region is freed.
+         * An alternative is a separate allocation and setting NUDGE_FREE_ARG,
+         * but the caller is the one who knows the structure of the arg.
+         */
+        if (wow64)
+            stack.ExpandableStackBase = ((byte*)stack.ExpandableStackBase) - PAGE_SIZE;
+
+        stack.ExpandableStackLimit =
+            ((byte *)stack.ExpandableStackBase) - stack_commit;
+        num_commit_bytes = stack_commit + PAGE_SIZE;
+        p = ((byte *)stack.ExpandableStackBase) - num_commit_bytes;
+        if (wow64)
+            num_commit_bytes += PAGE_SIZE;
+        p = VirtualAllocEx(hProcess, p, num_commit_bytes, MEM_COMMIT, PAGE_READWRITE);
+        if (p == NULL)
+            goto error;
+        if (!VirtualProtectEx(hProcess, p, PAGE_SIZE, PAGE_READWRITE|PAGE_GUARD,
+                              &old_prot))
+            goto error;
+
+        /* set the context: initialize with our own */
+        context.ContextFlags = CONTEXT_FULL;
+        GetThreadContext(GetCurrentThread(), &context);
+
+        /* set esp */
+        context.CXT_XSP = (ptr_uint_t)stack.ExpandableStackBase;
+        /* On vista, the kernel sets esp a random number of bytes in from the base of
+         * the stack as part of the stack ASLR.  RtlUserThreadStart assumes that
+         * esp will be set at least 8 bytes into the region by writing to esp+4 and
+         * esp+8. So we need to move up by at least 8 bytes.  The smallest offset
+         * I've seen is 56 bytes so we'll go with that as there could be another
+         * place assuming there's room above esp. We do it irregardless of whether we
+         * target api or not in case it's relied on elsewhere. */
+#define VISTA_THREAD_STACK_PAD 56
         if (platform >= PLATFORM_VISTA) {
-            /* pad after our argument so RtlUserThreadStart won't clobber it */
             context.CXT_XSP -= VISTA_THREAD_STACK_PAD;
         }
-    }
+        /* Anticipating x64 we align to 16 bytes everywhere */
+#define STACK_ALIGNMENT 16
+        /* write the argument buffer to the stack */
+        if (arg_buf != NULL) {
+            SIZE_T written;
+            if (wow64) { /* PR 252008: see above */
+                thread_arg = (void *)
+                    (((byte *)stack.ExpandableStackBase) + PAGE_SIZE - arg_buf_size);
+            } else {
+                context.CXT_XSP -= ALIGN_FORWARD(arg_buf_size, STACK_ALIGNMENT);
+                thread_arg = (void *)context.CXT_XSP;
+            }
+            if (!WriteProcessMemory(hProcess, thread_arg, arg_buf, arg_buf_size,
+                                    &written) ||
+                written != arg_buf_size) {
+                goto error;
+            }
+            if (platform >= PLATFORM_VISTA) {
+                /* pad after our argument so RtlUserThreadStart won't clobber it */
+                context.CXT_XSP -= VISTA_THREAD_STACK_PAD;
+            }
+        }
 
-    /* set eip and argument */
-    if (target_api) {
-        if (platform >= PLATFORM_VISTA) {
-            context.CXT_XIP = (ptr_uint_t)GetProcAddress(GetModuleHandle(L"ntdll.dll"),
-                                               "RtlUserThreadStart");
+        /* set eip and argument */
+        if (target_api) {
+            if (platform >= PLATFORM_VISTA) {
+                context.CXT_XIP = (ptr_uint_t)
+                    GetProcAddress(GetModuleHandle(L"ntdll.dll"), "RtlUserThreadStart");
+            } else {
+                context.CXT_XIP = (ptr_uint_t)get_kernel_thread_start_thunk();
+            }
+            /* For kernel32!BaseThreadStartThunk and ntdll!RltUserThreadStartThunk
+             * Eax contains the address of the thread routine and Ebx the arg */
+            context.THREAD_START_ADDR = (ptr_uint_t)start_addr;
+            context.THREAD_START_ARG = (ptr_uint_t)thread_arg;
         } else {
-            context.CXT_XIP = (ptr_uint_t)get_kernel_thread_start_thunk();
+            void *buf[2];
+            BOOL res;
+            SIZE_T written;
+            /* directly targeting the start_address */
+            context.CXT_XIP = (ptr_uint_t)start_addr;
+            context.CXT_XSP -= sizeof(buf);
+            /* set up arg on stack, give NULL return address */
+            buf[1] = thread_arg;
+            buf[0] = NULL;
+            res = WriteProcessMemory(hProcess, (void *)context.CXT_XSP, &buf,
+                                     sizeof(buf), &written);
+            if (!res || written != sizeof(buf)) {
+                goto error;
+            }
         }
-        /* For kernel32!BaseThreadStartThunk and ntdll!RltUserThreadStartThunk
-         * Eax contains the address of the thread routine and Ebx the arg */
-        context.THREAD_START_ADDR = (ptr_uint_t)start_addr;
-        context.THREAD_START_ARG = (ptr_uint_t)thread_arg;
-    } else {
-        void *buf[2];
-        BOOL res;
-        SIZE_T written;
-        /* directly targeting the start_address */
-        context.CXT_XIP = (ptr_uint_t)start_addr;
-        context.CXT_XSP -= sizeof(buf);
-        /* set up arg on stack, give NULL return address */
-        buf[1] = thread_arg;
-        buf[0] = NULL;
-        res = WriteProcessMemory(hProcess, (void *)context.CXT_XSP, &buf,
-                                 sizeof(buf), &written);
-        if (!res || written != sizeof(buf)) {
+        if (context.CXT_XIP == 0) {
+            DO_ASSERT(false);
             goto error;
         }
-    }
-    if (context.CXT_XIP == 0) {
-        DO_ASSERT(false);
-        goto error;
-    }
 
-    /* NOTE - CreateThread passes NULL for object attributes so despite Nebbet
-     * must be optional (checked NTsp6a, XPsp2). We don't pass NULL so we can
-     * specify the security descriptor. */
-    if (!NT_SUCCESS(NtCreateThread(&hThread, THREAD_ALL_ACCESS, &oa,
-                                   hProcess, &cid, &context, &stack,
-                                   (byte)(suspended ? TRUE : FALSE)))) {
-        goto error;
-    }
+        /* NOTE - CreateThread passes NULL for object attributes so despite Nebbet
+         * must be optional (checked NTsp6a, XPsp2). We don't pass NULL so we can
+         * specify the security descriptor. */
+        if (!NT_SUCCESS(NtCreateThread(&hThread, THREAD_ALL_ACCESS, &oa,
+                                       hProcess, &cid, &context, &stack,
+                                       (byte)(suspended ? TRUE : FALSE)))) {
+            goto error;
+        }
 
-    if (tid != NULL)
-       *tid = (ptr_uint_t)cid.UniqueThread;
+        if (tid != NULL)
+            *tid = (ptr_uint_t)cid.UniqueThread;
+    }
 
  exit:
     if (sd != NULL) {
@@ -410,6 +466,7 @@ nt_create_thread(HANDLE hProcess, PTHREAD_START_ROUTINE start_addr,
     }
     DO_ASSERT(hThread == NULL);
     hThread = NULL; /* just to be safe */
+    SetLastError(ERROR_INVALID_HANDLE);
     goto exit;
 }
 
@@ -466,7 +523,8 @@ get_dr_marker(process_id_t ProcessID, dr_marker_t *marker,
               hotp_policy_status_table_t **hotp_status, int *found);
 
 DWORD
-nudge_dr(process_id_t pid, BOOL allow_upgraded_perms, DWORD timeout_ms, nudge_arg_t *nudge_arg)
+nudge_dr(process_id_t pid, BOOL allow_upgraded_perms, DWORD timeout_ms,
+         nudge_arg_t *nudge_arg)
 {
     DWORD res = ERROR_SUCCESS;
     int found;
@@ -605,9 +663,19 @@ generic_nudge(process_id_t pid, BOOL allow_upgraded_perms, DWORD action_mask,
               DWORD timeout_ms)
 {
     nudge_arg_t arg = {0};
+    DWORD platform = 0;
+    get_platform(&platform);
+    DO_ASSERT(platform != 0);
     arg.version = NUDGE_ARG_CURRENT_VERSION;
     arg.nudge_action_mask = action_mask;
     /* default flags */
+    if (platform >= PLATFORM_WIN_8) {
+        /* i#1309: We use NtCreateThreadEx which is different: */
+        /* The kernel owns and frees the stack. */
+        arg.flags |= NUDGE_NUDGER_FREE_STACK;
+        /* The arg is placed in a new kernel alloc. */
+        arg.flags |= NUDGE_FREE_ARG;
+    }
     arg.client_id = client_id;
     arg.client_arg = client_arg;
     return nudge_dr(pid, allow_upgraded_perms, timeout_ms, &arg);
@@ -671,7 +739,8 @@ inject_dll(process_id_t pid, const WCHAR *dll_name, BOOL allow_upgraded_perms,
         }
     }
 
-    pfnThreadRtn = (PTHREAD_START_ROUTINE) GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");
+    pfnThreadRtn = (PTHREAD_START_ROUTINE)
+        GetProcAddress(GetModuleHandle(TEXT("Kernel32")), "LoadLibraryW");
     if (pfnThreadRtn == NULL) {
         res = GetLastError();
         goto exit;
diff --git a/suite/tests/CMakeLists.txt b/suite/tests/CMakeLists.txt
index 4cc0c22f92..3ae2d0c16a 100644
--- a/suite/tests/CMakeLists.txt
+++ b/suite/tests/CMakeLists.txt
@@ -1237,52 +1237,70 @@ function(torun test key source native standalone_dr dr_ops exe_ops added_out)
   endif ()
 
   if (is_runall)
-    # to run the test we use a series of commands in runall.cmake.
+    # To run the test we use a series of commands in runall.cmake.
     file(READ "${testpath}/${srcbase}.runall" runall)
     if (UNIX)
-      # swap from drrun to run_in_bg to run the test in the background
-      # XXX i#1874: add support for running on Android
-      set(tmpfile "${CMAKE_CURRENT_BINARY_DIR}/${test}-out")
-      string(REGEX REPLACE "^[^/]*(/.*)/drrun" "\\1/run_in_bg;-out;${tmpfile};\\1/drrun"
+      # We always run infloop.
+      get_target_path_for_execution(app_path linux.infloop)
+    else ()
+      # It's easier to run our own infloop, which prints so we know when it's up,
+      # than to run notepad or sthg.
+      get_target_path_for_execution(app_path win32.infloop)
+    endif ()
+    # Swap from drrun to run_in_bg to run the test in the background and print the pid.
+    # XXX i#1874: add support for running on Android
+    set(tmpfile "${CMAKE_CURRENT_BINARY_DIR}/${key}-out")
+    if (UNIX)
+      set(pidfile "")
+      string(REGEX REPLACE "^(.*)/drrun" "\\1/run_in_bg;-out;${tmpfile};\\1/drrun"
         rundr "${rundr}")
-      # Remove timeout args to drrun to ensure that it doesn't fork the app.
-      string(REGEX REPLACE ";-s;[0-9]+" "" rundr "${rundr}")
-      string(REGEX REPLACE ";-killpg" "" rundr "${rundr}")
-      set(clear_arg "")
-      if ("${runall}" MATCHES "<reset>")
-        set(nudge_arg "-type\;reset")
-      elseif ("${runall}" MATCHES "<freeze>")
-        set(nudge_arg "-type\;freeze")
-      elseif ("${runall}" MATCHES "<persist>")
-        set(nudge_arg "-type\;persist")
-        # clear out pcache dir prior to run
-        set(clear_arg "${PCACHE_SHARED_DIR}")
-      elseif ("${runall}" MATCHES "<use-persisted>")
-        set(nudge_arg "<use-persisted>")
-      elseif ("${runall}" MATCHES "<client")
-        string(REGEX MATCHALL "<client_nudge[^>]+" nudge_arg "${runall}")
-        string(REGEX REPLACE "<client_nudge" "-client" nudge_arg "${nudge_arg}")
-        string(REGEX REPLACE " " "\\\\;" nudge_arg "${nudge_arg}")
-      endif ()
-    endif (UNIX)
-    # FIXME i#120: for Windows, get app name to run from .runall file.
-    # For linux we always run linux.infloop.
-    get_target_path_for_execution(infloop_path linux.infloop)
-    set(cmd_with_at ${rundr} ${infloop_path} ${exe_ops})
+    else ()
+      set(pidfile "${CMAKE_CURRENT_BINARY_DIR}/${key}-pid")
+      # We want the pid not of drrun but its child so we pass -pidfile to drrun rather
+      # than -pid to run_in_bg.
+      # XXX i#120: add systemwide injection testing.  For now we only test drrun.
+      string(REGEX REPLACE "^(.*)/drrun.exe"
+        "\\1/run_in_bg;-out;${tmpfile};\\1/drrun.exe;-pidfile;${pidfile}"
+        rundr "${rundr}")
+    endif ()
+    # Remove timeout args to drrun to ensure that it doesn't fork the app.
+    string(REGEX REPLACE ";-s;[0-9]+" "" rundr "${rundr}")
+    string(REGEX REPLACE ";-killpg" "" rundr "${rundr}")
+    set(clear_arg "")
+    if ("${runall}" MATCHES "<reset>")
+      set(nudge_arg "-type\;reset")
+    elseif ("${runall}" MATCHES "<freeze>")
+      set(nudge_arg "-type\;freeze")
+    elseif ("${runall}" MATCHES "<persist>")
+      set(nudge_arg "-type\;persist")
+      # clear out pcache dir prior to run
+      set(clear_arg "${PCACHE_SHARED_DIR}")
+    elseif ("${runall}" MATCHES "<use-persisted>")
+      set(nudge_arg "<use-persisted>")
+    elseif ("${runall}" MATCHES "<client")
+      string(REGEX MATCHALL "<client_nudge[^>]+" nudge_arg "${runall}")
+      string(REGEX REPLACE "<client_nudge" "-client" nudge_arg "${nudge_arg}")
+      string(REGEX REPLACE " " "\\\\;" nudge_arg "${nudge_arg}")
+    endif ()
+    set(cmd_with_at ${rundr} ${app_path} ${exe_ops})
     # we pass intra-arg spaces via @@ and inter-arg via @ and ; via !
     # to get around the pain of trying to quote everything just right:
     # much simpler this way.
     string(REGEX REPLACE " " "@@" cmd_with_at "${cmd_with_at}")
     string(REGEX REPLACE ";" "@" cmd_with_at "${cmd_with_at}")
     if (NOT APPLE) # XXX i#1286: implement nudge for MacOS
-      get_target_path_for_execution(toolbindir nudgeunix)
+      if (UNIX)
+        get_target_path_for_execution(toolbindir nudgeunix)
+      else ()
+        get_target_path_for_execution(toolbindir drconfig)
+      endif ()
     else ()
       set(toolbindir "i#1286-not-implemented-for-Mac")
     endif ()
     get_filename_component(toolbindir ${toolbindir} DIRECTORY)
     add_test(${test} ${CMAKE_COMMAND} -D toolbindir=${toolbindir}
       -D cmd=${cmd_with_at} -D out=${tmpfile} -D nudge=${nudge_arg}
-      -D clear=${clear_arg}
+      -D clear=${clear_arg} -D pidfile=${pidfile}
       -P ${CMAKE_CURRENT_SOURCE_DIR}/runall.cmake)
   elseif (is_runcmp)
     # Some apps have enough output that ctest runs out of memory when
@@ -1934,7 +1952,9 @@ if (CLIENT_INTERFACE)
     #tobuild_ci_runall(client.custom_traces client-interface/custom_traces.runall
     #  "" "" "")
     #tobuild_ci_runall(client.decode-bb client-interface/decode-bb.runall "" "" "")
-    #tobuild_ci_runall(client.nudge_test client-interface/nudge_test.runall "" "" "")
+    # I reworked Windows .runall support to run the new win32.infloop but have not yet
+    # looked at other tests:
+    tobuild_ci(client.nudge_test client-interface/nudge_test.runall "" "" "")
     #tobuild_ci_runall(client.pc-check client-interface/pc-check.runall "" "" "")
     if (NOT X64)
       # FIXME i#16, does not work in Win-X64 because of inline assembly code.
@@ -3121,6 +3141,7 @@ if (UNIX)
   # when running tests in parallel: have to generate pcaches first
   set(linux.persist-use_FLAKY_depends linux.persist_FLAKY)
 else (UNIX)
+  add_exe(win32.infloop win32/infloop.c)
   if (VPS)
     # too flaky across platforms so we limit to VPS only: not too useful
     # for other than testing our ASLR anyway
diff --git a/suite/tests/client-interface/nudge_test.template b/suite/tests/client-interface/nudge_test.template
index d3dec6cb84..a947792c20 100644
--- a/suite/tests/client-interface/nudge_test.template
+++ b/suite/tests/client-interface/nudge_test.template
@@ -1,38 +1,4 @@
 thank you for testing the client interface
-#if defined(RUNREGRESSION_XP) && defined(PROGRAM_SHEPHERDING) && defined(security) && defined(no_executable_if_rx_text) && !defined(executable_after_load)
-  SEC_VIO_STOP
-No such process found.
-# if defined(WINDOWS)
-Window not found: Untitled - Notepad
-# endif
-#else
-# if defined(WINDOWS)
-#  if defined(DEBUG)
-Process notepad.exe, running SC debug
-#  else
-#   if defined(PROFILE)
-Process notepad.exe, running SC profile
-#   else
-Process notepad.exe, running SC release
-#   endif
-#  endif
-# endif
 nudge delivered 10
 nudge delivered 11
-# if defined(WINDOWS)
-#  if defined(DEBUG)
-Process notepad.exe, running SC debug
-#  else
-#   if defined(PROFILE)
-Process notepad.exe, running SC profile
-#   else
-Process notepad.exe, running SC release
-#   endif
-#  endif
-Close message sent.
-# endif
 done
-#endif
-#if defined(WINDOWS)
-No such process found.
-#endif
diff --git a/suite/tests/runall.cmake b/suite/tests/runall.cmake
index d08bbedcd1..9a7701077c 100644
--- a/suite/tests/runall.cmake
+++ b/suite/tests/runall.cmake
@@ -1,4 +1,5 @@
 # **********************************************************
+# Copyright (c) 2018 Google, Inc.    All rights reserved.
 # Copyright (c) 2009-2010 VMware, Inc.    All rights reserved.
 # **********************************************************
 
@@ -38,7 +39,8 @@
 #     should have intra-arg space=@@ and inter-arg space=@ and ;=!
 # * toolbindir
 # * out = file where output of background process will be sent
-# * nudge = arguments to nudgeunix
+# * pidfile = file where the pid of the background process will be written
+# * nudge = arguments to nudgeunix or drconfig
 # * clear = dir to clear ahead of time
 
 # intra-arg space=@@ and inter-arg space=@
@@ -57,7 +59,13 @@ if (NOT "${clear}" STREQUAL "")
   endif ()
 endif ()
 
-# run in the background
+# Remove any stale files.
+if (pidfile)
+  file(REMOVE ${pidfile})
+endif ()
+file(REMOVE ${out})
+
+# Run the target in the background.
 execute_process(COMMAND ${cmd}
   RESULT_VARIABLE cmd_result
   ERROR_VARIABLE cmd_err
@@ -70,23 +78,46 @@ if (UNIX)
   find_program(SLEEP "sleep")
   if (NOT SLEEP)
     message(FATAL_ERROR "cannot find 'sleep'")
-  endif (NOT SLEEP)
+  endif ()
+  set(nudge_cmd nudgeunix)
 else (UNIX)
-  message(FATAL_ERROR "need a sleep on Windows")
-  # FIXME i#120: add tools/sleep.exe?
-  # or use perl?  trying to get away from perl though
+  # We use "ping" on Windows to "sleep" :)
+  find_program(PING "ping")
+  if (NOT PING)
+    message(FATAL_ERROR "cannot find 'ping'")
+  endif ()
+  set(nudge_cmd drconfig)
 endif (UNIX)
 
+function (do_sleep ms)
+  if (UNIX)
+    execute_process(COMMAND "${SLEEP}" ${ms})
+  else ()
+    # XXX: ping's units are secs.  For now we always do 1 sec.
+    # We could try to use cygwin bash or perl.
+    execute_process(COMMAND ${PING} 127.0.0.1 -n 1 OUTPUT_QUIET)
+  endif ()
+endfunction (do_sleep)
+
+if (pidfile)
+  while (NOT EXISTS "${pidfile}")
+    do_sleep(0.1)
+  endwhile ()
+  file(READ "${pidfile}" pid)
+  string(REGEX REPLACE "\n" "" pid ${pid})
+endif ()
+
 while (NOT EXISTS "${out}")
-  execute_process(COMMAND "${SLEEP}" 0.1)
+  do_sleep(0.1)
 endwhile ()
 file(READ "${out}" output)
 # we require that all runall tests write at least one line up front
 while (NOT "${output}" MATCHES "\n")
-  execute_process(COMMAND "${SLEEP}" 0.1)
+  do_sleep(0.1)
   file(READ "${out}" output)
 endwhile()
 
+set(orig_nudge "${nudge}")
 if ("${nudge}" MATCHES "<use-persisted>")
   # ensure using pcaches, instead of nudging
   file(READ "/proc/${pid}/maps" maps)
@@ -94,7 +125,14 @@ if ("${nudge}" MATCHES "<use-persisted>")
     set(fail_msg "no .dpc files found in ${maps}: not using pcaches!")
   endif ()
 else ()
-  execute_process(COMMAND "${toolbindir}/nudgeunix" -pid ${pid} ${nudge}
+  # nudgeunix and drconfig have different syntax:
+  if (WIN32)
+    # XXX i#120: expand beyond -client.
+    string(REGEX REPLACE "-client" "-nudge_pid;${pid}" nudge "${nudge}")
+  else ()
+    set(nudge "-pid;${pid};${nudge}")
+  endif ()
+  execute_process(COMMAND "${toolbindir}/${nudge_cmd}" ${nudge}
     RESULT_VARIABLE nudge_result
     ERROR_VARIABLE nudge_err
     OUTPUT_VARIABLE nudge_out
@@ -102,17 +140,17 @@ else ()
   # combine out and err
   set(nudge_err "${nudge_out}${nudge_err}")
   if (nudge_result)
-    message(FATAL_ERROR "*** nudgeunix failed (${nudge_result}): ${nudge_err}***\n")
+    message(FATAL_ERROR "*** ${nudge_cmd} failed (${nudge_result}): ${nudge_err}***\n")
   endif (nudge_result)
 endif ()
 
-if ("${nudge}" MATCHES "-client")
+if ("${orig_nudge}" MATCHES "-client")
   # wait for more output to file
   string(LENGTH "${output}" prev_outlen)
   file(READ "${out}" output)
   string(LENGTH "${output}" new_outlen)
   while (NOT ${new_outlen} GREATER ${prev_outlen})
-    execute_process(COMMAND "${SLEEP}" 0.1)
+    do_sleep(0.1)
     file(READ "${out}" output)
     string(LENGTH "${output}" new_outlen)
   endwhile()
@@ -120,7 +158,7 @@ else ()
   # for reset or other DR tests there won't be further output
   # so we have to guess how long to wait.
   # FIXME: should we instead turn on stderr_mask?
-  execute_process(COMMAND "${SLEEP}" 0.5)
+  do_sleep(0.5)
 endif ()
 
 if (UNIX)
@@ -139,7 +177,16 @@ if (UNIX)
     message(FATAL_ERROR "*** kill failed (${kill_result}): ${kill_err}***\n")
   endif (kill_result)
 else (UNIX)
-  # FIXME i#120: for Windows, use ${toolbindir}/DRkill.exe
+  # win32.infloop has a title with the pid in it so we can uniquely target it
+  # for a cleaner exit than using drkill.
+  execute_process(COMMAND "${toolbindir}/closewnd.exe" "Infloop pid=${pid}" 10
+    RESULT_VARIABLE kill_result
+    ERROR_VARIABLE kill_err
+    OUTPUT_VARIABLE kill_out)
+  set(kill_err "${kill_out}${kill_err}")
+  if (kill_result)
+    message(FATAL_ERROR "*** kill failed (${kill_result}): ${kill_err}***\n")
+  endif (kill_result)
 endif (UNIX)
 
 if (NOT "${fail_msg}" STREQUAL "")
@@ -149,7 +196,7 @@ endif ()
 # we require that test print "done" as last line once done
 file(READ "${out}" output)
 while (NOT "${output}" MATCHES "\ndone\n")
-  execute_process(COMMAND "${SLEEP}" 0.1)
+  do_sleep(0.1)
   file(READ "${out}" output)
 endwhile()
 
@@ -160,6 +207,8 @@ message("${output}")
 if (UNIX)
   # sometimes infloop keeps running: FIXME: figure out why
   execute_process(COMMAND "${KILL}" -9 ${pid} ERROR_QUIET OUTPUT_QUIET)
-  find_program(PKILL "pkill")
   # we can't run pkill b/c there are other tests running infloop (i#1341)
-endif (UNIX)
+else ()
+  # We could run "${toolbindir}/DRkill.exe" -pid ${pid} but we shouldn't need to
+  # as the app itself has a timeout.
+endif ()
diff --git a/suite/tests/win32/infloop.c b/suite/tests/win32/infloop.c
new file mode 100644
index 0000000000..46d2c40e78
--- /dev/null
+++ b/suite/tests/win32/infloop.c
@@ -0,0 +1,59 @@
+/* **********************************************************
+ * Copyright (c) 2018 Google, Inc.  All rights reserved.
+ * **********************************************************/
+
+/*
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of Google, Inc. nor the names of its contributors may be
+ *   used to endorse or promote products derived from this software without
+ *   specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
+ * DAMAGE.
+ */
+
+/* An app that stays up long enough for testing nudges. */
+#include "tools.h"
+#include <windows.h>
+
+/* Timeout to avoid leaving stale processes in case something goes wrong. */
+static VOID CALLBACK
+TimerProc(HWND hwnd, UINT msg, UINT_PTR id, DWORD time)
+{
+    print("timed out\n");
+    ExitProcess(1);
+}
+
+int
+main(int argc, const char *argv[])
+{
+    /* We put the pid into the title so that tools/closewnd can target it
+     * uniquely when run in a parallel test suite.
+     * runall.cmake assumes this precise title.
+     */
+    char title[64];
+    _snprintf_s(title, BUFFER_SIZE_BYTES(title), BUFFER_SIZE_ELEMENTS(title),
+                "Infloop pid=%d", GetProcessId(GetCurrentProcess()));
+    SetTimer(NULL, NULL, 180*1000/*3 mins*/, TimerProc);
+    MessageBoxA(NULL, "DynamoRIO test: will be auto-closed", title, MB_OK);
+    print("done\n");
+    return 0;
+}
diff --git a/tools/drdeploy.c b/tools/drdeploy.c
index 6898f67191..8a1e60126b 100644
--- a/tools/drdeploy.c
+++ b/tools/drdeploy.c
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2011-2017 Google, Inc.  All rights reserved.
+ * Copyright (c) 2011-2018 Google, Inc.  All rights reserved.
  * Copyright (c) 2008-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -1597,16 +1597,15 @@ _tmain(int argc, TCHAR *targv[])
                 printf("process %d is not running under DR\n", nudge_pid);
             if (res != DR_SUCCESS && res != DR_NUDGE_TIMEOUT) {
                 count = 0;
-                res = ERROR_SUCCESS;
             }
         } else
             res = dr_nudge_process(process, nudge_id, nudge_arg, nudge_timeout, &count);
 
         printf("%d processes nudged\n", count);
         if (res == DR_NUDGE_TIMEOUT)
-            printf("timed out waiting for nudge to complete");
+            printf("timed out waiting for nudge to complete\n");
         else if (res != DR_SUCCESS)
-            printf("nudge operation failed, verify adequate permissions for this operation.");
+            printf("nudge operation failed, verify permissions and parameters.\n");
     }
 #  ifdef WINDOWS
     /* FIXME i#840: Process iterator NYI for Linux. */
