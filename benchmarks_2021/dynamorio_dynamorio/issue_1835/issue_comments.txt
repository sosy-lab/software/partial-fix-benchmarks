PC-relative operand support on ARM broken
One problem is that rel-addr instrs won't show up as reading the PC b/c they have no explicit PC opnd.
I guess we just document it, b/c we're not decoding into rel-addr: only synthetic tool instrs will be marked that way and we'll assume the tool can explicitly deal with them, even if they're added as app instrs.
