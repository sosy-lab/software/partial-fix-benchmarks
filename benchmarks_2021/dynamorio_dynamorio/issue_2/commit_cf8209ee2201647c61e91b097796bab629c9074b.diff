diff --git a/clients/drcachesim/simulator/cache_simulator.cpp b/clients/drcachesim/simulator/cache_simulator.cpp
index efae28dc16..8840dd42b4 100644
--- a/clients/drcachesim/simulator/cache_simulator.cpp
+++ b/clients/drcachesim/simulator/cache_simulator.cpp
@@ -126,8 +126,11 @@ cache_simulator_t::cache_simulator_t(unsigned int num_cores,
         return;
     }
 
+    bool warmup_enabled = ((warmup_refs > 0) || (warmup_fraction > 0.0));
+
     if (!llcache->init(knob_LL_assoc, (int)knob_line_size,
-                       (int)knob_LL_size, NULL, new cache_stats_t(knob_LL_miss_file))) {
+                       (int)knob_LL_size, NULL,
+                       new cache_stats_t(knob_LL_miss_file, warmup_enabled))) {
         ERRMSG("Usage error: failed to initialize LL cache.  Ensure sizes and "
                "associativity are powers of 2, that the total size is a multiple "
                "of the line size, and that any miss file path is writable.\n");
@@ -150,9 +153,11 @@ cache_simulator_t::cache_simulator_t(unsigned int num_cores,
         }
 
         if (!icaches[i]->init(knob_L1I_assoc, (int)knob_line_size,
-                              (int)knob_L1I_size, llcache, new cache_stats_t) ||
+                              (int)knob_L1I_size, llcache,
+                              new cache_stats_t("", warmup_enabled)) ||
             !dcaches[i]->init(knob_L1D_assoc, (int)knob_line_size,
-                              (int)knob_L1D_size, llcache, new cache_stats_t,
+                              (int)knob_L1D_size, llcache,
+                              new cache_stats_t("", warmup_enabled),
                               data_prefetcher == PREFETCH_POLICY_NEXTLINE ?
                               new prefetcher_t((int)knob_line_size) : nullptr)) {
             ERRMSG("Usage error: failed to initialize L1 caches.  Ensure sizes and "
diff --git a/clients/drcachesim/simulator/cache_stats.cpp b/clients/drcachesim/simulator/cache_stats.cpp
index 12a5ac5403..ff882c0804 100644
--- a/clients/drcachesim/simulator/cache_stats.cpp
+++ b/clients/drcachesim/simulator/cache_stats.cpp
@@ -34,8 +34,8 @@
 #include <iomanip>
 #include "cache_stats.h"
 
-cache_stats_t::cache_stats_t(const std::string &miss_file) :
-    caching_device_stats_t(miss_file),
+cache_stats_t::cache_stats_t(const std::string &miss_file, bool warmup_enabled) :
+    caching_device_stats_t(miss_file, warmup_enabled),
     num_flushes(0), num_prefetch_hits(0), num_prefetch_misses(0)
 {
 }
diff --git a/clients/drcachesim/simulator/cache_stats.h b/clients/drcachesim/simulator/cache_stats.h
index 2beee55ec9..04911f5fdc 100644
--- a/clients/drcachesim/simulator/cache_stats.h
+++ b/clients/drcachesim/simulator/cache_stats.h
@@ -42,7 +42,8 @@
 class cache_stats_t : public caching_device_stats_t
 {
  public:
-    explicit cache_stats_t(const std::string &miss_file = "");
+    explicit cache_stats_t(const std::string &miss_file = "",
+                           bool warmup_enabled = false);
 
     // In addition to caching_device_stats_t::access,
     // cache_stats_t::access processes prefetching requests.
diff --git a/clients/drcachesim/simulator/caching_device_stats.cpp b/clients/drcachesim/simulator/caching_device_stats.cpp
index 170aa7df0f..7b7ad8b4cf 100644
--- a/clients/drcachesim/simulator/caching_device_stats.cpp
+++ b/clients/drcachesim/simulator/caching_device_stats.cpp
@@ -35,8 +35,11 @@
 #include <iomanip>
 #include "caching_device_stats.h"
 
-caching_device_stats_t::caching_device_stats_t(const std::string &miss_file) :
-    success(true), num_hits(0), num_misses(0), num_child_hits(0), file(nullptr)
+caching_device_stats_t::caching_device_stats_t(const std::string &miss_file,
+                                               bool warmup_enabled) :
+    success(true), num_hits(0), num_misses(0), num_child_hits(0),
+    num_hits_at_reset(0), num_misses_at_reset(0), num_child_hits_at_reset(0),
+    warmup_enabled(warmup_enabled), file(nullptr)
 {
     if (miss_file.empty()) {
         dump_misses = false;
@@ -107,6 +110,15 @@ caching_device_stats_t::dump_miss(const memref_t &memref)
 #endif
 }
 
+void
+caching_device_stats_t::print_warmup(std::string prefix)
+{
+    std::cerr << prefix << std::setw(18) << std::left << "Warmup hits:" <<
+        std::setw(20) << std::right << num_hits_at_reset << std::endl;
+    std::cerr << prefix << std::setw(18) << std::left << "Warmup misses:" <<
+        std::setw(20) << std::right << num_misses_at_reset << std::endl;
+}
+
 void
 caching_device_stats_t::print_counts(std::string prefix)
 {
@@ -146,6 +158,9 @@ void
 caching_device_stats_t::print_stats(std::string prefix)
 {
     std::cerr.imbue(std::locale("")); // Add commas, at least for my locale
+    if (warmup_enabled) {
+        print_warmup(prefix);
+    }
     print_counts(prefix);
     print_rates(prefix);
     print_child_stats(prefix);
@@ -155,6 +170,9 @@ caching_device_stats_t::print_stats(std::string prefix)
 void
 caching_device_stats_t::reset()
 {
+    num_hits_at_reset = num_hits;
+    num_misses_at_reset = num_misses;
+    num_child_hits_at_reset = num_child_hits;
     num_hits = 0;
     num_misses = 0;
     num_child_hits = 0;
diff --git a/clients/drcachesim/simulator/caching_device_stats.h b/clients/drcachesim/simulator/caching_device_stats.h
index 84f3f4cf30..690ea401a2 100644
--- a/clients/drcachesim/simulator/caching_device_stats.h
+++ b/clients/drcachesim/simulator/caching_device_stats.h
@@ -46,7 +46,8 @@
 class caching_device_stats_t
 {
  public:
-    explicit caching_device_stats_t(const std::string &miss_file);
+    explicit caching_device_stats_t(const std::string &miss_file,
+                                    bool warmup_enabled = false);
     virtual ~caching_device_stats_t();
 
     // Called on each access.
@@ -67,6 +68,7 @@ class caching_device_stats_t
     bool success;
 
     // print different groups of information, beneficial for code reuse
+    virtual void print_warmup(std::string prefix);
     virtual void print_counts(std::string prefix); // hit/miss numbers
     virtual void print_rates(std::string prefix); // hit/miss rates
     virtual void print_child_stats(std::string prefix); // child/total info
@@ -77,6 +79,14 @@ class caching_device_stats_t
     int_least64_t num_misses;
     int_least64_t num_child_hits;
 
+    // Stats saved when the last reset was called. This helps us get insight
+    // into what the stats were when the cache was warmed up.
+    int_least64_t num_hits_at_reset;
+    int_least64_t num_misses_at_reset;
+    int_least64_t num_child_hits_at_reset;
+    // Enabled if options warmup_refs > 0 || warmup_fraction > 0
+    bool warmup_enabled;
+
     // We provide a feature of dumping misses to a file.
     bool dump_misses;
 #ifdef HAS_ZLIB
diff --git a/clients/drcachesim/tests/warmup-valid.templatex b/clients/drcachesim/tests/warmup-valid.templatex
new file mode 100644
index 0000000000..a231e78b32
--- /dev/null
+++ b/clients/drcachesim/tests/warmup-valid.templatex
@@ -0,0 +1,27 @@
+Hello, world!
+---- <application exited with code 0> ----
+Cache simulation results:
+Core #0 \(1 thread\(s\)\)
+  L1I stats:
+    Warmup hits:                  *[0-9,\.]*..
+    Warmup misses:                *[0-9,\.]*..
+    Hits:                         *[0-9,\.]*....
+    Misses:                       *[0-9,\.]*..
+.*    Miss rate:                        [0-1][,\.]..%
+  L1D stats:
+    Warmup hits:                  *[0-9,\.]*..
+    Warmup misses:                *[0-9,\.]*..
+    Hits:                         *[0-9,\.]*....
+    Misses:                       *[0-9,\.]*...
+.*   Miss rate:                        [0-9][,\.]..%
+Core #1 \(0 thread\(s\)\)
+Core #2 \(0 thread\(s\)\)
+Core #3 \(0 thread\(s\)\)
+LL stats:
+    Warmup hits:                  *[0-9,\.]*..
+    Warmup misses:                *[0-9,\.]*..
+    Hits:                         *[0-9,\.]*..
+    Misses:                       *[0-9,\.]*...
+.*   Local miss rate:                 [0-9].[,\.]..%
+    Child hits:                   *[0-9,\.]*.....
+    Total miss rate:                  [0-3][,\.]..%
diff --git a/clients/drcachesim/tests/warmup-zeros.templatex b/clients/drcachesim/tests/warmup-zeros.templatex
new file mode 100644
index 0000000000..a1cbb2ecea
--- /dev/null
+++ b/clients/drcachesim/tests/warmup-zeros.templatex
@@ -0,0 +1,27 @@
+Hello, world!
+---- <application exited with code 0> ----
+Cache simulation results:
+Core #0 \(1 thread\(s\)\)
+  L1I stats:
+    Warmup hits:                  *[0]*..
+    Warmup misses:                *[0]*..
+    Hits:                         *[0-9,\.]*....
+    Misses:                       *[0-9,\.]*..
+.*    Miss rate:                        [0-1][,\.]..%
+  L1D stats:
+    Warmup hits:                  *[0]*..
+    Warmup misses:                *[0]*..
+    Hits:                         *[0-9,\.]*....
+    Misses:                       *[0-9,\.]*...
+.*   Miss rate:                        [0-9][,\.]..%
+Core #1 \(0 thread\(s\)\)
+Core #2 \(0 thread\(s\)\)
+Core #3 \(0 thread\(s\)\)
+LL stats:
+    Warmup hits:                  *[0]*..
+    Warmup misses:                *[0]*..
+    Hits:                         *[0-9,\.]*..
+    Misses:                       *[0-9,\.]*...
+.*   Local miss rate:                 [0-9].[,\.]..%
+    Child hits:                   *[0-9,\.]*.....
+    Total miss rate:                  [0-3][,\.]..%
diff --git a/suite/tests/CMakeLists.txt b/suite/tests/CMakeLists.txt
index 33667efc8c..8948d3bfa9 100644
--- a/suite/tests/CMakeLists.txt
+++ b/suite/tests/CMakeLists.txt
@@ -2450,6 +2450,12 @@ if (CLIENT_INTERFACE)
       torunonly_drcachesim(delay-simple ${ci_shared_app}
         "-trace_after_instrs 50000 -exit_after_tracing 10000" "")
 
+      # Test that "Warmup hits" and "Warmup misses" are printed out
+      torunonly_drcachesim(warmup-valid ${ci_shared_app} "-warmup_refs 1" "")
+
+      # Test that warmup was enabled but not triggered.
+      torunonly_drcachesim(warmup-zeros ${ci_shared_app} "-warmup_refs 1000000000" "")
+
       # FIXME i#1799: clang does not support "asm goto" used in annotation
       # FIXME i#1551, i#1569: get working on ARM/AArch64
       if (NOT ARM AND NOT AARCH64 AND NOT CMAKE_COMPILER_IS_CLANG)
