i#58: properly parse the executable path on recent OSX kernels (#2266)

The OSX kernel used to place the bare executable path above envp.
On recent XNU versions, the kernel now prefixes the executable path
with the string executable_path= so it can be parsed getenv style.
Use (st_shndx == 0) instead of (st_value == 0) when distinguishing imports.
Imports can have an st_value != 0, specially on Aarch64.

Fixes issue #2272
Fixed the logic used to detect imports on ext/drsyms/drsyms_elf.c.
Now we use the same logic as in core/unix/module_elf.c symbol_is_import.

Fixes issue #2272
Fixed the logic used to detect imports on ext/drsyms/drsyms_elf.c.
Now we use the same logic as in core/unix/module_elf.c symbol_is_import.

Fixes issue #2272
i#2272: drsyms uses the wrong criteria to distinguish imports on aarch64 (#2280)

Fixes the logic used to detect imports in ext/drsyms/drsyms_elf.c.
Now we use the same logic as in core/unix/module_elf.c symbol_is_import.

Fixes #2272
