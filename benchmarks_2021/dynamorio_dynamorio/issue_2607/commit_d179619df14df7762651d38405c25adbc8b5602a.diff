diff --git a/core/fcache.c b/core/fcache.c
index 991a78796f..ec2f9c9b60 100644
--- a/core/fcache.c
+++ b/core/fcache.c
@@ -1522,28 +1522,19 @@ fcache_free_unit(dcontext_t *dcontext, fcache_unit_t *unit, bool dealloc_or_reus
     }
 }
 
-/* assuming size will either be aligned at VM_ALLOCATION_BOUNDARY or
- * smaller where no adjustment is necessary
+/* We do not consider guard pages in our sizing, since the VMM no longer uses
+ * larger-than-page block sizing (i#2607, i#4424).  Guards will be added on top.
  */
-#define FCACHE_GUARDED(size)                                                             \
-    ((size) -                                                                            \
-     ((DYNAMO_OPTION(guard_pages) && ((size) >= VM_ALLOCATION_BOUNDARY - 2 * PAGE_SIZE)) \
-          ? (2 * PAGE_SIZE)                                                              \
-          : 0))
-
-#define SET_CACHE_PARAMS(cache, which)                                                  \
-    do {                                                                                \
-        cache->max_size = FCACHE_GUARDED(FCACHE_OPTION(cache_##which##_max));           \
-        cache->max_unit_size = FCACHE_GUARDED(FCACHE_OPTION(cache_##which##_unit_max)); \
-        cache->max_quadrupled_unit_size =                                               \
-            FCACHE_GUARDED(FCACHE_OPTION(cache_##which##_unit_quadruple));              \
-        cache->free_upgrade_size =                                                      \
-            FCACHE_GUARDED(FCACHE_OPTION(cache_##which##_unit_upgrade));                \
-        cache->init_unit_size =                                                         \
-            FCACHE_GUARDED(FCACHE_OPTION(cache_##which##_unit_init));                   \
-        cache->finite_cache = dynamo_options.finite_##which##_cache;                    \
-        cache->regen_param = dynamo_options.cache_##which##_regen;                      \
-        cache->replace_param = dynamo_options.cache_##which##_replace;                  \
+#define SET_CACHE_PARAMS(cache, which)                                                   \
+    do {                                                                                 \
+        cache->max_size = FCACHE_OPTION(cache_##which##_max);                            \
+        cache->max_unit_size = FCACHE_OPTION(cache_##which##_unit_max);                  \
+        cache->max_quadrupled_unit_size = FCACHE_OPTION(cache_##which##_unit_quadruple); \
+        cache->free_upgrade_size = FCACHE_OPTION(cache_##which##_unit_upgrade);          \
+        cache->init_unit_size = FCACHE_OPTION(cache_##which##_unit_init);                \
+        cache->finite_cache = dynamo_options.finite_##which##_cache;                     \
+        cache->regen_param = dynamo_options.cache_##which##_regen;                       \
+        cache->replace_param = dynamo_options.cache_##which##_replace;                   \
     } while (0);
 
 static fcache_t *
diff --git a/core/heap.c b/core/heap.c
index ead694f58f..1a67a1e1cf 100644
--- a/core/heap.c
+++ b/core/heap.c
@@ -166,31 +166,11 @@ DECLARE_NEVERPROT_VAR(static bool out_of_vmheap_once, false);
 #define MEMSET_HEADER(p, value) VARIABLE_SIZE(p) = HEAP_TO_PTR_UINT(value)
 #define GET_VARIABLE_ALLOCATION_SIZE(p) (VARIABLE_SIZE(p) + HEADER_SIZE)
 
-/* heap is allocated in units
- * we start out with a small unit, then each additional unit we
- * need doubles in size, up to a maximum, we default to 32kb initial size
- * (24kb useful with guard pages), max size defaults to 64kb (56kb useful with
- * guard pages), we keep the max small to save memory, it doesn't seem to be
- * perf hit! Though with guard pages we are wasting quite a bit of reserved
- * (though not committed) space */
-/* the only big things global heap is used for are pc sampling
- * hash table and sideline sampling hash table -- if none of those
- * are in use, 16KB should be plenty, we default to 32kb since guard
- * pages are on by default (gives 24kb useful) max size is same as for
- * normal heap units.
- */
-/* the old defaults were 32kb (usable) for initial thread private and 16kb
- * (usable) for initial global, changed to simplify the logic for allocating
- * in multiples of the os allocation granularity.  The new defaults prob.
- * make more sense with the shared cache then the old did anyways.
- */
-/* restrictions -
- * any guard pages are included in the size, size must be > UNITOVERHEAD
- * for best performance sizes should be of the form
- * 2^n * page_size (where n is a positve integer) and max should be a multiple
- * of the os allocation granularity so that with enough doublings we are
- * reserving memory in multiples of the allocation granularity and not wasting
- * any virtual address space (beyond our guard pages)
+/* The heap is allocated in units.
+ * We start out with a small unit. Then each additional unit we
+ * need doubles in size, up to a maximum.
+ * We keep the initial units small for thread-private heaps, since with
+ * thousands of threads the space can add up.
  */
 #define HEAP_UNIT_MIN_SIZE DYNAMO_OPTION(initial_heap_unit_size)
 #define HEAP_UNIT_MAX_SIZE INTERNAL_OPTION(max_heap_unit_size)
@@ -211,12 +191,12 @@ DECLARE_NEVERPROT_VAR(static bool out_of_vmheap_once, false);
 #define UNIT_COMMIT_END(u) (u->end_pc)
 #define UNIT_RESERVED_END(u) (u->reserved_end_pc)
 
-/* gets the allocated size of the unit (reserved size + guard pages) */
-#define UNITALLOC(u) (UNIT_RESERVED_SIZE(u) + GUARD_PAGE_ADJUSTMENT)
-/* gets unit overhead, includes the reserved (guard pages) and committed
- * (sizeof(heap_unit_t)) portions
+/* Gets the allocated size of the unit (reserved size; doesn't include guard pages
+ * as those are not considered part of the usable space).
  */
-#define UNITOVERHEAD (sizeof(heap_unit_t) + GUARD_PAGE_ADJUSTMENT)
+#define UNITALLOC(u) (UNIT_RESERVED_SIZE(u))
+/* Gets unit overhead: includes reserved and committed (sizeof(heap_unit_t)) portions. */
+#define UNITOVERHEAD sizeof(heap_unit_t)
 
 /* any alloc request larger than this needs a special unit */
 #define MAXROOM (HEAP_UNIT_MAX_SIZE - UNITOVERHEAD)
@@ -875,7 +855,7 @@ vmm_place_vmcode(vm_heap_t *vmh, size_t size, heap_error_code_t *error_code)
                      get_random_offset(DYNAMO_OPTION(vm_max_offset) /
                                        DYNAMO_OPTION(vmm_block_size)) *
                          DYNAMO_OPTION(vmm_block_size));
-        preferred = ALIGN_FORWARD(preferred, DYNAMO_OPTION(vmm_block_size));
+        preferred = ALIGN_FORWARD(preferred, OS_ALLOC_GRANULARITY);
         /* overflow check: w/ vm_base shouldn't happen so debug-only check */
         ASSERT(!POINTER_OVERFLOW_ON_ADD(preferred, size));
         /* let's assume a single chunk is sufficient to reserve */
@@ -1907,7 +1887,7 @@ vmm_heap_alloc(size_t size, uint prot, heap_error_code_t *error_code, which_vmm_
 void
 vmm_heap_init()
 {
-    IF_WINDOWS(ASSERT(DYNAMO_OPTION(vmm_block_size) == OS_ALLOC_GRANULARITY));
+    IF_WINDOWS(ASSERT(ALIGNED(OS_ALLOC_GRANULARITY, DYNAMO_OPTION(vmm_block_size))));
 #ifdef X64
     /* add reachable regions before we allocate the heap, xref PR 215395 */
     /* i#774, i#901: we no longer need the DR library nor ntdll.dll to be
@@ -3812,8 +3792,7 @@ threadunits_init(dcontext_t *dcontext, thread_units_t *tu, size_t size, bool rea
     int i;
     DODEBUG({ tu->num_units = 0; });
     tu->which = VMM_HEAP | (reachable ? VMM_REACHABLE : 0);
-    tu->top_unit =
-        heap_create_unit(tu, size - GUARD_PAGE_ADJUSTMENT, false /*can reuse*/);
+    tu->top_unit = heap_create_unit(tu, size, false /*can reuse*/);
     tu->cur_unit = tu->top_unit;
     tu->dcontext = dcontext;
     tu->writable = true;
@@ -4363,11 +4342,7 @@ common_heap_alloc(thread_units_t *tu, size_t size HEAPACCT(which_heap_t which))
                             unit_size *= 2;
                         if (unit_size > HEAP_UNIT_MAX_SIZE)
                             unit_size = HEAP_UNIT_MAX_SIZE;
-                        /* size for heap_create_unit doesn't include any guard
-                         * pages */
                         ASSERT(unit_size > UNITOVERHEAD);
-                        ASSERT(unit_size > (size_t)GUARD_PAGE_ADJUSTMENT);
-                        unit_size -= GUARD_PAGE_ADJUSTMENT;
                         new_unit = heap_create_unit(tu, unit_size, false /*can reuse*/);
                         prev->next_local = new_unit;
 #ifdef DEBUG_MEMORY
@@ -4910,7 +4885,7 @@ typedef struct _special_heap_unit_t {
 #define SPECIAL_UNIT_COMMIT_SIZE(u) ((u)->end_pc - (u)->alloc_pc)
 #define SPECIAL_UNIT_RESERVED_SIZE(u) ((u)->reserved_end_pc - (u)->alloc_pc)
 #define SPECIAL_UNIT_HEADER_INLINE(u) ((u)->alloc_pc != (u)->start_pc)
-#define SPECIAL_UNIT_ALLOC_SIZE(u) (SPECIAL_UNIT_RESERVED_SIZE(u) + GUARD_PAGE_ADJUSTMENT)
+#define SPECIAL_UNIT_ALLOC_SIZE(u) (SPECIAL_UNIT_RESERVED_SIZE(u))
 
 /* the cfree list stores a next ptr and a count */
 typedef struct _cfree_header {
@@ -5143,18 +5118,11 @@ special_heap_init_internal(uint block_size, uint block_alignment, bool use_lock,
     if (block_alignment != 0)
         block_size = ALIGN_FORWARD(block_size, block_alignment);
     if (unit_size == 0) {
-        unit_size = (block_size * 16 > HEAP_UNIT_MIN_SIZE) ? (block_size * 16)
-                                                           : HEAP_UNIT_MIN_SIZE;
-        /* Whether using 16K or 64K vmm blocks, HEAP_UNIT_MIN_SIZE of 32K wastes
-         * space, and our main uses (stubs, whether global or coarse, and signal
-         * pending queue) don't need a lot of space, so shrinking.
-         * This tuning is a little fragile (just like for regular heap units and
-         * fcache units) so be careful when changing default parameters.
+        /* Our main uses (stubs, whether global or coarse, and signal
+         * pending queue) don't need a lot of space, so we have a smaller min size
+         * than regular heap units which use HEAP_UNIT_MIN_SIZE.
          */
-        if (unit_size == HEAP_UNIT_MIN_SIZE) {
-            ASSERT(unit_size > (size_t)GUARD_PAGE_ADJUSTMENT);
-            unit_size -= GUARD_PAGE_ADJUSTMENT;
-        }
+        unit_size = (block_size * 16 > PAGE_SIZE) ? (block_size * 16) : PAGE_SIZE;
     }
     if (heap_region == NULL) {
         unit_size = (size_t)ALIGN_FORWARD(unit_size, PAGE_SIZE);
diff --git a/core/heap.h b/core/heap.h
index 5a3490b6d0..4aecc0759d 100644
--- a/core/heap.h
+++ b/core/heap.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2010-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2010-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2001-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -351,7 +351,7 @@ global_unprotected_heap_free(void *p, size_t size HEAPACCT(which_heap_t which));
 #define NONPERSISTENT_HEAP_TYPE_FREE(dc, p, type, which) \
     NONPERSISTENT_HEAP_ARRAY_FREE(dc, p, type, 1, which)
 
-#define MIN_VMM_BLOCK_SIZE IF_WINDOWS_ELSE(16U * 1024, 4U * 1024)
+#define MIN_VMM_BLOCK_SIZE (4U * 1024)
 
 /* special heap of same-sized blocks that avoids global locks */
 void *
diff --git a/core/optionsx.h b/core/optionsx.h
index afac4ef6d4..009b9ad93a 100644
--- a/core/optionsx.h
+++ b/core/optionsx.h
@@ -323,8 +323,7 @@ OPTION_DEFAULT_INTERNAL(bool, heap_accounting_assert, true,
 
 #if defined(UNIX)
 OPTION_NAME_INTERNAL(bool, profile_pcs, "prof_pcs", "pc-sampling profiling")
-/* for default size 0, special_heap_init() will use initial_heap_unit_size instead */
-OPTION_DEFAULT_INTERNAL(uint_size, prof_pcs_heap_size, 0,
+OPTION_DEFAULT_INTERNAL(uint_size, prof_pcs_heap_size, 24 * 1024,
                         "special heap size for pc-sampling profiling")
 #else
 #    ifdef WINDOWS_PC_SAMPLE
@@ -1263,33 +1262,29 @@ OPTION_INTERNAL(bool, simulate_contention,
                 "simulate lock contention for testing purposes only")
 
 /* Virtual memory manager.
- * Our current default allocation unit matches the allocation granularity on
- * Windows, to avoid worrying about external fragmentation.
- * Since most of our allocations fall within this range this makes the
- * common operation be finding a single empty block.
- *
- * On Linux we save a lot of wasted alignment space by using a smaller
- * granularity (PR 415959, i#2575).
- *
- * XXX: for Windows, if we reserve the whole region up front and
- * just commit pieces, why do we need to match the Windows kernel
- * alloc granularity while within the region?
+ * We assume that our reservations cover 99% of the usage, and that we do not
+ * need to tune our sizes for standalone allocations where we would want 64K
+ * units for Windows.  If we exceed the reservations we'll end up with less
+ * efficient sizing, but that is worth the simpler and cleaner common case
+ * sizing.  We save a lot of wasted alignment space by using a smaller
+ * granularity (PR 415959, i#2575) and we avoid the complexity of adding guard
+ * pages while maintaining larger-than-page sizing (i#2607).
  *
  * vmm_block_size may be adjusted by adjust_defaults_for_page_size().
  */
-OPTION_DEFAULT(uint_size, vmm_block_size, (IF_WINDOWS_ELSE(64, 4) * 1024),
+OPTION_DEFAULT(uint_size, vmm_block_size, 4 * 1024,
                "allocation unit for virtual memory manager")
 /* initial_heap_unit_size may be adjusted by adjust_defaults_for_page_size(). */
-OPTION_DEFAULT(uint_size, initial_heap_unit_size, 32 * 1024,
+OPTION_DEFAULT(uint_size, initial_heap_unit_size, 24 * 1024,
                "initial private heap unit size")
 /* We avoid wasted space for every thread on UNIX for the
- * non-persistent heap which often stays under 12K (i#2575) (+8K for guards).
+ * non-persistent heap which often stays under 12K (i#2575).
  */
 /* initial_heap_nonpers_size may be adjusted by adjust_defaults_for_page_size(). */
-OPTION_DEFAULT(uint_size, initial_heap_nonpers_size, IF_WINDOWS_ELSE(32, 20) * 1024,
+OPTION_DEFAULT(uint_size, initial_heap_nonpers_size, IF_WINDOWS_ELSE(24, 12) * 1024,
                "initial private non-persistent heap unit size")
 /* initial_global_heap_unit_size may be adjusted by adjust_defaults_for_page_size(). */
-OPTION_DEFAULT(uint_size, initial_global_heap_unit_size, 32 * 1024,
+OPTION_DEFAULT(uint_size, initial_global_heap_unit_size, 24 * 1024,
                "initial global heap unit size")
 /* if this is too small then once past the vm reservation we have too many
  * DR areas and subsequent problems with DR areas and allmem synch (i#369)
@@ -1315,84 +1310,85 @@ OPTION_DEFAULT(uint_size, cache_commit_increment, 4 * 1024, "cache commit increm
  * private-configuration caches larger.  We could even get rid of the
  * fcache shifting.
  */
-/* Note that with guard pages any size of 64KB or bigger will get 8KB less than specified
- * to make sure we don't waste virtual memory in the address space
- */
 OPTION(uint_size, cache_bb_max, "max size of bb cache, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 /* for default configuration of all-shared we want a tiny bb cache for
  * our temp private bbs
  */
-/* x64 does not support resizing individual cache units so start at 64 */
-OPTION_DEFAULT(uint_size, cache_bb_unit_init, (IF_X64_ELSE(64, 4) * 1024),
+/* The 56K values below are to hit 64K with two 4K guard pages.
+ * We no longer need to hit 64K since VMM blocks are now 4K, but we keep the
+ * sizes to match historical values and to avoid i#4433.
+ */
+/* x64 does not support resizing individual cache units so start at the max. */
+OPTION_DEFAULT(uint_size, cache_bb_unit_init, (IF_X64_ELSE(56, 4) * 1024),
                "initial bb cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_bb_unit_max, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_bb_unit_max, (56 * 1024),
                "maximum bb cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 /* w/ init at 4, we quadruple to 16 and then to 64 */
-OPTION_DEFAULT(uint_size, cache_bb_unit_quadruple, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_bb_unit_quadruple, (56 * 1024),
                "bb cache units are grown by 4X until this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 
 OPTION(uint_size, cache_trace_max, "max size of trace cache, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-/* x64 does not support resizing individual cache units so start at 64 */
-OPTION_DEFAULT(uint_size, cache_trace_unit_init, (IF_X64_ELSE(64, 8) * 1024),
+/* x64 does not support resizing individual cache units so start at the max. */
+OPTION_DEFAULT(uint_size, cache_trace_unit_init, (IF_X64_ELSE(56, 8) * 1024),
                "initial trace cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_trace_unit_max, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_trace_unit_max, (56 * 1024),
                "maximum trace cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_trace_unit_quadruple, (IF_X64_ELSE(64, 32) * 1024),
+OPTION_DEFAULT(uint_size, cache_trace_unit_quadruple, (IF_X64_ELSE(56, 32) * 1024),
                "trace cache units are grown by 4X until this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 
 OPTION(uint_size, cache_shared_bb_max, "max size of shared bb cache, in KB or MB")
 /* override the default shared bb fragment cache size */
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_shared_bb_unit_init, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_shared_bb_unit_init, (56 * 1024),
                /* FIXME: cannot handle resizing of cache setting to unit_max, FIXME:
                   should be 32*1024 */
                "initial shared bb cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_shared_bb_unit_max, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_shared_bb_unit_max, (56 * 1024),
                "maximum shared bb cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 OPTION_DEFAULT(uint_size, cache_shared_bb_unit_quadruple,
-               (64 * 1024), /* FIXME: should be 32*1024 */
+               (56 * 1024), /* FIXME: should be 32*1024 */
                "shared bb cache units are grown by 4X until this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 
 OPTION(uint_size, cache_shared_trace_max, "max size of shared trace cache, in KB or MB")
 /* override the default shared trace fragment cache size */
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_shared_trace_unit_init, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_shared_trace_unit_init, (56 * 1024),
                /* FIXME: cannot handle resizing of cache setting to unit_max, FIXME:
                   should be 32*1024 */
                "initial shared trace cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_shared_trace_unit_max, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_shared_trace_unit_max, (56 * 1024),
                "maximum shared trace cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 OPTION_DEFAULT(uint_size, cache_shared_trace_unit_quadruple,
-               (64 * 1024), /* FIXME: should be 32*1024 */
+               (56 * 1024), /* FIXME: should be 32*1024 */
                "shared trace cache units are grown by 4X until this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 
 OPTION(uint_size, cache_coarse_bb_max, "max size of coarse bb cache, in KB or MB")
 /* override the default coarse bb fragment cache size */
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_init, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_init, (56 * 1024),
                /* FIXME: cannot handle resizing of cache setting to unit_max, FIXME:
                   should be 32*1024 */
                "initial coarse bb cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_max, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_max, (56 * 1024),
                "maximum coarse bb cache unit size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_quadruple,
-               (64 * 1024), /* FIXME: should be 32*1024 */
+               (56 * 1024), /* FIXME: should be 32*1024 */
                "coarse bb cache units are grown by 4X until this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 
@@ -1406,19 +1402,19 @@ OPTION_DEFAULT(bool, finite_shared_trace_cache, false,
                "adaptive working set shared trace cache management")
 OPTION_DEFAULT(bool, finite_coarse_bb_cache, false,
                "adaptive working set shared bb cache management")
-OPTION_DEFAULT(uint_size, cache_bb_unit_upgrade, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_bb_unit_upgrade, (56 * 1024),
                "bb cache units are always upgraded to this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_trace_unit_upgrade, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_trace_unit_upgrade, (56 * 1024),
                "trace cache units are always upgraded to this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_shared_bb_unit_upgrade, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_shared_bb_unit_upgrade, (56 * 1024),
                "shared bb cache units are always upgraded to this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_shared_trace_unit_upgrade, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_shared_trace_unit_upgrade, (56 * 1024),
                "shared trace cache units are always upgraded to this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
-OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_upgrade, (64 * 1024),
+OPTION_DEFAULT(uint_size, cache_coarse_bb_unit_upgrade, (56 * 1024),
                "shared coarse cache units are always upgraded to this size, in KB or MB")
 /* default size is in Kilobytes, Examples: 4, 4k, 4m, or 0 for unlimited */
 
