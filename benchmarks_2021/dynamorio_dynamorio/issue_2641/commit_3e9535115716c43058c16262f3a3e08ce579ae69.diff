diff --git a/core/os_shared.h b/core/os_shared.h
index 79f78d5a25..4d35525078 100644
--- a/core/os_shared.h
+++ b/core/os_shared.h
@@ -562,6 +562,7 @@ bool get_stack_bounds(dcontext_t *dcontext, byte **base, byte **top);
 
 bool is_readable_without_exception(const byte *pc, size_t size);
 bool is_readable_without_exception_query_os(byte *pc, size_t size);
+bool is_readable_without_exception_query_os_noblock(byte *pc, size_t size);
 bool safe_read(const void *base, size_t size, void *out_buf);
 bool safe_read_ex(const void *base, size_t size, void *out_buf, size_t *bytes_read);
 bool safe_write_ex(void *base, size_t size, const void *in_buf, size_t *bytes_written);
diff --git a/core/unix/loader.c b/core/unix/loader.c
index 3326d3848f..f5f9185d4b 100644
--- a/core/unix/loader.c
+++ b/core/unix/loader.c
@@ -1591,6 +1591,30 @@ privload_mem_is_elf_so_header(byte *mem)
     return true;
 }
 
+static bool
+dynamorio_lib_gap_empty(void)
+{
+    /* XXX: get_dynamorio_dll_start() is already calling
+     * memquery_library_bounds_by_iterator() which is doing this maps walk: can we
+     * avoid this extra walk by somehow passing info back to us?  Have an
+     * "interrupted" output param or sthg and is_dynamorio_dll_interrupted()?
+     */
+    memquery_iter_t iter;
+    memquery_iterator_start(&iter, NULL, false);
+    while (memquery_iterator_next(&iter)) {
+        if (iter.vm_start >= get_dynamorio_dll_start() &&
+            iter.vm_end <= get_dynamorio_dll_end() &&
+            iter.comment[0] != '\0' &&
+            strstr(iter.comment, DYNAMORIO_LIBRARY_NAME) == NULL) {
+            /* There's a non-.bss mapping inside: probably vvar and/or vdso. */
+            return false;
+        }
+        if (iter.vm_start >= get_dynamorio_dll_end())
+            break;
+    }
+    return true;
+}
+
 /* XXX: This routine is called before dynamorio relocation when we are in a
  * fragile state and thus no globals access or use of ASSERT/LOG/STATS!
  */
@@ -1730,8 +1754,21 @@ privload_early_inject(void **sp, byte *old_libdr_base, size_t old_libdr_size)
     }
 
     /* i#1227: if we reloaded ourselves, unload the old libdynamorio */
-    if (old_libdr_base != NULL)
-        ;// DO NOT CHECK IN: os_unmap_file(old_libdr_base, old_libdr_size);
+    if (old_libdr_base != NULL) {
+        /* i#2641: we can't blindly unload the whole region as vvar+vdso may be
+         * in the text-data gap.
+         */
+        memquery_iter_t iter;
+        memquery_iterator_start(&iter, NULL, false);
+        while (memquery_iterator_next(&iter)) {
+            if (iter.vm_start >= old_libdr_base &&
+                iter.vm_end <= old_libdr_base + old_libdr_size &&
+                strstr(iter.comment, DYNAMORIO_LIBRARY_NAME) != NULL)
+                os_unmap_file(iter.vm_start, iter.vm_end - iter.vm_start);
+            if (iter.vm_start >= old_libdr_base + old_libdr_size)
+                break;
+        }
+    }
 
     dynamorio_set_envp(envp);
 
@@ -1760,19 +1797,18 @@ privload_early_inject(void **sp, byte *old_libdr_base, size_t old_libdr_size)
     exe_map = module_vaddr_from_prog_header((app_pc)exe_ld.phdrs,
                                             exe_ld.ehdr->e_phnum, NULL, &exe_end);
     /* i#1227: on a conflict with the app: reload ourselves */
-    if (get_dynamorio_dll_start() < exe_end &&
-        get_dynamorio_dll_end() > exe_map) {
+    if ((get_dynamorio_dll_start() < exe_end &&
+         get_dynamorio_dll_end() > exe_map) ||
+        /* i#2641: we can't handle something in the text-data gap.
+         * Various parts of DR assume there's nothing inside (and we even fill the
+         * gap with a PROT_NONE mmap later: i#1659), so we reload to avoid it,
+         * under the assumption that it's rare and we're not paying this cost
+         * very often.
+         */
+        !dynamorio_lib_gap_empty()) {
         reload_dynamorio(sp, exe_map, exe_end);
         ASSERT_NOT_REACHED();
     }
-#if 1//DO NOT CHECK IN
-    // Can we have a single maps walk for get_dynamorio_dll_start() and this var,
-    // w/o sthg this hacky?
-    if (vvar_in_gap) {
-        reload_dynamorio(sp, get_dynamorio_dll_start(), get_dynamorio_dll_end());
-        ASSERT_NOT_REACHED();
-    }
-#endif
 
     exe_map = elf_loader_map_phdrs(&exe_ld,
                                    /* fixed at preferred address,
diff --git a/core/unix/memquery.c b/core/unix/memquery.c
index 6b626bd5ec..6392f70598 100644
--- a/core/unix/memquery.c
+++ b/core/unix/memquery.c
@@ -97,7 +97,7 @@ memquery_library_bounds_by_iterator(const char *name, app_pc *start/*IN/OUT*/,
              (iter.comment[0] == '\0' && prev_end != NULL &&
               prev_end != iter.vm_start))) {
             last_lib_base = iter.vm_start;
-            /* Include a prior anon mapping if contiguous and a header and this
+            /* Include a prior anon mapping if interrupted and a header and this
              * mapping is not a header.  This happens for some page mapping
              * schemes (i#2566).
              */
@@ -188,13 +188,6 @@ memquery_library_bounds_by_iterator(const char *name, app_pc *start/*IN/OUT*/,
             cur_end = iter.vm_end;
         } else if (found_library) {
             /* hit non-matching, we expect module segments to be adjacent */
-#if 1//DO NOT CHECK IN
-# ifndef STANDALONE_UNIT_TEST
-            extern bool vvar_in_gap;
-            if (strstr(iter.comment, "vvar") != NULL)
-                vvar_in_gap = true;
-# endif
-#endif
             break;
         }
         prev_base = iter.vm_start;
@@ -207,16 +200,20 @@ memquery_library_bounds_by_iterator(const char *name, app_pc *start/*IN/OUT*/,
      * header to know since we can't assume that a subsequent anonymous
      * region is .bss. */
     if (image_size != 0 && cur_end - mod_start < image_size) {
-        /* Found a .bss section. Check current mapping (note might only be
-         * part of the mapping (due to os region merging? FIXME investigate). */
-#if 0 // FIXME i#2641: these fail on 4.4.0-93 w/ vvar+vdso in middle of libdynamorio
-        ASSERT_CURIOSITY(iter.vm_start == cur_end /* no gaps, FIXME might there be
-                                                   * a gap if the file has large
-                                                   * alignment and no data section?
-                                                   * curiosity for now*/);
-        ASSERT_CURIOSITY(iter.inode == 0); /* .bss is anonymous */
-        ASSERT_CURIOSITY(iter.vm_end - mod_start >= image_size);/* should be big enough */
-#endif
+        if (iter.comment[0] != '\0') {
+            /* There's something else in the text-data gap: xref i#2641. */
+        } else {
+            /* Found a .bss section. Check current mapping (note might only be
+             * part of the mapping (due to os region merging? FIXME investigate).
+             */
+            ASSERT_CURIOSITY(iter.vm_start == cur_end /* no gaps, FIXME might there be
+                                                       * a gap if the file has large
+                                                       * alignment and no data section?
+                                                       * curiosity for now*/);
+            ASSERT_CURIOSITY(iter.inode == 0); /* .bss is anonymous */
+            /* should be big enough */
+            ASSERT_CURIOSITY(iter.vm_end - mod_start >= image_size);
+        }
         count++;
         cur_end = mod_start + image_size;
     } else {
diff --git a/core/unix/memquery.h b/core/unix/memquery.h
index 565c414787..4ced887642 100644
--- a/core/unix/memquery.h
+++ b/core/unix/memquery.h
@@ -1,5 +1,5 @@
 /* *******************************************************************************
- * Copyright (c) 2013-2015 Google, Inc.  All rights reserved.
+ * Copyright (c) 2013-2017 Google, Inc.  All rights reserved.
  * *******************************************************************************/
 
 /*
@@ -124,8 +124,16 @@ int
 find_vm_areas_via_probe(void);
 #endif
 
-/* This routine does not acquire locks */
+/* This routine might acquire locks.  is_readable_without_exception_query_os_noblock()
+ * can be used to avoid blocking.
+ */
 bool
 memquery_from_os(const byte *pc, OUT dr_mem_info_t *info, OUT bool *have_type);
 
+/* The result can change if another thread grabs the lock, but this will identify
+ * whether the current thread holds the lock, avoiding a hang.
+ */
+bool
+memquery_from_os_will_block(void);
+
 #endif /* _MEMQUERY_H_ */
diff --git a/core/unix/memquery_emulate.c b/core/unix/memquery_emulate.c
index abb08b8f10..315549b549 100644
--- a/core/unix/memquery_emulate.c
+++ b/core/unix/memquery_emulate.c
@@ -68,6 +68,12 @@ memquery_exit(void)
 {
 }
 
+bool
+memquery_from_os_will_block(bool may_alloc)
+{
+    return false;
+}
+
 bool
 memquery_iterator_start(memquery_iter_t *iter, app_pc start, bool may_alloc)
 {
diff --git a/core/unix/memquery_linux.c b/core/unix/memquery_linux.c
index 9e1097fbe0..bff3045dd9 100644
--- a/core/unix/memquery_linux.c
+++ b/core/unix/memquery_linux.c
@@ -123,6 +123,22 @@ memquery_exit(void)
     DELETE_LOCK(maps_iter_buf_lock);
 }
 
+bool
+memquery_from_os_will_block(void)
+{
+#ifdef DEADLOCK_AVOIDANCE
+    return memory_info_buf_lock.owner != INVALID_THREAD_ID;
+#else
+    /* "may_alloc" is false for memquery_from_os() */
+    bool res = true;
+    if (mutex_trylock(&memory_info_buf_lock)) {
+        res = false;
+        mutex_unlock(&memory_info_buf_lock);
+    }
+    return res;
+#endif
+}
+
 bool
 memquery_iterator_start(memquery_iter_t *iter, app_pc start, bool may_alloc)
 {
diff --git a/core/unix/memquery_macos.c b/core/unix/memquery_macos.c
index 72d67eaf0a..61a5a07d4b 100644
--- a/core/unix/memquery_macos.c
+++ b/core/unix/memquery_macos.c
@@ -1,5 +1,5 @@
 /* *******************************************************************************
- * Copyright (c) 2013-2014 Google, Inc.  All rights reserved.
+ * Copyright (c) 2013-2017 Google, Inc.  All rights reserved.
  * *******************************************************************************/
 
 /*
@@ -93,6 +93,22 @@ memquery_exit(void)
     DELETE_LOCK(memquery_backing_lock);
 }
 
+bool
+memquery_from_os_will_block(void)
+{
+#ifdef DEADLOCK_AVOIDANCE
+    return memquery_backing_lock.owner != INVALID_THREAD_ID;
+#else
+    /* "may_alloc" is false for memquery_from_os() */
+    bool res = true;
+    if (mutex_trylock(&memquery_backing_lock)) {
+        res = false;
+        mutex_unlock(&memquery_backing_lock);
+    }
+    return res;
+#endif
+}
+
 static bool
 memquery_file_backing(struct proc_regionwithpathinfo *info, app_pc addr)
 {
diff --git a/core/unix/os.c b/core/unix/os.c
index 5b0fe70c5e..36075e0c49 100644
--- a/core/unix/os.c
+++ b/core/unix/os.c
@@ -4681,6 +4681,14 @@ is_readable_without_exception_query_os(byte *pc, size_t size)
     return is_readable_without_exception_internal(pc, size, true);
 }
 
+bool
+is_readable_without_exception_query_os_noblock(byte *pc, size_t size)
+{
+    if (memquery_from_os_will_block())
+        return false;
+    return is_readable_without_exception_internal(pc, size, true);
+}
+
 bool
 is_user_address(byte *pc)
 {
diff --git a/core/utils.c b/core/utils.c
index 3a7b70eaf0..25d97f820f 100644
--- a/core/utils.c
+++ b/core/utils.c
@@ -2242,13 +2242,12 @@ report_dynamorio_problem(dcontext_t *dcontext, uint dumpcore_flag,
     }
     for (num = 0, pc = (ptr_uint_t *) report_ebp;
          num < REPORT_NUM_STACK && pc != NULL &&
-             is_readable_without_exception_query_os((app_pc) pc, 2*sizeof(reg_t));
+             is_readable_without_exception_query_os_noblock((app_pc) pc, 2*sizeof(reg_t));
          num++, pc = (ptr_uint_t *) *pc) {
         len = snprintf(curbuf, REPORT_LEN_STACK_EACH, PFX" "PFX"\n",
                        pc, *(pc+1));
         curbuf += (len == -1 ? REPORT_LEN_STACK_EACH : (len < 0 ? 0 : len));
     }
-
 #ifdef CLIENT_INTERFACE
     /* Only walk the module list if we think the data structs are safe */
     if (!TEST(DUMPCORE_INTERNAL_EXCEPTION, dumpcore_flag)) {
diff --git a/core/win32/os.c b/core/win32/os.c
index 282f46c309..b81d198042 100644
--- a/core/win32/os.c
+++ b/core/win32/os.c
@@ -5674,6 +5674,12 @@ is_readable_without_exception_query_os(byte *pc, size_t size)
     return is_readable_without_exception(pc, size);
 }
 
+bool
+is_readable_without_exception_query_os_noblock(byte *pc, size_t size)
+{
+    return is_readable_without_exception_query_os(pc, size);
+}
+
 /* Reads size bytes starting at base and puts them in out_buf, this is safe
  * to call even if the memory at base is unreadable, returns true if the
  * read succeeded */
