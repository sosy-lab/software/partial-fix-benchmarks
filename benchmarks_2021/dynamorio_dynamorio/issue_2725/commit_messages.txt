i#2720 cpusim tests: add -ignore_all_libs option (#2721)

Adds a new drcpusim option -ignore_all_libs and uses it on cpuid tests to
avoid failures on Travis Mac where system libraries are using recent
instructions.  These cpuid tests are mainly testing cpuid fooling, not
instruction set violations.

Fixes #2720
i#2725 xfer under app locks: add dr_app_recurlock_lock

I realized halfway through this PR that callers of
mutex_wait_contended_lock may not have a dcontext value in scope. To
avoid the call to get_thread_private_dcontext when it isn't needed I put
all of the new logic under the CLIENT_INTERFACE and added an ASSERT to
verify that callers didn't pass a non-NULL mcontext. Other options I can
think of off the top of my head:

1) Instead of adding the mcontext parameter to the relevant 'lock'
functions, create duplicate definitions that only can be called in the
CLIENT_INTERFACE to have compile-time prevention of passing an mcontext
outside of the CLIENT_INTERFACE

2) In mutex_wait_contended_lock, if we are outside of the
CLIENT_INTERFACE but have a non-NULL mcontext, call
get_thread_private_dcontext anyway.

Let me know if you'd like me to move to either of those options, or
anything you think is best.

Fixes #2725
i#2725 xfer under app locks: add dr_app_recurlock_lock (#2726)

This PR adds a new function to support transfer/relocation while waiting for a contended application mutex lock.

As mentioned in code comments, this routine is to allow client code called via a clean call to lock application locks safely. Deadlocks can be avoided during detach/xfer using this lock.

Fixes #2725
i#2725 xfer under app locks: update release.dox (#2740)

Forgot to do this in the original PR.

Issue: #2725
i#2725 xfer under app locks: allow deletion of in-use mutex.

Issue #2725
i#2725 xfer under app locks: allow deletion of in-use mutex during detach (#2744)

At this phase of detach, it doesn't matter what state these app locks are in: the threads are no longer executing client code. So, we can skip an assert verifying that the mutex is unlocked.

Encodes that we don't support DEBUG without DEADLOCK_AVOIDANCE by introducing a build error.

Fixes #2725
