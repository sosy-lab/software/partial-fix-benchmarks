i#1569 AArch64: Fix macros to create AND,ANDs with imms. (#3010)

Instructions that take logical immediates are not encoded with shifts.

Issue: #1569
i#2876 clang-format: Add first version of clang-format style.

This patch adds a first version of a clang-format style definition for
DynamoRIO. I think it is relatively close to the style guide, but
please let me know of any other rules that should be accounted for.

I have re-formatted 3 files from the aarch64 backend. We do not have to
include them in the final commit, but I thought they would be useful to
illustrate the impact of the style rules.

Issue #2876

Change-Id: I03c3f3530159b9a770909f5554cf13b30ae45da0
i#2876 clang-format: Add first version of clang-format style. (#3012)

This patch adds a first version of a clang-format style definition for
DynamoRIO. I think it is relatively close to the style guide, but
I expect some more tweaking.

Issue #2876
i#2876 clang-format: Add first version of clang-format style. (#3012)

This patch adds a first version of a clang-format style definition for
DynamoRIO. I think it is relatively close to the style guide, but
I expect some more tweaking.

Issue #2876
i#2876 clang-format: massive reformat of every file

Massive clang-format of every C or C++ source file (*.h, *.c, *.cpp),
except for:
+ third_party/
+ ext/drsysm/libelftc/include/
+ ext/drsyms/demangle.cc

This is a single, large commit by design to present a single history
disruption point and bring the code base into a consistent format.
clang-format version 6.0 was used.

Tweaks the clang-format rules to indent 4 after pre-processor hashes, to
allow single-line case labels, and align trailing comments.

Disables the pp_indent vera style check as we changed the indent rule and
clang-format now covers it.  Leaves the other checks, even though some are
redundant (they found clang-format errors).

Moves C++ token defines from globals_shared.h and globals.h to a new
header, core/lib/c_defines.h, to avoid a clang-format parsing error where
it fails to identify the include guard.

Adds genapi.pl removal of extra indentation inside API_EXPORT_ONLY and
CLIENT_INTERFACE regions.

Adds exclusions around large regions we don't want to format, in these
files:
+ core/win32/syscallx.h
+ core/arch/x86/decode_table.c
+ core/arch/arm/table_a32_pred.c
+ core/arch/arm/table_a32_unpred.c
+ core/arch/arm/table_t32_16.c
+ core/arch/arm/table_t32_16_it.c
+ core/arch/arm/table_t32_base.c
+ core/arch/arm/table_t32_coproc.c
+ core/arch/arm/table_encode.c

Adds smaller exclusions to work around clang-format bugs:
+ Several missing break-after-return-type
+ Several >90-char lines
+ Misc scattered issues, all listed in #2876

Issue: #2876
i#2876 clang-format: massive reformat of every file (#3093)

Massive clang-format of every C or C++ source file (*.h, *.c, *.cpp),
except for:
+ third_party/
+ ext/drsysm/libelftc/include/
+ ext/drsyms/demangle.cc

This is a single, large commit by design to present a single history
disruption point and bring the code base into a consistent format.
clang-format version 6.0 was used.

Tweaks the clang-format rules to indent 4 after pre-processor hashes, to
allow single-line case labels, and align trailing comments.

Disables the pp_indent vera style check as we changed the indent rule and
clang-format now covers it.  Leaves the other checks, even though some are
redundant (they found clang-format errors).

Moves C++ token defines from globals_shared.h and globals.h to a new
header, core/lib/c_defines.h, to avoid a clang-format parsing error where
it fails to identify the include guard.

Adds genapi.pl removal of extra indentation inside API_EXPORT_ONLY and
CLIENT_INTERFACE regions.

Adds exclusions around large regions we don't want to format, in these
files:
+ core/win32/syscallx.h
+ core/arch/x86/decode_table.c
+ core/arch/arm/table_a32_pred.c
+ core/arch/arm/table_a32_unpred.c
+ core/arch/arm/table_t32_16.c
+ core/arch/arm/table_t32_16_it.c
+ core/arch/arm/table_t32_base.c
+ core/arch/arm/table_t32_coproc.c
+ core/arch/arm/table_encode.c

Adds smaller exclusions to work around clang-format bugs:
+ Several missing break-after-return-type
+ Several >90-char lines
+ Misc scattered issues, all listed in #2876

Issue: #2876
i#2876 clang-format: check format in test suite

Adds checking of diff formatting to runsuite.cmake.  If
clang-format-diff{,.py} is found, it is used to check the formatting, and
any format change is a fatal error.  On Travis, not finding the program is
a fatal error.

Adds installation of clang-format-6.0 from apt.llvm.org on Travis.

Issue: #2876
i#2876 clang-format: check format in test suite (#3094)

Adds checking of diff formatting to runsuite.cmake.  If
clang-format-diff{,.py} is found, it is used to check the formatting, and
any format change is a fatal error.

Adds installation of clang-format-6.0 from apt.llvm.org on Travis to the
6th build, 64-bit clang.  It's shorter than the test runs, so this won't add
extra time.  Letting the test runs complete also allows a PR to get feedback
on build and test failures along with format issues simultaneously, rather
than simply having all builds fail up front with format errors.

Fixes #2876
i#2876 clang-format: fix Mac build

Fixes the Mac build which was failing on a C++ comment inside configure.h.

Issue: #2876
i#2876 clang-format: fix Mac build (#3096)

Fixes the Mac build which was failing on a C++ comment inside configure.h.

Issue: #2876
i#2876 clang-format: Fix format of optionsx.h

There was a double parenthesis causing clang-format to fail to format
core/optionsx.h.  We fix that here.

Issue: #2876
i#2876 clang-format: Fix format of optionsx.h (#4182)

There was a double parenthesis causing clang-format to fail to format
core/optionsx.h.  We fix that here.

Issue: #2876
