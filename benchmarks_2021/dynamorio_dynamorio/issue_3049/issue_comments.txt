Bug to fix: drmgr counters being nonzero when exit, leading to corrupted drmemtrace traces in subsequent execution
> 4. ctest -R .*burst_static will fail 2 out of 3 times (test case: tool.drcacheoff.burst_static).

Fail in what way?  Please provide details: exact output.  If this is release build, what happens in debug build?
Sure! Here is the output of running `ctest --verbose -R \.*burst_static$` under debug build:
```
UpdateCTestConfiguration  from :/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/DartConfiguration.tcl
Parse Config file:/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/DartConfiguration.tcl
UpdateCTestConfiguration  from :/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/DartConfiguration.tcl
Parse Config file:/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/DartConfiguration.tcl
Test project /usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug
Constructing a list of tests
Done constructing a list of tests
Updating test list for fixtures
Added 0 tests to meet fixture requirements
Checking test dependency graph...
Checking test dependency graph end
test 244
    Start 244: code_api|tool.drcacheoff.burst_static

244: Test command: /usr/bin/cmake "-D" "precmd=foreach@/usr/bin/cmake@-E@remove_directory@drmemtrace.tool.drcacheoff.burst_static.*.dir" "-D" "cmd=/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/clients/bin64/tool.drcacheoff.burst_static" "-D" "postcmd=firstglob@/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/clients/bin64/drcachesim@-indir@drmemtrace.tool.drcacheoff.burst_static.*.dir" "-D" "postcmd2=" "-D" "postcmd3=" "-D" "cmp=/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/suite/tests/offline-burst_static.expect" "-P" "/usr/local/google/home/yelouis/prj/Work/dynamorio/suite/tests/runmulti.cmake"
244: Test timeout computed to be: 1500
244: Running |/usr/bin/cmake;-E;remove_directory /usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/suite/tests/drmemtrace.tool.drcacheoff.burst_static.249608.1806.dir|
244: Running |/usr/bin/cmake;-E;remove_directory /usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/suite/tests/drmemtrace.tool.drcacheoff.burst_static.249608.2579.dir|
244: Running |/usr/bin/cmake;-E;remove_directory /usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/suite/tests/drmemtrace.tool.drcacheoff.burst_static.249608.3756.dir|
244: Running cmd |/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/clients/bin64/tool.drcacheoff.burst_static|
244: Running postcmd |/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/clients/bin64/drcachesim;-indir;/usr/local/google/home/yelouis/prj/Work/dynamorio/build_debug/suite/tests/drmemtrace.tool.drcacheoff.burst_static.249655.4415.dir|
244: CMake Error at /usr/local/google/home/yelouis/prj/Work/dynamorio/suite/tests/runmulti.cmake:130 (message):
244:   output |pre-DR init
244: 
244:   pre-DR start
244: 
244:   pre-DR detach
244: 
244:   DynamoRIO statistics:
244: 
244:                 Peak threads under DynamoRIO control :                 1
244:                                 Threads ever created :                 1
244:                                    System calls, pre :                 1
244:                                   System calls, post :                 1
244:                      Basic block fragments generated :               276
244:                            Trace fragments generated :                18
244:                Peak fcache combined capacity (bytes) :             94208
244:                       Peak fcache units on live list :                 4
244:                       Peak fcache units on free list :                 4
244:                   Peak special heap capacity (bytes) :             16384
244:                         Peak heap units on live list :                11
244:                         Peak heap units on free list :                 5
244:                            Peak client raw mmap size :             36864
244:                          Peak stack capacity (bytes) :            147456
244:                           Peak heap capacity (bytes) :            544768
244:                    Peak total memory from OS (bytes) :           1449984
244:                             Peak vmm blocks for heap :               213
244:                            Peak vmm blocks for cache :                64
244:                            Peak vmm blocks for stack :                42
244:                     Peak vmm blocks for special heap :                25
244:                     Peak vmm blocks for special mmap :                10
244:               Peak vmm virtual memory in use (bytes) :           1449984
244: 
244:   all done
244: 
244:   pre-DR init
244: 
244:   pre-DR start
244: 
244:   pre-DR detach
244: 
244:   DynamoRIO statistics:
244: 
244:                 Peak threads under DynamoRIO control :                 1
244:                                 Threads ever created :                 2
244:                                    System calls, pre :                 2
244:                                   System calls, post :                 2
244:                      Basic block fragments generated :               552
244:                            Trace fragments generated :                36
244:                Peak fcache combined capacity (bytes) :             94208
244:                       Peak fcache units on live list :                 4
244:                       Peak fcache units on free list :                 4
244:                   Peak special heap capacity (bytes) :             16384
244:                         Peak heap units on live list :                11
244:                         Peak heap units on free list :                 5
244:                            Peak client raw mmap size :             36864
244:                          Peak stack capacity (bytes) :            204800
244:                           Peak heap capacity (bytes) :            544768
244:                    Peak total memory from OS (bytes) :           1449984
244:                             Peak vmm blocks for heap :               213
244:                            Peak vmm blocks for cache :                64
244:                            Peak vmm blocks for stack :                58
244:                     Peak vmm blocks for special heap :                25
244:                     Peak vmm blocks for special mmap :                10
244:               Peak vmm virtual memory in use (bytes) :           1449984
244: 
244:   all done
244: 
244:   pre-DR init
244: 
244:   pre-DR start
244: 
244:   pre-DR detach
244: 
244:   DynamoRIO statistics:
244: 
244:                 Peak threads under DynamoRIO control :                 1
244:                                 Threads ever created :                 3
244:                                    System calls, pre :                 3
244:                                   System calls, post :                 3
244:                      Basic block fragments generated :               828
244:                            Trace fragments generated :                54
244:                Peak fcache combined capacity (bytes) :             94208
244:                       Peak fcache units on live list :                 4
244:                       Peak fcache units on free list :                 4
244:                   Peak special heap capacity (bytes) :             16384
244:                         Peak heap units on live list :                11
244:                         Peak heap units on free list :                 5
244:                            Peak client raw mmap size :             36864
244:                          Peak stack capacity (bytes) :            262144
244:                           Peak heap capacity (bytes) :            544768
244:                    Peak total memory from OS (bytes) :           1449984
244:                             Peak vmm blocks for heap :               213
244:                            Peak vmm blocks for cache :                64
244:                            Peak vmm blocks for stack :                74
244:                     Peak vmm blocks for special heap :                25
244:                     Peak vmm blocks for special mmap :                10
244:               Peak vmm virtual memory in use (bytes) :           1449984
244: 
244:   all done
244: 
244:   Cache simulation results:
244: 
244:   Core #0 (1 thread(s))
244: 
244:     L1I stats:
244:       Hits:                                0
244:       Misses:                              0
244:       Invalidations:                       0
244:     L1D stats:
244:       Hits:                                0
244:       Misses:                              0
244:       Invalidations:                       0
244: 
244:   Core #1 (0 thread(s))
244: 
244:   Core #2 (0 thread(s))
244: 
244:   Core #3 (0 thread(s))
244: 
244:   LL stats:
244: 
244:       Hits:                                0
244:       Misses:                              0
244:       Invalidations:                       0
244: 
244:   | failed to match expected output |pre-DR init
244: 
244:   pre-DR start
244: 
244:   pre-DR detach
244: 
244:   DynamoRIO statistics:
244: 
244:    *Peak threads under DynamoRIO control : *1
244: 
244:   .*
244: 
244:   all done
244: 
244:   pre-DR init
244: 
244:   pre-DR start
244: 
244:   pre-DR detach
244: 
244:   DynamoRIO statistics:
244: 
244:    *Peak threads under DynamoRIO control : *1
244: 
244:   .*
244: 
244:   all done
244: 
244:   pre-DR init
244: 
244:   pre-DR start
244: 
244:   pre-DR detach
244: 
244:   DynamoRIO statistics:
244: 
244:    *Peak threads under DynamoRIO control : *1
244: 
244:   .*
244: 
244:   all done
244: 
244:   Cache simulation results:
244: 
244:   Core #0 \(1 thread\(s\)\)
244: 
244:     L1I stats:
244:       Hits:                         *[0-9,\.]*.....
244:       Misses:                       *[0-9,\.]*...
244:       Invalidations:                *0
244: 
244:   .* Miss rate: 0[,\.]..%
244: 
244:     L1D stats:
244:       Hits:                         *[0-9,\.]*.....
244:       Misses:                       *[0-9,\.]*.
244:       Invalidations:                *0
244: 
244:   .* Miss rate: 0[,\.]..%
244: 
244:   Core #1 \(0 thread\(s\)\)
244: 
244:   Core #2 \(0 thread\(s\)\)
244: 
244:   Core #3 \(0 thread\(s\)\)
244: 
244:   LL stats:
244: 
244:       Hits:                         *[0-9,\.]*.
244:       Misses:                       *[0-9,\.]*..
244:       Invalidations:                *0
244: 
244:   .* Local miss rate: *[0-9]*[,\.]..%
244: 
244:       Child hits:                   *[0-9,\.]*...
244:       Total miss rate:                  [0-1][,\.]..%
244: 
244:   |
244: 
244: 
1/1 Test #244: code_api|tool.drcacheoff.burst_static ...***Failed    1.50 sec

0% tests passed, 1 tests failed out of 1

Total Test time (real) =   2.97 sec

The following tests FAILED:
	244 - code_api|tool.drcacheoff.burst_static (Failed)
Errors while running CTest
```
This test case actively invokes and stops memtrace 3 times in one run and it is expected that there should be 3 raw trace directories generated with each having the same amount of memory accesses.
After adding drwrap_init() and drwrap_exit() to memtrace(or drcachesim) client, the actual running result shows that the 1st generated trace directory has the expected number of memory accesses (around 600K entries), but the 2nd and 3rd trace directories, though generated, each only has 5 entries like this:
```
type:OFFLINE_TYPE_THREAD 261254 tidx:0
type:OFFLINE_TYPE_PID 261254 tidx:0
type:OFFLINE_TYPE_TIMESTAMP 13173741696965458 tidx:0
type:OFFLINE_TYPE_EXTENDED 2 3 10 tidx:0
type:OFFLINE_TYPE_EXTENDED 1 0 0 tidx:0
```
When this raw trace is parsed by drcachesim, there is no memory accesses recorded, thus there is no cache miss rate shown, which fails the test case's expected output. The reason why the test case fails 2 out of 3 times is because it only checks 1 of the 3 generated directories, but the problem itself will show up pretty consistently, i.e., traces generated starting from the second DR invocation are corrupted.
Some further investigations:
I have confirmed that this problem can be fixed by adding only the following two lines into `...drwrap.c:drwrap_exit` (as suggested in [PR 3050](https://github.com/DynamoRIO/dynamorio/pull/3050))
```
    drmgr_unregister_bb_app2app_event(drwrap_event_bb_app2app);
    drmgr_unregister_bb_instrumentation_event(drwrap_event_bb_analysis);
```
So I suspect that the resources that are not being cleaned up could be either the static `cblist_app2app; cblist_instrumentation;` or the static `bb_callbacks`.

`bb_callbacks` looks more like the culprit because it is only freed in the `instrument_exit()` under the condition of `IF_DEBUG_ELSE(true, doing_detach)`, and the condition of `instrument_exit()` being called is subject to `dynamo_shared_exit()` and `dynamo_process_exit()`.
It is still not clear what the problem is: `doing_detach` is true for any reattach test.  `cblist_app2app` and `cblist_instrumentation` are cleared out by `drmgr_exit`.

I think this is coming from `bb_event_count` in drmgr.  I think it should be zeroed out at exit to make drmgr robust to clients failing to unregister at exit which is not uncommon (unregistering is considered only necessary when dynamically changing events mid-run).  I'm hoping you can add that zeroing to this PR?


Cool, I have confirmed that `bb_event_count` is indeed the root cause! :+1:  Solely zeroing it out can solve the problem. 
But instead of zeroing it out at the exit, do you think it should be assigned zero at initialization phase? So that it will not be a random value if the static memory is touched before.
And also I noticed that there are several other static variables[1][2], which are used as counters, might also potentially suffer from the same problem.
[1]`quartet_count`
[2]`pair_count`
static variables are guaranteed initialized to 0 by the language.
For sure it is 0 if it initialized and not touched. But my concern is that what if there is any random access (like some malbehavior in the user applications) that change its value.
But I guess it is too broad of a problem to solve.
I will follow the the same style as other DR components and zeroing out those variables at exit.