diff --git a/api/docs/API.doxy b/api/docs/API.doxy
index b53c454ec3..5c17c8f891 100644
--- a/api/docs/API.doxy
+++ b/api/docs/API.doxy
@@ -210,7 +210,7 @@ EXPAND_ONLY_PREDEF     = YES
 SEARCH_INCLUDES        = YES
 INCLUDE_PATH           =
 INCLUDE_FILE_PATTERNS  =
-PREDEFINED             = WINDOWS UNIX LINUX X64 X86 ARM ANDROID DYNAMORIO_API DR_PAGE_SIZE_COMPATIBILITY DR_LOG_DEFINE_COMPATIBILITY AARCHXX AARCH64
+PREDEFINED             = WINDOWS UNIX LINUX X64 X86 ARM ANDROID DYNAMORIO_API DR_PAGE_SIZE_COMPATIBILITY DR_LOG_DEFINE_COMPATIBILITY AARCHXX AARCH64 DR_NO_FAST_IR
 EXPAND_AS_DEFINED      = IF_X64_ELSE
 SKIP_FUNCTION_MACROS   = YES
 #---------------------------------------------------------------------------
diff --git a/api/docs/release.dox b/api/docs/release.dox
index 41c8c2ec56..a48f80ceb9 100644
--- a/api/docs/release.dox
+++ b/api/docs/release.dox
@@ -244,6 +244,8 @@ Further non-compatibility-affecting changes include:
  - Added drreg_restore_all() to restore all unreserved registers currently
    maintained by drreg.
  - Added a func_view tool to drcachesim for analyzing function traces.
+ - Added a non-heap-using instruction structure #instr_noalloc_t for use when
+   decoding in a signal handler.
 
 **************************************************
 <hr>
diff --git a/core/arch/arch_exports.h b/core/arch/arch_exports.h
index 1e83618734..eaf279d6b7 100644
--- a/core/arch/arch_exports.h
+++ b/core/arch/arch_exports.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2011-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2011-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2000-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -1937,9 +1937,34 @@ cache_pc
 get_native_ret_ibl_xfer_entry(dcontext_t *dcontext);
 #endif
 
+/* DR_API EXPORT TOFILE dr_ir_instr.h */
+/* DR_API EXPORT BEGIN */
 enum {
 #ifdef X86
     MAX_INSTR_LENGTH = 17,
+    MAX_SRC_OPNDS = 8, /* pusha */
+    MAX_DST_OPNDS = 8, /* popa */
+#elif defined(AARCH64)
+    /* The maximum instruction length is 64 to allow for an OP_ldstex containing
+     * up to 16 real instructions. The longest such block seen so far in real
+     * code had 7 instructions so this is likely to be enough. With the current
+     * implementation, a larger value would significantly slow down the search
+     * for such blocks in the decoder: see decode_ldstex().
+     */
+    MAX_INSTR_LENGTH = 64,
+    MAX_SRC_OPNDS = 8,
+    MAX_DST_OPNDS = 8,
+#elif defined(ARM)
+    MAX_INSTR_LENGTH = 4,
+    /* With register lists we can see quite long operand lists. */
+    MAX_SRC_OPNDS = 33 /* vstm s0-s31 */
+    MAX_DST_OPNDS = MAX_SRC_OPNDS,
+#endif
+};
+/* DR_API EXPORT END */
+
+enum {
+#ifdef X86
     /* size of 32-bit-offset jcc instr, assuming it has no
      * jcc branch hint!
      */
@@ -1960,20 +1985,12 @@ enum {
     CTI_FAR_ABS_LENGTH = 7, /* 9A 1B 07 00 34 39 call 0739:3400071B            */
                             /* 07                                              */
 #elif defined(AARCH64)
-    /* The maximum instruction length is 64 to allow for an OP_ldstex containing
-     * up to 16 real instructions. The longest such block seen so far in real
-     * code had 7 instructions so this is likely to be enough. With the current
-     * implementation, a larger value would significantly slow down the search
-     * for such blocks in the decoder: see decode_ldstex().
-     */
-    MAX_INSTR_LENGTH = 64,
     CBR_LONG_LENGTH = 4,
     JMP_LONG_LENGTH = 4,
     JMP_SHORT_LENGTH = 4,
     CBR_SHORT_REWRITE_LENGTH = 4,
     SVC_LENGTH = 4,
 #elif defined(ARM)
-    MAX_INSTR_LENGTH = ARM_INSTR_SIZE,
     CBR_LONG_LENGTH = ARM_INSTR_SIZE,
     JMP_LONG_LENGTH = ARM_INSTR_SIZE,
     JMP_SHORT_LENGTH = THUMB_SHORT_INSTR_SIZE,
diff --git a/core/arch/arm/decode.c b/core/arch/arm/decode.c
index d7e6f3d12d..d4bf3d932b 100644
--- a/core/arch/arm/decode.c
+++ b/core/arch/arm/decode.c
@@ -159,9 +159,6 @@ decode_in_it_block(decode_state_t *state, app_pc pc)
     return false;
 }
 
-/* With register lists we can see quite long operand lists */
-#define MAX_OPNDS IF_X64_ELSE(8, 33 /*vstm s0-s31*/)
-
 bool
 is_isa_mode_legal(dr_isa_mode_t mode)
 {
@@ -2426,8 +2423,8 @@ decode_common(dcontext_t *dcontext, byte *pc, byte *orig_pc, instr_t *instr)
     decode_info_t di;
     byte *next_pc;
     uint num_dsts = 0, num_srcs = 0;
-    opnd_t dsts[MAX_OPNDS];
-    opnd_t srcs[MAX_OPNDS];
+    opnd_t dsts[MAX_DST_OPNDS];
+    opnd_t srcs[MAX_SRC_OPNDS];
 
     CLIENT_ASSERT(instr->opcode == OP_INVALID || instr->opcode == OP_UNDECODED,
                   "decode: instr is already decoded, may need to call instr_reset()");
diff --git a/core/arch/instr.h b/core/arch/instr.h
index 7e66185090..9f7f214595 100644
--- a/core/arch/instr.h
+++ b/core/arch/instr.h
@@ -186,7 +186,7 @@ enum {
     /* DR_API EXPORT BEGIN */
     INSTR_DO_NOT_MANGLE = 0x00200000,
     /* DR_API EXPORT END */
-    /* Available: 0x00400000, */
+    INSTR_IS_NOALLOC_STRUCT = 0x00400000,
     /* used to indicate that an indirect call can be treated as a direct call */
     INSTR_IND_CALL_DIRECT = 0x00800000,
 #    ifdef WINDOWS
@@ -498,6 +498,37 @@ struct _instr_t {
 };     /* instr_t */
 #endif /* DR_FAST_IR */
 
+/**
+ * A version of #instr_t which guarantees to not use heap allocation for regular
+ * decoding and encoding.  It inlines all the possible operands and encoding space
+ * inside the structure.  Some operations could still use heap if custom label data is
+ * used to point at heap-allocated structures through extension libraries or custom
+ * code.
+ *
+ * The 'instr' field should be passed to decoding and other routines.  E.g.:
+ *
+ *    instr_noalloc_t noalloc;
+ *    instr_noalloc_init(dcontext, &noalloc);
+ *    pc = decode(dcontext, ptr, &noalloc.instr);
+ *
+ * Some operations are not supported on this instruction format:
+ * + instr_clone()
+ * + instr_remove_srcs()
+ * + instr_remove_dsts()
+ * + Automated re-relativization when encoding.
+ *
+ * This format does not support caching encodings, so it is less efficient for encoding.
+ * It is intended for use when decoding in a signal handler or other locations where
+ * heap allocation is unsafe.
+ */
+typedef struct instr_noalloc_t {
+    instr_t instr;
+    opnd_t srcs[MAX_SRC_OPNDS - 1];
+    opnd_t dsts[MAX_DST_OPNDS];
+    byte encode_buf[MAX_INSTR_LENGTH];
+} instr_noalloc_t;
+// NOCHECK not under DR_FAST_IR?
+
 /****************************************************************************
  * INSTR ROUTINES
  */
@@ -531,6 +562,16 @@ DR_API
 void
 instr_init(dcontext_t *dcontext, instr_t *instr);
 
+#if 0 // def DR_FAST_IR // NOCHECK
+DR_API
+/**
+ * NOCHECK Initializes \p instr.
+ * Sets the x86/x64 mode of \p instr to the mode of dcontext.
+ */
+void
+instr_noalloc_init(dcontext_t *dcontext, instr_noalloc_t *instr);
+#endif
+
 DR_API
 /**
  * Deallocates all memory that was allocated by \p instr.  This
@@ -815,7 +856,7 @@ void
 instr_exit_branch_set_type(instr_t *instr, uint type);
 
 DR_API
-/** Returns number of bytes of heap used by \p instr. */
+/** Returns the total number of bytes of memory used by \p instr. */
 int
 instr_mem_usage(instr_t *instr);
 
diff --git a/core/arch/instr_shared.c b/core/arch/instr_shared.c
index 87bff940d2..ad5d17bae4 100644
--- a/core/arch/instr_shared.c
+++ b/core/arch/instr_shared.c
@@ -110,6 +110,12 @@ instr_destroy(dcontext_t *dcontext, instr_t *instr)
 instr_t *
 instr_clone(dcontext_t *dcontext, instr_t *orig)
 {
+    /* We could heap-allocate an instr_noalloc_t but it's intended for use in a
+     * signal handler or other places where we don't want any heap allocation.
+     */
+    CLIENT_ASSERT(!TEST(INSTR_IS_NOALLOC_STRUCT, orig->flags),
+                  "Cloning an instr_noalloc_t is not supported.");
+
     instr_t *instr = (instr_t *)heap_alloc(dcontext, sizeof(instr_t) HEAPACCT(ACCT_IR));
     memcpy((void *)instr, (void *)orig, sizeof(instr_t));
     instr->next = NULL;
@@ -160,12 +166,23 @@ instr_init(dcontext_t *dcontext, instr_t *instr)
     instr_set_isa_mode(instr, dr_get_isa_mode(dcontext));
 }
 
+/* zeroes out the fields of instr */
+void
+instr_noalloc_init(dcontext_t *dcontext, instr_noalloc_t *instr)
+{
+    memset(instr, 0, sizeof(*instr));
+    instr->instr.flags |= INSTR_IS_NOALLOC_STRUCT;
+    instr_set_isa_mode(&instr->instr, dr_get_isa_mode(dcontext));
+}
+
 /* Frees all dynamically allocated storage that was allocated by instr */
 void
 instr_free(dcontext_t *dcontext, instr_t *instr)
 {
     if (instr_is_label(instr) && instr_get_label_callback(instr) != NULL)
         (*instr->label_cb)(dcontext, instr);
+    if (TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags))
+        return;
     if (TEST(INSTR_RAW_BITS_ALLOCATED, instr->flags)) {
         instr_free_raw_bits(dcontext, instr);
     }
@@ -184,10 +201,11 @@ instr_free(dcontext_t *dcontext, instr_t *instr)
     }
 }
 
-/* Returns number of bytes of heap used by instr */
 int
 instr_mem_usage(instr_t *instr)
 {
+    if (TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags))
+        return sizeof(instr_noalloc_t);
     int usage = 0;
     if ((instr->flags & INSTR_RAW_BITS_ALLOCATED) != 0) {
         usage += instr->length;
@@ -295,10 +313,23 @@ instr_build_bits(dcontext_t *dcontext, int opcode, uint num_bytes)
 static int
 private_instr_encode(dcontext_t *dcontext, instr_t *instr, bool always_cache)
 {
-    /* We cannot use a stack buffer for encoding since our stack on x64 linux
-     * can be too far to reach from our heap.  We need reachable heap.
-     */
-    byte *buf = heap_reachable_alloc(dcontext, MAX_INSTR_LENGTH HEAPACCT(ACCT_IR));
+    byte *buf;
+    byte stack_buf[MAX_INSTR_LENGTH];
+    if (TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags)) {
+        /* We have no choice: we live with no persistent caching if the stack is
+         * too far away, because the instr's raw bits will be on the stack.
+         * (We can't use encode_buf here bc the re-rel below does not support
+         * the same buffer; maybe it could w/ a memmove in the encode code?)
+         */
+        buf = stack_buf;
+    } else {
+        /* We cannot efficiently use a stack buffer for encoding since our stack on x64
+         * linux can be too far to reach from our heap.  We need reachable heap.
+         * Otherwise we can't keep the encoding around since re-relativization won't
+         * work.
+         */
+        buf = heap_reachable_alloc(dcontext, MAX_INSTR_LENGTH HEAPACCT(ACCT_IR));
+    }
     uint len;
     /* Do not cache instr opnds as they are pc-relative to final encoding location.
      * Rather than us walking all of the operands separately here, we have
@@ -316,7 +347,8 @@ private_instr_encode(dcontext_t *dcontext, instr_t *instr, bool always_cache)
                                                             instr_get_isa_mode(instr)
                                                                 _IF_ARM(false))
                                         ->name);
-            heap_reachable_free(dcontext, buf, MAX_INSTR_LENGTH HEAPACCT(ACCT_IR));
+            if (!TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags))
+                heap_reachable_free(dcontext, buf, MAX_INSTR_LENGTH HEAPACCT(ACCT_IR));
             return 0;
         }
         /* if unreachable, we can't cache, since re-relativization won't work */
@@ -364,7 +396,8 @@ private_instr_encode(dcontext_t *dcontext, instr_t *instr, bool always_cache)
         instr->bytes = tmp;
         instr_set_operands_valid(instr, valid);
     }
-    heap_reachable_free(dcontext, buf, MAX_INSTR_LENGTH HEAPACCT(ACCT_IR));
+    if (!TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags))
+        heap_reachable_free(dcontext, buf, MAX_INSTR_LENGTH HEAPACCT(ACCT_IR));
     return len;
 }
 
@@ -503,16 +536,26 @@ instr_set_num_opnds(dcontext_t *dcontext, instr_t *instr, int instr_num_dsts,
         CLIENT_ASSERT_TRUNCATE(instr->num_dsts, byte, instr_num_dsts,
                                "instr_set_num_opnds: too many dsts");
         instr->num_dsts = (byte)instr_num_dsts;
-        instr->dsts = (opnd_t *)heap_alloc(
-            dcontext, instr_num_dsts * sizeof(opnd_t) HEAPACCT(ACCT_IR));
+        if (TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags)) {
+            instr_noalloc_t *noalloc = (instr_noalloc_t *)instr;
+            noalloc->instr.dsts = noalloc->dsts;
+        } else {
+            instr->dsts = (opnd_t *)heap_alloc(
+                dcontext, instr_num_dsts * sizeof(opnd_t) HEAPACCT(ACCT_IR));
+        }
     }
     if (instr_num_srcs > 0) {
         /* remember that src0 is static, rest are dynamic */
         if (instr_num_srcs > 1) {
             CLIENT_ASSERT(instr->num_srcs <= 1 && instr->srcs == NULL,
                           "instr_set_num_opnds: srcs are already set");
-            instr->srcs = (opnd_t *)heap_alloc(
-                dcontext, (instr_num_srcs - 1) * sizeof(opnd_t) HEAPACCT(ACCT_IR));
+            if (TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags)) {
+                instr_noalloc_t *noalloc = (instr_noalloc_t *)instr;
+                noalloc->instr.srcs = noalloc->srcs;
+            } else {
+                instr->srcs = (opnd_t *)heap_alloc(
+                    dcontext, (instr_num_srcs - 1) * sizeof(opnd_t) HEAPACCT(ACCT_IR));
+            }
         }
         CLIENT_ASSERT_TRUNCATE(instr->num_srcs, byte, instr_num_srcs,
                                "instr_set_num_opnds: too many srcs");
@@ -556,6 +599,9 @@ void
 instr_remove_srcs(dcontext_t *dcontext, instr_t *instr, uint start, uint end)
 {
     opnd_t *new_srcs;
+    CLIENT_ASSERT(!TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags),
+                  /* We could implement, but it does not seem an important use case. */
+                  "instr_remove_srcs not supported for instr_noalloc_t");
     CLIENT_ASSERT(start >= 0 && end <= instr->num_srcs && start < end,
                   "instr_remove_srcs: ordinals invalid");
     if (instr->num_srcs - 1 > (byte)(end - start)) {
@@ -585,6 +631,9 @@ void
 instr_remove_dsts(dcontext_t *dcontext, instr_t *instr, uint start, uint end)
 {
     opnd_t *new_dsts;
+    CLIENT_ASSERT(!TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags),
+                  /* We could implement, but it does not seem an important use case. */
+                  "instr_remove_srcs not supported for instr_noalloc_t");
     CLIENT_ASSERT(start >= 0 && end <= instr->num_dsts && start < end,
                   "instr_remove_dsts: ordinals invalid");
     if (instr->num_dsts > (byte)(end - start)) {
@@ -972,7 +1021,8 @@ instr_free_raw_bits(dcontext_t *dcontext, instr_t *instr)
 {
     if ((instr->flags & INSTR_RAW_BITS_ALLOCATED) == 0)
         return;
-    heap_reachable_free(dcontext, instr->bytes, instr->length HEAPACCT(ACCT_IR));
+    if (!TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags))
+        heap_reachable_free(dcontext, instr->bytes, instr->length HEAPACCT(ACCT_IR));
     instr->bytes = NULL;
     instr->flags &= ~INSTR_RAW_BITS_VALID;
     instr->flags &= ~INSTR_RAW_BITS_ALLOCATED;
@@ -986,12 +1036,21 @@ void
 instr_allocate_raw_bits(dcontext_t *dcontext, instr_t *instr, uint num_bytes)
 {
     byte *original_bits = NULL;
-    if ((instr->flags & INSTR_RAW_BITS_VALID) != 0)
+    if (TEST(INSTR_RAW_BITS_VALID, instr->flags))
         original_bits = instr->bytes;
-    if ((instr->flags & INSTR_RAW_BITS_ALLOCATED) == 0 || instr->length != num_bytes) {
-        /* We need reachable heap for rip-rel re-relativization. */
-        byte *new_bits =
-            (byte *)heap_reachable_alloc(dcontext, num_bytes HEAPACCT(ACCT_IR));
+    if (!TEST(INSTR_RAW_BITS_ALLOCATED, instr->flags) || instr->length != num_bytes) {
+        byte *new_bits;
+        if (TEST(INSTR_IS_NOALLOC_STRUCT, instr->flags)) {
+            /* This may not be reachable, so re-relativization is limited. */
+            instr_noalloc_t *noalloc = (instr_noalloc_t *)instr;
+            CLIENT_ASSERT(num_bytes <= sizeof(noalloc->encode_buf),
+                          "instr_allocate_raw_bits exceeds instr_noalloc_t capacity");
+            new_bits = noalloc->encode_buf;
+        } else {
+            /* We need reachable heap for rip-rel re-relativization. */
+            new_bits =
+                (byte *)heap_reachable_alloc(dcontext, num_bytes HEAPACCT(ACCT_IR));
+        }
         if (original_bits != NULL) {
             /* copy original bits into modified bits so can just modify
              * a few and still have all info in one place
diff --git a/core/globals.h b/core/globals.h
index a6da8bf6ea..b50f8f923f 100644
--- a/core/globals.h
+++ b/core/globals.h
@@ -232,10 +232,12 @@ typedef byte *cache_pc; /* fragment cache pc */
 extern const char dynamorio_version_string[];
 extern const char dynamorio_buildmark[];
 
+#ifdef DR_FAST_IR
 struct _opnd_t;
 typedef struct _opnd_t opnd_t;
 struct _instr_t;
 typedef struct _instr_t instr_t;
+#endif
 struct _instr_list_t;
 struct _fragment_t;
 typedef struct _fragment_t fragment_t;
diff --git a/core/lib/globals_shared.h b/core/lib/globals_shared.h
index 48c2e66924..0704e33a70 100644
--- a/core/lib/globals_shared.h
+++ b/core/lib/globals_shared.h
@@ -99,6 +99,13 @@
 #    define UNIX
 #endif
 
+#ifdef DR_NO_FAST_IR
+#    undef DR_FAST_IR
+#    undef INSTR_INLINE
+#else
+#    define DR_FAST_IR 1
+#endif
+
 #ifdef API_EXPORT_ONLY
 #    ifdef WINDOWS
 #        define WIN32_LEAN_AND_MEAN
@@ -365,19 +372,18 @@ extern file_t our_stdin;
  */
 typedef uint client_id_t;
 
-#ifdef API_EXPORT_ONLY
-#    ifndef DR_FAST_IR
+#ifndef DR_FAST_IR
 /**
  * Internal structure of opnd_t is below abstraction layer.
  * But compiler needs to know field sizes to copy it around
  */
 typedef struct {
-#        ifdef X64
+#    ifdef X64
     uint black_box_uint;
     uint64 black_box_uint64;
-#        else
+#    else
     uint black_box_uint[3];
-#        endif
+#    endif
 } opnd_t;
 
 /**
@@ -386,19 +392,18 @@ typedef struct {
  * instead of always allocated on the heap.
  */
 typedef struct {
-#        ifdef X64
+#    ifdef X64
     uint black_box_uint[26];
-#        else
+#    else
     uint black_box_uint[17];
-#        endif
+#    endif
 } instr_t;
-#    else
+#else
 struct _opnd_t;
 typedef struct _opnd_t opnd_t;
 struct _instr_t;
 typedef struct _instr_t instr_t;
-#    endif /* !DR_FAST_IR */
-#endif     /* API_EXPORT_ONLY */
+#endif
 
 #ifndef IN
 #    define IN /* marks input param */
@@ -1246,8 +1251,9 @@ typedef char liststring_t[MAX_LIST_OPTION_LENGTH];
  * share/config.c) to set up new eventlogs (mainly for vista where our
  * installer doesn't work yet xref case 8482).*/
 #    define L_EVENT_FILE_VALUE_NAME L"File"
-#    define L_EVENT_FILE_NAME_PRE_VISTA \
-        L"%SystemRoot%\\system32\\config\\" L_EXPAND_LEVEL(EVENTLOG_NAME) L".evt"
+#    define L_EVENT_FILE_NAME_PRE_VISTA                                          \
+        L"%SystemRoot%\\system32\\config\\" L_EXPAND_LEVEL(EVENTLOG_NAME) L".ev" \
+                                                                          L"t"
 #    define L_EVENT_FILE_NAME_VISTA \
         L"%SystemRoot%\\system32\\winevt\\logs\\" L_EXPAND_LEVEL(EVENTLOG_NAME) L".elf"
 #    define L_EVENT_MAX_SIZE_NAME L"MaxSize"
@@ -1301,8 +1307,9 @@ typedef char liststring_t[MAX_LIST_OPTION_LENGTH];
 #    define INJECT_HELPER_DLL2_NAME "drearlyhelp2.dll"
 
 #    define DEBUGGER_INJECTION_HIVE HKEY_LOCAL_MACHINE
-#    define DEBUGGER_INJECTION_KEY \
-        "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Image File Execution Options"
+#    define DEBUGGER_INJECTION_KEY                                               \
+        "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Image File Execution " \
+        "Options"
 #    define DEBUGGER_INJECTION_VALUE_NAME "Debugger"
 
 #    define DEBUGGER_INJECTION_HIVE_L L"\\Registry\\Machine\\"
diff --git a/suite/tests/api/drdecode_aarch64.c b/suite/tests/api/drdecode_aarch64.c
index c8cd3a6b7c..51d7e26b41 100644
--- a/suite/tests/api/drdecode_aarch64.c
+++ b/suite/tests/api/drdecode_aarch64.c
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2015 Google, Inc.  All rights reserved.
+ * Copyright (c) 2015-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2016 ARM Limited.  All rights reserved.
  * **********************************************************/
 
@@ -39,6 +39,14 @@
 
 #define GD GLOBAL_DCONTEXT
 
+#define ASSERT(x)                                                                    \
+    ((void)((!(x)) ? (printf("ASSERT FAILURE: %s:%d: %s\n", __FILE__, __LINE__, #x), \
+                      abort(), 0)                                                    \
+                   : 0))
+
+#define BUFFER_SIZE_BYTES(buf) sizeof(buf)
+#define BUFFER_SIZE_ELEMENTS(buf) (BUFFER_SIZE_BYTES(buf) / sizeof(buf[0]))
+
 static void
 test_disasm(void)
 {
@@ -49,11 +57,46 @@ test_disasm(void)
         pc = disassemble_with_info(GD, pc, STDOUT, false /*no pc*/, true);
 }
 
+/* XXX: It would be nice to share some of this code w/ the other
+ * platforms but we'd need cross-platform register references or keep
+ * the encoded instr around and compare operands or sthg.
+ */
+static void
+test_noalloc(void)
+{
+    byte buf[128];
+    byte *pc, *end;
+    instr_t *instr;
+
+    instr = XINST_CREATE_load(GD, opnd_create_reg(DR_REG_X0),
+                              OPND_CREATE_MEMPTR(DR_REG_X1, 0));
+    end = instr_encode(GD, instr, buf);
+    ASSERT(end - buf < BUFFER_SIZE_ELEMENTS(buf));
+    instr_destroy(GD, instr);
+
+    instr_noalloc_t noalloc;
+    instr_noalloc_init(GD, &noalloc);
+    pc = decode(GD, buf, &noalloc.instr);
+    ASSERT(pc != NULL);
+    ASSERT(opnd_get_reg(instr_get_dst(&noalloc, 0)) == DR_REG_X0);
+
+    instr_reset(GD, &noalloc.instr);
+    pc = decode(GD, buf, &noalloc.instr);
+    ASSERT(pc != NULL);
+    ASSERT(opnd_get_reg(instr_get_dst(&noalloc, 0)) == DR_REG_X0);
+
+    /* There should be no leak reported even w/o a reset b/c there's no
+     * extra heap.
+     */
+}
+
 int
 main()
 {
     test_disasm();
 
+    test_noalloc();
+
     printf("done\n");
 
     return 0;
diff --git a/suite/tests/api/drdecode_x86.c b/suite/tests/api/drdecode_x86.c
index f5a2f02a37..5aa5337c61 100644
--- a/suite/tests/api/drdecode_x86.c
+++ b/suite/tests/api/drdecode_x86.c
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2011-2018 Google, Inc.  All rights reserved.
+ * Copyright (c) 2011-2020 Google, Inc.  All rights reserved.
  * **********************************************************/
 
 /*
@@ -124,6 +124,35 @@ test_ptrsz_imm(void)
     instrlist_clear_and_destroy(GD, ilist);
 }
 
+static void
+test_noalloc(void)
+{
+    byte buf[128];
+    byte *pc, *end;
+    instr_t *instr;
+
+    instr = XINST_CREATE_load(GD, opnd_create_reg(DR_REG_XAX),
+                              OPND_CREATE_MEMPTR(DR_REG_XAX, 42));
+    end = instr_encode(GD, instr, buf);
+    ASSERT(end - buf < BUFFER_SIZE_ELEMENTS(buf));
+    instr_destroy(GD, instr);
+
+    instr_noalloc_t noalloc;
+    instr_noalloc_init(GD, &noalloc);
+    pc = decode(GD, buf, &noalloc.instr);
+    ASSERT(pc != NULL);
+    ASSERT(opnd_get_reg(instr_get_dst(&noalloc, 0)) == DR_REG_XAX);
+
+    instr_reset(GD, &noalloc.instr);
+    pc = decode(GD, buf, &noalloc.instr);
+    ASSERT(pc != NULL);
+    ASSERT(opnd_get_reg(instr_get_dst(&noalloc, 0)) == DR_REG_XAX);
+
+    /* There should be no leak reported even w/o a reset b/c there's no
+     * extra heap.
+     */
+}
+
 int
 main()
 {
@@ -133,6 +162,8 @@ main()
 
     test_ptrsz_imm();
 
+    test_noalloc();
+
     printf("done\n");
 
     return 0;
