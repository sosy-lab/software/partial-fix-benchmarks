diff --git a/api/docs/release.dox b/api/docs/release.dox
index 9ae29f264d..74889bc85a 100644
--- a/api/docs/release.dox
+++ b/api/docs/release.dox
@@ -268,6 +268,9 @@ Further non-compatibility-affecting changes include:
    handler.
  - Added atomics for safe and visible aligned loads and stores on all platforms:
    dr_atomic_load32(), dr_atomic_store32(), dr_atomic_load64() dr_atomic_store64().
+ - The state restore event (dr_register_restore_state_event()) is now called for
+   all translation attempts, even when the register state already contains
+   application values, to allow clients to restore memory.
 
 **************************************************
 <hr>
diff --git a/core/lib/instrument.c b/core/lib/instrument.c
index 8e554eb951..cc11897ba3 100644
--- a/core/lib/instrument.c
+++ b/core/lib/instrument.c
@@ -1816,6 +1816,44 @@ instrument_restore_state(dcontext_t *dcontext, bool restore_memory,
     return res;
 }
 
+/* The client may need to translate memory (e.g., DRWRAP_REPLACE_RETADDR using
+ * sentinel addresses outside of the code cache as targets) even when the register
+ * state already contains application values.
+ */
+bool
+instrument_restore_nonfcache_state_prealloc(dcontext_t *dcontext, bool restore_memory,
+                                            INOUT priv_mcontext_t *mcontext,
+                                            OUT dr_mcontext_t *client_mcontext)
+{
+    if (!dr_xl8_hook_exists())
+        return true;
+    dr_restore_state_info_t client_info;
+    dr_mcontext_init(client_mcontext);
+    priv_mcontext_to_dr_mcontext(client_mcontext, mcontext);
+    client_info.raw_mcontext = client_mcontext;
+    client_info.raw_mcontext_valid = true;
+    client_info.mcontext = client_mcontext;
+    client_info.fragment_info.tag = NULL;
+    client_info.fragment_info.cache_start_pc = NULL;
+    client_info.fragment_info.is_trace = false;
+    client_info.fragment_info.app_code_consistent = true;
+    bool res = instrument_restore_state(dcontext, restore_memory, &client_info);
+    dr_mcontext_to_priv_mcontext(mcontext, client_mcontext);
+    return res;
+}
+
+/* The large dr_mcontext_t on the stack makes a difference, so we provide two
+ * versions to avoid a double alloc on the same callstack.
+ */
+bool
+instrument_restore_nonfcache_state(dcontext_t *dcontext, bool restore_memory,
+                                   INOUT priv_mcontext_t *mcontext)
+{
+    dr_mcontext_t client_mcontext;
+    return instrument_restore_nonfcache_state_prealloc(dcontext, restore_memory, mcontext,
+                                                       &client_mcontext);
+}
+
 #    ifdef CUSTOM_TRACES
 /* Ask whether to end trace prior to adding next_tag fragment.
  * Return values:
diff --git a/core/lib/instrument.h b/core/lib/instrument.h
index 92e87eb458..f54b194582 100644
--- a/core/lib/instrument.h
+++ b/core/lib/instrument.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2010-2018 Google, Inc.  All rights reserved.
+ * Copyright (c) 2010-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2002-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -105,6 +105,13 @@ instrument_fragment_deleted(dcontext_t *dcontext, app_pc tag, uint flags);
 bool
 instrument_restore_state(dcontext_t *dcontext, bool restore_memory,
                          dr_restore_state_info_t *info);
+bool
+instrument_restore_nonfcache_state(dcontext_t *dcontext, bool restore_memory,
+                                   INOUT priv_mcontext_t *mcontext);
+bool
+instrument_restore_nonfcache_state_prealloc(dcontext_t *dcontext, bool restore_memory,
+                                            INOUT priv_mcontext_t *mcontext,
+                                            OUT dr_mcontext_t *client_mcontext);
 
 module_data_t *
 copy_module_area_to_module_data(const module_area_t *area);
diff --git a/core/lib/instrument_api.h b/core/lib/instrument_api.h
index ef8064986a..6f9dce9bd5 100644
--- a/core/lib/instrument_api.h
+++ b/core/lib/instrument_api.h
@@ -601,6 +601,10 @@ DR_API
  * if it is false, DR may only be querying for the address (\p
  * mcontext.pc) or register state and may not relocate this thread.
  *
+ * DR will call this event for all translation attempts, even when the
+ * register state already contains application values, to allow
+ * clients to restore memory.
+ *
  * The \p app_code_consistent parameter indicates whether the original
  * application code containing the instruction being translated is
  * guaranteed to still be in the same state it was when the code was
diff --git a/core/synch.c b/core/synch.c
index 59ef7a21d4..22f9f65524 100644
--- a/core/synch.c
+++ b/core/synch.c
@@ -449,23 +449,8 @@ translate_mcontext(thread_record_t *trec, priv_mcontext_t *mcontext, bool restor
             *mcontext = *get_mcontext(trec->dcontext);
 #ifdef CLIENT_INTERFACE
             if (dr_xl8_hook_exists()) {
-                /* The client may need to translate here if it's using sentinel
-                 * addresses outside of the code cache as targets.
-                 */
-                dr_restore_state_info_t client_info;
-                dr_mcontext_t client_mcontext;
-                dr_mcontext_init(&client_mcontext);
-                priv_mcontext_to_dr_mcontext(&client_mcontext, mcontext);
-                client_info.raw_mcontext = &client_mcontext;
-                client_info.raw_mcontext_valid = true;
-                client_info.mcontext = &client_mcontext;
-                client_info.fragment_info.tag = NULL;
-                client_info.fragment_info.cache_start_pc = NULL;
-                client_info.fragment_info.is_trace = false;
-                client_info.fragment_info.app_code_consistent = true;
-                if (!instrument_restore_state(trec->dcontext, true, &client_info))
+                if (!instrument_restore_nonfcache_state(trec->dcontext, true, mcontext))
                     return false;
-                dr_mcontext_to_priv_mcontext(mcontext, &client_mcontext);
             }
 #endif
             return true;
diff --git a/core/translate.c b/core/translate.c
index a073e8968e..512ceebe12 100644
--- a/core/translate.c
+++ b/core/translate.c
@@ -1066,6 +1066,12 @@ recreate_app_state_internal(dcontext_t *tdcontext, priv_mcontext_t *mcontext,
                             bool just_pc, fragment_t *owning_f, bool restore_memory)
 {
     recreate_success_t res = (just_pc ? RECREATE_SUCCESS_PC : RECREATE_SUCCESS_STATE);
+#ifdef CLIENT_INTERFACE
+    dr_mcontext_t xl8_mcontext;
+    dr_mcontext_t raw_mcontext;
+    dr_mcontext_init(&xl8_mcontext);
+    dr_mcontext_init(&raw_mcontext);
+#endif
 #ifdef WINDOWS
     if (get_syscall_method() == SYSCALL_METHOD_SYSENTER &&
         mcontext->pc == vsyscall_after_syscall && mcontext->xsp != 0) {
@@ -1080,6 +1086,13 @@ recreate_app_state_internal(dcontext_t *tdcontext, priv_mcontext_t *mcontext,
             /* no translation needed, ignoring sysenter stack hacks */
             LOG(THREAD_GET, LOG_INTERP | LOG_SYNCH, 2,
                 "recreate_app no translation needed (at vsyscall)\n");
+#    ifdef CLIENT_INTERFACE
+            if (dr_xl8_hook_exists()) {
+                if (!instrument_restore_nonfcache_state_prealloc(
+                        tdcontext, restore_memory, mcontext, &xl8_mcontext))
+                    return RECREATE_FAILURE;
+            }
+#    endif
             return res;
         } else {
             /* this is a dynamo system call! */
@@ -1129,6 +1142,13 @@ recreate_app_state_internal(dcontext_t *tdcontext, priv_mcontext_t *mcontext,
                 "recreate_app: restoring xdx (at sysenter)\n");
             mcontext->xdx = tdcontext->app_xdx;
         }
+#    endif
+#    ifdef CLIENT_INTERFACE
+        if (dr_xl8_hook_exists()) {
+            if (!instrument_restore_nonfcache_state_prealloc(tdcontext, restore_memory,
+                                                             mcontext, &xl8_mcontext))
+                return RECREATE_FAILURE;
+        }
 #    endif
         return res;
     }
@@ -1171,6 +1191,13 @@ recreate_app_state_internal(dcontext_t *tdcontext, priv_mcontext_t *mcontext,
         } else
 #endif
         mcontext->pc = POST_SYSCALL_PC(tdcontext);
+#ifdef CLIENT_INTERFACE
+        if (dr_xl8_hook_exists()) {
+            if (!instrument_restore_nonfcache_state_prealloc(tdcontext, restore_memory,
+                                                             mcontext, &xl8_mcontext))
+                return RECREATE_FAILURE;
+        }
+#endif
         return res;
     } else if (mcontext->pc == get_reset_exit_stub(tdcontext)) {
         LOG(THREAD_GET, LOG_INTERP | LOG_SYNCH, 2,
@@ -1178,6 +1205,13 @@ recreate_app_state_internal(dcontext_t *tdcontext, priv_mcontext_t *mcontext,
             tdcontext->next_tag);
         /* context is completely native except the pc */
         mcontext->pc = tdcontext->next_tag;
+#ifdef CLIENT_INTERFACE
+        if (dr_xl8_hook_exists()) {
+            if (!instrument_restore_nonfcache_state_prealloc(tdcontext, restore_memory,
+                                                             mcontext, &xl8_mcontext))
+                return RECREATE_FAILURE;
+        }
+#endif
         return res;
     } else if (in_generated_routine(tdcontext, mcontext->pc)) {
         LOG(THREAD_GET, LOG_INTERP | LOG_SYNCH, 2,
@@ -1203,10 +1237,6 @@ recreate_app_state_internal(dcontext_t *tdcontext, priv_mcontext_t *mcontext,
 #endif
 #ifdef CLIENT_INTERFACE
         dr_restore_state_info_t client_info;
-        dr_mcontext_t xl8_mcontext;
-        dr_mcontext_t raw_mcontext;
-        dr_mcontext_init(&xl8_mcontext);
-        dr_mcontext_init(&raw_mcontext);
 #endif
 #ifdef WINDOWS
         /* i#889: restore private PEB/TEB for faithful recreation */
diff --git a/suite/tests/client-interface/drwrap-test-detach.cpp b/suite/tests/client-interface/drwrap-test-detach.cpp
index 001c962ad6..80bfe4215a 100644
--- a/suite/tests/client-interface/drwrap-test-detach.cpp
+++ b/suite/tests/client-interface/drwrap-test-detach.cpp
@@ -64,7 +64,16 @@ extern "C" { /* Make it easy to get the name across platforms. */
 EXPORT void
 wrapped_subfunc(void)
 {
-    /* Empty. */
+#ifdef LINUX
+    /* Test non-fcache translation by making a syscall.
+     * Just easier to do on Linux so we do limit this to that OS.
+     * We don't want to do this on every invocation since we'll then never
+     * test the non-syscall translation.
+     */
+    static int syscall_sometimes = 0;
+    if (syscall_sometimes++ % 10 == 0 && getpid() == 0)
+        print("That's weird.\n"); /* Avoid the branch getting optimized away. */
+#endif
 }
 EXPORT void
 wrapped_func(void)
