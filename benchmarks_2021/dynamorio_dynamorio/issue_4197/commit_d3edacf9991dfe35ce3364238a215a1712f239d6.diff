diff --git a/api/docs/release.dox b/api/docs/release.dox
index ab844beb4f..9ae29f264d 100644
--- a/api/docs/release.dox
+++ b/api/docs/release.dox
@@ -260,6 +260,8 @@ Further non-compatibility-affecting changes include:
    application, but reduces overhead.
  - Added -record_dynsym_only to drcachesim for faster function tracing symbol
    lookups when internal symbols are not needed.
+ - Added -record_replace_retaddrd_only to drcachesim for faster function tracing
+   wrapping for well-behaved applications.
  - Added dr_merge_arith_flags() as a convenience routine to merge arithmetic flags
    for restoration done by outlined code.
  - Added dr_annotation_pass_pc() to obtain the interrupted PC in an annotation
diff --git a/clients/drcachesim/common/options.cpp b/clients/drcachesim/common/options.cpp
index 54319a743e..a67ef374f2 100644
--- a/clients/drcachesim/common/options.cpp
+++ b/clients/drcachesim/common/options.cpp
@@ -493,6 +493,14 @@ droption_t<bool> op_record_dynsym_only(
     "Symbol lookup can be expensive for large applications and libraries.  This option "
     " causes the symbol lookup for -record_function and -record_heap to look in the "
     " dynamic symbol table *only*.");
+droption_t<bool> op_record_replace_retaddr(
+    DROPTION_SCOPE_ALL, "record_replace_retaddr", false,
+    "Wrap by replacing retaddr for -record_function and -record_heap.",
+    "Function wrapping can be expensive for large concurrent applications.  This option "
+    "causes the post-function control point to be located using return address "
+    "replacement, which has lower overhead, but runs the risk of breaking an "
+    "application that examines or changes its own return addresses in the recorded "
+    "functions.");
 droption_t<unsigned int> op_miss_count_threshold(
     DROPTION_SCOPE_FRONTEND, "miss_count_threshold", 50000,
     "For cache miss analysis: minimum LLC miss count for a load to be eligible for "
diff --git a/clients/drcachesim/common/options.h b/clients/drcachesim/common/options.h
index 20c0b4b062..44bd268abb 100644
--- a/clients/drcachesim/common/options.h
+++ b/clients/drcachesim/common/options.h
@@ -125,6 +125,7 @@ extern droption_t<std::string> op_record_function;
 extern droption_t<bool> op_record_heap;
 extern droption_t<std::string> op_record_heap_value;
 extern droption_t<bool> op_record_dynsym_only;
+extern droption_t<bool> op_record_replace_retaddr;
 extern droption_t<unsigned int> op_miss_count_threshold;
 extern droption_t<double> op_miss_frac_threshold;
 extern droption_t<double> op_confidence_threshold;
diff --git a/clients/drcachesim/tests/burst_malloc.cpp b/clients/drcachesim/tests/burst_malloc.cpp
index d792102adc..a798ca7391 100644
--- a/clients/drcachesim/tests/burst_malloc.cpp
+++ b/clients/drcachesim/tests/burst_malloc.cpp
@@ -118,6 +118,8 @@ main(int argc, const char *argv[])
     if (!my_setenv("DYNAMORIO_OPTIONS",
                    "-stderr_mask 0xc -rstats_to_stderr"
                    " -client_lib ';;-offline -record_heap"
+                   // Test the low-overhead-wrapping option.
+                   " -record_replace_retaddr"
                    // Test large values that require two entries.
                    " -record_function \"malloc|1&return_big_value|1\"'"))
         std::cerr << "failed to set env var!\n";
diff --git a/clients/drcachesim/tracer/func_trace.cpp b/clients/drcachesim/tracer/func_trace.cpp
index 33369aca6f..af1d5bed28 100644
--- a/clients/drcachesim/tracer/func_trace.cpp
+++ b/clients/drcachesim/tracer/func_trace.cpp
@@ -263,8 +263,11 @@ instru_funcs_module_load(void *drcontext, const module_data_t *mod, bool loaded)
             NOTIFY(1, "Duplicate-pc hook: %s!%s == id %d\n", mod_name, f->name, id);
             continue;
         }
+        uint flags = 0;
+        if (!f->noret && op_record_replace_retaddr.get_value())
+            flags = DRWRAP_REPLACE_RETADDR;
         if (drwrap_wrap_ex(f_pc, func_pre_hook, f->noret ? nullptr : func_post_hook,
-                           (void *)(ptr_uint_t)id, 0)) {
+                           (void *)(ptr_uint_t)id, flags)) {
             NOTIFY(1, "Inserted hooks for %s!%s @" PFX " == id %d\n", mod_name, f->name,
                    f_pc, id);
         } else {
