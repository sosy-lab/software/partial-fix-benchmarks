diff --git a/core/fcache.c b/core/fcache.c
index ea79b11810..991a78796f 100644
--- a/core/fcache.c
+++ b/core/fcache.c
@@ -487,11 +487,11 @@ typedef struct _fcache {
      * not much of a space hit at all since there are 2 caches per thread
      * and then 2 global caches.
      */
-    uint max_size; /* maximum sum of sizes */
-    uint max_unit_size;
-    uint max_quadrupled_unit_size;
-    uint free_upgrade_size;
-    uint init_unit_size;
+    size_t max_size; /* maximum sum of sizes */
+    size_t max_unit_size;
+    size_t max_quadrupled_unit_size;
+    size_t free_upgrade_size;
+    size_t init_unit_size;
     bool finite_cache;
     uint regen_param;
     uint replace_param;
@@ -1525,11 +1525,10 @@ fcache_free_unit(dcontext_t *dcontext, fcache_unit_t *unit, bool dealloc_or_reus
 /* assuming size will either be aligned at VM_ALLOCATION_BOUNDARY or
  * smaller where no adjustment is necessary
  */
-#define FCACHE_GUARDED(size)                                     \
-    ((size) -                                                    \
-     ((DYNAMO_OPTION(guard_pages) &&                             \
-       ((size) >= VM_ALLOCATION_BOUNDARY - 2 * (uint)PAGE_SIZE)) \
-          ? (2 * (uint)PAGE_SIZE)                                \
+#define FCACHE_GUARDED(size)                                                             \
+    ((size) -                                                                            \
+     ((DYNAMO_OPTION(guard_pages) && ((size) >= VM_ALLOCATION_BOUNDARY - 2 * PAGE_SIZE)) \
+          ? (2 * PAGE_SIZE)                                                              \
           : 0))
 
 #define SET_CACHE_PARAMS(cache, which)                                                  \
@@ -2518,7 +2517,7 @@ static bool
 try_for_more_space(dcontext_t *dcontext, fcache_t *cache, fcache_unit_t *unit,
                    uint slot_size)
 {
-    uint commit_size = DYNAMO_OPTION(cache_commit_increment);
+    size_t commit_size = DYNAMO_OPTION(cache_commit_increment);
     ASSERT(CACHE_PROTECTED(cache));
 
     if (unit->end_pc < unit->reserved_end_pc &&
@@ -2529,8 +2528,7 @@ try_for_more_space(dcontext_t *dcontext, fcache_t *cache, fcache_unit_t *unit,
         while (unit->cur_pc + slot_size > unit->end_pc + commit_size)
             commit_size *= 2;
         if (unit->end_pc + commit_size > unit->reserved_end_pc) {
-            ASSERT_TRUNCATE(commit_size, uint, unit->reserved_end_pc - unit->end_pc);
-            commit_size = (uint)(unit->reserved_end_pc - unit->end_pc);
+            commit_size = unit->reserved_end_pc - unit->end_pc;
         }
         cache_extend_commitment(unit, commit_size);
         if (unit->cur_pc + slot_size > unit->end_pc) {
diff --git a/core/heap.c b/core/heap.c
index e58554ffe6..6cc378ef25 100644
--- a/core/heap.c
+++ b/core/heap.c
@@ -1119,7 +1119,7 @@ vmm_is_reserved_unit(vm_heap_t *vmh, vm_addr_t p, size_t size)
     ASSERT(CHECK_TRUNCATE_TYPE_uint(size / DYNAMO_OPTION(vmm_block_size)));
     ASSERT(bitmap_are_reserved_blocks(vmh->blocks, vmh->num_blocks,
                                       vmm_addr_to_block(vmh, p),
-                                      (uint)size / DYNAMO_OPTION(vmm_block_size)));
+                                      (uint)(size / DYNAMO_OPTION(vmm_block_size))));
     return true;
 }
 
@@ -1380,7 +1380,7 @@ vmm_heap_reserve_blocks(vm_heap_t *vmh, size_t size_in, byte *base, which_vmm_t
 
     size = ALIGN_FORWARD(size_in, DYNAMO_OPTION(vmm_block_size));
     ASSERT_TRUNCATE(request, uint, size / DYNAMO_OPTION(vmm_block_size));
-    request = (uint)size / DYNAMO_OPTION(vmm_block_size);
+    request = (uint)(size / DYNAMO_OPTION(vmm_block_size));
 
     if (base != NULL)
         must_start = vmm_addr_to_block(vmh, base);
@@ -1437,7 +1437,7 @@ vmm_heap_free_blocks(vm_heap_t *vmh, vm_addr_t p, size_t size_in, which_vmm_t wh
 
     size = ALIGN_FORWARD(size_in, DYNAMO_OPTION(vmm_block_size));
     ASSERT_TRUNCATE(request, uint, size / DYNAMO_OPTION(vmm_block_size));
-    request = (uint)size / DYNAMO_OPTION(vmm_block_size);
+    request = (uint)(size / DYNAMO_OPTION(vmm_block_size));
 
     LOG(GLOBAL, LOG_HEAP, 2, "vmm_heap_free_blocks %s: size=%d blocks=%d p=" PFX "\n",
         vmh->name, size, request, p);
@@ -1892,14 +1892,14 @@ vmh_exit(vm_heap_t *vmh, bool contains_stacks)
      * stack, global_do_syscall.
      */
     DOCHECK(1, {
-        uint perstack =
-            ALIGN_FORWARD_UINT(
+        uint perstack = (uint)
+            (ALIGN_FORWARD_UINT(
                 DYNAMO_OPTION(stack_size) +
                     (DYNAMO_OPTION(guard_pages)
                          ? (2 * PAGE_SIZE)
                          : (DYNAMO_OPTION(stack_guard_pages) ? PAGE_SIZE : 0)),
                 DYNAMO_OPTION(vmm_block_size)) /
-            DYNAMO_OPTION(vmm_block_size);
+             DYNAMO_OPTION(vmm_block_size));
         uint unfreed_blocks;
         if (!contains_stacks IF_CLIENT_INTERFACE(|| standalone_library))
             unfreed_blocks = 0;
diff --git a/core/module_list.c b/core/module_list.c
index 1a133547e0..49b595754c 100644
--- a/core/module_list.c
+++ b/core/module_list.c
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2011-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2011-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2008-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -737,7 +737,7 @@ restore_unreadable_section(app_pc module_base, app_pc seg_start, size_t seg_len,
 void
 module_calculate_digest(OUT module_digest_t *digest, app_pc module_base,
                         size_t module_size, bool full_digest, bool short_digest,
-                        uint short_digest_size, uint sec_char_include,
+                        size_t short_digest_size, uint sec_char_include,
                         uint sec_char_exclude)
 {
     struct MD5Context md5_short_cxt;
diff --git a/core/module_shared.h b/core/module_shared.h
index 4d2dec4cd2..1bb98acb11 100644
--- a/core/module_shared.h
+++ b/core/module_shared.h
@@ -464,7 +464,7 @@ typedef struct {
 void
 module_calculate_digest(/*OUT*/ module_digest_t *digest, app_pc module_base,
                         size_t module_size, bool full_digest, bool short_digest,
-                        uint short_digest_size, uint sec_char_include,
+                        size_t short_digest_size, uint sec_char_include,
                         uint sec_char_exclude);
 
 /* actually in utils.c since os-independent */
diff --git a/core/options.c b/core/options.c
index e5895f966c..5d631d9f51 100644
--- a/core/options.c
+++ b/core/options.c
@@ -122,7 +122,7 @@ typedef enum option_modifier_t {
     OPTION_MOD_DYNAMIC
 } option_modifier_t;
 
-typedef uint uint_size;
+typedef ptr_uint_t uint_size;
 typedef uint uint_time;
 typedef ptr_uint_t uint_addr;
 
@@ -153,6 +153,36 @@ const internal_options_t default_internal_options = {
 #    undef OPTION_COMMAND
 #endif
 
+/* Definitions for our default values.  See options.h for why we can't
+ * have them in an enum in the header.
+ */
+#undef OPTION_STRING
+#undef EMPTY_STRING
+/* We don't have strings in the default values. */
+#define OPTION_STRING(x) 0
+#define EMPTY_STRING 0
+#define liststring_t int
+#define pathstring_t int
+#define OPTION_COMMAND(type, name, default_value, command_line_option, statement, \
+                       description, flag, pcache)                                 \
+    const type OPTION_DEFAULT_VALUE_##name = default_value;
+#define OPTION_COMMAND_INTERNAL(type, name, default_value, command_line_option, \
+                                statement, description, flag, pcache)           \
+    const type OPTION_DEFAULT_VALUE_##name = default_value;
+#include "optionsx.h"
+#undef liststring_t
+#undef pathstring_t
+#undef OPTION_COMMAND
+#undef OPTION_COMMAND_INTERNAL
+#undef OPTION_STRING
+#undef EMPTY_STRING
+/* Restore. */
+#define OPTION_STRING(x) x
+#define EMPTY_STRING \
+    {                \
+        0            \
+    }
+
 #ifdef EXPOSE_INTERNAL_OPTIONS
 #    define OPTION_COMMAND_INTERNAL OPTION_COMMAND
 #else
@@ -416,13 +446,13 @@ parse_uint(uint *var, void *value)
 }
 
 static void
-parse_uint_size(uint *var, void *value)
+parse_uint_size(ptr_uint_t *var, void *value)
 {
     char unit;
-    int num;
-    int factor = 0;
+    ptr_int_t num;
+    ptr_int_t factor = 0;
 
-    if (sscanf((char *)value, "%d%c", &num, &unit) == 1) {
+    if (sscanf((char *)value, SZFMT "%c", &num, &unit) == 1) {
         /* no unit specifier, default unit is Kilo for compatibility */
         factor = 1024;
     } else {
@@ -434,7 +464,7 @@ parse_uint_size(uint *var, void *value)
         case 'M': // Mega (bytes)
         case 'm': factor = 1024 * 1024; break;
         case 'G': // Giga (bytes)
-        case 'g': factor = 1024 * 1024 * 1024; break;
+        case 'g': factor = 1024ULL * 1024 * 1024; break;
         default:
             /* var should be pre-initialized to default */
             OPTION_PARSE_ERROR(ERROR_OPTION_UNKNOWN_SIZE_SPECIFIER, 4,
@@ -674,20 +704,21 @@ set_dynamo_options_other_process(options_t *options, const char *optstr)
  * option value
  */
 bool
-check_param_bounds(uint *val, uint min, uint max, const char *name)
+check_param_bounds(ptr_uint_t *val, ptr_uint_t min, ptr_uint_t max, const char *name)
 {
     bool ret = false;
-    uint new_val;
+    ptr_uint_t new_val;
     if ((max == 0 && *val != 0 && *val < min) ||
         (max > 0 && (*val < min || *val > max))) {
         if (max == 0) {
             new_val = min;
-            USAGE_ERROR("%s must be >= %u, resetting from %u to %u", name, min, *val,
-                        new_val);
+            USAGE_ERROR("%s must be >= " SZFMT ", resetting from " SZFMT " to " SZFMT,
+                        name, min, *val, new_val);
         } else {
             new_val = max;
-            USAGE_ERROR("%s must be >= %u and <= %u, resetting from %u to %u", name, min,
-                        max, *val, new_val);
+            USAGE_ERROR("%s must be >= " SZFMT " and <= " SZFMT ", resetting from " SZFMT
+                        " to " SZFMT,
+                        name, min, max, *val, new_val);
         }
         *val = new_val;
         ret = true;
@@ -696,7 +727,7 @@ check_param_bounds(uint *val, uint min, uint max, const char *name)
         if (*val == 0) {
             LOG(GLOBAL, LOG_CACHE, 1, "%s: <unlimited>\n", name);
         } else {
-            LOG(GLOBAL, LOG_CACHE, 1, "%s: %u KB\n", name, *val / 1024);
+            LOG(GLOBAL, LOG_CACHE, 1, "%s: " SZFMT " KB\n", name, *val / 1024);
         }
     });
     return ret;
@@ -728,9 +759,19 @@ PRINT_STRING_uint(char *optionbuff, const void *val_ptr, const char *option)
 static void
 PRINT_STRING_uint_size(char *optionbuff, const void *val_ptr, const char *option)
 {
-    uint value = *(const uint *)val_ptr;
-    snprintf(optionbuff, MAX_OPTION_LENGTH, "-%s %d%s ", option,
-             (value % 1024 == 0 ? value / 1024 : value), (value % 1024 == 0 ? "K" : "B"));
+    ptr_uint_t value = *(const ptr_uint_t *)val_ptr;
+    char code = 'B';
+    if (value >= 1024ULL * 1024 * 1024 && value % 1024ULL * 1024 * 1024 == 0) {
+        value = value / (1024ULL * 1024 * 1024);
+        code = 'G';
+    } else if (value >= 1024 * 1024 && value % 1024 * 1024 == 0) {
+        value = value / (1024 * 1024);
+        code = 'M';
+    } else if (value >= 1024 && value % 1024 == 0) {
+        value = value / 1024;
+        code = 'K';
+    }
+    snprintf(optionbuff, MAX_OPTION_LENGTH, "-%s " SZFMT "%c ", option, value, code);
 }
 static void
 PRINT_STRING_uint_time(char *optionbuff, const void *val_ptr, const char *option)
@@ -799,7 +840,9 @@ DIFF_uint(const void *ptr1, const void *ptr2)
 static int
 DIFF_uint_size(const void *ptr1, const void *ptr2)
 {
-    return DIFF_uint(ptr1, ptr2);
+    ptr_uint_t val1 = *(const ptr_uint_t *)(ptr1);
+    ptr_uint_t val2 = *(const ptr_uint_t *)(ptr2);
+    return val1 != val2;
 }
 static int
 DIFF_uint_time(const void *ptr1, const void *ptr2)
@@ -809,7 +852,7 @@ DIFF_uint_time(const void *ptr1, const void *ptr2)
 static int
 DIFF_uint_addr(const void *ptr1, const void *ptr2)
 {
-    return DIFF_uint(ptr1, ptr2);
+    return DIFF_uint_size(ptr1, ptr2);
 }
 static int
 DIFF_pathstring_t(const void *ptr1, const void *ptr2)
@@ -854,7 +897,7 @@ COPY_uint(void *ptr1, const void *ptr2)
 static void
 COPY_uint_size(void *ptr1, const void *ptr2)
 {
-    COPY_uint(ptr1, ptr2);
+    *(ptr_uint_t *)(ptr1) = *(const ptr_uint_t *)(ptr2);
 }
 static void
 COPY_uint_time(void *ptr1, const void *ptr2)
@@ -864,7 +907,7 @@ COPY_uint_time(void *ptr1, const void *ptr2)
 static void
 COPY_uint_addr(void *ptr1, const void *ptr2)
 {
-    *(ptr_uint_t *)(ptr1) = *(const ptr_uint_t *)(ptr2);
+    COPY_uint_size(ptr1, ptr2);
 }
 static void
 COPY_pathstring_t(void *ptr1, const void *ptr2)
@@ -2679,6 +2722,19 @@ unit_test_options(void)
     set_dynamo_options_defaults(&dynamo_options);
     get_dynamo_options_string(&dynamo_options, buf, MAXIMUM_PATH, 1);
     print_file(STDERR, "default ops string: %s\n", buf);
+
+#    ifdef X64
+    /* Sanity-check pointer-sized integer values handling >int sizes. */
+    set_dynamo_options(&dynamo_options, "-vmheap_size 16384M -stack_size 64K");
+    EXPECT_EQ(dynamo_options.vmheap_size, 8 * 1024 * 1024 * 1024ULL);
+    char opstring[MAX_OPTIONS_STRING];
+    /* Ensure we print it back out with the shortest value+suffix.
+     * We include -stack_size to avoid printing out "0G" or sthg.
+     */
+    get_dynamo_options_string(&dynamo_options, opstring, sizeof(opstring), true);
+    EXPECT_EQ(0, strcmp(opstring, "-stack_size 64K -vmheap_size 16G "));
+#    endif
+
     SELF_PROTECT_OPTIONS();
     d_r_write_unlock(&options_lock);
 }
diff --git a/core/options.h b/core/options.h
index b023f71995..51bcc54ea8 100644
--- a/core/options.h
+++ b/core/options.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2012-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2012-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2003-2009 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -111,11 +111,7 @@ enum {
 };
 
 /* for all option uses */
-/* N.B.: for 64-bit should we make the uint_size options be size_t?
- * For now we're ok with all such options maxing out at 4GB, and in fact
- * some like the fcache options are assumed to not be larger.
- */
-#define uint_size uint
+#define uint_size ptr_uint_t
 #define uint_time uint
 /* So far all addr_t are external so we don't have a 64-bit problem */
 #define uint_addr ptr_uint_t
@@ -156,17 +152,34 @@ typedef enum {
 #define EMPTY_STRING 0     /* no string in enum */
 #define OPTION_COMMAND(type, name, default_value, command_line_option, statement, \
                        description, flag, pcache)                                 \
-    OPTION_DEFAULT_VALUE_##name = (ptr_uint_t)default_value,                      \
     OPTION_IS_INTERNAL_##name = false, OPTION_IS_STRING_##name = ISSTRING_##type, \
     OPTION_AFFECTS_PCACHE_##name = pcache,
 #define OPTION_COMMAND_INTERNAL(type, name, default_value, command_line_option,  \
                                 statement, description, flag, pcache)            \
-    OPTION_DEFAULT_VALUE_##name = (ptr_uint_t)default_value,                     \
     OPTION_IS_INTERNAL_##name = true, OPTION_IS_STRING_##name = ISSTRING_##type, \
     OPTION_AFFECTS_PCACHE_##name = pcache,
 enum option_is_internal {
 #include "optionsx.h"
 };
+/* We have to make the default values not part of the enum since MSVC uses
+ * "long" as the enum type and we'd have truncation of large values.
+ * Instead we have to declare constant variables.
+ * Hopefully the compiler will treat as constants and optimize away any
+ * memory references.
+ */
+#undef OPTION_COMMAND
+#undef OPTION_COMMAND_INTERNAL
+#define liststring_t int
+#define pathstring_t int
+#define OPTION_COMMAND(type, name, default_value, command_line_option, statement, \
+                       description, flag, pcache)                                 \
+    extern const type OPTION_DEFAULT_VALUE_##name;
+#define OPTION_COMMAND_INTERNAL(type, name, default_value, command_line_option, \
+                                statement, description, flag, pcache)           \
+    extern const type OPTION_DEFAULT_VALUE_##name;
+#include "optionsx.h"
+#undef liststring_t
+#undef pathstring_t
 #undef OPTION_COMMAND
 #undef OPTION_COMMAND_INTERNAL
 #undef OPTION_STRING
@@ -255,7 +268,7 @@ extern const internal_options_t default_internal_options;
  * option value
  */
 bool
-check_param_bounds(uint *val, uint min, uint max, const char *name);
+check_param_bounds(ptr_uint_t *val, ptr_uint_t min, ptr_uint_t max, const char *name);
 
 int
 options_init(void);
diff --git a/core/utils.h b/core/utils.h
index cd42a41133..87740b24e1 100644
--- a/core/utils.h
+++ b/core/utils.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2010-2019 Google, Inc.  All rights reserved.
+ * Copyright (c) 2010-2020 Google, Inc.  All rights reserved.
  * Copyright (c) 2000-2010 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -2361,6 +2361,12 @@ profile_callers_exit(void);
             EXPECT_RELATION_INTERNAL(#expr, value_once, ==, expected); \
         } while (0)
 
+#    define EXPECT_EQ(expr, expected)                                  \
+        do {                                                           \
+            ptr_uint_t value_once = (ptr_uint_t)(expr);                \
+            EXPECT_RELATION_INTERNAL(#expr, value_once, ==, expected); \
+        } while (0)
+
 #    define EXPECT_NE(expr, expected)                                  \
         do {                                                           \
             ptr_uint_t value_once = (ptr_uint_t)(expr);                \
