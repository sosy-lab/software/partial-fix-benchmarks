i#4423: Split VMM stats into reachable vs unreachable (#4429)

Given the vmcode vs vmheap split and completely different size limits
on the two, the VMM stats need to be split to be useful.  Here we
split the heap, special heap, and special mmap stats into reachable
and unreachable versions.  We also split the public versions in
dr_statistics_t, renaming and reordering fields, which is not
considered a compatibility break because we are in between releases.

Additionally tested on a large AArch64 app where the stats used to
look something like this:
                          Peak vmm blocks for heap :             14679
                         Peak vmm blocks for cache :              5139
                         Peak vmm blocks for stack :              9039
                  Peak vmm blocks for special heap :              4518
                  Peak vmm blocks for special mmap :              9043
Now they are more useful as this (different params so not perfect
numeric comparison) to see that vmcode is using 470M while vmheap is
at 2120M:
              Peak vmm blocks for unreachable heap :              8401
                         Peak vmm blocks for stack :             14586
      Peak vmm blocks for unreachable special heap :              3646
      Peak vmm blocks for unreachable special mmap :              7292
                Peak vmm blocks for reachable heap :              3650
                         Peak vmm blocks for cache :              3863
        Peak vmm blocks for reachable special mmap :                 5

Fixes #4423
i#2607: Externalize guard pages from units

Decouples guard page use from cache and heap unit size.

Changes the Windows -vmm_block_size to the page size so that there is
nothing special about 64K for 4K pages.  The logic is that 99% of our
usage stays within the VMM reservations these days, so it is not worth
the complexity of dealing with 64K sizing for 4K pages.

Removes the guard page adjustments to cache and heap units.  Now the
specified sizes are the actual usable sizes, with any guard pages
added externally and not considered as part of the unit size.

Adjusts the default heap unit sizes down by 8K to maintain the same
sizing as before.

Leaves the default cache unit sizes how they were.  The sub-64K did
*not* have 8K subtracted and remain small.  The 64K will now occupy
72K, but shrinking to 56K is complicated by the quadrupling scheme
from 4K to 16K to 64K which previously ended up at 56K.

Issue: #2607, #4424
Fixes #2607
i#2607: Externalize guard pages from units (#4430)

Decouples guard page use from cache and heap unit sizes.

Changes the Windows -vmm_block_size to the page size so that there is
nothing special about 64K for 4K pages.  The logic is that 99% of our
usage stays within the VMM reservations these days, so it is not worth
the complexity of dealing with 64K sizing for 4K pages.

Removes the guard page adjustments to cache and heap units.  Now the
specified sizes are the actual usable sizes, with any guard pages
added externally and not considered as part of the unit size.

Adjusts the default heap unit and cache unit sizes down by 8K to maintain
the same sizing as before.

Issue: #2607, #4424, #4433
Fixes #2607
i#4424: Adjust guard pages and unit sizes for >4K pages

Adds a new option -per_thread_guard_pages controlling whether
per-thread allocations have guard pages.  This option is turned off by
default for >4K pages, removing guard pages from thread-private
initial cache units, all thread-private heap units, thread-private
signal-pending special heap units, stacks, TLS segments (DR and
privlib), and thread-private gencode.  This saves a lot of space on
many-threaded apps, while maintaining guards throughout the VMM from
shared units, which are used far more often than private these days.

Implements the option with a new VMM flag VMM_PER_THREAD, added to the
reservation calls for the above list of unit types.

Additionally, for >4K pages, tunes the default unit sizes to take into
account the guard changes.  Increases the shared unit sizes further
for a better ratio of content to guard pages.

Tested on a large app on a 64K-page machine which originally hit OOM
without "-no_guard_pages" before (along with other now-fixed bugs:
i#4335; i#4418).  Here are some stats for a smaller version of the app
that is easier to capture statistics for:

-no_guard_pages:
              Peak threads under DynamoRIO control :               152
                              Threads ever created :               701
              Peak vmm blocks for unreachable heap :               914
                         Peak vmm blocks for stack :               610
      Peak vmm blocks for unreachable special heap :               152
      Peak vmm blocks for unreachable special mmap :               304
                Peak vmm blocks for reachable heap :               161
                         Peak vmm blocks for cache :               368
        Peak vmm blocks for reachable special mmap :                 5
            Peak vmm virtual memory in use (bytes) :         164757504

Here's this PR with -per_thread_guard_pages (i.e., overriding the new
default: so matching the HEAD defaults):
              Peak threads under DynamoRIO control :               157
                              Threads ever created :               706
              Peak vmm blocks for unreachable heap :              1690
                         Peak vmm blocks for stack :               945
      Peak vmm blocks for unreachable special heap :               471
      Peak vmm blocks for unreachable special mmap :               942
                Peak vmm blocks for reachable heap :               482
                         Peak vmm blocks for cache :               741
        Peak vmm blocks for reachable special mmap :                 7
            Peak vmm virtual memory in use (bytes) :         345899008

Vs the new defaults in this PR:
              Peak threads under DynamoRIO control :               141
                              Threads ever created :               701
              Peak vmm blocks for unreachable heap :               884
                         Peak vmm blocks for stack :               566
      Peak vmm blocks for unreachable special heap :               141
      Peak vmm blocks for unreachable special mmap :               282
                Peak vmm blocks for reachable heap :               152
                         Peak vmm blocks for cache :               411
        Peak vmm blocks for reachable special mmap :                 7
            Peak vmm virtual memory in use (bytes) :         160104448

Fixes #4424
i#4424: Adjust guard pages and unit sizes for >4K pages (#4441)

Adds a new option -per_thread_guard_pages controlling whether
per-thread allocations have guard pages.  This option is turned off by
default for >4K pages, removing guard pages from thread-private
initial cache units, all thread-private heap units, thread-private
signal-pending special heap units, stacks, TLS segments (DR and
privlib), and thread-private gencode.  This saves a lot of space on
many-threaded apps, while maintaining guards throughout the VMM from
shared units, which are used far more often than private these days.

Implements the option with a new VMM flag VMM_PER_THREAD, added to the
reservation calls for the above list of unit types.

Additionally, for >4K pages, tunes the default unit sizes to take into
account the guard changes.  Increases the shared unit sizes further
for a better ratio of content to guard pages.

Tested on a large app on a 64K-page machine which originally hit OOM
without "-no_guard_pages" before (along with other now-fixed bugs:
i#4335; i#4418).  Here are some stats for a smaller version of the app
that is easier to capture statistics for:

-no_guard_pages:
              Peak threads under DynamoRIO control :               152
                              Threads ever created :               701
              Peak vmm blocks for unreachable heap :               914
                         Peak vmm blocks for stack :               610
      Peak vmm blocks for unreachable special heap :               152
      Peak vmm blocks for unreachable special mmap :               304
                Peak vmm blocks for reachable heap :               161
                         Peak vmm blocks for cache :               368
        Peak vmm blocks for reachable special mmap :                 5
            Peak vmm virtual memory in use (bytes) :         164757504

Here's this PR with -per_thread_guard_pages (i.e., overriding the new
default: so matching the HEAD defaults):
              Peak threads under DynamoRIO control :               157
                              Threads ever created :               706
              Peak vmm blocks for unreachable heap :              1690
                         Peak vmm blocks for stack :               945
      Peak vmm blocks for unreachable special heap :               471
      Peak vmm blocks for unreachable special mmap :               942
                Peak vmm blocks for reachable heap :               482
                         Peak vmm blocks for cache :               741
        Peak vmm blocks for reachable special mmap :                 7
            Peak vmm virtual memory in use (bytes) :         345899008

Vs the new defaults in this PR:
              Peak threads under DynamoRIO control :               141
                              Threads ever created :               701
              Peak vmm blocks for unreachable heap :               884
                         Peak vmm blocks for stack :               566
      Peak vmm blocks for unreachable special heap :               141
      Peak vmm blocks for unreachable special mmap :               282
                Peak vmm blocks for reachable heap :               152
                         Peak vmm blocks for cache :               411
        Peak vmm blocks for reachable special mmap :                 7
            Peak vmm virtual memory in use (bytes) :         160104448

Issue: #4424, #4335, #4418
Fixes #4424
