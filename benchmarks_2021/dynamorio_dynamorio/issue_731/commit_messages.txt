i#4014 offline phys: Exit up front on -offline -use_physical (#4015)

In drcachesim, the combination of -offline and -use_physical is not
supported at this time.  We make that clear in the option docs and
with an up-front exit when the two are requested at once.

Issue: #4014
i#731,i#3271: Keep IR cti+copied bits valid and re-relativized

Adds rip-relative information tracking at IR levels 1-3 for
instruction references on x86, extending the existing tracking for
data references.

Adds decode_sizeof_ex() and instr_get_rel_data_or_instr_target() to
support rip-relative displacement and target handling.

Changes decode_from_copy() to preserve raw bits for all instructions,
eliminating problems where untracked encoding features are lost such
as in #4017.

Documents the changes and describes how to avoid the new behavior.
Does just that for intra-sequence cti's in the rseq native code copy.

Adds test cases to api.ir.

Implementing the same thing for AArchXX is left unimplemented, tracked
by i#4016.

Issue: #731, #3271, #3339, #4016, #4017
Fixes #731
Fixes #3271
Fixes #4017
i#731,i#3271: Keep IR cti+copied bits valid and re-relativized (#4018)

Adds rip-relative information tracking at IR levels 1-3 for
instruction references on x86, extending the existing tracking for
data references.

Adds decode_sizeof_ex() and instr_get_rel_data_or_instr_target() to
support rip-relative displacement and target handling.

Changes decode_from_copy() to preserve raw bits for all instructions,
eliminating problems where untracked encoding features are lost such
as in #4017.

Documents the changes and describes how to avoid the new behavior.
Does just that for intra-sequence cti's in the rseq native code copy and
Windows syscall wrapper copies.

Adds test cases to api.ir.

Implementing the same thing for AArchXX is left unimplemented, tracked
by i#4016.

Issue: #731, #3271, #3339, #4016, #4017
Fixes #731
Fixes #3271
Fixes #4017
i#731 re-rel: Convert native rseq PC targets to instrs

For i#731 with automatic re-relativization of absolute PC's, in
d6f5fca we simply kept the hardcoded offset for intra-region branch
targets in our native rseq copy.  However, with subsequent mangling
that offset can become incorrect and target the middle of an
instruction, leading to a crash.  We instead take the time to convert
these PC targets to instr_t* targets.

We also tweak the disassembly output to show the instr_t pointer value
for level 3 instructions too, since jumps can target them as well as
synthetic instructions.  This helped with verifying and debugging this
change.

Tested on an inserted system call for locally forcing rseq restarts,
which leads to system call mangling and crashes without this fix.

Issue: #731, #2350
i#731 re-rel: Convert native rseq PC targets to instrs (#4023)

For i#731 with automatic re-relativization of absolute PC's, in
d6f5fca we simply kept the hardcoded offset for intra-region branch
targets in our native rseq copy.  However, with subsequent mangling
that offset can become incorrect and target the middle of an
instruction, leading to a crash.  We instead take the time to convert
these PC targets to instr_t* targets.

We also tweak the disassembly output to show the instr_t pointer value
for level 3 instructions too, since jumps can target them as well as
synthetic instructions.  This helped with verifying and debugging this
change.

Tested on an inserted system call for locally forcing rseq restarts,
which leads to system call mangling and crashes without this fix.

Issue: #731, #2350
