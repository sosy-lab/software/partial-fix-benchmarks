auto-re-relativize level 1-3 ctis when encoding
_From [bruen...@google.com](https://code.google.com/u/109494838902877177630/) on April 12, 2012 11:15:43_

this was PR 253447 and you can see my old FIXME comment about it in encode.c about a plan to extend the rip-rel auto-re-encode machinery I put in place for x64 via INSTR_RIP_REL_VALID and instr_t.rip_rel_pos.  here's the comment:

```
/* FIXME PR 253447: if want to support ctis as well, need
 * instr->rip_rel_disp_sz and need to set both for non-x64 as well
 * in decode_sizeof(): or only in decode_cti()?
 */
```

_From [bruen...@google.com](https://code.google.com/u/109494838902877177630/) on April 12, 2012 11:16:57_

changing to match original title

**Summary:** auto-re-relativize level 1-3 ctis when encoding  

_From [bruen...@google.com](https://code.google.com/u/109494838902877177630/) on November 15, 2012 13:04:55_

for the next release we are documenting this under the standalone section of the docs.  be sure to remove that when we add the real fix.

I plan to implement this for x86.  AArchXX is covered by #4016.