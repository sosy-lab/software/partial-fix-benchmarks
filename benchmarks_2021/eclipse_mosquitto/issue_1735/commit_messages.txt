add a refcount to library init/cleanup

Add a refcount around mosquitto_lib_init and mosquitto_lib_cleanup so
that multiple calls to init/cleanup don't trigger memory leaks or
double-frees.

Signed-off-by: Martin Kelly <mkelly@xevo.com>
DLT logging is now configurable at runtime with `log_dest dlt`.

Closes #1735. Thanks to Brian Orpin.
Don't try to start DLT logging if DLT unavailable.

This is to avoid a long delay when shutting down the broker.

Closes #1735. Thanks to Colin Law.
Don't try to start DLT logging if DLT unavailable.

This is to avoid a long delay when shutting down the broker.

Closes #1735. Thanks to Colin Law.
