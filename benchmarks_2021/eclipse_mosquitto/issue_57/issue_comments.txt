Bridge connection enters a connect-disconnect loop when incomplete QoS 2 publish, and local broker fails to persist for any reason.
We see the same in our production environment with the latest version.

Unfortunately this seems endemic in MQTT implementations: see also https://github.com/eclipse/paho.mqtt.java/issues/27 for the same bug in the java client

Under the right circumstances this happens for most/all acknowledgements so PUBCOMP and PUBACK can probably trigger the same behaviour (Possibly SUBACKs as well)

Thanks for the nudge, I believe this is now fixed.
