Merge branch 'skip-openssl-init' of https://github.com/loigu/paho.mqtt.c into fixes #136

Conflicts:
	src/MQTTAsync.c
	src/MQTTClient.c
	src/MQTTClient.h
	src/SSLSocket.c
	src/SSLSocket.h
Reduce keepalive check interval for low keepalive values #156
Merge branch 'develop' into feature/installdir

* develop: (41 commits)
  Updates the RPM packaging makefile
  Stop mosquitto at end of Travis build
  Reduce keepalive check interval for low keepalive values #156
  Added RPM spec for Paho C
  Add REVERSED bitfield definition for big endian processors #212
  Some cppcheck complaint fixes #36
  Some CMake cleanup & correction #223
  Remove const from eyecatchers #168
  Fix for issue #244
  Make fix for issue #186 work on Windows too
  Fix for issue #186
  Actual fix for issue #179
  Fix for issue #179
  Update README to refer to Github for bug raising
  Reduce log verbosity for test 2d
  Fix for issue #190
  Non-SSL version of the library report an error if the caller requests an SSL connection. Possible soluiton for Issue #251
  Compile static ssl libraries with OPENSSL=1.
  Extend the 'MQTTAsync_willOptions_initializer' to initialize the new binary payloads fields
  In test5 - 2d the client must not have the valid client certificate!
  ...

Signed-off-by: Pedro Luis Castedo Cepeda <pedroluis.castedo@upm.es>
Merge branch 'develop' into installdir

* develop: (32 commits)
  Updates the RPM packaging makefile
  Stop mosquitto at end of Travis build
  Reduce keepalive check interval for low keepalive values #156
  Added RPM spec for Paho C
  Add REVERSED bitfield definition for big endian processors #212
  Some cppcheck complaint fixes #36
  Some CMake cleanup & correction #223
  Remove const from eyecatchers #168
  Fix for issue #244
  Make fix for issue #186 work on Windows too
  Fix for issue #186
  Actual fix for issue #179
  Fix for issue #179
  Update README to refer to Github for bug raising
  Reduce log verbosity for test 2d
  Fix for issue #190
  Non-SSL version of the library report an error if the caller requests an SSL connection. Possible soluiton for Issue #251
  Compile static ssl libraries with OPENSSL=1.
  Extend the 'MQTTAsync_willOptions_initializer' to initialize the new binary payloads fields
  In test5 - 2d the client must not have the valid client certificate!
  ...
