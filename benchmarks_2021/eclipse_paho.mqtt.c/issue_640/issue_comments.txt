Don't remove queued messages when cleaning state
It's reasonable that the client library should do it.
Actually, reading this again.  If there are inflight messages (ones for which an ack has not been received) then these should be removed if session_present = 0, as there is a session state mismatch.  There is a difference with queued up messages that have never been sent, though.  These should not be removed.
Thanks @icraggs 