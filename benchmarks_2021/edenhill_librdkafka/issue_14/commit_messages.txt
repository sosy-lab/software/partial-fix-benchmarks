Let non-broker threads trigger leader queries.
Re-delegate all affected topic+partitions when a broker fails (issue #14)
Proper broker metadata refresh on leader failure. (issue #14)

Config property:
Config property: "topic.metadata.refresh.fast.cnt" (def 10)
Config property: "topic.metadata.refresh.fast.interval.ms" (def 250ms)v
