rebalance fails (sometimes) on long connection loss
Can you provide output from   debug=cgrp,topic,broker   when this happens?

The log is from the application so it will contain some more log lines.
The bug is visible for me at about 18:20:50
The group with the name "QC-WSP.Metrics->WSP.Metrics_WSP.Metrics" is not rebalancing
http://www.pastefile.com/V45ks8

I think this might be related to issue #785 which Ive spent some time troubleshooting and found a couple of state transition bugs.

The original issue #785 is different. But the bug described in the comments of the last 5days is very similar and looks related.

Hi,
I have just tested the last commit (686c404d95679183b876532946323edd4c882170)
and I still have the problem.
On the 2nd reconnection (after 2 long kafa disconnections) I don't get the assign rebalance callback anymore.

@vin-d can you reproduce with `debug=cgrp,topic,broker,metadata,protocol` and send me the logs?
Please indicate where you bring the cluster back up again.
Thanks

full log is here : https://drive.google.com/file/d/0B9qWE-U2505lT3Iwa2lJbE5seVU/view?usp=sharing

This time I got the assign cb back after second disconnection ended, but the problem triggered after 3rd disconnection ended : 

```
2016-10-31 16:24:14Z KafKaForwarderApp.task-1 |Fatal| : KAFKA triggered a rebalance event =  Local: Assign partitions. {KFtest-out:0}
```

kafka server out for approx 3min from 16:23:53

```
2016-10-31 16:24:53Z KafKaForwarderApp.task-1 |Fatal| : KAFKA triggered a rebalance event =  Local: Revoke partitions. {KFtest-out:0}
2016-10-31 16:27:35Z KafKaForwarderApp.task-1 |Fatal| : KAFKA triggered a rebalance event =  Local: Assign partitions. {KFtest-out:0}
```

kafka server out for approx 3min from 16:27:32

```
2016-10-31 16:28:32Z KafKaForwarderApp.task-1 |Fatal| : KAFKA triggered a rebalance event =  Local: Revoke partitions. {KFtest-out:0}
2016-10-31 16:31:14Z KafKaForwarderApp.task-1 |Fatal| : KAFKA triggered a rebalance event =  Local: Assign partitions. {KFtest-out:0}
```

kafka server out for approx 3min from 16:31:24

```
2016-10-31 16:32:24Z KafKaForwarderApp.task-1 |Fatal| : KAFKA triggered a rebalance event =  Local: Revoke partitions. {KFtest-out:0}
2016-10-31 16:39:59Z end of log
```

Thanks again for your troubleshooting efforts, @vin-d.
This surfaced a corner case where it would get stuck in waiting for a valid group coordinator but without actually querying for it.
Fix is on the way.

Please give latest master a shot

@vin-d, any luck?

i'm testing it right now.

have played my script 3 times and every thing is good.
Give me 10 more minutes, i'll test it 5 times.

On Wed, Nov 2, 2016 at 9:49 AM, Magnus Edenhill notifications@github.com
wrote:

> @vin-d https://github.com/vin-d, any luck?
> 
> —
> You are receiving this because you were mentioned.
> Reply to this email directly, view it on GitHub
> https://github.com/edenhill/librdkafka/issues/844#issuecomment-257805499,
> or mute the thread
> https://github.com/notifications/unsubscribe-auth/ATQE26_Qiq7stQXaBfQ7rbZqnD38QWJ1ks5q6E6qgaJpZM4Ka_4V
> .

## 

The information transmitted is intended only for the person or entity to 
which it is addressed and may contain confidential and/or privileged 
material. Any review, retransmission, dissemination or other use of, or 
taking of any action in reliance upon, this information by persons or 
entities other than the intended recipient is prohibited. If you received 
this in error, please contact the sender and delete the material from any 
computer.

One word : Success !
Thank you, I don't see any stability issue anymore regarding disconnections (In fact, I don't see stability issue at all).
(test done with 48099dbc24929c195dbeb6cfe75cdbe00b4c3941)

Thank you, that's great news!

@vin-d thanks for testing ;)

I also retested it now (39ba864f9d649e3888ec0c9ae4ef3fd73bae5ddb): Success

Thanks guys!
