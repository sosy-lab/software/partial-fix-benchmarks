pytest: Add test for waitanyinvoice

Signed-off-by: Christian Decker <decker.christian@gmail.com>
closingd: cap fee at the fee of the final commitment tx.

Fixes: #348
Signed-off-by: Rusty Russell <rusty@rustcorp.com.au>
closingd: start with proper maximum fee, not our guesstimate.

Fixes: #348
Signed-off-by: Rusty Russell <rusty@rustcorp.com.au>
closingd: start with proper maximum fee, not our guesstimate.

Fixes: #348
Signed-off-by: Rusty Russell <rusty@rustcorp.com.au>
