IDBFS: don't ignore changes files if timestamp decreased (#12409)

This saves a file not only if the file's timestamp was increased due to
a direct change, but also when the timestamp decreased due to e.g.
an in-process unzip which restored a file with an earlier timestamp.
Document and test that PROXY_TO_PTHREAD fixes the pthread_create issue (#12431)

That is, pthread_create in that mode works as expected, with the pthread being
synchronously available (since the creation is proxied to the main runtime thread,
which can spin the browser event loop to create the worker).

Related to discussion in #11554
Make it easier to detect thread pool exhaustion

Off-the-shelf C++ code doesn't know anything about JS event loop, and "normal" usages of `pthread_create` + `pthread_join` or any form of locks just deadlock silently.

This frequently leads to user confusion and those issues are not the easiest to debug. Some long-term solutions were proposed in https://github.com/emscripten-core/emscripten/issues/9910 and in https://github.com/emscripten-core/emscripten/issues/11554#issuecomment-699082669, but they require more design work.

For now this can at least fix https://github.com/emscripten-core/emscripten/issues/11554 and 1) warn users that the thread pool is exhausted in debug builds (`-s ASSERTIONS=1`) and 2) add an option `PTHREAD_POOL_SIZE_STRICT` to make such exhaustion a hard error in release builds for users who don't expect to create any more threads than preconfigured during the build time.

---

Another alternative I considered was untying this warning from `ASSERTIONS`. There are two reasons to go that route:

1. This is not always an error and advanced users who return to event loop between thread creation & usage might want to disable this particular warning separately from other assertions.
2. Majority of users might want to see at least a warning even in release builds where other assertions are disabled, since number of threads can depend on the input data.

If that's an acceptable option, then I'd instead define `PTHREAD_POOL_SIZE_STRICT` as follows:

 - `-s PTHREAD_POOL_SIZE_STRICT=0` -> no warnings for users who know what they're doing and adapted their code to break out to the event loop
 - `-s PTHREAD_POOL_SIZE_STRICT=1` -> default option, show warning but still try to create a thread, so that users at least know why there's a deadlock if it occurs
 - `-s PTHREAD_POOL_SIZE_STRICT=2` -> for users who saw the warning and decided to go all-in on the limit, as they are just trying to compile a code to the Web and don't want to mess with event loop APIs
Make it easier to detect thread pool exhaustion

Off-the-shelf C++ code doesn't know anything about JS event loop, and "normal" usages of `pthread_create` + `pthread_join` or any form of locks just deadlock silently.

This frequently leads to user confusion and those issues are not the easiest to debug. Some long-term solutions were proposed in https://github.com/emscripten-core/emscripten/issues/9910 and in https://github.com/emscripten-core/emscripten/issues/11554#issuecomment-699082669, but they require more design work.

For now this can at least fix https://github.com/emscripten-core/emscripten/issues/11554 and 1) warn users that the thread pool is exhausted in debug builds (`-s ASSERTIONS=1`) and 2) add an option `PTHREAD_POOL_SIZE_STRICT` to make such exhaustion a hard error in release builds for users who don't expect to create any more threads than preconfigured during the build time.

---

Another alternative I considered was untying this warning from `ASSERTIONS`. There are two reasons to go that route:

1. This is not always an error and advanced users who return to event loop between thread creation & usage might want to disable this particular warning separately from other assertions.
2. Majority of users might want to see at least a warning even in release builds where other assertions are disabled, since number of threads can depend on the input data.

If that's an acceptable option, then I'd instead define `PTHREAD_POOL_SIZE_STRICT` as follows:

 - `-s PTHREAD_POOL_SIZE_STRICT=0` -> no warnings for users who know what they're doing and adapted their code to break out to the event loop
 - `-s PTHREAD_POOL_SIZE_STRICT=1` -> default option, show warning but still try to create a thread, so that users at least know why there's a deadlock if it occurs
 - `-s PTHREAD_POOL_SIZE_STRICT=2` -> for users who saw the warning and decided to go all-in on the limit, as they are just trying to compile a code to the Web and don't want to mess with event loop APIs
Make it easier to detect thread pool exhaustion

Off-the-shelf C++ code doesn't know anything about JS event loop, and "normal" usages of `pthread_create` + `pthread_join` or any form of locks just deadlock silently.

This frequently leads to user confusion and those issues are not the easiest to debug. Some long-term solutions were proposed in https://github.com/emscripten-core/emscripten/issues/9910 and in https://github.com/emscripten-core/emscripten/issues/11554#issuecomment-699082669, but they require more design work.

For now this can at least fix https://github.com/emscripten-core/emscripten/issues/11554 and 1) warn users that the thread pool is exhausted in debug builds (`-s ASSERTIONS=1`) and 2) add an option `PTHREAD_POOL_SIZE_STRICT` to make such exhaustion a hard error in release builds for users who don't expect to create any more threads than preconfigured during the build time.

---

Another alternative I considered was untying this warning from `ASSERTIONS`. There are two reasons to go that route:

1. This is not always an error and advanced users who return to event loop between thread creation & usage might want to disable this particular warning separately from other assertions.
2. Majority of users might want to see at least a warning even in release builds where other assertions are disabled, since number of threads can depend on the input data.

If that's an acceptable option, then I'd instead define `PTHREAD_POOL_SIZE_STRICT` as follows:

 - `-s PTHREAD_POOL_SIZE_STRICT=0` -> no warnings for users who know what they're doing and adapted their code to break out to the event loop
 - `-s PTHREAD_POOL_SIZE_STRICT=1` -> default option, show warning but still try to create a thread, so that users at least know why there's a deadlock if it occurs
 - `-s PTHREAD_POOL_SIZE_STRICT=2` -> for users who saw the warning and decided to go all-in on the limit, as they are just trying to compile a code to the Web and don't want to mess with event loop APIs
Make it easier to detect thread pool exhaustion (#13591)

Off-the-shelf C++ code doesn't know anything about JS event loop, and "normal" usages of `pthread_create` + `pthread_join` or any form of locks just deadlock silently.

This frequently leads to user confusion and those issues are not the easiest to debug. Some long-term solutions were proposed in https://github.com/emscripten-core/emscripten/issues/9910 and in https://github.com/emscripten-core/emscripten/issues/11554#issuecomment-699082669, but they require more design work.

For now this can at least fix https://github.com/emscripten-core/emscripten/issues/11554 and 1) warn users that the thread pool is exhausted in debug builds (`-s ASSERTIONS=1`) and 2) add an option `PTHREAD_POOL_SIZE_STRICT` to make such exhaustion a hard error in release builds for users who don't expect to create any more threads than preconfigured during the build time.

---

Another alternative I considered was untying this warning from `ASSERTIONS`. There are two reasons to go that route:

1. This is not always an error and advanced users who return to event loop between thread creation & usage might want to disable this particular warning separately from other assertions.
2. Majority of users might want to see at least a warning even in release builds where other assertions are disabled, since number of threads can depend on the input data.

If that's an acceptable option, then I'd instead define `PTHREAD_POOL_SIZE_STRICT` as follows:

 - `-s PTHREAD_POOL_SIZE_STRICT=0` -> no warnings for users who know what they're doing and adapted their code to break out to the event loop
 - `-s PTHREAD_POOL_SIZE_STRICT=1` -> default option, show warning but still try to create a thread, so that users at least know why there's a deadlock if it occurs
 - `-s PTHREAD_POOL_SIZE_STRICT=2` -> for users who saw the warning and decided to go all-in on the limit, as they are just trying to compile a code to the Web and don't want to mess with event loop APIs
