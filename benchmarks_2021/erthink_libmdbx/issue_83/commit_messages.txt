mdbx: drop unused WindowsNT API prototypes.

Change-Id: Ic929646653d0576671d6150a698e892d2145ae30
mdbx: workaround for NtExtendSection() on Wine.

This fixes https://github.com/erthink/libmdbx/issues/83

Change-Id: I8e00aa91c86348fad9dbe4285143671d9cb3f802
mdbx-windows: rework workaround for Wine.

Resolves https://github.com/erthink/libmdbx/issues/83
in accordance with https://bugs.winehq.org/show_bug.cgi?id=48620

Change-Id: Ieb4385efdcd86c865184a783363cf6e90da14f61
mdbx-windows: more for Wine.

Related to https://github.com/erthink/libmdbx/issues/83.

UnlockFile() cold return ERROR_LOCK_VIOLATION when file not locked, instead of ERROR_NOT_LOCKED.
Current versions of Wine seem to work correctly.

Change-Id: Ibc5bd4352184efc7f88705e7ae04d6656286a96e
mdbx-windows: more for Wine.

Related to https://github.com/erthink/libmdbx/issues/83.

UnlockFile() cold return ERROR_LOCK_VIOLATION when file not locked, instead of ERROR_NOT_LOCKED.
Current versions of Wine seem to work correctly.

Change-Id: Ibc5bd4352184efc7f88705e7ae04d6656286a96e
mdbx-windows: one more workaround for Wine.

SetFileInformationByHandle() is not implemented by Wine.

Change-Id: I61783c8d173397094cd6cbad7efc9d7aac57b470
002c:Call KERNEL32.SetFileInformationByHandle(00000060,00000006,0022f890,00000008) ret=127dd1b2
002c:fixme:file:SetFileInformationByHandle 0000000000000060, 6, 000000000022F890, 8
002c:Ret  KERNEL32.SetFileInformationByHandle() retval=00000000 ret=127dd1b2

Related to https://github.com/erthink/libmdbx/issues/83
mdbx-windows: more for running under Wine.

Wine unable create mapped-view large tran secton size.

More for https://github.com/erthink/libmdbx/issues/83

Change-Id: I93e98d58a827c785f8257cd9cac387a333e59621
mdbx-windows: more for running under Wine.

Wine unable create mapped-view large tran secton size.

More for https://github.com/erthink/libmdbx/issues/83

Change-Id: I93e98d58a827c785f8257cd9cac387a333e59621
