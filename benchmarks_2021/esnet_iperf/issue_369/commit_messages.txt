Add new iperf3 status update for 6 June 2017.
Towards issue #369...implement daemon(3) for systems that don't have it.
Various improvements to alternate daemon(3) implementation:

o Just call it daemon, and only compile it if there isn't a daemon(3)
function on the system already.  This makes consumers of daemon(3)
simpler (as in main.c).

o Move the code to iperf_util.c and iperf_util.h, it doesn't need to
be in its own file.

Still to do:

o Fix the actual re-implementation of daemon per comments.

o Regen generated build infrastructure files (arguably I shoudn't
have committed those to this task branch in the first place).

Clawing our way towards #369.
Implement daemon(3) for systems that don't have it.

Fixes (and based on a patch in) #369, with some reworking by @bmah888 and @pprindeville.
