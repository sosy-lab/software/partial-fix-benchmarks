Merge pull request #2060 from matthiasclasen/instance-api

Add an instance api
main: Don't spuriously print usage info

Currently if any command errors out with G_IO_ERROR_NOT_SUPPORTED,
flatpak prints the usage information (the output of --help), but this is
not correct. For example if the create-usb command hits that error when
trying to use extended attributes on a filesystem that doesn't support
them, the help output is printed as we saw here:
https://github.com/flatpak/flatpak/issues/2019#issuecomment-416798304

So this commit removes the check for G_IO_ERROR_NOT_SUPPORTED in
flatpak-main.c and the helper function it uses. The flatpak_run()
function handles printing usage info for the overall flatpak command,
and subcommands use the usage_error() function to print usage
information, so there's no need for it.
main: Don't spuriously print usage info

Currently if any command errors out with G_IO_ERROR_NOT_SUPPORTED,
flatpak prints the usage information (the output of --help), but this is
not correct. For example if the create-usb command hits that error when
trying to use extended attributes on a filesystem that doesn't support
them, the help output is printed as we saw here:
https://github.com/flatpak/flatpak/issues/2019#issuecomment-416798304

So this commit removes the check for G_IO_ERROR_NOT_SUPPORTED in
flatpak-main.c and the helper function it uses. The flatpak_run()
function handles printing usage info for the overall flatpak command,
and subcommands use the usage_error() function to print usage
information, so there's no need for it.

Closes: #2082
Approved by: alexlarsson
create-usb: Always use archive mode

Change the create-usb command so that it always creates the destination
repository using the "archive" mode, rather than using archive mode when
xattrs aren't supported and bare-user otherwise. This has a few
advantages:

1. The archive mode works with FAT filesystems, which is what most USB
drives are, and which doesn't support xattrs. This should fix
https://github.com/flatpak/flatpak/issues/2019 but it would be good to
confirm.

2. At least in some quick testing I did, archive mode is about twice as
performant as bare-user mode, in terms of how long it takes for the
create-usb command to complete.

3. This ensures that a tool can safely change the permissions on
".ostree/repo" and subdirectories after create-usb completes, which is
important for Endless since otherwise you can't use `ostree create-usb`
as root and then `flatpak create-usb` as a non-root user on the same USB
drive (or in other words copy OS updates and apps to the same USB).
create-usb: Always use archive mode

Change the create-usb command so that it always creates the destination
repository using the "archive" mode, rather than using archive mode when
xattrs aren't supported and bare-user otherwise. This has a few
advantages:

1. The archive mode works with FAT filesystems, which is what most USB
drives are, and which doesn't support xattrs. This should fix
https://github.com/flatpak/flatpak/issues/2019 but it would be good to
confirm.

2. At least in some quick testing I did, archive mode is about twice as
performant as bare-user mode, in terms of how long it takes for the
create-usb command to complete.

3. This ensures that a tool can safely change the permissions on
".ostree/repo" and subdirectories after create-usb completes, which is
important for Endless since otherwise you can't use `ostree create-usb`
as root and then `flatpak create-usb` as a non-root user on the same USB
drive (or in other words copy OS updates and apps to the same USB).

Closes: #2124
Approved by: alexlarsson
create-usb: Always use archive mode

Change the create-usb command so that it always creates the destination
repository using the "archive" mode, rather than using archive mode when
xattrs aren't supported and bare-user otherwise. This has a few
advantages:

1. The archive mode works with FAT filesystems, which is what most USB
drives are, and which doesn't support xattrs. This should fix
https://github.com/flatpak/flatpak/issues/2019 but it would be good to
confirm.

2. At least in some quick testing I did, archive mode is about twice as
performant as bare-user mode, in terms of how long it takes for the
create-usb command to complete.

3. This ensures that a tool can safely change the permissions on
".ostree/repo" and subdirectories after create-usb completes, which is
important for Endless since otherwise you can't use `ostree create-usb`
as root and then `flatpak create-usb` as a non-root user on the same USB
drive (or in other words copy OS updates and apps to the same USB).

Closes: #2124
Approved by: alexlarsson
create-usb: Always use archive mode

Change the create-usb command so that it always creates the destination
repository using the "archive" mode, rather than using archive mode when
xattrs aren't supported and bare-user otherwise. This has a few
advantages:

1. The archive mode works with FAT filesystems, which is what most USB
drives are, and which doesn't support xattrs. This should fix
https://github.com/flatpak/flatpak/issues/2019 but it would be good to
confirm.

2. At least in some quick testing I did, archive mode is about twice as
performant as bare-user mode, in terms of how long it takes for the
create-usb command to complete.

3. This ensures that a tool can safely change the permissions on
".ostree/repo" and subdirectories after create-usb completes, which is
important for Endless since otherwise you can't use `ostree create-usb`
as root and then `flatpak create-usb` as a non-root user on the same USB
drive (or in other words copy OS updates and apps to the same USB).

Closes: #2124
Approved by: alexlarsson
