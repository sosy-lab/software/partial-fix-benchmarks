dockerfile: bump to v1.2.0

Signed-off-by: Eduardo Silva <eduardo@treasure-data.com>
scheduler: fix backoff jitter calculation (#798 #670 #649)

The backoff jitter algorithm helps to define a suggested 'sleep' time for
retries, the current algorithm is broken where in some cases it generate
an invalid negative number leading to issues when using OS syscalls to
set timers, the following error is visible:

   timerfd_settime: Invalid argument, errno=22 at ...mk_core/mk_event_epoll.c:190

This patch fix the exponential calculation and adjust properly the final value.

Signed-off-by: Eduardo Silva <eduardo@treasure-data.com>
scheduler: fix backoff jitter calculation (#798 #670 #649)

The backoff jitter algorithm helps to define a suggested 'sleep' time for
retries, the current algorithm is broken where in some cases it generate
an invalid negative number leading to issues when using OS syscalls to
set timers, the following error is visible:

   timerfd_settime: Invalid argument, errno=22 at ...mk_core/mk_event_epoll.c:190

This patch fix the exponential calculation and adjust properly the final value.

Signed-off-by: Eduardo Silva <eduardo@treasure-data.com>
lib: monkey: Fix file descriptor leak in _mk_event_timeout_create

If the call to timerfd_settime fails the file descriptor created by the
previous call to timerfd_create is not closed.

Potentially an additional fix for fluent/fluent-bit#798 which is about
running out of file descriptors.
lib: monkey: Fix file descriptor leak in _mk_event_timeout_create

If the call to timerfd_settime fails the file descriptor created by the
previous call to timerfd_create is not closed.

Potentially an additional fix for fluent/fluent-bit#798 which is about
running out of file descriptors.

Signed-off-by: Jonas Fonseca <jonas.fonseca@gmail.com>
