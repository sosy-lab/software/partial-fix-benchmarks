timerfd_settime error 22 and task retry warnings
If I am not wrong that exception can happen if Fluent Bit is running out of available file descriptors. If you are facing problems to flush the data because of output destination is down or erroring, Fluent Bit will schedule a retry, the error in question is about it could not schedule due to resources problems in your environment.
did the problem persists ? any change in your environment ?
Yeah, it's still happening
I'm having a similar issue at the moment, I'm just missing the warn lines.

```
[2018/11/19 19:40:57] [  Error] timerfd_settime: Invalid argument, errno=22 at /tmp/fluent-bit-0.14.1/lib/monkey/mk_core/mk_event_epoll.c:189
```

Current environment is AWS Elasticbeanstalk, coming in from Docker's logging driver, into AWS Elasticsearch.

I don't get any other error or warning lines from fluent-bit, and I'm just trying to figure out why a few hours of logs have gone missing...
Invalid argument error (errno=22) is also returned if  sec or nsec are negative values. 
There is no checks in function for sec/nsec argument values. 
https://github.com/fluent/fluent-bit/blob/aa883c99269e982c4c40d770f79f56b57f21e32d/lib/monkey/mk_core/mk_event_epoll.c#L163-L164

Hi we also appear to have hit the issue on one of our fluentbit pods.

This is taken from the pod logs:

`[2019/02/26 20:29:51] [  Error] timerfd_settime: Invalid argument, errno=22 at /tmp/fluent-bit-0.14.8/lib/monkey/mk_core/mk_event_epoll.c:190`

fluentbit version: 0.14.8
kubernetes version: v1.12.4

It looks like a fd leak. Instances which moan about this have a lot of fds pointing to `anon_inode:[timerfd]`. Restarting the process helps.
I'm encountering a similar issue here. What fluentbit configurations can I change to remove this error?

```
[2019/05/06 13:18:39] [ Error] timerfd_settime: Invalid argument, errno=22 at /tmp/fluent-bit-1.0.5/lib/monkey/mk_core/mk_event_epoll.c:190 stream:stdout time:May 6th 2019, 09:18:39.028 

[2019/05/06 13:18:39] [ warn] [sched] retry for task 57 could not be scheduled stream:stderr time:May 6th 2019, 09:18:39.028
```

The node fd allocated is well below the maximum available fd, what exactly could be causing this error in that case?
FYI: I've pushed a fix for the root cause of the problem, thanks to everybody who helped to troubleshoot the problem.

ref: https://github.com/fluent/fluent-bit/commit/1ef74fb0674490f665c3139a406896453f4d4a53
Fixed in v1.1.1 release:

https://fluentbit.io/announcements/v1.1.1/
@edsiper greetings! I hope you will saw my comment here. 

Current setup: 
`fluentbit ver 1.1.3` currentu this is the lates one. With fixed docker parser.
`kubernetes 1.14.2` 

Facing fd leakage, I'm 100% sure that this is `fluentbit` problem, because tested a lot with and without of `fluentbit`.

At the peak situation looks like:
```
user@master-server:~$ uptime
 12:20:29 up 9 days, 21:17,  1 user,  load average: 86.51, 83.03, 84.52
user@master-server:~$ sudo su -
root@master-server ~ # find /proc/*/fd/* -type l -lname 'anon_inode:inotify' -print 2>/dev/null | cut -d/ -f3 |xargs -I '{}' -- ps --no-headers -o '%U %p %c' -p '{}' | sort | uniq -c | sort -nr
   4081 root     105443 dockerd
      5 root     107275 kubelet
      2 root          1 systemd
      2 alubovs+  45213 systemd
      1 systemd+  93441 systemd-resolve
      1 root      76038 systemd-udevd
      1 root      58515 fluent-bit
      1 root       3922 agetty
      1 root       1343 sssd
      1 root     125455 configmap-reloa
      1 root     125340 prometheus-conf
      1 root       1174 rpc.gssd
      1 root     110834 kube-proxy
      1 message+   1374 dbus-daemon
```

After restart of `dockerd` and 30 mins, we have pretty huge volumes of logs:
```
root@master-server ~ # find /proc/*/fd/* -type l -lname 'anon_inode:inotify' -print 2>/dev/null | cut -d/ -f3 |xargs -I '{}' -- ps --no-headers -o '%U %p %c' -p '{}' | sort | uniq -c | sort -nr
     58 root      50349 dockerd
      5 root     107275 kubelet
      2 root          1 systemd
      2 alubovs+  10897 systemd
      2 abraich+ 110521 systemd
      1 systemd+  93441 systemd-resolve
      1 root      76038 systemd-udevd
      1 root      58515 fluent-bit
      1 root       3922 agetty
      1 root       1343 sssd
      1 root     125455 configmap-reloa
      1 root     125340 prometheus-conf
      1 root       1174 rpc.gssd
      1 root     110834 kube-proxy
      1 message+   1374 dbus-daemon
```

P/S: If needed I could send config file of `fluentbit` or any other info.
UPD: fd leakage reason is steel unknown. May be it is not the `fluentbit` but a very rare chain of behaviors in our applications. 