Add a setting for volume envelope processing (compliant or EMU)
As I understand you're refering to this [fluid_conv.h:36](https://github.com/FluidSynth/fluidsynth/blob/0edbe06621e8c9cc10837a754af14c645bf6ed60/fluidsynth/src/utils/fluid_conv.h#L36)

Apparently this was not fixed properly as noted in 29f6ac104913fe348e538b04f4bbfc65adbd2a28. Reopen.
I've tried to implement everything we talked about in the attenuation-mode branch: c37a21c

That voice->channel->synth dance is not pretty, but I don't see any other way to implement this... what do you think?

I've changed the setting name to attenuation-mode to make it's purpose a bit clearer. Otherwise people might think it had something to do with the volume envelope, which is not really the case (or only partly).

It's not tested yet, btw. Will do that tomorrow.
  
I would directly store `unsigned char attenuation_mode` in `fluid_voice_t` initialized once in `new_fluid_voice()`. Looking at the struct there seems to be at least one padding byte anyway, this is where I'd store it:

https://github.com/FluidSynth/fluidsynth/blob/f3903d1a2f536600b32aaa9d4310cef62f6a0e0e/src/synth/fluid_voice.h#L107
> I would directly store unsigned char attenuation_mode in fluid_voice_t

Yes, that is probably a better way to do it.

But thinking about this again, maybe we should simply close this ticket and not implement this mode switch. The ticket is 7 years old and nobody seems to have taken issue with the volume response since. And the original request (implementing an emu and compliant mode switch), is actually wrong. The poster wanted a "quirks" mode, where Fluidsynth behaves like Timidity. And Timidity's behaviour is neither compliant nor compatible with EMU hardware.

Maybe we just hard-code the 0.4 factor for the `GEN_ATTENUATION.gen` value, remove atten2amp and have cb2amp use the correct -200 factor with a range of 0 - 1440. After all, less dynamic volume response could be achieved by simply modifying the modulator range in the soundfont.
Anyway, I've implemented the changes (fixed the ABS mode and store attenuation_mode in voice) and opened a pull-request #318. 
> But thinking about this again, maybe we should simply close this ticket and not implement this mode switch. 

Not sure. I had a personal interest in a less dynamic vol resp. The findings of the past days ofc make me think it'd be really better to just override the modulator range in the SF or perhaps use the manip def mod API. So I'll need to do comprehensively test this before making a decision. Anyway, thanks so far for implementing and better understanding this.