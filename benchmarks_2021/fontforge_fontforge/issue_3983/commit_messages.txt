Merge pull request #4111 from jtanx/warns

Multiple warnings dialogue text selection bug fixes
Make kern in MetricsView toggleable with `-`

This, in my opinion, solves #3983.

It is still not possible to type a negative integer in LTR order. I
think that making that possible will cause needless disruption to this
file and is also a waste of time. Instead, first type the number, then a
minus sign, which will "toggle" the value negative.

I've documented this as the right way to do it, as well. I understand
why some people might not like this compromise, but it has been broken
for over ten years. If you have a better way to fix it, feel free :-)
Make kern in MetricsView toggleable with `-`

This, in my opinion, solves #3983.

It is still not possible to type a negative integer in LTR order. I
think that making that possible will cause needless disruption to this
file and is also a waste of time. Instead, first type the number, then a
minus sign, which will "toggle" the value negative.

I've documented this as the right way to do it, as well. I understand
why some people might not like this compromise, but it has been broken
for over ten years. If you have a better way to fix it, feel free :-)
Make kern in MetricsView toggleable with `-`

This, in my opinion, solves #3983.

It is still not possible to type a negative integer in LTR order. I
think that making that possible will cause needless disruption to this
file and is also a waste of time. Instead, first type the number, then a
minus sign, which will "toggle" the value negative.

I've documented this as the right way to do it, as well. I understand
why some people might not like this compromise, but it has been broken
for over ten years. If you have a better way to fix it, feel free :-)
Make kern in MetricsView toggleable with `-`

This, in my opinion, solves #3983.

It is still not possible to type a negative integer in LTR order. I
think that making that possible will cause needless disruption to this
file and is also a waste of time. Instead, first type the number, then a
minus sign, which will "toggle" the value negative.

I've documented this as the right way to do it, as well. I understand
why some people might not like this compromise, but it has been broken
for over ten years. If you have a better way to fix it, feel free :-)
Make kern in MetricsView toggleable with `-` (#4116)

* Make kern in MetricsView toggleable with `-`

This, in my opinion, solves #3983.

It is still not possible to type a negative integer in LTR order. I
think that making that possible will cause needless disruption to this
file and is also a waste of time. Instead, first type the number, then a
minus sign, which will "toggle" the value negative.

I've documented this as the right way to do it, as well. I understand
why some people might not like this compromise, but it has been broken
for over ten years. If you have a better way to fix it, feel free :-)
