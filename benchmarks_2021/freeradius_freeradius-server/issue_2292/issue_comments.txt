references to TLS-Client-Cert-CN in raddb
I've pushed some fixes which will help with the docs and debug message.

For the rest, the `TLS-Client-Cert-Common-Name` attribute is available during the setup of the incoming RadSec connection.  It is not available after that.

If the  `TLS-Client-Cert-Common-Name` attribute from RadSec was available for later packets, it would be difficult to differentiate that from the  `TLS-Client-Cert-Common-Name` attribute that comes from an EAP-TLS connection.

You also can't update the configuration while the server is running.  That would cause issues.

I've pushed a fix which allows you to do `%{listen:TLS-Client-Cert-Common-Name}`, or any other `TLS-` attribute from the radsec connection.  It's not perfect, but it should work.

Please check that it works for you.  If it does, this PR can be closed.
That sounds exactly like what I was hoping for. I'm not sure how I can use it though. In the authorize section?

```
(0) # Executing section authorize from file /opt/freeradius/etc/raddb/sites-enabled/me
(0)   authorize {
(0)     Listener does not contain config item "TLS-Client-Cert-Subject"
(0)     EXPAND %{listen:TLS-Client-Cert-Subject}
(0)        -->
(0)     Listener does not contain config item "TLS-Client-Cert-Common-Name"
(0)     EXPAND %{listen:TLS-Client-Cert-Common-Name}
(0)        -->
```

Just tried to display the value in debug with `"%{listen:TLS-Client-Cert-Subject}"` nothing more.
The `"%{listen:TLS-Client-Cert-Subject}"` string will only be available for RadSec connections.

So... it this a RadSec connection?
Surprisingly, there is a value in `%{listen:TLS-Client-Cert-Subject-Alt-Name-Dns}`

I would expect a Common-Name and Subject...

```
(0) TLS_accept: SSLv3/TLS write server done
(0) <<< recv TLS 1.0 Handshake [length 055f], Certificate
(0) TLS - Creating attributes from certificate OIDs
(0) TLS - Creating attributes from certificate OIDs
(0)   TLS-Client-Cert-Subject-Alt-Name-Dns := "one.eduroam.nl"
(0)   TLS-Client-Cert-Subject-Alt-Name-Dns := "two.eduroam.nl"
(0)   TLS-Client-Cert-X509v3-Basic-Constraints += "CA:FALSE"
(0)   TLS-Client-Cert-X509v3-Extended-Key-Usage += "TLS Web Client Authentication, TLS Web Server Authentication"
(0)   TLS-Client-Cert-X509v3-Subject-Key-Identifier += "5C:3D:84:6F:F4:35:EA:5C:F8:E7:..."
(0)   TLS-Client-Cert-X509v3-Authority-Key-Identifier += "keyid:D2:F2:23:BD:4A:A1:7F:CF:A0:...\n"
(0)   TLS-Client-Cert-X509v3-Extended-Key-Usage-OID += "1.3.6.1.5.5.7.3.2"
(0)   TLS-Client-Cert-X509v3-Extended-Key-Usage-OID += "1.3.6.1.5.5.7.3.1"
(0) TLS_accept: SSLv3/TLS read client certificate
```
The debug output shows which certificate attributes it creates.  Those attributes are the only ones which are available via `%{listen:TLS-...}`.

The attributes for the server certificate are already known.  They're in the certificate for that listener.

Are you trying to use multiple server certificates with one listener?
No, it's just one certificate, not multiple (server) certificates. In this example I used a client certificate with multiple subjectAltNames, but I actually used a dummy CN=localhost certificate too.

```
# openssl x509 -noout -text -in /opt/localhost.pem | grep "Subject:"
        Subject: CN = localhost
```

Tried another server with production certificates; but I just don't get Subject or Common-Name, only the additional certificate attributes.

It looks to me like the `TLS-Client-Cert-Subject` and `TLS-Client-Cert-Common-Name` aren't created, but just the subjectAltNames or exteded attributes. Yet, I would expect them as they're the most basic client-certificate attributes. Client certificate validation is enabled and otherwise the extended attributes would not be seen either.
What happens when you use those same certificates for EAP-TLS?  i.e. do you get the various certificate attributes produced?
Yes, I see them in debug:

```
(4) eap_tls: Continuing EAP-TLS
(4) eap_tls: [eaptls verify] = ok
(4) eap_tls: Done initial handshake
(4) eap_tls: TLS_accept: SSLv3/TLS write server done
(4) eap_tls: <<< recv TLS 1.2  [length 030c]
(4) eap_tls: TLS - Creating attributes from certificate OIDs
(4) eap_tls:   TLS-Client-Cert-Serial := "f8e40394ae3235d7"
(4) eap_tls:   TLS-Client-Cert-Expiration := "210728200227Z"
(4) eap_tls:   TLS-Client-Cert-Subject := "/CN=localhost"
(4) eap_tls:   TLS-Client-Cert-Issuer := "/CN=localhost"
(4) eap_tls:   TLS-Client-Cert-Common-Name := "localhost"
(4) eap_tls:   TLS-Client-Cert-X509v3-Subject-Key-Identifier += "02:8D:EC:AC:E5:18:1F:03:7D:C9:A6:A5:2E:8E:98:99:BC:6F:B7:6C"
(4) eap_tls:   TLS-Client-Cert-X509v3-Authority-Key-Identifier += "keyid:02:8D:EC:AC:E5:18:1F:03:7D:C9:A6:A5:2E:8E:98:99:BC:6F:B7:6C\n"
(4) eap_tls:   TLS-Client-Cert-X509v3-Basic-Constraints += "CA:TRUE"
```

And they're indeed available in post-auth:

```
(5)   post-auth {
(5)     EXPAND %{TLS-Client-Cert-Common-Name}
(5)        --> localhost
(5)     EXPAND %{TLS-Client-Cert-Subject}
(5)        --> /CN=localhost
```

Just never for RadSec, then it starts with `TLS-Client-Cert-Subject-Alt-Name-Dns` if present and `TLS-Client-Cert-X509v3-Basic-Constraints` and onwards.

I now realize I also don't get `TLS-Client-Cert-Serial`, `TLS-Client-Cert-Expiration` and `TLS-Client-Cert-Issuer` either for the RadSec connection, besides `TLS-Client-Cert-Common-Name` and `TLS-Client-Cert-Subject`

I'm not sure I understand the way I could check for list values BTW, but I also cannot use &listen: instead of %{listen, so I can't check for an OID or subjectAltName for instance:
```
if (&listen:TLS-Client-Cert-X509v3-Extended-Key-Usage-OID == "1.3.6.1.5.5.7.3.1") {
```
as that gives `Invalid list qualifier`, so I can only use it as `"%{listen:TLS-Client-Cert-X509v3-Extended-Key-Usage-OID}"` showing only the last value; but I think that's the same for EAP-TLS. For the Subject and Common Name this shouldn't matter and having the values in `%{listen:` is already very valuable.
I'll see if I can track down why the certs are created for EAP-TLS, but not for RadSec.

As for `&listen`, there's no "listen" list.  You can't use it.  You MUST use `%{listen:TLS-...}` as I said.  If `&listen` worked, I would have suggested using it.

You can't really check for existence.  The best that you can do is to check if the string expansion is empty.
I didn't really want to check for existence of the attribute, sorry to be confusing; and checking for an empty string would be fine indeed. What I did want to do, is see if any of the values contain something. I know you did tell me to use `%{listen:...}`.

I guess I need to go for each value of the list, so with EAP-TLS I can do
```
foreach &TLS-Client-Cert-Subject-Alt-Name-Dns {
}
```

but that doesn't work for "listen":
```
foreach "%{listen:TLS-Client-Cert-X509v3-Extended-Key-Usage-OID}" {
}
```

I do understand why I don't get the Subject and other attributes.
```
if (certs && identity && (lookup <= 1) && subject[0]) {
```
... there is no identity!
As I said... `%{listen:...}` is a string expansion.  It's not an attribute list.  It doesn't return attributes.  It returns strings.  I don't really know how to make this more clear.

You can't do `foreach "%{User-Name}" ...`, as that's a string expansion.  Just as with `%{listen:...}`

I do understand what you're saying, I completely understand what the limitations are. I do understand it's a string expansion. I do understand I cannot do a foreach.

I just have difficulty explaining what I'd like to do apparently. What I fail to make clear is that this would be a nice way to check for attribute values, if it's a multi-valued attribute. The `TLS-Client-Cert-X509v3-Extended-Key-Usage-OID` may contain an entitlement to be eligible as IdP and it may have the same attribute to be entitled as SP.
Also, one might want to check for each value of the subjectAltName.

In EAP-TLS a check like that would be possible, because there I can use it as attribute list. That's the only thing I'm comparing it to, and by saying that the behavior for RadSec is different from EAP-TLS, in a negative way.
Now my last line in that comment was:

> I do understand why I don't get the Subject and other attributes.
> 
> if (certs && identity && (lookup <= 1) && subject[0]) {
> ... there is no identity!

That was completely unrelated to the list and string expansion and what not. This was about the missing TLS-attributes like the Common-Name, Serial and Subject.

I now understand why, because there is no identity in the RadSec context. I guess that's trivial to fix?
For v3, we can't make the radsec `TLS-` attributes into a separate list.  That would require significant changes. However, when the RadSec connection starts, the server creates a fake `request` that contains all of those attributes.  You can leverage this request, and cache the attributes.  Then, look them up in a later user authentication session.

For multi-valued attributes, the `%{listen:..}` syntax will only look at the first one.  Changing that may require significant rework.  This functionality is much more likely to be in v4.

As for there being no identity, it should be possible to fix that.  The issue again is that we don't want the RadSec `TLS-` attributes to interfere with the EAP-TLS attributes.
That last patch works well, you made my day! :-)

(I presume that fake `request` you describe is part of v4 - I don't see that happen with v3.)