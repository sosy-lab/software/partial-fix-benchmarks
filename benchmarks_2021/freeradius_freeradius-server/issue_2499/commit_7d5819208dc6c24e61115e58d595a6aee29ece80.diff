diff --git a/doc/ChangeLog b/doc/ChangeLog
index 153a8accdcd..d194a3104f6 100644
--- a/doc/ChangeLog
+++ b/doc/ChangeLog
@@ -78,6 +78,7 @@ FreeRADIUS 3.0.18 Tue 17 Apr 2018 14:00:00 EDT urgency=low
 	* Add RFC 7532 "bang path" support for realms.
 	  Fixes #2492.
 	* Update dictionary.ukerna documentation.  Fixes #2493
+	* Add support for systemd service and watchdogs. #2499
 
 	Bug fixes
 	* The session-state list is no longer cleaned in the
diff --git a/src/include/process.h b/src/include/process.h
index 435b9f74982..8c3c7275e0e 100644
--- a/src/include/process.h
+++ b/src/include/process.h
@@ -33,6 +33,10 @@ RCSIDH(process_h, "$Id$")
 extern "C" {
 #endif
 
+#ifdef HAVE_SYSTEMD_WATCHDOG
+extern struct timeval sd_watchdog_interval;
+#endif
+
 typedef enum fr_state_action_t {	/* server action */
 	FR_ACTION_INVALID = 0,
 	FR_ACTION_RUN,
diff --git a/src/main/process.c b/src/main/process.c
index 9eeeaf4a612..6a02bb8ba0a 100644
--- a/src/main/process.c
+++ b/src/main/process.c
@@ -44,9 +44,19 @@ RCSID("$Id$")
 #	include <sys/wait.h>
 #endif
 
+#ifdef HAVE_SYSTEMD_WATCHDOG
+#  include <systemd/sd-daemon.h>
+#endif
+
 extern pid_t radius_pid;
 extern fr_cond_t *debug_condition;
 
+#ifdef HAVE_SYSTEMD_WATCHDOG
+struct timeval sd_watchdog_interval;
+static fr_event_t *sd_watchdog_ev;
+
+#endif
+
 static bool spawn_flag = false;
 static bool just_started = true;
 time_t fr_start_time = (time_t)-1;
@@ -347,6 +357,32 @@ void radius_update_listener(rad_listen_t *this)
 #  define FD_MUTEX_UNLOCK(_x)
 #endif
 
+/*
+ *	Emit a systemd watchdog notification and reschedule the event.
+ */
+#ifdef HAVE_SYSTEMD_WATCHDOG
+typedef struct {
+	fr_event_list_t *el;
+	struct timeval now;
+} sd_watchdog_data_t;
+
+static sd_watchdog_data_t sdwd;
+
+static void sd_watchdog_event(void *ctx)
+{
+	sd_watchdog_data_t *s = (sd_watchdog_data_t *)ctx;
+	struct timeval when;
+
+	DEBUG("Emitting systemd watchdog notification");
+	sd_notify(0, "WATCHDOG=1");
+
+	timeradd(&when, &sd_watchdog_interval, &s->now);
+	if (!fr_event_insert(s->el, sd_watchdog_event, ctx, &when, &sd_watchdog_ev)) {
+		rad_panic("Failed to insert event");
+	}
+}
+#endif
+
 static int request_num_counter = 1;
 #ifdef WITH_PROXY
 static int request_will_proxy(REQUEST *request) CC_HINT(nonnull);
@@ -5458,6 +5494,19 @@ int radius_event_init(TALLOC_CTX *ctx) {
 	el = fr_event_list_create(ctx, event_status);
 	if (!el) return 0;
 
+#ifdef HAVE_SYSTEMD_WATCHDOG
+	if (sd_watchdog_interval.tv_sec || sd_watchdog_interval.tv_usec) {
+		struct timeval now;
+
+		fr_event_now(el, &now);
+
+		sdwd.now = now;
+		sdwd.el = el;
+
+		sd_watchdog_event(&sdwd);
+	}
+#endif
+
 	return 1;
 }
 
diff --git a/src/main/radiusd.c b/src/main/radiusd.c
index 72c1fb94764..4f64184210a 100644
--- a/src/main/radiusd.c
+++ b/src/main/radiusd.c
@@ -31,6 +31,7 @@ RCSID("$Id$")
 #include <freeradius-devel/modules.h>
 #include <freeradius-devel/state.h>
 #include <freeradius-devel/rad_assert.h>
+#include <freeradius-devel/autoconf.h>
 
 #include <sys/file.h>
 
@@ -51,6 +52,10 @@ RCSID("$Id$")
 #	define WIFEXITED(stat_val) (((stat_val) & 255) == 0)
 #endif
 
+#ifdef HAVE_SYSTEMD
+#  include <systemd/sd-daemon.h>
+#endif
+
 /*
  *  Global variables.
  */
@@ -379,6 +384,25 @@ int main(int argc, char *argv[])
 		}
 	}
 
+	/*
+	 *  The systemd watchdog enablement must be checked before we
+	 *  daemonize, but the notifications can come from any process.
+	 */
+#ifdef HAVE_SYSTEMD_WATCHDOG
+	if (!check_config) {
+		uint64_t usec;
+
+		if ((sd_watchdog_enabled(0, &usec) > 0) && (usec > 0)) {
+			usec /= 2;
+			fr_timeval_from_usec(&sd_watchdog_interval, usec);
+
+			INFO("systemd watchdog interval is %pT secs", &sd_watchdog_interval);
+		} else {
+			INFO("systemd watchdog is disabled");
+		}
+	}
+#endif
+
 #ifndef __MINGW32__
 	/*
 	 *  Disconnect from session
