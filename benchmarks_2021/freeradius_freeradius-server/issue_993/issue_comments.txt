SQL drivers should all support the sql_fields method
This is a continuation of https://github.com/FreeRADIUS/freeradius-server/issues/984

So db2, iodbc, and unixodbc were all identical

We don't want to provide the method for rlm_sql_null, as that would cause it to register a map_proc which'd do nothing. sql_fields doesn't have to be defined for every driver.

Now complete! Thanks @jpereira!
