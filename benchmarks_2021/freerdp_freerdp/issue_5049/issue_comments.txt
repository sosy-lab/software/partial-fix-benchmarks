2.0.0-rc4 doesn't build with libressl
Sorry, this didn't work. Looks like libressl has `#define OPENSSL_VERSION_NUMBER 0x20000000L` (very annoying of them) and proper fix could look like: 
```
diff --git a/libfreerdp/crypto/tls.c b/libfreerdp/crypto/tls.c
index 20fde415d..76f51701f 100644
--- a/libfreerdp/crypto/tls.c
+++ b/libfreerdp/crypto/tls.c
@@ -656,7 +656,7 @@ static BOOL tls_prepare(rdpTls* tls, BIO* underlying, SSL_METHOD* method,
 #endif


-#if OPENSSL_VERSION_NUMBER >= 0x10100000L || defined(LIBRESSL_VERSION_NUMBER)
+#if OPENSSL_VERSION_NUMBER >= 0x10100000L && !defined(LIBRESSL_VERSION_NUMBER)
 	SSL_CTX_set_security_level(tls->ctx, settings->TlsSecLevel);
 #endif

--
2.19.1
```

With this patch applied to 2.0.0-rc4 freerdp builds and run (checked with remmina). 
@kapsh thx for the info, indeed annoying. Will update tomorrow
Thank you, it works now.