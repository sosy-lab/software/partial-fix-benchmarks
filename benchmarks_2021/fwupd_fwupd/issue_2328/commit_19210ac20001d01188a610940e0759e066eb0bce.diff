diff --git a/libfwupdplugin/fu-common.c b/libfwupdplugin/fu-common.c
index 4822fc9d0f..846ab8310f 100644
--- a/libfwupdplugin/fu-common.c
+++ b/libfwupdplugin/fu-common.c
@@ -2033,6 +2033,55 @@ fu_common_kernel_locked_down (void)
 #endif
 }
 
+/**
+ * fu_common_cpuid:
+ * @leaf: The CPUID level, now called the 'leaf' by Intel
+ * @eax: (out) (nullable): EAX register
+ * @ebx: (out) (nullable): EBX register
+ * @ecx: (out) (nullable): ECX register
+ * @edx: (out) (nullable): EDX register
+ * @error: A #GError or NULL
+ *
+ * Calls CPUID and returns the registers for the given leaf.
+ *
+ * Return value: %TRUE if the registers are set.
+ *
+ * Since: 1.5.0
+ **/
+gboolean
+fu_common_cpuid (guint32 leaf,
+		 guint32 *eax,
+		 guint32 *ebx,
+		 guint32 *ecx,
+		 guint32 *edx,
+		 GError **error)
+{
+#ifdef HAVE_CPUID_H
+	guint eax_tmp = 0;
+	guint ebx_tmp = 0;
+	guint ecx_tmp = 0;
+	guint edx_tmp = 0;
+
+	/* get vendor */
+	__get_cpuid(leaf, &eax_tmp, &ebx_tmp, &ecx_tmp, &edx_tmp);
+	if (eax != NULL)
+		*eax = eax_tmp;
+	if (ebx != NULL)
+		*ebx = ebx_tmp;
+	if (ecx != NULL)
+		*ecx = ecx_tmp;
+	if (edx != NULL)
+		*edx = edx_tmp;
+	return TRUE;
+#else
+	g_set_error_literal (error,
+			     G_IO_ERROR,
+			     G_IO_ERROR_NOT_SUPPORTED,
+			     "no <cpuid.h> support");
+	return FALSE;
+#endif
+}
+
 /**
  * fu_common_is_cpu_intel:
  *
@@ -2045,15 +2094,13 @@ fu_common_kernel_locked_down (void)
 gboolean
 fu_common_is_cpu_intel (void)
 {
-#ifdef HAVE_CPUID_H
-	guint eax = 0;
 	guint ebx = 0;
 	guint ecx = 0;
 	guint edx = 0;
-	guint level = 0;
 
-	/* get vendor */
-	__get_cpuid(level, &eax, &ebx, &ecx, &edx);
+	if (!fu_common_cpuid (0x0, NULL, &ebx, &ecx, &edx, NULL))
+		return FALSE;
+#ifdef HAVE_CPUID_H
 	if (ebx == signature_INTEL_ebx &&
 	    edx == signature_INTEL_edx &&
 	    ecx == signature_INTEL_ecx) {
diff --git a/libfwupdplugin/fu-common.h b/libfwupdplugin/fu-common.h
index d8ac00ce26..0b99ac43fe 100644
--- a/libfwupdplugin/fu-common.h
+++ b/libfwupdplugin/fu-common.h
@@ -226,6 +226,12 @@ gchar		**fu_common_strnsplit		(const gchar	*str,
 						 const gchar	*delimiter,
 						 gint		 max_tokens);
 gboolean	 fu_common_kernel_locked_down	(void);
+gboolean	 fu_common_cpuid		(guint32	 leaf,
+						 guint32	*eax,
+						 guint32	*ebx,
+						 guint32	*ecx,
+						 guint32	*edx,
+						 GError		**error);
 gboolean	 fu_common_is_cpu_intel		(void);
 gboolean	 fu_common_is_live_media	(void);
 GPtrArray	*fu_common_get_volumes_by_kind	(const gchar	*kind,
diff --git a/libfwupdplugin/fwupdplugin.map b/libfwupdplugin/fwupdplugin.map
index 3e90774854..ec78769c2c 100644
--- a/libfwupdplugin/fwupdplugin.map
+++ b/libfwupdplugin/fwupdplugin.map
@@ -612,6 +612,7 @@ LIBFWUPDPLUGIN_1.4.6 {
 
 LIBFWUPDPLUGIN_1.5.0 {
   global:
+    fu_common_cpuid;
     fu_common_filename_glob;
     fu_common_is_cpu_intel;
     fu_device_report_metadata_post;
diff --git a/plugins/pci-bcr/fu-plugin-pci-bcr.c b/plugins/pci-bcr/fu-plugin-pci-bcr.c
index e2fc1e9f49..ee44cb4ea2 100644
--- a/plugins/pci-bcr/fu-plugin-pci-bcr.c
+++ b/plugins/pci-bcr/fu-plugin-pci-bcr.c
@@ -11,10 +11,10 @@
 
 struct FuPluginData {
 	gboolean		 has_device;
+	guint8			 bcr_addr;
 	guint8			 bcr;
 };
 
-#define BCR			0xdc
 #define BCR_WPD			(1 << 0)
 #define BCR_BLE			(1 << 1)
 #define BCR_SMM_BWP		(1 << 5)
@@ -22,9 +22,34 @@ struct FuPluginData {
 void
 fu_plugin_init (FuPlugin *plugin)
 {
-	fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
+	FuPluginData *priv = fu_plugin_alloc_data (plugin, sizeof (FuPluginData));
 	fu_plugin_set_build_hash (plugin, FU_BUILD_HASH);
 	fu_plugin_add_udev_subsystem (plugin, "pci");
+
+	/* this is true except for some Atoms */
+	priv->bcr_addr = 0xdc;
+}
+
+gboolean
+fu_plugin_startup (FuPlugin *plugin, GError **error)
+{
+	FuPluginData *priv = fu_plugin_get_data (plugin);
+	guint32 eax = 0;
+	guint64 tmp;
+	g_autofree gchar *group = NULL;
+
+	/* has a quirk */
+	if (!fu_common_cpuid (0x1, &eax, NULL, NULL, NULL, error))
+		return FALSE;
+	group = g_strdup_printf ("DeviceInstanceId=CPUID\\PRO_%01X&FAM_%01X&MOD_%01X",
+				 (eax >> 12) & 0x3,
+				 (eax >> 8) & 0xf,
+				 (eax >> 4) & 0xf);
+	tmp = fu_plugin_lookup_quirk_by_id_as_uint64 (plugin, group, "BcrAddr");
+	if (tmp > 0)
+		priv->bcr_addr = tmp;
+	return TRUE;
+
 }
 
 static void
@@ -132,7 +157,7 @@ fu_plugin_udev_device_added (FuPlugin *plugin, FuUdevDevice *device, GError **er
 		return FALSE;
 
 	/* grab BIOS Control Register */
-	if (!fu_udev_device_pread (device, BCR, &priv->bcr, error)) {
+	if (!fu_udev_device_pread (device, priv->bcr_addr, &priv->bcr, error)) {
 		g_prefix_error (error, "could not read BCR");
 		return FALSE;
 	}
diff --git a/plugins/pci-bcr/pci-bcr.quirk b/plugins/pci-bcr/pci-bcr.quirk
index 4ddb5007de..7ade46c602 100644
--- a/plugins/pci-bcr/pci-bcr.quirk
+++ b/plugins/pci-bcr/pci-bcr.quirk
@@ -7,3 +7,7 @@ Plugin = pci_bcr
 #  -> drivers/mtd/spi-nor/controllers/intel-spi-pci.c
 [DeviceInstanceId=PCI\VEN_8086&CLASS_0C8000]
 Plugin = pci_bcr
+
+# Intel Atom Bay Trail [Silvermont]
+[DeviceInstanceId=CPUID\PRO_0&FAM_6&MOD_7]
+BcrAddr = 0x54
