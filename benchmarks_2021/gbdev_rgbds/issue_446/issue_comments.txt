RGBASM can fail but output a corrupted .o file
Build the file in memory. Given that the largest Game Boy Color game released outside Japan is 4 MiB, you will probably have more than enough RAM for this on any PC manufactured in the two decades since the release of the Game Boy Color.

However, an I/O error in `fflush()` when writing the file from memory to the file system can still cause problems. To avoid all chance of writing an invalid file: Write the object code to a temporary file in the same directory, then rename it over the original file.
A .o file built from a ROM with a single ASM file will contain not only the 4 MiB ROM, but also symbol information; I don't know exactly how much, but hopefully not too significant.
I think a temporary file is the way to go, as @pinobatch says, but not in the same directory. I would generate it in the OS temporary folder. I don't know how Windows deals with this, to be fair.
If the OS temporary folder and the folder to which the output shall be written are on different drives or different partitions, renaming the object file into place involves a copy that can fail. Three plausible scenarios follow:

- `/tmp` in RAM, output file on HDD
- `/tmp` on HDD, output file on USB flash drive
- `/tmp` on HDD, output file on SMB or NFS mount
I don't know how Windows handles `mktemp`, but I'd look into that. If that fails, it could be tried to write to a buffer in memory, though that might be unnecessary, as mktemp failing is probably a fairly niche case.