Runtime & Build Fixes for FreeBSD
I have made a patch and tested it.
https://github.com/tuaris/freebsd-glusterfs7/blob/master/net/glusterfs7/files/patch-libglusterfs_src_syscall.c

Looks like it works.  I will submit this and [a few other patches](https://github.com/tuaris/freebsd-glusterfs7/blob/master/net/glusterfs7/files) using review.gluster.org.

```
# gluster volume create replicated replica 3 sun.gluster:/gluster/replicated earth.gluster:/gluster/replicated moon.gluster:/gluster/replicated
volume create: replicated: success: please start the volume to access data
# gluster volume start replicated
volume start: replicated: success
# gluster volume status
Status of volume: replicated
Gluster process                             TCP Port  RDMA Port  Online  Pid
------------------------------------------------------------------------------
Brick sun.gluster:/gluster/replicated       N/A       N/A        N       N/A  
Brick earth.gluster:/gluster/replicated     N/A       N/A        N       N/A  
Brick moon.gluster:/gluster/replicated      N/A       N/A        N       N/A  
Self-heal Daemon on localhost               N/A       N/A        N       N/A  
Self-heal Daemon on earth.gluster           N/A       N/A        N       N/A  
Self-heal Daemon on sun.gluster.morante.com N/A       N/A        N       N/A  
 
Task Status of Volume replicated
------------------------------------------------------------------------------
There are no active volume tasks

@tuaris if things are still working good for you, can you submit the patches?
@amarts yep, just trying to figure out how to test if these changes won't have negative effects on other platoforms.
A patch https://review.gluster.org/24829 has been posted that references this issue.

FreeBSD patches for fuse mount utility

Change-Id: Ib2bac85c28905bb8997fbb64db2308f2a6f31720
Fixes: #1376


A patch https://review.gluster.org/24830 has been posted that references this issue.

Missing link to mntent_compat for glusterd

Change-Id: I5d6d38759de4492de3256995e79d01b9ed7befef
Fixes: #1376


A patch https://review.gluster.org/24831 has been posted that references this issue.

BSD: extattr_set_link and extattr_set_fd return bytes written not 0

Change-Id: I120006dab4e199ad3d3f4a5ebc9c352f3c49ea7d
Fixes: #1376


A patch https://review.gluster.org/24832 has been posted that references this issue.

Add missing links to userspace RCU

Change-Id: I77519eb5ffb6a4a1f51f8c60f3fdd0eb0576c4ee
Fixes: #1376


A patch https://review.gluster.org/24833 has been posted that references this issue.

Use sysctl for get_mem_size() on FreeBSD

Change-Id: Iab2600260707af02e2b8da9e489cbb12b9fc14a6
Fixes: #1376


A patch https://review.gluster.org/24834 has been posted that references this issue.

Remove need for /proc on FreeBSD

Change-Id: Ieebd9a54307813954011ac8833824831dce6da10
Fixes: #1376


@amarts I apologize if I'm doing something wrong.  I'm not very familiar with this tooling :).  I think I should have created separate issues and branches for each of these changes.
Nope, all ok for now. Thanks.
Thank you for your assistance in merging these in.  What can I do to help with FreeBSD smoke and regression testing?