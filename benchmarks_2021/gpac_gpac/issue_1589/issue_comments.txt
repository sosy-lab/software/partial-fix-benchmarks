Windows 10 long path support
More info:

https://github.com/staxrip/staxrip/wiki/Windows-10-long-path-support
Hi, 

Thanks for the wiki page, it was very useful. 

I think I have something that works. I haven't done a ton of tests with long paths (mostly focused on non-regression), but something like this (with paths around 1300 chars long) seems to work now:

```sh
MP4Box -add "C:\Users\adavid\dev\gpac\filters\tests\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\counter_30s_audio.aac" -new "C:\Users\adavid\dev\gpac\filters\tests\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\longout.mp4"
```


Note that inside gpac, a lot of path objects are defined with a GF_MAX_PATH (=1024) limit so some operations might hit that limit before the windows limit.





@stax76 can you check it it's working for you (and close this issue if it's all ok)

thanks
I usually don't make builds but wait for builds made by Patman.
closing, reopen if needed
Please reopen, I don't have permission to do so.

It still produces and error:

MP4Box 1.1.0-DEV-rev384-gf9e004333-x64-gcc10.2.0 Patman

D:\Projekte\VB\staxrip\bin\Apps\Support\MP4Box\MP4Box.exe -add "C:\Users\frank\long path video samples\aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\fitness_temp\ID1 9ms German.aac:name= " -new "C:\Users\frank\long path video samples\aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\fitness_temp\ID1 9ms German.m4a"

Cannot create directory "": last error 3
[Core] system failure for file opening of "\gpac_280_00006517_2" in mode "w+b": 0x00000005
Cannot open destination file C:\Users\frank\long path video samples\aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\fitness_temp\ID1 9ms German.m4a: I/O Error

Please note that it has to be tested on Windows, I checked the manifest using Resource Hacker, and it's missing the long path flag, this much likely means that any Win32 API you call will not support long path. Manifest is necessary, you will need to add this manifest:

```
<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<assembly xmlns="urn:schemas-microsoft-com:asm.v1" manifestVersion="1.0">
    <assemblyIdentity type="win32" name="MP4Box" version="1.0.0.0"/>
    <application xmlns="urn:schemas-microsoft-com:asm.v3">
        <windowsSettings>
            <longPathAware xmlns="http://schemas.microsoft.com/SMI/2016/WindowsSettings">true</longPathAware>
        </windowsSettings>
    </application>
</assembly>
```
@aureliendavid can you take a look ?
Hi @stax76 

The problem is that the changes I made only applies to the visual studio build and the official binaries. 

If I open mp4box.exe from https://download.tsi.telecom-paristech.fr/gpac/new_builds/win64/gpac-1.1.0-DEV-rev423-g37508607-master-x64.exe in Resource Hacker I do get this: 

![mp4boxmanifeset](https://user-images.githubusercontent.com/7168263/104020033-d25ddb80-51bc-11eb-936e-112a247b9886.png)

I would have to see how "Patman builds" are made (do you have a link?) but from the filename your mentioned I guess it is using mingw. I will look into how to enable long path support in our Makefile with mingw. 

Hello aureliendavid,

thank you for the support.

AFAIK @Patman86 is using mabs:

https://github.com/m-ab-s/media-autobuild_suite

@1480c1
@wiiaboo
I'm using mingw right! Mabs only for ffmpeg @stax76 😉
Thanks I'll look into this. 

It looks like there are stuff about manifests that can be done with windres on mingw. 

If someone has a good doc about all that I'll take it but I'll try some things. 


Ok I think 39a078f should solve this for mingw. 

It works for my tests, @stax76 I'll let you check your use cases and report back.
oops there was a typo in the previous commit, please use at least af9bc87 to test this feature
Great news, thanks.

I'll test it as soon as @Patman86 makes a new build.
It's causing a hang/freeze due to this code:

```
proc.Process.StartInfo.EnvironmentVariables("TEMP") = proj.TempDir
proc.Process.StartInfo.EnvironmentVariables("TMP") = proj.TempDir
```

Not sure if Windows, .NET Framework or MP4Box is responsible for the hang, happens only when the path exceeds 260 characters.
@aureliendavid 

The problem with the temp folder is caused by MP4Box according to a test I made. So far I tested audio and chapter demux and video, audio and chapter muxing and confirm that long paths are working now except when changing the temp folder. So it's almost done, thank you for working on it.
Good news - could you detail the faulty test?
If the env var TEMP and TMP on Windows have been changed to a path longer than 260 characters then MP4Box does not start processing and the process don't exit, it hangs.

To verify that MP4Box and not Windows or .NET is causing it, I changed TEMP and TMP for mkvmerge and here it worked correctly.

TEMP and TMP were changed in early days because MP4Box wrote large temp files to the system temp folder without deleting them afterwards, but maybe that has been fixed, in that case I could just remove the code.
well this is interesting.. 

it looks like the windows API function `GetTempPath()` hangs when both these conditions are true:
 1. the long path feature is enabled by manifest
 2. the TMP environmnent variable is longer that MAX_PATH (260)

the `GetEnvironmentVariable()` works under any of these conditions. 

this simple program hangs when 1 & 2 are true: 

```c
#include "windows.h"
int main()
{
    wchar_t tmp1[1024];
    GetEnvironmentVariable(L"TMP", tmp1, 1024); // this works ok 
    GetTempPath(1024, tmp1);                    // this hangs
}
```
(it also hangs without unicode and wide chars)

I haven't seen it mentioned anywhere from quickly googling (where can we even report bugs to microsoft?). 

I think we'll have to code our own `gf_get_temp_path()` using `GetEnvironmentVariable()`. [MSDN](https://docs.microsoft.com/en-us/windows/win32/api/fileapi/nf-fileapi-gettemppatha) explains: 

> The GetTempPath function checks for the existence of environment variables in the following order and uses the first path found:
> 
>     The path specified by the TMP environment variable.
>     The path specified by the TEMP environment variable.
>     The path specified by the USERPROFILE environment variable.
>     The Windows directory.




hm I think this might be hitting some more fundamental limit of windows processes

If I run this in cmd.exe: 

```
> set TMP=C:\aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa\
> calc
```

it hangs (and so does any other executable).

It looks like I can make it work within the visual studio debugger (although tmpfile() also hangs and would need to be replaced) but I can't run anything in a console so I don't even know how to test it for mingw for example. 



I guess long path is not supported everywhere in Windows.

I'll investigate if MP4Box still leaves large files in the temp folder.

Thank you for long path support, temp is not so important, issue can be closed.
> TEMP and TMP were changed in early days because MP4Box wrote large temp files to the system temp folder without deleting them afterwards, but maybe that has been fixed, in that case I could just remove the code.

@stax76 can you check that again? I think the cleanup is done properly in recent versions (if not can you detail your test?)

If you can do without the big TMP variable I'd happily mark this out of scope and a windows problem since it looks a pain to solve.. 

(sorry for the cross-post)
> Thank you for long path support, temp is not so important, issue can be closed.

feel free to open another issue if there are problems with temp files 
OK, I've removed the temp code now and hope it's fine, otherwise I will report it. Thanks again.