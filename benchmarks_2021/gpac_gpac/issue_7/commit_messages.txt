add missing GF_EXPORT

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5547 63c20433-aa62-49bd-875c-5a186b69a8fb
cleanup HLS before doing some updates:
http://www.gpac-licensing.com/2014/12/08/apple-hls-technical-depth/

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5548 63c20433-aa62-49bd-875c-5a186b69a8fb
safely handle up to HLS v3 + byte-range from v4
#7

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5550 63c20433-aa62-49bd-875c-5a186b69a8fb
HLS v4 preliminary - still missing separate streams
#7

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5551 63c20433-aa62-49bd-875c-5a186b69a8fb
add separate streams from HLSv4 and fix misc leaks - still missing multi-lang
#7

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5553 63c20433-aa62-49bd-875c-5a186b69a8fb
HLS/DASH: fix resilience when the first segment of the playlist is not available (anymore?)
#7

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5554 63c20433-aa62-49bd-875c-5a186b69a8fb
HLSv4: add multilang audio
#7

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5555 63c20433-aa62-49bd-875c-5a186b69a8fb
avoid leak on HLS descriptor redefinition
#7

git-svn-id: http://svn.code.sf.net/p/gpac/code/trunk/gpac@5556 63c20433-aa62-49bd-875c-5a186b69a8fb
Merge pull request #7 from rauf/changes

removed defines
