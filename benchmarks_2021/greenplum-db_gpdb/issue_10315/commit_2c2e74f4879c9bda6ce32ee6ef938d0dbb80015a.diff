diff --git a/src/backend/cdb/cdbpullup.c b/src/backend/cdb/cdbpullup.c
index 7fda71ed023d..d5781019a4f3 100644
--- a/src/backend/cdb/cdbpullup.c
+++ b/src/backend/cdb/cdbpullup.c
@@ -476,7 +476,11 @@ cdbpullup_missingVarWalker(Node *node, void *targetlist)
 	if (!node)
 		return false;
 
-	if (IsA(node, Var))
+	/*
+	 * Should also consider PlaceHolderVar in the targetlist.
+	 * See github issue: https://github.com/greenplum-db/gpdb/issues/10315
+	 */
+	if (IsA(node, Var) || IsA(node, PlaceHolderVar))
 	{
 		if (!targetlist)
 			return true;
diff --git a/src/test/regress/expected/join_gp.out b/src/test/regress/expected/join_gp.out
index 5bcd1d203ae6..f690ec8a73d5 100644
--- a/src/test/regress/expected/join_gp.out
+++ b/src/test/regress/expected/join_gp.out
@@ -1536,6 +1536,7 @@ select * from foo where exists (select 1 from bar where foo.a = bar.b);
 reset enable_hashagg;
 drop table foo;
 drop table bar;
+
 -- Fix github issue 10012
 create table fix_param_a (i int, j int);
 NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'i' as the Greenplum Database data distribution key for this table.
@@ -1601,3 +1602,44 @@ select * from fix_param_a left join fix_param_b on
  20 | 20 | 20 | 20
 (20 rows)
 
+-- Test targetlist contains placeholder var
+-- When creating a redistributed motion with hash keys,
+-- Greenplum planner will invoke `cdbpullup_findEclassInTargetList`.
+-- The following test case contains non-strict function `coalesce`
+-- in the subquery at nullable-side of outerjoin and thus will
+-- have PlaceHolderVar in targetlist. The case is to test if
+-- function `cdbpullup_findEclassInTargetList` handles PlaceHolderVar
+-- correct.
+-- See github issue: https://github.com/greenplum-db/gpdb/issues/10315
+create table t_issue_10315 ( id1 int, id2 int );
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'id1' as the Greenplum Database data distribution key for this table.
+HINT:  The 'DISTRIBUTED BY' clause determines the distribution of data. Make sure column(s) chosen are the optimal data distribution key to minimize skew.
+insert into t_issue_10315 select i,i from generate_series(1, 2)i;
+insert into t_issue_10315 select i,null from generate_series(1, 2)i;
+insert into t_issue_10315 select null,i from generate_series(1, 2)i;
+select *  from
+( select coalesce( bq.id1 ) id1, coalesce ( bq.id2 ) id2
+        from ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) bq  ) t
+full join ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) bq_all
+on t.id1 = bq_all.id1  and t.id2 = bq_all.id2
+full join ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) tq_all
+on (coalesce(t.id1) = tq_all.id1  and t.id2 = tq_all.id2) ;
+ id1 | id2 | id1 | id2 | id1 | id2 
+-----+-----+-----+-----+-----+-----
+   2 |   2 |   2 |   2 |   2 |   2
+   2 |     |     |     |     |    
+     |   1 |     |     |     |    
+     |   2 |     |     |     |    
+     |     |     |   2 |     |    
+     |     |     |   1 |     |    
+     |     |   2 |     |     |    
+     |     |   1 |     |     |    
+     |     |     |     |     |   2
+     |     |     |     |     |   1
+     |     |     |     |   2 |    
+   1 |   1 |   1 |   1 |   1 |   1
+   1 |     |     |     |     |    
+     |     |     |     |   1 |    
+(14 rows)
+
+drop table t_issue_10315;
diff --git a/src/test/regress/expected/join_gp_optimizer.out b/src/test/regress/expected/join_gp_optimizer.out
index 125e27ce1b13..033dcbf7e2a7 100644
--- a/src/test/regress/expected/join_gp_optimizer.out
+++ b/src/test/regress/expected/join_gp_optimizer.out
@@ -1523,6 +1523,7 @@ select * from foo where exists (select 1 from bar where foo.a = bar.b);
 reset enable_hashagg;
 drop table foo;
 drop table bar;
+
 -- Fix github issue 10012
 create table fix_param_a (i int, j int);
 NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'i' as the Greenplum Database data distribution key for this table.
@@ -1588,3 +1589,44 @@ select * from fix_param_a left join fix_param_b on
  20 | 20 | 20 | 20
 (20 rows)
 
+-- Test targetlist contains placeholder var
+-- When creating a redistributed motion with hash keys,
+-- Greenplum planner will invoke `cdbpullup_findEclassInTargetList`.
+-- The following test case contains non-strict function `coalesce`
+-- in the subquery at nullable-side of outerjoin and thus will
+-- have PlaceHolderVar in targetlist. The case is to test if
+-- function `cdbpullup_findEclassInTargetList` handles PlaceHolderVar
+-- correct.
+-- See github issue: https://github.com/greenplum-db/gpdb/issues/10315
+create table t_issue_10315 ( id1 int, id2 int );
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'id1' as the Greenplum Database data distribution key for this table.
+HINT:  The 'DISTRIBUTED BY' clause determines the distribution of data. Make sure column(s) chosen are the optimal data distribution key to minimize skew.
+insert into t_issue_10315 select i,i from generate_series(1, 2)i;
+insert into t_issue_10315 select i,null from generate_series(1, 2)i;
+insert into t_issue_10315 select null,i from generate_series(1, 2)i;
+select *  from
+( select coalesce( bq.id1 ) id1, coalesce ( bq.id2 ) id2
+        from ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) bq  ) t
+full join ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) bq_all
+on t.id1 = bq_all.id1  and t.id2 = bq_all.id2
+full join ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) tq_all
+on (coalesce(t.id1) = tq_all.id1  and t.id2 = tq_all.id2) ;
+ id1 | id2 | id1 | id2 | id1 | id2 
+-----+-----+-----+-----+-----+-----
+   2 |   2 |   2 |   2 |   2 |   2
+   2 |     |     |     |     |    
+     |   1 |     |     |     |    
+     |   2 |     |     |     |    
+     |     |     |   2 |     |    
+     |     |     |   1 |     |    
+     |     |   2 |     |     |    
+     |     |   1 |     |     |    
+     |     |     |     |     |   2
+     |     |     |     |     |   1
+     |     |     |     |   2 |    
+   1 |   1 |   1 |   1 |   1 |   1
+   1 |     |     |     |     |    
+     |     |     |     |   1 |    
+(14 rows)
+
+drop table t_issue_10315;
diff --git a/src/test/regress/sql/join_gp.sql b/src/test/regress/sql/join_gp.sql
index 5cfc7005610a..3b5cb0e2dd6b 100644
--- a/src/test/regress/sql/join_gp.sql
+++ b/src/test/regress/sql/join_gp.sql
@@ -742,3 +742,28 @@ select * from fix_param_a left join fix_param_b on
 	fix_param_a.i = fix_param_b.i and fix_param_b.j in
 		(select j from fix_param_c where fix_param_b.i = fix_param_c.i)
 	order by 1;
+
+-- Test targetlist contains placeholder var
+-- When creating a redistributed motion with hash keys,
+-- Greenplum planner will invoke `cdbpullup_findEclassInTargetList`.
+-- The following test case contains non-strict function `coalesce`
+-- in the subquery at nullable-side of outerjoin and thus will
+-- have PlaceHolderVar in targetlist. The case is to test if
+-- function `cdbpullup_findEclassInTargetList` handles PlaceHolderVar
+-- correct.
+-- See github issue: https://github.com/greenplum-db/gpdb/issues/10315
+create table t_issue_10315 ( id1 int, id2 int );
+
+insert into t_issue_10315 select i,i from generate_series(1, 2)i;
+insert into t_issue_10315 select i,null from generate_series(1, 2)i;
+insert into t_issue_10315 select null,i from generate_series(1, 2)i;
+
+select *  from
+( select coalesce( bq.id1 ) id1, coalesce ( bq.id2 ) id2
+        from ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) bq  ) t
+full join ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) bq_all
+on t.id1 = bq_all.id1  and t.id2 = bq_all.id2
+full join ( select r.id1, r.id2 from t_issue_10315 r group by r.id1, r.id2 ) tq_all
+on (coalesce(t.id1) = tq_all.id1  and t.id2 = tq_all.id2) ;
+
+drop table t_issue_10315;
