full join and coalesce lead to "could not find hash distribution key expressions in target list"
Reproduce this issue on the master branch also.

Thanks for reporting this, will look into it.
The plan produced by gpdb5 is:

```
                                                             QUERY PLAN
-------------------------------------------------------------------------------------------------------------------------------------
 Gather Motion 3:1  (slice3; segments: 3)  (cost=2000007502.45..2000007632.35 rows=8610 width=24)
   ->  Merge Full Join  (cost=2000007502.45..2000007632.35 rows=2870 width=24)
         Merge Cond: (COALESCE((COALESCE(bq.id1)))) = r.id1 AND (COALESCE(bq.id2)) = r.id2
         ->  Sort  (cost=1000005376.01..1000005397.54 rows=2870 width=16)
               Sort Key: (COALESCE((COALESCE(bq.id1)))), (COALESCE(bq.id2))
               ->  Result  (cost=1000004511.18..1000004813.27 rows=2870 width=16)
                     ->  Redistribute Motion 3:3  (slice2; segments: 3)  (cost=1000004511.18..1000004813.27 rows=2870 width=16)
                           Hash Key: COALESCE((COALESCE(bq.id1)))
                           ->  Merge Full Join  (cost=1000004511.18..1000004641.07 rows=2870 width=16)
                                 Merge Cond: (COALESCE(bq.id1)) = r.id1 AND (COALESCE(bq.id2)) = r.id2
                                 ->  Sort  (cost=2384.74..2406.27 rows=2870 width=8)
                                       Sort Key: (COALESCE(bq.id1)), (COALESCE(bq.id2))
                                       ->  Redistribute Motion 3:3  (slice1; segments: 3)  (cost=1391.50..1822.00 rows=2870 width=8)
                                             Hash Key: (COALESCE(bq.id1))
                                             ->  Subquery Scan bq  (cost=1391.50..1563.70 rows=2870 width=8)
                                                   ->  HashAggregate  (cost=1391.50..1477.60 rows=2870 width=8)
                                                         Group By: r.id1, r.id2
                                                         ->  Seq Scan on tab1 r  (cost=0.00..961.00 rows=28700 width=8)
                                 ->  Sort  (cost=2126.44..2147.97 rows=2870 width=8)
                                       Sort Key: r.id1, r.id2
                                       ->  HashAggregate  (cost=1391.50..1477.60 rows=2870 width=8)
                                             Group By: r.id1, r.id2
                                             ->  Seq Scan on tab2 r  (cost=0.00..961.00 rows=28700 width=8)
         ->  Sort  (cost=2126.44..2147.97 rows=2870 width=8)
               Sort Key: r.id1, r.id2
               ->  HashAggregate  (cost=1391.50..1477.60 rows=2870 width=8)
                     Group By: r.id1, r.id2
                     ->  Seq Scan on tab3 r  (cost=0.00..961.00 rows=28700 width=8)
 Optimizer status: legacy query optimizer
```

------------------------

The planner in gpdb6 and master branch throw an error when creating the first redistributed motion(from top):

```
  ->  Redistribute Motion 3:3  (slice2; segments: 3)  (cost=1000004511.18..1000004813.27 rows=2870 width=16)
                           Hash Key: COALESCE((COALESCE(bq.id1)))
```

The hash key is computed from targetlist of the motion's subplan, Greenplum planner invoke `cdbpullup_findEclassInTargetList` to confirm that each distkey(hashkey) can be found in targetlist.

Distkey if not directly appear in targetlist, it has to  satisfy the condition: ***all referenced Vars are in targetlist.***. (see code `cdbpullup_findEclassInTargetList`).

This walker 

```c
if (!IsA(key, Var) &&
			!cdbpullup_missingVarWalker((Node *) key, targetlist))
		{
			return key;
		}
```

does not handle placeHolderVar correct.

The case in this issue:

the distkey is 

```
CoalesceExpr [coalescetype=23 coalescecollid=0 location=586]
        [args]
                PlaceHolderVar [phrels=0x00000040 phid=1 phlevelsup=0]
                        [phexpr]
                                CoalesceExpr [coalescetype=23 coalescecollid=0 location=49]
                                        [args] Var [varno=6 varattno=1 vartype=23 varnoold=6 varoattno=1]
```

the targetlist is

```
TargetEntry [resno=1]
        Var [varno=2 varattno=1 vartype=23 varnoold=2 varoattno=1]
TargetEntry [resno=2]
        Var [varno=2 varattno=2 vartype=23 varnoold=2 varoattno=2]
TargetEntry [resno=3]
        PlaceHolderVar [phrels=0x00000040 phid=1 phlevelsup=0]
                [phexpr]
                        CoalesceExpr [coalescetype=23 coalescecollid=0 location=49]
                                [args] Var [varno=6 varattno=1 vartype=23 varnoold=6 varoattno=1]
TargetEntry [resno=4]
        PlaceHolderVar [phrels=0x00000040 phid=2 phlevelsup=0]
                [phexpr]
                        CoalesceExpr [coalescetype=23 coalescecollid=0 location=78]
                                [args] Var [varno=6 varattno=2 vartype=23 varnoold=6 varoattno=2]
```

---------------

The above is the root cause of the issue.

Will keep working on it to fix.

Open a pr to fix this on master: https://github.com/greenplum-db/gpdb/pull/10346
Fixed by https://github.com/greenplum-db/gpdb/pull/10346 on master and 
https://github.com/greenplum-db/gpdb/pull/10353 on 6X.

5X does not have such issue.

Thanks for reporting!