Presence of function named 'sum' in a non public schema throws exception for create table in gpdb6
Fixed by PR #4308. Thanks for the report!