diff --git a/src/backend/optimizer/plan/planner.c b/src/backend/optimizer/plan/planner.c
index 72ded114219..e5f8db1936b 100644
--- a/src/backend/optimizer/plan/planner.c
+++ b/src/backend/optimizer/plan/planner.c
@@ -1283,6 +1283,23 @@ grouping_planner(PlannerInfo *root, double tuple_fraction)
 	gp_motion_cost_per_row :
 	2.0 * cpu_tuple_cost;
 
+	/*
+	 * If limit clause contains volatile functions, they should be
+	 * evaluated only once. For such cases, we should not push down
+	 * the limit.
+	 *
+	 * Words on multi-stage limit: current interconnect implementation
+	 * model is sender will send when buffer is full. Under such
+	 * condition, multi-stage limit might improve performance for
+	 * some cases.
+	 *
+	 * TODO: we might investigate that evaluating limit clause first,
+	 * and then doing pushdown it in future.
+	 */
+	bool        limit_contain_volatile_functions;
+	limit_contain_volatile_functions = (contain_volatile_functions(parse->limitCount)
+					    || contain_volatile_functions(parse->limitOffset));
+
 	CdbPathLocus_MakeNull(&current_locus);
 
 	/* Tweak caller-supplied tuple_fraction if have LIMIT/OFFSET */
@@ -2022,7 +2039,8 @@ grouping_planner(PlannerInfo *root, double tuple_fraction)
 					 * the entire sorted result-set by plunking a limit on the
 					 * top of the unique-node.
 					 */
-					if (parse->limitCount)
+					if (parse->limitCount &&
+					    !limit_contain_volatile_functions)
 					{
 						/*
 						 * Our extra limit operation is basically a
@@ -2112,7 +2130,8 @@ grouping_planner(PlannerInfo *root, double tuple_fraction)
 		if (Gp_role == GP_ROLE_DISPATCH && result_plan->flow->flotype == FLOW_PARTITIONED)
 		{
 			/* pushdown the first phase of multi-phase limit (which takes offset into account) */
-			result_plan = pushdown_preliminary_limit(result_plan, parse->limitCount, count_est, parse->limitOffset, offset_est);
+		        if(!limit_contain_volatile_functions)
+			    result_plan = pushdown_preliminary_limit(result_plan, parse->limitCount, count_est, parse->limitOffset, offset_est);
 			
 			/* Focus on QE [merge to preserve order], prior to final LIMIT. */
 			result_plan = (Plan *) make_motion_gather_to_QE(result_plan, current_pathkeys != NIL);
diff --git a/src/test/regress/expected/limit_gp.out b/src/test/regress/expected/limit_gp.out
index 6bb554f9f21..b7981ab48ed 100644
--- a/src/test/regress/expected/limit_gp.out
+++ b/src/test/regress/expected/limit_gp.out
@@ -84,3 +84,100 @@ select * from generate_series(1,10) g limit count(*);
 ERROR:  argument of LIMIT must not contain aggregates
 LINE 1: select * from generate_series(1,10) g limit count(*);
                                                     ^
+-- Check volatile limit should not pushdown.
+create table t_volatile_limit (i int4);
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'i' as the Greenplum Database data distribution key for this table.
+HINT:  The 'DISTRIBUTED BY' clause determines the distribution of data. Make sure column(s) chosen are the optimal data distribution key to minimize skew.
+create table t_volatile_limit_1 (a int, b int) distributed randomly;
+-- Greenplum may generate two-stage limit plan to improve performance.
+-- But for limit clause contains volatile functions, if we push them down
+-- below the final gather motion, those volatile functions will be evaluated
+-- many times. For such cases, we should not push down the limit.
+-- Below test cases' limit clause contain function call `random` with order by.
+-- `random()` is a volatile function it may return different results each time
+-- invoked. If we push down to generate two-stage limit plan, `random()` will
+-- execute on each segment which leads to different limit values of QEs
+-- and QD and this cannot guarantee correct results. Suppose seg 0 contains the
+-- top 3 minimum values, but random() returns 1, then you lose 2 values.
+explain select * from t_volatile_limit order by i limit (random() * 10);
+                                         QUERY PLAN                                          
+---------------------------------------------------------------------------------------------
+ Limit  (cost=9024.35..9240.80 rows=9620 width=4)
+   ->  Gather Motion 3:1  (slice1; segments: 3)  (cost=9024.35..11188.85 rows=96200 width=4)
+         Merge Key: i
+         ->  Sort  (cost=9024.35..9264.85 rows=32067 width=4)
+               Sort Key: i
+               ->  Seq Scan on t_volatile_limit  (cost=0.00..1062.00 rows=32067 width=4)
+ Settings:  gp_enable_mk_sort=on; optimizer=off
+ Optimizer status: legacy query optimizer
+(8 rows)
+
+explain select * from t_volatile_limit order by i limit 2 offset (random()*5);
+                                         QUERY PLAN                                          
+---------------------------------------------------------------------------------------------
+ Limit  (cost=9240.80..9240.85 rows=2 width=4)
+   ->  Gather Motion 3:1  (slice1; segments: 3)  (cost=9024.35..11188.85 rows=96200 width=4)
+         Merge Key: i
+         ->  Sort  (cost=9024.35..9264.85 rows=32067 width=4)
+               Sort Key: i
+               ->  Seq Scan on t_volatile_limit  (cost=0.00..1062.00 rows=32067 width=4)
+ Settings:  gp_enable_mk_sort=on; optimizer=off
+ Optimizer status: legacy query optimizer
+(8 rows)
+
+explain select distinct(a), sum(b) from t_volatile_limit_1 group by a order by a, sum(b) limit (random()+3);
+                                                                QUERY PLAN                                                                 
+-------------------------------------------------------------------------------------------------------------------------------------------
+ Limit  (cost=1496.36..1496.39 rows=1 width=12)
+   ->  Gather Motion 3:1  (slice3; segments: 3)  (cost=1496.36..1496.39 rows=1 width=12)
+         Merge Key: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+         ->  Unique  (cost=1496.36..1496.37 rows=1 width=12)
+               Group By: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+               ->  Sort  (cost=1496.36..1496.36 rows=1 width=12)
+                     Sort Key (Distinct): t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                     ->  Redistribute Motion 3:3  (slice2; segments: 3)  (cost=1488.83..1496.35 rows=1 width=12)
+                           Hash Key: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                           ->  Unique  (cost=1488.83..1496.33 rows=1 width=12)
+                                 Group By: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                                 ->  Sort  (cost=1488.83..1491.33 rows=334 width=12)
+                                       Sort Key (Distinct): t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                                       ->  HashAggregate  (cost=1426.50..1439.00 rows=334 width=12)
+                                             Group By: t_volatile_limit_1.a
+                                             ->  Redistribute Motion 3:3  (slice1; segments: 3)  (cost=1391.50..1411.50 rows=334 width=12)
+                                                   Hash Key: t_volatile_limit_1.a
+                                                   ->  HashAggregate  (cost=1391.50..1391.50 rows=334 width=12)
+                                                         Group By: t_volatile_limit_1.a
+                                                         ->  Seq Scan on t_volatile_limit_1  (cost=0.00..961.00 rows=28700 width=8)
+ Settings:  gp_enable_mk_sort=on; optimizer=off
+ Optimizer status: legacy query optimizer
+(22 rows)
+
+explain select distinct(a), sum(b) from t_volatile_limit_1 group by a order by a, sum(b) limit 2 offset (random()*2);
+                                                                QUERY PLAN                                                                 
+-------------------------------------------------------------------------------------------------------------------------------------------
+ Limit  (cost=1496.39..1496.39 rows=1 width=12)
+   ->  Gather Motion 3:1  (slice3; segments: 3)  (cost=1496.36..1496.39 rows=1 width=12)
+         Merge Key: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+         ->  Unique  (cost=1496.36..1496.37 rows=1 width=12)
+               Group By: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+               ->  Sort  (cost=1496.36..1496.36 rows=1 width=12)
+                     Sort Key (Distinct): t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                     ->  Redistribute Motion 3:3  (slice2; segments: 3)  (cost=1488.83..1496.35 rows=1 width=12)
+                           Hash Key: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                           ->  Unique  (cost=1488.83..1496.33 rows=1 width=12)
+                                 Group By: t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                                 ->  Sort  (cost=1488.83..1491.33 rows=334 width=12)
+                                       Sort Key (Distinct): t_volatile_limit_1.a, (pg_catalog.sum((sum(t_volatile_limit_1.b))))
+                                       ->  HashAggregate  (cost=1426.50..1439.00 rows=334 width=12)
+                                             Group By: t_volatile_limit_1.a
+                                             ->  Redistribute Motion 3:3  (slice1; segments: 3)  (cost=1391.50..1411.50 rows=334 width=12)
+                                                   Hash Key: t_volatile_limit_1.a
+                                                   ->  HashAggregate  (cost=1391.50..1391.50 rows=334 width=12)
+                                                         Group By: t_volatile_limit_1.a
+                                                         ->  Seq Scan on t_volatile_limit_1  (cost=0.00..961.00 rows=28700 width=8)
+ Settings:  gp_enable_mk_sort=on; optimizer=off
+ Optimizer status: legacy query optimizer
+(22 rows)
+
+drop table t_volatile_limit;
+drop table t_volatile_limit_1;
diff --git a/src/test/regress/expected/limit_gp_optimizer.out b/src/test/regress/expected/limit_gp_optimizer.out
new file mode 100644
index 00000000000..1f671815386
--- /dev/null
+++ b/src/test/regress/expected/limit_gp_optimizer.out
@@ -0,0 +1,177 @@
+-- Check for MPP-19310 and MPP-19857 where mksort produces wrong result
+-- on OPT build, and fails assertion on debug build if a "LIMIT" query
+-- spills to disk.
+CREATE TABLE mksort_limit_test_table(dkey INT, jkey INT, rval REAL, tval TEXT default repeat('abcdefghijklmnopqrstuvwxyz', 300)) DISTRIBUTED BY (dkey);
+INSERT INTO mksort_limit_test_table VALUES(generate_series(1, 10000), generate_series(10001, 20000), sqrt(generate_series(10001, 20000)));
+SET gp_enable_mk_sort = on;
+--Should fit LESS (because of overhead) than (20 * 1024 * 1024) / (26 * 300 + 12) => 2684 tuples in memory, after that spills to disk
+SET statement_mem="20MB";
+-- Should work in memory
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey LIMIT 200) as temp ORDER BY jkey LIMIT 3;
+ dkey | str 
+------+-----
+    1 | ab
+    2 | ab
+    3 | ab
+(3 rows)
+
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey LIMIT 200) as temp ORDER BY jkey DESC LIMIT 3;
+ dkey | str 
+------+-----
+  200 | ab
+  199 | ab
+  198 | ab
+(3 rows)
+
+-- Should spill to disk (tested with 2 segments, for more segments it may not spill)
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey LIMIT 5000) as temp ORDER BY jkey LIMIT 3;
+ dkey | str 
+------+-----
+    1 | ab
+    2 | ab
+    3 | ab
+(3 rows)
+
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey LIMIT 5000) as temp ORDER BY jkey DESC LIMIT 3;
+ dkey | str 
+------+-----
+ 5000 | ab
+ 4999 | ab
+ 4998 | ab
+(3 rows)
+
+-- In memory descending sort
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey DESC LIMIT 200) as temp ORDER BY jkey LIMIT 3;
+ dkey | str 
+------+-----
+ 9801 | ab
+ 9802 | ab
+ 9803 | ab
+(3 rows)
+
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey DESC LIMIT 200) as temp ORDER BY jkey DESC LIMIT 3;
+ dkey  | str 
+-------+-----
+ 10000 | ab
+  9999 | ab
+  9998 | ab
+(3 rows)
+
+-- Spilled descending sort (tested with 2 segments, for more segments it may not spill)
+SELECT dkey, substring(tval from 1 for 2) as str from (SELECT * from mksort_limit_test_table ORDER BY dkey DESC LIMIT 5000) as temp ORDER BY jkey LIMIT 3;
+ dkey | str 
+------+-----
+ 5001 | ab
+ 5002 | ab
+ 5003 | ab
+(3 rows)
+
+SELECT dkey, substring(tval from 1 for 2) as str  from (SELECT * from mksort_limit_test_table ORDER BY dkey DESC LIMIT 5000) as temp ORDER BY jkey DESC LIMIT 3;
+ dkey  | str 
+-------+-----
+ 10000 | ab
+  9999 | ab
+  9998 | ab
+(3 rows)
+
+DROP TABLE  mksort_limit_test_table;
+-- Check invalid things in LIMIT
+select * from generate_series(1,10) g limit g;
+ERROR:  argument of LIMIT must not contain variables
+LINE 1: select * from generate_series(1,10) g limit g;
+                                                    ^
+select * from generate_series(1,10) g limit count(*);
+ERROR:  argument of LIMIT must not contain aggregates
+LINE 1: select * from generate_series(1,10) g limit count(*);
+                                                    ^
+-- Check volatile limit should not pushdown.
+create table t_volatile_limit (i int4);
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'i' as the Greenplum Database data distribution key for this table.
+HINT:  The 'DISTRIBUTED BY' clause determines the distribution of data. Make sure column(s) chosen are the optimal data distribution key to minimize skew.
+create table t_volatile_limit_1 (a int, b int) distributed randomly;
+-- Greenplum may generate two-stage limit plan to improve performance.
+-- But for limit clause contains volatile functions, if we push them down
+-- below the final gather motion, those volatile functions will be evaluated
+-- many times. For such cases, we should not push down the limit.
+-- Below test cases' limit clause contain function call `random` with order by.
+-- `random()` is a volatile function it may return different results each time
+-- invoked. If we push down to generate two-stage limit plan, `random()` will
+-- execute on each segment which leads to different limit values of QEs
+-- and QD and this cannot guarantee correct results. Suppose seg 0 contains the
+-- top 3 minimum values, but random() returns 1, then you lose 2 values.
+explain select * from t_volatile_limit order by i limit (random() * 10);
+                                      QUERY PLAN                                      
+--------------------------------------------------------------------------------------
+ Limit  (cost=0.00..431.00 rows=1 width=4)
+   ->  Gather Motion 3:1  (slice1; segments: 3)  (cost=0.00..431.00 rows=1 width=4)
+         Merge Key: i
+         ->  Sort  (cost=0.00..431.00 rows=1 width=4)
+               Sort Key: i
+               ->  Table Scan on t_volatile_limit  (cost=0.00..431.00 rows=1 width=4)
+ Settings:  gp_enable_mk_sort=on; optimizer=on
+ Optimizer status: PQO version 3.80.0
+(8 rows)
+
+explain select * from t_volatile_limit order by i limit 2 offset (random()*5);
+                                      QUERY PLAN                                      
+--------------------------------------------------------------------------------------
+ Limit  (cost=0.00..431.00 rows=1 width=4)
+   ->  Gather Motion 3:1  (slice1; segments: 3)  (cost=0.00..431.00 rows=1 width=4)
+         Merge Key: i
+         ->  Sort  (cost=0.00..431.00 rows=1 width=4)
+               Sort Key: i
+               ->  Table Scan on t_volatile_limit  (cost=0.00..431.00 rows=1 width=4)
+ Settings:  gp_enable_mk_sort=on; optimizer=on
+ Optimizer status: PQO version 3.80.0
+(8 rows)
+
+explain select distinct(a), sum(b) from t_volatile_limit_1 group by a order by a, sum(b) limit (random()+3);
+                                                         QUERY PLAN                                                         
+----------------------------------------------------------------------------------------------------------------------------
+ Limit  (cost=0.00..431.00 rows=1 width=12)
+   ->  Gather Motion 3:1  (slice2; segments: 3)  (cost=0.00..431.00 rows=1 width=12)
+         Merge Key: a, (pg_catalog.sum((sum(b))))
+         ->  Sort  (cost=0.00..431.00 rows=1 width=12)
+               Sort Key: a, (pg_catalog.sum((sum(b))))
+               ->  GroupAggregate  (cost=0.00..431.00 rows=1 width=12)
+                     Group By: a
+                     ->  Sort  (cost=0.00..431.00 rows=1 width=12)
+                           Sort Key: a
+                           ->  Redistribute Motion 3:3  (slice1; segments: 3)  (cost=0.00..431.00 rows=1 width=12)
+                                 Hash Key: a
+                                 ->  Result  (cost=0.00..431.00 rows=1 width=12)
+                                       ->  GroupAggregate  (cost=0.00..431.00 rows=1 width=12)
+                                             Group By: a
+                                             ->  Sort  (cost=0.00..431.00 rows=1 width=8)
+                                                   Sort Key: a
+                                                   ->  Table Scan on t_volatile_limit_1  (cost=0.00..431.00 rows=1 width=8)
+ Settings:  gp_enable_mk_sort=on; optimizer=on
+ Optimizer status: PQO version 3.80.0
+(19 rows)
+
+explain select distinct(a), sum(b) from t_volatile_limit_1 group by a order by a, sum(b) limit 2 offset (random()*2);
+                                                         QUERY PLAN                                                         
+----------------------------------------------------------------------------------------------------------------------------
+ Limit  (cost=0.00..431.00 rows=1 width=12)
+   ->  Gather Motion 3:1  (slice2; segments: 3)  (cost=0.00..431.00 rows=1 width=12)
+         Merge Key: a, (pg_catalog.sum((sum(b))))
+         ->  Sort  (cost=0.00..431.00 rows=1 width=12)
+               Sort Key: a, (pg_catalog.sum((sum(b))))
+               ->  GroupAggregate  (cost=0.00..431.00 rows=1 width=12)
+                     Group By: a
+                     ->  Sort  (cost=0.00..431.00 rows=1 width=12)
+                           Sort Key: a
+                           ->  Redistribute Motion 3:3  (slice1; segments: 3)  (cost=0.00..431.00 rows=1 width=12)
+                                 Hash Key: a
+                                 ->  Result  (cost=0.00..431.00 rows=1 width=12)
+                                       ->  GroupAggregate  (cost=0.00..431.00 rows=1 width=12)
+                                             Group By: a
+                                             ->  Sort  (cost=0.00..431.00 rows=1 width=8)
+                                                   Sort Key: a
+                                                   ->  Table Scan on t_volatile_limit_1  (cost=0.00..431.00 rows=1 width=8)
+ Settings:  gp_enable_mk_sort=on; optimizer=on
+ Optimizer status: PQO version 3.80.0
+(19 rows)
+
+drop table t_volatile_limit;
+drop table t_volatile_limit_1;
diff --git a/src/test/regress/sql/limit_gp.sql b/src/test/regress/sql/limit_gp.sql
index 9b84d9140be..d13b41d59af 100644
--- a/src/test/regress/sql/limit_gp.sql
+++ b/src/test/regress/sql/limit_gp.sql
@@ -32,3 +32,27 @@ DROP TABLE  mksort_limit_test_table;
 
 select * from generate_series(1,10) g limit g;
 select * from generate_series(1,10) g limit count(*);
+
+-- Check volatile limit should not pushdown.
+create table t_volatile_limit (i int4);
+create table t_volatile_limit_1 (a int, b int) distributed randomly;
+
+-- Greenplum may generate two-stage limit plan to improve performance.
+-- But for limit clause contains volatile functions, if we push them down
+-- below the final gather motion, those volatile functions will be evaluated
+-- many times. For such cases, we should not push down the limit.
+
+-- Below test cases' limit clause contain function call `random` with order by.
+-- `random()` is a volatile function it may return different results each time
+-- invoked. If we push down to generate two-stage limit plan, `random()` will
+-- execute on each segment which leads to different limit values of QEs
+-- and QD and this cannot guarantee correct results. Suppose seg 0 contains the
+-- top 3 minimum values, but random() returns 1, then you lose 2 values.
+explain select * from t_volatile_limit order by i limit (random() * 10);
+explain select * from t_volatile_limit order by i limit 2 offset (random()*5);
+
+explain select distinct(a), sum(b) from t_volatile_limit_1 group by a order by a, sum(b) limit (random()+3);
+explain select distinct(a), sum(b) from t_volatile_limit_1 group by a order by a, sum(b) limit 2 offset (random()*2);
+
+drop table t_volatile_limit;
+drop table t_volatile_limit_1;
