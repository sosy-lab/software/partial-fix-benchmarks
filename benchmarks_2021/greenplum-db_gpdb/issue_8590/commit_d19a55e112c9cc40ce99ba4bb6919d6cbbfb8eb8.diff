diff --git a/src/backend/optimizer/plan/planner.c b/src/backend/optimizer/plan/planner.c
index 0a6d67eb9dd..d40ea98aa0a 100644
--- a/src/backend/optimizer/plan/planner.c
+++ b/src/backend/optimizer/plan/planner.c
@@ -3297,7 +3297,22 @@ grouping_planner(PlannerInfo *root, double tuple_fraction)
 			(result_plan->flow->flotype == FLOW_PARTITIONED ||
 			 result_plan->flow->locustype == CdbLocusType_SegmentGeneral))
 		{
-			if (result_plan->flow->flotype == FLOW_PARTITIONED)
+			/*
+			 * If limit clause contains volatile functions, they should be
+			 * evaluated only once. For such cases, we should not push down
+			 * the limit.
+			 */
+			bool        limit_contain_volatile_functions;
+
+			limit_contain_volatile_functions = (contain_volatile_functions(parse->limitCount)
+												|| contain_volatile_functions(parse->limitOffset));
+
+			/*
+			 * TODO: deep analyze whether multi-stage limit will benefit performance
+			 * in future.
+			 */
+			if (result_plan->flow->flotype == FLOW_PARTITIONED &&
+				!limit_contain_volatile_functions)
 			{
 				/* pushdown the first phase of multi-phase limit (which takes offset into account) */
 				result_plan = pushdown_preliminary_limit(result_plan, parse->limitCount, count_est, parse->limitOffset, offset_est);
diff --git a/src/test/regress/expected/limit_gp.out b/src/test/regress/expected/limit_gp.out
index 06209caa1b6..56dbcdb5134 100644
--- a/src/test/regress/expected/limit_gp.out
+++ b/src/test/regress/expected/limit_gp.out
@@ -84,3 +84,37 @@ select * from generate_series(1,10) g limit count(*);
 ERROR:  aggregate functions are not allowed in LIMIT
 LINE 1: select * from generate_series(1,10) g limit count(*);
                                                     ^
+-- Check volatile limit should not pushdown.
+create table t_volatile_limit (i int4);
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause -- Using column named 'i' as the Greenplum Database data distribution key for this table.
+HINT:  The 'DISTRIBUTED BY' clause determines the distribution of data. Make sure column(s) chosen are the optimal data distribution key to minimize skew.
+insert into t_volatile_limit select generate_series(1, 100);
+-- Greenplum may generate two-stage limit plan to improve performance.
+-- But for limit clause contains volatile functions, if we push them down
+-- below the final gather motion, those volatile functions will be evaluated
+-- many times. For such cases, we should not push down the limit.
+explain select * from t_volatile_limit order by i limit (random() * 10);
+                                      QUERY PLAN                                      
+--------------------------------------------------------------------------------------
+ Limit  (cost=0.00..431.01 rows=34 width=4)
+   ->  Gather Motion 3:1  (slice1; segments: 3)  (cost=0.00..431.01 rows=100 width=4)
+         Merge Key: i
+         ->  Sort  (cost=0.00..431.00 rows=34 width=4)
+               Sort Key: i
+               ->  Seq Scan on t_volatile_limit  (cost=0.00..431.00 rows=34 width=4)
+ Optimizer: Pivotal Optimizer (GPORCA) version 3.80.0
+(7 rows)
+
+explain select * from t_volatile_limit order by i limit 2 offset (random()*5);
+                                      QUERY PLAN                                      
+--------------------------------------------------------------------------------------
+ Limit  (cost=0.00..431.01 rows=1 width=4)
+   ->  Gather Motion 3:1  (slice1; segments: 3)  (cost=0.00..431.01 rows=100 width=4)
+         Merge Key: i
+         ->  Sort  (cost=0.00..431.00 rows=34 width=4)
+               Sort Key: i
+               ->  Seq Scan on t_volatile_limit  (cost=0.00..431.00 rows=34 width=4)
+ Optimizer: Pivotal Optimizer (GPORCA) version 3.80.0
+(7 rows)
+
+drop table t_volatile_limit;
diff --git a/src/test/regress/sql/limit_gp.sql b/src/test/regress/sql/limit_gp.sql
index 9b84d9140be..bd69f39b7cc 100644
--- a/src/test/regress/sql/limit_gp.sql
+++ b/src/test/regress/sql/limit_gp.sql
@@ -32,3 +32,16 @@ DROP TABLE  mksort_limit_test_table;
 
 select * from generate_series(1,10) g limit g;
 select * from generate_series(1,10) g limit count(*);
+
+-- Check volatile limit should not pushdown.
+create table t_volatile_limit (i int4);
+insert into t_volatile_limit select generate_series(1, 100);
+
+-- Greenplum may generate two-stage limit plan to improve performance.
+-- But for limit clause contains volatile functions, if we push them down
+-- below the final gather motion, those volatile functions will be evaluated
+-- many times. For such cases, we should not push down the limit.
+explain select * from t_volatile_limit order by i limit (random() * 10);
+explain select * from t_volatile_limit order by i limit 2 offset (random()*5);
+
+drop table t_volatile_limit;
