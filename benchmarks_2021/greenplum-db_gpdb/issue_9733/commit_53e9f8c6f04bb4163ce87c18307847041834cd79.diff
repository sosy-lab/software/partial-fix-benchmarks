diff --git a/src/backend/optimizer/path/allpaths.c b/src/backend/optimizer/path/allpaths.c
index a82192d5269..404340f5f99 100644
--- a/src/backend/optimizer/path/allpaths.c
+++ b/src/backend/optimizer/path/allpaths.c
@@ -144,6 +144,8 @@ static void remove_unused_subquery_outputs(Query *subquery, RelOptInfo *rel);
 
 static void bring_to_outer_query(PlannerInfo *root, RelOptInfo *rel, List *outer_quals);
 static void bring_to_singleQE(PlannerInfo *root, RelOptInfo *rel);
+static bool is_path_contain_outer_params(Node *node, void *context);
+static bool path_param_walk(Node *node, void *context);
 
 
 /*
@@ -415,6 +417,18 @@ set_rel_size(PlannerInfo *root, RelOptInfo *rel,
 	Assert(rel->rows > 0 || IS_DUMMY_REL(rel));
 }
 
+static bool
+path_param_walk(Node *node, void *context)
+{
+	return contains_outer_params(node, context);
+}
+
+static bool
+is_path_contain_outer_params(Node *node, void *context)
+{
+	return path_walker(node, path_param_walk, context);
+}
+
 /*
  * Decorate the Paths of 'rel' with Motions to bring the relation's
  * result to OuterQuery locus. The final plan will look something like
@@ -454,11 +468,18 @@ bring_to_outer_query(PlannerInfo *root, RelOptInfo *rel, List *outer_quals)
 		{
 			/*
 			 * Cannot pass a param through motion, so if this is a parameterized
-			 * path, we can't use it.
+			 * path, we can't use it. Only check param_info field of a path is
+			 * not enough, we have to walk the path's parts to find if any exprs
+			 * ref outerParams. See github issue https://github.com/greenplum-db/gpdb/issues/9733
+			 * for details.
 			 */
 			if (origpath->param_info)
 				continue;
 
+			if (is_path_contain_outer_params((Node *) origpath,
+											 (void *) root))
+				continue;
+
 			CdbPathLocus_MakeOuterQuery(&outerquery_locus);
 
 			path = cdbpath_create_motion_path(root,
@@ -527,6 +548,10 @@ bring_to_singleQE(PlannerInfo *root, RelOptInfo *rel)
 			if (origpath->param_info)
 				continue;
 
+			if (is_path_contain_outer_params((Node *) origpath,
+											 (void *) root))
+				continue;
+
 			CdbPathLocus_MakeSingleQE(&target_locus,
 									  origpath->locus.numsegments);
 
diff --git a/src/backend/optimizer/util/walkers.c b/src/backend/optimizer/util/walkers.c
index 690c132d9c5..812e1440d9f 100644
--- a/src/backend/optimizer/util/walkers.c
+++ b/src/backend/optimizer/util/walkers.c
@@ -978,3 +978,324 @@ check_collation_walker(Node *node, check_collation_context *context)
 	}
 }
 
+bool path_walker(Node *node,
+				 bool (*walker) (),
+				 void *context)
+{
+	if (node == NULL)
+		return false;
+
+	/* Guard against stack overflow due to overly complex expressions */
+	check_stack_depth();
+
+	switch (nodeTag(node))
+	{
+		case T_IndexPath:
+			{
+				IndexPath *path = (IndexPath *) node;
+				if (walker((Node *) path->indexclauses, context))
+					return true;
+				if (walker((Node *) path->indexquals, context))
+					return true;
+				if (walker((Node *) path->indexorderbys, context))
+					return true;
+			}
+			break;
+		case T_CtePath:
+			{
+				CtePath *path = (CtePath *) node;
+				return path_walker((Node* ) path->subpath, walker, context);
+			}
+		case T_BitmapHeapPath:
+			{
+				BitmapHeapPath *path = (BitmapHeapPath *) node;
+				return path_walker((Node *) path->bitmapqual, walker, context);
+			}
+		case T_BitmapAndPath:
+			{
+				BitmapAndPath *path = (BitmapAndPath *) node;
+				if (walker((Node *) path->bitmapquals, context))
+					return true;
+			}
+			break;
+		case T_BitmapOrPath:
+			{
+				BitmapOrPath *path = (BitmapOrPath *) node;
+				if (walker((Node *) path->bitmapquals, context))
+					return true;
+			}
+			break;
+		case T_TidPath:
+			{
+				TidPath *path = (TidPath *) node;
+				if (walker((Node *) path->tidquals, context))
+					return true;
+			}
+			break;
+		case T_SubqueryScanPath:
+			{
+				SubqueryScanPath *path = (SubqueryScanPath *) node;
+				return path_walker((Node *) path->subpath, walker, context);
+			}
+		case T_TableFunctionScanPath:
+			{
+				TableFunctionScanPath *path = (TableFunctionScanPath *) node;
+				return path_walker((Node *) path->subpath, walker, context);
+			}
+		case T_ForeignPath:
+			{
+				ForeignPath *path = (ForeignPath *) node;
+				if (walker((Node *) path->fdw_private, context))
+					return true;
+				return path_walker((Node *) path->fdw_outerpath, walker, context);
+			}
+		case T_CustomPath:
+			{
+				CustomPath *path = (CustomPath *) node;
+				if (walker((Node *) path->custom_paths, context))
+					return true;
+				if (walker((Node *) path->custom_private, context))
+					return true;
+			}
+			break;
+		case T_NestPath:
+			{
+				NestPath *path = (NestPath *) node;
+				if (path_walker((Node *) path->outerjoinpath, walker, context))
+					return true;
+				if (path_walker((Node *) path->innerjoinpath, walker, context))
+					return true;
+				if (walker((Node *) path->joinrestrictinfo, context))
+					return true;
+			}
+			break;
+		case T_MergePath:
+			{
+				MergePath *path = (MergePath *) node;
+				if (path_walker((Node *) &(path->jpath), walker, context))
+					return true;
+				if (walker((Node *) path->path_mergeclauses, context))
+					return true;
+				if (walker((Node *) path->outersortkeys, context))
+					return true;
+				if (walker((Node *) path->innersortkeys, context))
+					return true;
+			}
+			break;
+		case T_HashPath:
+			{
+				HashPath *path = (HashPath *) node;
+				if (path_walker((Node *) &(path->jpath), walker, context))
+					return true;
+				if (walker((Node *) path->path_hashclauses, context))
+					return true;
+			}
+			break;
+		case T_AppendPath:
+			{
+				AppendPath *path = (AppendPath *) node;
+				ListCell   *lc;
+				foreach(lc, path->subpaths)
+				{
+					if (path_walker((Node *) lfirst(lc), walker, context))
+						return true;
+				}
+			}
+			break;
+		case T_MergeAppendPath:
+			{
+				MergeAppendPath *path = (MergeAppendPath *) node;
+				ListCell   *lc;
+				foreach(lc, path->subpaths)
+				{
+					if (path_walker((Node *) lfirst(lc), walker, context))
+						return true;
+				}
+			}
+			break;
+		case T_ResultPath:
+			{
+				ResultPath *path = (ResultPath *) node;
+				if (walker((Node *) path->quals, context))
+					return true;
+			}
+			break;
+		case T_MaterialPath:
+			{
+				MaterialPath *path = (MaterialPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+			}
+			break;
+		case T_UniquePath:
+			{
+				UniquePath *path = (UniquePath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->in_operators, context))
+					return true;
+				if (walker((Node *) path->uniq_exprs, context))
+					return true;
+			}
+			break;
+		case T_GatherPath:
+			{
+				GatherPath *path = (GatherPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+			}
+			break;
+		case T_ProjectionPath:
+			{
+				ProjectionPath *path = (ProjectionPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->cdb_restrict_clauses, context))
+					return true;
+			}
+			break;
+		case T_SortPath:
+			{
+				SortPath *path = (SortPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+			}
+			break;
+		case T_GroupPath:
+			{
+				GroupPath *path = (GroupPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->groupClause, context))
+					return true;
+				if (walker((Node *) path->qual, context))
+					return true;
+			}
+			break;
+		case T_UpperUniquePath:
+			{
+				UpperUniquePath *path = (UpperUniquePath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+			}
+			break;
+		case T_AggPath:
+			{
+				AggPath *path = (AggPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->groupClause, context))
+					return true;
+				if (walker((Node *) path->qual, context))
+					return true;
+			}
+			break;
+		case T_GroupingSetsPath:
+			{
+				GroupingSetsPath *path = (GroupingSetsPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->rollup_groupclauses, context))
+					return true;
+				if (walker((Node *) path->rollup_lists, context))
+					return true;
+				if (walker((Node *) path->qual, context))
+					return true;
+			}
+			break;
+		case T_WindowAggPath:
+			{
+				WindowAggPath *path = (WindowAggPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->winclause, context))
+					return true;
+				if (walker((Node *) path->winpathkeys, context))
+					return true;
+			}
+			break;
+		case T_MinMaxAggPath:
+			{
+				MinMaxAggPath *path = (MinMaxAggPath *) node;
+				if (walker((Node *) path->mmaggregates, context))
+					return true;
+				if (walker((Node *) path->quals, context))
+					return true;
+			}
+			break;
+		case T_TupleSplitPath:
+			{
+				TupleSplitPath *path = (TupleSplitPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->groupClause, context))
+					return true;
+			}
+			break;
+		case T_SetOpPath:
+			{
+				SetOpPath *path = (SetOpPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->distinctList, context))
+					return true;
+			}
+			break;
+		case T_RecursiveUnionPath:
+			{
+				RecursiveUnionPath *path = (RecursiveUnionPath *) node;
+				if (path_walker((Node *) path->leftpath, walker, context))
+					return true;
+				if (path_walker((Node *) path->rightpath, walker, context))
+					return true;
+				if (walker((Node *) path->distinctList, context))
+					return true;
+			}
+			break;
+		case T_LockRowsPath:
+			{
+				LockRowsPath *path = (LockRowsPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->rowMarks, context))
+					return true;
+			}
+			break;
+		case T_LimitPath:
+			{
+				LimitPath *path = (LimitPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->limitOffset, context))
+					return true;
+				if (walker((Node *) path->limitCount, context))
+					return true;
+			}
+			break;
+		case T_PartitionSelectorPath:
+			{
+				PartitionSelectorPath *path = (PartitionSelectorPath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+				if (walker((Node *) path->dsinfo, context))
+					return true;
+				if (walker((Node *) path->partKeyExprs, context))
+					return true;
+				if (walker((Node *) path->partKeyAttnos, context))
+					return true;
+			}
+			break;
+		case T_SplitUpdatePath:
+			{
+				SplitUpdatePath *path = (SplitUpdatePath *) node;
+				if (path_walker((Node *) path->subpath, walker, context))
+					return true;
+			}
+			break;
+		case T_ModifyTablePath:
+		default:
+			break;
+	}
+
+	return false;
+}
diff --git a/src/include/optimizer/walkers.h b/src/include/optimizer/walkers.h
index 77873b9c7ac..9120dbc4549 100644
--- a/src/include/optimizer/walkers.h
+++ b/src/include/optimizer/walkers.h
@@ -35,6 +35,8 @@ extern bool walk_plan_node_fields(Plan *plan, bool (*walker) (), void *context);
 
 extern bool plan_tree_walker(Node *node, bool (*walker) (), void *context, bool recurse_into_subplans);
 
+extern bool path_walker(Node *node, bool (*walker) (), void *context);
+
 #ifdef __cplusplus
 extern "C" {
 #endif
diff --git a/src/test/regress/expected/join_gp.out b/src/test/regress/expected/join_gp.out
index b8f720b792a..92089edf380 100644
--- a/src/test/regress/expected/join_gp.out
+++ b/src/test/regress/expected/join_gp.out
@@ -1321,3 +1321,71 @@ NOTICE:  prefetch join qual in slice 0 of plannode 1
 
 reset Test_print_prefetch_joinqual;
 reset optimizer;
+-- Github Issue: https://github.com/greenplum-db/gpdb/issues/9733
+-- Previously in the function bring_to_outer_query and
+-- bring_to_singleQE it depends on the path->param_info field
+-- to determine if the path contains outerParams. This is not
+-- enought. The following case would SegFault before because
+-- the indexpath's orderby clause contains outerParams.
+create table gist_tbl_github9733 (b box, p point, c circle);
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause, and no column type is suitable for a distribution key. Creating a NULL policy entry.
+insert into gist_tbl_github9733
+select box(point(0.05*i, 0.05*i), point(0.05*i, 0.05*i)),
+       point(0.05*i, 0.05*i),
+       circle(point(0.05*i, 0.05*i), 1.0)
+from generate_series(0,10000) as i;
+vacuum analyze gist_tbl_github9733;
+create index gist_tbl_point_index_github9733 on gist_tbl_github9733 using gist (p);
+set enable_seqscan=off;
+set enable_bitmapscan=off;
+explain (costs off)
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+ERROR:  could not devise a query plan for the given query (pathnode.c:469)
+  
+reset enable_seqscan;
+explain (costs off)
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+                                        QUERY PLAN                                         
+-------------------------------------------------------------------------------------------
+ Nested Loop
+   ->  Values Scan on "*VALUES*"
+   ->  Materialize
+         ->  Subquery Scan on ss
+               ->  Limit
+                     ->  Sort
+                           Sort Key: ((gist_tbl_github9733.p <-> ("*VALUES*".column1)[0]))
+                           ->  Result
+                                 Filter: (gist_tbl_github9733.p <@ "*VALUES*".column1)
+                                 ->  Materialize
+                                       ->  Gather Motion 3:1  (slice1; segments: 3)
+                                             ->  Seq Scan on gist_tbl_github9733
+ Optimizer: Postgres query optimizer
+(13 rows)
+
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+      p      
+-------------
+ (0.5,0.5)
+ (0.45,0.45)
+ (0.75,0.75)
+ (0.7,0.7)
+ (1,1)
+ (0.95,0.95)
+(6 rows)
+
+reset enable_bitmapscan;
diff --git a/src/test/regress/expected/join_gp_optimizer.out b/src/test/regress/expected/join_gp_optimizer.out
index ac91f4bd708..e86d6f4ed86 100644
--- a/src/test/regress/expected/join_gp_optimizer.out
+++ b/src/test/regress/expected/join_gp_optimizer.out
@@ -1319,3 +1319,71 @@ NOTICE:  prefetch join qual in slice 0 of plannode 1
 
 reset Test_print_prefetch_joinqual;
 reset optimizer;
+-- Github Issue: https://github.com/greenplum-db/gpdb/issues/9733
+-- Previously in the function bring_to_outer_query and
+-- bring_to_singleQE it depends on the path->param_info field
+-- to determine if the path contains outerParams. This is not
+-- enought. The following case would SegFault before because
+-- the indexpath's orderby clause contains outerParams.
+create table gist_tbl_github9733 (b box, p point, c circle);
+NOTICE:  Table doesn't have 'DISTRIBUTED BY' clause, and no column type is suitable for a distribution key. Creating a NULL policy entry.
+insert into gist_tbl_github9733
+select box(point(0.05*i, 0.05*i), point(0.05*i, 0.05*i)),
+       point(0.05*i, 0.05*i),
+       circle(point(0.05*i, 0.05*i), 1.0)
+from generate_series(0,10000) as i;
+vacuum analyze gist_tbl_github9733;
+create index gist_tbl_point_index_github9733 on gist_tbl_github9733 using gist (p);
+set enable_seqscan=off;
+set enable_bitmapscan=off;
+explain (costs off)
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+ERROR:  could not devise a query plan for the given query (pathnode.c:469)
+  
+reset enable_seqscan;
+explain (costs off)
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+                                        QUERY PLAN                                         
+-------------------------------------------------------------------------------------------
+ Nested Loop
+   ->  Values Scan on "*VALUES*"
+   ->  Materialize
+         ->  Subquery Scan on ss
+               ->  Limit
+                     ->  Sort
+                           Sort Key: ((gist_tbl_github9733.p <-> ("*VALUES*".column1)[0]))
+                           ->  Result
+                                 Filter: (gist_tbl_github9733.p <@ "*VALUES*".column1)
+                                 ->  Materialize
+                                       ->  Gather Motion 3:1  (slice1; segments: 3)
+                                             ->  Seq Scan on gist_tbl_github9733
+ Optimizer: Postgres query optimizer
+(13 rows)
+
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+      p      
+-------------
+ (0.5,0.5)
+ (0.45,0.45)
+ (0.75,0.75)
+ (0.7,0.7)
+ (1,1)
+ (0.95,0.95)
+(6 rows)
+
+reset enable_bitmapscan;
diff --git a/src/test/regress/sql/join_gp.sql b/src/test/regress/sql/join_gp.sql
index 8580bf5f7dd..6a38050c8e8 100644
--- a/src/test/regress/sql/join_gp.sql
+++ b/src/test/regress/sql/join_gp.sql
@@ -627,3 +627,45 @@ on t1.b = t2.b and t1.a > any (select sum(b) from t3_test_pretch_join_qual t3 wh
 
 reset Test_print_prefetch_joinqual;
 reset optimizer;
+
+-- Github Issue: https://github.com/greenplum-db/gpdb/issues/9733
+-- Previously in the function bring_to_outer_query and
+-- bring_to_singleQE it depends on the path->param_info field
+-- to determine if the path contains outerParams. This is not
+-- enought. The following case would SegFault before because
+-- the indexpath's orderby clause contains outerParams.
+create table gist_tbl_github9733 (b box, p point, c circle);
+insert into gist_tbl_github9733
+select box(point(0.05*i, 0.05*i), point(0.05*i, 0.05*i)),
+       point(0.05*i, 0.05*i),
+       circle(point(0.05*i, 0.05*i), 1.0)
+from generate_series(0,10000) as i;
+vacuum analyze gist_tbl_github9733;
+create index gist_tbl_point_index_github9733 on gist_tbl_github9733 using gist (p);
+set enable_seqscan=off;
+set enable_bitmapscan=off;
+explain (costs off)
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+  
+reset enable_seqscan;
+explain (costs off)
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+
+select p from
+  (values (box(point(0,0), point(0.5,0.5))),
+          (box(point(0.5,0.5), point(0.75,0.75))),
+          (box(point(0.8,0.8), point(1.0,1.0)))) as v(bb)
+cross join lateral
+  (select p from gist_tbl_github9733 where p <@ bb order by p <-> bb[0] limit 2) ss;
+
+reset enable_bitmapscan;
