Http Code 100 problem with htx
I confirm the bug. It was introduced by the commit 4710d20743. I'm working on a fix. It will be pretty simple to do so. 
Thanks.
Hmmm that's me again, sorry guys!


In fact the bug did not affect only 100 responses. The header "transfer-encoding" header was also added in responses to HEAD/CONNECT requests and all 1xx, 204 and 304 responses. I pushed a fix in 2.0-dev. Thanks !
Reopen it. Must be backported in 1.9
now merged into 1.9.
Thanks guys!
Hi guys. I see this Issue again on 1.9.8 with htx mode and HTTP/1.1.
HTTP Request is hanging after server response 'HTTP/1.1 100 Continue'.

Try this.

```
dd if=/dev/zero bs=1 count=1025 | curl -vvv --data-binary "@-" http://any.your.domain.com
```

Then I see

```
> POST / HTTP/1.1
> User-Agent: curl/7.29.0
> Host: ---
> Accept: */*
> Content-Length: 1025
> Content-Type: application/x-www-form-urlencoded
> Expect: 100-continue
>
< HTTP/1.1 100 Continue
...
```
When I try this without option htx, It works fine.
@steveum0105, I pushed 2 fixes into the haproxy-1.9 repository :

  * http://git.haproxy.org/?p=haproxy-1.9.git;a=commit;h=6d30abbb
  * http://git.haproxy.org/?p=haproxy-1.9.git;a=commit;h=674e755d

It should be ok now. Thanks !