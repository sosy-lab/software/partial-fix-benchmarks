BUG/MEDIUM: lua: Fix dumping of stick table entries for STD_T_DICT

The issue can easily be reproduced with "stick on" statement

backend BE_NAME
    stick-table type ip size 1k
    stick on src

and calling dump() method on BE_NAME stick table from Lua

Before the fix, HAProxy would return 500 and log something like
the following:
  runtime error: attempt to index a string value from [C] method 'dump'

Where one would expect a Lua table like this:

{
    ["IP_ADDR"] = {
        ["server_id"] = 1,
        ["server_name"] = "srv1"
    }
}

This patch needs to backported to 1.9 and later releases.
BUG/MINOR: config: Make use_backend and use-server post-parsing less obscur

During use_backend and use-server post-parsing, if the log-format string used to
specify the backend or the server is just a single string, the log-format string
(a list, internally) is replaced by the string itself. Because the field is an
union, the list is not emptied according to the rules of art. The element, when
released, is not removed from the list. There is no bug, but it is clearly not
obvious and error prone.

This patch should fix #544. The fix for the use_backend post-parsing may be
backported to all stable releases. use-server is static in 2.1 and prior.
BUG/MINOR: config: Make use_backend and use-server post-parsing less obscur

During use_backend and use-server post-parsing, if the log-format string used to
specify the backend or the server is just a single string, the log-format string
(a list, internally) is replaced by the string itself. Because the field is an
union, the list is not emptied according to the rules of art. The element, when
released, is not removed from the list. There is no bug, but it is clearly not
obvious and error prone.

This patch should fix #544. The fix for the use_backend post-parsing may be
backported to all stable releases. use-server is static in 2.1 and prior.

(cherry picked from commit f82ea4ae4ca4a6fca70a6e874643db887a39f037)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUG/MINOR: config: Make use_backend and use-server post-parsing less obscur

During use_backend and use-server post-parsing, if the log-format string used to
specify the backend or the server is just a single string, the log-format string
(a list, internally) is replaced by the string itself. Because the field is an
union, the list is not emptied according to the rules of art. The element, when
released, is not removed from the list. There is no bug, but it is clearly not
obvious and error prone.

This patch should fix #544. The fix for the use_backend post-parsing may be
backported to all stable releases. use-server is static in 2.1 and prior.

(cherry picked from commit f82ea4ae4ca4a6fca70a6e874643db887a39f037)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 83c847ee88d509d41e8cbe099af93dbf0ebbf75b)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUG/MINOR: config: Make use_backend and use-server post-parsing less obscur

During use_backend and use-server post-parsing, if the log-format string used to
specify the backend or the server is just a single string, the log-format string
(a list, internally) is replaced by the string itself. Because the field is an
union, the list is not emptied according to the rules of art. The element, when
released, is not removed from the list. There is no bug, but it is clearly not
obvious and error prone.

This patch should fix #544. The fix for the use_backend post-parsing may be
backported to all stable releases. use-server is static in 2.1 and prior.

(cherry picked from commit f82ea4ae4ca4a6fca70a6e874643db887a39f037)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 83c847ee88d509d41e8cbe099af93dbf0ebbf75b)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 8762feaabc0a14e474f60609231f63ac33639fe1)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUG/MINOR: config: Make use_backend and use-server post-parsing less obscur

During use_backend and use-server post-parsing, if the log-format string used to
specify the backend or the server is just a single string, the log-format string
(a list, internally) is replaced by the string itself. Because the field is an
union, the list is not emptied according to the rules of art. The element, when
released, is not removed from the list. There is no bug, but it is clearly not
obvious and error prone.

This patch should fix #544. The fix for the use_backend post-parsing may be
backported to all stable releases. use-server is static in 2.1 and prior.

(cherry picked from commit f82ea4ae4ca4a6fca70a6e874643db887a39f037)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 83c847ee88d509d41e8cbe099af93dbf0ebbf75b)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 8762feaabc0a14e474f60609231f63ac33639fe1)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 74e1fc7690e915e46e48e0a2507742147185a824)
Signed-off-by: Willy Tarreau <w@1wt.eu>
