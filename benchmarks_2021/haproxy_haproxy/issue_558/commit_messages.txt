MINOR: ssl: rework add cert chain to CTX to be libssl independent

SSL_CTX_set1_chain is used for openssl >= 1.0.2 and a loop with
SSL_CTX_add_extra_chain_cert for openssl < 1.0.2.
SSL_CTX_add_extra_chain_cert exist with openssl >= 1.0.2 and can be
used for all openssl version (is new name is SSL_CTX_add0_chain_cert).
This patch use SSL_CTX_add_extra_chain_cert to remove any #ifdef for
compatibilty. In addition sk_X509_num()/sk_X509_value() replace
sk_X509_shift() to extract CA from chain, as it is used in others part
of the code.
BUG/MINOR: peers: init bind_proc to 1 if it wasn't initialized

Tim reported that in master-worker mode, if a stick-table is declared
but not used in the configuration, its associated peers listener won't
bind.

This problem is due the fact that the master-worker and the daemon mode,
depend on the bind_proc field of the peers proxy to disable the listener.
Unfortunately the bind_proc is left to 0 if no stick-table were used in
the configuration, stopping the listener on all processes.

This fixes sets the bind_proc to the first process if it wasn't
initialized.

Should fix bug #558. Should be backported as far as 1.8.
BUG/MINOR: peers: avoid an infinite loop with peers_fe is NULL

Fix an infinite loop which was added in an attempt to fix #558.
If the peers_fe is NULL, it will loop forever.

Must be backported with a2cfd7e as far as 1.8.
BUG/MINOR: peers: init bind_proc to 1 if it wasn't initialized

Tim reported that in master-worker mode, if a stick-table is declared
but not used in the configuration, its associated peers listener won't
bind.

This problem is due the fact that the master-worker and the daemon mode,
depend on the bind_proc field of the peers proxy to disable the listener.
Unfortunately the bind_proc is left to 0 if no stick-table were used in
the configuration, stopping the listener on all processes.

This fixes sets the bind_proc to the first process if it wasn't
initialized.

Should fix bug #558. Should be backported as far as 1.8.

(cherry picked from commit a2cfd7e356f4d744294b510b05d88bf58304db25)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: avoid an infinite loop with peers_fe is NULL

Fix an infinite loop which was added in an attempt to fix #558.
If the peers_fe is NULL, it will loop forever.

Must be backported with a2cfd7e as far as 1.8.

(cherry picked from commit 3ef2d565303af6dd7c24b62fdb22e43f1d1638bf)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: init bind_proc to 1 if it wasn't initialized

Tim reported that in master-worker mode, if a stick-table is declared
but not used in the configuration, its associated peers listener won't
bind.

This problem is due the fact that the master-worker and the daemon mode,
depend on the bind_proc field of the peers proxy to disable the listener.
Unfortunately the bind_proc is left to 0 if no stick-table were used in
the configuration, stopping the listener on all processes.

This fixes sets the bind_proc to the first process if it wasn't
initialized.

Should fix bug #558. Should be backported as far as 1.8.

(cherry picked from commit a2cfd7e356f4d744294b510b05d88bf58304db25)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit f9009aea0de127ede6f73c0c84153c1c57303229)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: avoid an infinite loop with peers_fe is NULL

Fix an infinite loop which was added in an attempt to fix #558.
If the peers_fe is NULL, it will loop forever.

Must be backported with a2cfd7e as far as 1.8.

(cherry picked from commit 3ef2d565303af6dd7c24b62fdb22e43f1d1638bf)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 4907d7f6a13e403dea5a7bbb2e3a9923fa587807)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: init bind_proc to 1 if it wasn't initialized

Tim reported that in master-worker mode, if a stick-table is declared
but not used in the configuration, its associated peers listener won't
bind.

This problem is due the fact that the master-worker and the daemon mode,
depend on the bind_proc field of the peers proxy to disable the listener.
Unfortunately the bind_proc is left to 0 if no stick-table were used in
the configuration, stopping the listener on all processes.

This fixes sets the bind_proc to the first process if it wasn't
initialized.

Should fix bug #558. Should be backported as far as 1.8.

(cherry picked from commit a2cfd7e356f4d744294b510b05d88bf58304db25)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit f9009aea0de127ede6f73c0c84153c1c57303229)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 98235321cede42de20a04a983f0bb1b80626068f)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 6f96168ecec865ba640ab732a1198eb8270f3f86)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: avoid an infinite loop with peers_fe is NULL

Fix an infinite loop which was added in an attempt to fix #558.
If the peers_fe is NULL, it will loop forever.

Must be backported with a2cfd7e as far as 1.8.

(cherry picked from commit 3ef2d565303af6dd7c24b62fdb22e43f1d1638bf)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 4907d7f6a13e403dea5a7bbb2e3a9923fa587807)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 0412f697dc4d90b261e63c7da527b52678a7264c)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 275c60aee9562e0e76ecf331daa5e5a6cc10bba4)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: init bind_proc to 1 if it wasn't initialized

Tim reported that in master-worker mode, if a stick-table is declared
but not used in the configuration, its associated peers listener won't
bind.

This problem is due the fact that the master-worker and the daemon mode,
depend on the bind_proc field of the peers proxy to disable the listener.
Unfortunately the bind_proc is left to 0 if no stick-table were used in
the configuration, stopping the listener on all processes.

This fixes sets the bind_proc to the first process if it wasn't
initialized.

Should fix bug #558. Should be backported as far as 1.8.

(cherry picked from commit a2cfd7e356f4d744294b510b05d88bf58304db25)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit f9009aea0de127ede6f73c0c84153c1c57303229)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 98235321cede42de20a04a983f0bb1b80626068f)
Signed-off-by: Willy Tarreau <w@1wt.eu>
BUG/MINOR: peers: avoid an infinite loop with peers_fe is NULL

Fix an infinite loop which was added in an attempt to fix #558.
If the peers_fe is NULL, it will loop forever.

Must be backported with a2cfd7e as far as 1.8.

(cherry picked from commit 3ef2d565303af6dd7c24b62fdb22e43f1d1638bf)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 4907d7f6a13e403dea5a7bbb2e3a9923fa587807)
Signed-off-by: Willy Tarreau <w@1wt.eu>
(cherry picked from commit 0412f697dc4d90b261e63c7da527b52678a7264c)
Signed-off-by: Willy Tarreau <w@1wt.eu>
