Panel_new: reorder arguments

Reorder owner and type so they match the order of Panel_init
Linux: handle hugepages

Subtract hugepages from normal memory.
Add a HugePageMeter.

Closes: #447
Linux: handle hugepages

Subtract hugepages from normal memory.
Add a HugePageMeter.

Closes: #447
Linux: handle hugepages

Subtract hugepages from normal memory.
Add a HugePageMeter.

Closes: #447
Linux: handle hugepages

Subtract hugepages from normal memory.
Add a HugePageMeter.

Closes: #447
Linux: handle hugepages

Subtract hugepages from normal memory.
Add a HugePageMeter.

Closes: #447
Linux: restore memory calculation regarding HugePages

Subtract the total amount of huge page memory from total and used memory.

Restores behavior from #450 (see also #447)

Follow-up of 3d497a37
