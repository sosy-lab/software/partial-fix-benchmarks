Enhancement request: huge page support
Does #385 report correct memory usage in your system?

If I understand the source changes in #385 correctly, then no. That looks like a different problem.