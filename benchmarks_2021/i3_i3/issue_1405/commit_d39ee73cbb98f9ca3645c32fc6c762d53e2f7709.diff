diff --git a/i3-nagbar/main.c b/i3-nagbar/main.c
index 805066f82..52dcb2445 100644
--- a/i3-nagbar/main.c
+++ b/i3-nagbar/main.c
@@ -27,6 +27,7 @@
 #include <xcb/xcb.h>
 #include <xcb/xcb_aux.h>
 #include <xcb/xcb_event.h>
+#include <xcb/randr.h>
 
 #include "libi3.h"
 #include "i3-nagbar.h"
@@ -286,6 +287,77 @@ static int handle_expose(xcb_connection_t *conn, xcb_expose_event_t *event) {
     return 1;
 }
 
+/**
+ * Return the position and size the i3-nagbar window should use.
+ * This will be the primary output or a fallback if it cannot be determined.
+ */
+static xcb_rectangle_t get_window_position() {
+    /* Default values if we cannot determine the primary output or its CRTC info. */
+    xcb_rectangle_t result = (xcb_rectangle_t){50, 50, 500, font.height + logical_px(8) + logical_px(8)};
+
+    xcb_randr_get_screen_resources_current_cookie_t rcookie;
+    xcb_randr_get_output_primary_cookie_t pcookie;
+
+    rcookie = xcb_randr_get_screen_resources_current(conn, root);
+    pcookie = xcb_randr_get_output_primary(conn, root);
+
+    xcb_randr_get_output_primary_reply_t *primary;
+    xcb_randr_get_screen_resources_current_reply_t *res;
+    xcb_randr_output_t *randr_outputs;
+
+    if ((primary = xcb_randr_get_output_primary_reply(conn, pcookie, NULL)) == NULL) {
+        DLOG("Could not determine the primary output.\n");
+        goto free_resources;
+    }
+
+    if ((res = xcb_randr_get_screen_resources_current_reply(conn, rcookie, NULL)) == NULL) {
+        goto free_resources;
+    }
+
+    int len = xcb_randr_get_screen_resources_current_outputs_length(res);
+    randr_outputs = xcb_randr_get_screen_resources_current_outputs(res);
+    for (int i = 0; i < len; i++) {
+        if (randr_outputs[i] != primary->output)
+            continue;
+
+        xcb_randr_get_output_info_cookie_t ocookie;
+        ocookie = xcb_randr_get_output_info(conn, randr_outputs[i], res->config_timestamp);
+
+        xcb_randr_get_output_info_reply_t *output;
+        if ((output = xcb_randr_get_output_info_reply(conn, ocookie, NULL)) == NULL) {
+            goto free_resources;
+        }
+
+        if (output->crtc == XCB_NONE) {
+            goto free_resources;
+        }
+
+        xcb_randr_get_crtc_info_cookie_t icookie;
+        icookie = xcb_randr_get_crtc_info(conn, output->crtc, res->config_timestamp);
+
+        xcb_randr_get_crtc_info_reply_t *crtc;
+        if ((crtc = xcb_randr_get_crtc_info_reply(conn, icookie, NULL)) == NULL) {
+            goto free_resources;
+        }
+
+        DLOG("Found primary output on position x = %i / y = %i / w = %i / h = %i",
+             crtc->x, crtc->y, crtc->width, crtc->height);
+        if (crtc->width == 0 || crtc->height == 0) {
+            DLOG("Primary output is not active, ignoring it.\n");
+            goto free_resources;
+        }
+
+        result.x = crtc->x;
+        result.y = crtc->y;
+        goto free_resources;
+    }
+
+free_resources:
+    FREE(res);
+    FREE(primary);
+    return result;
+}
+
 int main(int argc, char *argv[]) {
     /* The following lines are a terribly horrible kludge. Because terminal
      * emulators have different ways of interpreting the -e command line
@@ -408,16 +480,18 @@ int main(int argc, char *argv[]) {
     font = load_font(pattern, true);
     set_font(&font);
 
+    xcb_rectangle_t win_pos = get_window_position();
+
     /* Open an input window */
     win = xcb_generate_id(conn);
 
     xcb_create_window(
         conn,
         XCB_COPY_FROM_PARENT,
-        win,                                                                         /* the window id */
-        root,                                                                        /* parent == root */
-        50, 50, 500, font.height + logical_px(8) + logical_px(8) /* 8 px padding */, /* dimensions */
-        0,                                                                           /* x11 border = 0, we draw our own */
+        win,                                                 /* the window id */
+        root,                                                /* parent == root */
+        win_pos.x, win_pos.y, win_pos.width, win_pos.height, /* dimensions */
+        0,                                                   /* x11 border = 0, we draw our own */
         XCB_WINDOW_CLASS_INPUT_OUTPUT,
         XCB_WINDOW_CLASS_COPY_FROM_PARENT, /* copy visual from parent */
         XCB_CW_BACK_PIXEL | XCB_CW_EVENT_MASK,
