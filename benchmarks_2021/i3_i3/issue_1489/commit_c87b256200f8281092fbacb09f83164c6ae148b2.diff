diff --git a/docs/userguide b/docs/userguide
index d1660c49e..13dae4fe0 100644
--- a/docs/userguide
+++ b/docs/userguide
@@ -1011,31 +1011,6 @@ force_display_urgency_hint <timeout> ms
 force_display_urgency_hint 500 ms
 ---------------------------------
 
-=== Delaying exiting on zero displays
-
-Outputs may disappear momentarily and come back later. For example,
-using a docking station that does not announce the undock (e.g. ACPI Undock 
-event triggered through manually pushing a button before actually ejecting 
-the notebook). During the removal of the notebook from the docking station,
-all outputs disappear momentarily.
-
-To prevent i3 from exiting when no output is available momentarily, you can 
-tell i3 to delay a certain time first and check available outputs again using 
-the +delay_exit_on_zero_displays+ directive. Setting the value to 0 disables 
-this feature.
-
-The default is 500ms.
-
-*Syntax*:
-----------------------------------------
-delay_exit_on_zero_displays <timeout> ms
-----------------------------------------
-
-*Example*:
-----------------------------------
-delay_exit_on_zero_displays 500 ms
-----------------------------------
-
 === Focus on window activation
 
 [[focus_on_window_activation]]
diff --git a/include/config.h b/include/config.h
index ff360bb5d..fd6fe6c09 100644
--- a/include/config.h
+++ b/include/config.h
@@ -167,10 +167,6 @@ struct Config {
      * flag can be delayed using an urgency timer. */
     float workspace_urgency_timer;
 
-    /** Use a timer to delay exiting when no output is available.
-      * This can prevent i3 from exiting when all outputs disappear momentarily. */
-    float zero_disp_exit_timer_ms;
-
     /** Behavior when a window sends a NET_ACTIVE_WINDOW message. */
     enum {
         /* Focus if the target workspace is visible, set urgency hint otherwise. */
diff --git a/include/config_directives.h b/include/config_directives.h
index 72646651b..c0c70bb48 100644
--- a/include/config_directives.h
+++ b/include/config_directives.h
@@ -51,7 +51,6 @@ CFGFUN(force_focus_wrapping, const char *value);
 CFGFUN(force_xinerama, const char *value);
 CFGFUN(fake_outputs, const char *outputs);
 CFGFUN(force_display_urgency_hint, const long duration_ms);
-CFGFUN(delay_exit_on_zero_displays, const long duration_ms);
 CFGFUN(focus_on_window_activation, const char *mode);
 CFGFUN(show_marks, const char *value);
 CFGFUN(hide_edge_borders, const char *borders);
diff --git a/parser-specs/config.spec b/parser-specs/config.spec
index 1191157c6..b9542c8c3 100644
--- a/parser-specs/config.spec
+++ b/parser-specs/config.spec
@@ -39,7 +39,6 @@ state INITIAL:
   'workspace_auto_back_and_forth'          -> WORKSPACE_BACK_AND_FORTH
   'fake_outputs', 'fake-outputs'           -> FAKE_OUTPUTS
   'force_display_urgency_hint'             -> FORCE_DISPLAY_URGENCY_HINT
-  'delay_exit_on_zero_displays'            -> DELAY_EXIT_ON_ZERO_DISPLAYS
   'focus_on_window_activation'             -> FOCUS_ON_WINDOW_ACTIVATION
   'show_marks'                             -> SHOW_MARKS
   'workspace'                              -> WORKSPACE
@@ -228,17 +227,6 @@ state FORCE_DISPLAY_URGENCY_HINT_MS:
   end
       -> call cfg_force_display_urgency_hint(&duration_ms)
 
-# delay_exit_on_zero_displays <delay> ms
-state DELAY_EXIT_ON_ZERO_DISPLAYS:
-  duration_ms = number
-      -> DELAY_EXIT_ON_ZERO_DISPLAYS_MS
-
-state DELAY_EXIT_ON_ZERO_DISPLAYS_MS:
-  'ms'
-      ->
-  end
-      -> call cfg_delay_exit_on_zero_displays(&duration_ms)
-
 # focus_on_window_activation <smart|urgent|focus|none>
 state FOCUS_ON_WINDOW_ACTIVATION:
   mode = word
diff --git a/src/config.c b/src/config.c
index 0dc593652..d8db85e63 100644
--- a/src/config.c
+++ b/src/config.c
@@ -206,10 +206,6 @@ void load_configuration(xcb_connection_t *conn, const char *override_configpath,
     if (config.workspace_urgency_timer == 0)
         config.workspace_urgency_timer = 0.5;
 
-    /* Set default zero displays exit delay to 500ms */
-    if (config.zero_disp_exit_timer_ms == 0)
-        config.zero_disp_exit_timer_ms = 500;
-
     parse_configuration(override_configpath, true);
 
     if (reload) {
diff --git a/src/config_directives.c b/src/config_directives.c
index 463b38e77..d772387dc 100644
--- a/src/config_directives.c
+++ b/src/config_directives.c
@@ -367,10 +367,6 @@ CFGFUN(force_display_urgency_hint, const long duration_ms) {
     config.workspace_urgency_timer = duration_ms / 1000.0;
 }
 
-CFGFUN(delay_exit_on_zero_displays, const long duration_ms) {
-    config.zero_disp_exit_timer_ms = duration_ms;
-}
-
 CFGFUN(focus_on_window_activation, const char *mode) {
     if (strcmp(mode, "smart") == 0)
         config.focus_on_window_activation = FOWA_SMART;
diff --git a/src/main.c b/src/main.c
index b4ec3720e..0dc259363 100644
--- a/src/main.c
+++ b/src/main.c
@@ -655,8 +655,6 @@ int main(int argc, char *argv[]) {
             ELOG("ERROR: No screen at (%d, %d), starting on the first screen\n",
                  pointerreply->root_x, pointerreply->root_y);
             output = get_first_output();
-            if (!output)
-                die("No usable outputs available.\n");
         }
 
         con_focus(con_descend_focused(output_get_content(output->con)));
diff --git a/src/randr.c b/src/randr.c
index 7cf9d99e8..69a866ab6 100644
--- a/src/randr.c
+++ b/src/randr.c
@@ -69,7 +69,7 @@ Output *get_first_output(void) {
     if (output->active)
         return output;
 
-    return NULL;
+    die("No usable outputs available.\n");
 }
 
 /*
@@ -564,8 +564,6 @@ static void handle_output(xcb_connection_t *conn, xcb_randr_output_t id,
     if (!new->active) {
         DLOG("width/height 0/0, disabling output\n");
         return;
-    } else {
-        new->to_be_disabled = false;
     }
 
     DLOG("mode: %dx%d+%d+%d\n", new->rect.width, new->rect.height,
@@ -587,7 +585,11 @@ static void handle_output(xcb_connection_t *conn, xcb_randr_output_t id,
     new->changed = true;
 }
 
-static bool __randr_query_outputs(void) {
+/*
+ * (Re-)queries the outputs via RandR and stores them in the list of outputs.
+ *
+ */
+void randr_query_outputs(void) {
     Output *output, *other, *first;
     xcb_randr_get_output_primary_cookie_t pcookie;
     xcb_randr_get_screen_resources_current_cookie_t rcookie;
@@ -601,7 +603,7 @@ static bool __randr_query_outputs(void) {
     xcb_randr_output_t *randr_outputs;
 
     if (randr_disabled)
-        return true;
+        return;
 
     /* Get screen resources (primary output, crtcs, outputs, modes) */
     rcookie = xcb_randr_get_screen_resources_current(conn, root);
@@ -613,7 +615,7 @@ static bool __randr_query_outputs(void) {
         DLOG("primary output is %08x\n", primary->output);
     if ((res = xcb_randr_get_screen_resources_current_reply(conn, rcookie, NULL)) == NULL) {
         disable_randr(conn);
-        return true;
+        return;
     }
     cts = res->config_timestamp;
 
@@ -695,11 +697,6 @@ static bool __randr_query_outputs(void) {
             DLOG("Output %s disabled, re-assigning workspaces/docks\n", output->name);
 
             first = get_first_output();
-            if (!first) {
-                FREE(res);
-                FREE(primary);
-                return false;
-            }
 
             /* TODO: refactor the following code into a nice function. maybe
              * use an on_destroy callback which is implement differently for
@@ -812,32 +809,6 @@ static bool __randr_query_outputs(void) {
 
     FREE(res);
     FREE(primary);
-
-    return true;
-}
-
-/*
- * (Re-)queries the outputs via RandR and stores them in the list of outputs.
- *
- */
-void randr_query_outputs(void) {
-    static bool first_query = true;
-
-    if (first_query) {
-        /* find monitors at least once via RandR */
-        if (!__randr_query_outputs())
-            die("No usable outputs available.\n");
-        first_query = false;
-    } else {
-        /* requery */
-        if (!__randr_query_outputs()) {
-            DLOG("sleep %f ms due to zero displays\n", config.zero_disp_exit_timer_ms);
-            usleep(config.zero_disp_exit_timer_ms * 1000);
-
-            if (!__randr_query_outputs())
-                die("No usable outputs available.\n");
-        }
-    }
 }
 
 /*
diff --git a/testcases/t/201-config-parser.t b/testcases/t/201-config-parser.t
index fbcc586a5..a2b0a3a93 100644
--- a/testcases/t/201-config-parser.t
+++ b/testcases/t/201-config-parser.t
@@ -369,40 +369,6 @@ is(parser_calls($config),
    $expected,
    'force_display_urgency_hint ok');
 
-################################################################################
-# delay_exit_on_zero_displays
-################################################################################
-
-is(parser_calls('delay_exit_on_zero_displays 300'),
-   "cfg_delay_exit_on_zero_displays(300)\n",
-   'delay_exit_on_zero_displays ok');
-
-is(parser_calls('delay_exit_on_zero_displays 500 ms'),
-   "cfg_delay_exit_on_zero_displays(500)\n",
-   'delay_exit_on_zero_displays ok');
-
-is(parser_calls('delay_exit_on_zero_displays 700ms'),
-   "cfg_delay_exit_on_zero_displays(700)\n",
-   'delay_exit_on_zero_displays ok');
-
-$config = <<'EOT';
-delay_exit_on_zero_displays 300
-delay_exit_on_zero_displays 500 ms
-delay_exit_on_zero_displays 700ms
-delay_exit_on_zero_displays 700
-EOT
-
-$expected = <<'EOT';
-cfg_delay_exit_on_zero_displays(300)
-cfg_delay_exit_on_zero_displays(500)
-cfg_delay_exit_on_zero_displays(700)
-cfg_delay_exit_on_zero_displays(700)
-EOT
-
-is(parser_calls($config),
-   $expected,
-   'delay_exit_on_zero_displays ok');
-
 ################################################################################
 # workspace
 ################################################################################
@@ -475,7 +441,7 @@ client.focused          #4c7899 #285577 #ffffff #2e9ef4
 EOT
 
 my $expected_all_tokens = <<'EOT';
-ERROR: CONFIG: Expected one of these tokens: <end>, '#', 'set', 'bindsym', 'bindcode', 'bind', 'bar', 'font', 'mode', 'floating_minimum_size', 'floating_maximum_size', 'floating_modifier', 'default_orientation', 'workspace_layout', 'new_window', 'new_float', 'hide_edge_borders', 'for_window', 'assign', 'no_focus', 'focus_follows_mouse', 'mouse_warping', 'force_focus_wrapping', 'force_xinerama', 'force-xinerama', 'workspace_auto_back_and_forth', 'fake_outputs', 'fake-outputs', 'force_display_urgency_hint', 'delay_exit_on_zero_displays', 'focus_on_window_activation', 'show_marks', 'workspace', 'ipc_socket', 'ipc-socket', 'restart_state', 'popup_during_fullscreen', 'exec_always', 'exec', 'client.background', 'client.focused_inactive', 'client.focused', 'client.unfocused', 'client.urgent', 'client.placeholder'
+ERROR: CONFIG: Expected one of these tokens: <end>, '#', 'set', 'bindsym', 'bindcode', 'bind', 'bar', 'font', 'mode', 'floating_minimum_size', 'floating_maximum_size', 'floating_modifier', 'default_orientation', 'workspace_layout', 'new_window', 'new_float', 'hide_edge_borders', 'for_window', 'assign', 'no_focus', 'focus_follows_mouse', 'mouse_warping', 'force_focus_wrapping', 'force_xinerama', 'force-xinerama', 'workspace_auto_back_and_forth', 'fake_outputs', 'fake-outputs', 'force_display_urgency_hint', 'focus_on_window_activation', 'show_marks', 'workspace', 'ipc_socket', 'ipc-socket', 'restart_state', 'popup_during_fullscreen', 'exec_always', 'exec', 'client.background', 'client.focused_inactive', 'client.focused', 'client.unfocused', 'client.urgent', 'client.placeholder'
 EOT
 
 my $expected_end = <<'EOT';
