[enhancement] Control i3bar click events
I kind of regret not making those more extensible.

Maybe we should deprecate them in favor of something that takes a button number, similar to a bindsym.

I like that. In fact, I was going to suggest it but then figured it wouldn't be favored as it's inconsistent with the current commands.

Just FYI: I will be working on a PR for this tonight.
