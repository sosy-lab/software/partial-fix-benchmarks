diff --git a/docs/userguide b/docs/userguide
index f46a0be78..f34f02345 100644
--- a/docs/userguide
+++ b/docs/userguide
@@ -573,6 +573,26 @@ workspace_layout default|stacking|tabbed
 workspace_layout tabbed
 ---------------------
 
+[[title_alignment]]
+=== Window title alignment
+
+By default, window titles are aligned to the left side of the titlebar. Using
++title_alignment+, this can be changed to have the titles be displayed centered
+or aligned to the right.
+
+Note that this directive will only have an effect if a pango font is used. It
+does not work when using X core protocol fonts, see <<fonts>>.
+
+*Syntax*:
+---------------------------------
+title_alignment left|center|right
+---------------------------------
+
+*Example*:
+----------------------
+title_alignment center
+----------------------
+
 === Border style for new windows
 
 This option determines which border style new windows will have. The default is
diff --git a/i3-input/main.c b/i3-input/main.c
index 2241f2cff..f6ac45af4 100644
--- a/i3-input/main.c
+++ b/i3-input/main.c
@@ -141,12 +141,12 @@ static int handle_expose(void *data, xcb_connection_t *conn, xcb_expose_event_t
 
     /* draw the prompt … */
     if (prompt != NULL) {
-        draw_text(prompt, pixmap, pixmap_gc, NULL, logical_px(4), logical_px(4), logical_px(492));
+        draw_text(prompt, pixmap, pixmap_gc, NULL, logical_px(4), logical_px(4), logical_px(492), A_LEFT);
     }
     /* … and the text */
     if (input_position > 0) {
         i3String *input = i3string_from_ucs2(glyphs_ucs, input_position);
-        draw_text(input, pixmap, pixmap_gc, NULL, prompt_offset + logical_px(4), logical_px(4), logical_px(492));
+        draw_text(input, pixmap, pixmap_gc, NULL, prompt_offset + logical_px(4), logical_px(4), logical_px(492), A_LEFT);
         i3string_free(input);
     }
 
diff --git a/i3-nagbar/main.c b/i3-nagbar/main.c
index 674fcb7d1..d16f7deb8 100644
--- a/i3-nagbar/main.c
+++ b/i3-nagbar/main.c
@@ -204,7 +204,7 @@ static int handle_expose(xcb_connection_t *conn, xcb_expose_event_t *event) {
     draw_text(prompt, pixmap, pixmap_gc, NULL,
               logical_px(4) + logical_px(4),
               logical_px(4) + logical_px(4),
-              rect.width - logical_px(4) - logical_px(4));
+              rect.width - logical_px(4) - logical_px(4), A_LEFT);
 
     /* render close button */
     const char *close_button_label = "X";
@@ -272,7 +272,7 @@ static int handle_expose(xcb_connection_t *conn, xcb_expose_event_t *event) {
         draw_text(buttons[c].label, pixmap, pixmap_gc, NULL,
                   y - w - line_width + logical_px(6),
                   logical_px(4) + logical_px(3),
-                  rect.width - y + w + line_width - logical_px(6));
+                  rect.width - y + w + line_width - logical_px(6), A_LEFT);
 
         y -= w;
     }
diff --git a/i3bar/include/common.h b/i3bar/include/common.h
index 0d46ab6af..4861107ce 100644
--- a/i3bar/include/common.h
+++ b/i3bar/include/common.h
@@ -24,13 +24,6 @@ struct rect_t {
     int h;
 };
 
-typedef enum {
-    /* First value to make it the default. */
-    ALIGN_LEFT,
-    ALIGN_CENTER,
-    ALIGN_RIGHT
-} blockalign_t;
-
 /* This data structure describes the way a status block should be rendered. These
  * variables are updated each time the statusline is re-rendered. */
 struct status_block_render_desc {
@@ -55,7 +48,7 @@ struct status_block {
     uint32_t min_width;
     char *min_width_str;
 
-    blockalign_t align;
+    alignment_t align;
 
     bool urgent;
     bool no_separator;
diff --git a/i3bar/src/child.c b/i3bar/src/child.c
index 3570dde91..04049dd10 100644
--- a/i3bar/src/child.c
+++ b/i3bar/src/child.c
@@ -221,11 +221,11 @@ static int stdin_string(void *context, const unsigned char *val, size_t len) {
     }
     if (strcasecmp(ctx->last_map_key, "align") == 0) {
         if (len == strlen("center") && !strncmp((const char *)val, "center", strlen("center"))) {
-            ctx->block.align = ALIGN_CENTER;
+            ctx->block.align = A_CENTER;
         } else if (len == strlen("right") && !strncmp((const char *)val, "right", strlen("right"))) {
-            ctx->block.align = ALIGN_RIGHT;
+            ctx->block.align = A_RIGHT;
         } else {
-            ctx->block.align = ALIGN_LEFT;
+            ctx->block.align = A_LEFT;
         }
         return 1;
     }
diff --git a/i3bar/src/xcb.c b/i3bar/src/xcb.c
index 9bf361871..044b43fc9 100644
--- a/i3bar/src/xcb.c
+++ b/i3bar/src/xcb.c
@@ -193,7 +193,7 @@ static void draw_separator(i3_output *output, uint32_t x, struct status_block *b
         /* Draw a custom separator. */
         uint32_t separator_x = MAX(x - block->sep_block_width, center_x - separator_symbol_width / 2);
         draw_util_text(config.separator_symbol, &output->statusline_buffer, sep_fg, bar_bg,
-                       separator_x, logical_px(ws_voff_px), x - separator_x);
+                       separator_x, logical_px(ws_voff_px), x - separator_x, A_LEFT);
     }
 }
 
@@ -223,13 +223,13 @@ uint32_t predict_statusline_length(bool use_short_text) {
         } else {
             uint32_t padding_width = block->min_width - render->width;
             switch (block->align) {
-                case ALIGN_LEFT:
+                case A_LEFT:
                     render->x_append = padding_width;
                     break;
-                case ALIGN_RIGHT:
+                case A_RIGHT:
                     render->x_offset = padding_width;
                     break;
-                case ALIGN_CENTER:
+                case A_CENTER:
                     render->x_offset = padding_width / 2;
                     render->x_append = padding_width / 2 + padding_width % 2;
                     break;
@@ -319,7 +319,7 @@ void draw_statusline(i3_output *output, uint32_t clip_left, bool use_focus_color
 
         draw_util_text(text, &output->statusline_buffer, fg_color, bg_color,
                        x + render->x_offset + border_width, logical_px(ws_voff_px),
-                       render->width - 2 * border_width);
+                       render->width - 2 * border_width, A_LEFT);
         x += full_render_width;
 
         /* If this is not the last block, draw a separator. */
@@ -1980,7 +1980,8 @@ void draw_bars(bool unhide) {
                 draw_util_text(ws_walk->name, &(outputs_walk->buffer), fg_color, bg_color,
                                workspace_width + logical_px(ws_hoff_px) + logical_px(1),
                                logical_px(ws_voff_px),
-                               ws_walk->name_width);
+                               ws_walk->name_width,
+                               A_LEFT);
 
                 workspace_width += 2 * logical_px(ws_hoff_px) + 2 * logical_px(1) + ws_walk->name_width;
                 if (TAILQ_NEXT(ws_walk, tailq) != NULL)
@@ -2009,7 +2010,8 @@ void draw_bars(bool unhide) {
             draw_util_text(binding.name, &(outputs_walk->buffer), fg_color, bg_color,
                            workspace_width + logical_px(ws_hoff_px) + logical_px(1),
                            logical_px(ws_voff_px),
-                           binding.width);
+                           binding.width,
+                           A_LEFT);
 
             unhide = true;
             workspace_width += 2 * logical_px(ws_hoff_px) + 2 * logical_px(1) + binding.width;
diff --git a/include/config.h b/include/config.h
index acdd2c899..5f9288b7b 100644
--- a/include/config.h
+++ b/include/config.h
@@ -181,6 +181,9 @@ struct Config {
         FOWA_NONE
     } focus_on_window_activation;
 
+    /* Specifies how the window title shall be aligned. */
+    alignment_t title_alignment;
+
     /** Specifies whether or not marks should be displayed in the window
      * decoration. Marks starting with a "_" will be ignored either way. */
     bool show_marks;
diff --git a/include/config_directives.h b/include/config_directives.h
index bcbea1115..fb98d3822 100644
--- a/include/config_directives.h
+++ b/include/config_directives.h
@@ -52,6 +52,7 @@ CFGFUN(force_xinerama, const char *value);
 CFGFUN(fake_outputs, const char *outputs);
 CFGFUN(force_display_urgency_hint, const long duration_ms);
 CFGFUN(focus_on_window_activation, const char *mode);
+CFGFUN(title_alignment, const char *value);
 CFGFUN(show_marks, const char *value);
 CFGFUN(hide_edge_borders, const char *borders);
 CFGFUN(assign, const char *workspace);
diff --git a/include/libi3.h b/include/libi3.h
index da5ad891e..80785f6c1 100644
--- a/include/libi3.h
+++ b/include/libi3.h
@@ -392,6 +392,12 @@ char *convert_ucs2_to_utf8(xcb_char2b_t *text, size_t num_glyphs);
  */
 xcb_char2b_t *convert_utf8_to_ucs2(char *input, size_t *real_strlen);
 
+typedef enum alignment_t {
+    A_LEFT,
+    A_CENTER,
+    A_RIGHT
+} alignment_t;
+
 /* Represents a color split by color channel. */
 typedef struct color_t {
     double red;
@@ -426,7 +432,7 @@ bool font_is_pango(void);
  *
  */
 void draw_text(i3String *text, xcb_drawable_t drawable, xcb_gcontext_t gc,
-               xcb_visualtype_t *visual, int x, int y, int max_width);
+               xcb_visualtype_t *visual, int x, int y, int max_width, alignment_t align);
 
 /**
  * ASCII version of draw_text to print static strings.
@@ -584,7 +590,8 @@ color_t draw_util_hex_to_color(const char *color);
  * drawing are used. This will be the case when using XCB to draw text.
  *
  */
-void draw_util_text(i3String *text, surface_t *surface, color_t fg_color, color_t bg_color, int x, int y, int max_width);
+void draw_util_text(i3String *text, surface_t *surface, color_t fg_color, color_t bg_color, int x, int y,
+                    int max_width, alignment_t align);
 
 /**
  * Draws a filled rectangle.
diff --git a/libi3/draw_util.c b/libi3/draw_util.c
index b69d51b40..55f0d3003 100644
--- a/libi3/draw_util.c
+++ b/libi3/draw_util.c
@@ -136,7 +136,8 @@ static void draw_util_set_source_color(xcb_connection_t *conn, surface_t *surfac
  * drawing are used. This will be the case when using XCB to draw text.
  *
  */
-void draw_util_text(i3String *text, surface_t *surface, color_t fg_color, color_t bg_color, int x, int y, int max_width) {
+void draw_util_text(i3String *text, surface_t *surface, color_t fg_color, color_t bg_color, int x, int y,
+                    int max_width, alignment_t align) {
     RETURN_UNLESS_SURFACE_INITIALIZED(surface);
 
 #if CAIRO_SUPPORT
@@ -145,7 +146,7 @@ void draw_util_text(i3String *text, surface_t *surface, color_t fg_color, color_
 #endif
 
     set_font_colors(surface->gc, fg_color, bg_color);
-    draw_text(text, surface->id, surface->gc, surface->visual_type, x, y, max_width);
+    draw_text(text, surface->id, surface->gc, surface->visual_type, x, y, max_width, align);
 
 #if CAIRO_SUPPORT
     /* Notify cairo that we (possibly) used another way to draw on the surface. */
diff --git a/libi3/font.c b/libi3/font.c
index 00e7dfb30..0418712ad 100644
--- a/libi3/font.c
+++ b/libi3/font.c
@@ -100,7 +100,7 @@ static bool load_pango_font(i3Font *font, const char *desc) {
  */
 static void draw_text_pango(const char *text, size_t text_len,
                             xcb_drawable_t drawable, xcb_visualtype_t *visual, int x, int y,
-                            int max_width, bool pango_markup) {
+                            int max_width, bool pango_markup, alignment_t align) {
     /* Create the Pango layout */
     /* root_visual_type is cached in load_pango_font */
     cairo_surface_t *surface = cairo_xcb_surface_create(conn, drawable,
@@ -113,6 +113,17 @@ static void draw_text_pango(const char *text, size_t text_len,
     pango_layout_set_width(layout, max_width * PANGO_SCALE);
     pango_layout_set_wrap(layout, PANGO_WRAP_CHAR);
     pango_layout_set_ellipsize(layout, PANGO_ELLIPSIZE_END);
+    switch (align) {
+        case A_LEFT:
+            pango_layout_set_alignment(layout, PANGO_ALIGN_LEFT);
+            break;
+        case A_CENTER:
+            pango_layout_set_alignment(layout, PANGO_ALIGN_CENTER);
+            break;
+        case A_RIGHT:
+            pango_layout_set_alignment(layout, PANGO_ALIGN_RIGHT);
+            break;
+    }
 
     if (pango_markup)
         pango_layout_set_markup(layout, text, text_len);
@@ -395,7 +406,7 @@ static void draw_text_xcb(const xcb_char2b_t *text, size_t text_len, xcb_drawabl
  *
  */
 void draw_text(i3String *text, xcb_drawable_t drawable, xcb_gcontext_t gc,
-               xcb_visualtype_t *visual, int x, int y, int max_width) {
+               xcb_visualtype_t *visual, int x, int y, int max_width, alignment_t align) {
     assert(savedFont != NULL);
 #if PANGO_SUPPORT
     if (visual == NULL) {
@@ -415,7 +426,8 @@ void draw_text(i3String *text, xcb_drawable_t drawable, xcb_gcontext_t gc,
         case FONT_TYPE_PANGO:
             /* Render the text using Pango */
             draw_text_pango(i3string_as_utf8(text), i3string_get_num_bytes(text),
-                            drawable, visual, x, y, max_width, i3string_is_markup(text));
+                            drawable, visual, x, y, max_width, i3string_is_markup(text),
+                            align);
             return;
 #endif
         default:
@@ -440,7 +452,7 @@ void draw_text_ascii(const char *text, xcb_drawable_t drawable,
             if (text_len > 255) {
                 /* The text is too long to draw it directly to X */
                 i3String *str = i3string_from_utf8(text);
-                draw_text(str, drawable, gc, NULL, x, y, max_width);
+                draw_text(str, drawable, gc, NULL, x, y, max_width, A_LEFT);
                 i3string_free(str);
             } else {
                 /* X11 coordinates for fonts start at the baseline */
@@ -454,7 +466,7 @@ void draw_text_ascii(const char *text, xcb_drawable_t drawable,
         case FONT_TYPE_PANGO:
             /* Render the text using Pango */
             draw_text_pango(text, strlen(text),
-                            drawable, root_visual_type, x, y, max_width, false);
+                            drawable, root_visual_type, x, y, max_width, false, A_LEFT);
             return;
 #endif
         default:
diff --git a/parser-specs/config.spec b/parser-specs/config.spec
index ef3bc2e0e..0e2e8c91c 100644
--- a/parser-specs/config.spec
+++ b/parser-specs/config.spec
@@ -40,6 +40,7 @@ state INITIAL:
   'fake_outputs', 'fake-outputs'           -> FAKE_OUTPUTS
   'force_display_urgency_hint'             -> FORCE_DISPLAY_URGENCY_HINT
   'focus_on_window_activation'             -> FOCUS_ON_WINDOW_ACTIVATION
+  'title_alignment'                        -> TITLE_ALIGNMENT
   'show_marks'                             -> SHOW_MARKS
   'workspace'                              -> WORKSPACE
   'ipc_socket', 'ipc-socket'               -> IPC_SOCKET
@@ -218,6 +219,11 @@ state FORCE_DISPLAY_URGENCY_HINT:
   duration_ms = number
       -> FORCE_DISPLAY_URGENCY_HINT_MS
 
+# title_alignment
+state TITLE_ALIGNMENT:
+  value = 'left', 'center', 'right'
+      -> call cfg_title_alignment($value)
+
 # show_marks
 state SHOW_MARKS:
   value = word
diff --git a/src/config_directives.c b/src/config_directives.c
index ec99321a9..0cd77daf0 100644
--- a/src/config_directives.c
+++ b/src/config_directives.c
@@ -286,6 +286,18 @@ CFGFUN(focus_on_window_activation, const char *mode) {
     DLOG("Set new focus_on_window_activation mode = %i.\n", config.focus_on_window_activation);
 }
 
+CFGFUN(title_alignment, const char *value) {
+    if (strcmp(value, "left") == 0) {
+        config.title_alignment = A_LEFT;
+    } else if (strcmp(value, "center") == 0) {
+        config.title_alignment = A_CENTER;
+    } else if (strcmp(value, "right") == 0) {
+        config.title_alignment = A_RIGHT;
+    } else {
+        ELOG("Unknown title_alignment \"%s\".\n", value);
+    }
+}
+
 CFGFUN(show_marks, const char *value) {
     config.show_marks = eval_boolstr(value);
 }
diff --git a/src/restore_layout.c b/src/restore_layout.c
index 7e1b78aec..7189b2538 100644
--- a/src/restore_layout.c
+++ b/src/restore_layout.c
@@ -176,7 +176,7 @@ static void update_placeholder_contents(placeholder_state *state) {
         DLOG("con %p (placeholder 0x%08x) line %d: %s\n", state->con, state->window, n, serialized);
 
         i3String *str = i3string_from_utf8(serialized);
-        draw_text(str, state->pixmap, state->gc, NULL, 2, (n * (config.font.height + 2)) + 2, state->rect.width - 2);
+        draw_text(str, state->pixmap, state->gc, NULL, 2, (n * (config.font.height + 2)) + 2, state->rect.width - 2, A_LEFT);
         i3string_free(str);
         n++;
         free(serialized);
@@ -187,7 +187,7 @@ static void update_placeholder_contents(placeholder_state *state) {
     int text_width = predict_text_width(line);
     int x = (state->rect.width / 2) - (text_width / 2);
     int y = (state->rect.height / 2) - (config.font.height / 2);
-    draw_text(line, state->pixmap, state->gc, NULL, x, y, text_width);
+    draw_text(line, state->pixmap, state->gc, NULL, x, y, text_width, A_LEFT);
     i3string_free(line);
     xcb_flush(conn);
     xcb_aux_sync(conn);
diff --git a/src/sighandler.c b/src/sighandler.c
index 400cd5a59..cc383b115 100644
--- a/src/sighandler.c
+++ b/src/sighandler.c
@@ -155,7 +155,7 @@ static int sig_draw_window(xcb_window_t win, int width, int height, int font_hei
             set_font_colors(pixmap_gc, draw_util_hex_to_color(bt_colour), draw_util_hex_to_color("#000000"));
 
         draw_text(crash_text_i3strings[i], pixmap, pixmap_gc, NULL,
-                  8, 5 + i * font_height, width - 16);
+                  8, 5 + i * font_height, width - 16, A_LEFT);
 
         /* and reset the colour again for other lines */
         if (i == backtrace_string_index)
diff --git a/src/x.c b/src/x.c
index ed23431c5..4f7233cef 100644
--- a/src/x.c
+++ b/src/x.c
@@ -557,7 +557,8 @@ void x_draw_decoration(Con *con) {
                        p->color->text, p->color->background,
                        con->deco_rect.x + logical_px(2),
                        con->deco_rect.y + text_offset_y,
-                       con->deco_rect.width - 2 * logical_px(2));
+                       con->deco_rect.width - 2 * logical_px(2),
+                       config.title_alignment);
         I3STRING_FREE(title);
 
         goto after_title;
@@ -590,7 +591,7 @@ void x_draw_decoration(Con *con) {
             draw_util_text(mark, &(parent->frame_buffer),
                            p->color->text, p->color->background,
                            con->deco_rect.x + con->deco_rect.width - mark_width - logical_px(2),
-                           con->deco_rect.y + text_offset_y, mark_width);
+                           con->deco_rect.y + text_offset_y, mark_width, A_LEFT);
 
             I3STRING_FREE(mark);
         }
@@ -603,7 +604,8 @@ void x_draw_decoration(Con *con) {
                    p->color->text, p->color->background,
                    con->deco_rect.x + logical_px(2),
                    con->deco_rect.y + text_offset_y,
-                   con->deco_rect.width - mark_width - 2 * logical_px(2));
+                   con->deco_rect.width - mark_width - 2 * logical_px(2),
+                   config.title_alignment);
     if (con->title_format != NULL)
         I3STRING_FREE(title);
 
