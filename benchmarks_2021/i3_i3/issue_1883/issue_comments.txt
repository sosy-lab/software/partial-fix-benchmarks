Multiple monitors: statusbar(s) only on one screen until mod+shift+r
> if this bug report should be logged under i3-status instead, feel free to move it :).

i3status is only the tool providing a stream of data. The actual dock client is i3bar, which is part of i3. So the ticket is in the right place here. :)

The i3 log is probably not all too helpful here. We'll need the log output for i3bar. See [this](https://github.com/i3/i3/blob/next/docs/debugging#L154) for an instruction of how to get it.

It'd also be good if you could tell us how you configure your two screens. This really feels like i3bar is started before you configured your randr to know about the second screen. Do you execute some xrandr commands or use something like autorandr during startup?

Ah it's probably because my xrandr command is further down than my i3bar code in my [i3config file](https://github.com/japhir/i3config)? So I should restructure my config file and I should be fine? Why would reloading the config file solve this issue?

I'm not sure right now whether we first parse everything and then execute in a specific order or whether we do it as we read the config (and am too tired to look it up now). So yeah, it'd be easiest if you could try switching the order.

Reloading would work because the randr setup is already done and when i3bar is restarted, it can open on the correct monitor. Changing the randr config again doesn't do anything anymore anyway (and probably won't even be executed unless you call it with exec_always).

Thank you! Apparently it is executed line-by-line, and changing the order of the config file fixed this.

:+1: 

Great! 

It seems that the issue isn't solved yet ... When rebooting it sometimes does work while at other times it doesn't.
I've enabled logging so here is the logfile that has tracked the behaviour when it fails to load correctly. 
http://logs.i3wm.org/logs/5724313353191424.bz2

here is a screenshot of how it looks:
![2015-09-02-19 16 19](https://cloud.githubusercontent.com/assets/10659193/9639681/7af4db74-51ad-11e5-9705-6daef05b0942.png)

I think the screenshot proves that my theory isn't true because if it were, both bars would be identical. Since your two bars actually show different workspace buttons, it seems like i3bar thinks it's on the correct output even though it isn't.

As for the log file please read my previous comment.

Oh I'm sorry about the log, I thought my log wasn't useful because I only turned on logging after booting with the error, so I provided a new one ;).

I tried getting the i3bar log but this is what I get when running the code:

```
➜  ~  killall i3bar
➜  ~  for c in $(i3-msg -t get_bar_config | python -c \ 
            'import json,sys,print("\n".join(json.load(sys.stdin)))'); do \
            (i3bar --bar_id=$c >i3bar.$c.log 2>&1) & \
      done;
zsh: command not found: import json,sys,print("\n".join(json.load(sys.stdin)))
```

I've tried installing jq since I thought it was the json thingie, but apparently I'm missing some packages that reckognize "json,sys,print". 

Hey all,

I got the same problem: 
![2015-09-03_144631_436229663](https://cloud.githubusercontent.com/assets/3579374/9658836/aab7e348-524c-11e5-99cb-3193cc197d80.png)

The way to debug i3bar does not work on this problem (as far as I can tell). Because after killing the bar and restarting it, it is displayed in the correct manner.

So here is a i3log after restarting my whole machine: http://sprunge.us/TSZj
The lines starting with "[" are the i3bar output. Hope it helps. If you need more information, let me know.

```
hendrik ~$ i3 --moreversion
Binary i3 version:  4.10.3-249-gad826f0 (2015-07-30, branch "makepkg") © 2009 Michael Stapelberg and contributors
Running i3 version: 4.10.3-249-gad826f0 (2015-07-30, branch "makepkg") (pid 1852)
Loaded i3 config: /home/hendrik/.i3/config (Last modified: 2015-09-03T14:36:13 CEST, 1615 seconds ago)

The i3 binary you just called: /usr/bin/i3
The i3 binary you are running: i3

hendrik ~$ xrandr
Screen 0: minimum 8 x 8, current 3286 x 1080, maximum 32767 x 32767
LVDS1 connected primary 1366x768+1920+0 (normal left inverted right x axis y axis) 344mm x 193mm
   1366x768      60.05*+
   1280x720      60.00  
   1024x768      60.00  
   1024x576      60.00  
   960x540       60.00  
   800x600       60.32    56.25  
   864x486       60.00  
   640x480       59.94  
   720x405       60.00  
   680x384       60.00  
   640x360       60.00  
DP1 disconnected (normal left inverted right x axis y axis)
HDMI1 disconnected (normal left inverted right x axis y axis)
VGA1 connected 1920x1080+0+0 (normal left inverted right x axis y axis) 531mm x 299mm
   1920x1080     60.00*+
   1680x1050     59.95  
   1600x900      59.98  
   1280x1024     75.02    60.02  
   1440x900      59.89  
   1280x800      59.81  
   1152x864      75.00  
   1280x720      59.97  
   1024x768      75.08    70.07    60.00  
   832x624       74.55  
   800x600       72.19    75.00    60.32    56.25  
   640x480       75.00    72.81    66.67    60.00  
   720x400       70.08  
VIRTUAL1 disconnected (normal left inverted right x axis y axis)
```

Given that the second bar seems to have the geometry of the output it's supposed to be on I believe the bug is in i3, not in i3bar.

Actually, looking through the log file, before the restart both i3bar windows seem to be requested for `x=0` which is why i3 positions them both on the left screen. So maybe it actually is i3bar at fault still.

@He-Ro Can you please apply the following patch and redo what you did and provide the log file (with the i3bar log)? I want to see what i3bar thinks the outputs are.

```
cd $I3_CHECKOUT
git checkout next && git pull
curl http://pastebin.com/raw.php?i=qvgs9Qcw | git apply
make && sudo make install
```

Here is the the log: http://sprunge.us/gDLd

I restarted i3 at the end of the log.
At that moment the bars were displayed correctly and the coordinates for LVDS1 change from "0 0 1366 768" to "1920 0 1366 768".

Also interesting to note:
The xrandr command I use in the i3 config is as follows:

```
exec_always --no-startup-id xrandr --output LVDS1 --auto --primary --right-of VGA1 --output VGA1 --auto
```

This whole issue also appears when using "--below VGA1", but not when using either "--left-of VGA1" or "--above VGA1".

It seems that i3bar does ask i3 to reconfigure the bar, but i3 denies it and only reconfigures the height of it:

```
2015-09-03 23:43:16 - handlers.c:handle_configure_request:299 - window 0x00a00007 wants to be at 1920x749 with 1366x19
2015-09-03 23:43:16 - handlers.c:handle_configure_request:331 - Configure request!
2015-09-03 23:43:16 - con.c:con_is_floating:429 - checking if con 0x1042da0 is floating
2015-09-03 23:43:16 - handlers.c:handle_configure_request:391 - Dock window, only height reconfiguration allowed
2015-09-03 23:43:16 - handlers.c:handle_configure_request:393 - Height given, changing
```

I think dock windows should be allowed to moved to another output ( @stapelberg Any reason we don't do this already? Or was it just not implemented?). However, this still doesn't answer the question why i3bar starts two clients on a single output in the first place.

@He-Ro In the broken state (both bars on one screen), can you please execute this and give us the file? I have a vague theory of what is going on.

```
i3-msg -t get_tree > /tmp/layout.json
```

I captured two layouts: 
One without any open windows: http://sprunge.us/NiZX
And one with a terminal each on workspaces 1 (LVDS1) and workspace 2 (VGA1): http://sprunge.us/RDUi

Well, that contradicts my theory for the most part. I think we need to prepare some more logging to get more information. I'll try to prepare it today so you can try it again, if that's okay with you. Your help is much appreciated!

What we'll need to see is 1) actual timestamps of the i3bar log so we have a clear timeline of what happened when and 2) we need to see more information about what i3bar thinks the current state of outputs is. In particular whether they are active etc. at the point of time where it decides what to do with the clients.

I'm happy, if I can be of help.
If you get it prepared before tomorrow I can send you the logs right away. Otherwise I will get around to it in about a week due to vacation time.

@He-Ro Adding a timestamp is too much to do right now, but this patch will log the outputs better in i3bar:

```
curl http://pastebin.com/raw.php?i=azgxBve7 | git apply
make && sudo make install
```

What I've done
1. Applied the patch
2. Restarted my machine
3. Saved the layout: http://sprunge.us/gScL
4. Restarted i3, thus the bars were displayed at the correct locations
5. Retrieved the log file: http://sprunge.us/hgWH

Hope it helps ;)

To be honest, I'm quite confused. The log makes no sense to me. The i3bar log part alone seems to be completely out of order. It's probably because error log is printed to stderr. Can you redo this with this, please? I hope I'll have enough then and you can enjoy your vacation. :)

```
curl http://pastebin.com/raw.php?i=wB0rjmzD | git apply
make && sudo make install
```

Same as before:
- layout: http://sprunge.us/gAUT
- log: http://sprunge.us/UCZh

@He-Ro Thanks for your great support on this again!

~~I think I got it now. So initially there's only LVDS1, positioned at (0,0) and i3bar creates a client for it. Now the xrandr change is made and we have VGA1 at (0,0) and LVDS1 at (1920,0). i3bar now creates the bar for VGA1 and tries to move the bar for LVDS1, which fails because i3 doesn't allow that. Hence, both bars end up on the same output.~~

~~This completely depends on the fact that i3 happens to disable VGA1 (and not LVDS1) in the beginning (since they're detected as clones). If it was the other way around, you wouldn't see this bug. It also explains why positioning the outputs the other way around works fine.~~

_Edit:_ Actually this still doesn't explain it. In particular it wouldn't explain how both clients end up in the same dockarea in the tree. I'll have to keep looking…

OK, new theory: a few RandR events are fired at once (probably due to making LVDS1 primary first and then updating the positions). i3bar is first notified that there is only LVDS1 and that it's at (0,0). So i3bar issues a request to open a bar for LVDS1 at (0,0). However, i3, in the meantime, already noticed that LVDS1 is at (1920,0) now, so when the request comes in to put the dock at (0,0), i3 assigns it to VGA1 instead.

Now i3bar is notified of the RandR update and tries to update the position of what it believes to be the LVDS1 dock (but which i3 actually put on VGA1), which is rejected in i3, but i3bar doesn't know that. At the same time it opens a dock for VGA1 which is at (0,0), so the request comes into i3 and it opens another dock for VGA1.

Boom. That's it.

So it all boils down to the race condition that i3bar is lagging behind with RandR events a tiny bit.

@stapelberg The only proper fix to this I can come up with right now is correctly handling the reconfigure request of the dock client. Well, that or having i3bar not reconfigure its clients but destroy and recreate them. What do you think?

@Airblader Handling reconfigure requests for dock clients to support moves sounds like the best fix to me.

@He-Ro / @japhir It'd be great if one of you could test my patch and see if the issue disappeared. I can locally move my bar clients around with this patch, but we should still verify it actually solves the problem.

```
# cd into your i3 checkout, then:
curl https://patch-diff.githubusercontent.com/raw/i3/i3/pull/1895.patch | git apply
make && sudo make install
```

:+1: Seems to work for me. I restarted my machine three times and it worked like a charm every time! Thanks, great work :)

Cool, thanks for the feedback! I'll pretty up the PR then so we can get it merged. Enjoy your vacation. =D
