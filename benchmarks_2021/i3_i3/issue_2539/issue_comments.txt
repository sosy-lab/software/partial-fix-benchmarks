Seemingly random i3 freezes (SIGSTOP)
I don’t see a link to logs.i3wm.org. Did you follow http://i3wm.org/docs/debugging.html? (In case you actually provided a link to a logfile, please ignore me.)

My first guess would be that either of the following two things happen:
1. `child.pid` is `-1` in i3bar/src/child.c:stop_child
2. Something else (third-party application) SIGSTOPs i3

How reliably can you reproduce the issue? I agree with Michael's guesses and it'd be easy enough to check the first one if the issue happens reasonably often. 

@aksr Any updates? 

I'm also experiencing this issue with Arch 4.8.4-1 and most recent i3 from git. I am not able to consistently reproduce this issue, although I did reduce it's frequency from one freeze every 20 minutes to 1 freeze every 3 or 4 hours by switching from chromium-stable to chrome-beta as browser. I do believe there might be a correlation in that, but I am not entirely sure, as it may have been a coincidence. My i3 logs provide no information on what might be happening. Every input given to i3 while its frozen is executed after the SIGCONT is sent. As I said I can't reproduce this consistently but I would gladly provide whatever log or action you request me to do when i3 freezes.

Thank you for investigating the issue. 

I think I had the same problem. It occured while using gtk3 programs such as firefox and transmission with a compositing manager (compton in my case).
For a period I switched to plain i3 without compositing and I hadn't any freeze.

I didn't know that I could unfreeze it with a SIGCONT. I'll try it when the next freeze occours.
I can say I experienced freezes while not using any compositing manager so that might not be the end of it
Yes, I don't use any compositing manager either.
I had a freeze without compositing and I think that I nailed the cause, at least in my case. I think it was the module window_title_async from py3status or i3ipc-python that it uses. I noticed that the freeze every time occured to me when changing the window focus. Now I'm running i3 with compositing and without that module for a coulde of days and it didn't froze yet.
CC @ultrabug @acrisci
Interesting. Misbehaving IPC clients can indeed make i3 hang/crash in a variety of ways. We should add that to our debugging steps.
I have the same issue here. I'm on ArchLinux, i3 version 4.13 (2016-11-08). I'm experimenting this issue since september. I'll try to follow your debugging procedure in order to gather more information about this strange behaviour
I've had random freezes of i3 when I started it using dbus-launch like so:

    dbus-launch --exit-with-session -- i3

Apparently dbus tried to read from stdin from the tty that I ran startx from. Since the .xinitrc was run as a background job, the read caused the whole process group to be stopped by SIGTTIN.
The workaround was to redirect stdin from /dev/null like so:

    dbus-launch --exit-with-session -- i3 </dev/null

So maybe it helps to add </dev/null to the script or add the line

    exec </dev/null

to redirect stdin for all processes after it.

Why is .xinitrc run as a background job in your setup? I was under the impression that .xinitrc is usually run as the session itself, i.e. a foreground job.

Aside from that, I’d recommend to use a display manager instead of startx.
> I’d recommend to use a display manager instead of startx.

That's a recommendation I'd veto… :-)
Why? Using startx is an additional step you need to perform on every boot, and it requires you lock your text consoles, which many people neglect to do. What’s the advantage?
> Using startx is an additional step you need to perform on every boot

Not if you set up your system to run it automatically upon logging into VT1. :-)

> and it requires you lock your text consoles

Why? All other VTs require a login as well (by default – on Arch, anyway). The only attack would be to somehow crash the X session which would leave you logged in (as user!). But if you can crash that, you can probably also crash i3lock which leaves you logged in despite any display manager anyway.

> What’s the advantage?

I'm not running a software I don't need (and which can cause odd bugs as we've learned in another ticket). :-) I don't use other kinds of sessions (not on VT1, anyway) and the user login can be handled perfectly fine by the TTY.

I wouldn't necessarily say there's much of an advantage to startx, just that there's not necessarily much advantage to using a display manager either.
Okay, so I’m not missing any arguments then.

I just really dislike the failure mode of startx: I think when your X session terminates for whatever reason, the resulting state should be a login prompt, not a shell.

Commonly used display managers (e.g. gdm, lightdm, possibly kdm) have the additional advantage that they have a _huge_ user base and hence are more likely to be tested with whichever feature modern desktop environments come up with :). I realize that one can apply this argument to the window manager as well, but I’d say that display managers don’t play a crucial role in the look&feel of the overall user interface (after all, you see them for a few seconds when booting, much like your BIOS/(U)EFI splash screen and boot loader).

If you don’t value either of these reasons, then we’ll just agree to disagree :).
> Why is .xinitrc run as a background job in your setup? I was under the impression that .xinitrc is usually run as the session itself, i.e. a foreground job.

Turns out xinit starts both the X server and .xinitrc in separate process groups, in the background:

    $ ps -H -o stat,pgid,cmd -t /dev/tty2
    STAT  PGID CMD
    Ss+   3919 /bin/sh /usr/bin/startx
    S+    3919   xinit /home/test/.xinitrc -- /etc/X11/xinit/xserverrc :1 vt2 -auth /tmp/serverauth.wgFUTQwZr2
    S<    8813     /usr/lib/xorg-server/Xorg -nolisten tcp :1 vt2 -auth /tmp/serverauth.wgFUTQwZr2
    S     8818     i3

Xorg's and i3's `pgid` are different from xinit's while xinit being the foreground (STAT shows +).

It probably has to do this so it can exit if any of the two processes exit. It then kills the other and exits itself.

Note though that I don't start i3 using dbus-launch anymore. It's not needed anymore because it's now automatically started by the systemd user daemon (which happens by default these days on Arch I think).

> I think when your X session terminates for whatever reason, the resulting state should be a login prompt, not a shell.

Solution to this is starting startx with exec, so it replaces your shell. So you get back to the login prompt if your X session crashes.
> I just really dislike the failure mode of startx: I think when your X session terminates for whatever reason, the resulting state should be a login prompt, not a shell.

Untested, but `startx; exit`? :-) Or what @ferreum said.

> If you don’t value either of these reasons, then we’ll just agree to disagree :).

Just to clarify, from my point of view there's nothing wrong about your recommendation. I just do it differently and thus don't share that recommendation. All in good fun. :-)
To rule out between `py3status` or `i3ipc-python` that it uses, I was able to reproduce i3wm freeze using this script `i3ipc-rename-ws.py` with `i3status` so I think the issue is somewhere in `i3ipc-python`.
```
i3ipc-rename-ws.py
-------------------------
#!/usr/bin/env python

import i3ipc

i3 = i3ipc.Connection()

def on_window_focus(i3, e):
    focused = i3.get_tree().find_focused()
    ws_name = "%s:%s" % (focused.workspace().num, focused.window_class)
    i3.command('rename workspace to "%s"' % ws_name)

i3.on("window::focus", on_window_focus)
i3.main()
```

I have been getting random freezes as well. The last time it happened I was switching tabs in firefox, I am using Compton.
I'm ashamed to discover the ping only now, sorry @Airblader ...

I'll ping @tobes and @guiniol on this as well since I guess this relates with https://github.com/ultrabug/py3status/issues/941

This is an interesting problem, thanks for the rule out @lasers 👍 
I just reproduced the problem simply by running the i3ipc-rename-ws.py script.
Just to be clear because I was cc'd. In the issue I opened (ultrabug/py3status#941), I do not have the whole of i3 freezing, but only py3status, which restarts when I send `SIGCONT`.
py3status freezes sometimes when I lock the screen with i3lock. When I check strace, those are the instances where py3status receives a `SIGSTOP`. When everything works as expected (ie, py3status restarts properly after I unlock the screen), py3status never receives a `SIGSTOP`. I have no idea what is sending those `SIGSTOP` and why they are only sent sometimes and only when I lock the screen.

EDIT: I haven't been able to reproduce the issue described here with the `i3ipc-rename-ws.py` script.
Just to add that in `py3status` we start our output to `i3bar` with `"stop_signal": SIGTSTP` in the header block so we should not be being sent the `SIGSTOP` from my understanding.

I can't trigger things with `i3ipc-rename-ws.py` but maybe a version that is more reproducible might help as maybe a value of `ws_name` causes the issue.
**UPDATE**: I got my problem tracked down to a faulty RAM module, so not i3 related!

I am running 4.12.8-2-ARCH and are also experiencing random freezes.

At first I thought it was a driver problem and switched graphic card from Intel to Nvidia, but that didn't solve the problem.

I am unable to reproduce the problem as it is completely random. I have however noticed that it occurs mostly while either browsing and scrolling on a webpage in either Firefox or Chromium, or when editing text using Neovim or some other editor and running the cursor across some text.

When it happens I have to do a hard-reboot and I cannot locate any useful information in the systemd journal or elsewhere.

It happens at least once a day when I use my computer and often several times.

For me, when the freeze happens, `ipc_send_message` is stuck in an infinite loop:

https://github.com/i3/i3/blob/7a7481e5784fc4db9b5724c9d2755040f5a47fed/libi3/safewrappers.c#L73-L77


```
#0  0x00007fe998c83884 in write () at /usr/lib/libpthread.so.0
#1  0x00007fe99cdc7920 in __interceptor_write(int, void*, SIZE_T) (fd=<optimized out>, ptr=0x7fff09f4ad50, count=<optimized out>) at /build/gcc-multilib/src/gcc/libsanitizer/sanitizer_common/sanitizer_common_interceptors.inc:871
#2  0x0000555eb3a62d3f in writeall (fd=12, buf=0x7fff09f4ad50, count=14) at ../../i3/libi3/safewrappers.c:74
#3  0x0000555eb3a61735 in ipc_send_message (sockfd=12, message_size=3318, message_type=2147483648, payload=0x621000074500 "{\"change\":\"focus\",\"current\":{\"id\":106927505813056,\"type\":\"workspace\",\"orientation\":\"horizontal\",\"scratchpad_state\":\"none\",\"percent\":null,\"urgent\":false,\"focused\":false,\"output\":\"fake-0\",\"layout\":\"spli"...) at ../../i3/libi3/ipc_send_message.c:35
#4  0x0000555eb39cda3a in ipc_send_event (event=0x555eb3a9bcc0 "workspace", message_type=2147483648, payload=0x621000074500 "{\"change\":\"focus\",\"current\":{\"id\":106927505813056,\"type\":\"workspace\",\"orientation\":\"horizontal\",\"scratchpad_state\":\"none\",\"percent\":null,\"urgent\":false,\"focused\":false,\"output\":\"fake-0\",\"layout\":\"spli"...) at ../../i3/src/ipc.c:60
#5  0x0000555eb39da70b in ipc_send_workspace_event (change=0x555eb3ac3340 "focus", current=0x614000002e40, old=0x614000003840) at ../../i3/src/ipc.c:1333
#6  0x0000555eb3a3a9ff in _workspace_show (workspace=0x614000002e40) at ../../i3/src/workspace.c:440
#7  0x0000555eb3a3af17 in workspace_show (workspace=0x614000002e40) at ../../i3/src/workspace.c:490
```
I think I found a pattern here.

On a clean i3 session start this script:

```
#!/usr/bin/env python
import i3ipc


def on_ws_focus(self, e):
    print(e.old.name, '->', e.current.name)


for i in range(10):
    try:
        i3 = i3ipc.Connection()
        i3.on('workspace::focus', on_ws_focus)
        i3.main()
    except KeyboardInterrupt:
        print('ctrl-c pressed for connection #', i)
```

each time you ctrl-c it, a new i3ipc connection is started.

Now, run this script:

```
for i in {1..32}
do
i3-msg workspace 2
i3-msg workspace 1
done
```


First time nothing happens, the loop is completed. Now press ctrl-c on the ipc python script and rerun the workspace switch script: i3 freezes.

For me it's not random and I can reproduce it whenever I want, I just have to run Lazarus IDE and sometimes right after loading and showing on the screen it locks up, sometimes few minutes after, it's not Lazarus because when I login to different DE it's fine there.
I'm guessing this is another occurence of a misbehaving IPC client. We've discussed this in the past. See #2280.
Try isolating the issue by using the new [go library](https://github.com/i3/go-i3) to test the script or test the script with [sway](https://github.com/SirCmpwn/sway) to try to get some more info where the problem is. If it's really a problem with i3ipc-python, open an issue on my issue tracker and we'll discuss it there.
I think the problem in the script is that the event IPC connection is not closed upon `KeyboardInterrupt`.

I filed issue #2999 to improve i3’s behavior when IPC clients misbehave.

In the go.i3wm.org/i3 Go package, users must call [Next()](https://godoc.org/go.i3wm.org/i3#EventReceiver.Next) in a loop (i3 might block until the next call to `Next()`) and [Close()](https://godoc.org/go.i3wm.org/i3#EventReceiver.Close) once done. A good way to do this is to use defer to ensure that `Close` is called:

```go
func() {
  recv := i3.Subscribe(i3.WindowEventType)
  defer recv.Close() // ensure cleanup to prevent i3 deadlocks
  for recv.Next() {
    ev := recv.Event().(*i3.WindowEvent)
    // …process ev… — blocking here might mean blocking i3
  }
}()
// At this point, the IPC connection is closed.
```

If receiving all events is not a requirement, one can receive events in a different goroutine and discard events when the processing goroutine is busy:
```go
// Make this channel buffered if required
// (e.g. if each event is persisted to (slow) storage):
windowEvents := make(chan *i3.WindowEvent)
go func() {
  recv := i3.Subscribe(i3.WindowEventType)
  defer recv.Close() // ensure cleanup to prevent i3 deadlocks
  for recv.Next() {
    select {
      case windowEvents <- recv.Event().(*i3.WindowEvent):
      default:
        // discard event to never block i3
    }
  }
}()
for ev := range windowEvents {
  // …process ev…
}
```
I got freezes when I connect Ethernet cable while using wifi. Ubuntu 16.04,  i3 4.14.1-1. Logs show nothing. I use conky instead of i3status
Now that Crandel mentions it I do seem to get occasionally lockups when plugging in a Thunderbolt 3 dock that includes an Ethernet port (USB device internally), and the freezes happen far more often if the Ethernet port is connected than if I am only using wireless. It could be a spurious event from NetworkManager (nm-applet) triggering the issue in my case since I also use that, but interesting coincidence nonetheless.
This is old and possibly not directly related, but it's the closest hit to my (workaround solved) problem, so thought it might be useful to somebody. I use i3-input to switch workspaces, and once every couple of days it hangs. Keyboard input is stolen, but mouse still works. Multi-monitor setup, seems to be related to typing too fast, but that's a guess, no peripherals involved.

I tried ssh-ing in from another machine and looking for things to kill, nothing. So for ages I just used to reboot the machine. Yesterday I discovered that if someone Skypes me, which on my machine shows a popup in the top right of the screen, the keyboard unlocks and everything is fine again.

Perhaps that's a clue, or at least useful to someone.
(sorry if this is a thread hijack, I scoured the internet looking for a solution)

Too early to tell, but I think disabling the `bar` from i3 config prevents i3 from freezing.