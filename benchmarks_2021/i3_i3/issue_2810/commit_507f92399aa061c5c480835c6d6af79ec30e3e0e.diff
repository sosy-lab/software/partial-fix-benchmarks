diff --git a/docs/userguide b/docs/userguide
index da5d98737..f92b4bea4 100644
--- a/docs/userguide
+++ b/docs/userguide
@@ -2092,8 +2092,7 @@ using one of the following methods:
 +mark+:: A container with the specified mark, see <<vim_like_marks>>.
 
 Note that swapping does not work with all containers. Most notably, swapping
-floating containers or containers that have a parent-child relationship to one
-another does not work.
+containers that have a parent-child relationship to one another does not work.
 
 *Syntax*:
 ----------------------------------------
diff --git a/src/con.c b/src/con.c
index d50c29be7..aa50d59bd 100644
--- a/src/con.c
+++ b/src/con.c
@@ -2316,6 +2316,16 @@ i3String *con_parse_title_format(Con *con) {
     return formatted;
 }
 
+static Rect *get_floating_rect_and_disable(Con *con) {
+    Rect *result = NULL;
+    if (con_is_floating(con)) {
+        result = smalloc(sizeof(Rect));
+        *result = con->parent->rect;
+        floating_disable(con, false);
+    }
+    return result;
+}
+
 /*
  * Swaps the two containers.
  *
@@ -2335,11 +2345,6 @@ bool con_swap(Con *first, Con *second) {
         return false;
     }
 
-    if (con_is_floating(first) || con_is_floating(second)) {
-        ELOG("Floating windows cannot be swapped.\n");
-        return false;
-    }
-
     if (first == second) {
         DLOG("Swapping container %p with itself, nothing to do.\n", first);
         return false;
@@ -2359,6 +2364,8 @@ bool con_swap(Con *first, Con *second) {
     const bool focused_within_second = (second == old_focus || con_has_parent(old_focus, second));
     fullscreen_mode_t first_fullscreen_mode = first->fullscreen_mode;
     fullscreen_mode_t second_fullscreen_mode = second->fullscreen_mode;
+    Rect *first_floating_rect = get_floating_rect_and_disable(first);
+    Rect *second_floating_rect = get_floating_rect_and_disable(second);
 
     if (first_fullscreen_mode != CF_NONE) {
         con_disable_fullscreen(first);
@@ -2467,10 +2474,12 @@ bool con_swap(Con *first, Con *second) {
     second->percent = first_percent;
     fake->percent = 0.0;
 
+    /* The two windows exchange their original fullscreen status */
     SWAP(first_fullscreen_mode, second_fullscreen_mode, fullscreen_mode_t);
+    /* And their floating status */
+    SWAP(first_floating_rect, second_floating_rect, Rect *);
 
 swap_end:
-    /* The two windows exchange their original fullscreen status */
     if (first_fullscreen_mode != CF_NONE) {
         con_enable_fullscreen(first, first_fullscreen_mode);
     }
@@ -2490,5 +2499,16 @@ bool con_swap(Con *first, Con *second) {
     con_force_split_parents_redraw(first);
     con_force_split_parents_redraw(second);
 
+    if (first_floating_rect) {
+        floating_enable(first, false);
+        floating_reposition(first->parent, *first_floating_rect);
+        free(first_floating_rect);
+    }
+    if (second_floating_rect) {
+        floating_enable(second, false);
+        floating_reposition(second->parent, *second_floating_rect);
+        free(second_floating_rect);
+    }
+
     return result;
 }
diff --git a/testcases/t/291-swap.t b/testcases/t/291-swap.t
index da2d564d0..f6fc4c178 100644
--- a/testcases/t/291-swap.t
+++ b/testcases/t/291-swap.t
@@ -28,8 +28,27 @@ my ($ws, $ws1, $ws2, $ws3);
 my ($node, $nodes, $expected_focus, $A, $B, $F);
 my ($result);
 my @fullscreen_permutations = ([], ["A"], ["B"], ["A", "B"]);
+my $rect_A = [ 100, 100, 100, 100 ];
+my $rect_B = [ 200, 200, 200, 200 ];
 my @urgent;
 
+sub fullscreen_windows {
+    my $ws = shift if @_;
+
+    scalar grep { $_->{fullscreen_mode} != 0 } @{get_ws_content($ws)}
+}
+
+sub cmp_floating_rect {
+    my ($window, $rect, $prefix) = @_;
+    my ($absolute, $top) = $window->rect;
+
+    is($absolute->{width}, $rect->[2], "$prefix: width matches");
+    is($absolute->{height}, $rect->[3], "$prefix: height matches");
+
+    is($top->{x}, $rect->[0], "$prefix: x matches");
+    is($top->{y}, $rect->[1], "$prefix: y matches");
+}
+
 ###############################################################################
 # Invalid con_id should not crash i3
 # See issue #2895.
@@ -502,6 +521,119 @@ cmp_float($nodes->[1]->{nodes}->[0]->{percent}, 0.75, 'A has 75% height');
 
 kill_all_windows;
 
+###############################################################################
+# Swap floating windows in the same workspace. Check that they exchange rects.
+###############################################################################
+$ws = fresh_workspace;
+
+$A = open_floating_window(wm_class => 'mark_A', rect => $rect_A);
+$B = open_floating_window(wm_class => 'mark_B', rect => $rect_B);
+
+cmd '[con_mark=B] swap container with mark A';
+cmp_floating_rect($A, $rect_B, 'A got B\'s rect');
+cmp_floating_rect($B, $rect_A, 'B got A\'s rect');
+
+kill_all_windows;
+
+###############################################################################
+# Swap a fullscreen floating and a normal floating window.
+###############################################################################
+$ws1 = fresh_workspace;
+$A = open_floating_window(wm_class => 'mark_A', rect => $rect_A, fullscreen => 1);
+$ws2 = fresh_workspace;
+$B = open_floating_window(wm_class => 'mark_B', rect => $rect_B);
+
+cmd '[con_mark=B] swap container with mark A';
+sync_with_i3;
+
+cmp_floating_rect($A, $rect_B, 'A got B\'s rect');
+
+$nodes = get_ws($ws1);
+$node = $nodes->{floating_nodes}->[0]->{nodes}->[0];
+is($node->{window}, $B->{id}, 'B is on the first workspace');
+is($node->{fullscreen_mode}, 1, 'B is now fullscreened');
+
+$nodes = get_ws($ws2);
+$node = $nodes->{floating_nodes}->[0]->{nodes}->[0];
+is($node->{window}, $A->{id}, 'A is on the second workspace');
+is($node->{fullscreen_mode}, 0, 'A is not fullscreened anymore');
+
+kill_all_windows;
+
+###############################################################################
+# Swap a floating window which is in a workspace that has another, regular
+# window with a regular window in another workspace. A & B focused.
+#
+# Before:
+# +-----------+
+# |         F |
+# |   +---+   |
+# |   | A |   |
+# |   +---+   |
+# |           |
+# +-----------+
+#
+# +-----------+
+# |           |
+# |           |
+# |     B     |
+# |           |
+# |           |
+# +-----------+
+#
+# After: Same as above but A <-> B. A & B focused.
+###############################################################################
+$ws1 = fresh_workspace;
+open_window;
+$A = open_floating_window(wm_class => 'mark_A', rect => $rect_A);
+$ws2 = fresh_workspace;
+$B = open_window(wm_class => 'mark_B');
+$expected_focus = get_focused($ws2);
+
+cmd '[con_mark=B] swap container with mark A';
+cmd "workspace $ws1"; # TODO: Why does rect check fails without switching workspaces?
+sync_with_i3;
+
+$nodes = get_ws($ws1);
+$node = $nodes->{floating_nodes}->[0]->{nodes}->[0];
+is($node->{window}, $B->{id}, 'B is floating on the first workspace');
+is(get_focused($ws1), $expected_focus, 'B is focused');
+cmp_floating_rect($B, $rect_A, 'B got A\'s rect');
+
+$nodes = get_ws_content($ws2);
+$node = $nodes->[0];
+is($node->{window}, $A->{id}, 'A is on the second workspace');
+
+kill_all_windows;
+
+###############################################################################
+# Swap a sticky, floating container A and a floating fullscreen container B.
+# A should remain sticky and floating and should be fullscreened.
+###############################################################################
+$ws1 = fresh_workspace;
+open_window;
+$A = open_floating_window(wm_class => 'mark_A', rect => $rect_A);
+$expected_focus = get_focused($ws1);
+cmd 'sticky enable';
+$B = open_floating_window(wm_class => 'mark_B', rect => $rect_B);
+cmd 'fullscreen enable';
+
+cmd '[con_mark=B] swap container with mark A';
+sync_with_i3;
+
+is(@{get_ws($ws1)->{floating_nodes}}, 2, '2 fullscreen containers on first workspace');
+is(get_focused($ws1), $expected_focus, 'A is focused');
+
+$ws2 = fresh_workspace;
+cmd 'fullscreen disable';
+cmp_floating_rect($A, $rect_B, 'A got B\'s rect');
+is(@{get_ws($ws2)->{floating_nodes}}, 1, 'only A in new workspace');
+
+cmd "workspace $ws1"; # TODO: Why does rect check fails without switching workspaces?
+cmp_floating_rect($B, $rect_A, 'B got A\'s rect');
+
+kill_all_windows;
+
 ###############################################################################
 # Swapping containers moves the urgency hint correctly.
 ###############################################################################
