diff --git a/include/ipc.h b/include/ipc.h
index c6ad35c77..6f42d0f86 100644
--- a/include/ipc.h
+++ b/include/ipc.h
@@ -24,6 +24,14 @@
 
 extern char *current_socketpath;
 
+typedef struct pending_message_t {
+    const char *payload;
+    uint32_t type;
+
+    TAILQ_ENTRY(pending_message_t)
+    pending_messages;
+} pending_message_t;
+
 typedef struct ipc_client {
     int fd;
 
@@ -35,6 +43,11 @@ typedef struct ipc_client {
      * event has been sent by i3. */
     bool first_tick_sent;
 
+    struct ev_io *callback;
+    struct ev_timer *timeout;
+    TAILQ_HEAD(pending_messages_head, pending_message_t)
+    pending_messages_head;
+
     TAILQ_ENTRY(ipc_client)
     clients;
 } ipc_client;
diff --git a/src/ipc.c b/src/ipc.c
index 7f9ac2933..00a260b9c 100644
--- a/src/ipc.c
+++ b/src/ipc.c
@@ -38,8 +38,38 @@ static void set_nonblock(int sockfd) {
         err(-1, "Could not set O_NONBLOCK");
 }
 
+//TODO
+static bool socket_is_writeable(int sockfd) {
+    fd_set wset;
+    FD_ZERO(&wset);
+    FD_SET(sockfd, &wset);
+
+    struct timeval tv;
+    tv.tv_sec = 0;
+    tv.tv_usec = 111;
+
+    return select(sockfd + 1, NULL, &wset, NULL, &tv) > 0;
+}
+
+//TODO
 static void free_ipc_client(ipc_client *client) {
     close(client->fd);
+
+    ev_io_stop(main_loop, client->callback);
+    FREE(client->callback);
+    if (client->timeout) {
+        ev_timer_stop(main_loop, client->timeout);
+        FREE(client->timeout);
+    }
+
+    struct pending_messages_head *head = &(client->pending_messages_head);
+    while (!TAILQ_EMPTY(head)) {
+        pending_message_t *message = TAILQ_FIRST(head);
+        TAILQ_REMOVE(head, message, pending_messages);
+        free(message->payload);
+        free(message);
+    }
+
     for (int i = 0; i < client->num_events; i++)
         free(client->events[i]);
     free(client->events);
@@ -47,6 +77,61 @@ static void free_ipc_client(ipc_client *client) {
     free(client);
 }
 
+typedef enum {
+    PUSHED_NONE,
+    PUSHED_SOME,
+    PUSHED_ALL
+} ipc_push_result;
+
+//TODO
+static ipc_push_result ipc_push_pending(ipc_client *client) {
+    struct pending_messages_head *head = &(client->pending_messages_head);
+    pending_message_t *next = TAILQ_FIRST(head);
+    ipc_push_result result = PUSHED_NONE;
+    while (next) {
+        pending_message_t *current = next;
+        next = TAILQ_NEXT(next, pending_messages);
+        const char *payload = current->payload;
+
+        if (socket_is_writeable(client->fd)) {
+            ipc_send_message(client->fd, strlen(payload), current->type, (const uint8_t *)payload);
+            result = PUSHED_SOME;
+            TAILQ_REMOVE(head, current, pending_messages);
+        } else {
+            return result;
+        }
+        TAILQ_REMOVE(head, current, pending_messages);
+        free(current->payload);
+        free(current);
+    }
+
+    return PUSHED_ALL;
+}
+
+static void ipc_client_timeout(EV_P_ ev_timer *w, int revents);
+static void ipc_socket_writeable_cb(EV_P_ struct ev_io *w, int revents);
+
+//TODO
+static void ipc_send_message_or_queue(ipc_client *client, pending_message_t *message) {
+    TAILQ_INSERT_TAIL(&(client->pending_messages_head), message, pending_messages);
+    if (ipc_push_pending(client) == PUSHED_ALL) {
+        return;
+    }
+
+    /* Socket was not writeable, schedule a watcher. */
+    ev_io_start(main_loop, client->callback);
+    if (client->timeout) {
+        /* Keep the old timeout, otherwise we might keep a dead connection by
+         * continuously renewing its timeouts. */
+        return;
+    }
+    struct ev_timer *timeout = scalloc(1, sizeof(struct ev_timer));
+    ev_timer_init(timeout, ipc_client_timeout, 10.0, 0.);
+    timeout->data = client;
+    client->timeout = timeout;
+    ev_timer_start(main_loop, timeout);
+}
+
 /*
  * Sends the specified event to all IPC clients which are currently connected
  * and subscribed to this kind of event.
@@ -66,7 +151,10 @@ void ipc_send_event(const char *event, uint32_t message_type, const char *payloa
         if (!interested)
             continue;
 
-        ipc_send_message(current->fd, strlen(payload), message_type, (const uint8_t *)payload);
+        pending_message_t *message = smalloc(sizeof(pending_message_t));
+        message->payload = sstrdup(payload);
+        message->type = message_type;
+        ipc_send_message_or_queue(current, message);
     }
 }
 
@@ -1285,6 +1373,44 @@ static void ipc_receive_message(EV_P_ struct ev_io *w, int revents) {
     FREE(message);
 }
 
+static void ipc_client_timeout(EV_P_ ev_timer *w, int revents) {
+    /* No need to be polite and check for writeability, the other callback would
+     * have been called by now. */
+    ipc_client *client = (ipc_client *)w->data;
+
+    DLOG("Client %p on fd %d timed out, killing\n", client, client->fd);
+    free_ipc_client(client);
+}
+
+static void ipc_socket_writeable_cb(EV_P_ ev_io *w, int revents) {
+    DLOG("writeable %d\n", w->fd);
+    ipc_client *client = (ipc_client *)w->data;
+
+    /* If this callback is called then there should be a corresponding active
+     * timer. Would have caused a segfault anyway so an assertion here is ok. */
+    assert(client->timeout != NULL);
+
+    const ipc_push_result result = ipc_push_pending(client);
+    if (result == PUSHED_NONE) {
+        /* This should never happen but I am not brave enough to put an
+         * assertion here. Just clean everything to get out of this ugly
+         * situation. */
+        ELOG("libev reported socket as writeable but we couldn't push any message to ipc client with fd %d\n", w->fd);
+        free_ipc_client(client);
+        return;
+    }
+
+    ev_timer_stop(main_loop, client->timeout);
+    if (result == PUSHED_SOME) {
+        ev_timer_set(client->timeout, 10.0, 0.);
+        ev_timer_start(main_loop, client->timeout);
+    } else {
+        /* Fresh start: stop the timeout and disable this callback. */
+        FREE(client->timeout);
+        ev_io_stop(EV_A_ w);
+    }
+}
+
 /*
  * Handler for activity on the listening socket, meaning that a new client
  * has just connected and we should accept() him. Sets up the event handler
@@ -1313,10 +1439,17 @@ void ipc_new_client(EV_P_ struct ev_io *w, int revents) {
     ev_io_init(package, ipc_receive_message, client, EV_READ);
     ev_io_start(EV_A_ package);
 
+    ipc_client *new = scalloc(1, sizeof(ipc_client));
+
+    package = scalloc(1, sizeof(struct ev_io));
+    package->data = new;
+    ev_io_init(package, ipc_socket_writeable_cb, client, EV_WRITE);
+
     DLOG("IPC: new client connected on fd %d\n", w->fd);
 
-    ipc_client *new = scalloc(1, sizeof(ipc_client));
     new->fd = client;
+    new->callback = package;
+    TAILQ_INIT(&(new->pending_messages_head));
 
     TAILQ_INSERT_TAIL(&all_clients, new, clients);
 }
