Programs with splash screens causes layouts to crash i3
Probably a duplicate of https://github.com/i3/i3/issues/3610 but I haven't attempted to reproduce that one yet.
```diff
diff --git a/libi3/draw_util.c b/libi3/draw_util.c
index f88360dc3..bca397f17 100644
--- a/libi3/draw_util.c
+++ b/libi3/draw_util.c
@@ -23,7 +23,7 @@ static void draw_util_set_source_color(surface_t *surface, color_t color);
 
 #define RETURN_UNLESS_SURFACE_INITIALIZED(surface)                               \
     do {                                                                         \
-        if ((surface)->id == XCB_NONE) {                                         \
+        if ((surface)->cr == NULL || (surface)->id == XCB_NONE) {                \
             ELOG("Surface %p is not initialized, skipping drawing.\n", surface); \
             return;                                                              \
         }                                                                        \
```

I'd like to find the root cause but this patch fixes the crash for me.
TheSilvus uploaded a backtrace in the closed bug report: https://github.com/i3/i3/issues/3610#issuecomment-459988658
The linked file doesn't contain an actual backtrace. I do have a backtrace though.
Regression: 7ac37d8ae4