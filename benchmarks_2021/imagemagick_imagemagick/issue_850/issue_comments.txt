Parsing comments in pnm fails
Thanks for the problem report.  We can reproduce it and will have a patch to fix it in GIT master branch @ https://github.com/ImageMagick/ImageMagick later today.  The patch will be available in the beta releases of ImageMagick @ https://www.imagemagick.org/download/beta/ by sometime tomorrow. 

ImageMagick best practices strongly encourages you to configure a security policy that suits your local environment.  See https://www.imagemagick.org/script/security-policy.php.