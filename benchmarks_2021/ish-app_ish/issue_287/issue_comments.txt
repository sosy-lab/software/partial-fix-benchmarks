cannot find name of tty
This is because when you run programs normally ttyname fails.
okay. I did a little googling and didn't find any obvious solutions. Since this is still open, does that mean it's an issue in ish?
Yep, it'll get fixed eventually.

This might actually work if you do it over an ssh connection.
I just tried it over ssh and it gave me the same message.
darn. guess I'm wrong then
This should be fixed now
This sudo giving no tty present and no askpass program specified this is happening to me now. Happy to try debugging steps.
I’m getting the same response on adding a user and after:
`su user`
`sudo apk update`

`sudo: no tty present and no askpass program specified`

Doing sudo via SSH is ok, but the following issue still occurs as @Pheoxy reported.

```console
# su user
$ sudo ls
sudo: no tty present and no askpass program specified
```

I'm using iSH 1.0 build 62 (TestFlight) on iPadOS 13.2.
Probably something to do with the interaction with multiwindow.