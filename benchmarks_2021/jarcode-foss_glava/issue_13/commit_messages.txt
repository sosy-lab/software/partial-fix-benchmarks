Added configurable solid background color, closes #16
Added support for old versions of 'glfw3native.h', fixes #13
fixed include order in xwin.c, fixes #13
Backported for GLFW 3.1, fixes #13, *again*
Backported for GLFW 3.1, fixes #13, *again*
