diff --git a/INSTALL b/INSTALL
index 2800df46f1..6c53bfc089 100644
--- a/INSTALL
+++ b/INSTALL
@@ -104,7 +104,6 @@ any of the following arguments (not a definitive list) to 'configure':
 --enable-debug
     Enable assertions and validation code.  This incurs a substantial
     performance hit, but is very useful during application development.
-    Implies --enable-ivsalloc.
 
 --enable-code-coverage
     Enable code coverage support, for use during jemalloc test development.
@@ -123,12 +122,6 @@ any of the following arguments (not a definitive list) to 'configure':
     Disable statistics gathering functionality.  See the "opt.stats_print"
     option documentation for usage details.
 
---enable-ivsalloc
-    Enable validation code for malloc_usable_size() and sallocx(), which
-    verifies that pointers reside within jemalloc-owned extents before
-    dereferencing metadata.  This incurs a minor performance hit, and causes
-    the functions to return 0 for failed lookups.
-
 --enable-prof
     Enable heap profiling and leak detection functionality.  See the "opt.prof"
     option documentation for usage details.  When enabled, there are several
diff --git a/configure.ac b/configure.ac
index 4709e27c9f..73450b4c78 100644
--- a/configure.ac
+++ b/configure.ac
@@ -958,7 +958,7 @@ fi
 dnl Do not compile with debugging by default.
 AC_ARG_ENABLE([debug],
   [AS_HELP_STRING([--enable-debug],
-                  [Build debugging code (implies --enable-ivsalloc)])],
+                  [Build debugging code])],
 [if test "x$enable_debug" = "xno" ; then
   enable_debug="0"
 else
@@ -972,26 +972,9 @@ if test "x$enable_debug" = "x1" ; then
 fi
 if test "x$enable_debug" = "x1" ; then
   AC_DEFINE([JEMALLOC_DEBUG], [ ])
-  enable_ivsalloc="1"
 fi
 AC_SUBST([enable_debug])
 
-dnl Do not validate pointers by default.
-AC_ARG_ENABLE([ivsalloc],
-  [AS_HELP_STRING([--enable-ivsalloc],
-                  [Validate pointers passed through the public API])],
-[if test "x$enable_ivsalloc" = "xno" ; then
-  enable_ivsalloc="0"
-else
-  enable_ivsalloc="1"
-fi
-],
-[enable_ivsalloc="0"]
-)
-if test "x$enable_ivsalloc" = "x1" ; then
-  AC_DEFINE([JEMALLOC_IVSALLOC], [ ])
-fi
-
 dnl Only optimize if not debugging.
 if test "x$enable_debug" = "x0" ; then
   if test "x$GCC" = "xyes" ; then
diff --git a/include/jemalloc/internal/jemalloc_internal_defs.h.in b/include/jemalloc/internal/jemalloc_internal_defs.h.in
index d3d7694432..44896ae28f 100644
--- a/include/jemalloc/internal/jemalloc_internal_defs.h.in
+++ b/include/jemalloc/internal/jemalloc_internal_defs.h.in
@@ -224,12 +224,6 @@
 #undef JEMALLOC_INTERNAL_FFSL
 #undef JEMALLOC_INTERNAL_FFS
 
-/*
- * JEMALLOC_IVSALLOC enables ivsalloc(), which verifies that pointers reside
- * within jemalloc-owned extents before dereferencing them.
- */
-#undef JEMALLOC_IVSALLOC
-
 /*
  * If defined, explicitly attempt to more uniformly distribute large allocation
  * pointer alignments across all cache indices.
diff --git a/include/jemalloc/internal/jemalloc_preamble.h.in b/include/jemalloc/internal/jemalloc_preamble.h.in
index 0e2ce3122a..dc21cf4977 100644
--- a/include/jemalloc/internal/jemalloc_preamble.h.in
+++ b/include/jemalloc/internal/jemalloc_preamble.h.in
@@ -132,13 +132,6 @@ static const bool config_xmalloc =
     false
 #endif
     ;
-static const bool config_ivsalloc =
-#ifdef JEMALLOC_IVSALLOC
-    true
-#else
-    false
-#endif
-    ;
 static const bool config_cache_oblivious =
 #ifdef JEMALLOC_CACHE_OBLIVIOUS
     true
@@ -164,5 +157,16 @@ static const bool have_percpu_arena =
     false
 #endif
     ;
+/*
+ * Undocumented, and not recommended; the application should take full
+ * responsibility for tracking provenance.
+ */
+static const bool force_ivsalloc =
+#ifdef JEMALLOC_FORCE_IVSALLOC
+    true
+#else
+    false
+#endif
+    ;
 
 #endif /* JEMALLOC_PREAMBLE_H */
diff --git a/src/jemalloc.c b/src/jemalloc.c
index e08226c97b..27a9fd7b72 100644
--- a/src/jemalloc.c
+++ b/src/jemalloc.c
@@ -2678,12 +2678,14 @@ je_sallocx(const void *ptr, int flags) {
 	tsdn_t *tsdn;
 
 	assert(malloc_initialized() || IS_INITIALIZER);
+	assert(ptr != NULL);
 
 	tsdn = tsdn_fetch();
 	witness_assert_lockless(tsdn);
 
-	if (config_ivsalloc) {
+	if (config_debug || force_ivsalloc) {
 		usize = ivsalloc(tsdn, ptr);
+		assert(force_ivsalloc || usize != 0);
 	} else {
 		usize = isalloc(tsdn, ptr);
 	}
@@ -2885,10 +2887,15 @@ je_malloc_usable_size(JEMALLOC_USABLE_SIZE_CONST void *ptr) {
 	tsdn = tsdn_fetch();
 	witness_assert_lockless(tsdn);
 
-	if (config_ivsalloc) {
-		ret = ivsalloc(tsdn, ptr);
+	if (unlikely(ptr == NULL)) {
+		ret = 0;
 	} else {
-		ret = (ptr == NULL) ? 0 : isalloc(tsdn, ptr);
+		if (config_debug || force_ivsalloc) {
+			ret = ivsalloc(tsdn, ptr);
+			assert(force_ivsalloc || ret != 0);
+		} else {
+			ret = isalloc(tsdn, ptr);
+		}
 	}
 
 	witness_assert_lockless(tsdn);
