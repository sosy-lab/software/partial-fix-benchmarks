Fix creating http request (#1346)

This patch mainly adds `Host` field into the http request header.

IoT.js-DCO-1.0-Signed-off-by: Daeyeon Jeong daeyeon.jeong@samsung.com
Check the given path in process.readSource

The path argument in the process.readSource must point
to an existing file, not a directory.
Fix #1351

IoT.js-DCO-1.0-Signed-off-by: Imre Kiss kissi.szeged@partner.samsung.com
Check the given path in process.readSource

The path argument in the process.readSource must point
to an existing file, not a directory.
Fix #1351

IoT.js-DCO-1.0-Signed-off-by: Imre Kiss kissi.szeged@partner.samsung.com
Check the given path in process.readSource

The path argument in the process.readSource must point
to an existing file, not a directory.
Fix #1351

IoT.js-DCO-1.0-Signed-off-by: Imre Kiss kissi.szeged@partner.samsung.com
Check the given path in process.readSource

The path argument in the process.readSource must point
to an existing file, not a directory.
Fix #1351

IoT.js-DCO-1.0-Signed-off-by: Imre Kiss kissi.szeged@partner.samsung.com
Check the given path in process.readSource

The path argument in the process.readSource must point
to an existing file, not a directory.
Fix #1351

IoT.js-DCO-1.0-Signed-off-by: Imre Kiss kissi.szeged@partner.samsung.com
Check the given path in process.readSource (#1359)

The path argument in the process.readSource must point
to an existing file, not a directory.
Fix #1351

IoT.js-DCO-1.0-Signed-off-by: Imre Kiss kissi.szeged@partner.samsung.com
