diff --git a/docs/api/IoT.js-API-ADC.md b/docs/api/IoT.js-API-ADC.md
index 3d1bf7d7b..775ff7903 100644
--- a/docs/api/IoT.js-API-ADC.md
+++ b/docs/api/IoT.js-API-ADC.md
@@ -5,15 +5,16 @@ The following table shows ADC module APIs available for each platform.
 |  | Linux<br/>(Ubuntu) | Raspbian<br/>(Raspberry Pi) | NuttX<br/>(STM32F4-Discovery) | TizenRT<br/>(Artik053) |
 | :---: | :---: | :---: | :---: | :---: |
 | adc.open | O | X | O | O |
+| adc.openSync | O | X | O | O |
 | adcpin.read | O | X | O | O |
 | adcpin.readSync | O | X | O | O |
 | adcpin.close | O | X | O | O |
 | adcpin.closeSync | O | X | O | O |
 
 
-## Class: ADC
+# ADC
 
-This class allows reading analogue data from hardware pins.
+This module allows reading analogue data from hardware pins.
 
 The hardware pins can be read from or written to, therefore they are called bidirectional IO pins. This module provides the reading part.
 
@@ -21,21 +22,20 @@ On NuttX, you have to know the number of pins that is defined on the target boar
   * [STM32F4-discovery](../targets/nuttx/stm32f4dis/IoT.js-API-Stm32f4dis.md#adc-pin)
 
 
-### new ADC(configuration[, callback])
-
+### adc.open(configuration[, callback])
 * `configuration` {Object}
   * `device` {string} Mandatory configuration on Linux.
-  * `pin` {int} Mandatory configuration on NuttX and TizenRT.
+  * `pin` {number} Mandatory configuration on NuttX and TizenRT.
 * `callback` {Function}
   * `err`: {Error|null}
-* Returns: `AdcPin` {adc.AdcPin}
+* Returns: {Object} An instance of AdcPin.
 
-Opens an ADC pin with the specified configuration.
+Opens an ADC pin with the specified configuration asynchronously.
 
 **Example**
 ```js
-var Adc = require('adc');
-var adc0 = new Adc({
+var adc = require('adc');
+var adc0 = adc.open({
   device: '/sys/devices/12d10000.adc/iio:device0/in_voltage0_raw'
 }, function(err) {
   if (err) {
@@ -44,6 +44,24 @@ var adc0 = new Adc({
 });
 ```
 
+### adc.openSync(configuration)
+* `configuration` {Object}
+  * `device` {string} Mandatory configuration on Linux.
+  * `pin` {number} Mandatory configuration on NuttX and TizenRT.
+* `callback` {Function}
+  * `err`: {Error|null}
+* Returns: {Object} An instance of AdcPin.
+
+Opens an ADC pin with the specified configuration synchronously.
+
+**Example**
+```js
+var adc = require('adc');
+var adc0 = adc.openSync({
+  device: '/sys/devices/12d10000.adc/iio:device0/in_voltage0_raw'
+});
+```
+
 ### adc.read(callback)
 * `callback` {Function}
   * `err`: {Error|null}
@@ -64,7 +82,7 @@ adc0.read(function(err, value) {
 
 
 ### adc.readSync()
-* Returns: `{int}` Analog value.
+* Returns: `{number}` Analog value.
 
 Reads the analog value from the pin synchronously.
 
diff --git a/src/iotjs_binding.c b/src/iotjs_binding.c
index 799a965b0..7741c2eaf 100644
--- a/src/iotjs_binding.c
+++ b/src/iotjs_binding.c
@@ -71,14 +71,6 @@ jerry_value_t iotjs_jval_create_byte_array(uint32_t len, const char* data) {
 }
 
 
-jerry_value_t iotjs_jval_dummy_function(const jerry_value_t function_obj,
-                                        const jerry_value_t this_val,
-                                        const jerry_value_t args_p[],
-                                        const jerry_length_t args_count) {
-  return this_val;
-}
-
-
 jerry_value_t iotjs_jval_create_function(jerry_external_handler_t handler) {
   jerry_value_t jval = jerry_create_external_function(handler);
 
diff --git a/src/iotjs_binding.h b/src/iotjs_binding.h
index 60291746d..586c4ef09 100644
--- a/src/iotjs_binding.h
+++ b/src/iotjs_binding.h
@@ -27,10 +27,6 @@ typedef const jerry_object_native_info_t JNativeInfoType;
 /* Constructors */
 jerry_value_t iotjs_jval_create_string(const iotjs_string_t* v);
 jerry_value_t iotjs_jval_create_byte_array(uint32_t len, const char* data);
-jerry_value_t iotjs_jval_dummy_function(const jerry_value_t function_obj,
-                                        const jerry_value_t this_val,
-                                        const jerry_value_t args_p[],
-                                        const jerry_length_t args_count);
 jerry_value_t iotjs_jval_create_function(jerry_external_handler_t handler);
 jerry_value_t iotjs_jval_create_error_without_error_flag(const char* msg);
 
diff --git a/src/js/adc.js b/src/js/adc.js
index 02a2cb854..20c322bbc 100644
--- a/src/js/adc.js
+++ b/src/js/adc.js
@@ -13,14 +13,16 @@
  * limitations under the License.
  */
 
-function Adc() {
-  if (!(this instanceof Adc)) {
-    return new Adc();
-  }
-}
-
-Adc.prototype.open = function(configuration, callback) {
-  return new native.Adc(configuration, callback);
+var adc = {
+  open: function(config, callback) {
+    var adcPin = new native(config, function(err) {
+      callback(err, adcPin);
+    });
+    return adcPin;
+  },
+  openSync: function(config) {
+    return new native(config);
+  },
 };
 
-module.exports = Adc;
+module.exports = adc;
diff --git a/src/modules/iotjs_module_adc.c b/src/modules/iotjs_module_adc.c
index ba90aa69b..b688a1481 100644
--- a/src/modules/iotjs_module_adc.c
+++ b/src/modules/iotjs_module_adc.c
@@ -20,12 +20,10 @@
 static JNativeInfoType this_module_native_info = {.free_cb = NULL };
 
 
-static iotjs_adc_t* iotjs_adc_instance_from_jval(const jerry_value_t jadc);
-
-
-static iotjs_adc_t* iotjs_adc_create(const jerry_value_t jadc) {
+static iotjs_adc_t* adc_create(const jerry_value_t jadc) {
   iotjs_adc_t* adc = IOTJS_ALLOC(iotjs_adc_t);
   IOTJS_VALIDATED_STRUCT_CONSTRUCTOR(iotjs_adc_t, adc);
+  iotjs_adc_create_platform_data(adc);
   _this->jobject = jadc;
   jerry_set_object_native_pointer(jadc, adc, &this_module_native_info);
 
@@ -33,206 +31,203 @@ static iotjs_adc_t* iotjs_adc_create(const jerry_value_t jadc) {
 }
 
 
-static void iotjs_adc_destroy(iotjs_adc_t* adc) {
-#if defined(__linux__)
-  IOTJS_VALIDATED_STRUCT_DESTRUCTOR(iotjs_adc_t, adc);
-  iotjs_string_destroy(&_this->device);
-#endif
+static void adc_destroy(iotjs_adc_t* adc) {
+  IOTJS_VALIDATED_STRUCT_CONSTRUCTOR(iotjs_adc_t, adc);
+  iotjs_adc_destroy_platform_data(_this->platform_data);
   IOTJS_RELEASE(adc);
 }
 
 
-#define THIS iotjs_adc_reqwrap_t* adc_reqwrap
-
-
-static iotjs_adc_reqwrap_t* iotjs_adc_reqwrap_create(
-    const jerry_value_t jcallback, iotjs_adc_t* adc, AdcOp op) {
+static iotjs_adc_reqwrap_t* adc_reqwrap_create(const jerry_value_t jcallback,
+                                               iotjs_adc_t* adc, AdcOp op) {
   iotjs_adc_reqwrap_t* adc_reqwrap = IOTJS_ALLOC(iotjs_adc_reqwrap_t);
   IOTJS_VALIDATED_STRUCT_CONSTRUCTOR(iotjs_adc_reqwrap_t, adc_reqwrap);
 
   iotjs_reqwrap_initialize(&_this->reqwrap, jcallback, (uv_req_t*)&_this->req);
 
   _this->req_data.op = op;
-  _this->adc_instance = adc;
+  _this->adc_data = adc;
   return adc_reqwrap;
 }
 
-static void iotjs_adc_reqwrap_destroy(THIS) {
+static void adc_reqwrap_destroy(iotjs_adc_reqwrap_t* adc_reqwrap) {
   IOTJS_VALIDATED_STRUCT_DESTRUCTOR(iotjs_adc_reqwrap_t, adc_reqwrap);
   iotjs_reqwrap_destroy(&_this->reqwrap);
   IOTJS_RELEASE(adc_reqwrap);
 }
 
 
-static void iotjs_adc_reqwrap_dispatched(THIS) {
+static void adc_reqwrap_dispatched(iotjs_adc_reqwrap_t* adc_reqwrap) {
   IOTJS_VALIDATABLE_STRUCT_METHOD_VALIDATE(iotjs_adc_reqwrap_t, adc_reqwrap);
-  iotjs_adc_reqwrap_destroy(adc_reqwrap);
+  adc_reqwrap_destroy(adc_reqwrap);
 }
 
 
-static uv_work_t* iotjs_adc_reqwrap_req(THIS) {
+static uv_work_t* adc_reqwrap_req(iotjs_adc_reqwrap_t* adc_reqwrap) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_reqwrap_t, adc_reqwrap);
   return &_this->req;
 }
 
 
-static jerry_value_t iotjs_adc_reqwrap_jcallback(THIS) {
+static jerry_value_t adc_reqwrap_jcallback(iotjs_adc_reqwrap_t* adc_reqwrap) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_reqwrap_t, adc_reqwrap);
   return iotjs_reqwrap_jcallback(&_this->reqwrap);
 }
 
 
-static iotjs_adc_t* iotjs_adc_instance_from_jval(const jerry_value_t jadc) {
+static iotjs_adc_t* adc_instance_from_jval(const jerry_value_t jadc) {
   uintptr_t handle = iotjs_jval_get_object_native_handle(jadc);
   return (iotjs_adc_t*)handle;
 }
 
 
-iotjs_adc_reqwrap_t* iotjs_adc_reqwrap_from_request(uv_work_t* req) {
+static iotjs_adc_reqwrap_t* adc_reqwrap_from_request(uv_work_t* req) {
   return (iotjs_adc_reqwrap_t*)(iotjs_reqwrap_from_request((uv_req_t*)req));
 }
 
 
-iotjs_adc_reqdata_t* iotjs_adc_reqwrap_data(THIS) {
+static iotjs_adc_reqdata_t* adc_reqwrap_data(iotjs_adc_reqwrap_t* adc_reqwrap) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_reqwrap_t, adc_reqwrap);
   return &_this->req_data;
 }
 
 
-iotjs_adc_t* iotjs_adc_instance_from_reqwrap(THIS) {
+static iotjs_adc_t* adc_instance_from_reqwrap(
+    iotjs_adc_reqwrap_t* adc_reqwrap) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_reqwrap_t, adc_reqwrap);
-  return _this->adc_instance;
+  return _this->adc_data;
 }
 
 
-#undef THIS
+static void adc_worker(uv_work_t* work_req) {
+  iotjs_adc_reqwrap_t* req_wrap = adc_reqwrap_from_request(work_req);
+  iotjs_adc_reqdata_t* req_data = adc_reqwrap_data(req_wrap);
+  iotjs_adc_t* adc = adc_instance_from_reqwrap(req_wrap);
+
+  switch (req_data->op) {
+    case kAdcOpOpen:
+      req_data->result = iotjs_adc_open(adc);
+      break;
+    case kAdcOpRead:
+      req_data->result = iotjs_adc_read(adc);
+      break;
+    case kAdcOpClose:
+      req_data->result = iotjs_adc_close(adc);
+      break;
+    default:
+      IOTJS_ASSERT(!"Invalid Adc Operation");
+  }
+}
+
+
+static const char* adc_error_string(uint8_t op) {
+  switch (op) {
+    case kAdcOpClose:
+      return "Close error, cannot close ADC";
+    case kAdcOpOpen:
+      return "Open error, cannot open ADC";
+    case kAdcOpRead:
+      return "Read error, cannot read ADC";
+    default:
+      return "Unknown ADC error";
+  }
+}
 
 
-static void iotjs_adc_after_work(uv_work_t* work_req, int status) {
-  iotjs_adc_reqwrap_t* req_wrap = iotjs_adc_reqwrap_from_request(work_req);
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_reqwrap_t, req_wrap);
+static void adc_after_worker(uv_work_t* work_req, int status) {
+  iotjs_adc_reqwrap_t* req_wrap = adc_reqwrap_from_request(work_req);
+  iotjs_adc_reqdata_t* req_data = adc_reqwrap_data(req_wrap);
 
-  iotjs_adc_reqdata_t* req_data = &_this->req_data;
   iotjs_jargs_t jargs = iotjs_jargs_create(2);
   bool result = req_data->result;
 
   if (status) {
-    iotjs_jargs_append_error(&jargs, "System error");
+    iotjs_jargs_append_error(&jargs, "ADC System Error");
   } else {
     switch (req_data->op) {
       case kAdcOpOpen:
         if (!result) {
-          iotjs_jargs_append_error(&jargs, "Failed to open ADC device");
+          iotjs_jargs_append_error(&jargs, adc_error_string(req_data->op));
         } else {
           iotjs_jargs_append_null(&jargs);
         }
         break;
       case kAdcOpRead:
         if (!result) {
-          iotjs_jargs_append_error(&jargs, "Cannot read from ADC device");
+          iotjs_jargs_append_error(&jargs, adc_error_string(req_data->op));
         } else {
+          iotjs_adc_t* adc = adc_instance_from_reqwrap(req_wrap);
+          IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+
           iotjs_jargs_append_null(&jargs);
-          iotjs_jargs_append_number(&jargs, req_data->value);
+          iotjs_jargs_append_number(&jargs, _this->value);
         }
         break;
       case kAdcOpClose:
         if (!result) {
-          iotjs_jargs_append_error(&jargs, "Cannot close ADC device");
+          iotjs_jargs_append_error(&jargs, adc_error_string(req_data->op));
         } else {
           iotjs_jargs_append_null(&jargs);
         }
         break;
       default: {
-        IOTJS_ASSERT(!"Unreachable");
+        IOTJS_ASSERT(!"ADC after worker failed");
         break;
       }
     }
   }
 
-  const jerry_value_t jcallback = iotjs_adc_reqwrap_jcallback(req_wrap);
-  iotjs_make_callback(jcallback, jerry_create_undefined(), &jargs);
+  const jerry_value_t jcallback = adc_reqwrap_jcallback(req_wrap);
+  if (jerry_value_is_function(jcallback)) {
+    iotjs_make_callback(jcallback, jerry_create_undefined(), &jargs);
+  }
 
   if (req_data->op == kAdcOpClose) {
-    iotjs_adc_destroy(_this->adc_instance);
+    IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_reqwrap_t, req_wrap);
+    adc_destroy(_this->adc_data);
   }
 
   iotjs_jargs_destroy(&jargs);
-  iotjs_adc_reqwrap_dispatched(req_wrap);
-}
-
-
-static void iotjs_adc_read_worker(uv_work_t* work_req) {
-  ADC_WORKER_INIT;
-  int32_t value = iotjs_adc_read(adc);
-
-  if (value < 0) {
-    req_data->result = false;
-    return;
-  }
-
-  req_data->value = value;
-  req_data->result = true;
-}
-
-
-static void iotjs_adc_close_worker(uv_work_t* work_req) {
-  ADC_WORKER_INIT;
-
-  // Release driver
-  if (!iotjs_adc_close(adc)) {
-    req_data->result = false;
-    return;
-  }
-
-  req_data->result = true;
+  adc_reqwrap_dispatched(req_wrap);
 }
 
 
-#define ADC_ASYNC(call, this, jcallback, op)                                   \
-  do {                                                                         \
-    uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());         \
-    iotjs_adc_reqwrap_t* req_wrap =                                            \
-        iotjs_adc_reqwrap_create(jcallback, this, op);                         \
-    uv_work_t* req = iotjs_adc_reqwrap_req(req_wrap);                          \
-    uv_queue_work(loop, req, iotjs_adc_##call##_worker, iotjs_adc_after_work); \
+#define ADC_CALL_ASYNC(op, jcallback)                                       \
+  do {                                                                      \
+    uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());      \
+    iotjs_adc_reqwrap_t* req_wrap = adc_reqwrap_create(jcallback, adc, op); \
+    uv_work_t* req = adc_reqwrap_req(req_wrap);                             \
+    uv_queue_work(loop, req, adc_worker, adc_after_worker);                 \
   } while (0)
 
-JS_FUNCTION(AdcConstructor) {
+
+JS_FUNCTION(AdcCons) {
   DJS_CHECK_THIS();
+  DJS_CHECK_ARGS(1, object);
+  DJS_CHECK_ARG_IF_EXIST(1, function);
 
   // Create ADC object
   const jerry_value_t jadc = JS_GET_THIS();
-  iotjs_adc_t* adc = iotjs_adc_create(jadc);
-  IOTJS_ASSERT(adc == iotjs_adc_instance_from_jval(jadc));
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_t* adc = adc_create(jadc);
+  IOTJS_ASSERT(adc == adc_instance_from_jval(jadc));
+
+  jerry_value_t jconfig;
+  JS_GET_REQUIRED_ARG_VALUE(0, jconfig, IOTJS_MAGIC_STRING_CONFIG, object);
 
-  const jerry_value_t jconfiguration = JS_GET_ARG_IF_EXIST(0, object);
-  if (jerry_value_is_null(jconfiguration)) {
-    return JS_CREATE_ERROR(TYPE,
-                           "Bad arguments - configuration should be Object");
+  jerry_value_t config_res = iotjs_adc_set_platform_config(adc, jconfig);
+  if (jerry_value_has_error_flag(config_res)) {
+    return config_res;
   }
+  IOTJS_ASSERT(jerry_value_is_undefined(config_res));
 
-#if defined(__linux__)
-  DJS_GET_REQUIRED_CONF_VALUE(jconfiguration, _this->device,
-                              IOTJS_MAGIC_STRING_DEVICE, string);
-#elif defined(__NUTTX__) || defined(__TIZENRT__)
-  DJS_GET_REQUIRED_CONF_VALUE(jconfiguration, _this->pin,
-                              IOTJS_MAGIC_STRING_PIN, number);
-#endif
-
-  if (jargc > 1) {
-    const jerry_value_t jcallback = jargv[1];
-    if (jerry_value_is_function(jcallback)) {
-      ADC_ASYNC(open, adc, jcallback, kAdcOpOpen);
-    } else {
-      return JS_CREATE_ERROR(TYPE,
-                             "Bad arguments - callback should be Function");
-    }
-  } else {
-    jerry_value_t jdummycallback =
-        iotjs_jval_create_function(&iotjs_jval_dummy_function);
-    ADC_ASYNC(open, adc, jdummycallback, kAdcOpOpen);
-    jerry_release_value(jdummycallback);
+  jerry_value_t jcallback = JS_GET_ARG_IF_EXIST(1, function);
+
+  // If the callback doesn't exist, it is completed synchronously.
+  // Otherwise, it will be executed asynchronously.
+  if (!jerry_value_is_null(jcallback)) {
+    ADC_CALL_ASYNC(kAdcOpOpen, jcallback);
+  } else if (!iotjs_adc_open(adc)) {
+    return JS_CREATE_ERROR(COMMON, adc_error_string(kAdcOpOpen));
   }
 
   return jerry_create_undefined();
@@ -243,13 +238,7 @@ JS_FUNCTION(Read) {
   JS_DECLARE_THIS_PTR(adc, adc);
   DJS_CHECK_ARG_IF_EXIST(0, function);
 
-  jerry_value_t jcallback = JS_GET_ARG_IF_EXIST(0, function);
-
-  if (jerry_value_is_null(jcallback)) {
-    return JS_CREATE_ERROR(TYPE, "Bad arguments - callback required");
-  } else {
-    ADC_ASYNC(read, adc, jcallback, kAdcOpRead);
-  }
+  ADC_CALL_ASYNC(kAdcOpRead, JS_GET_ARG_IF_EXIST(0, function));
 
   return jerry_create_undefined();
 }
@@ -257,60 +246,49 @@ JS_FUNCTION(Read) {
 JS_FUNCTION(ReadSync) {
   JS_DECLARE_THIS_PTR(adc, adc);
 
-  int32_t value = iotjs_adc_read(adc);
-  if (value < 0) {
-    return JS_CREATE_ERROR(COMMON, "ADC Read Error");
+  if (!iotjs_adc_read(adc)) {
+    return JS_CREATE_ERROR(COMMON, adc_error_string(kAdcOpRead));
   }
 
-  return jerry_create_number(value);
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+
+  return jerry_create_number(_this->value);
 }
 
 JS_FUNCTION(Close) {
   JS_DECLARE_THIS_PTR(adc, adc);
   DJS_CHECK_ARG_IF_EXIST(0, function);
 
-  jerry_value_t jcallback = JS_GET_ARG_IF_EXIST(0, function);
-
-  if (jerry_value_is_null(jcallback)) {
-    jerry_value_t jdummycallback =
-        iotjs_jval_create_function(&iotjs_jval_dummy_function);
-    ADC_ASYNC(close, adc, jdummycallback, kAdcOpClose);
-    jerry_release_value(jdummycallback);
-  } else {
-    ADC_ASYNC(close, adc, jcallback, kAdcOpClose);
-  }
+  ADC_CALL_ASYNC(kAdcOpClose, JS_GET_ARG_IF_EXIST(0, function));
 
-  return jerry_create_null();
+  return jerry_create_undefined();
 }
 
 JS_FUNCTION(CloseSync) {
   JS_DECLARE_THIS_PTR(adc, adc);
 
   bool ret = iotjs_adc_close(adc);
-  iotjs_adc_destroy(adc);
+  adc_destroy(adc);
   if (!ret) {
-    return JS_CREATE_ERROR(COMMON, "ADC Close Error");
+    return JS_CREATE_ERROR(COMMON, adc_error_string(kAdcOpClose));
   }
 
-  return jerry_create_null();
+  return jerry_create_undefined();
 }
 
 jerry_value_t InitAdc() {
-  jerry_value_t jadc = jerry_create_object();
-  jerry_value_t jadcConstructor =
-      jerry_create_external_function(AdcConstructor);
-  iotjs_jval_set_property_jval(jadc, IOTJS_MAGIC_STRING_ADC, jadcConstructor);
-
+  jerry_value_t jadc_cons = jerry_create_external_function(AdcCons);
   jerry_value_t jprototype = jerry_create_object();
+
   iotjs_jval_set_method(jprototype, IOTJS_MAGIC_STRING_READ, Read);
   iotjs_jval_set_method(jprototype, IOTJS_MAGIC_STRING_READSYNC, ReadSync);
   iotjs_jval_set_method(jprototype, IOTJS_MAGIC_STRING_CLOSE, Close);
   iotjs_jval_set_method(jprototype, IOTJS_MAGIC_STRING_CLOSESYNC, CloseSync);
-  iotjs_jval_set_property_jval(jadcConstructor, IOTJS_MAGIC_STRING_PROTOTYPE,
+
+  iotjs_jval_set_property_jval(jadc_cons, IOTJS_MAGIC_STRING_PROTOTYPE,
                                jprototype);
 
   jerry_release_value(jprototype);
-  jerry_release_value(jadcConstructor);
 
-  return jadc;
+  return jadc_cons;
 }
diff --git a/src/modules/iotjs_module_adc.h b/src/modules/iotjs_module_adc.h
index af3492b20..5e0af1a80 100644
--- a/src/modules/iotjs_module_adc.h
+++ b/src/modules/iotjs_module_adc.h
@@ -27,22 +27,18 @@ typedef enum {
   kAdcOpClose,
 } AdcOp;
 
+// Forward declaration of platform data. These are only used by platform code.
+// Generic ADC module never dereferences platform data pointer.
+typedef struct iotjs_adc_platform_data_s iotjs_adc_platform_data_t;
 
 typedef struct {
   jerry_value_t jobject;
-
-#if defined(__linux__)
-  iotjs_string_t device;
-#elif defined(__NUTTX__) || defined(__TIZENRT__)
-  uint32_t pin;
-#endif
-  int32_t device_fd;
+  iotjs_adc_platform_data_t* platform_data;
+  int32_t value;
 } IOTJS_VALIDATED_STRUCT(iotjs_adc_t);
 
 
 typedef struct {
-  int32_t value;
-
   bool result;
   AdcOp op;
 } iotjs_adc_reqdata_t;
@@ -52,29 +48,19 @@ typedef struct {
   iotjs_reqwrap_t reqwrap;
   uv_work_t req;
   iotjs_adc_reqdata_t req_data;
-  iotjs_adc_t* adc_instance;
+  iotjs_adc_t* adc_data;
 } IOTJS_VALIDATED_STRUCT(iotjs_adc_reqwrap_t);
 
 
-#define THIS iotjs_adc_reqwrap_t* adc_reqwrap
-
-iotjs_adc_reqwrap_t* iotjs_adc_reqwrap_from_request(uv_work_t* req);
-iotjs_adc_reqdata_t* iotjs_adc_reqwrap_data(THIS);
-
-iotjs_adc_t* iotjs_adc_instance_from_reqwrap(THIS);
-
-#undef THIS
-
-
-#define ADC_WORKER_INIT                                                     \
-  iotjs_adc_reqwrap_t* req_wrap = iotjs_adc_reqwrap_from_request(work_req); \
-  iotjs_adc_reqdata_t* req_data = iotjs_adc_reqwrap_data(req_wrap);         \
-  iotjs_adc_t* adc = iotjs_adc_instance_from_reqwrap(req_wrap);
-
-
-int32_t iotjs_adc_read(iotjs_adc_t* adc);
+bool iotjs_adc_read(iotjs_adc_t* adc);
 bool iotjs_adc_close(iotjs_adc_t* adc);
-void iotjs_adc_open_worker(uv_work_t* work_req);
-
+bool iotjs_adc_open(iotjs_adc_t* adc);
+
+// Platform-related functions; they are implemented
+// by platform code (i.e.: linux, nuttx, tizen).
+void iotjs_adc_create_platform_data(iotjs_adc_t* adc);
+void iotjs_adc_destroy_platform_data(iotjs_adc_platform_data_t* platform_data);
+jerry_value_t iotjs_adc_set_platform_config(iotjs_adc_t* adc,
+                                            const jerry_value_t jconfig);
 
 #endif /* IOTJS_MODULE_ADC_H */
diff --git a/src/modules/linux/iotjs_module_adc-linux.c b/src/modules/linux/iotjs_module_adc-linux.c
index 930370c5b..ee5ae419a 100644
--- a/src/modules/linux/iotjs_module_adc-linux.c
+++ b/src/modules/linux/iotjs_module_adc-linux.c
@@ -13,8 +13,9 @@
  * limitations under the License.
  */
 
-#ifndef IOTJS_MODULE_ADC_LINUX_GENERAL_INL_H
-#define IOTJS_MODULE_ADC_LINUX_GENERAL_INL_H
+#if !defined(__linux__)
+#error "Module __FILE__ is for Linux only"
+#endif
 
 #include <stdio.h>
 #include <stdlib.h>
@@ -24,28 +25,57 @@
 #include "iotjs_systemio-linux.h"
 #include "modules/iotjs_module_adc.h"
 
-
 #define ADC_PIN_FORMAT ADC_INTERFACE ADC_PIN_INTERFACE
-
 #define ADC_PATH_BUFFER_SIZE DEVICE_IO_PATH_BUFFER_SIZE
 #define ADC_PIN_BUFFER_SIZE DEVICE_IO_PIN_BUFFER_SIZE
 #define ADC_VALUE_BUFFER_SIZE 64
 
+struct iotjs_adc_platform_data_s {
+  iotjs_string_t device;
+};
+
+
+void iotjs_adc_create_platform_data(iotjs_adc_t* adc) {
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  _this->platform_data = IOTJS_ALLOC(iotjs_adc_platform_data_t);
+}
+
+
+void iotjs_adc_destroy_platform_data(iotjs_adc_platform_data_t* platform_data) {
+  iotjs_string_destroy(&platform_data->device);
+  IOTJS_RELEASE(platform_data);
+}
+
+
+jerry_value_t iotjs_adc_set_platform_config(iotjs_adc_t* adc,
+                                            const jerry_value_t jconfig) {
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
+
+  DJS_GET_REQUIRED_CONF_VALUE(jconfig, platform_data->device,
+                              IOTJS_MAGIC_STRING_DEVICE, string);
+
+  return jerry_create_undefined();
+}
+
 
 // Implementation used here are based on:
 //  https://www.kernel.org/doc/Documentation/adc/sysfs.txt
 
-int32_t iotjs_adc_read(iotjs_adc_t* adc) {
+bool iotjs_adc_read(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
 
-  const char* device_path = iotjs_string_data(&_this->device);
+  const char* device_path = iotjs_string_data(&platform_data->device);
   char buffer[ADC_VALUE_BUFFER_SIZE];
 
   if (!iotjs_systemio_open_read_close(device_path, buffer, sizeof(buffer))) {
-    return -1;
+    return false;
   }
 
-  return atoi(buffer);
+  _this->value = atoi(buffer) != 0;
+
+  return true;
 }
 
 
@@ -53,16 +83,17 @@ bool iotjs_adc_close(iotjs_adc_t* adc) {
   return true;
 }
 
-void iotjs_adc_open_worker(uv_work_t* work_req) {
-  ADC_WORKER_INIT;
+bool iotjs_adc_open(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
 
   DDDLOG("%s()", __func__);
-  const char* device_path = iotjs_string_data(&_this->device);
+  const char* device_path = iotjs_string_data(&platform_data->device);
 
   // Check if ADC interface exists.
-  req_data->result = iotjs_systemio_check_path(device_path);
-}
-
+  if (!iotjs_systemio_check_path(device_path)) {
+    return false;
+  }
 
-#endif /* IOTJS_MODULE_ADC_LINUX_GENERAL_INL_H */
+  return true;
+}
diff --git a/src/modules/nuttx/iotjs_module_adc-nuttx.c b/src/modules/nuttx/iotjs_module_adc-nuttx.c
index 4397c0513..deec08d86 100644
--- a/src/modules/nuttx/iotjs_module_adc-nuttx.c
+++ b/src/modules/nuttx/iotjs_module_adc-nuttx.c
@@ -13,8 +13,9 @@
  * limitations under the License.
  */
 
-#if defined(__NUTTX__)
-
+#if !defined(__NUTTX__)
+#error "Module __FILE__ is for NuttX only"
+#endif
 
 #include <uv.h>
 #include <nuttx/analog/adc.h>
@@ -25,22 +26,48 @@
 #include "modules/iotjs_module_adc.h"
 #include "modules/iotjs_module_stm32f4dis.h"
 
-
 #define ADC_DEVICE_PATH_FORMAT "/dev/adc%d"
 #define ADC_DEVICE_PATH_BUFFER_SIZE 12
 
+struct iotjs_adc_platform_data_s {
+  uint32_t pin;
+};
+
+void iotjs_adc_create_platform_data(iotjs_adc_t* adc) {
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  _this->platform_data = IOTJS_ALLOC(iotjs_adc_platform_data_t);
+  _this->platform_data->pin = 0;
+}
+
+
+void iotjs_adc_destroy_platform_data(iotjs_adc_platform_data_t* platform_data) {
+  IOTJS_RELEASE(platform_data);
+}
+
+
+jerry_value_t iotjs_adc_set_platform_config(iotjs_adc_t* adc,
+                                            const jerry_value_t jconfig) {
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
+
+  DJS_GET_REQUIRED_CONF_VALUE(jconfig, platform_data->pin,
+                              IOTJS_MAGIC_STRING_PIN, number);
+
+  return jerry_create_undefined();
+}
+
 
-static void iotjs_adc_get_path(char* buffer, int32_t number) {
+static void adc_get_path(char* buffer, int32_t number) {
   // Create ADC device path
   snprintf(buffer, ADC_DEVICE_PATH_BUFFER_SIZE - 1, ADC_DEVICE_PATH_FORMAT,
            number);
 }
 
 
-static bool iotjs_adc_read_data(uint32_t pin, struct adc_msg_s* msg) {
+static bool adc_read_data(uint32_t pin, struct adc_msg_s* msg) {
   int32_t adc_number = ADC_GET_NUMBER(pin);
   char path[ADC_DEVICE_PATH_BUFFER_SIZE] = { 0 };
-  iotjs_adc_get_path(path, adc_number);
+  adc_get_path(path, adc_number);
 
   const iotjs_environment_t* env = iotjs_environment_get();
   uv_loop_t* loop = iotjs_environment_loop(env);
@@ -74,27 +101,31 @@ static bool iotjs_adc_read_data(uint32_t pin, struct adc_msg_s* msg) {
 }
 
 
-int32_t iotjs_adc_read(iotjs_adc_t* adc) {
+bool iotjs_adc_read(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
 
   struct adc_msg_s msg;
 
-  if (!iotjs_adc_read_data(_this->pin, &msg)) {
-    return -1;
+  if (!adc_read_data(platform_data->pin, &msg)) {
+    return false;
   }
 
-  return msg.am_data;
+  _this->value = msg.am_data;
+
+  return true;
 }
 
 
 bool iotjs_adc_close(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
 
-  uint32_t pin = _this->pin;
+  uint32_t pin = platform_data->pin;
   int32_t adc_number = ADC_GET_NUMBER(pin);
 
   char path[ADC_DEVICE_PATH_BUFFER_SIZE] = { 0 };
-  iotjs_adc_get_path(path, adc_number);
+  adc_get_path(path, adc_number);
 
   // Release driver
   if (unregister_driver(path) < 0) {
@@ -107,28 +138,24 @@ bool iotjs_adc_close(iotjs_adc_t* adc) {
 }
 
 
-void iotjs_adc_open_worker(uv_work_t* work_req) {
-  ADC_WORKER_INIT;
+bool iotjs_adc_open(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
 
-  uint32_t pin = _this->pin;
+  uint32_t pin = platform_data->pin;
   int32_t adc_number = ADC_GET_NUMBER(pin);
   int32_t timer = SYSIO_GET_TIMER(pin);
   struct adc_dev_s* adc_dev = iotjs_adc_config_nuttx(adc_number, timer, pin);
 
   char path[ADC_DEVICE_PATH_BUFFER_SIZE] = { 0 };
-  iotjs_adc_get_path(path, adc_number);
+  adc_get_path(path, adc_number);
 
   if (adc_register(path, adc_dev) != 0) {
-    req_data->result = false;
-    return;
+    return false;
   }
 
   DDDLOG("%s - path: %s, number: %d, timer: %d", __func__, path, adc_number,
          timer);
 
-  req_data->result = true;
+  return true;
 }
-
-
-#endif // __NUTTX__
diff --git a/src/modules/tizenrt/iotjs_module_adc-tizenrt.c b/src/modules/tizenrt/iotjs_module_adc-tizenrt.c
index 35a8b1374..2b0fdeb21 100644
--- a/src/modules/tizenrt/iotjs_module_adc-tizenrt.c
+++ b/src/modules/tizenrt/iotjs_module_adc-tizenrt.c
@@ -13,7 +13,9 @@
  * limitations under the License.
  */
 
-#if defined(__TIZENRT__)
+#if !defined(__TIZENRT__)
+#error "Module __FILE__ is for TizenRT only"
+#endif
 
 #include <errno.h>
 #include <fcntl.h>
@@ -27,6 +29,11 @@
 
 #define S5J_ADC_MAX_CHANNELS 4
 
+struct iotjs_adc_platform_data_s {
+  int32_t device_fd;
+  uint32_t pin;
+};
+
 // There is one file for all ADC inputs so wee need one common file descriptor
 static int32_t device_fd;
 // This is simple ref counter. Each time ADC is opened, it is increased.
@@ -34,28 +41,59 @@ static size_t device_fd_counter = 0;
 // Path of ADC device.
 #define TIZENRT_ADC_DEVICE "/dev/adc0"
 
-int32_t iotjs_adc_read(iotjs_adc_t* adc) {
+void iotjs_adc_create_platform_data(iotjs_adc_t* adc) {
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  _this->platform_data = IOTJS_ALLOC(iotjs_adc_platform_data_t);
+  _this->platform_data->device_fd = -1;
+  _this->platform_data->pin = 0;
+}
+
+
+void iotjs_adc_destroy_platform_data(iotjs_adc_platform_data_t* platform_data) {
+  IOTJS_RELEASE(platform_data);
+}
+
+
+jerry_value_t iotjs_adc_set_platform_config(iotjs_adc_t* adc,
+                                            const jerry_value_t jconfig) {
+  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
+
+  DJS_GET_REQUIRED_CONF_VALUE(jconfig, platform_data->pin,
+                              IOTJS_MAGIC_STRING_PIN, number);
+
+  return jerry_create_undefined();
+}
+
+
+bool iotjs_adc_read(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
+
   int ret, nbytes;
   size_t readsize, i, nsamples;
   struct adc_msg_s samples[S5J_ADC_MAX_CHANNELS];
-  ret = ioctl(_this->device_fd, ANIOC_TRIGGER, 0);
+  ret = ioctl(platform_data->device_fd, ANIOC_TRIGGER, 0);
+
   if (ret < 0) {
-    return -1;
+    _this->value = -1;
+    return false;
   }
+
   readsize = sizeof(samples);
   while (true) {
-    nbytes = read(_this->device_fd, samples, readsize);
+    nbytes = read(platform_data->device_fd, samples, readsize);
     if (nbytes > 0) {
       nsamples = (size_t)nbytes / sizeof(struct adc_msg_s);
       int sample = -1;
       for (i = 0; i < nsamples; ++i) {
-        if (_this->pin == samples[i].am_channel) {
+        if (platform_data->pin == samples[i].am_channel) {
           sample = samples[i].am_data;
         }
       }
-      if (-1 != sample) {
-        return sample;
+      if (sample != -1) {
+        _this->value = sample;
+        return true;
       }
     } /* else {
       // The read function is blocking but there are events,
@@ -66,31 +104,37 @@ int32_t iotjs_adc_read(iotjs_adc_t* adc) {
   }
 }
 
+
 bool iotjs_adc_close(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
-  if (_this->device_fd > 0) {
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
+
+  if (platform_data->device_fd > 0) {
     device_fd_counter--;
   }
-  if (0 == device_fd_counter) {
-    close(_this->device_fd);
+
+  if (device_fd_counter == 0) {
+    close(platform_data->device_fd);
   }
+
   return true;
 }
 
 
-void iotjs_adc_open_worker(uv_work_t* work_req) {
-  ADC_WORKER_INIT;
+bool iotjs_adc_open(iotjs_adc_t* adc) {
   IOTJS_VALIDATED_STRUCT_METHOD(iotjs_adc_t, adc);
-  if (0 == device_fd_counter) {
+  iotjs_adc_platform_data_t* platform_data = _this->platform_data;
+
+  if (device_fd_counter == 0) {
     device_fd = open(TIZENRT_ADC_DEVICE, O_RDONLY);
   }
-  _this->device_fd = device_fd;
-  if (_this->device_fd < 0) {
-    req_data->result = false;
-    return;
+
+  platform_data->device_fd = device_fd;
+  if (platform_data->device_fd < 0) {
+    return false;
   }
+
   device_fd_counter++;
-  req_data->result = true;
-}
 
-#endif // __TIZENRT__
+  return true;
+}
diff --git a/test/run_pass/test_adc.js b/test/run_pass/test_adc.js
index 26b8ecd6d..be85b6b1c 100644
--- a/test/run_pass/test_adc.js
+++ b/test/run_pass/test_adc.js
@@ -14,9 +14,8 @@
  */
 
 
-var Adc = require('adc');
+var adc = require('adc');
 var assert = require('assert');
-var adc = new Adc();
 var configuration = {};
 
 if (process.platform === 'linux') {
@@ -27,18 +26,18 @@ if (process.platform === 'linux') {
 } else if (process.platform === 'tizenrt') {
   configuration.pin = 0;
 } else {
-  assert.fail();
+  assert.assert(false, "Unsupported platform: " + process.platform);
 }
 
+// start async test
 asyncTest();
 
-// read async test
 function asyncTest() {
   var adc0 = adc.open(configuration, function(err) {
     console.log('ADC initialized');
 
     if (err) {
-      assert.fail();
+      assert.assert(false, "Failed to open device.");
     }
 
     var loopCnt = 5;
@@ -48,13 +47,16 @@ function asyncTest() {
       if (--loopCnt < 0) {
         console.log('test1 complete');
         clearInterval(test1Loop);
-        adc0.closeSync();
-        syncTestst();
+        adc0.close(function (err) {
+          assert.equal(err, null);
+
+          // start sync test
+          syncTest();
+        });
       } else {
         adc0.read(function(err, value) {
           if (err) {
-            console.log('read failed');
-            assert.fail();
+            assert.assert(false, "Failed to read device.");
           }
 
           console.log(value);
@@ -64,33 +66,26 @@ function asyncTest() {
   });
 }
 
-// read sync test
-function syncTestst() {
-  var adc0 = adc.open(configuration, function(err) {
-    console.log('ADC initialized');
-
-    if (err) {
-      assert.fail();
-    }
+function syncTest() {
+  var adc0 = adc.openSync(configuration);
+  console.log('ADC initialized');
 
-    var loopCnt = 5,
-        value = -1;
+  var loopCnt = 5,
+      value = -1;
 
-    console.log('test2 start(read sync test)');
-    var test2Loop = setInterval(function() {
-      if (--loopCnt < 0) {
-        console.log('test2 complete');
-        clearInterval(test2Loop);
-        adc0.close();
+  console.log('test2 start(read sync test)');
+  var test2Loop = setInterval(function() {
+    if (--loopCnt < 0) {
+      console.log('test2 complete');
+      clearInterval(test2Loop);
+      adc0.closeSync();
+    } else {
+      value = adc0.readSync();
+      if (value < 0) {
+        assert.assert(false, "Failed to read device.");
       } else {
-        value = adc0.readSync();
-        if (value < 0) {
-          console.log('read failed');
-          assert.fail();
-        } else {
-          console.log(value);
-        }
+        console.log(value);
       }
-    }, 1000);
-  });
+    }
+  }, 1000);
 }
