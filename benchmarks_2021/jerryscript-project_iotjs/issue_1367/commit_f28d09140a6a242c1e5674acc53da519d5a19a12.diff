diff --git a/docs/api/IoT.js-API-UART.md b/docs/api/IoT.js-API-UART.md
index f7b8825b5..6526da311 100644
--- a/docs/api/IoT.js-API-UART.md
+++ b/docs/api/IoT.js-API-UART.md
@@ -4,21 +4,18 @@ The following shows uart module APIs available for each platform.
 
 |  | Linux<br/>(Ubuntu) | Raspbian<br/>(Raspberry Pi) | NuttX<br/>(STM32F4-Discovery) | TizenRT<br/>(Artik053) |
 | :---: | :---: | :---: | :---: | :---: |
-| uart.open | O | O | O | O |
-| uartport.write | O | O | O | O |
-| uartport.writeSync | O | O | O | O |
-| uartport.close | O | O | X | O |
-| uartport.closeSync | O | O | X | O |
+| uart.open             | O | O | O | O |
+| uart.openSync         | O | O | O | O |
+| uartport.write        | O | O | O | O |
+| uartport.writeSync    | O | O | O | O |
+| uartport.close        | O | O | X | O |
+| uartport.closeSync    | O | O | X | O |
 
 ## Class: UART
 
 The UART (Universal Asynchronous Receiver/Transmitter) class supports asynchronous serial communication.
 
-### new UART()
-
-Returns with a new UART object.
-
-### uart.open(configuration[, callback])
+### uart.open(configuration, callback)
 * `configuration` {Object}
   * `device` {string} Mandatory configuration.
   * `baudRate` {number} Specifies how fast data is sent over a serial line. **Default:** `9600`.
@@ -29,7 +26,7 @@ Returns with a new UART object.
 
 Opens an UARTPort object with the specified configuration.
 
-The `baudRate` must be equal to one of these values: [0, 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400].
+The `baudRate` must be equal to one of these values: [50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400, 4800, 9600, 19200, 38400, 57600, 115200, 230400].
 
 The `dataBits` must be equal to one of these values: [5, 6, 7, 8].
 
@@ -40,10 +37,8 @@ You can read more information about the usage of the UART on stm32f4-discovery b
 **Example**
 
 ```js
+var uart = require('uart');
 
-var Uart = require('uart');
-
-var uart = new Uart();
 var configuration = {
   device: '/dev/ttyUSB0'
   baudRate: 115200,
@@ -54,12 +49,40 @@ var serial = uart.open(configuration, function(err) {
   // Do something.
 });
 
+serial.closeSync();
+
+```
+
+### uart.openSync(configuration)
+* `configuration` {Object}
+  * `device` {string} Mandatory configuration.
+  * `baudRate` {number} Specifies how fast data is sent over a serial line. **Default:** `9600`.
+  * `dataBits` {number} Number of data bits that are being transmitted. **Default:** `8`.
+* Returns: {UARTPort}.
+
+Opens an UARTPort object with the specified configuration.
+
+**Example**
+
+```js
+var uart = require('uart');
+
+var configuration = {
+  device: '/dev/ttyUSB0'
+  baudRate: 115200,
+  dataBits: 8,
+}
+
+var serial = uart.openSync(configuration);
+
+serial.closeSync();
+
 ```
 
 ## Class: UARTPort
 The UARTPort class is responsible for transmitting and receiving serial data.
 
-### uartport.write(data[, callback]).
+### uartport.write(data, callback).
 * `data` {string}.
 * `callback` {Function}.
   * `err` {Error|null}.
@@ -69,12 +92,13 @@ Writes the given `data` to the UART device asynchronously.
 **Example**
 
 ```js
+var serial = uart.openSync({device: '/dev/ttyUSB0'});
 
 serial.write('Hello?', function(err) {
   if (err) {
     // Do something.
   }
-  serial.close();
+  serial.closeSync();
 });
 
 ```
@@ -87,10 +111,9 @@ Writes the given `data` to the UART device synchronously.
 **Example**
 
 ```js
-
+var serial = uart.openSync({device: '/dev/ttyUSB0'});
 serial.writeSync('Hello?');
-
-serial.close();
+serial.closeSync();
 
 ```
 
diff --git a/src/js/uart.js b/src/js/uart.js
index 0a7689735..4493553a1 100644
--- a/src/js/uart.js
+++ b/src/js/uart.js
@@ -14,126 +14,23 @@
  */
 
 var EventEmitter = require('events').EventEmitter;
-var util = require('util');
 
-// VALIDATION ARRAYS
-var BAUDRATE = [0, 50, 75, 110, 134, 150, 200, 300, 600, 1200, 1800, 2400,
-                 4800, 9600, 19200, 38400, 57600, 115200, 230400];
-var DATABITS = [5, 6, 7, 8];
-
-var defaultConfiguration = {
-  baudRate: 9600,
-  dataBits: 8,
-};
-
-
-function Uart() {
-  if (!(this instanceof Uart)) {
-    return new Uart();
-  }
-}
-
-Uart.prototype.open = function(configuration, callback) {
-  return uartPortOpen(configuration, callback);
-};
-
-
-function uartPortOpen(configuration, callback) {
-  var _binding = null;
-
-  function UartPort(configuration, callback) { // constructor
-    var self = this;
-
-    if (util.isObject(configuration)) {
-      if (!util.isString(configuration.device)) {
-        throw new TypeError(
-          'Bad configuration - device is mandatory and should be String');
-      }
-    } else {
-      throw new TypeError('Bad arguments - configuration should be Object');
+var uart = {
+  open: function(config, callback) {
+    for(var prop in EventEmitter.prototype) {
+        native.prototype[prop] = EventEmitter.prototype[prop];
     }
-
-    // validate baud rate
-    if (configuration.baudRate !== undefined) {
-      if (BAUDRATE.indexOf(configuration.baudRate) === -1) {
-        throw new TypeError('Invalid \'baudRate\': ' + configuration.baudRate);
-      }
-    } else {
-      configuration.baudRate = defaultConfiguration.baudRate;
-    }
-
-    // validate data bits
-    if (configuration.dataBits !== undefined) {
-      if (DATABITS.indexOf(configuration.dataBits) === -1) {
-        throw new TypeError('Invalid \'databits\': ' + configuration.dataBits);
-      }
-    } else {
-      configuration.dataBits = defaultConfiguration.dataBits;
-    }
-
-    EventEmitter.call(this);
-
-    _binding = new native(configuration, this, function(err) {
-      util.isFunction(callback) && callback.call(self, err);
+    var uartPort = new native(config, function(err) {
+      callback(err);
     });
-
-    process.on('exit', (function(self) {
-      return function() {
-        if (_binding !== null) {
-          self.closeSync();
-        }
-      };
-    })(this));
-  }
-
-  util.inherits(UartPort, EventEmitter);
-
-  UartPort.prototype.write = function(buffer, callback) {
-    var self = this;
-
-    if (_binding === null) {
-      throw new Error('UART port is not opened');
-    }
-
-    _binding.write(buffer, function(err) {
-      util.isFunction(callback) && callback.call(self, err);
-    });
-  };
-
-  UartPort.prototype.writeSync = function(buffer) {
-    var self = this;
-
-    if (_binding === null) {
-      throw new Error('UART port is not opened');
-    }
-
-    _binding.write(buffer);
-  };
-
-  UartPort.prototype.close = function(callback) {
-    var self = this;
-
-    if (_binding === null) {
-      throw new Error('UART port is not opened');
-    }
-
-    _binding.close(function(err) {
-      util.isFunction(callback) && callback.call(self, err);
-      _binding = null;
-    });
-  };
-
-  UartPort.prototype.closeSync = function() {
-    if (_binding === null) {
-      throw new Error('UART port is not opened');
+    return uartPort;
+  },
+  openSync: function(config) {
+    for(var prop in EventEmitter.prototype) {
+        native.prototype[prop] = EventEmitter.prototype[prop];
     }
+    return new native(config);
+  },
+};
 
-    _binding.close();
-    _binding = null;
-  };
-
-  return new UartPort(configuration, callback);
-}
-
-
-module.exports = Uart;
+module.exports = uart;
diff --git a/src/modules.json b/src/modules.json
index b7347d844..b9000108a 100644
--- a/src/modules.json
+++ b/src/modules.json
@@ -327,7 +327,7 @@
       "native_files": ["modules/iotjs_module_uart.c"],
       "init": "InitUart",
       "js_file": "js/uart.js",
-      "require": ["events", "util"]
+      "require": ["events"]
     },
     "udp": {
       "native_files": ["modules/iotjs_module_udp.c"],
diff --git a/src/modules/iotjs_module_uart.c b/src/modules/iotjs_module_uart.c
index 6a9c4816e..f468bd6b3 100644
--- a/src/modules/iotjs_module_uart.c
+++ b/src/modules/iotjs_module_uart.c
@@ -21,139 +21,78 @@
 
 IOTJS_DEFINE_NATIVE_HANDLE_INFO_THIS_MODULE(uart);
 
-static iotjs_uart_t* iotjs_uart_create(jerry_value_t juart) {
+static iotjs_uart_t* uart_create(const jerry_value_t juart) {
   iotjs_uart_t* uart = IOTJS_ALLOC(iotjs_uart_t);
-  IOTJS_VALIDATED_STRUCT_CONSTRUCTOR(iotjs_uart_t, uart);
-
-  iotjs_handlewrap_initialize(&_this->handlewrap, juart,
-                              (uv_handle_t*)(&_this->poll_handle),
+  iotjs_handlewrap_initialize(&uart->handlewrap, juart,
+                              (uv_handle_t*)(&uart->poll_handle),
                               &this_module_native_info);
 
-  _this->device_fd = -1;
+  uart->device_fd = -1;
   return uart;
 }
 
 static void iotjs_uart_destroy(iotjs_uart_t* uart) {
-  IOTJS_VALIDATED_STRUCT_DESTRUCTOR(iotjs_uart_t, uart);
-  iotjs_handlewrap_destroy(&_this->handlewrap);
-  iotjs_string_destroy(&_this->device_path);
+  iotjs_handlewrap_destroy(&uart->handlewrap);
+  iotjs_string_destroy(&uart->device_path);
   IOTJS_RELEASE(uart);
 }
 
-
-#define THIS iotjs_uart_reqwrap_t* uart_reqwrap
-
-
-static iotjs_uart_reqwrap_t* iotjs_uart_reqwrap_create(jerry_value_t jcallback,
-                                                       iotjs_uart_t* uart,
-                                                       UartOp op) {
+static iotjs_uart_reqwrap_t* uart_reqwrap_create(jerry_value_t jcallback,
+                                                 iotjs_uart_t* uart,
+                                                 UartOp op) {
   iotjs_uart_reqwrap_t* uart_reqwrap = IOTJS_ALLOC(iotjs_uart_reqwrap_t);
-  IOTJS_VALIDATED_STRUCT_CONSTRUCTOR(iotjs_uart_reqwrap_t, uart_reqwrap);
 
-  iotjs_reqwrap_initialize(&_this->reqwrap, jcallback, (uv_req_t*)&_this->req);
+  iotjs_reqwrap_initialize(&uart_reqwrap->reqwrap, jcallback,
+                           (uv_req_t*)&uart_reqwrap->req);
 
-  _this->req_data.op = op;
-  _this->uart_instance = uart;
+  uart_reqwrap->req_data.op = op;
+  uart_reqwrap->uart_data = uart;
 
   return uart_reqwrap;
 }
 
-
-static void iotjs_uart_reqwrap_destroy(THIS) {
-  IOTJS_VALIDATED_STRUCT_DESTRUCTOR(iotjs_uart_reqwrap_t, uart_reqwrap);
-  iotjs_reqwrap_destroy(&_this->reqwrap);
+static void uart_reqwrap_destroy(iotjs_uart_reqwrap_t* uart_reqwrap) {
+  iotjs_reqwrap_destroy(&uart_reqwrap->reqwrap);
   IOTJS_RELEASE(uart_reqwrap);
 }
 
-
-static void iotjs_uart_reqwrap_dispatched(THIS) {
-  IOTJS_VALIDATABLE_STRUCT_METHOD_VALIDATE(iotjs_uart_reqwrap_t, uart_reqwrap);
-  iotjs_uart_reqwrap_destroy(uart_reqwrap);
-}
-
-
-static uv_work_t* iotjs_uart_reqwrap_req(THIS) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_reqwrap_t, uart_reqwrap);
-  return &_this->req;
-}
-
-
-static jerry_value_t iotjs_uart_reqwrap_jcallback(THIS) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_reqwrap_t, uart_reqwrap);
-  return iotjs_reqwrap_jcallback(&_this->reqwrap);
-}
-
-
-static iotjs_uart_t* iotjs_uart_instance_from_jval(jerry_value_t juart) {
-  iotjs_handlewrap_t* handlewrap = iotjs_handlewrap_from_jobject(juart);
-  return (iotjs_uart_t*)handlewrap;
-}
-
-
 iotjs_uart_reqwrap_t* iotjs_uart_reqwrap_from_request(uv_work_t* req) {
   return (iotjs_uart_reqwrap_t*)(iotjs_reqwrap_from_request((uv_req_t*)req));
 }
 
-
-iotjs_uart_reqdata_t* iotjs_uart_reqwrap_data(THIS) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_reqwrap_t, uart_reqwrap);
-  return &_this->req_data;
+iotjs_uart_reqdata_t* iotjs_uart_reqwrap_data(
+    iotjs_uart_reqwrap_t* uart_reqwrap) {
+  return &uart_reqwrap->req_data;
 }
 
-
-iotjs_uart_t* iotjs_uart_instance_from_reqwrap(THIS) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_reqwrap_t, uart_reqwrap);
-  return _this->uart_instance;
+iotjs_uart_t* iotjs_uart_instance_from_reqwrap(
+    iotjs_uart_reqwrap_t* uart_reqwrap) {
+  return uart_reqwrap->uart_data;
 }
 
-
-#undef THIS
-
-
 static void handlewrap_close_callback(uv_handle_t* handle) {
   iotjs_uart_t* uart = (iotjs_uart_t*)handle->data;
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
 
-  if (close(_this->device_fd) < 0) {
+  if (close(uart->device_fd) < 0) {
     DLOG("UART Close Error");
     IOTJS_ASSERT(0);
   }
 }
 
-static bool iotjs_uart_close(iotjs_uart_t* uart) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-
-  iotjs_handlewrap_close(&_this->handlewrap, handlewrap_close_callback);
-
-  return true;
-}
-
-
-static void iotjs_uart_write_worker(uv_work_t* work_req) {
-  UART_WORKER_INIT;
-
-  if (!iotjs_uart_write(uart)) {
-    req_data->result = false;
-    return;
-  }
-
-  req_data->result = true;
-}
-
-
-static void iotjs_uart_close_worker(uv_work_t* work_req) {
-  UART_WORKER_INIT;
-
-  if (!iotjs_uart_close(uart)) {
-    req_data->result = false;
-    return;
+static const char* uart_error_str(uint8_t op) {
+  switch (op) {
+    case kUartOpClose:
+      return "Close error, failed to close UART device";
+    case kUartOpOpen:
+      return "Open error, failed to open UART device";
+    case kUartOpWrite:
+      return "Write error, cannot write to UART device";
+    default:
+      return "Unknown error";
   }
-
-  req_data->result = true;
 }
 
-
-static void iotjs_uart_after_worker(uv_work_t* work_req, int status) {
+static void uart_after_worker(uv_work_t* work_req, int status) {
   iotjs_uart_reqwrap_t* req_wrap = iotjs_uart_reqwrap_from_request(work_req);
   iotjs_uart_reqdata_t* req_data = iotjs_uart_reqwrap_data(req_wrap);
 
@@ -163,30 +102,15 @@ static void iotjs_uart_after_worker(uv_work_t* work_req, int status) {
     iotjs_jargs_append_error(&jargs, "System error");
   } else {
     switch (req_data->op) {
-      case kUartOpOpen: {
-        if (!req_data->result) {
-          iotjs_jargs_append_error(&jargs, "Failed to open UART device");
-        } else {
-          iotjs_jargs_append_null(&jargs);
-        }
-        break;
-      }
       case kUartOpWrite: {
         iotjs_uart_t* uart = iotjs_uart_instance_from_reqwrap(req_wrap);
-        IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-
-        iotjs_string_destroy(&_this->buf_data);
-
-        if (!req_data->result) {
-          iotjs_jargs_append_error(&jargs, "Cannot write to device");
-        } else {
-          iotjs_jargs_append_null(&jargs);
-        }
-        break;
+        iotjs_string_destroy(&uart->buf_data);
+        /* FALLTHRU */
       }
-      case kUartOpClose: {
+      case kUartOpClose:
+      case kUartOpOpen: {
         if (!req_data->result) {
-          iotjs_jargs_append_error(&jargs, "Failed to close UART device");
+          iotjs_jargs_append_error(&jargs, uart_error_str(req_data->op));
         } else {
           iotjs_jargs_append_null(&jargs);
         }
@@ -199,159 +123,231 @@ static void iotjs_uart_after_worker(uv_work_t* work_req, int status) {
     }
   }
 
-  jerry_value_t jcallback = iotjs_uart_reqwrap_jcallback(req_wrap);
-  iotjs_make_callback(jcallback, jerry_create_undefined(), &jargs);
+  jerry_value_t jcallback = iotjs_reqwrap_jcallback(&req_wrap->reqwrap);
+  if (jerry_value_is_function(jcallback)) {
+    iotjs_make_callback(jcallback, jerry_create_undefined(), &jargs);
+  }
 
   iotjs_jargs_destroy(&jargs);
-  iotjs_uart_reqwrap_dispatched(req_wrap);
+  uart_reqwrap_destroy(req_wrap);
 }
 
-
-static void iotjs_uart_onread(jerry_value_t jthis, char* buf) {
-  jerry_value_t jemit = iotjs_jval_get_property(jthis, "emit");
-  IOTJS_ASSERT(jerry_value_is_function(jemit));
-
-  iotjs_jargs_t jargs = iotjs_jargs_create(2);
-  jerry_value_t str = jerry_create_string((const jerry_char_t*)"data");
-  jerry_value_t data = jerry_create_string((const jerry_char_t*)buf);
-  iotjs_jargs_append_jval(&jargs, str);
-  iotjs_jargs_append_jval(&jargs, data);
-  iotjs_jhelper_call_ok(jemit, jthis, &jargs);
-
-  jerry_release_value(str);
-  jerry_release_value(data);
-  iotjs_jargs_destroy(&jargs);
-  jerry_release_value(jemit);
+static void uart_worker(uv_work_t* work_req) {
+  iotjs_uart_reqwrap_t* req_wrap = iotjs_uart_reqwrap_from_request(work_req);
+  iotjs_uart_reqdata_t* req_data = iotjs_uart_reqwrap_data(req_wrap);
+  iotjs_uart_t* uart = iotjs_uart_instance_from_reqwrap(req_wrap);
+
+  switch (req_data->op) {
+    case kUartOpOpen:
+      req_data->result = iotjs_uart_open(uart);
+      break;
+    case kUartOpWrite:
+      req_data->result = iotjs_uart_write(uart);
+      break;
+    case kUartOpClose:
+      iotjs_handlewrap_close(&uart->handlewrap, handlewrap_close_callback);
+      req_data->result = true;
+      break;
+    default:
+      IOTJS_ASSERT(!"Invalid Operation");
+  }
 }
 
-
-void iotjs_uart_read_cb(uv_poll_t* req, int status, int events) {
+static void iotjs_uart_read_cb(uv_poll_t* req, int status, int events) {
   iotjs_uart_t* uart = (iotjs_uart_t*)req->data;
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-
   char buf[UART_WRITE_BUFFER_SIZE];
-  int i = read(_this->device_fd, buf, UART_WRITE_BUFFER_SIZE - 1);
+  int i = read(uart->device_fd, buf, UART_WRITE_BUFFER_SIZE - 1);
   if (i > 0) {
     buf[i] = '\0';
     DDDLOG("%s - read data: %s", __func__, buf);
-    iotjs_uart_onread(_this->jemitter_this, buf);
+    jerry_value_t jemit =
+        iotjs_jval_get_property(iotjs_handlewrap_jobject(&uart->handlewrap),
+                                "emit");
+    IOTJS_ASSERT(jerry_value_is_function(jemit));
+
+    iotjs_jargs_t jargs = iotjs_jargs_create(2);
+    jerry_value_t str = jerry_create_string((const jerry_char_t*)"data");
+    jerry_value_t data = jerry_create_string((const jerry_char_t*)buf);
+    iotjs_jargs_append_jval(&jargs, str);
+    iotjs_jargs_append_jval(&jargs, data);
+    iotjs_jhelper_call_ok(jemit, iotjs_handlewrap_jobject(&uart->handlewrap),
+                          &jargs);
+
+    jerry_release_value(str);
+    jerry_release_value(data);
+    iotjs_jargs_destroy(&jargs);
+    jerry_release_value(jemit);
   }
 }
 
+void iotjs_uart_register_read_cb(iotjs_uart_t* uart) {
+  uv_poll_t* poll_handle = &uart->poll_handle;
+  uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());
+  uv_poll_init(loop, poll_handle, uart->device_fd);
+  poll_handle->data = uart;
+  uv_poll_start(poll_handle, UV_READABLE, iotjs_uart_read_cb);
+}
 
-#define UART_ASYNC(call, this, jcallback, op)                          \
-  do {                                                                 \
-    uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get()); \
-    iotjs_uart_reqwrap_t* req_wrap =                                   \
-        iotjs_uart_reqwrap_create(jcallback, this, op);                \
-    uv_work_t* req = iotjs_uart_reqwrap_req(req_wrap);                 \
-    uv_queue_work(loop, req, iotjs_uart_##call##_worker,               \
-                  iotjs_uart_after_worker);                            \
+static jerry_value_t uart_set_configuration(iotjs_uart_t* uart,
+                                            jerry_value_t jconfig) {
+  jerry_value_t jdevice =
+      iotjs_jval_get_property(jconfig, IOTJS_MAGIC_STRING_DEVICE);
+  if (!jerry_value_is_string(jdevice)) {
+    jerry_release_value(jdevice);
+    return JS_CREATE_ERROR(
+        TYPE, "Bad configuration - device is mandatory and must be a String");
+  }
+  uart->device_path = iotjs_jval_as_string(jdevice);
+  jerry_release_value(jdevice);
+
+  jerry_value_t jbaud_rate =
+      iotjs_jval_get_property(jconfig, IOTJS_MAGIC_STRING_BAUDRATE);
+  if (jerry_value_is_undefined(jbaud_rate)) {
+    uart->baud_rate = 9600;
+  } else {
+    if (!jerry_value_is_number(jbaud_rate)) {
+      jerry_release_value(jbaud_rate);
+      return JS_CREATE_ERROR(TYPE,
+                             "Bad configuration - baud rate must be a Number");
+    }
+    unsigned br = (unsigned)iotjs_jval_as_number(jbaud_rate);
+    jerry_release_value(jbaud_rate);
+
+    if (br != 230400 && br != 115200 && br != 57600 && br != 38400 &&
+        br != 19200 && br != 9600 && br != 4800 && br != 2400 && br != 1800 &&
+        br != 1200 && br != 600 && br != 300 && br != 200 && br != 150 &&
+        br != 134 && br != 110 && br != 75 && br != 50) {
+      return JS_CREATE_ERROR(TYPE, "Invalid baud rate");
+    }
+
+    uart->baud_rate = br;
+  }
+
+  jerry_value_t jdata_bits =
+      iotjs_jval_get_property(jconfig, IOTJS_MAGIC_STRING_DATABITS);
+  if (jerry_value_is_undefined(jdata_bits)) {
+    uart->data_bits = 8;
+  } else {
+    if (!jerry_value_is_number(jdata_bits)) {
+      jerry_release_value(jdata_bits);
+      return JS_CREATE_ERROR(TYPE,
+                             "Bad configuration - data bits must be a Number");
+    }
+    uint8_t db = (uint8_t)iotjs_jval_as_number(jdata_bits);
+    jerry_release_value(jdata_bits);
+
+    if (db > 8 || db < 5) {
+      return JS_CREATE_ERROR(TYPE, "Invalid data bits");
+    }
+
+    uart->data_bits = db;
+  }
+
+  return jerry_create_undefined();
+}
+
+#define UART_CALL_ASYNC(op, jcallback)                                         \
+  do {                                                                         \
+    uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());         \
+    iotjs_uart_reqwrap_t* req_wrap = uart_reqwrap_create(jcallback, uart, op); \
+    uv_work_t* req = &req_wrap->req;                                           \
+    uv_queue_work(loop, req, uart_worker, uart_after_worker);                  \
   } while (0)
 
 
-JS_FUNCTION(UartConstructor) {
+JS_FUNCTION(UartCons) {
   DJS_CHECK_THIS();
-  DJS_CHECK_ARGS(3, object, object, function);
+  DJS_CHECK_ARGS(1, object);
+  DJS_CHECK_ARG_IF_EXIST(1, function);
 
   // Create UART object
   jerry_value_t juart = JS_GET_THIS();
-  iotjs_uart_t* uart = iotjs_uart_create(juart);
-  IOTJS_ASSERT(uart == iotjs_uart_instance_from_jval(juart));
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
+  iotjs_uart_t* uart = uart_create(juart);
 
-  jerry_value_t jconfiguration = JS_GET_ARG(0, object);
-  jerry_value_t jemitter_this = JS_GET_ARG(1, object);
-  _this->jemitter_this = jerry_acquire_value(jemitter_this);
-  jerry_value_t jcallback = JS_GET_ARG(2, function);
+  jerry_value_t jconfig = JS_GET_ARG(0, object);
 
   // set configuration
-  jerry_value_t jdevice =
-      iotjs_jval_get_property(jconfiguration, IOTJS_MAGIC_STRING_DEVICE);
-  jerry_value_t jbaud_rate =
-      iotjs_jval_get_property(jconfiguration, IOTJS_MAGIC_STRING_BAUDRATE);
-  jerry_value_t jdata_bits =
-      iotjs_jval_get_property(jconfiguration, IOTJS_MAGIC_STRING_DATABITS);
-
-  _this->device_path = iotjs_jval_as_string(jdevice);
-  _this->baud_rate = iotjs_jval_as_number(jbaud_rate);
-  _this->data_bits = iotjs_jval_as_number(jdata_bits);
+  jerry_value_t res = uart_set_configuration(uart, jconfig);
+  if (jerry_value_has_error_flag(res)) {
+    return res;
+  }
 
   DDDLOG("%s - path: %s, baudRate: %d, dataBits: %d", __func__,
-         iotjs_string_data(&_this->device_path), _this->baud_rate,
-         _this->data_bits);
+         iotjs_string_data(&uart->device_path), uart->baud_rate,
+         uart->data_bits);
 
-  jerry_release_value(jdevice);
-  jerry_release_value(jbaud_rate);
-  jerry_release_value(jdata_bits);
+  jerry_value_t jcallback = JS_GET_ARG_IF_EXIST(1, function);
 
-  UART_ASYNC(open, uart, jcallback, kUartOpOpen);
+  // If the callback doesn't exist, it is completed synchronously.
+  // Otherwise, it will be executed asynchronously.
+  if (!jerry_value_is_null(jcallback)) {
+    UART_CALL_ASYNC(kUartOpOpen, jcallback);
+  } else if (!iotjs_uart_open(uart)) {
+    return JS_CREATE_ERROR(COMMON, "UART Error: cannot open UART");
+  }
 
   return jerry_create_undefined();
 }
 
-
 JS_FUNCTION(Write) {
   JS_DECLARE_THIS_PTR(uart, uart);
   DJS_CHECK_ARGS(1, string);
   DJS_CHECK_ARG_IF_EXIST(1, function);
 
-  const jerry_value_t jcallback = JS_GET_ARG_IF_EXIST(1, function);
+  uart->buf_data = JS_GET_ARG(0, string);
+  uart->buf_len = iotjs_string_size(&uart->buf_data);
 
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
+  UART_CALL_ASYNC(kUartOpWrite, JS_GET_ARG_IF_EXIST(1, function));
 
-  _this->buf_data = JS_GET_ARG(0, string);
-  _this->buf_len = iotjs_string_size(&_this->buf_data);
+  return jerry_create_undefined();
+}
 
-  if (!jerry_value_is_null(jcallback)) {
-    UART_ASYNC(write, uart, jcallback, kUartOpWrite);
-  } else {
-    bool result = iotjs_uart_write(uart);
-    iotjs_string_destroy(&_this->buf_data);
+JS_FUNCTION(WriteSync) {
+  JS_DECLARE_THIS_PTR(uart, uart);
+  DJS_CHECK_ARGS(1, string);
 
-    if (!result) {
-      return JS_CREATE_ERROR(COMMON, "UART Write Error");
-    }
+  uart->buf_data = JS_GET_ARG(0, string);
+  uart->buf_len = iotjs_string_size(&uart->buf_data);
+
+  bool result = iotjs_uart_write(uart);
+  iotjs_string_destroy(&uart->buf_data);
+
+  if (!result) {
+    return JS_CREATE_ERROR(COMMON, "UART Write Error");
   }
 
-  return jerry_create_null();
+  return jerry_create_undefined();
 }
 
-
 JS_FUNCTION(Close) {
   JS_DECLARE_THIS_PTR(uart, uart);
   DJS_CHECK_ARG_IF_EXIST(0, function);
 
-  const jerry_value_t jcallback = JS_GET_ARG_IF_EXIST(0, function);
+  UART_CALL_ASYNC(kUartOpClose, JS_GET_ARG_IF_EXIST(0, function));
 
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-  jerry_release_value(_this->jemitter_this);
+  return jerry_create_undefined();
+}
 
-  if (!jerry_value_is_null(jcallback)) {
-    UART_ASYNC(close, uart, jcallback, kUartOpClose);
-  } else {
-    if (!iotjs_uart_close(uart)) {
-      return JS_CREATE_ERROR(COMMON, "UART Close Error");
-    }
-  }
+JS_FUNCTION(CloseSync) {
+  JS_DECLARE_THIS_PTR(uart, uart);
 
+  iotjs_handlewrap_close(&uart->handlewrap, handlewrap_close_callback);
   return jerry_create_undefined();
 }
 
-
 jerry_value_t InitUart() {
-  jerry_value_t juart_constructor =
-      jerry_create_external_function(UartConstructor);
+  jerry_value_t juart_cons = jerry_create_external_function(UartCons);
 
   jerry_value_t prototype = jerry_create_object();
-
   iotjs_jval_set_method(prototype, IOTJS_MAGIC_STRING_WRITE, Write);
+  iotjs_jval_set_method(prototype, IOTJS_MAGIC_STRING_WRITESYNC, WriteSync);
   iotjs_jval_set_method(prototype, IOTJS_MAGIC_STRING_CLOSE, Close);
+  iotjs_jval_set_method(prototype, IOTJS_MAGIC_STRING_CLOSESYNC, CloseSync);
 
-  iotjs_jval_set_property_jval(juart_constructor, IOTJS_MAGIC_STRING_PROTOTYPE,
+  iotjs_jval_set_property_jval(juart_cons, IOTJS_MAGIC_STRING_PROTOTYPE,
                                prototype);
 
   jerry_release_value(prototype);
 
-  return juart_constructor;
+  return juart_cons;
 }
diff --git a/src/modules/iotjs_module_uart.h b/src/modules/iotjs_module_uart.h
index f8e54bc97..3571b8034 100644
--- a/src/modules/iotjs_module_uart.h
+++ b/src/modules/iotjs_module_uart.h
@@ -31,52 +31,40 @@ typedef enum {
   kUartOpWrite,
 } UartOp;
 
+typedef struct {
+  UartOp op;
+  bool result;
+} iotjs_uart_reqdata_t;
 
 typedef struct {
   iotjs_handlewrap_t handlewrap;
-  jerry_value_t jemitter_this;
   int device_fd;
-  int baud_rate;
+  unsigned baud_rate;
   uint8_t data_bits;
   iotjs_string_t device_path;
   iotjs_string_t buf_data;
   unsigned buf_len;
   uv_poll_t poll_handle;
-} IOTJS_VALIDATED_STRUCT(iotjs_uart_t);
-
-
-typedef struct {
-  UartOp op;
-  bool result;
-} iotjs_uart_reqdata_t;
-
+} iotjs_uart_t;
 
 typedef struct {
   iotjs_reqwrap_t reqwrap;
   uv_work_t req;
   iotjs_uart_reqdata_t req_data;
-  iotjs_uart_t* uart_instance;
-} IOTJS_VALIDATED_STRUCT(iotjs_uart_reqwrap_t);
+  iotjs_uart_t* uart_data;
+} iotjs_uart_reqwrap_t;
+
 
 #define THIS iotjs_uart_reqwrap_t* uart_reqwrap
 
 iotjs_uart_reqwrap_t* iotjs_uart_reqwrap_from_request(uv_work_t* req);
 iotjs_uart_reqdata_t* iotjs_uart_reqwrap_data(THIS);
-
 iotjs_uart_t* iotjs_uart_instance_from_reqwrap(THIS);
 
 #undef THIS
 
-
-#define UART_WORKER_INIT                                                      \
-  iotjs_uart_reqwrap_t* req_wrap = iotjs_uart_reqwrap_from_request(work_req); \
-  iotjs_uart_reqdata_t* req_data = iotjs_uart_reqwrap_data(req_wrap);         \
-  iotjs_uart_t* uart = iotjs_uart_instance_from_reqwrap(req_wrap);
-
-
-void iotjs_uart_read_cb(uv_poll_t* req, int status, int events);
-
-void iotjs_uart_open_worker(uv_work_t* work_req);
+void iotjs_uart_register_read_cb(iotjs_uart_t* uart);
+bool iotjs_uart_open(iotjs_uart_t* uart);
 bool iotjs_uart_write(iotjs_uart_t* uart);
 
 #endif /* IOTJS_MODULE_UART_H */
diff --git a/src/modules/linux/iotjs_module_uart-linux.c b/src/modules/linux/iotjs_module_uart-linux.c
index 714e17583..03de4f4c6 100644
--- a/src/modules/linux/iotjs_module_uart-linux.c
+++ b/src/modules/linux/iotjs_module_uart-linux.c
@@ -24,10 +24,8 @@
 
 #include "modules/iotjs_module_uart.h"
 
-static int baud_to_constant(int baudRate) {
+static unsigned baud_to_constant(unsigned baudRate) {
   switch (baudRate) {
-    case 0:
-      return B0;
     case 50:
       return B50;
     case 75:
@@ -65,10 +63,9 @@ static int baud_to_constant(int baudRate) {
     case 230400:
       return B230400;
   }
-  return -1;
+  return B0;
 }
 
-
 static int databits_to_constant(int dataBits) {
   switch (dataBits) {
     case 8:
@@ -83,56 +80,44 @@ static int databits_to_constant(int dataBits) {
   return -1;
 }
 
-
-void iotjs_uart_open_worker(uv_work_t* work_req) {
-  UART_WORKER_INIT;
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-
-  int fd = open(iotjs_string_data(&_this->device_path),
-                O_RDWR | O_NOCTTY | O_NDELAY);
+bool iotjs_uart_open(iotjs_uart_t* uart) {
+  int fd =
+      open(iotjs_string_data(&uart->device_path), O_RDWR | O_NOCTTY | O_NDELAY);
   if (fd < 0) {
-    req_data->result = false;
-    return;
+    return false;
   }
 
   struct termios options;
   tcgetattr(fd, &options);
   options.c_cflag = CLOCAL | CREAD;
-  options.c_cflag |= (tcflag_t)baud_to_constant(_this->baud_rate);
-  options.c_cflag |= (tcflag_t)databits_to_constant(_this->data_bits);
+  options.c_cflag |= (tcflag_t)baud_to_constant(uart->baud_rate);
+  options.c_cflag |= (tcflag_t)databits_to_constant(uart->data_bits);
   options.c_iflag = IGNPAR;
   options.c_oflag = 0;
   options.c_lflag = 0;
   tcflush(fd, TCIFLUSH);
   tcsetattr(fd, TCSANOW, &options);
 
-  _this->device_fd = fd;
-  uv_poll_t* poll_handle = &_this->poll_handle;
-
-  uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());
-  uv_poll_init(loop, poll_handle, fd);
-  poll_handle->data = uart;
-  uv_poll_start(poll_handle, UV_READABLE, iotjs_uart_read_cb);
+  uart->device_fd = fd;
+  iotjs_uart_register_read_cb(uart);
 
-  req_data->result = true;
+  return true;
 }
 
-
 bool iotjs_uart_write(iotjs_uart_t* uart) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
   int bytesWritten = 0;
   unsigned offset = 0;
-  int fd = _this->device_fd;
-  const char* buf_data = iotjs_string_data(&_this->buf_data);
+  int fd = uart->device_fd;
+  const char* buf_data = iotjs_string_data(&uart->buf_data);
 
   DDDLOG("%s - data: %s", __func__, buf_data);
 
   do {
     errno = 0;
-    bytesWritten = write(fd, buf_data + offset, _this->buf_len - offset);
+    bytesWritten = write(fd, buf_data + offset, uart->buf_len - offset);
     tcdrain(fd);
 
-    DDDLOG("%s - size: %d", __func__, _this->buf_len - offset);
+    DDDLOG("%s - size: %d", __func__, uart->buf_len - offset);
 
     if (bytesWritten != -1) {
       offset += (unsigned)bytesWritten;
@@ -145,7 +130,7 @@ bool iotjs_uart_write(iotjs_uart_t* uart) {
 
     return false;
 
-  } while (_this->buf_len > offset);
+  } while (uart->buf_len > offset);
 
   return true;
 }
diff --git a/src/modules/nuttx/iotjs_module_uart-nuttx.c b/src/modules/nuttx/iotjs_module_uart-nuttx.c
index ff42bcf41..c46e2199a 100644
--- a/src/modules/nuttx/iotjs_module_uart-nuttx.c
+++ b/src/modules/nuttx/iotjs_module_uart-nuttx.c
@@ -13,50 +13,40 @@
  * limitations under the License.
  */
 
-#if defined(__NUTTX__)
-
+#if !defined(__NUTTX__)
+#error "Module __FILE__ is for nuttx only"
+#endif
 
 #include "modules/iotjs_module_uart.h"
 
 
-void iotjs_uart_open_worker(uv_work_t* work_req) {
-  UART_WORKER_INIT;
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-
-  int fd = open(iotjs_string_data(&_this->device_path),
-                O_RDWR | O_NOCTTY | O_NDELAY);
+bool iotjs_uart_open(iotjs_uart_t* uart) {
+  int fd =
+      open(iotjs_string_data(&uart->device_path), O_RDWR | O_NOCTTY | O_NDELAY);
 
   if (fd < 0) {
-    req_data->result = false;
-    return;
+    return false;
   }
 
-  _this->device_fd = fd;
-  uv_poll_t* poll_handle = &_this->poll_handle;
-
-  uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());
-  uv_poll_init(loop, poll_handle, fd);
-  poll_handle->data = uart;
-  uv_poll_start(poll_handle, UV_READABLE, iotjs_uart_read_cb);
+  uart->device_fd = fd;
+  iotjs_uart_register_read_cb(uart);
 
-  req_data->result = true;
+  return true;
 }
 
-
 bool iotjs_uart_write(iotjs_uart_t* uart) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
   int bytesWritten = 0;
   unsigned offset = 0;
-  int fd = _this->device_fd;
-  const char* buf_data = iotjs_string_data(&_this->buf_data);
+  int fd = uart->device_fd;
+  const char* buf_data = iotjs_string_data(&uart->buf_data);
 
   DDDLOG("%s - data: %s", __func__, buf_data);
 
   do {
     errno = 0;
-    bytesWritten = write(fd, buf_data + offset, _this->buf_len - offset);
+    bytesWritten = write(fd, buf_data + offset, uart->buf_len - offset);
 
-    DDDLOG("%s - size: %d", __func__, _this->buf_len - offset);
+    DDDLOG("%s - size: %d", __func__, uart->buf_len - offset);
 
     if (bytesWritten != -1) {
       offset += (unsigned)bytesWritten;
@@ -69,10 +59,7 @@ bool iotjs_uart_write(iotjs_uart_t* uart) {
 
     return false;
 
-  } while (_this->buf_len > offset);
+  } while (uart->buf_len > offset);
 
   return true;
 }
-
-
-#endif // __NUTTX__
diff --git a/src/modules/tizenrt/iotjs_module_uart-tizenrt.c b/src/modules/tizenrt/iotjs_module_uart-tizenrt.c
index bc2b9ed2b..03dbd94d8 100644
--- a/src/modules/tizenrt/iotjs_module_uart-tizenrt.c
+++ b/src/modules/tizenrt/iotjs_module_uart-tizenrt.c
@@ -13,49 +13,39 @@
  * limitations under the License.
  */
 
-#if defined(__TIZENRT__)
-
+#if !defined(__TIZENRT__)
+#error "Module __FILE__ is for TizenRT only"
+#endif
 
 #include "modules/iotjs_module_uart.h"
 
-void iotjs_uart_open_worker(uv_work_t* work_req) {
-  UART_WORKER_INIT;
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
-
-  int fd = open(iotjs_string_data(&_this->device_path),
-                O_RDWR | O_NOCTTY | O_NDELAY);
+bool iotjs_uart_open(iotjs_uart_t* uart) {
+  int fd =
+      open(iotjs_string_data(&uart->device_path), O_RDWR | O_NOCTTY | O_NDELAY);
 
   if (fd < 0) {
-    req_data->result = false;
-    return;
+    return false;
   }
 
-  _this->device_fd = fd;
-  uv_poll_t* poll_handle = &_this->poll_handle;
-
-  uv_loop_t* loop = iotjs_environment_loop(iotjs_environment_get());
-  uv_poll_init(loop, poll_handle, fd);
-  poll_handle->data = uart;
-  uv_poll_start(poll_handle, UV_READABLE, iotjs_uart_read_cb);
+  uart->device_fd = fd;
+  iotjs_uart_register_read_cb(uart);
 
-  req_data->result = true;
+  return true;
 }
 
-
 bool iotjs_uart_write(iotjs_uart_t* uart) {
-  IOTJS_VALIDATED_STRUCT_METHOD(iotjs_uart_t, uart);
   int bytesWritten = 0;
   unsigned offset = 0;
-  int fd = _this->device_fd;
-  const char* buf_data = iotjs_string_data(&_this->buf_data);
+  int fd = uart->device_fd;
+  const char* buf_data = iotjs_string_data(&uart->buf_data);
 
   DDDLOG("%s - data: %s", __func__, buf_data);
 
   do {
     errno = 0;
-    bytesWritten = write(fd, buf_data + offset, _this->buf_len - offset);
+    bytesWritten = write(fd, buf_data + offset, uart->buf_len - offset);
 
-    DDDLOG("%s - size: %d", __func__, _this->buf_len - offset);
+    DDDLOG("%s - size: %d", __func__, uart->buf_len - offset);
 
     if (bytesWritten != -1) {
       offset += (unsigned)bytesWritten;
@@ -68,10 +58,7 @@ bool iotjs_uart_write(iotjs_uart_t* uart) {
 
     return false;
 
-  } while (_this->buf_len > offset);
+  } while (uart->buf_len > offset);
 
   return true;
 }
-
-
-#endif // __TIZENRT__
diff --git a/test/run_pass/test_uart.js b/test/run_pass/test_uart.js
index f57c761b1..7629bacec 100644
--- a/test/run_pass/test_uart.js
+++ b/test/run_pass/test_uart.js
@@ -14,9 +14,7 @@
  */
 
 var assert = require('assert');
-var Uart = require('uart');
-
-var uart = new Uart();
+var uart = require('uart');
 
 var configuration = {
   baudRate: 115200,
diff --git a/test/run_pass/test_uart_api.js b/test/run_pass/test_uart_api.js
index 9bd97e79a..c4c409885 100644
--- a/test/run_pass/test_uart_api.js
+++ b/test/run_pass/test_uart_api.js
@@ -14,11 +14,10 @@
  */
 
 var assert = require('assert');
-var Uart = require('uart');
-var uart = new Uart();
+var uart = require('uart');
 
 // ------ Test API existence
-assert.equal(typeof Uart, 'function',
-             'uart module does not export construction function');
 assert.equal(typeof uart.open, 'function',
              'uart does not provide \'open\' function');
+assert.equal(typeof uart.openSync, 'function',
+             'uart does not provide \'openSync\' function');
