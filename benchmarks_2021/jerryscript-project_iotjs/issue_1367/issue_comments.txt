Remove JavaScript layer and rework API in System I/O module
I've been thinking a while now about the optional callback parameter and it's effect to make the call async. I think it can be a bit confusing that a given call is sync or not simply on the number of arguments. In my opinion we should provide a sync and async methods just like our fs api to reduce the possible programming errors.
Considering the opinion of many people, I would like to confirm the following.
```js
var Gpio = require('gpio');

// async API
gpio.open({pin: 1}, function(err, gpioPin) {
    gpioPin.read(function(err, value) {
    });
});

// sync API
var gpioPin = gpio.openSync({pin: 1});
var value = gpioPin.readSync();
```
Done.