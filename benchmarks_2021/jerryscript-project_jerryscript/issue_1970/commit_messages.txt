Remove unnecessary if statement

JerryScript-DCO-1.0-Signed-off-by: Tamas Keri tkeri@inf.u-szeged.hu
jmem_run_free_unused_memory_callbacks should be called only if jmem_heap_allocated_size is not equals to 0.

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
jmem_run_free_unused_memory_callbacks should be called only if jmem_heap_allocated_size is not equals to 0.

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
jmem_run_free_unused_memory_callbacks should be called only if jmem_heap_allocated_size is not equals to 0.

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
jmem_run_free_unused_memory_callbacks should be called only if jmem_heap_allocated_size is not equals to 0.

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
jmem_run_free_unused_memory_callbacks should be called only if jmem_heap_allocated_size is not equals to 0.

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
GC should ignore not fully initialized objects.

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
GC should ignore not fully initialized objects

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
GC should ignore not fully initialized objects

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
GC should ignore not fully initialized objects (#1983)

Fixes #1970 which caused segmentation fault.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
