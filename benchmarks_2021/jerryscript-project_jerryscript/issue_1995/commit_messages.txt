Allow "<NUL>" character within string literals in strict mode

JerryScript-DCO-1.0-Signed-off-by: Yanhui Shen shen.elf@gmail.com
[unix-main] call jerry_run_all_enqueued_jobs before cleanup

Related Issue: #1995

JerryScript-DCO-1.0-Signed-off-by: Zidong Jiang zidong.jiang@intel.com
[unix-main] call jerry_run_all_enqueued_jobs before cleanup

Related Issue: #1995

JerryScript-DCO-1.0-Signed-off-by: Zidong Jiang zidong.jiang@intel.com
[unix-main] call jerry_run_all_enqueued_jobs before cleanup

Related Issue: #1995

JerryScript-DCO-1.0-Signed-off-by: Zidong Jiang zidong.jiang@intel.com
[unix-main] call jerry_run_all_enqueued_jobs before cleanup

Related Issue: #1995

JerryScript-DCO-1.0-Signed-off-by: Zidong Jiang zidong.jiang@intel.com
