diff --git a/docs/03.API-EXAMPLE.md b/docs/03.API-EXAMPLE.md
index 1ffe2e8d80..d7866bd692 100644
--- a/docs/03.API-EXAMPLE.md
+++ b/docs/03.API-EXAMPLE.md
@@ -551,6 +551,30 @@ Value of x is 12
 Value of x is 17
 ```
 
+## Changing the seed of pseudorandom generated numbers
+
+If you want to change the seed of Math.random() generated numbers, you have to initialize srand with a seed value.
+A recommended method is using `jerry_port_get_current_time(void)` or something based on a constantly changing value, therefore every run produces truly random numbers.
+
+[doctest]: # ()
+
+```c
+#include <string.h>
+#include "jerryscript.h"
+
+int
+main (void)
+{
+  const jerry_char_t script[] = "var a = Math.random(); print(a);";
+  size_t script_size = strlen ((const char *) script);
+
+  srand ((unsigned) (jerry_port_get_current_time ()));
+  bool ret_value = jerry_run_simple (script, script_size, JERRY_INIT_EMPTY);
+
+  return ret_value ? 0 : 1;
+}
+```
+
 ## Further steps
 
 For further API description, please visit [API Reference page](https://jerryscript-project.github.io/jerryscript/api-reference/) on [JerryScript home page](https://jerryscript-project.github.io/jerryscript/).
diff --git a/jerry-main/main-unix-minimal.c b/jerry-main/main-unix-minimal.c
index 87bb05f6de..1c37710e54 100644
--- a/jerry-main/main-unix-minimal.c
+++ b/jerry-main/main-unix-minimal.c
@@ -71,6 +71,7 @@ int
 main (int argc,
       char **argv)
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   if (argc <= 1 || (argc == 2 && (!strcmp ("-h", argv[1]) || !strcmp ("--help", argv[1]))))
   {
     print_help (argv[0]);
diff --git a/jerry-main/main-unix.c b/jerry-main/main-unix.c
index e1590d007d..8e28b02273 100644
--- a/jerry-main/main-unix.c
+++ b/jerry-main/main-unix.c
@@ -424,6 +424,7 @@ int
 main (int argc,
       char **argv)
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   const char *file_names[argc];
   int files_counter = 0;
 
diff --git a/targets/curie_bsp/jerry_app/quark/main.c b/targets/curie_bsp/jerry_app/quark/main.c
index e7b49a60fc..db20ef79a6 100644
--- a/targets/curie_bsp/jerry_app/quark/main.c
+++ b/targets/curie_bsp/jerry_app/quark/main.c
@@ -136,6 +136,7 @@ void eval_jerry_script (int argc, char *argv[], struct tcmd_handler_ctx *ctx)
 
 void jerry_start ()
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   jerry_init (JERRY_INIT_EMPTY);
   jerry_value_t global_obj_val = jerry_get_global_object ();
   jerry_value_t print_func_name_val = jerry_create_string ((jerry_char_t *) "print");
diff --git a/targets/mbed/source/main.cpp b/targets/mbed/source/main.cpp
index 39aedb1a31..3ee15aa36a 100644
--- a/targets/mbed/source/main.cpp
+++ b/targets/mbed/source/main.cpp
@@ -24,6 +24,7 @@ static Serial pc (USBTX, USBRX); //tx, rx
 
 static int jerry_task_init (void)
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   int retcode;
 
   DECLARE_JS_CODES;
diff --git a/targets/mbedos5/jerryscript-mbed/jerryscript-mbed-launcher/source/launcher.cpp b/targets/mbedos5/jerryscript-mbed/jerryscript-mbed-launcher/source/launcher.cpp
index edeca99a78..51b55b5477 100644
--- a/targets/mbedos5/jerryscript-mbed/jerryscript-mbed-launcher/source/launcher.cpp
+++ b/targets/mbedos5/jerryscript-mbed/jerryscript-mbed-launcher/source/launcher.cpp
@@ -67,6 +67,7 @@ static int load_javascript() {
 }
 
 int jsmbed_js_init() {
+    srand ((unsigned) jerry_port_get_current_time());
     jerry_init_flag_t flags = JERRY_INIT_EMPTY;
     jerry_init(flags);
 
diff --git a/targets/nuttx-stm32f4/jerry_main.c b/targets/nuttx-stm32f4/jerry_main.c
index 2cdbfadf42..7dcfcd1e74 100644
--- a/targets/nuttx-stm32f4/jerry_main.c
+++ b/targets/nuttx-stm32f4/jerry_main.c
@@ -346,6 +346,8 @@ int main (int argc, FAR char *argv[])
 int jerry_main (int argc, char *argv[])
 #endif
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
+
   if (argc > JERRY_MAX_COMMAND_LINE_ARGS)
   {
     jerry_port_log (JERRY_LOG_LEVEL_ERROR,
diff --git a/targets/particle/source/main.cpp b/targets/particle/source/main.cpp
index f8463c01e1..565c409d9a 100644
--- a/targets/particle/source/main.cpp
+++ b/targets/particle/source/main.cpp
@@ -70,6 +70,7 @@ js_delay (const jerry_value_t func_value, /**< function object */
 static void
 init_jerry ()
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   jerry_init (JERRY_INIT_EMPTY);
 
   /* Create an empty JS object */
diff --git a/targets/riot-stm32f4/source/main-riotos.c b/targets/riot-stm32f4/source/main-riotos.c
index bb770723d3..6659b8cb41 100644
--- a/targets/riot-stm32f4/source/main-riotos.c
+++ b/targets/riot-stm32f4/source/main-riotos.c
@@ -97,6 +97,7 @@ const shell_command_t shell_commands[] = {
 
 int main (void)
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   printf ("You are running RIOT on a(n) %s board.\n", RIOT_BOARD);
   printf ("This board features a(n) %s MCU.\n", RIOT_MCU);
 
diff --git a/targets/tizenrt-artik053/apps/jerryscript/jerry_main.c b/targets/tizenrt-artik053/apps/jerryscript/jerry_main.c
index 21c33bb7c6..934649097f 100644
--- a/targets/tizenrt-artik053/apps/jerryscript/jerry_main.c
+++ b/targets/tizenrt-artik053/apps/jerryscript/jerry_main.c
@@ -320,6 +320,7 @@ static jerry_log_level_t jerry_log_level = JERRY_LOG_LEVEL_ERROR;
 static int
 jerry_cmd_main (int argc, char *argv[])
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
 
   if (argc > JERRY_MAX_COMMAND_LINE_ARGS)
   {
diff --git a/targets/zephyr/src/main-zephyr.c b/targets/zephyr/src/main-zephyr.c
index a109589c45..905bc6171b 100644
--- a/targets/zephyr/src/main-zephyr.c
+++ b/targets/zephyr/src/main-zephyr.c
@@ -79,6 +79,7 @@ static int shell_cmd_handler (char *source_buffer)
 
 void main (void)
 {
+  srand ((unsigned) (jerry_port_get_current_time ()));
   uint32_t zephyr_ver = sys_kernel_version_get ();
   printf ("JerryScript build: " __DATE__ " " __TIME__ "\n");
   printf ("JerryScript API %d.%d\n", JERRY_API_MAJOR_VERSION, JERRY_API_MINOR_VERSION);
