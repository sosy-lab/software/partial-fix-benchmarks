Parsing Problem for Regex containing {} brackets
Hi!
I've checked the standard and for me it seems that the Atom should not accept the "{" character (as the PatternCharacter can be a SourceCharacter but not any of `^ $ \ . * + ? ( ) [ ] { } |`) thus the engine is ECMA compliant. Still that is true that other engines allow this extra element.

ES6 maybe?
Thanks @galpeter for a deeper look into the standard. I now agree that the current implementation is ECMA compliant (and other engines implement this as sort of extension).

(Re @zherczeg ) Seems ES6 also excluded "{" as an Atom (Atom -> PatternCharacter -> SourceCharacter but not SyntaxCharacter), and SyntaxCharacter is simply the same list as in ES5.1.

The standard tests for ECMA262 also use the escaped versions:
https://github.com/tc39/test262/blob/master/harness/nativeFunctionMatcher.js

However, I would like to know if such an extension could be provided upon giving JerryScript a setting (either in build-time or as a runtime argument) that provides the compatibility with other engines? This may up to active developers as JerryScript should be kept small to fit in the embedded systems.

Maybe the developers could:
1) Close the issue; or
2) Re-label this as a feature request.
@billyauhk @szledan was kind enough to create a feature allow parsing `{` characters. Please see: #2169 
Patch landed, closing bug.