instanceof throws TypeError for instance derived from native-backed constructor
Hi Gabriel, sorry for the late answer. IMHO this is a bug and it should work. @zherczeg is this intentional or is it a bug?
This issue seems to have unanswered questions still.

@zherczeg Could you please follow up on this?
Hi @gabrielschulhof!

Sorry for the late response, that's a valid issue since this should definitely work. I've just submitted a PR (#3246) to resolve this problem. By far, now you can use [jerry_binary_operation](https://github.com/jerryscript-project/jerryscript/blob/master/docs/02.API-REFERENCE.md#jerry_binary_operation), with `JERRY_BIN_OP_INSTANCEOF` first argument to perform `instanceof` operation from the API.