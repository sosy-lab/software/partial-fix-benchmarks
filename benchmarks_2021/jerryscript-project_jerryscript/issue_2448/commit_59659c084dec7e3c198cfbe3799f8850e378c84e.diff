diff --git a/jerry-core/CMakeLists.txt b/jerry-core/CMakeLists.txt
index fdec1c297c..00fa16dc5c 100644
--- a/jerry-core/CMakeLists.txt
+++ b/jerry-core/CMakeLists.txt
@@ -39,6 +39,7 @@ set(FEATURE_SYSTEM_ALLOCATOR   OFF     CACHE BOOL   "Enable system allocator?")
 set(FEATURE_VALGRIND           OFF     CACHE BOOL   "Enable Valgrind support?")
 set(FEATURE_VM_EXEC_STOP       OFF     CACHE BOOL   "Enable VM execution stopping?")
 set(MEM_HEAP_SIZE_KB           "512"   CACHE STRING "Size of memory heap, in kilobytes")
+set(REGEXP_RECURSION_LIMIT     "0"     CACHE STRING "Limit of regexp recurion depth")
 
 # Option overrides
 if(USING_MSVC)
@@ -94,6 +95,7 @@ message(STATUS "FEATURE_SYSTEM_ALLOCATOR    " ${FEATURE_SYSTEM_ALLOCATOR})
 message(STATUS "FEATURE_VALGRIND            " ${FEATURE_VALGRIND})
 message(STATUS "FEATURE_VM_EXEC_STOP        " ${FEATURE_VM_EXEC_STOP})
 message(STATUS "MEM_HEAP_SIZE_KB            " ${MEM_HEAP_SIZE_KB})
+message(STATUS "REGEXP_RECURSION_LIMIT      " ${REGEXP_RECURSION_LIMIT})
 
 # Include directories
 set(INCLUDE_CORE_PUBLIC "${CMAKE_CURRENT_SOURCE_DIR}/include")
@@ -242,6 +244,11 @@ if(FEATURE_REGEXP_STRICT_MODE)
   set(DEFINES_JERRY ${DEFINES_JERRY} ENABLE_REGEXP_STRICT_MODE)
 endif()
 
+# RegExp recursion depth limit
+if(REGEXP_RECURSION_LIMIT)
+  set(DEFINES_JERRY ${DEFINES_JERRY} REGEXP_RECURSION_LIMIT=${REGEXP_RECURSION_LIMIT})
+endif()
+
 # RegExp byte-code dumps
 if(FEATURE_REGEXP_DUMP)
   set(DEFINES_JERRY ${DEFINES_JERRY} REGEXP_DUMP_BYTE_CODE)
diff --git a/jerry-core/ecma/operations/ecma-regexp-object.c b/jerry-core/ecma/operations/ecma-regexp-object.c
index d618b8438a..957676d0ff 100644
--- a/jerry-core/ecma/operations/ecma-regexp-object.c
+++ b/jerry-core/ecma/operations/ecma-regexp-object.c
@@ -63,6 +63,13 @@
  */
 #define RE_IS_CAPTURE_GROUP(x) (((x) < RE_OP_NON_CAPTURE_GROUP_START) ? 1 : 0)
 
+/**
+ * Check RegExp recursion depth limit
+ */
+#ifdef REGEXP_RECURSION_LIMIT
+JERRY_STATIC_ASSERT (REGEXP_RECURSION_LIMIT > 0, regexp_recursion_limit_must_be_greater_than_zero);
+#endif /* REGEXP_RECURSION_LIMIT */
+
 /**
  * Parse RegExp flags (global, ignoreCase, multiline)
  *
@@ -364,6 +371,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
                  const lit_utf8_byte_t *str_p, /**< input string pointer */
                  const lit_utf8_byte_t **out_str_p) /**< [out] matching substring iterator */
 {
+  REGEXP_RECURSION_COUNTER_DECREASE_AND_TEST ();
   const lit_utf8_byte_t *str_curr_p = str_p;
 
   while (true)
@@ -376,12 +384,14 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
       {
         JERRY_TRACE_MSG ("Execute RE_OP_MATCH: match\n");
         *out_str_p = str_curr_p;
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_TRUE; /* match */
       }
       case RE_OP_CHAR:
       {
         if (str_curr_p >= re_ctx_p->input_end_p)
         {
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -393,6 +403,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (ch1 != ch2)
         {
           JERRY_TRACE_MSG ("fail\n");
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -404,6 +415,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
       {
         if (str_curr_p >= re_ctx_p->input_end_p)
         {
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -413,6 +425,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (lit_char_is_line_terminator (ch))
         {
           JERRY_TRACE_MSG ("fail\n");
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -432,6 +445,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (!(re_ctx_p->flags & RE_FLAG_MULTILINE))
         {
           JERRY_TRACE_MSG ("fail\n");
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -442,6 +456,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         }
 
         JERRY_TRACE_MSG ("fail\n");
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
       case RE_OP_ASSERT_END:
@@ -457,6 +472,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (!(re_ctx_p->flags & RE_FLAG_MULTILINE))
         {
           JERRY_TRACE_MSG ("fail\n");
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -467,6 +483,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         }
 
         JERRY_TRACE_MSG ("fail\n");
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
       case RE_OP_ASSERT_WORD_BOUNDARY:
@@ -498,6 +515,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (is_wordchar_left == is_wordchar_right)
           {
             JERRY_TRACE_MSG ("fail\n");
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return ECMA_VALUE_FALSE; /* fail */
           }
         }
@@ -509,6 +527,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (is_wordchar_left != is_wordchar_right)
           {
             JERRY_TRACE_MSG ("fail\n");
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return ECMA_VALUE_FALSE; /* fail */
           }
         }
@@ -563,6 +582,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
 
         if (!ECMA_IS_VALUE_ERROR (match_value))
         {
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
@@ -588,6 +608,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (str_curr_p >= re_ctx_p->input_end_p)
         {
           JERRY_TRACE_MSG ("fail\n");
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -618,6 +639,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (!is_match)
           {
             JERRY_TRACE_MSG ("fail\n");
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return ECMA_VALUE_FALSE; /* fail */
           }
         }
@@ -627,6 +649,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (is_match)
           {
             JERRY_TRACE_MSG ("fail\n");
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return ECMA_VALUE_FALSE; /* fail */
           }
         }
@@ -657,6 +680,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (str_curr_p >= re_ctx_p->input_end_p)
           {
             JERRY_TRACE_MSG ("fail\n");
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return ECMA_VALUE_FALSE; /* fail */
           }
 
@@ -666,6 +690,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ch1 != ch2)
           {
             JERRY_TRACE_MSG ("fail\n");
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return ECMA_VALUE_FALSE; /* fail */
           }
         }
@@ -689,6 +714,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -703,6 +729,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         bc_p = old_bc_p;
 
         re_ctx_p->saved_p[RE_GLOBAL_START_IDX] = old_start_p;
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
       case RE_OP_SAVE_AND_MATCH:
@@ -710,6 +737,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         JERRY_TRACE_MSG ("End of pattern is reached: match\n");
         re_ctx_p->saved_p[RE_GLOBAL_END_IDX] = str_curr_p;
         *out_str_p = str_curr_p;
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_TRUE; /* match */
       }
       case RE_OP_ALTERNATIVE:
@@ -774,6 +802,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (ecma_is_value_true (match_value))
         {
           *out_str_p = sub_str_p;
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return match_value; /* match */
         }
         else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -832,6 +861,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -856,6 +886,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -865,6 +896,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         }
 
         re_ctx_p->saved_p[start_idx] = old_start_p;
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
       case RE_OP_CAPTURE_NON_GREEDY_GROUP_END:
@@ -910,6 +942,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -958,6 +991,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         if (re_ctx_p->num_of_iterations_p[iter_idx] >= min
             && str_curr_p== re_ctx_p->saved_p[start_idx])
         {
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           return ECMA_VALUE_FALSE; /* fail */
         }
 
@@ -979,6 +1013,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -1003,6 +1038,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
             if (ecma_is_value_true (match_value))
             {
               *out_str_p = sub_str_p;
+              REGEXP_RECURSION_COUNTER_INCREASE ();
               return match_value; /* match */
             }
             else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -1024,6 +1060,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -1035,6 +1072,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
         /* restore if fails */
         re_ctx_p->saved_p[end_idx] = old_end_p;
         re_ctx_p->num_of_iterations_p[iter_idx]--;
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
       case RE_OP_NON_GREEDY_ITERATOR:
@@ -1059,6 +1097,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
             if (ecma_is_value_true (match_value))
             {
               *out_str_p = sub_str_p;
+              REGEXP_RECURSION_COUNTER_INCREASE ();
               return match_value; /* match */
             }
             else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -1082,6 +1121,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           str_curr_p = sub_str_p;
           num_of_iter++;
         }
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
       default:
@@ -1125,6 +1165,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           if (ecma_is_value_true (match_value))
           {
             *out_str_p = sub_str_p;
+            REGEXP_RECURSION_COUNTER_INCREASE ();
             return match_value; /* match */
           }
           else if (ECMA_IS_VALUE_ERROR (match_value))
@@ -1140,6 +1181,7 @@ re_match_regexp (re_matcher_ctx_t *re_ctx_p, /**< RegExp matcher context */
           lit_utf8_read_prev (&str_curr_p);
           num_of_iter--;
         }
+        REGEXP_RECURSION_COUNTER_INCREASE ();
         return ECMA_VALUE_FALSE; /* fail */
       }
     }
@@ -1232,6 +1274,7 @@ ecma_regexp_exec_helper (ecma_value_t regexp_value, /**< RegExp object */
   re_ctx.input_start_p = input_curr_p;
   const lit_utf8_byte_t *input_end_p = re_ctx.input_start_p + input_buffer_size;
   re_ctx.input_end_p = input_end_p;
+  REGEXP_RECURSION_COUNTER_INIT ();
 
   /* 1. Read bytecode header and init regexp matcher context. */
   re_ctx.flags = bc_p->header.status_flags;
diff --git a/jerry-core/ecma/operations/ecma-regexp-object.h b/jerry-core/ecma/operations/ecma-regexp-object.h
index 97e94cb7eb..038831ae20 100644
--- a/jerry-core/ecma/operations/ecma-regexp-object.h
+++ b/jerry-core/ecma/operations/ecma-regexp-object.h
@@ -18,6 +18,19 @@
 
 #ifndef CONFIG_DISABLE_REGEXP_BUILTIN
 
+#ifdef REGEXP_RECURSION_LIMIT
+#define REGEXP_RECURSION_COUNTER_DECREASE_AND_TEST() if (--re_ctx_p->recursion_counter == 0) \
+  { \
+    return ecma_raise_range_error (ECMA_ERR_MSG ("RegExp executor recursion limit is exceeded.")); \
+  }
+#define REGEXP_RECURSION_COUNTER_INCREASE() (++re_ctx_p->recursion_counter)
+#define REGEXP_RECURSION_COUNTER_INIT() (re_ctx.recursion_counter = REGEXP_RECURSION_LIMIT)
+#else
+#define REGEXP_RECURSION_COUNTER_DECREASE_AND_TEST()
+#define REGEXP_RECURSION_COUNTER_INCREASE()
+#define REGEXP_RECURSION_COUNTER_INIT()
+#endif /* REGEXP_RECURSION_LIMIT */
+
 #include "ecma-globals.h"
 #include "re-compiler.h"
 
@@ -46,6 +59,9 @@ typedef struct
   const lit_utf8_byte_t **saved_p;      /**< saved result string pointers, ECMA 262 v5, 15.10.2.1, State */
   const lit_utf8_byte_t *input_start_p; /**< start of input pattern string */
   const lit_utf8_byte_t *input_end_p;   /**< end of input pattern string */
+#ifdef REGEXP_RECURSION_LIMIT
+  uint32_t recursion_counter;             /**< RegExp recursion counter */
+#endif /* REGEXP_RECURSION_LIMIT */
   uint32_t num_of_captures;             /**< number of capture groups */
   uint32_t num_of_non_captures;         /**< number of non-capture groups */
   uint32_t *num_of_iterations_p;        /**< number of iterations */
diff --git a/jerry-core/parser/regexp/re-compiler.c b/jerry-core/parser/regexp/re-compiler.c
index 89df5829af..e05818c835 100644
--- a/jerry-core/parser/regexp/re-compiler.c
+++ b/jerry-core/parser/regexp/re-compiler.c
@@ -246,6 +246,7 @@ static ecma_value_t
 re_parse_alternative (re_compiler_ctx_t *re_ctx_p, /**< RegExp compiler context */
                       bool expect_eof) /**< expect end of file */
 {
+  REGEXP_RECURSION_COUNTER_DECREASE_AND_TEST ();
   uint32_t idx;
   re_bytecode_ctx_t *bc_ctx_p = re_ctx_p->bytecode_ctx_p;
   ecma_value_t ret_value = ECMA_VALUE_EMPTY;
@@ -440,6 +441,7 @@ re_parse_alternative (re_compiler_ctx_t *re_ctx_p, /**< RegExp compiler context
         else
         {
           re_insert_u32 (bc_ctx_p, alterantive_offset, re_get_bytecode_length (bc_ctx_p) - alterantive_offset);
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           should_loop = false;
         }
         break;
@@ -453,6 +455,7 @@ re_parse_alternative (re_compiler_ctx_t *re_ctx_p, /**< RegExp compiler context
         else
         {
           re_insert_u32 (bc_ctx_p, alterantive_offset, re_get_bytecode_length (bc_ctx_p) - alterantive_offset);
+          REGEXP_RECURSION_COUNTER_INCREASE ();
           should_loop = false;
         }
 
@@ -559,7 +562,7 @@ re_compile_bytecode (const re_compiled_code_t **out_bytecode_p, /**< [out] point
   re_ctx.flags = flags;
   re_ctx.highest_backref = 0;
   re_ctx.num_of_non_captures = 0;
-
+  REGEXP_RECURSION_COUNTER_INIT ();
   re_bytecode_ctx_t bc_ctx;
   bc_ctx.block_start_p = NULL;
   bc_ctx.block_end_p = NULL;
diff --git a/jerry-core/parser/regexp/re-compiler.h b/jerry-core/parser/regexp/re-compiler.h
index 3933597a10..2e6e603134 100644
--- a/jerry-core/parser/regexp/re-compiler.h
+++ b/jerry-core/parser/regexp/re-compiler.h
@@ -41,6 +41,9 @@ typedef struct
   uint32_t num_of_captures;          /**< number of capture groups */
   uint32_t num_of_non_captures;      /**< number of non-capture groups */
   uint32_t highest_backref;          /**< highest backreference */
+#ifdef REGEXP_RECURSION_LIMIT
+  uint32_t recursion_counter;             /**< RegExp recursion counter */
+#endif /* REGEXP_RECURSION_LIMIT */
   re_bytecode_ctx_t *bytecode_ctx_p; /**< pointer of RegExp bytecode context */
   re_token_t current_token;          /**< current token */
   re_parser_ctx_t *parser_ctx_p;     /**< pointer of RegExp parser context */
diff --git a/tools/build.py b/tools/build.py
index cc6816d1e3..08df9c2753 100755
--- a/tools/build.py
+++ b/tools/build.py
@@ -126,6 +126,8 @@ def devhelp(helpstring):
                          help='specify profile file')
     coregrp.add_argument('--regexp-strict-mode', metavar='X', choices=['ON', 'OFF'], type=str.upper,
                          help=devhelp('enable regexp strict mode (%(choices)s)'))
+    coregrp.add_argument('--regexp-recursion-limit', metavar='N', type=int,
+                         help='regexp recursion depth limit')
     coregrp.add_argument('--show-opcodes', metavar='X', choices=['ON', 'OFF'], type=str.upper,
                          help=devhelp('enable parser byte-code dumps (%(choices)s)'))
     coregrp.add_argument('--show-regexp-opcodes', metavar='X', choices=['ON', 'OFF'], type=str.upper,
@@ -194,6 +196,7 @@ def build_options_append(cmakeopt, cliarg):
     build_options_append('FEATURE_MEM_STRESS_TEST', arguments.mem_stress_test)
     build_options_append('FEATURE_PROFILE', arguments.profile)
     build_options_append('FEATURE_REGEXP_STRICT_MODE', arguments.regexp_strict_mode)
+    build_options_append('REGEXP_RECURSION_LIMIT', arguments.regexp_recursion_limit)
     build_options_append('FEATURE_PARSER_DUMP', arguments.show_opcodes)
     build_options_append('FEATURE_REGEXP_DUMP', arguments.show_regexp_opcodes)
     build_options_append('FEATURE_SNAPSHOT_EXEC', arguments.snapshot_exec)
diff --git a/tools/cppcheck/suppressions-list b/tools/cppcheck/suppressions-list
index ea48aec21b..7472a898b4 100644
--- a/tools/cppcheck/suppressions-list
+++ b/tools/cppcheck/suppressions-list
@@ -2,3 +2,4 @@ wrongmathcall:tests/unit-libm/test-libm.inc.h
 variableScope:jerry-libm/*.c
 invalidPointerCast:jerry-libm/*.c
 commaSeparatedReturn:*
+ConfigurationNotChecked:jerry-core/ecma/operations/ecma-regexp-object.c
diff --git a/tools/run-tests.py b/tools/run-tests.py
index c8f8101542..9a06087c13 100755
--- a/tools/run-tests.py
+++ b/tools/run-tests.py
@@ -142,6 +142,8 @@
             ['--jerry-cmdline-test=on']),
     Options('buildoption_test-cmdline_snapshot',
             ['--jerry-cmdline-snapshot=on']),
+    Options('buildoption_test-regexp_recursion_limit',
+            ['--regexp-recursion-limit=1000']),
 ]
 
 def get_arguments():
