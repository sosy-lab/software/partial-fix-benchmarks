diff --git a/jerry-core/ecma/base/ecma-helpers.c b/jerry-core/ecma/base/ecma-helpers.c
index 215c068596..ab925add46 100644
--- a/jerry-core/ecma/base/ecma-helpers.c
+++ b/jerry-core/ecma/base/ecma-helpers.c
@@ -580,12 +580,15 @@ ecma_find_named_property (ecma_object_t *obj_p, /**< object to find property in
   JERRY_ASSERT (obj_p != NULL);
   JERRY_ASSERT (name_p != NULL);
 
-  ecma_property_t *property_p = ecma_lcache_lookup (obj_p, name_p);
+  ecma_property_t *property_p = NULL;
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
+  property_p = ecma_lcache_lookup (obj_p, name_p);
   if (property_p != NULL)
   {
     return property_p;
   }
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
   ecma_property_header_t *prop_iter_p = ecma_get_property_list (obj_p);
 
@@ -597,11 +600,13 @@ ecma_find_named_property (ecma_object_t *obj_p, /**< object to find property in
                                              name_p,
                                              &property_real_name_cp);
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
     if (property_p != NULL
         && !ecma_is_property_lcached (property_p))
     {
       ecma_lcache_insert (obj_p, property_real_name_cp, property_p);
     }
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
     return property_p;
   }
@@ -693,11 +698,13 @@ ecma_find_named_property (ecma_object_t *obj_p, /**< object to find property in
     ecma_property_hashmap_create (obj_p);
   }
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
   if (property_p != NULL
       && !ecma_is_property_lcached (property_p))
   {
     ecma_lcache_insert (obj_p, property_name_cp, property_p);
   }
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
   return property_p;
 } /* ecma_find_named_property */
@@ -762,10 +769,12 @@ ecma_free_property (ecma_object_t *object_p, /**< object the property belongs to
     }
   }
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
   if (ecma_is_property_lcached (property_p))
   {
     ecma_lcache_invalidate (object_p, name_cp, property_p);
   }
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
   if (ECMA_PROPERTY_GET_NAME_TYPE (*property_p) == ECMA_DIRECT_STRING_PTR)
   {
@@ -1241,6 +1250,8 @@ ecma_set_property_configurable_attr (ecma_property_t *property_p, /**< [in,out]
   }
 } /* ecma_set_property_configurable_attr */
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
+
 /**
  * Check whether the property is registered in LCache
  *
@@ -1277,6 +1288,8 @@ ecma_set_property_lcached (ecma_property_t *property_p, /**< property */
   }
 } /* ecma_set_property_lcached */
 
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
+
 /**
  * Construct empty property descriptor, i.e.:
  *  property descriptor with all is_defined flags set to false and the rest - to default value.
diff --git a/jerry-core/ecma/base/ecma-helpers.h b/jerry-core/ecma/base/ecma-helpers.h
index d5f380d5dc..a7b7f347ea 100644
--- a/jerry-core/ecma/base/ecma-helpers.h
+++ b/jerry-core/ecma/base/ecma-helpers.h
@@ -339,8 +339,10 @@ void ecma_set_property_enumerable_attr (ecma_property_t *property_p, bool is_enu
 bool ecma_is_property_configurable (ecma_property_t property);
 void ecma_set_property_configurable_attr (ecma_property_t *property_p, bool is_configurable);
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
 bool ecma_is_property_lcached (ecma_property_t *property_p);
 void ecma_set_property_lcached (ecma_property_t *property_p, bool is_lcached);
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
 ecma_property_descriptor_t ecma_make_empty_property_descriptor (void);
 void ecma_free_property_descriptor (ecma_property_descriptor_t *prop_desc_p);
diff --git a/jerry-core/ecma/base/ecma-lcache.c b/jerry-core/ecma/base/ecma-lcache.c
index 6d4850106f..6e2b6566c3 100644
--- a/jerry-core/ecma/base/ecma-lcache.c
+++ b/jerry-core/ecma/base/ecma-lcache.c
@@ -61,8 +61,6 @@ ecma_lcache_row_index (jmem_cpointer_t object_cp, /**< compressed pointer to obj
   return (size_t) ((name_hash ^ object_cp) & ECMA_LCACHE_HASH_MASK);
 } /* ecma_lcache_row_index */
 
-#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
-
 /**
  * Insert an entry into LCache
  */
@@ -77,7 +75,6 @@ ecma_lcache_insert (ecma_object_t *object_p, /**< object */
                 || ECMA_PROPERTY_GET_TYPE (*prop_p) == ECMA_PROPERTY_TYPE_NAMEDACCESSOR
                 || ECMA_PROPERTY_GET_TYPE (*prop_p) == ECMA_PROPERTY_TYPE_INTERNAL);
 
-#ifndef CONFIG_ECMA_LCACHE_DISABLE
   jmem_cpointer_t object_cp;
 
   ECMA_SET_NON_NULL_POINTER (object_cp, object_p);
@@ -115,9 +112,6 @@ ecma_lcache_insert (ecma_object_t *object_p, /**< object */
   entry_p->prop_p = prop_p;
 
   ecma_set_property_lcached (entry_p->prop_p, true);
-#else  /* CONFIG_ECMA_LCACHE_DISABLE */
-  JERRY_UNUSED (name_cp);
-#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 } /* ecma_lcache_insert */
 
 /**
@@ -133,7 +127,6 @@ ecma_lcache_lookup (ecma_object_t *object_p, /**< object */
   JERRY_ASSERT (object_p != NULL);
   JERRY_ASSERT (prop_name_p != NULL);
 
-#ifndef CONFIG_ECMA_LCACHE_DISABLE
   jmem_cpointer_t object_cp;
   ECMA_SET_NON_NULL_POINTER (object_cp, object_p);
 
@@ -188,7 +181,6 @@ ecma_lcache_lookup (ecma_object_t *object_p, /**< object */
 
     entry_p++;
   }
-#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
   return NULL;
 } /* ecma_lcache_lookup */
@@ -206,7 +198,6 @@ ecma_lcache_invalidate (ecma_object_t *object_p, /**< object */
   JERRY_ASSERT (ECMA_PROPERTY_GET_TYPE (*prop_p) == ECMA_PROPERTY_TYPE_NAMEDDATA
                 || ECMA_PROPERTY_GET_TYPE (*prop_p) == ECMA_PROPERTY_TYPE_NAMEDACCESSOR);
 
-#ifndef CONFIG_ECMA_LCACHE_DISABLE
   jmem_cpointer_t object_cp;
   ECMA_SET_NON_NULL_POINTER (object_cp, object_p);
 
@@ -228,11 +219,10 @@ ecma_lcache_invalidate (ecma_object_t *object_p, /**< object */
     }
     entry_p++;
   }
-#else  /* CONFIG_ECMA_LCACHE_DISABLE */
-  JERRY_UNUSED (name_cp);
-#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 } /* ecma_lcache_invalidate */
 
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
+
 /**
  * @}
  * @}
diff --git a/jerry-core/ecma/base/ecma-lcache.h b/jerry-core/ecma/base/ecma-lcache.h
index a484863954..a0ab398ca5 100644
--- a/jerry-core/ecma/base/ecma-lcache.h
+++ b/jerry-core/ecma/base/ecma-lcache.h
@@ -23,10 +23,14 @@
  * @{
  */
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
+
 void ecma_lcache_insert (ecma_object_t *object_p, jmem_cpointer_t name_cp, ecma_property_t *prop_p);
 ecma_property_t *ecma_lcache_lookup (ecma_object_t *object_p, const ecma_string_t *prop_name_p);
 void ecma_lcache_invalidate (ecma_object_t *object_p, jmem_cpointer_t name_cp, ecma_property_t *prop_p);
 
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
+
 /**
  * @}
  * @}
diff --git a/jerry-core/ecma/operations/ecma-reference.c b/jerry-core/ecma/operations/ecma-reference.c
index 961e3c857c..81fc2e59cb 100644
--- a/jerry-core/ecma/operations/ecma-reference.c
+++ b/jerry-core/ecma/operations/ecma-reference.c
@@ -87,6 +87,7 @@ ecma_op_resolve_reference_value (ecma_object_t *lex_env_p, /**< starting lexical
 
       ecma_object_t *binding_obj_p = ecma_get_lex_env_binding_object (lex_env_p);
 
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
       ecma_property_t *property_p = ecma_lcache_lookup (binding_obj_p, name_p);
 
       if (property_p != NULL)
@@ -110,6 +111,7 @@ ecma_op_resolve_reference_value (ecma_object_t *lex_env_p, /**< starting lexical
         ecma_value_t base_value = ecma_make_object_value (binding_obj_p);
         return ecma_op_function_call (getter_p, base_value, NULL, 0);
       }
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
       ecma_value_t prop_value = ecma_op_object_find (binding_obj_p, name_p);
 
diff --git a/jerry-core/vm/vm.c b/jerry-core/vm/vm.c
index 70a2cdf813..815c81ef3a 100644
--- a/jerry-core/vm/vm.c
+++ b/jerry-core/vm/vm.c
@@ -53,7 +53,6 @@ vm_op_get_value (ecma_value_t object, /**< base object */
 {
   if (ecma_is_value_object (object))
   {
-    ecma_object_t *object_p = ecma_get_object_from_value (object);
     ecma_string_t *property_name_p = NULL;
 
     if (ecma_is_value_integer_number (property))
@@ -73,6 +72,8 @@ vm_op_get_value (ecma_value_t object, /**< base object */
 
     if (property_name_p != NULL)
     {
+#ifndef CONFIG_ECMA_LCACHE_DISABLE
+      ecma_object_t *object_p = ecma_get_object_from_value (object);
       ecma_property_t *property_p = ecma_lcache_lookup (object_p, property_name_p);
 
       if (property_p != NULL &&
@@ -80,6 +81,7 @@ vm_op_get_value (ecma_value_t object, /**< base object */
       {
         return ecma_fast_copy_value (ECMA_PROPERTY_VALUE_PTR (property_p)->value);
       }
+#endif /* !CONFIG_ECMA_LCACHE_DISABLE */
 
       /* There is no need to free the name. */
       return ecma_op_object_get (ecma_get_object_from_value (object), property_name_p);
