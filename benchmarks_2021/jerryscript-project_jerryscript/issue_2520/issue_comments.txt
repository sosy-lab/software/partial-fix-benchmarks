Consolidate configuration macros
I think this is a good idea as long as we finish it completely.

One more thing: we need an easy way to enable group of features excluding some of them. E.g. I want an ES 5.1 engine except regular expressions.
Looks good, better before 2.0 release
As someone who is "fairly" new to contributing, i highly support the idea. The current ones make enable/disable way too confusing, as contributors (unless they have a fair bit of knowledge in the system) have to constantly think about the "disable the disabled to make it enabled", which leads to constant headaches and macro define checkups. A unified system can help to spare this process, and make contributing much easier.   
As for the proposed names personally i don't think the `JERRY_CONFIG_` is a good idea instead of just `JERRY_`. To me it seems redundant and makes the macro names unnecessarily long.  
The macro documentation idea could be helpfull for new contributors,i think, as we have a lot of them, also the macro-cmake naming could be unified as well.

@galpeter #2793 and #2903 got merged. Is follow-up planned still or did these two PR address all aspects of this issue?
The PR's merged related to this issue reworked the macro names.
So all visible and configurable macros are now have a common format. I'll close this issue.