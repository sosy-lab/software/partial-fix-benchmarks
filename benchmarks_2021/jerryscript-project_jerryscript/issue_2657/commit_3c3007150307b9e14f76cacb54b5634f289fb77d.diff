diff --git a/jerry-core/parser/js/js-parser-expr.c b/jerry-core/parser/js/js-parser-expr.c
index 9e4f46d111..576d3df4d4 100644
--- a/jerry-core/parser/js/js-parser-expr.c
+++ b/jerry-core/parser/js/js-parser-expr.c
@@ -1403,6 +1403,11 @@ parser_parse_unary_expression (parser_context_t *context_p, /**< context */
     }
     case LEXER_KEYW_SUPER:
     {
+      if (context_p->status_flags & PARSER_CLASS_EXTENDS_EXPR)
+      {
+        parser_raise_error (context_p, PARSER_ERR_UNEXPECTED_SUPER_REFERENCE);
+      }
+
       if ((lexer_check_next_character (context_p, LIT_CHAR_DOT)
             || lexer_check_next_character (context_p, LIT_CHAR_LEFT_SQUARE))
           && context_p->status_flags & (PARSER_CLASS_HAS_SUPER | PARSER_IS_ARROW_FUNCTION))
diff --git a/jerry-core/parser/js/js-parser-internal.h b/jerry-core/parser/js/js-parser-internal.h
index 5c9002fddc..86d0d35ed0 100644
--- a/jerry-core/parser/js/js-parser-internal.h
+++ b/jerry-core/parser/js/js-parser-internal.h
@@ -39,40 +39,41 @@
  */
 typedef enum
 {
-  PARSER_IS_STRICT = (1u << 0),               /**< strict mode code */
-  PARSER_IS_FUNCTION = (1u << 1),             /**< function body is parsed */
-  PARSER_IS_CLOSURE = (1u << 2),              /**< function body is encapsulated in {} block */
-  PARSER_IS_FUNC_EXPRESSION = (1u << 3),      /**< a function expression is parsed */
-  PARSER_IS_PROPERTY_GETTER = (1u << 4),      /**< a property getter function is parsed */
-  PARSER_IS_PROPERTY_SETTER = (1u << 5),      /**< a property setter function is parsed */
-  PARSER_HAS_NON_STRICT_ARG = (1u << 7),      /**< the function has arguments which
-                                               *   are not supported in strict mode */
-  PARSER_ARGUMENTS_NEEDED = (1u << 8),        /**< arguments object must be created */
-  PARSER_ARGUMENTS_NOT_NEEDED = (1u << 9),    /**< arguments object must NOT be created */
-  PARSER_LEXICAL_ENV_NEEDED = (1u << 10),     /**< lexical environment object must be created */
-  PARSER_NO_REG_STORE = (1u << 11),           /**< all local variables must be stored
-                                               *   in the lexical environment object */
-  PARSER_INSIDE_WITH = (1u << 12),            /**< code block is inside a with statement */
-  PARSER_RESOLVE_BASE_FOR_CALLS = (1u << 13), /**< the this object must be resolved when
-                                               *   a function without a base object is called */
-  PARSER_HAS_INITIALIZED_VARS = (1u << 14),   /**< a CBC_INITIALIZE_VARS instruction must be emitted */
-  PARSER_HAS_LATE_LIT_INIT = (1u << 15),      /**< allocate memory for this string after
-                                               *   the local parser data is freed */
-  PARSER_NO_END_LABEL = (1u << 16),           /**< return instruction must be inserted
-                                               *   after the last byte code */
+  PARSER_IS_STRICT = (1u << 0),                     /**< strict mode code */
+  PARSER_IS_FUNCTION = (1u << 1),                   /**< function body is parsed */
+  PARSER_IS_CLOSURE = (1u << 2),                    /**< function body is encapsulated in {} block */
+  PARSER_IS_FUNC_EXPRESSION = (1u << 3),            /**< a function expression is parsed */
+  PARSER_IS_PROPERTY_GETTER = (1u << 4),            /**< a property getter function is parsed */
+  PARSER_IS_PROPERTY_SETTER = (1u << 5),            /**< a property setter function is parsed */
+  PARSER_HAS_NON_STRICT_ARG = (1u << 7),            /**< the function has arguments which
+                                                     *   are not supported in strict mode */
+  PARSER_ARGUMENTS_NEEDED = (1u << 8),              /**< arguments object must be created */
+  PARSER_ARGUMENTS_NOT_NEEDED = (1u << 9),          /**< arguments object must NOT be created */
+  PARSER_LEXICAL_ENV_NEEDED = (1u << 10),           /**< lexical environment object must be created */
+  PARSER_NO_REG_STORE = (1u << 11),                 /**< all local variables must be stored
+                                                     *   in the lexical environment object */
+  PARSER_INSIDE_WITH = (1u << 12),                  /**< code block is inside a with statement */
+  PARSER_RESOLVE_BASE_FOR_CALLS = (1u << 13),       /**< the this object must be resolved when
+                                                     *   a function without a base object is called */
+  PARSER_HAS_INITIALIZED_VARS = (1u << 14),         /**< a CBC_INITIALIZE_VARS instruction must be emitted */
+  PARSER_HAS_LATE_LIT_INIT = (1u << 15),            /**< allocate memory for this string after
+                                                     *   the local parser data is freed */
+  PARSER_NO_END_LABEL = (1u << 16),                 /**< return instruction must be inserted
+                                                     *   after the last byte code */
   PARSER_DEBUGGER_BREAKPOINT_APPENDED = (1u << 17), /**< pending (unsent) breakpoint
                                                      *   info is available */
 #ifndef CONFIG_DISABLE_ES2015_ARROW_FUNCTION
-  PARSER_IS_ARROW_FUNCTION = (1u << 18),      /**< an arrow function is parsed */
-  PARSER_ARROW_PARSE_ARGS = (1u << 19),       /**< parse the argument list of an arrow function */
+  PARSER_IS_ARROW_FUNCTION = (1u << 18),            /**< an arrow function is parsed */
+  PARSER_ARROW_PARSE_ARGS = (1u << 19),             /**< parse the argument list of an arrow function */
 #endif /* !CONFIG_DISABLE_ES2015_ARROW_FUNCTION */
 #ifndef CONFIG_DISABLE_ES2015_CLASS
   /* These three status flags must be in this order. See PARSER_CLASS_PARSE_OPTS_OFFSET. */
-  PARSER_CLASS_CONSTRUCTOR = (1u << 20),      /**< a class constructor is parsed (this value must be kept in
-                                               *   in sync with ECMA_PARSE_CLASS_CONSTRUCTOR) */
-  PARSER_CLASS_HAS_SUPER = (1u << 21),        /**< class has super reference */
-  PARSER_CLASS_STATIC_FUNCTION = (1u << 22),  /**< this function is a static class method */
-  PARSER_CLASS_SUPER_PROP_REFERENCE = (1u << 23),  /**< super property call or assignment */
+  PARSER_CLASS_CONSTRUCTOR = (1u << 20),            /**< a class constructor is parsed (this value must be kept in
+                                                     *   in sync with ECMA_PARSE_CLASS_CONSTRUCTOR) */
+  PARSER_CLASS_HAS_SUPER = (1u << 21),              /**< class has super reference */
+  PARSER_CLASS_STATIC_FUNCTION = (1u << 22),        /**< this function is a static class method */
+  PARSER_CLASS_SUPER_PROP_REFERENCE = (1u << 23),   /**< super property call or assignment */
+  PARSER_CLASS_EXTENDS_EXPR = (1u << 24),           /**< class extends expression is parsed */
 #endif /* !CONFIG_DISABLE_ES2015_CLASS */
 } parser_general_flags_t;
 
diff --git a/jerry-core/parser/js/js-parser-statm.c b/jerry-core/parser/js/js-parser-statm.c
index d17061c337..141f64f484 100644
--- a/jerry-core/parser/js/js-parser-statm.c
+++ b/jerry-core/parser/js/js-parser-statm.c
@@ -612,11 +612,15 @@ parser_parse_super_class_context_start (parser_context_t *context_p) /**< contex
 
   lexer_next_token (context_p);
 
+  context_p->status_flags |= PARSER_CLASS_EXTENDS_EXPR;
+
   /* NOTE: Currently there is no proper way to check whether the currently parsed expression
      is a valid lefthand-side expression or not, so we do not throw syntax error and parse
      the class extending value as an expression. */
   parser_parse_expression (context_p, PARSE_EXPR | PARSE_EXPR_NO_COMMA);
 
+  context_p->status_flags &= (uint32_t) ~PARSER_CLASS_EXTENDS_EXPR;
+
 #ifndef JERRY_NDEBUG
   PARSER_PLUS_EQUAL_U16 (context_p->context_stack_depth, PARSER_SUPER_CLASS_CONTEXT_STACK_ALLOCATION);
 #endif /* !JERRY_NDEBUG */
diff --git a/tests/jerry/es2015/regression-test-issue-2657.js b/tests/jerry/es2015/regression-test-issue-2657.js
new file mode 100644
index 0000000000..e67d081df6
--- /dev/null
+++ b/tests/jerry/es2015/regression-test-issue-2657.js
@@ -0,0 +1,20 @@
+// Copyright JS Foundation and other contributors, http://js.foundation
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+try {
+  eval ("var Mixin1 = (superclass) => class extends super.lass {};");
+  assert (false);
+} catch (e) {
+  assert (e instanceof SyntaxError)
+}
