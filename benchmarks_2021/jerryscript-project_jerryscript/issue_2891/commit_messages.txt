Allow extending statements with block context. (#3296)

The block contexts bound to different statements are handled in the same way.

JerryScript-DCO-1.0-Signed-off-by: Zoltan Herczeg zherczeg.u-szeged@partner.samsung.com
Fix delete property with empty base

This patch finally resolves #2891.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Fix delete property with empty base

This patch finally resolves #2891.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Improve delete property with undefined base

This patch finally resolves #2891 also the removes the related bytecode since it has become unnecessary.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Improve delete property with undefined base

This patch finally resolves #2891 also the removes the related bytecode since it has become unnecessary.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Improve delete property with undefined base (#3312)

This patch finally resolves #2891 also the removes the related bytecode since it has become unnecessary.

JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
