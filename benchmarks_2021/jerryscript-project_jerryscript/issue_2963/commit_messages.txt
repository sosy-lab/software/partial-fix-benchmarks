Math.sign and Math.trunc (#2943)

JerryScript-DCO-1.0-Signed-off-by: Örkényi Virág orkvi@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
Rework the engine's internal recursion limit (#2969)

This patch unifies the recursion limit checking for RegExp, function call and JSON as well.
Until now the limit was only a counter which was increased/decreased at certain points.
This counter has been substituted with a numeric limit which allows to restrict the stack usage.

This patch fixes #2963 and resolves the closed #2258 issue.

Co-authored-by: Gabor Loki loki@inf.u-szeged.hu
JerryScript-DCO-1.0-Signed-off-by: Robert Fancsik frobert@inf.u-szeged.hu
