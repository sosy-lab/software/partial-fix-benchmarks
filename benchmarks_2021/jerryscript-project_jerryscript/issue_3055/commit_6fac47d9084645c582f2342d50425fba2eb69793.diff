diff --git a/jerry-core/parser/js/byte-code.h b/jerry-core/parser/js/byte-code.h
index 77685d918e..ee3bdedec7 100644
--- a/jerry-core/parser/js/byte-code.h
+++ b/jerry-core/parser/js/byte-code.h
@@ -604,6 +604,26 @@
 #define CBC_HIGHEST_BIT_MASK 0x80
 #define CBC_LOWER_SEVEN_BIT_MASK 0x7f
 
+/**
+ * Literal encoding limit when full literal encoding mode is enabled
+ */
+#define CBC_FULL_LITERAL_ENCODING_LIMIT 128
+
+/**
+ * Literal encoding delta when full literal encoding mode is enabled
+ */
+#define CBC_FULL_LITERAL_ENCODING_DELTA 0x8000
+
+/**
+ * Literal encoding limit when full literal encoding mode is disabled
+ */
+#define CBC_SMALL_LITERAL_ENCODING_LIMIT 255
+
+/**
+ * Literal encoding delta when full literal encoding mode is disabled
+ */
+#define CBC_SMALL_LITERAL_ENCODING_DELTA 0xfe01
+
 /**
  * Literal indicies belong to one of the following groups:
  *
diff --git a/jerry-core/parser/js/js-parser.c b/jerry-core/parser/js/js-parser.c
index 06fc9d80bb..793c5874a2 100644
--- a/jerry-core/parser/js/js-parser.c
+++ b/jerry-core/parser/js/js-parser.c
@@ -1182,14 +1182,14 @@ parse_print_final_cbc (ecma_compiled_code_t *compiled_code_p, /**< compiled code
   if (!(compiled_code_p->status_flags & CBC_CODE_FLAGS_FULL_LITERAL_ENCODING))
   {
     JERRY_DEBUG_MSG ("small_lit_enc");
-    encoding_limit = 255;
-    encoding_delta = 0xfe01;
+    encoding_limit = CBC_SMALL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_SMALL_LITERAL_ENCODING_DELTA;
   }
   else
   {
     JERRY_DEBUG_MSG ("full_lit_enc");
-    encoding_limit = 128;
-    encoding_delta = 0x8000;
+    encoding_limit = CBC_FULL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_FULL_LITERAL_ENCODING_DELTA;
   }
 
   if (compiled_code_p->status_flags & CBC_CODE_FLAGS_UINT16_ARGUMENTS)
@@ -1803,9 +1803,19 @@ parser_post_processing (parser_context_t *context_p) /**< context */
     byte_code_p += sizeof (cbc_uint8_arguments_t);
   }
 
+  uint16_t encoding_limit;
+  uint16_t encoding_delta;
+
   if (context_p->literal_count > CBC_MAXIMUM_SMALL_VALUE)
   {
     compiled_code_p->status_flags |= CBC_CODE_FLAGS_FULL_LITERAL_ENCODING;
+    encoding_limit = CBC_FULL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_FULL_LITERAL_ENCODING_DELTA;
+  }
+  else
+  {
+    encoding_limit = CBC_SMALL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_SMALL_LITERAL_ENCODING_DELTA;
   }
 
   if (context_p->status_flags & PARSER_IS_STRICT)
@@ -1951,16 +1961,21 @@ parser_post_processing (parser_context_t *context_p) /**< context */
 
     while (flags & (CBC_HAS_LITERAL_ARG | CBC_HAS_LITERAL_ARG2))
     {
-      uint8_t first_byte = page_p->bytes[offset];
+      uint16_t first_byte = page_p->bytes[offset];
 
       uint8_t *opcode_pos_p = dst_p - 1;
-      *dst_p++ = first_byte;
+      *dst_p++ = (uint8_t) first_byte;
       real_offset++;
       PARSER_NEXT_BYTE_UPDATE (page_p, offset, real_offset);
 
       if (first_byte > literal_one_byte_limit)
       {
         *dst_p++ = page_p->bytes[offset];
+
+        if (first_byte > encoding_limit)
+        {
+          first_byte = (uint16_t) (((first_byte << 8) | dst_p[-1]) - encoding_delta);
+        }
         real_offset++;
       }
       PARSER_NEXT_BYTE_UPDATE (page_p, offset, real_offset);
diff --git a/jerry-core/vm/vm.c b/jerry-core/vm/vm.c
index f20f3ecef5..c2ca112c18 100644
--- a/jerry-core/vm/vm.c
+++ b/jerry-core/vm/vm.c
@@ -773,13 +773,13 @@ vm_init_loop (vm_frame_ctx_t *frame_ctx_p) /**< frame context */
   /* Prepare. */
   if (!(bytecode_header_p->status_flags & CBC_CODE_FLAGS_FULL_LITERAL_ENCODING))
   {
-    encoding_limit = 255;
-    encoding_delta = 0xfe01;
+    encoding_limit = CBC_SMALL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_SMALL_LITERAL_ENCODING_DELTA;
   }
   else
   {
-    encoding_limit = 128;
-    encoding_delta = 0x8000;
+    encoding_limit = CBC_FULL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_FULL_LITERAL_ENCODING_DELTA;
   }
 
   if (frame_ctx_p->bytecode_header_p->status_flags & CBC_CODE_FLAGS_UINT16_ARGUMENTS)
@@ -931,13 +931,13 @@ vm_loop (vm_frame_ctx_t *frame_ctx_p) /**< frame context */
   /* Prepare for byte code execution. */
   if (!(bytecode_header_p->status_flags & CBC_CODE_FLAGS_FULL_LITERAL_ENCODING))
   {
-    encoding_limit = 255;
-    encoding_delta = 0xfe01;
+    encoding_limit = CBC_SMALL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_SMALL_LITERAL_ENCODING_DELTA;
   }
   else
   {
-    encoding_limit = 128;
-    encoding_delta = 0x8000;
+    encoding_limit = CBC_FULL_LITERAL_ENCODING_LIMIT;
+    encoding_delta = CBC_FULL_LITERAL_ENCODING_DELTA;
   }
 
   if (bytecode_header_p->status_flags & CBC_CODE_FLAGS_UINT16_ARGUMENTS)
diff --git a/tests/jerry/regression-test-issue-3055.js b/tests/jerry/regression-test-issue-3055.js
new file mode 100644
index 0000000000..fbbd1f221b
--- /dev/null
+++ b/tests/jerry/regression-test-issue-3055.js
@@ -0,0 +1,19 @@
+// Copyright JS Foundation and other contributors, http://js.foundation
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+
+var src = '(function () {'
+for (var i = 0; i < 550; i++) { src += 'var a' + i + ' = 5; ' }
+src += '})()'
+eval(src)
