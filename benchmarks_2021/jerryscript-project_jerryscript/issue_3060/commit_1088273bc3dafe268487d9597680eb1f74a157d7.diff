diff --git a/jerry-core/ecma/base/ecma-helpers-external-pointers.c b/jerry-core/ecma/base/ecma-helpers-external-pointers.c
index 6ac1b283e4..b9a20d6be1 100644
--- a/jerry-core/ecma/base/ecma-helpers-external-pointers.c
+++ b/jerry-core/ecma/base/ecma-helpers-external-pointers.c
@@ -14,6 +14,7 @@
  */
 
 #include "ecma-alloc.h"
+#include "ecma-array-object.h"
 #include "ecma-globals.h"
 #include "ecma-objects.h"
 #include "ecma-helpers.h"
@@ -37,6 +38,13 @@ ecma_create_native_pointer_property (ecma_object_t *obj_p, /**< object to create
                                      void *info_p) /**< native pointer's type info */
 {
   ecma_string_t *name_p = ecma_get_magic_string (LIT_INTERNAL_MAGIC_STRING_NATIVE_POINTER);
+
+  if (ecma_get_object_type (obj_p) == ECMA_OBJECT_TYPE_ARRAY
+      && ((ecma_extended_object_t *) obj_p)->u.array.is_fast_mode)
+  {
+    ecma_fast_array_convert_to_normal (obj_p);
+  }
+
   ecma_property_t *property_p = ecma_find_named_property (obj_p, name_p);
 
   bool is_new = (property_p == NULL);
@@ -107,6 +115,13 @@ ecma_native_pointer_t *
 ecma_get_native_pointer_value (ecma_object_t *obj_p, /**< object to get property value from */
                                void *info_p) /**< native pointer's type info */
 {
+  if (ecma_get_object_type (obj_p) == ECMA_OBJECT_TYPE_ARRAY
+      && ((ecma_extended_object_t *) obj_p)->u.array.is_fast_mode)
+  {
+    /* Fast access mode array can not have native pointer properties */
+    return NULL;
+  }
+
   ecma_string_t *name_p = ecma_get_magic_string (LIT_INTERNAL_MAGIC_STRING_NATIVE_POINTER);
   ecma_property_t *property_p = ecma_find_named_property (obj_p, name_p);
 
@@ -149,6 +164,13 @@ bool
 ecma_delete_native_pointer_property (ecma_object_t *obj_p, /**< object to delete property from */
                                      void *info_p) /**< native pointer's type info */
 {
+  if (ecma_get_object_type (obj_p) == ECMA_OBJECT_TYPE_ARRAY
+      && ((ecma_extended_object_t *) obj_p)->u.array.is_fast_mode)
+  {
+    /* Fast access mode array can not have native pointer properties */
+    return false;
+  }
+
   ecma_string_t *name_p = ecma_get_magic_string (LIT_INTERNAL_MAGIC_STRING_NATIVE_POINTER);
   ecma_property_t *property_p = ecma_find_named_property (obj_p, name_p);
 
diff --git a/jerry-core/ecma/base/ecma-helpers.c b/jerry-core/ecma/base/ecma-helpers.c
index 1a21a09dea..f7d3596708 100644
--- a/jerry-core/ecma/base/ecma-helpers.c
+++ b/jerry-core/ecma/base/ecma-helpers.c
@@ -335,9 +335,6 @@ ecma_create_property (ecma_object_t *object_p, /**< the object */
   JERRY_ASSERT (ECMA_PROPERTY_PAIR_ITEM_COUNT == 2);
   JERRY_ASSERT (name_p != NULL);
   JERRY_ASSERT (object_p != NULL);
-  JERRY_ASSERT (ecma_is_lexical_environment (object_p)
-                || ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
-                || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
 
   jmem_cpointer_t *property_list_head_p = &object_p->u1.property_list_cp;
 
@@ -475,6 +472,9 @@ ecma_create_named_data_property (ecma_object_t *object_p, /**< object */
                                                                 *         if this field is non-NULL */
 {
   JERRY_ASSERT (object_p != NULL && name_p != NULL);
+  JERRY_ASSERT (ecma_is_lexical_environment (object_p)
+              || ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
+              || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
   JERRY_ASSERT (ecma_find_named_property (object_p, name_p) == NULL);
   JERRY_ASSERT ((prop_attributes & ~ECMA_PROPERTY_CONFIGURABLE_ENUMERABLE_WRITABLE) == 0);
 
@@ -501,6 +501,9 @@ ecma_create_named_accessor_property (ecma_object_t *object_p, /**< object */
                                                                     *         if this field is non-NULL */
 {
   JERRY_ASSERT (object_p != NULL && name_p != NULL);
+  JERRY_ASSERT (ecma_is_lexical_environment (object_p)
+              || ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
+              || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
   JERRY_ASSERT (ecma_find_named_property (object_p, name_p) == NULL);
   JERRY_ASSERT ((prop_attributes & ~ECMA_PROPERTY_CONFIGURABLE_ENUMERABLE) == 0);
 
@@ -533,6 +536,9 @@ ecma_find_named_property (ecma_object_t *obj_p, /**< object to find property in
 {
   JERRY_ASSERT (obj_p != NULL);
   JERRY_ASSERT (name_p != NULL);
+  JERRY_ASSERT (ecma_is_lexical_environment (obj_p)
+                || ecma_get_object_type (obj_p) != ECMA_OBJECT_TYPE_ARRAY
+                || !((ecma_extended_object_t *) obj_p)->u.array.is_fast_mode);
 
   ecma_property_t *property_p = NULL;
 
diff --git a/jerry-core/ecma/builtin-objects/ecma-builtin-string-prototype.c b/jerry-core/ecma/builtin-objects/ecma-builtin-string-prototype.c
index 952b91ff80..c26ea5a1bb 100644
--- a/jerry-core/ecma/builtin-objects/ecma-builtin-string-prototype.c
+++ b/jerry-core/ecma/builtin-objects/ecma-builtin-string-prototype.c
@@ -1506,13 +1506,17 @@ ecma_builtin_string_prototype_object_split (ecma_value_t this_to_string_val, /**
         ecma_object_t *match_obj_p = ecma_get_object_from_value (match_result);
         ecma_string_t *zero_str_p = ecma_get_ecma_string_from_uint32 (0);
         ecma_string_t *magic_index_str_p = ecma_get_magic_string (LIT_MAGIC_STRING_INDEX);
-        ecma_property_value_t *index_prop_value_p;
+        ecma_value_t index_prop_value;
 
         if (separator_is_regexp)
         {
-          index_prop_value_p = ecma_get_named_data_property (match_obj_p, magic_index_str_p);
+          JERRY_ASSERT (ecma_get_object_type (match_obj_p) != ECMA_OBJECT_TYPE_ARRAY
+                        || !((ecma_extended_object_t *) match_obj_p)->u.array.is_fast_mode);
+
+          ecma_property_value_t *index_prop_value_p = ecma_get_named_data_property (match_obj_p, magic_index_str_p);
           ecma_number_t index_num = ecma_get_number_from_value (index_prop_value_p->value);
           ecma_value_assign_number (&index_prop_value_p->value, index_num + (ecma_number_t) curr_pos);
+          index_prop_value = index_prop_value_p->value;
         }
         else
         {
@@ -1526,14 +1530,14 @@ ecma_builtin_string_prototype_object_split (ecma_value_t this_to_string_val, /**
 
           JERRY_ASSERT (ecma_is_value_true (put_comp));
 
-          index_prop_value_p = ecma_create_named_data_property (match_obj_p,
-                                                                magic_index_str_p,
-                                                                ECMA_PROPERTY_FLAG_WRITABLE,
-                                                                NULL);
+          index_prop_value = ecma_make_uint32_value (curr_pos);
+
+          put_comp = ecma_builtin_helper_def_prop (match_obj_p,
+                                                   magic_index_str_p,
+                                                   index_prop_value,
+                                                   ECMA_PROPERTY_FLAG_WRITABLE);
 
-          ecma_named_data_property_assign_value (match_obj_p,
-                                                 index_prop_value_p,
-                                                 ecma_make_uint32_value (curr_pos));
+          JERRY_ASSERT (ecma_is_value_true (put_comp));
         }
 
         ecma_value_t match_comp_value = ecma_op_object_get (match_obj_p, zero_str_p);
@@ -1546,7 +1550,7 @@ ecma_builtin_string_prototype_object_split (ecma_value_t this_to_string_val, /**
 
         ecma_free_value (match_comp_value);
 
-        ecma_number_t index_num = ecma_get_number_from_value (index_prop_value_p->value);
+        ecma_number_t index_num = ecma_get_number_from_value (index_prop_value);
         JERRY_ASSERT (index_num >= 0);
 
 
diff --git a/jerry-core/ecma/operations/ecma-array-object.c b/jerry-core/ecma/operations/ecma-array-object.c
index 506daaad0b..bf5adcf0a5 100644
--- a/jerry-core/ecma/operations/ecma-array-object.c
+++ b/jerry-core/ecma/operations/ecma-array-object.c
@@ -943,6 +943,19 @@ ecma_op_array_object_set_length (ecma_object_t *object_p, /**< the array object
   return ecma_reject (is_throw);
 } /* ecma_op_array_object_set_length */
 
+/**
+ * Property descriptor bitset for fast array data properties.
+ * If the property desciptor fields contains all the flags below
+ * attempt to stay fast access array during [[DefineOwnProperty]] operation.
+ */
+#define ECMA_FAST_ARRAY_DATA_PROP_FLAGS (ECMA_PROP_IS_VALUE_DEFINED \
+                                         | ECMA_PROP_IS_ENUMERABLE_DEFINED \
+                                         | ECMA_PROP_IS_ENUMERABLE \
+                                         | ECMA_PROP_IS_CONFIGURABLE_DEFINED \
+                                         | ECMA_PROP_IS_CONFIGURABLE \
+                                         | ECMA_PROP_IS_WRITABLE_DEFINED \
+                                         | ECMA_PROP_IS_WRITABLE)
+
 /**
  * [[DefineOwnProperty]] ecma array object's operation
  *
@@ -1009,13 +1022,24 @@ ecma_op_array_object_define_own_property (ecma_object_t *object_p, /**< the arra
 
   ecma_extended_object_t *ext_object_p = (ecma_extended_object_t *) object_p;
 
-  /* Note for further optimization: for enumerable, configurable, writable data properties
-     it's not necessary to convert it back to normal property list based array */
-  if (JERRY_UNLIKELY (ext_object_p->u.array.is_fast_mode))
+  if (ext_object_p->u.array.is_fast_mode)
   {
-    ecma_fast_array_convert_to_normal (object_p);
+    if ((property_desc_p->flags & ECMA_FAST_ARRAY_DATA_PROP_FLAGS) == ECMA_FAST_ARRAY_DATA_PROP_FLAGS)
+    {
+      if (ecma_fast_array_set_property (object_p, property_name_p, property_desc_p->value))
+      {
+        return ECMA_VALUE_TRUE;
+      }
+
+      JERRY_ASSERT (!ext_object_p->u.array.is_fast_mode);
+    }
+    else
+    {
+      ecma_fast_array_convert_to_normal (object_p);
+    }
   }
 
+  JERRY_ASSERT (!ext_object_p->u.array.is_fast_mode);
   uint32_t index = ecma_string_get_array_index (property_name_p);
 
   if (index == ECMA_STRING_NOT_ARRAY_INDEX)
diff --git a/jerry-core/ecma/operations/ecma-container-object.c b/jerry-core/ecma/operations/ecma-container-object.c
index ee681936fd..c010294e3e 100644
--- a/jerry-core/ecma/operations/ecma-container-object.c
+++ b/jerry-core/ecma/operations/ecma-container-object.c
@@ -245,10 +245,16 @@ ecma_op_container_to_key (ecma_value_t key_arg) /**< key argument */
   {
     ecma_object_t *obj_p = ecma_get_object_from_value (key_arg);
     ecma_string_t *key_string_p = ecma_get_magic_string (LIT_INTERNAL_MAGIC_STRING_MAP_KEY);
-    ecma_property_t *property_p = ecma_find_named_property (obj_p, key_string_p);
+
+    ecma_property_ref_t property_ref;
+    ecma_property_t property = ecma_op_object_get_own_property (obj_p,
+                                                                key_string_p,
+                                                                &property_ref,
+                                                                ECMA_PROPERTY_GET_NO_OPTIONS);
+
     ecma_string_t *object_key_string;
 
-    if (property_p == NULL)
+    if (property == ECMA_PROPERTY_TYPE_NOT_FOUND || property == ECMA_PROPERTY_TYPE_NOT_FOUND_AND_STOP)
     {
       object_key_string = ecma_new_map_key_string (key_arg);
       ecma_value_t put_comp = ecma_builtin_helper_def_prop (obj_p,
@@ -261,7 +267,7 @@ ecma_op_container_to_key (ecma_value_t key_arg) /**< key argument */
     }
     else
     {
-      object_key_string = ecma_get_string_from_value (ECMA_PROPERTY_VALUE_PTR (property_p)->value);
+      object_key_string = ecma_get_string_from_value (property_ref.value_p->value);
     }
 
     ecma_ref_ecma_string (object_key_string);
@@ -383,7 +389,7 @@ ecma_value_t
 ecma_op_container_set (ecma_value_t this_arg, /**< this argument */
                        ecma_value_t key_arg, /**< key argument */
                        ecma_value_t value_arg, /**< value argument */
-                      lit_magic_string_id_t lit_id) /**< internal class id */
+                       lit_magic_string_id_t lit_id) /**< internal class id */
 {
   ecma_map_object_t *map_object_p = ecma_op_container_get_object (this_arg, lit_id);
 
diff --git a/jerry-core/ecma/operations/ecma-objects-general.c b/jerry-core/ecma/operations/ecma-objects-general.c
index 84257e9052..1884105455 100644
--- a/jerry-core/ecma/operations/ecma-objects-general.c
+++ b/jerry-core/ecma/operations/ecma-objects-general.c
@@ -284,6 +284,8 @@ ecma_op_general_object_define_own_property (ecma_object_t *object_p, /**< the ob
 {
   JERRY_ASSERT (object_p != NULL
                 && !ecma_is_lexical_environment (object_p));
+  JERRY_ASSERT (ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
+                || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
   JERRY_ASSERT (property_name_p != NULL);
 
   ecma_property_types_t property_desc_type = ECMA_PROPERTY_TYPE_GENERIC;
diff --git a/jerry-core/ecma/operations/ecma-objects.c b/jerry-core/ecma/operations/ecma-objects.c
index 28cbf0a9d1..360da252ca 100644
--- a/jerry-core/ecma/operations/ecma-objects.c
+++ b/jerry-core/ecma/operations/ecma-objects.c
@@ -744,6 +744,10 @@ inline ecma_value_t JERRY_ATTR_ALWAYS_INLINE
 ecma_op_object_get_own_data_prop (ecma_object_t *object_p, /**< the object */
                                   ecma_string_t *property_name_p) /**< property name */
 {
+  JERRY_ASSERT (ecma_is_lexical_environment (object_p)
+                || ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
+                || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
+
   ecma_value_t result = ecma_op_object_find_own (ecma_make_object_value (object_p),
                                                  object_p,
                                                  property_name_p);
diff --git a/jerry-core/ecma/operations/ecma-regexp-object.c b/jerry-core/ecma/operations/ecma-regexp-object.c
index 4fe2d5e89e..146c105d59 100644
--- a/jerry-core/ecma/operations/ecma-regexp-object.c
+++ b/jerry-core/ecma/operations/ecma-regexp-object.c
@@ -1395,6 +1395,8 @@ ecma_regexp_exec_helper (ecma_value_t regexp_value, /**< RegExp object */
           capture_value = ecma_make_string_value (capture_str_p);
         }
 
+        JERRY_ASSERT (!((ecma_extended_object_t *) result_array_obj_p)->u.array.is_fast_mode);
+
         ecma_property_value_t *prop_value_p;
         prop_value_p = ecma_create_named_data_property (result_array_obj_p,
                                                         index_str_p,
diff --git a/jerry-core/vm/opcodes.c b/jerry-core/vm/opcodes.c
index e4f9657e76..d0e3dc7ab2 100644
--- a/jerry-core/vm/opcodes.c
+++ b/jerry-core/vm/opcodes.c
@@ -91,6 +91,10 @@ opfunc_set_accessor (bool is_getter, /**< is getter accessor */
                      ecma_value_t accessor) /**< accessor value */
 {
   ecma_object_t *object_p = ecma_get_object_from_value (object);
+
+  JERRY_ASSERT (ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
+                || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
+
   ecma_property_t *property_p = ecma_find_named_property (object_p, accessor_name_p);
 
   if (property_p != NULL
diff --git a/jerry-core/vm/vm.c b/jerry-core/vm/vm.c
index 85bd6afa77..f20f3ecef5 100644
--- a/jerry-core/vm/vm.c
+++ b/jerry-core/vm/vm.c
@@ -1259,6 +1259,10 @@ vm_loop (vm_frame_ctx_t *frame_ctx_p) /**< frame context */
 #endif /* ENABLED (JERRY_ES2015_CLASS) */
 
           ecma_object_t *object_p = ecma_get_object_from_value (stack_top_p[index]);
+
+          JERRY_ASSERT (ecma_get_object_type (object_p) != ECMA_OBJECT_TYPE_ARRAY
+                        || !((ecma_extended_object_t *) object_p)->u.array.is_fast_mode);
+
           ecma_property_t *property_p = ecma_find_named_property (object_p, prop_name_p);
 
           if (property_p != NULL
diff --git a/tests/jerry/es2015/regression-test-issue-3043-3046.js b/tests/jerry/es2015/regression-test-issue-3043-3046.js
new file mode 100644
index 0000000000..ad03a7aac2
--- /dev/null
+++ b/tests/jerry/es2015/regression-test-issue-3043-3046.js
@@ -0,0 +1,25 @@
+// Copyright JS Foundation and other contributors, http://js.foundation
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+// issue #3046
+var map = new Map();
+var array = [1.5];
+map.set(array);
+assert(map.has(array));
+
+// issue #3043
+map = new Map();
+array = [0];
+map.set(array);
+assert(map.has(array));
diff --git a/tests/jerry/es2015/regression-test-issue-3045.js b/tests/jerry/es2015/regression-test-issue-3045.js
new file mode 100644
index 0000000000..8ea33a6852
--- /dev/null
+++ b/tests/jerry/es2015/regression-test-issue-3045.js
@@ -0,0 +1,17 @@
+// Copyright JS Foundation and other contributors, http://js.foundation
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+var set = new Set();
+set.add();
+assert(!set.has([1]));
