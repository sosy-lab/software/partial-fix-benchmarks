diff --git a/jerry-core/ecma/operations/ecma-arraybuffer-object.c b/jerry-core/ecma/operations/ecma-arraybuffer-object.c
index 71a610cc97..23248ef630 100644
--- a/jerry-core/ecma/operations/ecma-arraybuffer-object.c
+++ b/jerry-core/ecma/operations/ecma-arraybuffer-object.c
@@ -15,6 +15,7 @@
 
 #include "ecma-arraybuffer-object.h"
 #include "ecma-try-catch-macro.h"
+#include "ecma-typedarray-object.h"
 #include "ecma-objects.h"
 #include "ecma-builtins.h"
 #include "ecma-exceptions.h"
@@ -173,7 +174,20 @@ ecma_is_arraybuffer (ecma_value_t target) /**< the target value */
 ecma_length_t JERRY_ATTR_PURE
 ecma_arraybuffer_get_length (ecma_object_t *object_p) /**< pointer to the ArrayBuffer object */
 {
-  JERRY_ASSERT (ecma_object_class_is (object_p, LIT_MAGIC_STRING_ARRAY_BUFFER_UL));
+  /**
+   * It is possible during the initialization of a TypedArray with another TypedArray,
+   * that the buffer is a TypedArray object, not an ArrayBuffer object. In these
+   * cases we should return with the internal TypedArray's buffer length.
+   */
+  if (!ecma_object_class_is (object_p, LIT_MAGIC_STRING_ARRAY_BUFFER_UL))
+  {
+    if (ecma_is_typedarray (ecma_make_object_value (object_p)))
+    {
+      ecma_extended_object_t *buffer_object_p = (ecma_extended_object_t *) object_p;
+      ecma_value_t buffer = buffer_object_p->u.pseudo_array.u2.arraybuffer;
+      object_p = ecma_get_object_from_value (buffer);
+    }
+  }
 
   ecma_extended_object_t *ext_object_p = (ecma_extended_object_t *) object_p;
   return ext_object_p->u.class_prop.u.length;
@@ -187,7 +201,20 @@ ecma_arraybuffer_get_length (ecma_object_t *object_p) /**< pointer to the ArrayB
 inline lit_utf8_byte_t * JERRY_ATTR_PURE JERRY_ATTR_ALWAYS_INLINE
 ecma_arraybuffer_get_buffer (ecma_object_t *object_p) /**< pointer to the ArrayBuffer object */
 {
-  JERRY_ASSERT (ecma_object_class_is (object_p, LIT_MAGIC_STRING_ARRAY_BUFFER_UL));
+  /**
+   * It is possible during the initialization of a TypedArray with another TypedArray,
+   * that the buffer is a TypedArray object, not an ArrayBuffer object. In these
+   * cases we should return with the internal TypedArray's buffer.
+   */
+  if (!ecma_object_class_is (object_p, LIT_MAGIC_STRING_ARRAY_BUFFER_UL))
+  {
+    if (ecma_is_typedarray (ecma_make_object_value (object_p)))
+    {
+      ecma_extended_object_t *buffer_object_p = (ecma_extended_object_t *) object_p;
+      ecma_value_t buffer = buffer_object_p->u.pseudo_array.u2.arraybuffer;
+      object_p = ecma_get_object_from_value (buffer);
+    }
+  }
 
   ecma_extended_object_t *ext_object_p = (ecma_extended_object_t *) object_p;
 
diff --git a/jerry-core/ecma/operations/ecma-typedarray-object.c b/jerry-core/ecma/operations/ecma-typedarray-object.c
index 9127bcbf37..4332661c30 100644
--- a/jerry-core/ecma/operations/ecma-typedarray-object.c
+++ b/jerry-core/ecma/operations/ecma-typedarray-object.c
@@ -997,6 +997,16 @@ ecma_typedarray_get_length (ecma_object_t *typedarray_p) /**< the pointer to the
     ecma_length_t buffer_length = ecma_arraybuffer_get_length (arraybuffer_p);
     uint8_t shift = ecma_typedarray_get_element_size_shift (typedarray_p);
 
+    if (!ecma_object_class_is (arraybuffer_p, LIT_MAGIC_STRING_ARRAY_BUFFER_UL))
+    {
+      if (ecma_is_typedarray (ecma_make_object_value (arraybuffer_p)))
+      {
+        uint8_t internal_shift = ecma_typedarray_get_element_size_shift (arraybuffer_p);
+        uint8_t element_size = (uint8_t) (1 << internal_shift);
+        return (buffer_length >> shift) / element_size;
+      }
+    }
+
     return buffer_length >> shift;
   }
 
diff --git a/tests/jerry/es2015/regression-test-issue-3836.js b/tests/jerry/es2015/regression-test-issue-3836.js
new file mode 100644
index 0000000000..2d4953a6a6
--- /dev/null
+++ b/tests/jerry/es2015/regression-test-issue-3836.js
@@ -0,0 +1,34 @@
+// Copyright JS Foundation and other contributors, http://js.foundation
+//
+// Licensed under the Apache License, Version 2.0 (the "License");
+// you may not use this file except in compliance with the License.
+// You may obtain a copy of the License at
+//
+//     http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software
+// distributed under the License is distributed on an "AS IS" BASIS
+// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+// See the License for the specific language governing permissions and
+// limitations under the License.
+
+function validate_typedarray (typedarray, result) {
+  assert(typedarray.length === result.length);
+  for (var i=0;i<typedarray.length;i++) {
+    assert(typedarray[i] === result[i]);
+  }
+}
+
+var v1 = new Float64Array(6);
+v1.buffer.constructor = Uint8Array;
+var v2 = new Float64Array(v1);
+
+assert(v2.buffer.constructor === Uint8Array);
+validate_typedarray(v2, [0,0,0,0,0,0]);
+
+var v3 = new Uint32Array(6);
+v3.buffer.constructor = Float64Array;
+var v4 = new Uint8Array(v3);
+
+assert(v4.buffer.constructor === Float64Array);
+validate_typedarray(v4, [0,0,0,0,0,0]);
