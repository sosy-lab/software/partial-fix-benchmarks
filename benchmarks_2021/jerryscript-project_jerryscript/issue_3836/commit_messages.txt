Fix error handling in SerializeJSONProperty (#3816)

Fixes #3813.

JerryScript-DCO-1.0-Signed-off-by: Dániel Bátyai dbatyai@inf.u-szeged.hu
Fix the TypedArray initialization where the internal buffer is another TypedArray

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
Fix TypedArray initialization with another TypedArray

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
Fix TypedArray initialization with another TypedArray

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
Fix TypedArray initialization with another TypedArray

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
Fix TypedArray initialization with another TypedArray

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
Fix TypedArray initialization with another TypedArray

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
Fix TypedArray initialization with another TypedArray (#3850)

Fixes #3836

JerryScript-DCO-1.0-Signed-off-by: Adam Szilagyi aszilagy@inf.u-szeged.hu
