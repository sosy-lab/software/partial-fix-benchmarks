Fix check-signed-off.sh: remove CR from git show output

When merging PRs via the GitHub web interface, CR characters can
sporadically appear in the commit message. This can also cause
signed-off-by message mismatch in check-signed-off.sh (even if
everything else is OK). To be on the safe side, this patch removes
any CR characters from the output of git show when performing the
check.

JerryScript-DCO-1.0-Signed-off-by: Akos Kiss akiss@inf.u-szeged.hu
Use const object parameters in getter API functions

Related issue: #668

JerryScript-DCO-1.0-Signed-off-by: László Langó llango.u-szeged@partner.samsung.com
Use const object parameters in getter API functions

Related issue: #668

JerryScript-DCO-1.0-Signed-off-by: László Langó llango.u-szeged@partner.samsung.com
Use const object parameters in getter API functions

Related issue: #668

JerryScript-DCO-1.0-Signed-off-by: László Langó llango.u-szeged@partner.samsung.com
Use const object parameters in getter API functions

Related issue: #668

JerryScript-DCO-1.0-Signed-off-by: László Langó llango.u-szeged@partner.samsung.com
Use const object parameters in getter API functions

Related issue: #668

JerryScript-DCO-1.0-Signed-off-by: László Langó llango.u-szeged@partner.samsung.com
