diff --git a/jerry-core/jerry-api.h b/jerry-core/jerry-api.h
index 0913a01f21..ad3ad2c77d 100644
--- a/jerry-core/jerry-api.h
+++ b/jerry-core/jerry-api.h
@@ -1,4 +1,5 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
+/* Copyright 2015-2016 Samsung Electronics Co., Ltd.
+ * Copyright 2016 University of Szeged.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -22,9 +23,7 @@
 #include <sys/types.h>
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
 /** \addtogroup jerry Jerry engine interface
@@ -147,25 +146,25 @@ typedef bool (*jerry_object_field_foreach_t) (const jerry_api_string_t *field_na
 /**
  * Returns whether the given jerry_api_value_t is void.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_void (const jerry_api_value_t *value_p);
 
 /**
  * Returns whether the given jerry_api_value_t is null.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_null (const jerry_api_value_t *value_p);
 
 /**
  * Returns whether the given jerry_api_value_t is undefined.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_undefined (const jerry_api_value_t *value_p);
 
 /**
  * Returns whether the given jerry_api_value_t has boolean type.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_boolean (const jerry_api_value_t *value_p);
 
 /**
@@ -174,19 +173,19 @@ jerry_api_value_is_boolean (const jerry_api_value_t *value_p);
  * More specifically, returns true if the type is JERRY_API_DATA_TYPE_FLOAT32,
  * JERRY_API_DATA_TYPE_FLOAT64 or JERRY_API_DATA_TYPE_UINT32, false otherwise.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_number (const jerry_api_value_t *value_p);
 
 /**
  * Returns whether the given jerry_api_value_t is string.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_string (const jerry_api_value_t *value_p);
 
 /**
  * Returns whether the given jerry_api_value_t is object.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_object (const jerry_api_value_t *value_p);
 
 /**
@@ -197,7 +196,7 @@ jerry_api_value_is_object (const jerry_api_value_t *value_p);
  * jerry_api_is_function() functiron return true for its v_object member,
  * otherwise false.
  */
-extern EXTERN_C bool
+bool
 jerry_api_value_is_function (const jerry_api_value_t *value_p);
 
 /**
@@ -205,7 +204,7 @@ jerry_api_value_is_function (const jerry_api_value_t *value_p);
  * If the given jerry_api_value_t structure has type other than
  * JERRY_API_DATA_TYPE_BOOLEAN, JERRY_ASSERT fails.
  */
-extern EXTERN_C bool
+bool
 jerry_api_get_boolean_value (const jerry_api_value_t *value_p);
 
 /**
@@ -219,7 +218,7 @@ jerry_api_get_boolean_value (const jerry_api_value_t *value_p);
  * JERRY_API_DATA_TYPE_FLOAT64 the function returns the v_float64 member.
  * As long as the type is none of the above, JERRY_ASSERT falis.
  */
-extern EXTERN_C double
+double
 jerry_api_get_number_value (const jerry_api_value_t *value_p);
 
 /**
@@ -227,7 +226,7 @@ jerry_api_get_number_value (const jerry_api_value_t *value_p);
  * If the given jerry_api_value_t structure has type other than
  * JERRY_API_DATA_TYPE_STRING, JERRY_ASSERT fails.
  */
-extern EXTERN_C jerry_api_string_t *
+jerry_api_string_t *
 jerry_api_get_string_value (const jerry_api_value_t *value_p);
 
 /**
@@ -235,35 +234,35 @@ jerry_api_get_string_value (const jerry_api_value_t *value_p);
  * If the given jerry_api_value_t structure has type other than
  * JERRY_API_DATA_TYPE_OBJECT, JERRY_ASSERT fails.
  */
-extern EXTERN_C jerry_api_object_t *
+jerry_api_object_t *
 jerry_api_get_object_value (const jerry_api_value_t *value_p);
 
 /**
  * Creates and returns a jerry_api_value_t with type
  * JERRY_API_DATA_TYPE_VOID.
  */
-extern EXTERN_C jerry_api_value_t
+jerry_api_value_t
 jerry_api_create_void_value (void);
 
 /**
  * Creates and returns a jerry_api_value_t with type
  * JERRY_API_DATA_TYPE_NULL.
  */
-extern EXTERN_C jerry_api_value_t
+jerry_api_value_t
 jerry_api_create_null_value (void);
 
 /**
  * Creates and returns a jerry_api_value_t with type
  * JERRY_API_DATA_TYPE_UNDEFINED.
  */
-extern EXTERN_C jerry_api_value_t
+jerry_api_value_t
 jerry_api_create_undefined_value (void);
 
 /**
  * Creates a JERRY_API_DATA_TYPE_BOOLEAN jerry_api_value_t from the given
  * boolean parameter and returns with it.
  */
-extern EXTERN_C jerry_api_value_t
+jerry_api_value_t
 jerry_api_create_boolean_value (bool value);
 
 /**
@@ -271,119 +270,77 @@ jerry_api_create_boolean_value (bool value);
  * with it.
  * The v_float64 member will be set and the will be JERRY_API_DATA_TYPE_FLOAT64.
  */
-extern EXTERN_C jerry_api_value_t
+jerry_api_value_t
 jerry_api_create_number_value (double value);
 
 /**
  * Creates a JERRY_API_DATA_TYPE_OBJECT type jerry_api_value_t from the
  * given jerry_api_object_t *parameter and returns with it.
  */
-extern EXTERN_C jerry_api_value_t
+jerry_api_value_t
 jerry_api_create_object_value (jerry_api_object_t *value);
 
 /**
  * Creates a JERRY_API_DATA_TYPE_STRING type jerry_api_value_t from the
  * given jerry_api_string_t *parameter and returns with it.
  */
-extern EXTERN_C jerry_api_value_t
-jerry_api_create_string_value (jerry_api_string_t *value);
+jerry_api_value_t jerry_api_create_string_value (jerry_api_string_t *value);
 
-extern EXTERN_C ssize_t
-jerry_api_string_to_char_buffer (const jerry_api_string_t *, jerry_api_char_t *, ssize_t);
-extern EXTERN_C
+ssize_t jerry_api_string_to_char_buffer (const jerry_api_string_t *, jerry_api_char_t *, ssize_t);
 jerry_api_string_t *jerry_api_acquire_string (jerry_api_string_t *);
-extern EXTERN_C
-void jerry_api_release_string (jerry_api_string_t *);
-
-extern EXTERN_C
 jerry_api_object_t *jerry_api_acquire_object (jerry_api_object_t *);
-extern EXTERN_C
-void jerry_api_release_object (jerry_api_object_t *);
 
-extern EXTERN_C
+void jerry_api_release_object (jerry_api_object_t *);
+void jerry_api_release_string (jerry_api_string_t *);
 void jerry_api_release_value (jerry_api_value_t *);
 
-extern EXTERN_C
+jerry_api_object_t *jerry_api_create_array_object (jerry_api_size_t);
+jerry_api_object_t *jerry_api_create_object (void);
 jerry_api_string_t *jerry_api_create_string (const jerry_api_char_t *);
-extern EXTERN_C
 jerry_api_string_t *jerry_api_create_string_sz (const jerry_api_char_t *, jerry_api_size_t);
-extern EXTERN_C
-jerry_api_object_t *jerry_api_create_object (void);
 
-extern EXTERN_C
-jerry_api_object_t *jerry_api_create_array_object (jerry_api_size_t);
-extern EXTERN_C
 bool jerry_api_set_array_index_value (jerry_api_object_t *, jerry_api_length_t, jerry_api_value_t *);
-extern EXTERN_C
 bool jerry_api_get_array_index_value (jerry_api_object_t *, jerry_api_length_t, jerry_api_value_t *);
 
-extern EXTERN_C
 jerry_api_object_t *jerry_api_create_error (jerry_api_error_t, const jerry_api_char_t *);
-extern EXTERN_C
 jerry_api_object_t *jerry_api_create_error_sz (jerry_api_error_t, const jerry_api_char_t *, jerry_api_size_t);
-extern EXTERN_C
 jerry_api_object_t *jerry_api_create_external_function (jerry_external_handler_t);
 
-extern EXTERN_C
-bool jerry_api_is_function (const jerry_api_object_t *);
-extern EXTERN_C
 bool jerry_api_is_constructor (const jerry_api_object_t *);
+bool jerry_api_is_function (const jerry_api_object_t *);
 
-extern EXTERN_C
 bool jerry_api_add_object_field (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t,
                                  const jerry_api_value_t *, bool);
-extern EXTERN_C
 bool jerry_api_delete_object_field (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t);
-extern EXTERN_C
-bool jerry_api_get_object_field_value (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_value_t *);
 
-extern EXTERN_C
+bool jerry_api_get_object_field_value (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_value_t *);
 bool jerry_api_get_object_field_value_sz (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t,
                                           jerry_api_value_t *);
 
-extern EXTERN_C
 bool jerry_api_set_object_field_value (jerry_api_object_t *, const jerry_api_char_t *, const jerry_api_value_t *);
-
-extern EXTERN_C
 bool jerry_api_set_object_field_value_sz (jerry_api_object_t *, const jerry_api_char_t *, jerry_api_size_t,
                                           const jerry_api_value_t *);
 
-extern EXTERN_C
 bool jerry_api_foreach_object_field (jerry_api_object_t *, jerry_object_field_foreach_t, void *);
-
-extern EXTERN_C
 bool jerry_api_get_object_native_handle (jerry_api_object_t *, uintptr_t *);
-
-extern EXTERN_C
 void jerry_api_set_object_native_handle (jerry_api_object_t *, uintptr_t, jerry_object_free_callback_t);
-
-extern EXTERN_C
 bool jerry_api_call_function (jerry_api_object_t *, jerry_api_object_t *, jerry_api_value_t *,
                               const jerry_api_value_t[], uint16_t);
-
-extern EXTERN_C
 bool jerry_api_construct_object (jerry_api_object_t *, jerry_api_value_t *, const jerry_api_value_t[], uint16_t);
-
-extern EXTERN_C
 jerry_completion_code_t jerry_api_eval (const jerry_api_char_t *, size_t, bool, bool, jerry_api_value_t *);
-
-extern EXTERN_C
 jerry_api_object_t *jerry_api_get_global (void);
 
-extern EXTERN_C
 void jerry_api_gc (void);
-
-extern EXTERN_C
 void jerry_register_external_magic_strings (const jerry_api_char_ptr_t *, uint32_t, const jerry_api_length_t *);
 
-extern EXTERN_C
 size_t jerry_parse_and_save_snapshot (const jerry_api_char_t *, size_t, bool, uint8_t *, size_t);
-
-extern EXTERN_C
 jerry_completion_code_t jerry_exec_snapshot (const void *, size_t, bool, jerry_api_value_t *);
 
 /**
  * @}
  */
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_API_H */
diff --git a/jerry-core/jerry-port.h b/jerry-core/jerry-port.h
index 036193d22d..9c34b43410 100644
--- a/jerry-core/jerry-port.h
+++ b/jerry-core/jerry-port.h
@@ -1,4 +1,5 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
+/* Copyright 2015-2016 Samsung Electronics Co., Ltd.
+ * Copyright 2016 University of Szeged.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -19,9 +20,7 @@
 #include <stdio.h>
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
 /** \addtogroup jerry_port Jerry engine port
@@ -31,17 +30,15 @@
 /**
  * Target port functions for console output
  */
-extern EXTERN_C
 int jerry_port_logmsg (FILE *stream, const char *format, ...);
-
-extern EXTERN_C
 int jerry_port_errormsg (const char *format, ...);
-
-extern EXTERN_C
 int jerry_port_putchar (int c);
 
 /**
  * @}
  */
 
-#endif /* !JERRY_API_H */
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
+#endif /* !JERRY_PORT_H */
diff --git a/jerry-core/jerry.h b/jerry-core/jerry.h
index f73530350a..25df2f7039 100644
--- a/jerry-core/jerry.h
+++ b/jerry-core/jerry.h
@@ -1,4 +1,5 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
+/* Copyright 2015-2016 Samsung Electronics Co., Ltd.
+ * Copyright 2016 University of Szeged.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -22,15 +23,14 @@
 #include "jerry-api.h"
 #include "jerry-port.h"
 
+#ifdef __cplusplus
+extern "C" {
+#endif /* !__cplusplus */
+
 /** \addtogroup jerry Jerry engine interface
  * @{
  */
 
-/**
- * Jerry flags
- */
-typedef uint32_t jerry_flag_t;
-
 #define JERRY_FLAG_EMPTY                  (0u)      /**< empty flag set */
 #define JERRY_FLAG_SHOW_OPCODES           (1u << 0) /**< dump byte-code to stdout after parse */
 #define JERRY_FLAG_MEM_STATS              (1u << 1) /**< dump memory statistics */
@@ -43,6 +43,11 @@ typedef uint32_t jerry_flag_t;
 #define JERRY_FLAG_ENABLE_LOG             (1u << 5) /**< enable logging */
 #define JERRY_FLAG_ABORT_ON_FAIL          (1u << 6) /**< abort instead of exit in case of failure */
 
+/**
+ * Jerry flags
+ */
+typedef uint32_t jerry_flag_t;
+
 /**
  * Error codes
  */
@@ -80,17 +85,16 @@ extern FILE *jerry_log_file;
  */
 typedef void (*jerry_error_callback_t) (jerry_fatal_code_t);
 
-extern EXTERN_C void jerry_init (jerry_flag_t);
-extern EXTERN_C void jerry_cleanup (void);
 
-extern EXTERN_C void jerry_get_memory_limits (size_t *, size_t *);
-extern EXTERN_C void jerry_reg_err_callback (jerry_error_callback_t);
+void jerry_init (jerry_flag_t);
+void jerry_cleanup (void);
 
-extern EXTERN_C bool jerry_parse (const jerry_api_char_t *, size_t);
-extern EXTERN_C jerry_completion_code_t jerry_run (void);
+void jerry_get_memory_limits (size_t *, size_t *);
+void jerry_reg_err_callback (jerry_error_callback_t);
 
-extern EXTERN_C jerry_completion_code_t
-jerry_run_simple (const jerry_api_char_t *, size_t, jerry_flag_t);
+bool jerry_parse (const jerry_api_char_t *, size_t);
+jerry_completion_code_t jerry_run (void);
+jerry_completion_code_t jerry_run_simple (const jerry_api_char_t *, size_t, jerry_flag_t);
 
 #ifdef CONFIG_JERRY_ENABLE_CONTEXTS
 /** \addtogroup jerry Jerry run contexts-related interface
@@ -102,11 +106,11 @@ jerry_run_simple (const jerry_api_char_t *, size_t, jerry_flag_t);
  */
 typedef struct jerry_ctx_t jerry_ctx_t;
 
-extern EXTERN_C jerry_ctx_t *jerry_new_ctx (void);
-extern EXTERN_C void jerry_cleanup_ctx (jerry_ctx_t *);
+jerry_ctx_t *jerry_new_ctx (void);
+void jerry_cleanup_ctx (jerry_ctx_t *);
 
-extern EXTERN_C void jerry_push_ctx (jerry_ctx_t *);
-extern EXTERN_C void jerry_pop_ctx (void);
+void jerry_push_ctx (jerry_ctx_t *);
+void jerry_pop_ctx (void);
 
 /**
  * @}
@@ -117,4 +121,7 @@ extern EXTERN_C void jerry_pop_ctx (void);
  * @}
  */
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_H */
diff --git a/jerry-libc/include/ctype.h b/jerry-libc/include/ctype.h
deleted file mode 100644
index 749908998e..0000000000
--- a/jerry-libc/include/ctype.h
+++ /dev/null
@@ -1,25 +0,0 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
- *
- * Licensed under the Apache License, Version 2.0 (the "License");
- * you may not use this file except in compliance with the License.
- * You may obtain a copy of the License at
- *
- *     http://www.apache.org/licenses/LICENSE-2.0
- *
- * Unless required by applicable law or agreed to in writing, software
- * distributed under the License is distributed on an "AS IS" BASIS
- * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
- * See the License for the specific language governing permissions and
- * limitations under the License.
- */
-
-#ifndef JERRY_LIBC_CTYPE_H
-#define JERRY_LIBC_CTYPE_H
-
-#ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
-#endif /* !__cplusplus */
-
-#endif /* !JERRY_LIBC_CTYPE_H */
diff --git a/jerry-libc/include/setjmp.h b/jerry-libc/include/setjmp.h
index 521b63dfcb..eb590c1c68 100644
--- a/jerry-libc/include/setjmp.h
+++ b/jerry-libc/include/setjmp.h
@@ -1,4 +1,5 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
+/* Copyright 2015-2016 Samsung Electronics Co., Ltd.
+ * Copyright 2016 University of Szeged.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -19,9 +20,7 @@
 #include <stdint.h>
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
 /**
@@ -56,7 +55,10 @@
  */
 typedef uint64_t jmp_buf[14];
 
-extern EXTERN_C int setjmp (jmp_buf env);
-extern EXTERN_C void longjmp (jmp_buf env, int val);
+int setjmp (jmp_buf env);
+void longjmp (jmp_buf env, int val);
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_LIBC_SETJMP_H */
diff --git a/jerry-libc/include/stdio.h b/jerry-libc/include/stdio.h
index fbef311d15..bee7a1a5b4 100644
--- a/jerry-libc/include/stdio.h
+++ b/jerry-libc/include/stdio.h
@@ -1,4 +1,5 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
+/* Copyright 2015-2016 Samsung Electronics Co., Ltd.
+ * Copyright 2016 University of Szeged.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -20,9 +21,7 @@
 #include <stddef.h>
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
 /**
@@ -47,17 +46,20 @@ extern FILE *stderr;
 /**
  * I/O routines
  */
-extern EXTERN_C int vfprintf (FILE *stream, const char *format, va_list ap);
-extern EXTERN_C int puts (const char *s);
-extern EXTERN_C FILE *fopen (const char *path, const char *mode);
-extern EXTERN_C int fclose (FILE *fp);
-extern EXTERN_C size_t fread (void *ptr, size_t size, size_t nmemb, FILE *stream);
-extern EXTERN_C size_t fwrite (const void *ptr, size_t size, size_t nmemb, FILE *stream);
-extern EXTERN_C int fseek (FILE *stream, long offset, int whence);
-extern EXTERN_C long ftell (FILE *stream);
-extern EXTERN_C int printf (const char *format, ...);
-extern EXTERN_C void rewind (FILE *stream);
-extern EXTERN_C int fprintf (FILE *stream, const char *format, ...);
-extern EXTERN_C int putchar (int c);
+int vfprintf (FILE *stream, const char *format, va_list ap);
+int puts (const char *s);
+FILE *fopen (const char *path, const char *mode);
+int fclose (FILE *fp);
+size_t fread (void *ptr, size_t size, size_t nmemb, FILE *stream);
+size_t fwrite (const void *ptr, size_t size, size_t nmemb, FILE *stream);
+int fseek (FILE *stream, long offset, int whence);
+long ftell (FILE *stream);
+int printf (const char *format, ...);
+void rewind (FILE *stream);
+int fprintf (FILE *stream, const char *format, ...);
+int putchar (int c);
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_LIBC_STDIO_H */
diff --git a/jerry-libc/include/stdlib.h b/jerry-libc/include/stdlib.h
index 0c3f2e5efa..7da3cef4d9 100644
--- a/jerry-libc/include/stdlib.h
+++ b/jerry-libc/include/stdlib.h
@@ -1,4 +1,5 @@
-/* Copyright 2015 Samsung Electronics Co., Ltd.
+/* Copyright 2015-2016 Samsung Electronics Co., Ltd.
+ * Copyright 2016 University of Szeged.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -17,14 +18,9 @@
 #define JERRY_LIBC_STDLIB_H
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
-extern EXTERN_C void __attribute__ ((noreturn)) exit (int);
-extern EXTERN_C void __attribute__ ((noreturn)) abort (void);
-
 /**
  * Maximum integer that could be returned by random number generator
  *
@@ -33,7 +29,12 @@ extern EXTERN_C void __attribute__ ((noreturn)) abort (void);
  */
 #define RAND_MAX (0x7fffffffu)
 
-extern EXTERN_C int rand (void);
-extern EXTERN_C void srand (unsigned int);
+void __attribute__ ((noreturn)) exit (int);
+void __attribute__ ((noreturn)) abort (void);
+int rand (void);
+void srand (unsigned int);
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_LIBC_STDLIB_H */
diff --git a/jerry-libc/include/string.h b/jerry-libc/include/string.h
index 5d3d6cd78c..fe0a7eb046 100644
--- a/jerry-libc/include/string.h
+++ b/jerry-libc/include/string.h
@@ -19,18 +19,19 @@
 #include <stddef.h>
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
-extern EXTERN_C int    memcmp (const void *s1, const void *s2, size_t n);
-extern EXTERN_C void*  memcpy (void *dest, const void *src, size_t n);
-extern EXTERN_C void*  memset (void *s, int c, size_t n);
-extern EXTERN_C int    strcmp (const char *s1, const char *s2);
-extern EXTERN_C size_t strlen (const char *s);
-extern EXTERN_C void*  memmove (void *dest, const void *src, size_t n);
-extern EXTERN_C int    strncmp (const char *s1, const char *s2, size_t n);
-extern EXTERN_C char*  strncpy (char *dest, const char *src, size_t n);
+void *memcpy (void *dest, const void *src, size_t n);
+void *memset (void *s, int c, size_t n);
+void *memmove (void *dest, const void *src, size_t n);
+int memcmp (const void *s1, const void *s2, size_t n);
+int strcmp (const char *s1, const char *s2);
+int strncmp (const char *s1, const char *s2, size_t n);
+char *strncpy (char *dest, const char *src, size_t n);
+size_t strlen (const char *s);
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_LIBC_STRING_H */
diff --git a/jerry-libc/include/time.h b/jerry-libc/include/time.h
index b01e3d773c..27bc87b382 100644
--- a/jerry-libc/include/time.h
+++ b/jerry-libc/include/time.h
@@ -18,9 +18,7 @@
 #define JERRY_LIBC_TIME_H
 
 #ifdef __cplusplus
-# define EXTERN_C "C"
-#else /* !__cplusplus */
-# define EXTERN_C
+extern "C" {
 #endif /* !__cplusplus */
 
 /**
@@ -41,6 +39,9 @@ struct timezone
   int tz_dsttime;         /**< type of DST correction */
 };
 
-extern EXTERN_C int gettimeofday (void *tp, void *tzp);
+int gettimeofday (void *tp, void *tzp);
 
+#ifdef __cplusplus
+}
+#endif /* !__cplusplus */
 #endif /* !JERRY_LIBC_TIME_H */
diff --git a/jerry-libc/jerry-libc.c b/jerry-libc/jerry-libc.c
index 634dc164bd..e6d056c1e3 100644
--- a/jerry-libc/jerry-libc.c
+++ b/jerry-libc/jerry-libc.c
@@ -18,7 +18,6 @@
  * Jerry libc's common functions implementation
  */
 
-#include <ctype.h>
 #include <stdio.h>
 #include <stdlib.h>
 #include <string.h>
diff --git a/jerry-libc/target/darwin/jerry-libc-target.c b/jerry-libc/target/darwin/jerry-libc-target.c
index 02eaa6a06f..f8a66bcda3 100644
--- a/jerry-libc/target/darwin/jerry-libc-target.c
+++ b/jerry-libc/target/darwin/jerry-libc-target.c
@@ -18,7 +18,6 @@
  * Jerry libc platform-specific functions darwin implementation
  */
 
-#include <ctype.h>
 #include <fcntl.h>
 #include <signal.h>
 #include <stdarg.h>
diff --git a/jerry-libc/target/linux/jerry-libc-target.c b/jerry-libc/target/linux/jerry-libc-target.c
index 67009d92a8..442c47a63b 100644
--- a/jerry-libc/target/linux/jerry-libc-target.c
+++ b/jerry-libc/target/linux/jerry-libc-target.c
@@ -18,7 +18,6 @@
  * Jerry libc platform-specific functions linux implementation
  */
 
-#include <ctype.h>
 #include <fcntl.h>
 #include <signal.h>
 #include <stdarg.h>
diff --git a/tools/vera++/scripts/rules/jerry_braces_on_separate_line.tcl b/tools/vera++/scripts/rules/jerry_braces_on_separate_line.tcl
index fea8925981..903ae61b9d 100644
--- a/tools/vera++/scripts/rules/jerry_braces_on_separate_line.tcl
+++ b/tools/vera++/scripts/rules/jerry_braces_on_separate_line.tcl
@@ -101,7 +101,8 @@ foreach file_name [getSourceFileNames] {
                     set expect_newline false
                 }
             } elseif {$tok_type in {leftbrace rightbrace}} {
-                if {[lindex $prev_tok 1] == $line_num} {
+                if {[lindex $prev_tok 1] == $line_num
+                    && ![regexp {^extern "C"} [getLine $file_name $line_num]]} {
                     report $file_name $line_num "brace should be placed on a separate line"
                 }
                 set expect_newline true
diff --git a/tools/vera++/scripts/rules/jerry_braces_same_line_or_column.tcl b/tools/vera++/scripts/rules/jerry_braces_same_line_or_column.tcl
index a8f637f573..996e11c0f4 100644
--- a/tools/vera++/scripts/rules/jerry_braces_same_line_or_column.tcl
+++ b/tools/vera++/scripts/rules/jerry_braces_same_line_or_column.tcl
@@ -44,8 +44,13 @@ foreach file_name [getSourceFileNames] {
         }
 
         if {$tok_type == "leftbrace"} {
-            lappend cols $col_num
-            lappend lines $line_num
+            if {![regexp {^extern "C"} [getLine $file_name $line_num]]} {
+                lappend cols $col_num
+            } else {
+                lappend cols 0
+            }
+
+            lappend lines $line_num                
         } elseif {$tok_type == "rightbrace"} {
             if {[llength $lines] > 0} {
                 if {[lindex $lines end] != $line_num && [lindex $cols end] != $col_num} {
diff --git a/tools/vera++/scripts/rules/jerry_indentation.tcl b/tools/vera++/scripts/rules/jerry_indentation.tcl
index 6608702d64..2ca71f38ce 100644
--- a/tools/vera++/scripts/rules/jerry_indentation.tcl
+++ b/tools/vera++/scripts/rules/jerry_indentation.tcl
@@ -52,7 +52,7 @@ foreach fileName [getSourceFileNames] {
             set lastCheckedLineNumber $lineNumber
         } elseif {$type == "space"} {
         } elseif {$type != "eof"} {
-            if {$type == "rightbrace"} {
+            if {$type == "rightbrace" && $indent > 0} {
                 incr indent -2
             }
 
@@ -79,7 +79,8 @@ foreach fileName [getSourceFileNames] {
                 if {$lineNumber == $lastCheckedLineNumber} {
                     if {$type == "leftbrace"} {
                         if {![regexp {^[[:blank:]]*\{[[:blank:]]*$} $line]
-                            && ![regexp {[^\{=]=[^\{=]\{.*\},?} $line]} {
+                            && ![regexp {[^\{=]=[^\{=]\{.*\},?} $line]
+                            && ![regexp {^extern "C"} $line]} {
                             report $fileName $lineNumber "Left brace is not the only non-space character in the line: '$line'"
                         }
                     }
@@ -100,7 +101,10 @@ foreach fileName [getSourceFileNames] {
             }
 
             if {$type == "leftbrace"} {
-                incr indent 2
+                set line [getLine $fileName $lineNumber]
+                if {![regexp {^extern "C"} $line]} {
+                    incr indent 2
+                }
             } elseif {$type == "leftparen"} {
                 incr parentheses_level 1
             } elseif {$type == "rightparen"} {
