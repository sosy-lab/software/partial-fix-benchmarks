diff --git a/Makefile b/Makefile
index 4136aaef40..5c17adcc24 100644
--- a/Makefile
+++ b/Makefile
@@ -75,6 +75,11 @@ BUILD_NAME:=
    CMAKE_DEFINES:=$(CMAKE_DEFINES) -DENABLE_LOG=$(LOG)
   endif
 
+ # Date system calls
+  ifneq ($(DATE_SYS_CALLS),)
+   CMAKE_DEFINES:=$(CMAKE_DEFINES) -DENABLE_DATE_SYS_CALLS=$(DATE_SYS_CALLS)
+  endif
+
  # All-in-one build
   ifneq ($(ALL_IN_ONE),)
    CMAKE_DEFINES:=$(CMAKE_DEFINES) -DENABLE_ALL_IN_ONE=$(ALL_IN_ONE)
diff --git a/jerry-core/CMakeLists.txt b/jerry-core/CMakeLists.txt
index dbe3c4713d..a500393d43 100644
--- a/jerry-core/CMakeLists.txt
+++ b/jerry-core/CMakeLists.txt
@@ -185,6 +185,11 @@ project (JerryCore C ASM)
    set(DEFINES_JERRY ${DEFINES_JERRY} JERRY_ENABLE_LOG)
   endif()
 
+ # Date system calls
+  if("${ENABLE_DATE_SYS_CALLS}" STREQUAL "ON")
+   set(DEFINES_JERRY ${DEFINES_JERRY} JERRY_ENABLE_DATE_SYS_CALLS)
+  endif()
+
 # Platform-specific configuration
  set(DEFINES_JERRY ${DEFINES_JERRY} ${DEFINES_JERRY_${PLATFORM_EXT}})
 
diff --git a/jerry-core/ecma/builtin-objects/ecma-builtin-date.c b/jerry-core/ecma/builtin-objects/ecma-builtin-date.c
index 0d14e99dcc..ed3298ef79 100644
--- a/jerry-core/ecma/builtin-objects/ecma-builtin-date.c
+++ b/jerry-core/ecma/builtin-objects/ecma-builtin-date.c
@@ -33,7 +33,9 @@
 #define BUILTIN_UNDERSCORED_ID date
 #include "ecma-builtin-internal-routines-template.inc.h"
 
+#ifdef JERRY_ENABLE_DATE_SYS_CALLS
 #include <sys/time.h>
+#endif /* JERRY_ENABLE_DATE_SYS_CALLS */
 
 /** \addtogroup ecma ECMA
  * @{
@@ -451,16 +453,19 @@ ecma_builtin_date_utc (ecma_value_t this_arg __attr_unused___, /**< this argumen
 static ecma_value_t
 ecma_builtin_date_now (ecma_value_t this_arg __attr_unused___) /**< this argument */
 {
-  struct timeval tv;
   ecma_number_t *now_num_p = ecma_alloc_number ();
   *now_num_p = ECMA_NUMBER_ZERO;
 
+#ifdef JERRY_ENABLE_DATE_SYS_CALLS
+  struct timeval tv;
+
   if (gettimeofday (&tv, NULL) != 0)
   {
     return ecma_raise_type_error ("gettimeofday failed");
   }
 
   *now_num_p = ((ecma_number_t) tv.tv_sec) * 1000.0 + ((ecma_number_t) (tv.tv_usec / 1000));
+#endif /* JERRY_ENABLE_DATE_SYS_CALLS */
 
   return ecma_make_number_value (now_num_p);
 } /* ecma_builtin_date_now */
diff --git a/jerry-core/ecma/builtin-objects/ecma-builtin-helpers-date.c b/jerry-core/ecma/builtin-objects/ecma-builtin-helpers-date.c
index ed755e924a..eb3db57a60 100644
--- a/jerry-core/ecma/builtin-objects/ecma-builtin-helpers-date.c
+++ b/jerry-core/ecma/builtin-objects/ecma-builtin-helpers-date.c
@@ -26,16 +26,10 @@
 
 #ifndef CONFIG_ECMA_COMPACT_PROFILE_DISABLE_DATE_BUILTIN
 
+#ifdef JERRY_ENABLE_DATE_SYS_CALLS
 #include <sys/time.h>
 
-/**
- * Timezone structure
- */
-struct timezone
-{
-  int tz_minuteswest;     /**< minutes west of Greenwich */
-  int tz_dsttime;         /**< type of DST correction */
-};
+#endif /* JERRY_ENABLE_DATE_SYS_CALLS */
 
 /** \addtogroup ecma ECMA
  * @{
@@ -456,15 +450,21 @@ ecma_date_week_day (ecma_number_t time) /**< time value */
 ecma_number_t __attr_always_inline___
 ecma_date_local_tza ()
 {
+#ifdef JERRY_ENABLE_DATE_SYS_CALLS
   struct timeval tv;
   struct timezone tz;
 
+  tz.tz_minuteswest = 0; /* gettimeofday may not fill tz, so zero-initializing */
+
   if (gettimeofday (&tv, &tz) != 0)
   {
-    return ecma_raise_type_error ("gettimeofday failed");
+    return ecma_number_make_nan ();
   }
 
   return tz.tz_minuteswest * -ECMA_DATE_MS_PER_MINUTE;
+#else /* !JERRY_ENABLE_DATE_SYS_CALLS */
+  return ECMA_NUMBER_ZERO;
+#endif /* JERRY_ENABLE_DATE_SYS_CALLS */
 } /* ecma_date_local_tza */
 
 /**
@@ -483,15 +483,21 @@ ecma_date_daylight_saving_ta (ecma_number_t time) /**< time value */
     return time; /* time is NaN */
   }
 
+#ifdef JERRY_ENABLE_DATE_SYS_CALLS
   struct timeval tv;
   struct timezone tz;
 
+  tz.tz_dsttime = 0; /* gettimeofday may not fill tz, so zero-initializing */
+
   if (gettimeofday (&tv, &tz) != 0)
   {
-    return ecma_raise_type_error ("gettimeofday failed");
+    return ecma_number_make_nan ();
   }
 
   return tz.tz_dsttime;
+#else /* !JERRY_ENABLE_DATE_SYS_CALLS */
+  return ECMA_NUMBER_ZERO;
+#endif /* JERRY_ENABLE_DATE_SYS_CALLS */
 } /* ecma_date_daylight_saving_ta */
 
 /**
diff --git a/jerry-libc/include/sys/time.h b/jerry-libc/include/sys/time.h
index e952e3e3ed..986adea2a1 100644
--- a/jerry-libc/include/sys/time.h
+++ b/jerry-libc/include/sys/time.h
@@ -31,6 +31,15 @@ struct timeval
   unsigned long tv_usec;  /**< microseconds */
 };
 
+/**
+ * Timezone structure
+ */
+struct timezone
+{
+  int tz_minuteswest;     /**< minutes west of Greenwich */
+  int tz_dsttime;         /**< type of DST correction */
+};
+
 int gettimeofday (void *tp, void *tzp);
 
 #ifdef __cplusplus
