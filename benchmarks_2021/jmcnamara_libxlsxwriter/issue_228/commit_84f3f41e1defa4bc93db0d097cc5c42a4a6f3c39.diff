diff --git a/examples/constant_memory.c b/examples/constant_memory.c
index e628c2d1..da055a59 100644
--- a/examples/constant_memory.c
+++ b/examples/constant_memory.c
@@ -17,7 +17,8 @@ int main() {
 
     /* Set the worksheet options. */
     lxw_workbook_options options = {.constant_memory = LXW_TRUE,
-                                    .tmpdir = NULL};
+                                    .tmpdir = NULL,
+                                    .use_zip64 = LXW_FALSE};
 
     /* Create a new workbook with options. */
     lxw_workbook  *workbook  = workbook_new_opt("constant_memory.xlsx", &options);
diff --git a/include/xlsxwriter/workbook.h b/include/xlsxwriter/workbook.h
index b2e08255..7dbcedd0 100644
--- a/include/xlsxwriter/workbook.h
+++ b/include/xlsxwriter/workbook.h
@@ -203,27 +203,40 @@ typedef struct lxw_doc_properties {
  *
  * The following properties are supported:
  *
- * - `constant_memory`: Reduces the amount of data stored in memory so that
- *   large files can be written efficiently.
+ * - `constant_memory`: This option reduces the amount of data stored in
+ *   memory so that large files can be written efficiently. This option is off
+ *   by default. See the note below for limitations when this mode is on.
  *
- *   @note In this mode a row of data is written and then discarded when a
- *   cell in a new row is added via one of the `worksheet_write_*()`
- *   functions. Therefore, once this option is active, data should be written in
- *   sequential row order. For this reason the `worksheet_merge_range()`
- *   doesn't work in this mode. See also @ref ww_mem_constant.
- *
- * - `tmpdir`: libxlsxwriter stores workbook data in temporary files prior
- *   to assembling the final XLSX file. The temporary files are created in the
+ * - `tmpdir`: libxlsxwriter stores workbook data in temporary files prior to
+ *   assembling the final XLSX file. The temporary files are created in the
  *   system's temp directory. If the default temporary directory isn't
  *   accessible to your application, or doesn't contain enough space, you can
  *   specify an alternative location using the `tmpdir` option.
+ *
+ * - `use_zip64`: Make the zip library use ZIP64 extensions when writing very
+ *   large xlsx files to allow the zip container, or individual XML files
+ *   within it, to be greater than 4 GB. See [ZIP64 on Wikipedia][zip64_wiki]
+ *   for more information. This option is off by default.
+ *
+ *   [zip64_wiki]: https://en.wikipedia.org/wiki/Zip_(file_format)#ZIP64
+ *
+ * @note In `constant_memory` mode a row of data is written and then discarded
+ * when a cell in a new row is added via one of the `worksheet_write_*()`
+ * functions. Therefore, once this option is active, data should be written in
+ * sequential row order. For this reason the `worksheet_merge_range()` doesn't
+ * work in this mode. See also @ref ww_mem_constant.
+ *
  */
 typedef struct lxw_workbook_options {
-    /** Optimize the workbook to use constant memory for worksheets */
+    /** Optimize the workbook to use constant memory for worksheets. */
     uint8_t constant_memory;
 
     /** Directory to use for the temporary files created by libxlsxwriter. */
     char *tmpdir;
+
+    /** Allow ZIP64 extensions when creating the xlsx file zip container. */
+    uint8_t use_zip64;
+
 } lxw_workbook_options;
 
 /**
@@ -269,7 +282,6 @@ typedef struct lxw_workbook {
     uint8_t has_png;
     uint8_t has_jpeg;
     uint8_t has_bmp;
-    uint8_t use_zip64;
 
     lxw_hash_table *used_xf_formats;
 
@@ -314,30 +326,37 @@ lxw_workbook *workbook_new(const char *filename);
  * additional options to be set.
  *
  * @code
- *    lxw_workbook_options options = {.constant_memory = 1,
- *                                    .tmpdir = "C:\\Temp"};
+ *    lxw_workbook_options options = {.constant_memory = LXW_TRUE,
+ *                                    .tmpdir = "C:\\Temp",
+ *                                    .use_zip64 = LXW_FALSE};
  *
  *    lxw_workbook  *workbook  = workbook_new_opt("filename.xlsx", &options);
  * @endcode
  *
  * The options that can be set via #lxw_workbook_options are:
  *
- * - `constant_memory`: Reduces the amount of data stored in memory so that
- *   large files can be written efficiently.
+ * - `constant_memory`: This option reduces the amount of data stored in
+ *   memory so that large files can be written efficiently. This option is off
+ *   by default. See the note below for limitations when this mode is on.
  *
- *   @note In this mode a row of data is written and then discarded when a
- *   cell in a new row is added via one of the `worksheet_write_*()`
- *   functions. Therefore, once this option is active, data should be written in
- *   sequential row order. For this reason the `worksheet_merge_range()`
- *   doesn't work in this mode. See also @ref ww_mem_constant.
- *
- * - `tmpdir`: libxlsxwriter stores workbook data in temporary files prior
- *   to assembling the final XLSX file. The temporary files are created in the
+ * - `tmpdir`: libxlsxwriter stores workbook data in temporary files prior to
+ *   assembling the final XLSX file. The temporary files are created in the
  *   system's temp directory. If the default temporary directory isn't
  *   accessible to your application, or doesn't contain enough space, you can
- *   specify an alternative location using the `tmpdir` option.*
+ *   specify an alternative location using the `tmpdir` option.
  *
- * See @ref working_with_memory for more details.
+ * - `use_zip64`: Make the zip library use ZIP64 extensions when writing very
+ *   large xlsx files to allow the zip container, or individual XML files
+ *   within it, to be greater than 4 GB. See [ZIP64 on Wikipedia][zip64_wiki]
+ *   for more information. This option is off by default.
+ *
+ *   [zip64_wiki]: https://en.wikipedia.org/wiki/Zip_(file_format)#ZIP64
+ *
+ * @note In `constant_memory` mode a row of data is written and then discarded
+ * when a cell in a new row is added via one of the `worksheet_write_*()`
+ * functions. Therefore, once this option is active, data should be written in
+ * sequential row order. For this reason the `worksheet_merge_range()` doesn't
+ * work in this mode. See also @ref ww_mem_constant.
  *
  */
 lxw_workbook *workbook_new_opt(const char *filename,
@@ -827,20 +846,6 @@ lxw_chartsheet *workbook_get_chartsheet_by_name(lxw_workbook *workbook,
 lxw_error workbook_validate_sheet_name(lxw_workbook *workbook,
                                        const char *sheetname);
 
-/**
- * @brief Allow ZIP64 extensions when creating the xlsx file zip container.
- *
- * @param workbook Pointer to a lxw_workbook instance.
- *
- * Use ZIP64 extensions when writing the xlsx file zip container to allow
- * files greater than 4 GB.
- *
- * @code
- *     workbook_use_zip64(workbook);
- * @endcode
- */
-void workbook_use_zip64(lxw_workbook *workbook);
-
 void lxw_workbook_free(lxw_workbook *workbook);
 void lxw_workbook_assemble_xml_file(lxw_workbook *workbook);
 void lxw_workbook_set_default_xf_indices(lxw_workbook *workbook);
diff --git a/src/workbook.c b/src/workbook.c
index a1ae989f..30de321b 100644
--- a/src/workbook.c
+++ b/src/workbook.c
@@ -1470,6 +1470,7 @@ workbook_new_opt(const char *filename, lxw_workbook_options *options)
     if (options) {
         workbook->options.constant_memory = options->constant_memory;
         workbook->options.tmpdir = lxw_strdup(options->tmpdir);
+        workbook->options.use_zip64 = options->use_zip64;
     }
 
     return workbook;
@@ -1728,7 +1729,8 @@ workbook_close(lxw_workbook *self)
 
     /* Create a packager object to assemble sub-elements into a zip file. */
     packager = lxw_packager_new(self->filename,
-                                self->options.tmpdir, self->use_zip64);
+                                self->options.tmpdir,
+                                self->options.use_zip64);
 
     /* If the packager fails it is generally due to a zip permission error. */
     if (packager == NULL) {
@@ -2154,12 +2156,3 @@ workbook_validate_sheet_name(lxw_workbook *self, const char *sheetname)
 
     return LXW_NO_ERROR;
 }
-
-/*
- * Allow ZIP64 extensions when creating the xlsx file zip container.
- */
-void
-workbook_use_zip64(lxw_workbook *workbook)
-{
-    workbook->use_zip64 = LXW_TRUE;
-}
diff --git a/test/functional/src/test_optimize26.c b/test/functional/src/test_optimize26.c
index 852cd023..a49e86ff 100644
--- a/test/functional/src/test_optimize26.c
+++ b/test/functional/src/test_optimize26.c
@@ -11,7 +11,7 @@
 
 int main() {
 
-    lxw_workbook_options options = {1, NULL};
+    lxw_workbook_options options = {LXW_TRUE, NULL, LXW_FALSE};
 
     /* Use deprecated constructor for testing. */
     lxw_workbook  *workbook  = new_workbook_opt("test_optimize26.xlsx", &options);
