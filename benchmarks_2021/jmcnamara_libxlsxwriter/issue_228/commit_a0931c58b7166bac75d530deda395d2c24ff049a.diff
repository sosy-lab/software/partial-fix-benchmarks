diff --git a/include/xlsxwriter/packager.h b/include/xlsxwriter/packager.h
index 17004efb..74c5848a 100644
--- a/include/xlsxwriter/packager.h
+++ b/include/xlsxwriter/packager.h
@@ -54,6 +54,7 @@ typedef struct lxw_packager {
     char *filename;
     char *buffer;
     char *tmpdir;
+    uint8_t use_zip64;
 
 } lxw_packager;
 
@@ -64,7 +65,8 @@ extern "C" {
 #endif
 /* *INDENT-ON* */
 
-lxw_packager *lxw_packager_new(const char *filename, char *tmpdir);
+lxw_packager *lxw_packager_new(const char *filename, char *tmpdir,
+                               uint8_t use_zip64);
 void lxw_packager_free(lxw_packager *packager);
 lxw_error lxw_create_package(lxw_packager *self);
 
diff --git a/include/xlsxwriter/workbook.h b/include/xlsxwriter/workbook.h
index 10e99fad..b2e08255 100644
--- a/include/xlsxwriter/workbook.h
+++ b/include/xlsxwriter/workbook.h
@@ -269,6 +269,7 @@ typedef struct lxw_workbook {
     uint8_t has_png;
     uint8_t has_jpeg;
     uint8_t has_bmp;
+    uint8_t use_zip64;
 
     lxw_hash_table *used_xf_formats;
 
@@ -826,6 +827,20 @@ lxw_chartsheet *workbook_get_chartsheet_by_name(lxw_workbook *workbook,
 lxw_error workbook_validate_sheet_name(lxw_workbook *workbook,
                                        const char *sheetname);
 
+/**
+ * @brief Allow ZIP64 extensions when creating the xlsx file zip container.
+ *
+ * @param workbook Pointer to a lxw_workbook instance.
+ *
+ * Use ZIP64 extensions when writing the xlsx file zip container to allow
+ * files greater than 4 GB.
+ *
+ * @code
+ *     workbook_use_zip64(workbook);
+ * @endcode
+ */
+void workbook_use_zip64(lxw_workbook *workbook);
+
 void lxw_workbook_free(lxw_workbook *workbook);
 void lxw_workbook_assemble_xml_file(lxw_workbook *workbook);
 void lxw_workbook_set_default_xf_indices(lxw_workbook *workbook);
diff --git a/src/packager.c b/src/packager.c
index 8637e9d3..37c4a4e5 100644
--- a/src/packager.c
+++ b/src/packager.c
@@ -76,7 +76,7 @@ _open_zipfile_win32(const char *filename)
  * Create a new packager object.
  */
 lxw_packager *
-lxw_packager_new(const char *filename, char *tmpdir)
+lxw_packager_new(const char *filename, char *tmpdir, uint8_t use_zip64)
 {
     lxw_packager *packager = calloc(1, sizeof(lxw_packager));
     GOTO_LABEL_ON_MEM_ERROR(packager, mem_error);
@@ -89,6 +89,7 @@ lxw_packager_new(const char *filename, char *tmpdir)
     GOTO_LABEL_ON_MEM_ERROR(packager->filename, mem_error);
 
     packager->buffer_size = LXW_ZIP_BUFFER_SIZE;
+    packager->use_zip64 = use_zip64;
 
     /* Initialize the zip_fileinfo struct to Jan 1 1980 like Excel. */
     packager->zipfile_info.tmz_date.tm_sec = 0;
@@ -1066,7 +1067,8 @@ _add_file_to_zip(lxw_packager *self, FILE * file, const char *filename)
                                     NULL, 0, NULL, 0, NULL,
                                     Z_DEFLATED, Z_DEFAULT_COMPRESSION, 0,
                                     -MAX_WBITS, DEF_MEM_LEVEL,
-                                    Z_DEFAULT_STRATEGY, NULL, 0, 0, 0, 0);
+                                    Z_DEFAULT_STRATEGY, NULL, 0, 0, 0,
+                                    self->use_zip64);
 
     if (error != ZIP_OK) {
         LXW_ERROR("Error adding member to zipfile");
@@ -1119,7 +1121,8 @@ _add_buffer_to_zip(lxw_packager *self, unsigned char *buffer,
                                     NULL, 0, NULL, 0, NULL,
                                     Z_DEFLATED, Z_DEFAULT_COMPRESSION, 0,
                                     -MAX_WBITS, DEF_MEM_LEVEL,
-                                    Z_DEFAULT_STRATEGY, NULL, 0, 0, 0, 0);
+                                    Z_DEFAULT_STRATEGY, NULL, 0, 0, 0,
+                                    self->use_zip64);
 
     if (error != ZIP_OK) {
         LXW_ERROR("Error adding member to zipfile");
diff --git a/src/workbook.c b/src/workbook.c
index bd2aa254..66902160 100644
--- a/src/workbook.c
+++ b/src/workbook.c
@@ -1727,7 +1727,9 @@ workbook_close(lxw_workbook *self)
     _add_chart_cache_data(self);
 
     /* Create a packager object to assemble sub-elements into a zip file. */
-    packager = lxw_packager_new(self->filename, self->options.tmpdir);
+    packager = lxw_packager_new(self->filename,
+                                self->options.tmpdir,
+                                self->use_zip64);
 
     /* If the packager fails it is generally due to a zip permission error. */
     if (packager == NULL) {
@@ -2132,3 +2134,12 @@ workbook_validate_sheet_name(lxw_workbook *self, const char *sheetname)
 
     return LXW_NO_ERROR;
 }
+
+/*
+ * Allow ZIP64 extensions when creating the xlsx file zip container.
+ */
+void
+workbook_use_zip64(lxw_workbook *workbook)
+{
+    workbook->use_zip64 = LXW_TRUE;
+}
