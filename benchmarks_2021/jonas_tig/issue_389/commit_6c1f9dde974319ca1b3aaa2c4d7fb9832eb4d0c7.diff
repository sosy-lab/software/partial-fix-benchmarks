diff --git a/include/tig/status.h b/include/tig/status.h
index 8388d40c..466e8ee1 100644
--- a/include/tig/status.h
+++ b/include/tig/status.h
@@ -43,11 +43,7 @@ bool status_stage_info_(char *buf, size_t bufsize,
 	status_stage_info_(buf, sizeof(buf), type, status)
 extern struct view status_view;
 
-static inline void
-open_status_view(struct view *prev, enum open_flags flags)
-{
-	open_view(prev, &status_view, flags);
-}
+void open_status_view(struct view *prev, bool untracked_only, enum open_flags flags);
 
 #endif
 /* vim: set ts=8 sw=8 noexpandtab: */
diff --git a/src/main.c b/src/main.c
index 9e8fc90f..797dbd04 100644
--- a/src/main.c
+++ b/src/main.c
@@ -547,10 +547,9 @@ main_request(struct view *view, enum request request, struct line *line)
 		if (line->type == LINE_STAT_UNSTAGED
 		    || line->type == LINE_STAT_STAGED)
 			open_stage_view(view, NULL, line->type, flags);
-		else if (line->type == LINE_STAT_UNTRACKED) {
-			open_status_view(view, flags);
-			status_exists(&status_view, NULL, LINE_STAT_UNTRACKED);
-		} else
+		else if (line->type == LINE_STAT_UNTRACKED)
+			open_status_view(view, true, flags);
+		else
 			open_diff_view(view, flags);
 		break;
 
diff --git a/src/stage.c b/src/stage.c
index b70080f6..2b60e483 100644
--- a/src/stage.c
+++ b/src/stage.c
@@ -529,6 +529,7 @@ stage_open(struct view *view, enum open_flags flags)
 		break;
 
 	case LINE_STAT_UNTRACKED:
+		watch_register(&view->watch, WATCH_INDEX_UNTRACKED);
 		argv = file_argv;
 		view->encoding = get_path_encoding(stage_status.old.name, default_encoding);
 		break;
diff --git a/src/status.c b/src/status.c
index 084713c7..0e091a1e 100644
--- a/src/status.c
+++ b/src/status.c
@@ -24,6 +24,7 @@
 #include "tig/git.h"
 #include "tig/watch.h"
 #include "tig/status.h"
+#include "tig/main.h"
 #include "tig/stage.h"
 
 /*
@@ -31,6 +32,17 @@
  */
 
 static char status_onbranch[SIZEOF_STR];
+static bool show_untracked_only = false;
+
+void
+open_status_view(struct view *prev, bool untracked_only, enum open_flags flags)
+{
+	if (show_untracked_only != untracked_only) {
+		show_untracked_only = untracked_only;
+		flags |= OPEN_RELOAD;
+	}
+	open_view(prev, &status_view, flags);
+}
 
 /* This should work even for the "On branch" line. */
 static inline bool
@@ -148,11 +160,15 @@ status_run(struct view *view, const char *argv[], char status, enum line_type ty
 			watch_apply(&view->watch, WATCH_INDEX_STAGED_NO);
 		else if (type == LINE_STAT_UNSTAGED)
 			watch_apply(&view->watch, WATCH_INDEX_UNSTAGED_NO);
+		else if (type == LINE_STAT_UNTRACKED)
+			watch_apply(&view->watch, WATCH_INDEX_UNTRACKED_NO);
 	} else {
 		if (type == LINE_STAT_STAGED)
 			watch_apply(&view->watch, WATCH_INDEX_STAGED_YES);
 		else if (type == LINE_STAT_UNSTAGED)
 			watch_apply(&view->watch, WATCH_INDEX_UNSTAGED_YES);
+		else if (type == LINE_STAT_UNTRACKED)
+			watch_apply(&view->watch, WATCH_INDEX_UNTRACKED_YES);
 	}
 
 	io_done(&io);
@@ -370,8 +386,8 @@ status_open(struct view *view, enum open_flags flags)
 
 	update_index();
 
-	if (!status_run(view, staged_argv, staged_status, LINE_STAT_STAGED) ||
-	    !status_run(view, status_diff_files_argv, 0, LINE_STAT_UNSTAGED) ||
+	if ((!show_untracked_only && !status_run(view, staged_argv, staged_status, LINE_STAT_STAGED)) ||
+	    (!show_untracked_only && !status_run(view, status_diff_files_argv, 0, LINE_STAT_UNSTAGED)) ||
 	    !status_read_untracked(view))
 		return error("Failed to load status data");
 
@@ -738,6 +754,9 @@ status_request(struct view *view, enum request request, struct line *line)
 		return request;
 	}
 
+	if (show_untracked_only && view->parent == &main_view && !main_status_exists(view->parent, LINE_STAT_UNTRACKED))
+		return REQ_VIEW_CLOSE;
+
 	refresh_view(view);
 
 	return REQ_NONE;
diff --git a/src/tig.c b/src/tig.c
index 141432c6..515373f1 100644
--- a/src/tig.c
+++ b/src/tig.c
@@ -224,7 +224,7 @@ view_driver(struct view *view, enum request request)
 		open_blob_view(view, OPEN_DEFAULT);
 		break;
 	case REQ_VIEW_STATUS:
-		open_status_view(view, OPEN_DEFAULT);
+		open_status_view(view, false, OPEN_DEFAULT);
 		break;
 	case REQ_VIEW_STAGE:
 		open_stage_view(view, NULL, 0, OPEN_DEFAULT);
diff --git a/src/watch.c b/src/watch.c
index ad9752a0..14f85b2f 100644
--- a/src/watch.c
+++ b/src/watch.c
@@ -112,7 +112,7 @@ watch_index_handler(struct watch_handler *handler, enum watch_event event, enum
 
 	if (!check_file_mtime(&handler->last_modified, "%s/index", repo.git_dir) ||
 	    event == WATCH_EVENT_SWITCH_VIEW ||
-	    !index_diff(&diff, false, false))
+	    !index_diff(&diff, opt_show_untracked, false))
 		return WATCH_NONE;
 
 	if (check & WATCH_INDEX_STAGED) {
@@ -129,6 +129,13 @@ watch_index_handler(struct watch_handler *handler, enum watch_event event, enum
 			changed |= WATCH_INDEX_UNSTAGED_NO;
 	}
 
+	if (check & WATCH_INDEX_UNTRACKED) {
+		if (diff.untracked)
+			changed |= WATCH_INDEX_UNTRACKED_YES;
+		else
+			changed |= WATCH_INDEX_UNTRACKED_NO;
+	}
+
 	if (changed)
 		handler->last_modified = time(NULL);
 
diff --git a/test/main/untracked-test b/test/main/untracked-test
index d5570753..5c74fa9a 100755
--- a/test/main/untracked-test
+++ b/test/main/untracked-test
@@ -53,15 +53,15 @@ $YYY_MM_DD_HH_MM +0000 Unknown    o Staged changes
 
 [main] Untracked changes                                                    100%
 On branch master
-Changes to be committed:
-M .j
-Changes not staged for commit:
-M a
-M b.c
-M e/f
-M g h
 Untracked files:
 ? z
 
-[status] Press u to stage all files for addition                            100%
+
+
+
+
+
+
+
+[status] Nothing to update                                                  100%
 EOF
