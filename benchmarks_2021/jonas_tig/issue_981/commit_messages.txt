Fix cursor position after "Move to parent" in blame view

Fixes #973
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in UTF-8 mode by
default.

Use Unicode drawing characters in UTF-8 mode for the vertical display
separator and the line number separator.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in UTF-8 mode by
default.

Use Unicode drawing characters in UTF-8 mode for the vertical display
separator and the line number separator.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in UTF-8 mode by
default.

Use Unicode drawing characters in UTF-8 mode for the vertical display
separator and the line number separator.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in UTF-8 mode by
default.

Use Unicode drawing characters in UTF-8 mode for the vertical display
separator and the line number separator.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Use the UTF-8 vertical line character in the UTF-8 mode. Putty doesn't
show VT100 line drawing characters in the UTF-8 mode by default. Putty
0.70 and older doesn't even have that option.

For the vertical display separator, use space as background in the UTF-8
mode, then output the vertical line character on every line.

For the line number separator, imitate the graph drawing code. Use
draw_graphic() in the DEFAULT mode only, draw_chars() otherwise.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in the UTF-8 mode by
default. Putty 0.70 and older doesn't even have that option.

For the vertical display separator, output the vertical line character on
every line.

For the line number separator, use code similar to the graph drawing
code. Use draw_graphic() in the DEFAULT mode only, draw_chars()
otherwise.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in the UTF-8 mode by
default. Putty 0.70 and older doesn't even have that option.

For the vertical display separator, use '|' as background, then output
the vertical line character on every line.

For the line number separator, use draw_graphic() in the default mode
only. Use draw_chars() in the ASCII and UTF-8 modes.

Add a test for vertical lines. The display separator is always captured
as a line of '|' characters. The line number separators are captured
correctly. 'x' corresponds to ACS_VLINE.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics

Putty doesn't show VT100 line drawing characters in the UTF-8 mode by
default. Putty 0.70 and older doesn't even have that option.

For the vertical display separator, output the vertical line character on
every line. Use wbkgdset() instead of wbkgd() as the code takes care of
setting the window contents.

For the line number separator, use draw_graphic() in the default mode
only. Use draw_chars() in the ASCII and UTF-8 modes.

Add a test for vertical lines. The display separator is always captured
as a line of '|' characters. The line number separators are captured
correctly. 'x' corresponds to ACS_VLINE.

Closes #981
Fix vertical lines in Putty with UTF-8 graphics (#983)

Putty doesn't show VT100 line drawing characters in the UTF-8 mode by
default. Putty 0.70 and older doesn't even have that option.

For the vertical display separator, output the vertical line character on
every line. Use wbkgdset() instead of wbkgd() as the code takes care of
setting the window contents.

For the line number separator, use draw_graphic() in the default mode
only. Use draw_chars() in the ASCII and UTF-8 modes.

Add a test for vertical lines. The display separator is always captured
as a line of '|' characters. The line number separators are captured
correctly. 'x' corresponds to ACS_VLINE.

Closes #981
