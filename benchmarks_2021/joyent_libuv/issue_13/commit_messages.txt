Implement and add test for oio_now()
API Change: Remove unnecessary params from oio_tcp_handle_accept()

closes #13.
API Change: Remove unnecessary params from oio_tcp_handle_accept()

closes #13.
oio_tcp_handle_accept() does not require initialization of client

closes #13.
