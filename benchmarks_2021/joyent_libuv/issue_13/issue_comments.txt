oio_accept() should require oio_tcp_handle_init() called before
test changed in 2ba25c8249f562d37c4dcc111111e55f647d08a3

Isn't it possible to do call tcp_handle_init implicity from oio_accept on linux?
On windows this complicates stuff because socket options are set and iocp association is made in tcp_handle_init.
That work is wasted when you call oio_accept because the accepted socket is a different one.

ok - i can do.

did a force push just then. the actual fixing commit is c3eda7a4daea6036eecaac5f268a6b0f86cb9b82
