Destination Port Inferred Incorrectly When Assigning $du
If the $du is referring to a SRV record, chances are that 5060 is wrong too as the SRV may have any port.

It is only about the returned value of the variable $dp. The port is not set in the outbound proxy uri.

The logic is, if the $du has no port value, then return 5060 -- probably some old code relying on default value for port -- relevant piece of code inside the pv module:

```
    } else if(param->pvn.u.isname.name.n==2) /* port */ {
        if(uri.port.s==NULL)
            return pv_get_5060(msg, param, res);
        return pv_get_strintval(msg, param, res, &uri.port, (int)uri.port_no);
```

So I pushed a patch to return 5061 for tls, thinking that the target was to have a valid value if one tries to use $dp to build a new URI.

On the other hand, I think it may be better to make it return $null if port is not set in the uri.

I agree, returning $NULL if the port is not set is an improvement.

Or maybe adding a new variable like $dup (and $rup given it is the same for $rp), which will return based on uri value and $dp/$rp stay as expected destination port... just ideas...

Great point @oej - I hadn't considered the case of SRV records which may change that.

@miconda I could go for something like that. We could also just be very explicit about it, and rather than create a pseudo-var for something that may be wrong for reasons @oej mentioned, maybe just provide a function in core, like `default_port_for_transport("tls")`. This way it's very clear that it is not "based" off of the URI, but rather it just provides an opportunity to access the default port that a transport will use. Then, my case would be satisfied by `default_port_for_transport("$dP");` I have no idea if this is a better/worse solution, but I do like that it avoids any implication that it's actually the destination port for the request.

If going the `$dp` (or a similar pseudo var) route, it'd probably help to also toss some help text on the pseudovariables page. Something like:

> In cases where the destination URI does not contain a port number, `$dp` is only a guess at the destination port that will be used given the transport selected. Other factors (SRV record lookups, - maybe others?) could result in the request being sent to a different port which won't be known until Kamailio attempts to send the request.

It may be that {uri.port} transformation return empty string or 0 when the port is not in an uri, like:

```
$(du{uri.port})
```

For documentation, feel free to add the content you think it's helpful in the wiki portal where the variables are documented.
