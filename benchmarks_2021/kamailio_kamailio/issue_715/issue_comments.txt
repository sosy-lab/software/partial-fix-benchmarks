4.4.x TOPOS - bad sip message or missing Contact hdr on OPTIONS messages
Can you try with the patch referenced above and see if works fine for OPTIONS? If all ok, then I will backport.

The patch has worked.
Thank you!

I reopen this issue as the same happens on BYE requests.
BYE requests (according to my understanding of RFC - page 161) doesn't require Contact header.

Do you mind adding METHOD_BYE in the condition? (it seems that Contact is only mandatory on INVITEs).

Pushed a patch to check only for INVITE. If the issue is still there, reopen.
