Segmentation fault when using http_hdr with http_async_client
Hi,
is the pv module loaded before the http_async_client module? If not can you try?

Sorry, I forgot to say it but yes I verified that the pv module is loaded. Actually I added traces in the mod_init function of the http_async_client_module and when I remove my faulty line I can see the full init taking place including the pv_api being filled.
When I put back my faulty line, the traces I added are not displayed the seg fault occuring before the init.

If you get a segfault, can you paste here the backtrace taken with gdb from corefile? It will reveal the execution stack and the point where it crashes.

Here you are:
#0  0x0000000000000000 in ?? ()
#1  0x00007f1a90b8f3b3 in w_pv_parse_hdr_name (sp=0x7f1a9f82b128, in=0x7ffcf7e142b0) at http_async_client_mod.c:681
#2  0x00000000004d382f in pv_parse_spec2 (in=in@entry=0x7f1a9f82b110, e=e@entry=0x7f1a9f82b128, silent=silent@entry=0) at pvapi.c:873
#3  0x00000000004d5e61 in pv_cache_add (name=name@entry=0x7ffcf7e143b0) at pvapi.c:282
#4  0x00000000004d65cb in pv_cache_get (name=name@entry=0x9ffbb0 <s_tmp>) at pvapi.c:356
#5  0x0000000000668025 in yyparse () at cfg.y:2596
#6  0x000000000042362a in main (argc=6, argv=0x7ffcf7e14ef8) at main.c:2119

(gdb) up
#1  0x00007f1a90b8f3b3 in w_pv_parse_hdr_name (sp=0x7f1a9f82b128, in=0x7ffcf7e142b0) at http_async_client_mod.c:681
681             return pv_api.parse_hdr_name(sp, in);

Note that I'm not using the latest version of http_async_client yet.
I verifyed that sp and in are correct but actually pv_api is not:
print pv_api
$1 = {get_reason = 0x0, get_hdr = 0x0, parse_hdr_name = 0x0, get_status = 0x0, get_msg_body = 0x0, get_body_size = 0x0, get_msg_buf = 0x0, get_msg_len = 0x0}

Can you provide also the output from gdb for next commands:

```
frame 1
info locals
list
```

Results:
(gdb) frame 1
#1  0x00007f1a90b8f3b3 in w_pv_parse_hdr_name (sp=0x7f1a9f82b128, in=0x7ffcf7e142b0) at http_async_client_mod.c:681
681             return pv_api.parse_hdr_name(sp, in);
(gdb) info locals
No locals.
(gdb) list
676     AH_WRAP_GET_PV(ah_get_body_size,   get_body_size)
677     AH_WRAP_GET_PV(ah_get_msg_buf,     get_msg_buf)
678     AH_WRAP_GET_PV(ah_get_msg_len,     get_msg_len)
679
680     static int w_pv_parse_hdr_name(pv_spec_p sp, str *in) {
681             return pv_api.parse_hdr_name(sp, in);
682     }
683
684     static int ah_get_ok(struct sip_msg *msg, pv_param_t *param, pv_value_t *res) {
685             if (ah_reply) {

Hi,
we could reproduce the issue. When using instructions as

```
xlog("$http_hdr(Content-Type)");
```

the PV's internal name is parsed at runtime, and the PV API has been initialized already. When using

```
 $var(ct) = $http_hdr(Content-Type)
```

it's parsed at start time, before the API was initialized. I sent a patch that does the init sooner, and should fix the issue, can you confirm it fixes the issue for you?

Unless there is still an issue despite the fix, I'm going to close this issue.
@miconda, I'm planning to backport it to 4.4 branch too, but this patch will now make kamailio fail to start if the **http_async_client** module is loaded before **pv**. This is the way it always have been documented, but until now it worked no matter what the loading order was. Is it ok to backport it anyway?

For me it's ok to backport.

Camille, your fix works for me as well!
Many thanks
