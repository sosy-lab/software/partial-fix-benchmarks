diff --git a/misc-utils/wipefs.8 b/misc-utils/wipefs.8
index 01081fa8e7..cdc6b28119 100644
--- a/misc-utils/wipefs.8
+++ b/misc-utils/wipefs.8
@@ -77,6 +77,14 @@ Display help text and exit.
 .BR \-J , " \-\-json"
 Use JSON output format.
 .TP
+\fB\-\-lock\fR[=\fImode\fR]
+Use exclusive BSD lock for device or file it operates.  The optional argument
+\fImode\fP can be \fByes\fR, \fBno\fR (or 1 and 0) or \fBnonblock\fR.  If the \fImode\fR
+argument is omitted, it defaults to \fB"yes"\fR.  This option overwrites
+environment variable \fB$LOCK_BLOCK_DEVICE\fR.  The default is not to use any
+lock at all, but it's recommended to avoid collisions with udevd or other
+tools.
+.TP
 .BR \-i , " \-\-noheadings"
 Do not print a header line.
 .TP
@@ -115,6 +123,8 @@ Display version information and exit.
 .SH ENVIRONMENT
 .IP LIBBLKID_DEBUG=all
 enables libblkid debug output.
+.IP LOCK_BLOCK_DEVICE=<mode>
+use exclusive BSD lock.  The mode is "1" or "0".  See \fB\-\-lock\fR for more details.
 .SH EXAMPLE
 .TP
 .B wipefs /dev/sda*
diff --git a/misc-utils/wipefs.c b/misc-utils/wipefs.c
index 2a16d62229..77dfe6a28f 100644
--- a/misc-utils/wipefs.c
+++ b/misc-utils/wipefs.c
@@ -64,6 +64,7 @@ struct wipe_desc {
 struct wipe_control {
 	char		*devname;
 	const char	*type_pattern;		/* -t <pattern> */
+	const char	*lockmode;
 
 	struct libscols_table *outtab;
 	struct wipe_desc *offsets;		/* -o <offset> -o <offset> ... */
@@ -546,6 +547,12 @@ static int do_wipe(struct wipe_control *ctl)
 	if (!pr)
 		return -errno;
 
+	if (blkdev_lock(blkid_probe_get_fd(pr),
+			ctl->devname, ctl->lockmode) != 0) {
+		blkid_free_probe(pr);
+		return -1;
+	}
+
 	if (ctl->backup) {
 		const char *home = getenv ("HOME");
 		char *tmp = xstrdup(ctl->devname);
@@ -655,6 +662,8 @@ usage(void)
 	puts(_(" -p, --parsable      print out in parsable instead of printable format"));
 	puts(_(" -q, --quiet         suppress output messages"));
 	puts(_(" -t, --types <list>  limit the set of filesystem, RAIDs or partition tables"));
+	printf(
+	     _("     --lock[=<mode>] use exclusive device lock (%s, %s or %s)\n"), "yes", "no", "nonblock");
 
 	printf(USAGE_HELP_OPTIONS(21));
 
@@ -676,12 +685,15 @@ main(int argc, char **argv)
 	struct wipe_control ctl = { .devname = NULL };
 	int c;
 	char *outarg = NULL;
-
+	enum {
+		OPT_LOCK = CHAR_MAX + 1,
+	};
 	static const struct option longopts[] = {
 	    { "all",       no_argument,       NULL, 'a' },
 	    { "backup",    no_argument,       NULL, 'b' },
 	    { "force",     no_argument,       NULL, 'f' },
 	    { "help",      no_argument,       NULL, 'h' },
+	    { "lock",      optional_argument, NULL, OPT_LOCK },
 	    { "no-act",    no_argument,       NULL, 'n' },
 	    { "offset",    required_argument, NULL, 'o' },
 	    { "parsable",  no_argument,       NULL, 'p' },
@@ -745,7 +757,14 @@ main(int argc, char **argv)
 		case 't':
 			ctl.type_pattern = optarg;
 			break;
-
+		case OPT_LOCK:
+			ctl.lockmode = "1";
+			if (optarg) {
+				if (*optarg == '=')
+					optarg++;
+				ctl.lockmode = optarg;
+			}
+			break;
 		case 'h':
 			usage();
 		case 'V':
