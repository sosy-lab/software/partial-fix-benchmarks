Search replace ( _ssr ) broken when more than one match is hit
Not sure why this was closed,  considering it's not fixed?

Testcase says:
TC("abcda", _ssr["abcda";"a";"e"])   //issue #302

When the result should be "ebcde" , so probably makes sense to track fixing it?

Oops!
It was probably closed due to dyslexia :-)

I am changing the label on this issue from "bug" to "feature".
AFAICT, when _ssr was implemented, it worked only when there was 1 hit.
This has been the case since implementation.
At that time, it also required a "comma" before the last argument, as in

```
_ssr[ "this"; "is"; ,"at" ]
"that"
  _ssr[ "this" ;"is"; "at" ]
undefined error
```

On Tue, 24 Feb 2015 16:02:46 PST Tom Szczesny notifications@github.com wrote:

> I am changing the label on this issue from "bug" to "feature".
> AFAICT, when _ssr was implemented, it worked only when there was 1 hit.
> This has been the case since implementation.
> At that time, it also required a "comma" before the last argument, as in
> 
> ```
> _ssr[ "this"; "is"; ,"at" ]
> "that"
>   _ssr[ "this" ;"is"; "at" ]
> undefined error
> ```

Not in 3.2:

```
  _ssr["this and that"; "th"; "m"]
"mis and mat"
  _ssr["this and that"; "h?"; "y"]
"tys and tyt"
```

If there is no match, the original string is returned.

Sorry if I was not clear.
I have no issue with how _ssr was implemented in either 3.2 or 2.8

The original _ssr implementation **in Kona** (late 2010) appears to be:
1) an incomplete implementation (handles 1 match, but not two matches).
2) a non-conformant implemenation (requiring an extra "comma" to work at all)

When issue 250 was closed on April 28, 2014, the Kona implementation became conformant (in the 1-match case).  **Kona** no longer required the extra "comma" to work.  However, it was not complete.

An implementation of _ssr fhat works with 2 matches was never done **in Kona**.

Just because it has been broken since the original implementation 2010 doesn't mean that it's not a bug, considering the behavior differs from other K implementations in a way that doesn't really make sense.

A case could certainly be made to consider this a bug.  However, I'm trying to be consistent with the issue classification guideline from Kevin Lawler on May 10, 2011 (see issue #110):

"for kona I'm using - features: unimplemented functionality; bugs: broken aspects of existing implementation, esp. logic errors or regressions"

As a documentation note:  There are at least 2 previous issues related to _ssr: #250 and #145.

Note that issue #250 was addressed by making some adjustments to kx.c in function
Z V ex_(V a, I r)
Although this issue could possibly be addressed in a similar manner, there seems to be a more fundamental problem.

In k2.8 (and probably k3.2) _ssr is defined as the function

```
f:{if[_n~x;:_n]; 
   i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x;
   ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
```

in k2.8

```
  f:{if[_n~x;:_n]; 
     i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x;
     ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
  f["abcda";"a";"e"]
"ebcde"
```

in Kona

```
  f:{if[_n~x;:_n]; 
     i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x;
     ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
  f["abcda";"a";"e"]
  f["abcd";"a";"e"]
```

Kona gives no response at all (to either of the last 2 commands).

The first problem with the underlying function comes in the definition process.  In Kona:

```
  f:{if[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x;
     ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}

{i_f[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x; 
,/:[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
```

Note that when Kona repeats back the function definition, it has placed an additional underbar in the reserved word "if".  This is due to a faulty resolution to issue #288 (Named Recursion).  The "f" in "if" is being mistaken for the name of the function.

Most likely, this is also causing the problem described in issue #305.

Correcting that (by renaming the function to "g"), we now get a SIGSEGV:

```
 g:{if[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x; 
     ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}

{if[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x; 
,/:[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}

  g["abcda";"a";"e"]
rlwrap: warning: k killed by SIGSEGV.
```

Looks like the problem is in the 3rd (and final statement).  Removing that statement yields:
in k2.8

```
  h:{if[_n~x;:_n]; 1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x}
  h["abcda";"a"]
1 3
```

in Kona

```
  h:{if[_n~x;:_n]; 1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x}
  h["abcda";"a"]
1 3
```

Some confirmation of location:
in k2.8

```
  j:{i:0 4; ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
  j["abcda";"a";"e"]
"ebcde"
```

in Kona

```
  j:{i:0 4; ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
  j["abcda";"a";"e"]
rlwrap: warning: k killed by SIGSEGV.
```

One of the problems:
in k2.8

```
  @[ "abcda"; 0 4; :[;"e"] ]
"ebcde"
```

In Kona

```
  @[ "abcda"; 0 4; :[;"e"] ]
type error
>
```

On Wed, 25 Feb 2015 12:36:27 PST Tom Szczesny notifications@github.com wrote:

>   @["abcda";0 4;:[;"e"]]

Is this even valid (documented) syntax?

This works fine in kona:

```
  @["ABCDE";0 4;:;"e"]
"eBCDe"
```

Simpler.
In k2.8

```
  f: :[;"e"]
  f
:[;"e"]
```

in Kona

```
  f: :[;"e"]
type error
> 
```

On Wed, 25 Feb 2015 12:50:29 PST Tom Szczesny notifications@github.com wrote:

> ```
>   f: :[;"e"]
> ```

Doesn't seem documented in the ref manual.

In Kona:

```
  +[;1]
+[;1]
  *[;1]
*[;1]
  :[;1]
type error
>  
```

Looks like a projection.

On Wed, 25 Feb 2015 06:54:42 PST Tom Szczesny notifications@github.com wrote:

> "for kona I'm using - features: unimplemented functionality; bugs: broken
> aspects of existing implementation, esp. logic errors or regressions"

This is fine with me.

_ss and _ssr use a very limited pattern matching. Just ?[^-]
Proper regexp support would be very handy! pcre is very
popular but it is quite huge. re2 is my favorite but it is in
C++.  BSD's regcomp seems about the right size.

The form you suggested also works in k2.8

```
  @["ABCDE";0 4;:;"e"]
"eBCDe"
```

So, it would seem that we could use this form as a replacement.
However, it does not work in either language.
in k2.8

```
  h:{if[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x; 
     ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:;z];@[x;i;:;z]]}

  h["abcda";"a";"e"]
length error
{if[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x;
,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:;z];@[x;i;:;z]]}
                          ^
>   
```

in Kona

```
    h:{if[_n~x;:_n]; i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x; 
       ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:;z];@[x;i;:;z]]}

  h["abcda";"a";"e"]
(""
 ,"a"
 "bcd"
 ,"a"
 "")
type error
>  
```

Currently in Kona

```
  :[;1]
type error
> 
```

The problem appears to be that the parser is treating the colon as 
the leading character in a "conditional evaluation" phrase (page 167 of reference manual),
instead of a verb in a "projection" prhase (page 156 of reference manual). 

**:[;1]** is now a valid statement in Kona.
However, in k2.8

```
  @[ "abcda"; 0 4; :[;"e"] ]
"ebcde"
```

but, now, in Kona

```
  @[ "abcda"; 0 4; :[;"e"] ]
(0
 "aa")
```

We are a bit closer, but still problematic.
Now, in Kona

```
  @[ "abcda"; 0 4; :[;"e"] ]
(;"b";"c";"d";)
```

Now, in Kona

```
  @["abcda"; 0 4; :[;"e"]]
"ebcde"
```

but we still have a problem with

```
  _ssr["abcda";"a";"e"]
"abcda"
```

With faulty trim disabled in Kona, we now get

```
  g:{if[_n~x;:_n]; 
     i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x; 
     ,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
  g["abcda";"a";"e"]
"ebcde"
```

However, we still have

```
  _ssr["abcda";"a";"e"]
"abcda"
```

Fixed faulty trim.

Found the problem.  Now "just" have to figure out how to fix it.
in file p.c, if you add 2 display lines:

```
    O("s0: %s\n",s);
  DO(n, if(WORD_PART(m[i])){m[y]=m[i]; s2[y]=s2[i]; y++;}) //m and s are compacted
  s2[y]=m[y]=0;
    O("s2: %s\n",s2)
```

and run **_ssr["abcda";"a";"e"]**
at some point, you get

```
s0: if[_n~x;:_n];i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_ x;,/ :[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]}
s2: if[_n~x;:_n];i:1+2*!_.5*#x:(0,/(0,+/~+\(>':0,"["=y)-<':("]"=y$:),0)+/:x _ss y)_x;,/:[7=4:z;@[x;i;z];4:z$:;@[x;i;:[;z]];@[x;i;:;z]]
```

(The problem is that **,/ :** becomes **,/:**
Note that:

```
src/p.c:#define WORD_PART(x)      (ABS(x) > MARK_IGNORE)
```

So, we probably need an adjustment in the marking process.

Upon closer inspection, that may not be the problem, as the following case works fine in Kona:

```
  ,/ :[1;(,"a";"b";,"c");(,"d";"e";,"f")]
s0: ,/ :[1;(,"a";"b";,"c");(,"d";"e";,"f")]
s2: ,/:[1;(,"a";"b";,"c");(,"d";"e";,"f")]
s0: 1;(,"a";"b";,"c");(,"d";"e";,"f")]
s2: 1;(,"a";"b";,"c");(,"d";"e";,"f")
s0: ,"a";"b";,"c");(,"d";"e";,"f")
s2: ,"a";"b";,"c"
s0: ,"d";"e";,"f")
s2: ,"d";"e";,"f"
"abc"
```

but this fails:

```
  ,/:[1;(,"a";"b";,"c");(,"d";"e";,"f")]
s0: ,/:[1;(,"a";"b";,"c");(,"d";"e";,"f")]
s2: ,/:[1;(,"a";"b";,"c");(,"d";"e";,"f")]
s0: 1;(,"a";"b";,"c");(,"d";"e";,"f")]
s2: 1;(,"a";"b";,"c");(,"d";"e";,"f")
s0: ,"a";"b";,"c");(,"d";"e";,"f")
s2: ,"a";"b";,"c"
s0: ,"d";"e";,"f")
s2: ,"d";"e";,"f"
valence error
> 
```
