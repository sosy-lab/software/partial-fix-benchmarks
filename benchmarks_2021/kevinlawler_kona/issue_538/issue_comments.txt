Evaluation order bug?
This was broken by the fix to issue #482 done on Sep 1, 2017.
Thanks!
While this specific issue is fixed, looks the root cause is not.
````
  (a;a:2)
value error
a
^
parse error
````
It should return `2 2`