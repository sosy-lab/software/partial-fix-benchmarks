fix #192: Unexpected regex match
add some test cases related to Issue #192
Revert "fix #192: Unexpected regex match"

This reverts commit 47af49af70a717ac31c54ae55ad242a27f213def.
fix #192 (recorrect): Unexpected regex match
fix: Incomplete application of ONIG_OPTION_NOT_END_STRING to \Z (#192)
