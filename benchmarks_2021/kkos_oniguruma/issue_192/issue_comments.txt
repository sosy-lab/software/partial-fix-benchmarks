Unexpected regex match
Thank you for your research.
Fixed.
This is a bug that if the look-behind contains a branch with a character length of 0 and an anchor is included in the branch, the whole look-behind is ignored.

It may be quite serious, so an urgent release may be needed.
But there may be some other problems, so wait a while.