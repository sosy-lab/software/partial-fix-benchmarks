diff --git a/doc/yabai.1 b/doc/yabai.1
index ebc2506..d55e097 100644
--- a/doc/yabai.1
+++ b/doc/yabai.1
@@ -476,12 +476,17 @@ Set window frame based on a self\-defined grid.
 A signal is a simple way for the user to react to some event that has been processed.
 .SS "General Syntax"
 .sp
-yabai \-m signal \fI<COMMAND>\fP event=\fI<EVENT>\fP action=\fI<ACTION>\fP
+yabai \-m signal \fI<COMMAND>\fP
 .SS "COMMAND"
 .sp
-\fB\-\-add\fP
+\fB\-\-add label=\fI<LABEL>\fP event=\fI<EVENT>\fP action=\fI<ACTION>\fP\fP
+.RS 4
+Add a new labelled signal to execute an action after processing an event of the given type.
+.RE
+.sp
+\fB\-\-remove \fI<LABEL>\fP\fP
 .RS 4
-Add a new signal to execute an action after processing an event of the given type.
+Remove an existing signal with the given label.
 .RE
 .SS "EVENT"
 .sp
diff --git a/doc/yabai.asciidoc b/doc/yabai.asciidoc
index 7d62e43..7c99c57 100644
--- a/doc/yabai.asciidoc
+++ b/doc/yabai.asciidoc
@@ -371,13 +371,16 @@ A signal is a simple way for the user to react to some event that has been proce
 General Syntax
 ^^^^^^^^^^^^^^
 
-yabai -m signal '<COMMAND>' event='<EVENT>' action='<ACTION>'
+yabai -m signal '<COMMAND>'
 
 COMMAND
 ^^^^^^^
 
-*--add*::
-    Add a new signal to execute an action after processing an event of the given type.
+*--add label='<LABEL>' event='<EVENT>' action='<ACTION>'*::
+    Add a new labelled signal to execute an action after processing an event of the given type.
+
+*--remove '<LABEL>'*::
+    Remove an existing signal with the given label.
 
 EVENT
 ^^^^^
diff --git a/src/event.c b/src/event.c
index 349c8b6..9de2dd9 100644
--- a/src/event.c
+++ b/src/event.c
@@ -1,6 +1,6 @@
 #include "event.h"
 
-extern char **g_signal_event[EVENT_TYPE_COUNT];
+extern struct signal *g_signal_event[EVENT_TYPE_COUNT];
 extern struct event_loop g_event_loop;
 extern struct process_manager g_process_manager;
 extern struct display_manager g_display_manager;
@@ -11,15 +11,6 @@ extern struct bar g_bar;
 extern bool g_mission_control_active;
 extern int g_connection;
 
-enum event_type event_type_from_string(const char *str)
-{
-    for (int i = APPLICATION_LAUNCHED; i < EVENT_TYPE_COUNT; ++i) {
-        if (string_equals(str, event_type_str[i])) return i;
-    }
-
-    return EVENT_TYPE_UNKNOWN;
-}
-
 static void event_signal_populate_args(void *context, enum event_type type, char args[][255])
 {
     switch (type) {
@@ -87,7 +78,7 @@ static void event_signal_populate_args(void *context, enum event_type type, char
     }
 }
 
-void event_signal(void *context, enum event_type type)
+void event_signal_transmit(void *context, enum event_type type)
 {
     int signal_count = buf_len(g_signal_event[type]);
     if (!signal_count) return;
@@ -99,12 +90,46 @@ void event_signal(void *context, enum event_type type)
     debug("%s: %s\n", __FUNCTION__, event_type_str[type]);
 
     for (int i = 0; i < signal_count; ++i) {
-        fork_exec(g_signal_event[type][i], args[0], args[1]);
+        fork_exec(g_signal_event[type][i].command, args[0], args[1]);
     }
 
     exit(EXIT_SUCCESS);
 }
 
+void event_signal_add(enum event_type type, char *action, char *label)
+{
+    if (label) event_signal_remove(label);
+    buf_push(g_signal_event[type], ((struct signal) {
+        .command = action,
+        .label   = label
+    }));
+}
+
+bool event_signal_remove(char *label)
+{
+    for (int i = 0; i < EVENT_TYPE_COUNT; ++i) {
+        for (int j = 0; j < buf_len(g_signal_event[i]); ++j) {
+            if (string_equals(label, g_signal_event[i][j].label)) {
+                free(g_signal_event[i][j].command);
+                free(g_signal_event[i][j].label);
+                buf_del(g_signal_event[i], j);
+                return true;
+            }
+        }
+    }
+
+    return false;
+}
+
+enum event_type event_type_from_string(const char *str)
+{
+    for (int i = APPLICATION_LAUNCHED; i < EVENT_TYPE_COUNT; ++i) {
+        if (string_equals(str, event_type_str[i])) return i;
+    }
+
+    return EVENT_TYPE_UNKNOWN;
+}
+
 void event_destroy(struct event *event)
 {
     switch (event->type) {
diff --git a/src/event.h b/src/event.h
index 2c959b1..ec9536e 100644
--- a/src/event.h
+++ b/src/event.h
@@ -179,6 +179,12 @@ struct event
     volatile int *result;
 };
 
+struct signal
+{
+    char *command;
+    char *label;
+};
+
 #define event_create(e, t, d)\
     do {\
         e = malloc(sizeof(struct event));\
@@ -201,7 +207,9 @@ struct event
         e->result  = 0;\
     } while (0)
 
-void event_signal(void *context, enum event_type type);
+void event_signal_transmit(void *context, enum event_type type);
+void event_signal_add(enum event_type type, char *action, char *label);
+bool event_signal_remove(char *label);
 void event_destroy(struct event *event);
 enum event_type event_type_from_string(const char *str);
 
diff --git a/src/event_loop.c b/src/event_loop.c
index 6c47295..12eb22f 100644
--- a/src/event_loop.c
+++ b/src/event_loop.c
@@ -58,7 +58,7 @@ event_loop_run(void *context)
         struct event *event = queue_pop(queue);
         if (event) {
             int result = event_handler[event->type](event->context, event->param1, event->param2);
-            if (result == EVENT_SUCCESS) event_signal(event->context, event->type);
+            if (result == EVENT_SUCCESS) event_signal_transmit(event->context, event->type);
 
             if (event->status) *event->status = EVENT_PROCESSED;
             if (event->result) *event->result = result;
diff --git a/src/message.c b/src/message.c
index 89d3ac6..d16e9a6 100644
--- a/src/message.c
+++ b/src/message.c
@@ -1,6 +1,6 @@
 #include "message.h"
 
-extern char **g_signal_event[EVENT_TYPE_COUNT];
+extern struct signal *g_signal_event[EVENT_TYPE_COUNT];
 extern struct event_loop g_event_loop;
 extern struct display_manager g_display_manager;
 extern struct space_manager g_space_manager;
@@ -194,9 +194,11 @@ static const char *bool_str[] = { "off", "on" };
 
 /* --------------------------------DOMAIN SIGNAL-------------------------------- */
 #define COMMAND_SIGNAL_ADD "--add"
+#define COMMAND_SIGNAL_REM "--remove"
 
 #define ARGUMENT_SIGNAL_KEY_EVENT    "event"
 #define ARGUMENT_SIGNAL_KEY_ACTION   "action"
+#define ARGUMENT_SIGNAL_KEY_LABEL    "label"
 /* ----------------------------------------------------------------------------- */
 
 struct token
@@ -1599,6 +1601,7 @@ static void handle_domain_signal(FILE *rsp, struct token domain, char *message)
     if (token_equals(command, COMMAND_SIGNAL_ADD)) {
         enum event_type signal_type = EVENT_TYPE_UNKNOWN;
         char *signal_action = NULL;
+        char *signal_label  = NULL;
 
         struct token token = get_token(&message);
         while (token.text && token.length > 0) {
@@ -1616,16 +1619,28 @@ static void handle_domain_signal(FILE *rsp, struct token domain, char *message)
                 }
             } else if (string_equals(key, ARGUMENT_SIGNAL_KEY_ACTION)) {
                 signal_action = string_copy(value);
+            } else if (string_equals(key, ARGUMENT_SIGNAL_KEY_LABEL)) {
+                signal_label = string_copy(value);
             }
 
             token = get_token(&message);
         }
 
         if (signal_type > EVENT_TYPE_UNKNOWN && signal_type < EVENT_TYPE_COUNT && signal_action) {
-            buf_push(g_signal_event[signal_type], signal_action);
+            event_signal_add(signal_type, signal_action, signal_label);
         } else {
             daemon_fail(rsp, "signal was not added.\n");
         }
+    } else if (token_equals(command, COMMAND_SIGNAL_REM)) {
+        struct token token = get_token(&message);
+        if (token_is_valid(token)) {
+            char *label = token_to_string(token);
+            bool did_remove_signal = event_signal_remove(label);
+            if (!did_remove_signal) daemon_fail(rsp, "signal with label '%s' not found.\n", label);
+            free(label);
+        } else {
+            daemon_fail(rsp, "invalid label specified.\n", token.text);
+        }
     } else {
         daemon_fail(rsp, "unknown command '%.*s' for domain '%.*s'\n", command.length, command.text, domain.length, domain.text);
     }
diff --git a/src/misc/sbuffer.h b/src/misc/sbuffer.h
index 44d5984..3e06e95 100644
--- a/src/misc/sbuffer.h
+++ b/src/misc/sbuffer.h
@@ -16,8 +16,9 @@ struct buf_hdr
 
 #define buf_len(b) ((b) ? buf__hdr(b)->len : 0)
 #define buf_cap(b) ((b) ? buf__hdr(b)->cap : 0)
-#define buf_push(b, x) (buf__fit(b, 1), (b)[buf_len(b)] = (x), buf__hdr(b)->len++)
 #define buf_last(b) ((b)[buf_len(b)-1])
+#define buf_push(b, x) (buf__fit(b, 1), (b)[buf_len(b)] = (x), buf__hdr(b)->len++)
+#define buf_del(b, x) ((b) ? (b)[x] = (b)[buf_len(b)-1], buf__hdr(b)->len-- : 0)
 #define buf_free(b) ((b) ? free(buf__hdr(b)) : 0)
 
 static void *buf__grow_f(const void *buf, size_t new_len, size_t elem_size)
diff --git a/src/yabai.c b/src/yabai.c
index 14678ce..98927a3 100644
--- a/src/yabai.c
+++ b/src/yabai.c
@@ -38,7 +38,7 @@ struct daemon g_daemon;
 struct bar g_bar;
 int g_connection;
 
-char **g_signal_event[EVENT_TYPE_COUNT];
+struct signal *g_signal_event[EVENT_TYPE_COUNT];
 bool g_mission_control_active;
 char g_sa_socket_file[MAXLEN];
 char g_socket_file[MAXLEN];
