diff --git a/src/prepare.c b/src/prepare.c
index 7f12ea4c..8f34401c 100644
--- a/src/prepare.c
+++ b/src/prepare.c
@@ -57,6 +57,9 @@ static zend_trait_precedence * pthreads_preparation_copy_trait_precedence(PTHREA
 static  zend_trait_method_reference * pthreads_preparation_copy_trait_method_reference(PTHREAD thread, zend_trait_method_reference *reference TSRMLS_DC); /* }}} */
 #endif
 
+/* {{{ fix the scope of methods such that inheritance works correctly */
+static int pthreads_apply_method_prototype(zend_op_array *ops, zend_class_entry **ce TSRMLS_DC); /* }}} */
+
 /* {{{ fix the scope of methods such that self:: and parent:: work everywhere */
 static int pthreads_apply_method_scope(zend_op_array *ops, PTHREAD thread TSRMLS_DC); /* }}} */
 
@@ -82,8 +85,9 @@ static zend_class_entry* pthreads_copy_entry(PTHREAD thread, zend_class_entry *c
 	zend_initialize_class_data(prepared, 1 TSRMLS_CC);
 	
 	/* set ce flags (reset by initialize) */
-	prepared->ce_flags |= candidate->ce_flags;
+	prepared->ce_flags = candidate->ce_flags;
 	
+	/* set parent */
 	if (candidate->parent)
 	    prepared->parent = pthreads_prepared_entry(thread, candidate->parent TSRMLS_CC);
 	
@@ -306,7 +310,7 @@ zend_class_entry* pthreads_prepared_entry(PTHREAD thread, zend_class_entry *cand
 	
 	if (candidate) {
         char *lower = pthreads_global_string(
-            (char*) candidate->name, candidate->name_length, 1 TSRMLS_CC);
+            (char*) candidate->name, candidate->name_length+1, 1 TSRMLS_CC);
         
 		if (lower != NULL) {
 			/* perform lookup for existing class */
@@ -315,7 +319,7 @@ zend_class_entry* pthreads_prepared_entry(PTHREAD thread, zend_class_entry *cand
 			    /* create a new user class for this context */
 	            prepared = pthreads_copy_entry(
 	                thread, candidate TSRMLS_CC);
-	                
+	            
 			    /* update class table */
                 zend_hash_update(   
                     CG(class_table), lower, prepared->name_length+1, &prepared, sizeof(zend_class_entry*), (void**)&searched);
@@ -413,37 +417,48 @@ void pthreads_prepare(PTHREAD thread TSRMLS_DC){
 	if (thread->options & PTHREADS_INHERIT_CLASSES) {
 		zend_class_entry **entry;
 		HashTable *table[2] = {PTHREADS_CG(thread->cls, class_table), CG(class_table)};
-
+        HashTable store;
+        
+        zend_hash_init(&store, zend_hash_num_elements(table[1]), NULL, NULL, 0);
+        
 		for(zend_hash_internal_pointer_reset_ex(table[0], &position);
 			zend_hash_get_current_data_ex(table[0], (void**) &entry, &position)==SUCCESS;
 			zend_hash_move_forward_ex(table[0], &position)) {
-			char *lcname;
-			uint lcnamel;
-			ulong idx;
-			zend_class_entry *prepared;
-			zend_class_entry **exists;
+			if ((*entry)->type == ZEND_USER_CLASS) {
+			    char *lcname;
+			    uint lcnamel;
+			    ulong idx;
+			    zend_class_entry *prepared;
+			    zend_class_entry **exists;
 			
-			if (zend_hash_get_current_key_ex(table[0], &lcname, &lcnamel, &idx, 0, &position)==HASH_KEY_IS_STRING) {
-				if (zend_hash_find(table[1], lcname, lcnamel, (void**)&exists) != SUCCESS){
-					if ((prepared=pthreads_prepared_entry(thread, *entry TSRMLS_CC))==NULL) {
-						zend_error(
-							E_ERROR, "pthreads detected failure while preparing %s in %s", (*entry)->name, thread->std.ce->name, thread->tid
-						);
-						return;
-					}
-				} else prepared = *exists;
-
-				/*
-				* fix scope in properties
-				*/
-				zend_hash_apply_with_argument(&prepared->properties_info, (apply_func_arg_t) pthreads_apply_property_scope, (void*) thread TSRMLS_CC);
-
-				/*
-				* fix scope in methods
-				*/
-				zend_hash_apply_with_argument(&prepared->function_table, (apply_func_arg_t) pthreads_apply_method_scope, (void*) thread TSRMLS_CC);
+			    if (zend_hash_get_current_key_ex(table[0], &lcname, &lcnamel, &idx, 0, &position)==HASH_KEY_IS_STRING) {
+				    if (zend_hash_find(table[1], lcname, lcnamel, (void**)&exists) != SUCCESS){
+					    if ((prepared=pthreads_prepared_entry(thread, *entry TSRMLS_CC))==NULL) {
+						    zend_error(
+							    E_ERROR, "pthreads detected failure while preparing %s in %s", (*entry)->name, thread->std.ce->name, thread->tid
+						    );
+						    return;
+					    }
+					
+					    zend_hash_next_index_insert(&store, (void**) &prepared, sizeof(zend_class_entry*), NULL);
+				    }
+			    }
 			}
 		}
+		
+		for(zend_hash_internal_pointer_reset_ex(&store, &position);
+			zend_hash_get_current_data_ex(&store, (void**) &entry, &position)==SUCCESS;
+			zend_hash_move_forward_ex(&store, &position)) {
+			
+			zend_hash_apply_with_argument(
+			    &(*entry)->properties_info, (apply_func_arg_t) pthreads_apply_property_scope, (void*) thread TSRMLS_CC);
+			zend_hash_apply_with_argument(
+			    &(*entry)->function_table, (apply_func_arg_t) pthreads_apply_method_scope, (void*) thread TSRMLS_CC);
+			zend_hash_apply_with_argument(
+			    &(*entry)->function_table, (apply_func_arg_t) pthreads_apply_method_prototype, (void*) entry TSRMLS_CC);
+		}
+		
+		zend_hash_destroy(&store);     
 	}
 	
 	/* merge included files with parent */
@@ -462,7 +477,7 @@ void pthreads_prepare(PTHREAD thread TSRMLS_DC){
 	EG(regular_list).pDestructor =  (dtor_func_t) pthreads_prepared_resource_dtor;	
 } /* }}} */
 
-/* {{{ copy property info 
+/* {{{ copy property info
 	@TODO possibly adjust scope here */
 static void pthreads_preparation_property_info_ctor(zend_property_info *pi) {} /* }}} */
 
@@ -542,11 +557,45 @@ static  zend_trait_method_reference * pthreads_preparation_copy_trait_method_ref
 } /* }}} */
 #endif
 
+/* {{{ fix method prototype for prepared entries, enabling inheritance to function correctly */
+static int pthreads_apply_method_prototype(zend_op_array *ops, zend_class_entry **ce TSRMLS_DC) {
+	if (ops && ce) {
+	    if (ops->prototype) {
+	        zend_class_entry **scope;
+	        zend_function *prototype;
+	        
+	        if ((*ce)->parent) {
+	            if (memcmp(ops->prototype->common.scope->name, (*ce)->parent->name, (*ce)->parent->name_length+1) == SUCCESS) {
+	                if ((zend_hash_find(
+	                        &(*ce)->parent->function_table, 
+	                        ops->prototype->common.function_name,
+	                        strlen(ops->prototype->common.function_name)+1,
+	                        (void**) &prototype) == SUCCESS)) {
+	                    ops->prototype = prototype;
+	                } else ops->prototype = NULL;
+	            }
+	        } else if (memcmp(ops->prototype->common.scope->name, (*ce)->name, (*ce)->name_length+1) == SUCCESS) {
+	            if ((zend_hash_find(
+	                    &(*ce)->function_table, 
+	                    ops->prototype->common.function_name,
+	                    strlen(ops->prototype->common.function_name)+1,
+	                    (void**) &prototype) == SUCCESS)) {
+	                ops->prototype = prototype;
+	            } else ops->prototype = NULL;
+	        }
+	    } else {
+	        ops->prototype = NULL;
+	    }
+	}
+	return ZEND_HASH_APPLY_KEEP;
+} /* }}} */
+
 /* {{{ fix method scope for prepared entries, enabling self:: and parent:: to work */
 static int pthreads_apply_method_scope(zend_op_array *ops, PTHREAD thread TSRMLS_DC) {
 	if (thread && ops) {
-		ops->scope = pthreads_prepared_entry(
-		    thread,  ops->scope TSRMLS_CC);
+	    if (ops->scope) {
+	        ops->scope = pthreads_prepared_entry(thread, ops->scope TSRMLS_CC);
+	    }
 	}
 	return ZEND_HASH_APPLY_KEEP;
 } /* }}} */
@@ -554,8 +603,7 @@ static int pthreads_apply_method_scope(zend_op_array *ops, PTHREAD thread TSRMLS
 /* {{{ fix scope for prepared entry properties, enabling private members in foreign objects to work */
 static int pthreads_apply_property_scope(zend_property_info *info, PTHREAD thread TSRMLS_DC) {
 	if (thread && info) {
-	    info->ce = pthreads_prepared_entry(
-	        thread, info->ce TSRMLS_CC);
+	    info->ce = pthreads_prepared_entry(thread, info->ce TSRMLS_CC);
 	}
 	return ZEND_HASH_APPLY_KEEP;
 } /* }}} */
