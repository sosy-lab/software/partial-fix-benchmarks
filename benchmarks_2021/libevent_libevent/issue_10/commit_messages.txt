https-client: modify it to reproduce the issue

See: #258
http: avoid race during evhttp_make_request() and evhttp_connection_cb()

$ https-client with -url http://127.1:81 -retries 2
==================
WARNING: ThreadSanitizer: data race (pid=19704)
  Write of size 4 at 0x7d480000b500 by thread T1 (mutexes: write M12):
    #0 evhttp_connection_reset_ ../http.c:1286 (https-client+0x000000451ef6)
    #1 evhttp_connection_cb_cleanup ../http.c:1319 (https-client+0x0000004520a5)
    #2 evhttp_connection_cb ../http.c:1522 (https-client+0x000000452e94)
    #3 bufferevent_run_eventcb_ ../bufferevent.c:279 (https-client+0x00000041c3a7)
    #4 bufferevent_writecb ../bufferevent_sock.c:263 (https-client+0x000000425295)
    #5 event_persist_closure ../event.c:1549 (https-client+0x00000042d132)
    #6 event_process_active_single_queue ../event.c:1608 (https-client+0x00000042d51a)
    #7 event_process_active ../event.c:1707 (https-client+0x00000042e010)
    #8 event_base_loop ../event.c:1930 (https-client+0x00000042ece0)
    #9 evbase_thread ../sample/https-client.c:57 (https-client+0x00000040b8ec)
    #10 <null> <null> (libtsan.so.0+0x000000023559)

  Previous read of size 4 at 0x7d480000b500 by main thread:
    #0 evhttp_connected ../http.c:406 (https-client+0x00000044f27c)
    #1 evhttp_make_request ../http.c:2541 (https-client+0x0000004566a7)
    #2 main ../sample/https-client.c:456 (https-client+0x00000040c67a)

  Location is heap block of size 384 at 0x7d480000b400 allocated by main thread:
    #0 calloc <null> (libtsan.so.0+0x0000000259ad)
    #1 event_mm_calloc_ ../event.c:3428 (https-client+0x000000437655)
    #2 evhttp_connection_base_bufferevent_new ../http.c:2256 (https-client+0x0000004552f5)
    #3 main ../sample/https-client.c:409 (https-client+0x00000040c452)

  Mutex M12 (0x7d0c00029250) created at:
    #0 pthread_mutex_init <null> (libtsan.so.0+0x000000027e15)
    #1 evthread_posix_lock_alloc ../evthread_pthread.c:53 (https-client+0x00000044e5c5)
    #2 bufferevent_enable_locking_ ../bufferevent.c:827 (https-client+0x00000041e9ad)
    #3 bufferevent_init_common_ ../bufferevent.c:331 (https-client+0x00000041c6a2)
    #4 bufferevent_socket_new ../bufferevent_sock.c:357 (https-client+0x0000004255bc)
    #5 main ../sample/https-client.c:392 (https-client+0x00000040c3a0)

  Thread T1 (tid=19706, running) created by main thread at:
    #0 pthread_create <null> (libtsan.so.0+0x000000027414)
    #1 main ../sample/https-client.c:377 (https-client+0x00000040c325)

SUMMARY: ThreadSanitizer: data race ../http.c:1286 evhttp_connection_reset_

Refs: #258
test: fix bufferevent/bufferevent_pair_release_lock for freebsd

On FreeBSD with kqueue there is a call to evthread_debug_lock_mark_unlocked()
during event_base_free(), that will fail with an assert because of unmatched
"held_by", fix this by reseting lock callbacks to NULL before
event_base_free().

Trace:
  bufferevent/bufferevent_pair_release_lock: [warn] Trying to disable lock functions after they have been set up will probaby not work.
  [warn] Trying to disable lock functions after they have been set up will probaby not work.

    FAIL libevent/test/regress_bufferevent.c:259: lock: lock error[err] libevent/evthread.c:277: Assertion lock->held_by == me failed in evthread_debug_lock_mark_unlocked
  [New Thread 802006400 (LWP 100070/regress)]

  Program received signal SIGABRT, Aborted.
  [Switching to Thread 802006400 (LWP 100070/regress)]
  0x000000080167d6ca in thr_kill () from /lib/libc.so.7
  (gdb) bt
  #0  0x000000080167d6ca in thr_kill () from /lib/libc.so.7
  #1  0x0000000801752149 in abort () from /lib/libc.so.7
  #2  0x00000000004dff44 in event_exit (errcode=-559030611) at libevent/log.c:105
  #3  0x00000000004e053c in event_errx (eval=-559030611, fmt=0x5182cc "%s:%d: Assertion %s failed in %s") at libevent/log.c:162
  #4  0x00000000004d9954 in evthread_debug_lock_mark_unlocked (mode=0, lock=0x802017060) at libevent/evthread.c:277
  #5  0x00000000004d909a in debug_lock_unlock (mode=0, lock_=0x802017060) at libevent/evthread.c:290
  #6  0x00000000004e132c in evsig_dealloc_ (base=0x80201e300) at libevent/signal.c:434
  #7  0x00000000004e36c1 in kq_dealloc (base=0x80201e300) at libevent/kqueue.c:435
  #8  0x00000000004c9a44 in event_base_free_ (base=0x80201e300, run_finalizers=1) at libevent/event.c:855
  #9  0x00000000004c931a in event_base_free (base=0x0) at libevent/event.c:887
  #10 0x0000000000452657 in lock_unlock_free_thread_cbs () at libevent/test/regress_bufferevent.c:279
  #11 0x0000000000452621 in free_lock_unlock_profiler (data=0x8020170a0) at libevent/test/regress_bufferevent.c:317
  #12 0x000000000044bc8f in test_bufferevent_pair_release_lock (arg=0x8020170a0) at libevent/test/regress_bufferevent.c:334
  #13 0x00000000004b2288 in testcase_run_bare_ (testcase=0x737660) at libevent/test/tinytest.c:105
  #14 0x00000000004b1e72 in testcase_run_one (group=0x738c90, testcase=0x737660) at libevent/test/tinytest.c:252
  #15 0x00000000004b2930 in tinytest_main (c=3, v=0x7fffffffead0, groups=0x738c20) at libevent/test/tinytest.c:434
  #16 0x00000000004982fe in main (argc=3, argv=0x7fffffffead0) at libevent/test/regress_main.c:459
  (gdb) f 4
  #4  0x00000000004d9954 in evthread_debug_lock_mark_unlocked (mode=0, lock=0x802017060) at libevent/evthread.c:277
  277                     EVUTIL_ASSERT(lock->held_by == me);
  Current language:  auto; currently minimal
  (gdb) p lock
  $1 = (struct debug_lock *) 0x802017060
  (gdb) p lock->held_by
  $2 = 0
  (gdb) p me
  $3 = 34393318400
be_sock: bufferevent_socket_connect_hostname(): make it thread-safe

If you use bufferevent_socket_connect_hostname() to resolve then ipv4 answer
can be returned before ipv6 scheduled and then if you will destroy bufferevent
after ipv4 answer will come then ipv6 will trigger UAF:

$ a.out
=================================================================
==29733==ERROR: AddressSanitizer: heap-use-after-free on address 0x60200000ef50 at pc 0x0000004b7aef bp 0x7fffffffd940 sp 0x7fffffffd0f8
READ of size 2 at 0x60200000ef50 thread T0
    #0 0x4b7aee in __interceptor_index (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4b7aee)
    #1 0x5060eb in string_num_dots /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2739
    #2 0x5078df in search_request_new /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:3214
    #3 0x506afd in evdns_base_resolve_ipv6 /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2935
    #4 0x50aa94 in evdns_getaddrinfo /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:4719
    #5 0x51de4f in evutil_getaddrinfo_async_ /src/oss/libevent/libevent-github/.cmake-debug/../evutil.c:1567
    #6 0x4fe023 in bufferevent_socket_connect_hostname /src/oss/libevent/libevent-github/.cmake-debug/../bufferevent_sock.c:519
    #7 0x524f54 in evhttp_connection_connect_ /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2493
    #8 0x525156 in evhttp_make_request /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2548
    #9 0x52d373 in main (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d373)
    #10 0x7ffff6849b44 in __libc_start_main /tmp/buildd/glibc-2.19/csu/libc-start.c:287
    #11 0x445806 in _start (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x445806)

0x60200000ef50 is located 0 bytes inside of 15-byte region [0x60200000ef50,0x60200000ef5f)
freed by thread T1 here:
    #0 0x4cc4f2 in __interceptor_free (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4cc4f2)
    #1 0x5141c1 in event_mm_free_ /src/oss/libevent/libevent-github/.cmake-debug/../event.c:3512
    #2 0x522402 in evhttp_connection_free /src/oss/libevent/libevent-github/.cmake-debug/../http.c:1206
    #3 0x52cc5f in connection_closer (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52cc5f)
    #4 0x50e80e in event_process_active_single_queue /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1642
    #5 0x50ed57 in event_process_active /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1734
    #6 0x50f458 in event_base_loop /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1957
    #7 0x50eddf in event_base_dispatch /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1768
    #8 0x52d075 in event_dispatch_thread (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d075)
    #9 0x7ffff74fc0a3 in start_thread /tmp/buildd/glibc-2.19/nptl/pthread_create.c:309

Fixes: #217
be_sock: bufferevent_socket_connect_hostname(): make it thread-safe

If you use bufferevent_socket_connect_hostname() to resolve then ipv4 answer
can be returned before ipv6 scheduled and then if you will destroy bufferevent
after ipv4 answer will come then ipv6 will trigger UAF:

$ a.out
=================================================================
==29733==ERROR: AddressSanitizer: heap-use-after-free on address 0x60200000ef50 at pc 0x0000004b7aef bp 0x7fffffffd940 sp 0x7fffffffd0f8
READ of size 2 at 0x60200000ef50 thread T0
    #0 0x4b7aee in __interceptor_index (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4b7aee)
    #1 0x5060eb in string_num_dots /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2739
    #2 0x5078df in search_request_new /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:3214
    #3 0x506afd in evdns_base_resolve_ipv6 /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2935
    #4 0x50aa94 in evdns_getaddrinfo /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:4719
    #5 0x51de4f in evutil_getaddrinfo_async_ /src/oss/libevent/libevent-github/.cmake-debug/../evutil.c:1567
    #6 0x4fe023 in bufferevent_socket_connect_hostname /src/oss/libevent/libevent-github/.cmake-debug/../bufferevent_sock.c:519
    #7 0x524f54 in evhttp_connection_connect_ /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2493
    #8 0x525156 in evhttp_make_request /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2548
    #9 0x52d373 in main (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d373)
    #10 0x7ffff6849b44 in __libc_start_main /tmp/buildd/glibc-2.19/csu/libc-start.c:287
    #11 0x445806 in _start (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x445806)

0x60200000ef50 is located 0 bytes inside of 15-byte region [0x60200000ef50,0x60200000ef5f)
freed by thread T1 here:
    #0 0x4cc4f2 in __interceptor_free (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4cc4f2)
    #1 0x5141c1 in event_mm_free_ /src/oss/libevent/libevent-github/.cmake-debug/../event.c:3512
    #2 0x522402 in evhttp_connection_free /src/oss/libevent/libevent-github/.cmake-debug/../http.c:1206
    #3 0x52cc5f in connection_closer (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52cc5f)
    #4 0x50e80e in event_process_active_single_queue /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1642
    #5 0x50ed57 in event_process_active /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1734
    #6 0x50f458 in event_base_loop /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1957
    #7 0x50eddf in event_base_dispatch /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1768
    #8 0x52d075 in event_dispatch_thread (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d075)
    #9 0x7ffff74fc0a3 in start_thread /tmp/buildd/glibc-2.19/nptl/pthread_create.c:309

Fixes: #217
Closes: #222
Gist: https://gist.github.com/azat/92cbb34232ac02d7972b (from #217 but thread-safe)
be_sock: bufferevent_socket_connect_hostname(): make it thread-safe

If you use bufferevent_socket_connect_hostname() to resolve, then ipv4 answer
can be returned before ipv6 scheduled and if you will destroy bufferevent after
ipv4 answer will come then ipv6 will trigger UAF:

$ a.out
=================================================================
==29733==ERROR: AddressSanitizer: heap-use-after-free on address 0x60200000ef50 at pc 0x0000004b7aef bp 0x7fffffffd940 sp 0x7fffffffd0f8
READ of size 2 at 0x60200000ef50 thread T0
    #0 0x4b7aee in __interceptor_index (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4b7aee)
    #1 0x5060eb in string_num_dots /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2739
    #2 0x5078df in search_request_new /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:3214
    #3 0x506afd in evdns_base_resolve_ipv6 /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2935
    #4 0x50aa94 in evdns_getaddrinfo /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:4719
    #5 0x51de4f in evutil_getaddrinfo_async_ /src/oss/libevent/libevent-github/.cmake-debug/../evutil.c:1567
    #6 0x4fe023 in bufferevent_socket_connect_hostname /src/oss/libevent/libevent-github/.cmake-debug/../bufferevent_sock.c:519
    #7 0x524f54 in evhttp_connection_connect_ /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2493
    #8 0x525156 in evhttp_make_request /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2548
    #9 0x52d373 in main (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d373)
    #10 0x7ffff6849b44 in __libc_start_main /tmp/buildd/glibc-2.19/csu/libc-start.c:287
    #11 0x445806 in _start (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x445806)

0x60200000ef50 is located 0 bytes inside of 15-byte region [0x60200000ef50,0x60200000ef5f)
freed by thread T1 here:
    #0 0x4cc4f2 in __interceptor_free (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4cc4f2)
    #1 0x5141c1 in event_mm_free_ /src/oss/libevent/libevent-github/.cmake-debug/../event.c:3512
    #2 0x522402 in evhttp_connection_free /src/oss/libevent/libevent-github/.cmake-debug/../http.c:1206
    #3 0x52cc5f in connection_closer (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52cc5f)
    #4 0x50e80e in event_process_active_single_queue /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1642
    #5 0x50ed57 in event_process_active /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1734
    #6 0x50f458 in event_base_loop /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1957
    #7 0x50eddf in event_base_dispatch /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1768
    #8 0x52d075 in event_dispatch_thread (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d075)
    #9 0x7ffff74fc0a3 in start_thread /tmp/buildd/glibc-2.19/nptl/pthread_create.c:309

Fixes: #217
Closes: #222
Gist: https://gist.github.com/azat/92cbb34232ac02d7972b (from #217 but thread-safe)
be_sock: bufferevent_socket_connect_hostname(): make it thread-safe

If you use bufferevent_socket_connect_hostname() to resolve, then ipv4 answer
can be returned before ipv6 scheduled and if you will destroy bufferevent after
ipv4 answer will come then ipv6 will trigger UAF:

$ a.out
=================================================================
==29733==ERROR: AddressSanitizer: heap-use-after-free on address 0x60200000ef50 at pc 0x0000004b7aef bp 0x7fffffffd940 sp 0x7fffffffd0f8
READ of size 2 at 0x60200000ef50 thread T0
    #0 0x4b7aee in __interceptor_index (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4b7aee)
    #1 0x5060eb in string_num_dots /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2739
    #2 0x5078df in search_request_new /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:3214
    #3 0x506afd in evdns_base_resolve_ipv6 /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2935
    #4 0x50aa94 in evdns_getaddrinfo /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:4719
    #5 0x51de4f in evutil_getaddrinfo_async_ /src/oss/libevent/libevent-github/.cmake-debug/../evutil.c:1567
    #6 0x4fe023 in bufferevent_socket_connect_hostname /src/oss/libevent/libevent-github/.cmake-debug/../bufferevent_sock.c:519
    #7 0x524f54 in evhttp_connection_connect_ /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2493
    #8 0x525156 in evhttp_make_request /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2548
    #9 0x52d373 in main (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d373)
    #10 0x7ffff6849b44 in __libc_start_main /tmp/buildd/glibc-2.19/csu/libc-start.c:287
    #11 0x445806 in _start (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x445806)

0x60200000ef50 is located 0 bytes inside of 15-byte region [0x60200000ef50,0x60200000ef5f)
freed by thread T1 here:
    #0 0x4cc4f2 in __interceptor_free (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4cc4f2)
    #1 0x5141c1 in event_mm_free_ /src/oss/libevent/libevent-github/.cmake-debug/../event.c:3512
    #2 0x522402 in evhttp_connection_free /src/oss/libevent/libevent-github/.cmake-debug/../http.c:1206
    #3 0x52cc5f in connection_closer (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52cc5f)
    #4 0x50e80e in event_process_active_single_queue /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1642
    #5 0x50ed57 in event_process_active /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1734
    #6 0x50f458 in event_base_loop /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1957
    #7 0x50eddf in event_base_dispatch /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1768
    #8 0x52d075 in event_dispatch_thread (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d075)
    #9 0x7ffff74fc0a3 in start_thread /tmp/buildd/glibc-2.19/nptl/pthread_create.c:309

Fixes: #217
Closes: #222
Closes: #219
Gist: https://gist.github.com/azat/92cbb34232ac02d7972b (from #217 but thread-safe)
be_sock: bufferevent_socket_connect_hostname(): make it thread-safe

If you use bufferevent_socket_connect_hostname() to resolve, then ipv4 answer
can be returned before ipv6 scheduled and if you will destroy bufferevent after
ipv4 answer will come (in a separate thread of course) then ipv6 will trigger
UAF:

$ a.out
=================================================================
==29733==ERROR: AddressSanitizer: heap-use-after-free on address 0x60200000ef50 at pc 0x0000004b7aef bp 0x7fffffffd940 sp 0x7fffffffd0f8
READ of size 2 at 0x60200000ef50 thread T0
    #0 0x4b7aee in __interceptor_index (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4b7aee)
    #1 0x5060eb in string_num_dots /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2739
    #2 0x5078df in search_request_new /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:3214
    #3 0x506afd in evdns_base_resolve_ipv6 /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:2935
    #4 0x50aa94 in evdns_getaddrinfo /src/oss/libevent/libevent-github/.cmake-debug/../evdns.c:4719
    #5 0x51de4f in evutil_getaddrinfo_async_ /src/oss/libevent/libevent-github/.cmake-debug/../evutil.c:1567
    #6 0x4fe023 in bufferevent_socket_connect_hostname /src/oss/libevent/libevent-github/.cmake-debug/../bufferevent_sock.c:519
    #7 0x524f54 in evhttp_connection_connect_ /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2493
    #8 0x525156 in evhttp_make_request /src/oss/libevent/libevent-github/.cmake-debug/../http.c:2548
    #9 0x52d373 in main (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d373)
    #10 0x7ffff6849b44 in __libc_start_main /tmp/buildd/glibc-2.19/csu/libc-start.c:287
    #11 0x445806 in _start (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x445806)

0x60200000ef50 is located 0 bytes inside of 15-byte region [0x60200000ef50,0x60200000ef5f)
freed by thread T1 here:
    #0 0x4cc4f2 in __interceptor_free (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x4cc4f2)
    #1 0x5141c1 in event_mm_free_ /src/oss/libevent/libevent-github/.cmake-debug/../event.c:3512
    #2 0x522402 in evhttp_connection_free /src/oss/libevent/libevent-github/.cmake-debug/../http.c:1206
    #3 0x52cc5f in connection_closer (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52cc5f)
    #4 0x50e80e in event_process_active_single_queue /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1642
    #5 0x50ed57 in event_process_active /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1734
    #6 0x50f458 in event_base_loop /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1957
    #7 0x50eddf in event_base_dispatch /src/oss/libevent/libevent-github/.cmake-debug/../event.c:1768
    #8 0x52d075 in event_dispatch_thread (/src/oss/libevent/libevent-github/.invest/217-evhttp-threaded/a.out+0x52d075)
    #9 0x7ffff74fc0a3 in start_thread /tmp/buildd/glibc-2.19/nptl/pthread_create.c:309

Fixes: #217
Closes: #222
Closes: #219
Gist: https://gist.github.com/azat/92cbb34232ac02d7972b (from #217 but thread-safe)
evdns: do not pass NULL to memcpy() in evdns_server_request_format_response()

In case of OPT pseudo-RR `class` field is treated as a requestor's UDP
payload size, and class will have 512 (DNS_MAX_UDP_SIZE), and data is
NULL:

  (gdb) p *item
  $4 = {
    next = 0x0,
    name = 0x602000000130 "",
    type = 41,
    class = 512,
    ttl = 0,
    is_name = 0 '\000',
    datalen = 0,
    data = 0x0
  }

And UBSAN will reports:

  ../evdns.c:2493:5: runtime error: null pointer passed as argument 2, which is declared to never be null
      #0 0x7ffff70b65bb in evdns_server_request_format_response ../evdns.c:2493
      #1 0x7ffff70b706b in evdns_server_request_respond ../evdns.c:2529
      #2 0x5555557975ab in regress_dns_server_cb ../test/regress_testutils.c:263
      #3 0x7ffff70a8489 in request_parse ../evdns.c:1576
      #4 0x7ffff70aa445 in server_udp_port_read ../evdns.c:1726
      #5 0x7ffff70ac5cc in server_port_ready_callback ../evdns.c:1849
      #6 0x7ffff6d3054c in event_persist_closure ../event.c:1645
      #7 0x7ffff6d311cd in event_process_active_single_queue ../event.c:1704
      #8 0x7ffff6d33258 in event_process_active ../event.c:1805
      #9 0x7ffff6d361b5 in event_base_loop ../event.c:2047
      #10 0x7ffff6d334ac in event_base_dispatch ../event.c:1839
      #11 0x555555739df2 in test_edns ../test/regress_dns.c:2639
      #12 0x5555557b9e96 in testcase_run_bare_ ../test/tinytest.c:173
      #13 0x5555557ba8f0 in testcase_run_one ../test/tinytest.c:333
      #14 0x5555557bc9a0 in tinytest_main ../test/tinytest.c:527
      #15 0x555555787faa in main ../test/regress_main.c:528
      #16 0x7ffff606c001 in __libc_start_main (/usr/lib/libc.so.6+0x27001)
      #17 0x55555569436d in _start (/src/le/libevent/.cmake-debug/bin/regress+0x14036d)
