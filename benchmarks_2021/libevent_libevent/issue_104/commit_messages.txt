Added a Travis-CI configuration file.

Initial stab at a first Travis config file.
Tweaked callbacks to prevent race condition (https://github.com/libevent/libevent/issues/104)
Tweaked callbacks to prevent race condition (https://github.com/libevent/libevent/issues/104)
Merge remote-tracking branch 'origin/master' into use-rbtree-for-dns-hostsdb-build

* origin/master:
  Fix a crash in evdns related to shutting down evdns
  Add missing headerfile for cmake
  regress_dns: fix leaks in getaddrinfo_async{,_cancel_stress} tests
  Fix a crash in evdns related to shutting down evdns
  Increment version to 2.1.4-alpha-dev
  Update release date in changelog
  Fixup make install for cmake projects
  Renamed sin to saddr due to name conflict
  Correctly skip ipv6 http test on systems without ipv6
  Remove integer-overflow unit tests
  evtag: detect tags over 32-bits earlier
  Catch over-large port numbers early in http
  Fix ubsan warnings when parsing ipv4/ipv6 addrs
  Fix a use-after-free error on EV_CLOSURE_EVENT_FINALIZE callbacks
  Fix an illegal read error in the evbuffer_add_reference tests
  Add new APIs to whatsnew-2.1
  Increment version to 2.1.4-alpha
  Bring changelog up to date for 2.1.4-alpha
  Avoid double-close paths in http tests
  Add missing include to regress_finalize.h
  Skip http/ipv6_for_domain test when we have no ipv6 support
  Add an include to evrpc-internal to fix openbsd compilation warning
  Heap-allocate zlib data structure in regress_zlib tests
  Fix consts in WIN32-Code/getopt*.[ch]
  Fix duplicate paragraph in evbuffer_ptr documentation
  Initialize async bufferevent timeout CBs unconditionally
  add a cast to https-client.c
  Export event_extra not event_extras.
  Update to the latest version of tinytest
  Fix 'make distcheck' by adding regress.gen.[ch] to DISTCLEANFILES
  Move assert(ev) to before we use ev in EV_CLOSURE_EVENT_FINALIZE case
  Add -Qunused-arguments for clang on macos
  Add option to build shared library
  Do not offer EV_FEATURE_EARLY_CLOSE if we have no EPOLLRDHUP
  Stop checking for inet_aton; we don't use it.
  Rename event_extras to event_extra
  BUGFIX: Fix compilation on systems with EPOLLRDHUP undefined.
  Add cmake-related files to .gitignore
  Added -Qunused-arguments for clang on macosx
  bufferevent_pair: don't call downcast(NULL)
  Tweaked callbacks to prevent race condition (https://github.com/libevent/libevent/issues/104)
  Tweaked callbacks to prevent race condition (https://github.com/libevent/libevent/issues/104)
  Added a Travis-CI configuration file.
  Check does arch have the epoll_create and __NR_epoll_wait syscalls.
  Check for OSX when checking for clang.
  Guard against EVENT_NOWIN32 being set during testing.
  Fix https-client compilation on Windows.
  CMake: Get rid of python not found warning when regress tests turned off.
  Fix CMake compile when OpenSSL is disabled.
  Split epoll lookup table into a separate header file
  Expand EV_CLOSED documentation a bit
  Disclaimerize cmake a little in the README
  Fix broken autotools build.
  Fix a c90 warning
  Check if we're on OSX before disabling deprecation in le-proxy
  Change all uses of WIN32 to _WIN32
  Fix include bug.
  Implemented EV_CLOSED event for epoll backend (EPOLLRDHUP).
  Forgotten headers for old nmake project compatability.
  Clean up the README some.
  Update README with CMake build instructions.
  Rename README to README.md and use markdown to format.
  Fix so that old nmake project still builds.
  Set USE_DEBUG=1 on EVENT__ENABLE_VERBOSE_DEBUG
  Some work on making it possible to simply do add_subdirectory() on the project.
  Fix typo
  Add CMake config and install targets.
  Fix even more coverity warnings.
  Fix a couple of compilation warnings in regress_http.c
  Remove spurious checks in evrpc.c error cases (coverity)
  Whoops; fix compilation in bench.c
  Fix coverity warnings in benchmark tools.
  Fix a pile of coverity warnings in the unit tests
  Update unit test to make sure that the callback happens after the output data is written
  evhttp_request_set_on_complete_cb to be more specific about what the function actually does and usage
  Provide on request complete callback facility
  Added unit test for max event counts
  Fixed bug using wrong variable in max event compare
  Add access to max event count stats
  Remove unneeded declaration in bufferevent-internal.h
  Fix needless bufferevent includes in evdns.c
  Fix a couple of "#ifdef WIN32" instances
  Sample HTTPS Client: Set hostname for SNI extension (by f69m)
  fix for ServFail from RIPE Atlas release
  Small tweaks to https-client.c
  Minor optimizations on bufferevent_trigger options
  Make bufferevent_trigger_nolock_() inline
  Unit tests for active_by_fd; unsupport active_by_fd(TIMEOUT)
  Add event_base_active_by_signal by analogy
  Sanity-check arguments to event_base_active_by_fd()
  Refactor evmap_{io,signal}_active_() to tolerate bad inputs
  Typo fixes from Linus Nordberg
  Typo fixes from Linus Nordberg
  Added EVENT__ENABLE_GCC_WARNINGS, turns all warnings into errors.
  Only look for ZLib when it is used (if tests are included).
  Fix the "make verify" target on NetBSD
  Add a "make verify_coverage" target generation coverage info.
  Get rid of unknown pragma warnings.
  Minimum required python version is 2.4.
  Change the BSD license from 4 to 3-clause.
  Added a test for testing if kqueue works with pipes.
  Fix kqueue support.
  Get rid of deprecation warnings for OpenSSL on OSX 10.7+
  Fix the make "verify" target on Windows.
  Clarify event_base_loop exit conditions
  Added a "make verify" target.
  Only test the event backends available on the system.
  Don't segfault on no found event backend.
  Fix bench_cascade program on Windows.
  Only include WIN32 getopt where it is used.
  Add copyright and licensing files for CMake modules.
  Use evutil_closesocket instead.
  Added some GCC specific options.
  Link libm on unix platforms.
  Generate a dummy evconfig-private.h so things build properly.
  More work on adding tests to CMake project
  Add all tests and benchmarks to CMake project.
  Initial CMake commit.
  Fix non-C89 variable declaration.
  Fix non-C89 variable declaration.
  Rename flush_outdated_host_addresses to clear_host_addresses
  bug fix for issues #293 evdns_base_load_hosts doesn't remove outdated addresses
  bug fix for issues #293 evdns_base_load_hosts doesn't remove outdated adresses
  Clarifications in response to merge req. comments
  start writing a changelog for 2.1.4-(beta?)
  update the 2.0 changelog
  Add an option to trigger bufferevent event callbacks
  Add an option to trigger bufferevent I/O callbacks
  Add watermark introspection
  Document deferred eventcb behaviour
  Fix a typo
  Try another doxygen tweak
  Small doxygen tweaks
  Allow registering callback for parsing HTTP headers
  Add a variant of evhttp_send_reply_chunk() with a callback on evhttp_write_buffer()
  https-client: code cleanup
  POST supported, args supported
  event_base_active_by_fd
