add tests for extended methods

define and test a CUSTOM http method
update allowed_methods field from 16 to 32bits.

also change EVHTTP_REQ_UNKNOWN_ value.
should fix #796
http: add WebDAV methods support

WebDAV introduced new HTTP methods (RFC4918):
PROPFIND, PROPPATCH, MKCOL, LOCK, UNLOCK, COPY, MOVE.

Add support of the methods.
update allowed_methods field from 16 to 32bits.

also change EVHTTP_REQ_UNKNOWN_ value.
should fix #796
update allowed_methods field from 16 to 32bits.

also change EVHTTP_REQ_UNKNOWN_ value.
should fix #796
http: replace EVHTTP_REQ_UNKNOWN_ with 0

From the server perspective the evhttp_response_phrase_internal() should
not be called with 0 before this patch, it will be called with
EVHTTP_REQ_UNKNOWN_ hence this patch should not change behavior.

Fixes: 68eb526d7b ("http: add WebDAV methods support")
Fixes: #789
Fixes: #796
Reported-by: Thomas Bernard <miniupnp@free.fr>
