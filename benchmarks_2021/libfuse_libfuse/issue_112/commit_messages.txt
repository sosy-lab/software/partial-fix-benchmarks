Update ChangeLog to include recent documentation improvements
Enable more capabilities by default, and document defaults.

Fixes #112.
Enable more capabilities by default, and document defaults.

Fixes #112.
Added support for FUSE_PARALLEL_DIROPS

Enabled by default since we haven't released libfuse 3.0 yet :-).

Fixes #112.
