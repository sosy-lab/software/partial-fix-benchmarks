Incompatible function pointer cast
Thanks for the report! Would you be able to provide a patch? I don't have an Ubuntu system to test or reproduce. It looks as if it's as easy as changing `(GHRFunc) clean_req` to `(GHRFunc) &clean_req`.
Changing `(GHRFunc) clean_req to (GHRFunc) &clean_req` still produces the warning.

I think the issue is that the number of parameters of clean_req() doesn't match the spec. GHRFunc is defined as expecting 3 gpointers (void*), whereas clean_req(void *key_, struct request *req) only accepts 2 parameters.
Right, sorry. In that case we'll need to add the extra argument to the function definition, e.g.

```
static int clean_req(void *key_, struct request *req, gpointer user_data) {
    (void) user_data;
    // [...]
```
