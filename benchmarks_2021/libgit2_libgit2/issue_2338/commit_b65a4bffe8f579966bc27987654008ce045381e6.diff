diff --git a/src/map.h b/src/map.h
index da3d1e19ad4..b5eea09a1e2 100644
--- a/src/map.h
+++ b/src/map.h
@@ -40,6 +40,10 @@ typedef struct { /* memory mapped buffer	*/
 	assert((prot & GIT_PROT_WRITE) || (prot & GIT_PROT_READ)); \
 	assert((flags & GIT_MAP_FIXED) == 0); } while (0)
 
+#ifndef NO_MMAP
+extern long git_mmap_pagesize(void);
+#endif
+
 extern int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offset);
 extern int p_munmap(git_map *map);
 
diff --git a/src/msink.c b/src/msink.c
new file mode 100644
index 00000000000..9f0736c4be2
--- /dev/null
+++ b/src/msink.c
@@ -0,0 +1,86 @@
+/*
+ * Copyright (C) the libgit2 contributors. All rights reserved.
+ *
+ * This file is part of libgit2, distributed under the GNU GPL v2 with
+ * a Linking Exception. For full terms see the included COPYING file.
+ */
+
+#ifndef NO_MMAP
+
+#include "msink.h"
+
+static long s_mmap_pagesize = 0;
+
+GIT_INLINE(long) mmap_pagesize(void)
+{
+	if (0 == s_mmap_pagesize)
+		s_mmap_pagesize = git_mmap_pagesize();
+	return s_mmap_pagesize;
+}
+
+int git_msink_append(git_msink_file *msf, const void *blob, size_t size)
+{
+	int error = 0;
+	size_t wrsize;
+	const char *data = (const char *) blob;
+
+	for (; 0 != size; size -= wrsize)
+	{
+		if (NULL == msf->block.data) {
+			error = p_mmap(&msf->block, mmap_pagesize()
+				, GIT_PROT_WRITE, GIT_MAP_SHARED
+				, msf->fd, msf->block_begin);
+			if (0 < error)
+				break;
+		}
+
+		assert(msf->block.len > msf->block_offset);
+
+		wrsize = min(size, msf->block.len - msf->block_offset);
+		memcpy((char *) msf->block.data + msf->block_offset, data, wrsize);
+		msf->block_offset += wrsize;
+		data += wrsize;
+
+		if (msf->block.len == msf->block_offset) {
+			/* no more space left */
+			error = git_msink_free(msf);
+			if (0 > error)
+				break;
+			msf->block_offset = 0;
+			msf->block_begin += mmap_pagesize();
+		}
+	}
+
+	return error;
+}
+
+git_off_t git_msink_seek(git_msink_file *msf, git_off_t offset, int whence)
+{
+	git_off_t curpos = git_msink_curpos(msf);
+
+	switch (whence)
+	{
+	case SEEK_CUR:
+		offset += curpos;
+		break;
+
+	case SEEK_END:
+		offset += git_msink_length(msf);
+		break;
+	}
+
+	if (0 <= offset) {
+		git_off_t curblock = msf->block_begin;
+		/* if seeking back, make sure to remember the file length */
+		if (curpos > offset && curpos > msf->file_length)
+			msf->file_length = curpos;
+		msf->block_offset = offset % mmap_pagesize();
+		msf->block_begin = offset - msf->block_offset;
+		if (curblock != msf->block_begin)
+			git_msink_free(msf);
+	}
+
+	return offset;
+}
+
+#endif
diff --git a/src/msink.h b/src/msink.h
new file mode 100644
index 00000000000..1f34dd929b7
--- /dev/null
+++ b/src/msink.h
@@ -0,0 +1,58 @@
+/*
+ * Copyright (C) the libgit2 contributors. All rights reserved.
+ *
+ * This file is part of libgit2, distributed under the GNU GPL v2 with
+ * a Linking Exception. For full terms see the included COPYING file.
+ */
+#ifndef INCLUDE_msink_h__
+#define INCLUDE_msink_h__
+
+#ifndef NO_MMAP
+
+#include "common.h"
+#include "map.h"
+#include "posix.h"
+
+typedef struct git_msink_file {
+	git_file fd;
+	git_map block;
+	size_t block_offset;
+	git_off_t block_begin;
+	git_off_t file_length;
+} git_msink_file;
+
+GIT_INLINE(int) git_msink_free(git_msink_file *msf)
+{
+	int error;
+	if (NULL == msf->block.data)
+		return 0;
+
+	error = p_munmap(&msf->block);
+	msf->block.data = NULL;
+	return error;
+}
+
+GIT_INLINE(git_off_t) git_msink_curpos(git_msink_file *msf)
+{
+	return msf->block_begin + msf->block_offset;
+}
+
+GIT_INLINE(git_off_t) git_msink_length(git_msink_file *msf)
+{
+	return max(msf->file_length, git_msink_curpos(msf));
+}
+
+GIT_INLINE(bool) git_msink_truncate(git_msink_file *msf)
+{
+	// might not be able to truncate if any mapped views are open
+	return 0 <= git_msink_free(msf)
+		&& 0 == p_ftruncate(msf->fd, git_msink_length(msf));
+}
+
+extern int git_msink_append(git_msink_file *msf, const void *data, size_t size);
+
+extern git_off_t git_msink_seek(git_msink_file *msf, git_off_t offset, int whence);
+
+#endif
+
+#endif
diff --git a/src/posix.h b/src/posix.h
index f85b1aebd5c..7de8116b152 100644
--- a/src/posix.h
+++ b/src/posix.h
@@ -68,6 +68,7 @@ extern int p_rename(const char *from, const char *to);
 
 #ifndef GIT_WIN32
 
+#define p_ftruncate(fd, size) ftruncate(fd, size)
 #define p_stat(p,b) stat(p, b)
 #define p_chdir(p) chdir(p)
 #define p_rmdir(p) rmdir(p)
@@ -83,6 +84,8 @@ typedef int GIT_SOCKET;
 
 #else
 
+extern int p_ftruncate(int fd, git_off_t size);
+
 typedef SOCKET GIT_SOCKET;
 extern struct tm * p_localtime_r (const time_t *timer, struct tm *result);
 extern struct tm * p_gmtime_r (const time_t *timer, struct tm *result);
diff --git a/src/unix/map.c b/src/unix/map.c
index e62ab3e76dd..190e764b53a 100644
--- a/src/unix/map.c
+++ b/src/unix/map.c
@@ -12,6 +12,11 @@
 #include <sys/mman.h>
 #include <errno.h>
 
+long git_mmap_pagesize(void)
+{
+	return sysconf(_SC_PAGE_SIZE);
+}
+
 int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offset)
 {
 	int mprot = 0;
@@ -34,6 +39,17 @@ int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offs
 	else
 		mflag = MAP_SHARED;
 
+	if (PROT_WRITE == mprot) {
+		/* make sure the file is large enough */
+		struct stat st;
+		git_off_t filesize;
+		if (0 > fstat(fd, &st))
+			return -1;
+		filesize = offset + len;
+		if (st.st_size < filesize && 0 > ftruncate(fd, filesize))
+			return -1;
+	}
+
 	out->data = mmap(NULL, len, mprot, mflag, fd, offset);
 
 	if (!out->data || out->data == MAP_FAILED) {
@@ -55,4 +71,3 @@ int p_munmap(git_map *map)
 }
 
 #endif
-
diff --git a/src/win32/map.c b/src/win32/map.c
index 902ea3994ad..c18bc090d19 100644
--- a/src/win32/map.c
+++ b/src/win32/map.c
@@ -23,6 +23,11 @@ static DWORD get_page_size(void)
 	return page_size;
 }
 
+long git_mmap_pagesize(void)
+{
+	return get_page_size();
+}
+
 int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offset)
 {
 	HANDLE fh = (HANDLE)_get_osfhandle(fd);
@@ -31,8 +36,9 @@ int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offs
 	DWORD view_prot = 0;
 	DWORD off_low = 0;
 	DWORD off_hi = 0;
-	git_off_t page_start;
-	git_off_t page_offset;
+	DWORD end_low;
+	DWORD end_hi;
+	git_off_t end;
 
 	GIT_MMAP_VALIDATE(out, len, prot, flags);
 
@@ -46,6 +52,12 @@ int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offs
 		return -1;
 	}
 
+	if ((offset % page_size) != 0) { /* offset must be multiple of page size */
+		errno = EINVAL;
+		giterr_set(GITERR_OS, "Failed to mmap. Offset must be multiple of page size");
+		return -1;
+	}
+
 	if (prot & GIT_PROT_WRITE)
 		fmap_prot |= PAGE_READWRITE;
 	else if (prot & GIT_PROT_READ)
@@ -56,26 +68,22 @@ int p_mmap(git_map *out, size_t len, int prot, int flags, int fd, git_off_t offs
 	if (prot & GIT_PROT_READ)
 		view_prot |= FILE_MAP_READ;
 
-	page_start = (offset / page_size) * page_size;
-	page_offset = offset - page_start;
+	assert(sizeof(git_off_t) == 8);
 
-	if (page_offset != 0) { /* offset must be multiple of page size */
-		errno = EINVAL;
-		giterr_set(GITERR_OS, "Failed to mmap. Offset must be multiple of page size");
-		return -1;
-	}
+	end = offset + len;
+	end_low = (DWORD) end;
+	end_hi = (DWORD) (end >> 32);
 
-	out->fmh = CreateFileMapping(fh, NULL, fmap_prot, 0, 0, NULL);
+	out->fmh = CreateFileMapping(fh, NULL, fmap_prot, end_hi, end_low, NULL);
 	if (!out->fmh || out->fmh == INVALID_HANDLE_VALUE) {
 		giterr_set(GITERR_OS, "Failed to mmap. Invalid handle value");
 		out->fmh = NULL;
 		return -1;
 	}
 
-	assert(sizeof(git_off_t) == 8);
+	off_low = (DWORD) offset;
+	off_hi = (DWORD) (offset >> 32);
 
-	off_low = (DWORD)(page_start);
-	off_hi = (DWORD)(page_start >> 32);
 	out->data = MapViewOfFile(out->fmh, view_prot, off_hi, off_low, len);
 	if (!out->data) {
 		giterr_set(GITERR_OS, "Failed to mmap. No data written");
diff --git a/src/win32/posix_w32.c b/src/win32/posix_w32.c
index 0d070f6b59e..1e5ee58e43d 100644
--- a/src/win32/posix_w32.c
+++ b/src/win32/posix_w32.c
@@ -696,3 +696,11 @@ int p_inet_pton(int af, const char *src, void *dst)
 	errno = EINVAL;
 	return -1;
 }
+
+int p_ftruncate(int fd, git_off_t size)
+{
+	if (0 > p_lseek(fd, size, SEEK_SET))
+		return -1;
+
+	return SetEndOfFile((HANDLE) _get_osfhandle(fd)) ? 0 : -1;
+}
