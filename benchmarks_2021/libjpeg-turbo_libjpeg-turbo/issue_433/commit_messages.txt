Build: Add missing jpegtran-icc test dependency

The jpegtran-icc test must run after the cjpeg-rgb-islow test, since
the latter generates testout_rgb_islow.jpg.
rdppm.c: Fix buf overrun caused by bad binary PPM

This extends the fix in 1e81b0c3ea26f4ea8f56de05367469333de64a9f to
include binary PPM files with maximum values < 255, thus preventing a
malformed binary PPM input file with those specifications from
triggering an overrun of the rescale array and potentially crashing
cjpeg, TJBench, or any program that uses the tjLoadImage() function.

Fixes #433
rdppm.c: Fix buf overrun caused by bad binary PPM

This extends the fix in 1e81b0c3ea26f4ea8f56de05367469333de64a9f to
include binary PPM files with maximum values < 255, thus preventing a
malformed binary PPM input file with those specifications from
triggering an overrun of the rescale array and potentially crashing
cjpeg, TJBench, or any program that uses the tjLoadImage() function.

Fixes #433
rdppm.c: Fix buf overrun caused by bad binary PPM

This extends the fix in 1e81b0c3ea26f4ea8f56de05367469333de64a9f to
include binary PPM files with maximum values < 255, thus preventing a
malformed binary PPM input file with those specifications from
triggering an overrun of the rescale array and potentially crashing
cjpeg, TJBench, or any program that uses the tjLoadImage() function.

Fixes #433
rdppm.c: Fix buf overrun caused by bad binary PPM

This extends the fix in 1e81b0c3ea26f4ea8f56de05367469333de64a9f to
include binary PPM files with maximum values < 255, thus preventing a
malformed binary PPM input file with those specifications from
triggering an overrun of the rescale array and potentially crashing
cjpeg, TJBench, or any program that uses the tjLoadImage() function.

Fixes #433
rdppm.c: Fix buf overrun caused by bad binary PPM

This extends the fix in 1e81b0c3ea26f4ea8f56de05367469333de64a9f to
include binary PPM files with maximum values < 255, thus preventing a
malformed binary PPM input file with those specifications from
triggering an overrun of the rescale array and potentially crashing
cjpeg, TJBench, or any program that uses the tjLoadImage() function.

Fixes #433
