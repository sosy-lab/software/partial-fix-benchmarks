diff --git a/src/driver-hidpp20.c b/src/driver-hidpp20.c
index 5d04c5cbe..90c5545bf 100644
--- a/src/driver-hidpp20.c
+++ b/src/driver-hidpp20.c
@@ -1561,6 +1561,8 @@ hidpp20drv_probe(struct ratbag_device *device)
 	drv_data = zalloc(sizeof(*drv_data));
 	ratbag_set_drv_data(device, drv_data);
 	hidpp_device_init(&base, device->hidraw[0].fd);
+	base.hid_reports = (struct hidpp_hid_report*) device->hidraw[0].reports;
+	base.num_hid_reports = device->hidraw[0].num_reports;
 	hidpp_device_set_log_handler(&base, hidpp20_log, HIDPP_LOG_PRIORITY_RAW, device);
 
 	device_idx = ratbag_device_data_hidpp20_get_index(device->data);
diff --git a/src/hidpp-generic.c b/src/hidpp-generic.c
index 8977be8eb..2964d2272 100644
--- a/src/hidpp-generic.c
+++ b/src/hidpp-generic.c
@@ -306,6 +306,32 @@ hidpp_read_response(struct hidpp_device *dev, uint8_t *buf, size_t size)
 	return rc >= 0 ? rc : -errno;
 }
 
+void
+hidpp_get_supported_report_types(struct hidpp_device *dev)
+{
+	if (!dev->hid_reports) {
+		hidpp_log_debug(dev, "hidpp: we don't have information about the hid reports, ignoring checks\n");
+		return;
+	}
+
+	/* reset the bits we are gonna check */
+	dev->supported_report_types &= (0xff & ~HIDPP_REPORT_SHORT);
+	dev->supported_report_types &= (0xff & ~HIDPP_REPORT_LONG);
+
+	for (unsigned i = 0; i < dev->num_hid_reports; i++)
+		if (dev->hid_reports[i].usage_page >> 2 & 0xff) /* vendor defined usage page (0xff00-0xffff) */
+			switch (dev->hid_reports[i].report_id) {
+				case REPORT_ID_SHORT:
+					hidpp_log_debug(dev, "hidpp: device supports short reports\n");
+					dev->supported_report_types |= HIDPP_REPORT_SHORT;
+					break;
+				case REPORT_ID_LONG:
+					hidpp_log_debug(dev, "hidpp: device supports long reports\n");
+					dev->supported_report_types |= HIDPP_REPORT_LONG;
+					break;
+			}
+}
+
 void
 hidpp_log(struct hidpp_device *dev,
 	  enum hidpp_log_priority priority,
@@ -362,7 +388,10 @@ void
 hidpp_device_init(struct hidpp_device *dev, int fd)
 {
 	dev->hidraw_fd = fd;
+	dev->hid_reports = NULL;
+	dev->num_hid_reports = 0;
 	hidpp_device_set_log_handler(dev, simple_log, HIDPP_LOG_PRIORITY_INFO, NULL);
+	dev->supported_report_types = 0;
 }
 
 void
diff --git a/src/hidpp-generic.h b/src/hidpp-generic.h
index 113dbc36f..c3549adcf 100644
--- a/src/hidpp-generic.h
+++ b/src/hidpp-generic.h
@@ -95,13 +95,25 @@ typedef void (*hidpp_log_handler)(void *userdata,
 				  const char *format, va_list args)
 	__attribute__ ((format (printf, 3, 0)));
 
+struct hidpp_hid_report {
+	unsigned int report_id;
+	unsigned int usage_page;
+	unsigned int usage;
+};
+
 struct hidpp_device {
 	int hidraw_fd;
+	struct hidpp_hid_report *hid_reports;
+	unsigned num_hid_reports;
 	void *userdata;
 	hidpp_log_handler log_handler;
 	enum hidpp_log_priority log_priority;
+	uint8_t supported_report_types;
 };
 
+#define HIDPP_REPORT_SHORT	(1 << 0)
+#define HIDPP_REPORT_LONG	(1 << 1)
+
 void
 hidpp_device_init(struct hidpp_device *dev, int fd);
 void
@@ -140,6 +152,9 @@ hidpp_write_command(struct hidpp_device *dev, uint8_t *cmd, int size);
 int
 hidpp_read_response(struct hidpp_device *dev, uint8_t *buf, size_t size);
 
+void
+hidpp_get_supported_report_types(struct hidpp_device *dev);
+
 void
 hidpp_log(struct hidpp_device *dev,
 	  enum hidpp_log_priority priority,
diff --git a/src/hidpp20.c b/src/hidpp20.c
index 704933850..808cdad0f 100644
--- a/src/hidpp20.c
+++ b/src/hidpp20.c
@@ -133,6 +133,16 @@ hidpp20_request_command_allow_error(struct hidpp20_device *device, union hidpp20
 	}
 	msg->msg.address |= DEVICE_SW_ID;
 
+	/* some mice don't support short reports */
+	if (msg->msg.report_id == REPORT_ID_SHORT && !(device->base.supported_report_types & HIDPP_REPORT_SHORT))
+		msg->msg.report_id = REPORT_ID_LONG;
+
+	/* sanity check */
+	if (msg->msg.report_id == REPORT_ID_LONG && !(device->base.supported_report_types & HIDPP_REPORT_LONG)) {
+		hidpp_log_error(&device->base, "hidpp20: trying to use unsupported report type\n");
+		return -EINVAL;
+	}
+
 	msg_len = msg->msg.report_id == REPORT_ID_SHORT ? SHORT_MESSAGE_LENGTH : LONG_MESSAGE_LENGTH;
 
 	/* Send the message to the Device */
@@ -2884,6 +2894,8 @@ hidpp20_device_new(const struct hidpp_device *base, unsigned int idx)
 	dev->proto_major = 1;
 	dev->proto_minor = 0;
 
+	hidpp_get_supported_report_types(&(dev->base));
+
 	rc = hidpp20_root_get_protocol_version(dev, &dev->proto_major, &dev->proto_minor);
 	if (rc) {
 		/* communication error, best to ignore the device */
