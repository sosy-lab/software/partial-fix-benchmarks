C89 buildfix
fix #5257
Merge pull request #5258 from fr500/master

fix #5257
Update (#5279)

* Remove horizontal slide animation for XMB

* Add Horizontal Animation configuration

* add translations

* fix one translation error

* Add serial detection for Wii

* Cleanups

* Load Content Special should now produce a filebrowser

* Turn this off

* Revert "(GLUI) Don't select an entry when scrolling"

This reverts commit 9b1ab23aa18cf3e66cf69b9ca7ab2a6059f3b6ba.

* (MUI) Attempt to fix the single click

* Revert "(MUI) Attempt to fix the single click"

This reverts commit d5d0f580b060f67af98b0ab16ec2a69bc019be49.

* Bump up APK code

* fix windows buildbot urls to point to the correct folder

* (MUI) First batch of comments

* Start documenting menu_driver.h

* (MUI) More comments

* (MUI) More comments

* MOre documentation

* More documentation

* Document menu_event.c

* Document menu_cbs.c

* Update Russian translation

* (menu-setting.c) Allow Max Timing Skew to be set to 0

* Set buildbot dir for MSVC 2005 RetroArch build to MSVC 2005 cores

* Add "Delete Core" option to Core Information menu (#5132)

* msvc2005: add missing winmm dependency

* fire tv hack

* buildfix

* extra devices

* if swap override is not specified it should be false

* update CHANGES.md

* [WiiU] Rewrite exception handler

* Update CHANGES.md

* Preliminary MOD/S3M/XM support through ibmx library (part of micromod repository)

* Update CHANGES.md

* First working prototype.

* Buildfix

* Shouldn't be part of images

* Add mod/s3m/xm support to audio mixer in GUI

* Get rid of logging

* Refactor this to be a bit safer with string lists

* Update CHANGES.md

* Buildfixes

* Some more build fixes

* gdi: enable OSD text drawing

* caca: enable OSD text drawing

* Proper fix for MOD support

* buildfix

* Fix some Coverity warnings

* Prevent dereference after null check warning by Coverity

* allow specifying libretro device and analog dpad mode on remap files

* allow saving analog dpad mode and libretro device to remap files

* remove remaps

* add new variables for "content dir"

* add menu settings

* allow savestates and savefiles to go into content dir via bool setting in addition of the empty string

* allow systemfiles into content dir via bool setting in addition of the empty string

* allow screenshots into content dir via bool setting in addition of the empty string

* make all the directories reseteable

* update win32 platform driver

* update android platform driver

* update CHANGES.md

* update CHANGES.md

* Updates

* Try to avoid direct initialization

* (Wii) Backport
https://github.com/SuperrSonic/RA-SS/commit/0574b91595a231e15bafa9f2778d1b9b14944ba6
- untested

* fix new android paths

* (GX) Buildfix

* (GX) Another buildfix

* add unicode-aware option for word_wrap (only needed for xmb)

* Update msg_hash_it.h

* Update msg_hash_it.h

* Update msg_hash_it.h

* (GX) Buildfix

* Get rid of some incompatible implicit declaration warnings

* Add encoding_utf to Salamander

* Get rid of some warnings

* Set PPU_CFLAGS

* (PS3) Fix Salamander

* Cleanup retro_dirent_is_dir

* Update msg_hash_it.h

Update italian language

* Missed break here

* Update CHANGES.md

* Update CHANGES.md

* Update CHANGES.md

* Update CHANGES.md

* Update CHANGES.md

* update CHANGES.md

* C89 buildfix

* (Localization) Update Dutch language

* Update CHANGES.md

* Some C89 buildfixes

* C89 buildfix

* C89 buildfix

* Some C89 buildfixes

* Buildfix

* (xmb) deep list copy - Use malloc instead of calloc

* Cleanup

* ps3: use python2 for pkg.py script

* ps3: copy data folder to dist-cores when ran

* fix #5257

* fix saving the libretro device in overrides

* msvc2005: use non-unicode stb font driver

* ps3: build pkgcrypt.dll and copy it to ps3py folder before running pkg.py

* forgot about mingw not defining _MSC_VER

* Add RETRO_ENVIRONMENT_SET_HW_SHARED_CONTEXT

* Update CHANGES.md

* Bump up to 1.6.4

* Update imports.h

add additional imports  (semaphore)

* Added button press lightup on overlay.

input_overlay_add_inputs added, still need to implement dpad and analog visuals on overlay. Also still needs to be restructured so input_overlay_post_poll is only called once.

* use input_state instead of current_input->input_state.

* Moved input_overlay_add_inputs call out of input_driver.c, shortened arguments.

* reduce scope of input_overlay_add_inputs to file.

* Added analog overlay support to input_overlay_add_inputs.

* Fix MOD support not mixing core provided audio stream

* Add keyboard overlay support to input_overlay_add_inputs.

* Remove commented code in input_driver.c

* Cast ctype args to unsigned char

* Add option and menu setting for viewing inputs on overlay.

* Add support for buttons that are multiple inputs.

* Added sublabel to Show Inputs On Overlay

* Added option to change controller port to listen to for showing overlay input.

* msg_hash_it.h - fix build

* Update CHANGES.md

* Updates

* Updates

* Update msg_hash_it.h

* Update msg_hash_it.h

* (xmb) Improve responsiveness while browsing horizontally

* Try to be somewhat safer here in case of null pointer derefences

* (xmb) Fix segfault when entering certain lists

* (xmb) Defragment and shrink tween list after updates

* Add win32_get_video_output_size

* Add win32_get_video_output_prev

* Fix logic in win32_get_video_output_next

* Create win32_get_video_output_next

* Start filling in more resolution functionality for Windows

* emscripten: only fire keyup once, and don't wait so long to do so

* Don't hide 'Resolution' setting behind compile-time ifdefs anymore

* Start hooking up more resolution functions - not working properly yet

* Revert "Start hooking up more resolution functions - not working properly yet"

This reverts commit dccc9711d9f547777e59278b0ebc77499fb2eee5.

* win32_common.cpp - style nits

* Convert win32_common.cpp to C - gets rid of all the extern "C"
references as well.
Note to bparker - DragAcceptFiles has a minimum dependency for WinXP,
might have to go through a function pointer there or have a
compilation-time ifdef

* Convert wgl_ctx into C - also take care of serious warning

* Convert gdi_ctx.cpp to C

* (Lakka) Fix Online Updater

* (dinput.c) Cleanups

* Cleanup

* Make DragAcceptFiles go through function pointer

* Go through DragAcceptFiles function pointer for ui_win32_window too

* (dinput) Buildfix

* update icons

* update icons

* update icons

* update banner

* d3d9: only use wide char on msvc if UNICODE is defined
Update (#5279) (#5280)

* Remove horizontal slide animation for XMB

* Add Horizontal Animation configuration

* add translations

* fix one translation error

* Add serial detection for Wii

* Cleanups

* Load Content Special should now produce a filebrowser

* Turn this off

* Revert "(GLUI) Don't select an entry when scrolling"

This reverts commit 9b1ab23aa18cf3e66cf69b9ca7ab2a6059f3b6ba.

* (MUI) Attempt to fix the single click

* Revert "(MUI) Attempt to fix the single click"

This reverts commit d5d0f580b060f67af98b0ab16ec2a69bc019be49.

* Bump up APK code

* fix windows buildbot urls to point to the correct folder

* (MUI) First batch of comments

* Start documenting menu_driver.h

* (MUI) More comments

* (MUI) More comments

* MOre documentation

* More documentation

* Document menu_event.c

* Document menu_cbs.c

* Update Russian translation

* (menu-setting.c) Allow Max Timing Skew to be set to 0

* Set buildbot dir for MSVC 2005 RetroArch build to MSVC 2005 cores

* Add "Delete Core" option to Core Information menu (#5132)

* msvc2005: add missing winmm dependency

* fire tv hack

* buildfix

* extra devices

* if swap override is not specified it should be false

* update CHANGES.md

* [WiiU] Rewrite exception handler

* Update CHANGES.md

* Preliminary MOD/S3M/XM support through ibmx library (part of micromod repository)

* Update CHANGES.md

* First working prototype.

* Buildfix

* Shouldn't be part of images

* Add mod/s3m/xm support to audio mixer in GUI

* Get rid of logging

* Refactor this to be a bit safer with string lists

* Update CHANGES.md

* Buildfixes

* Some more build fixes

* gdi: enable OSD text drawing

* caca: enable OSD text drawing

* Proper fix for MOD support

* buildfix

* Fix some Coverity warnings

* Prevent dereference after null check warning by Coverity

* allow specifying libretro device and analog dpad mode on remap files

* allow saving analog dpad mode and libretro device to remap files

* remove remaps

* add new variables for "content dir"

* add menu settings

* allow savestates and savefiles to go into content dir via bool setting in addition of the empty string

* allow systemfiles into content dir via bool setting in addition of the empty string

* allow screenshots into content dir via bool setting in addition of the empty string

* make all the directories reseteable

* update win32 platform driver

* update android platform driver

* update CHANGES.md

* update CHANGES.md

* Updates

* Try to avoid direct initialization

* (Wii) Backport
https://github.com/SuperrSonic/RA-SS/commit/0574b91595a231e15bafa9f2778d1b9b14944ba6
- untested

* fix new android paths

* (GX) Buildfix

* (GX) Another buildfix

* add unicode-aware option for word_wrap (only needed for xmb)

* Update msg_hash_it.h

* Update msg_hash_it.h

* Update msg_hash_it.h

* (GX) Buildfix

* Get rid of some incompatible implicit declaration warnings

* Add encoding_utf to Salamander

* Get rid of some warnings

* Set PPU_CFLAGS

* (PS3) Fix Salamander

* Cleanup retro_dirent_is_dir

* Update msg_hash_it.h

Update italian language

* Missed break here

* Update CHANGES.md

* Update CHANGES.md

* Update CHANGES.md

* Update CHANGES.md

* Update CHANGES.md

* update CHANGES.md

* C89 buildfix

* (Localization) Update Dutch language

* Update CHANGES.md

* Some C89 buildfixes

* C89 buildfix

* C89 buildfix

* Some C89 buildfixes

* Buildfix

* (xmb) deep list copy - Use malloc instead of calloc

* Cleanup

* ps3: use python2 for pkg.py script

* ps3: copy data folder to dist-cores when ran

* fix #5257

* fix saving the libretro device in overrides

* msvc2005: use non-unicode stb font driver

* ps3: build pkgcrypt.dll and copy it to ps3py folder before running pkg.py

* forgot about mingw not defining _MSC_VER

* Add RETRO_ENVIRONMENT_SET_HW_SHARED_CONTEXT

* Update CHANGES.md

* Bump up to 1.6.4

* Update imports.h

add additional imports  (semaphore)

* Added button press lightup on overlay.

input_overlay_add_inputs added, still need to implement dpad and analog visuals on overlay. Also still needs to be restructured so input_overlay_post_poll is only called once.

* use input_state instead of current_input->input_state.

* Moved input_overlay_add_inputs call out of input_driver.c, shortened arguments.

* reduce scope of input_overlay_add_inputs to file.

* Added analog overlay support to input_overlay_add_inputs.

* Fix MOD support not mixing core provided audio stream

* Add keyboard overlay support to input_overlay_add_inputs.

* Remove commented code in input_driver.c

* Cast ctype args to unsigned char

* Add option and menu setting for viewing inputs on overlay.

* Add support for buttons that are multiple inputs.

* Added sublabel to Show Inputs On Overlay

* Added option to change controller port to listen to for showing overlay input.

* msg_hash_it.h - fix build

* Update CHANGES.md

* Updates

* Updates

* Update msg_hash_it.h

* Update msg_hash_it.h

* (xmb) Improve responsiveness while browsing horizontally

* Try to be somewhat safer here in case of null pointer derefences

* (xmb) Fix segfault when entering certain lists

* (xmb) Defragment and shrink tween list after updates

* Add win32_get_video_output_size

* Add win32_get_video_output_prev

* Fix logic in win32_get_video_output_next

* Create win32_get_video_output_next

* Start filling in more resolution functionality for Windows

* emscripten: only fire keyup once, and don't wait so long to do so

* Don't hide 'Resolution' setting behind compile-time ifdefs anymore

* Start hooking up more resolution functions - not working properly yet

* Revert "Start hooking up more resolution functions - not working properly yet"

This reverts commit dccc9711d9f547777e59278b0ebc77499fb2eee5.

* win32_common.cpp - style nits

* Convert win32_common.cpp to C - gets rid of all the extern "C"
references as well.
Note to bparker - DragAcceptFiles has a minimum dependency for WinXP,
might have to go through a function pointer there or have a
compilation-time ifdef

* Convert wgl_ctx into C - also take care of serious warning

* Convert gdi_ctx.cpp to C

* (Lakka) Fix Online Updater

* (dinput.c) Cleanups

* Cleanup

* Make DragAcceptFiles go through function pointer

* Go through DragAcceptFiles function pointer for ui_win32_window too

* (dinput) Buildfix

* update icons

* update icons

* update icons

* update banner

* d3d9: only use wide char on msvc if UNICODE is defined
