RetroArch flickers black on arm mali gpus
My android device with a Mali GPU does not exhibit this issue, but I have noticed it on a Famicom Mini when using XMB. This is the first time I've seen a report in the wild about it though... perhaps it is a bug in a certain version of the Mali driver or something.
I have that too, Odroid U3
On Linux there's a similar? problem when using mali-fbdev with keyboard input. Key presses also go to the console which then tries to draw an input line on the screen causing RetroArch to flicker.
It's running on top of Xorg (Mate desktop), happens with keyboard & joystick
@ensra on NES Mini there is no keyboard or X, just gamepad with mali-fbdev and it still flickers, but only with XMB; rgui is not affected.
@bparker06 You are right, I switched to rgui on the Amazon Fire for now and it works fine.
Hopefully the commit we just pushed should fix this.
Its fixed on my device.
Ah, that's great news to hear!