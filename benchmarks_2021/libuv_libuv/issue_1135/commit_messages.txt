doc: fix wrong man page link for uv_fs_lstat()

PR-URL: https://github.com/libuv/libuv/pull/1134
Reviewed-By: Colin Ihrig <cjihrig@gmail.com>
Reviewed-By: Santiago Gimeno <santiago.gimeno@gmail.com>
Reviewed-By: Imran Iqbal <imran@imraniqbal.org>
Revert "win,tty: add support for ANSI codes in win10 v1511"

Causes regressions on Windows 10 in applications that use ANSI codes.

Also revert one-liner commit 0895ccf ("win: fix typo in type name").

This reverts commit 0895ccfc8ced0c2442c8aab75ccef1f1a6b3938e.
This reverts commit 58ccfd4c210d3cc1a36dd82474976aac296589f2.

Refs: https://github.com/nodejs/node/issues/9542
Refs: https://github.com/libuv/libuv/pull/889
Refs: https://github.com/libuv/libuv/issues/1135
win, tty: handle empty buffer in uv_tty_write_bufs

In uv_tty_write_bufs, if the console supports Virtual Terminal sequences,
we try to convert the passed in utf8 buffer to utf16. However, we need
to check if the buffer is of non-zero length- otherwise,
MultiByteToWideChar returns an error.

Fixes: https://github.com/libuv/libuv/issues/1135
Fixes: https://github.com/nodejs/node/issues/9542
PR-URL: https://github.com/libuv/libuv/pull/1139
Refs: https://github.com/libuv/libuv/pull/889
Reviewed-By: Bartosz Sosnowski <bartosz@janeasystems.com>
Reviewed-By: Ben Noordhuis <info@bnoordhuis.nl>
Reviewed-By: Imran Iqbal <imran@imraniqbal.org>
Revert "win,tty: add support for ANSI codes in win10 v1511"

Causes regressions on Windows 10 in applications that use ANSI codes.

Also reverts commit 0895ccf ("win: fix typo in type name") and
commit d0c2641 ("win, tty: handle empty buffer in uv_tty_write_bufs".)

This reverts commit d0c26414b4287628b2fef25aa7e3971240a92383.
This reverts commit 0895ccfc8ced0c2442c8aab75ccef1f1a6b3938e.
This reverts commit 58ccfd4c210d3cc1a36dd82474976aac296589f2.

Refs: https://github.com/nodejs/node/issues/9542
Refs: https://github.com/libuv/libuv/pull/889
Refs: https://github.com/libuv/libuv/issues/1135
Revert "win,tty: add support for ANSI codes in win10 v1511"

Causes regressions on Windows 10 in applications that use ANSI codes.

Also reverts commit 0895ccf ("win: fix typo in type name") and
commit d0c2641 ("win, tty: handle empty buffer in uv_tty_write_bufs".)

This reverts commit d0c26414b4287628b2fef25aa7e3971240a92383.
This reverts commit 0895ccfc8ced0c2442c8aab75ccef1f1a6b3938e.
This reverts commit 58ccfd4c210d3cc1a36dd82474976aac296589f2.

PR-URL: https://github.com/libuv/libuv/pull/1138
Refs: https://github.com/libuv/libuv/issues/1135
Refs: https://github.com/libuv/libuv/pull/889
Refs: https://github.com/nodejs/node/issues/9542
Reviewed-By: Colin Ihrig <cjihrig@gmail.com>
Reviewed-By: Imran Iqbal <imran@imraniqbal.org>
Reviewed-By: Santiago Gimeno <santiago.gimeno@gmail.com>
