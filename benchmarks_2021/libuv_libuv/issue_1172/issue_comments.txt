Better error when a file descriptor is watched twice
It's not as easy to fix as one might think.

Option 1 is to make `uv__io_start()` and `uv__io_stop()` fallible but I expect that will shift the abort to the caller in many cases, simply because there is no way forward after an unexpected error.

Option 2 is to support multiplexing.  We can probably do that without breaking ABI but it's going to be a lot of work and stands a reasonable chance of introducing regressions.

EDIT: Worth mentioning that the script in nodejs/node#3604 stomps on open file descriptors, including ones that are in use by libuv and node.js.
FWIW, I'd like to see Option 2 implemented for v2 for example.
That seems like a change with no real benefit to me.

Why would someone want to watch one file descriptor multiple times?

Also, should this work across two different loops? That would make the implementation even harder.
> Why would someone want to watch one file descriptor multiple times?

To watch for different events.  Some libraries offer functions for watching fds for readability / writability, and it's easier to work with those by using different poll handles, for example.

Also, libev did this, so porting an application is easier if our semantics are more aligned.

Last, failing with an assert is just ugly.

> Also, should this work across two different loops? That would make the implementation even harder.

Not it won't. Different loops have different uv__io_t arrays, so events would be triggered in both loops just like now. No problem there.
Any progress being made in this? It causing us great deal of trouble at the moment :-( 
Doesn't look like anyone is working on this.
@JendaPlhak Nope. But PRs are welcome if you are up for it!
@bnoordhuis - I guess your option 2 is about allowing multiple watchers for the same fd right? By not making the `fd` as the primary key for the watcher list, instead bloating the list into an array of structures, with each entry can have multiple fd-watcher pairs, and processes every entry for an event on the fd. 

It looks great to me, and may not have side effects.

BTW, I tried asserting `assert(loop->watchers[w->fd] == NULL)` in uv__io_start but it caused a good number of libuv tests to fail.
> I tried asserting `assert(loop->watchers[w->fd] == NULL)` in uv__io_start but it caused a good number of libuv tests to fail.

That's because calling `uv__io_start()` on an active watcher is allowed, e.g., to flip the write bit when the read bit is already set.
given the complexity involved in a proper fix for this corner case issue, as a pragmatic approach can we document the behavior and resolve this? if maintainers agree, I can come up with a write up. thanks.
This issue has been automatically marked as stale because it has not had recent activity. It will be closed if no further activity occurs. Thank you for your contributions.

Sounds like this is a structural issue - maybe addressable in libuv 2?
This issue has been automatically marked as stale because it has not had recent activity. It will be closed if no further activity occurs. Thank you for your contributions.
