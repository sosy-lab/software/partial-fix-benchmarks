escape ASCII control chars in xml

stops some XML parse errors on corrupt metadata

see https://github.com/jcupitt/libvips/issues/1039
fix possible used-before-set in radiance.c

some versions of glib might not zero some parts of the Read struct

see https://github.com/jcupitt/libvips/issues/1040
fix vips7 split on empty filename

a "" filename in vips7 compat mode could trigger a read beyond the end of
the string

see https://github.com/jcupitt/libvips/issues/1040
