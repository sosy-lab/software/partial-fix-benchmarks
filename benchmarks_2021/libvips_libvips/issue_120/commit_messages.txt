oops, don't treat RGB16 as sRGB in colourspace

we still had code to treat RGB16 inputs as sRGB even though we now have
a special set of RGB16 paths

vips copy 16bitRGB.tif x.jpg

now works
set Type on memory strip

we need to set Type on memory strips so we can convert to the output
format correctly, thanks philipgiuliani

see https://github.com/jcupitt/libvips/issues/120
bump image size limits for tiff

see https://github.com/jcupitt/libvips/issues/120#issuecomment-45324200
