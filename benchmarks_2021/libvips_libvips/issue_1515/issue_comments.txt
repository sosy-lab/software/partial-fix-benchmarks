autorot spits out VIPS-WARNING **: can't remove metadata "orientation" on shared image on 8.9.0-rc
You're right, there was a copy missing. It seems to work for me now.

Thanks for reporting this!
Looks fine now to me
Found another one, when `autorotate=true` is set on loading and there are orientation exif tags.

```
$ vipsthumbnail  200x100-orientation-left.jpg[autorotate=true]

(vipsthumbnail:1482): VIPS-WARNING **: can't remove metadata "orientation" on shared image
(vipsthumbnail:1482): VIPS-WARNING **: can't remove metadata "exif-ifd0-Orientation" on shared image
```

with an image like this:
![200x100-orientation-left](https://user-images.githubusercontent.com/47106/71872378-86e19f00-311c-11ea-9509-b664d32d534a.jpg)

Ooop, you're right, I'll fix it.
It should be working now. There was a similar issue with tif orientation.
Looks fine now, thanks a lot.