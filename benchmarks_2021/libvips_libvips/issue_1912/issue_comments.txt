Clarification regarding VipsSourceCustom "read" signal
Hi @akash-akya, it's just like `read()`, so return 0 for EOF. There's a Ruby example here:

https://github.com/libvips/ruby-vips/blob/master/example/connection.rb

I'll have a look at the segv you found.
oh, I did try EOF actually, but it didn't raise any error and operation completed successfully (`vips_image_write_to_file` returned 0), but file is corrupt of course. 

This is roughly what I tried (might not compile)
```c
typedef struct _FileState {
  FILE *handle;
  guint counter;
} FileState;

static gint64 my_read(VipsSourceCustom *source, void *buffer, gint64 length,
                      void *user) {
  FileState *state = (FileState *)user;
  gint64 read;

  if (state->counter == 10) {
    return 0;
  } else {
    read = fread(buffer, sizeof(char), length, state->handle);
  }

  printf("read: %lld\n\r", read);

  state->counter = state->counter + 1;
  return read;
}

static gint64 my_seek(VipsSourceCustom *source, gint64 pos, int whence,
                      void *user) {
  return -1;
}

static bool vips_test() {
  VipsSourceCustom *source;
  VipsImage *image;
  FileState state;  

  source = vips_source_custom_new();
  if (!(state.handle = fopen("dog.jpg", "rb"))) {
    return false;
  }

  state.counter = 0;

  g_signal_connect(source, "read", G_CALLBACK(my_read), &state);
  g_signal_connect(source, "seek", G_CALLBACK(my_seek), &state);

  image = vips_image_new_from_source(VIPS_SOURCE(source), "", "access",
                                     VIPS_ACCESS_SEQUENTIAL, NULL);
  if (!image) {
    vips_error_clear();
    return false;
  }

  if (vips_image_write_to_file(image, "out.jpg", NULL)) {
    error("Failed to write VipsImage. error: %s", vips_error_buffer());
    vips_error_clear();
    return false;
  }

  g_object_unref(image);
  g_object_unref(source);
  fclose(state.handle);

  return true;
}
```

Output:
```c
read: 8
read: 4088
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096

(vix:31591): VIPS-WARNING **: 23:54:14.228: read gave 2 warnings
(vix:31591): VIPS-WARNING **: 23:54:14.228: VipsJpeg: Premature end of JPEG file
VipsJpeg: Premature end of JPEG file

```
I tried a few things, but I've not been able to make it crash. Do you have a test program which shows this bad behaviour?

I added a note to the docs to try to clarify EOF handling.
Ah gotcha. You're right, the write_to_file should return an error code, I suppose something is wrong there. I'll have a look.
> I tried a few things, but I've not been able to make it crash. Do you have a test program which shows this bad behaviour?

ahh, that's just something I tried by returning `-1`. Now that you clarified that we should return EOF for premature termination, it won't crash anymore. Thanks again for quick response! :)
Duh, of course, libvips readers are permissive by default, so truncated files just produce warnings.

If you set the `fail` flag, they will stop on the first warning:

```C
/* compile with:
 *      gcc -g -Wall customsource.c `pkg-config vips --cflags --libs`
 */

#include <vips/vips.h>

typedef struct _FileState {
  FILE *handle;
  guint counter;
} FileState;

static gint64 my_read(VipsSourceCustom *source, void *buffer, gint64 length,
                      void *user) {
  FileState *state = (FileState *)user;
  gint64 read;

  if (state->counter == 10) {
    return 0;
  } else {
    read = fread(buffer, sizeof(char), length, state->handle);
  }

  printf("read: %ld\n\r", read);

  state->counter = state->counter + 1;
  return read;
}

static gint64 my_seek(VipsSourceCustom *source, gint64 pos, int whence,
                      void *user) {
  return -1;
}

int main(int argc, char **argv) {
  VipsSourceCustom *source;
  VipsImage *image;
  FileState state;  

  if (VIPS_INIT(argv[0]))
    vips_error_exit(NULL);

  source = vips_source_custom_new();
  if (!(state.handle = fopen(argv[1], "rb")))
    vips_error_exit("unable to open %s", argv[1]);

  state.counter = 0;

  g_signal_connect(source, "read", G_CALLBACK(my_read), &state);
  g_signal_connect(source, "seek", G_CALLBACK(my_seek), &state);

  image = vips_image_new_from_source(VIPS_SOURCE(source), "", 
                  "access", VIPS_ACCESS_SEQUENTIAL, 
                  "fail", TRUE,
                  NULL);
  if (!image) 
    vips_error_exit(NULL);

  if (vips_image_write_to_file(image, "out.jpg", NULL)) 
    vips_error_exit(NULL);

  printf("successful write\n");

  g_object_unref(image);
  g_object_unref(source);
  fclose(state.handle);

  return 0;
}
```

Gives:

```
$ ./a.out ~/pics/nina.jpg
read: 8
read: 4088
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096
read: 4096

(a.out:79040): VIPS-WARNING **: 18:43:30.270: error in tile 0 x 96

(a.out:79040): VIPS-WARNING **: 18:43:30.271: error in tile 0 x 104
VipsJpeg: Premature end of JPEG file
VipsJpeg: out of order read at line 96
$ 
```

ie. the write failed.
🤦  my bad. I checked https://libvips.github.io/libvips/API/current/VipsImage.html but could not find there.
But it's clearly documented here: https://libvips.github.io/libvips/API/current/VipsForeignSave.html.

Sorry for the false issue