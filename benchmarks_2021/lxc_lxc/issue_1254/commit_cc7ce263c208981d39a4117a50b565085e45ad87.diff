diff --git a/doc/lxc-snapshot.sgml.in b/doc/lxc-snapshot.sgml.in
index 2986a3be52..383bbc9d4e 100644
--- a/doc/lxc-snapshot.sgml.in
+++ b/doc/lxc-snapshot.sgml.in
@@ -51,6 +51,7 @@ Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
       <command>lxc-snapshot</command>
       <arg choice="req">-n, --name <replaceable>name</replaceable></arg>
       <arg choice="opt">-c, --comment <replaceable>file</replaceable></arg>
+      <arg choice="opt">-f, --force</arg>
     </cmdsynopsis>
     <cmdsynopsis>
       <command>lxc-snapshot</command>
@@ -126,6 +127,17 @@ Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
 	   </listitem>
 	  </varlistentry>
 
+	  <varlistentry>
+	    <term> <option>-f,--force </option> </term>
+	   <listitem>
+            <para> Force the snapshot even if the container is running. Please
+            be careful with this mode, as it can produce non-persistent data
+            (typically with non-locked running databases, etc…). Has no effect
+            for a restoration, as it would totally break a running
+            container.</para>
+	   </listitem>
+	  </varlistentry>
+
 	  <varlistentry>
 	    <term> <option>-N, --newname</option> </term>
 	   <listitem>
diff --git a/src/lxc/lxccontainer.c b/src/lxc/lxccontainer.c
index 5ea32fb47b..729457a48a 100644
--- a/src/lxc/lxccontainer.c
+++ b/src/lxc/lxccontainer.c
@@ -3357,9 +3357,9 @@ static bool get_snappath_dir(struct lxc_container *c, char *snappath)
 	return true;
 }
 
-static int do_lxcapi_snapshot(struct lxc_container *c, const char *commentfile)
+static int do_lxcapi_snapshot(struct lxc_container *c, const char *commentfile, int flags)
 {
-	int i, flags, ret;
+	int i, ret;
 	struct lxc_container *c2;
 	char snappath[MAXPATHLEN], newname[20];
 
@@ -3390,7 +3390,7 @@ static int do_lxcapi_snapshot(struct lxc_container *c, const char *commentfile)
 	 * We pass LXC_CLONE_SNAPSHOT to make sure that a rdepends file entry is
 	 * created in the original container
 	 */
-	flags = LXC_CLONE_SNAPSHOT | LXC_CLONE_KEEPMACADDR | LXC_CLONE_KEEPNAME |
+	flags |= LXC_CLONE_SNAPSHOT | LXC_CLONE_KEEPMACADDR | LXC_CLONE_KEEPNAME |
 		LXC_CLONE_KEEPBDEVTYPE | LXC_CLONE_MAYBE_SNAPSHOT;
 	if (bdev_is_dir(c->lxc_conf, c->lxc_conf->rootfs.path)) {
 		ERROR("Snapshot of directory-backed container requested.");
@@ -3447,7 +3447,7 @@ static int do_lxcapi_snapshot(struct lxc_container *c, const char *commentfile)
 	return i;
 }
 
-WRAP_API_1(int, lxcapi_snapshot, const char *)
+WRAP_API_2(int, lxcapi_snapshot, const char *, int)
 
 static void lxcsnap_free(struct lxc_snapshot *s)
 {
diff --git a/src/lxc/lxccontainer.h b/src/lxc/lxccontainer.h
index fbb40bf694..d58f9e3027 100644
--- a/src/lxc/lxccontainer.h
+++ b/src/lxc/lxccontainer.h
@@ -663,12 +663,14 @@ struct lxc_container {
 	 * \param c Container.
 	 * \param commentfile Full path to file containing a description
 	 *  of the snapshot.
+	 * \param flags Additional \c LXC_CLONE* flags to change the cloning behaviour:
+	 *  - \ref LXC_CLONE_FORCE
 	 *
 	 * \return -1 on error, or zero-based snapshot number.
 	 *
 	 * \note \p commentfile may be \c NULL but this is discouraged.
 	 */
-	int (*snapshot)(struct lxc_container *c, const char *commentfile);
+	int (*snapshot)(struct lxc_container *c, const char *commentfile, int flags);
 
 	/*!
 	 * \brief Obtain a list of container snapshots.
diff --git a/src/lxc/tools/lxc_snapshot.c b/src/lxc/tools/lxc_snapshot.c
index 1a79a7a1dc..4f79864f35 100644
--- a/src/lxc/tools/lxc_snapshot.c
+++ b/src/lxc/tools/lxc_snapshot.c
@@ -44,13 +44,14 @@ static const struct option my_longopts[] = {
 	{"destroy", required_argument, 0, 'd'},
 	{"comment", required_argument, 0, 'c'},
 	{"showcomments", no_argument, 0, 'C'},
+	{ "force", no_argument, 0, 'f'},
 	LXC_COMMON_OPTIONS
 };
 
 static struct lxc_arguments my_args = {
 	.progname = "lxc-snapshot",
 	.help = "\
---name=NAME [-P lxcpath] [-L [-C]] [-c commentfile] [-r snapname [-N newname]]\n\
+--name=NAME [-P lxcpath] [-L [-C]] [-c commentfile] [-f] [-r snapname [-N newname]]\n\
 \n\
 lxc-snapshot snapshots a container\n\
 \n\
@@ -61,6 +62,7 @@ Options :\n\
   -N, --newname=NEWNAME  NEWNAME for the restored container\n\
   -d, --destroy=NAME     destroy snapshot NAME, e.g. 'snap0'\n\
                          use ALL to destroy all snapshots\n\
+  -f, --force            force the snapshot even if the container is running\n\
   -c, --comment=FILE     add FILE as a comment\n\
   -C, --showcomments     show snapshot comments\n\
   --rcfile=FILE          Load configuration file FILE\n",
@@ -70,17 +72,19 @@ Options :\n\
 	.task = SNAP,
 };
 
-static int do_snapshot(struct lxc_container *c, char *commentfile);
+static int do_snapshot(struct lxc_container *c, char *commentfile, int flags);
 static int do_snapshot_destroy(struct lxc_container *c, char *snapname);
 static int do_snapshot_list(struct lxc_container *c, int print_comments);
 static int do_snapshot_restore(struct lxc_container *c,
 			       struct lxc_arguments *args);
-static int do_snapshot_task(struct lxc_container *c, enum task task);
+static int do_snapshot_task(struct lxc_container *c, enum task task,
+                   int flags);
 static void print_file(char *path);
 
 int main(int argc, char *argv[])
 {
 	struct lxc_container *c;
+	int flags = 0;
 	int ret;
 
 	if (lxc_arguments_parse(&my_args, argc, argv))
@@ -102,6 +106,9 @@ int main(int argc, char *argv[])
 		}
 	}
 
+	if (my_args.force)
+		flags |= LXC_CLONE_FORCE;
+
 	c = lxc_container_new(my_args.name, my_args.lxcpath[0]);
 	if (!c) {
 		fprintf(stderr, "System error loading container\n");
@@ -130,7 +137,7 @@ int main(int argc, char *argv[])
 		exit(EXIT_FAILURE);
 	}
 
-	ret = do_snapshot_task(c, my_args.task);
+	ret = do_snapshot_task(c, my_args.task, flags);
 
 	lxc_container_put(c);
 
@@ -139,7 +146,7 @@ int main(int argc, char *argv[])
 	exit(EXIT_FAILURE);
 }
 
-static int do_snapshot_task(struct lxc_container *c, enum task task)
+static int do_snapshot_task(struct lxc_container *c, enum task task, int flags)
 {
 	int ret = 0;
 
@@ -154,7 +161,7 @@ static int do_snapshot_task(struct lxc_container *c, enum task task)
 		ret = do_snapshot_restore(c, &my_args);
 		break;
 	case SNAP:
-		ret = do_snapshot(c, my_args.commentfile);
+		ret = do_snapshot(c, my_args.commentfile, flags);
 		break;
 	default:
 		ret = 0;
@@ -181,6 +188,9 @@ static int my_parser(struct lxc_arguments *args, int c, char *arg)
 		args->task = DESTROY;
 		args->snapname = arg;
 		break;
+	case 'f':
+		args->force = 1;
+		break;
 	case 'c':
 		args->commentfile = arg;
 		break;
@@ -192,11 +202,11 @@ static int my_parser(struct lxc_arguments *args, int c, char *arg)
 	return 0;
 }
 
-static int do_snapshot(struct lxc_container *c, char *commentfile)
+static int do_snapshot(struct lxc_container *c, char *commentfile, int flags)
 {
 	int ret;
 
-	ret = c->snapshot(c, commentfile);
+	ret = c->snapshot(c, commentfile, flags);
 	if (ret < 0) {
 		ERROR("Error creating a snapshot");
 		return -1;
diff --git a/src/python-lxc/lxc.c b/src/python-lxc/lxc.c
index b69116d798..ba99cd6cd7 100644
--- a/src/python-lxc/lxc.c
+++ b/src/python-lxc/lxc.c
@@ -1376,6 +1376,7 @@ Container_snapshot(Container *self, PyObject *args, PyObject *kwds)
 {
     char *comment_path = NULL;
     static char *kwlist[] = {"comment_path", NULL};
+    int flags = 0;
     int retval = 0;
     int ret = 0;
     char newname[20];
@@ -1390,7 +1391,7 @@ Container_snapshot(Container *self, PyObject *args, PyObject *kwds)
         assert(comment_path != NULL);
     }
 
-    retval = self->container->snapshot(self->container, comment_path);
+    retval = self->container->snapshot(self->container, comment_path, flags);
 
     Py_XDECREF(py_comment_path);
 
diff --git a/src/tests/snapshot.c b/src/tests/snapshot.c
index b035096f1b..56cd649bed 100644
--- a/src/tests/snapshot.c
+++ b/src/tests/snapshot.c
@@ -83,7 +83,7 @@ int main(int argc, char *argv[])
 	}
 	c->load_config(c, NULL);
 
-	if (c->snapshot(c, NULL) != 0) {
+	if (c->snapshot(c, NULL, 0) != 0) {
 		fprintf(stderr, "%s: %d: failed to create snapshot\n", __FILE__, __LINE__);
 		goto err;
 	}
@@ -139,7 +139,7 @@ int main(int argc, char *argv[])
 		goto good;
 	}
 
-	if (c2->snapshot(c2, NULL) != 0) {
+	if (c2->snapshot(c2, NULL, 0) != 0) {
 		fprintf(stderr, "%s: %d: failed to create snapshot\n", __FILE__, __LINE__);
 		goto err;
 	}
