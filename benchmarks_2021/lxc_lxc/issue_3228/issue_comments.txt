LXC don't allow LXD get IPv4 using a bridge network
Another information i forget to mension, my system don't support cgroup2

**grep cgroup /proc/filesystems**
```
nodev	cgroup
```
Can you show `lxc console NAME --show-log`?
Sure.... 

```
Console log:

systemd 237 running in system mode. (+PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +SECCOMP +BLKID +ELFUTILS +KMOD -IDN2 +IDN -PCRE2 default-hierarchy=hybrid)
Detected virtualization lxc.
Detected architecture x86-64.

Welcome to Ubuntu 18.04.3 LTS!

Set hostname to <u18>.
Initializing machine ID from random generator.
Failed to read AF_UNIX datagram queue length, ignoring: No such file or directory
Failed to install release agent, ignoring: No such file or directory
Failed to create /init.scope control group: Permission denied
Failed to allocate manager object: Permission denied
[!!!!!!] Failed to allocate manager object, freezing.
Freezing execution.

```
Ok, good, that does confirm the cgroup issue, @brauner should be able to look into this regression soon.
Thanks, for the report. I see what the issue is and will have a patch in a bit.