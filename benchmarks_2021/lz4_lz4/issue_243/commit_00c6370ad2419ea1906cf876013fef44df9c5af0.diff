diff --git a/NEWS b/NEWS
index 9cd080e9..0276769e 100644
--- a/NEWS
+++ b/NEWS
@@ -2,13 +2,16 @@ v1.7.2
 Changed : moved to versioning; package, cli and library have same version number
 Improved: Small decompression speed boost
 Improved: Small compression speed improvement on 64-bits systems
-Improved: Performance on ARMv6 and ARMv7
+Improved: Significant speed boost on ARMv6 and ARMv7
+New : cli : --rm command
+New : cli : file attributes are preserved, by Przemyslaw Skibinki
 Added : Debianization, by Evgeniy Polyakov
 Makefile: Generates object files (*.o) for faster (re)compilation on low power systems
-New : recursive mode in benchmark, by Przemyslaw Skibinski
+New : recursive mode in benchmark
 Fix : cli : crash on some invalid inputs
 Fix : cli : -t correctly validates lz4-compressed files, by Nick Terrell
 Fix : better ratio on 64-bits big-endian targets
+Fix : cli : detects and reports fread() errors, thanks to Hiroshi Fujishima report #243
 
 r131
 New    : Dos/DJGPP target, thanks to Louis Santillan (#114)
diff --git a/programs/lz4io.c b/programs/lz4io.c
index 02f4c2b6..73957e18 100644
--- a/programs/lz4io.c
+++ b/programs/lz4io.c
@@ -466,20 +466,21 @@ static int LZ4IO_compressFilename_extRess(cRess_t ress, const char* srcFileName,
 
     /* read first block */
     readSize  = fread(srcBuffer, (size_t)1, blockSize, srcFile);
+    if (ferror(srcFile)) EXM_THROW(30, "Error reading %s ", srcFileName);
     filesize += readSize;
 
     /* single-block file */
     if (readSize < blockSize) {
         /* Compress in single pass */
         size_t const cSize = LZ4F_compressFrame(dstBuffer, dstBufferSize, srcBuffer, readSize, &prefs);
-        if (LZ4F_isError(cSize)) EXM_THROW(34, "Compression failed : %s", LZ4F_getErrorName(cSize));
+        if (LZ4F_isError(cSize)) EXM_THROW(31, "Compression failed : %s", LZ4F_getErrorName(cSize));
         compressedfilesize = cSize;
         DISPLAYUPDATE(2, "\rRead : %u MB   ==> %.2f%%   ",
                       (unsigned)(filesize>>20), (double)compressedfilesize/(filesize+!filesize)*100);   /* avoid division by zero */
 
         /* Write Block */
         {   size_t const sizeCheck = fwrite(dstBuffer, 1, cSize, dstFile);
-            if (sizeCheck!=cSize) EXM_THROW(35, "Write error : cannot write compressed block");
+            if (sizeCheck!=cSize) EXM_THROW(32, "Write error : cannot write compressed block");
     }   }
 
     else
@@ -488,9 +489,9 @@ static int LZ4IO_compressFilename_extRess(cRess_t ress, const char* srcFileName,
     {
         /* Write Archive Header */
         size_t headerSize = LZ4F_compressBegin(ctx, dstBuffer, dstBufferSize, &prefs);
-        if (LZ4F_isError(headerSize)) EXM_THROW(32, "File header generation failed : %s", LZ4F_getErrorName(headerSize));
+        if (LZ4F_isError(headerSize)) EXM_THROW(33, "File header generation failed : %s", LZ4F_getErrorName(headerSize));
         { size_t const sizeCheck = fwrite(dstBuffer, 1, headerSize, dstFile);
-          if (sizeCheck!=headerSize) EXM_THROW(33, "Write error : cannot write header"); }
+          if (sizeCheck!=headerSize) EXM_THROW(34, "Write error : cannot write header"); }
         compressedfilesize += headerSize;
 
         /* Main Loop */
@@ -499,26 +500,26 @@ static int LZ4IO_compressFilename_extRess(cRess_t ress, const char* srcFileName,
 
             /* Compress Block */
             outSize = LZ4F_compressUpdate(ctx, dstBuffer, dstBufferSize, srcBuffer, readSize, NULL);
-            if (LZ4F_isError(outSize)) EXM_THROW(34, "Compression failed : %s", LZ4F_getErrorName(outSize));
+            if (LZ4F_isError(outSize)) EXM_THROW(35, "Compression failed : %s", LZ4F_getErrorName(outSize));
             compressedfilesize += outSize;
             DISPLAYUPDATE(2, "\rRead : %u MB   ==> %.2f%%   ", (unsigned)(filesize>>20), (double)compressedfilesize/filesize*100);
 
             /* Write Block */
             { size_t const sizeCheck = fwrite(dstBuffer, 1, outSize, dstFile);
-              if (sizeCheck!=outSize) EXM_THROW(35, "Write error : cannot write compressed block"); }
+              if (sizeCheck!=outSize) EXM_THROW(36, "Write error : cannot write compressed block"); }
 
             /* Read next block */
             readSize  = fread(srcBuffer, (size_t)1, (size_t)blockSize, srcFile);
             filesize += readSize;
         }
-        if (ferror(srcFile)) EXM_THROW(36, "Error reading %s ", srcFileName);
+        if (ferror(srcFile)) EXM_THROW(37, "Error reading %s ", srcFileName);
 
         /* End of Stream mark */
         headerSize = LZ4F_compressEnd(ctx, dstBuffer, dstBufferSize, NULL);
-        if (LZ4F_isError(headerSize)) EXM_THROW(37, "End of file generation failed : %s", LZ4F_getErrorName(headerSize));
+        if (LZ4F_isError(headerSize)) EXM_THROW(38, "End of file generation failed : %s", LZ4F_getErrorName(headerSize));
 
         { size_t const sizeCheck = fwrite(dstBuffer, 1, headerSize, dstFile);
-          if (sizeCheck!=headerSize) EXM_THROW(38, "Write error : cannot write end of stream"); }
+          if (sizeCheck!=headerSize) EXM_THROW(39, "Write error : cannot write end of stream"); }
         compressedfilesize += headerSize;
     }
 
@@ -532,7 +533,7 @@ static int LZ4IO_compressFilename_extRess(cRess_t ress, const char* srcFileName,
             UTIL_setFileStat(dstFileName, &statbuf);
     }
 
-    if (g_removeSrcFile) { if (remove(srcFileName)) EXM_THROW(39, "Remove error : %s: %s", srcFileName, strerror(errno)); } /* remove source file : --rm */
+    if (g_removeSrcFile) { if (remove(srcFileName)) EXM_THROW(40, "Remove error : %s: %s", srcFileName, strerror(errno)); } /* remove source file : --rm */
 
     /* Final Status */
     DISPLAYLEVEL(2, "\r%79s\r", "");
