Fixed some more RTP extensions negotiations in VideoRoom
Use larger buffer when merging SDPs for WebRTC (fixes #1534)
Use larger buffer by default for SDP (see #1534)
