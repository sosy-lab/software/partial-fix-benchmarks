Videoroom plugin bitrate settings appear to have no effect
Thanks for opening the issue. I already started working on this, but had to stop as I'm attending the IETF meeting. I'll get back to this next week when I get back to the lab.

Echo Test on Janus demo site (1M bitrate limit) (effectively not being limited)

![bit_echo](https://cloud.githubusercontent.com/assets/8722680/5121156/3a775efc-7097-11e4-9226-77e0508e1baa.jpg)

MCU Videoroom on Janus demo site (128000 bitrate limit) (seems locked to ~200kb)

![bit_mcu](https://cloud.githubusercontent.com/assets/8722680/5121184/9e77a696-7097-11e4-8473-ab1d85c236fa.jpg)

MCU Videoroom on my own server (1024000 bitrate limit) (seems locked to ~200kb)

![bit_own](https://cloud.githubusercontent.com/assets/8722680/5121190/a96efff4-7097-11e4-86ac-894ed7ac2874.jpg)

There is no discernible difference when the bitrate setting is changed 8x higher. It seems to be running at some hard coded rate in the MCU plugin. 

Yes, as I anticipated there is some different behaviour when using the Video MCU plugin that needs to be addressed. Working on that.

In rtcp.c,  janus_rtcp_cap_remb does not execute in the MCU because it fails at line 440:

if(rtcp->type == RTCP_PSFB) {

when called from janus_videoroom.c line 1239:

janus_rtcp_cap_remb(buf, len, participant->bitrate);

The rtcp types it is getting in the 

while(rtcp) 

loop is RTCP_SR and RTCP_SDES before exiting. 

Hope this helps.

That's normal, as the plugin iterates on all the compound packets it receives until it finds a REMB one to modify. It doesn't find any (Chrome isn't sending them for sendonly sessions, apparently) and so does nothing. One of the things I was working on was to add or generate a new REMB packet to cap the bandwidth (not sure if I committed this stuff yet), but so far it didn't seem to work.

Okay. Well good luck. With bitrate set to 1024k I'm getting transmission rates of less than 100k on Chrome and 200k on Firefox. The odd thing is that in the Echo Test, Chrome starts out without any bitrate constraint and transmits at around 700k, whereas in the MCU it starts off already constrained to 300k. 

Yes, I know, if you look at the Chrome graphs you'll notice that, despite REMB messages being sent, the available bw reported doesn't move from a default value that likely is the 300k you see. On the other end, the Echo Test instead shows how the REMB messages do have effect. I'm still studying the possible causes, one being the different nature of the session (Echo Test is sendrecv, a Publisher in the MCU is sendonly), other being some RTCP interaction that may be missing or wrong in the MCU case. Hopefully this will get sorted out soon.

It did work before so I'll have to check whether something was changed in Chrome or in Janus. My guess is it's the former, as when I noticed it wasn't working as expected for me, it still was for some of my colleagues who had an older version of the browser.

When I uncomment lines 1219-1229:

```
    /* FIXME Badly: we're blinding forwarding the listener RTCP to the publisher: this probably means confusing him... */
    //~ janus_videoroom_listener *l = (janus_videoroom_listener *)session->participant;
    //~ if(l && l->feed) {
        //~ janus_videoroom_participant *p = l->feed;
        //~ if(p && p->session) {
            //~ if((!video && p->audio_active) || (video && p->video_active)) {
                //~ if(p->bitrate > 0)
                    //~ janus_rtcp_cap_remb(buf, len, p->bitrate);
                //~ gateway->relay_rtcp(p->session->handle, video, buf, len);
            //~ }
        //~ }
    //~ }
```

It works!  As long as I add a listener to the room.

I guess that's because one's forwarding the REMB coming from listeners to the publisher.

In fact, while experimenting I think I found the issue. The main difference with the MCU is that publishers, as I anticipated, are sendonly, which means they only send but do not receive media. Just setting sendrecv in the SDP didn't help, but as soon as I bounced back to a publisher the frames it sent to the MCU (just to have it receive something as well as echo test users do) then the REMB messages started working again.

That said, I was also trying to generate REMB manually pretty much as we do with FIR/PLI (what I already started working on before the IETF) and it doesn't work. I'll have to check why it works when relaying those of listeners instead. The reason it was commented is that it's going to flood the publisher with nonsense info when several listeners are there.

One reason may be that REMB is ignored when sent alone (as I'm doing) while it's not when part of a compound RTCP packet? (RR+SDES+REMB as listeners send).

Ok, got it working... even though it's all very silly.

Apparently a REMB is indeed discarded if it isn't transported in a compound packet with a SR/RR. SR only works if it's a sendrecv session and there's incoming data, though: for a sendonly session as the publisher's, it HAS to be a RR.

For the echo test we just bounce back the SR+SDES+REMB we get modifying the REMB value, which is why it works there: in fact, there it's a sendrecv session, the message we send back has a SR, everything works. To get it working with the MCU, I had to prepend an empty RR with no info and a SDES to a REMB generated every second, and this had the bandwidth working again.

Now I'll just have to make this a bit cleaner, and then I'll push the changes, so that you can check whether it works for you as well.

Closing the issue for now as this fixes it for me, feel free to reopen if that's not the case.

I made a fresh build with latest version and unfortunately this didn't work for me. The google max send rate was still stubbornly stuck at 300k and i remained streaming at 100k, although the room was set for 1024k. I will take another look at this later on today with higher debug levels. 

The changes now only send a single REMB as soon as the first RTP packet is received. I wonder whether this REMB may be missing, e.g., due to the packet loss you're sometimes experiencing. A good idea would be to trigger a new REMB every second or so.

That said, have you tried dynamically changing the bandwidth for a publisher? You can do so typying this command on the JavaScript console for the MCU demo:

```
mcutest.send({"message":{"request":"configure","bitrate":512000}});
```

This will send a new REMB to the publisher with the new bitrate value, so playing with this should clarify if it's just a matter of missing/lost feedback or not. As a result, you should see the bweCompound graph in bweforvideo adapt to reflect the new reported bandwidth availability.

As a side note, what version of Chrome are you using?

Just updated to v41 (was on 39) and I noticed that it seems to ignore the first REMB: that is, the `googAvailableSendBandwidth` value is correct, but the `googTargetEncBitrate` is stuck to 300 and `googActualEncBitrate` is low. Anyway, as soon as you send a different value using the command I gave above, the whole thing starts working, and adaptation goes on as it should. Can you confirm this is what you're getting as well?

My guess is that Chrome doesn't like being shoveled a high bitrate value all of a sudden, and needs to be eased into it. I'll try to send incremental REMB changes at start rather than suddenly passing the target room/participant bitrate, which should do the job.

Thanks! About to test this again now.

Chrome Version 39.0.2171.65 m (64-bit)

I have been using this to publish:

var publish = { "request": "configure", "audio": true, "video": true, "bitrate": 1024000 };
my_handle.send({"message": publish, "jsep": jsep});

I added this debug line in janus_videoroom_incoming_rtp
and on debug level 7 it does not fire at all.

```
        if(sdeslen > 0) {
            /* ... and then finally a REMB */
            JANUS_LOG(LOG_VERB, "Sending rtp REMB\n");
            janus_rtcp_remb((char *)(&rtcpbuf)+rrlen+sdeslen, 24, bitrate);
            gateway->relay_rtcp(handle, video, rtcpbuf, rrlen+sdeslen+24);
        }
```

The only time janus_rtcp_ remb fires is in the message handler at initial configuration. I could try to send   periodic configure requests and see if that works.

LOG_VERB is debug level 5, so you should see it even with lower debug.

This seems to suggest that `janus_rtcp_sdes` fails, but either you should see an error (line 506 of `rtcp.c`) or the arguments are invalid (line 493 of the same file). Can you add some debug to that method as well to see why it's failing for you?

Ok, I will check that. Adding a console configure command after the stream has commenced seems to work fine.

There were no "Buffer too small for SDES message" lines in the log.

It doesn't hit janus_rtcp_sdes except after a "configure" command.

Which means it never passes the if in line 1179 of the videoroom? that doesn't make sense, when a publisher is created `remb_latest` is set to 0 and `remb_startup` to 4 so it most definitely should get there at least once... did you get the latest checkout? did you also issue a `make clean` to make sure there was no leftover object that might mess up?

Just to make sure, try also printing the four variables that if checks, to see why it doesn't seem to be working for you.

Sorry, I ended up playing around in a different direction.  It seems to me that sending a bitrate configure message after a video onplaying event is a much better solution than setting an arbitrary bitrate for everyone, because one can adapt the bitrate to each publisher's video resolution. It's working perfectly. 

I can check those variables, but is it possible that there is only incoming rtcp, and no rtp? 

That part is only involved for publishers, so they most definitely send RTP.
Yes, the configure was there to enable per-publisher settings, so it's good to know it works there. A global bitrate cap can still be useful if one wants to limit bandwidth consumptim as a whole (e.g., server with limited bw). Anyway, if this works for you I guess we can close the issue again.

@gatecrasher777 sorry for replying on an old post, any details on how you solved that