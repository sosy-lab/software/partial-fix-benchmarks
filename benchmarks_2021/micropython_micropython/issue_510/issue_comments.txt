Another case of memory corruption, this time deterministic, cells are suspected
Extra detail: to help investigate this (and few previous cases), I find it helpful to define m_new() to use m_malloc0(), like m_new0(). When I initially did that, I got segfault in mp_obj_cell_get(mp_obj_t self_in) with a clean NULL, which made me think of cell issues.

Again, after further changing Python source, it no longer that clean. But will m_malloc0 it still fails with small pointer values (0x1, 0x04, etc.), while with m_malloc there's clear garbage in pointers.

```
Traceback (most recent call last):
  File "test_http_client.py", line 24, in <module>
  File "/home/pfalcon/projects-3rdparty/micropython-lib/asyncio/asyncio.py", line 50, in run_forever
  File "/home/pfalcon/projects-3rdparty/micropython-lib/asyncio/asyncio.py", line 118, in wait
  File "/home/pfalcon/projects-3rdparty/micropython-lib/asyncio/asyncio.py", line 67, in <lambda>
AttributeError: 'int' object has no attribute 'call_soon'
```

```
Program received signal SIGSEGV, Segmentation fault.
0x08065973 in mp_obj_cell_get (self_in=0x800293e) at ../py/objcell.c:16
16      return self->obj;

(gdb) bt
#0  0x08065973 in mp_obj_cell_get (self_in=0x800293e) at ../py/objcell.c:16
#1  0x08077620 in mp_execute_byte_code_2 (code_info=code_info@entry=0x80a01a0 "\030", ip_in_out=ip_in_out@entry=0xbfffd75c, 
    fastn=fastn@entry=0xbfffd780, sp_in_out=sp_in_out@entry=0xbfffd760, exc_stack=exc_stack@entry=0xbfffd790, 
    exc_sp_in_out=exc_sp_in_out@entry=0xbfffd764, inject_exc=inject_exc@entry=0x0) at ../py/vm.c:318
#2  0x08078856 in mp_execute_byte_code (code=0x80a01a0 "\030", args=0xbfffd8cc, n_args=3, args2=0x80a6214, n_args2=0, ret=0xbfffd820)
    at ../py/vm.c:122
#3  0x08068c66 in fun_bc_call (self_in=0x80a6200, n_args=3, n_kw=0, args=0xbfffd8cc) at ../py/objfun.c:332
#4  0x08061f82 in mp_call_function_n_kw (fun_in=0x80a6200, n_args=3, n_kw=0, args=0xbfffd8cc) at ../py/runtime.c:484
#5  0x08065a70 in closure_call (self_in=0x80a62d0, n_args=2, n_kw=0, args=0x80a6150) at ../py/objclosure.c:28
#6  0x08061f82 in mp_call_function_n_kw (fun_in=0x80a62d0, n_args=2, n_kw=0, args=0x80a6150) at ../py/runtime.c:484
#7  0x08062587 in mp_call_method_n_kw_var (have_self=false, n_args_n_kw=0, args=0x80a6068, args@entry=0x80a6064)
    at ../py/runtime.c:636
```

Here's another nice segfault, it's clear it's string in ptr value:

```
0x08064c74 in mp_obj_subscr (base=0x80a6320, index=0x1, value=value@entry=0xc) at ../py/obj.c:337
337     if (type->subscr != NULL) {
(gdb) print type
$1 = (mp_obj_type_t *) 0x6c6c6143
```

Ok, proof that it's related to free variables/cells is that rewriting line 67 for lambda to avoid being closure fixes segfault:

```
self.add_reader(ret.obj.fileno(), lambda self, c, f: self.call_soon(c, f), self, cb, ret.obj)
```

So, what do we have here? Apparently, cell captures free var pointing to object which later gets GCed. Case of pointer-to-field? Can't be in our case, because we have self which points to the whole object.

Ah, I mean I first added cb to lambda args, it kept segfaulting, and stopped when self was added too.

```
poll:  [((<closure <cell <EpollEventLoop object at 0x2bbd30>> >, (<generator object 'fun-name' at 0x2bfde0>, <_socket 4>)), 1)]
Calling <closure <cell <EpollEventLoop object at 0x2bbd30>> >(<generator object 'fun-name' at 0x2bfde0>, <_socket 4>)
0 174 <generator object 'fun-name' at 0x2bfde0> (<_socket 4>,)
mem: total=247441, current=127128, peak=127180
GC: total: 128000, used: 52304, free: 75696
 No. of 1-blocks: 1021, 2-blocks: 192, max blk sz: 296
Send args: (<_socket 4>,)
<generator object 'fun-name' at 0x2bfde0> finished
epoll.wait -1
poll:  [((<closure (nil) >, (<generator object 'fun-name' at 0x2bfde0>, <_socket 4>)), 1)]
Calling <closure (nil) >(<generator object 'fun-name' at 0x2bfde0>, <_socket 4>)
Segmentation fault
```

So, closure for that lambda had proper self for EpollEventLoop, and suddenly - oops - it got nullized.

```
poll:  [((<closure 0x80a6730 (closed: 0x80a6698) <cell <EpollEventLoop object at 0x8099a50>> >, (<generator object 'fun-name' at 0x809db00>, <_socket 9>)), 1)]
Hardware watchpoint 1: *0x80a6698

Old value = 134859952
New value = 0
__memset_sse2_rep () at ../sysdeps/i386/i686/multiarch/memset-sse2-rep.S:497
497 ../sysdeps/i386/i686/multiarch/memset-sse2-rep.S: No such file or directory.
(gdb) bt
#0  __memset_sse2_rep () at ../sysdeps/i386/i686/multiarch/memset-sse2-rep.S:497
#1  0x08049a25 in m_malloc0 (num_bytes=32) at ../py/malloc.c:92
#2  0x0804aeb2 in vstr_init (vstr=0x80a6680, alloc=32) at ../py/vstr.c:19
#3  0x0804afd9 in vstr_new () at ../py/vstr.c:51
#4  0x0807027c in str_modulo_format (pattern=0x612, n_args=2, args=0x80a6678) at ../py/objstr.c:960
#5  0x0806e5bb in str_binary_op (op=10, lhs_in=0x612, rhs_in=0x80a6670) at ../py/objstr.c:272
#6  0x08061c63 in mp_binary_op (op=10, lhs=0x612, rhs=0x80a6670) at ../py/runtime.c:432
#7  0x08077a3b in mp_execute_byte_code_2 (code_info=0x809ffa0 "(", ip_in_out=0xbfffda64, fastn=0x80a6518, sp_in_out=0xbfffda68, 
    exc_stack=0xbfffd9fc, exc_sp_in_out=0xbfffda6c, inject_exc=0x0) at ../py/vm.c:671
#8  0x08078a32 in mp_execute_byte_code (code=0x809ffa0 "(", args=0x809dc14, n_args=2, args2=0x809c834, n_args2=0, ret=0xbfffdac0)
    at ../py/vm.c:122
```

So, lambda->closed was GCed and now in its place vstr is being allocated. 

But how is this possible? I tried different compilers...

Oh yeah, I forgot that GC blocks are 16 bytes. So, pointer-to-a-field:

```
mp_obj_t mp_obj_new_closure(mp_obj_t fun, mp_obj_t closure_tuple) {
    mp_obj_closure_t *o = m_new_obj(mp_obj_closure_t);
    o->base.type = &closure_type;
    o->fun = fun;
    mp_obj_tuple_get(closure_tuple, &o->n_closed, &o->closed);
    return o;
}
```

If it's closure_tuple, a tuple is what we should store in closure.

Well, it's 2 things: closure_tuple pointer-to-field, and "new" gc_realloc.

Also https://github.com/micropython/micropython/commit/c86889dafb1debfcdc698212289828a73e8b0aee

Pushed directly to master - there're quite a bunch of changes, I was investigating and testing it for awhile, and if we lived with such breakage for so many time, we probably can live with fixes to it ;-).

Btw, it would be nice to optimize closure's cells and get rid of tuple to hold them - but that needs to be done exactly by getting rid of tuple on all levels - including compiler.

#406 not to be forgotten in this regard.

Kudos to you.  I hope you are on the other side of the planet, else you were up all night bug hunting and fixing!

I take responsibility for the pointer-to-field in closure.  I think it's been that way for a long time....  The "new" gc_realloc I was always a bit uneasy about, but couldn't spot any obvious problems with it.

Okay, with 3558f62fb5c21a19a518c2ba75860f0b5963219e puts the closed over variables directly in the closure object (as per @pfalcon's suggestion above).

And dde739d364b0faf48e74a88b65dc2468a3aaf64b continues @pfalcon's work trying to make gc_realloc more sane.

Episodes continue. I have a script (yep, same asyncio WIP) where changing

```
query = "GET / HTTP/1.0\r\n\r\n"
```

to

```
query = "GET / HTTP/1.0\r\nHost: bar\r\n\r\n"
```

leads to segfault.

Well, some more information would be useful :)

I guess try the usual suspects: use old gc_realloc, change heap size, enable stack-overflow detection in VM.

Yeah, working on it. But there's cleat butterfly effect and we need to improve infra to be able to fight such issues. E.g., #518

Fixing #406 in various ways doesn't affect this issue. But going from closure to function kinda fixes it. Kinda, because enabling #518 (which doesn't (shouldn't?) affect execution context) makes it segfault again. That's to exemplify butterfly effect.

OMG, it's "only" case on non-init memory,

```
-#define m_new(type, num) ((type*)(m_malloc(sizeof(type) * (num))))
+#define m_new(type, num) ((type*)(m_malloc0(sizeof(type) * (num))))
```

Fixes it. Apparently, closure code is culprit.

Ok, with deterministic issues framed, non-deterministic issues unleashed. These are dependent on particular run, heap size, compiler version, compiler options, etc. Pretty similar to ones we had with rge-sm.py .

I prepared code to reproduce it, but it still depends on pending pull requests to address other causes for segfaults:
https://github.com/micropython/micropython-lib/commits/asyncio-segfault

> compiler options

For example, segfaults happen with -Os, and don't happen with other optimization levels (note that I did
https://github.com/micropython/micropython/commit/1c1d902cd395f1b521e4fe1e11f8f26f50eafc3f to be able to get all code on the same line wrt to optimization).

Try valgrind? I've not used that tool myself, but I know others at work have used it to find issues.

I don't have too much experience with valgrind either, but what I know and how I used it is that it intended to be used with explicit malloc()/free() alloc model (not GC), and requires libc (in the sense, generic, not app-managed) heap (because it instruments it heavily).

Running it on uPy just gives gazillion of nonsensical warnings of non-initialized values originating where it can't be (like stack allocation in a func which just calls another func). Here's good explanation why has got to provide lot false positives http://www.hpl.hp.com/hosted/linux/mail-archives/gc/2004-November/000629.html .

So, someone who may have more experience with valgrind may want to look at this, but I don't hold my breath, IMHO we'd need to have extensive trace capabilities and process these tracing (effectively, custom valgrind).

I now have a MemoryError running rge-sm.py on a 32-bit system with gcc 4.8.2.  I can fix it with one line:

```
diff --git a/py/vm.c b/py/vm.c
index 8a0bc7d..94bc10c 100644
--- a/py/vm.c
+++ b/py/vm.c
@@ -151,6 +151,7 @@ mp_vm_return_kind_t mp_execute_byte_code(const byte *code, const mp_obj_t *args,
             return MP_VM_RETURN_NORMAL;
         case MP_VM_RETURN_EXCEPTION:
             *ret = state[n_state - 1];
+            if (n_state > 10) {m_free(state, n_state); }
             return MP_VM_RETURN_EXCEPTION;
         case MP_VM_RETURN_YIELD: // byte-code shouldn't yield
         default:
```

Ie, free the VM state if it was allocated on the heap.

Ok, that was an older issue I reported re: rge-sm and 4.8. Good find. But do you understand causes for that? What would keep n_state live on heap?

Also, as you commit it, can you please add symbolic constant for that "10"? (Also, free should happen unconditionally on return, not just for MP_VM_RETURN_EXCEPTION case, right?)

No, I don't understand causes.  Maybe stale pointers are remaining on the heap?

d5e8482c4a401d59688c1de44689d9f3538c7249 adds proper freeing of state.

With latest @dpgeorge's changes, asyncio-segfault as is randomly no longer segfaults on me (gcc 4.8). However, tweaking Host: string again, causes it with with

```
memory allocation failed, allocating 20472 bytes
MemoryError: 
```

or segfaults.

... but the same case passes on 4.7.

Till next time...

Ok, I finally figured out what happens here. It's totally on my side, not uPy. Case of foreign system interface issue. So, this up uses epoll, and it conveniently allows to associate arbitrary value with passed-in fd. CPython blatantly ignores that possibility and forces to use python-level dicts, but I wanted to be smarter and more efficient. But once object reference is handed out to the kernel, it may become unreachable from uPy heap, and thus will be GCed. And then epoll will return it...

And the most depressing thing about is that there's no efficient way out! So, kernel offers scalable way to handle async I/O, but it's all in vain with a GC language. I could cry that we need "fixed object - do not GC", but epoll interface doesn't make it easy to "unfix" such object later. But most importantly, such "fixing" is performance-wise not scalable (would be little evil to just keep "fixed" object alive - but that's not enough, need to keep any object referenced from them). Then, the generic solution is to build own booking structures - in parallel to kernel's, and far from being efficient, in our case.

This is really depressing...

Additionally aggravated by Python's closures-which-do-no-close-anything, by binding variable references, instead of capturing values at the time of definition...

Having reference counting + GC (a la CPython) would not help with this issue, as it would still be collected by the GC.

How would you do it if the memory was managed with malloc/free?

Can't you just keep a reference to the required object in some FFI instance?

Does CPython runs its GC automatically? I thought it does it only on explicit call.

Well, just reference counting would help to keep it alive, but there would be problems with releasing it with current epoll interface. There might be opportunity to work it around, for example, epoll_wait() returns reference to Python object, then it should be passed around the code, until read() is hit, which, if it sees EOF, can "unlock" this object. But that's clearly breaks separation between layers. And would that work for write()? What if events for an fd never fired - its callback object still needs to be released eventually. 

> Can't you just keep a reference to the required object in some FFI instance?

Again, level mismatch, FFI doesn't know semantics  of params. Even if taught a little bit of it ("this arg is callback arg, keep ref to it on heap"), it will be the same problem - helps object will be kept it alive, but not released.

So, it should be handled level higher. And the culprit appear to be kernel's epoll interface - if only it allowed to query data associate with fd... Or returned it on epoll_ctl(EPOLL_CTL_DEL)... Actually, man page mentions that older kernel version _required_ callback structure pointer even for EPOLL_CTL_DEL. And why it says "The event is ignored" for EPOLL_CTL_DEL, I really should check - what if it fills it in after all??

> what if it fills it in after all??

Nope, it doesn't ;-(
