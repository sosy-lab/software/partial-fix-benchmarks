Switching to use of a monotonic clock
Indeed time() is used.
We could  change to clock_gettime(CLOCK_MONOTONIC, xxx)
@miniupnp hi, Thomas.

Thanks for looking into it, and maybe you'll find following useful:

```clock_gettime()``` may be absent in *libc and not exactly in librt, so something like ``` -Wl,--as-needed -lrt -Wl,--no-as-needed``` will be solution for uclibc & similar
```CLOCK_MONOTONIC``` define may be absent on some platforms & ancient kernels and even if define and function exist - ```clock_gettime(CLOCK_MONOTONIC, ...)``` call may fail, so sort of fallback will be still required.
But still, I think it's better than portable ```times()/sysconf(_SC_CLK_TPS)``` because no counter overflows are expected.

@miniupnp shouldn't interface statistics cache
https://github.com/miniupnp/miniupnp/blob/master/miniupnpd/linux/getifstats.c#L51
https://github.com/miniupnp/miniupnp/blob/master/miniupnpd/mac/getifstats.c#L48
https://github.com/miniupnp/miniupnp/blob/master/miniupnpd/bsd/getifstats.c#L43
and  ipv6 pinhole code
https://github.com/miniupnp/miniupnp/blob/master/miniupnpd/netfilter/iptpinhole.c#L461
https://github.com/miniupnp/miniupnp/blob/master/miniupnpd/pf/pfpinhole.c#L382
be switched to upnp_time() as well?
indeed. I missed them because they are in subdirectories
Thank you !
thank you
The latest `miniupnpd 2.0.20180410` is exiting on the first UPnP query
```
miniupnpd[4915]: NAT-PMP request received from 192.168.101.176:59647 12bytes
miniupnpd[4915]: NAT-PMP port mapping request : 4443->192.168.101.176:4443 tcp lifetime=3600s
miniupnpd[4915]: UPnP permission rule 0 matched : port mapping accepted
miniupnpd[4915]: Check protocol tcp for port 4443 on ext_if eth0 10.10.50.62, 3E320A0A
miniupnpd[4915]: cleaning expired Port Mappings
miniupnpd[4915]: setting timeout to 2771498559 sec
miniupnpd[4915]: select(all): Invalid argument
miniupnpd[4915]: Failed to select open sockets. EXITING
```
I'm guessing it is related to this PR.

```
# uname -a
Linux pbx 3.16.54-astlinux #1 SMP PREEMPT Wed Apr 4 13:22:55 CDT 2018 x86_64 GNU/Linux
```

`miniupnpd 2.0.20180203` works fine.
oops

I forgot to check gettimeofday() calls
@abelbeck can you check with my last commit eaaf4f1 ?
I have done the fix in a hurry but I'm not able to test it soon
also available as miniupnpd-2.0.20180412
@miniupnp I built `miniupnpd-2.0.20180412` and it now works with limited testing.

Thanks for the prompt fix.