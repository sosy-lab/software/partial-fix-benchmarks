fix VT100 line drawing characters (#130), proper scrolling and copy/paste
option HandleDPI=false to disable handling of DPI changes (#547, #492)
revising DPI change handling, now on WM_WINDOWPOSCHANGED (#492, #487, #470)
revise DPI handling (#470; #492, #487); always consider individual monitor DPI
scale Options menu in Windows 10 Anniversary update (#492)
fix font scaling behaviour in Windows 7 and XP (#492)
tweaked DPI scaling to avoid terminal resizing on font selection (~#492)
add some debug stuff to analyse window decoration metrics (#597, ~#547, #492)
fruitless attempt to fix DPI scaling of context menu (#588, ~#492)
