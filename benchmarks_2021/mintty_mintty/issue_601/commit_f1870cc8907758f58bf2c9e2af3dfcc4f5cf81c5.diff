diff --git a/docs/mintty.1 b/docs/mintty.1
index 0e9b5028..0e9fb08b 100644
--- a/docs/mintty.1
+++ b/docs/mintty.1
@@ -1642,7 +1642,7 @@ The RowSpacing value is added to that.
 (Corresponds roughly to the xterm resource \fBscaleHeight\fP.)
 
 .TP
-\fBLigatures support\fP (LigaturesSupport=0)
+\fBInteractive Ligatures support\fP (LigaturesSupport=0)
 By default, ligatures, as supported by the selected font, are rendered 
 if they are output to the terminal in one chunk. When this option is 
 set =1, mintty redisplays the left part of the line whenever a character 
@@ -1654,6 +1654,21 @@ This option is not capable of disabling ligatures, however, as they are
 applied by Windows font handling. Setting \fIFontRender=textout\fP 
 disables Uniscribe, including ligatures support.
 
+.TP
+\fBLigatures supported\fP (Ligatures=1)
+This setting can affect the set of ligatures applied, as supported by the 
+selected font.
+When this option is set =1, the default set of ligatures is applied.
+With a value greater than 1, additional ligatures are enabled, e.g. 
+turning "<-", "->", "<--", "-->" into arrows, and "<=", ">=" into 
+less/greater or equal symbols. Note the ambiguity of ligature transformation 
+as e.g. "<=" could as well be meant to be an arrow. There is currently 
+no mechanism to affect ligature transformation in more detail.
+
+This option is not capable of disabling ligatures, however, as they are 
+applied by Windows font handling. Setting \fIFontRender=textout\fP 
+disables Uniscribe, including ligatures support.
+
 .TP
 \fB\fP(ColSpacing=0)
 Additional column padding; ColSpacing=1 can avoid boldened glyphs being clipped.
diff --git a/src/config.c b/src/config.c
index 5b4412ab..fc010fa6 100644
--- a/src/config.c
+++ b/src/config.c
@@ -195,6 +195,7 @@ const config default_cfg = {
   .col_spacing = 0,
   .row_spacing = 0,
   .padding = 1,
+  .ligatures = 1,
   .ligatures_support = 0,
   .handle_dpichanged = 2,
   .check_version_update = 900,
@@ -449,6 +450,7 @@ options[] = {
   {"ColSpacing", OPT_INT, offcfg(col_spacing)},
   {"RowSpacing", OPT_INT, offcfg(row_spacing)},
   {"Padding", OPT_INT, offcfg(padding)},
+  {"Ligatures", OPT_INT, offcfg(ligatures)},
   {"LigaturesSupport", OPT_INT, offcfg(ligatures_support)},
   {"HandleDPI", OPT_INT, offcfg(handle_dpichanged)},
   {"CheckVersionUpdate", OPT_INT, offcfg(check_version_update)},
diff --git a/src/config.h b/src/config.h
index 9d640879..3e331f29 100644
--- a/src/config.h
+++ b/src/config.h
@@ -194,6 +194,7 @@ typedef struct {
   int geom_sync;
   int col_spacing, row_spacing;
   int padding;
+  int ligatures;
   int ligatures_support;
   int handle_dpichanged;
   int check_version_update;
diff --git a/src/wintext.c b/src/wintext.c
index e8a907e8..27482eab 100644
--- a/src/wintext.c
+++ b/src/wintext.c
@@ -2292,12 +2292,18 @@ text_out_start(HDC hdc, LPCWSTR psz, int cch, int *dxs)
   if (!use_uniscribe)
     return;
 
+#if CYGWIN_VERSION_API_MINOR >= 74
+  static SCRIPT_CONTROL sctrl_lig = (SCRIPT_CONTROL){.fMergeNeutralItems = 1};
+#else
+  SCRIPT_CONTROL sctrl_lig = (SCRIPT_CONTROL){.fReserved = 1};
+#endif
   HRESULT hr = ScriptStringAnalyse(hdc, psz, cch, 0, -1, 
     // could | SSA_FIT and use `width` (from win_text) instead of MAXLONG
     // to justify to monospace cell widths;
     // SSA_LINK is needed for Hangul and default-size CJK
     SSA_GLYPHS | SSA_FALLBACK | SSA_LINK, MAXLONG, 
-    NULL, NULL, dxs, NULL, NULL, &ssa);
+    cfg.ligatures > 1 ? &sctrl_lig : 0, 
+    NULL, dxs, NULL, NULL, &ssa);
   if (!SUCCEEDED(hr) && hr != USP_E_SCRIPT_NOT_IN_FONT)
     use_uniscribe = false;
 }
diff --git a/wiki/Changelog.md b/wiki/Changelog.md
index 9f026c98..aab9dfab 100644
--- a/wiki/Changelog.md
+++ b/wiki/Changelog.md
@@ -10,11 +10,13 @@ Font rendering
   * Fix adjusted row spacing ("Leading") for negative font leading (#948, #946).
   * Search bar button symbols configurable (#955).
   * Support optional enforced single-width character rendering (#964).
+  * Support enabling of additional ligatures such as arrows (#601).
 
 Configuration
   * New options MenuTitleCtrlLeft, MenuTitleCtrlRight (#953).
   * Option SearchBar can configure button symbols (#955).
   * New option values Charwidth=single / Charwidth=single-unicode (#964).
+  * New option Ligatures (#601).
 
 Other
   * Fix access to shortcut icon with path prefix %ProgramW6432%.
