diff --git a/docs/mintty.1 b/docs/mintty.1
index bc9f251e..0970b101 100644
--- a/docs/mintty.1
+++ b/docs/mintty.1
@@ -1954,6 +1954,7 @@ pairs of key and action descriptors
 (if a semicolon shall be embedded into any of the action descriptors, 
 a non-whitespace control character (i.e. none of \fB^I^J^K^L^M\fP) can be 
 specified as an alternative separator by starting the whole setting with it).
+(corresponds roughly to the xterm resource \fBtranslations\fP)
 
 Supported keys are described as follows:
 .br
@@ -1963,10 +1964,46 @@ Supported keys are described as follows:
 indicating Ctrl, Alt, Shift as combined with the function key, 
 attached with a "+" separator, e.g. \fBCS+F9\fP
 .br
-\(en any character reachable on the keyboard without Shift; 
-AltGr-generated characters are supported
-
-\fINote:\fP The definition is ignored if the Ctrl+Shift+key combination would 
+\(en special keys (some of which do not occur on typical keyboards); 
+Space, Enter, Esc and Tab are only considered with at least one modifier
+  Tab
+  Esc
+  Enter
+  Space
+  Back
+  Pause
+  Break
+  ScrollLock
+  PrintScreen
+  NumLock
+  LWin
+  RWin
+  Menu
+  Select
+  Print
+  Exec
+  Help
+  Sleep
+  Attn
+  CrSel
+  ExSel
+  ErEof
+  Play
+  Zoom
+.br
+\(en any character reachable on the keyboard without Shift, considered if 
+pressed while Ctrl and Shift are also held; AltGr-generated characters are supported
+
+\fINote:\fP A key that needs a modifier already to be sent (e.g. Break 
+which is often Ctrl+Pause) also needs that modifier in the key definition 
+to be matched.
+
+\fINote:\fP Like in xterm, a key redefinition takes precedence over 
+modifyOtherKeys mode (ESC sequence). Shortcut override mode however 
+disables key redefinitions.
+
+\fINote:\fP For the "any character, with Ctrl and Shift" option, 
+the definition is ignored if the Ctrl+Shift+key combination would 
 generate a valid input character already. This is particularly the case 
 if a valid control character is mapped to the key in the keyboard layout.
 E.g. if you add a definition for Ctrl+Shift+minus and Shift+minus is "_" 
@@ -1979,6 +2016,18 @@ Supported actions are described as follows:
 .br
 \(en \fB`command`\fP: enters the output of the command
 .br
+\(en \fBfullscreen (*)\fP: switches to fullscreen mode
+.br
+\(en \fBtoggle-fullscreen (*)\fP: toggles fullscreen mode
+.br
+\(en \fBwin-max: resizes to window size
+.br
+\(en \fBwin-restore: resizes to normal size
+.br
+\(en \fBwin-icon: turns window into icon
+.br
+\(en \fBclose: closes terminal window
+.br
 \(en \fBnew\fP: opens a new terminal window when key is released
 .br
 \(en \fBoptions\fP: opens the Options dialog
@@ -1989,8 +2038,6 @@ Supported actions are described as follows:
 .br
 \(en \fBsearch\fP: opens the search bar
 .br
-\(en \fBfullscreen (*)\fP: switches to fullscreen mode
-.br
 \(en \fBdefault-size (*)\fP: switches to default window size
 .br
 \(en \fBscrollbar-outer\fP: toggles the scrollbar (xterm compatible)
@@ -2025,7 +2072,7 @@ Supported actions are described as follows:
 .br
 \(en \fBtoggle-char-info\fP: toggles character info display
 .br
-\(en \fBexport-html\fP: exports the screen as an HTML file
+\(en \fBexport-html (*)\fP: exports the screen as an HTML file
 
 \fINote (*):\fP The exact behaviour of some actions depends on the 
 Shift state as being mapped from.
diff --git a/src/wininput.c b/src/wininput.c
index 409fdc36..3fc9bb22 100644
--- a/src/wininput.c
+++ b/src/wininput.c
@@ -1034,6 +1034,10 @@ vk_name(uint key)
 #define trace_key(tag)	
 #endif
 
+/*
+   Some auxiliary functions for user-defined key assignments.
+ */
+
 static void
 menu_text()
 {
@@ -1066,6 +1070,30 @@ newwin_begin()
   newwin_home = false; newwin_monix = 0; newwin_moniy = 0;
 }
 
+static void
+window_full()
+{
+  win_maximise(2);
+}
+
+static void
+window_max()
+{
+  win_maximise(1);
+}
+
+static void
+window_restore()
+{
+  win_maximise(0);
+}
+
+static void
+window_min()
+{
+  win_set_iconic(true);
+}
+
 /*
    Simplified variant of term_cmd().
  */
@@ -1082,6 +1110,38 @@ key_cmd(char * cmd)
   }
 }
 
+static struct {
+  uchar vkey;
+  bool unmod;
+  string nam;
+} vktab[] = {
+  {VK_CANCEL, false, "Break"},
+  {VK_BACK, true, "Back"},
+  {VK_TAB, false, "Tab"},
+  {VK_RETURN, false, "Enter"},
+  {VK_PAUSE, true, "Pause"},
+  {VK_ESCAPE, false, "Esc"},
+  {VK_SPACE, false, "Space"},
+  {VK_SNAPSHOT, true, "PrintScreen"},
+  {VK_LWIN, true, "LWin"},
+  {VK_RWIN, true, "RWin"},
+  {VK_APPS, true, "Menu"},
+  {VK_NUMLOCK, true, "NumLock"},
+  {VK_SCROLL, true, "ScrollLock"},
+  // exotic keys:
+  {VK_SELECT, true, "Select"},
+  {VK_PRINT, true, "Print"},
+  {VK_EXECUTE, true, "Exec"},
+  {VK_HELP, true, "Help"},
+  {VK_SLEEP, true, "Sleep"},
+  {VK_ATTN, true, "Attn"},
+  {VK_CRSEL, true, "CrSel"},
+  {VK_EXSEL, true, "ExSel"},
+  {VK_EREOF, true, "ErEof"},
+  {VK_PLAY, true, "Play"},
+  {VK_ZOOM, true, "Zoom"},
+};
+
 static struct {
   string name;
   union {
@@ -1107,13 +1167,19 @@ static struct {
   {"default-size-zoom", {IDM_DEFSIZE_ZOOM}},
 #endif
 
+  {"fullscreen", {.fct = window_full}},
+  {"win-max", {.fct = window_max}},
+  {"win-restore", {.fct = window_restore}},
+  {"win-icon", {.fct = window_min}},
+  {"close", {.fct = win_close}},
+
   {"new", {.fct = newwin_begin}},
   {"options", {IDM_OPTIONS}},
   {"menu-text", {.fct = menu_text}},
   {"menu-pointer", {.fct = menu_pointer}},
 
   {"search", {IDM_SEARCH}},
-  {"fullscreen", {IDM_FULLSCREEN}},
+  {"toggle-fullscreen", {IDM_FULLSCREEN}},
   {"default-size", {IDM_DEFSIZE}},
   {"scrollbar-outer", {IDM_SCROLLBAR}},
   {"scrollbar-inner", {.fct = toggle_scrollbar}},
@@ -1133,6 +1199,7 @@ static struct {
   {"toggle-logging", {IDM_TOGLOG}},
   {"toggle-char-info", {IDM_TOGCHARINFO}},
   {"export-html", {IDM_HTML}},
+  {"print-screen", {.fct = print_screen}},
 };
 
 bool
@@ -1392,16 +1459,44 @@ win_key_down(WPARAM wp, LPARAM lp)
         return false;
       }
 
+      /* Look up a function tag for either of
+         * (modified) special key (like Tab, Pause, ...)
+         * (modified) function key
+         * Ctrl+Shift-modified character (letter or other layout key)
+         Arguably, Ctrl+Shift-character assignments could be 
+         overridden by modify_other_keys mode, but we stay consistent 
+         with xterm here, where the Translations resource takes 
+         priority over modifyOtherKeys mode.
+       */
       char * tag = 0;
-      if (VK_F1 <= key && key <= VK_F24) {
+      int vki = -1;
+      for (uint i = 0; i < lengthof(vktab); i++)
+        if (key == vktab[i].vkey) {
+          vki = i;
+          break;
+        }
+      if (vki >= 0 && !altgr && (mods || vktab[vki].unmod)) {
+        tag = asform("%s%s%s%s%s",
+                     ctrl ? "C" : "",
+                     alt ? "A" : "",
+                     shift ? "S" : "",
+                     mods ? "+" : "",
+                     vktab[vki].nam);
+      }
+      else if (VK_F1 <= key && key <= VK_F24) {
         tag = asform("%s%s%s%sF%d",
                      ctrl ? "C" : "",
                      alt ? "A" : "",
                      shift ? "S" : "",
                      mods ? "+" : "",
                      key - VK_F1 + 1);
-      }
-      else if ((mods & ~MDK_ALT) == (cfg.ctrl_exchange_shift ? MDK_CTRL : (MDK_CTRL | MDK_SHIFT))) {
+      } else if (
+                 // !term.modify_other_keys &&
+                 (mods & ~MDK_ALT) == (cfg.ctrl_exchange_shift
+                                       ? MDK_CTRL
+                                       : (MDK_CTRL | MDK_SHIFT))
+                )
+      {
         uchar kbd0[256];
         GetKeyboardState(kbd0);
         wchar wbuf[4];
