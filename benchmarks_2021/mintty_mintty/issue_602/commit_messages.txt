Merge branch 'master' of https://github.com/mintty/mintty
fix description of copy/paste shortcuts and refer to option CtrlExchangeShift=yes (#602, #524)
user-defined shortcuts and function keys (#705, #602, #645, #399, #252, ~#726, ~#524, ~#451, ~#523)
user-defined special keys (#705, #602, #645, #399, #252, ~#726, ~#524, ~#451, ~#523)
and more definable functions
user-defined keypad keys (#705, #602, #645, #399, #252, ~#726, ~#524, ~#451, ~#523)
