CTRL + C & CTRL + V copy paste
Duplicate #524, the name of the option is `CtrlExchangeShift`.
And your annotation "like any other application" is not quite true (cf. https://github.com/mintty/mintty/issues/523#issuecomment-193487441) because mintty is a terminal application and as such it needs to be able to provide all control characters to the application running in it, like any other _terminal application_.

Fixed description in 2.7.0.

I'm sorry mintty, I have to agree with JohanVandeplas here. I had issue with lack of copy and paste without doing some work just to activate it. And I beleive JohanVandeplas was implying that most applications use the standard ctrl+ c and ctrl+v. It is realllllllly frustrating. I will look at how to get this done but now your right click copy, right click paste is bugging up.  
You haven't read my previous comment which would also be the response to yours.
Particularly, let me point out again: A terminal application is not a typical Windows application, and Control keys are functional for a terminal, so you cannot just redefine them as meta functions without complication. Especially the request to exchange only the copy/paste keys creates an inconsistency.

Having said this, and also checking related issues, I did not exclude such a feature completely. If you make a proposal how to configure it in a suitable way, _that_ might be helpful.
Actually, this is a duplicate of #645. You might add a "thumbs up" count there to promote the request.
You accuse me of too much. I have read the previous comments and have understood your reasonings. I'm really not sure why right click copy and right click paste only works half of the time. I just there was a more consistently successful way to copy/paste in and out of the mintty terminal. Some times when I paste it immediately executes my command line. And sometimes it does nothing
Pardon on the typo above. Let me correct here.
"I WISH just there was a more consistently successful way to copy/paste in and out of the mintty terminal. "
Now what about `CtrlExchangeShift=yes`, does it not achieve what you want?

About clicks not working: this is a different issue, and I've never heard of such a problem. Could "sometimes/half of the time" mean in different contexts (like e.g. command line / editor)? Do you have "Enter" configured for either Right or Middle mouse button in Options - Mouse?
> why right click copy and right click paste only works half of the time.

Some applications (e.g. Vim with mouse enabled setting) handle mouse by their own.
Shift + RightClick can show the mintty menu even in that case.
While the original poster of this issue (JohanVandeplas) seems to be satisfied with `CtrlExchangeShift`,
@jasonenglehart has not yet responded to my question whether that achieves what he wants, nor has he put a "thumbs-up" to issue #645. So I'm still hesitating whether (and how) to introduce this quirk for the upcoming next release.
Satisfied.. Well now I understand better why it's not so easy... And I'm ok with the provided solution.

jasonenglehart mentioned inconsistency in whether or not commands are executed when pasting. I had that too but I figured it was just because I copied the return in the end..
Have to honest. I do not know what you mean by CtrlExchangeShift. I asked my other com sci buddies and they are confused to. I'm using an US Windows keyboard. Can you elaborate?
Edit your mintty config file (typically $HOME/.minttyrc) with a text editor.
Add the line
```
CtrlExchangeShift=yes
```
Then start a new mintty window.

It's a "hidden setting", not in the GUI Options dialog, because I consider it exotic. That could be changed if desired (with other complications because the "Keys" section is already full, and it would be nice to keep the config dialog small...).
As the answer to the original request "is it possible" is yes (`CtrlExchangeShift`), I'm closing this again.
For the more specific request variant, see #645.
Released 2.9.1.