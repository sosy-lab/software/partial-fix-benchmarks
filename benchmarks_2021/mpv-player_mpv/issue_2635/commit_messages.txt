command: change heuristic for files with 1 chapter

For files with only 1 chapter, the "cycle" command was ignored. Reenable
it, but don't let it terminate playback of the file.

For the full story, see #2550.

Fixes #2550.
vo_opengl: flip image if backend uses flipped rendering

Should fix #2635 (untested).
image_writer: fix writing flipped images as jpg

next_scanline is usually an unsigned int.

Fixes #2635 (again).
