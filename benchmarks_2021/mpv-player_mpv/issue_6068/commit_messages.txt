build: add a version requirement for vulkan pkg-config check

`vkGetPhysicalDeviceProperties2KHR` was added in Vulkan 1.0.39,
but 1.0.61 from Ubuntu Xenial (16.04) was the lowest anyone tried
to build mpv with as of late, so we are marking that as the minimum
required version.

This fixes issues arising during build time from having too old
version of vulkan available on a system, instead of causing a failure
to build.
command: remove the video-aspect property

This property being special in the way it is broke use cases like
"cycle-values" with parameters including -1 or "no".

Unfortunately, this has the potential to break existing user scripts
which rely on the value of "video-aspect" to calculate the video aspect.
It doesn't seem like scripts actually use this value, but if this is
needed then we could introduce a new option in its place, preferably in
a place where it belongs more (like video-out-params).

User scripts can work around the lack of this option by doing e.g.
`video-out-params/dw` / `video-out-params/dh` (or ditto for
video-params) so I don't think this option having the special case it
does is that important.

Fixes #6068.
command: remove the video-aspect property

This property being special in the way it is broke use cases like
"cycle-values" with parameters including -1 or "no".

Unfortunately, this has the potential to break existing user scripts
which rely on the value of "video-aspect" to calculate the video aspect.
It doesn't seem like scripts actually use this value, but if this is
needed then we could introduce a new option in its place, preferably in
a place where it belongs more (like video-out-params).

User scripts can work around the lack of this option by doing e.g.
`video-out-params/dw` / `video-out-params/dh` (or ditto for
video-params) so I don't think this option having the special case it
does is that important.

Fixes #6068.
command: remove the video-aspect property

This property being special in the way it is broke use cases like
"cycle-values" with parameters including -1 or "no".

Unfortunately, this has the potential to break existing user scripts
which rely on the value of "video-aspect" to calculate the video aspect.
It doesn't seem like scripts actually use this value, but if this is
needed then we could introduce a new option in its place, preferably in
a place where it belongs more (like video-out-params).

User scripts can work around the lack of this option by doing e.g.
`video-out-params/dw` / `video-out-params/dh` (or ditto for
video-params) so I don't think this option having the special case it
does is that important.

Fixes #6068.
options: rename --video-aspect to --video-aspect-override

The justification for this is the fact that the `video-aspect` property
doesn't work well with `cycle_values` commands that include the value
"-1".

The "video-aspect" property has effectively no change in behavior, but
we may want to make it read-only in the future. I think it's probably
fine to leave as-is, though.

Fixes #6068.
options: rename --video-aspect to --video-aspect-override

The justification for this is the fact that the `video-aspect` property
doesn't work well with `cycle_values` commands that include the value
"-1".

The "video-aspect" property has effectively no change in behavior, but
we may want to make it read-only in the future. I think it's probably
fine to leave as-is, though.

Fixes #6068.
options: rename --video-aspect to --video-aspect-override

The justification for this is the fact that the `video-aspect` property
doesn't work well with `cycle_values` commands that include the value
"-1".

The "video-aspect" property has effectively no change in behavior, but
we may want to make it read-only in the future. I think it's probably
fine to leave as-is, though.

Fixes #6068.
options: rename --video-aspect to --video-aspect-override

The justification for this is the fact that the `video-aspect` property
doesn't work well with `cycle_values` commands that include the value
"-1".

The "video-aspect" property has effectively no change in behavior, but
we may want to make it read-only in the future. I think it's probably
fine to leave as-is, though.

Fixes #6068.
options: rename --video-aspect to --video-aspect-override

The justification for this is the fact that the `video-aspect` property
doesn't work well with `cycle_values` commands that include the value
"-1".

The "video-aspect" property has effectively no change in behavior, but
we may want to make it read-only in the future. I think it's probably
fine to leave as-is, though.

Fixes #6068.
