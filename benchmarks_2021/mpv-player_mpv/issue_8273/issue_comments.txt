Performance regression with video scaling/rendering
do you mean versions <0.30 plays it fine or do you actually mean <=0.30 (including). there are two conflicting statements (in "Expected behavior"). but i guess you mean the latter from the other info you provided.

please try with `--no-config` and without `--hwdec`. i want to know if it's an actual regression in rendering in cocoa-cb or a regression in hwdec behaviour.

since you actually can build mpv yourself, can you please bisect which commit between 0.30 and 0.31 caused the problem?
also make sure it actually picks the same video from youtube. it probably is safer to download the video and test it with the downloaded one.
Try with `--hdr-compute-peak=no`, it's on by default for some stuff since those versions and is known to be very performance intensive.

**EDIT:** Actually that might not be it after all, since Apple doesn't do compute shaders on OpenGL last I checked.
> EDIT: Actually that might not be it after all, since Apple doesn't do compute shaders on OpenGL last I checked.

The shader in the log is identical. The problem is most likely elsewhere, possibly in the macOS-specific presentation/context code.

(OP also isn't even playing a HDR video, nor is the shader itself doing anything fancy.)
@Akemi Regression is caused by commit  8a0929973de15d9574595c5a098bb3446757ef16.
Though, I'm still unsure of what is causing a similar resource usage increase from full-screening prior versions.
i am still interested if this is something that can only be observed with hwdec or if this happens also with sw decoding. since the hwdec overlay stuff happens right after this, it might interfere with it in some way. so please try with `--no-config` and without `hwdec`.

could we please stop guessing and answer the crucial questions beforehand.
i can observe a slight increase, if one can call it that. i am not really sure what to do about this.

no hwdec (master left, aforementioned commit reverted right):
![nohwdec-right-fix](https://user-images.githubusercontent.com/680386/99295004-24315780-2845-11eb-94c5-cddf419218f6.png)
same with hwdec:
![hwdec-left-fix](https://user-images.githubusercontent.com/680386/99295013-25fb1b00-2845-11eb-89f1-d9544b298b53.png)

Yes, it also occurs without hardware decoding. 
[0.30.log](https://github.com/mpv-player/mpv/files/5549092/0.30.log)
[0.32.log](https://github.com/mpv-player/mpv/files/5549093/0.32.log)
<img width="362" alt="Screen Shot 2020-11-16 at 2 05 38 PM" src="https://user-images.githubusercontent.com/16373102/99296593-021fe100-2815-11eb-9a57-ecb9c67f27cb.png">

Undoing https://github.com/mpv-player/mpv/commit/8a0929973de15d9574595c5a098bb3446757ef16 on master does reduce the GPU usage. Deleting that block all-together also fixes the increased GPU usage from fullscreening.

> i can observe a slight increase, if one can call it that. i am not really sure what to do about this.

About this, on my AMD gpu the difference is very slight aswell
<img width="362" alt="Screen Shot 2020-11-17 at 5 25 48 AM" src="https://user-images.githubusercontent.com/16373102/99378559-8fa31580-2895-11eb-8921-d46e57727f0b.png">
Could you check performance on an intel GPU on your end? 

Edit: For some reason, glClear() is very expensive for the intel iGPU on MacOS. Is there some reason for clearing the FB considering it's going to be overwritten by the next video frame anyways?
see the commit you linked. it's a workaround for another bug. i will do something about this, hopefully this weekend.

can't really check on intel. i don't have any other system as the one with my vega56.
opened a PR to fix this #8369. feel free to test. on a side note, this reverts the behaviour to the behaviour before the mentioned commit. though you will still see the performance regression when the video aspect ratio is different from the fullscreen aspect ration for example. since the condition is true in that case and it will clear the framebuffer.