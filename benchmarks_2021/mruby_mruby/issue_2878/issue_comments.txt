Coverity: null pointer dereference when #method_missing is undefined
I can't reopen the issue but the fix is incomplete and also fails for `n==CALL_MAXARGS`:

``` ruby
undef method_missing
begin; oops(*[*0..127]); rescue => e; p e.args; end
```
