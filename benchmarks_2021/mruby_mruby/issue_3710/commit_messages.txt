Increment the version number to `1.3.0`.
Fixed disclosure of local variables by `eval`; ref #3710

This patch fixes the latter part of #3710. We need to change
`struct REnv` to fix the former part of the issue.
Array size can be cause integer overflow; fix #3710
Now `local_variables` works when for closures; fix #3710
Array size can be cause integer overflow; fix #3710
Get local variable names from orphan block; ref #3710
Merge pull request #5014 from dearblue/local_variables

Get local variable names from orphan block; ref #3710
Get local variable names from orphan block; ref #3710
