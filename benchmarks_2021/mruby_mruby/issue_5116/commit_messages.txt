Remove no longer used `MRB_IV_SEGMENT_SIZE; close #5188 [ci skip]
Fix C source compilation with `MRB_USE_ALL_SYMBOLS`; ref #5187

However, compiling by `mrbc` fails with another issue (#5116).
Symbols should work with `MRB_USE_ALL_SYMBOLS`; fix #5116
Fix `Symbol.all_symbols` to include preallocated symbols; ref #5116
