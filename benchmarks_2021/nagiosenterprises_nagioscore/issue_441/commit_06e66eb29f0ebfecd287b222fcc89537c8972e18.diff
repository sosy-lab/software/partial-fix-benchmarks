diff --git a/Changelog b/Changelog
index 1a21edaab..08591df21 100644
--- a/Changelog
+++ b/Changelog
@@ -17,6 +17,7 @@ FIXES
 * Command line macro detection skips potential macros with no ending dollar sign (Bryan Heden, Jake Omann)
 * Fixed a lockup condition sometimes encountered on shutdown or restart (Aaron Beck)
 * Fixed negative time offset calculations computing incorrectly sometimes (bbeutel)
+* Fixed reloads causing defunct (zombie) processes (#441) (Bryan Heden)
 
 
 4.3.4 - 2017-08-24
diff --git a/base/nagios.c b/base/nagios.c
index 760c87801..dbe005d68 100644
--- a/base/nagios.c
+++ b/base/nagios.c
@@ -767,7 +767,7 @@ int main(int argc, char **argv) {
 			broker_program_state(NEBTYPE_PROCESS_START, NEBFLAG_NONE, NEBATTR_NONE, NULL);
 #endif
 
-			/* initialize status data unless we're starting */
+			/* initialize status data only if we're starting (no restarts) */
 			if(sigrestart == FALSE) {
 				initialize_status_data(config_file);
 				timing_point("Status data initialized\n");
@@ -841,7 +841,7 @@ int main(int argc, char **argv) {
 			qh_deinit(qh_socket_path ? qh_socket_path : DEFAULT_QUERY_SOCKET);
 
 			/* 03/01/2007 EG Moved from sighandler() to prevent FUTEX locking problems under NPTL */
-			/* 03/21/2007 EG SIGSEGV signals are still logged in sighandler() so we don't loose them */
+			/* 03/21/2007 EG SIGSEGV signals are still logged in sighandler() so we don't lose them */
 			/* did we catch a signal? */
 			if(caught_signal == TRUE) {
 
@@ -869,7 +869,7 @@ int main(int argc, char **argv) {
 			/* clean up the scheduled downtime data */
 			cleanup_downtime_data();
 
-			/* clean up the status data unless we're restarting */
+			/* clean up the status data if we are not restarting */
 			if(sigrestart == FALSE) {
 				cleanup_status_data(TRUE);
 				}
@@ -885,6 +885,22 @@ int main(int argc, char **argv) {
 				logit(NSLOG_PROCESS_INFO, TRUE, "Successfully shutdown... (PID=%d)\n", (int)getpid());
 				}
 
+			/* try and wait on any child processes that we didn't
+			   catch with the SIGCHLD handler */
+			if (sigrestart == TRUE) {
+
+				int status = 0;
+				pid_t child_pid;
+				log_debug_info(DEBUGL_PROCESS, 0, "Calling waitpid(,,WNOHANG|WUNTRACED) on all children...\n");
+
+				while ((child_pid = waitpid(-1, &status, WNOHANG | WUNTRACED)) > 0) {
+
+					log_debug_info(DEBUGL_PROCESS, 0, " * child PID: (%d), status: (%d)\n", child_pid, status);
+					}
+
+				log_debug_info(DEBUGL_PROCESS, 0, "All children have been wait()ed on\n");
+				}
+
 			/* close debug log */
 			close_debug_log();
 
diff --git a/base/utils.c b/base/utils.c
index ca0faa960..a3c0abe69 100644
--- a/base/utils.c
+++ b/base/utils.c
@@ -1628,6 +1628,8 @@ void setup_sighandler(void) {
 	sigaction(SIGHUP, &sig_action, NULL);
 	if(daemon_dumps_core == FALSE && daemon_mode == TRUE)
 		sigaction(SIGSEGV, &sig_action, NULL);
+	sig_action.sa_flags = SA_NOCLDWAIT;
+	sigaction(SIGCHLD, &sig_action, NULL);
 #else /* HAVE_SIGACTION */
 	signal(SIGPIPE, SIG_IGN);
 	signal(SIGQUIT, sighandler);
@@ -1635,6 +1637,7 @@ void setup_sighandler(void) {
 	signal(SIGHUP, sighandler);
 	if(daemon_dumps_core == FALSE && daemon_mode == TRUE)
 		signal(SIGSEGV, sighandler);
+	signal(SIGCHLD, sighandler);
 #endif /* HAVE_SIGACTION */
 
 	return;
@@ -1694,6 +1697,15 @@ void sighandler(int sig) {
 	if(sig == SIGHUP)
 		sigrestart = TRUE;
 
+	else if(sig == SIGCHLD) {
+		pid_t child_pid;
+		int status = 0;
+		logit(NSLOG_PROCESS_INFO, FALSE, "Caught SIGCHLD, calling waitpid() with WNOHANG|WUNTRACED\n");
+		while ((child_pid = waitpid(-1, &status, WNOHANG|WUNTRACED)) > 0) {
+			logit(NSLOG_PROCESS_INFO, FALSE, " * waitpid() on child_pid = (%d)\n", (int)child_pid);
+		}
+	}
+
 	/* else begin shutting down... */
 	else if(sig < 16) {
 		logit(NSLOG_PROCESS_INFO, TRUE, "Caught SIG%s, shutting down...\n", sigs[sig]);
diff --git a/base/workers.c b/base/workers.c
index 7ab564f6b..449c862f7 100644
--- a/base/workers.c
+++ b/base/workers.c
@@ -619,7 +619,7 @@ static int handle_worker_result(int sd, int events, void *arg)
 	char *buf, *error_reason = NULL;
 	unsigned long size;
 	int ret;
-	struct kvvec * kvv;
+	static struct kvvec kvv = KVVEC_INITIALIZER;
 	struct wproc_worker *wp = (struct wproc_worker *)arg;
 
 	if(iocache_capacity(wp->ioc) == 0) {
@@ -659,8 +659,7 @@ static int handle_worker_result(int sd, int events, void *arg)
 		}
 
 		/* for everything else we need to actually parse */
-		kvv = buf2kvvec(buf, size, '=', '\0', KVVEC_COPY);
-		if (kvv == NULL) {
+		if (buf2kvvec_prealloc(&kvv, buf, size, '=', '\0', KVVEC_ASSIGN) <= 0) {
 			logit(NSLOG_RUNTIME_ERROR, TRUE,
 				  "wproc: Failed to parse key/value vector from worker response with len %lu. First kv=%s",
 				  size, buf ? buf : "(NULL)");
@@ -670,8 +669,8 @@ static int handle_worker_result(int sd, int events, void *arg)
 		memset(&wpres, 0, sizeof(wpres));
 		wpres.job_id = -1;
 		wpres.type = -1;
-		wpres.response = kvv;
-		parse_worker_result(&wpres, kvv);
+		wpres.response = &kvv;
+		parse_worker_result(&wpres, &kvv);
 
 		job = get_job(wp, wpres.job_id);
 		if (!job) {
@@ -808,8 +807,6 @@ static int handle_worker_result(int sd, int events, void *arg)
 		destroy_job(job);
 	}
 
-	kvvec_destroy(kvv, KVVEC_FREE_ALL);
-
 	return 0;
 }
 
@@ -1004,8 +1001,10 @@ int init_workers(int desired_workers)
 	if (desired_workers < (int)workers.len)
 		return -1;
 
-	while (desired_workers-- > 0)
-		spawn_core_worker();
+	while (desired_workers-- > 0) {
+		int worker_pid = spawn_core_worker();
+		log_debug_info(DEBUGL_WORKERS, 2, "Spawned new worker with pid: (%d)\n", worker_pid);
+	}
 
 	return 0;
 }
diff --git a/include/logging.h b/include/logging.h
index 4a86fb21a..ed98648fd 100644
--- a/include/logging.h
+++ b/include/logging.h
@@ -40,7 +40,7 @@
 #define DEBUGL_ALL                      -1
 #define DEBUGL_NONE                     0
 #define DEBUGL_FUNCTIONS                1
-#define DEBUGL_CONFIG			2
+#define DEBUGL_CONFIG                   2
 #define DEBUGL_PROCESS                  4
 #define DEBUGL_STATUSDATA               4
 #define DEBUGL_RETENTIONDATA            4
@@ -58,9 +58,10 @@
 #define DEBUGL_MACROS                   2048
 #define DEBUGL_IPC                      4096
 #define DEBUGL_SCHEDULING               8192
+#define DEBUGL_WORKERS                  16384
 
 #define DEBUGV_BASIC                    0
-#define DEBUGV_MORE			1
+#define DEBUGV_MORE                     1
 #define DEBUGV_MOST                     2
 
 NAGIOS_BEGIN_DECL
diff --git a/lib/wproc.c b/lib/wproc.c
index 86c984d97..2f12d1eca 100644
--- a/lib/wproc.c
+++ b/lib/wproc.c
@@ -48,7 +48,7 @@ static simple_worker *spawn_worker(void (*init_func)(void *), void *init_arg)
 		simple_worker *worker = calloc(1, sizeof(simple_worker));
 		close(sv[1]);
 		if (!worker) {
-			kill(SIGKILL, pid);
+			kill(pid, SIGKILL);
 			close(sv[0]);
 			return NULL;
 		}
diff --git a/sample-config/nagios.cfg.in b/sample-config/nagios.cfg.in
index d91ac2d20..1cf7f66d2 100644
--- a/sample-config/nagios.cfg.in
+++ b/sample-config/nagios.cfg.in
@@ -1255,20 +1255,23 @@ enable_environment_macros=0
 # be written to the debug file.  OR values together to log multiple
 # types of information.
 # Values: 
-#          -1 = Everything
-#          0 = Nothing
-#	   1 = Functions
-#          2 = Configuration
-#          4 = Process information
-#	   8 = Scheduled events
-#          16 = Host/service checks
-#          32 = Notifications
-#          64 = Event broker
-#          128 = External commands
-#          256 = Commands
-#          512 = Scheduled downtime
-#          1024 = Comments
-#          2048 = Macros
+#         -1      = Everything
+#          0      = Nothing
+#          1      = Functions
+#          2      = Configuration
+#          4      = Process information
+#          8      = Scheduled events
+#          16     = Host/service checks
+#          32     = Notifications
+#          64     = Event broker
+#          128    = External commands
+#          256    = Commands
+#          512    = Scheduled downtime
+#          1024   = Comments
+#          2048   = Macros
+#          4096   = Interprocess communication
+#          8192   = Scheduling
+#          16384  = Workers
 
 debug_level=0
 
