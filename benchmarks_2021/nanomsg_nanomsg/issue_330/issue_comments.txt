Doesn't build on Windows with CMake + mingw32
Would you mind replacing "ADDRESS_FAMILY" in win.h by "short" and reporting back whether it helped?

It does, we're making progress. Not yet successful though:

```
Scanning dependencies of target nanomsg
[  1%] Building C object src/CMakeFiles/nanomsg.dir/core/ep.c.obj
[  2%] Building C object src/CMakeFiles/nanomsg.dir/core/epbase.c.obj
[  3%] Building C object src/CMakeFiles/nanomsg.dir/core/global.c.obj
Z:\sp\Dropbox\Prj\repo\nanomsg\__build\src\nanomsg-0.4-beta\src\core\global.c: In function 'nn_global_submit_errors':
Z:\sp\Dropbox\Prj\repo\nanomsg\__build\src\nanomsg-0.4-beta\src\core\global.c:992:54: error: '_TRUNCATE' undeclared (first use in this function)
                 len = _snprintf_s (curbuf, buf_left, _TRUNCATE,
                                                      ^
Z:\sp\Dropbox\Prj\repo\nanomsg\__build\src\nanomsg-0.4-beta\src\core\global.c:992:54: note: each undeclared identifier is reported only once for each function it appears in
src\CMakeFiles\nanomsg.dir\build.make:99: recipe for target 'src/CMakeFiles/nanomsg.dir/core/global.c.obj' failed
mingw32-make[2]: *** [src/CMakeFiles/nanomsg.dir/core/global.c.obj] Error 1
CMakeFiles\Makefile2:1334: recipe for target 'src/CMakeFiles/nanomsg.dir/all' failed
mingw32-make[1]: *** [src/CMakeFiles/nanomsg.dir/all] Error 2
Makefile:146: recipe for target 'all' failed
mingw32-make: *** [all] Error 2
```

What version of Windows is that, btw?

Windows 8.1 + TDM-GCC.

I'm getting build errors as well on Win7 but once I include limits.h it compiles :)

```
In file included from C:\work\nanomsg\nanomsg\src\aio\../utils/err.h:33:0,
                 from C:\work\nanomsg\nanomsg\src\aio\usock_win.inc:26,
                 from C:\work\nanomsg\nanomsg\src\aio\usock.c:26:
C:\work\nanomsg\nanomsg\src\aio\usock_win.inc: In function 'nn_usock_setsockopt':
C:\work\nanomsg\nanomsg\src\aio\usock_win.inc:193:25: error: 'INT_MAX' undeclared (first use in this function)
     nn_assert (optlen < INT_MAX);
```

as for the Win 8.1 issue while I was fixing the build for mingw I had to specify a windows version as mingw seemed to not set that so [1]
that might cause you're problem

```
    if (MINGW)
        add_definitions (-DNN_HAVE_MINGW -DNN_HAVE_STDINT -D_WIN32_WINNT=0x0600)
    endif ()
```

[1] https://github.com/nanomsg/nanomsg/blob/master/CMakeLists.txt#L39

Can you make a patch for the limits.h thing?

@zerotacg: tried removing `-D_WIN32_WINNT=0x0600` but it made no difference, still getting undeclared `_TRUNCATE`.

removing the define would make it worse I assume you should set it to the correct version not sure which one that is for win8 though

but still, I followed the _TRUNCATE and it is defined in _mingw.h which seems to be included from

```
stdlib.h
|-crtdefs.h
  |-_mingw.h
```

I do not see any conditions that prevent it from being defined though, could you try and investigate why it does not get defined for you?

kind of the same goes for your initial error as well which is defined in winsock2.h there seems to be no condition to prevent i from being defined

for reference, I'm using mingw482_32 with qt

An #undef maybe?

seems not to be the case, maybe I'm just missing something
I'm trying to setup a vm to try compiling on 8.1 and will see how that goes
@stepelu could you give me details of your compiler setup?
like cmake & mingw versions etc

I could build it without problems with my usual qt+mingw setup on a 8.1 vm I'll look into tdm later on and see if we can do something about it

just a quick run on tdm gcc yields the same errors you get and it in there version of winsock2.h ADDRESS_FAMILY is not defined

I've replaced ADDRESS_FAMILY by short in the mainline.

the ADDRESS_FAMILY is just one symptom there are lots of other symbols not defined. I opened a bug on the tdm project [1] to try and get some information why it's missing. I wild guess would be they don't support newer/older/certain versions of windows api which nanomsg is using. While fixing cmake for mingw the first time around I noticed nanomsg requires vista and up, for example
@stepelu if that's an option for you, try a different mingw distro

[1] https://sourceforge.net/p/tdm-gcc/bugs/236/

Thank you for checking this. The reason I'm using TDM-GCC is that I want to avoid mingw-related DLL dependencies. I know you can pass options to mingw to get static linking, but I have not found a way to achieve this consistently independently of the build system used (cmake, make, automaker, batch sripts...).
Regarding the dependency on Vista, is version 1.0 of nanomsg supposed to support WindowsXP?

Only thing that pops up when not compiling for vista is a linking error for CancelIoEx [1][2] not sure how to work around that and if there is anything else that is not compatible

[1] https://github.com/nanomsg/nanomsg/blob/master/src/aio/usock_win.inc#L1034
[2] http://msdn.microsoft.com/en-us/library/windows/desktop/aa363792%28v=vs.85%29.aspx

I've played with CancelIo and CancelIoEx when implementing Windows port. You can replace one by another, but it looks like there's some kind of bug in CancelIo which doesn't occur with CancelIoEx.

John from the tdm project mentions [1] that the used features are not available in the w32api version tdm is based on. You might be able to use an more up-to-date version of the w32api package/module in combination with tdm.

It shouldn't be to hard to implement compatibility though.

[1] https://sourceforge.net/p/tdm-gcc/bugs/236/#e168

I believe this is fixed now.  See bug #212 which addresses a build solution for this; the key components of which are now integrated.

That said, this does _not_ make this work on Windows XP.  This is strictly a Cmake/mingw solution.

I just tested with a fresh install of TDM-MinGW64 and latest CMake 3.40 and, sorry, but it don't work:

```
W:\OpenSource\ZeroMQ\nanomsg\build>"c:\Program Files (x86)\CMake\bin\cmake.exe"
-G "MinGW Makefiles" ..
-- The C compiler identification is GNU 5.1.0
-- Check for working C compiler: C:/TDM-GCC-64/bin/gcc.exe
-- Check for working C compiler: C:/TDM-GCC-64/bin/gcc.exe -- works
-- Detecting C compiler ABI info
-- Detecting C compiler ABI info - done
-- Detecting C compile features
-- Detecting C compile features - done
-- Configuring done
-- Generating done
-- Build files have been written to: W:/OpenSource/ZeroMQ/nanomsg/build

W:\OpenSource\ZeroMQ\nanomsg\build>mingw32-make
Scanning dependencies of target nanomsg
[  0%] Building C object src/CMakeFiles/nanomsg.dir/core/ep.c.obj
[  1%] Building C object src/CMakeFiles/nanomsg.dir/core/epbase.c.obj
[  1%] Building C object src/CMakeFiles/nanomsg.dir/core/global.c.obj
W:\OpenSource\ZeroMQ\nanomsg\src\core\global.c: In function 'nn_global_submit_counter':
W:\OpenSource\ZeroMQ\nanomsg\src\core\global.c:1071:9: warning: implicit declaration of function 'gmtime_r' [-Wimplicit-function-declaration]
         gmtime_r (&numtime, &strtime);
         ^
W:\OpenSource\ZeroMQ\nanomsg\src\core\global.c: In function 'nn_global_submit_errors':
W:\OpenSource\ZeroMQ\nanomsg\src\core\global.c:1174:23: warning: implicit declaration of function '_snprintf_s' [-Wimplicit-function-declaration]
                 len = _snprintf_s (curbuf, buf_left, _TRUNCATE,
                       ^
[  2%] Building C object src/CMakeFiles/nanomsg.dir/core/pipe.c.obj
[  2%] Building C object src/CMakeFiles/nanomsg.dir/core/poll.c.obj
[  3%] Building C object src/CMakeFiles/nanomsg.dir/core/sock.c.obj
[  3%] Building C object src/CMakeFiles/nanomsg.dir/core/sockbase.c.obj
[  4%] Building C object src/CMakeFiles/nanomsg.dir/core/symbol.c.obj
[  4%] Building C object src/CMakeFiles/nanomsg.dir/aio/ctx.c.obj
[  5%] Building C object src/CMakeFiles/nanomsg.dir/aio/fsm.c.obj
[  6%] Building C object src/CMakeFiles/nanomsg.dir/aio/poller.c.obj
[  6%] Building C object src/CMakeFiles/nanomsg.dir/aio/pool.c.obj
[  7%] Building C object src/CMakeFiles/nanomsg.dir/aio/timer.c.obj
[  7%] Building C object src/CMakeFiles/nanomsg.dir/aio/timerset.c.obj
[  8%] Building C object src/CMakeFiles/nanomsg.dir/aio/usock.c.obj
[  8%] Building C object src/CMakeFiles/nanomsg.dir/aio/worker.c.obj
In file included from W:/OpenSource/ZeroMQ/nanomsg/src/utils/err.h:33:0,
                 from W:\OpenSource\ZeroMQ\nanomsg\src\aio\worker_win.inc:27,
                 from W:\OpenSource\ZeroMQ\nanomsg\src\aio\worker.c:26:
W:\OpenSource\ZeroMQ\nanomsg\src\aio\worker_win.inc: In function 'nn_worker_routine':
W:/OpenSource/ZeroMQ/nanomsg/src/utils/fast.h:27:38: warning: passing argument 1 of '__builtin_expect' makes integer from pointer without a cast [-Wint-conversion]
 #define nn_fast(x) __builtin_expect ((x), 1)
                                      ^
W:\OpenSource\ZeroMQ\nanomsg\src\aio\worker_win.inc:176:17: note: in expansion of macro 'nn_fast'
             if (nn_fast (entries [i].lpOverlapped)) {
                 ^
W:/OpenSource/ZeroMQ/nanomsg/src/utils/fast.h:27:38: note: expected 'long int' but argument is of type 'LPOVERLAPPED {aka struct _OVERLAPPED *}'
 #define nn_fast(x) __builtin_expect ((x), 1)
                                      ^
W:\OpenSource\ZeroMQ\nanomsg\src\aio\worker_win.inc:176:17: note: in expansion of macro 'nn_fast'
             if (nn_fast (entries [i].lpOverlapped)) {
                 ^
[  9%] Building C object src/CMakeFiles/nanomsg.dir/utils/alloc.c.obj
[  9%] Building C object src/CMakeFiles/nanomsg.dir/utils/atomic.c.obj
[ 10%] Building C object src/CMakeFiles/nanomsg.dir/utils/chunk.c.obj
[ 10%] Building C object src/CMakeFiles/nanomsg.dir/utils/chunkref.c.obj
[ 11%] Building C object src/CMakeFiles/nanomsg.dir/utils/clock.c.obj
[ 11%] Building C object src/CMakeFiles/nanomsg.dir/utils/closefd.c.obj
[ 12%] Building C object src/CMakeFiles/nanomsg.dir/utils/efd.c.obj
[ 12%] Building C object src/CMakeFiles/nanomsg.dir/utils/err.c.obj
[ 13%] Building C object src/CMakeFiles/nanomsg.dir/utils/glock.c.obj
[ 14%] Building C object src/CMakeFiles/nanomsg.dir/utils/hash.c.obj
[ 14%] Building C object src/CMakeFiles/nanomsg.dir/utils/list.c.obj
[ 15%] Building C object src/CMakeFiles/nanomsg.dir/utils/msg.c.obj
[ 15%] Building C object src/CMakeFiles/nanomsg.dir/utils/mutex.c.obj
[ 16%] Building C object src/CMakeFiles/nanomsg.dir/utils/queue.c.obj
[ 16%] Building C object src/CMakeFiles/nanomsg.dir/utils/random.c.obj
[ 17%] Building C object src/CMakeFiles/nanomsg.dir/utils/sem.c.obj
[ 17%] Building C object src/CMakeFiles/nanomsg.dir/utils/sleep.c.obj
[ 18%] Building C object src/CMakeFiles/nanomsg.dir/utils/thread.c.obj
[ 18%] Building C object src/CMakeFiles/nanomsg.dir/utils/wire.c.obj
[ 19%] Building C object src/CMakeFiles/nanomsg.dir/devices/device.c.obj
[ 19%] Building C object src/CMakeFiles/nanomsg.dir/devices/tcpmuxd.c.obj
[ 20%] Building C object src/CMakeFiles/nanomsg.dir/protocols/utils/dist.c.obj
[ 20%] Building C object src/CMakeFiles/nanomsg.dir/protocols/utils/excl.c.obj
[ 21%] Building C object src/CMakeFiles/nanomsg.dir/protocols/utils/fq.c.obj
[ 22%] Building C object src/CMakeFiles/nanomsg.dir/protocols/utils/lb.c.obj
[ 22%] Building C object src/CMakeFiles/nanomsg.dir/protocols/utils/priolist.c.obj
[ 23%] Building C object src/CMakeFiles/nanomsg.dir/protocols/bus/bus.c.obj
[ 23%] Building C object src/CMakeFiles/nanomsg.dir/protocols/bus/xbus.c.obj
[ 24%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pipeline/push.c.obj
[ 24%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pipeline/pull.c.obj
[ 25%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pipeline/xpull.c.obj
[ 25%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pipeline/xpush.c.obj
[ 26%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pair/pair.c.obj
[ 26%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pair/xpair.c.obj
[ 27%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pubsub/pub.c.obj
[ 27%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pubsub/sub.c.obj
[ 28%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pubsub/trie.c.obj
[ 29%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pubsub/xpub.c.obj
[ 29%] Building C object src/CMakeFiles/nanomsg.dir/protocols/pubsub/xsub.c.obj
[ 30%] Building C object src/CMakeFiles/nanomsg.dir/protocols/reqrep/req.c.obj
[ 30%] Building C object src/CMakeFiles/nanomsg.dir/protocols/reqrep/rep.c.obj
[ 31%] Building C object src/CMakeFiles/nanomsg.dir/protocols/reqrep/task.c.obj
[ 31%] Building C object src/CMakeFiles/nanomsg.dir/protocols/reqrep/xrep.c.obj
[ 32%] Building C object src/CMakeFiles/nanomsg.dir/protocols/reqrep/xreq.c.obj
[ 32%] Building C object src/CMakeFiles/nanomsg.dir/protocols/survey/respondent.c.obj
[ 33%] Building C object src/CMakeFiles/nanomsg.dir/protocols/survey/surveyor.c.obj
[ 33%] Building C object src/CMakeFiles/nanomsg.dir/protocols/survey/xrespondent.c.obj
[ 34%] Building C object src/CMakeFiles/nanomsg.dir/protocols/survey/xsurveyor.c.obj
[ 34%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/backoff.c.obj
[ 35%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/dns.c.obj
[ 35%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/iface.c.obj
[ 36%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/literal.c.obj
[ 37%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/port.c.obj
[ 37%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/streamhdr.c.obj
[ 38%] Building C object src/CMakeFiles/nanomsg.dir/transports/utils/base64.c.obj
[ 38%] Building C object src/CMakeFiles/nanomsg.dir/transports/inproc/binproc.c.obj
[ 39%] Building C object src/CMakeFiles/nanomsg.dir/transports/inproc/cinproc.c.obj
[ 39%] Building C object src/CMakeFiles/nanomsg.dir/transports/inproc/inproc.c.obj
[ 40%] Building C object src/CMakeFiles/nanomsg.dir/transports/inproc/ins.c.obj
[ 40%] Building C object src/CMakeFiles/nanomsg.dir/transports/inproc/msgqueue.c.obj
[ 41%] Building C object src/CMakeFiles/nanomsg.dir/transports/inproc/sinproc.c.obj
[ 41%] Building C object src/CMakeFiles/nanomsg.dir/transports/ipc/aipc.c.obj
[ 42%] Building C object src/CMakeFiles/nanomsg.dir/transports/ipc/bipc.c.obj
[ 42%] Building C object src/CMakeFiles/nanomsg.dir/transports/ipc/cipc.c.obj
[ 43%] Building C object src/CMakeFiles/nanomsg.dir/transports/ipc/ipc.c.obj
[ 43%] Building C object src/CMakeFiles/nanomsg.dir/transports/ipc/sipc.c.obj
[ 44%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcp/atcp.c.obj
[ 45%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcp/btcp.c.obj
[ 45%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcp/ctcp.c.obj
[ 46%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcp/stcp.c.obj
[ 46%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcp/tcp.c.obj
[ 47%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcpmux/atcpmux.c.obj
[ 47%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcpmux/btcpmux.c.obj
[ 48%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcpmux/ctcpmux.c.obj
[ 48%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcpmux/stcpmux.c.obj
[ 49%] Building C object src/CMakeFiles/nanomsg.dir/transports/tcpmux/tcpmux.c.obj
[ 49%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/aws.c.obj
[ 50%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/bws.c.obj
[ 50%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/cws.c.obj
[ 51%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/sws.c.obj
[ 52%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/ws.c.obj
[ 52%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/ws_handshake.c.obj
[ 53%] Building C object src/CMakeFiles/nanomsg.dir/transports/ws/sha1.c.obj
[ 53%] Linking C shared library ..\libnanomsg.dll
CMakeFiles\nanomsg.dir/objects.a(global.c.obj):global.c:(.text+0x2026): undefined reference to `gmtime_r'
CMakeFiles\nanomsg.dir/objects.a(global.c.obj):global.c:(.text+0x2232): undefined reference to `gmtime_r'
CMakeFiles\nanomsg.dir/objects.a(global.c.obj):global.c:(.text+0x23f8): undefined reference to `gmtime_r'
collect2.exe: error: ld returned 1 exit status
src\CMakeFiles\nanomsg.dir\build.make:2643: recipe for target 'libnanomsg.dll' failed
mingw32-make[2]: *** [libnanomsg.dll] Error 1
CMakeFiles\Makefile2:1675: recipe for target 'src/CMakeFiles/nanomsg.dir/all' failed
mingw32-make[1]: *** [src/CMakeFiles/nanomsg.dir/all] Error 2
Makefile:159: recipe for target 'all' failed
mingw32-make: *** [all] Error 2
```

Argh.  So mingw is problematic.  Its this weird mutant that is neither Windows nor POSIX.  Aargh.

Its looks like I'm going to have to actually install this stupid beast to debug it properly, since I can't seem to get anyone else to provide reasonably correct PRs for it.  Grrrr....

For what is worth, I moved to Visual Studio 2015.
I feel your pain, thanks for your work.
On 18 Nov 2015 17:47, "gdamore" notifications@github.com wrote:

> Argh. So mingw is problematic. Its this weird mutant that is neither
> Windows nor POSIX. Aargh.
> 
> Its looks like I'm going to have to actually install this stupid beast to
> debug it properly, since I can't seem to get anyone else to provide
> reasonably correct PRs for it. Grrrr....
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/nanomsg/nanomsg/issues/330#issuecomment-157797205.

Visual Studio was the right choice.  I think its free, yes?

The plain version yes, it is.
On 18 Nov 2015 17:53, "gdamore" notifications@github.com wrote:

> Visual Studio was the right choice. I think its free, yes?
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/nanomsg/nanomsg/issues/330#issuecomment-157800283.

So we need to #include <stdio.h> for _sprintf_s on Windows.
And I think we should also use gmtime_s on windows regardless of mingw or not -- this isn't cygwin, its just mingw with the Windows APIs.

Finally it appears that we should be checking against NULL with lpOverlapped.  That's a pointer.

Ok, got those fixed, and went through all holy hell to get mingw compilation "working" on AppVeyor, only to discover that mingw is missing a bunch of stuff we need for Winsock.

I'm throwing in the towel on this.  Use Visual Studio.  It's free.  If someone wants to submit a PR that actually _fixes_ these issues, I'm happy to take a look.  But otherwise, my answer is "Use Visual Studio".

(Or even better, use a POSIX platform -- Linux, Solaris, etc.)

Closing as will-not-fix for now.

The mingw version bundled with qt works [1]

The API provided seems to be defined in the bundled w32api package [2]
I'm having a hard time though to determine the version included in the different mingw installations.

[1] https://ci.appveyor.com/project/zerotacg/nanomsg
[2] https://github.com/nanomsg/nanomsg/issues/330#issuecomment-64259132
