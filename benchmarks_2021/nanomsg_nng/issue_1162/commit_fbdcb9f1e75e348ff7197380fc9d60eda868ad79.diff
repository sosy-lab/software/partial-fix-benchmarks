diff --git a/src/protocol/reqrep0/req.c b/src/protocol/reqrep0/req.c
index b8ca498d..781cacf5 100644
--- a/src/protocol/reqrep0/req.c
+++ b/src/protocol/reqrep0/req.c
@@ -37,6 +37,7 @@ struct req0_ctx {
 	uint32_t       request_id; // request ID, without high bit set
 	nni_aio *      recv_aio;   // user aio waiting to recv - only one!
 	nni_aio *      send_aio;   // user aio waiting to send
+	req0_pipe *    req_pipe;   // pipe we are using
 	nng_msg *      req_msg;    // request message (owned by protocol)
 	size_t         req_len;    // length of request message (for stats)
 	nng_msg *      rep_msg;    // reply message
@@ -67,6 +68,8 @@ struct req0_pipe {
 	req0_sock *   req;
 	nni_list_node node;
 	nni_list      contexts; // contexts with pending traffic
+	nni_list      resend;   // contexts waiting to resend
+	bool          busy;
 	bool          closed;
 	nni_aio       aio_send;
 	nni_aio       aio_recv;
@@ -180,6 +183,7 @@ req0_pipe_init(void *arg, nni_pipe *pipe, void *s)
 	nni_aio_init(&p->aio_send, req0_send_cb, p);
 	NNI_LIST_NODE_INIT(&p->node);
 	NNI_LIST_INIT(&p->contexts, req0_ctx, pipe_node);
+	NNI_LIST_INIT(&p->resend, req0_ctx, send_node);
 	p->pipe = pipe;
 	p->req  = s;
 	return (0);
@@ -205,6 +209,56 @@ req0_pipe_start(void *arg)
 	return (0);
 }
 
+static void
+req0_pipe_resend(req0_pipe *p, bool done)
+{
+	req0_ctx * ctx;
+	req0_sock *s = p->req;
+
+	if ((!done) && p->busy) {
+		// If we already are processing the queue, then there
+		// is nothing to else to do.  (We only get called on
+		// not being busy from our callback.
+		return;
+	}
+	// We already running the queue.
+	if ((ctx = nni_list_first(&p->resend)) != NULL) {
+		nni_list_remove(&p->resend, ctx);
+
+		// Schedule a resubmit timer.  We don't do this
+		// if the retry is "disabled" with NNG_DURATION_INFINITE.
+		if (ctx->retry > 0) {
+			nni_timer_schedule(
+			    &ctx->timer, nni_clock() + ctx->retry);
+		}
+
+		// If we were called from send_cb (done), then we're already
+		// busy.  If we were called from other cases we need to
+		//
+		// If we weren't "busy" (sending) before, then mark
+		// us busy now.  This also may apply back-pressure.
+		if (!p->busy) {
+			p->busy = true;
+			nni_list_remove(&s->ready_pipes, p);
+			nni_list_append(&s->busy_pipes, p);
+			if (nni_list_empty(&s->ready_pipes)) {
+				nni_pollable_clear(&s->writable);
+			}
+		}
+		nni_msg_clone(ctx->req_msg);
+		nni_aio_set_msg(&p->aio_send, ctx->req_msg);
+		nni_pipe_send(p->pipe, &p->aio_send);
+	} else if (done) {
+		// No work to do.  So we're back to being idle.
+		NNI_ASSERT(p->busy);
+		p->busy = false;
+		nni_list_remove(&s->busy_pipes, p);
+		nni_list_append(&s->ready_pipes, p);
+		nni_pollable_raise(&s->writable);
+		req0_run_send_queue(s, NULL);
+	}
+}
+
 static void
 req0_pipe_close(void *arg)
 {
@@ -227,7 +281,10 @@ req0_pipe_close(void *arg)
 	}
 
 	while ((ctx = nni_list_first(&p->contexts)) != NULL) {
+		// If we were scheduled for sending (or resend), unschedule
+		nni_list_node_remove(&ctx->send_node);
 		nni_list_remove(&p->contexts, ctx);
+		ctx->req_pipe = NULL;
 		// Reset the timer on this so it expires immediately.
 		// This is actually easier than canceling the timer and
 		// running the send_queue separately.  (In particular, it
@@ -247,6 +304,7 @@ req0_send_cb(void *arg)
 {
 	req0_pipe *p = arg;
 	req0_sock *s = p->req;
+	req0_ctx * ctx;
 	nni_aio *  aio;
 	nni_list   sent_list;
 
@@ -269,12 +327,17 @@ req0_send_cb(void *arg)
 		nni_mtx_unlock(&s->mtx);
 		return;
 	}
+	NNI_ASSERT(p->busy);
+	req0_pipe_resend(p, true);
+#if 0
+	p->busy = false;
 	nni_list_remove(&s->busy_pipes, p);
 	nni_list_append(&s->ready_pipes, p);
 	if (nni_list_empty(&s->send_queue)) {
 		nni_pollable_raise(&s->writable);
 	}
 	req0_run_send_queue(s, &sent_list);
+#endif
 	nni_mtx_unlock(&s->mtx);
 
 	while ((aio = nni_list_first(&sent_list)) != NULL) {
@@ -359,13 +422,24 @@ req0_ctx_timeout(void *arg)
 {
 	req0_ctx * ctx = arg;
 	req0_sock *s   = ctx->sock;
+	req0_pipe *p;
 
 	nni_mtx_lock(&s->mtx);
 	if ((ctx->req_msg != NULL) && (!s->closed)) {
 		if (!nni_list_node_active(&ctx->send_node)) {
-			nni_list_append(&s->send_queue, ctx);
+
+			// If we have already identified a pipe that we
+			// should be schedule on, resend to the same
+			// pipe (as long as it is still connected).
+			if ((p = ctx->req_pipe) != NULL) {
+				nni_list_append(&p->resend, ctx);
+				req0_pipe_resend(p, false);
+			} else {
+				// Otherwise wait for *any* pipe.
+				nni_list_append(&s->send_queue, ctx);
+				req0_run_send_queue(s, NULL);
+			}
 		}
-		req0_run_send_queue(s, NULL);
 	}
 	nni_mtx_unlock(&s->mtx);
 }
@@ -449,13 +523,16 @@ req0_run_send_queue(req0_sock *s, nni_list *sent_list)
 		// first send attempt.
 		nni_list_remove(&s->send_queue, ctx);
 
-		// Schedule a resubmit timer.  We only do this if we got
-		// a pipe to send to.  Otherwise, we should get handled
-		// the next time that the send_queue is run.  We don't do this
-		// if the retry is "disabled" with NNG_DURATION_INFINITE.
-		if (ctx->retry > 0) {
-			nni_timer_schedule(
-			    &ctx->timer, nni_clock() + ctx->retry);
+		if ((aio = ctx->send_aio) != NULL) {
+			ctx->send_aio = NULL;
+			nni_aio_bump_count(aio, ctx->req_len);
+			// If the list was passed in, we want to do a
+			// synchronous completion later.
+			if (sent_list != NULL) {
+				nni_list_append(sent_list, aio);
+			} else {
+				nni_aio_finish(aio, 0, 0);
+			}
 		}
 
 		// Put us on the pipe list of active contexts.
@@ -463,7 +540,13 @@ req0_run_send_queue(req0_sock *s, nni_list *sent_list)
 		// if the pipe is removed.
 		nni_list_node_remove(&ctx->pipe_node);
 		nni_list_append(&p->contexts, ctx);
+		ctx->req_pipe = p;
+
+		NNI_ASSERT(!p->busy);
+		nni_list_append(&p->resend, ctx);
+		req0_pipe_resend(p, false);
 
+#if 0
 		nni_list_remove(&s->ready_pipes, p);
 		nni_list_append(&s->busy_pipes, p);
 		if (nni_list_empty(&s->ready_pipes)) {
@@ -488,6 +571,7 @@ req0_run_send_queue(req0_sock *s, nni_list *sent_list)
 		nni_msg_clone(ctx->req_msg);
 		nni_aio_set_msg(&p->aio_send, ctx->req_msg);
 		nni_pipe_send(p->pipe, &p->aio_send);
+#endif
 	}
 }
 
@@ -509,6 +593,7 @@ req0_ctx_reset(req0_ctx *ctx)
 
 	nni_list_node_remove(&ctx->pipe_node);
 	nni_list_node_remove(&ctx->send_node);
+	ctx->req_pipe = NULL;
 	if (ctx->request_id != 0) {
 		nni_idhash_remove(s->requests, ctx->request_id);
 		ctx->request_id = 0;
diff --git a/src/protocol/reqrep0/req_test.c b/src/protocol/reqrep0/req_test.c
index 75cff14a..527d4ab5 100644
--- a/src/protocol/reqrep0/req_test.c
+++ b/src/protocol/reqrep0/req_test.c
@@ -183,6 +183,141 @@ test_req_rep_exchange(void)
 	TEST_CHECK(nng_close(rep) == 0);
 }
 
+void
+test_req_resend(void)
+{
+	nng_socket req;
+	nng_socket rep;
+
+	TEST_NNG_PASS(nng_req0_open(&req));
+	TEST_NNG_PASS(nng_rep0_open(&rep));
+
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_REQ_RESENDTIME, 10));
+
+	TEST_NNG_PASS(testutil_marry(rep, req));
+
+	TEST_NNG_SEND_STR(req, "ping");
+	TEST_NNG_RECV_STR(rep, "ping");
+	TEST_NNG_RECV_STR(rep, "ping");
+	TEST_NNG_RECV_STR(rep, "ping");
+
+	TEST_NNG_PASS(nng_close(req));
+	TEST_NNG_PASS(nng_close(rep));
+}
+
+void
+test_req_resend_sticky(void)
+{
+	nng_socket req;
+	nng_socket rep1;
+	nng_socket rep2;
+	nng_msg *  m;
+
+	// This test demonstrates that we always resend retries to the same
+	// peer, as long as it is connected.
+	TEST_NNG_PASS(nng_req0_open(&req));
+	TEST_NNG_PASS(nng_rep0_open(&rep1));
+	TEST_NNG_PASS(nng_rep0_open(&rep2));
+
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep1, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep2, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep1, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep2, NNG_OPT_SENDTIMEO, 200));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_REQ_RESENDTIME, 10));
+
+	TEST_NNG_PASS(testutil_marry(rep1, req));
+	TEST_NNG_SEND_STR(req, "ping");
+	TEST_NNG_RECV_STR(rep1, "ping");
+	TEST_NNG_PASS(testutil_marry(rep2, req));
+	TEST_NNG_RECV_STR(rep1, "ping");
+	TEST_NNG_RECV_STR(rep1, "ping");
+	TEST_NNG_RECV_STR(rep1, "ping");
+	TEST_NNG_FAIL(nng_recvmsg(rep2, &m, 0), NNG_ETIMEDOUT);
+
+	TEST_NNG_PASS(nng_close(req));
+	TEST_NNG_PASS(nng_close(rep1));
+	TEST_NNG_PASS(nng_close(rep2));
+}
+
+void
+test_req_resend_reconnect(void)
+{
+	nng_socket req;
+	nng_socket rep1;
+	nng_socket rep2;
+
+	TEST_NNG_PASS(nng_req0_open(&req));
+	TEST_NNG_PASS(nng_rep0_open(&rep1));
+	TEST_NNG_PASS(nng_rep0_open(&rep2));
+
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep1, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep2, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep1, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep2, NNG_OPT_SENDTIMEO, SECOND));
+	// We intentionally set the retry time long; that way we only see
+	// the retry from loss of our original peer.
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_REQ_RESENDTIME, 60 * SECOND));
+
+	TEST_NNG_PASS(testutil_marry(rep1, req));
+
+	TEST_NNG_SEND_STR(req, "ping");
+	TEST_NNG_RECV_STR(rep1, "ping");
+
+	TEST_NNG_PASS(nng_close(rep1));
+	TEST_NNG_PASS(testutil_marry(rep2, req));
+
+	TEST_NNG_RECV_STR(rep2, "ping");
+	TEST_NNG_SEND_STR(rep2, "rep2");
+	TEST_NNG_RECV_STR(req, "rep2");
+
+	TEST_NNG_PASS(nng_close(req));
+	TEST_NNG_PASS(nng_close(rep2));
+}
+
+void
+test_req_resend_disconnect(void)
+{
+	nng_socket req;
+	nng_socket rep1;
+	nng_socket rep2;
+
+	TEST_NNG_PASS(nng_req0_open(&req));
+	TEST_NNG_PASS(nng_rep0_open(&rep1));
+	TEST_NNG_PASS(nng_rep0_open(&rep2));
+
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep1, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep2, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep1, NNG_OPT_SENDTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep2, NNG_OPT_SENDTIMEO, SECOND));
+	// We intentionally set the retry time long; that way we only see
+	// the retry from loss of our original peer.
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_REQ_RESENDTIME, 60 * SECOND));
+
+	TEST_NNG_PASS(testutil_marry(rep1, req));
+	TEST_NNG_SEND_STR(req, "ping");
+	TEST_NNG_RECV_STR(rep1, "ping");
+
+	TEST_NNG_PASS(testutil_marry(rep2, req));
+	TEST_NNG_PASS(nng_close(rep1));
+
+	TEST_NNG_RECV_STR(rep2, "ping");
+	TEST_NNG_SEND_STR(rep2, "rep2");
+	TEST_NNG_RECV_STR(req, "rep2");
+
+	TEST_NNG_PASS(nng_close(req));
+	TEST_NNG_PASS(nng_close(rep2));
+}
+
 void
 test_req_cancel(void)
 {
@@ -193,22 +328,22 @@ test_req_cancel(void)
 	nng_socket   req;
 	nng_socket   rep;
 
-	TEST_CHECK(nng_rep_open(&rep) == 0);
-	TEST_CHECK(nng_req_open(&req) == 0);
+	TEST_NNG_PASS(nng_rep_open(&rep));
+	TEST_NNG_PASS(nng_req_open(&req));
 
-	TEST_CHECK(nng_setopt_ms(req, NNG_OPT_RECVTIMEO, SECOND) == 0);
-	TEST_CHECK(nng_setopt_ms(rep, NNG_OPT_RECVTIMEO, SECOND) == 0);
-	TEST_CHECK(nng_setopt_ms(req, NNG_OPT_SENDTIMEO, SECOND) == 0);
-	TEST_CHECK(nng_setopt_ms(rep, NNG_OPT_SENDTIMEO, SECOND) == 0);
-	TEST_CHECK(nng_setopt_ms(req, NNG_OPT_REQ_RESENDTIME, retry) == 0);
-	TEST_CHECK(nng_setopt_int(req, NNG_OPT_SENDBUF, 16) == 0);
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep, NNG_OPT_RECVTIMEO, SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_SENDTIMEO, 5 * SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(rep, NNG_OPT_SENDTIMEO, 5 * SECOND));
+	TEST_NNG_PASS(nng_setopt_ms(req, NNG_OPT_REQ_RESENDTIME, retry));
+	TEST_NNG_PASS(nng_setopt_int(req, NNG_OPT_SENDBUF, 16));
 
-	TEST_CHECK(nng_msg_alloc(&abc, 0) == 0);
-	TEST_CHECK(nng_msg_append(abc, "abc", 4) == 0);
-	TEST_CHECK(nng_msg_alloc(&def, 0) == 0);
-	TEST_CHECK(nng_msg_append(def, "def", 4) == 0);
+	TEST_NNG_PASS(nng_msg_alloc(&abc, 0));
+	TEST_NNG_PASS(nng_msg_append(abc, "abc", 4));
+	TEST_NNG_PASS(nng_msg_alloc(&def, 0));
+	TEST_NNG_PASS(nng_msg_append(def, "def", 4));
 
-	TEST_CHECK(testutil_marry(rep, req) == 0);
+	TEST_NNG_PASS(testutil_marry(rep, req));
 
 	// Send req #1 (abc).
 	TEST_CHECK(nng_sendmsg(req, abc, 0) == 0);
@@ -221,36 +356,36 @@ test_req_cancel(void)
 	// Send the next next request ("def").  Note that
 	// the REP side server will have already buffered the receive
 	// request, and should simply be waiting for us to reply to abc.
-	TEST_CHECK(nng_sendmsg(req, def, 0) == 0);
+	TEST_NNG_PASS(nng_sendmsg(req, def, 0));
 
 	// Receive the first request (should be abc) on the REP server.
-	TEST_CHECK(nng_recvmsg(rep, &cmd, 0) == 0);
+	TEST_NNG_PASS(nng_recvmsg(rep, &cmd, 0));
 	TEST_ASSERT(cmd != NULL);
 	TEST_CHECK(nng_msg_len(cmd) == 4);
 	TEST_CHECK(strcmp(nng_msg_body(cmd), "abc") == 0);
 
 	// REP sends the reply to first command.  This will be discarded
 	// by the REQ socket.
-	TEST_CHECK(nng_sendmsg(rep, cmd, 0) == 0);
+	TEST_NNG_PASS(nng_sendmsg(rep, cmd, 0));
 
 	// Now get the next command from the REP; should be "def".
-	TEST_CHECK(nng_recvmsg(rep, &cmd, 0) == 0);
+	TEST_NNG_PASS(nng_recvmsg(rep, &cmd, 0));
 	TEST_ASSERT(cmd != NULL);
 	TEST_CHECK(nng_msg_len(cmd) == 4);
 	TEST_CHECK(strcmp(nng_msg_body(cmd), "def") == 0);
 	TEST_MSG("Received body was %s", nng_msg_body(cmd));
 
 	// And send it back to REQ.
-	TEST_CHECK(nng_sendmsg(rep, cmd, 0) == 0);
+	TEST_NNG_PASS(nng_sendmsg(rep, cmd, 0));
 
 	// Try a req command.  This should give back "def"
-	TEST_CHECK(nng_recvmsg(req, &cmd, 0) == 0);
+	TEST_NNG_PASS(nng_recvmsg(req, &cmd, 0));
 	TEST_CHECK(nng_msg_len(cmd) == 4);
 	TEST_CHECK(strcmp(nng_msg_body(cmd), "def") == 0);
 	nng_msg_free(cmd);
 
-	TEST_CHECK(nng_close(req) == 0);
-	TEST_CHECK(nng_close(rep) == 0);
+	TEST_NNG_PASS(nng_close(req));
+	TEST_NNG_PASS(nng_close(rep));
 }
 
 void
@@ -453,7 +588,7 @@ test_req_poll_contention(void)
 	// It can take a little bit of time for the eased back-pressure
 	// to reflect across the network.
 	testutil_sleep(100);
-	
+
 	// Should be come writeable now...
 	TEST_CHECK(testutil_pollfd(fd) == true);
 
@@ -825,6 +960,10 @@ TEST_LIST = {
 	{ "req recv bad state", test_req_recv_bad_state },
 	{ "req recv garbage", test_req_recv_garbage },
 	{ "req rep exchange", test_req_rep_exchange },
+	{ "req resend", test_req_resend },
+	{ "req resend sticky", test_req_resend_sticky },
+	{ "req resend disconnect", test_req_resend_disconnect },
+	{ "req resend reconnect", test_req_resend_reconnect },
 	{ "req cancel", test_req_cancel },
 	{ "req cancel abort recv", test_req_cancel_abort_recv },
 	{ "req cancel post recv", test_req_cancel_post_recv },
