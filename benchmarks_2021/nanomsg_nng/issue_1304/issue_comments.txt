Non-blocking send on rep sockets always fails
Looking at this now.  I *think* the problem may be the rearchitecture of REP which avoids some extra queueing layers.  Stay tuned.
8 is NNG_EAGAIN btw.
I see the code where this is happening.  The refactor use nng_aio_schedule() results in always returning NNG_EAGAIN.  We need to check to see if we can complete the request immediately before calling that.
So we shouldn't call nni_aio_schedule() until we know we are going to sleep.

Fixing this breaks a test case case, that incorrectly tested for a zero timeout.