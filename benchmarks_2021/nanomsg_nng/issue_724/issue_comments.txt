`NNG_RECVMAXSZ` option can be disregarded if `nng_listen()` happens before `nng_setopt(...)`.
I think maybe you hit a race condition, where the pipe was created by listen, but when you ran the setsockopt() call the pipe was in the process of attaching/connecting, so wasn't included in the socket option setting.

There is nothing platform specific here, but changes in timing can indeed make a difference.

Usually most options that affect dialers or listeners (and this one of them) should be set before starting the dialer or listener (or calling nng_listen() or nng_dial().)  That's not say this edge case behavior isn't a bug, because it is. 

What I'm not sure is whether the effort to close the race is worthwhile.  It might be better to make the above statement a bit more explicit, both in docs, and by not attempting to make it do anything more than that.

I'll need to think about this some more.  In the meantime, its fairly trivially avoidable -- just do your call first.  :-) 
👍  on avoiding it by setting options before listening.  I'll be doing that for now.

I don't _think_ it's a race, based on a couple tests I ran just now:

1. adding a sleep() call between nng_listen() and nng_setopt_size() results in the same behavior, even sleeping up to 10 seconds;
2. Doing a synchronous dial right after the listen, then sleeping, then setting the option does not make the situation better.

Just to be clear, here is the code that I would expect to be race-free, but which still has the same issue.

```c
#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include "nng.h"
#include "protocol/pair1/pair.h"

#ifdef WIN32
#include <Windows.h>
#define sleep(x) Sleep((x) * 1000)
#endif

int main(int argc, char *argv[]) {
	nng_socket a;
	nng_socket b;
	nng_listener l;

	// don't actually care about the contents of the message
	char sendbuf[100];
	char recvbuf[100];
	char addr[] = "tcp://127.0.0.1:31313";
	size_t recvbufsize = 100;

	assert(nng_pair1_open(&a) == 0);
	assert(nng_pair1_open(&b) == 0);
	assert(nng_listen(a, addr, &l, 0) == 0);
	assert(nng_dial(b, addr, NULL, 0) == 0);
	sleep(2);
	assert(nng_setopt_size(a, NNG_OPT_RECVMAXSZ, 100) == 0);
	sleep(2);

	assert(nng_setopt_ms(a, NNG_OPT_RECVTIMEO, 100) == 0);

	assert(nng_send(b, sendbuf, 100, 0) == 0);
	if (nng_recv(a, recvbuf, &recvbufsize, 0) == NNG_ETIMEDOUT) {
		printf("It worked!\n");
	}
	else {
		printf("Sad times, didn't work!\n");
	}
	assert(nng_close(a) == 0);
	assert(nng_close(b) == 0);
	return 0;
}
```

And you're definitely right about this not being platform specific: **I was totally wrong about this only happening on Windows.  Linux shows the exact behavior that I thought was Windows-only**.  (As an aside, that probably means I have my Travis CI builds misconfigured in my bindings project – I think my current tests should be failing on all platforms instead of only Windows.  But that's no concern of this project, of course!)
[The commit that I tracked this behavior to](https://github.com/nanomsg/nng/commit/5b61fa0e04348ee7661dbb19bc9c50ca22af74dc) seems to be a refactoring so that listeners and dialers can be abstracted as "endpoints", and they're able to use the same function for `set_recvmaxsz()`.  Since the old functions (`tcptran_listener_set_recvmaxsz`, `tcptra_dialer_set_recvmaxsz`) and the new function (`tcptran_ep_set_recvmaxsz`) seem to be logically identical, I'm wondering what the issue is somewhere else.  

Maybe the field is getting set correctly in the `tcptran_ep` struct, but not getting copied to the associated `tcptran_pipe`?  Honestly I'm not even sure if that sentence even made sense, since I'm not very familiar with the implementation at this point :-).
I will take a hard look at this tomorrow.

On Sun, Sep 16, 2018, 6:12 PM Cody Piersall <notifications@github.com>
wrote:

> The commit that I tracked this behavior to
> <https://github.com/nanomsg/nng/commit/5b61fa0e04348ee7661dbb19bc9c50ca22af74dc>
> seems to be a refactoring so that listeners and dialers can be abstracted
> as "endpoints", and they're able to use the same function for
> set_recvmaxsz(). Since the old functions (tcptran_listener_set_recvmaxsz,
> tcptra_dialer_set_recvmaxsz) and the new function (
> tcptran_ep_set_recvmaxsz) seem to be logically identical, I'm wondering
> what the issue is somewhere else.
>
> Maybe the field is getting set correctly in the tcptran_ep struct, but
> not getting copied to the associated tcptran_pipe? Honestly I'm not even
> sure if that sentence even made sense, since I'm not very familiar with the
> implementation at this point :-).
>
> —
> You are receiving this because you commented.
>
>
> Reply to this email directly, view it on GitHub
> <https://github.com/nanomsg/nng/issues/724#issuecomment-421865652>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/ABPDfTpCHHNLAhADY1euQqfiSVVhMuyzks5ubvcKgaJpZM4WonSt>
> .
>

After looking at this a bit more, it does seem to me that the pipe options are getting set when the pipe is _allocated_ (e.g. `tcptran_pipe_alloc()` for the tcp transport), but the option is not getting updated on any existing pipe in `tcptran_ep_set_recvmaxsz()`.

Since the endpoints for all the transports keep a list of pipes, it seems like the thing to do would be to walk through the list of pipes in `tcptran_ep_set_recvmaxsz()` and set `rcvmax` on all the associated pipes.  For consistency, this would need to be done for all the transports and options other than just `rcvmax`; based on the tcptran_pipe fields that get set in `tcptran_pipe_alloc()`, at least `keepalive`, `nodelay`, and `rcvmax` need to be set for the tcp transport.  I haven't looked at all the other transports, but IPC seems to only need `rcvmax` updated.
(Sorry for the stream-of-consciousness posts here.)

The following patch does update the pipe's rcvmax value.  I'm not totally sure this is the best approach though, and a proper PR would need to update all pipe options that get set via listeners/dialers, and for all transports.  But this does show that the idea is correct.

```
diff --git a/src/transport/tcp/tcp.c b/src/transport/tcp/tcp.c
index fed8872..394d07f 100644
--- a/src/transport/tcp/tcp.c
+++ b/src/transport/tcp/tcp.c
@@ -995,12 +995,19 @@ tcptran_ep_set_recvmaxsz(void *arg, const void *v, size_t sz, nni_opt_type t)
 {
 	tcptran_ep *ep = arg;
 	size_t      val;
+	tcptran_pipe *p;
 	int         rv;
 	if ((rv = nni_copyin_size(&val, v, sz, 0, NNI_MAXSZ, t)) == 0) {
 		nni_mtx_lock(&ep->mtx);
 		ep->rcvmax = val;
+		NNI_LIST_FOREACH (&ep->pipes, p) {
+			nni_mtx_lock(&p->mtx);
+			p->rcvmax = val;
+			nni_mtx_unlock(&p->mtx);
+		}
 		nni_mtx_unlock(&ep->mtx);
 	}
+
 	return (rv);
 }
 ```

I should be able to get a PR together in the next couple weeks; however if you get to it first, that would be just fine to me :-).  Off to bed for me now.
Some pipe options are documented to only take effect before the endpoint is
started.  This is necessary for certain transport parameters.

Yes we can walk the pipe list for some of these.  I haven't decided yet if
this actually desirable.  We would have to do it in a race free manner if
we do.

On Fri, Sep 21, 2018, 8:52 PM Cody Piersall <notifications@github.com>
wrote:

> (Sorry for the stream-of-consciousness posts here.)
>
> The following patch does update the pipe's rcvmax value. I'm not totally
> sure this is the best approach though, and a proper PR would need to update
> all pipe options that get set via listeners/dialers, and for all
> transports. But this does show that the idea is correct.
>
> diff --git a/src/transport/tcp/tcp.c b/src/transport/tcp/tcp.c
> index fed8872..394d07f 100644
> --- a/src/transport/tcp/tcp.c
> +++ b/src/transport/tcp/tcp.c
> @@ -995,12 +995,19 @@ tcptran_ep_set_recvmaxsz(void *arg, const void *v, size_t sz, nni_opt_type t)
>  {
>  	tcptran_ep *ep = arg;
>  	size_t      val;
> +	tcptran_pipe *p;
>  	int         rv;
>  	if ((rv = nni_copyin_size(&val, v, sz, 0, NNI_MAXSZ, t)) == 0) {
>  		nni_mtx_lock(&ep->mtx);
>  		ep->rcvmax = val;
> +		NNI_LIST_FOREACH (&ep->pipes, p) {
> +			nni_mtx_lock(&p->mtx);
> +			p->rcvmax = val;
> +			nni_mtx_unlock(&p->mtx);
> +		}
>  		nni_mtx_unlock(&ep->mtx);
>  	}
> +
>  	return (rv);
>  }
>
> I should be able to get a PR together in the next couple weeks; however if
> you get to it first, that would be just fine to me :-). Off to bed for me
> now.
>
> —
> You are receiving this because you commented.
>
>
> Reply to this email directly, view it on GitHub
> <https://github.com/nanomsg/nng/issues/724#issuecomment-423714238>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/ABPDfdazG6AebOfu_03CW_jlAxTUwccyks5udbPzgaJpZM4WonSt>
> .
>
