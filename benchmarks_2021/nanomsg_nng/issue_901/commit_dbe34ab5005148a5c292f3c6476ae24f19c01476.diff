diff --git a/src/core/options.h b/src/core/options.h
index 7b66dbfb..cbf95d35 100644
--- a/src/core/options.h
+++ b/src/core/options.h
@@ -90,4 +90,101 @@ extern int nni_setopt(
 extern int nni_chkopt(
     const nni_chkoption *, const char *, const void *, size_t, nni_type);
 
+//
+// This next block sets up to define the various typed option functions.
+// To make it easier to cover them all at once, we use macros.
+//
+
+#define NNI_DEFGET(base, pointer)                                        \
+	int nng_##base##_get(                                                \
+	    nng_##base pointer s, const char *nm, void *vp, size_t *szp)     \
+	{                                                                    \
+		return (nni_##base##_getx(s, nm, vp, szp, NNI_TYPE_OPAQUE)); \
+	}
+
+#define NNI_DEFTYPEDGET(base, suffix, pointer, type, nnitype)    \
+	int nng_##base##_get_##suffix(                               \
+	    nng_##base pointer s, const char *nm, type *vp)          \
+	{                                                            \
+		size_t sz = sizeof(*vp);                             \
+		return (nni_##base##_getx(s, nm, vp, &sz, nnitype)); \
+	}
+
+#define NNI_DEFGETALL(base)                                      \
+	NNI_DEFGET(base, )                                           \
+	NNI_DEFTYPEDGET(base, int, , int, NNI_TYPE_INT32)            \
+	NNI_DEFTYPEDGET(base, bool, , bool, NNI_TYPE_BOOL)           \
+	NNI_DEFTYPEDGET(base, size, , size_t, NNI_TYPE_SIZE)         \
+	NNI_DEFTYPEDGET(base, uint64, , uint64_t, NNI_TYPE_UINT64)   \
+	NNI_DEFTYPEDGET(base, string, , char *, NNI_TYPE_STRING)     \
+	NNI_DEFTYPEDGET(base, ptr, , void *, NNI_TYPE_POINTER)       \
+	NNI_DEFTYPEDGET(base, ms, , nng_duration, NNI_TYPE_DURATION) \
+	NNI_DEFTYPEDGET(base, addr, , nng_sockaddr, NNI_TYPE_SOCKADDR)
+
+#define NNI_DEFGETALL_PTR(base)                                        \
+	NNI_DEFGET(base, *)                                           \
+	NNI_DEFTYPEDGET(base, int, *, int, NNI_TYPE_INT32)            \
+	NNI_DEFTYPEDGET(base, bool, *, bool, NNI_TYPE_BOOL)           \
+	NNI_DEFTYPEDGET(base, size, *, size_t, NNI_TYPE_SIZE)         \
+	NNI_DEFTYPEDGET(base, uint64, *, uint64_t, NNI_TYPE_UINT64)   \
+	NNI_DEFTYPEDGET(base, string, *, char *, NNI_TYPE_STRING)     \
+	NNI_DEFTYPEDGET(base, ptr, *, void *, NNI_TYPE_POINTER)       \
+	NNI_DEFTYPEDGET(base, ms, *, nng_duration, NNI_TYPE_DURATION) \
+	NNI_DEFTYPEDGET(base, addr, *, nng_sockaddr, NNI_TYPE_SOCKADDR)
+
+#define NNI_DEFSET(base, pointer)                                              \
+	int nng_##base##_set(                                                      \
+	    nng_##base pointer s, const char *nm, const void *vp, size_t sz)       \
+	{                                                                   \
+		return (nni_##base##_setx(s, nm, vp, sz, NNI_TYPE_OPAQUE)); \
+	}
+
+#define NNI_DEFTYPEDSETEX(base, suffix, pointer, type, len, nnitype)            \
+	int nng_##base##_set_##suffix(nng_##base pointer s, const char *nm, type v) \
+	{                                                                    \
+		return (nni_##base##_setx(s, nm, &v, len, nnitype));         \
+	}
+
+#define NNI_DEFTYPEDSET(base, suffix, pointer, type, nnitype)                   \
+	int nng_##base##_set_##suffix(nng_##base pointer s, const char *nm, type v) \
+	{                                                                    \
+		return (nni_##base##_setx(s, nm, &v, sizeof(v), nnitype));   \
+	}
+
+#define NNI_DEFSTRINGSET(base, pointer)                           \
+	int nng_##base##_set_string(                                  \
+	    nng_##base pointer s, const char *nm, const char *v)      \
+	{                                                             \
+		return (nni_##base##_setx(s, nm, v,                   \
+		    v != NULL ? strlen(v) + 1 : 0, NNI_TYPE_STRING)); \
+	}
+
+#define NNI_DEFSOCKADDRSET(base, pointer)                            \
+	int nng_##base##_set_adddr(                                      \
+	    nng_##base pointer s, const char *nm, const nng_sockaddr *v) \
+	{                                                                \
+		return (nni_##base##_setx(                        \
+		    s, nm, v, sizeof(*v), NNI_TYPE_SOCKADDR));    \
+	}
+
+#define NNI_DEFSETALL(base)                                      \
+	NNI_DEFSET(base, )                                           \
+	NNI_DEFTYPEDSET(base, int, , int, NNI_TYPE_INT32)            \
+	NNI_DEFTYPEDSET(base, bool, , bool, NNI_TYPE_BOOL)           \
+	NNI_DEFTYPEDSET(base, size, , size_t, NNI_TYPE_SIZE)         \
+	NNI_DEFTYPEDSET(base, ms, , nng_duration, NNI_TYPE_DURATION) \
+	NNI_DEFTYPEDSET(base, ptr, , void *, NNI_TYPE_POINTER)       \
+	NNI_DEFSTRINGSET(base, )                                     \
+	NNI_DEFSOCKADDRSET(base, )
+
+#define NNI_DEFSETALL_PTR(base)                                   \
+	NNI_DEFSET(base, *)                                           \
+	NNI_DEFTYPEDSET(base, int, *, int, NNI_TYPE_INT32)            \
+	NNI_DEFTYPEDSET(base, bool, *, bool, NNI_TYPE_BOOL)           \
+	NNI_DEFTYPEDSET(base, size, *, size_t, NNI_TYPE_SIZE)         \
+	NNI_DEFTYPEDSET(base, ms, *, nng_duration, NNI_TYPE_DURATION) \
+	NNI_DEFTYPEDSET(base, ptr, *, void *, NNI_TYPE_POINTER)       \
+	NNI_DEFSTRINGSET(base, *)                                     \
+	NNI_DEFSOCKADDRSET(base, *)
+
 #endif // CORE_OPTIONS_H
diff --git a/src/core/stream.c b/src/core/stream.c
index c0dc38d6..e7ebc4e0 100644
--- a/src/core/stream.c
+++ b/src/core/stream.c
@@ -282,86 +282,10 @@ nni_stream_checkopt(const char *scheme, const char *name, const void *data,
 	return (NNG_ENOTSUP);
 }
 
-//
-// This next block sets up to define the various typed option functions.
-// To make it easier to cover them all at once, we use macros.
-//
-
-#define DEFGET(base)                                                         \
-	int nng_##base##_get(                                                \
-	    nng_##base *s, const char *nm, void *vp, size_t *szp)            \
-	{                                                                    \
-		return (nni_##base##_getx(s, nm, vp, szp, NNI_TYPE_OPAQUE)); \
-	}
-
-#define DEFTYPEDGET(base, suffix, type, nnitype)                     \
-	int nng_##base##_get_##suffix(                               \
-	    nng_##base *s, const char *nm, type *vp)                 \
-	{                                                            \
-		size_t sz = sizeof(*vp);                             \
-		return (nni_##base##_getx(s, nm, vp, &sz, nnitype)); \
-	}
-
-#define DEFGETALL(base)                                        \
-	DEFGET(base)                                           \
-	DEFTYPEDGET(base, int, int, NNI_TYPE_INT32)            \
-	DEFTYPEDGET(base, bool, bool, NNI_TYPE_BOOL)           \
-	DEFTYPEDGET(base, size, size_t, NNI_TYPE_SIZE)         \
-	DEFTYPEDGET(base, uint64, uint64_t, NNI_TYPE_UINT64)   \
-	DEFTYPEDGET(base, string, char *, NNI_TYPE_STRING)     \
-	DEFTYPEDGET(base, ptr, void *, NNI_TYPE_POINTER)       \
-	DEFTYPEDGET(base, ms, nng_duration, NNI_TYPE_DURATION) \
-	DEFTYPEDGET(base, addr, nng_sockaddr, NNI_TYPE_SOCKADDR)
-
-DEFGETALL(stream)
-DEFGETALL(stream_dialer)
-DEFGETALL(stream_listener)
-
-#define DEFSET(base)                                                        \
-	int nng_##base##_set(                                               \
-	    nng_##base *s, const char *nm, const void *vp, size_t sz)       \
-	{                                                                   \
-		return (nni_##base##_setx(s, nm, vp, sz, NNI_TYPE_OPAQUE)); \
-	}
-
-#define DEFTYPEDSETEX(base, suffix, type, len, nnitype)                      \
-	int nng_##base##_set_##suffix(nng_##base *s, const char *nm, type v) \
-	{                                                                    \
-		return (nni_##base##_setx(s, nm, &v, len, nnitype));         \
-	}
-
-#define DEFTYPEDSET(base, suffix, type, nnitype)                             \
-	int nng_##base##_set_##suffix(nng_##base *s, const char *nm, type v) \
-	{                                                                    \
-		return (nni_##base##_setx(s, nm, &v, sizeof(v), nnitype));   \
-	}
-
-#define DEFSTRINGSET(base)                                            \
-	int nng_##base##_set_string(                                  \
-	    nng_##base *s, const char *nm, const char *v)             \
-	{                                                             \
-		return (nni_##base##_setx(s, nm, v,                   \
-		    v != NULL ? strlen(v) + 1 : 0, NNI_TYPE_STRING)); \
-	}
-
-#define DEFSOCKADDRSET(base)                                      \
-	int nng_##base##_set_adddr(                               \
-	    nng_##base *s, const char *nm, const nng_sockaddr *v) \
-	{                                                         \
-		return (nni_##base##_setx(                        \
-		    s, nm, v, sizeof(*v), NNI_TYPE_SOCKADDR));    \
-	}
+NNI_DEFGETALL_PTR(stream)
+NNI_DEFGETALL_PTR(stream_dialer)
+NNI_DEFGETALL_PTR(stream_listener)
 
-#define DEFSETALL(base)                                        \
-	DEFSET(base)                                           \
-	DEFTYPEDSET(base, int, int, NNI_TYPE_INT32)            \
-	DEFTYPEDSET(base, bool, bool, NNI_TYPE_BOOL)           \
-	DEFTYPEDSET(base, size, size_t, NNI_TYPE_SIZE)         \
-	DEFTYPEDSET(base, ms, nng_duration, NNI_TYPE_DURATION) \
-	DEFTYPEDSET(base, ptr, void *, NNI_TYPE_POINTER)       \
-	DEFSTRINGSET(base)                                     \
-	DEFSOCKADDRSET(base)
-
-DEFSETALL(stream)
-DEFSETALL(stream_dialer)
-DEFSETALL(stream_listener)
\ No newline at end of file
+NNI_DEFSETALL_PTR(stream)
+NNI_DEFSETALL_PTR(stream_dialer)
+NNI_DEFSETALL_PTR(stream_listener)
\ No newline at end of file
diff --git a/src/nng.c b/src/nng.c
index abbf9c62..c8485337 100644
--- a/src/nng.c
+++ b/src/nng.c
@@ -318,7 +318,7 @@ nng_ctx_send(nng_ctx cid, nng_aio *aio)
 }
 
 static int
-nng_ctx_getx(nng_ctx id, const char *n, void *v, size_t *szp, nni_type t)
+nni_ctx_getx(nng_ctx id, const char *n, void *v, size_t *szp, nni_type t)
 {
 	nni_ctx *ctx;
 	int      rv;
@@ -337,39 +337,39 @@ nng_ctx_getx(nng_ctx id, const char *n, void *v, size_t *szp, nni_type t)
 int
 nng_ctx_getopt(nng_ctx id, const char *name, void *val, size_t *szp)
 {
-	return (nng_ctx_getx(id, name, val, szp, NNI_TYPE_OPAQUE));
+	return (nni_ctx_getx(id, name, val, szp, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_ctx_getopt_bool(nng_ctx id, const char *name, bool *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_ctx_getx(id, name, vp, &sz, NNI_TYPE_BOOL));
+	return (nni_ctx_getx(id, name, vp, &sz, NNI_TYPE_BOOL));
 }
 
 int
 nng_ctx_getopt_int(nng_ctx id, const char *name, int *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_ctx_getx(id, name, vp, &sz, NNI_TYPE_INT32));
+	return (nni_ctx_getx(id, name, vp, &sz, NNI_TYPE_INT32));
 }
 
 int
 nng_ctx_getopt_size(nng_ctx id, const char *name, size_t *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_ctx_getx(id, name, vp, &sz, NNI_TYPE_SIZE));
+	return (nni_ctx_getx(id, name, vp, &sz, NNI_TYPE_SIZE));
 }
 
 int
 nng_ctx_getopt_ms(nng_ctx id, const char *name, nng_duration *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_ctx_getx(id, name, vp, &sz, NNI_TYPE_DURATION));
+	return (nni_ctx_getx(id, name, vp, &sz, NNI_TYPE_DURATION));
 }
 
 static int
-nng_ctx_setx(nng_ctx id, const char *n, const void *v, size_t sz, nni_type t)
+nni_ctx_setx(nng_ctx id, const char *n, const void *v, size_t sz, nni_type t)
 {
 	nni_ctx *ctx;
 	int      rv;
@@ -388,31 +388,31 @@ nng_ctx_setx(nng_ctx id, const char *n, const void *v, size_t sz, nni_type t)
 int
 nng_ctx_setopt(nng_ctx id, const char *name, const void *val, size_t sz)
 {
-	return (nng_ctx_setx(id, name, val, sz, NNI_TYPE_OPAQUE));
+	return (nni_ctx_setx(id, name, val, sz, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_ctx_setopt_bool(nng_ctx id, const char *name, bool v)
 {
-	return (nng_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_BOOL));
+	return (nni_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_BOOL));
 }
 
 int
 nng_ctx_setopt_int(nng_ctx id, const char *name, int v)
 {
-	return (nng_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_INT32));
+	return (nni_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_INT32));
 }
 
 int
 nng_ctx_setopt_size(nng_ctx id, const char *name, size_t v)
 {
-	return (nng_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_SIZE));
+	return (nni_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_SIZE));
 }
 
 int
 nng_ctx_setopt_ms(nng_ctx id, const char *name, nng_duration v)
 {
-	return (nng_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_DURATION));
+	return (nni_ctx_setx(id, name, &v, sizeof(v), NNI_TYPE_DURATION));
 }
 
 int
@@ -559,7 +559,7 @@ nng_dialer_id(nng_dialer d)
 }
 
 static int
-nng_dialer_setx(
+nni_dialer_setx(
     nng_dialer did, const char *n, const void *v, size_t sz, nni_type t)
 {
 	nni_dialer *d;
@@ -577,7 +577,7 @@ nng_dialer_setx(
 }
 
 static int
-nng_dialer_getx(
+nni_dialer_getx(
     nng_dialer did, const char *n, void *v, size_t *szp, nni_type t)
 {
 	nni_dialer *d;
@@ -597,115 +597,115 @@ nng_dialer_getx(
 int
 nng_dialer_setopt(nng_dialer d, const char *name, const void *v, size_t sz)
 {
-	return (nng_dialer_setx(d, name, v, sz, NNI_TYPE_OPAQUE));
+	return (nni_dialer_setx(d, name, v, sz, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_dialer_setopt_bool(nng_dialer d, const char *name, bool v)
 {
-	return (nng_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_BOOL));
+	return (nni_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_BOOL));
 }
 
 int
 nng_dialer_setopt_int(nng_dialer d, const char *name, int v)
 {
-	return (nng_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_INT32));
+	return (nni_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_INT32));
 }
 
 int
 nng_dialer_setopt_size(nng_dialer d, const char *name, size_t v)
 {
-	return (nng_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_SIZE));
+	return (nni_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_SIZE));
 }
 
 int
 nng_dialer_setopt_ms(nng_dialer d, const char *name, nng_duration v)
 {
-	return (nng_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_DURATION));
+	return (nni_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_DURATION));
 }
 
 int
 nng_dialer_setopt_uint64(nng_dialer d, const char *name, uint64_t v)
 {
-	return (nng_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_UINT64));
+	return (nni_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_UINT64));
 }
 
 int
 nng_dialer_setopt_ptr(nng_dialer d, const char *name, void *v)
 {
-	return (nng_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_POINTER));
+	return (nni_dialer_setx(d, name, &v, sizeof(v), NNI_TYPE_POINTER));
 }
 
 int
 nng_dialer_setopt_string(nng_dialer d, const char *name, const char *v)
 {
-	return (nng_dialer_setx(d, name, v, strlen(v) + 1, NNI_TYPE_STRING));
+	return (nni_dialer_setx(d, name, v, strlen(v) + 1, NNI_TYPE_STRING));
 }
 
 int
 nng_dialer_getopt(nng_dialer d, const char *name, void *val, size_t *szp)
 {
-	return (nng_dialer_getx(d, name, val, szp, NNI_TYPE_OPAQUE));
+	return (nni_dialer_getx(d, name, val, szp, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_dialer_getopt_bool(nng_dialer d, const char *name, bool *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_BOOL));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_BOOL));
 }
 
 int
 nng_dialer_getopt_int(nng_dialer d, const char *name, int *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_INT32));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_INT32));
 }
 
 int
 nng_dialer_getopt_size(nng_dialer d, const char *name, size_t *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_SIZE));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_SIZE));
 }
 
 int
 nng_dialer_getopt_sockaddr(nng_dialer d, const char *name, nng_sockaddr *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_SOCKADDR));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_SOCKADDR));
 }
 
 int
 nng_dialer_getopt_uint64(nng_dialer d, const char *name, uint64_t *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_UINT64));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_UINT64));
 }
 
 int
 nng_dialer_getopt_ptr(nng_dialer d, const char *name, void **vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_POINTER));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_POINTER));
 }
 
 int
 nng_dialer_getopt_string(nng_dialer d, const char *name, char **vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_STRING));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_STRING));
 }
 
 int
 nng_dialer_getopt_ms(nng_dialer d, const char *name, nng_duration *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_dialer_getx(d, name, vp, &sz, NNI_TYPE_DURATION));
+	return (nni_dialer_getx(d, name, vp, &sz, NNI_TYPE_DURATION));
 }
 
 int
-nng_listener_setx(
+nni_listener_setx(
     nng_listener lid, const char *name, const void *v, size_t sz, nni_type t)
 {
 	nni_listener *l;
@@ -725,53 +725,53 @@ nng_listener_setx(
 int
 nng_listener_setopt(nng_listener l, const char *name, const void *v, size_t sz)
 {
-	return (nng_listener_setx(l, name, v, sz, NNI_TYPE_OPAQUE));
+	return (nni_listener_setx(l, name, v, sz, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_listener_setopt_bool(nng_listener l, const char *name, bool v)
 {
-	return (nng_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_BOOL));
+	return (nni_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_BOOL));
 }
 
 int
 nng_listener_setopt_int(nng_listener l, const char *name, int v)
 {
-	return (nng_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_INT32));
+	return (nni_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_INT32));
 }
 
 int
 nng_listener_setopt_size(nng_listener l, const char *name, size_t v)
 {
-	return (nng_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_SIZE));
+	return (nni_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_SIZE));
 }
 
 int
 nng_listener_setopt_ms(nng_listener l, const char *name, nng_duration v)
 {
-	return (nng_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_DURATION));
+	return (nni_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_DURATION));
 }
 
 int
 nng_listener_setopt_uint64(nng_listener l, const char *name, uint64_t v)
 {
-	return (nng_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_UINT64));
+	return (nni_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_UINT64));
 }
 
 int
 nng_listener_setopt_ptr(nng_listener l, const char *name, void *v)
 {
-	return (nng_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_POINTER));
+	return (nni_listener_setx(l, name, &v, sizeof(v), NNI_TYPE_POINTER));
 }
 
 int
 nng_listener_setopt_string(nng_listener l, const char *n, const char *v)
 {
-	return (nng_listener_setx(l, n, v, strlen(v) + 1, NNI_TYPE_STRING));
+	return (nni_listener_setx(l, n, v, strlen(v) + 1, NNI_TYPE_STRING));
 }
 
 int
-nng_listener_getx(
+nni_listener_getx(
     nng_listener lid, const char *name, void *v, size_t *szp, nni_type t)
 {
 	nni_listener *l;
@@ -791,28 +791,28 @@ nng_listener_getx(
 int
 nng_listener_getopt(nng_listener l, const char *name, void *v, size_t *szp)
 {
-	return (nng_listener_getx(l, name, v, szp, NNI_TYPE_OPAQUE));
+	return (nni_listener_getx(l, name, v, szp, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_listener_getopt_bool(nng_listener l, const char *name, bool *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_BOOL));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_BOOL));
 }
 
 int
 nng_listener_getopt_int(nng_listener l, const char *name, int *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_INT32));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_INT32));
 }
 
 int
 nng_listener_getopt_size(nng_listener l, const char *name, size_t *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_SIZE));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_SIZE));
 }
 
 int
@@ -820,35 +820,35 @@ nng_listener_getopt_sockaddr(
     nng_listener l, const char *name, nng_sockaddr *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_SOCKADDR));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_SOCKADDR));
 }
 
 int
 nng_listener_getopt_uint64(nng_listener l, const char *name, uint64_t *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_UINT64));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_UINT64));
 }
 
 int
 nng_listener_getopt_ptr(nng_listener l, const char *name, void **vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_POINTER));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_POINTER));
 }
 
 int
 nng_listener_getopt_string(nng_listener l, const char *name, char **vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_STRING));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_STRING));
 }
 
 int
 nng_listener_getopt_ms(nng_listener l, const char *name, nng_duration *vp)
 {
 	size_t sz = sizeof(*vp);
-	return (nng_listener_getx(l, name, vp, &sz, NNI_TYPE_DURATION));
+	return (nni_listener_getx(l, name, vp, &sz, NNI_TYPE_DURATION));
 }
 
 int
@@ -878,7 +878,7 @@ nng_listener_close(nng_listener lid)
 }
 
 static int
-nng_setx(
+nni_socket_setx(
     nng_socket s, const char *name, const void *val, size_t sz, nni_type t)
 {
 	nni_sock *sock;
@@ -898,11 +898,11 @@ nng_setx(
 int
 nng_setopt(nng_socket s, const char *name, const void *val, size_t sz)
 {
-	return (nng_setx(s, name, val, sz, NNI_TYPE_OPAQUE));
+	return (nni_socket_setx(s, name, val, sz, NNI_TYPE_OPAQUE));
 }
 
 static int
-nng_getx(nng_socket s, const char *name, void *val, size_t *szp, nni_type t)
+nni_socket_getx(nng_socket s, const char *name, void *val, size_t *szp, nni_type t)
 {
 	nni_sock *sock;
 	int       rv;
@@ -921,99 +921,99 @@ nng_getx(nng_socket s, const char *name, void *val, size_t *szp, nni_type t)
 int
 nng_getopt(nng_socket s, const char *name, void *val, size_t *szp)
 {
-	return (nng_getx(s, name, val, szp, NNI_TYPE_OPAQUE));
+	return (nni_socket_getx(s, name, val, szp, NNI_TYPE_OPAQUE));
 }
 
 // Convenience option wrappers.
 int
 nng_setopt_int(nng_socket s, const char *name, int val)
 {
-	return (nng_setx(s, name, &val, sizeof(val), NNI_TYPE_INT32));
+	return (nni_socket_setx(s, name, &val, sizeof(val), NNI_TYPE_INT32));
 }
 
 int
 nng_setopt_bool(nng_socket s, const char *name, bool val)
 {
-	return (nng_setx(s, name, &val, sizeof(val), NNI_TYPE_BOOL));
+	return (nni_socket_setx(s, name, &val, sizeof(val), NNI_TYPE_BOOL));
 }
 
 int
 nng_setopt_size(nng_socket s, const char *name, size_t val)
 {
-	return (nng_setx(s, name, &val, sizeof(val), NNI_TYPE_SIZE));
+	return (nni_socket_setx(s, name, &val, sizeof(val), NNI_TYPE_SIZE));
 }
 
 int
 nng_setopt_ms(nng_socket s, const char *name, nng_duration val)
 {
-	return (nng_setx(s, name, &val, sizeof(val), NNI_TYPE_DURATION));
+	return (nni_socket_setx(s, name, &val, sizeof(val), NNI_TYPE_DURATION));
 }
 
 int
 nng_setopt_uint64(nng_socket s, const char *name, uint64_t val)
 {
-	return (nng_setx(s, name, &val, sizeof(val), NNI_TYPE_UINT64));
+	return (nni_socket_setx(s, name, &val, sizeof(val), NNI_TYPE_UINT64));
 }
 
 int
 nng_setopt_ptr(nng_socket s, const char *name, void *val)
 {
-	return (nng_setx(s, name, &val, sizeof(val), NNI_TYPE_POINTER));
+	return (nni_socket_setx(s, name, &val, sizeof(val), NNI_TYPE_POINTER));
 }
 
 int
 nng_setopt_string(nng_socket s, const char *name, const char *val)
 {
-	return (nng_setx(s, name, val, strlen(val) + 1, NNI_TYPE_STRING));
+	return (nni_socket_setx(s, name, val, strlen(val) + 1, NNI_TYPE_STRING));
 }
 
 int
 nng_getopt_bool(nng_socket s, const char *name, bool *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_BOOL));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_BOOL));
 }
 
 int
 nng_getopt_int(nng_socket s, const char *name, int *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_INT32));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_INT32));
 }
 
 int
 nng_getopt_size(nng_socket s, const char *name, size_t *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_SIZE));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_SIZE));
 }
 
 int
 nng_getopt_uint64(nng_socket s, const char *name, uint64_t *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_UINT64));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_UINT64));
 }
 
 int
 nng_getopt_ms(nng_socket s, const char *name, nng_duration *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_DURATION));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_DURATION));
 }
 
 int
 nng_getopt_ptr(nng_socket s, const char *name, void **valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_DURATION));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_DURATION));
 }
 
 int
 nng_getopt_string(nng_socket s, const char *name, char **valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_getx(s, name, valp, &sz, NNI_TYPE_STRING));
+	return (nni_socket_getx(s, name, valp, &sz, NNI_TYPE_STRING));
 }
 
 int
@@ -1132,7 +1132,7 @@ nng_strerror(int num)
 }
 
 static int
-nng_pipe_getx(nng_pipe p, const char *name, void *val, size_t *szp, nni_type t)
+nni_pipe_getx(nng_pipe p, const char *name, void *val, size_t *szp, nni_type t)
 {
 	int       rv;
 	nni_pipe *pipe;
@@ -1151,63 +1151,63 @@ nng_pipe_getx(nng_pipe p, const char *name, void *val, size_t *szp, nni_type t)
 int
 nng_pipe_getopt(nng_pipe p, const char *name, void *val, size_t *szp)
 {
-	return (nng_pipe_getx(p, name, val, szp, NNI_TYPE_OPAQUE));
+	return (nni_pipe_getx(p, name, val, szp, NNI_TYPE_OPAQUE));
 }
 
 int
 nng_pipe_getopt_bool(nng_pipe p, const char *name, bool *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_BOOL));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_BOOL));
 }
 
 int
 nng_pipe_getopt_int(nng_pipe p, const char *name, int *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_INT32));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_INT32));
 }
 
 int
 nng_pipe_getopt_size(nng_pipe p, const char *name, size_t *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_SIZE));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_SIZE));
 }
 
 int
 nng_pipe_getopt_uint64(nng_pipe p, const char *name, uint64_t *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_UINT64));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_UINT64));
 }
 
 int
 nng_pipe_getopt_ms(nng_pipe p, const char *name, nng_duration *valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_DURATION));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_DURATION));
 }
 
 int
 nng_pipe_getopt_ptr(nng_pipe p, const char *name, void **valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_POINTER));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_POINTER));
 }
 
 int
 nng_pipe_getopt_sockaddr(nng_pipe p, const char *name, nng_sockaddr *sap)
 {
 	size_t sz = sizeof(*sap);
-	return (nng_pipe_getx(p, name, sap, &sz, NNI_TYPE_SOCKADDR));
+	return (nni_pipe_getx(p, name, sap, &sz, NNI_TYPE_SOCKADDR));
 }
 
 int
 nng_pipe_getopt_string(nng_pipe p, const char *name, char **valp)
 {
 	size_t sz = sizeof(*valp);
-	return (nng_pipe_getx(p, name, valp, &sz, NNI_TYPE_STRING));
+	return (nni_pipe_getx(p, name, valp, &sz, NNI_TYPE_STRING));
 }
 
 nng_socket
@@ -1267,6 +1267,16 @@ nng_pipe_id(nng_pipe p)
 	return (((int) p.id > 0) ? (int) p.id : -1);
 }
 
+NNI_DEFSETALL(ctx)
+NNI_DEFGETALL(ctx)
+NNI_DEFSETALL(dialer)
+NNI_DEFGETALL(dialer)
+NNI_DEFSETALL(listener)
+NNI_DEFGETALL(listener)
+NNI_DEFSETALL(socket)
+NNI_DEFGETALL(socket)
+NNI_DEFGETALL(pipe)
+
 // Message handling.
 int
 nng_msg_alloc(nng_msg **msgp, size_t size)
