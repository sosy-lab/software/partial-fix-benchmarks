Stream get/set functions naming consistency
As  I commented in your PR, I'd rather resolve this the other way way around, where the newer shorter API versions become the preferred, and we add "legacy" bindings for the longer names.  If you want to pick this up, I'd be grateful.  Otherwise I'll get to it later.
I suspected that was a possibility. I'll update the PR accordingly. 