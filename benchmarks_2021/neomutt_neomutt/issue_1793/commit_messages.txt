Revert "mime_lookup: use compose mailcap entries when replying"

This reverts commit 672a84d9c4110343ab4d43d542b0c0933365d023.
fix(notmuch): ensure message_id is correct

The notmuch-backend is initializes a provided email struct with content
from notmuch. In rare instances, a provided email struct has a
message_id that does not match the one from notmuch. No validation to
ensure message_ids match is performed.

An issue occurs when running nm_mbox_check(), which stores all messages
in a hash table. The incorrect message_id is the key so our email is
considered missing, reloaded into the mailbox, and a duplicate appears
in the index. This triggers a warning from neomutt regarding external
modification.

Instead, validate the message_ids to ensure a match so our hash table
keys are correct. As a result, a reload is no longer performed and
neomutt's warning need not occur.

Fixes #1793
fix(notmuch): ensure message_id is correct

The notmuch-backend is initializes a provided email struct with content
from notmuch. In rare instances, a provided email struct has a
message_id that does not match the one from notmuch. No validation to
ensure message_ids match is performed.

An issue occurs when running nm_mbox_check(), which stores all messages
in a hash table. The incorrect message_id is the key so our email is
considered missing, reloaded into the mailbox, and a duplicate appears
in the index. This triggers a warning from neomutt regarding external
modification.

Instead, validate the message_ids to ensure a match so our hash table
keys are correct. As a result, a reload is no longer performed and
neomutt's warning need not occur.

Fixes #1793
fix(notmuch): ensure message_id is correct

The notmuch-backend initializes a provided email struct with content
from notmuch. In rare instances, a provided email struct has a
message_id that does not match the one from notmuch. No validation to
ensure message_ids match is performed.

An issue occurs when running nm_mbox_check(), which stores all messages
in a hash table. The incorrect message_id is the key so our email is
considered missing, reloaded into the mailbox, and a duplicate appears
in the index. This triggers a warning from neomutt regarding external
modification.

Instead, validate the message_ids to ensure a match so our hash table
keys are correct. As a result, a reload is no longer performed and
neomutt's warning need not occur.

Fixes #1793
notmuch: ensure message_id is correct

The notmuch-backend initializes a provided email struct with content
from notmuch. In rare instances, a provided email struct has a
message_id that does not match the one from notmuch. No validation to
ensure message_ids match is performed.

An issue occurs when running nm_mbox_check(), which stores all messages
in a hash table. The incorrect message_id is the key so our email is
considered missing, reloaded into the mailbox, and a duplicate appears
in the index. This triggers a warning from neomutt regarding external
modification.

Instead, validate the message_ids to ensure a match so our hash table
keys are correct. As a result, a reload is no longer performed and
neomutt's warning need not occur.

Fixes #1793
