Mention self_encrypt options in the documentation. (closes #3983)

Add a paragraph to the "Sending Cryptographically Signed/Encrypted
Messages" section of the manual.
Disable message security if the backend is not available.

Gitlab issue #3 exposed an awkward corner case: if mutt is configured
without PGP or S/MIME, and with GPGME, but $crypt_use_gpgme is unset.
In this case, no backend will be available, but WithCrypto will be set
with both APPLICATION_PGP and APPLICATION_SMIME bits.

That will allow various config vars to enable encryption or signing,
even though there will be no backend available to perform them.  The
message security flag might then be set, but when the user hits send,
will end up back at the compose menu due to the error.

The pgp or smime menu might not even be available to clear the
security setting!

Add a check in send.c before the compose menu is invoked, and give a
warning message for the menu ops inside the compose menu.

I believe this should prevent the issue.  However this is a corner
case combined with user misconfiguration, so I don't believe is worth
a large effort to completely eradicate.
Disable message security if the backend is not available.

Gitlab issue #3 exposed an awkward corner case: if mutt is configured
without PGP or S/MIME, and with GPGME, but $crypt_use_gpgme is unset.
In this case, no backend will be available, but WithCrypto will be set
with both APPLICATION_PGP and APPLICATION_SMIME bits.

That will allow various config vars to enable encryption or signing,
even though there will be no backend available to perform them.  The
message security flag might then be set, but when the user hits send,
will end up back at the compose menu due to the error.

The pgp or smime menu might not even be available to clear the
security setting!

Add a check in send.c before the compose menu is invoked, and give a
warning message for the menu ops inside the compose menu.

I believe this should prevent the issue.  However this is a corner
case combined with user misconfiguration, so I don't believe is worth
a large effort to completely eradicate.
Regexlist #3

Signed-off-by: Bo YU <yuzibode@126.com>
Implement set summary #3

Add a pager and stunk it there

Signed-off-by: Bo YU <yuzibode@126.com>
Implement set summary #3

Add a pager and stunk it there

Signed-off-by: Bo YU <yuzibode@126.com>
Implement set summary #3

Add a pager and stunk it there

Signed-off-by: Bo YU <yuzibode@126.com>
imap: add missing free_edata pointer

imap sometimes crashes when changing folder or quiting neomutt.

 #1  0x000055555563833f in mutt_email_free (e=0x555555b0cb50) at email/email.c:55
 #2  0x00005555555bb4e9 in mx_fastclose_mailbox (ctx=ctx@entry=0x555555980050) at mx.c:407
 #3  0x00005555555bd593 in mx_mbox_close (pctx=0x5555558a69b0 <Context>, index_hint=index_hint@entry=0x7fffffffa074) at mx.c:727
 ...

This change adds the missing free_edata pointer to imap_free_edata.
imap: add missing free_edata pointer

imap sometimes crashes when changing folder or quiting neomutt.

 #1  0x000055555563833f in mutt_email_free (e=0x555555b0cb50) at email/email.c:55
 #2  0x00005555555bb4e9 in mx_fastclose_mailbox (ctx=ctx@entry=0x555555980050) at mx.c:407
 #3  0x00005555555bd593 in mx_mbox_close (pctx=0x5555558a69b0 <Context>, index_hint=index_hint@entry=0x7fffffffa074) at mx.c:727
 ...

This change adds the missing free_edata pointer to imap_free_edata.
neomutt: check if the mail was parsed before storing it in hcache

Neomutt would segfault if the mail that should be inserted into the
header cache doesn't exist / failed to parse.

I was previously observing the following crash:

> $ bt
> #0  memcpy (__len=0xa8, __src=0x0, __dest=0x7ffdcc8a8890) at /nix/store/hycbi8v31600cimx211ilj8p922nharl-glibc-2.30-dev/include/bits/string_fortified.h:34
> #1  dump (uidvalidity=0x0, off=0x7ffdcc8a8884, e=0x0, hc=0xc18330) at hcache/hcache.c:164
> #2  mutt_hcache_store (hc=hc@entry=0xc18330, key=0xc7d820 "/some/path/that/doesn't exist",
>     keylen=0x65, e=e@entry=0x0, uidvalidity=uidvalidity@entry=0x0) at hcache/hcache.c:592
> #3  0x000000000049d677 in append_message (h=h@entry=0xc18330, m=m@entry=0xc10580, q=q@entry=0xc3f460, msg=msg@entry=0xc0e4b0, dedup=dedup@entry=0x0)
>     at notmuch/mutt_notmuch.c:971
> #4  0x000000000049dd3c in read_mesgs_query (dedup=0x0, q=0xc3f460, m=0xc10580) at notmuch/mutt_notmuch.c:1125
> #5  nm_mbox_open (m=0xc10580) at notmuch/mutt_notmuch.c:2171
> #6  0x000000000044ee5e in mx_mbox_open (m=0xc10580, flags=<optimized out>) at mx.c:369
> #7  0x000000000043e574 in main (argc=0x1, argv=<optimized out>, envp=<optimized out>) at main.c:1152
> #8  0x00007f53c2303d8b in __libc_start_main () from /nix/store/9rabxvqbv0vgjmydiv59wkz768b5fmbc-glibc-2.30/lib/libc.so.6
> #9  0x000000000040bf6a in _start () at ../sysdeps/x86_64/start.S:120
notmuch: fix crash in append_message()

Check if the mail was parsed before storing it in hcache.

Neomutt would segfault if the mail that should be inserted into the
header cache doesn't exist / failed to parse.

I was previously observing the following crash:

> $ bt
> #0  memcpy (__len=0xa8, __src=0x0, __dest=0x7ffdcc8a8890) at /nix/store/hycbi8v31600cimx211ilj8p922nharl-glibc-2.30-dev/include/bits/string_fortified.h:34
> #1  dump (uidvalidity=0x0, off=0x7ffdcc8a8884, e=0x0, hc=0xc18330) at hcache/hcache.c:164
> #2  mutt_hcache_store (hc=hc@entry=0xc18330, key=0xc7d820 "/some/path/that/doesn't exist",
>     keylen=0x65, e=e@entry=0x0, uidvalidity=uidvalidity@entry=0x0) at hcache/hcache.c:592
> #3  0x000000000049d677 in append_message (h=h@entry=0xc18330, m=m@entry=0xc10580, q=q@entry=0xc3f460, msg=msg@entry=0xc0e4b0, dedup=dedup@entry=0x0)
>     at notmuch/mutt_notmuch.c:971
> #4  0x000000000049dd3c in read_mesgs_query (dedup=0x0, q=0xc3f460, m=0xc10580) at notmuch/mutt_notmuch.c:1125
> #5  nm_mbox_open (m=0xc10580) at notmuch/mutt_notmuch.c:2171
> #6  0x000000000044ee5e in mx_mbox_open (m=0xc10580, flags=<optimized out>) at mx.c:369
> #7  0x000000000043e574 in main (argc=0x1, argv=<optimized out>, envp=<optimized out>) at main.c:1152
> #8  0x00007f53c2303d8b in __libc_start_main () from /nix/store/9rabxvqbv0vgjmydiv59wkz768b5fmbc-glibc-2.30/lib/libc.so.6
> #9  0x000000000040bf6a in _start () at ../sysdeps/x86_64/start.S:120
notmuch: fix crash in append_message()

Check if the mail was parsed before storing it in hcache.

Neomutt would segfault if the mail that should be inserted into the
header cache doesn't exist / failed to parse.

I was previously observing the following crash:

> $ bt
> #0  memcpy (__len=0xa8, __src=0x0, __dest=0x7ffdcc8a8890) at /nix/store/hycbi8v31600cimx211ilj8p922nharl-glibc-2.30-dev/include/bits/string_fortified.h:34
> #1  dump (uidvalidity=0x0, off=0x7ffdcc8a8884, e=0x0, hc=0xc18330) at hcache/hcache.c:164
> #2  mutt_hcache_store (hc=hc@entry=0xc18330, key=0xc7d820 "/some/path/that/doesn't exist",
>     keylen=0x65, e=e@entry=0x0, uidvalidity=uidvalidity@entry=0x0) at hcache/hcache.c:592
> #3  0x000000000049d677 in append_message (h=h@entry=0xc18330, m=m@entry=0xc10580, q=q@entry=0xc3f460, msg=msg@entry=0xc0e4b0, dedup=dedup@entry=0x0)
>     at notmuch/mutt_notmuch.c:971
> #4  0x000000000049dd3c in read_mesgs_query (dedup=0x0, q=0xc3f460, m=0xc10580) at notmuch/mutt_notmuch.c:1125
> #5  nm_mbox_open (m=0xc10580) at notmuch/mutt_notmuch.c:2171
> #6  0x000000000044ee5e in mx_mbox_open (m=0xc10580, flags=<optimized out>) at mx.c:369
> #7  0x000000000043e574 in main (argc=0x1, argv=<optimized out>, envp=<optimized out>) at main.c:1152
> #8  0x00007f53c2303d8b in __libc_start_main () from /nix/store/9rabxvqbv0vgjmydiv59wkz768b5fmbc-glibc-2.30/lib/libc.so.6
> #9  0x000000000040bf6a in _start () at ../sysdeps/x86_64/start.S:120
