Add OVR() palette to the sprite editor
You just wrote to add something, and said there is no space to?
You can store the pallete in code, just make it in the pallete editor and copy it into a string.
Added **Advanced Mode** button where OVR palette can be selected, also flags and BPP mode.
![screen](https://user-images.githubusercontent.com/1101448/90265349-21485480-de5b-11ea-98fd-1725248f6d18.gif)

Does this overrule/replace the copy/paste palette functionality?
> Does this overrule/replace the copy/paste palette functionality?

No, this doesn't, copypaste buttons moved to the right near rgb sliders.