diff --git a/database/rrd.c b/database/rrd.c
index 119efa62ea472..df298cd0c513f 100644
--- a/database/rrd.c
+++ b/database/rrd.c
@@ -148,3 +148,19 @@ char *rrdset_cache_dir(RRDHOST *host, const char *id, const char *config_section
 
     return ret;
 }
+
+// ----------------------------------------------------------------------------
+// Miscellaneous functions
+
+bool_t alarm_entry_isrepeating(RRDHOST *host, ALARM_ENTRY *ae) {
+    RRDCALC *rc = NULL;
+    for(rc = host->alarms; rc; rc = rc->next) {
+      if(ae->alarm_id == rc->id) {
+        break;
+      }
+    }
+    if (!rc) {
+      // TODO: Handle this!
+    }
+    return rrdcalc_isrepeating(rc);
+}
diff --git a/database/rrd.h b/database/rrd.h
index 15efc9d839e9c..4ecd64e592af8 100644
--- a/database/rrd.h
+++ b/database/rrd.h
@@ -473,20 +473,11 @@ struct alarm_entry {
     uint32_t updated_by_id;
     uint32_t updates_id;
 
-    uint32_t warn_repeat_every;
-    uint32_t crit_repeat_every;
     time_t last_repeat;
 
     struct alarm_entry *next;
 };
 
-typedef struct repeating_alarm_entry {
-    ALARM_ENTRY *alarm_entry;
-    struct repeating_alarm_entry *next;
-} REPEATING_ALARM_ENTRY;
-
-typedef REPEATING_ALARM_ENTRY *REPEATING_ALARM_ENTRY_LIST;
-
 typedef struct alarm_log {
     uint32_t next_log_id;
     uint32_t next_alarm_id;
@@ -588,8 +579,6 @@ struct rrdhost {
     uint32_t health_max_unique_id;                  // the max alarm log unique id given for the host
     uint32_t health_max_alarm_id;                   // the max alarm id given for the host
 
-    REPEATING_ALARM_ENTRY_LIST health_rep_alarm_entry_list;    // list of repeating alarm entries
-
     // templates of alarms
     // these are used to create alarms when charts
     // are created or renamed, that match them
@@ -871,6 +860,11 @@ extern collected_number rrddim_set(RRDSET *st, const char *id, collected_number
 
 extern long align_entries_to_pagesize(RRD_MEMORY_MODE mode, long entries);
 
+// ----------------------------------------------------------------------------
+// Miscellaneous functions
+
+extern bool_t alarm_entry_isrepeating(RRDHOST *host, ALARM_ENTRY *ae);
+
 // ----------------------------------------------------------------------------
 // RRD internal functions
 
diff --git a/database/rrdcalc.c b/database/rrdcalc.c
index 5f2158d7df6f0..8149bb84b85db 100644
--- a/database/rrdcalc.c
+++ b/database/rrdcalc.c
@@ -81,9 +81,9 @@ static void rrdsetcalc_link(RRDSET *st, RRDCALC *rc) {
 
     if(!rc->units) rc->units = strdupz(st->units);
 
-    {
+    if(!rrdcalc_isrepeating(rc)) {
         time_t now = now_realtime_sec();
-        health_alarm_log(
+        ALARM_ENTRY *ae = health_create_alarm_entry(
                 host,
                 rc->id,
                 rc->next_event_id++,
@@ -102,10 +102,9 @@ static void rrdsetcalc_link(RRDSET *st, RRDCALC *rc) {
                 rc->units,
                 rc->info,
                 0,
-                0,
-                rc->warn_repeat_every,
-                rc->crit_repeat_every
+                0
         );
+        health_alarm_log(host, ae);
     }
 }
 
@@ -144,9 +143,9 @@ inline void rrdsetcalc_unlink(RRDCALC *rc) {
 
     RRDHOST *host = st->rrdhost;
 
-    {
+    if(!rrdcalc_isrepeating(rc)) {
         time_t now = now_realtime_sec();
-        health_alarm_log(
+        ALARM_ENTRY *ae = health_create_alarm_entry(
                 host,
                 rc->id,
                 rc->next_event_id++,
@@ -165,10 +164,9 @@ inline void rrdsetcalc_unlink(RRDCALC *rc) {
                 rc->units,
                 rc->info,
                 0,
-                0,
-                rc->warn_repeat_every,
-                rc->crit_repeat_every
+                0
         );
+        health_alarm_log(host, ae);
     }
 
     debug(D_HEALTH, "Health unlinking alarm '%s.%s' from chart '%s' of host '%s'", rc->chart?rc->chart:"NOCHART", rc->name, st->id, host->hostname);
@@ -332,6 +330,7 @@ inline RRDCALC *rrdcalc_create_from_template(RRDHOST *host, RRDCALCTEMPLATE *rt,
     rc->delay_max_duration = rt->delay_max_duration;
     rc->delay_multiplier = rt->delay_multiplier;
 
+    rc->last_repeat = 0;
     rc->warn_repeat_every = rt->warn_repeat_every;
     rc->crit_repeat_every = rt->crit_repeat_every;
 
diff --git a/database/rrdcalc.h b/database/rrdcalc.h
index c5bb1e19a5539..801f55928ef0d 100644
--- a/database/rrdcalc.h
+++ b/database/rrdcalc.h
@@ -86,6 +86,7 @@ struct rrdcalc {
     // ------------------------------------------------------------------------
     // runtime information
 
+    RRDCALC_STATUS old_status;      // the old status of the alarm
     RRDCALC_STATUS status;          // the current status of the alarm
 
     calculated_number value;        // the current value of the alarm
@@ -96,6 +97,7 @@ struct rrdcalc {
     time_t last_updated;            // the last update timestamp of the alarm
     time_t next_update;             // the next update timestamp of the alarm
     time_t last_status_change;      // the timestamp of the last time this alarm changed status
+    time_t last_repeat;             // the last time the alarm got repeated
 
     time_t db_after;                // the first timestamp evaluated by the db lookup
     time_t db_before;               // the last timestamp evaluated by the db lookup
@@ -141,4 +143,11 @@ extern uint32_t rrdcalc_get_unique_id(RRDHOST *host, const char *chart, const ch
 extern RRDCALC *rrdcalc_create_from_template(RRDHOST *host, RRDCALCTEMPLATE *rt, const char *chart);
 extern void rrdcalc_add_to_host(RRDHOST *host, RRDCALC *rc);
 
+static inline bool_t rrdcalc_isrepeating(RRDCALC *rc) {
+  if (unlikely(rc->warn_repeat_every > 0 || rc->crit_repeat_every > 0)) {
+    return TRUE;
+  }
+  return FALSE;
+}
+
 #endif //NETDATA_RRDCALC_H
diff --git a/database/rrdhost.c b/database/rrdhost.c
index d6cdc9655c29a..796b532efcd7e 100644
--- a/database/rrdhost.c
+++ b/database/rrdhost.c
@@ -168,9 +168,8 @@ RRDHOST *rrdhost_create(const char *hostname,
     if(config_get_boolean(CONFIG_SECTION_GLOBAL, "delete orphan hosts files", 1) && !is_localhost)
         rrdhost_flag_set(host, RRDHOST_FLAG_DELETE_ORPHAN_HOST);
 
-    host->health_default_warn_repeat_every = config_get_duration(CONFIG_SECTION_HEALTH, "repeat warning notifications every", "never");
-    host->health_default_crit_repeat_every = config_get_duration(CONFIG_SECTION_HEALTH, "repeat critical notifications every", "never");
-    host->health_rep_alarm_entry_list = NULL;
+    host->health_default_warn_repeat_every = config_get_duration(CONFIG_SECTION_HEALTH, "default repeat warning", "never");
+    host->health_default_crit_repeat_every = config_get_duration(CONFIG_SECTION_HEALTH, "default repeat critical", "never");
 
     // ------------------------------------------------------------------------
     // initialize health variables
@@ -537,13 +536,6 @@ void rrdhost_free(RRDHOST *host) {
     while(host->alarms)
         rrdcalc_unlink_and_free(host, host->alarms);
 
-    while(host->health_rep_alarm_entry_list) {
-        health_alarm_log_free_one_nochecks_nounlink(host->health_rep_alarm_entry_list->alarm_entry);
-        REPEATING_ALARM_ENTRY *tmp = host->health_rep_alarm_entry_list;
-        host->health_rep_alarm_entry_list = host->health_rep_alarm_entry_list->next;
-        freez(tmp);
-    }
-
     while(host->templates)
         rrdcalctemplate_unlink_and_free(host, host->templates);
 
diff --git a/health/health.c b/health/health.c
index fef5d51b36d8a..8ddaaf5cfd9c6 100644
--- a/health/health.c
+++ b/health/health.c
@@ -1,7 +1,5 @@
 // SPDX-License-Identifier: GPL-3.0-or-later
 
-#include <assert.h>
-
 #include "health.h"
 
 struct health_cmdapi_thread_status {
@@ -258,61 +256,28 @@ static inline void health_alarm_log_process(RRDHOST *host) {
 
     ALARM_ENTRY *ae;
     for(ae = host->health_log.alarms; ae && ae->unique_id >= host->health_last_processed_id; ae = ae->next) {
-        assert(ae->warn_repeat_every == 0 && ae->crit_repeat_every == 0);
-        if(unlikely(
-            !(ae->flags & HEALTH_ENTRY_FLAG_PROCESSED) &&
-            !(ae->flags & HEALTH_ENTRY_FLAG_UPDATED)
-            )) {
+        if(likely(!alarm_entry_isrepeating(host, ae))) {
+            if(unlikely(
+                !(ae->flags & HEALTH_ENTRY_FLAG_PROCESSED) &&
+                !(ae->flags & HEALTH_ENTRY_FLAG_UPDATED)
+                )) {
 
-            if(unlikely(ae->unique_id < first_waiting))
-                first_waiting = ae->unique_id;
+                if(unlikely(ae->unique_id < first_waiting))
+                    first_waiting = ae->unique_id;
 
-            if(likely(now >= ae->delay_up_to_timestamp)) {
-                health_process_notifications(host, ae);
+                if(likely(now >= ae->delay_up_to_timestamp)) {
+                    health_process_notifications(host, ae);
+                }
             }
         }
+        else {
+          // TODO: Handle this!
+        }
     }
 
     // remember this for the next iteration
     host->health_last_processed_id = first_waiting;
 
-    // process repeating alarm entries
-    REPEATING_ALARM_ENTRY *it;
-    for(it = host->health_rep_alarm_entry_list; it; it = it->next) {
-        ALARM_ENTRY *ae = it->alarm_entry;
-        assert(ae->warn_repeat_every > 0 || ae->crit_repeat_every > 0);
-        if(unlikely(ae->last_repeat == 0)) {
-            if(likely(now >= ae->delay_up_to_timestamp)) {
-                health_process_notifications(host, ae);
-                ae->last_repeat = now;
-                debug(D_HEALTH, "Alarm entry from alarm %u is executed as repeating for the first time.", ae->alarm_id);
-            }
-        }
-        else {
-            int repeat_every = 0;
-            RRDCALC *rc;
-            for(rc = host->alarms; rc ; rc = rc->next) {
-                if(unlikely(rc->status == RRDCALC_STATUS_WARNING)) {
-                    if(ae->alarm_id == rc->id)
-                        repeat_every = ae->warn_repeat_every;
-                }
-                else if(unlikely(rc->status == RRDCALC_STATUS_CRITICAL)) {
-                    if(ae->alarm_id == rc->id)
-                        repeat_every = ae->crit_repeat_every;
-                }
-                else if(likely(rc->status == RRDCALC_STATUS_CLEAR)) {
-                    if(ae->alarm_id == rc->id)
-                        break;
-                }
-            }
-            if(unlikely(repeat_every > 0 && (ae->last_repeat + repeat_every) < now)) {
-                health_process_notifications(host, ae);
-                ae->last_repeat = now;
-                debug(D_HEALTH, "Alarm entry from alarm %u is executed as a repeating entry.", ae->alarm_id);
-            }
-        }
-    }
-
     netdata_rwlock_unlock(&host->health_log.alarm_log_rwlock);
 
     if(host->health_log.count <= host->health_log.max)
@@ -335,11 +300,15 @@ static inline void health_alarm_log_process(RRDHOST *host) {
 
         ALARM_ENTRY *t = ae->next;
 
-        assert(ae->warn_repeat_every == 0 && ae->crit_repeat_every == 0);
-        health_alarm_log_free_one_nochecks_nounlink(ae);
+        if(likely(!alarm_entry_isrepeating(host, ae))) {
+            health_alarm_log_free_one_nochecks_nounlink(ae);
+            host->health_log.count--;
+        }
+        else {
+          // TODO: Handle this
+        }
 
         ae = t;
-        host->health_log.count--;
     }
 
     netdata_rwlock_unlock(&host->health_log.alarm_log_rwlock);
@@ -798,21 +767,22 @@ void *health_main(void *ptr) {
 						rc->delay_last = delay;
 						rc->delay_up_to_timestamp = now + delay;
 
-						health_alarm_log(
-								host, rc->id, rc->next_event_id++, now, rc->name, rc->rrdset->id,
-								rc->rrdset->family, rc->exec, rc->recipient, now - rc->last_status_change,
-								rc->old_value, rc->value, rc->status, status, rc->source, rc->units, rc->info,
-								rc->delay_last,
-								(
-								    ((rc->options & RRDCALC_FLAG_NO_CLEAR_NOTIFICATION)? HEALTH_ENTRY_FLAG_NO_CLEAR_NOTIFICATION : 0) |
-								    ((rc->rrdcalc_flags & RRDCALC_FLAG_SILENCED)? HEALTH_ENTRY_FLAG_SILENCED : 0)
-								),
-								rc->warn_repeat_every,
-								rc->crit_repeat_every
-						);
-
-						rc->last_status_change = now;
-						rc->status = status;
+            if(likely(!rrdcalc_isrepeating(rc))) {
+                ALARM_ENTRY *ae = health_create_alarm_entry(
+                    host, rc->id, rc->next_event_id++, now, rc->name, rc->rrdset->id,
+                    rc->rrdset->family, rc->exec, rc->recipient, now - rc->last_status_change,
+                    rc->old_value, rc->value, rc->status, status, rc->source, rc->units, rc->info,
+                    rc->delay_last,
+                    (
+                        ((rc->options & RRDCALC_FLAG_NO_CLEAR_NOTIFICATION)? HEALTH_ENTRY_FLAG_NO_CLEAR_NOTIFICATION : 0) |
+                        ((rc->rrdcalc_flags & RRDCALC_FLAG_SILENCED)? HEALTH_ENTRY_FLAG_SILENCED : 0)
+                    )
+                );
+                health_alarm_log(host, ae);
+            }
+            rc->last_status_change = now;
+            rc->old_status = rc->status;
+            rc->status = status;
 					}
 
 					rc->last_updated = now;
@@ -822,6 +792,35 @@ void *health_main(void *ptr) {
 						next_run = rc->next_update;
 				}
 
+        // process repeating alarms
+        RRDCALC *rc;
+        for(rc = host->alarms; rc ; rc = rc->next) {
+            int repeat_every = 0;
+            if(unlikely(rrdcalc_isrepeating(rc))) {
+                if(unlikely(rc->status == RRDCALC_STATUS_WARNING))
+                    repeat_every = rc->warn_repeat_every;
+                else if(unlikely(rc->status == RRDCALC_STATUS_CRITICAL))
+                    repeat_every = rc->crit_repeat_every;
+            }
+            if(unlikely(repeat_every > 0 && (rc->last_repeat + repeat_every) < now)) {
+                rc->last_repeat = now;
+                ALARM_ENTRY *ae = health_create_alarm_entry(
+                    host, rc->id, rc->next_event_id++, now, rc->name, rc->rrdset->id,
+                    rc->rrdset->family, rc->exec, rc->recipient, now - rc->last_status_change,
+                    rc->old_value, rc->value, rc->old_status, rc->status, rc->source, rc->units, rc->info,
+                    rc->delay_last,
+                    (
+                        ((rc->options & RRDCALC_FLAG_NO_CLEAR_NOTIFICATION)? HEALTH_ENTRY_FLAG_NO_CLEAR_NOTIFICATION : 0) |
+                        ((rc->rrdcalc_flags & RRDCALC_FLAG_SILENCED)? HEALTH_ENTRY_FLAG_SILENCED : 0)
+                    )
+                );
+                ae->last_repeat = rc->last_repeat;
+                health_process_notifications(host, ae);
+                debug(D_HEALTH, "Notification sent for the repeating alarm %u.", ae->alarm_id);
+                health_alarm_log_free_one_nochecks_nounlink(ae);
+            }
+        }
+
 				rrdhost_unlock(host);
 			}
 
diff --git a/health/health.h b/health/health.h
index f8398fcec0339..cc0692a5676ba 100644
--- a/health/health.h
+++ b/health/health.h
@@ -108,7 +108,7 @@ extern void health_alarm_log_save(RRDHOST *host, ALARM_ENTRY *ae);
 extern ssize_t health_alarm_log_read(RRDHOST *host, FILE *fp, const char *filename);
 extern void health_alarm_log_load(RRDHOST *host);
 
-extern void health_alarm_log(
+extern ALARM_ENTRY* health_create_alarm_entry(
         RRDHOST *host,
         uint32_t alarm_id,
         uint32_t alarm_event_id,
@@ -127,9 +127,9 @@ extern void health_alarm_log(
         const char *units,
         const char *info,
         int delay,
-        uint32_t flags,
-        uint32_t warn_repear_every,
-        uint32_t crit_repeat_every);
+        uint32_t flags);
+
+extern void health_alarm_log(RRDHOST *host, ALARM_ENTRY *ae);
 
 extern void health_readdir(RRDHOST *host, const char *user_path, const char *stock_path, const char *subpath);
 extern char *health_user_config_dir(void);
diff --git a/health/health_config.c b/health/health_config.c
index 0279981a99335..954e9995b35c9 100644
--- a/health/health_config.c
+++ b/health/health_config.c
@@ -254,11 +254,6 @@ static inline int health_parse_repeat(
         uint32_t *crit_repeat_every
 ) {
 
-    uint32_t default_duration = 0;
-    char given_default = FALSE;
-    char given_warn_repeat = FALSE;
-    char given_crit_repeat = FALSE;
-
     char *s = string;
     while(*s) {
         char *key = s;
@@ -277,31 +272,15 @@ static inline int health_parse_repeat(
                 error("Health configuration at line %zu of file '%s': invalid value '%s' for '%s' keyword",
                         line, file, value, key);
             }
-            else given_warn_repeat = TRUE;
         }
         else if(!strcasecmp(key, "critical")) {
             if (!config_parse_duration(value, (int*)crit_repeat_every)) {
                 error("Health configuration at line %zu of file '%s': invalid value '%s' for '%s' keyword",
                         line, file, value, key);
             }
-            else given_crit_repeat = TRUE;
-        }
-        else {
-            if (!config_parse_duration(key, (int*)&default_duration)) {
-                error("Health configuration at line %zu of file '%s': invalid value '%s'",
-                        line, file, key);
-            }
-            else given_default = TRUE;
         }
     }
 
-    if(given_default) {
-        if(!given_warn_repeat)
-            *warn_repeat_every = default_duration;
-        if(!given_crit_repeat)
-            *crit_repeat_every = default_duration;
-    }
-
     return TRUE;
 }
 
@@ -554,6 +533,7 @@ static int health_readfile(const char *filename, void *data) {
             rc->value = NAN;
             rc->old_value = NAN;
             rc->delay_multiplier = 1.0;
+            rc->old_status = RRDCALC_STATUS_UNINITIALIZED;
             rc->warn_repeat_every = host->health_default_warn_repeat_every;
             rc->crit_repeat_every = host->health_default_crit_repeat_every;
 
@@ -734,7 +714,7 @@ static int health_readfile(const char *filename, void *data) {
                 rc->options |= health_parse_options(value);
             }
             else if(hash == hash_repeat && !strcasecmp(key, HEALTH_REPEAT_KEY)){
-                health_parse_repeat(line,filename, value,
+                health_parse_repeat(line, filename, value,
                     &rc->warn_repeat_every,
                     &rc->crit_repeat_every);
             }
@@ -863,6 +843,11 @@ static int health_readfile(const char *filename, void *data) {
             else if(hash == hash_options && !strcasecmp(key, HEALTH_OPTIONS_KEY)) {
                 rt->options |= health_parse_options(value);
             }
+            else if(hash == hash_repeat && !strcasecmp(key, HEALTH_REPEAT_KEY)){
+                health_parse_repeat(line, filename, value,
+                    &rt->warn_repeat_every,
+                    &rt->crit_repeat_every);
+            }
             else {
                 error("Health configuration at line %zu of file '%s' for template '%s' has unknown key '%s'.",
                         line, filename, rt->name, key);
diff --git a/health/health_log.c b/health/health_log.c
index 1fde88a213000..a83b8ad66aed5 100644
--- a/health/health_log.c
+++ b/health/health_log.c
@@ -78,6 +78,7 @@ inline void health_alarm_log_save(RRDHOST *host, ALARM_ENTRY *ae) {
                         "\t%08x\t%08x\t%08x"
                         "\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s"
                         "\t%d\t%d\t%d\t%d"
+                        "\t%016lx"
                         "\t" CALCULATED_NUMBER_FORMAT_AUTO "\t" CALCULATED_NUMBER_FORMAT_AUTO
                         "\n"
                             , (ae->flags & HEALTH_ENTRY_FLAG_SAVED)?'U':'A'
@@ -110,6 +111,8 @@ inline void health_alarm_log_save(RRDHOST *host, ALARM_ENTRY *ae) {
                             , ae->old_status
                             , ae->delay
 
+                            , (uint64_t)ae->last_repeat
+
                             , ae->new_value
                             , ae->old_value
         ) < 0))
@@ -134,7 +137,7 @@ inline ssize_t health_alarm_log_read(RRDHOST *host, FILE *fp, const char *filena
         host->health_log_entries_written++;
         line++;
 
-        int max_entries = 30, entries = 0;
+        int max_entries = 31, entries = 0;
         char *pointers[max_entries];
 
         pointers[entries++] = s++;
@@ -174,6 +177,22 @@ inline ssize_t health_alarm_log_read(RRDHOST *host, FILE *fp, const char *filena
                 continue;
             }
 
+            time_t last_repeat = (time_t)strtoul(pointers[25], NULL, 16);
+
+            RRDCALC *rc = NULL;
+            for(rc = host->alarms; rc; rc = rc->next) {
+                if(rc->id == alarm_id && rrdcalc_isrepeating(rc)) {
+                    break;
+                }
+            }
+            if (!rc) {
+                // TODO: Handle this!
+            }
+            else {
+                rc->last_repeat = last_repeat;
+                continue;
+            }
+
             if(unlikely(*pointers[0] == 'A')) {
                 // make sure it is properly numbered
                 if(unlikely(host->health_log.alarms && unique_id < host->health_log.alarms->unique_id)) {
@@ -185,23 +204,27 @@ inline ssize_t health_alarm_log_read(RRDHOST *host, FILE *fp, const char *filena
                 ae = callocz(1, sizeof(ALARM_ENTRY));
             }
             else if(unlikely(*pointers[0] == 'U')) {
-                // find the original
-                for(ae = host->health_log.alarms; ae; ae = ae->next) {
-                    if(unlikely(unique_id == ae->unique_id)) {
-                        if(unlikely(*pointers[0] == 'A')) {
-                            error("HEALTH [%s]: line %zu of file '%s' adds duplicate alarm log entry %u. Using the later."
-                                  , host->hostname, line, filename, unique_id);
-                            *pointers[0] = 'U';
-                            duplicate++;
+                if(likely(!alarm_entry_isrepeating(host, ae))) {
+                    // find the original
+                    for(ae = host->health_log.alarms; ae; ae = ae->next) {
+                        if(unlikely(unique_id == ae->unique_id)) {
+                            if(unlikely(*pointers[0] == 'A')) {
+                                error("HEALTH [%s]: line %zu of file '%s' adds duplicate alarm log entry %u. Using the later."
+                                      , host->hostname, line, filename, unique_id);
+                                *pointers[0] = 'U';
+                                duplicate++;
+                            }
+                            break;
+                        }
+                        else if(unlikely(unique_id > ae->unique_id)) {
+                            // no need to continue
+                            // the linked list is sorted
+                            ae = NULL;
+                            break;
                         }
-                        break;
-                    }
-                    else if(unlikely(unique_id > ae->unique_id)) {
-                        // no need to continue
-                        // the linked list is sorted
-                        ae = NULL;
-                        break;
                     }
+                } else {
+                    // TODO: Handle this!
                 }
             }
 
@@ -267,11 +290,10 @@ inline ssize_t health_alarm_log_read(RRDHOST *host, FILE *fp, const char *filena
             ae->old_status  = str2i(pointers[23]);
             ae->delay       = str2i(pointers[24]);
 
-            ae->new_value   = str2l(pointers[25]);
-            ae->old_value   = str2l(pointers[26]);
+            ae->last_repeat = last_repeat;
 
-            ae->warn_repeat_every = 0;
-            ae->crit_repeat_every = 0;
+            ae->new_value   = str2l(pointers[26]);
+            ae->old_value   = str2l(pointers[27]);
 
             char value_string[100 + 1];
             freez(ae->old_value_string);
@@ -342,7 +364,7 @@ inline void health_alarm_log_load(RRDHOST *host) {
 // ----------------------------------------------------------------------------
 // health alarm log management
 
-inline void health_alarm_log(
+inline ALARM_ENTRY* health_create_alarm_entry(
         RRDHOST *host,
         uint32_t alarm_id,
         uint32_t alarm_event_id,
@@ -361,100 +383,70 @@ inline void health_alarm_log(
         const char *units,
         const char *info,
         int delay,
-        uint32_t flags,
-        uint32_t warn_repeat_every,
-        uint32_t crit_repeat_every
+        uint32_t flags
 ) {
-    debug(D_HEALTH, "Health adding alarm log entry with id: %u", host->health_log.next_log_id);
-
-    bool_t create_new_alarm_entry = TRUE;
-    bool_t is_repeating = make_bool(warn_repeat_every > 0 || crit_repeat_every > 0);
-    REPEATING_ALARM_ENTRY *target_alarm_entity = NULL;
-    if(unlikely(is_repeating)) {
-        REPEATING_ALARM_ENTRY *it;
-        for(it = host->health_rep_alarm_entry_list; it && it->next; it = it->next)
-            if(it->alarm_entry->alarm_id == alarm_id) {
-                target_alarm_entity = it;
-                create_new_alarm_entry = FALSE;
-                break;
-            }
+    debug(D_HEALTH, "Creating alarm log entry with id: %u", host->health_log.next_log_id);
+
+    ALARM_ENTRY *ae = callocz(1, sizeof(ALARM_ENTRY));
+    ae->name = strdupz(name);
+    ae->hash_name = simple_hash(ae->name);
+
+    if(chart) {
+        ae->chart = strdupz(chart);
+        ae->hash_chart = simple_hash(ae->chart);
     }
 
-    ALARM_ENTRY *ae = NULL;
-    if(likely(create_new_alarm_entry)) {
-        ae = callocz(1, sizeof(ALARM_ENTRY));
-        ae->name = strdupz(name);
-        ae->hash_name = simple_hash(ae->name);
+    if(family)
+        ae->family = strdupz(family);
 
-        if(chart) {
-            ae->chart = strdupz(chart);
-            ae->hash_chart = simple_hash(ae->chart);
-        }
+    if(exec) ae->exec = strdupz(exec);
+    if(recipient) ae->recipient = strdupz(recipient);
+    if(source) ae->source = strdupz(source);
+    if(units) ae->units = strdupz(units);
+    if(info) ae->info = strdupz(info);
 
-        if(family)
-            ae->family = strdupz(family);
-
-        if(exec) ae->exec = strdupz(exec);
-        if(recipient) ae->recipient = strdupz(recipient);
-        if(source) ae->source = strdupz(source);
-        if(units) ae->units = strdupz(units);
-        if(info) ae->info = strdupz(info);
-
-        ae->unique_id = host->health_log.next_log_id++;
-        ae->alarm_id = alarm_id;
-        ae->alarm_event_id = alarm_event_id;
-        ae->when = when;
-        ae->old_value = old_value;
-        ae->new_value = new_value;
-
-        char value_string[100 + 1];
-        ae->old_value_string = strdupz(format_value_and_unit(value_string, 100, ae->old_value, ae->units, -1));
-        ae->new_value_string = strdupz(format_value_and_unit(value_string, 100, ae->new_value, ae->units, -1));
-
-        ae->old_status = old_status;
-        ae->new_status = new_status;
-        ae->duration = duration;
-        ae->delay = delay;
-        ae->delay_up_to_timestamp = when + delay;
-        ae->flags |= flags;
-
-        ae->warn_repeat_every = warn_repeat_every;
-        ae->crit_repeat_every = crit_repeat_every;
-        ae->last_repeat = 0;
-
-        if(ae->old_status == RRDCALC_STATUS_WARNING || ae->old_status == RRDCALC_STATUS_CRITICAL)
-            ae->non_clear_duration += ae->duration;
-    }
-    else {
-        ae = target_alarm_entity->alarm_entry;
-        netdata_rwlock_wrlock(&host->health_log.alarm_log_rwlock);
-        // TODO: Update more necessary fields
-        ae->old_status = old_status;
-        ae->new_status = new_status;
-        netdata_rwlock_unlock(&host->health_log.alarm_log_rwlock);
+    ae->unique_id = host->health_log.next_log_id++;
+    ae->alarm_id = alarm_id;
+    ae->alarm_event_id = alarm_event_id;
+    ae->when = when;
+    ae->old_value = old_value;
+    ae->new_value = new_value;
+
+    char value_string[100 + 1];
+    ae->old_value_string = strdupz(format_value_and_unit(value_string, 100, ae->old_value, ae->units, -1));
+    ae->new_value_string = strdupz(format_value_and_unit(value_string, 100, ae->new_value, ae->units, -1));
+
+    ae->old_status = old_status;
+    ae->new_status = new_status;
+    ae->duration = duration;
+    ae->delay = delay;
+    ae->delay_up_to_timestamp = when + delay;
+    ae->flags |= flags;
+
+    ae->last_repeat = 0;
+
+    if(ae->old_status == RRDCALC_STATUS_WARNING || ae->old_status == RRDCALC_STATUS_CRITICAL)
+        ae->non_clear_duration += ae->duration;
+
+    return ae;
+}
+
+inline void health_alarm_log(
+        RRDHOST *host,
+        ALARM_ENTRY *ae
+) {
+    debug(D_HEALTH, "Health adding alarm log entry with id: %u", ae->unique_id);
+
+    if(unlikely(alarm_entry_isrepeating(host, ae))) {
+        error("Repeating alarms cannot be added to host's alarm log entries. It seems somewhere in the logic, API is being misused. Alarm id: %u", ae->alarm_id);
+        return;
     }
 
     // link it
     netdata_rwlock_wrlock(&host->health_log.alarm_log_rwlock);
-    if(likely(!is_repeating)) {
-        ae->next = host->health_log.alarms;
-        host->health_log.alarms = ae;
-        host->health_log.count++;
-    }
-    else {
-        if(unlikely(!target_alarm_entity)) {
-            // TODO: Remove these entries once host is going to be freed
-            REPEATING_ALARM_ENTRY *rae = callocz(1, sizeof(REPEATING_ALARM_ENTRY));
-            rae->alarm_entry = ae;
-            if(unlikely(!host->health_rep_alarm_entry_list)) {
-                host->health_rep_alarm_entry_list = rae;
-            }
-            else {
-                rae->next = host->health_rep_alarm_entry_list;
-                host->health_rep_alarm_entry_list = rae;
-            }
-        }
-    }
+    ae->next = host->health_log.alarms;
+    host->health_log.alarms = ae;
+    host->health_log.count++;
     netdata_rwlock_unlock(&host->health_log.alarm_log_rwlock);
 
     // match previous alarms
diff --git a/web/gui/main.js b/web/gui/main.js
index 2b1860cb32ff6..f8c1873e990ec 100644
--- a/web/gui/main.js
+++ b/web/gui/main.js
@@ -550,7 +550,7 @@ function renderStreamedHosts(options) {
 }
 
 function renderMachines(machinesArray) {
-    // let html = isSignedIn() 
+    // let html = isSignedIn()
     //     ? `<div class="info-item">My nodes</div>`
     //     : `<div class="info-item">My nodes</div>`;
 
@@ -728,13 +728,13 @@ function renderMyNetdataMenu(machinesArray) {
     if (isSignedIn()) {
         html += (
             `<div class="filter-control">
-                <input 
+                <input
                     id="my-netdata-menu-filter-input"
-                    type="text" 
+                    type="text"
                     placeholder="filter nodes..."
                     autofocus
                     autocomplete="off"
-                    value="${myNetdataMenuFilterValue}" 
+                    value="${myNetdataMenuFilterValue}"
                     onkeydown="myNetdataFilterDidChange(event)"
                 />
                 <span class="filter-control__clear" onclick="myNetdataFilterClearDidClick(event)"><i class="fas fa-times"></i><span>
@@ -918,7 +918,7 @@ function gotoServerModalHandler(guid) {
 
     if (!isSignedIn()) {
         // When the registry is enabled, if the user's known URLs are not working
-        // we consult the registry to get additional URLs.  
+        // we consult the registry to get additional URLs.
         setTimeout(function () {
             if (gotoServerStop === false) {
                 document.getElementById('gotoServerResponse').innerHTML = '<b>Added all the known URLs for this machine.</b>';
@@ -979,7 +979,7 @@ function notifyForSwitchRegistry() {
     }
 }
 
-var deleteRegistryGuid = null; 
+var deleteRegistryGuid = null;
 var deleteRegistryUrl = null;
 
 function deleteRegistryModalHandler(guid, name, url) {
@@ -992,7 +992,7 @@ function deleteRegistryModalHandler(guid, name, url) {
     document.getElementById('deleteRegistryServerName2').innerHTML = name;
     document.getElementById('deleteRegistryServerURL').innerHTML = url;
     document.getElementById('deleteRegistryResponse').innerHTML = '';
- 
+
     $('#deleteRegistryModal').modal('show');
 }
 
@@ -1015,7 +1015,7 @@ function notifyForDeleteRegistry() {
                         deleteRegistryUrl = null;
                         $('#deleteRegistryModal').modal('hide');
                         NETDATA.registry.init();
-                    });    
+                    });
                 });
         } else {
             NETDATA.registry.delete(deleteRegistryUrl, function (result) {
@@ -1026,7 +1026,7 @@ function notifyForDeleteRegistry() {
                 } else {
                     responseEl.innerHTML = "<b>Sorry, this command was rejected by the registry server!</b>";
                 }
-            });              
+            });
         }
     }
 }
@@ -2069,6 +2069,14 @@ function alarmsUpdateModal() {
                     + ((chart.red !== null) ? ('<tr><td width="10%" style="text-align:right">red&nbsp;threshold</td><td><code>' + chart.red + ' ' + units + '</code></td></tr>') : '');
             }
 
+            if (alarm.warn_repeat_every > 0) {
+                html += '<tr><td width="10%" style="text-align:right">repeat&nbsp;warning</td><td>' + NETDATA.seconds4human(alarm.warn_repeat_every) + '</td></tr>';
+            }
+
+            if (alarm.crit_repeat_every > 0) {
+                html += '<tr><td width="10%" style="text-align:right">repeat&nbsp;critical</td><td>' + NETDATA.seconds4human(alarm.crit_repeat_every) + '</td></tr>';
+            }
+
             var delay = '';
             if ((alarm.delay_up_duration > 0 || alarm.delay_down_duration > 0) && alarm.delay_multiplier !== 0 && alarm.delay_max_duration > 0) {
                 if (alarm.delay_up_duration === alarm.delay_down_duration) {
@@ -4485,7 +4493,7 @@ function getCloudAccountAgents() {
     if (!isSignedIn()) {
         return [];
     }
-    
+
     return fetch(
         `${NETDATA.registry.cloudBaseURL}/api/v1/accounts/${cloudAccountID}/agents`,
         {
@@ -4544,7 +4552,7 @@ function postCloudAccountAgents(agentsToSync) {
         "agents": agents,
         "merge": false,
     };
-    
+
     return fetch(
         `${NETDATA.registry.cloudBaseURL}/api/v1/accounts/${cloudAccountID}/agents`,
         {
@@ -4572,7 +4580,7 @@ function postCloudAccountAgents(agentsToSync) {
                 "url": a.urls[0],
                 "alternate_urls": a.urls
             }
-        })        
+        })
     });
 }
 
@@ -4629,7 +4637,7 @@ function updateMyNetdataAfterFilterChange() {
 
     if (options.hosts.length > 1) {
         const streamedEl = document.getElementById("my-netdata-menu-streamed")
-        streamedEl.innerHTML = renderStreamedHosts(options);    
+        streamedEl.innerHTML = renderStreamedHosts(options);
     }
 }
 
@@ -4644,7 +4652,7 @@ function myNetdataFilterDidChange(e) {
     const inputEl = e.target;
     setTimeout(() => {
         myNetdataMenuFilterValue = inputEl.value;
-        updateMyNetdataAfterFilterChange();        
+        updateMyNetdataAfterFilterChange();
     }, 1);
 }
 
@@ -4655,9 +4663,9 @@ function myNetdataFilterClearDidClick(e) {
     const inputEl = document.getElementById("my-netdata-menu-filter-input");
     inputEl.value = "";
     myNetdataMenuFilterValue = "";
-    
-    updateMyNetdataAfterFilterChange();        
-    
+
+    updateMyNetdataAfterFilterChange();
+
     inputEl.focus();
 }
 
@@ -4742,7 +4750,7 @@ function handleSignInMessage(e) {
 }
 
 function handleSignOutMessage(e) {
-    clearCloudVariables();    
+    clearCloudVariables();
     renderAccountUI();
     renderMyNetdataMenu(registryAgents);
 }
@@ -4819,14 +4827,14 @@ function explicitlySyncAgents() {
     const sync = json ? JSON.parse(json): {};
     delete sync[cloudAccountID];
     localStorage.setItem("cloud.sync", JSON.stringify(sync));
-    
+
     NETDATA.registry.init();
 }
 
 function syncAgents(callback) {
     const json = localStorage.getItem("cloud.sync");
     const sync = json ? JSON.parse(json): {};
-    
+
     const currentAgent = {
         guid: NETDATA.registry.machine_guid,
         name: NETDATA.registry.hostname,
@@ -4834,10 +4842,10 @@ function syncAgents(callback) {
         alternate_urls: [NETDATA.serverDefault],
     }
 
-    const localAgents = sync[cloudAccountID] 
-        ? [currentAgent] 
+    const localAgents = sync[cloudAccountID]
+        ? [currentAgent]
         : registryAgents.concat([currentAgent]);
-    
+
     console.log("Checking if sync is needed.", localAgents);
 
     const agentsToSync = mergeAgents(cloudAgents, localAgents);
@@ -4849,15 +4857,15 @@ function syncAgents(callback) {
 
     if (agentsToSync) {
         console.log("Synchronizing with netdata.cloud.");
-        
+
         postCloudAccountAgents(agentsToSync).then((agents) => {
             // TODO: clear syncTime on error!
             cloudAgents = agents;
             callback(cloudAgents);
         });
 
-        return        
-    } 
+        return
+    }
 
     callback(cloudAgents);
 }
@@ -4905,10 +4913,10 @@ function netdataRegistryCallback(machinesArray) {
 
     initCloud();
 
-    registryAgents = machinesArray;  
+    registryAgents = machinesArray;
 
     if (isSignedIn()) {
-        // We call getCloudAccountAgents() here because it requires that 
+        // We call getCloudAccountAgents() here because it requires that
         // NETDATA.registry is initialized.
         clearMyNetdataMenu();
         getCloudAccountAgents().then((agents) => {
@@ -4916,17 +4924,17 @@ function netdataRegistryCallback(machinesArray) {
                 errorMyNetdataMenu();
                 return;
             }
-            cloudAgents = agents; 
+            cloudAgents = agents;
             syncAgents((agents) => {
                 const agentsMap = {}
                 for (const agent of agents) {
                     agentsMap[agent.guid] = agent;
                 }
-    
+
                 NETDATA.registry.machines = agentsMap;
                 NETDATA.registry.machines_array = agents;
-    
-                renderMyNetdataMenu(agents);    
+
+                renderMyNetdataMenu(agents);
             });
         });
     } else {
@@ -4934,8 +4942,8 @@ function netdataRegistryCallback(machinesArray) {
     }
 };
 
-// If we know the cloudBaseURL and agentID from local storage render (eagerly) 
-// the account ui before receiving the definitive response from the web server. 
+// If we know the cloudBaseURL and agentID from local storage render (eagerly)
+// the account ui before receiving the definitive response from the web server.
 // This improves the perceived performance.
 function tryFastInitCloud() {
     const baseURL = localStorage.getItem("cloud.baseURL");
@@ -4945,13 +4953,13 @@ function tryFastInitCloud() {
         NETDATA.registry.cloudBaseURL = baseURL;
         NETDATA.registry.machine_guid = agentID;
         NETDATA.registry.isCloudEnabled = true;
-    
+
         initCloud();
     }
 }
 
 function initializeApp() {
-    window.addEventListener("message", handleMessage, false);    
+    window.addEventListener("message", handleMessage, false);
 
 //    tryFastInitCloud();
 }
