Java setter method invocation fails in an OSGi environment after a bundle redeploy
Just to make sure I understand, after the bundle is redeployed, the method signature in question did not change, right?  But then the method invocation stops working cause it doesn't think the object matches up.

I'm not sure why it would fail at that point, but I suspect it's related to the caching I added in 3.3.  For performance optimization I had the C library retain a map/dictionary of java fully qualified names to Method objects.  Nothing is ever really clearing from that cache, so if the bundle is reloaded it would potentially have the wrong method reference.  See https://github.com/mrj0/jep/blob/dev_3.4/src/jep/pyjobject.c#L307

Given your scenario, I think we need to rethink that performance optimization.  It's probably also not secure in the sense that that cache in memory can have classes loaded from different classloaders.  The simplest fix is probably to tie the cache of classname to methods to the Jep instance/interpreter in question, instead of it being global across interpreters.  That seems relatively minor so I could probably get that working in dev_3.4 branch.

We do have plans to rework all that caching and determination of attributes/methods for efficiency and cleanliness, but that's a rather large task and not coming anytime soon.

Your understanding is correct.

The idea of coupling the cache life-cycle to that of the Jep instance looks good to me, although that will imply a larger memory footprint.

Alternatively, being my use case probably not that common  (in practice, I am redefining classes on a live application, which is not that frequent in JavaSE or JavaEE), a simple solution could consist in providing a static method to let client code invalidate the global cache. Then, I could just call such method when the bundle is redeployed.

It'd still be faster and about the same footprint as what was there prior to 3.3, since back then every PyJobject that was instantiated had to do reflection lookups and make new PyJmethods.  The concept of a global cache seems a bit wrong since it's potentially not respecting the ClassLoader and an OSGi environment could theoretically have two classes of the same name and different versions co-exist in different interpreters.

Ok I put a fix up on the dev_3.4 branch.  The extra memory usage and slightly more reflection calls should be negligible.  I need to do a bit more testing of it in multi-threaded environments to verify I didn't mess up the memory management, but in theory it should work and resolve the issue.  Can you try rebuilding the library and see if it does indeed resolve your issue?

Now It works like a charm. Thank you for being so helpful.

Fixed in 3.4.

I have this issue again after updating jep project to the last commit. Before I was aligned to this commit  https://github.com/mrj0/jep/tree/778b516930bdc11ecbb7751d560d373964009639 
If I rollback to that commit then everything works again.

That is unfortunate, I introduced a memory leak of references on that commit.  I just pushed up a one line fix that should take care of the memory more safely.  Can you test the latest on branch dev_3.4?  I'm hoping that that will fix it....

I try the last version and now works everything again. Thank you very much!

Great!  Thanks for testing.  Closing as fixed in 3.4.
