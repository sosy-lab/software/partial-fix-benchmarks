Default use of EGC_ALWAYS in nodeMCU Emergency GC
I would like to see this as an option. It would remove the need to recompile the firmware to change the mode....

OK, I have implemented this in node adding the `node.setEGCmode(modes[, memlimit])` function and four `node.EGC` constants: `EGC_NOT_ACTIVE`, `EGC_ON_ALLOC_FAILURE` , `EGC_ON_MEM_LIMIT`,  `EGC_ALWAYS`.  The second option only applies in the case of the third option, so 

``` Lua
node.setEGCmode(node.EGC_ON_ALLOC_FAILURE+node.EGC_ON_MEM_LIMIT, 20000)
```

Will run the EGC if there is an memory allocation failure (before retrying the allocation) or if the heap is less than 20000.  The performance impact is huge, here is an example starting from reboot after the loadfile of the benchmark:

```
 for i=1,3 do benchmark(100,i) end 
1844.330 mSec, check = 2000, heap = 37976
2013.452 mSec, check = 2000, heap = 37920
1983.193 mSec, check = 2000, heap = 37960
> for i=1,3 do benchmark(100,i) end 
1864.835 mSec, check = 2000, heap = 37888
2018.612 mSec, check = 2000, heap = 37824
1983.717 mSec, check = 2000, heap = 37904
> node.setEGCmode(node.EGC_ON_ALLOC_FAILURE)
> for i=1,3 do benchmark(100,i) end 
 168.514 mSec, check = 2000, heap = 35496
 291.740 mSec, check = 2000, heap = 37440
 253.626 mSec, check = 2000, heap = 35368
> for i=1,3 do benchmark(100,i) end 
 161.296 mSec, check = 2000, heap = 37384
 297.843 mSec, check = 2000, heap = 35208
 256.625 mSec, check = 2000, heap = 34192
```

I have dropped this commit into my fork here:  463caab but I have not created a PR because I am not sure that we want to expose this to dev and the wider community just yet.  Maybe this is a case for a separate branch to do this memory hunting. As Philip effectively says, there are gremlins still hiding in this allocation code.

Can we get this merged into the new 1.5 based dev branch? This could help reduce the amount of time spent inside the lua VM on each callback -- thereby helping the stability of the platform.

Philip, I am planning to include this, but I would prefer to improve stability by eliminating issues that could cause panics, etc. rather then dodging the issue by making them less frequent.  

On 30/12/2015 11:05, Terry Ellison wrote:

> Philip, I am planning to include this, but I would prefer to improve 
> stability by eliminating issues that could cause panics, etc. rather 
> then dodging the issue by making them less frequent.
> 
> —
> Reply to this email directly or view it on GitHub 
> https://github.com/nodemcu/nodemcu-firmware/issues/609#issuecomment-168024682.
> 
> Understood. We could add this in a phased approach -- with the first 
> phase being just to add the call, and then a second phase to set the 
> default mode to a more efficient one.

2 phase approach sounds good.

I must try this to see if it affects my issue with the upvalues.

I have updated this slightly and issued the PR.  the command is not `node.egc.setmode(mode, [llimit].  This gist contains my little [benchmark.lua](https://gist.github.com/TerryE/a511bd0b05d4a77c3f9c), so:

```
> -- Default EGC setting
> for i=1,3 do benchmark(100,1) end
1859.443 mSec, check = 2000, heap = 41200
1848.054 mSec, check = 2000, heap = 41208
1861.330 mSec, check = 2000, heap = 41184

> node.egc.setmode(node.egc.ON_ALLOC_FAILURE)
> for i=1,3 do benchmark(100,1) end
 176.434 mSec, check = 2000, heap = 38752
 176.242 mSec, check = 2000, heap = 40480
 175.878 mSec, check = 2000, heap = 39992
> for i=1,3 do benchmark(100,1) end
 173.330 mSec, check = 2000, heap = 38728
 175.056 mSec, check = 2000, heap = 40464
 174.969 mSec, check = 2000, heap = 40032
```

Essentially the default mode is to fire the CG before every alloc.  The second run only fires it on allocation failure and then retries the alloc -- more saw-toothing and possibly fragmentation of the heap, but a _lot_ less processing overhead in the garbage collector.  My thinking is that when we were working with 15K heap the "always" option was probably needed, but with ~ 45K now I'd rather have the up to 10x speed increase.  

I've put this out so we can all play.

One little side effect is that is we go this way then it would be wise to switch over to c_mallocs and c_zallocs to using the Lua allocator.

Oops, just has to redo the push. I copied a constant from the source just before I did the commit  but did a ^X instead of a ^C.  All fixed now :relieved: 

As Phillip commented in the PR the default sertting is (node.ecg.ALWAYS, 4096 ) and in this case the second parameter has no effect. 

I play around with the benchmark (I dropped the WDclr down a level because it was tripping on some tests).  Try your own.  Clearly this allocator swap only impacts allocator-heavy work -- string manipulation, array manipulation, functions, ...  But here is a typical example:

```
> node.egc.setmode(node.egc.ALWAYS)
> benchmark(100,300,2) 
52793.186 mSec, check = 30000, heap = 39640
41032
41032
41032
> node.egc.setmode(node.egc.ON_ALLOC_FAILURE)
> benchmark(100,300,2) 
4719.111 mSec, check = 30000, heap = 38648
40984
41024
41024
```

That's **4.7** sec vs **52.8** or **13×faster**.  The reason for the repeated GCs is the white / grey / black marking system means that most of the space is recovered on the first pass but some take 3 passes.  This isn't the case in the `ALWAYS` case as there is only grey stuff available since the GC is always running anyway.

See PR #1068
