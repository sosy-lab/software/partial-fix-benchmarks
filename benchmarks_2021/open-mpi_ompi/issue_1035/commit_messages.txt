bump fortran mpi version

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
cm: fix selection priority

This patch removes a priority check that disables cm if the previous
pml had higher priority. The check was incorrect as coded and is
unnecessary as we finalize all but one pml anyway.

Fixes #1035

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
cm: fix selection priority

This patch removes a priority check that disables cm if the previous
pml had higher priority. The check was incorrect as coded and is
unnecessary as we finalize all but one pml anyway.

Fixes open-mpi/ompi#1035

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
cm: fix selection priority

This patch removes a priority check that disables cm if the previous
pml had higher priority. The check was incorrect as coded and is
unnecessary as we finalize all but one pml anyway.

Fixes open-mpi/ompi#1035

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
cm: fix selection priority

This patch removes a priority check that disables cm if the previous
pml had higher priority. The check was incorrect as coded and is
unnecessary as we finalize all but one pml anyway.

Fixes open-mpi/ompi#1035

(cherry picked from open-mpi/ompi@2fd176ac7f99b554da1706a0bfe7d93c648c8804)

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
cm: fix selection priority

This patch removes a priority check that disables cm if the previous
pml had higher priority. The check was incorrect as coded and is
unnecessary as we finalize all but one pml anyway.

Fixes open-mpi/ompi#1035

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>

(cherry picked from open-mpi/ompi@2fd176ac7f99b554da1706a0bfe7d93c648c8804)
cm: fix selection priority

This patch removes a priority check that disables cm if the previous
pml had higher priority. The check was incorrect as coded and is
unnecessary as we finalize all but one pml anyway.

Fixes open-mpi/ompi#1035

(cherry picked from open-mpi/ompi@2fd176ac7f99b554da1706a0bfe7d93c648c8804)

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
Merge pull request #1035 from rhc54/cmr2.0/usock

Restore the usock oob component
