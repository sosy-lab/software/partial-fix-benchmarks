regression: orte_submit_job() high error rates during startup
Error -117 corresponds to ```ORTE_ERR_SYS_LIMITS_PIPES``` (see orte/include/orte/constants.h), so it sounds like you are running out of file descriptors. I don't see an error 69 anywhere so I'm not sure where that is coming from.

I'd guess we are leaking file descriptors ☹️ 
Hrm... bisect is turning out to be tricky, as the build was broken in that window.

I think the culprit is either:
https://github.com/open-mpi/ompi/commit/eb67c2fd44f4635b6cff490e5f03de6ea8dafc23 (@rhc54)
or
https://github.com/open-mpi/ompi/pull/2432/commits/de3de131afe552e17cb9b38e9346a20dbb95c09c (@hppritcha)

Any clue?
> eb67c2f (@rhc54)

Does the "stil opal ignored" refers to that this code was not yet in effect at that date?
yes - and it still isn't

Ok, thanks, so that reduces it to one candidate then.

Now verifying that by cherry-picking the alps fix that came a couple of days later.
Are you using the DVM? Or are you trying to run natively on the Cray? If the DVM, then I don't see how de3de13 could have had an impact as we wouldn't be using the Cray pmix component.
Yes, this is about the dvm.
I suspect you are chasing a ghost, then, as that component cannot impact the DVM.
Hmmm, trying them both in isolation indeed doesn't allow me to recreate the problem.
So no ghosts ...

Then something must have survived in the build dir during the bisect, otherwise I can't really explain it.

Will dig further.
You might want to update and try this again - there was a commit early this morning that addressed memory leaks (44c1ff60f189842c1edfb1083d528d38be840303). I had to modify it slightly to fix the DVM (#2701)
Thanks, tried the latest, but didn't work.

The good news is that I found the offending commit (https://github.com/open-mpi/ompi/commit/dd491db21f06729eec1f73ac48048d3fed3dc44b), and this time I think it makes more sense.

The irony is that I upgraded my branch specifically because I needed that fix, as I didn't get any output of my jobs :-)
Didn't look yet what the exact issue is.
Huh - let me know what you find. I'm not sure I see how that causes leaked file descriptors, but there may be something subtle in there.
I didn't mean the change itself. But more that this enabled output again, which involves file descriptors at least.
It seems orted is not closing fd's correctly.

I'm running the following payload:
```
 sh -c 'lsof -p $(pidof orted)'
```

Look at the increase for consecutive jobs:
```
> wc -l */rank.0/stdout|sort -n -k2
   7957 total
    122 1/rank.0/stdout
    127 2/rank.0/stdout
    154 3/rank.0/stdout
    162 4/rank.0/stdout
    172 5/rank.0/stdout
    177 6/rank.0/stdout
    189 7/rank.0/stdout
    197 8/rank.0/stdout
    212 9/rank.0/stdout
    217 10/rank.0/stdout
    226 11/rank.0/stdout
    235 12/rank.0/stdout
    244 13/rank.0/stdout
    231 14/rank.0/stdout
      0 15/rank.0/stdout
    231 16/rank.0/stdout
    253 17/rank.0/stdout
    253 18/rank.0/stdout
    252 19/rank.0/stdout
    257 20/rank.0/stdout
    266 21/rank.0/stdout
    278 22/rank.0/stdout
    280 23/rank.0/stdout
    293 24/rank.0/stdout
    292 25/rank.0/stdout
    301 26/rank.0/stdout
    314 27/rank.0/stdout
    321 28/rank.0/stdout
    327 29/rank.0/stdout
    333 30/rank.0/stdout
    342 31/rank.0/stdout
    344 32/rank.0/stdout
    355 33/rank.0/stdout
```

And if I look at the output of lsof of the 33rd job:
```
> grep std 33/rank.0/stdout |awk '{print $9}'|sort|uniq -c
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/1/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/1/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/10/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/10/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/11/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/11/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/12/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/12/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/13/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/13/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/14/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/14/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/15/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/15/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/16/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/16/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/17/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/17/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/18/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/18/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/19/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/19/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/2/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/2/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/20/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/20/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/21/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/21/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/22/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/22/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/23/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/23/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/24/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/24/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/25/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/25/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/26/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/26/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/27/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/27/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/28/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/28/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/29/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/29/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/3/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/3/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/30/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/30/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/31/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/31/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/32/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/32/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/33/rank.0/stderr
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/33/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/4/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/4/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/5/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/5/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/6/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/6/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/7/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/7/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/8/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/8/rank.0/stdout
      3 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/9/rank.0/stderr
      2 /lustre/atlas1/bip103/scratch/marksant1/tmp/output/9/rank.0/stdout
```

You can see that it still has multiple handles per stdout and stderr of the finished tasks.

Do you agree that this looks fishy?
yeah, that isn't right - it looks like the IO channels aren't being released
@rhc54 @marksantcroos Does this only affect master, or does it also affect 2.0.x and/or 2.x?
@jsquyres Only master as far as I can tell.
@rhc54 I've tracked it down a bit further. Its the fd thats opened here that doesn't get closed: https://github.com/open-mpi/ompi/blob/master/orte/mca/iof/base/iof_base_setup.c#L314
@marksantcroos okay, thanks - I'll take a look at it this morning and chase it down
Thanks Ralph!

Some further hints, at https://github.com/open-mpi/ompi/blob/master/orte/mca/iof/orted/iof_orted.c#L274 the list of procs is already empty so the loop afterwards is never entered.
I think the (premature?) removal happens here: https://github.com/open-mpi/ompi/blob/master/orte/mca/iof/orted/iof_orted_read.c#L170
Hi @rhc54 fa419d3, doesn't fix it.
crud - ok, will take another shot at it
sorry about that - missed one spot 😦 
> sorry about that - missed one spot 😦

That sounds you have a handle on it :)
Ah, I missed that PR. I tried that, and still the same problem.
sigh - ok

@marksantcroos This latest PR should (hopefully) finally do the trick
Still no luck.
I'm afraid I'll have to let you take it from here, then - I'm seeing everything cleaned up on my end. It must be something associated with your particular use-case - some code path that I'm not hitting.
ACK. Will try to make a reproducer outside of Titan. Thanks for your efforts.
Would it by any chance be possible to have orte-dvm launch orted's while running on my laptop? That might ease the reproducing and/or debugging!
Maybe?? There is an MCA param to let you simulate larger clusters:

```
    orte_ras_base.multiplier = 1;
    mca_base_var_register("orte", "ras", "base", "multiplier",
                          "Simulate a larger cluster by launching N daemons/node",
                          MCA_BASE_VAR_TYPE_INT,
                          NULL, 0, 0,
                          OPAL_INFO_LVL_9,
                          MCA_BASE_VAR_SCOPE_READONLY, &orte_ras_base.multiplier);
```

I haven't tried it with the DVM, but you are welcome to do so

"There is an MCA parameter for that" ;-)
Ok, I can reproduce it easily on my laptop too:
```
netbook:leaks mark$ orte-dvm --report-uri dvm_uri --mca orte_ras_base_multiplier 2 &
[1] 37256
netbook:leaks mark$ DVM ready

netbook:leaks mark$ orterun --hnp file:dvm_uri -np 2 -N 1 --output-filename output:nocopy /bin/date
[ORTE] Task: 0 is launched! (Job ID: [44684,1])
[ORTE] Task: 0 returned: 0 (Job ID: [44684,1])
netbook:leaks mark$ lsof -p `pgrep orted`|grep stdout
orted   37262 mark   21u     REG                1,4        29 18240418 /Users/mark/proj/openmpi/leaks/output/1/rank.1/stdout
orted   37262 mark   25u     REG                1,4        29 18240418 /Users/mark/proj/openmpi/leaks/output/1/rank.1/stdout
```

one clear difference stands out - you are redirecting output to files. In orte_iof_base_setup, we open a sink for those files - best guess is that it isn't being cleaned up, or is overwriting an already-opened sink (which then is leaked).
Ah, sorry, I thought that was "clear" by my earlier comment https://github.com/open-mpi/ompi/issues/2691#issuecomment-272145085
@marksantcroos Trying it one more time with #2735 
File descriptor leakage is gone, but so is my output :)
```
export OMPI_MCA_orte_message_to_ralph="Jay, it works!"
```
LOL - hooray!!