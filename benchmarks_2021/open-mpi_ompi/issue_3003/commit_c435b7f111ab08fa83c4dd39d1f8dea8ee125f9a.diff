diff --git a/ompi/mpi/c/wtick.c b/ompi/mpi/c/wtick.c
index 2cb171c95c5..a246288e777 100644
--- a/ompi/mpi/c/wtick.c
+++ b/ompi/mpi/c/wtick.c
@@ -13,6 +13,8 @@
  * Copyright (c) 2015-2016 Research Organization for Information Science
  *                         and Technology (RIST). All rights reserved.
  * Copyright (c) 2017      IBM Corporation.  All rights reserved.
+ * Copyright (c) 2017      Los Alamos National Security, LLC. All rights
+ *                         reserved.
  * $COPYRIGHT$
  *
  * Additional copyrights may follow
@@ -25,6 +27,9 @@
 #include <sys/time.h>
 #endif
 #include <stdio.h>
+#ifdef HAVE_TIME_H
+#include <time.h>
+#endif
 
 #include MCA_timer_IMPLEMENTATION_HEADER
 #include "ompi/mpi/c/bindings.h"
@@ -43,8 +48,7 @@ double MPI_Wtick(void)
 
     /*
      * See https://github.com/open-mpi/ompi/issues/3003
-     * For now we are forcing the use of gettimeofday() until we find a
-     * more portable solution.
+     * to get an idea what's going on here.
      */
 #if 0
 #if OPAL_TIMER_CYCLE_NATIVE
@@ -60,8 +64,20 @@ double MPI_Wtick(void)
 #elif OPAL_TIMER_USEC_NATIVE
     return 0.000001;
 #endif
+#else
+#if defined(__linux__) && OPAL_HAVE_CLOCK_GETTIME
+    struct timespec spec;
+    double wtick = 0.0;
+    if (0 == clock_getres(CLOCK_MONOTONIC, &spec)){
+        wtick =  spec.tv_sec + spec.tv_nsec * 1.0e-09;
+    } else {
+        /* guess */
+        wtick = 1.0e-09;
+    }
+    return wtick;
 #else
     /* Otherwise, we already return usec precision. */
     return 0.000001;
 #endif
+#endif
 }
diff --git a/ompi/mpi/c/wtime.c b/ompi/mpi/c/wtime.c
index a8db80ce2de..2b72d27ba71 100644
--- a/ompi/mpi/c/wtime.c
+++ b/ompi/mpi/c/wtime.c
@@ -13,6 +13,8 @@
  * Copyright (c) 2015      Research Organization for Information Science
  *                         and Technology (RIST). All rights reserved.
  * Copyright (c) 2017      IBM Corporation.  All rights reserved.
+ * Copyright (c) 2017      Los Alamos National Security, LLC. All rights
+ *                         reserved.
  * $COPYRIGHT$
  *
  * Additional copyrights may follow
@@ -25,6 +27,9 @@
 #include <sys/time.h>
 #endif
 #include <stdio.h>
+#ifdef HAVE_TIME_H
+#include <time.h>
+#endif  /* HAVE_TIME_H */
 
 #include MCA_timer_IMPLEMENTATION_HEADER
 #include "ompi/mpi/c/bindings.h"
@@ -42,9 +47,8 @@ double MPI_Wtime(void)
     double wtime;
 
     /*
-     * See https://github.com/open-mpi/ompi/issues/3003
-     * For now we are forcing the use of gettimeofday() until we find a
-     * more portable solution.
+     * See https://github.com/open-mpi/ompi/issues/3003 to find out
+     * what's happening here.
      */
 #if 0
 #if OPAL_TIMER_CYCLE_NATIVE
@@ -52,12 +56,19 @@ double MPI_Wtime(void)
 #elif OPAL_TIMER_USEC_NATIVE
     wtime = ((double) opal_timer_base_get_usec()) / 1000000.0;
 #endif
+#else
+#if defined(__linux__) && OPAL_HAVE_CLOCK_GETTIME
+    struct timespec tp = {.tv_sec = 0, .tv_nsec = 0};
+    (void) clock_gettime(CLOCK_MONOTONIC, &tp);
+    wtime = tp.tv_sec;
+    wtime += tp.tv_nsec/1.0e+9;
 #else
     /* Fall back to gettimeofday() if we have nothing else */
     struct timeval tv;
     gettimeofday(&tv, NULL);
     wtime = tv.tv_sec;
     wtime += (double)tv.tv_usec / 1000000.0;
+#endif
 #endif
 
     OPAL_CR_NOOP_PROGRESS();
