diff --git a/ompi/mpi/c/wtick.c b/ompi/mpi/c/wtick.c
index 0a71efdd231..b6df0570f64 100644
--- a/ompi/mpi/c/wtick.c
+++ b/ompi/mpi/c/wtick.c
@@ -11,6 +11,8 @@
  *                         All rights reserved.
  * Copyright (c) 2007-2014 Cisco Systems, Inc.  All rights reserved.
  * Copyright (c) 2017      IBM Corporation.  All rights reserved.
+ * Copyright (c) 2017      Los Alamos National Security, LLC. All rights
+ *                         reserved.
  * $COPYRIGHT$
  * 
  * Additional copyrights may follow
@@ -23,6 +25,9 @@
 #include <sys/time.h>
 #endif
 #include <stdio.h>
+#ifdef HAVE_TIME_H
+#include <time.h>
+#endif
 
 #include MCA_timer_IMPLEMENTATION_HEADER
 #include "ompi/mpi/c/bindings.h"
@@ -43,8 +48,7 @@ double MPI_Wtick(void)
 
     /*
      * See https://github.com/open-mpi/ompi/issues/3003
-     * For now we are forcing the use of gettimeofday() until we find a
-     * more portable solution.
+     * to get an idea what's going on here.
      */
 #if 0
 #if OPAL_TIMER_USEC_NATIVE
@@ -57,8 +61,20 @@ double MPI_Wtick(void)
     }
     return (double)opal_timer_base_get_freq();
 #endif
+#else
+#if defined(__linux__) && OPAL_HAVE_CLOCK_GETTIME
+    struct timespec spec;
+    double wtick = 0.0;
+    if (0 == clock_getres(CLOCK_MONOTONIC, &spec)){
+        wtick =  spec.tv_sec + spec.tv_nsec * 1.0e-09;
+    } else {
+        /* guess */
+        wtick = 1.0e-09;
+    }
+    return wtick;
 #else
     /* Otherwise, we already return usec precision. */
     return 0.000001;
 #endif
+#endif
 }
diff --git a/ompi/mpi/c/wtime.c b/ompi/mpi/c/wtime.c
index 4d958724681..7bf1858aab7 100644
--- a/ompi/mpi/c/wtime.c
+++ b/ompi/mpi/c/wtime.c
@@ -11,6 +11,8 @@
  *                         All rights reserved.
  * Copyright (c) 2006-2014 Cisco Systems, Inc.  All rights reserved.
  * Copyright (c) 2017      IBM Corporation.  All rights reserved.
+ * Copyright (c) 2017      Los Alamos National Security, LLC. All rights
+ *                         reserved.
  * $COPYRIGHT$
  * 
  * Additional copyrights may follow
@@ -23,6 +25,9 @@
 #include <sys/time.h>
 #endif
 #include <stdio.h>
+#ifdef HAVE_TIME_H
+#include <time.h>
+#endif  /* HAVE_TIME_H */
 
 #include MCA_timer_IMPLEMENTATION_HEADER
 #include "ompi/mpi/c/bindings.h"
@@ -42,9 +47,8 @@ double MPI_Wtime(void)
     double wtime;
 
     /*
-     * See https://github.com/open-mpi/ompi/issues/3003
-     * For now we are forcing the use of gettimeofday() until we find a
-     * more portable solution.
+     * See https://github.com/open-mpi/ompi/issues/3003 to find out
+     * what's happening here.
      */
 #if 0
 #if OPAL_TIMER_USEC_NATIVE
@@ -55,12 +59,19 @@ double MPI_Wtime(void)
     wtime = ((double) opal_timer_base_get_cycles()) /
         ((double) opal_timer_base_get_freq());
 #endif
+#else
+#if defined(__linux__) && OPAL_HAVE_CLOCK_GETTIME
+    struct timespec tp = {.tv_sec = 0, .tv_nsec = 0};
+    (void) clock_gettime(CLOCK_MONOTONIC, &tp);
+    wtime = tp.tv_sec;
+    wtime += tp.tv_nsec/1.0e+9;
 #else
     /* Fall back to gettimeofday() if we have nothing else */
     struct timeval tv;
     gettimeofday(&tv, NULL);
     wtime = tv.tv_sec;
     wtime += (double)tv.tv_usec / 1000000.0;
+#endif
 #endif
 
     OPAL_CR_NOOP_PROGRESS();
