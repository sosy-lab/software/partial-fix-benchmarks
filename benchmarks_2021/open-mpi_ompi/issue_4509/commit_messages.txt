Merge pull request #4565 from benmenadue/master

Use malloc instead of posix_memalign for small (<= sizeof(void *)) alignments
rcache/grdma: try to prevent erroneous free error messages

It is possible to have parts of an in-use registered region be passed
to munmap or madvise. This does not necessarily mean the user has made
an error but does mean the entire region should be invalidated. This
commit checks that the munmap or madvise base matches the beginning of
the cached region. If it does and the region is in-use then we print
an error. There will certainly be false-negatives where a user
unmaps something that really is in-use but that is preferrable to a
false-positive.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: try to prevent erroneous free error messages

It is possible to have parts of an in-use registered region be passed
to munmap or madvise. This does not necessarily mean the user has made
an error but does mean the entire region should be invalidated. This
commit checks that the munmap or madvise base matches the beginning of
the cached region. If it does and the region is in-use then we print
an error. There will certainly be false-negatives where a user
unmaps something that really is in-use but that is preferrable to a
false-positive.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: try to prevent erroneous free error messages

It is possible to have parts of an in-use registered region be passed
to munmap or madvise. This does not necessarily mean the user has made
an error but does mean the entire region should be invalidated. This
commit checks that the munmap or madvise base matches the beginning of
the cached region. If it does and the region is in-use then we print
an error. There will certainly be false-negatives where a user
unmaps something that really is in-use but that is preferrable to a
false-positive.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit d3fa1bbbb0ecd3428e52300752ea6869a1ffac46)
Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: try to prevent erroneous free error messages

It is possible to have parts of an in-use registered region be passed
to munmap or madvise. This does not necessarily mean the user has made
an error but does mean the entire region should be invalidated. This
commit checks that the munmap or madvise base matches the beginning of
the cached region. If it does and the region is in-use then we print
an error. There will certainly be false-negatives where a user
unmaps something that really is in-use but that is preferrable to a
false-positive.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit d3fa1bbbb0ecd3428e52300752ea6869a1ffac46)
Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: try to prevent erroneous free error messages

It is possible to have parts of an in-use registered region be passed
to munmap or madvise. This does not necessarily mean the user has made
an error but does mean the entire region should be invalidated. This
commit checks that the munmap or madvise base matches the beginning of
the cached region. If it does and the region is in-use then we print
an error. There will certainly be false-negatives where a user
unmaps something that really is in-use but that is preferrable to a
false-positive.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit d3fa1bbbb0ecd3428e52300752ea6869a1ffac46)
Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: fix crash when part of a registration is unmapped

This commit fixes an issue when a registration is created for a large
region and then invalidated while part of it is in use.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: fix crash when part of a registration is unmapped

This commit fixes an issue when a registration is created for a large
region and then invalidated while part of it is in use.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: fix crash when part of a registration is unmapped

This commit fixes an issue when a registration is created for a large
region and then invalidated while part of it is in use.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit 39d598899bcd1dff2bbbe6f6eff9aa09ccb810b4)
Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: fix crash when part of a registration is unmapped

This commit fixes an issue when a registration is created for a large
region and then invalidated while part of it is in use.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit 39d598899bcd1dff2bbbe6f6eff9aa09ccb810b4)
Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
rcache/grdma: fix crash when part of a registration is unmapped

This commit fixes an issue when a registration is created for a large
region and then invalidated while part of it is in use.

References #4509

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit 39d598899bcd1dff2bbbe6f6eff9aa09ccb810b4)
Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
