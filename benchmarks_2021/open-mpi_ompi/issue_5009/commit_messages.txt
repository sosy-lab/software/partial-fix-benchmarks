Merge pull request #5053 from tkordenbrock/topic/master/btl-portals4.del_proc.fix

master: btl-portals4: don't free module resources when proc count goes to zero
io/romio314: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
io/romio314: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
io/romio314: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
io/romio314: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
io/romio321: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>

(back-ported from commit open-mpi/ompi@4d876ec6fedd2f5d299259eff53ba37b12c5d7e0)

Signed-off-by: Gilles Gouaillardet <gilles@rist.or.jp>
io/romio321: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>

(back-ported from commit open-mpi/ompi@4d876ec6fedd2f5d299259eff53ba37b12c5d7e0)

Signed-off-by: Gilles Gouaillardet <gilles@rist.or.jp>
io/romio314: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit 4d876ec6fedd2f5d299259eff53ba37b12c5d7e0)
io/romio314: fix minmax datatypes

romio assumes that all predefined datatypes are contiguous. Because of
the (terribly named) composed datatypes MPI_SHORT_INT, MPI_DOUBLE_INT,
MPI_LONG_INT, etc this is an incorrect assumption. The simplest way to
fix this is to override the MPI_Type_get_envelope and
MPI_Type_get_contents calls with calls that will work on these
datatypes. Note that not all calls to these MPI functions are
replaced, only the ones used when flattening a non-contiguous
datatype.

References #5009

Signed-off-by: Nathan Hjelm <hjelmn@lanl.gov>
(cherry picked from commit 4d876ec6fedd2f5d299259eff53ba37b12c5d7e0)
