diff --git a/ompi/datatype/ompi_datatype_create_contiguous.c b/ompi/datatype/ompi_datatype_create_contiguous.c
index fb44673ef5c..6a287caa41c 100644
--- a/ompi/datatype/ompi_datatype_create_contiguous.c
+++ b/ompi/datatype/ompi_datatype_create_contiguous.c
@@ -3,7 +3,7 @@
  * Copyright (c) 2004-2006 The Trustees of Indiana University and Indiana
  *                         University Research and Technology
  *                         Corporation.  All rights reserved.
- * Copyright (c) 2004-2013 The University of Tennessee and The University
+ * Copyright (c) 2004-2019 The University of Tennessee and The University
  *                         of Tennessee Research Foundation.  All rights
  *                         reserved.
  * Copyright (c) 2004-2006 High Performance Computing Center Stuttgart,
@@ -29,13 +29,12 @@ int32_t ompi_datatype_create_contiguous( int count, const ompi_datatype_t* oldTy
 {
     ompi_datatype_t* pdt;
 
-    if( 0 == count ) {
-        pdt = ompi_datatype_create( 0 );
-        ompi_datatype_add( pdt, &ompi_mpi_datatype_null.dt, 0, 0, 0 );
-    } else {
-        pdt = ompi_datatype_create( oldType->super.desc.used + 2 );
-        opal_datatype_add( &(pdt->super), &(oldType->super), count, 0, (oldType->super.ub - oldType->super.lb) );
+    if( (0 == count) || (0 == oldType->super.size) ) {
+        return ompi_datatype_duplicate( &ompi_mpi_datatype_null.dt, newType);
     }
+
+    pdt = ompi_datatype_create( oldType->super.desc.used + 2 );
+    opal_datatype_add( &(pdt->super), &(oldType->super), count, 0, (oldType->super.ub - oldType->super.lb) );
     *newType = pdt;
     return OMPI_SUCCESS;
 }
diff --git a/ompi/datatype/ompi_datatype_create_indexed.c b/ompi/datatype/ompi_datatype_create_indexed.c
index 457efb1e6ff..e72b41afc7d 100644
--- a/ompi/datatype/ompi_datatype_create_indexed.c
+++ b/ompi/datatype/ompi_datatype_create_indexed.c
@@ -3,7 +3,7 @@
  * Copyright (c) 2004-2006 The Trustees of Indiana University and Indiana
  *                         University Research and Technology
  *                         Corporation.  All rights reserved.
- * Copyright (c) 2004-2013 The University of Tennessee and The University
+ * Copyright (c) 2004-2019 The University of Tennessee and The University
  *                         of Tennessee Research Foundation.  All rights
  *                         reserved.
  * Copyright (c) 2004-2006 High Performance Computing Center Stuttgart,
@@ -34,24 +34,28 @@
 int32_t ompi_datatype_create_indexed( int count, const int* pBlockLength, const int* pDisp,
                                       const ompi_datatype_t* oldType, ompi_datatype_t** newType )
 {
-    ompi_datatype_t* pdt;
-    int i;
     ptrdiff_t extent, disp, endat;
+    ompi_datatype_t* pdt;
     size_t dLength;
+    int i;
 
-    if( 0 == count ) {
+    /* ignore all cases that lead to an empty type */
+    ompi_datatype_type_size(oldType, &dLength);
+    for( i = 0; (i < count) && (0 == pBlockLength[i]); i++ );  /* find first non zero */
+    if( (i == count) || (0 == dLength) ) {
         return ompi_datatype_duplicate( &ompi_mpi_datatype_null.dt, newType);
     }
 
-    disp = pDisp[0];
-    dLength = pBlockLength[0];
+    disp = pDisp[i];
+    dLength = pBlockLength[i];
     endat = disp + dLength;
     ompi_datatype_type_extent( oldType, &extent );
 
-    pdt = ompi_datatype_create( count * (2 + oldType->super.desc.used) );
-    for( i = 1; i < count; i++ ) {
-        if( endat == pDisp[i] ) {
-            /* contiguous with the previsious */
+    pdt = ompi_datatype_create( (count - i) * (2 + oldType->super.desc.used) );
+    for( i += 1; i < count; i++ ) {
+        if( 0 == pBlockLength[i] )  /* ignore empty length */
+            continue;
+        if( endat == pDisp[i] ) { /* contiguous with the previsious */
             dLength += pBlockLength[i];
             endat += pBlockLength[i];
         } else {
@@ -71,26 +75,28 @@ int32_t ompi_datatype_create_indexed( int count, const int* pBlockLength, const
 int32_t ompi_datatype_create_hindexed( int count, const int* pBlockLength, const ptrdiff_t* pDisp,
                                        const ompi_datatype_t* oldType, ompi_datatype_t** newType )
 {
-    ompi_datatype_t* pdt;
-    int i;
     ptrdiff_t extent, disp, endat;
+    ompi_datatype_t* pdt;
     size_t dLength;
+    int i;
 
-    if( 0 == count ) {
-        *newType = ompi_datatype_create( 0 );
-        ompi_datatype_add( *newType, &ompi_mpi_datatype_null.dt, 0, 0, 0);
-        return OMPI_SUCCESS;
+    /* ignore all cases that lead to an empty type */
+    ompi_datatype_type_size(oldType, &dLength);
+    for( i = 0; (i < count) && (0 == pBlockLength[i]); i++ );  /* find first non zero */
+    if( (i == count) || (0 == dLength) ) {
+        return ompi_datatype_duplicate( &ompi_mpi_datatype_null.dt, newType);
     }
 
-    ompi_datatype_type_extent( oldType, &extent );
-    pdt = ompi_datatype_create( count * (2 + oldType->super.desc.used) );
-    disp = pDisp[0];
-    dLength = pBlockLength[0];
+    disp = pDisp[i];
+    dLength = pBlockLength[i];
     endat = disp + dLength * extent;
+    ompi_datatype_type_extent( oldType, &extent );
 
-    for( i = 1; i < count; i++ ) {
-        if( endat == pDisp[i] ) {
-            /* contiguous with the previsious */
+    pdt = ompi_datatype_create( (count - i) * (2 + oldType->super.desc.used) );
+    for( i += 1; i < count; i++ ) {
+        if( 0 == pBlockLength[i] )  /* ignore empty length */
+            continue;
+        if( endat == pDisp[i] ) { /* contiguous with the previsious */
             dLength += pBlockLength[i];
             endat += pBlockLength[i] * extent;
         } else {
@@ -110,21 +116,15 @@ int32_t ompi_datatype_create_hindexed( int count, const int* pBlockLength, const
 int32_t ompi_datatype_create_indexed_block( int count, int bLength, const int* pDisp,
                                             const ompi_datatype_t* oldType, ompi_datatype_t** newType )
 {
-    ompi_datatype_t* pdt;
-    int i;
     ptrdiff_t extent, disp, endat;
+    ompi_datatype_t* pdt;
     size_t dLength;
+    int i;
 
-    ompi_datatype_type_extent( oldType, &extent );
     if( (count == 0) || (bLength == 0) ) {
-        if( 0 == count ) {
-            return ompi_datatype_duplicate(&ompi_mpi_datatype_null.dt, newType);
-        } else {
-            *newType = ompi_datatype_create(1);
-            ompi_datatype_add( *newType, oldType, 0, pDisp[0] * extent, extent );
-            return OMPI_SUCCESS;
-        }
+        return ompi_datatype_duplicate(&ompi_mpi_datatype_null.dt, newType);
     }
+    ompi_datatype_type_extent( oldType, &extent );
     pdt = ompi_datatype_create( count * (2 + oldType->super.desc.used) );
     disp = pDisp[0];
     dLength = bLength;
@@ -150,20 +150,15 @@ int32_t ompi_datatype_create_indexed_block( int count, int bLength, const int* p
 int32_t ompi_datatype_create_hindexed_block( int count, int bLength, const ptrdiff_t* pDisp,
                                              const ompi_datatype_t* oldType, ompi_datatype_t** newType )
 {
-    ompi_datatype_t* pdt;
-    int i;
     ptrdiff_t extent, disp, endat;
+    ompi_datatype_t* pdt;
     size_t dLength;
+    int i;
 
-    ompi_datatype_type_extent( oldType, &extent );
     if( (count == 0) || (bLength == 0) ) {
-        *newType = ompi_datatype_create(1);
-        if( 0 == count )
-            ompi_datatype_add( *newType, &ompi_mpi_datatype_null.dt, 0, 0, 0 );
-        else
-            ompi_datatype_add( *newType, oldType, 0, pDisp[0] * extent, extent );
-        return OMPI_SUCCESS;
+        return ompi_datatype_duplicate(&ompi_mpi_datatype_null.dt, newType);
     }
+    ompi_datatype_type_extent( oldType, &extent );
     pdt = ompi_datatype_create( count * (2 + oldType->super.desc.used) );
     disp = pDisp[0];
     dLength = bLength;
diff --git a/ompi/datatype/ompi_datatype_create_struct.c b/ompi/datatype/ompi_datatype_create_struct.c
index 98daa8bacbb..9c78f53fee3 100644
--- a/ompi/datatype/ompi_datatype_create_struct.c
+++ b/ompi/datatype/ompi_datatype_create_struct.c
@@ -3,7 +3,7 @@
  * Copyright (c) 2004-2006 The Trustees of Indiana University and Indiana
  *                         University Research and Technology
  *                         Corporation.  All rights reserved.
- * Copyright (c) 2004-2013 The University of Tennessee and The University
+ * Copyright (c) 2004-2019 The University of Tennessee and The University
  *                         of Tennessee Research Foundation.  All rights
  *                         reserved.
  * Copyright (c) 2004-2006 High Performance Computing Center Stuttgart,
@@ -31,27 +31,27 @@
 int32_t ompi_datatype_create_struct( int count, const int* pBlockLength, const ptrdiff_t* pDisp,
                                      ompi_datatype_t* const * pTypes, ompi_datatype_t** newType )
 {
-    int i;
     ptrdiff_t disp = 0, endto, lastExtent, lastDisp;
-    int lastBlock;
     ompi_datatype_t *pdt, *lastType;
+    int lastBlock;
+    int i, start_from;
 
-    if( 0 == count ) {
-        *newType = ompi_datatype_create( 0 );
-        ompi_datatype_add( *newType, &ompi_mpi_datatype_null.dt, 0, 0, 0);
-        return OMPI_SUCCESS;
+    /* Find first non-zero length element */
+    for( i = 0; (i < count) && (0 == pBlockLength[i]); i++ );
+    if( i == count ) {  /* either nothing or nothing relevant */
+        return ompi_datatype_duplicate( &ompi_mpi_datatype_null.dt, newType);
     }
-
-    /* if we compute the total number of elements before we can
+    /* compute the total number of elements before we can
      * avoid increasing the size of the desc array often.
      */
-    lastType = (ompi_datatype_t*)pTypes[0];
-    lastBlock = pBlockLength[0];
+    start_from = i;
+    lastType = (ompi_datatype_t*)pTypes[start_from];
+    lastBlock = pBlockLength[start_from];
     lastExtent = lastType->super.ub - lastType->super.lb;
-    lastDisp = pDisp[0];
-    endto = pDisp[0] + lastExtent * lastBlock;
+    lastDisp = pDisp[start_from];
+    endto = pDisp[start_from] + lastExtent * lastBlock;
 
-    for( i = 1; i < count; i++ ) {
+    for( i = (start_from + 1); i < count; i++ ) {
         if( (pTypes[i] == lastType) && (pDisp[i] == endto) ) {
             lastBlock += pBlockLength[i];
             endto = lastDisp + lastBlock * lastExtent;
@@ -68,16 +68,16 @@ int32_t ompi_datatype_create_struct( int count, const int* pBlockLength, const p
     disp += lastType->super.desc.used;
     if( lastBlock != 1 ) disp += 2;
 
-    lastType = (ompi_datatype_t*)pTypes[0];
-    lastBlock = pBlockLength[0];
+    lastType = (ompi_datatype_t*)pTypes[start_from];
+    lastBlock = pBlockLength[start_from];
     lastExtent = lastType->super.ub - lastType->super.lb;
-    lastDisp = pDisp[0];
-    endto = pDisp[0] + lastExtent * lastBlock;
+    lastDisp = pDisp[start_from];
+    endto = pDisp[start_from] + lastExtent * lastBlock;
 
     pdt = ompi_datatype_create( (int32_t)disp );
 
     /* Do again the same loop but now add the elements */
-    for( i = 1; i < count; i++ ) {
+    for( i = (start_from + 1); i < count; i++ ) {
         if( (pTypes[i] == lastType) && (pDisp[i] == endto) ) {
             lastBlock += pBlockLength[i];
             endto = lastDisp + lastBlock * lastExtent;
diff --git a/ompi/datatype/ompi_datatype_create_vector.c b/ompi/datatype/ompi_datatype_create_vector.c
index 1de8df4d2d2..c4829a4b54c 100644
--- a/ompi/datatype/ompi_datatype_create_vector.c
+++ b/ompi/datatype/ompi_datatype_create_vector.c
@@ -3,7 +3,7 @@
  * Copyright (c) 2004-2006 The Trustees of Indiana University and Indiana
  *                         University Research and Technology
  *                         Corporation.  All rights reserved.
- * Copyright (c) 2004-2013 The University of Tennessee and The University
+ * Copyright (c) 2004-2019 The University of Tennessee and The University
  *                         of Tennessee Research Foundation.  All rights
  *                         reserved.
  * Copyright (c) 2004-2006 High Performance Computing Center Stuttgart,
@@ -28,23 +28,14 @@
 
 #include "ompi/datatype/ompi_datatype.h"
 
-/* Open questions ...
- *  - how to improuve the handling of these vectors (creating a temporary datatype
- *    can be ONLY a initial solution.
- *
- */
-
 int32_t ompi_datatype_create_vector( int count, int bLength, int stride,
                                      const ompi_datatype_t* oldType, ompi_datatype_t** newType )
 {
     ompi_datatype_t *pTempData, *pData;
     ptrdiff_t extent = oldType->super.ub - oldType->super.lb;
 
-
-    if( 0 == count ) {
-        *newType = ompi_datatype_create( 0 );
-        ompi_datatype_add( *newType, &ompi_mpi_datatype_null.dt, 0, 0, 0);
-        return OMPI_SUCCESS;
+    if( (0 == count) || (0 == bLength) ) {
+        return ompi_datatype_duplicate( &ompi_mpi_datatype_null.dt, newType);
     }
 
     pData = ompi_datatype_create( oldType->super.desc.used + 2 );
@@ -72,10 +63,8 @@ int32_t ompi_datatype_create_hvector( int count, int bLength, ptrdiff_t stride,
     ompi_datatype_t *pTempData, *pData;
     ptrdiff_t extent = oldType->super.ub - oldType->super.lb;
 
-    if( 0 == count ) {
-        *newType = ompi_datatype_create( 0 );
-        ompi_datatype_add( *newType, &ompi_mpi_datatype_null.dt, 0, 0, 0);
-        return OMPI_SUCCESS;
+    if( (0 == count) || (0 == bLength) ) {
+        return ompi_datatype_duplicate( &ompi_mpi_datatype_null.dt, newType);
     }
 
     pTempData = ompi_datatype_create( oldType->super.desc.used + 2 );
