Enable or disable each algorithm (#158)

* Enable or disable each algorithm

* Enable disable tests in travis and silencing warnings
Updated Visual Studio solution to 2017 version, fixed build warnings (issue #159), added signature and Picnic support.
Updated Windows build (added sig, fixed warnings, 2017 update) (#169)

* Updated Visual Studio solution to 2017 version, fixed build warnings (issue #159), added signature and Picnic support.

* commit

treat warnings as errors

* Fix ENABLED_PICNIC macro in sig_picnic.

* Prettyprint

* Try to fix Travis error or macOS.
