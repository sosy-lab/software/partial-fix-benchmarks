Remove kex_ntru from master
~~PR here: https://github.com/open-quantum-safe/liboqs/pull/377~~
See https://github.com/open-quantum-safe/liboqs/pull/390 instead :-)
Resolved by #402.