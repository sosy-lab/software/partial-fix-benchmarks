Update server.c

removed instant test setup from server.c related to #265
appveyor ci support, fixes #280
appveyor ci support, fixes #280
appveyor ci support, fixes #280
Squashed commit of the following:

commit f2f5e97f913a2944c2ca0b699939c785769b1f8d
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 16:46:31 2015 +0200

    remove small memleaks during startup

commit 23fc4a0c6b2e2cdfbfab8c71546030ff05554065
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 16:21:21 2015 +0200

    clean up the branch

commit cb1cead03ca5c3cf3d26ec8ac5418d0c5f3921b9
Merge: a3ad1e8 60a46e6
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 16:19:14 2015 +0200

    Merge branch 'master' into service_call_interface

    Conflicts:
    	tools/pyUANamespace/ua_node_types.py

commit a3ad1e845b539a7bed64358b057a54fe7acb3e75
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 16:14:00 2015 +0200

    simplify methods API on the client side

commit 60a46e6abf1a46d8b39fa302d8c5aa825da045a9
Merge: 232bb4e 028ed26
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jul 7 15:17:08 2015 +0200

    Merge branch 'master' of https://github.com/acplt/open62541

commit 232bb4ecf76efdc18e9b23d420d48b75ecb6530a
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jul 7 15:16:43 2015 +0200

    Backported (parts of) 1b6331b6333a49286e273a49286e27 from service_call_implementation: Fix duplicate reference from parent to child being created by namespace generator.

commit 028ed266a207950d2ec9d8dac3ad85925e381b79
Author: ChristianFimmers <christian.fimmers@rwth-aachen.de>
Date:   Mon Jul 6 10:39:49 2015 +0200

    Merge pull request #290

commit 6b2e0c6c36ba6304a340eef0c212d7bf78f67b60
Author: Stasik0 <github@stasik.com>
Date:   Mon Jul 6 10:39:49 2015 +0200

    one more space removed from the tag, relates to #289

commit fd71ca7fa29c41a1a4f72ffe35005b9661a31403
Author: Stasik0 <github@stasik.com>
Date:   Mon Jul 6 10:38:27 2015 +0200

    removing non-ascii literal, fixing amalgamation tag in case no git found, relates to #289

commit 0b9b7095cafca2ff96bbab379395c9cc9e953240
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Fri Jul 3 17:54:43 2015 +0200

    no compiler flag required to compile amalgamated server

commit 4d5ddc81f891c276887218227f97a7d7e64876fe
Merge: 66caa20 6ff67cf
Author: Sten Grüner <Stasik0@users.noreply.github.com>
Date:   Thu Jul 2 09:10:57 2015 +0200

    Merge pull request #287 from acplt/simple_tcp

    simplfy closing connections in the network layer

commit 6ff67cf40c7b4560136f0ab295743f83d3cb359c
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Wed Jul 1 20:48:51 2015 +0200

    simplfy closing connections in the network layer

    sockets are only closed in the main loop. if a worker wants to close a connection, it is shutdown.
    this is then picked up by the select-call in the next main loop iteration.
    when a connection is closed, two jobs are returned. one to immediately detach the securechannel and a delayed job that eventually frees the connection memory.

commit 66caa20e93169ae4daaeac752ba62b45517b5018
Author: Stasik0 <github@stasik.com>
Date:   Wed Jul 1 08:39:19 2015 +0200

    fixing client blocking on server disonnect

commit 54cb2b4a7dcc6fdb3fb65b0497da276919f5fe79
Merge: 50512f4 435eaf9
Author: Julius Pfrommer <jpfr@users.noreply.github.com>
Date:   Tue Jun 30 21:45:42 2015 +0200

    Merge pull request #286 from acplt/externalNodestoreLeakFix

    fixed a memleak when using external namespaces

commit 435eaf918d2f5532a255c6f4062ab1e736698401
Author: LEvertz <l.evertz@plt.rwth-aachen.de>
Date:   Tue Jun 30 17:04:59 2015 +0200

    added a function to delete external namespaces to the server --> fixes a
    memleak when using external namespaces

commit 50512f48fe6679f615e806fe4d39af9b148ce6ff
Author: Stasik0 <github@stasik.com>
Date:   Mon Jun 29 09:55:52 2015 +0200

    added MSVC-builds to the readme

commit a06f3ed9af3098690f3079ea80642c3f1287ecd9
Author: FlorianPalm <palm@plt.rwth-aachen.de>
Date:   Sun Jun 28 23:28:56 2015 +0200

    fixed a typo in README

commit cee54343c09d99d4ddfcb6a106867d96f597b4f3
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sun Jun 28 17:53:53 2015 +0200

    restore calcSizeBinary as a single function UA_calcSizeBinary

commit 462b813509806d2ffb606944874ccfbe213fb43b
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sun Jun 28 15:25:31 2015 +0200

    replace redundant functions with ifdef, some cosmetic cleanup

commit 1652c19b8bb7ceba0a11c34ed933db2d4052c3a4
Author: Julius Pfrommer <jpfr@users.noreply.github.com>
Date:   Sun Jun 28 11:06:24 2015 +0200

    wrap coveralls. the service fails regularly

commit 88cc9a3da2c5043d7a898c5b0577890e2235a584
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sun Jun 28 09:24:05 2015 +0200

    small documentation improvements

commit 0f60259a7a9bfff0f9a603a2125b7d490631f211
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sat Jun 27 23:42:40 2015 +0200

    install sphinx theme

commit c2f87ed825265e9bc4442413168103c4ec2cb06f
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sat Jun 27 23:26:26 2015 +0200

    add documentation generation with sphinx

commit bc5151e65d34d25f04ca1fe75aeabf4e1f6e141c
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sat Jun 27 17:43:42 2015 +0200

    improve documentation, get rid of a duplicate function for string comparison

commit 5caa99787017dac826914c4d37954a73ba9e5e76
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sat Jun 27 15:38:51 2015 +0200

    always copy data from the data source (removing the release function pointer)

commit d0e09b24ffed3a9cbff9d34042dbdc4cd194fb9d
Author: Stasik0 <github@stasik.com>
Date:   Sat Jun 27 15:32:48 2015 +0200

    removing enforcing NameSpaceId in BrowseName, relates to #284

commit a8bbad7e0adbfde590a41e6fbf9abe821eb56fab
Author: Stasik0 <github@stasik.com>
Date:   Sat Jun 27 11:53:11 2015 +0200

    64bit support + artifacts

commit d4c98b6c4e36f8e00a96e9bf6e4650fff1d66de5
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sat Jun 27 13:14:37 2015 +0200

    fix client amalgamation

commit 6b52e3c5dc30c2b78fb5ab1b2a6feff80be3849c
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Sat Jun 27 11:58:17 2015 +0200

    add a define for multithreading

commit df4753b00d74b21f8a76e644fc63d9e5d1564206
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Fri Jun 26 21:48:49 2015 +0200

    improve amalgamation

commit 2c869f59ab9580477cf84bfffc95be9dc648858a
Author: Stasik0 <github@stasik.com>
Date:   Fri Jun 26 11:41:58 2015 +0200

    appveyor ci support, fixes #280

commit de5cb7d32b21207534db86e156369d069b7c4cfd
Author: FlorianPalm <palm@plt.rwth-aachen.de>
Date:   Wed Jun 24 20:57:05 2015 +0200

    Update server.c

    removed instant test setup from server.c related to #265

commit 1adbb44483473da7438605e91fef657593a3ee91
Merge: 0632a83 494ac62
Author: FlorianPalm <palm@plt.rwth-aachen.de>
Date:   Wed Jun 24 18:00:18 2015 +0200

    Merge branch 'master' of https://github.com/acplt/open62541

commit 0632a8363b6364f011ce1366b1f9085046e1f97b
Author: FlorianPalm <palm@plt.rwth-aachen.de>
Date:   Wed Jun 24 17:59:02 2015 +0200

    related to #265, added another define as well as "faster" encoding functions for ARM7TDMI with "mixed" endian

commit 494ac6234ecb19395b9edde89acb60d33f232ef6
Author: Stasik0 <github@stasik.com>
Date:   Wed Jun 24 17:25:44 2015 +0200

    typo

commit 263644e8fbc6161fc866ef2b293e49ab2e23bca8
Author: Stasik0 <github@stasik.com>
Date:   Wed Jun 24 17:23:51 2015 +0200

    partial revert of CMake changes

commit 6f95b1146d2a3e1138a397a6c6713853980ba5e4
Author: Stasik0 <github@stasik.com>
Date:   Wed Jun 24 17:18:25 2015 +0200

    fixing compile dependencies

commit b3dc6cff718c814eb63adff1756b499c24a9cb53
Author: Stasik0 <github@stasik.com>
Date:   Wed Jun 24 16:57:44 2015 +0200

    masking out non-windows parts of the example - get rid of F_OK completely

commit f23534441816cacc08479b392209f218d006a8cc
Author: Stasik0 <github@stasik.com>
Date:   Wed Jun 24 16:42:29 2015 +0200

    adding dependencies in CMake

commit b2277cfc2bec6ec58bc953efda6a981c91b2a97f
Author: Sten Grüner <Stasik0@users.noreply.github.com>
Date:   Wed Jun 24 16:10:07 2015 +0200

    F_OK is now direclty defined in server.c
Merge pull request #291 from acplt/service_call_implementation

commit efd4337064bab252472de657b01eded8309b25a0
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 18:41:40 2015 +0200

    checking for empty call requests and cosmetic improvements

commit e9efbb784d9fcde3dfb6ec7fb35f718c3619f5ea
Merge: 9c55fe6 60a46e6
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 17:13:22 2015 +0200

    Merge branch 'master' into service_call_implementation

commit 9c55fe640d837b4cbe7941b999584d3f56879e07
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 17:12:41 2015 +0200

    Squashed commit of the following:

    commit f2f5e97f913a2944c2ca0b699939c785769b1f8d
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Tue Jul 7 16:46:31 2015 +0200

        remove small memleaks during startup

    commit 23fc4a0c6b2e2cdfbfab8c71546030ff05554065
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Tue Jul 7 16:21:21 2015 +0200

        clean up the branch

    commit cb1cead03ca5c3cf3d26ec8ac5418d0c5f3921b9
    Merge: a3ad1e8 60a46e6
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Tue Jul 7 16:19:14 2015 +0200

        Merge branch 'master' into service_call_interface

        Conflicts:
        	tools/pyUANamespace/ua_node_types.py

    commit a3ad1e845b539a7bed64358b057a54fe7acb3e75
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Tue Jul 7 16:14:00 2015 +0200

        simplify methods API on the client side

    commit 60a46e6abf1a46d8b39fa302d8c5aa825da045a9
    Merge: 232bb4e 028ed26
    Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
    Date:   Tue Jul 7 15:17:08 2015 +0200

        Merge branch 'master' of https://github.com/acplt/open62541

    commit 232bb4ecf76efdc18e9b23d420d48b75ecb6530a
    Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
    Date:   Tue Jul 7 15:16:43 2015 +0200

        Backported (parts of) 1b6331b6333a49286e273a49286e27 from service_call_implementation: Fix duplicate reference from parent to child being created by namespace generator.

    commit 028ed266a207950d2ec9d8dac3ad85925e381b79
    Author: ChristianFimmers <christian.fimmers@rwth-aachen.de>
    Date:   Mon Jul 6 10:39:49 2015 +0200

        Merge pull request #290

    commit 6b2e0c6c36ba6304a340eef0c212d7bf78f67b60
    Author: Stasik0 <github@stasik.com>
    Date:   Mon Jul 6 10:39:49 2015 +0200

        one more space removed from the tag, relates to #289

    commit fd71ca7fa29c41a1a4f72ffe35005b9661a31403
    Author: Stasik0 <github@stasik.com>
    Date:   Mon Jul 6 10:38:27 2015 +0200

        removing non-ascii literal, fixing amalgamation tag in case no git found, relates to #289

    commit 0b9b7095cafca2ff96bbab379395c9cc9e953240
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Fri Jul 3 17:54:43 2015 +0200

        no compiler flag required to compile amalgamated server

    commit 4d5ddc81f891c276887218227f97a7d7e64876fe
    Merge: 66caa20 6ff67cf
    Author: Sten Grüner <Stasik0@users.noreply.github.com>
    Date:   Thu Jul 2 09:10:57 2015 +0200

        Merge pull request #287 from acplt/simple_tcp

        simplfy closing connections in the network layer

    commit 6ff67cf40c7b4560136f0ab295743f83d3cb359c
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Wed Jul 1 20:48:51 2015 +0200

        simplfy closing connections in the network layer

        sockets are only closed in the main loop. if a worker wants to close a connection, it is shutdown.
        this is then picked up by the select-call in the next main loop iteration.
        when a connection is closed, two jobs are returned. one to immediately detach the securechannel and a delayed job that eventually frees the connection memory.

    commit 66caa20e93169ae4daaeac752ba62b45517b5018
    Author: Stasik0 <github@stasik.com>
    Date:   Wed Jul 1 08:39:19 2015 +0200

        fixing client blocking on server disonnect

    commit 54cb2b4a7dcc6fdb3fb65b0497da276919f5fe79
    Merge: 50512f4 435eaf9
    Author: Julius Pfrommer <jpfr@users.noreply.github.com>
    Date:   Tue Jun 30 21:45:42 2015 +0200

        Merge pull request #286 from acplt/externalNodestoreLeakFix

        fixed a memleak when using external namespaces

    commit 435eaf918d2f5532a255c6f4062ab1e736698401
    Author: LEvertz <l.evertz@plt.rwth-aachen.de>
    Date:   Tue Jun 30 17:04:59 2015 +0200

        added a function to delete external namespaces to the server --> fixes a
        memleak when using external namespaces

    commit 50512f48fe6679f615e806fe4d39af9b148ce6ff
    Author: Stasik0 <github@stasik.com>
    Date:   Mon Jun 29 09:55:52 2015 +0200

        added MSVC-builds to the readme

    commit a06f3ed9af3098690f3079ea80642c3f1287ecd9
    Author: FlorianPalm <palm@plt.rwth-aachen.de>
    Date:   Sun Jun 28 23:28:56 2015 +0200

        fixed a typo in README

    commit cee54343c09d99d4ddfcb6a106867d96f597b4f3
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sun Jun 28 17:53:53 2015 +0200

        restore calcSizeBinary as a single function UA_calcSizeBinary

    commit 462b813509806d2ffb606944874ccfbe213fb43b
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sun Jun 28 15:25:31 2015 +0200

        replace redundant functions with ifdef, some cosmetic cleanup

    commit 1652c19b8bb7ceba0a11c34ed933db2d4052c3a4
    Author: Julius Pfrommer <jpfr@users.noreply.github.com>
    Date:   Sun Jun 28 11:06:24 2015 +0200

        wrap coveralls. the service fails regularly

    commit 88cc9a3da2c5043d7a898c5b0577890e2235a584
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sun Jun 28 09:24:05 2015 +0200

        small documentation improvements

    commit 0f60259a7a9bfff0f9a603a2125b7d490631f211
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sat Jun 27 23:42:40 2015 +0200

        install sphinx theme

    commit c2f87ed825265e9bc4442413168103c4ec2cb06f
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sat Jun 27 23:26:26 2015 +0200

        add documentation generation with sphinx

    commit bc5151e65d34d25f04ca1fe75aeabf4e1f6e141c
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sat Jun 27 17:43:42 2015 +0200

        improve documentation, get rid of a duplicate function for string comparison

    commit 5caa99787017dac826914c4d37954a73ba9e5e76
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sat Jun 27 15:38:51 2015 +0200

        always copy data from the data source (removing the release function pointer)

    commit d0e09b24ffed3a9cbff9d34042dbdc4cd194fb9d
    Author: Stasik0 <github@stasik.com>
    Date:   Sat Jun 27 15:32:48 2015 +0200

        removing enforcing NameSpaceId in BrowseName, relates to #284

    commit a8bbad7e0adbfde590a41e6fbf9abe821eb56fab
    Author: Stasik0 <github@stasik.com>
    Date:   Sat Jun 27 11:53:11 2015 +0200

        64bit support + artifacts

    commit d4c98b6c4e36f8e00a96e9bf6e4650fff1d66de5
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sat Jun 27 13:14:37 2015 +0200

        fix client amalgamation

    commit 6b52e3c5dc30c2b78fb5ab1b2a6feff80be3849c
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Sat Jun 27 11:58:17 2015 +0200

        add a define for multithreading

    commit df4753b00d74b21f8a76e644fc63d9e5d1564206
    Author: Julius Pfrommer <julius.pfrommer@web.de>
    Date:   Fri Jun 26 21:48:49 2015 +0200

        improve amalgamation

    commit 2c869f59ab9580477cf84bfffc95be9dc648858a
    Author: Stasik0 <github@stasik.com>
    Date:   Fri Jun 26 11:41:58 2015 +0200

        appveyor ci support, fixes #280

    commit de5cb7d32b21207534db86e156369d069b7c4cfd
    Author: FlorianPalm <palm@plt.rwth-aachen.de>
    Date:   Wed Jun 24 20:57:05 2015 +0200

        Update server.c

        removed instant test setup from server.c related to #265

    commit 1adbb44483473da7438605e91fef657593a3ee91
    Merge: 0632a83 494ac62
    Author: FlorianPalm <palm@plt.rwth-aachen.de>
    Date:   Wed Jun 24 18:00:18 2015 +0200

        Merge branch 'master' of https://github.com/acplt/open62541

    commit 0632a8363b6364f011ce1366b1f9085046e1f97b
    Author: FlorianPalm <palm@plt.rwth-aachen.de>
    Date:   Wed Jun 24 17:59:02 2015 +0200

        related to #265, added another define as well as "faster" encoding functions for ARM7TDMI with "mixed" endian

    commit 494ac6234ecb19395b9edde89acb60d33f232ef6
    Author: Stasik0 <github@stasik.com>
    Date:   Wed Jun 24 17:25:44 2015 +0200

        typo

    commit 263644e8fbc6161fc866ef2b293e49ab2e23bca8
    Author: Stasik0 <github@stasik.com>
    Date:   Wed Jun 24 17:23:51 2015 +0200

        partial revert of CMake changes

    commit 6f95b1146d2a3e1138a397a6c6713853980ba5e4
    Author: Stasik0 <github@stasik.com>
    Date:   Wed Jun 24 17:18:25 2015 +0200

        fixing compile dependencies

    commit b3dc6cff718c814eb63adff1756b499c24a9cb53
    Author: Stasik0 <github@stasik.com>
    Date:   Wed Jun 24 16:57:44 2015 +0200

        masking out non-windows parts of the example - get rid of F_OK completely

    commit f23534441816cacc08479b392209f218d006a8cc
    Author: Stasik0 <github@stasik.com>
    Date:   Wed Jun 24 16:42:29 2015 +0200

        adding dependencies in CMake

    commit b2277cfc2bec6ec58bc953efda6a981c91b2a97f
    Author: Sten Grüner <Stasik0@users.noreply.github.com>
    Date:   Wed Jun 24 16:10:07 2015 +0200

        F_OK is now direclty defined in server.c

commit 203ef3503807776015d4db8d257e63dc1b948f29
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jul 7 14:53:35 2015 +0200

    fix two memory bugs

commit 7791f6e455a348a9b9acd9ae76b7776364fffeee
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Thu Jul 2 12:07:36 2015 +0200

    remove private headers from the example server. simplify the callback signature

commit 5a623caab225aa6e86de76341ef9973d65d9ef82
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jun 30 17:42:52 2015 +0200

    In/Out Variable nodes now get a random ID on creation (thank to @Stasik0 for the patch in master)

commit d853a343924a153f05b6bcb16abb959a47e5dd09
Merge: f7f9ed3 67840c9
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jun 30 17:39:17 2015 +0200

    Merge branch 'master' into service_call_implementation

    Conflicts:
    	examples/client.c
    	include/ua_client.h
    	src/client/ua_client.c
    	src/server/ua_server_binary.c

commit f7f9ed359f43f9a06c10a7b717441fd2e156c75c
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jun 30 17:33:41 2015 +0200

    Service_Call now checks whether Arguments encoded in the InputArguments variable of a method is an extensionObject or an Argument and picks the appropriate strategy (ExtensionObjects should ultimatively be removed once the namespace compilers find out how).

commit 3a5f33b1537df99e1ef0e33c8f81aa17b77a1e4a
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jun 30 17:12:23 2015 +0200

    Recreated UA_Server_addMethodNode for userspace operations. FIXME: Cannot be called, service_call needs to evade extensionObject decoding when arguments are not stored as extensionObjects.

commit 33dd54bf091f8c5b626b0b16d1b6333a49286e27
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Mon Jun 29 19:33:06 2015 +0200

    Renamed serviceCall headers into more appropriate form (still holding I/O data for the actual function call). Service call now checks the structure of passed arguments; return argument checking is still on the TODO list.

commit c93008c60603306fc1f0e15cb6d645699f5b543c
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Mon Jun 29 16:06:25 2015 +0200

    Python compiler can now compile flat extension objects (i.e. extObj's that don't contain other extObj's) directly from XML. This allows for any Arguments to be shown when calling methods. FIXME: There are preparations in the extObj printCode function that will allow hierarchic encodings of extObjs. Note also that identifying arrays inside extObj structs is not extensively tested.

commit f0fc16c5697f89d9c33da42e7f7de12e73bc67ab
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 25 21:03:39 2015 +0200

    Modifications in namespace compiler now allow basic scalar extensionObjects to be read and enconded from XML files. WARNING: ExtensionObjects containing ExtensionObjects and arrays > 1 element will propably fail to encode.

commit d78aab3cd29dea1115600a07271d5b9896c219f7
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Wed Jun 24 18:18:25 2015 +0200

    Added methods base structure for testing argument conformance to the methods definition.

commit bdb0efe99bd94bfd565075a0cc57410c750dbbd7
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Wed Jun 24 17:51:30 2015 +0200

    Revert "Use UA_Argument for method calls. A memleak remains in the argument datasource."

    This reverts commit b1a2edcbdf6821e58e99031d8b96c7ccd45c01a2.

commit b1a2edcbdf6821e58e99031d8b96c7ccd45c01a2
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jun 23 22:42:08 2015 +0200

    Use UA_Argument for method calls. A memleak remains in the argument datasource.

commit 13148053bf70b65e74198c4a43750795f2868349
Merge: 256d2fb 68252e5
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jun 23 19:45:31 2015 +0200

    Merge branch 'master' into service_call_implementation

commit 256d2fbdd190a4e4f685f4946ddbba9ce49cc9a3
Merge: 7ae87bc 2b2a866
Author: Julius Pfrommer <julius.pfrommer@web.de>
Date:   Tue Jun 23 17:26:58 2015 +0200

    Merge branch 'master' into service_call_implementation

commit 7ae87bc3bdf58511c05cf58b5a62a6d152a2c1b4
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Fri Jun 19 10:14:46 2015 +0200

    Added missing #ifdef ENABLE_METHODCALLS in example server.

commit be08b2d01b00950815af71730541d6757b126ee3
Merge: 728ae56 b515f7d
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 18:37:39 2015 +0200

    Merge branch 'master' into service_call_implementation

    Conflicts:
    	src/server/ua_server_binary.c

commit 728ae565465a6d61936030f7cba538b3095f5893
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 18:31:51 2015 +0200

    Commented on UA_NodeAttachedMethod currently _not_ being a dynamic struct (even though _new() exists). This may change in case the current implementation is lacking.

commit e59ee567f2e20cb1db46d3acacf964b06c5b8c57
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 17:33:17 2015 +0200

    Detaching methods from nodes now possible; attaching a method sets/unsets the executable bit

commit 2c02507427ff7880579cf7d807a74d61f1473b27
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 17:29:53 2015 +0200

    Removed List from UA_NodeAttachedMethod; UA_MethodNode->attachedMethod is now a direct struct; Attaching methods now replaces node for Multithreading safety.

commit c4379a0a9ac151543ec35c1e727f3d202d886e18
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 14:14:03 2015 +0200

    Methods are no longer being decorated by a MethodCall Manager. Instead MethodType nodes hold a UA_NodeAttachedMethod struct directly as per @jpfr's and @Stasik0's request. NodeAttachedMethod is still necessary, as otherwise the const attribute of the UA_Node is not maintained.

commit 4eeb09e507164508f173f1680340e6f77c6060b6
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 12:09:34 2015 +0200

    Slightly changed clientside UA_Client_CallMethod() function to return a statuscode (equal to callStatus also contained in outArgs), with outArgs now being set as a pointer-pointer.

commit 4b641ee683f03a6da512885ddb43e94b06efe658
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 11:22:03 2015 +0200

    Server/Client return call argument statuscodes to userspace; Separated function call return status from argument status in ArgumentList. Actually working client example.

commit 7f1e4d8a9b1ed11f4aa247cf82975d2940611f49
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 10:47:51 2015 +0200

    Statuscodes and returned arguments are now being made available to the client in the outputargs struct.

commit c319932e99270c89f5d8d5754929fc669fb67cb6
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Thu Jun 18 10:25:11 2015 +0200

    Fixed several (really stupid) memory allocation errors on the serverside.

commit e6713751a1dcc6b29a719d949f10280510eed933
Merge: 3d38228 a4cd3c5
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Wed Jun 17 18:58:04 2015 +0200

    Merge branch 'master' into service_call_implementation

    Conflicts:
    	tools/generate_datatypes.py

commit 3d38228995beff954a456f0e5546c15ca1cc8987
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Wed Jun 17 18:54:30 2015 +0200

    Server now executes userspace function when the call service is invoked. Basic scaffolding for clientside support put in place.

commit bf40c249c3a096c5a207b88d33c414d3a0bc2027
Author: ichrispa <Chris_Paul.Iatrou@tu-dresden.de>
Date:   Tue Jun 16 16:16:56 2015 +0200

    Cumulative commit for enabling methodCalls service set (cmake -DENABLE_METHODCALLS); creating/remove method hooks is still WIP.
