GetEndpointUrl
Hey @AlexSTI ,

is there a mismatch between what you describe and the code?

> We will give our endpoint descriptor to the requester and not what he sent if it sent 0

Did you mean the following?

``` c
if ( request->endpointUrl.length == 0 )
```

No, in effect I ment what I wrote. Infact, if you look at the entire loop code, you can see:

```
    if(!relevant_endpoints[j])
        continue;
    retval = UA_EndpointDescription_copy(&server->endpointDescriptions[j], &response->endpoints[k]);
    if(retval != UA_STATUSCODE_GOOD)
        break;

    // We will give our endpoint descriptor to the requester and not what he sent if it sent 0...

    if ( request->endpointUrl.length != 0 )
    {
        UA_String_deleteMembers(&response->endpoints[k].endpointUrl);
        retval = UA_String_copy(&request->endpointUrl, &response->endpoints[k].endpointUrl);
    }
    k++;
```

So, before, there should be an entire copy of the endpoint from the server and just in case the requester asked something different, I copy what he asked for...

Please tell me if I got too tired...

I added a unit test checking your described scenario and in current master everything already works as expected.
Probably fixed in e15cdfa
This thread has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.