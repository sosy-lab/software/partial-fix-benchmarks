allow deep numeric ranges to reach into scalars containing an array (#777)

(variants, strings, ...)
Create specific method for splitting endpoint url. Fix #778
Create specific method for splitting endpoint url. Fix #778
Create specific method for splitting endpoint url. Fix #778

Windows wants strncpy_s

Port is max 6 chars

Code refactoring and cleanup

Add support for IPv6

cosmetic improvements; untabify, break overly long lines
Code review: @Pro's MDNS implementation (#732)

* Service RegisterServer and Discovery (#687)

* New RegisterServer service

Add support for Discovery Server (LDS/GDS) by providing RegisterServer service.
FindServers service is updated to return the registered servers.

* Add examples for discovery server

* Use compile switch for LDS

* Use periodic jobs for server register

* Add lastSeen for registered servers and fix compilation errors

* Add cleanup of stale registrations

* Fix UA_string output with printf (use `%.*s`)

* Use correct port number

* LDS customizable timeout for automatic de-register (#695)

* Enable valgrind for unit tests and fix memory leaks

* Fix build errors introduced by cmake rewrite in current master

* LDS customizable timeout for automatic de-register

* LDS-ME (using mDNS) (#730)

* Enable coveralls for Pro/open62541

* Move periodic server register functionality into core implementation

* Add mDNS library as dependency

* Cleanup and enable Mingw x64 on Appveyor

* Set define for mDNS dll

* Fix memory leak

* Check for defined variable to aviod deleting root dir

* Add LDS-ME

LDS Multicast Extension using mDNS for service discovery according to OPC UA Part 12
To be implemented:
- RegisterServer2
- FindServersOnNetwork
- GDS has to call FindServers on all known LDS

* Update to most recent schema definition from https://opcfoundation.org/UA/schemas/Opc.Ua.Types.bsd.xml

* Update to most recent node ids from https://opcfoundation.org/UA/schemas/NodeIds.csv

* Implement RegisterServer2 and FindServersOnNetwork

* Fix memory leak

* Use clang-3.7 because older scan build finds invalid bug

* Fix for mDNS class comparison if value is signed

* cosmetic cleanup (untabify, indention)

* fix after rebase

* Use monotonic clock for timeouts

* Fix crash of register server caused by invalid memory

* Set correct job id for periodic server register

* Use valid SD DNS name ([servername]._opcua-tcp._tcp.[hostname]. -> [servername]-[hostname]._opcua-tcp._tcp.local.)
Set A record for SD DNS

* Remove output of last seen time, since it is now monotonic and thus not human readable

* Fix removal of all remaining DNS records

* Fix discovery test due to previous changes

* Get interface ip addresses for windows

* Update mdnsd submodule

* Additional check output

* Also set A record for hostname

* Send multicast query to _opcua-tcp._tcp.local to find other servers after startup

* Update mdnsd

* Update mdnsd

* Limit mDNS label to max 63 chars according to DNS RFC

* Fix rebase issues

* fix a cppcheck issue for unreachable code

* Untabify

* Check if path or caps is set in TXT record

* Do not remove (TTL=0) `_services._dns-sd._udp.local.` because it may still be used by other PC in the net.

* Update mdnsd

* Fix correct construction of hostname if it is shorter than 63 chars

* Fix correct construction of hostname if it is shorter than 63 chars

* fixup

* Fix correct construction of hostname if it is shorter than 63 chars

* fixup

* Fix correct construction of hostname if it is shorter than 63 chars

* Fix correct construction of hostname if it is shorter than 63 chars

* fixup

* RegisterServer does not require a session

* untabify where it lead to compiler-warnings (if-guards)

* Update mdnsd

* Allow changing the LDS URL for RegisterServer and set default value if NULL

* Typo

* add Callbacks for RegisterServer and mDNS detection

* untabify

* Setting correct port for DiscoveryServer using NetworkLayer information. Better use of UA_EndpointUrl_split

* Include isTxtReceived parameter in callback

* Fix access after free

* Add EndpointUrl to CreateSessionRequest

* Copy&Paste error

* Fix #769: Wrong NodeIDs offset for BinaryEncoding

* fixup

* Fix #769: Wrong NodeIDs offset for BinaryEncoding

* Add additional node ids for previous change of binary encoding offset

* Also check for UA_STATUSCODE_BADSERVICEUNSUPPORTED

* Check correct response code

* Avoid double loops for finding correct node id

* Remove findDataType

* Disable xmlEncodingId for now

* Use UA_UInt64 for repeated job interval

UA_UInt32 for interval in nsec means maximum interval of 429 seconds -> too small

* Fix conversion to UInt64

* Move SplitEndoint into ua_connection.h

* Create specific method for splitting endpoint url. Fix #778

* Windows wants strncpy_s

* Port is max 6 chars

* Fix uninitialized parameter warning

* Handle path without leading slash

* Additional unit tests to cover discovery functionalities

* Untabify

* Fix broken build after merge

* Fix after merge

* fix after merge

* fix after merge

* Removed undef SLIST

* Readd missing BSD_SOURCE define needed for mdns

* Use correct define for amalgamation

* Add missing header for cygwin on Windows

* Fix compilation errors on windows using Visual Studio

* Fix double free

* If applicationType is set to DiscoveryServer, set it to Server

See http://forum.unified-automation.com/topic1987.html

* Fixup for merge

* fix output of UA_String

* Small bug fixes

* Fixup after merge

* Fixup 2 after merge

* Add missing library if win32 compile

* Delete server after test

* Fix wrong positive clang warnings

* Add additional assert for NULL

* Add additional assert for NULL

* Fix MS build

* Additional NULL check for clang analyzer

* Fix MS Build

* Fix #950 wrong requestServer structure passed to registerServer callback.

Thanks @XB32Z

* Reintegrate concepts of PR #932 which were removed in previous merge

Don't copy elements for RegisterServer call

* Fix build on appveyor

* Update mdns

* make mdns non-default; cosmetic improvements

* Enable discovery for unit tests

* Fix missing semicolon

* Travis fold log

* Travis fold log

* Avoid variable exit of local scope

* Refactor: move mdns functions into separate header

* Refactor: simplify and extract functions

* Add UA_ENABLE_DISCOVERY_SEMAPHORE to disable semaphore support on emedded devices

* Fix bug introduced in commit 83edcef3c1f1b1e816151d56e1aa0f40e674fa11

* Enable mdns for unit tests

* Cleaner travis outpu and build separation

* Untabify

Command used:
`find . \( -name '*.c' -o -name '*.h' \) -not \( -path "./deps/*" -prune \) ! -type d -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;`

* Untabify master

Command used:
`find . \( -name '*.c' -o -name '*.h' \) -not \( -path "./deps/*" -prune \) ! -type d -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;`

* Untabify

Command used:
`find . \( -name '*.c' -o -name '*.h' \) -not \( -path "./deps/*" -prune \) ! -type d -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;`

* Client must not create a session for FindServer calls.

Non-Multicast functionalities tested with OPC Foundation LDS Server. Fix #938

* Fix memleak

* Use correct uri

* untabify

* merge changes by @Pro

* split out mdns service functionality into separate file

* make local functions static

* split a large function, mark malloc that needs to be return-checked

* split large functions

* Return valid status code

* Add service description

* untabify

* Update mdns library

* Split loop into separate function

* Split overlong line

* Use correct define

* Fix build on VS2008

* fixup merge

* fixup merge

* Fix build on VS 2008

* Hide no_session connect for cleaner public api
Code review: @Pro's MDNS implementation (#732)

* Service RegisterServer and Discovery (#687)

* New RegisterServer service

Add support for Discovery Server (LDS/GDS) by providing RegisterServer service.
FindServers service is updated to return the registered servers.

* Add examples for discovery server

* Use compile switch for LDS

* Use periodic jobs for server register

* Add lastSeen for registered servers and fix compilation errors

* Add cleanup of stale registrations

* Fix UA_string output with printf (use `%.*s`)

* Use correct port number

* LDS customizable timeout for automatic de-register (#695)

* Enable valgrind for unit tests and fix memory leaks

* Fix build errors introduced by cmake rewrite in current master

* LDS customizable timeout for automatic de-register

* LDS-ME (using mDNS) (#730)

* Enable coveralls for Pro/open62541

* Move periodic server register functionality into core implementation

* Add mDNS library as dependency

* Cleanup and enable Mingw x64 on Appveyor

* Set define for mDNS dll

* Fix memory leak

* Check for defined variable to aviod deleting root dir

* Add LDS-ME

LDS Multicast Extension using mDNS for service discovery according to OPC UA Part 12
To be implemented:
- RegisterServer2
- FindServersOnNetwork
- GDS has to call FindServers on all known LDS

* Update to most recent schema definition from https://opcfoundation.org/UA/schemas/Opc.Ua.Types.bsd.xml

* Update to most recent node ids from https://opcfoundation.org/UA/schemas/NodeIds.csv

* Implement RegisterServer2 and FindServersOnNetwork

* Fix memory leak

* Use clang-3.7 because older scan build finds invalid bug

* Fix for mDNS class comparison if value is signed

* cosmetic cleanup (untabify, indention)

* fix after rebase

* Use monotonic clock for timeouts

* Fix crash of register server caused by invalid memory

* Set correct job id for periodic server register

* Use valid SD DNS name ([servername]._opcua-tcp._tcp.[hostname]. -> [servername]-[hostname]._opcua-tcp._tcp.local.)
Set A record for SD DNS

* Remove output of last seen time, since it is now monotonic and thus not human readable

* Fix removal of all remaining DNS records

* Fix discovery test due to previous changes

* Get interface ip addresses for windows

* Update mdnsd submodule

* Additional check output

* Also set A record for hostname

* Send multicast query to _opcua-tcp._tcp.local to find other servers after startup

* Update mdnsd

* Update mdnsd

* Limit mDNS label to max 63 chars according to DNS RFC

* Fix rebase issues

* fix a cppcheck issue for unreachable code

* Untabify

* Check if path or caps is set in TXT record

* Do not remove (TTL=0) `_services._dns-sd._udp.local.` because it may still be used by other PC in the net.

* Update mdnsd

* Fix correct construction of hostname if it is shorter than 63 chars

* Fix correct construction of hostname if it is shorter than 63 chars

* fixup

* Fix correct construction of hostname if it is shorter than 63 chars

* fixup

* Fix correct construction of hostname if it is shorter than 63 chars

* Fix correct construction of hostname if it is shorter than 63 chars

* fixup

* RegisterServer does not require a session

* untabify where it lead to compiler-warnings (if-guards)

* Update mdnsd

* Allow changing the LDS URL for RegisterServer and set default value if NULL

* Typo

* add Callbacks for RegisterServer and mDNS detection

* untabify

* Setting correct port for DiscoveryServer using NetworkLayer information. Better use of UA_EndpointUrl_split

* Include isTxtReceived parameter in callback

* Fix access after free

* Add EndpointUrl to CreateSessionRequest

* Copy&Paste error

* Fix #769: Wrong NodeIDs offset for BinaryEncoding

* fixup

* Fix #769: Wrong NodeIDs offset for BinaryEncoding

* Add additional node ids for previous change of binary encoding offset

* Also check for UA_STATUSCODE_BADSERVICEUNSUPPORTED

* Check correct response code

* Avoid double loops for finding correct node id

* Remove findDataType

* Disable xmlEncodingId for now

* Use UA_UInt64 for repeated job interval

UA_UInt32 for interval in nsec means maximum interval of 429 seconds -> too small

* Fix conversion to UInt64

* Move SplitEndoint into ua_connection.h

* Create specific method for splitting endpoint url. Fix #778

* Windows wants strncpy_s

* Port is max 6 chars

* Fix uninitialized parameter warning

* Handle path without leading slash

* Additional unit tests to cover discovery functionalities

* Untabify

* Fix broken build after merge

* Fix after merge

* fix after merge

* fix after merge

* Removed undef SLIST

* Readd missing BSD_SOURCE define needed for mdns

* Use correct define for amalgamation

* Add missing header for cygwin on Windows

* Fix compilation errors on windows using Visual Studio

* Fix double free

* If applicationType is set to DiscoveryServer, set it to Server

See http://forum.unified-automation.com/topic1987.html

* Fixup for merge

* fix output of UA_String

* Small bug fixes

* Fixup after merge

* Fixup 2 after merge

* Add missing library if win32 compile

* Delete server after test

* Fix wrong positive clang warnings

* Add additional assert for NULL

* Add additional assert for NULL

* Fix MS build

* Additional NULL check for clang analyzer

* Fix MS Build

* Fix #950 wrong requestServer structure passed to registerServer callback.

Thanks @XB32Z

* Reintegrate concepts of PR #932 which were removed in previous merge

Don't copy elements for RegisterServer call

* Fix build on appveyor

* Update mdns

* make mdns non-default; cosmetic improvements

* Enable discovery for unit tests

* Fix missing semicolon

* Travis fold log

* Travis fold log

* Avoid variable exit of local scope

* Refactor: move mdns functions into separate header

* Refactor: simplify and extract functions

* Add UA_ENABLE_DISCOVERY_SEMAPHORE to disable semaphore support on emedded devices

* Fix bug introduced in commit 83edcef3c1f1b1e816151d56e1aa0f40e674fa11

* Enable mdns for unit tests

* Cleaner travis outpu and build separation

* Untabify

Command used:
`find . \( -name '*.c' -o -name '*.h' \) -not \( -path "./deps/*" -prune \) ! -type d -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;`

* Untabify master

Command used:
`find . \( -name '*.c' -o -name '*.h' \) -not \( -path "./deps/*" -prune \) ! -type d -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;`

* Untabify

Command used:
`find . \( -name '*.c' -o -name '*.h' \) -not \( -path "./deps/*" -prune \) ! -type d -exec bash -c 'expand -t 4 "$0" > /tmp/e && mv /tmp/e "$0"' {} \;`

* Client must not create a session for FindServer calls.

Non-Multicast functionalities tested with OPC Foundation LDS Server. Fix #938

* Fix memleak

* Use correct uri

* untabify

* merge changes by @Pro

* split out mdns service functionality into separate file

* make local functions static

* split a large function, mark malloc that needs to be return-checked

* split large functions

* Return valid status code

* Add service description

* untabify

* Update mdns library

* Split loop into separate function

* Split overlong line

* Use correct define

* Fix build on VS2008

* fixup merge

* fixup merge

* Fix build on VS 2008

* Hide no_session connect for cleaner public api
