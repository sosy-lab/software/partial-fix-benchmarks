connect issue - DNS lookup of 192.163.1.193 failed with error Servname not supported for ai_socktype
Hey @ilike28 ,

can you post the connection string, i.e. opc.tcp://hostname:port
It looks as if sth went wrong with the port number.

The Server is locol server....
I just copy and paste the URL to the client source code.

OPC UA  Foundation- Server
opc.tcp://tq021110740:62541/Quickstarts/SimpleEventsServer

Unified Automation - Server
opc.tcp://tq021110740:48010

Just try "opc.tcp://tq021110740:62541" for the getEndpoints function

On 29 August 2016 at 10:05, ilike28 notifications@github.com wrote:

> The Server is locol server....
> 
> OPC UA Foundation- Server
> opc.tcp://tq021110740:62541/Quickstarts/SimpleEventsServer
> 
> Unified Automation - Server
> opc.tcp://tq021110740:48010
> 
> —
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub
> https://github.com/open62541/open62541/issues/778#issuecomment-243058647,
> or mute the thread
> https://github.com/notifications/unsubscribe-auth/AAcoshZc1Sh9AiBnvGk_hrsZAF4o0jF-ks5qkpKsgaJpZM4JvLug
> .

It Work!!!! !!!
Thank you ~~~

Great!
Anyway, we should do sometihing about connection strings with application names.

Either use application names to filter endpoints or at least ignore them....

Marked as up for grabs for developers.

FYI:
In the `feature_mdns` branch I already implemented correct handling of endpoints with application names:
https://github.com/open62541/open62541/blob/feature_mdns/plugins/ua_network_tcp.c#L603
which uses:
https://github.com/open62541/open62541/blob/feature_mdns/src/ua_types.c#L987
https://github.com/open62541/open62541/blob/feature_mdns/include/ua_types.h#L235

If needed I can extract it into a separate PR to merge it into 0.2

> If needed I can extract it into a separate PR to merge it into 0.2

Sounds great!
A couple of things you might consider:
- I'd rather put the endpointurl-parser into ua_connection.c/.h. Strictly speaking, you do not invent a new datatype.
- It should be enough to export one function only. For example, `UA_EndpointUrl_split_ptr` could be made static.
- Be very careful about zero-delimited buffers and copying.

``` c
strncpy(path, pathTmp, strlen(pathTmp));
path[strlen(pathTmp)]='\0';
```

Bam! The length of `path` is never checked. But the length and content of pathTmp can be set arbitrarily by an attacker. For example during discovery where the server opens up a client connection.
That's an exploitable buffer overflow where a server can be made to run code sent by a remote connection.

At the least, there should be a minimal length for `path` and a check for the length of `pathTmp`. Similar as is done (and documented) for the hostname.

Another solution is to keep the appraoch from `UA_EndpointUrl_split_ptr` where the output path points **into** the endpointurl. That needs no copying between buffers at all.

This thread has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.