fix https://github.com/OpenSC/OpenSC/issues/1786

Thanks to Alexandre Gonzalo
pkcs11-tool: fixed displaying secret key attributes

fixes https://github.com/OpenSC/OpenSC/issues/1805
pkcs11-tool: fixed displaying secret key attributes (#1807)

fixes https://github.com/OpenSC/OpenSC/issues/1805
