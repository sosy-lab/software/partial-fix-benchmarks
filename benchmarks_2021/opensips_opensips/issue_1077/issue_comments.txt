resolve.c - check_ip_address function incorrectly returns 0 when one IP is substring of another
@vkuzmenok, could you point the file and line where the above mention code resides ? Thank you
[https://github.com/OpenSIPS/opensips/blob/eae2985abd62d2ae8a6138519cc3634d7bd022ce/resolve.c#L636](https://github.com/OpenSIPS/opensips/blob/eae2985abd62d2ae8a6138519cc3634d7bd022ce/resolve.c#L636)
and the correct check should be one similar to https://github.com/OpenSIPS/opensips/blob/eae2985abd62d2ae8a6138519cc3634d7bd022ce/resolve.c#L626 ?
Yes
@vkuzmenok , could you please test and confirm the above fix. If everything is ok, I will do a backport to the 2.x and 1.11 branches too.

Thanks, Bogdan
keeping it open until the backports are done
@bogdan-iancu, From code perspective fix looks OK. We still use 1.11 version. Is it possible to get a 1.11 branch build to test?
Backport is done, all the way to 1.11 branch.
@bogdan-iancu, did you also ported the dead code removal to the all branches?
yes, did so