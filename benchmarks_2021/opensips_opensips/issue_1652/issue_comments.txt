[CRASH] OpenSIPS crashes since of child that serves rtpproxy
I see that OpenSIPS reports that a core dump was generated - can you inspect that corefile and send us the backtrace?
@razvancrainea  unfortunately seems that opensips  has written to the standard input of that program instead of to a file, I cannot find a coredump. I will specify "-w" option to get an exact place for core dumps.
@razvancrainea here is a backtrace, please take a look:
https://drive.google.com/open?id=1lpOvGF_7H0psjpElmGkrPtOe4xF4bjNY
@razvancrainea and one more:
https://drive.google.com/open?id=1PqfH2yE2PKyw1iVCXtH9qhqiPj6VDkIj
@razvancrainea sorry for disturbing you, but I'm experiencing really huge problems now since of that, could you be so kind and at least hint where to look into?
Can you run in debug mode?
Also, are you using clustering support?
@razvancrainea you mean to run openSIPS with log_level=4  and debug_mode=yes ?
no I don't use any cluster support.
Just `log_level-4`, no `debug_mode`.
In the meantime, I found a bug that might have caused the issues that generated the corefile. However, they are not related to the rtpproxy process, so there might be a different issue in there.
@razvancrainea Is it related to nathelper? 

Alright I will enable log_level4 and will see if this works out.
Let me know which  gmail address you have, so I can share a log file only for you.
Thanks!
I've just committed a fix on master and 2.4 branch. Can you pull the latest sources and try again, and confirm if this was fixed or not?
You can send your logs at razvan at opensips.org.
@razvancrainea alright! Will check this out soon and will report. 
@razvancrainea I recompiled all SBCs yesterday and seems to be working fine since that.
But not 100% sure, because not that much time left. I'll report today in the evening as well, just in case!

Thanks for the job done!
@razvancrainea All seems to be working fine, sorry for so late response.
I will close this ticket.

Please let me know if possible, whether this bug could have affection on NAT pings over TCP?
I had a sporadic problem just before this one, when OpenSIPS was crashing when tried to ping remote host (with OPTIONS) over TCP transporting.
Yes, this could have happened for TCP pinging as well.