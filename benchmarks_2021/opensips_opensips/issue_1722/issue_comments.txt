[BUG] Possibility of duplicate key error in activewatcher table
@wdoekes , take a look at the above commit - it should address the pending issue. If you can double check / run some testing, it will be great !
Ok, the ec1889638a9d43280ba1362f6108a175efd4a854 patch introduced a problem (reported by @danpascu), so I reverted for the moment - I will take the time to come up with a better solution.
Any updates here? No progress has been made in the last 15 days, marking as stale. Will close this issue if no further updates are made in the next 30 days.

yeah, still on my todo list, to find a solution to the hen-and-egg problem that was present into the previous fix :P 
Yea. I haven't gotten around to taking a proper look either. Thanks for keeping it alive :)
> Ok, the ec18896 patch introduced a problem (reported by @danpascu)

So, we have:

commit ec1889638a9d43280ba1362f6108a175efd4a854
```
Author: Bogdan-Andrei Iancu <bogdan@opensips.org>
Date:   Fri Jun 14 13:49:10 2019 +0300

    Proper error handling in handle_subscribe
    
    1) be sure we send back a 500 reply in all error cases
    2) detect the cases of subscription session overlapping (see #1722)
```
commit 110b1852772a24840b66ccece6a65a70ce9a181e
```
Author: Bogdan-Andrei Iancu <bogdan@opensips.org>
Date:   Fri Jun 21 19:08:01 2019 +0300

    Revert "Proper error handling in handle_subscribe"
    
    This reverts commit ec1889638a9d43280ba1362f6108a175efd4a854.
```

Unfortunately, there is no mention what was wrong with said commit.

If I may venture a guess, I would theorize that:

- Perhaps msg was NULL (_it checks msg before sending the 2XX.. should it check before doing send_error_reply?_). Adding a check is simple enough:
```
+error_500_reply:
+       if (msg) {
+               static const str reply_500 = str_init("Server Internal Error");
+               if (send_error_reply(msg, 500, reply_500) < 0)
+                       LM_ERR("failed to send reply on error case\n");
+       }
+
```

- The update_shtable() before insert_shtable() opens the possibility for race conditions. Doing this check inside insert_shtable() instead would be the right fix, I think:
```
diff --git a/modules/presence/hash.c b/modules/presence/hash.c
index 8fa01b057..c0487e674 100644
--- a/modules/presence/hash.c
+++ b/modules/presence/hash.c
@@ -237,6 +237,7 @@ error:
 int insert_shtable(shtable_t htable,unsigned int hash_code, subs_t* subs)
 {
        subs_t* new_rec= NULL;
+       subs_t* s;
 
        new_rec= mem_copy_subs_noc(subs);
        if(new_rec== NULL)
@@ -253,6 +254,17 @@ int insert_shtable(shtable_t htable,unsigned int hash_code, subs_t* subs)
 
        lock_get(&htable[hash_code].lock);
 
+       /* Check whether the record exists already, and if so fail.
+        * This should generally not happen, so we check it first after setting
+        * up new_rec. */
+       s = search_shtable(htable,subs->callid, subs->to_tag, subs->from_tag,
+                       hash_code);
+       if (s != NULL) {
+               lock_release(&htable[hash_code].lock);
+               LM_ERR("tried to insert over an existing sub\n");
+               goto error;
+       }
+
        new_rec->next= htable[hash_code].entries->next;
 
        htable[hash_code].entries->next= new_rec;
```

- Sending a 500 after DB failure, without dropping the sub, sounds wrong. Now it's in the hash, taking up space, while the user thinks it failed. Fixing this would involve calling delete_shtable():

```
                                if(insert_subs_db(subs) < 0)
                                {
+                                       /* We'll be reporting failure. So undo the insert we did. */
+                                       delete_shtable(subs_htable,hash_code,subs->to_tag);
                                        LM_ERR("failed to insert subscription in database\n");
-                                       goto error;
+                                       goto error_500_reply;
                                }
```

- However, it turns out that the delete_shtable() is less selective than insert/update. in kamailio they fixed the discrepancy here: kamailio/kamailio@74d7395332 ("Changed delete_shtable function to check dialog's full tag set")
```
--- a/modules/presence/hash.c
+++ b/modules/presence/hash.c
@@ -275,7 +275,7 @@ int insert_shtable(shtable_t htable,unsigned int hash_code, subs_t* subs)
        return 0;
 }
 
-int delete_shtable(shtable_t htable,unsigned int hash_code,str to_tag)
+int delete_shtable(shtable_t htable,unsigned int hash_code,subs_t* subs)
...
```
Barring a good reason against it, we should probably implement that change too.



@wdoekes, the problem with the reverted commit is how the To-tag is generated. 
My "fix" was to move the insertion (of the subscription) into the hash **before** sending back any reply - by doing this, I can test the collision and return a proper reply.
But the problem is that the To-tag (needed for checking the hash) is generated by the function sending the reply, which (with my commit) is triggered **after** the hash check :( .

So the dilemma is : how to build the To-tag (needed for hash check) without sending the SIP reply ? Doing the hash check after sending the reply doesn't help too much :P 
Any updates here? No progress has been made in the last 15 days, marking as stale. Will close this issue if no further updates are made in the next 30 days.

Any updates here? No progress has been made in the last 15 days, marking as stale. Will close this issue if no further updates are made in the next 30 days.

Any updates here? No progress has been made in the last 15 days, marking as stale. Will close this issue if no further updates are made in the next 30 days.

Marking as closed due to lack of progress for more than 30 days. If this issue is still relevant, please re-open it with additional details.

A possible fix (which I favor) is to split the `to-tag` generation and reply sending from the same function. The presence module will do:
* generate a new UAS side tag (the `to-tag`)
* check the subscription hash table for collisions and add into the hash (if collision, send a negative reply)
* send the 200 OK reply for the subscription by using the already generated `to-tag`
Sounds good.

Something like this, as addition to your original ec188963 then?
```
        else
        {
-               if(msg && send_2XX_reply(msg, reply_code, subs->expires, &subs->to_tag,
-                       &subs->local_contact)<0)
-               {
-                       LM_ERR("sending 2XX reply\n");
-                       goto error;
-               }
+               // - calc_tag_suffix
+               // - append to totag_prefix (module-specific?)
+               // - put result in &subs->to_tag
+               // - put result in msg and re-parse msg->to (so 
 
                if(subs->expires!= 0)
...
+               if(msg && send_2XX_reply(msg, reply_code, subs->expires, 0,
+                       &subs->local_contact)<0)
```
Yup, something like that. I hope to get some time in the next days to rework the "signaling" API to expose a `generate_totag()` function.
Hi @wdoekes , could you do some testing (on head) with my latest changes? this should fully solve the problem ;)
Awesome. We're on it.
Thanks @wdoekes , just let me know the result, so I can do the backport and close here.
Hi! Sorry about the delay. I finally got around updating my test setup and fixing it up to handle opensips 3.1.

I hacked tags.h to trigger the dupes:
```
--- a/tags.h
+++ b/tags.h
@@ -66,6 +66,8 @@ static inline void calc_tag_suffix( struct sip_msg *msg, char *tag_suffix)
 	if (msg->via1->branch)
 		suffix_source[ss_nr++]=msg->via1->branch->value;
 	MD5StringArray( tag_suffix, suffix_source, ss_nr );
+/* block the last 31 chars of the md5 */
+memcpy(tag_suffix + 1, "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx", 31);
 }
 
 inline static void init_tags( char *tag, char **suffix,
```
And then ran some SIPp to trigger the issue with this scenario:
[subscribe-dupes.txt](https://github.com/OpenSIPS/opensips/files/4041204/subscribe-dupes.txt)

Before your changeset:
```
INFO:presence:update_subscription: notify                                      
INFO:presence:send_notify_request: NOTIFY sip:user@host via sip:xxx on behalf of sip:user@host for event dialog, to_tag=513c-5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, cseq=1
INFO:presence:update_subscription: notify                                      
INFO:presence:send_notify_request: NOTIFY sip:user@host via sip:xxx on behalf of sip:user@host for event dialog, to_tag=513c-0xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, cseq=1
CRITICAL:db_mysql:wrapper_single_mysql_stmt_execute: driver error (1062): Duplicate entry 'sip:user@host for key 'presentity_uri'
ERROR:presence:insert_subs_db: unsuccessful sql insert                         
ERROR:presence:update_subscription: failed to insert subscription in database  
ERROR:presence:handle_subscribe: in update_subscription                        
```
(without any response)

After:
```
INFO:presence:send_notify_request: NOTIFY sip:user@host via sip:xxx on behalf of sip:user@host for event dialog, to_tag=513c-fxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, cseq=1
INFO:presence:send_notify_request: NOTIFY sip:user@host via sip:xxx on behalf of sip:user@host for event dialog, to_tag=513c-5xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, cseq=1
INFO:presence:send_notify_request: NOTIFY sip:user@host via sip:xxx on behalf of sip:user@host for event dialog, to_tag=513c-exxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx, cseq=1
ERROR:presence:update_subscription: subscription overlapping detected, rejecting
ERROR:presence:handle_subscribe: in update_subscription                        
```
(and with a nice 500 Server Internal Error)

And the records that were acknowledged were also found in the DB. Looking all good :ok_hand: 

_On a related note:_

I see you replaced an `LM_INFO("notify\n")` with an `LM_DBG` there too. As far as I'm concerned, the one in handle_expired_subs can go too, as the `handle_expired_subs` calls `send_notify_request` too, which has its own useful info.
```
# git show 4918544248c495b1099a54326f97a82df97bbe76 | grep LM_INFO -A2
-	LM_INFO("notify\n");
+
+	LM_DBG("send NOTIFY's out\n");
```
```patch
--- a/modules/presence/subscribe.c
+++ b/modules/presence/subscribe.c
@@ -1307,7 +1307,7 @@ int handle_expired_subs(subs_t* s)
                s->reason.len= 7;
                s->expires= 0;
 
-               LM_INFO("notify\n");
+               LM_DBG("send NOTIFY's out\n");
                if(send_notify_request(s, NULL, NULL, 1, NULL, 0)< 0)
                {
                        LM_ERR("send Notify not successful\n");
```
Thank you @wdoekes , I included your change of the log line. 
The backports to 3.0 and 2.4 were done, finally closing here :) 