[BUG] sql_cacher does not handle empty DB tables properly
@clifjones , thanks for the detailed report and for the fix. I took the liberty to change a bit your patch, mainly to remove the warning log - I don't think is the case to have it there.
Please re-test the fix as committed (devel only), and upon your confirmation I will do the backport too.
I just verified your patch and our tests pass using your proposed commit.
Thanks @clifjones , backports done, closing here