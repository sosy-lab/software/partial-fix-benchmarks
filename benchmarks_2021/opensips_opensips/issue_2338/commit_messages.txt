db_sqlite: fix memory overwrite error when reallocing rows
[topology_hiding] fix vulnerability in TH decoding

Extra checks were added to prevent buffer overflow/underflow when decoding the TH information (in non-dialog module) extracted from the Contact hdr. This information may be subject to malicious changes from an external attacker.

Credits for reporting and for the fix go to @wdoekes.
The suggested fix was re-worked a bit, but the idea is the same.
Fixes #2338
[topology_hiding] fix vulnerability in TH decoding

Extra checks were added to prevent buffer overflow/underflow when decoding the TH information (in non-dialog module) extracted from the Contact hdr. This information may be subject to malicious changes from an external attacker.

Credits for reporting and for the fix go to @wdoekes.
The suggested fix was re-worked a bit, but the idea is the same.
Fixes #2338

(cherry picked from commit 78909c344fe4c25718233e6a00f6e2bd19373be3)
[topology_hiding] fix vulnerability in TH decoding

Extra checks were added to prevent buffer overflow/underflow when decoding the TH information (in non-dialog module) extracted from the Contact hdr. This information may be subject to malicious changes from an external attacker.

Credits for reporting and for the fix go to @wdoekes.
The suggested fix was re-worked a bit, but the idea is the same.
Fixes #2338

(cherry picked from commit 78909c344fe4c25718233e6a00f6e2bd19373be3)
