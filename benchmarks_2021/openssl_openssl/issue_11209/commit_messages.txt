s_server normal shutdown

Partially fixes #11209
s_server normal shutdown

Partially fixes #11209

Reviewed-by: Matt Caswell <matt@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/11734)
Introducing option SSL_OP_IGNORE_UNEXPECTED_EOF

Partially fixes #11209.

Before OpenSSL 3.0 in case when peer does not send close_notify,
the behaviour was to set SSL_ERROR_SYSCALL error with errno 0.
This behaviour has changed. The SSL_OP_IGNORE_UNEXPECTED_EOF restores
the old behaviour for compatibility's sake.
Introducing option SSL_OP_IGNORE_UNEXPECTED_EOF

Partially fixes #11209.

Before OpenSSL 3.0 in case when peer does not send close_notify,
the behaviour was to set SSL_ERROR_SYSCALL error with errno 0.
This behaviour has changed. The SSL_OP_IGNORE_UNEXPECTED_EOF restores
the old behaviour for compatibility's sake.

Reviewed-by: Matt Caswell <matt@openssl.org>
Reviewed-by: Kurt Roeckx <kurt@roeckx.be>
Reviewed-by: Tomas Mraz <tmraz@fedoraproject.org>
(Merged from https://github.com/openssl/openssl/pull/11735)
