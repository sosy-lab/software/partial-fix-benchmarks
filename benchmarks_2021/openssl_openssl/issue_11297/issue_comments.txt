Continue on UNABLE_TO_VERIFY_LEAF_SIGNATURE
You can do that by writing your own verify callback that masks the error.  Look at apps/verify.c for example.
Are you sure that should work with UNABLE_TO_VERIFY_LEAF_SIGNATURE?

We uses Qt has framework which uses a custom verify callback. They allow to ignore certain ssl errors. We can ignore any error except UNABLE_TO_VERIFY_LEAF_SIGNATURE.
https://code.qt.io/cgit/qt/qtbase.git/tree/src/network/ssl/qsslsocket_openssl.cpp#n525

Also it looks like the whole verification will be stoped here as it immediately return and cannot go to "check_cert:" in line 1757.
https://github.com/openssl/openssl/blob/master/crypto/x509/x509_vfy.c#L1725
> Are you sure that should work with UNABLE_TO_VERIFY_LEAF_SIGNATURE?

It should but I have not tried to do so.

> We uses Qt has framework which uses a custom verify callback.

I didn't know you were doing that.  I don't know what you would have to do to work with QT.

> > Are you sure that should work with UNABLE_TO_VERIFY_LEAF_SIGNATURE?
> 
> It should but I have not tried to do so.

It immediately stops verification because openssl "returns" on THAT error. Any other error is not a problem.

I tried it with "openssl s_client", too. Expiration is not checked anymore after that error.
I was suggesting you write a custom verify callback and mask that error.
Sorry, I was not clear enough. :-)

I already did that - but that won't help.
https://github.com/openssl/openssl/blob/master/crypto/x509/x509_vfy.c#L1725
```
            return verify_cb_cert(ctx, xi, 0,
                                  X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE);
```

That should be like this because all other callbacks are executed the same way.
```
  if(!verify_cb_cert(ctx, xi, 0,
                                  X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE))
     return 0;
```

That should be handled like a self-signed certificate like mentioned some lines later.
```
      /*
         * Skip signature check for self signed certificates unless explicitly
         * asked for.  It doesn't add any security and just wastes time.  If
         * the issuer's public key is unusable, report the issuer certificate
         * and its depth (rather than the depth of the subject).
         */
```
Have you tried X509_V_FLAG_PARTIAL_CHAIN ?
I'm not entirely sure that the spec at https://www.bsi.bund.de/SharedDocs/Downloads/DE/BSI/Publikationen/TechnischeRichtlinien/TR03124/TR-03124-1.pdf?__blob=publicationFile&v=2 is a particularly good one to use.  I only skimmed it, but it looks like it's advocating a "listen on localhost on a specific port" model that is not guaranteed to actually result in communication with the desired entity, and I'm also concerned about the usage of certificates for conveying additional information.  It sounds like the verification procedures for the certificate may be influenced by the contents of the certificate in a way that can lead to attacks unless the additional information in the certificate is additionally validated against some out-of-band channel.
> Have you tried X509_V_FLAG_PARTIAL_CHAIN ?

No, Qt don't have an API for that. That would be another feature request there. They don't accept a lot of crypto library specific stuff because they supports multiple backends.

But why another parameter to skip the whole callback of X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE? In my opinion that error should be maskable like all other errors like @richsalz suggested.
What are the disadvantages if it can be masked by the application? I don't see why it should not be merged.
Ping? Why not fixing the issue that apps cannot mask that?
I just grepped for all `verify_cb_cert()`, and am noticing that we do this "direct return" in more places.  I believe that the reasoning is that these are spots where the callback should take over the verification completely from the point in the chain, and simply return yay or nay, which then gets propagated back.

``` console
$ git grep -n -C1 verify_cb_cert
crypto/x509/x509_vfy.c-169- */
crypto/x509/x509_vfy.c:170:static int verify_cb_cert(X509_STORE_CTX *ctx, X509 *x, int depth, int err)
crypto/x509/x509_vfy.c-171-{
--
crypto/x509/x509_vfy.c-207-        if (i > 0 && !check_key_level(ctx, cert) &&
crypto/x509/x509_vfy.c:208:            verify_cb_cert(ctx, cert, i, X509_V_ERR_CA_KEY_TOO_SMALL) == 0)
crypto/x509/x509_vfy.c-209-            return 0;
--
crypto/x509/x509_vfy.c-214-        if (i < num - 1 && !check_sig_level(ctx, cert) &&
crypto/x509/x509_vfy.c:215:            verify_cb_cert(ctx, cert, i, X509_V_ERR_CA_MD_TOO_WEAK) == 0)
crypto/x509/x509_vfy.c-216-            return 0;
--
crypto/x509/x509_vfy.c-240-    if (err != X509_V_OK) {
crypto/x509/x509_vfy.c:241:        if ((ok = verify_cb_cert(ctx, NULL, ctx->error_depth, err)) == 0)
crypto/x509/x509_vfy.c-242-            return ok;
--
crypto/x509/x509_vfy.c-295-    if (!check_key_level(ctx, ctx->cert) &&
crypto/x509/x509_vfy.c:296:        !verify_cb_cert(ctx, ctx->cert, 0, X509_V_ERR_EE_KEY_TOO_SMALL))
crypto/x509/x509_vfy.c-297-        return 0;
--
crypto/x509/x509_vfy.c-441-
crypto/x509/x509_vfy.c:442:    return verify_cb_cert(ctx, x, depth, X509_V_ERR_INVALID_PURPOSE);
crypto/x509/x509_vfy.c-443-}
--
crypto/x509/x509_vfy.c-484-            && (x->ex_flags & EXFLAG_CRITICAL)) {
crypto/x509/x509_vfy.c:485:            if (!verify_cb_cert(ctx, x, i,
crypto/x509/x509_vfy.c-486-                                X509_V_ERR_UNHANDLED_CRITICAL_EXTENSION))
--
crypto/x509/x509_vfy.c-489-        if (!allow_proxy_certs && (x->ex_flags & EXFLAG_PROXY)) {
crypto/x509/x509_vfy.c:490:            if (!verify_cb_cert(ctx, x, i,
crypto/x509/x509_vfy.c-491-                                X509_V_ERR_PROXY_CERTIFICATES_NOT_ALLOWED))
--
crypto/x509/x509_vfy.c-527-        }
crypto/x509/x509_vfy.c:528:        if (ret == 0 && !verify_cb_cert(ctx, x, i, X509_V_OK))
crypto/x509/x509_vfy.c-529-            return 0;
--
crypto/x509/x509_vfy.c-535-            && (plen > (x->ex_pathlen + proxy_path_length))) {
crypto/x509/x509_vfy.c:536:            if (!verify_cb_cert(ctx, x, i, X509_V_ERR_PATH_LENGTH_EXCEEDED))
crypto/x509/x509_vfy.c-537-                return 0;
--
crypto/x509/x509_vfy.c-560-                if (proxy_path_length > x->ex_pcpathlen) {
crypto/x509/x509_vfy.c:561:                    if (!verify_cb_cert(ctx, x, i,
crypto/x509/x509_vfy.c-562-                                        X509_V_ERR_PROXY_PATH_LENGTH_EXCEEDED))
--
crypto/x509/x509_vfy.c-676-            if (err != X509_V_OK
crypto/x509/x509_vfy.c:677:                && !verify_cb_cert(ctx, x, i, err))
crypto/x509/x509_vfy.c-678-                return 0;
--
crypto/x509/x509_vfy.c-707-                default:
crypto/x509/x509_vfy.c:708:                    if (!verify_cb_cert(ctx, x, i, rv))
crypto/x509/x509_vfy.c-709-                        return 0;
--
crypto/x509/x509_vfy.c-719-{
crypto/x509/x509_vfy.c:720:    return verify_cb_cert(ctx, ctx->cert, 0, errcode);
crypto/x509/x509_vfy.c-721-}
--
crypto/x509/x509_vfy.c-841- rejected:
crypto/x509/x509_vfy.c:842:    if (!verify_cb_cert(ctx, x, i, X509_V_ERR_CERT_REJECTED))
crypto/x509/x509_vfy.c-843-        return X509_TRUST_REJECTED;
--
crypto/x509/x509_vfy.c-1642-                continue;
crypto/x509/x509_vfy.c:1643:            if (!verify_cb_cert(ctx, x, i,
crypto/x509/x509_vfy.c-1644-                                X509_V_ERR_INVALID_POLICY_EXTENSION))
--
crypto/x509/x509_vfy.c-1695-        return 0;
crypto/x509/x509_vfy.c:1696:    if (i == 0 && !verify_cb_cert(ctx, x, depth,
crypto/x509/x509_vfy.c-1697-                                  X509_V_ERR_ERROR_IN_CERT_NOT_BEFORE_FIELD))
crypto/x509/x509_vfy.c-1698-        return 0;
crypto/x509/x509_vfy.c:1699:    if (i > 0 && !verify_cb_cert(ctx, x, depth, X509_V_ERR_CERT_NOT_YET_VALID))
crypto/x509/x509_vfy.c-1700-        return 0;
--
crypto/x509/x509_vfy.c-1704-        return 0;
crypto/x509/x509_vfy.c:1705:    if (i == 0 && !verify_cb_cert(ctx, x, depth,
crypto/x509/x509_vfy.c-1706-                                  X509_V_ERR_ERROR_IN_CERT_NOT_AFTER_FIELD))
crypto/x509/x509_vfy.c-1707-        return 0;
crypto/x509/x509_vfy.c:1708:    if (i < 0 && !verify_cb_cert(ctx, x, depth, X509_V_ERR_CERT_HAS_EXPIRED))
crypto/x509/x509_vfy.c-1709-        return 0;
--
crypto/x509/x509_vfy.c-1738-        if (n <= 0)
crypto/x509/x509_vfy.c:1739:            return verify_cb_cert(ctx, xi, 0,
crypto/x509/x509_vfy.c-1740-                                  X509_V_ERR_UNABLE_TO_VERIFY_LEAF_SIGNATURE);
--
crypto/x509/x509_vfy.c-1785-
crypto/x509/x509_vfy.c:1786:            if (ret != X509_V_OK && !verify_cb_cert(ctx, xi, issuer_depth, ret))
crypto/x509/x509_vfy.c-1787-                return 0;
--
crypto/x509/x509_vfy.c-1789-                ret = X509_V_ERR_UNABLE_TO_DECODE_ISSUER_PUBLIC_KEY;
crypto/x509/x509_vfy.c:1790:                if (!verify_cb_cert(ctx, xi, issuer_depth, ret))
crypto/x509/x509_vfy.c-1791-                    return 0;
--
crypto/x509/x509_vfy.c-1793-                ret = X509_V_ERR_CERT_SIGNATURE_FAILURE;
crypto/x509/x509_vfy.c:1794:                if (!verify_cb_cert(ctx, xs, n, ret))
crypto/x509/x509_vfy.c-1795-                    return 0;
--
crypto/x509/x509_vfy.c-2878-        return 1;
crypto/x509/x509_vfy.c:2879:    return verify_cb_cert(ctx, cert, 0, err);
crypto/x509/x509_vfy.c-2880-}
--
crypto/x509/x509_vfy.c-2933-            return 0;
crypto/x509/x509_vfy.c:2934:        return verify_cb_cert(ctx, cert, 0, X509_V_ERR_DANE_NO_MATCH);
crypto/x509/x509_vfy.c-2935-    }
--
crypto/x509/x509_vfy.c-3322-        if (num > depth)
crypto/x509/x509_vfy.c:3323:            return verify_cb_cert(ctx, NULL, num-1,
crypto/x509/x509_vfy.c-3324-                                  X509_V_ERR_CERT_CHAIN_TOO_LONG);
--
crypto/x509/x509_vfy.c-3326-            (!DANETLS_HAS_PKIX(dane) || dane->pdpth >= 0))
crypto/x509/x509_vfy.c:3327:            return verify_cb_cert(ctx, NULL, num-1, X509_V_ERR_DANE_NO_MATCH);
crypto/x509/x509_vfy.c-3328-        if (self_signed && sk_X509_num(ctx->chain) == 1)
crypto/x509/x509_vfy.c:3329:            return verify_cb_cert(ctx, NULL, num-1,
crypto/x509/x509_vfy.c-3330-                                  X509_V_ERR_DEPTH_ZERO_SELF_SIGNED_CERT);
crypto/x509/x509_vfy.c-3331-        if (self_signed)
crypto/x509/x509_vfy.c:3332:            return verify_cb_cert(ctx, NULL, num-1,
crypto/x509/x509_vfy.c-3333-                                  X509_V_ERR_SELF_SIGNED_CERT_IN_CHAIN);
crypto/x509/x509_vfy.c-3334-        if (ctx->num_untrusted < num)
crypto/x509/x509_vfy.c:3335:            return verify_cb_cert(ctx, NULL, num-1,
crypto/x509/x509_vfy.c-3336-                                  X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT);
crypto/x509/x509_vfy.c:3337:        return verify_cb_cert(ctx, NULL, num-1,
crypto/x509/x509_vfy.c-3338-                              X509_V_ERR_UNABLE_TO_GET_ISSUER_CERT_LOCALLY);
```

I can't tell you why we do it differently in different spots, and haven't the time right now to dig further for the moment.  It's possible that there is no other way in some cases.
This is solved for the master but not yet for 1.1.1.
Fixed for me. :-) Thank you very much!
> Fixed for me. :-) Thank you very much!

You are welcome.
Glad that this backport was accepted during the OTC vote -  with a very narrow margin.
As a note - we're hitting this change as a regression in apache/serf - our test cases with a leaf (no CA chain present) are now seeing the verify_cb_cert get triggered multiple times.  The first time receives the failure, the second time receives success.  Since we're only expecting to receive invalid certs, we're treating the second call as an unexpected success...

Up to and including 1.1.1h, OpenSSL would stop processing the certificate after sending along the leaf error to the app callback.  In -stable and master, if the app's callback doesn't return a failure, it will then proceed to the date check portion (check_cert_time) - which then triggers a successful verification app callback.

So, I'm guessing the change is that downstream apps have to forcibly error out from our callback if we want to abort?  Are we the only client that this will trip up?  It might be good to denote this in the Changelogs - 1.1.1h was fine, but 1.1.1i wasn't.  (Maybe the accidental cert breakage in 1.1.1h masked this issue from surfacing earlier?)

dev@serf.apache.org thread:
https://lists.apache.org/thread.html/r7dcac67871b30cc565dc7655414b8ccbe2f08051aa0f17de9c0b53e8%40%3Cdev.serf.apache.org%3E

Debian bug tracker link:
https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=978353
In the callback you decide what OpenSSL should do: ignore the error and continue or abort. It seems that you're telling it to ignore the error.

I'm not sure how the test works, but X509_verify_cert() should return the same if the certificate has not expired.