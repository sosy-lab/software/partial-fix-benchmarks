build fails in test_encoder_decoder with OPENSSL_TEST_RAND_ORDER=0
Could you point at the job that failed like this?  I have a hard time reproducing...
E.g. see:

https://github.com/openssl/openssl/pull/13508/checks?check_run_id=1452342581
Any recent PR:
https://github.com/openssl/openssl/runs/1452342581?check_suite_focus=true
https://github.com/openssl/openssl/runs/1451802233?check_suite_focus=true
https://github.com/openssl/openssl/runs/1449402656?check_suite_focus=true

Yeah ok...  I tried that configuration on my laptop, and those test pass flawlessly.
(and eh gods ubsan is slow)
I also failed to reproduce this when I tried.
According to the logs the environment this run in is Ubuntu 18.04:

> Environment: ubuntu-18.04

So, I tried installing an Ubuntu 18.04 VM to see if I could replicate it there. I don't see this failure there either. :-(

I'm currently thinking we should disable ubsan in the CIs (but keep asan). It runs horrifically slowly and ubsan is covered by a run-checker job. Of course that doesn't solve the root cause of this problem, but since no one can replicate it I'm not sure what else we can do.
> I'm currently thinking we should disable ubsan in the CIs (but keep asan).

#13514
This failure also kept nagging me.
I just found the key to reproduce it locally: `OPENSSL_TEST_RAND_ORDER=0`, e.g.,
```
test TESTS="test_encoder_decoder" OPENSSL_TEST_RAND_ORDER=0
```
This is independent of enabling sanitizers.
The test harness prints out the random seed used but it doesn't seem to appear in the CI failures.  That _0_ reproduces the problem is good but I doubt it's the actual seed.

I've created the issue #13572 to track this separately.


`OPENSSL_TEST_RAND_ORDER` being zero will not be 100% reliable.  This uses `time(2)` as the seed.
@levitte, since the order of (sub-)test execution plays a role and due to output given:
```
'OSSL_ENCODER_CTX_get_num_encoders(ectx) > 0' failed
```
I suspect that the list of encoders is not (re-)initialized properly.
This test works fine with the new minimal configuration where essentially only RSA is enabled:
```
HARNESS_JOBS=1 V=1 make -s -j4 test TESTS=test_encoder_decoder OPENSSL_TEST_RAND_ORDER=0
```
yields
```
04-test_encoder_decoder.t .. 
# The results of this test will end up in test-runs/test_encoder_decoder
1..1
    # INFO:  @ test/endecode_test.c:1209
    # Generating keys...
    # INFO:  @ test/endecode_test.c:1235
    # Generating keys...RSA done
    # INFO:  @ test/endecode_test.c:1237
    # Generating keys...RSA_PSS done
    # Subtest: ../../test/endecode_test
    1..14
    ok 1 - test_protected_RSA_via_DER
    ok 2 - test_unprotected_RSA_PSS_via_DER
    ok 3 - test_protected_RSA_PSS_via_DER
    ok 4 - test_protected_RSA_PSS_via_PEM
    ok 5 - test_unprotected_RSA_via_DER
    ok 6 - test_protected_RSA_via_PEM
    ok 7 - test_unprotected_RSA_via_PEM
    ok 8 - test_unprotected_RSA_via_legacy_PEM
    ok 9 - test_protected_RSA_via_legacy_PEM
    ok 10 - test_public_RSA_via_PEM
    ok 11 - test_public_RSA_PSS_via_PEM
    ok 12 - test_public_RSA_via_DER
    ok 13 - test_public_RSA_PSS_via_DER
    ok 14 - test_unprotected_RSA_PSS_via_PEM
../../util/wrap.pl ../../test/endecode_test => 0
ok 1
```

@levitte could you please fix this ASAP? It is still causing build trouble.
I'm running tests now.  I can't see any reason why `no-asm` should make any difference, so I'm trying without that to avoid the abysmal slowness...
Wheee, issue reproduced!

This is really mysterious.  From a trace I did, this seems to hit encoders for RSA and DSA only, and it seems like some sort of internal state somewhere is flipped, because getting the encoder implementations seems to work fine 2 or 3 times, and then fail constantly for the rest of the run.
Yeah, I still have the suspicion this is due to incomplete (re-)initialization,
because in canonical order (of the algorithms) in test execution it does not show up.
> Yeah, I still have the suspicion this is due to incomplete (re-)initialization,

Yeah, I'm thinking the same...  now, I just need to find the spot where that happens
A little bit more tracing showed that the keys for which this happens have libctx == NULL, and further investigation shows that these keys are legacy EVP_PKEYs.

I guess the big question is, why is that order sensitive???
~Next observation; it looks like it's `test_protected_RSA_via_PVK` that triggers this weirdness.  Every run I do work wonders until that one, then there are random failures following, all for RSA and DSA keys~

UPDATE: Nope, it's a little wider than that, but seems to be revolving around MSBLOB and PVK
Found one cause!  `do_i2b` (in `crypto/pem/pvkfmt.c`) forcedly downgrades the input pkey.  That needs being corrected...
Found another troublesome bit, where the EVP_PKEY operation cache got too filled up.  That's unfortunately a result of calling `EVP_KEYMGM_provided_do_all()` and using the result to "fetch" new keymgmt methods.

That's an ouch on a much more fundamental level.  Will require some pondering.
> That's an ouch on a much more fundamental level. Will require some pondering.

Pondered, and I believe that the proper solution is not to save away all the KEYMGMT instances that were found in the `OSSL_DECODER_CTX_new_by_EVP_PKEY()`, and instead fetch them later, when in the EVP_PKEY constructor callback.  That will also make better use of the property query string.
Tentative fix in #13661...  although I'm hitting trouble when I run tests, and would need some help figuring that out.
> ... and would need some help figuring that out.

I actually figured it out.