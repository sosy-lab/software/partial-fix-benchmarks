 why  SSL3_BUFFER_get_left(wb) != 0  when DTLSv1_2 SSL_write fail ?
Which version of OpenSSL did you see this behaviour in?
Looking at the code in master, I just can't see a route through the code that would end up triggering this assertion. So, its a bit of a mystery at the moment. If you let me know the specific version you are using then I can check that version.

Can you easily reproduce this issue?
I got the stack in openssl-1.1.1f， and found that the code openssl-1.1.1f is the same as openssl-3.0.0. I do not understand it that stack had  fail.
I realize that this stack may be caused by SSL_write in multi threads, SSL_write do not have mutex. If it is a single thread, will the code （```openssl/ssl/record/rec_layer_d1.c:807```） not  go?  What is the purpose of adding this code?
Ah! An SSL object must *not* be shared across multiple threads. So, it is fine to call SSL_write in different threads - but each thread must be using a different SSL object (or you must implement appropriate locking to prevent concurrent access to the same SSL object).

> If it is a single thread, will the code （openssl/ssl/record/rec_layer_d1.c:807） not go? What is the purpose of adding this code?

Right - in a single threaded application that assertion should never fail. So the code is a sanity check.
@mattcaswell 
https://github.com/openssl/openssl/blob/2d840893e78253bcce428603fdbcda159bdebe08/ssl/record/rec_layer_d1.c#L801-L810
The comment（```openssl/ssl/record/rec_layer_d1.c:803-804```)  is not   proper.
Thank you！