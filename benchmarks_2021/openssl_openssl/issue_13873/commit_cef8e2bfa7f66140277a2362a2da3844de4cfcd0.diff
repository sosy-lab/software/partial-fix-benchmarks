diff --git a/crypto/ocsp/ocsp_http.c b/crypto/ocsp/ocsp_http.c
index c5508698c838..12404d58e0d2 100644
--- a/crypto/ocsp/ocsp_http.c
+++ b/crypto/ocsp/ocsp_http.c
@@ -13,29 +13,35 @@
 
 #ifndef OPENSSL_NO_OCSP
 
-# ifndef OPENSSL_NO_DEPRECATED_3_0
 int OCSP_REQ_CTX_set1_req(OSSL_HTTP_REQ_CTX *rctx, const OCSP_REQUEST *req)
 {
     return OSSL_HTTP_REQ_CTX_i2d(rctx, "application/ocsp-request",
                                  ASN1_ITEM_rptr(OCSP_REQUEST),
                                  (ASN1_VALUE *)req);
 }
-# endif
 
 OSSL_HTTP_REQ_CTX *OCSP_sendreq_new(BIO *io, const char *path,
-                                    OCSP_REQUEST *req, int maxline)
+                                    const OCSP_REQUEST *req, int maxline)
 {
-    BIO *req_mem = HTTP_asn1_item2bio(ASN1_ITEM_rptr(OCSP_REQUEST),
-                                      (ASN1_VALUE *)req);
-    OSSL_HTTP_REQ_CTX *res =
-        HTTP_REQ_CTX_new(io, io, 0 /* no HTTP proxy used */, NULL, NULL, path,
-                         NULL /* headers */, "application/ocsp-request",
-                         req_mem /* may be NULL */,
-                         maxline, 0 /* default max_resp_len */,
-                         0 /* no timeout, blocking indefinite */, NULL,
-                         1 /* expect_asn1 */);
-    BIO_free(req_mem);
-    return res;
+    OSSL_HTTP_REQ_CTX *rctx = NULL;
+
+    if ((rctx = OSSL_HTTP_REQ_CTX_new(io, io, 1 /* POST */,
+                                      maxline, 0 /* default max_resp_len */,
+                                      0 /* no timeout, blocking indefinitely */,
+                                      NULL, 1 /* expect_asn1 */)) == NULL)
+        return NULL;
+
+    if (!OSSL_HTTP_REQ_CTX_set_request_line(rctx, NULL, NULL, path))
+        goto err;
+
+    if (req != NULL && !OCSP_REQ_CTX_set1_req(rctx, req))
+        goto err;
+
+    return rctx;
+
+ err:
+    OSSL_HTTP_REQ_CTX_free(rctx);
+    return NULL;
 }
 
 int OCSP_sendreq_nbio(OCSP_RESPONSE **presp, OSSL_HTTP_REQ_CTX *rctx)
diff --git a/doc/man3/OCSP_sendreq_new.pod b/doc/man3/OCSP_sendreq_new.pod
index 6e346bdd445f..d8f30bc8b008 100644
--- a/doc/man3/OCSP_sendreq_new.pod
+++ b/doc/man3/OCSP_sendreq_new.pod
@@ -3,13 +3,13 @@
 =head1 NAME
 
 OCSP_sendreq_new,
+OCSP_REQ_CTX_set1_req,
 OCSP_sendreq_nbio,
 OCSP_sendreq_bio,
 OCSP_REQ_CTX_i2d,
 OCSP_REQ_CTX_add1_header,
 OCSP_REQ_CTX_free,
-OCSP_set_max_response_length,
-OCSP_REQ_CTX_set1_req
+OCSP_set_max_response_length
 - OCSP responder query functions
 
 =head1 SYNOPSIS
@@ -17,7 +17,8 @@ OCSP_REQ_CTX_set1_req
  #include <openssl/ocsp.h>
 
  OSSL_HTTP_REQ_CTX *OCSP_sendreq_new(BIO *io, const char *path,
-                                     OCSP_REQUEST *req, int maxline);
+                                     const OCSP_REQUEST *req, int maxline);
+ int OCSP_REQ_CTX_set1_req(OSSL_HTTP_REQ_CTX *rctx, const OCSP_REQUEST *req);
 
  int OCSP_sendreq_nbio(OCSP_RESPONSE **presp, OSSL_HTTP_REQ_CTX *rctx);
 
@@ -27,26 +28,23 @@ Deprecated since OpenSSL 3.0, can be hidden entirely by defining
 B<OPENSSL_API_COMPAT> with a suitable version value, see
 L<openssl_user_macros(7)>:
 
- int OCSP_REQ_CTX_i2d(OCSP_REQ_CT *rctx, const char *content_type,
-                      const ASN1_ITEM *it, ASN1_VALUE *req);
+ int OCSP_REQ_CTX_i2d(OCSP_REQ_CT *rctx, const ASN1_ITEM *it, ASN1_VALUE *req);
  int OCSP_REQ_CTX_add1_header(OCSP_REQ_CT *rctx,
                               const char *name, const char *value);
  void OCSP_REQ_CTX_free(OCSP_REQ_CTX *rctx);
  void OCSP_set_max_response_length(OCSP_REQ_CT *rctx,
                                    unsigned long len);
- int OCSP_REQ_CTX_set1_req(OCSP_REQ_CTX *rctx, const OCSP_REQUEST *req);
 
 =head1 DESCRIPTION
 
-These functions perform an OCSP request / response transfer over HTTP, using
-the HTTP request functions described in L<OSSL_HTTP_REQ_CTX(3)>.
+These functions perform an OCSP POST request / response transfer over HTTP,
+using the HTTP request functions described in L<OSSL_HTTP_REQ_CTX(3)>.
 
 The function OCSP_sendreq_new() builds a complete B<OSSL_HTTP_REQ_CTX>
 structure using connection B<BIO> I<io>, the URL path I<path>, the OCSP
-request I<req> and with a response header maximum line length of I<maxline>.
-If I<maxline> is zero a default value of 4k is used. The OCSP request I<req>
-may be set to NULL and provided later with L<OSSL_HTTP_REQ_CTX_i2d(3)> if
-required.
+request I<req>, and with a response header maximum line length of I<maxline>.
+If I<maxline> is zero a default value of 4k is used.
+The I<req> may be set to NULL and provided later using OCSP_REQ_CTX_set1_req().
 
 The I<io> and I<path> arguments to OCSP_sendreq_new() correspond to the
 components of the URL.
@@ -54,6 +52,10 @@ For example if the responder URL is C<http://example.com/ocspreq> the BIO
 I<io> should be connected to host C<example.com> on port 80 and I<path>
 should be set to C</ocspreq>.
 
+OCSP_REQ_CTX_set1_req finalizes the HTTP request context I<rctx>
+by adding the HTTP header C<Content-Type> C<application/ocsp-request>
+and the DER encoding of the OCSP request I<req>.
+
 OCSP_sendreq_nbio() performs I/O on the OCSP request context I<rctx>.
 When the operation is complete it assigns the response, a pointer to a
 B<OCSP_RESPONSE> structure, in I<*presp>.
@@ -64,15 +66,13 @@ response header maximum line length 4k. It waits indefinitely on a response.
 It does not support setting a timeout or adding headers and is retained
 for compatibility; use OCSP_sendreq_nbio() instead.
 
-OCSP_REQ_CTX_set1_req(rctx, req) is equivalent to the following:
+OCSP_REQ_CTX_i2d(rctx, it, req) is equivalent to the following:
 
- OSSL_HTTP_REQ_CTX_i2d(rctx, "application/ocsp-request",
-                       ASN1_ITEM_rptr(OCSP_REQUEST), (ASN1_VALUE *)req)
+  OSSL_HTTP_REQ_CTX_i2d(rctx, "application/ocsp-request", it, req)
 
 The other deprecated type and functions have been superseded by the
 following equivalents:
 B<OCSP_REQ_CTX> by L<OSSL_HTTP_REQ_CTX(3)>,
-OCSP_REQ_CTX_i2d() by L<OSSL_HTTP_REQ_CTX_i2d(3)>,
 OCSP_REQ_CTX_add1_header() by L<OSSL_HTTP_REQ_CTX_add1_header(3)>,
 OCSP_REQ_CTX_free() by L<OSSL_HTTP_REQ_CTX_free(3)>, and
 OCSP_set_max_response_length() by
@@ -83,7 +83,7 @@ L<OSSL_HTTP_REQ_CTX_set_max_response_length(3)>.
 OCSP_sendreq_new() returns a valid B<OSSL_HTTP_REQ_CTX> structure or NULL
 if an error occurred.
 
-OCSP_sendreq_nbio(), OCSP_REQ_CTX_i2d(), and OCSP_REQ_CTX_set1_req()
+OCSP_REQ_CTX_set1_req(), OCSP_sendreq_nbio(), and OCSP_REQ_CTX_i2d()
 return 1 for success and 0 for failure.
 
 OCSP_sendreq_bio() returns the B<OCSP_RESPONSE> structure sent by the
@@ -91,7 +91,7 @@ responder or NULL if an error occurred.
 
 =head1 SEE ALSO
 
-L<crypto(7)>,
+L<OSSL_HTTP_REQ_CTX(3)>
 L<OCSP_cert_to_id(3)>,
 L<OCSP_request_add1_nonce(3)>,
 L<OCSP_REQUEST_new(3)>,
@@ -104,8 +104,7 @@ B<OCSP_REQ_CTX>,
 OCSP_REQ_CTX_i2d(),
 OCSP_REQ_CTX_add1_header(),
 OCSP_REQ_CTX_free(),
-OCSP_set_max_response_length(),
-and OCSP_REQ_CTX_set1_req()
+and OCSP_set_max_response_length()
 were deprecated in OpenSSL 3.0.
 
 =head1 COPYRIGHT
diff --git a/doc/man3/OSSL_HTTP_REQ_CTX.pod b/doc/man3/OSSL_HTTP_REQ_CTX.pod
index 4e30088de7b7..5de9fa5fe5a1 100644
--- a/doc/man3/OSSL_HTTP_REQ_CTX.pod
+++ b/doc/man3/OSSL_HTTP_REQ_CTX.pod
@@ -12,7 +12,7 @@ OSSL_HTTP_REQ_CTX_nbio,
 OSSL_HTTP_REQ_CTX_sendreq_d2i,
 OSSL_HTTP_REQ_CTX_get0_mem_bio,
 OSSL_HTTP_REQ_CTX_set_max_response_length
-- HTTP request functions
+- HTTP client low-level functions
 
 =head1 SYNOPSIS
 
diff --git a/include/openssl/ocsp.h.in b/include/openssl/ocsp.h.in
index 76f7c1602ac9..2571c24eda49 100644
--- a/include/openssl/ocsp.h.in
+++ b/include/openssl/ocsp.h.in
@@ -172,24 +172,23 @@ DECLARE_ASN1_DUP_FUNCTION(OCSP_CERTID)
 
 OCSP_RESPONSE *OCSP_sendreq_bio(BIO *b, const char *path, OCSP_REQUEST *req);
 OSSL_HTTP_REQ_CTX *OCSP_sendreq_new(BIO *io, const char *path,
-                                    OCSP_REQUEST *req, int maxline);
+                                    const OCSP_REQUEST *req, int maxline);
 int OCSP_sendreq_nbio(OCSP_RESPONSE **presp, OSSL_HTTP_REQ_CTX *rctx);
+int OCSP_REQ_CTX_set1_req(OSSL_HTTP_REQ_CTX *rctx, const OCSP_REQUEST *req);
 
 #  ifndef OPENSSL_NO_DEPRECATED_3_0
 typedef OSSL_HTTP_REQ_CTX OCSP_REQ_CTX;
-OSSL_DEPRECATEDIN_3_0
-int OCSP_REQ_CTX_set1_req(OSSL_HTTP_REQ_CTX *rctx, const OCSP_REQUEST *req);
 
-#   define OCSP_REQ_CTX_new(wb, rb, m, ml, mrl, t, ect, ea) \
-        OSSL_HTTP_REQ_CTX_new(wb, rb, m, ml, mrl, t, ect, ea)
+#   define OCSP_REQ_CTX_new(io, maxline)  \
+        OSSL_HTTP_REQ_CTX_new(io, io, 1, maxline, 0, 0, NULL, 1)
 #   define OCSP_REQ_CTX_free(r) \
         OSSL_HTTP_REQ_CTX_free(r)
-#   define OCSP_REQ_CTX_http(r, s, p, path) \
-        OSSL_HTTP_REQ_CTX_set_request_line(r, s, p, path)
+#   define OCSP_REQ_CTX_http(rctx, op, path) \
+        OSSL_HTTP_REQ_CTX_set_request_line(rctx, NULL, NULL, path)
 #   define OCSP_REQ_CTX_add1_header(r, n, v) \
         OSSL_HTTP_REQ_CTX_add1_header(r, n, v)
-#   define OCSP_REQ_CTX_i2d(r, c, i, req) \
-        OSSL_HTTP_REQ_CTX_i2d(r, c, i, req)
+#   define OCSP_REQ_CTX_i2d(r, i, req) \
+        OSSL_HTTP_REQ_CTX_i2d(r, "application/ocsp-request", i, req)
 #   define OCSP_REQ_CTX_nbio(r) \
         OSSL_HTTP_REQ_CTX_nbio(r)
 #   define OCSP_REQ_CTX_nbio_d2i(r, i) \
diff --git a/util/libcrypto.num b/util/libcrypto.num
index 8f621eced5c0..e6d5b1d67bef 100644
--- a/util/libcrypto.num
+++ b/util/libcrypto.num
@@ -3334,7 +3334,7 @@ EVP_PKEY_meth_get_verify                3403	3_0_0	EXIST::FUNCTION:DEPRECATEDIN_
 CRYPTO_128_wrap                         3404	3_0_0	EXIST::FUNCTION:
 X509_STORE_set_lookup_crls              3405	3_0_0	EXIST::FUNCTION:
 EVP_CIPHER_meth_get_ctrl                3406	3_0_0	EXIST::FUNCTION:DEPRECATEDIN_3_0
-OCSP_REQ_CTX_set1_req                   3407	3_0_0	EXIST::FUNCTION:DEPRECATEDIN_3_0,OCSP
+OCSP_REQ_CTX_set1_req                   3407	3_0_0	EXIST::FUNCTION:OCSP
 CONF_imodule_get_usr_data               3408	3_0_0	EXIST::FUNCTION:
 CRYPTO_new_ex_data                      3409	3_0_0	EXIST::FUNCTION:
 PEM_read_PKCS8_PRIV_KEY_INFO            3410	3_0_0	EXIST::FUNCTION:STDIO
