BIO_write/mem_write behavior change in OpenSSL 1.1
This regression was added in e5bf3c923c184b19e8c3ef7043080955479a2325. The commit doesn't discuss the behavior change. Was it accidental?
Almost certainly accidental. I can't match up your proposed patch with the current code. Perhaps you'd like to raise this as a PR rather than just an issue?
"referenced" this issue. It is a commit in the client code and github saw the url and updated this comment. So you see the "negative" effect on client code... It is also not how "write(2)" syscalls behave..
I'm quite amazed this hasn't been discovered before now (ok, half a year ago, but on a scale of 20ish years, that's more or less "now").  Fixing it is easy.