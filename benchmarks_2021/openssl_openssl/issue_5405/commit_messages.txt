bio_b64.c: prevent base64 filter BIO from decoding out-of-bound data

Fixes #5405, #1381

The base64 filter BIO reads its input in chunks of B64_BLOCK_SIZE bytes.
When processing input in PEM format it can happen in rare cases that

- the trailing PEM marker crosses the boundary of a chunk, and
- the beginning of the following chunk contains valid base64 encoded data.

This happened in issue #5405, where the PEM marker was split into
"-----END CER" and "TIFICATE-----" at the end of the first chunk.

The decoding of the first chunk terminated correctly at the '-' character,
which is treated as an EOF marker, and b64_read() returned. However,
when called the second time, b64_read() read the next chunk and interpreted
the string "TIFICATE" as valid base64 encoded data, adding 6 extra bytes
'4c 81 48 08 04 c4'.

This patch restores the assignment of the error code to 'ctx->cont', which
was deleted accidentally in commit 5562cfaca4f3 and which prevents b64_read()
from reading additional data on subsequent calls.

This issue was observed and reported by Annie Yousar.
bio_b64.c: prevent base64 filter BIO from decoding out-of-bound data

Fixes #5405, #1381

The base64 filter BIO reads its input in chunks of B64_BLOCK_SIZE bytes.
When processing input in PEM format it can happen in rare cases that

- the trailing PEM marker crosses the boundary of a chunk, and
- the beginning of the following chunk contains valid base64 encoded data.

This happened in issue #5405, where the PEM marker was split into
"-----END CER" and "TIFICATE-----" at the end of the first chunk.

The decoding of the first chunk terminated correctly at the '-' character,
which is treated as an EOF marker, and b64_read() returned. However,
when called the second time, b64_read() read the next chunk and interpreted
the string "TIFICATE" as valid base64 encoded data, adding 6 extra bytes
'4c 81 48 08 04 c4'.

This patch restores the assignment of the error code to 'ctx->cont', which
was deleted accidentally in commit 5562cfaca4f3 and which prevents b64_read()
from reading additional data on subsequent calls.

This issue was observed and reported by Annie Yousar.

Reviewed-by: Richard Levitte <levitte@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/5422)
bio_b64.c: prevent base64 filter BIO from decoding out-of-bound data

Fixes #5405, #1381

The base64 filter BIO reads its input in chunks of B64_BLOCK_SIZE bytes.
When processing input in PEM format it can happen in rare cases that

- the trailing PEM marker crosses the boundary of a chunk, and
- the beginning of the following chunk contains valid base64 encoded data.

This happened in issue #5405, where the PEM marker was split into
"-----END CER" and "TIFICATE-----" at the end of the first chunk.

The decoding of the first chunk terminated correctly at the '-' character,
which is treated as an EOF marker, and b64_read() returned. However,
when called the second time, b64_read() read the next chunk and interpreted
the string "TIFICATE" as valid base64 encoded data, adding 6 extra bytes
'4c 81 48 08 04 c4'.

This patch restores the assignment of the error code to 'ctx->cont', which
was deleted accidentally in commit 5562cfaca4f3 and which prevents b64_read()
from reading additional data on subsequent calls.

This issue was observed and reported by Annie Yousar.

Reviewed-by: Richard Levitte <levitte@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/5422)
bio_b64.c: prevent base64 filter BIO from decoding out-of-bound data

Fixes #5405, #1381

The base64 filter BIO reads its input in chunks of B64_BLOCK_SIZE bytes.
When processing input in PEM format it can happen in rare cases that

- the trailing PEM marker crosses the boundary of a chunk, and
- the beginning of the following chunk contains valid base64 encoded data.

This happened in issue #5405, where the PEM marker was split into
"-----END CER" and "TIFICATE-----" at the end of the first chunk.

The decoding of the first chunk terminated correctly at the '-' character,
which is treated as an EOF marker, and b64_read() returned. However,
when called the second time, b64_read() read the next chunk and interpreted
the string "TIFICATE" as valid base64 encoded data, adding 6 extra bytes
'4c 81 48 08 04 c4'.

This patch restores the assignment of the error code to 'ctx->cont', which
was deleted accidentally in commit 5562cfaca4f3 and which prevents b64_read()
from reading additional data on subsequent calls.

This issue was observed and reported by Annie Yousar.

Reviewed-by: Richard Levitte <levitte@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/5422)
