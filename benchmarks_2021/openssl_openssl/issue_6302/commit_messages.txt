apps/speed: fix possible OOB access in some EC arrays

because there are actually 17 curves defined, but only 16 are plugged for
ecdsa test.
Deduce array size using OSSL_NELEM and so remove various magic numbers,
which required some declarations moving.
Implement OPT_PAIR list search without a null-ending element.
Fix some comparison between signed and unsigned integer expressions.

cherry-picking from commit 5c6a69f539.

Partial Back-port of #6133 to 1.1.0

Reviewed-by: Andy Polyakov <appro@openssl.org>
Reviewed-by: Rich Salz <rsalz@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/6245)
Fix a possible crash in BN_from_montgomery_word

Thanks to Darovskikh Andrei for for reporting this issue.

Fixes: #5785
Fixes: #6302

Cherry-picked from f91e026e3832 (without test/bntest.c)
Fix a possible crash in BN_from_montgomery_word

Thanks to Darovskikh Andrei for for reporting this issue.

Fixes: #5785
Fixes: #6302

Cherry-picked from f91e026e3832 (without test/bntest.c)

Reviewed-by: Rich Salz <rsalz@openssl.org>
Reviewed-by: Matthias St. Pierre <Matthias.St.Pierre@ncp-e.com>
(Merged from https://github.com/openssl/openssl/pull/6310)
Improve compatibility of point and curve checks

We check that the curve name associated with the point is the same as that
for the curve.

Fixes #6302
Improve compatibility of point and curve checks

We check that the curve name associated with the point is the same as that
for the curve.

Fixes #6302

Reviewed-by: Rich Salz <rsalz@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/6323)

(cherry picked from commit b14e60155009f4f1d168e220fa01cd2b75557b72)
Improve compatibility of point and curve checks

We check that the curve name associated with the point is the same as that
for the curve.

Fixes #6302

Reviewed-by: Rich Salz <rsalz@openssl.org>
(Merged from https://github.com/openssl/openssl/pull/6323)
