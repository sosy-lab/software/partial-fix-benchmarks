Avoid NULL pointer dereference. Fixes #9043.
Avoid NULL pointer dereference. Fixes #9043.

Reviewed-by: Paul Dale <paul.dale@oracle.com>
(Merged from https://github.com/openssl/openssl/pull/9059)
Avoid NULL pointer dereference. Fixes #9043.

Reviewed-by: Paul Dale <paul.dale@oracle.com>
(Merged from https://github.com/openssl/openssl/pull/9059)
Avoid NULL pointer dereference. Fixes #9043.

Reviewed-by: Paul Dale <paul.dale@oracle.com>
(Merged from https://github.com/openssl/openssl/pull/9059)

(cherry picked from commit 9fd6f7d1cd2a3c8e2bc69dcb8bde8406eb6c2623)
Avoid NULL pointer dereference.

[manual merge from #9059 to 1.1.0]

Fixes: #9043
Avoid NULL pointer dereference.

[manual merge from #9059 to 1.1.0]

Fixes: #9043

Reviewed-by: Dmitry Belyavskiy <beldmit@gmail.com>
(Merged from https://github.com/openssl/openssl/pull/9322)
