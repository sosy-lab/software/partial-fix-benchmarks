diff --git a/src/opencl/nt_kernel.cl b/src/opencl/nt_kernel.cl
index 6a3496de76..594f0ee65c 100644
--- a/src/opencl/nt_kernel.cl
+++ b/src/opencl/nt_kernel.cl
@@ -22,8 +22,12 @@
 #include "opencl_device_info.h"
 #define AMD_PUTCHAR_NOCAST
 #include "opencl_misc.h"
+#include "opencl_unicode.h"
 #include "opencl_mask.h"
 
+/* If defined, we do not support full UTF-16 with surrogate pairs */
+//#define UCS_2
+
 //Init values
 #define INIT_A 0x67452301
 #define INIT_B 0xefcdab89
@@ -36,7 +40,7 @@
 #if BITMAP_SIZE_BITS_LESS_ONE < 0xffffffff
 #define BITMAP_SIZE_BITS (BITMAP_SIZE_BITS_LESS_ONE + 1)
 #else
-/*undefined, cause error.*/
+#error BITMAP_SIZE_BITS_LESS_ONE too large
 #endif
 
 inline void nt_crypt(__private uint *hash, __private uint *nt_buffer, uint md4_size) {
@@ -156,19 +160,104 @@ inline void nt_crypt(__private uint *hash, __private uint *nt_buffer, uint md4_s
 	hash[2] += (hash[3] ^ hash[0] ^ tmp) + nt_buffer[7]  + SQRT_3; hash[2] = rotate(hash[2] , 11u);
 }
 
-inline void prepare_key(__global uint * key, uint length, uint * nt_buffer)
+#if ISO_8859_1 || ASCII
+#define LUT(c) (c)
+#else
+#define LUT(c) (((c) < 0x80) ? (c) : cp[(c) & 0x7f])
+#endif
+
+#if UTF_8
+inline uint prepare_key(__global uint *key, uint length, uint *nt_buffer)
+{
+	const __global UTF8 *source = (const __global uchar*)key;
+	const __global UTF8 *sourceEnd = &source[length];
+	UTF16 *target = (UTF16*)nt_buffer;
+	const UTF16 *targetEnd = &target[PLAINTEXT_LENGTH];
+	UTF32 ch;
+	uint extraBytesToRead;
+
+	/* Input buffer is UTF-8 without zero-termination */
+	while (source < sourceEnd) {
+		if (*source < 0xC0) {
+			*target++ = (UTF16)*source++;
+			if (source >= sourceEnd || target >= targetEnd) {
+				break;
+			}
+			continue;
+		}
+		ch = *source;
+		// This point must not be reached with *source < 0xC0
+		extraBytesToRead =
+			opt_trailingBytesUTF8[ch & 0x3f];
+		if (source + extraBytesToRead >= sourceEnd) {
+			break;
+		}
+		switch (extraBytesToRead) {
+		case 3:
+			ch <<= 6;
+			ch += *++source;
+		case 2:
+			ch <<= 6;
+			ch += *++source;
+		case 1:
+			ch <<= 6;
+			ch += *++source;
+			++source;
+			break;
+		default:
+			*target = 0x80;
+			break; // from switch
+		}
+		if (*target == 0x80)
+			break; // from while
+		ch -= offsetsFromUTF8[extraBytesToRead];
+#ifdef UCS_2
+		/* UCS-2 only */
+		*target++ = (UTF16)ch;
+#else
+		/* full UTF-16 with surrogate pairs */
+		if (ch <= UNI_MAX_BMP) {  /* Target is a character <= 0xFFFF */
+			*target++ = (UTF16)ch;
+		} else {  /* target is a character in range 0xFFFF - 0x10FFFF. */
+			if (target + 1 >= targetEnd)
+				break;
+			ch -= halfBase;
+			*target++ = (UTF16)((ch >> halfShift) + UNI_SUR_HIGH_START);
+			*target++ = (UTF16)((ch & halfMask) + UNI_SUR_LOW_START);
+		}
+#endif
+		if (source >= sourceEnd || target >= targetEnd)
+			break;
+	}
+	*target = 0x80;	// Terminate
+
+	//dump_stuff_msg("buf", nt_buffer, 64);
+
+	return (uint)(target - (UTF16*)nt_buffer);
+}
+
+#else
+
+inline uint prepare_key(const __global uint *key, uint length, uint *nt_buffer)
 {
-	uint i = 0, nt_index, keychars;
+	uint i, nt_index, keychars;
+
 	nt_index = 0;
 	for (i = 0; i < (length + 3)/ 4; i++) {
 		keychars = key[i];
-		nt_buffer[nt_index++] = (keychars & 0xFF) | (((keychars >> 8) & 0xFF) << 16);
-		nt_buffer[nt_index++] = ((keychars >> 16) & 0xFF) | ((keychars >> 24) << 16);
+		nt_buffer[nt_index++] = LUT(keychars & 0xFF) | (LUT((keychars >> 8) & 0xFF) << 16);
+		nt_buffer[nt_index++] = LUT((keychars >> 16) & 0xFF) | (LUT(keychars >> 24) << 16);
 	}
 	nt_index = length >> 1;
 	nt_buffer[nt_index] = (nt_buffer[nt_index] & 0xFF) | (0x80 << ((length & 1) << 4));
+
+	//dump_stuff_msg("buf", nt_buffer, 64);
+
+	return length;
 }
 
+#endif /* UTF_8 */
+
 inline void cmp_final(uint gid,
 		uint iter,
 		__private uint *hash,
@@ -354,25 +443,25 @@ __kernel void nt(__global uint *keys,
 #endif
 
 	keys += base >> 6;
-	prepare_key(keys, md4_size, nt_buffer);
+	md4_size = prepare_key(keys, md4_size, nt_buffer);
 	md4_size = md4_size << 4;
 
 	for (i = 0; i < NUM_INT_KEYS; i++) {
 #if NUM_INT_KEYS > 1
-		PUTSHORT(nt_buffer, GPU_LOC_0, (int_keys[i] & 0xff));
+		PUTSHORT(nt_buffer, GPU_LOC_0, LUT(int_keys[i] & 0xff));
 #if 1 < MASK_FMT_INT_PLHDR
 #if LOC_1 >= 0
-		PUTSHORT(nt_buffer, GPU_LOC_1, ((int_keys[i] & 0xff00) >> 8));
+		PUTSHORT(nt_buffer, GPU_LOC_1, LUT((int_keys[i] & 0xff00) >> 8));
 #endif
 #endif
 #if 2 < MASK_FMT_INT_PLHDR
 #if LOC_2 >= 0
-		PUTSHORT(nt_buffer, GPU_LOC_2, ((int_keys[i] & 0xff0000) >> 16));
+		PUTSHORT(nt_buffer, GPU_LOC_2, LUT((int_keys[i] & 0xff0000) >> 16));
 #endif
 #endif
 #if 3 < MASK_FMT_INT_PLHDR
 #if LOC_3 >= 0
-		PUTSHORT(nt_buffer, GPU_LOC_3, ((int_keys[i] & 0xff000000) >> 24));
+		PUTSHORT(nt_buffer, GPU_LOC_3, LUT((int_keys[i] & 0xff000000) >> 24));
 #endif
 #endif
 #endif
diff --git a/src/opencl_nt_fmt_plug.c b/src/opencl_nt_fmt_plug.c
index b6eb4c04cd..f30c0a1591 100644
--- a/src/opencl_nt_fmt_plug.c
+++ b/src/opencl_nt_fmt_plug.c
@@ -40,29 +40,37 @@ john_register_one(&fmt_opencl_NT);
 #include "common-opencl.h"
 #include "config.h"
 #include "options.h"
+#include "unicode.h"
 #include "mask_ext.h"
 #include "bt_interface.h"
 
-#define FORMAT_LABEL		"nt-opencl"
-#define FORMAT_NAME		"NT"
-#define ALGORITHM_NAME		"MD4 OpenCL"
-#define BENCHMARK_COMMENT	""
-#define BENCHMARK_LENGTH	-1
-#define PLAINTEXT_LENGTH	27
-#define BUFSIZE                 ((PLAINTEXT_LENGTH+3)/4*4)
-#define CIPHERTEXT_LENGTH	32
-#define BINARY_SIZE		16
-#define BINARY_ALIGN		MEM_ALIGN_WORD
-#define SALT_SIZE		0
-#define SALT_ALIGN		1
-
-#define MIN_KEYS_PER_CRYPT	1
-#define MAX_KEYS_PER_CRYPT	1
-
+#define FORMAT_LABEL        "nt-opencl"
+#define FORMAT_NAME         "NT"
+#define ALGORITHM_NAME      "MD4 OpenCL"
+#define BENCHMARK_COMMENT   ""
+#define BENCHMARK_LENGTH    -1
+#define PLAINTEXT_LENGTH    27
+/* At most 3 bytes of UTF-8 needed per character */
+#define UTF8_MAX_LENGTH     (3 * PLAINTEXT_LENGTH)
+#define BUFSIZE             ((UTF8_MAX_LENGTH + 3) / 4 * 4)
+#define AUTOTUNE_LENGTH     8
+#define CIPHERTEXT_LENGTH   32
+#define BINARY_SIZE         16
+#define BINARY_ALIGN        MEM_ALIGN_WORD
+#define SALT_SIZE           0
+#define SALT_ALIGN          1
+
+#define MIN_KEYS_PER_CRYPT  1
+#define MAX_KEYS_PER_CRYPT  1
+
+/* Note: Some plaintexts will be replaced in init() depending on codepage */
 static struct fmt_tests tests[] = {
 	{"b7e4b9022cd45f275334bbdb83bb5be5", "John the Ripper"},
-	{"$NT$8bd6e4fb88e01009818749c5443ea712", "\xFC"},         // German u-diaeresis in ISO-8859-1
-	{"$NT$cc1260adb6985ca749f150c7e0b22063", "\xFC\xFC"},     // Two of the above
+	{"$NT$31d6cfe0d16ae931b73c59d7e0c089c0", ""},
+	{"$NT$31d6cfe0d16ae931b73c59d7e0c089c0", ""},
+	{"$NT$31d6cfe0d16ae931b73c59d7e0c089c0", ""},
+	{"$NT$31d6cfe0d16ae931b73c59d7e0c089c0", ""},
+	{"$NT$31d6cfe0d16ae931b73c59d7e0c089c0", ""},
 	{"$NT$7a21990fcd3d759941e45c490f143d5f", "12345"},
 	{"$NT$f9e37e83b83c47a93c2f09f66408631b", "abc123"},
 	{"$NT$8846f7eaee8fb117ad06bdd830b7586c", "password"},
@@ -324,7 +332,7 @@ static void init_kernel(unsigned int num_ld_hashes, char *bitmap_para)
 	sprintf(build_opts, "-D OFFSET_TABLE_SIZE=%u -D HASH_TABLE_SIZE=%u"
 		" -D SHIFT64_OT_SZ=%u -D SHIFT64_HT_SZ=%u -D NUM_LOADED_HASHES=%u"
 		" -D NUM_INT_KEYS=%u %s -D IS_STATIC_GPU_MASK=%d"
-		" -D CONST_CACHE_SIZE=%llu -D LOC_0=%d"
+		" -D CONST_CACHE_SIZE=%llu -D%s -D%s -DPLAINTEXT_LENGTH=%d -D LOC_0=%d"
 #if 1 < MASK_FMT_INT_PLHDR
 	" -D LOC_1=%d "
 #endif
@@ -334,9 +342,12 @@ static void init_kernel(unsigned int num_ld_hashes, char *bitmap_para)
 #if 3 < MASK_FMT_INT_PLHDR
 	"-D LOC_3=%d"
 #endif
-	, offset_table_size, hash_table_size, shift64_ot_sz, shift64_ht_sz,
+	,offset_table_size, hash_table_size, shift64_ot_sz, shift64_ht_sz,
 	num_ld_hashes, mask_int_cand.num_int_cand, bitmap_para, is_static_gpu_mask,
-	(unsigned long long)const_cache_size, static_gpu_locations[0]
+	(unsigned long long)const_cache_size, cp_id2macro(pers_opts.target_enc),
+	pers_opts.internal_enc == UTF_8 ? cp_id2macro(ASCII) :
+	cp_id2macro(pers_opts.internal_enc), PLAINTEXT_LENGTH,
+	static_gpu_locations[0]
 #if 1 < MASK_FMT_INT_PLHDR
 	, static_gpu_locations[1]
 #endif
@@ -362,6 +373,27 @@ static void init(struct fmt_main *_self)
 	opencl_prepare_dev(gpu_id);
 
 	opencl_read_source("$JOHN/kernels/nt_kernel.cl");
+
+	if (pers_opts.target_enc == UTF_8) {
+		self->params.plaintext_length = MIN(125, UTF8_MAX_LENGTH);
+		tests[1].plaintext = "\xC3\xBC";	// German u-umlaut in UTF-8
+		tests[1].ciphertext = "$NT$8bd6e4fb88e01009818749c5443ea712";
+		tests[2].plaintext = "\xC3\xBC\xC3\xBC"; // two of them
+		tests[2].ciphertext = "$NT$cc1260adb6985ca749f150c7e0b22063";
+		tests[3].plaintext = "\xE2\x82\xAC";	// euro sign
+		tests[3].ciphertext = "$NT$030926b781938db4365d46adc7cfbcb8";
+		tests[4].plaintext = "\xE2\x82\xAC\xE2\x82\xAC";
+		tests[4].ciphertext = "$NT$682467b963bb4e61943e170a04f7db46";
+	} else if (CP_to_Unicode[0xfc] == 0x00fc) {
+		tests[1].plaintext = "\xFC";	// German u-umlaut in UTF-8
+		tests[1].ciphertext = "$NT$8bd6e4fb88e01009818749c5443ea712";
+		tests[2].plaintext = "\xFC\xFC"; // two of them
+		tests[2].ciphertext = "$NT$cc1260adb6985ca749f150c7e0b22063";
+		tests[3].plaintext = "\xFC\xFC\xFC";	// 3 of them
+		tests[3].ciphertext = "$NT$2e583e8c210fb101994c19877ac53b89";
+		tests[4].plaintext = "\xFC\xFC\xFC\xFC";
+		tests[4].ciphertext = "$NT$243bb98e7704797f92b1dd7ded6da0d0";
+	}
 }
 
 static char *split(char *ciphertext, int index, struct fmt_main *self)
@@ -505,7 +537,7 @@ static void set_key(char *_key, int index)
 
 static char *get_key(int index)
 {
-	static char out[PLAINTEXT_LENGTH + 1];
+	static char out[UTF8_MAX_LENGTH + 1];
 	int i, len, int_index, t;
 	char *key;
 
@@ -908,10 +940,10 @@ static void auto_tune(struct db_main *db, long double kernel_run_ms)
 
 	int tune_gws, tune_lws;
 
-	char key[PLAINTEXT_LENGTH + 1];
+	char key[AUTOTUNE_LENGTH + 1];
 
-	memset(key, 0xF5, PLAINTEXT_LENGTH);
-	key[PLAINTEXT_LENGTH] = 0;
+	memset(key, 0xF5, AUTOTUNE_LENGTH);
+	key[AUTOTUNE_LENGTH] = 0;
 
 	gws_limit = MIN((0xf << 22) * 4 / BUFSIZE,
 			get_max_mem_alloc_size(gpu_id) / BUFSIZE);
@@ -1155,7 +1187,7 @@ struct fmt_main fmt_opencl_NT = {
 		SALT_ALIGN,
 		MIN_KEYS_PER_CRYPT,
 		MAX_KEYS_PER_CRYPT,
-		FMT_CASE | FMT_8_BIT | FMT_SPLIT_UNIFIES_CASE | FMT_UNICODE,
+		FMT_CASE | FMT_8_BIT | FMT_SPLIT_UNIFIES_CASE | FMT_UNICODE | FMT_UTF8,
 		{ NULL },
 		tests
 	}, {
