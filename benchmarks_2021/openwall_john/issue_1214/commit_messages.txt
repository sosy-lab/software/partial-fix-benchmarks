Drop obsolete header from wordlist.c (we now use pseudo-
intrinsics.h)
Fix a logic error in #ifdefs for including mman.h. This should
close #1214. It's strange we did not get complaints on many
systems.
Fix a bug in pp.c where munmap() was called even when we do
not HAVE_MMAP. And add a warning notice when building without
mmap.
AC: Change the way we look for mmap(). Closes #1214.
