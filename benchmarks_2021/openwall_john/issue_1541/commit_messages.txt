Fix a bug that prevented NETLM and NETHALFLM to be removed
even though they are plugins. Also prepare for upcoming
OpenCL version of LM format.
dynamic_compiler. magnum fixed $u handling on raw hashes. Fix #1541
Add tag to bare hashes $u -> $$U preparation (if needed). Also
fixes some CRLF issues. See #1541.
Fix for dynacomptest workaround. See #1541
