LM-opencl fails on OSX (all devices)
Please add printf after current line number 132 in opencl_lm_b_plug.c: 

```
 fprintf(stderr, "Arg 1:%\n", arg_name);
```

also add after 138:

```
fprintf(stderr, "Arg 2:%\n", arg_name);
```

And use -v=4 option in command line.

What line numbers would that be with current code?

Here's latest code with verbose=4

```
$ for i in 0 1 2 ; do ../run/john -test -form:lm-opencl -dev=$i  -verb=4; done
Device 0: Intel(R) Core(TM) i7-3615QM CPU @ 2.30GHz
Benchmarking: LM-opencl [DES BS OpenCL]... Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__CPU__ -DDEVICE_INFO=33 -DDEV_VER_MAJOR=1 -DDEV_VER_MINOR=1 -D_OPENCL_COMPILER -D FULL_UNROLL=1 -D USE_LOCAL_MEM=0 -D WORK_GROUP_SIZE=0 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
OpenCL error (CL_INVALID_KERNEL_ARGS) in file (opencl_lm_b_plug.c) at line (1173) - (Failed enqueue kernel lm_bs.)

Device 1: HD Graphics 4000
Benchmarking: LM-opencl [DES BS OpenCL]... Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=34 -DDEV_VER_MAJOR=1 -DDEV_VER_MINOR=2 -D_OPENCL_COMPILER -D FULL_UNROLL=1 -D USE_LOCAL_MEM=0 -D WORK_GROUP_SIZE=0 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
OpenCL error (CL_INVALID_KERNEL_ARGS) in file (opencl_lm_b_plug.c) at line (1173) - (Failed enqueue kernel lm_bs.)

Device 2: GeForce GT 650M
Benchmarking: LM-opencl [DES BS OpenCL]... Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=1 -D USE_LOCAL_MEM=1 -D WORK_GROUP_SIZE=32 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=1 -D USE_LOCAL_MEM=1 -D WORK_GROUP_SIZE=32 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=1 -D USE_LOCAL_MEM=1 -D WORK_GROUP_SIZE=64 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=1 -D USE_LOCAL_MEM=1 -D WORK_GROUP_SIZE=96 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Abort trap: 6
```

We need to get rid of that "unused variable" also, but I'm not sure what ifdefs to wrap it in.

I added a print after each call to clGetKernelArgInfo. They always come empty.

https://www.khronos.org/registry/cl/sdk/1.2/docs/man/xhtml/clGetKernelArgInfo.html

_Kernel argument information is only available if the program object associated with kernel is created with clCreateProgramWithSource and the program executable is built with the -cl-kernel-arg-info option specified in options argument to clBuildProgram or clCompileProgram._

I did a `git grep -- -cl-kernel-arg-info` and CL_KERNEL_ARG_INFO and it shows nothing. Maybe you missed using this?

We will need a fallback for OpenCL 1.1 systems anyway so I'm not sure your use of these functions are a good idea - can you please explain what you use them for? Can you do it some other way?

After my bugfix in cd8ed08

```
$ for i in 0 1 2 ; do ../run/john -test -form:lm-opencl -dev=$i  ; done
Device 0: Intel(R) Core(TM) i7-3615QM CPU @ 2.30GHz
Benchmarking: LM-opencl [DES BS OpenCL]... DONE
Raw:    18612K c/s real, 2753K c/s virtual

Device 1: HD Graphics 4000
Benchmarking: LM-opencl [DES BS OpenCL]... DONE
Raw:    17848K c/s real, 139810K c/s virtual

Device 2: GeForce GT 650M
Benchmarking: LM-opencl [DES BS OpenCL]... Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Abort trap: 6
```

```
$ lldb -- ../run/john -test=0 -form:lm-opencl -dev=2
(lldb) target create "../run/john"
Current executable set to '../run/john' (x86_64).
(lldb) settings set -- target.run-args  "-test=0" "-form:lm-opencl" "-dev=2"
(lldb) r
Process 46263 launched: '../run/john' (x86_64)
Device 2: GeForce GT 650M
Testing: LM-opencl [DES BS OpenCL]... Build log: <program source>:550:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Process 46263 stopped
* thread #1: tid = 0x51760c, 0x00007fff8cf9f286 libsystem_kernel.dylib`__pthread_kill + 10, queue = 'opencl_runtime', stop reason = signal SIGABRT
    frame #0: 0x00007fff8cf9f286 libsystem_kernel.dylib`__pthread_kill + 10
libsystem_kernel.dylib`__pthread_kill:
->  0x7fff8cf9f286 <+10>: jae    0x7fff8cf9f290            ; <+20>
    0x7fff8cf9f288 <+12>: movq   %rax, %rdi
    0x7fff8cf9f28b <+15>: jmp    0x7fff8cf9ac53            ; cerror_nocancel
    0x7fff8cf9f290 <+20>: retq   
(lldb) bt
* thread #1: tid = 0x51760c, 0x00007fff8cf9f286 libsystem_kernel.dylib`__pthread_kill + 10, queue = 'opencl_runtime', stop reason = signal SIGABRT
  * frame #0: 0x00007fff8cf9f286 libsystem_kernel.dylib`__pthread_kill + 10
    frame #1: 0x00007fff8674542f libsystem_pthread.dylib`pthread_kill + 90
    frame #2: 0x00007fff886deb53 libsystem_c.dylib`abort + 129
    frame #3: 0x00007fff847dfb81 libGPUSupportMercury.dylib`gpusGenerateCrashLog + 173
    frame #4: 0x000012344022728a GeForceGLDriver`___lldb_unnamed_function6431$$GeForceGLDriver + 9
    frame #5: 0x00007fff847e0538 libGPUSupportMercury.dylib`gpusQueueSubmitDataBuffers + 170
    frame #6: 0x00007fff897561f7 libclh.dylib`channelFlushUnitFlushGLK + 295
    frame #7: 0x00007fff896f007d libclh.dylib`channelFlush_UnderLock + 317
    frame #8: 0x00007fff896f0131 libclh.dylib`channelFlush + 33
    frame #9: 0x00007fff896f3005 libclh.dylib`channelManagerFlushAllChannels + 53
    frame #10: 0x00007fff89757745 libclh.dylib`clhCtxSynchronize + 21
    frame #11: 0x000012344030a158 GeForceGLDriver`gldFinishQueue + 253
    frame #12: 0x00007fff925207f0 OpenCL`___lldb_unnamed_function16$$OpenCL + 78
    frame #13: 0x00007fff92520cba OpenCL`___lldb_unnamed_function17$$OpenCL + 1104
    frame #14: 0x00007fff9253efc4 OpenCL`___lldb_unnamed_function436$$OpenCL + 95
    frame #15: 0x00007fff9254263b OpenCL`___lldb_unnamed_function471$$OpenCL + 288
    frame #16: 0x00007fff89eafc13 libdispatch.dylib`_dispatch_client_callout + 8
    frame #17: 0x00007fff89eb0e5e libdispatch.dylib`_dispatch_barrier_sync_f_invoke + 57
    frame #18: 0x00007fff925424bf OpenCL`___lldb_unnamed_function470$$OpenCL + 141
    frame #19: 0x00007fff9253e06b OpenCL`___lldb_unnamed_function422$$OpenCL + 442
    frame #20: 0x00007fff925331c1 OpenCL`clEnqueueReadBuffer + 813
    frame #21: 0x00000001001bbf10 john`lm_crypt(pcount=0x00007fff5fbfceb8, salt=<unavailable>) + 384 at opencl_lm_b_plug.c:1177
    frame #22: 0x000000010027a5c7 john`fmt_self_test(format=0x00000001004c2e00, db=<unavailable>) + 2055 at formats.c:569
    frame #23: 0x000000010026e71a john`benchmark_format(format=0x00000001004c2e00, salts=<unavailable>, results=<unavailable>) + 634 at bench.c:235
    frame #24: 0x000000010026f6c3 john`benchmark_all + 819 at bench.c:652
    frame #25: 0x00000001002832d3 john`john_run + 43 at john.c:1368
    frame #26: 0x0000000100283f4d john`main(argc=4, argv=0x00007fff5fbffa98) + 1039 at john.c:1741
    frame #27: 0x00007fff881905c9 libdyld.dylib`start + 1
    frame #28: 0x00007fff881905c9 libdyld.dylib`start + 1
(lldb) 
```

It works if unsetting full_unroll.

```
$ make -s && ../run/john -test -form:lm-opencl -dev=2

Make process completed.
Device 2: GeForce GT 650M
Benchmarking: LM-opencl [DES BS OpenCL]... Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

Build log: <program source>:157:16: warning: unused variable 'gws'
                unsigned int gws = get_global_size(0), i;
               ^

DONE
Raw:    19065K c/s real, 99864K c/s virtual
```

That unused variable warning is really annoying

Speed was almost doubled when forcing global keys.

Current code fails real crack mask mode on all devices

```
$ ../run/john | head -1
John the Ripper 1.8.0.6-jumbo-1-601-g7b85fb7 OMP [darwin14.4.0 64-bit AVX-ac]

$ for i in 0 1 2 ; do ../run/john -test -form:lm-opencl -dev=$i  ; done
Device 0: Intel(R) Core(TM) i7-3615QM CPU @ 2.30GHz
Benchmarking: LM-opencl [DES BS OpenCL]... DONE
Raw:    18743K c/s real, 2728K c/s virtual

Device 1: HD Graphics 4000
Benchmarking: LM-opencl [DES BS OpenCL]... DONE
Raw:    16131K c/s real, 139810K c/s virtual

Device 2: GeForce GT 650M
Benchmarking: LM-opencl [DES BS OpenCL]... DONE
Raw:    44150K c/s real, 132451K c/s virtual

$ for i in 0 1 2 ; do ../run/john test.in -inc --max-len=4  -form:lm-opencl -dev=$i  ; done
Device 0: Intel(R) Core(TM) i7-3615QM CPU @ 2.30GHz
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
Press 'q' or Ctrl-C to abort, almost any other key for status
0g 0:00:00:02 DONE (2015-08-03 01:17) 0g/s 10087Kp/s 10087Kc/s 10087KC/s ,,|}..,,|}
Session completed

Device 1: HD Graphics 4000
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
Press 'q' or Ctrl-C to abort, almost any other key for status
0g 0:00:00:07 N/A 0g/s 3159Kp/s 3159Kc/s 3159KC/s %T:#..%T:#
Session completed

Device 2: GeForce GT 650M
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
Press 'q' or Ctrl-C to abort, almost any other key for status
0g 0:00:00:04 N/A 0g/s 4684Kp/s 4684Kc/s 4684KC/s %T:#..%T:#
Session completed

$ for i in 0 1 2 ; do ../run/john test.in -mask:?l?l?l?l  -form:lm-opencl -dev=$i  ; done
Device 0: Intel(R) Core(TM) i7-3615QM CPU @ 2.30GHz
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
OpenCL error (CL_OUT_OF_RESOURCES) in file (opencl_lm_b_plug.c) at line (1241) - (Failed enqueue kernel lm_bs_*.)

Device 1: HD Graphics 4000
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
OpenCL error (CL_OUT_OF_RESOURCES) in file (opencl_lm_b_plug.c) at line (1241) - (Failed enqueue kernel lm_bs_*.)

Device 2: GeForce GT 650M
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
OpenCL error (CL_OUT_OF_RESOURCES) in file (opencl_lm_b_plug.c) at line (1241) - (Failed enqueue kernel lm_bs_*.)
```

``` diff
$ git diff
diff --git a/src/opencl_lm_b_plug.c b/src/opencl_lm_b_plug.c
index 3d9e154..45b2ffe 100644
--- a/src/opencl_lm_b_plug.c
+++ b/src/opencl_lm_b_plug.c
@@ -1238,6 +1238,7 @@ static int lm_crypt(int *pcount, struct db_salt *salt)
        if (!is_static_gpu_mask)
                HANDLE_CLERROR(clEnqueueWriteBuffer(queue[gpu_id], buffer_int_key_loc, CL_TRUE, 0, current_gws * sizeof(unsigned int), opencl_lm_int_key_loc, 0, NULL, NULL ), "Failed Copy data to gpu");

+       fprintf(stderr, "pcount %d count %d lws %zu gws %zu cur_gws %zu\n", *pcount, count, local_work_size, global_work_size, current_gws);
        HANDLE_CLERROR(clEnqueueNDRangeKernel(queue[gpu_id], krnl[gpu_id][0], 1, NULL, &current_gws, lws, 0, NULL, &evnt), "Failed enqueue kernel lm_bs_*.");
        clWaitForEvents(1, &evnt);
        clReleaseEvent(evnt);
@@ -1322,4 +1323,4 @@ void opencl_lm_b_register_functions(struct fmt_main *fmt)
        opencl_lm_init_global_variables = &init_global_variables;
 }

-#endif /* #if HAVE_OPENCL */
\ No newline at end of file
+#endif /* #if HAVE_OPENCL */
```

(on a side note you should fix your editor; last line of file should always have LF. Perhaps see if your editor supports [EditorConfig](http://editorconfig.org))

```
$ make -s && ../run/john test.in -mask:?l?l?l?l -form:lm-opencl -dev=2 -enc:ascii -v=5

Make process completed.
initUnicode(UNICODE, ASCII/ASCII)
ASCII -> ASCII -> ASCII
Device 2: GeForce GT 650M
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=0 -D USE_LOCAL_MEM=0 -D WORK_GROUP_SIZE=0 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
pcount 32768 count 1024 lws 0 gws 1024 cur_gws 1024
pcount 4194304 count 131072 lws 0 gws 131072 cur_gws 131072
pcount 8388608 count 262144 lws 32 gws 262144 cur_gws 262144
pcount 8388608 count 262144 lws 64 gws 262144 cur_gws 262144
pcount 8388608 count 262144 lws 128 gws 262144 cur_gws 262144
pcount 8388608 count 262144 lws 256 gws 262144 cur_gws 262144
pcount 8388608 count 262144 lws 512 gws 262144 cur_gws 262144
pcount 8388608 count 262144 lws 1024 gws 262144 cur_gws 262144
pcount 8388608 count 262144 lws 1024 gws 262144 cur_gws 262144
GWS: 524288, LWS: 1024
pcount 1 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 2 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 3 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 4 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 5 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 7 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 10 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 14 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 20 count 1 lws 1024 gws 524288 cur_gws 1024
pcount 16777216 count 524288 lws 1024 gws 524288 cur_gws 524288
Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=0 -D USE_LOCAL_MEM=0 -D WORK_GROUP_SIZE=0 -D OFFSET_TABLE_SIZE=10 -D HASH_TABLE_SIZE=1 -D MASK_ENABLE=1 -D ITER_COUNT=550 -D LOC_0=0 -D LOC_1=1 -D LOC_2=2 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=1 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
pcount 1024 count 1024 lws 0 gws 1024 cur_gws 1024
OpenCL error (CL_OUT_OF_RESOURCES) in file (opencl_lm_b_plug.c) at line (1242) - (Failed enqueue kernel lm_bs_*.)
```

Buffer leak perhaps?

```
$ GWS=8192 ../run/john test.in -mask:?l?l?l?l -form:lm-opencl -dev=2 -enc:ascii -v=5
initUnicode(UNICODE, ASCII/ASCII)
ASCII -> ASCII -> ASCII
Device 2: GeForce GT 650M
Loaded 1 password hash (LM-opencl [DES BS OpenCL])
Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=0 -D USE_LOCAL_MEM=0 -D WORK_GROUP_SIZE=0 -D OFFSET_TABLE_SIZE=13 -D HASH_TABLE_SIZE=9 -D MASK_ENABLE=0 -D ITER_COUNT=1 -D LOC_0=-1 -D LOC_1=-1 -D LOC_2=-1 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=0 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
pcount 262144 count 8192 lws 32 gws 8192 cur_gws 8192
pcount 262144 count 8192 lws 64 gws 8192 cur_gws 8192
pcount 262144 count 8192 lws 128 gws 8192 cur_gws 8192
pcount 262144 count 8192 lws 256 gws 8192 cur_gws 8192
pcount 262144 count 8192 lws 512 gws 8192 cur_gws 8192
pcount 262144 count 8192 lws 1024 gws 8192 cur_gws 8192
GWS: 8192, LWS: 128
pcount 1 count 1 lws 128 gws 8192 cur_gws 128
pcount 2 count 1 lws 128 gws 8192 cur_gws 128
pcount 3 count 1 lws 128 gws 8192 cur_gws 128
pcount 4 count 1 lws 128 gws 8192 cur_gws 128
pcount 5 count 1 lws 128 gws 8192 cur_gws 128
pcount 7 count 1 lws 128 gws 8192 cur_gws 128
pcount 10 count 1 lws 128 gws 8192 cur_gws 128
pcount 14 count 1 lws 128 gws 8192 cur_gws 128
pcount 20 count 1 lws 128 gws 8192 cur_gws 128
pcount 262144 count 8192 lws 128 gws 8192 cur_gws 8192
Options used: -I ../run/kernels -cl-mad-enable -D__OS_X__ -D__GPU__ -DDEVICE_INFO=18 -DDEV_VER_MAJOR=10 -DDEV_VER_MINOR=4 -D_OPENCL_COMPILER -D FULL_UNROLL=0 -D USE_LOCAL_MEM=0 -D WORK_GROUP_SIZE=0 -D OFFSET_TABLE_SIZE=10 -D HASH_TABLE_SIZE=1 -D MASK_ENABLE=1 -D ITER_COUNT=550 -D LOC_0=0 -D LOC_1=1 -D LOC_2=2 -D LOC_3=-1 -D IS_STATIC_GPU_MASK=1 -D CONST_CACHE_SIZE=65536 -D SELECT_CMP_STEPS=2 -D BITMAP_SIZE_BITS_LESS_ONE=262143 -D REQ_BITMAP_BITS=18
pcount 8192 count 8192 lws 32 gws 8192 cur_gws 8192
OpenCL error (CL_OUT_OF_RESOURCES) in file (opencl_lm_b_plug.c) at line (1242) - (Failed enqueue kernel lm_bs_*.)
```

Why do we have a pcount way higher than 8192 in some of the above? Is it less directly coupled to GWS here?

This new bug is not OSX specific - I can't use mask mode on super or well either.

Otherthan these cl errors, is it failing to crack hashes when it should i.e. missing some cracks? Does the incremental test suggest that? 

No, that was an "uncrackable" hash. I haven't re-tested #1596 yet.

pcount for all mode except mask is 32x of gws. lws is zero for first run.

should be fixed in f2e566261d0ce965cd

Yep all work fine now.
