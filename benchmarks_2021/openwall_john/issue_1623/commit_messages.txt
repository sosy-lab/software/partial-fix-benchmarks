dmg: replaced oSSL hmac-sha1 with our own code
.pot: allow multiple .pot files to remove already cracked items. Fix #1623. Also document john-local.conf and removed references to john.local.conf
Re-write the "extra pot files" code to use a list section. See #1623.
Add ability to read all pot files in a directory. See #1623.
Extra pot-file feature: When traversing directory, only consider
files with extension of ".pot".  This is how the Windows code
path already handled it.  Also, accept symlinks.  And clean up
code, moving this to its own function to avoid indenting off
screen...  Closes #1623.
