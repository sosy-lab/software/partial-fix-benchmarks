GPU-side mask mode refuses length range starting too short
The problem is the varible length mask!! It is possible to bypass gpu mask for short length masks but then it'll also bypass much needed gpu-mask for longer masks. This is becasue mask truncation is done during cracking while gpu-masks are selected during inititialization. Our other options are:
1. Auto-increase min-length. 
2. Do not use gpu mask mode at all if min-length is too short.

I see. But when min/max lengths are specified, we're doing a loop that is not hot at all. Could we possibly add some magic in there, so the first few iterations (eg. length 1 and 2) are done without GPU-side mask, and then at length 3 or so, we re-assess the situation (simply re-init for GPU mask)?

I think the two alternatives you list are much worse than the current situation. It's definitely acceptable as-is now but it would be very very convenient to be able to just specify the length range you actually want covered and be done with it.

I have tried to figure out how to achieve this, but I failed... shouldn't it be fairly easy to do just a partial re-init per length?

This is an interesting difference:

```
$ ../run/john scraped.sam -form:lm-opencl -mask:?d -min-len=1 -max-len=1
Device 1: HD Graphics 4000
Loaded 3252 password hashes with no different salts (LM-opencl [DES BS OpenCL])
Press 'q' or Ctrl-C to abort, almost any other key for status
Format internal ranges cannot be truncated!
Use a bigger key length or non-gpu format.

$ ../run/john scraped.sam -form:lm-opencl -mask:?d
Device 1: HD Graphics 4000
Loaded 3252 password hashes with no different salts (LM-opencl [DES BS OpenCL])
Press 'q' or Ctrl-C to abort, almost any other key for status
(,,,)
10g 0:00:00:04  2.202g/s 2.202p/s 2.202c/s 7160C/s 0..9
```

I am also seeing this problem, another data point is this:

```
jtrexe --format=LM-opencl --max-len=1 --mask=?a pwfile2.txt
Device 0: Tahiti [AMD Radeon R9 200 / HD 7900 Series]
Using default input encoding: UTF-8
Using default target encoding: CP850
3g 0:00:00:05  0.5056g/s 802.4p/s 802.4c/s 16851C/s AA..AA
Session completed
Loaded 21 password hashes with no different salts (LM-opencl [DES BS OpenCL])
MM               (twom)
ZZ               (twoz)
AA               (twoa)
```

Note that with max-length set to 1, it finds two character long passwords. I have a vested interest in getting this bug fixed, so let me know how i can help.

That's strange, I haven't seen that and can't reproduce [on my laptop]. What if you just use -mask=?a and leave out the -max-len?

It works as expected.

```
Device 0: Tahiti [AMD Radeon R9 200 / HD 7900 Series]
Using default input encoding: UTF-8
Using default target encoding: CP850
3g 0:00:00:05  0.5145g/s 11.83p/s 11.83c/s 248.5C/s A..A
Session completed
Loaded 21 password hashes with no different salts (LM-opencl [DES BS OpenCL])
M                (onem)
Z                (onez)
A                (Administrator)
```

The workaround until this bug is closed is simple though: Just do the short lengths with the CPU format - it only takes a split second. Then continue with OpenCL.

```
$ ../run/john scraped.sam -form:lm --min-len=0 --max-len=3 --mask=?a -verb:1
Using default input encoding: UTF-8
Using default target encoding: CP850
Loaded 3252 password hashes with no different salts (LM [DES 128/128 AVX-16])
Will run 8 OpenMP threads
Press 'q' or Ctrl-C to abort, almost any other key for status
0g 0:00:00:00  (3) 0g/s 0p/s 0c/s 0C/s
345g 0:00:00:00 N/A 17250g/s 16667Kp/s 16667Kc/s 49450MC/s };}..|||
Session completed

$ ../run/john scraped.sam -form:lm-opencl --min-len=4 --max-len=7 --mask=?a -verb:1 
Using default input encoding: UTF-8
Using default target encoding: CP850
Loaded 3252 password hashes with no different salts (LM-opencl [DES BS OpenCL])
Remaining 2907 password hashes with no different salts
Press 'q' or Ctrl-C to abort, almost any other key for status
236g 0:00:00:05  (5) 41.11g/s 3939Kp/s 3939Kc/s 12385MC/s A|<|..A|<|
(...)
```

(Never mind the OpenCL speed shown here - it's just an Intel HD Graphics 4000)

Yea, no worries. That's probably what I'll end up doing in the meantime.

This is fixed now. LM & DEScrypt are still not 100% but they adjust and warn (until I get them fully working from length 0). All other formats should work just fine incrementing from 0.