Office OpenCL formats: GPU-side AES, hashing and verify enhancement
I think we should also join them to just office-opencl, with iteration count and hash type as costs.
WIP. I'm done with the separate formats. Need to squash a bug or two and then merge all three formats to a single new office-opencl format.

There is no significant speed gain compared to 8xOMP, just less CPU work (which is good enough).
Cool, I am waiting for the code to land :)
Bugs squashed now (I fought for 24h with a stupid copy-paste bug rendering in buffer overruns - impossible to find with the debugger).

Now I realised I still did AES-ECB on CPU for Office 2007. It seems to me AES-ECB should be just one block of AES with no bells and whistles but I have yet to get it working on GPU.