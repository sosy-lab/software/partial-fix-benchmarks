Tune MKPC and (only then) OMP_SCALE for many formats
Note that for some more important (to yourself or the public) formats, you can be more thorough and do the whole thing again for non-SIMD.

Mock-up example:
```c
#ifdef SIMD_COEF_32
#define MIN_KEYS_PER_CRYPT  (SIMD_COEF_32*SIMD_PARA_SHA256)
#define MAX_KEYS_PER_CRYPT  (SIMD_COEF_32*SIMD_PARA_SHA256*4)
#else
#define MIN_KEYS_PER_CRYPT   1
#define MAX_KEYS_PER_CRYPT  32
#endif

#if __MIC__
#define OMP_SCALE           64
#elif __x86_64__ || __i386__
#if SIMD_COEF_32
#define OMP_SCALE           4
#else
#define OMP_SCALE           16
#endif
#else
#define OMP_SCALE           0   /* Some other arch, always auto-tune */
#endif
```
  
With just a few lines of "code" in `omp_autotune.c`, I could make any non-intel (as in ARM, PPC and so on) ignore any pre-defined OMP_SCALE and always autotune. I really think that's the way to go (until @solardiz changes core and makes all of this crap obsolete anyway).
```
magnum@neon64:src [magnum %]$ ../run/john.b4 -test -form:wpapsk-pmk
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    16912 c/s real, 5811 c/s virtual

magnum@neon64:src [magnum %]$ ../run/john -test -form:wpapsk-pmk
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    104427 c/s real, 43946 c/s virtual

magnum@neon64:src [magnum %]$ ../run/john -test -form:wpapsk-pmk -tune=auto
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    160768 c/s real, 57830 c/s virtual
```
Some real gain here. The first 16x boost was really a bug in the format (although much easier to find now), but the last 63% boost is the difference between an OMP_SCALE tuned for intel, and an auto-tune on NEON64.
Scrap the last post. I now get double of those figures, for no obvious reason (nothing changed!). Are these little buggers throttling from temperature? This is a Raspberry Pi 3b or whatever they're called, running 64-bit Fedora Linux and otherwise idling (but when I posted the above, I had just built JtR using `-sj4`).
```
magnum@neon64:src [magnum %]$ ../run/john.b4 -test -form:wpapsk-pmk
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    223576 c/s real, 56889 c/s virtual

magnum@neon64:src [magnum %]$ ../run/john -test -form:wpapsk-pmk 
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    320380 c/s real, 81302 c/s virtual

magnum@neon64:src [magnum %]$ ../run/john -test -form:wpapsk-pmk -tune=report
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) 
DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    326656 c/s real, 82488 c/s virtual
```
  
FWIW here's figures from a 32-bit Pi (ran after long idle)
```
$ ../run/john.b4 -test -form:wpapsk-pmk && ../run/john -test -form:wpapsk-pmk && ../run/john -test -form:wpapsk-pmk -tune=report
Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    154880 c/s real, 39611 c/s virtual

Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    231424 c/s real, 57856 c/s virtual

Will run 4 OpenMP threads
Benchmarking: wpapsk-pmk, WPA/WPA2/PMF master key [MD5/SHA-1/SHA-2]... (4xOMP) 
DONE
Speed for cost 1 (key version [1:WPA 2:WPA2 3:802.11w]) of 1 and 2
Raw:    252988 c/s real, 63873 c/s virtual
```
> Are these little buggers throttling from temperature? This is a Raspberry Pi 3b

They sure are. The operating temperature limit is 85°C, and it should start to thermally throttle performance around 82°C. Temperature can be checked with `cat /sys/class/thermal/thermal_zone0/temp` (divide by 1000 to get °C). Note that an under-rated power supply can also result in throttling, at any temperature. Or a reboot...

I need heat sinks and perhaps even tiny fans before I start fiddling with relbench and stuff...
  
Even with heat sinks (and fans), they throttle very hard when you start to hit them using the multi-cpu stuff and NEON packed instructions.  The little buggers get really hot quickly, and do protect themselves pretty hard.
  
Thank you for your work on this, magnum!

Nitpick 1: I think for OMP_SCALE the tuning depends on OpenMP implementation (e.g., gcc libgomp version) and on OS kernel/version/settings (especially those affecting the scheduler) and on OpenMP env vars and on whether the machine is single- vs. multi-socket, and lastly on CPU arch/vendor. So limiting the presets to Intel CPUs might be a wrong thing to do - I think the setting should be in use on all CPU archs, unless there's clear indication the presets are very wrong on non-x86 archs (to an extent greater than what we commonly see between different x86 systems). But if we continue to do that anyway, perhaps s/intel/x86/ in the john.conf comment? Technically, our compile-time checks are for x86, not for Intel as a vendor.

Nitpick 2: while the specific CPU type is relatively unimportant, if we do specify it let's be specific. "core i7" is non-specific - there have been many generations of CPUs marketed as Intel Core, and each generation included some i7's. The way Dhiru specifies his CPU makes more sense here, if we do specify anything at all.
> Nitpick 1: I think for OMP_SCALE the tuning depends on OpenMP implementation (e.g., gcc libgomp version) and on OS kernel/version/settings (especially those affecting the scheduler) and on OpenMP env vars and on whether the machine is single- vs. multi-socket, and lastly on CPU arch/vendor. So limiting the presets to Intel CPUs might be a wrong thing to do - I think the setting should be in use on all CPU archs, unless there's clear indication the presets are very wrong on non-x86 archs (to an extent greater than what we commonly see between different x86 systems). But if we continue to do that anyway, perhaps s/intel/x86/ in the john.conf comment? Technically, our compile-time checks are for x86, not for Intel as a vendor.

Some things you say pushes for always using auto-tune, even with Intel. I'm aware there are severe limitations with it, but it usually works really really fine. Also, many formats need a much better MAX_KEYS_PER_CRYPT and they will get it sooner or later, regardless of arch. I did a bunch in #3115 and more will come.

> Nitpick 2: while the specific CPU type is relatively unimportant, if we do specify it let's be specific. 

I'm thinking we're tuning as a team for "Intel" and that is the closest we'll ever get to perfect. Perhaps Dhiru's desktop i7 figures won't be spot-on perfect for my Macbook i7, but they will likely be close. At least I haven't seen any real gross misfits. For NEON 32/64 archs, I have.
> For NEON 32/64 archs, I have.

BTW I'm planning to add clauses for NEON32/64 and drop it from the "always auto tune". I just need to sort the cooling...
@magnumripper I have a question about MKPC tuning. I can't remember if the `../run/john --format=net-ah --test -v:5 -tune=auto` command will automatically try different MKPC values, or if I have to try different MKPC values on my own. If it is the former, then that functionality seems to be broken at the moment.
> @magnumripper I have a question about MKPC tuning. I can't remember if the `../run/john --format=net-ah --test -v:5 -tune=auto` command will automatically try different MKPC values, or if I have to try different MKPC values on my own.

It will do it automagically, very similar to OpenCL auto-tune, and the `-v:5` should make it clear.

> If it is the former, then that functionality seems to be broken at the moment.

Works for me:
```
$ ../run/john -form:net-ah -test -v:5 -tune=auto
initUnicode(UNICODE, ASCII/ASCII)
ASCII -> ASCII -> ASCII
Will run 8 OpenMP threads
Benchmarking: net-ah, IPsec AH HMAC-MD5-96 [MD5 32/64]... (8xOMP) Loaded 4 hashes with 4 different salts to test db from test vectors

net-ah OMP autotune using test db
OMP scale 1: 1176 crypts (147x8) in 0.010062 seconds, 116875 c/s +
OMP scale 2: 2368 crypts (148x16) in 0.010069 seconds, 235177 c/s +
OMP scale 4: 3520 crypts (110x32) in 0.010025 seconds, 351122 c/s +
OMP scale 8: 8768 crypts (137x64) in 0.010003 seconds, 876537 c/s +
OMP scale 16: 14592 crypts (114x128) in 0.010019 seconds, 1456432 c/s +
OMP scale 32: 24320 crypts (95x256) in 0.010067 seconds, 2415814 c/s +
OMP scale 64: 29696 crypts (58x512) in 0.010086 seconds, 2944279 c/s +
OMP scale 128: 48128 crypts (47x1024) in 0.010184 seconds, 4725844 c/s +
OMP scale 256: 49152 crypts (24x2048) in 0.010170 seconds, 4833038 c/s +
OMP scale 512: 77824 crypts (19x4096) in 0.010333 seconds, 7531597 c/s +
OMP scale 1024: 81920 crypts (10x8192) in 0.014734 seconds, 5559929 c/s
OMP scale 2048: 98304 crypts (6x16384) in 0.011198 seconds, 8778710 c/s +
OMP scale 4096: 98304 crypts (3x32768) in 0.011312 seconds, 8690240 c/s
OMP scale 8192: 131072 crypts (2x65536) in 0.017124 seconds, 7654286 c/s
OMP scale 16384: 131072 crypts (1x131072) in 0.014630 seconds, 8959125 c/s +
OMP scale 32768: 262144 crypts (1x262144) in 0.033727 seconds, 7772526 c/s
OMP scale 65536: 524288 crypts (1x524288) in 0.061404 seconds, 8538336 c/s
OMP scale 131072: 1048576 crypts (1x1048576) in 0.128933 seconds, 8132720 c/s
Autotune found best speed at OMP scale of 16384
DONE
Warning: "Many salts" test limited: 9/256
Many salts:	8819K c/s real, 1163K c/s virtual
Only one salt:	5825K c/s real, 1115K c/s virtual
```
> If it is the former, then that functionality seems to be broken at the moment.

The net-ah format is broken though, or rather not-yet-fixed. Unless you run an OpenMP build, the OMP_SCALE and `omp_autotune()` stuff are `#ifdef`'d away. Plenty of formats are still to-do (and most are really trivial to fix, per OP in this thread).
I see. Thanks! I will fix it soon.
One way to list files that likely needs fixing is
```
git grep -l FMT_OMP *fmt_plug.c | xargs perl -ne 'BEGIN { $/=null } print $ARGV, "\n" if m/omp_autotune.*?;\s+#endif/m or m/#ifdef _OPENMP\s+(#define OMP_SCALE|omp_autotune)/m'
```
At the time of this writing there's "only" 60 of them left (out of 222).
@magnumripper Perhaps further tuning should be done with `--test --mask`, until you revise the default benchmarks to use mask or at least length 7. Right?
Not sure it matters, but yes I'll put that in the OP
Should we possibly automate the algorithm described in "How-to"? Write some script that would just do this, including patching the source code, so we'd only need to review and re-test the resulting changes (perhaps re-test on other machines)? This would enable us to re-tune easily after making relevant tree-wide changes e.g. to the benchmarks or to pseudo-intrinsics.
> @magnumripper Perhaps further tuning should be done with `--test --mask`, until you revise the default benchmarks to use mask or at least length 7. Right?

I reverted this for now, since we currently don't have self-tests for that.
49 to go. I'll have them done by tomorrow
20 to go
Done. There are actually less than a handfull left, for some rainy day. They had some problems surfacing so I just let them be. Most or all of them are so fast they wont scale anyway.

SybaseASE_fmt_plug.c
ntlmv1_mschapv2_fmt_plug.c
rawMD4_fmt_plug.c
rawSHA1_fmt_plug.c

Thanks, magnum!

Re: "they wont scale anyway" - this isn't only about scaling, but also about amortizing the overhead of function calls, etc. yet staying in cache in single-threaded builds.
Why do we usually put `#define OMP_SCALE ...` within `#ifndef OMP_SCALE` ... `#endif`? Since we don't do it in a few formats anyway, I assume this is legacy stuff that is no longer required, and we should drop those  `#ifndef OMP_SCALE` and `#endif` lines from all formats for consistency. Correct, @magnumripper?
That is/was for some tools that would build with different `-DOMP_SCALE=..` and benchmark stuff. I believe it's obsoleted by the `omp_tune()` stuff.
We ended up with OMP_SCALE tuning for some unspecified "Core i7" CPU. In my testing, these OMP_SCALE values are often insufficient for dual-socket systems, where 2x or 4x higher values would commonly be needed. Should we deal with this somehow?
I believe we could identify them using `../run/john -test -format:omp -tune=report` on some relevant hardware (eg. super). This will output things like this:
```
Benchmarking: scrypt (16384, 8, 1) [Salsa20/8 128/128 AVX]... (8xOMP) 
Autotuned OMP scale 16, preset is 1
DONE
Speed for cost 1 (N) of 16384, cost 2 (r) of 8, cost 3 (p) of 1
Raw:	276 c/s real, 35.7 c/s virtual
```
That auto-tune output is only emitted when there's a difference (so some formats will be reported just because results are inconsistent between runs)
On a side-note the scrypt format doesn't seem to have good figures for my laptop i7, with the new code. A non-OMP build shows `Autotuned MKPC 32, preset is 1` so I guess that should be bumped instead of OMP_SCALE. Are the current figures better for whatever hardware you've been using? In general I have refrained from changing core formats' figures except for md5crypt (unless some were inadvertently changed because I was tired and this was a tedious issue).

```
Benchmarking: scrypt (16384, 8, 1) [Salsa20/8 128/128 AVX]... (4xOMP) Loaded 10 hashes with 10 different salts to test db from test vectors

scrypt OMP autotune using test db with N of 16384

OMP scale 1: 8 crypts (2x4) in 0.011757 seconds, 680 c/s +
OMP scale 2: 16 crypts (2x8) in 0.019464 seconds, 822 c/s +
OMP scale 4: 16 crypts (1x16) in 0.020259 seconds, 789 c/s
OMP scale 8: 32 crypts (1x32) in 0.039103 seconds, 818 c/s
OMP scale 16: 64 crypts (1x64) in 0.074535 seconds, 858 c/s +
OMP scale 32: 128 crypts (1x128) in 0.140089 seconds, 913 c/s +
Autotune found best speed at OMP scale of 32
DONE
Speed for cost 1 (N) of 16384, cost 2 (r) of 8, cost 3 (p) of 1
Raw:	161 c/s real, 40.5 c/s virtual
```
Ummm but why is auto-tune getting 913 c/s and benchmark just 161 c/s?
> Ummm but why is auto-tune getting 913 c/s and benchmark just 161 c/s?

I was wondering, too. Makes no sense to me. I think we should investigate this before possibly making any changes to MKPC and/or OMP_SCALE. For scrypt-using formats where we'll commonly have many salts ("scrypt", "django-scrypt"), I expect we should use MKPC=1 and OMP_SCALE=1. With typical settings, scrypt is slow enough for that. Having high MKPC and/or OMP_SCALE along with a large number of salts has usability impact.
> On a side-note the scrypt format doesn't seem to have good figures for my laptop i7, with the new code.

Can you be more specific - e.g., show what its speeds were before and what they are now? Or were you merely referring to the (bogus) auto-tune speeds?
Yes, I just looked at current auto-tune
161 c/s is a good realistic figure for scrypt at those tunable costs on a laptop. 913 c/s is unrealistic (it's an OK speed for a 16-core), and obviously wrong - but perhaps there's an explanation how that figure is correct for something else (e.g., some of the tunable costs being different during auto-tuning, such as r=1 instead of the usual and reported r=8 - but I have no idea how that can happen).
> I believe we could identify them using `../run/john -test -tune=report -form:cpu-dynamic` on some relevant hardware

Running this right now on super while looking at the figures from #3795 
There are **so** many formats. The only way to get this straight is (I think) to look really closely to each one of them, eg. 15 minutes per format (which is very optimistic), 257 formats == 65 hours == a week full time.

I'm still trying to use scripts, relbench and other things to spot any really important ones.
Changed just a few ones, we might revisit this later (post release). We'll never be able to get it really good for a lot of platforms - I think we should rework core instead.

Anyway we should now have a lot better ballpark figures for many formats. A good number of them were completely off after Dhiru's usual copy-paste errors. I'm closing this.
magnum, have you figured out "why is auto-tune getting 913 c/s and benchmark just 161 c/s"? There might be an auto-tune bug we want to fix, or at least some auto-tune property we need to be aware of.
I did look into it but couldn't spot any reason. I agree we should try to explain that - reopening for that.
It is not picking the cost it should.

```
index 25d560d..34a0f29 100644
--- a/src/omp_autotune.c
+++ b/src/omp_autotune.c
@@ -128,9 +128,11 @@ void omp_autotune_run(struct db_main *db)
 
                tune_cost = MIN(db->max_cost[0], options.loader.max_cost[0]);
 
-               while (s->next && s->cost[0] < tune_cost)
-                       s = s->next;
+               //while (s->next && s->cost[0] < tune_cost)
+               //      s = s->next;
                salt = s->salt;
+               salt = "$7$C6..../....";
+               printf("tune: %s\n", salt);
        }
 
        if (john_main_process && options.verbosity >= VERB_MAX) {

```
```
$ make -sj8 && ../run/john -test -format:scrypt -tune=report -verb=5

Make process completed.
initUnicode(UNICODE, ASCII/ASCII)
ASCII -> ASCII -> ASCII
Will run 8 OpenMP threads
Benchmarking: scrypt (16384, 8, 1) [Salsa20/8 128/128 AVX]... (8xOMP) Loaded 10 hashes with 10 different salts to test db from test vectors

scrypt OMP autotune using test db with N of 16384

OMP scale 1: 8 crypts (1x8) in 0.037531 seconds, 213 c/s +
OMP scale 2: 16 crypts (1x16) in 0.062082 seconds, 257 c/s +
OMP scale 4: 32 crypts (1x32) in 0.117020 seconds, 273 c/s +
Autotuned OMP scale 4, preset is 1
DONE
Speed for cost 1 (N) of 16384, cost 2 (r) of 8, cost 3 (p) of 1
Raw:	288 c/s real, 35.9 c/s virtual

```
OK but why, can you see a logic error in there? IIRC we try to pick the hardest of the first cost, aren't we?
> OK but why, can you see a logic error in there?

You are working too much. There is a limitation in the algo and it is obvious.
* It is picking r=1 instead of r=8 because the check it is doing (and how it is doing) in cost[0] is not enough;
  * As an example: is r=8; C[0] = 1024 less or greater r=1; C[0] = 2048? I have NO idea.

Rambling: there is an easy fix for this situation (to these test vectors), but I doubt it applies to all hashes and ciphers out there: 
* A generic solution requires a _Comparator_ (as in `class Xxx implements Comparator`).
***

That said, I am 100% out of the discussion.


I was assuming `db->salts` (using test_db) was pointing at the first test vector's salt. Due to salt sorting, it wasn't. I'll commit a fix for this.
```diff
diff --git a/src/loader.c b/src/loader.c
index ac411b0fa..53021b890 100644
--- a/src/loader.c
+++ b/src/loader.c
@@ -1802,7 +1802,8 @@ void ldr_fix_database(struct db_main *db)
                ldr_remove_marked(db);
        }
        ldr_cost_ranges(db);
-       ldr_sort_salts(db);
+       if (db->real && db->real == db)
+               ldr_sort_salts(db);
        ldr_init_hash(db);
 
        ldr_init_sqid(db);
```
> I was assuming `db->salts` (using test_db) was pointing at the first test vector's salt. Due to salt sorting, it wasn't. I'll commit a fix for this.

@magnumripper As discussed in #3897, that fix doesn't achieve the desired effect you described here.