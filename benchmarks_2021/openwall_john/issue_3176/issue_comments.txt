Warn when -dev lists more OpenCL devices than the number in -fork
> when a user specifies e.g. "--dev=0,1" without a "--fork", John silently uses the first device only.  

At least one OpenCL format works properly without "--fork" (the format itself has multi-device support). 

> Important: this new logic should apply to OpenCL devices only

So, at least for mscash2-OpenCL, the warning would be wrong and should not be printed.
Yes, we should exclude mscash2-opencl if it still has this special property (I am unsure if it still does).

And maybe besides warning the user we should also increase the forks count to match the number of devices. Similarly to how we adjust incremental mode's MaxLen when (combined with mask) it'd be greater than the hash type's maximum, and we warn the user about this change JtR made for them.
@claudioandre it would be awesome if you have time to look into this issue, I know you're up to it.

It's probably a case of looking around for quite some time and then just add a handful lines of code at some good place.
BTW, IMO we should add a correct number of forks automagically and just notice the user.
> Yes, we should exclude mscash2-opencl if it still has this special property (I am unsure if it still does).

It does
```
$ ../run/john --test=10 --format=mscash2-opencl  -dev=6,7
../run/john: /usr/local/cuda-8.0/targets/x86_64-linux/lib/libOpenCL.so.1: no version information available (required by ../run/john)
Benchmarking: mscash2-opencl, MS Cache Hash 2 (DCC2) [PBKDF2-SHA1 OpenCL]... Device 6: GeForce GTX TITAN X
Device 7: GeForce GTX TITAN
DONE, GPU util:99%, GPU1:99%
Warning: "Many salts" test limited: 9/256
Many salts:	297123 c/s real, 296013 c/s virtual
Only one salt:	287165 c/s real, 286128 c/s virtual
```

```
$ nvidia-smi
Wed Mar  7 15:36:31 2018       
+------------------------------------------------------+                       
| NVIDIA-SMI 361.28     Driver Version: 361.28         |                       
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|===============================+======================+======================|
|   0  GeForce GTX TITAN   Off  | 0000:83:00.0     Off |                  N/A |
| 32%   45C    P0    83W / 250W |    100MiB /  6143MiB |     98%      Default |
+-------------------------------+----------------------+----------------------+
|   1  GeForce GTX TIT...  Off  | 0000:84:00.0     Off |                  N/A |
| 22%   64C    P2   218W / 250W |    168MiB / 12287MiB |     99%      Default |
+-------------------------------+----------------------+----------------------+
                                                                               
+-----------------------------------------------------------------------------+
| Processes:                                                       GPU Memory |
|  GPU       PID  Type  Process name                               Usage      |
|=============================================================================|
|    0     15331    C   ../run/john                                     83MiB |
|    1     15331    C   ../run/john                                    143MiB |
+-----------------------------------------------------------------------------+
```
> it would be awesome if you have time to look into this issue, I know you're up to it.

Kind of. `super` and `well` are not functional. Anyway, I will try to copy code from my desktop to them and see if I can build and test.

Both:
```
error:  while accessing https://github.com/magnumripper/JohnTheRipper.git/info/refs

fatal: HTTP request failed
```
The workaround for the GitHub TLS issue is to use git:// URLs (you can edit .git/config in the already cloned tree, then pull will work again). See: https://twitter.com/solardiz/status/971066158265831424

Yes, this is not ideal. Maybe we should build newer OpenSSL or LibreSSL or whatever, and git linked with it, somewhere under /opt.
> BTW, IMO we should add a correct number of forks automagically and just notice the user.

There is a problem to do this way. To achieve it we need:
* to parse `--devices`
* to load `database.format->params.label` (to compare with "-opencl")

So, it can't be done very soon.

But, very soon, e.g., inside `opt_init()` decisions are taken based on `options.fork`. That said, should we copy code from `opt_init()` to the routine that corrects the fork number? This seems a bad idea.
I figured we would hit that problem. If this is at all possible to solve without it getting nasty, I think instead of *copying* we should *move* needed code to a new function, then call it from `opt_init()` (so practically no change from now at that point) and then *again* called later if needed.

For now (or forever if the above ends up too messy - I'll have a look at it some rainy day) your PR looks fine, thank you!
I'm not saying it will be impossible, but I was also thinking about all the early setup process for:
- mpi + fork;
- .log or .rec (or both) files usage per forked process;
- and the postponed OpenCL initialization when forking.

It seems to be a very invasive change!
I just realized the merged fix will break MPI. I'll try to fix that real quick.
I just also realized it broke normal single-GPU use (because `options.fork` is 0 when option not present). Reverted it. I will commit a fix soon.
I committed a new version with the code moved to later, as MPI postpones `opencl_preinit()`. It's tested with & without fork, MPI and a simulated lack of OS_FORK.

The MPI case works so-so but is not 100%. It's hard for a single node to tell what the whole cluster looks like. It should be enough for the most common user errors though.
You just beat me, but, I'm afraid there is another problem:
```
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,2,6 --fork=2
```

Will not warn you! And that is what I was about to put some notes here.
I saw that too just now. It's not the end of the world - most users will either get it right or leave `-fork` out completely. But we should fix it... why does it happen? I think your original patch worked correctly in that regard.
After fork, each forked process sees only the GPU it is scheduled to use. So: 
* `get_number_of_devices_in_use() == 1`.

But, it is now very close. I'm testing non MPI. You test (at least) MPI next.

```
$ ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,2,6 --fork=2
../run/john: /usr/local/cuda-8.0/targets/x86_64-linux/lib/libOpenCL.so.1: no version information available (required by ../run/john)
Using default input encoding: UTF-8
Loaded 7 password hashes with 5 different salts (sha256crypt-opencl, crypt(3) $5$ [SHA256 OpenCL])
Remaining 5 password hashes with 3 different salts
Cost 1 (iteration count) is 5000 for all loaded hashes
Node numbers 1-2 of 2 (fork)
Device 2: Tahiti [AMD Radeon HD 7900 Series]
Device 7: GeForce GTX TITAN
Error: To fully use the 3 devices requested, you must specify --fork=3
(see doc/README-OPENCL)
```
The MPI case is very icky. An MPI node can see a correct figure for `get_number_of_requested_devices()` and it can know how many total processes are running. I think if we'd replace all `get_number_of_devices_in_use()` with `get_number_of_requested_devices()` we'd get very close.
Thinking loud: For MPI, the devices option is normally `-dev=gpu` which **may** resolve to completely different figures per host (some may have one GPU, some may have two, some may have eight).

Each node then gets its own figure of `get_number_of_requested_devices()`, which is **local**. So using `mpi_p` isn't a good number at all (it may be 100 but that's not relevant for anything here!). On most systems, we'll have `mpi_p_local` as well. That is the number of processes on this very host. Perhaps we should use that... but in case it's 0, it's not supported by the MPI framwork used, so we should probably ignore the test completely.
Also, in some never committed version I used something along the lines of this
```c
	if (database.format &&
	    strstr(database.format->params.label, "-opencl") &&
	    !strstr(database.format->params.label, "mscash2-opencl") &&
#if HAVE_MPI
	    mpi_p_local *
#endif
	    (options.fork ? options.fork : 1) < get_number_of_requested_devices())
	{
		if (john_main_process)
		fprintf(stderr, "Error: To fully use the %d devices requested, "
		(...)
```
So that will be
- If neither --fork or MPI was used: 1
- If --fork was used: number of forks
- If --fork was not used, but MPI was: number of processes on this very host
- In the strange case that we'd have both (which isn't really supported), we should end up with a correct figure anyway (eg. 4 local MPI processes running 2 forks each == 8)
So, perhaps to safe-guard against non-supported `mpi_p_local`
```c
	if (database.format &&
	    strstr(database.format->params.label, "-opencl") &&
	    !strstr(database.format->params.label, "mscash2-opencl") &&
#if HAVE_MPI
	    (mpi_p_local ? mpi_p_local : mpi_p) *
#endif
	    (options.fork ? options.fork : 1) < get_number_of_requested_devices())
	{
		if (john_main_process)
		fprintf(stderr, "Error: To fully use the %d devices requested, "
		(...)
```
That will *work* on a machine not supporting a correct `mpi_p_local`, but it won't protect from all user errors.
BTW we **could** opt to use one code path, very close to your original PR, for non-MPI and another one (closer to what's in place now) for MPI. I don't like it a lot but it might end up better.
The pending fix is, basically, this:

```
diff --git a/src/john.c b/src/john.c
index 240ce9366..fd95f510b 100644
--- a/src/john.c
+++ b/src/john.c
@@ -1395,7 +1395,7 @@ static void john_load(void)
        if (database.format &&
            strstr(database.format->params.label, "-opencl") &&
            !strstr(database.format->params.label, "mscash2-opencl") &&
-           (options.fork ? options.fork : 1) < get_number_of_devices_in_use())
+           (options.fork ? options.fork : 1) < get_number_of_requested_devices())
        {
                if (john_main_process)
                fprintf(stderr, "Error: To fully use the %d devices requested, "
@@ -1406,9 +1406,9 @@ static void john_load(void)
                        "(see doc/README-OPENCL)\n",
                        get_number_of_requested_devices(),
 #if HAVE_MPI
-                       get_number_of_devices_in_use(),
+                       get_number_of_requested_devices(),
 #endif
-                       get_number_of_devices_in_use());
+                       get_number_of_requested_devices());
                error();
        }
 #else
```
The downside of NOT using `get_number_of_devices_in_use()` is that you missed any correction it does (it knows if a device is not working properly and removes it from the list).

Anyway, I can't imagine JtR handling the issue better. The tests were:
```
rm -f ../run/pot; ../run/john alltests.in --format=raw-sha256 --max-run=30
rm -f ../run/pot; ../run/john alltests.in --format=raw-sha256-opencl  --max-run=30
rm -f ../run/pot; ../run/john alltests.in --format=raw-sha256-opencl -dev=7  --max-run=30
rm -f ../run/pot; ../run/john alltests.in -fork=3 --format=raw-sha256  --max-run=30
rm -f ../run/pot; ../run/john alltests.in -fork=4 --format=raw-sha256-opencl --max-run=30 #OPS
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,0 --max-run=30  #error
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,2 --fork=2 --max-run=30
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,2,6 --fork=2 --max-run=30 #error
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,2,6 --fork=3 --max-run=30
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=7,2,61 --fork=3 --max-run=30
rm -f ../run/pot; ../run/john alltests.in --format=sha256crypt-opencl  -dev=72,27,61 --fork=3
```
If your code is able to deal with MPI-run N nodes `--dev=GPU`, properly (node 1 has 1 GPU, ..., node N has 4 GPUs), that is it. :+1: 

```c
	if (database.format &&
	    strstr(database.format->params.label, "-opencl") &&
	    !strstr(database.format->params.label, "mscash2-opencl") &&
#if HAVE_MPI
	    (mpi_p_local ? mpi_p_local : mpi_p) *
#endif
	    (options.fork ? options.fork : 1) < get_number_of_requested_devices())
	{
		if (john_main_process)
		fprintf(stderr, "Error: To fully use the %d devices requested, "
		(...)
```
I'll try to merge our patches posted here and do a PR
Skipped the PR step, looks jolly good.
Thanks @claudioandre 
> error: while accessing https://github.com/magnumripper/JohnTheRipper.git/info/refs
> fatal: HTTP request failed

This is indeed off-topic here, but since it happened to be discussed on this issue before: I've just updated the packages on `well`, and it can access GitHub via https fine again. No update on `super` is currently planned, so have to continue with `git://` there.
Updated some packages on `super` as well, so it can access GitHub via https fine again.