Single mode fixes
I would like to add here, that we should NOT have a commented out line
```
# Un-commenting this stops Single mode from re-testing guessed plaintexts
# with all other salts.
#SingleRetestGuessed = N
```

But should be
```
# Change to 'N' to stop Single mode from re-testing guessed plaintexts
# with all other salts.
SingleRetestGuessed = Y
```

These commented out lines in the Options section should all be looked at, and (if possible), uncommented with proper default setting set, and the comment changed.  This will allow the option to show up if the ``` --list=parameters:Options ```

Yes, a bit off-topic here, but I wanted this to be talked about. If it is deemed 'a good thing', then we can easily create a issue to hang it on.
Good idea. Please open an issue for reviewing and fixing all such cases.
I will now look at this: https://github.com/magnumripper/JohnTheRipper/issues/2425#issuecomment-454890010