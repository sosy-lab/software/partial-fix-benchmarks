diff --git a/doc/NEWS b/doc/NEWS
index eee4573844..115af2e752 100644
--- a/doc/NEWS
+++ b/doc/NEWS
@@ -132,6 +132,9 @@ Major changes from 1.9.0-jumbo-1 (May 2019) in this bleeding-edge version:
 
 - Add support for --format=LIST  [magnum; 2020]
 
+- Add alternative syntax to --salts option for loading "N most populated
+  salts" as opposed to "salts having at least N hashes"  [magnum; 2020]
+
 
 Major changes from 1.8.0-jumbo-1 (December 2014) to 1.9.0-jumbo-1 (May 2019):
 
diff --git a/doc/OPTIONS b/doc/OPTIONS
index f84217f3ba..dcbca54f2a 100644
--- a/doc/OPTIONS
+++ b/doc/OPTIONS
@@ -248,15 +248,21 @@ In Jumbo, the --users/groups/shells options above allow a single wildcard
 text matching also becomes case insensitive.
 
 --salts=[-]N[:MAX]		load salts with[out] at least N passwords
-
-This is a feature which allows to achieve better performance in some
-special cases.  For example, you can crack only some salts using
-"--salts=2" faster and then crack the rest using "--salts=-2".  Total
-cracking time will be about the same, but you will likely get some
-passwords cracked earlier.  If MAX is listed, then no hashes are
-loaded where there are more than MAX salts.  This is so that if you
-have run --salts=25 and then later can run --salts=10:24 and none of
-the hashes that were already done from the --salts=25 will be re-done.
+--salts=#M[-N]			load M [to N] most populated salts
+
+This feature aids in exploiting re-used salts.  For example, you can attack
+all re-used salts using "--salts=2" faster and then crack the rest using
+"--salts=-2".  Total cracking time will be about the same but you will
+likely get some passwords cracked earlier.  If MAX is listed, then no
+salts having more than MAX hashes are loaded.  This is so that if you have
+run --salts=25 you can later run --salts=10:24 for excluding the salts
+already attacked.
+Alternative #M syntax:  Instead of requiring a certain number of hashes
+per salt, we simply say "load the most re-used salt" or "load the M to N
+most re-used salts".
+Regardless of syntax, the re-use counts are considered before sorting out
+already cracked hashes, so a resume or a later attack using same --salts
+option will pick the same set.
 
 --costs=[-]Cn[:Mn][,...]	load salts with[out] cost value Cn [to Mn]
 				for tunable cost parameters
diff --git a/src/loader.c b/src/loader.c
index 8e3468eec8..b61069eada 100644
--- a/src/loader.c
+++ b/src/loader.c
@@ -1409,7 +1409,16 @@ static void ldr_init_sqid(struct db_main *db)
 	} while ((current = current->next));
 }
 
-/* #define DEBUG_SALT_SORT */
+static void ldr_gen_salt_md5(struct db_salt *s, int dynamic)
+{
+#ifndef DYNAMIC_DISABLED
+	if (dynamic) {
+		dynamic_salt_md5(s);
+		return;
+	}
+#endif
+	dyna_salt_md5(s, ldr_fmt_salt_size);
+}
 
 /*
  * This was done as a structure to allow more data to be
@@ -1421,14 +1430,8 @@ static void ldr_init_sqid(struct db_main *db)
  * so for ease of debugging, I have left this as a simple
  * structure
  */
-typedef struct salt_cmp_s
-{
+typedef struct salt_cmp_s {
 	struct db_salt *p;
-#ifdef DEBUG_SALT_SORT
-	/* used by JimF in debugging.  Left in for now */
-	int org_idx;
-	char str[36];
-#endif
 } salt_cmp_t;
 
 /*
@@ -1448,94 +1451,70 @@ static int (*fmt_salt_compare)(const void *x, const void *y);
  * since qsort is pretty quick, and does not call compare any more than
  * is needed to partition sort the data.
  */
-static int ldr_salt_cmp(const void *x, const void *y) {
+static int ldr_salt_cmp(const void *x, const void *y)
+{
 	salt_cmp_t *X = (salt_cmp_t *)x;
 	salt_cmp_t *Y = (salt_cmp_t *)y;
 	int cmp = fmt_salt_compare(X->p->salt, Y->p->salt);
 	return cmp;
 }
 
-static int ldr_salt_cmp_num(const void *x, const void *y) {
+static int ldr_salt_cmp_default(const void *x, const void *y)
+{
 	salt_cmp_t *X = (salt_cmp_t *)x;
 	salt_cmp_t *Y = (salt_cmp_t *)y;
 	int cmp = dyna_salt_cmp(X->p->salt, Y->p->salt, ldr_fmt_salt_size);
 	return cmp;
 }
 
-static void ldr_gen_salt_md5(struct db_salt *s, int dynamic) {
-#ifndef DYNAMIC_DISABLED
-	if (dynamic) {
-		dynamic_salt_md5(s);
-		return;
-	}
-#endif
-	dyna_salt_md5(s, ldr_fmt_salt_size);
+/* Sort by salt's number of hashes (for --salt option) */
+static int ldr_salt_cmp_num(const void *x, const void *y)
+{
+	salt_cmp_t *X = (salt_cmp_t *)x;
+	salt_cmp_t *Y = (salt_cmp_t *)y;
+
+	if (X->p->count > Y->p->count) return -1;
+	if (X->p->count < Y->p->count) return 1;
+	return 0;
 }
 
 /*
- * If there are more than 1 salt AND the format exports a salt_compare
- * function, then we reorder the salt array into the order the format
- * wants them in.  The rationale is usually that the format can
- * gain speed if some of the salts are grouped together.  This was first
- * done for the WPAPSK format so that the format could group all ESSID's
- * in the salts. So the PBKDF2 is computed once (for the first instance
- * of the ESSID), then all of the other salts which are different but
- * have the same ESSID will not have to perform the very costly PBKDF2.
- * The format is designed to work that way, IF the salts come to it in
- * the right order.
- *
- * Later, a bug was found in dynamic (hopefully not in other formats)
- * where formats like md5(md5($p).$s) would fail if there were salts of
- * varying length within the same input file. The longer salts would
- * leave stale data. If we sort the salts based on salt string length,
- * this issue goes away with no performance overhead.  So this function
- * is now also used for dynamic.
- *
- * we sort salts always, so that they are put into a deterministic order.
+ * We always sort salts, so that they are put into a deterministic order.
  * That way, we can restore a session and skip ahead until we find the
  * last salt being worked on. Without a deterministic sort, that logic
  * would fail under many situations.
- *
  */
-static void ldr_sort_salts(struct db_main *db)
+static void ldr_sort_salts(struct db_main *db, int by_count)
 {
 	int i;
 	struct db_salt *s;
-#ifndef DEBUG_SALT_SORT
 	salt_cmp_t *ar;
-#else
-	salt_cmp_t ar[100];  /* array is easier to debug in VC */
-#endif
+
 	if (db->salt_count < 2)
 		return;
 
 	if (john_main_process)
-		log_event("Sorting salts, for performance");
+		log_event("Sorting salts, for %s", by_count ? "picking ones with most hashes" : "deterministic salt-resume");
 
 	fmt_salt_compare = db->format->methods.salt_compare;
-#ifndef DEBUG_SALT_SORT
 	ar = (salt_cmp_t *)mem_alloc(sizeof(salt_cmp_t)*db->salt_count);
-#endif
 	s = db->salts;
 
 	/* load our array of pointers. */
 	for (i = 0; i < db->salt_count; ++i) {
 		ar[i].p = s;
-#ifdef DEBUG_SALT_SORT
-		ar[i].org_idx = i;
-		strncpy(ar[i].str, (char*)s->salt, 36);
-		ar[i].str[35] = 0; /*just in case*/
-#endif
 		s = s->next;
 	}
 
 	ldr_fmt_salt_size = db->format->params.salt_size;
 
 	dyna_salt_init(db->format);
-	if (fmt_salt_compare)
+	if (by_count)
+		qsort(ar, db->salt_count, sizeof(ar[0]), ldr_salt_cmp_num);
+	else if (fmt_salt_compare)
 		qsort(ar, db->salt_count, sizeof(ar[0]), ldr_salt_cmp);
 	else /* Default sort function, ensuring salt resume works if possible */
-		qsort(ar, db->salt_count, sizeof(ar[0]), ldr_salt_cmp_num);
+		qsort(ar, db->salt_count, sizeof(ar[0]), ldr_salt_cmp_default);
 
 	/* Reset salt hash table, if we still have one */
 	if (db->salt_hash) {
@@ -1564,14 +1543,7 @@ static void ldr_sort_salts(struct db_main *db)
 	}
 	s->next = 0;
 
-#ifndef DEBUG_SALT_SORT
 	MEM_FREE(ar);
-#else
-	/* setting s here, allows me to debug quick-watch s=s->next
-	 * over and over again while watching the char* value of s->salt
-	 */
-	s = db->salts;
-#endif
 }
 
 /*
@@ -1686,6 +1658,39 @@ static void ldr_filter_salts(struct db_main *db)
 	} while ((current = current->next));
 }
 
+/*
+ * Keep [m-]n best salts in terms of number of password hashes.
+ */
+static void ldr_filter_n_best_salts(struct db_main *db)
+{
+	struct db_salt *current, *last;
+	int from = db->options->min_pps;
+	int to = db->options->max_pps;
+	int num = 0;
+
+	if (!from) {
+		if (!to) return;
+		from = to;
+	}
+
+	last = NULL;
+	if ((current = db->salts))
+	do {
+		num++;
+		if (num > to || num < from) {
+			dyna_salt_remove(current->salt);
+			if (last)
+				last->next = current->next;
+			else
+				db->salts = current->next;
+
+			db->salt_count--;
+			db->password_count -= current->count;
+		} else
+			last = current;
+	} while ((current = current->next));
+}
+
 /*
  * check if cost values for a particular salt match
  * what has been requested with the --costs= option
@@ -2058,13 +2063,17 @@ void ldr_fix_database(struct db_main *db)
 		MEM_FREE(db->salt_hash);
 
 	if (!ldr_loading_testdb) {
-		ldr_filter_salts(db);
+		if (db->options->best_pps) {
+			ldr_sort_salts(db, 1);
+			ldr_filter_n_best_salts(db);
+		} else
+			ldr_filter_salts(db);
 		ldr_filter_costs(db);
 		ldr_remove_marked(db);
 	}
 	ldr_cost_ranges(db);
 	if (!ldr_loading_testdb)
-		ldr_sort_salts(db);
+		ldr_sort_salts(db, 0);
 	ldr_init_hash(db);
 
 	ldr_init_sqid(db);
diff --git a/src/loader.h b/src/loader.h
index 30cbf8f250..484a134d16 100644
--- a/src/loader.h
+++ b/src/loader.h
@@ -197,9 +197,12 @@ struct db_options {
 /* Filters to use while loading */
 	struct list_main *users, *groups, *shells;
 
-/* Requested passwords per salt */
+/* Requested passwords per salt (load salts having at least [M:]N hashes) */
 	int min_pps, max_pps;
 
+/* If this is true, min/max_pps refers to "best counts" (load the [M-]N most used salts) */
+	int best_pps;
+
 #ifndef BENCH_BUILD
 /* Requested cost values */
 	unsigned int min_cost[FMT_TUNABLE_COSTS];
diff --git a/src/options.c b/src/options.c
index b5addc3abb..c1f426a4c6 100644
--- a/src/options.c
+++ b/src/options.c
@@ -409,7 +409,8 @@ JOHN_USAGE_REGEX \
 "--users=[-]LOGIN|UID[,..]  [do not] load this (these) user(s) only\n" \
 "--groups=[-]GID[,..]       load users [not] of this (these) group(s) only\n" \
 "--shells=[-]SHELL[,..]     load users with[out] this (these) shell(s) only\n" \
-"--salts=[-]COUNT[:MAX]     load salts with[out] COUNT [to MAX] hashes\n" \
+"--salts=[-]COUNT[:MAX]     load salts with[out] COUNT [to MAX] hashes, or\n" \
+"--salts=#M[-N]             load M [to N] most populated salts\n" \
 "--costs=[-]C[:M][,...]     load salts with[out] cost value Cn [to Mn]. For\n" \
 "                           tunable cost parameters, see doc/OPTIONS\n" \
 "--save-memory=LEVEL        enable memory saving, at LEVEL 1..3\n" \
@@ -813,13 +814,25 @@ void opt_init(char *name, int argc, char **argv)
 
 	if (salts_str) {
 		int two_salts = 0;
-		if (sscanf(salts_str, "%d:%d", &options.loader.min_pps, &options.loader.max_pps) == 2)
+
+		if (salts_str[0] == '#') {
+			options.loader.best_pps = 1;
+			salts_str++;
+		}
+
+		if (options.loader.best_pps &&
+		    sscanf(salts_str, "%d-%d", &options.loader.min_pps, &options.loader.max_pps) == 2)
 			two_salts = 1;
-		if (!two_salts && sscanf(salts_str, "%d,%d", &options.loader.min_pps, &options.loader.max_pps) == 2)
+		else if (sscanf(salts_str, "%d:%d", &options.loader.min_pps, &options.loader.max_pps) == 2)
 			two_salts = 1;
-		if (!two_salts){
+		else if (sscanf(salts_str, "%d,%d", &options.loader.min_pps, &options.loader.max_pps) == 2)
+			two_salts = 1;
+
+		if (!two_salts) {
 			sscanf(salts_str, "%d", &options.loader.min_pps);
-			if (options.loader.min_pps < 0) {
+			if (options.loader.best_pps)
+				options.loader.max_pps = options.loader.min_pps;
+			else if (options.loader.min_pps < 0) {
 				options.loader.max_pps = -1 - options.loader.min_pps;
 				options.loader.min_pps = 0;
 			}
