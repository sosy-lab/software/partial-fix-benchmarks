diff --git a/doc/ENCODINGS b/doc/ENCODINGS
index f18fba112e..0731f5d33d 100644
--- a/doc/ENCODINGS
+++ b/doc/ENCODINGS
@@ -15,8 +15,8 @@ The new defaults (which can be changed in john.conf) are:
   * Output to screen, log and .pot file is UTF-8.
   * Target encoding for LM is CP850 (and input will be converted accordingly).
   * Internal encoding (eg. for rules processing) is disabled. For Western
-    Europe, ISO-8859-1 is recommended but since this has a performance impact
-    you'll need to use the command-line option or un-comment the line in
+    Europe, ISO-8859-1 is recommended but since this has a slight performance
+    impact you'll need to use the command-line option or un-comment the line in
     john.conf.
 
 For temporarily running "the old way", just give --enc=raw. You will still
@@ -42,6 +42,10 @@ shelter as much of the needed input as possible. For US/Western Europe, any
 this with a Unicode format like NT, it will silently be treated in another
 way internally for performance reasons but the outcome will be the same).
 
+Example: A rule that replaces all instances of "c" within a word with "ç" could
+be written as "-U scç" where "-U" means the rule is rejected unless we're
+running with some legacy codepage configured to handle it.
+
 Mask mode also honors --internal-codepage (or plain --encoding). For
 example, the mask ?L is a placeholder for all lowercase Greek letters if you
 use CP737. If you instead use CP850, it'll be western-european ones.
@@ -58,6 +62,13 @@ UTF-8 multibyte sequences in the middle, resulting in garbage. You can reject
 such rules with -U to have them in use only when UTF-8 is not used.
 
 Caveats:
+Please note that for convenience, any rule, mask or placeholder given in UTF-8
+on command-line or in a config file will be silently translated to whatever
+internal codepage is in use.  This will normally result in just what you'd
+expect but it could sometimes lead to confusion.  For example, if you run that
+"scç" rule with the internal codepage set to CP866, the "ç" can't be translated
+and will currently lead to some other undefined substitution with no warning.
+
 Beware of UTF-8 BOM's. They will cripple the first word in your wordlist or
 the first entry in your hashfile. Try to avoid using Windows tools that add
 them. This version does try to discard them though.
diff --git a/doc/NEWS b/doc/NEWS
index eee4573844..4d8d72fa02 100644
--- a/doc/NEWS
+++ b/doc/NEWS
@@ -132,6 +132,10 @@ Major changes from 1.9.0-jumbo-1 (May 2019) in this bleeding-edge version:
 
 - Add support for --format=LIST  [magnum; 2020]
 
+- If command-line or config-file rules are given in UTF-8 but we're utilizing
+  a legacy internal codepage, convert them to that codepage.  Mask mode already
+  had the corresponding feature.  [magnum; 2020]
+
 
 Major changes from 1.8.0-jumbo-1 (December 2014) to 1.9.0-jumbo-1 (May 2019):
 
diff --git a/doc/RULES b/doc/RULES
index d6f26ccfc5..a7f0b62421 100644
--- a/doc/RULES
+++ b/doc/RULES
@@ -28,10 +28,9 @@ natively.  Those additional commands may also be useful on their own.
 -8	reject this rule unless current hash type uses 8-bit characters
 -s	reject this rule unless some password hashes were split at loading
 -p	reject this rule unless word pair commands are currently allowed
--u	reject this rule unless --encoding=utf-8 or config variable
-	DefaultEncoding = UTF-8 is used
--U	reject this rule if --encoding=utf-8 or config variable
-	DefaultEncoding = UTF-8 is used
+-u	reject this rule unless the rules engine is running in UTF-8 mode (with
+	no internal codepage configured - see doc/ENCODINGS)
+-U	reject this rule if the rules engine is running in UTF-8 mode
 ->N	reject this rule unless length N or longer is supported
 -<N	reject this rule unless length N or shorter is supported (--min-length)
 
@@ -88,14 +87,9 @@ than that of "l" (length).
 The complement of a class can be specified by uppercasing its name.  For
 example, "?D" matches everything but digits.
 
-NOTE, if running in --encoding=iso-8859-1 (or koi8-r/cp1251/cp866,etc), then the
-high bit characters are added to the respective classes. So in iso-8859-1 mode,
-lower case ?l would include àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßÿ while in 'normal'
-runs, it is only a-z.
-
-NOTE 2, the rules engine currently have very limited understanding of UTF-8 so
-character classes etc. will only work with ASCII characters, even if using
---encoding=utf-8.
+If you've made John encoding-aware (see doc/ENCODINGS), the applicable national
+characters are added to the respective classes. So in ISO-8859-1 mode, ?l would
+include àáâãäåæçèéêëìíîïðñòóôõöøùúûüýþßÿ while in ASCII mode it is only a-z.
 
 
 	Simple commands.
@@ -120,9 +114,8 @@ WN is a superset of TN.  While TN only case-toggles alphabet letters, eg.
 "a" <-> "A", the WN command will shift-toggle as well.  This is akin to
 using shift or not on a US keyboard, eg. "1" <-> "!".
 
-NOTE, all of these are encoding-aware. Eg. if you do not specify an encoding,
-the l command will lowercase A-Z only. If you use --encoding=iso-8859-1 it will
-also recognise ÀÁÂÃÄÅÆÇÈÉÊËÌÍÎÏÐÑÒÓÔÕÖØÙÚÛÜÝÞ and lowercase them properly.
+All of these are encoding-aware.  Eg. if the rules engine is running CP737 it
+will also recognise Greek letters and upper/lowercase them properly.
 
 
 	String commands.
@@ -193,6 +186,11 @@ Note that square brackets ("[" and "]") are special characters to the
 preprocessor: you should escape them with a backslash ("\") if using
 these commands.
 
+Note that for UTF-8 (where a single character may be composed from several
+bytes) any notion of "number of characters" or "position N" will in fact
+count bytes, not characters.  Using "--internal-codepage" will mitigate that
+issue completely.  See doc/ENCODINGS.
+
 
 	Charset conversion commands.
 
diff --git a/src/rpp.c b/src/rpp.c
index bf6abb4b5c..0976203370 100644
--- a/src/rpp.c
+++ b/src/rpp.c
@@ -12,6 +12,8 @@
 #include "logger.h"
 #include "common.h" /* for atoi16 */
 #include "misc.h"   /* for strtokm */
+#include "options.h"
+#include "unicode.h"
 
 int rpp_real_run = 0;
 
@@ -117,6 +119,12 @@ static void rpp_process_rule(struct rpp_context *ctx)
 	flag_p = flag_r = 0;
 	ctx->count = ctx->refs_count = 0;
 
+/*
+ * If rule is given in UTF-8 but we're using an internal codepage, convert it in-place to that codepage.
+ */
+	if (options.internal_cp != UTF_8 && valid_utf8(input) > 1)
+		utf8_to_cp_r((char*)input, (char*)input, strlen((char*)input));
+
 	while (*input && output < end)
 	switch (*input) {
 	case '\\':
