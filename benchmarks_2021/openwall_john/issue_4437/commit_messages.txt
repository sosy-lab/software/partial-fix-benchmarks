rules.c: Drop unneeded checks for db being present
rules.c: Handle rules given in UTF-8 when using internal codepage

If we detect that a command-line or config-file rule is encoded in
UTF-8 while there's an internal legacy codepage in use, convert the
rule as appropriate.  Closes #4437
rules.c: Handle rules given in UTF-8 when using internal codepage

If we detect that a command-line or config-file rule is encoded in
UTF-8 while there's an internal legacy codepage in use, convert the
rule as appropriate.  Closes #4437
rpp.c: Handle rules given in UTF-8 when using internal codepage

If we detect that a command-line or config-file rule is encoded in
UTF-8 while there's an internal legacy codepage in use, convert the
rule as appropriate.  Closes #4437
rpp.c: Handle rules given in UTF-8 when using internal codepage

If we detect that a command-line or config-file rule is encoded in
UTF-8 while there's an internal legacy codepage in use, convert the
rule as appropriate.  Closes #4437
Improve handling of UTF-8 rules w/ internal codepage

The first iteration (d595de99) could not handle the case where a
conversion fails because the target codepage lack some needed
character.  This new code does not touch any rule unless it starts
with -U.  After conversion it checks for success and otherwise turn
the -U into -- which is an "always reject" flag (new, and undocumented
in doc/RULES because, well, it's of no use except for this).
Closes #4437.

Also drops a hack for "best-effort" codepage conversion that hardly ever
did any good - it would break the new (and coming) heuristics.
Improve handling of UTF-8 rules w/ internal codepage

The first iteration (d595de99) could not handle the case where a
conversion fails because the target codepage lack some needed
character.  This new code does not touch any rule unless it starts
with -U.  After conversion it checks for success and otherwise turn
the -U into -- which is an "always reject" flag (new, and undocumented
in doc/RULES because, well, it's of no use except for this).
Closes #4437.

Also drops a hack for "best-effort" codepage conversion that hardly ever
did any good - it would break the new (and coming) heuristics.
Improve handling of UTF-8 rules w/ internal codepage

The first iteration (d595de99) could not handle the case where a
conversion fails because the target codepage lack some needed
character.  This new code does not touch any rule unless it starts
with -U.  After conversion it checks for success and otherwise turn
the -U into -- which is an "always reject" flag (new, and undocumented
in doc/RULES because, well, it's of no use except for this).
Closes #4437.

Also drops a hack for "best-effort" codepage conversion that hardly ever
did any good - it would break the new (and coming) heuristics.
