Audit salted formats failing to remove dupe salts.
Could we make the TS find these for us? It might be as easy as looking at the output for number of loaded salts.

That actually should be easy.  Just take a single hash, replicate it 5 times.  Then if a test run shows 1, we are fine. If it shows > 1, then we have problems.

Good addition to 'internal'  :+1: 

I am not sure how to get this to auto fail. BUT it is easy to see which ones do fail.

```
7z               guesses:    6 Expected count(s) (1)
agilekeychain    guesses:    6 Expected count(s) (1)
blockchain       guesses:    6 Expected count(s) (1)
cloudkeychain    guesses:    6 Expected count(s) (1)
cq               guesses:    6 Expected count(s) (1)
crc32            guesses:    6 Expected count(s) (1)
form=keychain    guesses:    6 Expected count(s) (1)
form=kwallet     guesses:    6 Expected count(s) (1)
form=openssl-enc guesses:    6 Expected count(s) (1)
form=ssh-ng      guesses:    6 Expected count(s) (1)
```

The ones with guess of 6 are failing to remove dupe salts.  (7z and agilekeychain in this example).

But notice that IKE and HDAA are not in this list. I am very surprised with that, but will dig deeper.

The algorithm is:
1. only work on salted hash (field 11 of -list=format-details is > 0)
2. read first line from -list=format-tests
3. output that line 6 times
4. run a 'normal' test, but only expect 1 guess.

I 'think' that algorithm should show the problem formats.  But I expected HDAA and IKE, since they had pointers.  Possibly they have a salt_hash or some other format method that allows them to 'salt-dupe' remove properly.

I do not think salt_hash() is involved. That would not make sense. An extreme comparison would be checking for dupe hashes with get_binary_hash_0() which would produce a false hit out of 16...

Looks like we might need to hash strings to auto test this (I think).  In loader, it looks like there is code to auto dump same hash.  I am not sure if I can alternate 2 salts x number of times, and have it work.  I 'might' have to smash the binary to get the same salt to work, meaning I would have to have information on what part of the hash is the binary.  That means this auto method 'may' not be able to be auto done.  Too bad, this seemed like a simple way to find this information.

Then it might be easier to do this in a JtR self-test. We can get_salt() twice with same data and see if they return the same thing. If they do not, and it's not a DYNA_SALT format, we have a fail.

```
diff --git a/src/formats.c b/src/formats.c
index b79e223..277b9d6 100644
--- a/src/formats.c
+++ b/src/formats.c
@@ -145,6 +145,7 @@ static char *fmt_self_test_body(struct fmt_main *format,
        void *binary, *salt;
        int binary_align_warned = 0, salt_align_warned = 0;
        int salt_cleaned_warned = 0, binary_cleaned_warned = 0;
+       int salt_dupe_warned = 0;
 #ifndef BENCH_BUILD
        int dhirutest = 0;
        int maxlength = 0;
@@ -376,6 +377,21 @@ static char *fmt_self_test_body(struct fmt_main *format,
                        salt_align_warned = 1;
                }

+               /* validate that salt dupe checks will work */
+               if (!salt_dupe_warned &&
+                   (format->params.flags & FMT_DYNA_SALT) == 0) {
+                       char *copy = malloc(format->params.salt_size);
+
+                       memcpy(copy, salt, format->params.salt_size);
+                       salt = format->methods.salt(ciphertext);
+                       if (memcmp(copy, salt, format->params.salt_size)) {
+                               puts("Warning: Salt dupe detection might be "
+                                    "broken");
+                               salt_dupe_warned = 1;
+                       }
+                       MEM_FREE(copy);
+               }
+
                /* validate that salt() returns cleaned buffer */
                if (extra_tests && !salt_cleaned_warned && format->params.salt_size) {
                        if ((format->params.flags & FMT_DYNA_SALT) == FMT_DYNA_SALT) {
```

Result

```
Testing: dynamic_21 [HTTP Digest Access Auth 128/128 AVX 480x4x3]... Warning: Salt dupe detection might be broken
Testing: hdaa, HTTP Digest access authentication [MD5 128/128 AVX 12x]... Warning: Salt dupe detection might be broken
Testing: IKE, PSK [HMAC MD5/SHA1 32/64]... Warning: Salt dupe detection might be broken
Testing: LUKS [PBKDF2-SHA1 32/64]... Warning: Salt dupe detection might be broken
Testing: PBKDF2-HMAC-SHA256, rounds=12000 [PBKDF2-SHA256 128/128 SSE4.1 4x]... Warning: Salt dupe detection might be broken
Testing: PFX, PKCS12 (.pfx, .p12) [32/64]... Warning: Salt dupe detection might be broken
Testing: PKZIP [32/64]... Warning: Salt dupe detection might be broken
Testing: SSH (one 2048-bit RSA and one 1024-bit DSA key) [RSA/DSA 32/64]... Warning: Salt dupe detection might be broken
Testing: PBKDF2-HMAC-SHA256-opencl, rounds=12000 [PBKDF2-SHA256 OpenCL]... Warning: Salt dupe detection might be broken
```

All CUDA formats pass without the warning

The PBKDF2-HMAC-SHA256 formats does not have pointers in the salts. And they do clean their buffers. Not sure what the real problem is.

Is this a bug in base64_convert()?

```
diff --git a/src/formats.c b/src/formats.c
index 277b9d6..4bcb8e8 100644
--- a/src/formats.c
+++ b/src/formats.c
@@ -388,6 +388,8 @@ static char *fmt_self_test_body(struct fmt_main *format,
                                puts("Warning: Salt dupe detection might be "
                                     "broken");
                                salt_dupe_warned = 1;
+                               dump_stuff(copy, format->params.salt_size);
+                               dump_stuff(salt, format->params.salt_size);
                        }
                        MEM_FREE(copy);
                }
```

```
Benchmarking: PBKDF2-HMAC-SHA256, rounds=12000 [PBKDF2-SHA256 128/128 SSE4.1 4x]... (8xOMP) Warning: Salt dupe detection might be broken
101a8310 62cc59eb fd1fa394 3286d01a 03000a00 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 e02e0000 
101a8310 62cc59eb fd1fa394 3286d01a 03000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 00000000 e02e0000 
```

I'm taking a break. This self-test is committed, but no fix to any format.

> and it's not a DYNA_SALT format, we have a fail.

for dynasalt we should do dyna compare.

> Is this a bug in base64_convert()?

I will test it.  I think I need to validate that any 'extra' junk bytes are force set to null, or possibly, I should pre-null the entire buffer.  part of the problem lies with having to provide a max-length, AND having to provide a buffer that is longer.  Exactly what should I count on, within convert, OR should I ask for 2 lengths. One is max length of string, one is true length of buffer?  Or should the caller be tasked with buffer pre-cleaning ???  

I will dig in and find the problem and a general working fix for it.  I will also smoke these other salt dupe stuff.

I added all of them you found to the first post in this issue, as a checkbox list of what needs done.

I got the compare to work with all salts. Simply remove the check for dyna_salt, and replace memcmp with dyna_salt_cmp()  The dyna_salt_cmp() takes same params as memcmp, but smart enough to handle a format with dyna_salt, by doing the dyna_salt compare, and if the format does not have dyna_salt, then it falls back to original memcmp call.  I will get that checked in (same end results, none of the dyna_salt hashes had issues).

I think this is a proper fix for the pbkdf2-hmac-sha256 problem.

```
diff --git a/src/base64_convert.c b/src/base64_convert.c
index 12fa9f6..85bfd13 100644
--- a/src/base64_convert.c
+++ b/src/base64_convert.c
@@ -602,7 +602,7 @@ int base64_convert(const void *from, b64_convert_type from_t, int from_len, void
                }
                case e_b64_mime:        /* mime */
                {
-                       char *fromWrk = (char*)from, fromTmp[256];
+                       char *fromWrk = (char*)from, fromTmp[256] = { 0 };
                        int alloced=0;
                        while (fromWrk[from_len-1]=='=')
                                from_len--;
```

This might need to be copied to other sections too - and the allocs should be callocs. OR we could use strncpy instead of strnzcpy. @jfoug what do you think?

I found some unrelated off-by-one problems too. Committing some fixes.

Go for it. I will be on the road the next 4 to 6 hours, so I am down for now.

OK so that fixed pbkdf2-hmac-sha256 and pbkdf2-hmac-sha256-opencl but no other fails disappeared.

> I think this is a proper fix for the pbkdf2-hmac-sha256 problem.

I may audit this some, and try for a solution that does not cause such a heavy overhead as a full memset.

I am glad you found the problems in pbkdf2-hmac-sha256, since it does not trigger a problem on my builds (it may for linux, I was just using cygwin this morning, and it showed no failures).  Good that you could find/fix it.  This is just like kwallet. It was a problem on mine, but did not show up on yours. But I could find/fix it, so it is gone.  I am sure there will be others that get triggered on other systems (just like that alignment stuff). I would think we would have fewer of these 'hidden' salt-dupe failures, but we could have some.

> > I think this is a proper fix for the pbkdf2-hmac-sha256 problem.
> 
> I may audit this some, and try for a solution that does not cause such a heavy overhead as a full memset.

That's fine but OTOH if we are calling that function in hot code we probably want to rewrite the caller.

This was auto-closed by a commit message including "This does not fix #953", lol.

I do not believe there is any call in hot code, other than something like scrypt, which is so slow, that  the hot code overhead is acceptable, and will get only a few extra cracks per day trying to speed it up.  But a full buffer memset is rarely 'needed', but often used because someone does not want to take the time to figure out proper cleanup of buffer.  Its like turning off the light by shooting it with a shotgun. It works, but .....   lol.

> Its like turning off the light by shooting it with a shotgun.

That's fool-proof, me like. Few end-users problems - but you just had me put some more attention to the liability clauses in JtR license  o.O

The SSH format seems to need dynamic salt, not because size varies but because we need to skip comparing the darn BIO structures. I tried to do it (looking at rar and zip) but somehow it does not work at all.

The 'real' hard one was PKZIP. That was a mo-fo.  It has 3 possible files, in a array. Each of those has a variable length allocated pointer.  There were also other non-static data.  The other non-static is easy. Simply move it to the start of the struct, then skip it.  The other, what i had to do was make all the pointers NULL at build time. I then allocated enough to store ALL of the data into a trailing 1 byte array (allocated large enough to hold things).  Then in set_salt, I 'late' set the pointers.  set_salt is called AFTER all salt dupe stuff if done. So in the salt dupe, all pointers are null, and thus compare properly.

Only 1 left.

I think these are all done.  when I test, I see no 'dupe salt' warnings any more.

```
$ ../run/john -test=0 | grep -v PASS
Will run 8 OpenMP threads
Device 0: Cedar (ATI Radeon HD 5450)
Local worksize (LWS) 7, global worksize (GWS) 49
^C
Wait...
```
