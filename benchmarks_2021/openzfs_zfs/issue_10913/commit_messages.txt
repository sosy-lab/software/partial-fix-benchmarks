FreeBSD: convert teardown inactive lock to a read-mostly sleepable lock

The lock is taken all the time and as a regular read-write lock
avoidably serves as a mount point-wide contention point.

This forward ports FreeBSD revision r357322.

To quote aforementioned commit:

Sample result doing an incremental -j 40 build:
before: 173.30s user 458.97s system 2595% cpu 24.358 total
after:  168.58s user 254.92s system 2211% cpu 19.147 total

Reviewed-by: Alexander Motin <mav@FreeBSD.org>
Reviewed-by: Ryan Moeller <freqlabs@FreeBSD.org>
Signed-off-by: Mateusz Guzik <mjguzik@gmail.com>
Closes #10896
Partial fix for #10913 - reads from secondarycache=none datasets shouldn't count at l2arc misses.

Signed-off-by: Adam Moss <c@yotes.com>
Partial fix for #10913 - reads from secondarycache=none datasets shouldn't count as l2arc misses.

Signed-off-by: Adam Moss <c@yotes.com>
ARC reads from pools without an l2arc should not be accounted as l2arc misses.
The other half of the fix for #10913

Signed-off-by: Adam Moss <c@yotes.com>
ARC reads from pools without an l2arc shouldn't be l2arc misses.
The other half of the fix for #10913

Signed-off-by: Adam Moss <c@yotes.com>
Non-l2arc pool reads shouldn't be l2arc misses.
The other half of the fix for #10913

Signed-off-by: Adam Moss <c@yotes.com>
