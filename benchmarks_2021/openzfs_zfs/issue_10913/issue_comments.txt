arcstat l2_misses can be very incorrect
Pinging @gamanakis just 'cos they're an L2ARC expert at this point. :)

ZFS version should be:
2.0.0 RC1 (just to be clear this isn't release, in the future)
> ZFS version should be:
> 2.0.0 RC1 (just to be clear this isn't release, in the future)

It's the name of the upstream branch I was using - I'm not making any claims about its naming accuracy. :)
@adamdmoss Please just use the VERSION you are using... To keep things consistent, which is:  2.0.0 RC1 
I gave the commit id - 29bc31f
> I gave the commit id - [29bc31f](https://github.com/openzfs/zfs/commit/29bc31f62f3021e703513b269d5fa629063df1da)

It asks for a version, not a branch and commit.
People might want to help you in 4 weeks and notice "release" without noticing your commit link. Thats why versions are asked, not commit references. Doing something else than just giving the version (such as a branch and commit) when there is a valid version in the release tree leads to needless errors.
As I said before: it asks for versions for a reason. Branches and commit might be fine, but if there is a version, please use a version. Thats all i'm going to say about it.