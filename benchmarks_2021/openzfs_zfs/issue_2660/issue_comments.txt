Using a pristine disk should not complain about missing EFI label
@dswartz See #2507 which is designed to address this.  I've love to see this in the next tag but it needs a little more work before it can be merged.  Specifically, we should add code to make the partition scanning generic.  If you're interested in helping push this one over the finish line let me know.

Sure thing!

Brian Behlendorf notifications@github.com wrote:

> @dswartz See #2507 which is designed to address this. I've love to see this in the next tag but it needs a little more work before it can be merged. Specifically, we should add code to make the partition scanning generic. If you're interested in helping push this one over the finish line let me know.
> 
> —
> Reply to this email directly or view it on GitHub.￼

@dswartz Great.  Basically what still needs to happen is we need to rework the partition scanning code to be more generic like @prakashsurya suggested in the last comment.

@behlendorf maybe we should use libblkid to do the partition scanning.
This could help with https://github.com/zfsonlinux/zfs/issues/634 as well. 
See https://github.com/karelzak/util-linux/blob/master/libblkid/src/blkid.h.in

@maci0 Pull requests #2502, #2507, and #2512 are all related infrastructure designed to address this.  But there have been some subtle gotchas which needed to be resolved before those changes can be merged.  It would be great if someone had time to review those changes and push them over the finish line.
