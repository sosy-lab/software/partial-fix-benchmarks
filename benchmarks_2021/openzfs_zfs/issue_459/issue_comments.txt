ArchLinux ZFS build for 2.6.32 LTS
The 2.6.32 kernel is still supported and will be at least until RHEL6 is end of life in late 2017.  Your build failure looks like the result of incorrect configure results, the code in question which is failing hasn't changed in a quite a long time.  Have you tried performing a `make distclean`, `./configure`, `make rpm`.

When running 'sh autogen.sh' in the Arch VM, I am getting the following output:

<pre>

configure.ac:53: warning: AC_LANG_CONFTEST: no AC_LANG_SOURCE call detected in body
../../lib/autoconf/lang.m4:194: AC_LANG_CONFTEST is expanded from...
../../lib/autoconf/general.m4:2730: _AC_RUN_IFELSE is expanded from...
../../lib/m4sugar/m4sh.m4:606: AS_IF is expanded from...
../../lib/autoconf/general.m4:2749: AC_RUN_IFELSE is expanded from...
config/always-no-unused-but-set-variable.m4:9: ZFS_AC_CONFIG_ALWAYS_NO_UNUSED_BUT_SET_VARIABLE is expanded from...
config/zfs-build.m4:36: ZFS_AC_CONFIG_ALWAYS is expanded from...
config/zfs-build.m4:40: ZFS_AC_CONFIG is expanded from...
configure.ac:53: the top level
configure.ac:53: warning: AC_LANG_CONFTEST: no AC_LANG_SOURCE call detected in body
../../lib/autoconf/lang.m4:194: AC_LANG_CONFTEST is expanded from...
../../lib/autoconf/general.m4:2730: _AC_RUN_IFELSE is expanded from...
../../lib/m4sugar/m4sh.m4:606: AS_IF is expanded from...
../../lib/autoconf/general.m4:2749: AC_RUN_IFELSE is expanded from...
../../lib/m4sugar/m4sh.m4:606: AS_IF is expanded from...
../../lib/autoconf/libs.m4:100: AC_CHECK_LIB is expanded from...
../../lib/m4sugar/m4sh.m4:606: AS_IF is expanded from...
config/user-libblkid.m4:17: ZFS_AC_CONFIG_USER_LIBBLKID is expanded from...
config/user.m4:4: ZFS_AC_CONFIG_USER is expanded from...
config/zfs-build.m4:40: ZFS_AC_CONFIG is expanded from...
configure.ac:53: the top level
configure.ac:53: warning: AC_LANG_CONFTEST: no AC_LANG_SOURCE call detected in body
../../lib/autoconf/lang.m4:194: AC_LANG_CONFTEST is expanded from...
../../lib/autoconf/general.m4:2730: _AC_RUN_IFELSE is expanded from...
../../lib/m4sugar/m4sh.m4:606: AS_IF is expanded from...
../../lib/autoconf/general.m4:2749: AC_RUN_IFELSE is expanded from...
config/user-frame-larger-than.m4:4: ZFS_AC_CONFIG_USER_FRAME_LARGER_THAN is expanded from...
config/user.m4:4: ZFS_AC_CONFIG_USER is expanded from...
config/zfs-build.m4:40: ZFS_AC_CONFIG is expanded from...
configure.ac:53: the top level
</pre>


That is just a snippet of the output as it continues to complain. I haven't looked into exactly what the cause is, but I haven't seen this error on any of the other distributions I've used.

Interesting, but you shouldn't need to run autogen.sh, the provided configure script should work.  Does a ./configure; make; work?

Ahh.. I guess the Lustre workflow got me used to running that first. I'll try that out on my box at home and see what happens.

Well that was a pain.. I finally have the non PREEMPT kernel installed. Simply running ./configure and make appears to build just fine for me on my box at home. I think I'll need some additional information to reproduce the OP's issue.

> Your build failure looks like the result of incorrect configure results, the code in question which is failing hasn't changed in a quite a long time. Have you tried performing a make distclean, ./configure, make rpm

That was the clean sources. Just git-cloned. I was running it as this:

<pre>
./configure --prefix=/usr --sysconfdir=/etc --libexecdir=/usr/lib --with-spl=/.../spl-build
make
</pre>

> Well that was a pain.. I finally have the non PREEMPT kernel installed.

In Arch Linux there is kernel26-lts package. It's configured without PREEMPT. This is the kernel, I want to compile zfs for. To not bother with compiling kernel where I don't want.

Thanks for the extra information. I was using the 3.1.1 based kernel, and no configure arguments. I'll try out the kernel26-lts kernel package with your configure arguments and see if I can reproduce your build failure. Although, with the Thanksgiving holiday, I may no be able to get to that for a couple days.

> I'll try out the kernel26-lts kernel package

Don't forget to install core/kernel26-lts-headers too. :)
Also, if you familiar with building packages from AUR, you can use this: http://aur.archlinux.org/packages.php?ID=47434

Ok, so with the kernel26-lts packages I was able to reproduce the failure. Although, I haven't had the time to dig into what the cause is just yet.

And thanks for the link, I was google-ing around a few days ago and ran across that. Are you at all familiar with packaging for Arch? I was thinking about trying to upstream a set of patches to allow one to simply 'make arch' and have it spit out a pacman installable package for archlinux (similarly to how one can 'make rpm' and 'make tgz'). Looking at the PKGBUILD for the AUR package, I think it should be fairly straight forward to accomplish.

> Are you at all familiar with packaging for Arch? I was thinking about trying to upstream a set of patches to allow one to simply 'make arch' and have it spit out a pacman installable package for archlinux (similarly to how one can 'make rpm' and 'make tgz').

Here is shord description of format: https://wiki.archlinux.org/index.php/Creating_Packages#Overview
I can help with what I know. But I think it is somewhat uncommon way of making packages, because in Arch if someone need "non-official" package, then it is searched in AUR. So make install in this case is just fine too. :)

By the way, I want to redo this package in AUR and split it into modules and tools parts. Is there some type of make-ing of zfs that allows to compile and install them separately? Quick browsing of Makefile didn't help me.

The kernel and userspace parts are separated by 'zfs-modules' and 'zfs' respectively. I'm not sure how easy it is to selectively compile and install them separately without using RPMs. If you run 'make rpm' on an RPM based system (i.e. RHEL or Fedora) it will create separate packages for the kernel components and the user components.

The AUR package looks to simply create a single package for all of ZFS and SPL, kernel and userspace. This isn't the _right_ thing to do, IMO. There should be separate packages for ZFS and SPL, and then each of those should be split into kernel and user packages like is done for the other package formats (rpm and deb).

Are the official packages managed like the AUR packages? With a completely external PKGBUILD to do all the arch specific magic?

Here's what I think is causing the build failures:

<pre>
configure:18343: checking whether blk_fetch_request() is available
configure:18374: cp conftest.c build && make modules -C
/usr/src/linux-2.6.32.48-1-lts
EXTRA_CFLAGS=-Werror-implicit-function-declaration -Werror
M=/home/dunecn/dev/zfs/build
In file included from include/linux/irq.h:29:0,
                 from
/usr/src/linux-2.6.32.48-1-lts/arch/x86/include/asm/hardirq.h:5,
                 from include/linux/hardirq.h:10,
                 from include/linux/pagemap.h:15,
                 from include/linux/blkdev.h:12,
                 from /home/dunecn/dev/zfs/build/conftest.c:75:
/usr/src/linux-2.6.32.48-1-lts/arch/x86/include/asm/irq_regs.h: In
function 'set_irq_regs':
/usr/src/linux-2.6.32.48-1-lts/arch/x86/include/asm/irq_regs.h:26:2:
error: variable 'tmp__' set but not used
[-Werror=unused-but-set-variable]
In file included from
/usr/src/linux-2.6.32.48-1-lts/arch/x86/include/asm/hardirq.h:5:0,
                 from include/linux/hardirq.h:10,
                 from include/linux/pagemap.h:15,
                 from include/linux/blkdev.h:12,
                 from /home/dunecn/dev/zfs/build/conftest.c:75:
include/linux/irq.h: In function 'alloc_desc_masks':
include/linux/irq.h:441:8: error: variable 'gfp' set but not used
[-Werror=unused-but-set-variable]
In file included from include/linux/blkdev.h:12:0,
                 from /home/dunecn/dev/zfs/build/conftest.c:75:
include/linux/pagemap.h: In function 'fault_in_pages_readable':
include/linux/pagemap.h:415:16: error: variable 'c' set but not used
[-Werror=unused-but-set-variable]
cc1: all warnings being treated as errors
</pre>


This failure, and a couple others, during the configure step are preventing some macros from correctly getting set in zfs_config.h. The above snippet is a result of the ZFS_AC_KERNEL_BLK_FETCH_REQUEST. This macro _should_ succeed and set <pre>#define HAVE_BLK_FETCH_REQUEST 1</pre> in zfs_config.h. Unfortunately, the -Werror errors is causing the compilations to fail.

> The kernel and userspace parts are separated by 'zfs-modules' and 'zfs' respectively. I'm not sure how easy it is to selectively compile and install them separately without using RPMs. If you run 'make rpm' on an RPM based system (i.e. RHEL or Fedora) it will create separate packages for the kernel components and the user components.

But this is not RPM-based system. And have no rpm utils. Does make rpm use some make calls which separate it or it separates modules and utils after?

> The AUR package looks to simply create a single package for all of ZFS and SPL, kernel and userspace. This isn't the right thing to do, IMO.

This is questionable. That package aim is provide working zfs and there is no significant arguments for providing separate spl. Because I do not know for what else it can be used too. I'll be glad to hear arguments for separating it.

> Are the official packages managed like the AUR packages? With a completely external PKGBUILD to do all the arch specific magic?

Yes. Official packages is "based" on PKGBUILDs too. PKGBUILD in Arch is analogue of ebuild in Gentoo or *.spec file in RPM.

> Unfortunately, the -Werror errors is causing the compilations to fail. 

Is that issue of configure-file or my environment?

It's issue of the Arch kernel26-lts header files, they don't play well with the '-Wunused-but-set-variables' compile option.

As a temporary workaround, you should be able to apply the following patch and then run, 'sh autogen.sh; ./configure; make', that is assuming you have the autotools programs installed (i.e. autoconf, automake, etc..)

<pre>
diff --git a/config/kernel-blk-end-request.m4
b/config/kernel-blk-end-request.m4
index 20ad1a9..e1215c1 100644
--- a/config/kernel-blk-end-request.m4
+++ b/config/kernel-blk-end-request.m4
@@ -5,6 +5,7 @@ dnl # changed in 2.6.31 so it may be used by non-GPL
modules.
 dnl #
 AC_DEFUN([ZFS_AC_KERNEL_BLK_END_REQUEST], [
        AC_MSG_CHECKING([whether blk_end_request() is available])
+       EXTRA_KCFLAGS="-Wno-unused-but-set-variable"
        ZFS_LINUX_TRY_COMPILE([
                #include <linux/blkdev.h>
        ],[
</pre>


That should allow the configure scripts to properly detect the kernel API and set the proper config.h macros.

The best fix for this is to annotate the offending variable in the configure tests with `__attribute__ ((unused))` to suppress the warning for that specific instance.  I'm happy to merge a patch with this fix if you open a pull request.

As an aside feel free to open a new issue for the arch packaging issues.  I'm not familiar enough with the distribution to know what the _right_ thing to do there is.

Sorry for long silence, I was busy. I confirm that this patch helps with this problem. But now process breaks in other place: http://pastebin.com/KfHR3eU2
Here I run make on today's trunk and receive error during build. After that I run make with V=1 to receive verbose log of compiling with kernel headers.
Is it appropriate to reopen this issue, or open new?

Hmm.. Can you confirm the version you are building has the above referenced commit? I'm not seeing the same behavior as you in my Arch VM. Can you run the following commands and paste their output: `pacman -Qs kernel26-lts`, `pacman -Qs gcc`, and `uname -r`. Here's what my setup looks like:

<pre>
$ pacman -Qs kernel26-lts
local/kernel26-lts 2.6.32.48-1
    The Linux Kernel and modules - stable longtime supported kernel package suitable for servers
local/kernel26-lts-headers 2.6.32.48-1
    Header files and scripts for building modules for kernel26-lts
$ pacman -Qs gcc
local/gcc 4.6.2-1 (base-devel)
    The GNU Compiler Collection - C and C++ frontends
local/gcc-libs 4.6.2-1 (base)
    Runtime libraries shipped by GCC
$ uname -r
2.6.32.48-1-lts
</pre>

Reopening the issue seems reasonable until everyone agrees it's fixed.  I've done this and updated the subject to reflect that this is specifically an issue with the 2.6.32 LTS kernel and the ArchLinux build environment.

> Hmm.. Can you confirm the version you are building has the above referenced commit?

Yes. I have the same output as yours. My compile fails on 32-bit version. I have setup 64-bit Arch in VM on other machine. And there compilation succeeded. Do you tried 32 or 64 bit?

64 bit.

<pre>
$ uname -a
Linux ARCH 2.6.32.48-1-lts #1 SMP Fri Nov 11 17:27:05 CET 2011 x86_64 QEMU Virtual CPU version 0.12.3 GenuineIntel GNU/Linux
</pre>


AFAIK, The ZFS on Linux port doesn't support 32-bit. Brian, can you confirm this?

That's right.  We don't support 32-bit builds there are too many open issues running zfs in that environment.  It may have _mostly_ worked in the past, but it's not recommended.  

So as problem arises only on 32-bit system and it is not supported, then issue can be closed.

I've flagged this as a 32-bit only issue.  We'll probably revisit it at some point time permitting, or perhaps someone in the community who's interested in 32-bit environment can propose a fix.
