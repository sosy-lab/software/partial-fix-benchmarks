dbuf_destroy() NULL dereference
[note that I was the one reporting that issue on the mailing list initially]

I had a similar one last night:

<pre>
[216505.257678] general protection fault: 0000 [#1] SMP
[216505.260012] CPU 2
[216505.260012] Modules linked in: kvm_amd kvm sp5100_tco i2c_piix4 amd64_edac_mod tpm_tis edac_core psmouse edac_mce_amd k10temp acpi_power_meter dcdbas hed serio_raw joydev zfs(P) zcommon(P) znvpair(P) zavl(P) zunicode(P) spl zlib_deflate usbhid hid raid10 raid456 async_pq async_xor xor async_memcpy async_raid6_recov usb_storage uas mpt2sas raid6_pq async_tx scsi_transport_sas raid1 bnx2 raid_class raid0 multipath linear
[216505.260012]
[216505.260012] Pid: 80, comm: kswapd0 Tainted: P            3.1.2-030102-generic #201111211835 Dell Inc. PowerEdge R515/03X0MN
[216505.260012] RIP: 0010:[<ffffffffa01d6b02>]  [<ffffffffa01d6b02>] dbuf_destroy+0x62/0x170 [zfs]
[216505.260012] RSP: 0018:ffff880211d29a30  EFLAGS: 00010282
[216505.260012] RAX: ffff8803bfc490f8 RBX: ffff8803bfc49000 RCX: dead000000100100
[216505.260012] RDX: dead000000200200 RSI: 0000000000000000 RDI: ffff8802a045d260
[216505.260012] RBP: ffff880211d29a50 R08: 0000000000000000 R09: 0000000000000000
[216505.260012] R10: 0000000000000000 R11: 0000000000000282 R12: ffff8802a045cfa0
[216505.260012] R13: ffff8802a045d260 R14: 0000000000000001 R15: ffff88040f8ea108
[216505.260012] FS:  00007f5fb2ca6720(0000) GS:ffff880217a20000(0000) knlGS:0000000000000000
[216505.260012] CS:  0010 DS: 0000 ES: 0000 CR0: 000000008005003b
[216505.260012] CR2: 00000000080d5f98 CR3: 000000011a453000 CR4: 00000000000006e0
[216505.260012] DR0: 0000000000000000 DR1: 0000000000000000 DR2: 0000000000000000
[216505.260012] DR3: 0000000000000000 DR6: 00000000ffff0ff0 DR7: 0000000000000400
[216505.260012] Process kswapd0 (pid: 80, threadinfo ffff880211d28000, task ffff88021042ade0)
[216505.260012] Stack:
[216505.260012]  ffff8803bfc49000 ffff8803bfc49000 0000000000000000 ffff8800dc2287f8
[216505.260012]  ffff880211d29a70 ffffffffa01d8dee ffff880211d29ab0 ffff8803bfc49000
[216505.260012]  ffff880211d29a90 ffffffffa01d872a ffff8803bfc49000 0000000000000000
[216505.260012] Call Trace:
[216505.260012]  [<ffffffffa01d8dee>] dbuf_evict+0x1e/0x30 [zfs]
[216505.260012]  [<ffffffffa01d872a>] dbuf_rele_and_unlock+0xaa/0x210 [zfs]
[216505.260012]  [<ffffffffa01d8c3f>] dmu_buf_rele+0x2f/0x40 [zfs]
[216505.260012]  [<ffffffffa020efc8>] sa_handle_destroy+0x68/0xa0 [zfs]
[216505.260012]  [<ffffffffa0259a6d>] zfs_znode_dmu_fini+0x1d/0x30 [zfs]
[216505.260012]  [<ffffffffa025c89d>] zfs_zinactive+0xad/0xd0 [zfs]
[216505.260012]  [<ffffffffa0257a3e>] zfs_inactive+0x5e/0x1c0 [zfs]
[216505.260012]  [<ffffffff81120e6e>] ? truncate_pagecache+0x5e/0x70
[216505.260012]  [<ffffffffa0269768>] zpl_evict_inode+0x28/0x30 [zfs]
[216505.260012]  [<ffffffff8118cc23>] evict+0xb3/0x1c0
[216505.260012]  [<ffffffff8118cfcf>] dispose_list+0x4f/0x60
[216505.260012]  [<ffffffff8118d164>] prune_icache_sb+0x184/0x370
[216505.260012]  [<ffffffff81175a9b>] prune_super+0x14b/0x1a0
[216505.260012]  [<ffffffff811260a4>] shrink_slab+0x194/0x2d0
[216505.260012]  [<ffffffff81127f8e>] balance_pgdat+0x68e/0x900
[216505.260012]  [<ffffffff811283c6>] kswapd+0x1c6/0x210
[216505.260012]  [<ffffffff81128200>] ? balance_pgdat+0x900/0x900
[216505.260012]  [<ffffffff81088da6>] kthread+0x96/0xa0
[216505.260012]  [<ffffffff81609174>] kernel_thread_helper+0x4/0x10
[216505.260012]  [<ffffffff81088d10>] ? kthread_worker_fn+0x190/0x190
[216505.260012]  [<ffffffff81609170>] ? gs_change+0x13/0x13
[216505.260012] Code: 43 28 4c 8b a0 88 00 00 00 4d 8d ac 24 c0 02 00 00 4c 89 ef e8 60 6c 42 e1 48 89 d8 49 03 84 24 e8 02 00 00 48 8b 08 48 8b 50 08
[216505.260012]  89 51 08 48 89 0a 48 ba 00 01 10 00 00 00 ad de 48 b9 00 02
[216505.260012] RIP  [<ffffffffa01d6b02>] dbuf_destroy+0x62/0x170 [zfs]
[216505.260012]  RSP <ffff880211d29a30>
[216505.592100] ---[ end trace dee17120179a2e27 ]---
</pre>


I had been transfering data via nbd to several zvols for a while without problems. This crash occurred while both nbd and rsync (on zfs, not zvol) where taking place concurrently.

nbd-server is stuck on

<pre>
[250865.019183] nbd-server      D ffff8801e7699ab0     0  6562   6561 0x00000000
[250865.019183]  ffff8801dea43518 0000000000000086 ffff8801dea434a8 ffff880100000000
[250865.019183]  0000000000013100 ffff8801dea43fd8 ffff8801dea42010 0000000000013100
[250865.019183]  ffff8801dea43fd8 0000000000013100 ffff8802125144d0 ffff8801e76996f0
[250865.019183] Call Trace:
[250865.019183]  [<ffffffff815fcc4f>] schedule+0x3f/0x60
[250865.019183]  [<ffffffff815fd87f>] __mutex_lock_slowpath+0xdf/0x160
[250865.019183]  [<ffffffff811376f1>] ? unmap_mapping_range+0x91/0x190
[250865.019183]  [<ffffffff815fd77b>] mutex_lock+0x2b/0x50
[250865.019183]  [<ffffffffa025c84d>] zfs_zinactive+0x5d/0xd0 [zfs]
[250865.019183]  [<ffffffffa0257a3e>] zfs_inactive+0x5e/0x1c0 [zfs]
[250865.019183]  [<ffffffff81120e6e>] ? truncate_pagecache+0x5e/0x70
[250865.019183]  [<ffffffffa0269768>] zpl_evict_inode+0x28/0x30 [zfs]
[250865.019183]  [<ffffffff8118cc23>] evict+0xb3/0x1c0
[250865.019183]  [<ffffffff8118cfcf>] dispose_list+0x4f/0x60
[250865.019183]  [<ffffffff8118d164>] prune_icache_sb+0x184/0x370
[250865.019183]  [<ffffffff81175a9b>] prune_super+0x14b/0x1a0
[250865.019183]  [<ffffffff811260a4>] shrink_slab+0x194/0x2d0
[250865.019183]  [<ffffffff81126fe2>] do_try_to_free_pages+0x2d2/0x3c0
[250865.019183]  [<ffffffff8112735b>] try_to_free_pages+0x9b/0x120
[250865.019183]  [<ffffffff8111b76f>] __alloc_pages_slowpath+0x31f/0x6e0
[250865.019183]  [<ffffffff811a1e8e>] ? alloc_page_buffers+0x7e/0x100
[250865.019183]  [<ffffffff8111bcd4>] __alloc_pages_nodemask+0x1a4/0x1f0
[250865.019183]  [<ffffffff8115419a>] alloc_pages_current+0xaa/0x120
[250865.019183]  [<ffffffff81112f27>] __page_cache_alloc+0x87/0x90
[250865.019183]  [<ffffffff81113c97>] grab_cache_page_write_begin+0x67/0xd0
[250865.019183]  [<ffffffff811a7d60>] ? I_BDEV+0x20/0x20
[250865.019183]  [<ffffffff811a5278>] block_write_begin+0x38/0x90
[250865.019183]  [<ffffffff81112c0a>] ? unlock_page+0x2a/0x40
[250865.019183]  [<ffffffff811a9043>] blkdev_write_begin+0x23/0x30
[250865.019183]  [<ffffffff81112282>] generic_perform_write+0xc2/0x1d0
[250865.019183]  [<ffffffff8112a128>] ? shmem_getxattr+0xc8/0x120
[250865.019183]  [<ffffffff811123f4>] generic_file_buffered_write+0x64/0xa0
[250865.019183]  [<ffffffff811150b8>] __generic_file_aio_write+0x238/0x460
[250865.019183]  [<ffffffff814eba2e>] ? sock_aio_read+0x19e/0x1b0
[250865.019183]  [<ffffffff811a8266>] blkdev_aio_write+0x36/0x90
[250865.019183]  [<ffffffff8117270a>] do_sync_write+0xda/0x120
[250865.019183]  [<ffffffff812d5548>] ? apparmor_file_permission+0x18/0x20
[250865.019183]  [<ffffffff8129e3a3>] ? security_file_permission+0x23/0x90
[250865.019183]  [<ffffffff81172ca8>] vfs_write+0xc8/0x190
[250865.019183]  [<ffffffff81172e61>] sys_write+0x51/0x90
[250865.019183]  [<ffffffff81607002>] system_call_fastpath+0x16/0x1b
</pre>


and rsync on:

<pre>
[250867.550421] rsync           D ffff8802102edf80     0  8950   2696 0x00000004
[250867.550421]  ffff8801aba05bc8 0000000000000086 0000000000000001 ffff8803dcf3f800
[250867.550421]  0000000000013100 ffff8801aba05fd8 ffff8801aba04010 0000000000013100
[250867.550421]  ffff8801aba05fd8 0000000000013100 ffff88020f3edbc0 ffff8802102edbc0
[250867.550421] Call Trace:
[250867.550421]  [<ffffffff815fcc4f>] schedule+0x3f/0x60
[250867.550421]  [<ffffffff815fd87f>] __mutex_lock_slowpath+0xdf/0x160
[250867.550421]  [<ffffffff815fd77b>] mutex_lock+0x2b/0x50
[250867.550421]  [<ffffffffa025a2f9>] zfs_mknode+0x139/0xc30 [zfs]
[250867.550421]  [<ffffffffa0223711>] ? txg_rele_to_quiesce+0x11/0x20 [zfs]
[250867.550421]  [<ffffffffa0254b37>] zfs_mkdir+0x437/0x560 [zfs]
[250867.550421]  [<ffffffff81188dd5>] ? d_lookup+0x35/0x60
[250867.550421]  [<ffffffffa02694ce>] zpl_mkdir+0x9e/0xe0 [zfs]
[250867.550421]  [<ffffffff8129dd2c>] ? security_inode_permission+0x1c/0x30
[250867.550421]  [<ffffffff8117e7f4>] vfs_mkdir+0xa4/0xe0
[250867.550421]  [<ffffffff8118242b>] sys_mkdirat+0xeb/0xf0
[250867.550421]  [<ffffffff81182448>] sys_mkdir+0x18/0x20
[250867.550421]  [<ffffffff81607002>] system_call_fastpath+0x16/0x1b
</pre>

All blocked tasks:

<pre>
[250865.019183] kswapd1         D ffff880210429ab0     0    81      2 0x00000000
[250865.019183] zvol/0          D ffff88020f3edf80     0   509      2 0x00000000
[250865.019183] zvol/1          D ffff88020f3ec890     0   510      2 0x00000000
[250865.019183] zvol/2          D ffff88020f3eb1a0     0   511      2 0x00000000
[250865.019183] zvol/3          D ffff88020f3e9ab0     0   512      2 0x00000000
[250865.019183] zvol/4          D ffff88020f3e83c0     0   513      2 0x00000000
[250865.019183] zvol/5          D ffff8802107b9ab0     0   514      2 0x00000000
[250865.019183] zvol/6          D ffff8802107bdf80     0   515      2 0x00000000
[250865.019183] zvol/7          D ffff8802107b83c0     0   516      2 0x00000000
[250865.019183] zvol/8          D ffff8802107bb1a0     0   517      2 0x00000000
[250865.019183] zvol/9          D ffff8802107bc890     0   518      2 0x00000000
[250865.019183] zvol/10         D ffff8802103f03c0     0   519      2 0x00000000
[250865.019183] zvol/11         D ffff8802103f31a0     0   520      2 0x00000000
[250865.019183] txg_quiesce     D ffff880209ab03c0     0   893      2 0x00000000
[250865.019183] sshd            D ffff880201484890     0  3869   2665 0x00000000
[250865.019183] nbd-server      D ffff8801e7699ab0     0  6562   6561 0x00000000
[250865.019183] flush-230:1664  D ffff880209ab1ab0     0  8208      2 0x00000000
[250867.550421] rsync           D ffff8802102edf80     0  8950   2696 0x00000004
</pre>

I could be wrong on this but dbuf_do_evict releases the db_mtx mutex before calling dbuf_destroy. And because the ref count is no longer protected we could get past the ASSERT in debuf_destroy by some other thread getting a copy of db. Everybody else seems to hold the mutex while destroying this object... 

Yes, I see what your saying but holding the db->mtx over the dbuf_destroy() call would also be dangerous.  Once dbuf_destroy() returns that memory might to longer be addressable.  There are also all the assertions which make it pretty clear that at this point no references should be held.  Of course something is wrong and this code does feel racy, just look at the comment over dbuf_clear().  Anyway, this is going to take some more thought to sort out.

<pre>
/*
 * "Clear" the contents of this dbuf.  This will mark the dbuf
 * EVICTING and clear *most* of its references.  Unfortunetely,
 * when we are not holding the dn_dbufs_mtx, we can't clear the
 * entry in the dn_dbufs list.  We have to wait until dbuf_destroy()
 * in this case.  For callers from the DMU we will usually see:
 *      dbuf_clear()->arc_buf_evict()->dbuf_do_evict()->dbuf_destroy()
 * For the arc callback, we will usually see:
 *      dbuf_do_evict()->dbuf_clear();dbuf_destroy()
 * Sometimes, though, we will get a mix of these two:
 *      DMU: dbuf_clear()->arc_buf_evict()
 *      ARC: dbuf_do_evict()->dbuf_destroy()
 */
</pre>

Closing this issue since it's 3 years stale and there has been significant change in this area.
