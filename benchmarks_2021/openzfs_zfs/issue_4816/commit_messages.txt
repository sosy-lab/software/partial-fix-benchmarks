Merge branch 'illumos-2605'

Adds support for resuming interrupted zfs send streams and include
all related send/recv bug fixes from upstream OpenZFS.

Unlike the upstream implementation this branch does not change
the existing ioctl interface.  Instead a new ZFS_IOC_RECV_NEW ioctl
was added to support resuming zfs send streams.  This was done by
applying the original upstream patch and then reverting the ioctl
changes in a follow up patch.  For this reason there are a handful
on commits between the relevant patches on this branch which are
not interoperable.  This was done to make it easier to extract
the new ZFS_IOC_RECV_NEW and submit it upstream.

Signed-off-by: Brian Behlendorf <behlendorf1@llnl.gov>
Closes #4742
Allow zfs_purgedir() to skip inodes undergoing eviction

When destroying a file which contains xattrs the xattr directory inode
and its child inodes may be acquired with zfs_zget().  This can result
in a deadlock if these inodes are part of the same disposal list.  This
is only possible in zfs_purgedir() because it is called from evict()
while processing this disposal list.

Prevent this deadlock by allowing zfs_zget() to fail in zfs_purgedir().
The object will be left in the unlinked set and processing of it will
be deferred via the existing mechanisms.

Signed-off-by: Brian Behlendorf <behlendorf1@llnl.gov>
Issue #4816
Allow zfs_purgedir() to skip inodes undergoing eviction

When destroying a file which contains xattrs the xattr directory inode
and its child inodes may be acquired with zfs_zget().  This can result
in a deadlock if these inodes are part of the same disposal list.  This
is only possible in zfs_purgedir() because it is called from evict()
while processing this disposal list.

Prevent this deadlock by allowing zfs_zget() to fail in zfs_purgedir().
The object will be left in the unlinked set and processing of it will
be deferred via the existing mechanisms.

Signed-off-by: Brian Behlendorf <behlendorf1@llnl.gov>
Issue #4816
