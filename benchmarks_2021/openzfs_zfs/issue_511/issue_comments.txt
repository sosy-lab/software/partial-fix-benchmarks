FHS conformance for the zpool.cache and intermediary build products
Moving the cache file to `/var/lib/zfs/zpool.cache` seems reasonable to be consistent with the FHS.  However, I don't think we should be moving the spl/zfs kernel headers since I'd like to keep them with the normal kernel headers under /usr/src/

The SPL build products are not normal kernel headers according to system policy or my understanding of the FHS, but `spl_config.h` and `Module.symvers` are the only things that are generated per-build.  The rest is a just a copy of the `include/` folder.

An easy way to satisfy the FHS would be to:
- Add `/var/lib/spl/${kver}/` to the list of places that are checked by the `ZFS_AC_SPL_MODULE_SYMVERS` macro.
- Use `/usr/src/spl-${mver}/include` directly and don't install a second copy.

Rather, most of the FHS violation is resolved by not installing redundant copies of the headers.

Plus, you've already written most of the autotools code to accommodate the change.  This works:

```
# rm -rf /usr/src/spl-0.6.0.44/$(uname -r)/
# cd zfs
# ./configure --with-spl=/usr/src/spl-0.6.0.44 --with-spl-obj=/var/lib/dkms/spl/0.6.0.44/build
# make
```

Reusing the `/var/lib/dkms` area seems like a good idea too.

True, the `spl_config.h` and `Module.symvers` are build products and can be located safely where it makes the most sense.  I just happened to locate them with the SPL headers because on my RHEL6 box the kernel packages do the same thing for `/usr/src/kernels/2.6.32-220.el6.x86_64/System.map` and `/usr/src/kernels/2.6.32-220.el6.x86_64/Module.symvers` which are also build products.  How about this.
- Extend `ZFS_AC_SPL_MODULE_SYMVERS` to include the additional search path.
- Add two new configure options which can be used to control the install location of the kernel build products and headers, similar to what was done with udevdir/udevruledir.  Right now `/usr/src/` is just hard coded in the build system and that's just wrong.  
  - \* --with-ksrcdir = /usr/src
  - \* --with-kobjdir = /var/lib/dkms

(Better configure option names welcome)

This way the Debian packaging can be cleanly updated to install things where it is best for dkms.  We'll certainly want to do something similar with the rpm packaging once it supports dkms as well.  The exact paths may differ between distros so it's best to leave this as flexible as possible.

Now as for unifying the /usr/include/zfs and /usr/src/zfs headers that's going to take a bit more work.  But your right it should absolutely be done.  Ideally we can just end up with a single unified /usr/include tree which is used by both the kernel and user builds.

I think that I mooted most of this issue in the DKMS configuration.  If DKMS support is added to the RedHat packaging, then the solution could just be copied from the Debian packaging.

The implicit FHS rule is that `/usr` must be read-only between invocations of the package manager.  The vanilla RPMs and alien DEBs conform because they depend on a particular kernel package and they don't automatically change anything between system upgrades.

Moving the `zpool.cache` should still happen, however.

It looks like automake is noticing the libexecdir FHS violation.

Marcin Mirosław writes at
http://groups.google.com/a/zfsonlinux.org/group/zfs-discuss/browse_thread/thread/5231e2f92ba43ff7/d323e85472992c62#d323e85472992c62

```
Hello!
With automake-1.11.1 everything is ok. With automake-1.11.2 i'm getting
such error:
$ automake --add-missing --copy --foreign
scripts/Makefile.am:4: `pkglibexecdir' is not a legitimate directory for
`SCRIPTS'
scripts/zpios-profile/Makefile.am:2: `pkglibexecdir' is not a legitimate
directory for `SCRIPTS'
scripts/zpios-test/Makefile.am:2: `pkglibexecdir' is not a legitimate
directory for `SCRIPTS'
scripts/zpool-config/Makefile.am:2: `pkglibexecdir' is not a legitimate
directory for `SCRIPTS'
scripts/zpool-layout/Makefile.am:2: `pkglibexecdir' is not a legitimate
directory for `SCRIPTS'

Thanks for help.
```

It might make more sense to use /boot/zfs/zpool.cache, which is what FreeBSD does.
