Pool imported on both nodes despite using multihost property
@gusioo thanks for reporting this.  That is not expected, even with the cache file the pool should not be importable.  Let me pull in @ofaaland and we'll see about reproducing this and determining the root cause.
I wonder if disabling the cache file (-o cachefile=none) at the pool creation and import could be a temporary workaround ?
Yes, disabling the cachefile would avoid any potential issue.
@gusioo PR #6971 opened with a fix, we'll see about getting it in the next 0.7 release.
i have the same problem - i have multihost=on, and a day later after starting both nodes freshly - i wonder why the pool is imported/mounted on both nodes!!

on the second node (which was booted later on),  i see this warning

[  296.360648] WARNING: MMP writes to pool 'hostmirrorpool' have not succeeded in over 5s; suspending pool 
[  296.360654] WARNING: Pool 'hostmirrorpool' has encountered an uncorrectable I/O failure and has been suspended.

why does that pool get imported and mounted at all, when i even cannot do that manually ? have there been changes between 0.7.12 and 0.8rc?  i'm sure, after exporting on node1 i needed "-f" before i could import it on node2, so i'm curious why (with 0.8rc) it now gets imported automatically


@devZer0 please describe your installation - is it 1 JBOD with 2 servers connected via HBA cards? `multihost=on` is sensitive for RW delays.
@kpande @devZer0 thank for determining the failure scenario.  This was a case which was considered and the intent was to prevent a pool which was using MMP and was suspended from being resumable.  But I don't see an explicit check for that in the code, we'll get one added with some other MMP improvement @ofaaland has been putting together.
i'm not sure if there is another "chance" for accidental parallel import - but i had run concurrent export/import in a loop on two different nodes accessing a shared zpool and i had seen at least 2 occurences that the pool was active on both even without being suspended intermittedly. i managed to competely corrupt it that way. i guess there is chance for "race condition".
@devZer0 I just created a PR to prevent pool from being resumed when multihost is enabled, and to update the manpage to inform users that is disallowed beccause it would be unsafe.  #8460 