diff --git a/common.h b/common.h
index bb7b6ea7e..3a0042263 100644
--- a/common.h
+++ b/common.h
@@ -102,7 +102,8 @@ typedef enum {
 
 /* SCAN options */
 #define REDIS_SCAN_NORETRY 0
-#define REDIS_SCAN_RETRY 1
+#define REDIS_SCAN_RETRY   1
+#define REDIS_SCAN_PREFIX  2
 
 /* GETBIT/SETBIT offset range limits */
 #define BITOP_MIN_OFFSET 0
diff --git a/redis.c b/redis.c
index ff0bd0298..244f6f6ae 100644
--- a/redis.c
+++ b/redis.c
@@ -723,6 +723,7 @@ static void add_class_constants(zend_class_entry *ce, int is_cluster) {
     zend_declare_class_constant_long(ce, ZEND_STRL("OPT_SCAN"), REDIS_OPT_SCAN);
     zend_declare_class_constant_long(ce, ZEND_STRL("SCAN_RETRY"), REDIS_SCAN_RETRY);
     zend_declare_class_constant_long(ce, ZEND_STRL("SCAN_NORETRY"), REDIS_SCAN_NORETRY);
+    zend_declare_class_constant_long(ce, ZEND_STRL("SCAN_PREFIX"), REDIS_SCAN_PREFIX);
 
     /* Cluster option to allow for slave failover */
     if (is_cluster) {
@@ -3470,7 +3471,7 @@ generic_scan_cmd(INTERNAL_FUNCTION_PARAMETERS, REDIS_SCAN_TYPE type) {
     RedisSock *redis_sock;
     HashTable *hash;
     char *pattern = NULL, *cmd, *key = NULL;
-    int cmd_len, num_elements, key_free = 0;
+    int cmd_len, num_elements, key_free = 0, pattern_free = 0;
     size_t key_len = 0, pattern_len = 0;
     zend_string *match_type = NULL;
     zend_long count = 0, iter;
@@ -3528,6 +3529,10 @@ generic_scan_cmd(INTERNAL_FUNCTION_PARAMETERS, REDIS_SCAN_TYPE type) {
         key_free = redis_key_prefix(redis_sock, &key, &key_len);
     }
 
+    if (redis_sock->scan & REDIS_SCAN_PREFIX) {
+        pattern_free = redis_key_prefix(redis_sock, &pattern, &pattern_len);
+    }
+
     /**
      * Redis can return to us empty keys, especially in the case where there
      * are a large number of keys to scan, and we're matching against a
@@ -3560,9 +3565,12 @@ generic_scan_cmd(INTERNAL_FUNCTION_PARAMETERS, REDIS_SCAN_TYPE type) {
         /* Get the number of elements */
         hash = Z_ARRVAL_P(return_value);
         num_elements = zend_hash_num_elements(hash);
-    } while(redis_sock->scan == REDIS_SCAN_RETRY && iter != 0 &&
+    } while (redis_sock->scan & REDIS_SCAN_RETRY && iter != 0 &&
             num_elements == 0);
 
+    /* Free our pattern if it was prefixed */
+    if (pattern_free) efree(pattern);
+
     /* Free our key if it was prefixed */
     if(key_free) efree(key);
 
diff --git a/redis_cluster.c b/redis_cluster.c
index ee218ba05..55c2085ee 100644
--- a/redis_cluster.c
+++ b/redis_cluster.c
@@ -2411,7 +2411,7 @@ static void cluster_kscan_cmd(INTERNAL_FUNCTION_PARAMETERS,
 {
     redisCluster *c = GET_CONTEXT();
     char *cmd, *pat = NULL, *key = NULL;
-    size_t key_len = 0, pat_len = 0;
+    size_t key_len = 0, pat_len = 0, pat_free = 0;
     int cmd_len, key_free = 0;
     short slot;
     zval *z_it;
@@ -2450,6 +2450,10 @@ static void cluster_kscan_cmd(INTERNAL_FUNCTION_PARAMETERS,
     key_free = redis_key_prefix(c->flags, &key, &key_len);
     slot = cluster_hash_key(key, key_len);
 
+    if (c->flags->scan & REDIS_SCAN_PREFIX) {
+        pat_free = redis_key_prefix(c->flags, &pat, &pat_len);
+    }
+
     // If SCAN_RETRY is set, loop until we get a zero iterator or until
     // we get non-zero elements.  Otherwise we just send the command once.
     do {
@@ -2488,7 +2492,10 @@ static void cluster_kscan_cmd(INTERNAL_FUNCTION_PARAMETERS,
 
         // Free our command
         efree(cmd);
-    } while (c->flags->scan == REDIS_SCAN_RETRY && it != 0 && num_ele == 0);
+    } while (c->flags->scan & REDIS_SCAN_RETRY && it != 0 && num_ele == 0);
+
+    // Free our pattern
+    if (pat_free) efree(pat);
 
     // Free our key
     if (key_free) efree(key);
@@ -2505,7 +2512,7 @@ PHP_METHOD(RedisCluster, scan) {
     int cmd_len;
     short slot;
     zval *z_it, *z_node;
-    long it, num_ele;
+    long it, num_ele, pat_free = 0;
     zend_long count = 0;
 
     /* Treat as read-only */
@@ -2534,6 +2541,10 @@ PHP_METHOD(RedisCluster, scan) {
         RETURN_FALSE;
     }
 
+    if (c->flags->scan & REDIS_SCAN_PREFIX) {
+        pat_free = redis_key_prefix(c->flags, &pat, &pat_len);
+    }
+
     /* With SCAN_RETRY on, loop until we get some keys, otherwise just return
      * what Redis does, as it does */
     do {
@@ -2570,7 +2581,9 @@ PHP_METHOD(RedisCluster, scan) {
         efree(cmd);
 
         num_ele = zend_hash_num_elements(Z_ARRVAL_P(return_value));
-    } while (c->flags->scan == REDIS_SCAN_RETRY && it != 0 && num_ele == 0);
+    } while (c->flags->scan & REDIS_SCAN_RETRY && it != 0 && num_ele == 0);
+
+    if (pat_free) efree(pat);
 
     Z_LVAL_P(z_it) = it;
 }
diff --git a/redis_commands.c b/redis_commands.c
index 6945ed4b6..dd1b7c9a7 100644
--- a/redis_commands.c
+++ b/redis_commands.c
@@ -4032,11 +4032,14 @@ void redis_setoption_handler(INTERNAL_FUNCTION_PARAMETERS,
             RETURN_TRUE;
         case REDIS_OPT_SCAN:
             val_long = zval_get_long(val);
-            if (val_long==REDIS_SCAN_NORETRY || val_long==REDIS_SCAN_RETRY) {
-                redis_sock->scan = val_long;
-                RETURN_TRUE;
+            if (val_long == REDIS_SCAN_NORETRY) {
+                redis_sock->scan &= ~REDIS_SCAN_RETRY;
+            } else if (val_long == REDIS_SCAN_RETRY || val_long == REDIS_SCAN_PREFIX) {
+                redis_sock->scan |= val_long;
+            } else {
+                break;
             }
-            break;
+            RETURN_TRUE;
         case REDIS_OPT_FAILOVER:
             val_long = zval_get_long(val);
             if (val_long == REDIS_FAILOVER_NONE ||
