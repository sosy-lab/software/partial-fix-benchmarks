pipeline shell (psql) default port doesn't works when postgresql is installed
The problem here is that `pipeline` symlinks to `/usr/bin/psql` upon installation. I think the correct thing to do here is just to link `/usr/bin/pipeline` to `<install directory>/bin/psql` instead of the system-wide binary.

hummm nope. not in my case. i used --prefix to install it to a development directory and it used the pipeline version of psql : 

> ker2x@lily:~/build/bin$ pipeline --help
> pipeline is the PipelineDB interactive terminal.
> 
> Usage:
> [...]
> Connection options:
>  -h, --host=HOSTNAME      database server host or socket directory (default: "local socket")
>  -p, --port=PORT          database server port (default: "6543")
>  -U, --username=USERNAME  database user name (default: "ker2x")
>  -w, --no-password        never prompt for password
>  -W, --password           force password prompt (should happen automatically)
> 
> For more information, type "\?" (for internal commands) or "\help" (for SQL
> commands) from within psql, or consult the psql section in the PostgreSQL
> documentation.
> 
> Report bugs to eng@pipelinedb.com.

You're definitely right @ker2x. And we'd rather maintain seamless compatibility so we don't want to have our own special clients. I think the best thing to do here is to just use PostgreSQL's defaults. We originally chose `6543` specifically to not conflict with an existing installation, but this is a conflict anyways :)

It's probably more likely that a user will have a system-wide `libpq` installed than it is that they'll have an existing PostgreSQL server running on the same hardware as PipelineDB. If that were the case, I believe any port conflicts that would cause would be more obvious to that user anyways.

So adding to @ker2x 's suggestions, I see three options:

1) Use all PostgreSQL defaults (ok)
2) Use our own client with its own environment vars (bad)
3) Keep our own defaults and force clients to explicitly override them (ok):

```
psql -p 6543 ...
```

@usmanm do you have any thoughts on this?

Well, if you really want to keep 6543 and make it behave as expected (6543 as default, as said in the --help) you can easily add :

>    if (!options->port)
>        options->port = DEF_PGPORT_STR;

at the end of : 

> static void
> parse_psql_options(int argc, char *argv[], struct adhoc_opts \* options)

to override libpq default.

I'm going to side with 1 (2 is a no go, and 3 are not ideal either). But does changing defaults also mean that we change the default database name etc? I think I like @ker2x's idea of just overriding `libpq` in the code.

Yep, I like that approach too.

**yay! \o/**

Forget about my patch, it sux. I don't see how it can be done without patching libpq.

We're just going to use standard defaults.
