diff --git a/src/backend/catalog/pipeline_query.c b/src/backend/catalog/pipeline_query.c
index d423f87f1..09a9fc867 100644
--- a/src/backend/catalog/pipeline_query.c
+++ b/src/backend/catalog/pipeline_query.c
@@ -558,6 +558,24 @@ GetContinuousView(Oid id)
 	return view;
 }
 
+/*
+ * RangeVarGetContinuousView
+ */
+ContinuousView *
+RangeVarGetContinuousView(RangeVar *cv_name)
+{
+	HeapTuple pq = GetPipelineQueryTuple(cv_name);
+	Oid id;
+
+	if (!HeapTupleIsValid(pq))
+		return NULL;
+
+	id = ((Form_pipeline_query) GETSTRUCT(pq))->id;
+	ReleaseSysCache(pq);
+
+	return GetContinuousView(id);
+}
+
 /*
  * GetAllContinuousViewIds
  */
diff --git a/src/backend/pipeline/cont_plan.c b/src/backend/pipeline/cont_plan.c
index 56fd5f38d..139b3c094 100644
--- a/src/backend/pipeline/cont_plan.c
+++ b/src/backend/pipeline/cont_plan.c
@@ -40,6 +40,8 @@
 #include "utils/snapmgr.h"
 #include "utils/syscache.h"
 
+ProcessUtility_hook_type SaveUtilityHook = NULL;
+
 /*
  * get_combiner_join_rel
  *
@@ -121,14 +123,14 @@ get_worker_select_stmt(ContinuousView* view, SelectStmt** viewptr)
 	return selectstmt;
 }
 
-static PlannedStmt*
+static PlannedStmt *
 get_worker_plan(ContinuousView *view)
 {
 	SelectStmt* stmt = get_worker_select_stmt(view, NULL);
 	return get_plan_from_stmt(view->id, (Node *) stmt, view->query, false);
 }
 
-static PlannedStmt*
+static PlannedStmt *
 get_plan_with_hook(Oid id, Node *node, const char* sql, bool is_combine)
 {
 	PlannedStmt *result;
@@ -155,7 +157,7 @@ get_plan_with_hook(Oid id, Node *node, const char* sql, bool is_combine)
 	return result;
 }
 
-PlannedStmt*
+PlannedStmt *
 GetContinuousViewOverlayPlan(ContinuousView *view)
 {
 	SelectStmt	*selectstmt;
@@ -168,7 +170,7 @@ GetContinuousViewOverlayPlan(ContinuousView *view)
 							  view->query, false);
 }
 
-PlannedStmt*
+PlannedStmt *
 GetContinuousViewOverlayPlanMod(ContinuousView *view, ViewModFunction mod_fn)
 {
 	SelectStmt	*selectstmt;
@@ -183,7 +185,7 @@ GetContinuousViewOverlayPlanMod(ContinuousView *view, ViewModFunction mod_fn)
 							  view->query, false);
 }
 
-static PlannedStmt*
+static PlannedStmt *
 get_combiner_plan(ContinuousView *view)
 {
 	List		*parsetree_list;
@@ -323,3 +325,93 @@ UnsetEStateSnapshot(EState *estate)
 	PopActiveSnapshot();
 	estate->es_snapshot = NULL;
 }
+
+/*
+ * get_res_target
+ */
+static ResTarget *
+get_res_target(char *name, List *tlist)
+{
+	ListCell *lc;
+
+	Assert(name);
+
+	foreach(lc, tlist)
+	{
+		ResTarget *rt = (ResTarget *) lfirst(lc);
+		if (pg_strcasecmp(rt->name, name) == 0)
+			return rt;
+	}
+
+	elog(ERROR, "column \"%s\" does not exist", name);
+
+	return NULL;
+}
+
+/*
+ * create_index_on_matrel
+ *
+ * Given a CREATE INDEX query on a continuous view, modify it to
+ * create the index on the continuous view's matrel instead.
+ */
+static void
+create_index_on_matrel(IndexStmt *stmt)
+{
+	ContinuousView *cv;
+	RangeVar *cv_name = stmt->relation;
+	SelectStmt	*viewstmt;
+	List *tlist;
+	ListCell *lc;
+	bool is_sw = GetGCFlag(cv_name);
+
+	stmt->relation = GetMatRelationName(cv_name);
+	cv = RangeVarGetContinuousView(cv_name);
+	get_worker_select_stmt(cv, &viewstmt);
+
+	Assert(viewstmt);
+	tlist = viewstmt->targetList;
+
+	foreach(lc, stmt->indexParams)
+	{
+		IndexElem *elem = (IndexElem *) lfirst(lc);
+		ResTarget *res;
+
+		if (!elem->name)
+			continue;
+
+		/*
+		 * If the column isn't a plain column reference, then it's wrapped in
+		 * a finalize call so we need to index on that with an expression index
+		 */
+		res = get_res_target(elem->name, tlist);
+		if (!IsA(res->val, ColumnRef))
+		{
+			if (is_sw)
+				elog(ERROR, "sliding-window aggregate columns cannot be indexed");
+			elem->expr = copyObject(res->val);
+			elem->name = NULL;
+		}
+	}
+}
+
+/*
+ * ProcessUtilityOnContView
+ *
+ * Hook to intercept relevant utility queries run on continuous views
+ */
+void
+ProcessUtilityOnContView(Node *parsetree, const char *sql, ProcessUtilityContext context,
+													  ParamListInfo params, DestReceiver *dest, char *tag)
+{
+	if (IsA(parsetree, IndexStmt))
+	{
+		IndexStmt *stmt = (IndexStmt *) parsetree;
+		if (IsAContinuousView(stmt->relation))
+			create_index_on_matrel(stmt);
+	}
+
+	if (SaveUtilityHook != NULL)
+		(*SaveUtilityHook) (parsetree, sql, context, params, dest, tag);
+	else
+		standard_ProcessUtility(parsetree, sql, context, params, dest, tag);
+}
diff --git a/src/backend/utils/init/pipelineinit.c b/src/backend/utils/init/pipelineinit.c
index 42a6feae4..d5de275a8 100644
--- a/src/backend/utils/init/pipelineinit.c
+++ b/src/backend/utils/init/pipelineinit.c
@@ -15,18 +15,31 @@
 #include "postgres.h"
 
 #include "miscadmin.h"
+#include "pipeline/cont_plan.h"
 #include "pipeline/cont_scheduler.h"
 #include "storage/shm_alloc.h"
+#include "tcop/utility.h"
 
 /*
  * InitPipeline
  *
  * This is called whenever a new backend is starting up.
  */
-void PipelineShmemInit()
+void
+PipelineShmemInit()
 {
 	srand(time(NULL) ^ MyProcPid);
 
 	ShmemDynAllocShmemInit();
 	ContQuerySchedulerShmemInit();
 }
+
+/*
+ * PipelineInstallHooks
+ */
+void
+PipelineInstallHooks()
+{
+	SaveUtilityHook = ProcessUtility_hook;
+	ProcessUtility_hook = ProcessUtilityOnContView;
+}
diff --git a/src/backend/utils/init/postinit.c b/src/backend/utils/init/postinit.c
index 93fbd74bd..c9d2246bd 100644
--- a/src/backend/utils/init/postinit.c
+++ b/src/backend/utils/init/postinit.c
@@ -971,7 +971,10 @@ InitPostgres(const char *in_dbname, Oid dboid, const char *username,
 
 	/* initialize all PipelineDB stuff */
 	if (!bootstrap)
+	{
 		PipelineShmemInit();
+		PipelineInstallHooks();
+	}
 
 	/* close the transaction we started above */
 	if (!bootstrap)
diff --git a/src/include/catalog/pipeline_query_fn.h b/src/include/catalog/pipeline_query_fn.h
index 66c00658a..e29a72972 100644
--- a/src/include/catalog/pipeline_query_fn.h
+++ b/src/include/catalog/pipeline_query_fn.h
@@ -45,6 +45,7 @@ extern RangeVar *GetCVNameFromMatRelName(RangeVar *matrel);
 extern void RemoveContinuousViewById(Oid oid);
 
 extern ContinuousView *GetContinuousView(Oid id);
+extern ContinuousView *RangeVarGetContinuousView(RangeVar *cv_name);
 extern Bitmapset *GetAllContinuousViewIds(void);
 extern Bitmapset *GetAdhocContinuousViewIds(void);
 
diff --git a/src/include/miscadmin.h b/src/include/miscadmin.h
index 92788fa99..6d96dffe8 100644
--- a/src/include/miscadmin.h
+++ b/src/include/miscadmin.h
@@ -408,6 +408,7 @@ extern void BaseInit(void);
 
 /* in utils/pipelineinit.c */
 extern void PipelineShmemInit(void);
+extern void PipelineInstallHooks(void);
 
 /* in utils/init/miscinit.c */
 extern bool IgnoreSystemIndexes;
diff --git a/src/include/pipeline/cont_plan.h b/src/include/pipeline/cont_plan.h
index dbf4a9399..76784bbbb 100644
--- a/src/include/pipeline/cont_plan.h
+++ b/src/include/pipeline/cont_plan.h
@@ -18,11 +18,14 @@
 #include "nodes/plannodes.h"
 #include "nodes/relation.h"
 #include "pipeline/cont_scheduler.h"
+#include "tcop/utility.h"
 #include "utils/rel.h"
 #include "utils/relcache.h"
 #include "utils/resowner.h"
 #include "utils/tuplestore.h"
 
+extern ProcessUtility_hook_type SaveUtilityHook;
+
 #define IS_STREAM_RTE(relid, root) ((planner_rt_fetch(relid, root)) && \
 	((planner_rt_fetch(relid, root))->relkind == RELKIND_STREAM))
 
@@ -44,4 +47,7 @@ extern EState *CreateEState(QueryDesc *query_desc);
 extern void SetEStateSnapshot(EState *estate);
 extern void UnsetEStateSnapshot(EState *estate);
 
+extern void ProcessUtilityOnContView(Node *parsetree, const char *sql, ProcessUtilityContext context,
+													  ParamListInfo params, DestReceiver *dest, char *tag);
+
 #endif
diff --git a/src/test/regress/expected/cont_index.out b/src/test/regress/expected/cont_index.out
new file mode 100644
index 000000000..398926090
--- /dev/null
+++ b/src/test/regress/expected/cont_index.out
@@ -0,0 +1,123 @@
+CREATE CONTINUOUS VIEW test_cont_index0 AS SELECT x::integer, COUNT(*), AVG(x) FROM stream GROUP BY x;
+CREATE INDEX test_ci_idx0 ON test_cont_index0 (x);
+\d+ test_cont_index0_mrel
+                 Table "public.test_cont_index0_mrel"
+ Column |   Type   | Modifiers | Storage  | Stats target | Description 
+--------+----------+-----------+----------+--------------+-------------
+ x      | integer  |           | plain    |              | 
+ count  | bigint   |           | plain    |              | 
+ avg    | bigint[] |           | extended |              | 
+ $pk    | bigint   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index0_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_cont_index0_mrel_expr_idx" btree (hash_group(x))
+Options: fillfactor=50
+
+CREATE INDEX test_ci_idx1 ON test_cont_index0 (avg);
+\d+ test_cont_index0_mrel
+                 Table "public.test_cont_index0_mrel"
+ Column |   Type   | Modifiers | Storage  | Stats target | Description 
+--------+----------+-----------+----------+--------------+-------------
+ x      | integer  |           | plain    |              | 
+ count  | bigint   |           | plain    |              | 
+ avg    | bigint[] |           | extended |              | 
+ $pk    | bigint   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index0_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_ci_idx1" btree (int8_avg(avg))
+    "test_cont_index0_mrel_expr_idx" btree (hash_group(x))
+Options: fillfactor=50
+
+CREATE INDEX test_ci_idx2 ON test_cont_index0 (x, avg);
+\d+ test_cont_index0_mrel
+                 Table "public.test_cont_index0_mrel"
+ Column |   Type   | Modifiers | Storage  | Stats target | Description 
+--------+----------+-----------+----------+--------------+-------------
+ x      | integer  |           | plain    |              | 
+ count  | bigint   |           | plain    |              | 
+ avg    | bigint[] |           | extended |              | 
+ $pk    | bigint   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index0_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_ci_idx1" btree (int8_avg(avg))
+    "test_ci_idx2" btree (x, int8_avg(avg))
+    "test_cont_index0_mrel_expr_idx" btree (hash_group(x))
+Options: fillfactor=50
+
+DROP CONTINUOUS VIEW test_cont_index0;
+CREATE CONTINUOUS VIEW test_cont_index1 WITH (max_age = '1 hour') AS SELECT x::integer, y::integer, COUNT(*), AVG(x) FROM stream GROUP BY x, y;
+CREATE INDEX test_ci_idx0 ON test_cont_index1 (x);
+\d+ test_cont_index1_mrel
+                               Table "public.test_cont_index1_mrel"
+      Column       |           Type           | Modifiers | Storage  | Stats target | Description 
+-------------------+--------------------------+-----------+----------+--------------+-------------
+ arrival_timestamp | timestamp with time zone |           | plain    |              | 
+ x                 | integer                  |           | plain    |              | 
+ y                 | integer                  |           | plain    |              | 
+ count             | bigint                   |           | plain    |              | 
+ avg               | bigint[]                 |           | extended |              | 
+ $pk               | bigint                   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index1_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_cont_index1_mrel_expr_idx" btree (ls_hash_group(arrival_timestamp, x, y))
+Options: fillfactor=50
+
+CREATE INDEX test_ci_idx1 ON test_cont_index1 (avg);
+ERROR:  sliding-window aggregate columns cannot be indexed
+\d+ test_cont_index1_mrel
+                               Table "public.test_cont_index1_mrel"
+      Column       |           Type           | Modifiers | Storage  | Stats target | Description 
+-------------------+--------------------------+-----------+----------+--------------+-------------
+ arrival_timestamp | timestamp with time zone |           | plain    |              | 
+ x                 | integer                  |           | plain    |              | 
+ y                 | integer                  |           | plain    |              | 
+ count             | bigint                   |           | plain    |              | 
+ avg               | bigint[]                 |           | extended |              | 
+ $pk               | bigint                   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index1_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_cont_index1_mrel_expr_idx" btree (ls_hash_group(arrival_timestamp, x, y))
+Options: fillfactor=50
+
+CREATE INDEX test_ci_idx2 ON test_cont_index1 (x, avg);
+ERROR:  sliding-window aggregate columns cannot be indexed
+\d+ test_cont_index1_mrel
+                               Table "public.test_cont_index1_mrel"
+      Column       |           Type           | Modifiers | Storage  | Stats target | Description 
+-------------------+--------------------------+-----------+----------+--------------+-------------
+ arrival_timestamp | timestamp with time zone |           | plain    |              | 
+ x                 | integer                  |           | plain    |              | 
+ y                 | integer                  |           | plain    |              | 
+ count             | bigint                   |           | plain    |              | 
+ avg               | bigint[]                 |           | extended |              | 
+ $pk               | bigint                   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index1_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_cont_index1_mrel_expr_idx" btree (ls_hash_group(arrival_timestamp, x, y))
+Options: fillfactor=50
+
+CREATE INDEX test_ci_idx3 ON test_cont_index1 (x, y);
+\d+ test_cont_index1_mrel
+                               Table "public.test_cont_index1_mrel"
+      Column       |           Type           | Modifiers | Storage  | Stats target | Description 
+-------------------+--------------------------+-----------+----------+--------------+-------------
+ arrival_timestamp | timestamp with time zone |           | plain    |              | 
+ x                 | integer                  |           | plain    |              | 
+ y                 | integer                  |           | plain    |              | 
+ count             | bigint                   |           | plain    |              | 
+ avg               | bigint[]                 |           | extended |              | 
+ $pk               | bigint                   | not null  | plain    |              | 
+Indexes:
+    "test_cont_index1_mrel_pkey" PRIMARY KEY, btree ("$pk")
+    "test_ci_idx0" btree (x)
+    "test_ci_idx3" btree (x, y)
+    "test_cont_index1_mrel_expr_idx" btree (ls_hash_group(arrival_timestamp, x, y))
+Options: fillfactor=50
+
+DROP CONTINUOUS VIEW test_cont_index1;
diff --git a/src/test/regress/parallel_schedule b/src/test/regress/parallel_schedule
index a2d63ad18..a684cd852 100644
--- a/src/test/regress/parallel_schedule
+++ b/src/test/regress/parallel_schedule
@@ -172,7 +172,7 @@ test: cont_view_namespace cont_complex_types cont_explain cont_multiple_sw keyed
 # ----------
 # Another group of parallel tests
 # ----------
-test: create_combinable_agg pipeline_regress cont_fss_agg cont_matrel cont_set_agg
+test: create_combinable_agg pipeline_regress cont_fss_agg cont_matrel cont_set_agg cont_index
 
 # ----------
 # Another group of parallel tests
diff --git a/src/test/regress/sql/cont_index.sql b/src/test/regress/sql/cont_index.sql
new file mode 100644
index 000000000..5ea28e84f
--- /dev/null
+++ b/src/test/regress/sql/cont_index.sql
@@ -0,0 +1,28 @@
+CREATE CONTINUOUS VIEW test_cont_index0 AS SELECT x::integer, COUNT(*), AVG(x) FROM stream GROUP BY x;
+
+CREATE INDEX test_ci_idx0 ON test_cont_index0 (x);
+\d+ test_cont_index0_mrel
+
+CREATE INDEX test_ci_idx1 ON test_cont_index0 (avg);
+\d+ test_cont_index0_mrel
+
+CREATE INDEX test_ci_idx2 ON test_cont_index0 (x, avg);
+\d+ test_cont_index0_mrel
+
+DROP CONTINUOUS VIEW test_cont_index0;
+
+CREATE CONTINUOUS VIEW test_cont_index1 WITH (max_age = '1 hour') AS SELECT x::integer, y::integer, COUNT(*), AVG(x) FROM stream GROUP BY x, y;
+
+CREATE INDEX test_ci_idx0 ON test_cont_index1 (x);
+\d+ test_cont_index1_mrel
+
+CREATE INDEX test_ci_idx1 ON test_cont_index1 (avg);
+\d+ test_cont_index1_mrel
+
+CREATE INDEX test_ci_idx2 ON test_cont_index1 (x, avg);
+\d+ test_cont_index1_mrel
+
+CREATE INDEX test_ci_idx3 ON test_cont_index1 (x, y);
+\d+ test_cont_index1_mrel
+
+DROP CONTINUOUS VIEW test_cont_index1;
