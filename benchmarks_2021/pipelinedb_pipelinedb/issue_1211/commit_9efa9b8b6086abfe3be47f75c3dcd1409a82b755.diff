diff --git a/src/backend/pipeline/cont_analyze.c b/src/backend/pipeline/cont_analyze.c
index cf3b43af7..c43cc8a05 100644
--- a/src/backend/pipeline/cont_analyze.c
+++ b/src/backend/pipeline/cont_analyze.c
@@ -1213,18 +1213,6 @@ ValidateContQuery(RangeVar *name, Node *node, const char *sql)
 					(errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
 					errmsg("continuous queries don't support DISTINCT expressions for \"%s\" aggregate", name),
 					parser_errposition(context->pstate, func->location)));
-
-		if (pg_strcasecmp(name, "fss_agg") == 0 && list_length(func->args))
-		{
-			Oid type = exprType(transformExpr(context->pstate, linitial(func->args), EXPR_KIND_WHERE));
-			TypeCacheEntry *typ = lookup_type_cache(type, 0);
-
-			if (!typ->typbyval)
-				ereport(ERROR,
-						(errcode(ERRCODE_FEATURE_NOT_SUPPORTED),
-						errmsg("fss_agg does not support reference types"),
-						parser_errposition(context->pstate, func->location)));
-		}
 	}
 
 	/* Ensure that any WINDOWs are legal */
diff --git a/src/backend/pipeline/fss.c b/src/backend/pipeline/fss.c
index 7d9dadb48..6e9de923d 100644
--- a/src/backend/pipeline/fss.c
+++ b/src/backend/pipeline/fss.c
@@ -16,6 +16,7 @@
 #include <math.h>
 #include "pipeline/fss.h"
 #include "pipeline/miscutils.h"
+#include "utils/datum.h"
 #include "utils/elog.h"
 #include "utils/palloc.h"
 
@@ -26,24 +27,37 @@
 #define MURMUR_SEED 0x02cd1b4c451c1fb8L
 
 FSS *
-FSSCreateWithMAndH(uint64_t k, TypeCacheEntry *typ, uint64_t m, uint64_t h)
+FSSFromBytes(struct varlena *bytes)
 {
-	Size sz = sizeof(FSS) + (sizeof(Counter) * h) + (sizeof(MonitoredElement) * m);
 	char *pos;
-	FSS *fss;
+	FSS *fss = (FSS *) bytes;
 
-	if (k > m)
-		elog(ERROR, "maximum value of k exceeded");
+	/* Fix pointers */
+	pos = (char *) fss;
+	pos += sizeof(FSS);
+	fss->bitmap_counter = (Counter *) pos;
+	pos += sizeof(Counter) * fss->h;
+	fss->monitored_elements = (MonitoredElement *) pos;
 
-	/* TODO(usmanm): Check bounds for k, m, h */
+	if (FSS_STORES_DATUMS(fss))
+	{
+		pos += sizeof(MonitoredElement) * fss->m;
+		fss->top_k = (ArrayType *) pos;
+	}
+	else
+		fss->top_k = NULL;
 
-	/* TODO(usmanm): Add support for ref types */
-	if (!typ->typbyval)
-		elog(ERROR, "fss does not support reference types");
+	return fss;
+}
+
+FSS *
+FSSCreateWithMAndH(uint16_t k, TypeCacheEntry *typ, uint16_t m, uint16_t h)
+{
+	Size sz = sizeof(FSS) + (sizeof(Counter) * h) + (sizeof(MonitoredElement) * m);
+	char *pos;
+	FSS *fss;
 
-	/* We only store datums if they're passed by value. */
-	if (typ->typbyval)
-		sz += sizeof(Datum) * k;
+	Assert (k <= m);
 
 	pos = palloc0(sz);
 
@@ -55,19 +69,25 @@ FSSCreateWithMAndH(uint64_t k, TypeCacheEntry *typ, uint64_t m, uint64_t h)
 
 	if (!typ->typbyval)
 	{
+		int i;
+
+		for (i = 0; i < m; i++)
+			fss->monitored_elements[i].varlen_index = (Datum ) i;
+
 		pos += sizeof(MonitoredElement) * m;
-		fss->top_k = (Datum *) pos;
+		fss->top_k = construct_empty_array(typ->type_id);
 	}
 	else
 		fss->top_k = NULL;
 
-	fss->packed = true;
 	fss->h = h;
 	fss->m = m;
 	fss->k = k;
 	fss->typ.typoid = typ->type_id;
 	fss->typ.typlen = typ->typlen;
 	fss->typ.typbyval = typ->typbyval;
+	fss->typ.typalign = typ->typalign;
+	fss->typ.typtype = typ->typtype;
 
 	SET_VARSIZE(fss, FSSSize(fss));
 
@@ -87,12 +107,63 @@ FSSDestroy(FSS *fss)
 	pfree(fss);
 }
 
+/*
+ * fss_resize_topk
+ *
+ * Resize the given FSS so that it can contiguously store the given top k array
+ */
+static FSS *
+fss_resize_topk(FSS *fss, ArrayType *top_k)
+{
+	Size delta = Abs((int) ARR_SIZE(top_k) - (int) ARR_SIZE(fss->top_k));
+	FSS *result = fss;
+
+	if (delta != 0)
+	{
+		result = repalloc(result, FSSSize(fss) + delta);
+		result = FSSFromBytes((struct varlena *) result);
+	}
+	memcpy(result->top_k, top_k, ARR_SIZE(top_k));
+	SET_VARSIZE(result, FSSSize(result));
+
+	return result;
+}
+
+/*
+ * get_monitored_value
+ *
+ * Get the monitored value, which could be either byval or byref. If it's byref,
+ * we need to pull the actual value out of FSS's varlena array.
+ */
+static Datum
+get_monitored_value(FSS *fss, MonitoredElement *element, bool *isnull)
+{
+	Datum result = element->value;
+	int index;
+
+	*isnull = IS_NULL(element);
+
+	if (fss->typ.typbyval)
+		return result;
+
+	Assert(fss->top_k);
+	Assert(element->varlen_index < fss->m);
+	Assert(element->varlen_index < ArrayGetNItems(ARR_NDIM(fss->top_k), ARR_DIMS(fss->top_k)));
+
+	index = (int) element->varlen_index;
+	result = array_ref(fss->top_k, 1, &index,
+			-1, fss->typ.typlen, fss->typ.typbyval, fss->typ.typalign, isnull);
+
+	return result;
+}
+
 FSS *
 FSSCopy(FSS *fss)
 {
 	Size size = FSSSize(fss);
 	char *new = palloc(size);
 	memcpy(new, (char *) fss, size);
+
 	return (FSS *) new;
 }
 
@@ -102,9 +173,9 @@ element_cmp(const void *a, const void *b)
 	MonitoredElement *m1 = (MonitoredElement *) a;
 	MonitoredElement *m2 = (MonitoredElement *) b;
 
-	if (!m1->set)
+	if (!IS_SET(m1))
 		return 1;
-	if (!m2->set)
+	if (!IS_SET(m2))
 		return -1;
 
 	/* We sort by (-frequency, error) */
@@ -121,45 +192,78 @@ element_cmp(const void *a, const void *b)
 	return 0;
 }
 
-void
-FSSIncrement(FSS *fss, Datum datum)
+FSS *
+FSSIncrement(FSS *fss, Datum datum, bool isnull)
 {
-	FSSIncrementWeighted(fss, datum, 1);
+	return FSSIncrementWeighted(fss, datum, isnull, 1);
 }
 
-void
-FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight)
+/*
+ * set_varlena
+ *
+ * Store the monitored varlena value in the given FSS's varlena array
+ */
+static FSS *
+set_varlena(FSS *fss, MonitoredElement *element, Datum value, bool isnull)
 {
-	StringInfo buf;
-	uint64_t hash;
-	Counter *counter;
-	bool compare_hash;
-	Datum elt;
-	int free_slot = -1;
-	MonitoredElement *m_elt;
-	int slot;
-	bool needs_sort = true;
-	int counter_idx;
+	ArrayType *result;
+	int index;
+
+	Assert(fss->top_k);
+	Assert(element->varlen_index < fss->m);
+
+	/* For byref types, the value is actually an index into the varlena array */
+	index = element->varlen_index;
+	result = array_set(fss->top_k, 1, &index, value,
+			isnull, -1, fss->typ.typlen, fss->typ.typbyval, fss->typ.typalign);
+
+	fss = fss_resize_topk(fss, result);
+
+	return fss;
+}
+
+/*
+ * hash_datum
+ *
+ * Hash a datum using the given FSS's type information
+ */
+static uint64_t
+hash_datum(FSS* fss, Datum d)
+{
+	uint64_t h;
+	StringInfoData buf;
 	TypeCacheEntry typ;
 
 	typ.type_id = fss->typ.typoid;
 	typ.typbyval = fss->typ.typbyval;
 	typ.typlen = fss->typ.typlen;
+	typ.typtype = fss->typ.typtype;
 
-	buf = makeStringInfo();
-	DatumToBytes(datum, &typ, buf);
-	hash = MurmurHash3_64(buf->data, buf->len, MURMUR_SEED);
-	pfree(buf->data);
-	pfree(buf);
+	initStringInfo(&buf);
+	DatumToBytes(d, &typ, &buf);
+	h = MurmurHash3_64(buf.data, buf.len, MURMUR_SEED);
+	pfree(buf.data);
+
+	return h;
+}
+
+FSS *
+FSSIncrementWeighted(FSS *fss, Datum incoming, bool incoming_null, uint64_t weight)
+{
+	uint64_t incoming_hash;
+	Counter *counter;
+	int free_slot = -1;
+	MonitoredElement *m_elt;
+	int slot;
+	bool needs_sort = true;
+	int counter_idx;
+	Datum store_value;
 
-	counter_idx = hash % fss->h;
+	incoming_hash = hash_datum(fss, incoming_null ? 0 : incoming);
+	counter_idx = incoming_hash % fss->h;
 	counter = &fss->bitmap_counter[counter_idx];
-	compare_hash = FSS_STORES_DATUMS(fss);
 
-	if (compare_hash)
-		elt = (Datum) hash;
-	else
-		elt = datum;
+	store_value = fss->typ.typbyval ? incoming : incoming_hash;
 
 	if (counter->count > 0)
 	{
@@ -170,13 +274,13 @@ FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight)
 		{
 			m_elt = &fss->monitored_elements[i];
 
-			if (!m_elt->set)
+			if (!IS_SET(m_elt))
 			{
 				free_slot = i;
 				break;
 			}
 
-			if (m_elt->value == elt)
+			if (m_elt->value == store_value && (incoming_null == IS_NULL(m_elt)))
 			{
 				found = true;
 				break;
@@ -191,14 +295,14 @@ FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight)
 			goto done;
 		}
 	}
-	else if (!fss->monitored_elements[fss->m - 1].set)
+	else if (!IS_SET(&fss->monitored_elements[fss->m - 1]))
 	{
 		int i;
 
 		/* Find the first free slot */
 		for (i = 0; i < fss->m; i++)
 		{
-			if (!fss->monitored_elements[i].set)
+			if (!IS_SET(&fss->monitored_elements[i]))
 			{
 				free_slot = i;
 				break;
@@ -206,25 +310,22 @@ FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight)
 		}
 	}
 
-
 	/* This is only executed if datum is not monitored */
 	if (counter->alpha + weight >= fss->monitored_elements[fss->m - 1].frequency)
 	{
 		/* Need to evict an element? */
 		if (free_slot == -1)
 		{
-			Counter *counter;
-
+			Counter *c;
 			/*
 			 * We always evict the last element because the monitored element array is
 			 * sorted by (-frequency, error).
 			 */
 			slot = fss->m - 1;
 			m_elt = &fss->monitored_elements[slot];
-			counter = &fss->bitmap_counter[m_elt->counter];
-			counter->count--;
-			counter->alpha = m_elt->frequency;
-
+			c = &fss->bitmap_counter[m_elt->counter];
+			c->count--;
+			c->alpha = m_elt->frequency;
 		}
 		else
 		{
@@ -232,12 +333,21 @@ FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight)
 			m_elt = &fss->monitored_elements[slot];
 		}
 
-		m_elt->value = elt;
+		SET(m_elt);
+		if (incoming_null)
+			SET_NULL(m_elt);
+
 		m_elt->frequency = counter->alpha + weight;
 		m_elt->error = counter->alpha;
-		m_elt->set = true;
 		m_elt->counter = counter_idx;
+		m_elt->value = store_value;
 		counter->count++;
+
+		if (!fss->typ.typbyval)
+		{
+			fss = set_varlena(fss, m_elt, incoming, incoming_null);
+			m_elt = &fss->monitored_elements[slot];
+		}
 	}
 	else
 	{
@@ -271,6 +381,10 @@ FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight)
 	}
 
 	fss->count++;
+
+	SET_VARSIZE(fss, FSSSize(fss));
+
+	return fss;
 }
 
 /*
@@ -284,6 +398,7 @@ FSSMerge(FSS *fss, FSS *incoming)
 {
 	int i, j, k;
 	MonitoredElement *tmp;
+	ArrayType *top_k;
 
 	Assert(fss->h == incoming->h);
 	Assert(fss->m == incoming->m);
@@ -302,38 +417,80 @@ FSSMerge(FSS *fss, FSS *incoming)
 	for (i = 0; i < fss->m; i++)
 	{
 		bool found = false;
-		MonitoredElement *elt1 = &incoming->monitored_elements[i];
+		MonitoredElement *incoming_elt = &incoming->monitored_elements[i];
 
-		if (!elt1->set)
+		if (!IS_SET(incoming_elt))
 			break;
 
 		for (j = 0; j < fss->m; j++)
 		{
-			MonitoredElement *elt2 = &tmp[j];
+			MonitoredElement *elt = &tmp[j];
 
-			if (elt1->value == elt2->value)
+			if (!IS_SET(elt))
+				break;
+
+			if (incoming_elt->value == elt->value && (IS_NULL(incoming_elt) == IS_NULL(elt)))
 			{
-				elt2->frequency += elt1->frequency;
-				elt2->error += elt1->error;
+				elt->frequency += incoming_elt->frequency;
+				elt->error += incoming_elt->error;
 				found = true;
 				break;
 			}
 		}
 
 		if (!found)
-			tmp[k++] = *elt1;
+		{
+			tmp[k] = *incoming_elt;
+			SET_NEW(&tmp[k]);
+			k++;
+		}
 	}
 
 	qsort(tmp, k, sizeof(MonitoredElement), element_cmp);
-	memcpy(fss->monitored_elements, tmp, sizeof(MonitoredElement) * fss->m);
 
+	/* If we added any new byref elements, we need to rebuild the varlena array */
+	if (k - fss->m > 0 && !fss->typ.typbyval)
+	{
+		top_k = construct_empty_array(fss->typ.typoid);
+
+		/*
+		 * For each monitored element in the sorted array, point its value to the
+		 * varlena array attached to the output FSS
+		 */
+		for (i = 0; i < fss->m; i++)
+		{
+			MonitoredElement *elt = &tmp[i];
+			Datum value;
+			bool isnull;
+
+			if (!IS_SET(elt))
+				break;
+
+			if (IS_NEW(elt))
+			{
+				value = get_monitored_value(incoming, elt, &isnull);
+				UNSET_NEW(elt);
+			}
+			else
+				value = get_monitored_value(fss, elt, &isnull);
+
+			top_k = array_set(top_k, 1, &i, value,
+					isnull, -1, fss->typ.typlen, fss->typ.typbyval, fss->typ.typalign);
+
+			elt->varlen_index = (Datum ) i;
+		}
+
+		fss = fss_resize_topk(fss, top_k);
+	}
+
+	memcpy(fss->monitored_elements, tmp, sizeof(MonitoredElement) * fss->m);
 	pfree(tmp);
 
 	for (i = 0; i < fss->m; i++)
 	{
 		MonitoredElement *elt = &fss->monitored_elements[i];
 
-		if (!elt->set)
+		if (!IS_SET(elt))
 			break;
 
 		fss->bitmap_counter[elt->counter].count++;
@@ -341,33 +498,45 @@ FSSMerge(FSS *fss, FSS *incoming)
 
 	fss->count += incoming->count;
 
+	SET_VARSIZE(fss, FSSSize(fss));
+
 	return fss;
 }
 
 Datum *
-FSSTopK(FSS *fss, uint16_t k, uint16_t *found)
+FSSTopK(FSS *fss, uint16_t k, bool **nulls, uint16_t *found)
 {
 	int i;
 	Datum *datums;
+	bool *null_k;
 
 	if (k > fss->k)
 		elog(ERROR, "maximum value for k exceeded");
 
 	datums = palloc(sizeof(Datum) * k);
 
+	if (nulls)
+		null_k = palloc0(sizeof(bool) * k);
+
 	for (i = 0; i < k; i++)
 	{
 		MonitoredElement *elt = &fss->monitored_elements[i];
+		bool isnull;
 
-		if (!elt->set)
+		if (!IS_SET(elt))
 			break;
 
-		datums[i] = elt->value;
+		datums[i] = get_monitored_value(fss, elt, &isnull);
+		if (nulls)
+			null_k[i] = isnull;
 	}
 
 	if (found)
 		*found = i;
 
+	if (nulls)
+		*nulls = null_k;
+
 	return datums;
 }
 
@@ -386,7 +555,7 @@ FSSTopKCounts(FSS *fss, uint16_t k, uint16_t *found)
 	{
 		MonitoredElement *elt = &fss->monitored_elements[i];
 
-		if (!elt->set)
+		if (!IS_SET(elt))
 			break;
 
 		counts[i] = elt->frequency;
@@ -410,7 +579,7 @@ FSSSize(FSS *fss)
 	Size sz = sizeof(FSS) + (sizeof(Counter) * fss->h) + (sizeof(MonitoredElement) * fss->m);
 
 	if (FSS_STORES_DATUMS(fss))
-		sz += sizeof(Datum) * fss->k;
+		sz += ARR_SIZE(fss->top_k);
 
 	return sz;
 }
@@ -447,8 +616,10 @@ FSSPrint(FSS *fss)
 	for (i = 0; i < fss->m; i++)
 	{
 		MonitoredElement *elt = &fss->monitored_elements[i];
-		if (elt->set)
-			appendStringInfo(buf, "| (%ld, %ld, %d) |", elt->value, elt->frequency, elt->error);
+		bool isnull;
+		Datum value = get_monitored_value(fss, elt, &isnull);
+		if (IS_SET(elt))
+			appendStringInfo(buf, "| (%ld, %ld, %d) |", value, elt->frequency, elt->error);
 		else
 			appendStringInfo(buf, "| (-, -, -) |");
 
diff --git a/src/backend/utils/adt/fssfuncs.c b/src/backend/utils/adt/fssfuncs.c
index aea8f647f..7dd657e54 100644
--- a/src/backend/utils/adt/fssfuncs.c
+++ b/src/backend/utils/adt/fssfuncs.c
@@ -30,30 +30,6 @@
 
 #define DEFAULT_K 5
 
-static FSS *
-fss_fix_ptrs(struct varlena *bytes)
-{
-	char *pos;
-	FSS *fss = (FSS *) bytes;
-
-	/* Fix pointers */
-	pos = (char *) fss;
-	pos += sizeof(FSS);
-	fss->bitmap_counter = (Counter *) pos;
-	pos += sizeof(Counter) * fss->h;
-	fss->monitored_elements = (MonitoredElement *) pos;
-
-	if (FSS_STORES_DATUMS(fss))
-	{
-		pos += sizeof(MonitoredElement) * fss->m;
-		fss->top_k = (Datum *) pos;
-	}
-	else
-		fss->top_k = NULL;
-
-	return fss;
-}
-
 Datum
 fss_print(PG_FUNCTION_ARGS)
 {
@@ -63,7 +39,7 @@ fss_print(PG_FUNCTION_ARGS)
 	if (PG_ARGISNULL(0))
 		PG_RETURN_NULL();
 
-	fss = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+	fss = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
 	initStringInfo(&buf);
 	appendStringInfo(&buf, "{ m = %d, h = %d, count = %ld, size = %ldkB }", fss->m, fss->h, fss->count, FSSSize(fss) / 1024);
@@ -86,16 +62,16 @@ fss_agg_trans(PG_FUNCTION_ARGS)
 
 	if (PG_ARGISNULL(0))
 	{
-		uint16_t k = PG_GETARG_INT64(2);
+		uint16_t k = PG_GETARG_UINT16(2);
 		Oid type = AggGetInitialArgType(fcinfo);
 		TypeCacheEntry *typ = lookup_type_cache(type, 0);
 		fcinfo->flinfo->fn_extra = typ;
 		state = FSSCreate(k, typ);
 	}
 	else
-		state = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+		state = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
-	FSSIncrement(state, incoming);
+	state = FSSIncrement(state, incoming, PG_ARGISNULL(1));
 
 	MemoryContextSwitchTo(old);
 
@@ -118,16 +94,16 @@ fss_agg_weighted_trans(PG_FUNCTION_ARGS)
 
 	if (PG_ARGISNULL(0))
 	{
-		uint16_t k = PG_GETARG_INT64(2);
+		uint16_t k = PG_GETARG_UINT16(2);
 		Oid type = AggGetInitialArgType(fcinfo);
 		TypeCacheEntry *typ = lookup_type_cache(type, 0);
 		fcinfo->flinfo->fn_extra = typ;
 		state = FSSCreate(k, typ);
 	}
 	else
-		state = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+		state = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
-	FSSIncrementWeighted(state, incoming, weight);
+	state = FSSIncrementWeighted(state, incoming, PG_ARGISNULL(1), weight);
 
 	MemoryContextSwitchTo(old);
 
@@ -149,18 +125,26 @@ fss_agg_transp(PG_FUNCTION_ARGS)
 
 	if (PG_ARGISNULL(0))
 	{
-		uint16_t k = PG_GETARG_INT64(2);
-		uint16_t m = PG_GETARG_INT64(3);
-		uint16_t h = PG_GETARG_INT64(4);
+		uint16_t k = PG_GETARG_UINT16(2);
+		uint16_t m = PG_GETARG_UINT16(3);
+		uint16_t h = PG_GETARG_UINT16(4);
 		Oid type = AggGetInitialArgType(fcinfo);
 		TypeCacheEntry *typ = lookup_type_cache(type, 0);
 		fcinfo->flinfo->fn_extra = typ;
+
+		if (k > m || k > USHRT_MAX)
+			elog(ERROR, "maximum value of k exceeded");
+		if (m > USHRT_MAX)
+			elog(LOG, "maximum value of m exceeded");
+		if (h > USHRT_MAX)
+			elog(ERROR, "maximum value of h exceeded");
+
 		state = FSSCreateWithMAndH(k, typ, m, h);
 	}
 	else
-		state = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+		state = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
-	FSSIncrement(state, incoming);
+	state = FSSIncrement(state, incoming, PG_ARGISNULL(1));
 
 	MemoryContextSwitchTo(old);
 
@@ -173,7 +157,7 @@ fss_merge_agg_trans(PG_FUNCTION_ARGS)
 	MemoryContext old;
 	MemoryContext context;
 	FSS *state;
-	FSS *incoming = fss_fix_ptrs(PG_GETARG_VARLENA_P(1));
+	FSS *incoming = FSSFromBytes(PG_GETARG_VARLENA_P(1));
 
 	if (!AggCheckCallContext(fcinfo, &context))
 		elog(ERROR, "fss_merge_agg_trans called in non-aggregate context");
@@ -186,7 +170,7 @@ fss_merge_agg_trans(PG_FUNCTION_ARGS)
 		PG_RETURN_POINTER(state);
 	}
 
-	state = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+	state = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 	state = FSSMerge(state, incoming);
 
 	MemoryContextSwitchTo(old);
@@ -200,6 +184,7 @@ fss_topk(PG_FUNCTION_ARGS)
 	FSS *fss;
 	Datum *datums;
 	uint64_t *freqs;
+	bool *null_k;
 	uint16_t found;
 	Tuplestorestate *store;
 	ReturnSetInfo *rsi;
@@ -209,7 +194,7 @@ fss_topk(PG_FUNCTION_ARGS)
 	if (PG_ARGISNULL(0))
 		PG_RETURN_NULL();
 
-	fss = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+	fss = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 	desc= CreateTemplateTupleDesc(2, false);
 	TupleDescInitEntry(desc, (AttrNumber) 1, "value", fss->typ.typoid, -1, 0);
 	TupleDescInitEntry(desc, (AttrNumber) 2, "frequency", INT8OID, -1, 0);
@@ -219,7 +204,7 @@ fss_topk(PG_FUNCTION_ARGS)
 	rsi->setDesc = BlessTupleDesc(desc);
 
 	store = tuplestore_begin_heap(false, false, work_mem);
-	datums = FSSTopK(fss, fss->k, &found);
+	datums = FSSTopK(fss, fss->k, &null_k, &found);
 	freqs = FSSTopKCounts(fss, fss->k, &found);
 
 	for (i = 0; i < found; i++)
@@ -230,6 +215,7 @@ fss_topk(PG_FUNCTION_ARGS)
 
 		MemSet(nulls, false, sizeof(nulls));
 
+		nulls[0] = null_k[i];
 		values[0] = datums[i];
 		values[1] = freqs[i];
 
@@ -254,9 +240,9 @@ fss_topk_values(PG_FUNCTION_ARGS)
 	if (PG_ARGISNULL(0))
 		PG_RETURN_NULL();
 
-	fss = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+	fss = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
-	datums = FSSTopK(fss, fss->k, &found);
+	datums = FSSTopK(fss, fss->k, NULL, &found);
 	typ = lookup_type_cache(fss->typ.typoid, 0);
 	arr = construct_array(datums, found, typ->type_id, typ->typlen, typ->typbyval, typ->typalign);
 
@@ -274,7 +260,7 @@ fss_topk_freqs(PG_FUNCTION_ARGS)
 	if (PG_ARGISNULL(0))
 		PG_RETURN_NULL();
 
-	fss = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+	fss = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
 	datums = FSSTopKCounts(fss, fss->k, &found);
 	arr = construct_array(datums, found, INT8OID, 8, true, 'd');
@@ -295,9 +281,9 @@ Datum
 fss_emptyp(PG_FUNCTION_ARGS)
 {
 	Oid typid = PG_GETARG_OID(0);
-	uint16_t k = PG_GETARG_INT64(1);
-	uint16_t m = PG_GETARG_INT64(2);
-	uint16_t h = PG_GETARG_INT64(3);
+	uint16_t k = PG_GETARG_UINT16(1);
+	uint16_t m = PG_GETARG_UINT16(2);
+	uint16_t h = PG_GETARG_UINT16(3);
 	FSS *fss = FSSCreateWithMAndH(k, lookup_type_cache(typid, 0), m, h);
 	PG_RETURN_POINTER(fss);
 }
@@ -312,13 +298,13 @@ fss_increment(PG_FUNCTION_ARGS)
 		fss = FSSCreate(DEFAULT_K, typ);
 	else
 	{
-		fss = fss_fix_ptrs(PG_GETARG_VARLENA_P(0));
+		fss = FSSFromBytes(PG_GETARG_VARLENA_P(0));
 
 		if (fss->typ.typoid != typ->type_id)
 			elog(ERROR, "type mismatch for incoming value");
 	}
 
-	FSSIncrement(fss, PG_GETARG_DATUM(1));
+	fss = FSSIncrement(fss, PG_GETARG_DATUM(1), PG_ARGISNULL(1));
 
 	PG_RETURN_POINTER(fss);
 }
diff --git a/src/include/pipeline/fss.h b/src/include/pipeline/fss.h
index e63cdacc1..17d20bd9d 100644
--- a/src/include/pipeline/fss.h
+++ b/src/include/pipeline/fss.h
@@ -19,12 +19,25 @@
 
 #define FSS_STORES_DATUMS(fss) ((fss)->top_k != NULL)
 
+#define ELEMENT_SET 	0x01
+#define ELEMENT_NEW 	0x02
+#define ELEMENT_NULL 0x04
+
+#define IS_SET(el) ((el)->flags & ELEMENT_SET)
+#define IS_NEW(el) ((el)->flags & ELEMENT_NEW)
+#define IS_NULL(el) ((el)->flags & ELEMENT_NULL)
+#define SET_NEW(el) ((el)->flags |= (ELEMENT_SET | ELEMENT_NEW))
+#define UNSET_NEW(el) ((el)->flags = ELEMENT_SET)
+#define SET(el) ((el)->flags |= ELEMENT_SET)
+#define SET_NULL(el) ((el)-> flags |= ELEMENT_NULL)
+
 typedef struct FSSTypeInfo
 {
 	Oid typoid;
 	int16 typlen;
 	bool typbyval;
 	char typalign;
+	char typtype;
 } FSSTypeInfo;
 
 typedef struct Counter
@@ -35,17 +48,17 @@ typedef struct Counter
 
 typedef struct MonitoredElement
 {
-	bool set;
+	char flags;
 	uint64_t frequency;
 	uint32_t error;
 	uint16_t counter;
+	uint16_t varlen_index;
 	Datum value;
 } MonitoredElement;
 
 typedef struct FSS
 {
 	uint32	vl_len_;
-	bool packed;
 	uint64_t count;
 	uint16_t h;
 	uint16_t m;
@@ -53,18 +66,19 @@ typedef struct FSS
 	FSSTypeInfo typ;
 	Counter *bitmap_counter; /* length h */
 	MonitoredElement *monitored_elements; /* length m */
-	Datum *top_k; /* length k */
+	ArrayType *top_k; /* length k */
 } FSS;
 
-extern FSS *FSSCreateWithMAndH(uint64_t k, TypeCacheEntry *typ, uint64_t m, uint64_t h);
+extern FSS *FSSFromBytes(struct varlena *bytes);
+extern FSS *FSSCreateWithMAndH(uint16_t k, TypeCacheEntry *typ, uint16_t m, uint16_t h);
 extern FSS *FSSCreate(uint64_t k, TypeCacheEntry *typ);
 extern void FSSDestroy(FSS *fss);
 
 extern FSS *FSSCopy(FSS *fss);
-extern void FSSIncrement(FSS *fss, Datum datum);
-extern void FSSIncrementWeighted(FSS *fss, Datum datum, uint64_t weight);
+extern FSS *FSSIncrement(FSS *fss, Datum datum, bool isnull);
+extern FSS *FSSIncrementWeighted(FSS *fss, Datum datum, bool isnull, uint64_t weight);
 extern FSS *FSSMerge(FSS *fss, FSS *incoming);
-extern Datum *FSSTopK(FSS *fss, uint16_t k, uint16_t *found);
+extern Datum *FSSTopK(FSS *fss, uint16_t k, bool **nulls, uint16_t *found);
 extern uint64_t *FSSTopKCounts(FSS *fss, uint16_t k, uint16_t *found);
 extern uint64_t FSSTotal(FSS *fss);
 extern Size FSSSize(FSS *fss);
diff --git a/src/test/regress/expected/cont_fss_agg.out b/src/test/regress/expected/cont_fss_agg.out
index e11b2b779..302077b1c 100644
--- a/src/test/regress/expected/cont_fss_agg.out
+++ b/src/test/regress/expected/cont_fss_agg.out
@@ -1,9 +1,6 @@
 CREATE CONTINUOUS VIEW test_fss_agg0 AS SELECT k::text, fss_agg(x::int, 3) FROM test_fss_agg_stream GROUP BY k;
 CREATE CONTINUOUS VIEW test_fss_agg1 AS SELECT k::text, fss_agg(x::float8, 3) FROM test_fss_agg_stream GROUP BY k;
 CREATE CONTINUOUS VIEW test_fss_agg2 AS SELECT k::text, fss_agg(k::text, 3) FROM test_fss_agg_stream GROUP BY k;
-ERROR:  fss_agg does not support reference types
-LINE 1: ... CONTINUOUS VIEW test_fss_agg2 AS SELECT k::text, fss_agg(k:...
-                                                             ^
 INSERT INTO test_fss_agg_stream (k, x) VALUES ('a', 1);
 INSERT INTO test_fss_agg_stream (k, x) VALUES ('a', 2);
 INSERT INTO test_fss_agg_stream (k, x) VALUES ('a', 3);
@@ -70,6 +67,7 @@ SELECT fss_topk(combine(fss_agg)) FROM test_fss_agg1;
 
 DROP CONTINUOUS VIEW test_fss_agg0;
 DROP CONTINUOUS VIEW test_fss_agg1;
+DROP CONTINUOUS VIEW test_fss_agg2;
 CREATE CONTINUOUS VIEW test_fss_agg3 AS SELECT fss_agg_weighted(x::integer, 3, y::integer) FROM test_fss_agg_stream;
 INSERT INTO test_fss_agg_stream (x, y) VALUES (0, 10);
 INSERT INTO test_fss_agg_stream (x, y) VALUES (0, 10);
@@ -111,12 +109,50 @@ SELECT fss_topk(fss_agg_weighted) FROM test_fss_agg3;
 INSERT INTO test_fss_agg_stream (x, y) VALUES (5, 500);
 INSERT INTO test_fss_agg_stream (x, y) VALUES (6, 1000);
 INSERT INTO test_fss_agg_stream (x, y) VALUES (7, 10000);
+INSERT INTO test_fss_agg_stream (x, y) VALUES (8, 10000);
+INSERT INTO test_fss_agg_stream (x, y) VALUES (8, 10000);
 SELECT fss_topk(fss_agg_weighted) FROM test_fss_agg3;
  fss_topk  
 -----------
+ (8,20000)
  (7,10000)
  (6,1000)
- (5,500)
 (3 rows)
 
+CREATE CONTINUOUS VIEW test_fss_agg4 AS SELECT fss_agg_weighted(t::text, 4, y::integer) FROM test_fss_agg_stream;
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 100);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('xxxx', 200);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 500);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 500);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('xxxxxxxxxxxxxxxxxxxxx', 500);
+SELECT fss_topk_values(fss_agg_weighted) FROM test_fss_agg4;
+                                                 fss_topk_values                                                 
+-----------------------------------------------------------------------------------------------------------------
+ {yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy,xxxxxxxxxxxxxxxxxxxxx,xxxx,xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx}
+(1 row)
+
+SELECT fss_topk_freqs(fss_agg_weighted) FROM test_fss_agg4;
+   fss_topk_freqs   
+--------------------
+ {1000,500,200,100}
+(1 row)
+
 DROP CONTINUOUS VIEW test_fss_agg3;
+DROP CONTINUOUS VIEW test_fss_agg4;
+CREATE CONTINUOUS VIEW test_fss_agg5 AS SELECT fss_agg(x::integer, 4) FROM test_fss_agg_stream;
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+INSERT INTO test_fss_agg_stream (x) VALUES (0);
+INSERT INTO test_fss_agg_stream (x) VALUES (0);
+INSERT INTO test_fss_agg_stream (x) VALUES (1);
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+SELECT fss_topk(fss_agg) FROM test_fss_agg5;
+ fss_topk 
+----------
+ (,4)
+ (0,2)
+ (1,1)
+(3 rows)
+
+DROP CONTINUOUS VIEW test_fss_agg5;
diff --git a/src/test/regress/sql/cont_fss_agg.sql b/src/test/regress/sql/cont_fss_agg.sql
index 39aaf6483..9194315bc 100644
--- a/src/test/regress/sql/cont_fss_agg.sql
+++ b/src/test/regress/sql/cont_fss_agg.sql
@@ -26,6 +26,7 @@ SELECT fss_topk(combine(fss_agg)) FROM test_fss_agg1;
 
 DROP CONTINUOUS VIEW test_fss_agg0;
 DROP CONTINUOUS VIEW test_fss_agg1;
+DROP CONTINUOUS VIEW test_fss_agg2;
 
 CREATE CONTINUOUS VIEW test_fss_agg3 AS SELECT fss_agg_weighted(x::integer, 3, y::integer) FROM test_fss_agg_stream;
 
@@ -48,6 +49,34 @@ SELECT fss_topk(fss_agg_weighted) FROM test_fss_agg3;
 INSERT INTO test_fss_agg_stream (x, y) VALUES (5, 500);
 INSERT INTO test_fss_agg_stream (x, y) VALUES (6, 1000);
 INSERT INTO test_fss_agg_stream (x, y) VALUES (7, 10000);
+INSERT INTO test_fss_agg_stream (x, y) VALUES (8, 10000);
+INSERT INTO test_fss_agg_stream (x, y) VALUES (8, 10000);
 
 SELECT fss_topk(fss_agg_weighted) FROM test_fss_agg3;
+
+CREATE CONTINUOUS VIEW test_fss_agg4 AS SELECT fss_agg_weighted(t::text, 4, y::integer) FROM test_fss_agg_stream;
+
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx', 100);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('xxxx', 200);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 500);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('yyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy', 500);
+INSERT INTO test_fss_agg_stream (t, y) VALUES ('xxxxxxxxxxxxxxxxxxxxx', 500);
+
+SELECT fss_topk_values(fss_agg_weighted) FROM test_fss_agg4;
+SELECT fss_topk_freqs(fss_agg_weighted) FROM test_fss_agg4;
+
 DROP CONTINUOUS VIEW test_fss_agg3;
+DROP CONTINUOUS VIEW test_fss_agg4;
+
+CREATE CONTINUOUS VIEW test_fss_agg5 AS SELECT fss_agg(x::integer, 4) FROM test_fss_agg_stream;
+
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+INSERT INTO test_fss_agg_stream (x) VALUES (0);
+INSERT INTO test_fss_agg_stream (x) VALUES (0);
+INSERT INTO test_fss_agg_stream (x) VALUES (1);
+INSERT INTO test_fss_agg_stream (x) VALUES (null);
+
+SELECT fss_topk(fss_agg) FROM test_fss_agg5;
+DROP CONTINUOUS VIEW test_fss_agg5;
diff --git a/src/test/unit/test_fss.c b/src/test/unit/test_fss.c
index a9429da56..16ae664a2 100644
--- a/src/test/unit/test_fss.c
+++ b/src/test/unit/test_fss.c
@@ -41,7 +41,7 @@ assert_sorted(FSS *fss)
 	{
 		MonitoredElement *elt = &fss->monitored_elements[i];
 
-		if (!elt->set)
+		if (!IS_SET(elt))
 		{
 			saw_unset = true;
 			continue;
@@ -83,12 +83,12 @@ START_TEST(test_basic)
 	{
 		int value = 10 * gaussian();
 		value %= 500;
-		FSSIncrement(fss, value);
+		FSSIncrement(fss, value, false);
 		counts[value + 500]++;
 		assert_sorted(fss);
 	}
 
-	values = FSSTopK(fss, K, NULL);
+	values = FSSTopK(fss, K, NULL, NULL);
 	freqs = FSSTopKCounts(fss, K, NULL);
 
 	soft_errors = 0;
@@ -122,8 +122,8 @@ START_TEST(test_merge)
 		{
 			int value = 10 * gaussian();
 			value %= 500;
-			FSSIncrement(fss1, value);
-			FSSIncrement(tmp, value);
+			FSSIncrement(fss1, value, false);
+			FSSIncrement(tmp, value, false);
 			assert_sorted(fss1);
 			assert_sorted(tmp);
 		}
@@ -132,9 +132,9 @@ START_TEST(test_merge)
 		FSSDestroy(tmp);
 	}
 
-	values1 = FSSTopK(fss1, K, NULL);
+	values1 = FSSTopK(fss1, K, NULL, NULL);
 	freqs1 = FSSTopKCounts(fss1, K, NULL);
-	values2 = FSSTopK(fss2, K, NULL);
+	values2 = FSSTopK(fss2, K, NULL, NULL);
 	freqs2 = FSSTopKCounts(fss2, K, NULL);
 	soft_errors = 0;
 
@@ -165,9 +165,9 @@ START_TEST(test_error)
 
 	for (i = 0; i <= min_freq; i++)
 	{
-		FSSIncrement(fss, 1);
-		FSSIncrement(fss, 2);
-		FSSIncrement(fss, 3);
+		FSSIncrement(fss, 1, false);
+		FSSIncrement(fss, 2, false);
+		FSSIncrement(fss, 3, false);
 		counts[1]++;
 		counts[2]++;
 		counts[3]++;
@@ -177,12 +177,12 @@ START_TEST(test_error)
 	for (i = 0; i < NUM_ITEMS - (2 * min_freq); i++)
 	{
 		int value = (int) uniform() % 500 + 4;
-		FSSIncrement(fss, value);
+		FSSIncrement(fss, value, false);
 		counts[value]++;
 		assert_sorted(fss);
 	}
 
-	values = FSSTopK(fss, K, NULL);
+	values = FSSTopK(fss, K, NULL, NULL);
 	freqs = FSSTopKCounts(fss, K, NULL);
 
 	for (i = 0; i < 3; i++)
@@ -206,9 +206,9 @@ START_TEST(test_weighted)
 
 	for (i = 0; i <= min_freq; i++)
 	{
-		FSSIncrementWeighted(fss, 1, 10);
-		FSSIncrementWeighted(fss, 2, 20);
-		FSSIncrementWeighted(fss, 3, 30);
+		fss = FSSIncrementWeighted(fss, 1, false, 10);
+		fss = FSSIncrementWeighted(fss, 2, false, 20);
+		fss = FSSIncrementWeighted(fss, 3, false, 30);
 		counts[1] += 10;
 		counts[2] += 20;
 		counts[3] += 30;
@@ -218,12 +218,12 @@ START_TEST(test_weighted)
 	for (i = 0; i < NUM_ITEMS - (2 * min_freq); i++)
 	{
 		int value = (int) uniform() % 500 + 4;
-		FSSIncrementWeighted(fss, value, 1);
+		fss = FSSIncrementWeighted(fss, value, false, 1);
 		counts[value]++;
 		assert_sorted(fss);
 	}
 
-	values = FSSTopK(fss, K, NULL);
+	values = FSSTopK(fss, K, NULL, NULL);
 	freqs = FSSTopKCounts(fss, K, NULL);
 
 	for (i = 0; i < 3; i++)
