pipeline_stream_insert should perform type validation
I don't think is a bug. There's no way to specify a header of column names when calling `pipeline_stream_insert`, so it's up to the user to ensure that the tuple being inserted matches the descriptor of the stream being written to.

Currently it's fairly easy to crash a worker with "bad" ordering, so we need to at least prevent that from being possible. It should behave the same as:

```
create table t (x integer, y text);
insert into t values ('text', 42);
ERROR:  invalid input syntax for integer: "text"
```

We should probably just make sure that the tuple desc of the continuous transform is equal to the tuple desc of the stream being inserted into.
