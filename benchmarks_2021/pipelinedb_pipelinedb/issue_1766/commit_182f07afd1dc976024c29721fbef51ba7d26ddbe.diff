diff --git a/src/backend/catalog/pipeline_query.c b/src/backend/catalog/pipeline_query.c
index bc3cf4f29..d5ff3fb6a 100644
--- a/src/backend/catalog/pipeline_query.c
+++ b/src/backend/catalog/pipeline_query.c
@@ -604,6 +604,8 @@ GetContQueryForId(Oid id)
 	cq->osrelid = row->osrelid;
 	cq->pkidxid = row->pkidxid;
 	cq->lookupidxid = row->lookupidxid;
+	cq->ttl_attno = row->ttl_attno;
+	cq->ttl = row->ttl;
 
 	if (cq->type == CONT_VIEW)
 	{
diff --git a/src/backend/commands/vacuum.c b/src/backend/commands/vacuum.c
index aba5e095c..1ffe10e17 100644
--- a/src/backend/commands/vacuum.c
+++ b/src/backend/commands/vacuum.c
@@ -35,7 +35,6 @@
 #include "catalog/pg_namespace.h"
 #include "commands/cluster.h"
 #include "commands/vacuum.h"
-#include "pipeline/ttl_vacuum.h"
 #include "miscadmin.h"
 #include "pgstat.h"
 #include "postmaster/autovacuum.h"
@@ -1180,9 +1179,6 @@ vacuum_rel(Oid relid, RangeVar *relation, int options, VacuumParams *params)
 
 	Assert(params != NULL);
 
-	/* If this is a matrel for a continuous view with a TTL, delete all expired tuples */
-	DeleteTTLExpiredTuples(relid);
-
 	/* Begin a transaction for vacuuming this relation */
 	StartTransactionCommand();
 
diff --git a/src/backend/pipeline/Makefile b/src/backend/pipeline/Makefile
index e11768bbc..f201a997e 100644
--- a/src/backend/pipeline/Makefile
+++ b/src/backend/pipeline/Makefile
@@ -15,9 +15,9 @@ top_builddir = ../../..
 include $(top_builddir)/src/Makefile.global
 
 OBJS = combiner_receiver.o planner.o update.o stream.o \
-			 matrel.o ttl_vacuum.o tdigest.o miscutils.o bloom.o hll.o cmsketch.o \
+			 matrel.o tdigest.o miscutils.o bloom.o hll.o cmsketch.o \
 			 analyzer.o scheduler.o worker.o combiner.o fss.o stream_fdw.o executor.o transform_receiver.o \
-			 queue.o
+			 queue.o reaper.o
 
 SUBDIRS = ipc
 
diff --git a/src/backend/pipeline/combiner.c b/src/backend/pipeline/combiner.c
index 808ec35d7..288beb5ed 100644
--- a/src/backend/pipeline/combiner.c
+++ b/src/backend/pipeline/combiner.c
@@ -41,7 +41,6 @@
 #include "pipeline/miscutils.h"
 #include "pipeline/stream.h"
 #include "pipeline/stream_fdw.h"
-#include "pipeline/ttl_vacuum.h"
 #include "storage/ipc.h"
 #include "tcop/dest.h"
 #include "tcop/pquery.h"
diff --git a/src/backend/pipeline/reaper.c b/src/backend/pipeline/reaper.c
new file mode 100644
index 000000000..4cbac93c9
--- /dev/null
+++ b/src/backend/pipeline/reaper.c
@@ -0,0 +1,305 @@
+/*-------------------------------------------------------------------------
+ *
+ * reaper.c
+ *
+ * Copyright (c) 2017, PipelineDB
+ *
+ * IDENTIFICATION
+ *    src/include/pipeline/reaper.c
+ *
+ *-------------------------------------------------------------------------
+ */
+#include "postgres.h"
+
+#include "postgres.h"
+#include "access/htup_details.h"
+#include "access/xact.h"
+#include "catalog/pipeline_query.h"
+#include "catalog/pipeline_query_fn.h"
+#include "executor/spi.h"
+#include "executor/tstoreReceiver.h"
+#include "miscadmin.h"
+#include "nodes/execnodes.h"
+#include "nodes/makefuncs.h"
+#include "optimizer/clauses.h"
+#include "optimizer/planner.h"
+#include "parser/parse_expr.h"
+#include "pipeline/analyzer.h"
+#include "pipeline/miscutils.h"
+#include "pipeline/matrel.h"
+#include "pipeline/reaper.h"
+#include "pipeline/scheduler.h"
+#include "storage/lmgr.h"
+#include "tcop/pquery.h"
+#include "tcop/tcopprot.h"
+#include "utils/hsearch.h"
+#include "utils/int8.h"
+#include "utils/lsyscache.h"
+#include "utils/memutils.h"
+#include "utils/rel.h"
+#include "utils/ruleutils.h"
+#include "utils/snapmgr.h"
+
+#define DEFAULT_SLEEP_S 2 /* Sleep for 2s unless there are CVs with TTLs */
+
+#define DELETE_TEMPLATE "DELETE FROM %s.%s WHERE \"$pk\" IN (%s);"
+#define SELECT_PK_WITH_LIMIT "SELECT \"$pk\" FROM %s.%s WHERE %s < now() - interval '%d seconds' LIMIT %d FOR UPDATE SKIP LOCKED"
+#define SELECT_PK_NO_LIMIT "SELECT \"$pk\" FROM %s.%s WHERE %s < now() - interval '%d seconds' FOR UPDATE SKIP LOCKED"
+
+int continuous_query_ttl_expiration_batch_size;
+int continuous_query_ttl_expiration_threshold;
+
+static char *
+get_delete_sql(RangeVar *cvname, RangeVar *matrelname)
+{
+	StringInfoData delete_sql;
+	StringInfoData select_sql;
+	char *ttl_col;
+	int ttl;
+
+	GetTTLInfo(cvname, &ttl_col, &ttl);
+
+	initStringInfo(&select_sql);
+
+	if (continuous_query_ttl_expiration_batch_size)
+		appendStringInfo(&select_sql, SELECT_PK_WITH_LIMIT,
+				matrelname->schemaname, matrelname->relname, ttl_col, ttl, continuous_query_ttl_expiration_batch_size);
+	else
+		appendStringInfo(&select_sql, SELECT_PK_NO_LIMIT, matrelname->schemaname, matrelname->relname, ttl_col, ttl);
+
+	initStringInfo(&delete_sql);
+	appendStringInfo(&delete_sql, DELETE_TEMPLATE,
+			matrelname->schemaname, matrelname->relname, select_sql.data);
+
+	return delete_sql.data;
+}
+
+/*
+ * DeleteTTLExpiredRows
+ */
+int
+DeleteTTLExpiredRows(RangeVar *cvname, RangeVar *matrel)
+{
+	MemoryContext oldcxt;
+	MemoryContext runctx;
+	bool save_continuous_query_materialization_table_updatable = continuous_query_materialization_table_updatable;
+	char *delete_cmd;
+	int num_deleted = 0;
+
+	continuous_query_materialization_table_updatable = true;
+
+	runctx = AllocSetContextCreate(CurrentMemoryContext,
+			"DeleteSWExpiredTuplesContext",
+			ALLOCSET_DEFAULT_MINSIZE,
+			ALLOCSET_DEFAULT_INITSIZE,
+			ALLOCSET_DEFAULT_MAXSIZE);
+
+	oldcxt = MemoryContextSwitchTo(runctx);
+
+	Assert(IsTTLContView(cvname));
+
+	/* Now we're certain relid is for a TTL continuous view's matrel */
+	delete_cmd = get_delete_sql(cvname, matrel);
+
+	PushActiveSnapshot(GetTransactionSnapshot());
+
+	if (SPI_connect() != SPI_OK_CONNECT)
+		elog(ERROR, "could not connect to SPI manager");
+
+	if (SPI_execute(delete_cmd, false, 0) != SPI_OK_DELETE)
+		elog(ERROR, "SPI_execute failed: %s", delete_cmd);
+
+	num_deleted = SPI_processed;
+
+	if (SPI_finish() != SPI_OK_FINISH)
+		elog(ERROR, "SPI_finish failed");
+
+	PopActiveSnapshot();
+
+	continuous_query_materialization_table_updatable = save_continuous_query_materialization_table_updatable;
+
+	MemoryContextSwitchTo(oldcxt);
+	MemoryContextDelete(runctx);
+
+	return num_deleted;
+}
+
+typedef struct ReaperEntry
+{
+	Oid relid;
+	TimestampTz last_expired;
+	int last_deleted;
+	int ttl;
+} ReaperEntry;
+
+static HTAB *last_expired = NULL;
+
+/*
+ * should_expire
+ */
+static bool
+should_expire(Oid relid)
+{
+	ReaperEntry *entry = (ReaperEntry *) hash_search(last_expired, &relid, HASH_FIND, NULL);
+	int threshold_ms;
+
+	/*
+	 * If there were any deletions on the last run, we want to keep expiring without waiting
+	 */
+	if (entry->last_deleted)
+		return true;
+
+	/* We attempt to expire when a configurable percentage of the overall TTL has elapsed */
+	threshold_ms = entry->ttl * (1000 * continuous_query_ttl_expiration_threshold / 100.0);
+
+	Assert(entry);
+
+	if (TimestampDifferenceExceeds(entry->last_expired, GetCurrentTimestamp(), threshold_ms))
+		return true;
+
+	return false;
+}
+
+/*
+ * set_last_expiration
+ */
+static void
+set_last_expiration(Oid relid, int deleted)
+{
+	ReaperEntry *entry = (ReaperEntry *) hash_search(last_expired, &relid, HASH_FIND, NULL);
+	Assert(entry);
+
+	entry->last_expired = GetCurrentTimestamp();
+	entry->last_deleted = deleted;
+}
+
+/*
+ * reset_entries
+ */
+static void
+reset_entries(void)
+{
+	ReaperEntry *entry;
+	HASH_SEQ_STATUS iter;
+
+	hash_seq_init(&iter, last_expired);
+
+	while ((entry = (ReaperEntry *) hash_seq_search(&iter)) != NULL)
+		entry->last_deleted = 0;
+}
+
+/*
+ * get_ttl_rels
+ */
+static List *
+get_ttl_rels(int *min_ttl)
+{
+	List *result = NIL;
+	int id = -1;
+	Bitmapset *ids = GetContinuousViewIds();
+
+	Assert(min_ttl);
+	Assert(last_expired);
+
+	while ((id = bms_next_member(ids, id)) >= 0)
+	{
+		ContQuery *cq = GetContQueryForId(id);
+		List *tup = NIL;
+		ReaperEntry *entry;
+		bool found;
+
+		Assert(cq);
+		if (!AttributeNumberIsValid(cq->ttl_attno))
+			continue;
+
+		tup = lappend(tup, makeInteger(cq->relid));
+		tup = lappend(tup, cq->name);
+		tup = lappend(tup, cq->matrel);
+		result = lappend(result, tup);
+
+		*min_ttl = Min(*min_ttl, cq->ttl_attno);
+
+		entry = (ReaperEntry *) hash_search(last_expired, &cq->relid, HASH_ENTER, &found);
+		if (!found)
+		{
+			entry->last_expired = 0;
+			entry->ttl = cq->ttl;
+		}
+	}
+
+	return result;
+}
+
+void
+ContinuousQueryReaperMain(void)
+{
+	HASHCTL hctl;
+
+	MemSet(&hctl, 0, sizeof(hctl));
+	hctl.hcxt = CurrentMemoryContext;
+	hctl.keysize = sizeof(Oid);
+	hctl.entrysize = sizeof(ReaperEntry);
+	last_expired = hash_create("ReaperHash", 32, &hctl, HASH_CONTEXT | HASH_ELEM | HASH_BLOBS);
+
+	for (;;)
+	{
+		List *ttl_rels = NIL;
+		ListCell *lc;
+		int min_sleep = DEFAULT_SLEEP_S;
+		int total_deleted = 0;
+
+		CHECK_FOR_INTERRUPTS();
+
+		if (get_sigterm_flag())
+			break;
+
+		/*
+		 * Keep trying to expire rows until nothing was actually deleted, then sleep for a reasonable amount of time
+		 */
+		for (;;)
+		{
+			int deleted = 0;
+
+			StartTransactionCommand();
+			SetCurrentStatementStartTimestamp();
+
+			ttl_rels = get_ttl_rels(&min_sleep);
+
+			foreach(lc, ttl_rels)
+			{
+				List *rels = lfirst(lc);
+				RangeVar *cv;
+				RangeVar *matrel;
+				Oid relid;
+
+				Assert(list_length(rels) == 3);
+
+				relid = intVal(linitial(rels));
+				cv = lsecond(rels);
+				matrel = lthird(rels);
+
+				/*
+				 * Skip any relations that aren't likely to have newly expired rows
+				 */
+				if (should_expire(relid))
+				{
+					deleted = DeleteTTLExpiredRows(cv, matrel);
+					total_deleted += deleted;
+					set_last_expiration(relid, deleted);
+				}
+			}
+
+			CommitTransactionCommand();
+
+			/*
+			 * If nothing was deleted on this run, we're done for now
+			 */
+			if (!deleted)
+				break;
+		}
+
+		reset_entries();
+		total_deleted = 0;
+		pg_usleep(min_sleep * 1000 * 1000);
+	}
+}
diff --git a/src/backend/pipeline/scheduler.c b/src/backend/pipeline/scheduler.c
index 807b5ce20..8abe884a7 100644
--- a/src/backend/pipeline/scheduler.c
+++ b/src/backend/pipeline/scheduler.c
@@ -28,6 +28,7 @@
 #include "pipeline/scheduler.h"
 #include "pipeline/ipc/pzmq.h"
 #include "pipeline/miscutils.h"
+#include "pipeline/reaper.h"
 #include "postmaster/fork_process.h"
 #include "postmaster/postmaster.h"
 #include "storage/ipc.h"
@@ -48,7 +49,7 @@
 
 #define MAX_PRIORITY 20 /* XXX(usmanm): can we get this from some sys header? */
 
-#define NUM_BG_WORKERS_PER_DB (continuous_query_num_workers + continuous_query_num_combiners + continuous_query_num_queues)
+#define NUM_BG_WORKERS_PER_DB (continuous_query_num_workers + continuous_query_num_combiners + continuous_query_num_queues + continuous_query_num_reapers)
 #define NUM_LOCKS_PER_DB NUM_BG_WORKERS_PER_DB
 
 #define BG_PROC_STATUS_TIMEOUT 10000
@@ -69,6 +70,8 @@ ContQueryProc *MyContQueryProc = NULL;
 static bool am_cont_scheduler = false;
 static bool am_cont_worker = false;
 static bool am_cont_queue = false;
+static bool am_cont_reaper = false;
+
 bool am_cont_combiner = false;
 
 /* guc parameters */
@@ -76,6 +79,7 @@ bool continuous_queries_enabled;
 int  continuous_query_num_combiners;
 int  continuous_query_num_workers;
 int  continuous_query_num_queues;
+int  continuous_query_num_reapers;
 int continuous_query_queue_mem;
 int  continuous_query_max_wait;
 int  continuous_query_combiner_work_mem;
@@ -158,6 +162,9 @@ GetContQueryProcName(ContQueryProc *proc)
 		case Queue:
 			snprintf(buf, NAMEDATALEN, "queue%d [%s]", proc->group_id, NameStr(proc->db_meta->db_name));
 			break;
+		case Reaper:
+			snprintf(buf, NAMEDATALEN, "reaper%d [%s]", proc->group_id, NameStr(proc->db_meta->db_name));
+			break;
 		case Scheduler:
 			return pstrdup("scheduler");
 			break;
@@ -372,6 +379,10 @@ cont_bgworker_main(Datum arg)
 			am_cont_worker = true;
 			run = &ContinuousQueryWorkerMain;
 			break;
+		case Reaper:
+			am_cont_reaper = true;
+			run = &ContinuousQueryReaperMain;
+			break;
 		default:
 			elog(ERROR, "unknown continuous process type: %d", proc->type);
 	}
@@ -552,6 +563,20 @@ start_database_workers(ContQueryDatabaseMetadata *db_meta)
 
 		success &= run_cont_bgworker(proc);
 	}
+	offset += continuous_query_num_queues;
+
+	for (i = 0; i < continuous_query_num_reapers; i++)
+	{
+		proc = &db_meta->db_procs[i + offset];
+		MemSet(proc, 0, sizeof(ContQueryProc));
+		proc->db_meta = db_meta;
+		proc->pzmq_id = rand() ^ MyProcPid;
+
+		proc->type = Reaper;
+		proc->group_id = i;
+
+		success &= run_cont_bgworker(proc);
+	}
 
 	SpinLockRelease(&db_meta->mutex);
 
diff --git a/src/backend/pipeline/ttl_vacuum.c b/src/backend/pipeline/ttl_vacuum.c
deleted file mode 100644
index 972a4ce56..000000000
--- a/src/backend/pipeline/ttl_vacuum.c
+++ /dev/null
@@ -1,194 +0,0 @@
-/*-------------------------------------------------------------------------
- *
- * ttl_vacuum.c
- *
- *   Support for vacuuming discarded tuples for continuous views with TTLs.
- *
- * Copyright (c) 2013-2016, PipelineDB
- *
- * src/backend/pipeline/ttl_vacuum.c
- *
- *-------------------------------------------------------------------------
- */
-#include "pipeline/ttl_vacuum.h"
-
-#include "postgres.h"
-#include "access/htup_details.h"
-#include "access/xact.h"
-#include "catalog/pipeline_query.h"
-#include "catalog/pipeline_query_fn.h"
-#include "executor/spi.h"
-#include "executor/tstoreReceiver.h"
-#include "miscadmin.h"
-#include "nodes/execnodes.h"
-#include "nodes/makefuncs.h"
-#include "optimizer/clauses.h"
-#include "optimizer/planner.h"
-#include "parser/parse_expr.h"
-#include "pipeline/analyzer.h"
-#include "pipeline/matrel.h"
-#include "storage/lmgr.h"
-#include "tcop/pquery.h"
-#include "tcop/tcopprot.h"
-#include "utils/hsearch.h"
-#include "utils/int8.h"
-#include "utils/lsyscache.h"
-#include "utils/memutils.h"
-#include "utils/rel.h"
-#include "utils/ruleutils.h"
-#include "utils/snapmgr.h"
-
-#define DELETE_TEMPLATE "DELETE FROM %s.%s WHERE \"$pk\" IN " \
-                           "(SELECT \"$pk\" FROM %s.%s WHERE %s < now() - interval '%d seconds' FOR UPDATE SKIP LOCKED);"
-
-static char *
-get_delete_sql(RangeVar *cvname, RangeVar *matrelname)
-{
-	StringInfoData buf;
-	char *ttl_col;
-	int ttl;
-
-	GetTTLInfo(cvname, &ttl_col, &ttl);
-
-	initStringInfo(&buf);
-	appendStringInfo(&buf, DELETE_TEMPLATE,
-			matrelname->schemaname, matrelname->relname, matrelname->schemaname, matrelname->relname, ttl_col, ttl);
-
-	return buf.data;
-}
-
-/*
- * DeleteTTLExpiredTuples
- */
-void
-DeleteTTLExpiredTuples(Oid relid)
-{
-	char *relname;
-	char *namespace;
-	RangeVar *matrel;
-	RangeVar *cvname;
-	MemoryContext oldcxt;
-	MemoryContext runctx;
-	bool save_continuous_query_materialization_table_updatable = continuous_query_materialization_table_updatable;
-	char *delete_cmd;
-
-	continuous_query_materialization_table_updatable = true;
-
-	StartTransactionCommand();
-
-	runctx = AllocSetContextCreate(CurrentMemoryContext,
-			"DeleteSWExpiredTuplesContext",
-			ALLOCSET_DEFAULT_MINSIZE,
-			ALLOCSET_DEFAULT_INITSIZE,
-			ALLOCSET_DEFAULT_MAXSIZE);
-
-	oldcxt = MemoryContextSwitchTo(runctx);
-
-	relname = get_rel_name(relid);
-
-	if (!relname)
-		goto end;
-
-	namespace = get_namespace_name(get_rel_namespace(relid));
-	matrel = makeRangeVar(namespace, relname, -1);
-
-	cvname = GetCVNameFromMatRelName(matrel);
-
-	if (!cvname)
-		goto end;
-
-	if (!IsTTLContView(cvname))
-		goto end;
-
-	/* Now we're certain relid is for a TTL continuous view's matrel */
-	delete_cmd = get_delete_sql(cvname, matrel);
-
-	PushActiveSnapshot(GetTransactionSnapshot());
-
-	if (SPI_connect() != SPI_OK_CONNECT)
-		elog(ERROR, "could not connect to SPI manager");
-
-	if (SPI_execute(delete_cmd, false, 0) != SPI_OK_DELETE)
-		elog(ERROR, "SPI_execute failed: %s", delete_cmd);
-
-	if (SPI_finish() != SPI_OK_FINISH)
-		elog(ERROR, "SPI_finish failed");
-
-	PopActiveSnapshot();
-
-end:
-	continuous_query_materialization_table_updatable = save_continuous_query_materialization_table_updatable;
-
-	MemoryContextSwitchTo(oldcxt);
-	MemoryContextDelete(runctx);
-
-	CommitTransactionCommand();
-}
-
-/*
- * NumTTLExpiredTuples
- */
-uint64_t
-NumTTLExpiredTuples(Oid relid)
-{
-	uint64_t count = 0;
-	char *relname = get_rel_name(relid);
-	char *namespace = get_namespace_name(get_rel_namespace(relid));
-	RangeVar *matrel = makeRangeVar(namespace, relname, -1);
-	RangeVar *cvname;
-	StringInfoData sql;
-	MemoryContext oldcontext;
-	MemoryContext runctx;
-	bool locked;
-
-	if (!relname)
-		return 0;
-
-	locked = ConditionalLockRelationOid(PipelineQueryRelationId, AccessShareLock);
-	if (!locked)
-		return 0;
-
-	cvname = GetCVNameFromMatRelName(matrel);
-	UnlockRelationOid(PipelineQueryRelationId, AccessShareLock);
-
-	if (!cvname)
-		return 0;
-
-	if (!IsTTLContView(cvname))
-		return 0;
-
-	runctx = AllocSetContextCreate(CurrentMemoryContext,
-			"NumSWExpiredTuplesContext",
-			ALLOCSET_DEFAULT_MINSIZE,
-			ALLOCSET_DEFAULT_INITSIZE,
-			ALLOCSET_DEFAULT_MAXSIZE);
-
-	oldcontext = MemoryContextSwitchTo(runctx);
-
-	initStringInfo(&sql);
-	appendStringInfo(&sql, "SELECT COUNT(*) FROM %s.%s", namespace, relname);
-
-	PushActiveSnapshot(GetTransactionSnapshot());
-
-	if (SPI_connect() != SPI_OK_CONNECT)
-		elog(ERROR, "could not connect to SPI manager");
-
-	if (SPI_execute(sql.data, false, 0) != SPI_OK_SELECT)
-		elog(ERROR, "SPI_execute failed: %s", sql.data);
-
-	if (SPI_processed > 0)
-	{
-		char *v = SPI_getvalue(SPI_tuptable->vals[0], SPI_tuptable->tupdesc, 1);
-		count = DatumGetInt64(DirectFunctionCall1(int8in, (Datum) v));
-	}
-
-	if (SPI_finish() != SPI_OK_FINISH)
-		elog(ERROR, "SPI_finish failed");
-
-	PopActiveSnapshot();
-
-	MemoryContextSwitchTo(oldcontext);
-	MemoryContextDelete(runctx);
-
-	return count;
-}
diff --git a/src/backend/postmaster/autovacuum.c b/src/backend/postmaster/autovacuum.c
index ffa389219..7d9f0e4e5 100644
--- a/src/backend/postmaster/autovacuum.c
+++ b/src/backend/postmaster/autovacuum.c
@@ -82,7 +82,6 @@
 #include "miscadmin.h"
 #include "nodes/print.h"
 #include "pgstat.h"
-#include "pipeline/ttl_vacuum.h"
 #include "postmaster/autovacuum.h"
 #include "postmaster/fork_process.h"
 #include "postmaster/postmaster.h"
@@ -2764,10 +2763,9 @@ relation_needs_vacanalyze(Oid relid,
 		/*
 		 * All expired tuples in a SW continuous view should be considered as *dead*.
 		 */
-		uint64_t swvactuples = NumTTLExpiredTuples(relid);
 		reltuples = classForm->reltuples;
-		vactuples = tabentry->n_dead_tuples + swvactuples;
-		anltuples = tabentry->changes_since_analyze + swvactuples;
+		vactuples = tabentry->n_dead_tuples;
+		anltuples = tabentry->changes_since_analyze;
 
 		vacthresh = (float4) vac_base_thresh + vac_scale_factor * reltuples;
 		anlthresh = (float4) anl_base_thresh + anl_scale_factor * reltuples;
diff --git a/src/backend/utils/adt/pipelinefuncs.c b/src/backend/utils/adt/pipelinefuncs.c
index 6e0248929..287d4f7b8 100644
--- a/src/backend/utils/adt/pipelinefuncs.c
+++ b/src/backend/utils/adt/pipelinefuncs.c
@@ -24,6 +24,7 @@
 #include "fmgr.h"
 #include "pipeline/analyzer.h"
 #include "pipeline/ipc/microbatch.h"
+#include "pipeline/reaper.h"
 #include "pipeline/stream.h"
 #include "miscadmin.h"
 #include "utils/array.h"
@@ -1104,3 +1105,34 @@ set_ttl(PG_FUNCTION_ARGS)
 	result = HeapTupleGetDatum(tup);
 	SRF_RETURN_NEXT(funcctx, result);
 }
+
+/*
+ * ttl_expire
+ */
+Datum
+ttl_expire(PG_FUNCTION_ARGS)
+{
+	RangeVar *cv_name;
+	RangeVar *matrel;
+	int result;
+	int save_batch_size = continuous_query_ttl_expiration_batch_size;
+
+	if (PG_ARGISNULL(0))
+		elog(ERROR, "continuous view name cannot be NULL");
+
+	cv_name = makeRangeVarFromNameList(textToQualifiedNameList(PG_GETARG_TEXT_P(0)));
+	matrel = GetMatRelName(cv_name);
+
+	if (!IsTTLContView(cv_name))
+		elog(ERROR, "continuous view \"%s\" does not have a TTL", cv_name->relname);
+
+	/*
+	 * DELETE everything
+	 */
+	continuous_query_ttl_expiration_batch_size = 0;
+	result = DeleteTTLExpiredRows(cv_name, matrel);
+	continuous_query_ttl_expiration_batch_size = save_batch_size;
+
+	PG_RETURN_INT32(result);
+}
+
diff --git a/src/backend/utils/adt/timestamp.c b/src/backend/utils/adt/timestamp.c
index 543002345..c39fb1aca 100644
--- a/src/backend/utils/adt/timestamp.c
+++ b/src/backend/utils/adt/timestamp.c
@@ -1515,6 +1515,7 @@ EncodeSpecialTimestamp(Timestamp dt, char *str)
 Datum
 now(PG_FUNCTION_ARGS)
 {
+//	elog(LOG, "TS=%ld", GetCurrentTransactionStartTimestamp());
 	PG_RETURN_TIMESTAMPTZ(GetCurrentTransactionStartTimestamp());
 }
 
diff --git a/src/backend/utils/misc/guc.c b/src/backend/utils/misc/guc.c
index 08ba3c2b2..a4a0015ea 100644
--- a/src/backend/utils/misc/guc.c
+++ b/src/backend/utils/misc/guc.c
@@ -2820,6 +2820,36 @@ static struct config_int ConfigureNamesInt[] =
 		NULL, NULL, NULL
 	},
 
+	{
+		{"continuous_query_num_reapers", PGC_BACKEND, RESOURCES_ASYNCHRONOUS,
+		 gettext_noop("Sets the number of parallel continuous query reaper processes."),
+		 NULL,
+		},
+		&continuous_query_num_reapers,
+		1, 1, MAX_BACKENDS,
+		NULL, NULL, NULL
+	},
+
+	{
+		{"continuous_query_ttl_expiration_batch_size", PGC_BACKEND, RESOURCES_ASYNCHRONOUS,
+		 gettext_noop("Sets the maximum number of TTL-expired rows to delete at a time."),
+		 NULL,
+		},
+		&continuous_query_ttl_expiration_batch_size,
+		10000, 0, INT_MAX,
+		NULL, NULL, NULL
+	},
+
+	{
+		{"continuous_query_ttl_expiration_threshold", PGC_BACKEND, RESOURCES_ASYNCHRONOUS,
+		 gettext_noop("Sets percentage of a relation's TTL that must have elapsed before attempting to expire rows again."),
+		 NULL,
+		},
+		&continuous_query_ttl_expiration_threshold,
+		5, 0, 100,
+		NULL, NULL, NULL
+	},
+
 	{
 		{"continuous_query_queue_mem", PGC_BACKEND, RESOURCES_MEM,
 		 gettext_noop("Sets the maximum amount of memory each queue process will use."),
diff --git a/src/include/catalog/pg_proc.h b/src/include/catalog/pg_proc.h
index 6e378599d..93cbf9213 100644
--- a/src/include/catalog/pg_proc.h
+++ b/src/include/catalog/pg_proc.h
@@ -5989,6 +5989,9 @@ DESCR("bucket cardinalities retrieval function");
 DATA(insert OID = 4517 ( bucket_ids PGNSP PGUID 12 1 0 0 0 f f f f t f i 1 0 1005 "17" _null_ _null_ _null_ _null_ _null_ bucket_ids _null_ _null_ _null_ ));
 DESCR("bucket ids retrieval function");
 
+DATA(insert OID = 4518 ( ttl_expire	PGNSP PGUID 12 1 1 0 0 f f f f f t i 1 0 20 "25" _null_ _null_ _null_ _null_ _null_ ttl_expire _null_ _null_ _null_ ));
+DESCR("force ttl expiration for a continuous view");
+
 /*
  * Symbolic values for provolatile column: these indicate whether the result
  * of a function is dependent *only* on the values of its explicit arguments,
diff --git a/src/include/catalog/pipeline_query_fn.h b/src/include/catalog/pipeline_query_fn.h
index 92649082a..f8cee3d7c 100644
--- a/src/include/catalog/pipeline_query_fn.h
+++ b/src/include/catalog/pipeline_query_fn.h
@@ -50,7 +50,9 @@ typedef struct ContQuery
 	int sw_step_ms;
 	uint64 sw_interval_ms;
 	bool is_sw;
-	Oid sw_attno;
+	AttrNumber ttl_attno;
+	AttrNumber sw_attno;
+	int ttl;
 
 	/* for transform */
 	Oid tgfn;
diff --git a/src/include/pipeline/reaper.h b/src/include/pipeline/reaper.h
new file mode 100644
index 000000000..402a4221f
--- /dev/null
+++ b/src/include/pipeline/reaper.h
@@ -0,0 +1,20 @@
+/*-------------------------------------------------------------------------
+ *
+ * reaper.h
+ *
+ * Copyright (c) 2017, PipelineDB
+ *
+ * IDENTIFICATION
+ *    src/include/pipeline/reaper.h
+ *
+ *-------------------------------------------------------------------------
+ */
+#ifndef REAPER_H
+#define REAPER_H
+
+extern int continuous_query_ttl_expiration_batch_size;
+extern int continuous_query_ttl_expiration_threshold;
+
+int DeleteTTLExpiredRows(RangeVar *cvname, RangeVar *matrel);
+
+#endif   /* REAPER_H */
diff --git a/src/include/pipeline/scheduler.h b/src/include/pipeline/scheduler.h
index ddd9ae881..1947c9472 100644
--- a/src/include/pipeline/scheduler.h
+++ b/src/include/pipeline/scheduler.h
@@ -27,6 +27,7 @@ typedef enum
 	Combiner = 0,
 	Worker,
 	Queue,
+	Reaper,
 	Scheduler /* unused */
 } ContQueryProcType;
 
@@ -77,6 +78,7 @@ extern bool continuous_queries_enabled;
 extern int  continuous_query_num_combiners;
 extern int  continuous_query_num_workers;
 extern int  continuous_query_num_queues;
+extern int  continuous_query_num_reapers;
 extern int  continuous_query_queue_mem;
 extern int  continuous_query_max_wait;
 extern int  continuous_query_combiner_work_mem;
@@ -109,6 +111,7 @@ extern pid_t StartContQueryScheduler(void);
 extern void ContinuousQueryCombinerMain(void);
 extern void ContinuousQueryWorkerMain(void);
 extern void ContinuousQueryQueueMain(void);
+extern void ContinuousQueryReaperMain(void);
 
 extern void SignalContQuerySchedulerDropDB(Oid db_oid);
 extern void SignalContQuerySchedulerRefreshDBList(void);
diff --git a/src/include/pipeline/ttl_vacuum.h b/src/include/pipeline/ttl_vacuum.h
deleted file mode 100644
index 90dfe1995..000000000
--- a/src/include/pipeline/ttl_vacuum.h
+++ /dev/null
@@ -1,25 +0,0 @@
-/*-------------------------------------------------------------------------
- *
- * sw_vacuum.h
- *
- *   Support for vacuuming discarded tuples for continuous views with TTLs.
- *
- * Copyright (c) 2013-2016, PipelineDB
- *
- * src/include/pipeline/sw_vacuum.h
- *
- *-------------------------------------------------------------------------
- */
-#ifndef TTL_VACUUM_H
-#define TTL_VACUUM_H
-
-#include "postgres.h"
-#include "catalog/pg_class.h"
-#include "executor/executor.h"
-#include "pgstat.h"
-#include "utils/relcache.h"
-
-extern uint64_t NumTTLExpiredTuples(Oid relid);
-extern void DeleteTTLExpiredTuples(Oid relid);
-
-#endif /* TTL_VACUUM_H */
diff --git a/src/include/utils/pipelinefuncs.h b/src/include/utils/pipelinefuncs.h
index e14306ffd..41082de35 100644
--- a/src/include/utils/pipelinefuncs.h
+++ b/src/include/utils/pipelinefuncs.h
@@ -58,4 +58,6 @@ extern Datum pipeline_flush(PG_FUNCTION_ARGS);
 
 extern Datum set_ttl(PG_FUNCTION_ARGS);
 
+extern Datum ttl_expire(PG_FUNCTION_ARGS);
+
 #endif
diff --git a/src/test/py/test_vacuum.py b/src/test/py/test_vacuum.py
index 9cd121fe3..e3944475e 100644
--- a/src/test/py/test_vacuum.py
+++ b/src/test/py/test_vacuum.py
@@ -9,130 +9,6 @@
 import time
 
 
-DURATION = 2 * 60 # 2 minutes
-
-Stat = namedtuple('Stat', ['rows', 'matrel_rows', 'disk_pages'])
-
-
-def test_disk_spill(pipeline, clean_db):
-  return
-  pipeline.create_stream('test_vacuum_stream', x='int', y='text')
-  pipeline.create_cv(
-    'test_vacuum', '''
-    SELECT x::int, COUNT(DISTINCT y::text)
-    FROM test_vacuum_stream
-    WHERE arrival_timestamp > clock_timestamp() - INTERVAL '5 second'
-    GROUP BY x
-    ''')
-  conn = psycopg2.connect('dbname=pipeline user=%s host=localhost port=%s' %
-                          (getpass.getuser(), pipeline.port))
-  conn.set_isolation_level(psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT)
-  db = conn.cursor()
-
-  # VACUUM FULL and ANALYZE everything so our disk page usage is minimal +
-  # accurate.
-  db.execute('VACUUM FULL')
-  db.execute('ANALYZE')
-
-  stop = False
-
-  def insert():
-    while not stop:
-      values = map(lambda _: (random.randint(0, 20),
-                              random.randint(0, 1000000)),
-                   xrange(1000))
-      pipeline.insert('test_vacuum_stream', ('x', 'y'), values)
-      time.sleep(0.001)
-
-  t = threading.Thread(target=insert)
-  t.start()
-
-  # Wait for SW AV to kick in
-  time.sleep(30)
-
-  def get_stat():
-    rows = pipeline.execute(
-      'SELECT COUNT(*) FROM test_vacuum').first()['count']
-    matrel_rows = pipeline.execute(
-      'SELECT COUNT(*) FROM test_vacuum_mrel').first()['count']
-    disk_pages = pipeline.execute("""
-    SELECT pg_relation_filepath(oid), relpages
-    FROM pg_class WHERE relname = 'test_vacuum_mrel';
-    """).first()['relpages']
-    for r in pipeline.execute("""
-    SELECT relname, relpages
-    FROM pg_class,
-         (SELECT reltoastrelid
-          FROM pg_class
-          WHERE relname = 'test_vacuum_mrel') AS ss
-    WHERE oid = ss.reltoastrelid OR
-          oid = (SELECT indexrelid
-                 FROM pg_index
-                 WHERE indrelid = ss.reltoastrelid)
-    ORDER BY relname;
-    """):
-      disk_pages += r['relpages']
-    for r in pipeline.execute("""
-    SELECT c2.relname, c2.relpages
-    FROM pg_class c, pg_class c2, pg_index i
-    WHERE c.relname = 'test_vacuum_mrel' AND
-          c.oid = i.indrelid AND
-          c2.oid = i.indexrelid
-    ORDER BY c2.relname;
-    """):
-      disk_pages += r['relpages']
-
-    return Stat(rows, matrel_rows, disk_pages)
-
-  start = time.time()
-  stats = []
-
-  pipeline.execute('ANALYZE test_vacuum')
-
-  while time.time() - start < DURATION:
-    stats.append(get_stat())
-    time.sleep(1)
-
-  time.sleep(10)
-  stop = True
-  t.join()
-
-  # We should always see the same number of rows
-  rows = map(lambda s: s.rows, stats)
-  assert len(set(rows)) == 1
-
-  # The number of matrel rows should go up and down
-  matrel_rows = map(lambda s: s.matrel_rows, stats)
-  assert sorted(matrel_rows) != matrel_rows
-
-  # TODO(usmanm): This check fails of CircleCI, so skip for now.
-  if os.environ.get('CIRCLE_PROJECT_USERNAME') is None:
-    # The number of disk pages should also go up and down
-    disk_pages = map(lambda s: s.disk_pages, stats)
-    assert sorted(disk_pages) != disk_pages
-
-  db.execute('ANALYZE')
-  before = get_stat()
-
-  time.sleep(5)
-
-  # Now vacuum while we're not inserting at all and see if it does a major
-  # clean up
-  # We need to VACUUM twice to really *free* up the heap space used by
-  # expired SW tuples. In the first pass, they're marked as deleted and
-  # in the second pass, they're physical space is marked as re-usable.
-  db.execute('VACUUM ANALYZE test_vacuum')
-  db.execute('VACUUM ANALYZE test_vacuum')
-
-  db.execute('ANALYZE')
-  after = get_stat()
-
-  assert after.matrel_rows < before.matrel_rows
-  assert after.disk_pages <= before.disk_pages
-
-  conn.close()
-
-
 def test_concurrent_vacuum_full(pipeline, clean_db):
   pipeline.create_stream('test_vacuum_stream', x='int')
   pipeline.create_cv(
@@ -142,8 +18,8 @@ def test_concurrent_vacuum_full(pipeline, clean_db):
 
   def insert():
     while not stop:
-      values = [(random.randint(0, 1000000), ) for _ in xrange(1000)]
-      pipeline.insert('test_vacuum_stream', ('x', ), values)
+      values = [(random.randint(0, 1000000),) for _ in xrange(1000)]
+      pipeline.insert('test_vacuum_stream', ('x',), values)
       time.sleep(0.01)
 
   threads = [threading.Thread(target=insert) for _ in range(4)]
diff --git a/src/test/regress/expected/sw_vacuum.out b/src/test/regress/expected/sw_expiration.out
similarity index 90%
rename from src/test/regress/expected/sw_vacuum.out
rename to src/test/regress/expected/sw_expiration.out
index 03f3d3829..0f4ed527c 100644
--- a/src/test/regress/expected/sw_vacuum.out
+++ b/src/test/regress/expected/sw_expiration.out
@@ -40,6 +40,12 @@ SELECT pg_sleep(3);
  
 (1 row)
 
+SELECT 0 * ttl_expire('sw_vacuum');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT * FROM sw_vacuum ORDER BY key;
  key | count 
 -----+-------
@@ -48,10 +54,7 @@ SELECT * FROM sw_vacuum ORDER BY key;
 SELECT key, SUM(count) FROM sw_vacuum_mrel GROUP BY key ORDER BY key;
  key | sum 
 -----+-----
- a   |   4
- b   |   4
- c   |   4
-(3 rows)
+(0 rows)
 
 INSERT INTO sw_vacuum_stream (key) VALUES ('a'), ('b'), ('c');
 INSERT INTO sw_vacuum_stream (key) VALUES ('a'), ('b'), ('c');
@@ -66,7 +69,7 @@ SELECT * FROM sw_vacuum ORDER BY key;
 SELECT (SELECT COUNT(*) FROM sw_vacuum) < (SELECT COUNT(*) FROM sw_vacuum_mrel);
  ?column? 
 ----------
- t
+ f
 (1 row)
 
 SELECT DISTINCT key FROM sw_vacuum_mrel ORDER BY key;
@@ -77,7 +80,12 @@ SELECT DISTINCT key FROM sw_vacuum_mrel ORDER BY key;
  c
 (3 rows)
 
-VACUUM sw_vacuum;
+SELECT 0 * ttl_expire('sw_vacuum');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT * FROM sw_vacuum ORDER BY key;
  key | count 
 -----+-------
@@ -113,7 +121,12 @@ SELECT key, SUM(count) FROM sw_vacuum_mrel GROUP BY key ORDER BY key;
  c   |   2
 (3 rows)
 
-VACUUM FULL sw_vacuum;
+SELECT 0 * ttl_expire('sw_vacuum');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT * FROM sw_vacuum ORDER BY key;
  key | count 
 -----+-------
diff --git a/src/test/regress/expected/ttl_vacuum.out b/src/test/regress/expected/ttl_expiration.out
similarity index 92%
rename from src/test/regress/expected/ttl_vacuum.out
rename to src/test/regress/expected/ttl_expiration.out
index 2107f1667..7ea63ff31 100644
--- a/src/test/regress/expected/ttl_vacuum.out
+++ b/src/test/regress/expected/ttl_expiration.out
@@ -56,7 +56,12 @@ SELECT pg_sleep(3);
  
 (1 row)
 
-VACUUM ttl0;
+SELECT 0 * ttl_expire('ttl0');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT x, "$pk" FROM ttl0_mrel ORDER BY ts;
  x | $pk 
 ---+-----
@@ -79,7 +84,12 @@ SELECT pg_sleep(3);
  
 (1 row)
 
-VACUUM FULL ttl0;
+SELECT 0 * ttl_expire('ttl0');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT x, "$pk" FROM ttl0_mrel ORDER BY ts;
  x | $pk 
 ---+-----
@@ -140,7 +150,12 @@ SELECT pg_sleep(6);
  
 (1 row)
 
-VACUUM FULL ttl2;
+SELECT 0 * ttl_expire('ttl2');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT x, count FROM ttl2;
  x | count 
 ---+-------
@@ -166,7 +181,12 @@ SELECT pg_sleep(2);
  
 (1 row)
 
-VACUUM FULL ttl2;
+SELECT 0 * ttl_expire('ttl2');
+ ?column? 
+----------
+        0
+(1 row)
+
 SELECT x, count FROM ttl2;
  x | count 
 ---+-------
@@ -181,7 +201,8 @@ SELECT set_ttl('ttl2', null, null);
 
 INSERT INTO ttl_stream (x) VALUES (2);
 INSERT INTO ttl_stream (x) VALUES (2);
-VACUUM FULL ttl2;
+SELECT 0 * ttl_expire('ttl2');
+ERROR:  continuous view "ttl2" does not have a TTL
 SELECT x, count FROM ttl2;
  x | count 
 ---+-------
diff --git a/src/test/regress/parallel_schedule b/src/test/regress/parallel_schedule
index 8eb42715f..8799a040c 100644
--- a/src/test/regress/parallel_schedule
+++ b/src/test/regress/parallel_schedule
@@ -119,7 +119,7 @@ test: pipeline_stream
 # ----------
 # Another group of parallel tests
 # ----------
-test: sw_vacuum ttl_vacuum
+test: sw_expiration ttl_expiration
 
 # ----------
 # Another group of parallel tests
diff --git a/src/test/regress/sql/bucket_agg.sql b/src/test/regress/sql/bucket_agg.sql
index 9e691df2a..a27233e30 100644
--- a/src/test/regress/sql/bucket_agg.sql
+++ b/src/test/regress/sql/bucket_agg.sql
@@ -61,3 +61,5 @@ GROUP BY g ORDER BY g;
 
 DROP CONTINUOUS VIEW bucket2;
 DROP STREAM bucket_stream;
+
+DELETE FROM v2_mrel WHERE "$pk" IN (SELECT "$pk" FROM public.v2_mrel WHERE ts < now() - interval '1 seconds' FOR UPDATE SKIP LOCKED);
\ No newline at end of file
diff --git a/src/test/regress/sql/sw_vacuum.sql b/src/test/regress/sql/sw_expiration.sql
similarity index 93%
rename from src/test/regress/sql/sw_vacuum.sql
rename to src/test/regress/sql/sw_expiration.sql
index 0afe3d3eb..e4b2b7047 100644
--- a/src/test/regress/sql/sw_vacuum.sql
+++ b/src/test/regress/sql/sw_expiration.sql
@@ -17,6 +17,7 @@ SELECT (SELECT COUNT(*) FROM sw_vacuum) < (SELECT COUNT(*) FROM sw_vacuum_mrel);
 SELECT DISTINCT key FROM sw_vacuum_mrel ORDER BY key;
 
 SELECT pg_sleep(3);
+SELECT 0 * ttl_expire('sw_vacuum');
 
 SELECT * FROM sw_vacuum ORDER BY key;
 SELECT key, SUM(count) FROM sw_vacuum_mrel GROUP BY key ORDER BY key;
@@ -28,7 +29,7 @@ SELECT * FROM sw_vacuum ORDER BY key;
 SELECT (SELECT COUNT(*) FROM sw_vacuum) < (SELECT COUNT(*) FROM sw_vacuum_mrel);
 SELECT DISTINCT key FROM sw_vacuum_mrel ORDER BY key;
 
-VACUUM sw_vacuum;
+SELECT 0 * ttl_expire('sw_vacuum');
 SELECT * FROM sw_vacuum ORDER BY key;
 SELECT key, SUM(count) FROM sw_vacuum_mrel GROUP BY key ORDER BY key;
 
@@ -37,7 +38,7 @@ SELECT pg_sleep(3);
 SELECT * FROM sw_vacuum ORDER BY key;
 SELECT key, SUM(count) FROM sw_vacuum_mrel GROUP BY key ORDER BY key;
 
-VACUUM FULL sw_vacuum;
+SELECT 0 * ttl_expire('sw_vacuum');
 SELECT * FROM sw_vacuum ORDER BY key;
 SELECT key, SUM(count) FROM sw_vacuum_mrel GROUP BY key ORDER BY key;
 
diff --git a/src/test/regress/sql/ttl_vacuum.sql b/src/test/regress/sql/ttl_expiration.sql
similarity index 95%
rename from src/test/regress/sql/ttl_vacuum.sql
rename to src/test/regress/sql/ttl_expiration.sql
index 3f0041818..c1aed624d 100644
--- a/src/test/regress/sql/ttl_vacuum.sql
+++ b/src/test/regress/sql/ttl_expiration.sql
@@ -44,7 +44,8 @@ INSERT INTO ttl_stream (x) VALUES (2);
 SELECT x, "$pk" FROM ttl0_mrel ORDER BY ts;
 
 SELECT pg_sleep(3);
-VACUUM ttl0;
+
+SELECT 0 * ttl_expire('ttl0');
 
 SELECT x, "$pk" FROM ttl0_mrel ORDER BY ts;
 
@@ -55,7 +56,7 @@ INSERT INTO ttl_stream (x) VALUES (2);
 SELECT x, "$pk" FROM ttl0_mrel ORDER BY ts;
 
 SELECT pg_sleep(3);
-VACUUM FULL ttl0;
+SELECT 0 * ttl_expire('ttl0');
 
 SELECT x, "$pk" FROM ttl0_mrel ORDER BY ts;
 SELECT x, "$pk" FROM ttl1_mrel ORDER BY ts;
@@ -90,7 +91,7 @@ INSERT INTO ttl_stream (x) VALUES (2);
 SELECT x, count FROM ttl2;
 
 SELECT pg_sleep(6);
-VACUUM FULL ttl2;
+SELECT 0 * ttl_expire('ttl2');
 
 SELECT x, count FROM ttl2;
 
@@ -102,7 +103,7 @@ INSERT INTO ttl_stream (x) VALUES (2);
 SELECT x, count FROM ttl2;
 
 SELECT pg_sleep(2);
-VACUUM FULL ttl2;
+SELECT 0 * ttl_expire('ttl2');
 
 SELECT x, count FROM ttl2;
 
@@ -111,7 +112,7 @@ SELECT set_ttl('ttl2', null, null);
 
 INSERT INTO ttl_stream (x) VALUES (2);
 INSERT INTO ttl_stream (x) VALUES (2);
-VACUUM FULL ttl2;
+SELECT 0 * ttl_expire('ttl2');
 
 SELECT x, count FROM ttl2;
 
