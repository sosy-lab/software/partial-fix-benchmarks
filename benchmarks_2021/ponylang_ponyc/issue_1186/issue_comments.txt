Segfault in String.find() (maybe linked to #1146)
note, the segfault happens only when there's no match in find. I guess the try...else...end block is suspicious.

If I use a lambda instead of a callable object it works fine:

``` pony
actor Main
  let bench: PonyBench
  new create(env: Env) =>  
    bench = PonyBench(env)
    bench[ISize]("builtin", lambda(): ISize => try "x".find("y") else ISize(-1) end end)
```

the error is here : https://github.com/ponylang/ponyc/blob/master/packages/ponybench/_bench.pony#L15

I suspected `DoNotOptimise` at first, but a simple call `f()` segfaults too.

I Couldn't find a code example that fails without ponybench. It has to do with the way the callable expression is passed around internally.

I ran this through lldb and got the following output:

```
Process 11098 launched: '/home/theodus/stuff/test/test' (x86_64)
Process 11098 stopped
* thread #2: tid = 11102, 0x0000000000000001, name = 'test', stop reason = signal SIGSEGV: invalid address (fault address: 0x1)
    frame #0: 0x0000000000000001
error: memory read failed for 0x0
```

@Theodus can you run with a debug compiler in debug mode and put the backtrace after the SIGSEGV in here?

Here is the backtrace:

```
* thread #2: tid = 23030, 0x0000000000000001, name = 'test', stop reason = signal SIGSEGV: invalid address (fault address: 0x1)
  * frame #0: 0x0000000000000001
    frame #1: 0x0000000000402fe6 test`ponybench__Bench_ISize_val_Dispatch + 182
```

Here is the output with a breakpoint at `ponybench__Bench_ISize_val_Dispatch`:

```
Process 23026 exited with status = 9 (0x00000009) 
Process 23179 launched: '/home/theodus/stuff/test/test' (x86_64)
Process 23179 stopped
* thread #2: tid = 23183, 0x0000000000402f30 test`ponybench__Bench_ISize_val_Dispatch, name = 'test', stop reason = breakpoint 1.1
    frame #0: 0x0000000000402f30 test`ponybench__Bench_ISize_val_Dispatch
test`ponybench__Bench_ISize_val_Dispatch:
->  0x402f30 <+0>: pushq  %rbp
    0x402f31 <+1>: pushq  %r15
    0x402f33 <+3>: pushq  %r14
    0x402f35 <+5>: pushq  %r13
```

Here is the output with the breakpoint when compiled in debug mode:

```
Process 23482 launched: '/home/theodus/stuff/test/test' (x86_64)
Process 23482 stopped
* thread #2: tid = 23487, 0x0000000000402c00 test`ponybench__Bench_ISize_val_Dispatch, name = 'test', stop reason = breakpoint 1.1
    frame #0: 0x0000000000402c00 test`ponybench__Bench_ISize_val_Dispatch
test`ponybench__Bench_ISize_val_Dispatch:
->  0x402c00 <+0>: subq   $0x58, %rsp
    0x402c04 <+4>: movl   0x4(%rdx), %eax
    0x402c07 <+7>: testl  %eax, %eax
    0x402c09 <+9>: movq   %rsi, 0x50(%rsp)
```

Is that from a debug person of the compiler? It looks like it came from a release version.

I'm using the command `/usr/local/ponyc/build/debug/ponyc`. It was compiled using `make config=debug`. Is there something else that I need to do in order to make a debug version of the compiler?

create a debug version of the binary you are running.  

ponyc --debug

> ```
> * frame #0: 0x0000000000000001
> ```

We're jumping to a very wrong address here. I'll look into it later.

A debug binary runs without a segfault.

Here's the simplest failing code I could write ( uses only builtin )

``` pony
class BuiltinFinder
  let _n: String
  let _h: String
  new val create(needle: String val, haystack: String val) =>
    _n = needle
    _h = haystack

  fun box apply(): ISize => try _h.find(_n) else ISize(-1) end

actor Runner
  let _env: Env

  new create(env: Env) =>
    _env = env

  be apply(f: {(): ISize} val) =>
    _env.out.print("start")
    f()
    _env.out.print("done")

actor Main
  new create(env: Env) =>
    let l: {(): ISize } val = lambda(): ISize val => try "x".find("y") else -1 end end
    let f = BuiltinFinder("x", "y")
    let r = Runner(env)
    r(l)
    r(f)
```

The lambda works well while `apply()` segfaults. Output:

```
start
done
start
zsh: segmentation fault ./tests
```

Hmm, ok so... @Theodus that tells us something at least.

This doesn't segfault for me with LLVM 3.6 but it does with 3.8

It looks like something in LLVM really doesn't like `llvm.invariant.start`, and that thing is reacting by stomping landing pads for exception handling. Maybe we're using `llvm.invariant.start` wrong but I can't find anything in the documentation. I think I'll just remove it from code generation, it shouldn't have any noticeable effect on optimised programs since we have a quite precise type-based alias analysis.

:/ the code in https://github.com/ponylang/ponyc/issues/1186#issuecomment-244120040 still segfaults

```
ponyc --version  
0.3.0-14-gd8c6c55 [release]
```

And, i checked, it runs fine with --debug

Sorry for that, I thought I had reduced your example to a more minimal case but apparently there are multiple factors here. I can't find anything wrong in the generated IR. Since it works with LLVM 3.6 maybe it's a bug within LLVM?

If somebody else wants to look into it, the bug is sometimes appearing during `pony_throw` (when raising an exception and unwinding the stack). At some point, the saved instruction pointer for the return address of the function that called `pony_throw` is modified to `0x1`.

So, here's the workaround, at the moment if you're stuck: compile with llvm 3.6 

```
make LLVM_CONFIG=llvm-config-3.6 LLVM_LINK=llvm-link-3.6 LLVM_OPT=opt-3.6
sudo make install
```

or ( depending on your distribution)

```
make LLVM_CONFIG=llvm-config36 LLVM_LINK=llvm-link36 LLVM_OPT=opt36
make install
```

Here's a reduced example if it helps:

``` pony
actor Main
  new create(env: Env) => Foo.test()

actor Foo
  let h: String = ""

  be test() => try h.find("x") end
```

There's no segfault if any of these are true:
- If `h` is not a field of `Foo`
- If we throw our own error manually: `try h.find("x"); error end`
- If we use a different method to get the error: `try h.i32() end`
- If we define an `Env` parameter on `test` and pass `env` in. _(Don't need to use `env`, just pass it, but it can't be something like `USize` instead.)_

---

Furthermore, this even simpler example results in `Illegal instruction`:

``` pony
actor Main
  new create(env: Env) => Foo.test()

actor Foo
  be test() => try error end
```

As another data point, there is no segfault if `h` is assigned by calling the `String` constructor rather than using a string literal.

Using Clang instead of Ponyc for the machine code generation solves the problem. That means the issue is on our side. I'll keep looking.

Some additional data.

``` pony
actor @Foo
  be test() => try error end
```

Building this actor as a library and sending a message for `test` from C results in `_Unwind_RaiseException` failing with error code `_URC_END_OF_STACK`. That means the exception handling code reached the end of the call stack without finding a suitable landing pad.

It also works with LLVM 3.7, so the breaking change is somewhere in LLVM 3.8.

I just tested this out on OSX with LLVM3.8.

Does not happen on OSX. 

@Praetonus did you test with LLVM 3.8.0 or 3.8.1 ?

@Theodus do you have 3.8.1 installed now? can you test this and see if going from 3.8.0 to 3.8.1 fixes this?

With LLVM 3.8.1 it still works when compiled with the `-d` flag, otherwise it segfaults.

So no change @Theodus ?

@SeanTAllen no change

@SeanTAllen I used LLVM 3.8.1.

I've made some progress on this. The assembly code generated by ponyc and by clang (with IR from ponyc) is different.

For @Perelandric's minimal example we get this from ponyc

```asm
Foo_Dispatch:
        cmpl    $1, 4(%rdx)
        je      .LBB6_1
.LBB6_4:
        retq
.LBB6_1:
        pushq   %rax
        callq   pony_throw
        addq    $8, %rsp
.LBB6_3:
        jmp     .LBB6_4
```

and this from clang

```asm
Foo_Dispatch:
        subq    $40, %rsp
        cmpl    $1, 4(%rdx)
        movq    %rsi, 32(%rsp)          # 8-byte Spill
        movq    %rdi, 24(%rsp)          # 8-byte Spill
        jne     .LBB3_2
        addq    $40, %rsp
        retq
.LBB3_2:
        callq   pony_throw
        jmp     .LBB3_3
.LBB3_3:
.LBB3_4:
        movq    %rdx, 16(%rsp)          # 8-byte Spill
        movq    %rax, 8(%rsp)           # 8-byte Spill
        addq    $40, %rsp
        retq
```

The thing is, when we exit from `pony_throw`, we skip the instruction right after it. So the code from ponyc doesn't restore the stack correctly and jumps to a garbage address.

I still have no idea why the code is different but it seems that the problem can be solved by shifting the landing pad by one instruction. I don't know why the landing pad is placed that way, maybe this is the bug.
Update: compiling from IR to assembly with clang with `-O3` gives the same code (and the same crash) as ponyc. The bug is definitely coming from the landing pad location.
Apparently it only occurs in small functions with conditional branching (tested only on x86_64). Longer functions or functions without branching land correctly right after `pony_throw`.
I've looked at the personality function for C++ in GCC and it does the same thing we're doing for the relevant part.

The problem appears to be in the LSDA data, the landing pad offset (retrieved at `lsda.c:232`) can be off by one instruction.

I'm a bit lost at that point. A bug in the LSDA generation seems unlikely but I don't really see what else could be wrong.
I'm able to fix the crash by modifying the LSDA data generated in the assembly. It really looks like a LLVM bug.

Since only small functions seem affected, I think we can fix the problem at our level by adding some no-op boilerplate around exception sources. I'll ask on the LLVM mailing list about all of this to see if a bug report should be opened on their side.
@Praetonus any status on this?
@SeanTAllen No, I've forgotten to open a bug report on LLVM's tracker. I'll do it in the next coming days.
Updated code for this based on lambda change:

```pony
class BuiltinFinder
  let _n: String
  let _h: String
  new val create(needle: String val, haystack: String val) =>
    _n = needle
    _h = haystack

  fun box apply(): ISize => try _h.find(_n) else ISize(-1) end

actor Runner
  let _env: Env

  new create(env: Env) =>
    _env = env

  be apply(f: {(): ISize} val) =>
    _env.out.print("start")
    f()
    _env.out.print("done")

actor Main
  new create(env: Env) =>
    let l: {(): ISize } val =  {(): ISize val => try "x".find("y") else -1 end }
    let f = BuiltinFinder("x", "y")
    let r = Runner(env)
    r(l)
    r(f)
```
@Praetonus did you open the issue for this on the LLVM tracker? If yes, can you reference it here?
@Praetonus says he is going to open the LLVM bug report this week.
I think I found the root of the issue. LLVM marks the crashing function as `nounwind`, which results in the landing pad address being incorrect. If I remove the `nounwind` attribute manually, the landing pad is generated correctly. I'll sum up all the info gathered in this thread and open that long overdue bug report.
LLVM bug opened: https://bugs.llvm.org/show_bug.cgi?id=36513
The bug has been fixed, the fix will be part of the next LLVM release.