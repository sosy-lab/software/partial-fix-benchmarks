diff --git a/CHANGELOG.md b/CHANGELOG.md
index 1e7edbf08b..34368bf958 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -11,6 +11,7 @@ All notable changes to the Pony compiler and standard library will be documented
 - Back pressure notifications now given when encountered while sending data during `TCPConnection` pending writes
 - Improve efficiency of muted TCPConnection on non Windows platforms (PR #1477)
 - Compiler assertion failure during type checking
+- Runtime memory allocator bug
 
 ### Added
 
diff --git a/src/common/platform.h b/src/common/platform.h
index fc14f6eb80..a000a5b263 100644
--- a/src/common/platform.h
+++ b/src/common/platform.h
@@ -230,12 +230,21 @@ inline uint32_t __pony_ctz(uint32_t x)
   return i;
 }
 
+#  ifdef PLATFORM_IS_ILP32
+inline uint32_t __pony_ctzl(uint32_t x)
+{
+  DWORD i = 0;
+  _BitScanForward(&i, x);
+  return i;
+}
+#  else
 inline uint64_t __pony_ctzl(uint64_t x)
 {
   DWORD i = 0;
   _BitScanForward64(&i, x);
   return i;
 }
+#  endif
 
 inline uint32_t __pony_clz(uint32_t x)
 {
@@ -244,12 +253,21 @@ inline uint32_t __pony_clz(uint32_t x)
   return 31 - i;
 }
 
+#  ifdef PLATFORM_IS_ILP32
+inline uint32_t __pony_clzl(uint32_t x)
+{
+  DWORD i = 0;
+  _BitScanReverse(&i, x);
+  return 31 - i;
+}
+#  else
 inline uint64_t __pony_clzl(uint64_t x)
 {
   DWORD i = 0;
   _BitScanReverse64(&i, x);
   return 63 - i;
 }
+#  endif
 
 #endif
 
diff --git a/src/libponyrt/ds/fun.c b/src/libponyrt/ds/fun.c
index 7c6ca3b2b9..09da215cf6 100644
--- a/src/libponyrt/ds/fun.c
+++ b/src/libponyrt/ds/fun.c
@@ -127,7 +127,7 @@ size_t ponyint_next_pow2(size_t i)
     i = 32;
   else
 #  endif
-    i = __pony_clz(i);
+    i = __pony_clzl(i);
   return 1 << (i == 0 ? 0 : 32 - i);
 #else
   i--;
diff --git a/src/libponyrt/mem/pool.c b/src/libponyrt/mem/pool.c
index 413e437a53..2392ed47b2 100644
--- a/src/libponyrt/mem/pool.c
+++ b/src/libponyrt/mem/pool.c
@@ -19,15 +19,6 @@
 #include <valgrind/helgrind.h>
 #endif
 
-/* Because of the way free memory is reused as its own linked list container,
- * the minimum allocation size is 32 bytes.
- */
-
-#define POOL_MAX_BITS 20
-#define POOL_MAX (1 << POOL_MAX_BITS)
-#define POOL_MIN (1 << POOL_MIN_BITS)
-#define POOL_COUNT (POOL_MAX_BITS - POOL_MIN_BITS + 1)
-
 /// Allocations this size and above are aligned on this size. This is needed
 /// so that the pagemap for the heap is aligned.
 #define POOL_ALIGN_INDEX (POOL_ALIGN_BITS - POOL_MIN_BITS)
@@ -700,7 +691,17 @@ size_t ponyint_pool_index(size_t size)
   if(size > POOL_MAX)
     return POOL_COUNT;
 
-  return ((size_t)64 - __pony_clzl(size)) - POOL_MIN_BITS;
+  size_t bits;
+#ifdef PLATFORM_IS_ILP32
+  bits = 31;
+#else
+  bits = 63;
+#endif
+
+  size_t index = bits - __pony_clzl(size);
+  if(index != (size_t)__pony_ctzl(size))
+    index++;
+  return index - POOL_MIN_BITS;
 }
 
 size_t ponyint_pool_size(size_t index)
diff --git a/src/libponyrt/mem/pool.h b/src/libponyrt/mem/pool.h
index fc102423ff..06741f2c1a 100644
--- a/src/libponyrt/mem/pool.h
+++ b/src/libponyrt/mem/pool.h
@@ -8,9 +8,18 @@
 
 PONY_EXTERN_C_BEGIN
 
+/* Because of the way free memory is reused as its own linked list container,
+ * the minimum allocation size is 32 bytes.
+ */
+
 #define POOL_MIN_BITS 5
+#define POOL_MAX_BITS 20
 #define POOL_ALIGN_BITS 10
+
+#define POOL_MIN (1 << POOL_MIN_BITS)
+#define POOL_MAX (1 << POOL_MAX_BITS)
 #define POOL_ALIGN (1 << POOL_ALIGN_BITS)
+#define POOL_COUNT (POOL_MAX_BITS - POOL_MIN_BITS + 1)
 
 __pony_spec_malloc__(void* ponyint_pool_alloc(size_t index));
 void ponyint_pool_free(size_t index, void* p);
diff --git a/test/libponyrt/mem/pool.cc b/test/libponyrt/mem/pool.cc
index e0b81b74a5..1e2c358423 100644
--- a/test/libponyrt/mem/pool.cc
+++ b/test/libponyrt/mem/pool.cc
@@ -70,3 +70,36 @@ TEST(Pool, LargeAlloc)
   ASSERT_TRUE(p != NULL);
   ponyint_pool_free_size(1 << 20, p);
 }
+
+TEST(Pool, Index)
+{
+  size_t index = ponyint_pool_index(1);
+  ASSERT_TRUE(index == 0);
+
+  index = ponyint_pool_index(31);
+  ASSERT_TRUE(index == 0);
+
+  index = ponyint_pool_index(32);
+  ASSERT_TRUE(index == 0);
+
+  index = ponyint_pool_index(33);
+  ASSERT_TRUE(index == 1);
+
+  index = ponyint_pool_index(63);
+  ASSERT_TRUE(index == 1);
+
+  index = ponyint_pool_index(64);
+  ASSERT_TRUE(index == 1);
+
+  index = ponyint_pool_index(65);
+  ASSERT_TRUE(index == 2);
+
+  index = ponyint_pool_index(POOL_MAX - 1);
+  ASSERT_TRUE(index == POOL_COUNT - 1);
+
+  index = ponyint_pool_index(POOL_MAX);
+  ASSERT_TRUE(index == POOL_COUNT - 1);
+
+  index = ponyint_pool_index(POOL_MAX + 1);
+  ASSERT_TRUE(index == POOL_COUNT);
+}
