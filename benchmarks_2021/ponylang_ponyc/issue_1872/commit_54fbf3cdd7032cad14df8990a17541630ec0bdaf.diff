diff --git a/CHANGELOG.md b/CHANGELOG.md
index 8e8a8d195c..f837a482e0 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -8,7 +8,7 @@ All notable changes to the Pony compiler and standard library will be documented
 
 - Compiler error instead of crash for invalid this-dot reference in a trait. ([PR #1879](https://github.com/ponylang/ponyc/pull/1879))
 - Compiler error instead of crash for too few args to constructor in case pattern. ([PR #1880](https://github.com/ponylang/ponyc/pull/1880))
-
+- Pony runtime hashmap bug that resulted in issues [#1483](https://github.com/ponylang/ponyc/issues/1483), [#1781](https://github.com/ponylang/ponyc/issues/1781), and [#1872](https://github.com/ponylang/ponyc/issues/1872). ([PR #1886](https://github.com/ponylang/ponyc/pull/1886)) 
 
 ### Added
 
diff --git a/src/libponyrt/ds/hash.c b/src/libponyrt/ds/hash.c
index 13fb003256..6b911adb9a 100644
--- a/src/libponyrt/ds/hash.c
+++ b/src/libponyrt/ds/hash.c
@@ -114,7 +114,6 @@ static void resize(hashmap_t* map, cmp_fn cmp, alloc_fn alloc,
 static size_t optimize_item(hashmap_t* map, alloc_fn alloc,
   free_size_fn fr, cmp_fn cmp, size_t old_index)
 {
-
   size_t mask = map->size - 1;
 
   size_t h = map->buckets[old_index].hash;
@@ -133,17 +132,45 @@ static size_t optimize_item(hashmap_t* map, alloc_fn alloc,
     ib_index = index >> HASHMAP_BITMAP_TYPE_BITS;
     ib_offset = index & HASHMAP_BITMAP_TYPE_MASK;
 
-    // don't need to check probe counts for filled buckets because
-    // earlier items are guaranteed to have a lower probe count
-    // than us and we cannot displace them
-    // found an earlier empty bucket so move item
+    // Reconstute our invariants
+    //
+    // During the process of removing "dead" items from our hash, it is
+    // possible to violate the invariants of our map. We will now proceed to
+    // detect and fix those violations.
+    //
+    // There are 2 possible invariant violations that we need to handle. One
+    // is fairly simple, the other rather more complicated
+    //
+    // 1. We are no longer at our natural hash location AND that location is
+    // empty. If that violation were allowed to continue then when searching
+    // later, we'd find the empty bucket and stop looking for this hashed item.
+    // Fixing this violation is handled by our `if` statement
+    //
+    // 2. Is a bit more complicated and is handled in our `else`
+    // statement. It's possible while restoring invariants for our most
+    // important invariant to get violated. That is, that items with a lower
+    // probe count should appear before those with a higher probe count.
+    // The else statement checks for this condition and repairs it.
     if((map->item_bitmap[ib_index] & ((bitmap_t)1 << ib_offset)) == 0)
     {
       ponyint_hashmap_clearindex(map, old_index);
       ponyint_hashmap_putindex(map, entry, h, cmp, alloc, fr, index);
       return 1;
     }
-
+    else
+    {
+      size_t item_probe_length =
+        get_probe_length(map, h, index, mask);
+      size_t there_probe_length =
+        get_probe_length(map, map->buckets[index].hash, index, mask);
+
+      if (item_probe_length > there_probe_length)
+      {
+        ponyint_hashmap_clearindex(map, old_index);
+        ponyint_hashmap_putindex(map, entry, h, cmp, alloc, fr, index);
+        return 1;
+      }
+    }
     // find next bucket index
     index = (index + 1) & mask;
   }
