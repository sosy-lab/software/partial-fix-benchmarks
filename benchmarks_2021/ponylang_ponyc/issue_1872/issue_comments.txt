Occasional segfault soon-after-startup for UDP client
Adding to this: if the printed counter advances beyond around 30k, I haven't seen it crash after that (and have run it to 600k+).  However, if you ^C the process and restart it, it fails about 5-10% of the time.  One doesn't need to restart or otherwise touch the reflection server once it's running, the bug is entirely triggered by the client.
@Cloven which segfaults, the client or the server?
Client only.
@Cloven how many CPUs does your machine have?
@Cloven also what type of CPU? Info about the type of Apple computer might be helpful.
@Cloven the pause is most likely the OSX giving something else access to the CPU, i get it without a crash from time to time and it appears to be the client being momentarily paused by OSX ( from what I can see).

I'm on 10.11.6 
Mid 2015 Macbook Pro.
LLVM 3.9.1
Same ponyc version you are using
I have 4 cpus so both the server and the client are running with 4 ponythreads.

I was about to hit comment on this as I had run about 100 times with no segfault but I just got one. No pause though and it was up to 67843 when it happened.
@Cloven can you build the client using `--debug` and see if you still get the problem?
I was able to recreate in the debugger. This is going to look familiar to some folks:

```
Process 29460 stopped
* thread #2: tid = 0x2d9c10, 0x000000010001232d client`ponyint_gc_release + 157, stop reason = EXC_BAD_ACCESS (code=1, address=0x8)
    frame #0: 0x000000010001232d client`ponyint_gc_release + 157
client`ponyint_gc_release:
->  0x10001232d <+157>: subq   %rcx, 0x8(%rax)
    0x100012331 <+161>: cmpq   $0x0, 0x18(%r14)
    0x100012336 <+166>: je     0x1000123ab               ; <+283>
    0x100012338 <+168>: movq   0x20(%r14), %rcx
(lldb) bt
* thread #2: tid = 0x2d9c10, 0x000000010001232d client`ponyint_gc_release + 157, stop reason = EXC_BAD_ACCESS (code=1, address=0x8)
  * frame #0: 0x000000010001232d client`ponyint_gc_release + 157
    frame #1: 0x000000010000b5a7 client`handle_message + 263
    frame #2: 0x0000000100016450 client`run_thread + 720
    frame #3: 0x00007fff9249099d libsystem_pthread.dylib`_pthread_body + 131
    frame #4: 0x00007fff9249091a libsystem_pthread.dylib`_pthread_start + 168
    frame #5: 0x00007fff9248e351 libsystem_pthread.dylib`thread_start + 13
(lldb)
```

That box is a 2012 macbook air (dual-core 2.0GHz Intel Core i7 (Turbo Boost
up to 3.2GHz) with 4MB shared L3 cache) with 8G of RAM.  I'll build with
debug when I can.  It's interesting that you're getting the bug more rarely
than I am.

On Thu, Apr 27, 2017 at 4:14 PM, Sean T Allen <notifications@github.com>
wrote:

> @Cloven <https://github.com/Cloven> can you build the client using --debug
> and see if you still get the problem?
>
> —
> You are receiving this because you were mentioned.
> Reply to this email directly, view it on GitHub
> <https://github.com/ponylang/ponyc/issues/1872#issuecomment-297864449>,
> or mute the thread
> <https://github.com/notifications/unsubscribe-auth/AAeOrxZ6nKcBobqAlb3wQ1hFHkK-Qn5xks5r0SFVgaJpZM4NK23S>
> .
>

I seem to be able to make this happen far more consistently with 2 threads rather than 4
Different segfault this time and yeah, running with 2 ponythreads seems to be the key to easily reproducing for me:

```
Process 32252 stopped
* thread #2: tid = 0x2ddce5, 0x0000000100015709 client`ponyint_gc_release(gc=0x0000000120fffca8, aref=0x0000000120f44e80) + 201 at gc.c:683, stop reason = EXC_BAD_ACCESS (code=1, address=0x8)
    frame #0: 0x0000000100015709 client`ponyint_gc_release(gc=0x0000000120fffca8, aref=0x0000000120f44e80) + 201 at gc.c:683
   680 	    void* p = obj->address;
   681 	    object_t* obj_local = ponyint_objectmap_getobject(&gc->local, p, &index);
   682
-> 683 	    pony_assert(obj_local->rc >= obj->rc);
   684 	    obj_local->rc -= obj->rc;
   685 	  }
   686
(lldb) bt
warning: could not load any Objective-C class information. This will significantly reduce the quality of type information available.
* thread #2: tid = 0x2ddce5, 0x0000000100015709 client`ponyint_gc_release(gc=0x0000000120fffca8, aref=0x0000000120f44e80) + 201 at gc.c:683, stop reason = EXC_BAD_ACCESS (code=1, address=0x8)
  * frame #0: 0x0000000100015709 client`ponyint_gc_release(gc=0x0000000120fffca8, aref=0x0000000120f44e80) + 201 at gc.c:683
    frame #1: 0x000000010000c0ce client`handle_message(ctx=0x0000000108fff9c8, actor=0x0000000120fffc00, msg=0x0000000120fbba80) + 222 at actor.c:68
    frame #2: 0x000000010000be7b client`ponyint_actor_run(ctx=0x0000000108fff9c8, actor=0x0000000120fffc00, batch=100) + 283 at actor.c:158
    frame #3: 0x000000010001e1d8 client`run(sched=0x0000000108fff980) + 168 at scheduler.c:287
    frame #4: 0x000000010001df49 client`run_thread(arg=0x0000000108fff980) + 57 at scheduler.c:338
    frame #5: 0x00007fff9249099d libsystem_pthread.dylib`_pthread_body + 131
    frame #6: 0x00007fff9249091a libsystem_pthread.dylib`_pthread_start + 168
    frame #7: 0x00007fff9248e351 libsystem_pthread.dylib`thread_start + 13
(lldb) p obj->rc
(size_t) $0 = 1
(lldb) p obj_local->rc
error: Couldn't apply expression side effects : Couldn't dematerialize a result variable: couldn't read its memory
```

I added an assert to verify and `obj_local` is indeed NULL.
And another slightly different:

```
src/libponyrt/gc/gc.c:163: recv_local_object: Assertion `obj != NULL` failed.

Backtrace:
  0   client                              0x000000010001d421 ponyint_assert_fail + 161
  1   client                              0x00000001000143ad recv_local_object + 125
  2   client                              0x00000001000142f9 ponyint_gc_recvobject + 137
  3   client                              0x0000000100017140 pony_traceunknown + 96
  4   client                              0x0000000100001bde net_UDPSocket_Dispatch + 286
  5   client                              0x000000010000bdfb ponyint_actor_run + 283
  6   client                              0x000000010001e188 run + 168
  7   client                              0x000000010001def9 run_thread + 57
  8   libsystem_pthread.dylib             0x00007fff9249099d _pthread_body + 131
  9   libsystem_pthread.dylib             0x00007fff9249091a _pthread_body + 0
  10  libsystem_pthread.dylib             0x00007fff9248e351 thread_start + 13
Process 32707 stopped
* thread #2: tid = 0x2df049, 0x00007fff9a257f06 libsystem_kernel.dylib`__pthread_kill + 10, stop reason = signal SIGABRT
    frame #0: 0x00007fff9a257f06 libsystem_kernel.dylib`__pthread_kill + 10
libsystem_kernel.dylib`__pthread_kill:
->  0x7fff9a257f06 <+10>: jae    0x7fff9a257f10            ; <+20>
    0x7fff9a257f08 <+12>: movq   %rax, %rdi
    0x7fff9a257f0b <+15>: jmp    0x7fff9a2527cd            ; cerror_nocancel
    0x7fff9a257f10 <+20>: retq
(lldb) bt
warning: could not load any Objective-C class information. This will significantly reduce the quality of type information available.
* thread #2: tid = 0x2df049, 0x00007fff9a257f06 libsystem_kernel.dylib`__pthread_kill + 10, stop reason = signal SIGABRT
  * frame #0: 0x00007fff9a257f06 libsystem_kernel.dylib`__pthread_kill + 10
    frame #1: 0x00007fff924934ec libsystem_pthread.dylib`pthread_kill + 90
    frame #2: 0x00007fff889db6df libsystem_c.dylib`abort + 129
    frame #3: 0x000000010001d513 client`ponyint_assert_fail(expr="obj != NULL", file="src/libponyrt/gc/gc.c", line=163, func="recv_local_object") + 403 at ponyassert.c:60
    frame #4: 0x00000001000143ad client`recv_local_object(ctx=0x0000000108fff848, p=0x0000000120e35580, t=0x000000010002c690, mutability=1) + 125 at gc.c:163
    frame #5: 0x00000001000142f9 client`ponyint_gc_recvobject(ctx=0x0000000108fff848, p=0x0000000120e35580, t=0x000000010002c690, mutability=1) + 137 at gc.c:457
    frame #6: 0x0000000100017140 client`pony_traceunknown(ctx=0x0000000108fff848, p=0x0000000120e35580, m=1) + 96 at trace.c:117
    frame #7: 0x0000000100001bde client`net_UDPSocket_Dispatch + 286
    frame #8: 0x000000010000c139 client`handle_message(ctx=0x0000000108fff848, actor=0x0000000118fffc00, msg=0x0000000120efaec0) + 457 at actor.c:103
    frame #9: 0x000000010000bdfb client`ponyint_actor_run(ctx=0x0000000108fff848, actor=0x0000000118fffc00, batch=100) + 283 at actor.c:158
    frame #10: 0x000000010001e188 client`run(sched=0x0000000108fff800) + 168 at scheduler.c:287
    frame #11: 0x000000010001def9 client`run_thread(arg=0x0000000108fff800) + 57 at scheduler.c:338
    frame #12: 0x00007fff9249099d libsystem_pthread.dylib`_pthread_body + 131
    frame #13: 0x00007fff9249091a libsystem_pthread.dylib`_pthread_start + 168
    frame #14: 0x00007fff9248e351 libsystem_pthread.dylib`thread_start + 13
```
with 2 ponythreads, debug compiler, debug client I can seemingly reproduce all the time.
It appears that anytime this happens, we've recently gone through this branch in `hash.c` search:

```c
    if(p_length >
        (oi_p_length = get_probe_length(map, elem_hash, index, mask))) {
      // our probe length is greater than the elements probe length
      // we would normally have swapped so return this position
      *pos = index;
      *probe_length = p_length;
      *oi_probe_length = oi_p_length;
      return NULL;
    }
```
big question: is there a bug in the hash implementation, are we doing a double free, or are we deleting from the object map early?
there's a bug in hash.c

So what I am regularly seeing and I need @dipinhora's assistance with:

The map size is always 64.
The index that we start from is always near the map size (60,61 etc)
The item we want is in the map. It seems always 2 away from the index we start at:

for example, index is 61
the value we want (for example 8726618367696014013) is at bucket 63.

get_probe_length(map, elem_hash, index, mask) returns a number lower than p_length before we get to the value in the bucket (in the above case after the index 62 lookup) so we never get the item we are looking for.

the elem_hash at 62 that triggers our issue is 10452750943628629886
A couple updates. The map size has always been 64 for I see this.

The item is always in the hash but its just after an element that should be higher than it based on probe_length. ie

the item is at index 63 

but there;s an item at 62

where when you run the probe length, our item when checked at 62 has a higher probe length than the item at 62.
@dipinhora thinks its a memory clobbering issue as we can't as yet reproduce with 1 thread and if you get rid of the actor that does the printing, the problem also goes away. its odd because the object map that hits the error looks fine except for being out of order. 

very odd.
I picked this up this morning while talking to sylvan. Same binaries as the other night and can't get a consistent crash anymore.
AND i just determined that if zoom isnt running, it happens

With some teamwork amongst @sylvanc, @dipinhora and myself, we appear to have found the source of the problem. Working on a fix.
Fix for this has been released as part of 0.14.0