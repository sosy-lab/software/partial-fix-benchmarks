diff --git a/src/libponyrt/sched/scheduler.c b/src/libponyrt/sched/scheduler.c
index 03a6df4bc1..1a3358d4f2 100644
--- a/src/libponyrt/sched/scheduler.c
+++ b/src/libponyrt/sched/scheduler.c
@@ -231,15 +231,7 @@ static scheduler_t* choose_victim(scheduler_t* sched)
 static pony_actor_t* steal(scheduler_t* sched)
 {
   bool block_sent = false;
-
-  if(ponyint_mutemap_size(&sched->mute_mapping) == 0)
-  {
-    // Only send block message if we don't have any muted actors.
-    // If we have at least one muted actor it means we aren't really
-    // blocked. There's work that can eventually be done.
-    send_msg(0, SCHED_BLOCK, 0);
-    block_sent = true;
-  }
+  uint8_t steal_attempts = 0;
 
   uint64_t tsc = ponyint_cpu_tick();
   pony_actor_t* actor;
@@ -263,22 +255,14 @@ static pony_actor_t* steal(scheduler_t* sched)
 
     if(read_msg(sched))
     {
-      // and actor was unmuted and added to our run queue.
-      // pop it and return. effectively, we are "stealing" from ourselves
-
+      // An actor was unmuted and added to our run queue. Pop it and return.
+      // Effectively, we are "stealing" from ourselves. We need to verify that
+      // popping succeeded (actor != NULL) as some other scheduler might have
+      // stolen the newly scheduled actor from us already. Schedulers, what a
+      // bunch of thieving bastards!
       actor = pop_global(sched);
       if(actor != NULL)
         break;
-      else
-      {
-        if (ponyint_mutemap_size(&sched->mute_mapping) == 0 && !block_sent)
-        {
-          // Someone else stole from our newly unmuted actor. If we have no
-          // more muted actors, we need to inform everyone that we are blocked
-          send_msg(0, SCHED_BLOCK, 0);
-          block_sent = true;
-        }
-      }
     }
 
     if(quiescent(sched, tsc, tsc2))
@@ -286,13 +270,49 @@ static pony_actor_t* steal(scheduler_t* sched)
       DTRACE2(WORK_STEAL_FAILURE, (uintptr_t)sched, (uintptr_t)victim);
       return NULL;
     }
+
+    // Determine if we are blocked.
+    //
+    // Note, "blocked" means we have no more work to do and we believe that we
+    // should check to see if we can terminate the program.
+    //
+    // To be blocked, we have to:
+    //
+    // 1. Not have any muted actors. If we are holding any muted actors then,
+    //    while we might not have any work to do, we aren't blocked. Blocked
+    //    means we can't make forward progress and the program might be ready
+    //    to terminate. Muted actors means that no, the program isn't ready
+    //    to terminate.
+    // 2. We have attempted to steal from every other scheduler and failed to
+    //    get any work. In the process of stealing from every other scheduler,
+    //    we will have also tried getting work off the ASIO inject queue
+    //    multiple times
+    // 3. We've been trying to steal for at least 10 billion cycles.
+    //    In many work stealing scenarios, we immediately get steal an actor.
+    //    Sending a block/unblock pair in that scenario is very wasteful.
+    //    Same applies to other "quick" steal scenarios.
+    //    10 billion cycles is roughly 5 seconds, depending on clock speed.
+    //    By waiting 5 seconds before sending a block message, we are going to
+    //    deal quiescence by a decent amount of time but also optimize work
+    //    stealing for generating far fewer block/unblock messages.
+    if (!block_sent)
+    {
+      steal_attempts++;
+
+      if (((tsc2 - tsc) > 10000000000) &&
+        (steal_attempts == (scheduler_count - 1)) &&
+        (ponyint_mutemap_size(&sched->mute_mapping) == 0))
+      {
+        send_msg(0, SCHED_BLOCK, 0);
+        block_sent = true;
+      }
+    }
   }
 
   if(block_sent)
   {
-    // Only send unblock message if sent one while trying to steal
+    // Only send unblock message if sent a block message while trying to steal
     send_msg(0, SCHED_UNBLOCK, 0);
-    ;
   }
   return actor;
 }
