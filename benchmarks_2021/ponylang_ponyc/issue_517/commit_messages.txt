Add gitignore to examples to ignore the binary (#2349)
Improve work-stealing "scheduler is blocked" logic

Prior to this commit, we sent actor block and unblock messages each time
we entered and left `steal`. Every instance of work stealing resulted in
a block/unblock message pair being sent; even if stealing was
immediately successful.

This was wasteful in a number of ways:

1. extra memory allocations
2. extra message sends
3. extra handling and processing of pointless block/unblock messages

This commit changes block/unblock message sending logic. Hat tip to
Scott Fritchie for pointing out to be how bad the issue was. He spent
some time with DTrace and come up with some truly terrifying numbers for
how much extra work was being done. Dipin Hora and I independently came
up with what was effectively the same solution for this problem. This
commit melds the best of his implementation with the best of mine.

With this commit applied, work stealing will only result in a
block/unblock message pair being sent if:

1) the scheduler in question has attempted to steal from every other
scheduler (new behavior)
2) the scheduler in question has tried to steal for at least 10 billion
clock cycles (about 5 seconds on most machines) (new behavior)
3) the scheduler in question has no unscheduled actors in its mutemap
(existing behavior)

Item 2 is the biggest change. What we are doing is increasing program
shutdown time by at least 5 seconds (perhaps slightly more due to cross
scheduler timing issues) in return for much better application
performance while running.

Issue #2317 is mostly fixed by this issue (although there is still a
small amount of memory growth due to another issue).

Issue #517 is changed by this commit. It has memory growth that is much
slower than before but still quite noticeable. On my machine #517 will
no longer OOM as it eventually gets to around 8 gigs in memory usage and
is able to keep up with freeing memory ahead of new memory allocations.
Given that there is still an underlying problem with memory allocation
patterns (the same as #2317), I think that it's possible that the
example program in #517 would still OOM on some test machines.

Fixes #647
Improve work-stealing "scheduler is blocked" logic

Prior to this commit, we sent actor block and unblock messages each time
we entered and left `steal`. Every instance of work stealing resulted in
a block/unblock message pair being sent; even if stealing was
immediately successful.

This was wasteful in a number of ways:

1. extra memory allocations
2. extra message sends
3. extra handling and processing of pointless block/unblock messages

This commit changes block/unblock message sending logic. Hat tip to
Scott Fritchie for pointing out to be how bad the issue was. He spent
some time with DTrace and come up with some truly terrifying numbers for
how much extra work was being done. Dipin Hora and I independently came
up with what was effectively the same solution for this problem. This
commit melds the best of his implementation with the best of mine.

With this commit applied, work stealing will only result in a
block/unblock message pair being sent if:

1) the scheduler in question has attempted to steal from every other
scheduler (new behavior)
2) the scheduler in question has tried to steal for at least 10 billion
clock cycles (about 5 seconds on most machines) (new behavior)
3) the scheduler in question has no unscheduled actors in its mutemap
(existing behavior)

Item 2 is the biggest change. What we are doing is increasing program
shutdown time by at least 5 seconds (perhaps slightly more due to cross
scheduler timing issues) in return for much better application
performance while running.

Issue #2317 is mostly fixed by this issue (although there is still a
small amount of memory growth due to another issue).

Issue #517 is changed by this commit. It has memory growth that is much
slower than before but still quite noticeable. On my machine #517 will
no longer OOM as it eventually gets to around 8 gigs in memory usage and
is able to keep up with freeing memory ahead of new memory allocations.
Given that there is still an underlying problem with memory allocation
patterns (the same as #2317), I think that it's possible that the
example program in #517 would still OOM on some test machines.

Fixes #647
Improve work-stealing "scheduler is blocked" logic

Prior to this commit, we sent actor block and unblock messages each time
we entered and left `steal`. Every instance of work stealing resulted in
a block/unblock message pair being sent; even if stealing was
immediately successful.

This was wasteful in a number of ways:

1. extra memory allocations
2. extra message sends
3. extra handling and processing of pointless block/unblock messages

This commit changes block/unblock message sending logic. Hat tip to
Scott Fritchie for pointing out to be how bad the issue was. He spent
some time with DTrace and come up with some truly terrifying numbers for
how much extra work was being done. Dipin Hora and I independently came
up with what was effectively the same solution for this problem. This
commit melds the best of his implementation with the best of mine.

With this commit applied, work stealing will only result in a
block/unblock message pair being sent if:

1) the scheduler in question has attempted to steal from every other
scheduler (new behavior)
2) the scheduler in question has tried to steal for at least 10 billion
clock cycles (about 5 seconds on most machines) (new behavior)
3) the scheduler in question has no unscheduled actors in its mutemap
(existing behavior)

Item 2 is the biggest change. What we are doing is increasing program
shutdown time by at least 5 seconds (perhaps slightly more due to cross
scheduler timing issues) in return for much better application
performance while running.

Issue #2317 is mostly fixed by this issue (although there is still a
small amount of memory growth due to another issue).

Issue #517 is changed by this commit. It has memory growth that is much
slower than before but still quite noticeable. On my machine #517 will
no longer OOM as it eventually gets to around 8 gigs in memory usage and
is able to keep up with freeing memory ahead of new memory allocations.
Given that there is still an underlying problem with memory allocation
patterns (the same as #2317), I think that it's possible that the
example program in #517 would still OOM on some test machines.

Fixes #647
Improve work-stealing "scheduler is blocked" logic

Prior to this commit, we sent actor block and unblock messages each time
we entered and left `steal`. Every instance of work stealing resulted in
a block/unblock message pair being sent; even if stealing was
immediately successful.

This was wasteful in a number of ways:

1. extra memory allocations
2. extra message sends
3. extra handling and processing of pointless block/unblock messages

This commit changes block/unblock message sending logic. Hat tip to
Scott Fritchie for pointing out to be how bad the issue was. He spent
some time with DTrace and come up with some truly terrifying numbers for
how much extra work was being done. Dipin Hora and I independently came
up with what was effectively the same solution for this problem. This
commit melds the best of his implementation with the best of mine.

With this commit applied, work stealing will only result in a
block/unblock message pair being sent if:

1) the scheduler in question has attempted to steal from every other
scheduler (new behavior)
2) the scheduler in question has tried to steal for at least 10 billion
clock cycles (about 5 seconds on most machines) (new behavior)
3) the scheduler in question has no unscheduled actors in its mutemap
(existing behavior)

Item 2 is the biggest change. What we are doing is increasing program
shutdown time by at least 5 seconds (perhaps slightly more due to cross
scheduler timing issues) in return for much better application
performance while running.

Issue #2317 is mostly fixed by this issue (although there is still a
small amount of memory growth due to another issue).

Issue #517 is changed by this commit. It has memory growth that is much
slower than before but still quite noticeable. On my machine #517 will
no longer OOM as it eventually gets to around 8 gigs in memory usage and
is able to keep up with freeing memory ahead of new memory allocations.
Given that there is still an underlying problem with memory allocation
patterns (the same as #2317), I think that it's possible that the
example program in #517 would still OOM on some test machines.

Fixes #647
Improve work-stealing "scheduler is blocked" logic

Prior to this commit, we sent actor block and unblock messages each time
we entered and left `steal`. Every instance of work stealing resulted in
a block/unblock message pair being sent; even if stealing was
immediately successful.

This was wasteful in a number of ways:

1. extra memory allocations
2. extra message sends
3. extra handling and processing of pointless block/unblock messages

This commit changes block/unblock message sending logic. Hat tip to
Scott Fritchie for pointing out to be how bad the issue was. He spent
some time with DTrace and come up with some truly terrifying numbers for
how much extra work was being done. Dipin Hora and I independently came
up with what was effectively the same solution for this problem. This
commit melds the best of his implementation with the best of mine.

With this commit applied, work stealing will only result in a
block/unblock message pair being sent if:

1) the scheduler in question has attempted to steal from every other
scheduler (new behavior)
2) the scheduler in question has tried to steal for at least 10 billion
clock cycles (about 5 seconds on most machines) (new behavior)
3) the scheduler in question has no unscheduled actors in its mutemap
(existing behavior)

Item 2 is the biggest change. What we are doing is increasing program
shutdown time by at least 5 seconds (perhaps slightly more due to cross
scheduler timing issues) in return for much better application
performance while running.

Issue #2317 is mostly fixed by this issue (although there is still a
small amount of memory growth due to another issue).

Issue #517 is changed by this commit. It has memory growth that is much
slower than before but still quite noticeable. On my machine #517 will
no longer OOM as it eventually gets to around 8 gigs in memory usage and
is able to keep up with freeing memory ahead of new memory allocations.
Given that there is still an underlying problem with memory allocation
patterns (the same as #2317), I think that it's possible that the
example program in #517 would still OOM on some test machines.

Fixes #647
Improve work-stealing "scheduler is blocked" logic (#2355)

Prior to this commit, we sent actor block and unblock messages each time
we entered and left `steal`. Every instance of work stealing resulted in
a block/unblock message pair being sent; even if stealing was
immediately successful.

This was wasteful in a number of ways:

1. extra memory allocations
2. extra message sends
3. extra handling and processing of pointless block/unblock messages

This commit changes block/unblock message sending logic. Hat tip to
Scott Fritchie for pointing out to be how bad the issue was. He spent
some time with DTrace and come up with some truly terrifying numbers for
how much extra work was being done. Dipin Hora and I independently came
up with what was effectively the same solution for this problem. This
commit melds the best of his implementation with the best of mine.

With this commit applied, work stealing will only result in a
block/unblock message pair being sent if:

1) the scheduler in question has attempted to steal from every other
scheduler (new behavior)
2) the scheduler in question has tried to steal for at least 10 billion
clock cycles (about 5 seconds on most machines) (new behavior)
3) the scheduler in question has no unscheduled actors in its mutemap
(existing behavior)

Item 2 is the biggest change. What we are doing is increasing program
shutdown time by at least 5 seconds (perhaps slightly more due to cross
scheduler timing issues) in return for much better application
performance while running.

Issue #2317 is mostly fixed by this issue (although there is still a
small amount of memory growth due to another issue).

Issue #517 is changed by this commit. It has memory growth that is much
slower than before but still quite noticeable. On my machine #517 will
no longer OOM as it eventually gets to around 8 gigs in memory usage and
is able to keep up with freeing memory ahead of new memory allocations.
Given that there is still an underlying problem with memory allocation
patterns (the same as #2317), I think that it's possible that the
example program in #517 would still OOM on some test machines.

Fixes #647
