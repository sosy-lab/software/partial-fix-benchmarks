Segfault on receiving a MUC PM
plugins_pre_priv_message_display() receives a `message` which is NULL from sv_ev_incoming_private_message() which passes `message->plain` to it.
Reason for this is that _private_chat_handler() does not copy over the message from `message->body`, like it is done for _handle_groupchat().

@paulfariello can you remind me why we need the `plain` and `body` parts? It would be cool if we could have one where we can be sure that the message is always inside.
Right now we try to set `plain` as early as possible so that later on we can just rely on `plain` containing something. This is better then testing for `body` in case of `plain` being empty on all later parts.
@paulfariello could you recheck the _handle_carbons() and _handle_chat() handlers?
@jubalh body is the content of `<body></body>` but for OMEMO a body is not required so it can sometime be empty. The idea is that coming out from `xmpp/message.c:_handle_chat` you can have either `body` or `plain`.

This is required since OMEMO decryption is done directly from `_handle_chat` because it needs lots of xmpp element from messages while GPG and OTR decryption are done in `event/server_events.c`.

I think what we should guarantee is that coming out of the `_sv_ev_incoming_{otr,omemo,gpg}` function plain should always be set.
> I think what we should guarantee is that coming out of the _sv_ev_incoming_{otr,omemo,gpg} function plain should always be set.

Yep thats good. Can you check this again?