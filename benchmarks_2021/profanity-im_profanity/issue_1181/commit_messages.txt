Set message->plain in carbon case

message->plain should always contain something. In the case of the
carbons we forgot to set if rom the body in case it's empy.
Log 1:1 messages from other clients also to file

Carbons where not logged so far.

Fix https://github.com/profanity-im/profanity/issues/1181
Log outgoing carbons instead of incoming

Incoming carbons are logged as normal message already.
So we had this logged twice but didn't log outgoing carbons,
send from our account but by another client, at all.

Fix https://github.com/profanity-im/profanity/issues/1181
