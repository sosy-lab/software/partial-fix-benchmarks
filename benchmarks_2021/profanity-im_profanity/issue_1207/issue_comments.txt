Support XEP-0359: Unique and Stable Stanza IDs
We don't implement </stanza-id> yet, because we don't use it until we have MAM.
https://github.com/profanity-im/profanity/issues/1207 merged