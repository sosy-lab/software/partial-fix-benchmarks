Set muc history correctly in _handle_groupchat()

Fix init. mistake introduced in e9c5c1979d836ed75c37d48651710b4fd125cfb2
Add option for legacy authentication

New option:
  /connect <account> [auth default|legacy]

Current patch is unfinished for accounts.

Fixes #1236.
Add option for legacy authentication

New option:
  /connect <account> [auth default|legacy]

Current patch is unfinished for accounts.

Fixes #1236.
Add option for legacy authentication

New options:
  /connect <account> [auth default|legacy]
  /account <account> set auth default|legacy

Fixes #1236.
Add option for legacy authentication

New options:
  /connect <account> [auth default|legacy]
  /account <account> set auth default|legacy

Fixes #1236.
