Merge pull request #159 from Icenowy/main-fix

Fix compliation of non-dynarec build
Added _obstack_begin wrapped function to libc (for #160)
Forgot the EXPORT for my__obstack_begin (should help #160)
Try to improve (and fix) obstack wrapping (should help #160)
