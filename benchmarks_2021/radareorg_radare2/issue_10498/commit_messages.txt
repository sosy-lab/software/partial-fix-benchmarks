Fix a few memory leaks for windbg (#10504)
Fix #10498 - Crash in fuzzed java file
Fix #10498 - Fix crash in fuzzed java files (#10511)
