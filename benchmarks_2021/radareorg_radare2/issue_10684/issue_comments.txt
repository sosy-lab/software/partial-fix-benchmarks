r2 commands "afi" and "afij" are inconsistent in data they provide
this matters for the cutter guys. cc @xarkes 
Current output to compare:
```
[0x1000011ec]> afi
#
offset: 0x1000011ec
name: fcn.1000011ec
size: 20
realsz: 20
stackframe: 0
call-convention: amd64
cyclomatic-cost : 7
cyclomatic-complexity: 1
bits: 64
type: fcn [NEW]
num-bbs: 1
edges: 0
end-bbs: 1
call-refs:
data-refs:
code-xrefs:
in-degree: 0
out-degree: 0
data-xrefs:
locals:0
args: 2
arg int arg5 @ r8
arg int arg2 @ rsi
diff: type: new
[0x1000011ec]> afij
[{"offset":4294971884,"name":"fcn.1000011ec","size":20,"realsz":20,"stackframe":0,"cc":1,"cost":7,"nbbs":1,"edges":0,"ebbs":1,"calltype":"amd64","type":"fcn","minbound":"4588","maxbound":"4608","range":"20","diff":"NEW","difftype":"new","indegree":0,"outdegree":0,"nargs":2,"nlocals":0}]
[0x1000011ec]> afij~{}
[
  {
    "offset": 4294971884,
    "name": "fcn.1000011ec",
    "size": 20,
    "realsz": 20,
    "stackframe": 0,
    "cc": 1,
    "cost": 7,
    "nbbs": 1,
    "edges": 0,
    "ebbs": 1,
    "calltype": "amd64",
    "type": "fcn",
    "minbound": "4588",
    "maxbound": "4608",
    "range": "20",
    "diff": "NEW",
    "difftype": "new",
    "indegree": 0,
    "outdegree": 0,
    "nargs": 2,
    "nlocals": 0
  }
]
[0x1000011ec]>
```