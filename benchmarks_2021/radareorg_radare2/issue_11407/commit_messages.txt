Add even moar help
Fixes one crash of #11407
Fixes one crash of #11407 (#11443)
Fix r2 -c '?btw 1 1 1' /bin/ls

There is no reason for the list returned by r_num_str_split_list
to have a link with a variable called 'free', and this cause crash with
ASAN, as seen on https://github.com/radare/radare2/issues/11407
Fix r_hex_str_is_valid to handle space like r_hex_str2bin

r_hex_str_is_valid is called only once from r_reg_arena_set_bytes,
to find the size of the hexadecimal string. However, it stop
at the first space, while the method used for the conversion
r_hex_str2bin is skipping space and continue to convert.

So with a long enough string passed to drw and arw, it is possible
to crash r2, as reported in https://github.com/radare/radare2/issues/11407
Fix r2 -c '?btw 1 1 1' /bin/ls (#11488)

There is no reason for the list returned by r_num_str_split_list
to have a link with a variable called 'free', and this cause crash with
ASAN, as seen on https://github.com/radare/radare2/issues/11407
Fix r_hex_str_is_valid to handle space like r_hex_str2bin (#11489)

r_hex_str_is_valid is called only once from r_reg_arena_set_bytes,
to find the size of the hexadecimal string. However, it stop
at the first space, while the method used for the conversion
r_hex_str2bin is skipping space and continue to convert.

So with a long enough string passed to drw and arw, it is possible
to crash r2, as reported in https://github.com/radare/radare2/issues/11407
Do not crash if a string with a size of R_NUMCALC_STRSZ is passed

Since str is a fixed array of R_NUMCALC_STRSZ, str[i] will crash
with ASAN if i == R_NUMCALC_STRSZ, as it happen with the loop
and 'i++'

Fix another issue reported on https://github.com/radare/radare2/issues/11407
Do not crash if a string with a size of R_NUMCALC_STRSZ is passed

Since str is a fixed array of R_NUMCALC_STRSZ, str[i] will crash
with ASAN if i == R_NUMCALC_STRSZ, as it happen with the loop
and 'i++'

Fix another issue reported on https://github.com/radare/radare2/issues/11407
Do not crash if a string with a size of R_NUMCALC_STRSZ is passed

Since str is a fixed array of R_NUMCALC_STRSZ, str[i] will crash
with ASAN if i == R_NUMCALC_STRSZ, as it happen with the loop
and 'i++'

Fix another issue reported on https://github.com/radare/radare2/issues/11407
Do not crash if a string with a size of R_NUMCALC_STRSZ is passed

Since str is a fixed array of R_NUMCALC_STRSZ, str[i] will crash
with ASAN if i == R_NUMCALC_STRSZ, as it happen with the loop
and 'i++'

Fix another issue reported on https://github.com/radare/radare2/issues/11407
Do not crash if a string with a size of R_NUMCALC_STRSZ is passed

Since str is a fixed array of R_NUMCALC_STRSZ, str[i] will crash
with ASAN if i == R_NUMCALC_STRSZ, as it happen with the loop
and 'i++'

Fix another issue reported on https://github.com/radare/radare2/issues/11407
Do not crash if a string with a size of R_NUMCALC_STRSZ is passed (#11498)

Since str is a fixed array of R_NUMCALC_STRSZ, str[i] will crash
with ASAN if i == R_NUMCALC_STRSZ, as it happen with the loop
and 'i++'

Fix another issue reported on https://github.com/radare/radare2/issues/11407
