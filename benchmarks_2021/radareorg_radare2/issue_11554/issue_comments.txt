Issue 10403 in oss-fuzz: radare2/ia_fuzz: Timeout in radare2_ia_fuzz

It loop, stack trace:

```
#0  0x00007fcc033a3c60 in __write_nocancel () from /lib64/libc.so.6
#1  0x00007fcc0332e053 in _IO_new_file_write () from /lib64/libc.so.6
#2  0x00007fcc0332e8f0 in __GI__IO_file_xsputn () from /lib64/libc.so.6
#3  0x00007fcc03323572 in fwrite () from /lib64/libc.so.6
#4  0x00007fcc06fb5d95 in r_bin_mdmp_init_directory_entry (obj=0x56325ae6cec0, entry=0x7ffc7e151690) at /home/misc/checkout/git/radare2/libr/..//libr/bin/p/../format/mdmp/mdmp.c:370
#5  0x00007fcc06fb6f84 in r_bin_mdmp_init_directory (obj=0x56325ae6cec0) at /home/misc/checkout/git/radare2/libr/..//libr/bin/p/../format/mdmp/mdmp.c:723
#6  0x00007fcc06fb7567 in r_bin_mdmp_init (obj=0x56325ae6cec0) at /home/misc/checkout/git/radare2/libr/..//libr/bin/p/../format/mdmp/mdmp.c:847
#7  0x00007fcc06fb78ab in r_bin_mdmp_new_buf (buf=0x56325ae6ce20) at /home/misc/checkout/git/radare2/libr/..//libr/bin/p/../format/mdmp/mdmp.c:893
#8  0x00007fcc06fb472b in load_bytes (bf=0x56325ae5d4e0, buf=0x56325ae5d4b0 "MDMP\223\247\b\177ELFs\001\037", sz=33, loadaddr=0, sdb=0x56325ae5e1d0) at /home/misc/checkout/git/radare2/libr/..//libr/bin/p/bin_mdmp.c:196
#9  0x00007fcc06f47572 in r_bin_object_new (binfile=0x56325ae5d4e0, plugin=0x56325ad98720, baseaddr=18446744073709551615, loadaddr=0, offset=0, sz=33) at obj.c:58
#10 0x00007fcc06f45d33 in r_bin_file_new_from_bytes (bin=0x56325ad8cac0, file=0x56325ae5d2d0 "/tmp/clusterfuzz-testcase-minimized-ia_fuzz-5756355915284480.dms", bytes=0x56325ae5d4b0 "MDMP\223\247\b\177ELFs\001\037", sz=33, file_sz=33, rawstr=0, baseaddr=18446744073709551615, loadaddr=0, fd=3, pluginname=0x0, xtrname=0x0, offset=0, steal_ptr=true) at bfile.c:515
#11 0x00007fcc06f389cf in r_bin_load_io2 (bin=0x56325ad8cac0, fd=3, baseaddr=18446744073709551615, loadaddr=0, xtr_idx=0, offset=0, name=0x0, sz=33) at bin.c:485
#12 0x00007fcc06f37670 in r_bin_load_io (bin=0x56325ad8cac0, fd=3, baseaddr=18446744073709551615, loadaddr=0, xtr_idx=0, offset=0, name=0x0) at bin.c:42
#13 0x00007fcc07be6b77 in r_core_file_do_load_for_io_plugin (r=0x56325a665580 <r>, baseaddr=18446744073709551615, loadaddr=0) at cfile.c:385
#14 0x00007fcc07be7327 in r_core_bin_load (r=0x56325a665580 <r>, filenameuri=0x56325ae5d2d0 "/tmp/clusterfuzz-testcase-minimized-ia_fuzz-5756355915284480.dms", baddr=18446744073709551615) at cfile.c:544
#15 0x000056325a4615ea in main (argc=2, argv=0x7ffc7e151f88, envp=0x7ffc7e151fa0) at radare2.c:1119
```

Message:
```
[ERROR] Size Mismatch - Stream data is larger than file size!
```
