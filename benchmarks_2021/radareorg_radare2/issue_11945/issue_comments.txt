Implement iSq command
The `.` does not make sense in `iS` because `iS` correspond to RBin data, additionally `om.` is already implemented so I think it's already good to go for the deletion.

We however should have a command in `o` that shows the relationship between RBIn and the mapping.
Just documenting using it in book should be enough. Please open a bug in
book repo for that.

On Fri, Oct 26, 2018, 6:22 PM Maijin <notifications@github.com> wrote:

> The . does not make sense in iS because iS correspond to RBin data,
> additionally om. is already implemented so I think it's already good to
> go for the deletion.
>
> We however should have a command in o that shows the relationship between
> RBIn and the mapping.
>
> —
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/radare/radare2/issues/11945#issuecomment-433362275>,
> or mute the thread
> <https://github.com/notifications/unsubscribe-auth/AAMZ_T314VQpCoZ3CDj3PMoMm0X9DV-Zks5uouJkgaJpZM4X7au0>
> .
>

For sure!
you can parse a bin without loading the maps, the . must be in i subcommands too. in fact its already in iS. but not in iSq.

> On 26 Oct 2018, at 12:22, Maijin <notifications@github.com> wrote:
> 
> The . does not make sense in iS because iS correspond to RBin data, additionally om. is already implemented so I think it's already good to go for the deletion.
> 
> We however should have a command in o that shows the relationship between RBIn and the mapping.
> 
> —
> You are receiving this because you authored the thread.
> Reply to this email directly, view it on GitHub <https://github.com/radare/radare2/issues/11945#issuecomment-433362275>, or mute the thread <https://github.com/notifications/unsubscribe-auth/AA3-ls3vGQbWHmQQdYmYyG7c2zrk5pggks5uouJlgaJpZM4X7au0>.
> 


the relation between bin and mapping has been explained by me in another issue and htis must be done inside rbinobjects, thats impotrtant for projects to work. but this is far from the scope of this issue

> On 26 Oct 2018, at 12:22, Maijin <notifications@github.com> wrote:
> 
> The . does not make sense in iS because iS correspond to RBin data, additionally om. is already implemented so I think it's already good to go for the deletion.
> 
> We however should have a command in o that shows the relationship between RBIn and the mapping.
> 
> —
> You are receiving this because you authored the thread.
> Reply to this email directly, view it on GitHub <https://github.com/radare/radare2/issues/11945#issuecomment-433362275>, or mute the thread <https://github.com/notifications/unsubscribe-auth/AA3-ls3vGQbWHmQQdYmYyG7c2zrk5pggks5uouJlgaJpZM4X7au0>.
> 

