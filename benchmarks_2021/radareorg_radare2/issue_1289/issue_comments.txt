Improvements of iS//iSj
```
04:25 < pancake> to get section entropy
04:25 < pancake> you should call rabin2 -K entropy -jS $FILE
```

For md5, sha1, just change the argument of -K

The index is implicit because its a JSON array. And vsize is just 99% the same as size. but im adding it :)

Only remains the webui

Also, add an option to disable hashing this, or try it with a big binary or invalid section information.

iS doesn't hash nothing by the default, it's the same as before. You have to specify the algorithm to use. I'll try with fuzzed binaries from r2r

Everything is done now, right? can we close this issue? @Maijin only needs to have it ready for the webui
