diff --git a/libr/anal/Makefile b/libr/anal/Makefile
index ad199025984..ada664bb0e6 100644
--- a/libr/anal/Makefile
+++ b/libr/anal/Makefile
@@ -5,7 +5,7 @@ EXTRA_CLEAN=doclean
 CFLAGS+=-I..
 
 NAME=r_anal
-DEPS=r_util r_reg r_syscall r_search r_cons r_flag r_hash r_crypto
+DEPS=r_util r_reg r_syscall r_search r_cons r_flag r_hash r_crypto r_parse
 CFLAGS+=-DCORELIB -Iarch -I$(TOP)/shlr
 CFLAGS+=-I$(LTOP)/asm/arch/include
 
@@ -29,7 +29,7 @@ include ${STATIC_ANAL_PLUGINS}
 
 STATIC_OBJS=$(addprefix $(LTOP)/anal/p/,$(STATIC_OBJ))
 OBJLIBS=meta.o reflines.o op.o fcn.o bb.o var.o
-OBJLIBS+=cond.o value.o cc.o class.o diff.o
+OBJLIBS+=cond.o value.o cc.o class.o diff.o type.o
 OBJLIBS+=hint.o anal.o data.o xrefs.o esil.o sign.o
 OBJLIBS+=anal_ex.o switch.o state.o cycles.o
 OBJLIBS+=esil_sources.o esil_interrupt.o
diff --git a/libr/anal/fcn.c b/libr/anal/fcn.c
index aa7e50fa74b..a89fe495a86 100644
--- a/libr/anal/fcn.c
+++ b/libr/anal/fcn.c
@@ -1,6 +1,7 @@
 /* radare - LGPL - Copyright 2010-2019 - nibble, alvaro, pancake */
 
 #include <r_anal.h>
+#include <r_parse.h>
 #include <r_util.h>
 #include <r_list.h>
 
@@ -2191,29 +2192,19 @@ R_API char *r_anal_fcn_to_string(RAnal *a, RAnalFunction *fs) {
 	return NULL;
 }
 
-// TODO: This function is not fully implemented
 /* set function signature from string */
 R_API int r_anal_str_to_fcn(RAnal *a, RAnalFunction *f, const char *sig) {
-	int length = 0;
-	if (!a || !f || !sig) {
-		eprintf ("r_anal_str_to_fcn: No function received\n");
-		return false;
+	r_return_val_if_fail (a || f || sig, false);
+	char *error_msg = NULL;
+	const char *out = r_parse_c_string (a, sig, &error_msg);
+	if (out) {
+		r_anal_save_parsed_type (a, out);
 	}
-	length = strlen (sig) + 10;
-	/* Add 'function' keyword */
-	char *str = calloc (1, length);
-	if (!str) {
-		eprintf ("Cannot allocate %d byte(s)\n", length);
-		return false;
+	if (error_msg) {
+		fprintf (stderr, "%s", error_msg);
+		free (error_msg);
 	}
-	strcpy (str, "function ");
-	strcat (str, sig);
-
-	/* TODO: improve arguments parsing */
-	/* TODO: implement parser */
-	/* TODO: simplify this complex api usage */
 
-	free (str);
 	return true;
 }
 
diff --git a/libr/anal/meson.build b/libr/anal/meson.build
index 4857a9bdf47..82011916182 100644
--- a/libr/anal/meson.build
+++ b/libr/anal/meson.build
@@ -29,6 +29,7 @@ r_anal_sources = [
   'sign.c',
   'state.c',
   'switch.c',
+  'type.c',
   'value.c',
   'var.c',
   'vtable.c',
@@ -133,6 +134,7 @@ r_anal = library('r_anal', r_anal_sources,
     r_crypto_dep,
     r_search_dep,
     r_cons_dep,
+    r_parse_dep,
     r_syscall_dep,
     r_flag_dep,
     r_hash_dep,
@@ -158,6 +160,7 @@ pkgconfig_mod.generate(r_anal,
     'r_reg',
     'r_asm',
     'r_crypto',
+    'r_parse',
     'r_syscall',
     'r_search',
     'r_cons',
diff --git a/libr/anal/type.c b/libr/anal/type.c
new file mode 100644
index 00000000000..574251186d9
--- /dev/null
+++ b/libr/anal/type.c
@@ -0,0 +1,110 @@
+/* radare - LGPL - Copyright 2019 - pancake, oddcoder, Anton Kochkov */
+
+#include <string.h>
+#include <r_anal.h>
+#include "sdb/sdb.h"
+
+/*!
+ * \brief Save the size of the given datatype in sdb
+ * \param sdb_types pointer to the sdb for types
+ * \param name the datatype whose size if to be stored
+ */
+static void save_type_size(Sdb *sdb_types, char *name) {
+	char *type = NULL;
+	r_return_if_fail (sdb_types && name);
+	if (!sdb_exists (sdb_types, name) || !(type = sdb_get (sdb_types, name, 0))) {
+		return;
+	}
+	char *type_name_size = r_str_newf ("%s.%s.%s", type, name, "0size");
+	r_return_if_fail (type_name_size);
+	int size = r_type_get_bitsize (sdb_types, name);
+	sdb_set (sdb_types, type_name_size, sdb_fmt ("%d", size), 0);
+	free (type);
+	free (type_name_size);
+}
+
+/*!
+ * \brief Save the sizes of the datatypes which have been parsed
+ * \param core pointer to radare2 core
+ * \param parsed the parsed c string in sdb format
+ */
+static void save_parsed_type_size(RAnal *anal, const char *parsed) {
+	r_return_if_fail (anal && parsed);
+	char *str = strdup (parsed);
+	if (str) {
+		char *ptr = NULL;
+		int offset = 0;
+		while ((ptr = strstr (str + offset, "=struct\n")) ||
+				(ptr = strstr (str + offset, "=union\n"))) {
+			*ptr = 0;
+			if (str + offset == ptr) {
+				break;
+			}
+			char *name = ptr - 1;
+			while (name > str && *name != '\n') {
+				name--;
+			}
+			if (*name == '\n') {
+				name++;
+			}
+			save_type_size (anal->sdb_types, name);
+			*ptr = '=';
+			offset = ptr + 1 - str;
+		}
+		free (str);
+	}
+}
+
+R_API void r_anal_remove_parsed_type(RAnal *anal, const char *name) {
+	r_return_if_fail (anal && name);
+	Sdb *TDB = anal->sdb_types;
+	SdbKv *kv;
+	SdbListIter *iter;
+	const char *type = sdb_const_get (TDB, name, 0);
+	if (!type) {
+		return;
+	}
+	int tmp_len = strlen (name) + strlen (type);
+	char *tmp = malloc (tmp_len + 1);
+	r_type_del (TDB, name);
+	if (tmp) {
+		snprintf (tmp, tmp_len + 1, "%s.%s.", type, name);
+		SdbList *l = sdb_foreach_list (TDB, true);
+		ls_foreach (l, iter, kv) {
+			if (!strncmp (sdbkv_key (kv), tmp, tmp_len)) {
+				r_type_del (TDB, sdbkv_key (kv));
+			}
+		}
+		ls_free (l);
+		free (tmp);
+	}
+}
+
+R_API void r_anal_save_parsed_type(RAnal *anal, const char *parsed) {
+	r_return_if_fail (anal && parsed);
+	// First, if this exists, let's remove it.
+	char *type = strdup (parsed);
+	if (type) {
+		char *name = NULL;
+		if ((name = strstr (type, "=type")) ||
+			(name = strstr (type, "=struct")) ||
+			(name = strstr (type, "=union")) ||
+			(name = strstr (type, "=enum")) ||
+			(name = strstr (type, "=typedef")) ||
+			(name = strstr (type, "=func"))) {
+			*name = 0;
+			while (name - 1 >= type && *(name - 1) != '\n') {
+				name--;
+			}
+		}
+		if (name) {
+			const char *cname = strdup (name);
+			r_anal_remove_parsed_type (anal, cname);
+			// Now add the type to sdb.
+			sdb_query_lines (anal->sdb_types, parsed);
+			save_parsed_type_size (anal, parsed);
+		}
+		free (type);
+	}
+}
+
diff --git a/libr/core/cmd_print.c b/libr/core/cmd_print.c
index 23f313759cb..48c661268cf 100644
--- a/libr/core/cmd_print.c
+++ b/libr/core/cmd_print.c
@@ -1207,7 +1207,7 @@ static void cmd_print_format(RCore *core, const char *_input, const ut8* block,
 				char *error_msg = NULL;
 				char *out = r_parse_c_file (core->anal, path, &error_msg);
 				if (out) {
-					r_core_save_parsed_type (core, out);
+					r_anal_save_parsed_type (core->anal, out);
 					r_core_cmd0 (core, ".ts*");
 					free (out);
 				} else {
diff --git a/libr/core/cmd_type.c b/libr/core/cmd_type.c
index b571b4e008e..35870d4590e 100644
--- a/libr/core/cmd_type.c
+++ b/libr/core/cmd_type.c
@@ -349,81 +349,6 @@ static void cmd_type_noreturn(RCore *core, const char *input) {
 	}
 }
 
-/*!
- * \brief Save the size of the given datatype in sdb
- * \param sdb_types pointer to the sdb for types
- * \param name the datatype whose size if to be stored
- */
-static void save_type_size(Sdb *sdb_types, char *name) {
-	char *type = NULL;
-	r_return_if_fail (sdb_types && name);
-	if (!sdb_exists (sdb_types, name) || !(type = sdb_get (sdb_types, name, 0))) {
-		return;
-	}
-	char *type_name_size = r_str_newf ("%s.%s.%s", type, name, "size");
-	if (!type_name_size) {
-		return;
-	}
-	int size = r_type_get_bitsize (sdb_types, name);
-	sdb_set (sdb_types, type_name_size, sdb_fmt ("%d", size), 0);
-	free (type);
-	free (type_name_size);
-}
-
-/*!
- * \brief Save the sizes of the datatypes which have been parsed
- * \param core pointer to radare2 core
- * \param parsed the parsed c string in sdb format
- */
-static void save_parsed_type_size(RCore *core, const char *parsed) {
-	r_return_if_fail (core && core->anal && parsed);
-	char *str = strdup (parsed);
-	if (str) {
-		char *ptr = NULL;
-		int offset = 0;
-		while ((ptr = strstr (str + offset, "=struct\n")) || (ptr = strstr (str + offset, "=union\n"))) {
-			*ptr = 0;
-			if (str + offset == ptr) {
-				break;
-			}
-			char *name = ptr - 1;
-			while (name > str && *name != '\n') {
-				name--;
-			}
-			if (*name == '\n') {
-				name++;
-			}
-			save_type_size (core->anal->sdb_types, name);
-			*ptr = '=';
-			offset = ptr + 1 - str;
-		}
-		free (str);
-	}
-}
-
-R_API void r_core_save_parsed_type(RCore *core, const char *parsed) {
-	r_return_if_fail (core && core->anal && parsed);
-	// First, if this exists, let's remove it.
-	char *type = strdup (parsed);
-	if (type) {
-		char *name = NULL;
-		if ((name = strstr (type, "=type")) || (name = strstr (type, "=struct")) || (name = strstr (type, "=union")) ||
-			(name = strstr (type, "=enum")) || (name = strstr (type, "=typedef")) || (name = strstr (type, "=func"))) {
-			*name = 0;
-			while (name - 1 >= type && *(name - 1) != '\n') {
-				name--;
-			}
-		}
-		if (name) {
-			r_core_cmdf (core, "\"t- %s\"", name);
-			// Now add the type to sdb.
-			sdb_query_lines (core->anal->sdb_types, parsed);
-			save_parsed_type_size (core, parsed);
-		}
-		free (type);
-	}
-}
-
 static Sdb *TDB_ = NULL; // HACK
 
 static int stdifstruct(void *user, const char *k, const char *v) {
@@ -464,7 +389,7 @@ static int print_struct_union_list_json(Sdb *TDB, SdbForeachCallback filter) {
 		}
 
 		pj_o (pj); // {
-		char *sizecmd = r_str_newf ("%s.%s.size", sdbkv_value (kv), k);
+		char *sizecmd = r_str_newf ("%s.%s.0size", sdbkv_value (kv), k);
 		if (!sizecmd) {
 			break;
 		}
@@ -591,7 +516,7 @@ static int print_typelist_json_cb(void *p, const char *k, const char *v) {
 	PJ *pj = pj_new ();
 	pj_o (pj);
 	Sdb *sdb = core->anal->sdb_types;
-	char *sizecmd = r_str_newf ("type.%s.size", k);
+	char *sizecmd = r_str_newf ("type.%s.0size", k);
 	char *size_s = sdb_querys (sdb, NULL, -1, sizecmd);
 	char *formatcmd = r_str_newf ("type.%s", k);
 	char *format_s = r_str_trim (sdb_querys (sdb, NULL, -1, formatcmd));
@@ -1249,7 +1174,7 @@ static int cmd_type(void *data, const char *input) {
 						char *out = r_parse_c_string (core->anal, tmp, &error_msg);
 						if (out) {
 							//		r_cons_strcat (out);
-							r_core_save_parsed_type (core, out);
+							r_anal_save_parsed_type (core->anal, out);
 							free (out);
 						}
 						if (error_msg) {
@@ -1263,7 +1188,7 @@ static int cmd_type(void *data, const char *input) {
 					char *out = r_parse_c_file (core->anal, filename, &error_msg);
 					if (out) {
 						//r_cons_strcat (out);
-						r_core_save_parsed_type (core, out);
+						r_anal_save_parsed_type (core->anal, out);
 						free (out);
 					}
 					if (error_msg) {
@@ -1300,7 +1225,7 @@ static int cmd_type(void *data, const char *input) {
 			char *error_msg = NULL;
 			char *out = r_parse_c_string (core->anal, tmp, &error_msg);
 			if (out) {
-				r_core_save_parsed_type (core, out);
+				r_anal_save_parsed_type (core->anal, out);
 				free (out);
 			}
 			if (error_msg) {
@@ -1585,25 +1510,7 @@ static int cmd_type(void *data, const char *input) {
 			const char *name = input + 1;
 			while (IS_WHITESPACE (*name)) name++;
 			if (*name) {
-				SdbKv *kv;
-				SdbListIter *iter;
-				const char *type = sdb_const_get (TDB, name, 0);
-				if (!type)
-					break;
-				int tmp_len = strlen (name) + strlen (type);
-				char *tmp = malloc (tmp_len + 1);
-				r_type_del (TDB, name);
-				if (tmp) {
-					snprintf (tmp, tmp_len + 1, "%s.%s.", type, name);
-					SdbList *l = sdb_foreach_list (TDB, true);
-					ls_foreach (l, iter, kv) {
-						if (!strncmp (sdbkv_key (kv), tmp, tmp_len)) {
-							r_type_del (TDB, sdbkv_key (kv));
-						}
-					}
-					ls_free (l);
-					free (tmp);
-				}
+				r_anal_remove_parsed_type (core->anal, name);
 			} else {
 				eprintf ("Invalid use of t- . See t-? for help.\n");
 			}
diff --git a/libr/include/r_anal.h b/libr/include/r_anal.h
index 4e8b1d20f53..6332045a25b 100644
--- a/libr/include/r_anal.h
+++ b/libr/include/r_anal.h
@@ -1521,6 +1521,10 @@ R_API RList* r_anal_fcn_get_vars (RAnalFunction *anal);
 R_API RList* r_anal_fcn_get_bbs (RAnalFunction *anal);
 R_API RList* r_anal_get_fcns (RAnal *anal);
 
+/* type.c */
+R_API void r_anal_remove_parsed_type(RAnal *anal, const char *name);
+R_API void r_anal_save_parsed_type(RAnal *anal, const char *parsed);
+
 /* var.c */
 R_API void r_anal_var_access_clear (RAnal *a, ut64 var_addr, int scope, int index);
 R_API int r_anal_var_access (RAnal *a, ut64 var_addr, char kind, int scope, int index, int xs_type, ut64 xs_addr);
diff --git a/libr/libs.mk b/libr/libs.mk
index 4132e8d270b..d21032d940e 100644
--- a/libr/libs.mk
+++ b/libr/libs.mk
@@ -4,7 +4,7 @@ ifeq ($(LIBS0),)
 LIBS0=util
 LIBS1=socket hash reg cons magic bp search config
 LIBS2=syscall lang io crypto flag
-LIBS3=fs anal bin parse
+LIBS3=fs parse anal bin
 LIBS4=asm
 LIBS5=egg
 LIBS6=debug
diff --git a/libr/util/ctype.c b/libr/util/ctype.c
index 60582b4fcf4..29b44dda01a 100644
--- a/libr/util/ctype.c
+++ b/libr/util/ctype.c
@@ -140,7 +140,7 @@ R_API int r_type_get_bitsize(Sdb *TDB, const char *type) {
 		return 0;
 	}
 	if (!strcmp (t, "type")){
-		query = r_str_newf ("type.%s.size", tmptype);
+		query = r_str_newf ("type.%s.0size", tmptype);
 		int r = (int)sdb_num_get (TDB, query, 0); // returns size in bits
 		free (query);
 		return r;
@@ -452,7 +452,7 @@ R_API void r_type_del(Sdb *TDB, const char *name) {
 	}
 	if (!strcmp (kind, "type")) {
 		sdb_unset (TDB, sdb_fmt ("type.%s", name), 0);
-		sdb_unset (TDB, sdb_fmt ("type.%s.size", name), 0);
+		sdb_unset (TDB, sdb_fmt ("type.%s.0size", name), 0);
 		sdb_unset (TDB, sdb_fmt ("type.%s.meta", name), 0);
 		sdb_unset (TDB, name, 0);
 	} else if (!strcmp (kind, "struct") || !strcmp (kind, "union")) {
