Added `dp` processes info support to gdbr ##debug (#15544)

Most servers/clients should have xml support by now so it should behave
like `dp` in any other debugger. vFile is the only way to get detailed pid info unfortunately.
Blindfix for #15543 - aka CVE-2019-19590
Temporariyly revert commits that break tests

Revert "Aim to fix another integer overflow in r_file_slurp"

This reverts commit b7cc6999ac0bfaff51039af960ac86b2e6bb1c91.

Revert "Fix integer overflow when assembling a 2GB file"

This reverts commit 37998eaf1acd37dcd7ee04eea6fe15cb9abcac9d.

Revert "Blindfix for #15543 - aka CVE-2019-19590"

This reverts commit 204b7317beb1ede1ba352b13f7ebb09efff1c55d.
Fix an int Overflow in r_asm_massemble() for #15543
Fix an int Overflow in r_asm_massemble() for #15543
Fix an int Overflow in r_asm_massemble() for #15543
Fix crash on oom when command line is too long. Aim to fix #15543
