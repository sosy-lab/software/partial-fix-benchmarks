`rasm2 -b64 'mov rax, 0xdeadbeef' | rasm2 -d -b64 -` doesn't round trip.
The 32bit sign bit is extended. Seems like an issue in the assembler with 64bit encoding here

> On 5 Apr 2020, at 23:54, Edd Barrett <notifications@github.com> wrote:
> 
> ﻿
> Hi,
> 
> Not sure if this is a bug or user stupidity...
> 
> Work environment
> 
> Debian amd64
> ELF
> radare2 4.4.0-git 24204 @ linux-x86-64 git.4.3.1-206-g38b8d5929
> commit: 38b8d5929d1855e7ca6b5b82956c03b46dd07eef build: 2020-04-05__22:00:27
> Expected behavior
> 
> $ rasm2 -b64 'mov rax, 0xdeadbeef' | rasm2 -d -b64 -
> mov rax, 0xdeadbeef
> Actual behavior
> 
> $ rasm2 -b64 'mov rax, 0xdeadbeef' | rasm2 -d -b64 -
> mov rax, 0xffffffffdeadbeef
> What's with the 0xffffffff prefix?
> 
> —
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub, or unsubscribe.

How do we move this one forward folks? Shall I write a test?
This still isn't quite right:
```
$ ./inst/bin/r2 -v
radare2 4.4.0-git 24317 @ openbsd-x86-64 git.4.3.1-276-g1ff6331a3
commit: 1ff6331a331d279833136baabe87492a377b33f4 build: 2020-04-11__23:22:28
$ ./inst/bin/rasm2 -b64 'mov rax, 0xdeadbeef' | ./inst/bin/rasm2 -d -b64 - 
mov rax, 0xffffffffdeadbeef
$ ./inst/bin/rasm2 -b64 'mov rax, 0x1deadbeef' | ./inst/bin/rasm2 -d -b64 - 
movabs rax, 0x1deadbeef
```

(I'm unable to re-open the bug)
Neither will round-trip.

For `mov rax, 0xdeadbeef` the assembler will assume you are doing `mov rax, -559038737` and assemble according to the MOV r/m64, imm32 variant of MOV, which sign-extends the immediate.

For `mov rax, 0x1deadbeef`, it is clear that the immediate is 64-bit and thus the MOV r64, imm64 variant is used, but current convention is to represent that variant by `movabs`.

In math, this is known as a [surjection](https://en.wikipedia.org/w/index.php?title=Surjective_function&oldid=945450727). Here, multiple assembly instructions are mapping to the same machine code. When you try to do the disassembly however, you will only get back 1 assembly instruction of the disassembler's choice.

The pr gives you the option to do `movabs rax, 0xdeadbeef` when what you want is actually the MOV r64, imm64 variant instead of the MOV r/m64, imm32 variant.
> Here, multiple assembly instructions are mapping to the same machine code

Yes, I understand that, and you are right that both encodings are valid, but I find the choice (somewhat) unexpected.

If you assemble the same in GNU assembler (which is pretty much de-facto for x86-64 on unix):
```
.intel_syntax noprefix
global main
main:
    mov rax, 0xdeadbeef
    ret
```

Then you get a `movabs` by default:
```
            ;-- main:                                                                                    
            0x00001340      48b8efbeadde.  movabs rax, 0xdeadbeef                                        
            0x0000134a      c3             ret                                                           
```

Presumably it infers this from the target register.

So would it not be better to use `movabs` in `rasm` too? One question I have -- if you did that, what syntax would you use to emit the sign extended version?


## nasm ##

Just for fun, I also tried nasm, which does *not* default to a `movabs`:
```
BITS 64
main:
    mov rax, 0xdeadbeef
    ret
```

```
            0x00000000      b8efbeadde     mov eax, 0xdeadbeef                                           
            0x00000005      c3             ret                                                           
```
> One question I have -- if you did that, what syntax would you use to emit the sign extended version?

See #16551.