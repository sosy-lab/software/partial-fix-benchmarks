[x86_64][ELF] Segmentation fault opening binary.
Interesing... ASAN does not catch anything, but a regular build segfaults.
[Here](http://178.62.54.169:8081/7XDwj/libBlocksRuntime.so.0.0.0) is another file which causes r2 to crash on opening. This may be a separate issue but I can't be bothered to create a new ticket. 
[Here](http://178.62.54.169:8081/7Ru67/libpcl_segmentation.so.1.10.1) is the original file uploaded using a more long-term host. 
> [Here](http://178.62.54.169:8081/7XDwj/libBlocksRuntime.so.0.0.0) is another file which causes r2 to crash on opening. This may be a separate issue but I can't be bothered to create a new ticket.

It doesn't crash for me with this one.
@SjRNMzU please see https://github.com/radareorg/radare2/pull/17387 . As I am not able to test your second binary, check if the PR fixes that as well for you.
I can confirm the patch fixes this issue. Both binaries do not crash using the latest master.

```
r2 ./libpcl-segmentation1.10/usr/lib/x86_64-linux-gnu/libpcl_segmentation.so.1.10.1
WARNING: parse_line_header: assertion 'hdr && bf && buf' failed (line 545)
[0x0062dcf0]>
```

Thanks!