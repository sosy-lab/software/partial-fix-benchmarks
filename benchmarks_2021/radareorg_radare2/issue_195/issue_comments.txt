Stopping analysis on jump
@cosarara97 do you remember your reasons for this code?

Yes. Basically, as I understood it, if you jump unconditionally without link, you have no way of returning, so the function is over (in that part of the file, at least) and what follows might or might not be code. If what follows is in fact code, it could perfectly be totally unrelated to that function. Although now that I look at it, the function should not stop but continue from the place jumped to. Anyway, the reason for that patch was to prevent the analysis from reading the garbage after the jump instruction as part of the function.

This was probably done to fix PLT analysis, but the function size should be different. Can you cook a test case for the regressions testsuite to ensure that we are not breaking anything?

That test must be something like

r2 -a x86 -b 32 -w -
wx .....
af
?v $f

And check output to ensure all situations work fine

On Aug 27, 2013, at 19:05, cosarara97 notifications@github.com wrote:

> Yes. Basically, as I understood it, if you jump unconditionally without link, you have no way of returning, so the function is over (in that part of the file, at least) and what follows might or might not be code. If what follows is in fact code, it could perfectly be totally unrelated to that function. Although now that I look at it, the function should not stop but continue from the place jumped to. Anyway, the reason for that patch was to prevent the analysis from reading the garbage after the jump as part of the function.
> 
> —
> Reply to this email directly or view it on GitHub.

> This was probably done to fix PLT analysis, but the function size should be different. Can you cook a test case for the regressions testsuite to ensure that we are not breaking anything?

Me or XVilka?
All I do with radare is analysis of raw ARM binaries, which are of course statically linked, so I doubt having ever done anything to do with PLT =P

Looks like reintroduced again :(

in commit 8ce16cb8a5366423f013e77d293b4575b00ec84f

Can you provide a r2 script that reproduces the bug? - write bytes, - set arch/bits - analyze - disasm/show bbs

I have rewritten the code analysis loop and added test cases in the r2-regressions suite. Fixed :)
