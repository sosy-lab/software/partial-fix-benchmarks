relocatables are wrong
A related broken test already exist https://github.com/radare/radare2-regressions/blob/master/t/cmd_info#L531

hm, can this be only elf64 bug?
compare:

```
$ ~/usr/bin/rabin2 -R /bin/ls | grep calloc
vaddr=0x080631b0 paddr=0x0001b1b0 type=SET_32 calloc
$ ~/usr/bin/rabin2 -R /bin/ls | grep malloc
vaddr=0x080630dc paddr=0x0001b0dc type=SET_32 malloc
```

and

```
$ objdump -R /bin/ls | grep calloc
080631b0 R_386_JUMP_SLOT   calloc
$ objdump -R /bin/ls | grep malloc
080630dc R_386_JUMP_SLOT   malloc
```

versus

```
$ ~/usr/bin/r2 bins/elf/hello-linux-x86_64 
 -- Please remove pregnant women, pregnant children, and pregnant pets from the monitor.
[0x00400410]> ir
[Relocations]
vaddr=0x00600cd8 paddr=0x002008c8 type=SET_64 __gmon_start__
vaddr=0x00600cf8 paddr=0x002008e8 type=SET_64 puts
vaddr=0x00600d00 paddr=0x002008f0 type=SET_64 __libc_start_main
vaddr=0x00600d08 paddr=0x002008f8 type=SET_64 __gmon_start__

4 relocations
```

and

```
$ objdump -R bins/elf/hello-linux-x86_64 

bins/elf/hello-linux-x86_64:     file format elf64-x86-64

DYNAMIC RELOCATION RECORDS
OFFSET           TYPE              VALUE 
00000000006008c8 R_X86_64_GLOB_DAT  __gmon_start__
00000000006008e8 R_X86_64_JUMP_SLOT  puts
00000000006008f0 R_X86_64_JUMP_SLOT  __libc_start_main
00000000006008f8 R_X86_64_JUMP_SLOT  __gmon_start__
```

yes, looks like just vaddr<-->paddr mapping is off by 0x410

disregard that it is not constant

can anyone test with 0.9.8? is this a regression? we should have at least a test to verify this

> On 22 May 2015, at 08:13, Jeffrey Crowell notifications@github.com wrote:
> 
> disregard that it is not constant
> 
> —
> Reply to this email directly or view it on GitHub https://github.com/radare/radare2/issues/2607#issuecomment-104533479.

nevermind :P it’s a regression and maijin pointed to the broken test. now we just have to find which was the offending commit.

> On 22 May 2015, at 12:36, pancake pancake@nopcode.org wrote:
> 
> can anyone test with 0.9.8? is this a regression? we should have at least a test to verify this
> 
> > On 22 May 2015, at 08:13, Jeffrey Crowell <notifications@github.com <mailto:notifications@github.com>> wrote:
> > 
> > disregard that it is not constant
> > 
> > —
> > Reply to this email directly or view it on GitHub https://github.com/radare/radare2/issues/2607#issuecomment-104533479.

```
$ git bisect good
6c3b50226031d1fb7836281b8afb813192570861 is the first bad commit
commit 6c3b50226031d1fb7836281b8afb813192570861
Author: Fedor Sakharov <fedor.sakharov@gmail.com>
Date:   Fri Oct 31 19:59:56 2014 +0300

    Re-implementation of relocations handling.

:040000 040000 330012fac77af34591b8b70b74ee10931a3d672d 0228584f047351a21d16739ad8f7a0ba13d1f00e M      libr
```

i'll fix this tonight

👍

> On 22 May 2015, at 22:30, Jeffrey Crowell notifications@github.com wrote:
> 
> ```$ git bisect good
> 6c3b502 is the first bad commit
> commit 6c3b502
> Author: Fedor Sakharov fedor.sakharov@gmail.com
> Date: Fri Oct 31 19:59:56 2014 +0300
> 
> Re-implementation of relocations handling.
> :040000 040000 330012fac77af34591b8b70b74ee10931a3d672d 0228584f047351a21d16739ad8f7a0ba13d1f00e M libr
> 
> i'll fix this tonight
> —
> Reply to this email directly or view it on GitHub.

Tonight (R)

My fix messed up flags for an unknown reason so I didn't commit yet

Cool!

> On 23 May 2015, at 18:27, Maijin notifications@github.com wrote:
> 
> awesome
> 
> —
> Reply to this email directly or view it on GitHub.

@crowell @radare Still a problem with R_X86_64_COPY mlssing

See http://paste.ubuntu.com/11309489/ && http://paste.ubuntu.com/11309511/

don't know if intended

let me look into this @maijin

any update on that?

`R_X86_64_GLOB_DAT` is incorrect currently, applying the same fix there breaks the cmd_pd tests due to string flags being messed up in a way i have not yet identified.

![its-magic-shia-labeouf-gif](https://cloud.githubusercontent.com/assets/917142/7849410/6eae6350-04d5-11e5-8d5c-67864c94be2a.gif)
