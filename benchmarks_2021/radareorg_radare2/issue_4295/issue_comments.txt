Spurious relocations from debugging information
that's not what nm says.. what do objdump says?

> On Mar 11 2016, at 7:06 pm, Ido Yariv &lt;notifications@github.com&gt;
> wrote:  
> 
> The following:
> 
> ```
> (gcc -m32 -x c - -c -g -o foo.o << EOF
> 
> int foo() { return 42; }
> 
> EOF
> 
> r2 -A -qc 'pdf' foo.o)
> ```
> 
> ...yields:
> 
> ```
> ╒ (fcn) entry0 10
> 
> │           0x00000034      55             push ebp
> 
> |           ;-- section_end..debug_line:
> 
> │           0x00000035  ~   89e5           mov ebp, esp
> 
> |           ;-- section_end..eh_frame:
> 
> │           0x00000037  ~   b82a000000     mov eax, 0x2a               ;
> ```
> 
> '*'  ; RELOC 32
> 
> ```
> │           0x0000003c      5d             pop ebp
> 
> ╘           0x0000003d      c3             ret
> ```
> 
> There shouldn't be a relocation at instruction 0x37 above.
> 
> —  
> Reply to this email directly or [view it on GitHub](https://github.com/radare/
> radare2/issues/4295).![](https://github.com/notifications/beacon/AA3-lpS6OvKoO
> W1gU0s17OXSnyj5m_uxks5psa8ZgaJpZM4Hu6Hc.gif)

objdump seems to behave perfectly fine:

```
(gcc -m32 -x c - -c -g -o foo.o << EOF
int foo() { return 42; }
EOF
objdump -dr foo.o)
```

results in:

```
00000000 <foo>:
   0:   55                      push   %ebp
   1:   89 e5                   mov    %esp,%ebp
   3:   b8 2a 00 00 00          mov    $0x2a,%eax
   8:   5d                      pop    %ebp
   9:   c3                      ret    
```

(the different offsets are due to https://github.com/radare/radare2/issues/4155, but seem unrelated here)

Can you share the binary and write a test for the regressions repo?

> On 11 Mar 2016, at 19:06, Ido Yariv notifications@github.com wrote:
> 
> The following:
> 
> (gcc -m32 -x c - -c -g -o foo.o << EOF
> int foo() { return 42; }
> EOF
> r2 -A -qc 'pdf' foo.o)
> ...yields:
> 
> ╒ (fcn) entry0 10
> │           0x00000034      55             push ebp
> |           ;-- section_end..debug_line:
> │           0x00000035  ~   89e5           mov ebp, esp
> |           ;-- section_end..eh_frame:
> │           0x00000037  ~   b82a000000     mov eax, 0x2a               ; '*'  ; RELOC 32 
> │           0x0000003c      5d             pop ebp
> ╘           0x0000003d      c3             ret
> There shouldn't be a relocation at instruction 0x37 above.
> 
> —
> Reply to this email directly or view it on GitHub.

Sure, https://github.com/radare/radare2-regressions/pull/332

Should be fixed in https://github.com/radare/radare2/pull/4307 can you confirm? will be good to confirm with the testsuite, and other binaries to avoid having regressions
