Radare offers to kill process even if it failed to attach to process.
The problem is that I don't want to kill the process,  but out of habit I pressing `Enter` key.

pressing 'n' is the same as detach

and kill is by default for security reasons.

but yep. if attach has failed this shuoldnt be asked, this is just a if
(pid != -1)

On 04/20/2016 05:57 PM, Andrey Torsunov wrote:

> radare2 0.10.2 11010 @ linux-little-x86-64 git.0.10.2-50-ga1eb929
> commit: a1eb92966e6769f96fe2d9d78da96f59cc299a01 build: 2016-04-20
> 
> ```
> ptrace_attach: Operation not permitted
> PIDPATH: /usr/lib/jvm/java-8-jdk/jre/bin/java
> attach 6365 6365
> bin.baddr 0x00400000
> Assuming filepath /usr/lib/jvm/java-8-jdk/jre/bin/java
> asm.bits 64
> r_debug_reg: error reading registers
> r_debug_reg: error reading registers
> r_debug_reg: error reading registers
>  -- Trust no one, nor a zero. Both lie.
> r_debug_reg: error reading registers
> [0x00000000]> q
> Do you want to quit? (Y/n)
> Do you want to kill the process? (Y/n) <------------| Why not just detach from process like in GDB?
> ```
> 
> ---
> 
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly or view it on GitHub:
> https://github.com/radare/radare2/issues/4670

> but yep. if attach has failed this shuoldnt be asked, this is just a if (pid != -1)

If you talking about `r.dbg->pid`, then it's not `-1`. 

> and kill is by default for security reasons.

Why not make it configurable?

Configurable??

> On 21 Apr 2016, at 11:10, Andrey Torsunov notifications@github.com wrote:
> 
> but yep. if attach has failed this shuoldnt be asked, this is just a if (pid != -1)
> 
> If you told about r.dbg->pid, then it's not -1.
> 
> and kill is by default for security reasons.
> 
> Why not make it configurable?
> 
> —
> You are receiving this because you commented.
> Reply to this email directly or view it on GitHub

You have scr.interactive if you dont want to be asked

> On 21 Apr 2016, at 11:10, Andrey Torsunov notifications@github.com wrote:
> 
> but yep. if attach has failed this shuoldnt be asked, this is just a if (pid != -1)
> 
> If you told about r.dbg->pid, then it's not -1.
> 
> and kill is by default for security reasons.
> 
> Why not make it configurable?
> 
> —
> You are receiving this because you commented.
> Reply to this email directly or view it on GitHub

If I set `scr.interactive=0`, then process will be killed without prompt: https://github.com/radare/radare2/blob/master/binr/radare2/radare2.c#L915 

> Configurable??

Yeah, something like `dbg.killporcessonexit=0`

 I thought, `r_debug_attach` looks like a good place for setting `-1` into `dbg->pid` and `dbg->tid`..
But [r_debug_native_attach](https://github.com/radare/radare2/blob/6115d68736882f8f7f5249e9d731d9e8615ff1a4/libr/debug/p/debug_native.c#L166) possibly never returns `-1` (for linux), so [condition at r_debug_atttach](https://github.com/radare/radare2/blob/6115d68736882f8f7f5249e9d731d9e8615ff1a4/libr/debug/debug.c#L135) will never be false.  Or am I wrong?

Yep thats the place

> On 21 Apr 2016, at 18:00, Andrey Torsunov notifications@github.com wrote:
> 
> r_debug_native_attach possibly never returns -1 (for linux), so condition at r_debug_atttach will never be false. I thought, r_debug_attach looks like a good place for setting -1 into dbg->pid and dbg->tid.. Or am I wrong?
> 
> —
> You are receiving this because you commented.
> Reply to this email directly or view it on GitHub

Maybe dbg.exitkills=true by default

Makes sense to have it

> On 21 Apr 2016, at 14:43, Andrey Torsunov notifications@github.com wrote:
> 
> Configurable??
> 
> Yeah, something like dbg.killporcessonexit=0
> 
> —
> You are receiving this because you commented.
> Reply to this email directly or view it on GitHub
