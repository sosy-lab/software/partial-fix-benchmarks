Problem with wtf! command
This issue is not related with the PR.
in virtual addressing mode there's no such concept as file size
If I move the position, it works fine:
```
$ r2 /bin/ls
[0x00404890]> s 0
[0x00000000]> wtf!
dumped 0x1ae00 bytes
Dumped 110080 bytes from 0x00000000 into dump.0x00000000
```
bear in mind that in VA there's no linear memory readable, so you cant dump the original file contents to disk using wtf! when in io.va. I have fixed the negative dumping issue. Please test and let me know if you find other issues or how would you expect it to behave .thanks
The output isn't as expected:
```
$ r2 /bin/ls
[0x00404890]> wtf!
Invalid length -4102800
Dumped -4102800 bytes from 0x00404890 into dump.0x00404890
[0x00404890]> s 0
[0x00000000]> wtf!
dumped 0x1ae00 bytes
Dumped 110080 bytes from 0x00000000 into dump.0x00000000
[0x00000000]> 
```
The issue with negative memory is "fixed". It must not output the line "Dumped -4102800 bytes from 0x00404890 into dump.0x00404890" because doesn't dump anything. You said there is no lineal memory readable, but if I move (seek) at 0 and wtf! , it dumps the complete file (/bin/ls) and the hashes of dump and original file are the same. Maybe it's a misconcept by my-self, sorry.
That negative value is because we are missing physical address with virtual address. In order to know how much to write from the file it uses `core->offset`. That offset should be translated to the physical correspondence in order to get what you specify. I would say there was something in the code base to do it. Sometimes i forget was is done on siol and what in master

```
sz = r_io_desc_size (core->io, core->file->desc) - core->offset;
```
Pushed into master. But bear in mind you are dumping from the entrypoint, not from the beginning of the file, thereby if the `vaddr` is not backed up for the `paddr` you are screwed. If instead you now the file's size from the current position just use `wtf filename size` and it will use vaddr.
Commit https://github.com/radare/radare2/commit/fe6aba1fbd7423fd5e3693de0b18fb3b46d0d4d4