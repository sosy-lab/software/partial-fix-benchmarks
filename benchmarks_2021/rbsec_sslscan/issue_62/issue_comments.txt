Feature Request - cipher suites ordered by server preference
This is an interesting feature request. I had a play around with some code to do this last night, and ended up rewriting the main ciphersuite scanning logic.

Previously ciphersuite scanning was performed by generating a list of all the supported ciphers on teh client, and then trying to connect with each of these individual ciphers one-by-one, and displaying which ones were accepted.

The new scanning method is to try and connect with all ciphersuites enabled (ALL:eNULL), and then each time the server accepts a connection, blacklist that cipher and try to connect again until the connection fails. This means that the ciphers in the main scanning phase will be in the order that the server chooses (or if the server doesn't have a preference, in the client's order).

This also has the side effect of making the ciphersuite scanning ~5x faster (depending on how many ciphers the server supports).

The code is still a bit experimental, it's currently in the [cipher_order branch](https://github.com/rbsec/sslscan/tree/cipher_order) and I'd like to do some more testing before merging it into master, so please give it a test and see if it's working fine for you.

There are a few more things I'd like to look at as well, like detecting if the server has no preference and notifying the user.

Awesome will give it a shot in some time to see how the scanning performs.

but nevertheless awesome logic.

Edit: Could'nt wait longer.
Here are quick results.
Commandline used: `--no-renegotiation --no-compression --no-heartbleed --no-cipher-details --no-preferred`

```
  Supported Server Cipher(s):
Accepted  TLSv1.0  256 bits  ECDHE-ECDSA-AES256-SHA
Accepted  TLSv1.0  128 bits  ECDHE-ECDSA-AES128-SHA
Accepted  TLSv1.0  112 bits  ECDHE-ECDSA-DES-CBC3-SHA
Accepted  TLSv1.1  256 bits  ECDHE-ECDSA-AES256-SHA
Accepted  TLSv1.1  128 bits  ECDHE-ECDSA-AES128-SHA
Accepted  TLSv1.1  112 bits  ECDHE-ECDSA-DES-CBC3-SHA
Accepted  TLSv1.2  256 bits  ECDHE-ECDSA-AES256-GCM-SHA384
Accepted  TLSv1.2  256 bits  ECDHE-ECDSA-AES256-SHA384
Accepted  TLSv1.2  256 bits  ECDHE-ECDSA-AES256-SHA
Accepted  TLSv1.2  128 bits  ECDHE-ECDSA-AES128-GCM-SHA256
Accepted  TLSv1.2  128 bits  ECDHE-ECDSA-AES128-SHA256
Accepted  TLSv1.2  128 bits  ECDHE-ECDSA-AES128-SHA
Accepted  TLSv1.2  112 bits  ECDHE-ECDSA-DES-CBC3-SHA
```

Old code
`real    0m9.593s
user    0m0.164s
sys     0m0.056s`

New code
`real    0m0.468s
user    0m0.028s
sys     0m0.004s`

Another test results

```
  Supported Server Cipher(s):
Accepted  TLSv1.0  256 bits  AES256-SHA
Accepted  TLSv1.0  128 bits  AES128-SHA
Accepted  TLSv1.0  112 bits  DES-CBC3-SHA
Accepted  TLSv1.2  256 bits  AES256-SHA256
Accepted  TLSv1.2  256 bits  AES256-SHA
Accepted  TLSv1.2  128 bits  AES128-SHA256
Accepted  TLSv1.2  128 bits  AES128-SHA
Accepted  TLSv1.2  112 bits  DES-CBC3-SHA
```

Old

```
real    2m14.401s
user    0m0.196s
sys     0m0.052s
```

New

```
real    0m6.789s
user    0m0.016s
sys     0m0.012s
```

I would say massive improvement

Made a few more changes based around this. Since we're already showing the ciphers in order of server preference (if it has one), it's redundant to have a separate preferred ciphers section in the output. Output will now look something like this:

```
  Supported Server Cipher(s):
Preferred TLSv1.0  256 bits  ECDHE-RSA-AES256-SHA          Curve P-521 DHE 521
Accepted  TLSv1.0  128 bits  ECDHE-RSA-AES128-SHA          Curve P-521 DHE 521
Accepted  TLSv1.0  256 bits  AES256-SHA                   
Accepted  TLSv1.0  128 bits  AES128-SHA                   
Accepted  TLSv1.0  112 bits  DES-CBC3-SHA                 
Preferred TLSv1.1  256 bits  ECDHE-RSA-AES256-SHA          Curve P-521 DHE 521
Accepted  TLSv1.1  128 bits  ECDHE-RSA-AES128-SHA          Curve P-521 DHE 521
Accepted  TLSv1.1  256 bits  AES256-SHA                   
Accepted  TLSv1.1  128 bits  AES128-SHA                   
Accepted  TLSv1.1  112 bits  DES-CBC3-SHA                 
Preferred TLSv1.2  256 bits  ECDHE-RSA-AES256-SHA384       Curve P-521 DHE 521
Accepted  TLSv1.2  256 bits  ECDHE-RSA-AES256-SHA          Curve P-521 DHE 521
Accepted  TLSv1.2  128 bits  ECDHE-RSA-AES128-SHA256       Curve P-521 DHE 521
Accepted  TLSv1.2  128 bits  ECDHE-RSA-AES128-SHA          Curve P-521 DHE 521
```

XML format has also had similar changes:

``` xml
 <ssltest host="256.256.256.256" port="443">
  <cipher status="preferred" sslversion="TLSv1.0" bits="256" cipher="ECDHE-RSA-AES256-SHA" curve="P-521" ecdhebits="521" />
  <cipher status="accepted" sslversion="TLSv1.0" bits="128" cipher="ECDHE-RSA-AES128-SHA" curve="P-521" ecdhebits="521" />
  <cipher status="accepted" sslversion="TLSv1.0" bits="256" cipher="AES256-SHA" />
  <cipher status="accepted" sslversion="TLSv1.0" bits="128" cipher="AES128-SHA" />
  <cipher status="accepted" sslversion="TLSv1.0" bits="112" cipher="DES-CBC3-SHA" />
  <cipher status="preferred" sslversion="TLSv1.1" bits="256" cipher="ECDHE-RSA-AES256-SHA" curve="P-521" ecdhebits="521" />
  <cipher status="accepted" sslversion="TLSv1.1" bits="128" cipher="ECDHE-RSA-AES128-SHA" curve="P-521" ecdhebits="521" />
  <cipher status="accepted" sslversion="TLSv1.1" bits="256" cipher="AES256-SHA" />
  <cipher status="accepted" sslversion="TLSv1.1" bits="128" cipher="AES128-SHA" />
  <cipher status="accepted" sslversion="TLSv1.1" bits="112" cipher="DES-CBC3-SHA" />
  <cipher status="preferred" sslversion="TLSv1.2" bits="256" cipher="ECDHE-RSA-AES256-SHA384" curve="P-521" ecdhebits="521" />
  <cipher status="accepted" sslversion="TLSv1.2" bits="256" cipher="ECDHE-RSA-AES256-SHA" curve="P-521" ecdhebits="521" />
  <cipher status="accepted" sslversion="TLSv1.2" bits="128" cipher="ECDHE-RSA-AES128-SHA256" curve="P-521" ecdhebits="521" />
```

This may break some scripts that parse the output (either the XML or text output), so it'll be version 1.11.0 when it's released.

I suppose with this change or somewhere in between we have lost the output where we showed which cipher failed.

Can we bring back that feature may be dump all non accepted ciphers in one shot making it quick.

You guys work fast, impressive. Tried to test it out and I'm getting a seg fault on the latest ubuntu with OpenSSL 1.0.1f (tried a few sites):

Version: 1.10.6-rbsec-12-g1c2721d
OpenSSL 1.0.1f 6 Jan 2014

OpenSSL version does not support SSLv2
SSLv2 ciphers will not be detected

Some servers will fail to response to SSLv3 ciphers over STARTTLS
If your scan hangs, try using the --tlsall option

Testing SSL server yahoo.com on port 443

  TLS renegotiation:
OpenSSL OpenSSL 1.0.1f 6 Jan 2014 looks like version 0.9.8m or later; I will try SSL_OP to enable renegotiation
Segmentation fault (core dumped)

you seem to have used the native openssl try using static build.

```
make static
```

Did a quick check and all seems to work fine on static build.

Yep, that was it. Works quite well, even on a tricky site like dh2048.badssl.com (which has a config issue with SSLv3 and SNI at the moment)

I'll do a bit more testing but so far it's brilliant.

@fishouttah20 
Segfault should be fixed. It was caused by the SSL_CTX_set_options() call inside tls_reneg_init(), which is called before options->ctx is actually initialised. Not quite sure how that was ever working in the first place (or why it's only broken on some SSL versions). I've tested it on an Ubuntu 15.4 system (OpenSSL 1.0.1f-1ubuntu11) - but please let me know if it works.

@anantshri 
With the new way of detecting valid ciphers there isn't a simple way to output all the ciphers that the server didn't support. You can display a full list of client-supported ciphers with the --show-ciphers option (which I'd never or noticed before this issue), so I suppose I could take that list and then remove ciphers one at time when they're detected. Will get a bit complex with the different SSL/TLS versions not supporting (some of) the same ciphers. It'll also dump out a massive list of unsupported ciphers, which isn't very friendly (slightly more so if it's in the XML).

To be honest, I've never felt that the output showing unsupported ciphers is actually very useful. That's actually the original reason that this fork of sslscan was created - the very first commit I made was to hide failed ciphers by default in the output. If I'm scanning a server, it's because I want to know what ciphers it does support, rather than a massive list of all the ciphers it doesn't - especially given that this list will potentially vary a lot depending on your version of OpenSSL. The static version currently has ~500 ciphers, some of which are supported on multiple protocols, so you're potentlaly looking at 1500+ "failed" ciphers.

I'll have a think to see if there's a nice way to do this, but to be honest I don't really consider it a very important feature, and I think the performance gains are worth dropping it.

Yep, Segfault fixed on my end with a local version of OpenSSL, though I didn't realize building the static version was so easy - I'll be using that from now on. ...now to tackle cross compiling it for Win...

The performance is impressive. To do an exhaustive list (-E) with testssl.sh is significantly slower (and not in server preference order)

Just to be difficult, is there a logic reason for checking TLSv1 first? Just thinking as the server prefers newer protocols first, a preference order would start with TLSv1.2 then 1.1, 1.0...etc.

I'm tickled pink with this feature regardless so closing this. Thanks again. Have a pint on me.

Static building is much better, because it means that you can get the SSLv2/SSLv3 ciphers without having to have a vulnerable version of OpenSSL installed systemwide. Windows building is...a PITA. There are some instructions in the INSTALL file which may work (good luck).

There's a little more testing I want to do before merging this into master, so I'm re-opening this issue until it's been merged. I also need to update the docs/changelog/etc.

The TLS/SSL version order is purely a legacy thing (the original version did SSLv2, then SSLv3 then TLSv1.0 - no newer versions). With the new ciphersuite order it might make more sense to try the newest protocols first and then work backwards from there. There are a few disruptive output changes in this branch anyway, so might be a good time to change it.

I was also thinking about adding this kind of logic for the SSL cert stuff - currently it just tries TLSv1.0 (or whatever version you specified), and errors if that's not supported. Now site are starting to drop TLSv1.0 this is probably worth changing, and it makes sense to try the highest versions first there.

This has now been merged in caeb03f9fb9ee2f94150ef6fb35fd1e7244d3708.

Amazing how a small suggestion can turn into a major rewrite.

I imagine all these changes have probably broken something (other than removing the --failed option), so please let me know if you run into any issue.

~rbsec

Brilliant, thanks again!
