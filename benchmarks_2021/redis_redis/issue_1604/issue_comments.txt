full disk ---> 0 size dump.rdb. all data lost.
Are you sure? Redis does this:
- Writes data to RDB file with a temporary name (the other file is not touched).
- Try to rename(2) the new file to the old one.

So in theory what you described should never happen.

I had a problem with a system crash this week. After reboot and cleanup (full disk) redis database
was 0 size. Today I tried to recreate the scenario  - and that is what happened. It is easy, just put
dump.rdb to a temp fs, fill it up and issue shutdown.

With a modern Redis (2.8.x), when I fill up a tiny filesystem and try to `SHUTDOWN`, I get:

Client:

``` haskell
127.0.0.1:6379> shutdown
(error) ERR Errors trying to SHUTDOWN. Check logs.
127.0.0.1:6379> save
(error) ERR
```

Server:

``` haskell
[53285] 13 Mar 11:11:45.130 # User requested shutdown...
[53285] 13 Mar 11:11:45.130 * Saving the final RDB snapshot before exiting.
[53285] 13 Mar 11:11:45.130 # Failed opening .rdb for saving: No space left on device
[53285] 13 Mar 11:11:45.130 # Error trying to save the DB, can't exit.
[53285] 13 Mar 11:12:00.394 # Failed opening .rdb for saving: No space left on device
[53285] 13 Mar 11:12:10.899 # User requested shutdown...
[53285] 13 Mar 11:12:10.899 # Redis is now ready to exit, bye bye...
```

If I go back to 2.4.14 (almost two years old by now), I get the same behavior:

Client:

``` haskell
redis 127.0.0.1:6379> shutdown
(error) ERR Errors trying to SHUTDOWN. Check logs.
```

Server:

``` haskell
[53689] 13 Mar 11:19:03 # User requested shutdown...
[53689] 13 Mar 11:19:03 * Saving the final RDB snapshot before exiting.
[53689] 13 Mar 11:19:03 # Failed saving the DB: No space left on device
[53689] 13 Mar 11:19:03 # Error trying to save the DB, can't exit.
```

In both cases Redis can't save the RDB on exit, so it refuses to exit with a normal `SHUTDOWN`.  (Though, this was tested on OS X and it's possible under Linux you run into a different edge case.)

Do you have a set of commands to reproduce your behavior?  (create tmpfs, start redis-server, write, save, fill up tmpfs, shutdown redis, start redis, now there's no data?)

Some file system recovery processes can zero out files, especially since you mentioned a cleanup after a reboot.

Set of commands:

```
    luba:/tmp/redis# ls -l
    total 1
    -rw-rw---- 1 redis redis 295 Mar 13 16:52 dump.rdb
    luba:/tmp/redis# df -h /tmp/redis
    Filesystem            Size  Used Avail Use% Mounted on
    /dev/mapper/luba-tmp  368M   13M  337M   4% /tmp
    luba:/tmp/redis# yes 33333333333333333333 > /tmp/fillup.txt
    yes: standard output: No space left on device
    yes: write error
    luba:/tmp/redis# df -h /tmp/redis
    Filesystem            Size  Used Avail Use% Mounted on
    /dev/mapper/luba-tmp  368M  368M     0 100% /tmp
    luba:/tmp/redis# /etc/init.d/redis-server start
    Starting redis-server: redis-server.
    luba:/tmp/redis# redis-cli
    redis 127.0.0.1:6379> shutdown
    redis 127.0.0.1:6379> quit
    luba:/tmp/redis# ls -l
    total 0
    -rw-rw---- 1 redis redis 0 Mar 13 16:56 dump.rdb
    luba:/tmp/redis#
```

Server log:

```
[15781] 13 Mar 16:55:49 * Server started, Redis version 2.4.14
[15781] 13 Mar 16:55:49 # WARNING overcommit_memory is set to 0! Background save may fail under low memory condition. To fix this issue add 'vm.overcommit_memory = 1' to /etc/sysctl.conf and then reboot or run the command 'sysctl vm.overcommit_memory=1' for this to take effect.
[15781] 13 Mar 16:55:49 * DB loaded from disk: 0 seconds
[15781] 13 Mar 16:55:49 * The server is now ready to accept connections on port 6379
[15781] 13 Mar 16:56:03 # User requested shutdown...
[15781] 13 Mar 16:56:03 * Saving the final RDB snapshot before exiting.
[15781] 13 Mar 16:56:03 * DB saved on disk
[15781] 13 Mar 16:56:03 * Removing the pid file.
[15781] 13 Mar 16:56:03 # Redis is now ready to exit, bye bye...
```

I confirmed this does fail under Linux on a modern Redis (unstable HEAD at the moment).  I think you hit "Comment and Close" by mistake instead of just "Comment" with your last post.

Tested on a generic vagrant vm:

``` haskell
vagrant@precise32:~/q$ lsb_release -a
No LSB modules are available.
Distributor ID: Ubuntu
Description:    Ubuntu 12.04 LTS
Release:    12.04
Codename:   precise
```

Steps:
- `mkdir q; sudo mount -t tmpfs -o size=20m tmpfs q`
- `redis-server`
- [different shell] `redis-cli` [add some things; `save`; verify redis.rdb has data]
- [fill up remaining space in your tmpfs mount] `cd q; dd if=/dev/zero of=here`
- [back to redis-cli] `shutdown`
- shutdown completes, but now redis.rdb now has zero size.

Before:

``` erlang
vagrant@precise32:~/q$ ls -latrh
total 8.0K
drwxr-xr-x 6 vagrant vagrant 4.0K Mar 13 16:13 ..
-rw-rw-r-- 1 vagrant vagrant   62 Mar 13 16:14 dump.rdb
drwxrwxrwt 2 root    root      60 Mar 13 16:14 .
```

After:

``` haskell
[5663] 13 Mar 16:16:31.671 # User requested shutdown...
[5663] 13 Mar 16:16:31.672 * Saving the final RDB snapshot before exiting.
[5663] 13 Mar 16:16:31.672 * DB saved on disk
[5663] 13 Mar 16:16:31.672 # Redis is now ready to exit, bye bye...
```

``` erlang
vagrant@precise32:~/q$ ls -latrh
total 20M
drwxr-xr-x 6 vagrant vagrant 4.0K Mar 13 16:13 ..
-rw-rw-r-- 1 vagrant vagrant  20M Mar 13 16:14 here
-rw-rw-r-- 1 vagrant vagrant    0 Mar 13 16:16 dump.rdb
drwxrwxrwt 2 root    root      80 Mar 13 16:16 .
```

Fixed!  I'll add a pull request soon.

Thanks so much for reporting this data loss error.

``` haskell
127.0.0.1:6379> save
(error) ERR
127.0.0.1:6379> shutdown
(error) ERR Errors trying to SHUTDOWN. Check logs.
```

``` haskell
[13630] 13 Mar 18:36:32.630 # Write error saving DB on disk: No space left on device
[13630] 13 Mar 18:36:36.974 # User requested shutdown...
[13630] 13 Mar 18:36:36.974 * Saving the final RDB snapshot before exiting.
[13630] 13 Mar 18:36:36.974 # Write error saving DB on disk: No space left on device
[13630] 13 Mar 18:36:36.974 # Error trying to save the DB, can't exit.
```

Sorry about closing - was a bit in a hurry. It fails in the same way on ext4, ext3 and tmpfs all on linux.

I am new to this. Commenting just to leave it closed.
