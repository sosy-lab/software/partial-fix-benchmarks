shell `ping6` doesn't display duplicate packages
I think there is no specific reason, other than a lazy approach to handling of packets... @OlegHahm according to `git blame` that error message is yours. Do you remember?
Well, I remember that it would require a rather big rewrite of the ping command handler in order to properly detect and handle duplicates. So, in fact you're correct: laziness is the main cause for this behavior.
> […] it would require a rather big rewrite of the ping command handler […] 

You mean something like #9523? 🙃