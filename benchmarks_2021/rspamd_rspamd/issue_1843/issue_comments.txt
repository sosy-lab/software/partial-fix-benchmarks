per_user bayes and redis: LUA script not executed
@vstakhov  @fatalbanana I upgraded to the rspamd 1.6.5 release this night, however, it seems that the classifier scripts are still not loaded. Is there anything I need to change with my configuration?
It should work as documented but I'm afraid it does not.
Any news on this?
It seems to be working as expected now (ie in 1.6.6).