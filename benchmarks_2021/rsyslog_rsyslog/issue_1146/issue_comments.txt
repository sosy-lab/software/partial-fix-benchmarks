Use unsigned int when reporting actual rcvbuf sizes in debug mode
We need to carefully check the socket API:

http://man7.org/linux/man-pages/man7/socket.7.html

It says that the parameter for SO_RECVBUF must be int. So IMHO we cannot simply change it to unsigned, as done e.g. in #1217 (I am not posting it there as I am not sure if the PR again gets deleted). On the other hand, the bug report indicates that int is insufficient, so there seems to be a mismatch between theory (man 7 socket) and practice... We need to find what is right, and **then** can apply a proper patch based on that knowledge.

is it actually reasonable to set the receive buffer size to something over 2G
would we possibly be better off just adding a test to prohibit values that are too large.

2G is a LOT of messages.
in any case, it's not supported by the OS API as said above. So I agree the right thing to do is probably to not permit more than 2G in the first place.
Whatever protects from displaying incorrect value is reasonable IMHO, so either limit or unsigned int, or some logic that would catch overflown values.
This thread has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.