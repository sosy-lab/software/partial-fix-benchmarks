maintain ChangeLog
bugfix: rsyslog identifies itself as "liblogging-stdlog" in internal messages

This occured when liblogging-stdlog was used, and was used by default (without
explicit configuration). This is a regression of the new default, which does
not correctly call stdlog_open() in the default case.

closes https://github.com/rsyslog/rsyslog/issues/1442
bugfix: rsyslog identifies itself as "liblogging-stdlog" in internal messages

This occured when liblogging-stdlog was used, and was used by default (without
explicit configuration). This is a regression of the new default, which does
not correctly call stdlog_open() in the default case.

closes https://github.com/rsyslog/rsyslog/issues/1442
bugfix: rsyslog identifies itself as "liblogging-stdlog" in internal messages

This occured when liblogging-stdlog was used, and was used by default (without
explicit configuration). This is a regression of the new default, which does
not correctly call stdlog_open() in the default case.

closes https://github.com/rsyslog/rsyslog/issues/1442
bugfix: rsyslog identifies itself as "liblogging-stdlog" in internal messages

This occured when liblogging-stdlog was used, and was used by default (without
explicit configuration). This is a regression of the new default, which does
not correctly call stdlog_open() in the default case.

closes https://github.com/rsyslog/rsyslog/issues/1442
