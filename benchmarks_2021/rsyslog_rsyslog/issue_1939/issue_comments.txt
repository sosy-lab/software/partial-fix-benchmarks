RainerScript: previous action is suspended
Since I could not find any existing solution I created following patch which is simple and works.
But considering performance is probably not the best way to do it. Any suggestions regarding this?
Also this should be configurable and optional.
I will submit a proper PR if there is any chance that such a functionality will be merged.

```diff
diff --git a/action.c b/action.c
index 4e467ee8b..2d829d852 100644
--- a/action.c
+++ b/action.c
@@ -1615,6 +1615,7 @@ doSubmitToActionQ(action_t * const pAction, wti_t * const pWti, smsg_t *pMsg)
        }
        pWti->execState.bPrevWasSuspended
                = (iRet == RS_RET_SUSPENDED || iRet == RS_RET_ACTION_FAILED);
+       msgAddJSON(pMsg, (uchar*)"!PreviousActionIsSuspended", json_object_new_boolean(pWti->execState.bPrevWasSuspended), 0, 0);

        if (iRet == RS_RET_ACTION_FAILED)       /* Increment failed counter */
                STATSCOUNTER_INC(pAction->ctrFail, pAction->mutCtrFail);
```

Usage in config:
```
action(...)
if $!PreviousActionIsSuspended then {
 # ... e.g. call failover
}
```

Sorry for the late reply. I am wrangling with the large number of issues and I lost this one :-(

The suggested solution looks good from a logical PoV, but performance is probably not very good. I even added related issue tracker #1978. It's not the same, but probably something we can look at in one go.
We should double-check how the action engine keeps this state. Best if we can reuse some of that.
details on upcoming solution proposal in #1978
This thread has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.