diff --git a/runtime/wti.c b/runtime/wti.c
index 83696fbfd1..8f84ca7037 100644
--- a/runtime/wti.c
+++ b/runtime/wti.c
@@ -9,7 +9,7 @@
  * (and in the web doc set on http://www.rsyslog.com/doc). Be sure to read it
  * if you are getting aquainted to the object.
  *
- * Copyright 2008-2017 Adiscon GmbH.
+ * Copyright 2008-2018 Adiscon GmbH.
  *
  * This file is part of the rsyslog runtime library.
  *
@@ -62,8 +62,8 @@ pthread_key_t thrd_wti_key;
 /* get the header for debug messages
  * The caller must NOT free or otherwise modify the returned string!
  */
-static uchar *
-wtiGetDbgHdr(wti_t *pThis)
+uchar * ATTR_NONNULL()
+wtiGetDbgHdr(const wti_t *const pThis)
 {
 	ISOBJ_TYPE_assert(pThis, wti);
 
@@ -83,6 +83,28 @@ wtiGetState(wti_t *pThis)
 	return ATOMIC_FETCH_32BIT(&pThis->bIsRunning, &pThis->mutIsRunning);
 }
 
+/* join terminated worker thread
+ * This may be called in any thread state, it will be a NOP if the
+ * thread is not to join.
+ */
+void ATTR_NONNULL()
+wtiJoinThrd(wti_t *const pThis)
+{
+	ISOBJ_TYPE_assert(pThis, wti);
+	if(wtiGetState(pThis) == WRKTHRD_WAIT_JOIN) {
+		DBGPRINTF("%s: joining terminated worker\n", wtiGetDbgHdr(pThis));
+		if(pthread_join(pThis->thrdID, NULL) != 0) {
+			LogMsg(errno, RS_RET_INTERNAL_ERROR, LOG_WARNING,
+				"rsyslog bug? wti cannot join terminated wrkr");
+		}
+		DBGPRINTF("%s: worker fully terminated\n", wtiGetDbgHdr(pThis));
+		wtiSetState(pThis, WRKTHRD_STOPPED);
+		if(dbgTimeoutToStderr) {
+			fprintf(stderr, "rsyslog debug: %s: thread joined\n",
+				wtiGetDbgHdr(pThis));
+		}
+	}
+}
 
 /* Set this thread to "always running" state (can not be unset)
  * rgerhards, 2009-07-20
@@ -122,7 +144,6 @@ wtiWakeupThrd(wti_t *pThis)
 
 	ISOBJ_TYPE_assert(pThis, wti);
 
-
 	if(wtiGetState(pThis)) {
 		/* we first try the cooperative "cancel" interface */
 		pthread_kill(pThis->thrdID, SIGTTIN);
@@ -149,20 +170,22 @@ wtiCancelThrd(wti_t *pThis, const uchar *const cancelobj)
 
 	ISOBJ_TYPE_assert(pThis, wti);
 
-	if(wtiGetState(pThis)) {
+	wtiJoinThrd(pThis);
+	if(wtiGetState(pThis) != WRKTHRD_STOPPED) {
 		LogMsg(0, RS_RET_ERR, LOG_WARNING, "%s: need to do cooperative cancellation "
 			"- some data may be lost, increase timeout?", cancelobj);
 		/* we first try the cooperative "cancel" interface */
 		pthread_kill(pThis->thrdID, SIGTTIN);
 		DBGPRINTF("sent SIGTTIN to worker thread %p, giving it a chance to terminate\n",
 			(void *) pThis->thrdID);
-		srSleep(0, 10000);
+		srSleep(0, 50000);
+		wtiJoinThrd(pThis);
 	}
 
-	if(wtiGetState(pThis)) {
+	if(wtiGetState(pThis) != WRKTHRD_STOPPED) {
 		LogMsg(0, RS_RET_ERR, LOG_WARNING, "%s: need to do hard cancellation", cancelobj);
 		if(dbgTimeoutToStderr) {
-			fprintf(stderr, "rsyslogd debug: %s: need to do hard cancellation\n",
+			fprintf(stderr, "rsyslog debug: %s: need to do hard cancellation\n",
 				cancelobj);
 		}
 		pthread_cancel(pThis->thrdID);
@@ -176,6 +199,7 @@ wtiCancelThrd(wti_t *pThis, const uchar *const cancelobj)
 		}
 	}
 
+	wtiJoinThrd(pThis);
 	RETiRet;
 }
 
@@ -211,6 +235,16 @@ wtiNewIParam(wti_t *const pWti, action_t *const pAction, actWrkrIParams_t **pipa
 /* Destructor */
 BEGINobjDestruct(wti) /* be sure to specify the object type also in END and CODESTART macros! */
 CODESTARTobjDestruct(wti)
+	if(wtiGetState(pThis) != WRKTHRD_STOPPED) {
+		DBGPRINTF("%s: rsyslog bug: worker not stopped during shutdown\n",
+			wtiGetDbgHdr(pThis));
+		if(dbgTimeoutToStderr) {
+			fprintf(stderr, "RSYSLOG BUG: %s: worker not stopped during shutdown\n",
+				wtiGetDbgHdr(pThis));
+		} else {
+			assert(wtiGetState(pThis) == WRKTHRD_STOPPED);
+		}
+	}
 	/* actual destruction */
 	batchFree(&pThis->batch);
 	free(pThis->actWrkrInfo);
@@ -417,7 +451,7 @@ wtiWorker(wti_t *__restrict__ const pThis)
 	/* indicate termination */
 	pthread_cleanup_pop(0); /* remove cleanup handler */
 	pthread_setcancelstate(iCancelStateSave, NULL);
-	dbgprintf("wti %p: worker exiting\n", pThis);
+	dbgprintf("wti %p: exiting\n", pThis);
 
 	RETiRet;
 }
diff --git a/runtime/wti.h b/runtime/wti.h
index 5f12dfbf8e..43072ecb5b 100644
--- a/runtime/wti.h
+++ b/runtime/wti.h
@@ -95,7 +95,9 @@ rsRetVal wtiConstructFinalize(wti_t * const pThis);
 rsRetVal wtiDestruct(wti_t **ppThis);
 rsRetVal wtiWorker(wti_t * const pThis);
 rsRetVal wtiSetDbgHdr(wti_t * const pThis, uchar *pszMsg, size_t lenMsg);
+uchar * ATTR_NONNULL() wtiGetDbgHdr(const wti_t *const pThis);
 rsRetVal wtiCancelThrd(wti_t * const pThis, const uchar *const cancelobj);
+void ATTR_NONNULL() wtiJoinThrd(wti_t *const pThis);
 rsRetVal wtiSetAlwaysRunning(wti_t * const pThis);
 rsRetVal wtiSetState(wti_t * const pThis, int bNew);
 rsRetVal wtiWakeupThrd(wti_t * const pThis);
diff --git a/runtime/wtp.c b/runtime/wtp.c
index c407c863bc..f48ffb3b9f 100644
--- a/runtime/wtp.c
+++ b/runtime/wtp.c
@@ -105,7 +105,6 @@ BEGINobjConstruct(wtp) /* be sure to specify the object type also in END macro!
 	pthread_attr_setschedparam(&pThis->attrThrd, &default_sched_param);
 	pthread_attr_setinheritsched(&pThis->attrThrd, PTHREAD_EXPLICIT_SCHED);
 #endif
-	pthread_attr_setdetachstate(&pThis->attrThrd, PTHREAD_CREATE_DETACHED);
 	/* set all function pointers to "not implemented" dummy so that we can safely call them */
 	pThis->pfChkStopWrkr = (rsRetVal (*)(void*,int))NotImplementedDummy_voidp_int;
 	pThis->pfGetDeqBatchSize = (rsRetVal (*)(void*,int*))NotImplementedDummy_voidp_intp;
@@ -195,6 +194,16 @@ wtpSetState(wtp_t *pThis, wtpState_t iNewState)
 	return RS_RET_OK;
 }
 
+/* join terminated worker threads */
+static void ATTR_NONNULL()
+wtpJoinTerminatedWrkr(wtp_t *const pThis)
+{
+	int i;
+	for(i = 0 ; i < pThis->iNumWorkerThreads ; ++i) {
+		wtiJoinThrd(pThis->pWrkr[i]);
+	}
+}
+
 
 /* check if the worker shall shutdown (1 = yes, 0 = no)
  * Note: there may be two mutexes locked, the bLockUsrMutex is the one in our "user"
@@ -252,6 +261,7 @@ wtpShutdownAll(wtp_t *pThis, wtpState_t tShutdownCmd, struct timespec *ptTimeout
 	wtpSetState(pThis, tShutdownCmd);
 	/* awake workers in retry loop */
 	for(i = 0 ; i < pThis->iNumWorkerThreads ; ++i) {
+		wtpJoinTerminatedWrkr(pThis);
 		pthread_cond_signal(&pThis->pWrkr[i]->pcondBusy);
 		wtiWakeupThrd(pThis->pWrkr[i]);
 	}
@@ -262,6 +272,7 @@ wtpShutdownAll(wtp_t *pThis, wtpState_t tShutdownCmd, struct timespec *ptTimeout
 	pthread_cleanup_push(mutexCancelCleanup, &pThis->mutWtp);
 	bTimedOut = 0;
 	while(pThis->iCurNumWrkThrd > 0 && !bTimedOut) {
+		wtpJoinTerminatedWrkr(pThis);
 		DBGPRINTF("%s: waiting %ldms on worker thread termination, %d still running\n",
 			   wtpGetDbgHdr(pThis), timeoutVal(ptTimeout),
 			   ATOMIC_FETCH_32BIT(&pThis->iCurNumWrkThrd, &pThis->mutCurNumWrkThrd));
@@ -326,10 +337,10 @@ wtpWrkrExecCleanup(wti_t *pWti)
 
 // TESTBENCH bughunt - remove when done! 2018-11-05 rgerhards
 if(dbgTimeoutToStderr) {
-fprintf(stderr, "%s: enter WrkrExecCleanup\n", wtpGetDbgHdr(pThis));
+	fprintf(stderr, "rsyslog debug: %s: enter WrkrExecCleanup\n", wtiGetDbgHdr(pWti));
 }
 	/* the order of the next two statements is important! */
-	wtiSetState(pWti, WRKTHRD_STOPPED);
+	wtiSetState(pWti, WRKTHRD_WAIT_JOIN);
 	ATOMIC_DEC(&pThis->iCurNumWrkThrd, &pThis->mutCurNumWrkThrd);
 
 	/* note: numWorkersNow is only for message generation, so we do not try
@@ -384,7 +395,6 @@ wtpWorker(void *arg) /* the arg is actually a wti object, even though we are in
 	uchar *pszDbgHdr;
 	uchar thrdName[32] = "rs:";
 #	endif
-	pthread_detach(pthread_self());
 
 	ISOBJ_TYPE_assert(pWti, wti);
 	pThis = pWti->pWtp;
@@ -408,7 +418,7 @@ wtpWorker(void *arg) /* the arg is actually a wti object, even though we are in
 
 // TESTBENCH bughunt - remove when done! 2018-11-05 rgerhards
 if(dbgTimeoutToStderr) {
-	fprintf(stderr, "%s: worker %p started\n", wtpGetDbgHdr(pThis), pThis);
+	fprintf(stderr, "rsyslog debug: %s: worker %p started\n", wtpGetDbgHdr(pThis), pThis);
 }
 	/* let the parent know we're done with initialization */
 	d_pthread_mutex_lock(&pThis->mutWtp);
@@ -426,10 +436,9 @@ if(dbgTimeoutToStderr) {
 
 	pthread_cond_broadcast(&pThis->condThrdTrm); /* activate anyone waiting on thread shutdown */
 	pthread_cleanup_pop(1); /* unlock mutex */
-// TESTBENCH bughunt - remove when done! 2018-11-05 rgerhards
-if(dbgTimeoutToStderr) {
-	fprintf(stderr, "worker %p exiting\n", pThis);
-}
+	if(dbgTimeoutToStderr) {
+		fprintf(stderr, "rsyslog debug: %s: worker exiting\n", wtiGetDbgHdr(pWti));
+	}
 	pthread_exit(0);
 }
 #if !defined(_AIX)
@@ -458,7 +467,7 @@ if(dbgTimeoutToStderr) {
 	if(wtpState != wtpState_RUNNING && !permit_during_shutdown) {
 		DBGPRINTF("%s: worker start requested during shutdown - ignored\n", wtpGetDbgHdr(pThis));
 		if(dbgTimeoutToStderr) {
-			fprintf(stderr, "rsyslogd debug: %s: worker start requested during shutdown - ignored\n",
+			fprintf(stderr, "rsyslog debug: %s: worker start requested during shutdown - ignored\n",
 				wtpGetDbgHdr(pThis));
 		}
 		return RS_RET_ERR; /* exceptional case, but really makes sense here! */
@@ -466,6 +475,7 @@ if(dbgTimeoutToStderr) {
 
 	d_pthread_mutex_lock(&pThis->mutWtp);
 
+	wtpJoinTerminatedWrkr(pThis);
 	/* find free spot in thread table. */
 	for(i = 0 ; i < pThis->iNumWorkerThreads ; ++i) {
 		if(wtiGetState(pThis->pWrkr[i]) == WRKTHRD_STOPPED) {
@@ -506,7 +516,7 @@ if(dbgTimeoutToStderr) {
 		ATOMIC_FETCH_32BIT(&pThis->iCurNumWrkThrd, &pThis->mutCurNumWrkThrd));
 // TESTBENCH bughunt - remove when done! 2018-11-05 rgerhards
 if(dbgTimeoutToStderr) {
-	fprintf(stderr, "%s: started with state %d, num workers now %d\n",
+	fprintf(stderr, "rsyslog debug: %s: started with state %d, num workers now %d\n",
 		wtpGetDbgHdr(pThis), iState,
 		ATOMIC_FETCH_32BIT(&pThis->iCurNumWrkThrd, &pThis->mutCurNumWrkThrd));
 }
diff --git a/runtime/wtp.h b/runtime/wtp.h
index 6ef31dca32..6fff374f3c 100644
--- a/runtime/wtp.h
+++ b/runtime/wtp.h
@@ -1,6 +1,6 @@
 /* Definition of the worker thread pool (wtp) object.
  *
- * Copyright 2008-2012 Adiscon GmbH.
+ * Copyright 2008-2018 Adiscon GmbH.
  *
  * This file is part of the rsyslog runtime library.
  *
@@ -33,6 +33,7 @@
 #define WRKTHRD_STOPPED  	0
 #define WRKTHRD_INITIALIZING	1
 #define WRKTHRD_RUNNING		3
+#define WRKTHRD_WAIT_JOIN	7
 
 
 /* possible states of a worker thread pool */
diff --git a/tests/known_issues.supp b/tests/known_issues.supp
index 8c10b51983..1c6d605136 100644
--- a/tests/known_issues.supp
+++ b/tests/known_issues.supp
@@ -5,11 +5,3 @@
    fun:dlopen*
    ...
 }
-
-{
-   This seems to be a problem in pthreads, see https://github.com/rsyslog/rsyslog/issues/2902
-   Memcheck:Leak
-   ...
-   fun:pthread_create@@*
-   ...
-}
