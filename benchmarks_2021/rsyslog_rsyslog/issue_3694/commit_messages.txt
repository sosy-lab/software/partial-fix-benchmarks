Merge pull request #3672 from rgerhards/i3667-test

testbench: test for leading space issue in RFC 5424 parser
imuxsock: support FreeBSD 12 out of the box

FreeBSD 12 uses RFC5424 on the system log socket by default. This
format is not supported by the special parser used in imuxsock.
Thus for FreeBSD the default needs to be changed to use the
regular parser chain by default. That is all this commit does.

Note: the code was merged untested, as we currently do not have
a matching CI environment. If it does not work, please report.

closes https://github.com/rsyslog/rsyslog/issues/3694
imuxsock: support FreeBSD 12 out of the box

FreeBSD 12 uses RFC5424 on the system log socket by default. This
format is not supported by the special parser used in imuxsock.
Thus for FreeBSD the default needs to be changed to use the
regular parser chain by default. That is all this commit does.

Note: the code was merged untested, as we currently do not have
a matching CI environment. If it does not work, please report.

closes https://github.com/rsyslog/rsyslog/issues/3694
imuxsock: support FreeBSD 12 out of the box

FreeBSD 12 uses RFC5424 on the system log socket by default. This
format is not supported by the special parser used in imuxsock.
Thus for FreeBSD the default needs to be changed to use the
regular parser chain by default. That is all this commit does.

Note: the code was merged untested, as we currently do not have
a matching CI environment. If it does not work, please report.

closes https://github.com/rsyslog/rsyslog/issues/3694
imuxsock: support FreeBSD 12 out of the box

FreeBSD 12 uses RFC5424 on the system log socket by default. This
format is not supported by the special parser used in imuxsock.
Thus for FreeBSD the default needs to be changed to use the
regular parser chain by default. That is all this commit does.

Note: the code was merged untested, as we currently do not have
a matching CI environment. If it does not work, please report.

closes https://github.com/rsyslog/rsyslog/issues/3694
