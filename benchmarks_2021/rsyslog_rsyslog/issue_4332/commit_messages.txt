Merge pull request #4327 from rgerhards/month-octal

build bugfix: version month value misinterpreted as octal
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)

- fix bug in ethernet packets parsing
  - fix removes build error with gcc10: 'multiple definition of...'
- resolve memory leak during interface init failure (device not freed after post-create error)
- add test 'impcap_bug_ether' to prove ethernet parser fix is working
Merge pull request #4343 from VultureProject/impcap-merge-upstream

IMPCAP::Fixes: segfault, memory and build corrections (resolves #4332)
