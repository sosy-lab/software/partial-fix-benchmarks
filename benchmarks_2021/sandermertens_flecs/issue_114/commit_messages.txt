#115 C++ api

* #115 implement C++ API

* #115 add C++ examples, templates

* Add module support, merge master

* add add/remove/set/hierarchy examples

* add inheritance example

* add override examples

* add type examples

* add prefab examples

* add nested prefab examples

* add system feature example

* add optional example

* add get_children example

* add on_demand example

* add snapshot example

* add snapshot_w_filter example

* add filter_iter example

* add world & snapshot filter iterators to C++ API

* cleanup C++ API

* fix testcase

* remove redundant headers, cleanup examples

* fix issue with accidental conversions

* fix static code analysis issues

* disable C++ examples for legacy builds

* process review comments, add comments

* update meson file
#114 initial commit for blob API
#114 filter out unnecessary data
#114 implement component writer
#114 implement table writer
#114 cleanup API, fix remaining issues
#114 add save_to_file example
#114 fix alignment issues
#114 add test
#114 add test for id fragmentation / alignment
#114 fix issues with id-only tables
#114 add test for unaligned components
#114 add support for serializing tags
#114 fix issues with serializing type flags
#114 add tests for inheritance
#114 add documentation to  header
#114 add reader/writer to C++ API, add example
#114 add reader/writer to C++ API, add example
#114 don't use variable size arrays in tests
#114 prevent duplicate entities when there are conflicts
#114 improve coverage
#114 improve coverage
#114 Serializing & deserializing of a world

* #114 initial commit for blob API

* implement table reader

* #114 filter out unnecessary data

* Add support for storing EcsId columns

* #114 implement component writer

* #114 implement table writer

* #114 cleanup API, fix remaining issues

* #114 add save_to_file example

* #114 fix alignment issues

* #114 add test

* #114 add test for id fragmentation / alignment

* #114 fix issues with id-only tables

* #114 add test for unaligned components

* #114 add support for serializing tags

* #114 fix issues with serializing type flags

* #114 add tests for inheritance

* #114 add documentation to  header

* #114 add reader/writer to C++ API, add example

* #114 don't use variable size arrays in tests

* #114 prevent duplicate entities when there are conflicts

* #114 improve coverage
