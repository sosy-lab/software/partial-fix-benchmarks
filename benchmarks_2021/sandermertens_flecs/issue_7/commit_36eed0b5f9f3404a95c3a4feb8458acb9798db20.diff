diff --git a/include/flecs.h b/include/flecs.h
index ba877e41d..80881bfd0 100644
--- a/include/flecs.h
+++ b/include/flecs.h
@@ -28,6 +28,9 @@ typedef char bool;
 /* The flecs world object */
 typedef struct ecs_world ecs_world_t;
 
+/* A world snapshot */
+typedef struct ecs_snapshot_t ecs_snapshot_t;
+
 /** A handle identifies an entity */
 typedef uint64_t ecs_entity_t;
 
@@ -219,6 +222,9 @@ typedef void (*ecs_module_init_action_t)(
 /* World entity */
 #define EcsWorld (13)
 
+/* Used to quickly check if component is builtin */
+#define EcsLastBuiltin (EEcsColSystem)
+
 /* Singleton entity */
 #define EcsSingleton (ECS_SINGLETON)
 
@@ -2096,6 +2102,22 @@ ecs_entity_t ecs_new_prefab(
     const char *id,
     const char *sig);
 
+/** Create a snapshot */
+FLECS_EXPORT
+ecs_snapshot_t* ecs_snapshot_take(
+    ecs_world_t *world);
+
+/** Restore a snapshot */
+FLECS_EXPORT
+void ecs_snapshot_restore(
+    ecs_world_t *world,
+    ecs_snapshot_t *snapshot);
+
+/** Free snapshot resources */
+FLECS_EXPORT
+void ecs_snapshot_free(
+    ecs_world_t *world,
+    ecs_snapshot_t *snapshot);
 
 /* -- Error handling & error codes -- */
 
diff --git a/include/flecs/util/chunked.h b/include/flecs/util/chunked.h
index 28add258a..bb8994900 100644
--- a/include/flecs/util/chunked.h
+++ b/include/flecs/util/chunked.h
@@ -67,6 +67,10 @@ FLECS_EXPORT
 const uint32_t* ecs_chunked_indices(
     const ecs_chunked_t *chunked);
 
+FLECS_EXPORT
+ecs_chunked_t* ecs_chunked_copy(
+    const ecs_chunked_t *src);
+
 FLECS_EXPORT
 void ecs_chunked_memory(
     ecs_chunked_t *chunked,
diff --git a/include/flecs/util/map.h b/include/flecs/util/map.h
index f6882e34d..33a468197 100644
--- a/include/flecs/util/map.h
+++ b/include/flecs/util/map.h
@@ -84,6 +84,10 @@ int ecs_map_remove(
     ecs_map_t *map,
     uint64_t key_hash);
 
+FLECS_EXPORT
+ecs_map_t* ecs_map_copy(
+    const ecs_map_t *map);
+
 FLECS_EXPORT
 ecs_map_iter_t ecs_map_iter(
     ecs_map_t *map);
diff --git a/include/flecs/util/os_api.h b/include/flecs/util/os_api.h
index 90a3d17bc..8eb727011 100644
--- a/include/flecs/util/os_api.h
+++ b/include/flecs/util/os_api.h
@@ -289,6 +289,11 @@ FLECS_EXPORT
 double ecs_time_to_double(
     ecs_time_t t);
 
+FLECS_EXPORT
+void* ecs_os_memdup(
+    const void *src, 
+    size_t size);
+
 #ifdef __cplusplus
 }
 #endif
diff --git a/include/flecs/util/vector.h b/include/flecs/util/vector.h
index ac285fe87..7217322b1 100644
--- a/include/flecs/util/vector.h
+++ b/include/flecs/util/vector.h
@@ -144,6 +144,11 @@ void ecs_vector_memory(
     uint32_t *allocd,
     uint32_t *used);
 
+FLECS_EXPORT
+ecs_vector_t* ecs_vector_copy(
+    const ecs_vector_t *src,
+    const ecs_vector_params_t *params);
+
 #ifdef __cplusplus
 }
 #endif
diff --git a/src/chunked.c b/src/chunked.c
index 7bc334ffa..111beece0 100644
--- a/src/chunked.c
+++ b/src/chunked.c
@@ -246,6 +246,34 @@ const uint32_t* ecs_chunked_indices(
     return ecs_vector_first(chunked->dense);
 }
 
+ecs_chunked_t* ecs_chunked_copy(
+    const ecs_chunked_t *src)
+{
+    ecs_chunked_t *dst = ecs_os_memdup(src, sizeof(ecs_chunked_t));
+    dst->chunks = ecs_vector_copy(src->chunks, &chunk_param);
+    dst->dense = ecs_vector_copy(src->dense, &dense_param);
+    dst->sparse = ecs_vector_copy(src->sparse, &sparse_param);
+    dst->free_stack = ecs_vector_copy(src->free_stack, &free_param);
+
+    /* Iterate chunks, copy data */
+    sparse_elem_t *sparse_array = ecs_vector_first(dst->sparse);
+    chunk_t *chunks = ecs_vector_first(dst->chunks);
+    uint32_t i, count = ecs_vector_count(dst->chunks);
+
+    for (i = 0; i < count; i ++) {
+        chunks[i].data = ecs_os_memdup(
+            chunks[i].data, dst->chunk_size * dst->element_size);
+        
+        uint32_t j;
+        for (j = 0; j < dst->chunk_size; j ++) {
+            sparse_array[j].ptr = 
+                ECS_OFFSET(chunks[i].data, j * dst->element_size);
+        }
+    }
+
+    return dst;
+}
+
 void ecs_chunked_memory(
     ecs_chunked_t *chunked,
     uint32_t *allocd,
diff --git a/src/flecs_private.h b/src/flecs_private.h
index 8a2c1a3d9..aa642c1ee 100644
--- a/src/flecs_private.h
+++ b/src/flecs_private.h
@@ -263,6 +263,12 @@ void ecs_table_clear(
     ecs_world_t *world,
     ecs_table_t *table);
 
+/* Clear data in columns */
+void ecs_table_replace_columns(
+    ecs_world_t *world,
+    ecs_table_t *table,
+    ecs_table_column_t *columns);
+    
 /* Merge data of one table into another table */
 void ecs_table_merge(
     ecs_world_t *world,
diff --git a/src/map.c b/src/map.c
index 676cddcc3..9b2c79f67 100644
--- a/src/map.c
+++ b/src/map.c
@@ -474,7 +474,6 @@ void ecs_map_memory(
     }
 
     if (total) {
-
         *total += map->bucket_count * sizeof(uint32_t) + sizeof(ecs_map_t);
         ecs_vector_memory(map->nodes, &map->node_params, total, NULL);
     }
@@ -485,6 +484,18 @@ void ecs_map_memory(
     }
 }
 
+ecs_map_t* ecs_map_copy(
+    const ecs_map_t *map)
+{
+    ecs_map_t *dst = ecs_os_memdup(map, sizeof(ecs_map_t));
+    dst->buckets = ecs_os_memdup(
+        map->buckets, map->bucket_count * sizeof(uint32_t));
+    
+    dst->nodes = ecs_vector_copy(map->nodes, &map->node_params);
+
+    return dst;
+}
+
 ecs_map_iter_t ecs_map_iter(
     ecs_map_t *map)
 {
diff --git a/src/misc.c b/src/misc.c
index 8b787bd4e..b0773307f 100644
--- a/src/misc.c
+++ b/src/misc.c
@@ -47,6 +47,12 @@ double ecs_time_measure(
     return ecs_time_to_double(stop);
 }
 
+void* ecs_os_memdup(const void *src, size_t size) {
+    void *dst = ecs_os_malloc(size);
+    memcpy(dst, src, size);  
+    return dst;  
+}
+
 /*
     This code was taken from sokol_time.h 
     
diff --git a/src/snapshot.c b/src/snapshot.c
new file mode 100644
index 000000000..2efd70ccc
--- /dev/null
+++ b/src/snapshot.c
@@ -0,0 +1,106 @@
+#include "flecs_private.h"
+
+static
+void dup_table(
+    ecs_world_t *world, 
+    ecs_table_t *table)
+{
+    uint32_t c, column_count = ecs_vector_count(table->type);
+
+    /* First create a copy of columns structure */
+    table->columns = ecs_os_memdup(
+        table->columns, sizeof(ecs_table_column_t) * (column_count + 1));
+    
+    /* Now copy each column separately */
+    for (c = 0; c < column_count + 1; c ++) {
+        ecs_table_column_t *column = &table->columns[c];
+        ecs_vector_params_t column_params = {.element_size = column->size};
+        column->data = ecs_vector_copy(column->data, &column_params);
+    }
+}
+
+/** Create a snapshot */
+ecs_snapshot_t* ecs_snapshot_take(
+    ecs_world_t *world)
+{
+    ecs_snapshot_t *result = ecs_os_malloc(sizeof(ecs_snapshot_t));
+
+    /* Copy high-level data structures */
+    result->entity_index = ecs_map_copy(world->main_stage.entity_index);
+    result->tables = ecs_chunked_copy(world->main_stage.tables);
+
+    /* We need to dup the table data, because right now the copied tables are
+     * still pointing to columns in the main stage. */
+    uint32_t i, count = ecs_chunked_count(result->tables);
+    for (i = 0; i < count; i ++) {
+        ecs_table_t *table = ecs_chunked_get(result->tables, ecs_table_t, i);
+
+        if (table->flags & EcsTableHasBuiltins) {
+            continue;
+        }
+
+        dup_table(world, table);
+    }
+
+    snapshot->last_handle = world->last_handle;
+
+    return result;
+}
+
+/** Restore a snapshot */
+void ecs_snapshot_restore(
+    ecs_world_t *world,
+    ecs_snapshot_t *snapshot)
+{
+    /* Replace entity index with snapshot entity index */
+    ecs_map_free(world->main_stage.entity_index);
+    world->main_stage.entity_index = snapshot->entity_index;
+
+    /* Move snapshot data to table */
+    uint32_t i, count = ecs_chunked_count(snapshot->tables);
+    for (i = 0; i < count; i ++) {
+        ecs_table_t *src = ecs_chunked_get(snapshot->tables, ecs_table_t, i);
+        if (src->flags & EcsTableHasBuiltins) {
+            continue;
+        }
+
+        ecs_table_t *dst = ecs_chunked_get(world->main_stage.tables, ecs_table_t, i);
+        ecs_table_replace_columns(world, dst, src->columns);
+    }
+
+    /* Clear data from remaining tables */
+    uint32_t world_count = ecs_chunked_count(world->main_stage.tables);
+    for (; i < world_count; i ++) {
+        ecs_table_t *table = ecs_chunked_get(world->main_stage.tables, ecs_table_t, i);
+        ecs_table_replace_columns(world, table, NULL);
+    }
+
+    ecs_chunked_free(snapshot->tables);
+    ecs_os_free(snapshot);
+
+    world->should_match = true;
+    world->should_resolve = true;
+    world->last_handle = snapshot->last_handle;
+}
+
+/** Cleanup snapshot */
+void ecs_snapshot_free(
+    ecs_world_t *world,
+    ecs_snapshot_t *snapshot)
+{
+    ecs_map_free(snapshot->entity_index);
+
+    uint32_t i, count = ecs_chunked_count(snapshot->tables);
+    for (i = 0; i < count; i ++) {
+        ecs_table_t *src = ecs_chunked_get(snapshot->tables, ecs_table_t, i);
+        if (src->flags & EcsTableHasBuiltins) {
+            continue;
+        }
+
+        ecs_table_replace_columns(world, src, NULL);
+        ecs_os_free(src->columns);
+    }    
+
+    ecs_chunked_free(snapshot->tables);
+    ecs_os_free(snapshot);
+}
diff --git a/src/table.c b/src/table.c
index 2e96ba8f1..f06621e0d 100644
--- a/src/table.c
+++ b/src/table.c
@@ -54,6 +54,10 @@ ecs_table_column_t* new_columns(
             }
         }
 
+        if (table && buf[i] <= EcsLastBuiltin) {
+            table->flags |= EcsTableHasBuiltins;
+        }        
+
         if (table && buf[i] == EEcsPrefab) {
             table->flags |= EcsTableIsPrefab;
         }
@@ -108,7 +112,7 @@ void ecs_table_deinit(
 }
 
 static
-void ecs_table_free_columns(
+void ecs_table_clear_columns(
     ecs_table_t *table)
 {
     uint32_t i, column_count = ecs_vector_count(table->type);
@@ -119,12 +123,34 @@ void ecs_table_free_columns(
     }
 }
 
+void ecs_table_replace_columns(
+    ecs_world_t *world,
+    ecs_table_t *table,
+    ecs_table_column_t *columns)
+{
+    uint32_t prev_count = ecs_vector_count(table->columns[0].data);
+
+    ecs_table_clear_columns(table);
+    if (columns) {
+        ecs_os_free(table->columns);
+        table->columns = columns;
+    }
+
+    uint32_t count = ecs_vector_count(table->columns[0].data);
+
+    if (!prev_count && count) {
+        activate_table(world, table, 0, true);
+    } else if (prev_count && !count) {
+        activate_table(world, table, 0, false);
+    }
+}
+
 void ecs_table_clear(
     ecs_world_t *world,
     ecs_table_t *table)
 {
     ecs_table_deinit(world, table);
-    ecs_table_free_columns(table);
+    ecs_table_clear_columns(table);
 }
 
 void ecs_table_free(
@@ -132,7 +158,7 @@ void ecs_table_free(
     ecs_table_t *table)
 {
     (void)world;
-    ecs_table_free_columns(table);
+    ecs_table_clear_columns(table);
     ecs_os_free(table->columns);
     ecs_vector_free(table->frame_systems);
 }
diff --git a/src/types.h b/src/types.h
index af079c105..4aa26481a 100644
--- a/src/types.h
+++ b/src/types.h
@@ -149,6 +149,7 @@ typedef struct ecs_table_column_t {
 #define EcsTableIsStaged  (1)
 #define EcsTableIsPrefab (2)
 #define EcsTableHasPrefab (4)
+#define EcsTableHasBuiltins (8)
 
 /** A table is the Flecs equivalent of an archetype. Tables store all entities
  * with a specific set of components. Tables are automatically created when an
@@ -385,7 +386,12 @@ typedef struct ecs_thread_t {
     uint16_t index;                           /* Index of thread */
 } ecs_thread_t;
 
-
+/* World snapshot */
+struct ecs_snapshot_t {
+    ecs_map_t *entity_index;
+    ecs_chunked_t *tables;
+    ecs_entity_t last_handle;
+};
 
 /** The world stores and manages all ECS data. An application can have more than
  * one world, but data is not shared between worlds. */
diff --git a/src/vector.c b/src/vector.c
index ac94759ef..caccd5794 100644
--- a/src/vector.c
+++ b/src/vector.c
@@ -374,3 +374,16 @@ void ecs_vector_memory(
         *used += array->count * params->element_size;
     }
 }
+
+ecs_vector_t* ecs_vector_copy(
+    const ecs_vector_t *src,
+    const ecs_vector_params_t *params)
+{
+    if (!src) {
+        return NULL;
+    }
+
+    ecs_vector_t *dst = ecs_vector_new(params, src->size);
+    memcpy(dst, src, sizeof(ecs_vector_t) + params->element_size * src->count);
+    return dst;
+}
