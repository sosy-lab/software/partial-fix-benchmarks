diff --git a/include/flecs.h b/include/flecs.h
index 80881bfd0..f6c744d00 100644
--- a/include/flecs.h
+++ b/include/flecs.h
@@ -2105,7 +2105,8 @@ ecs_entity_t ecs_new_prefab(
 /** Create a snapshot */
 FLECS_EXPORT
 ecs_snapshot_t* ecs_snapshot_take(
-    ecs_world_t *world);
+    ecs_world_t *world,
+    ecs_type_filter_t *filter);
 
 /** Restore a snapshot */
 FLECS_EXPORT
diff --git a/src/snapshot.c b/src/snapshot.c
index 1b4db119a..f8ca10cc5 100644
--- a/src/snapshot.c
+++ b/src/snapshot.c
@@ -21,7 +21,8 @@ void dup_table(
 
 /** Create a snapshot */
 ecs_snapshot_t* ecs_snapshot_take(
-    ecs_world_t *world)
+    ecs_world_t *world,
+    ecs_type_filter_t *filter)
 {
     ecs_snapshot_t *result = ecs_os_malloc(sizeof(ecs_snapshot_t));
 
@@ -35,11 +36,22 @@ ecs_snapshot_t* ecs_snapshot_take(
     for (i = 0; i < count; i ++) {
         ecs_table_t *table = ecs_chunked_get(result->tables, ecs_table_t, i);
 
+        /* Skip tables with builtin components to avoid dropping critical data
+         * like systems or components when restoring the snapshot */
         if (table->flags & EcsTableHasBuiltins) {
             continue;
         }
 
-        dup_table(world, table);
+        if (!filter || ecs_type_match_w_filter(world, table->type, filter)) {
+            dup_table(world, table);
+        } else {
+            /* If the table does not match the filter, instead of copying just
+             * set the columns to NULL. This way the restore will ignore the
+             * table. 
+             * Note that this does not cause a memory leak, as at this point the
+             * columns member still points to the live data. */
+            table->columns = NULL;
+        }
     }
 
     result->last_handle = world->last_handle;
@@ -64,6 +76,12 @@ void ecs_snapshot_restore(
             continue;
         }
 
+        /* If table has no columns, it was filtered out and should not be
+         * restored. */
+        if (!src->columns) {
+            continue;
+        }
+
         ecs_table_t *dst = ecs_chunked_get(world->main_stage.tables, ecs_table_t, i);
         ecs_table_replace_columns(world, dst, src->columns);
     }
diff --git a/test/api/project.json b/test/api/project.json
index 82af184c1..5c46a54bd 100644
--- a/test/api/project.json
+++ b/test/api/project.json
@@ -895,7 +895,9 @@
                 "snapshot_after_delete",
                 "snapshot_after_new_type",
                 "snapshot_after_add",
-                "snapshot_after_remove"
+                "snapshot_after_remove",
+                "snapshot_w_include_filter",
+                "snapshot_w_exclude_filter"
             ]
         }, {
             "id": "Modules",
diff --git a/test/api/src/Snapshot.c b/test/api/src/Snapshot.c
index 7f0ca93e6..6301994bf 100644
--- a/test/api/src/Snapshot.c
+++ b/test/api/src/Snapshot.c
@@ -9,7 +9,7 @@ void Snapshot_simple_snapshot() {
     test_assert(e != 0);
     test_assert(ecs_has(world, e, Position));
 
-    ecs_snapshot_t *s = ecs_snapshot_take(world);
+    ecs_snapshot_t *s = ecs_snapshot_take(world, NULL);
 
     Position *p = ecs_get_ptr(world, e, Position);
     test_int(p->x, 10);
@@ -37,7 +37,7 @@ void Snapshot_snapshot_after_new() {
     test_assert(e != 0);
     test_assert(ecs_has(world, e, Position));
 
-    ecs_snapshot_t *s = ecs_snapshot_take(world);
+    ecs_snapshot_t *s = ecs_snapshot_take(world, NULL);
 
     ecs_entity_t e2 = ecs_new(world, Position);
     test_assert(e2 != 0);
@@ -61,7 +61,7 @@ void Snapshot_snapshot_after_delete() {
     test_assert(e != 0);
     test_assert(ecs_has(world, e, Position));
 
-    ecs_snapshot_t *s = ecs_snapshot_take(world);
+    ecs_snapshot_t *s = ecs_snapshot_take(world, NULL);
 
     ecs_delete(world, e);
     test_assert(!ecs_has(world, e, Position));
@@ -86,7 +86,7 @@ void Snapshot_snapshot_after_new_type() {
     test_assert(e != 0);
     test_assert(ecs_has(world, e, Position));
 
-    ecs_snapshot_t *s = ecs_snapshot_take(world);
+    ecs_snapshot_t *s = ecs_snapshot_take(world, NULL);
 
     ecs_entity_t e2 = ecs_new(world, Position);
     ecs_add(world, e2, Velocity);
@@ -110,7 +110,7 @@ void Snapshot_snapshot_after_add() {
     test_assert(e != 0);
     test_assert(ecs_has(world, e, Position));
 
-    ecs_snapshot_t *s = ecs_snapshot_take(world);
+    ecs_snapshot_t *s = ecs_snapshot_take(world, NULL);
 
     ecs_add(world, e, Velocity);
     test_assert(ecs_has(world, e, Velocity));
@@ -136,7 +136,7 @@ void Snapshot_snapshot_after_remove() {
     ecs_add(world, e, Velocity);
     test_assert(ecs_has(world, e, Velocity));
 
-    ecs_snapshot_t *s = ecs_snapshot_take(world);
+    ecs_snapshot_t *s = ecs_snapshot_take(world, NULL);
 
     ecs_remove(world, e, Velocity);
     test_assert(!ecs_has(world, e, Velocity));
@@ -147,3 +147,145 @@ void Snapshot_snapshot_after_remove() {
 
     ecs_fini(world);
 }
+
+void Snapshot_snapshot_w_include_filter() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+    ECS_COMPONENT(world, Velocity);
+    
+    ecs_entity_t e1 = ecs_set(world, 0, Position, {10, 20});
+    test_assert(e1 != 0);
+    test_assert(ecs_has(world, e1, Position));
+
+    ecs_entity_t e2 = ecs_set(world, 0, Position, {15, 25});
+    test_assert(e2 != 0);
+    ecs_add(world, e2, Velocity);
+    test_assert(ecs_has(world, e2, Position));
+    test_assert(ecs_has(world, e2, Velocity));
+
+    ecs_entity_t e3 = ecs_set(world, 0, Velocity, {25, 35});
+    test_assert(e3 != 0);
+    test_assert(ecs_has(world, e3, Velocity));
+
+    ecs_snapshot_t *s = ecs_snapshot_take(world, &(ecs_type_filter_t){
+        .include = ecs_type(Position)
+    });
+
+    Position *p = ecs_get_ptr(world, e1, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 10);
+    test_int(p->y, 20);
+
+    p->x ++;
+    p->y ++;
+
+    p = ecs_get_ptr(world, e2, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 15);
+    test_int(p->y, 25);
+
+    p->x ++;
+    p->y ++;
+
+    Velocity *v = ecs_get_ptr(world, e3, Velocity);
+    test_assert(v != NULL);
+    test_int(v->x, 25); 
+    test_int(v->y, 35);
+
+    v->x ++;
+    v->y ++;
+
+    ecs_snapshot_restore(world, s);
+
+    /* Restored */
+    p = ecs_get_ptr(world, e1, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 10);
+    test_int(p->y, 20);
+
+    /* Restored */
+    p = ecs_get_ptr(world, e2, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 15);
+    test_int(p->y, 25);
+
+    /* Not restored */
+    v = ecs_get_ptr(world, e3, Velocity);
+    test_assert(v != NULL);
+    test_int(v->x, 26); 
+    test_int(v->y, 36);
+
+    ecs_fini(world);
+}
+
+void Snapshot_snapshot_w_exclude_filter() {
+    ecs_world_t *world = ecs_init();
+
+    ECS_COMPONENT(world, Position);
+    ECS_COMPONENT(world, Velocity);
+    
+    ecs_entity_t e1 = ecs_set(world, 0, Position, {10, 20});
+    test_assert(e1 != 0);
+    test_assert(ecs_has(world, e1, Position));
+
+    ecs_entity_t e2 = ecs_set(world, 0, Position, {15, 25});
+    test_assert(e2 != 0);
+    ecs_add(world, e2, Velocity);
+    test_assert(ecs_has(world, e2, Position));
+    test_assert(ecs_has(world, e2, Velocity));
+
+    ecs_entity_t e3 = ecs_set(world, 0, Velocity, {25, 35});
+    test_assert(e3 != 0);
+    test_assert(ecs_has(world, e3, Velocity));
+
+    ecs_snapshot_t *s = ecs_snapshot_take(world, &(ecs_type_filter_t){
+        .exclude = ecs_type(Position)
+    });
+
+    Position *p = ecs_get_ptr(world, e1, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 10);
+    test_int(p->y, 20);
+
+    p->x ++;
+    p->y ++;
+
+    p = ecs_get_ptr(world, e2, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 15);
+    test_int(p->y, 25);
+
+    p->x ++;
+    p->y ++;
+
+    Velocity *v = ecs_get_ptr(world, e3, Velocity);
+    test_assert(v != NULL);
+    test_int(v->x, 25); 
+    test_int(v->y, 35);
+
+    v->x ++;
+    v->y ++;
+
+    ecs_snapshot_restore(world, s);
+
+    /* Not restored */
+    p = ecs_get_ptr(world, e1, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 11);
+    test_int(p->y, 21);
+
+    /* Not restored */
+    p = ecs_get_ptr(world, e2, Position);
+    test_assert(p != NULL);
+    test_int(p->x, 16);
+    test_int(p->y, 26);
+
+    /* Restored */
+    v = ecs_get_ptr(world, e3, Velocity);
+    test_assert(v != NULL);
+    test_int(v->x, 25); 
+    test_int(v->y, 35);
+
+    ecs_fini(world);
+}
diff --git a/test/api/src/main.c b/test/api/src/main.c
index a7304308e..7ca872551 100644
--- a/test/api/src/main.c
+++ b/test/api/src/main.c
@@ -818,6 +818,8 @@ void Snapshot_snapshot_after_delete(void);
 void Snapshot_snapshot_after_new_type(void);
 void Snapshot_snapshot_after_add(void);
 void Snapshot_snapshot_after_remove(void);
+void Snapshot_snapshot_w_include_filter(void);
+void Snapshot_snapshot_w_exclude_filter(void);
 
 // Testsuite 'Modules'
 void Modules_simple_module(void);
@@ -3991,7 +3993,7 @@ static bake_test_suite suites[] = {
     },
     {
         .id = "Snapshot",
-        .testcase_count = 6,
+        .testcase_count = 8,
         .testcases = (bake_test_case[]){
             {
                 .id = "simple_snapshot",
@@ -4016,6 +4018,14 @@ static bake_test_suite suites[] = {
             {
                 .id = "snapshot_after_remove",
                 .function = Snapshot_snapshot_after_remove
+            },
+            {
+                .id = "snapshot_w_include_filter",
+                .function = Snapshot_snapshot_w_include_filter
+            },
+            {
+                .id = "snapshot_w_exclude_filter",
+                .function = Snapshot_snapshot_w_exclude_filter
             }
         }
     },
