Signal handlers use async-signal-unsafe functions
Also ptrace() hiding in detach() does not belong to these functions. Looks like here we have no other choice but to change the design.

@elfring: Please review the referenced commit.

@elfring: Please review the referenced V2 instead.

Would you like to consider other software design options?
- [sigwaitinfo()](http://pubs.opengroup.org/onlinepubs/9699919799/functions/sigwaitinfo.html)
- [Self-pipe trick](http://cr.yp.to/docs/selfpipe.html)
- [Signal-safe locks](http://locklessinc.com/articles/signalsafe_locks/)

I think sigwaitinfo() requires masking all signals so that they aren't processed asynchronously. Then you need a thread with a loop to check for signals as execution is suspended until the signal is delivered.

The self-pipe trick also involves a child. The problem is that int to string formatting and memcpy() is not possible as only write() and read() are async-signal-safe. So I can't transfer the signal number. Furthermore, the child would have to kill its parent or the event loop would have to be the parent to be able to exit scanmem.

The signal-safe locks look complex and are overkill for just protecting a printf() basically. If they would be in a common lib, then okay but not this way. I don't see who actually uses this.

So for me the fork approach is still the best as I only have to create a temporary second thread/process. Or dropping the error message is also an option just calling `_exit()`.

> Or dropping the error message is also an option just calling _exit().
- Is it an easy solution to omit just a questionable message display?
- Do you need any more detailed output?

Yes, omitting the message is an easy solution. The other scanmem maintainters have put it there to see the number of the signal which killed scanmem. I guess it is just for debugging purposes. Personally I don't need it.

@coolwanglu has touched the sighandler() the last time with commit fbf59282.

I imagine that you will need to fiddle with the function "sigwaitinfo" (and an additional thread) if other contributors would insist on the safe display of a signal number.

Or I return the signal number with `_exit()`. :stuck_out_tongue:

> Or I return the signal number with _exit().

Funny idea. - I guess that such a design approach would conflict with other status codes which would be more meaningful, doesn't it?

Yeah, but I also just checked tracing scanmem with `ltrace`. It shows the triggered signal. So we can drop the error message.

I've checked it without the message. The ugliest thing is the missing newline in that case. Then the scanmem prompt and the console prompt are in the same line.

So I'd like to keep the `fork()` method. Sure, the child has the same signal handler and data but if that one receives a signal, then another child is forked which is okay. This stuff is just for printing a message and to exit. The current implementation is safe IMHO and we shouldn't be pedantic here. This is a game cheating tool and not openssl.

I would prefer an approach which will work without the extra `fork()` call in this use case.

> … The current implementation is safe …

Which one?

The fork implementation. It might only require atomic operations or signal disabling to ensure that there is always only one forked off child.

The other methods all require an extra process/thread permanently. But okay, then I'll implement the self-pipe trick and write the signal number as binary data to the pipe. Okay?

The event handling process will only do the printing like with the fork method. After printing it has to exit itself so that the parent can do the _exit. Or a second pipe is required to inform the parent about completion.

The message display will be portable if it is not performed within a signal handling context, won't it?

> The message display will be portable if it is not performed within a signal handling context, won't it?

Yes, it will be portable. The only disadvantage is that this adds a lot of new code and a thread doing nothing most of the time. The solutions with the second pipe is better as then the signal handler has a timeout (with select()) and is not blocked or requires manual polling when waiting for the child.

You can delete the message display if you do not want to make it portable because of a bit more software development efforts.

Alright, I've implemented the self-pipe trick with two non-blocking pipes, an event handler process, read(), write(), select() and _exit(). I've even disassembled this to confirm async-signal-safety.
@elfring: Please review the latest version of the patch.

I find the introduction of a second pipe questionable in this use case.

Like I already wrote: The second pipe is required as waitpid() is either blocking or non-blocking without a timeout or kernel event which would require manual polling with sleeps. Sleeps and blocking are bad in a signal handler.

> Sleeps and blocking are bad in a signal handler.
- How does [the function name "sleep_sec_unless_input"](https://github.com/sriemer/scanmem/commit/af0450a4fd983423256449b2ac87cc042fd7bf75#diff-2a2978cdc4f40064bba3debe28b999a5R76) fit to this view?
- I suggest to reconsider the shown software design once more.

It differs as it has kernel support for not sleeping longer than required.
No, I won't reconsider the design. Please provide a patch if you know a better solution.

- Please use only a single pipe as it is described for the "self-pipe trick".
- Would an approach around the function "sigwaitinfo" become safer?

So you're rather concerned about the `select()` call in the signal handler as it might race with receiving a further signal. But why does it belong to the async-signal-safe functions then?

I'll implement the `sleep()` polling `waitpid()` with WNOHANG and drop the second pipe. All other methods you've proposed don't work as the parent has to exit to detach from the target process.

I've found a [self-pipe trick implementation](http://man7.org/tlpi/code/online/book/altio/self_pipe.c.html). It is only used to protect a `select()` running in a loop in `main()`. This is definitely not our case here. It protects the `select()` call from signals touching the same data. If `select()` is interrupted by a signal changing the same data, then `select()` might wait indefinitely. The pipe to a child we use is something different. The `sighandler()` also doesn't touch data of the main process.

Maybe I should think about that in [ugtrain](https://github.com/ugtrain/ugtrain) as the main loop communicates with the game process it attacks via FIFOs. `select()` is required for fast reaction on new memory allocations and `free()`s within the game process.

`usleep()` doesn't belong to the async-signal-safe functions. So the only chance is to always sleep one second. This is too much delay. I can only imagine keeping two pipes and using `pselect()` instead of `select()` in `sighandler()` or using `sigprocmask()` at the start of the `sighandler()` to block further signals.
But we have Android support now and Android violates POSIX standards. So `pselect()` might not be available there.
IMHO the `select()` calls are safe as they don't touch the same data as any signal handler.

The fix is merged. So closing. Keeping two pipes and `select()` calls.

Why do you insist to use a second pipe in this software approach?

For not delaying execution too much. Okay, it would be also possible to use select for polling for the exit of the child in the area of milli seconds. The pipe approach is cleaner then and the event handler could be also used for other tasks later on. E.g. there is still a performance issue in GC polling the progress from libscanmem. Maybe that can be moved to the event handler.

- Will the chances increase a bit later to [drop the second pipe from the current signal handler implementation](https://github.com/scanmem/scanmem/commit/b58d9870329920df63e9a2eb45583fd46349410a#diff-2a2978cdc4f40064bba3debe28b999a5R92)?
- Do you juggle with any special target conflicts here?

I've implemented a method now to make only the sighandler() async-signal-safe by using write() for the error message and implementing manual int to str conversion.
@elfring: Please review.

Fixed as discussed. Thanks!

Thanks for [your small source code improvement](https://github.com/scanmem/scanmem/commit/7f468c4c448584460805da53bd48f6ed77c51c62).
