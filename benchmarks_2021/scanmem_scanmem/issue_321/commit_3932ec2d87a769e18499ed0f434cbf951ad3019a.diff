diff --git a/Makefile.am b/Makefile.am
index ab835638..37ff2732 100644
--- a/Makefile.am
+++ b/Makefile.am
@@ -12,6 +12,7 @@ AM_CFLAGS = -std=gnu99 -Wall
 # Utilities library, statically linked in sm and libsm
 noinst_LTLIBRARIES = libutil.la
 libutil_la_SOURCES = common.h \
+    common.c \
     show_message.c
 
 # libscanmem
diff --git a/common.c b/common.c
new file mode 100644
index 00000000..a883376b
--- /dev/null
+++ b/common.c
@@ -0,0 +1,83 @@
+/*
+    Common helper and utility functions
+
+    Copyright (C) 2018        Sebastian Parschauer  <s.parschauer(a)gmx.de>
+
+    This file is part of libscanmem.
+
+    This library is free software: you can redistribute it and/or modify
+    it under the terms of the GNU Lesser General Public License as published
+    by the Free Software Foundation; either version 3 of the License, or
+    (at your option) any later version.
+
+    This library is distributed in the hope that it will be useful,
+    but WITHOUT ANY WARRANTY; without even the implied warranty of
+    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+    GNU Lesser General Public License for more details.
+
+    You should have received a copy of the GNU Lesser General Public License
+    along with this library.  If not, see <http://www.gnu.org/licenses/>.
+*/
+
+#include <errno.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <sys/types.h>
+
+#include "common.h"
+
+/*
+ * We check if a process is running in /proc directly.
+ * Also zombies are detected.
+ *
+ * Requirements: Linux kernel, mounted /proc
+ * Assumption: (pid > 0)  --> Please check your PID before!
+ */
+enum pstate sm_check_process(pid_t pid)
+{
+    FILE *fp = NULL;
+    char *line = NULL;
+    size_t alloc_len = 0;
+    unsigned int lnr = 0;
+    char status = '\0';
+    char path_str[128] = "/proc/";
+    int pr_len, path_len = sizeof("/proc/") - 1;
+
+    /* append $pid/status and check if file exists */
+    pr_len = sprintf((path_str + path_len), "%d/status", pid);
+    if (pr_len <= 0)
+        goto err;
+    path_len += pr_len;
+
+    fp = fopen(path_str, "r");
+    if (!fp) {
+        if (errno != ENOENT)
+            goto err;
+        else
+            return PROC_DEAD;
+    }
+
+    /* read process status */
+    while (getline(&line, &alloc_len, fp) != -1) {
+        if (lnr == 0)
+            ; /* ignore the process name */
+        else if (lnr == 1 && alloc_len > sizeof("State:\t"))
+            status = line[sizeof("State:\t") - 1];
+        else
+            break;
+        lnr++;
+    }
+    if (line)
+        free(line);
+    fclose(fp);
+
+    if (status < 'A' || status > 'Z')
+       goto err;
+
+    /* zombies are not running - parent doesn't wait */
+    if (status == 'Z' || status == 'X')
+        return PROC_ZOMBIE;
+    return PROC_RUNNING;
+err:
+    return PROC_ERR;
+}
diff --git a/common.h b/common.h
index 6653ad56..0c320893 100644
--- a/common.h
+++ b/common.h
@@ -22,6 +22,8 @@
 #ifndef COMMON_H
 #define COMMON_H
 
+#include <sys/types.h>
+
 #ifndef MIN
 # define MIN(a,b) ((a) < (b) ? (a) : (b))
 #endif
@@ -69,4 +71,15 @@
 # define util_getenv getenv
 #endif
 
+/* states returned by sm_check_process() - Sync with GUI! */
+enum pstate {
+    PROC_RUNNING,
+    PROC_ERR,  /* error during detection */
+    PROC_DEAD,
+    PROC_ZOMBIE
+};
+
+/* Function declarations */
+enum pstate sm_check_process(pid_t pid);
+
 #endif /* COMMON_H */
diff --git a/handlers.c b/handlers.c
index 14b0ab34..79bfca14 100644
--- a/handlers.c
+++ b/handlers.c
@@ -294,6 +294,10 @@ bool handler__set(globals_t * vars, char **argv, unsigned argc)
 
         if (cont) {
             sleep(1);
+            if (sm_check_process(vars->target) != PROC_RUNNING) {
+                vars->target = 0;
+                break;
+            }
         } else {
             break;
         }
