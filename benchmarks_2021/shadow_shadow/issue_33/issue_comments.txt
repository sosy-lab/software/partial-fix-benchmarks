Support multiple workers
Plug-ins using OpenSSL are not yet thread-safe. We need to set callbacks as defined in

```
man CRYPTO_lock
```

I believe multiple workers are functional if the plug-in is properly multi-threaded. Scallion does not work at this time with multiple workers, but the other plugins do. This should be fixed in Shadow/Tor.

I'm closing this, as the multi-threaded engine is functional.
