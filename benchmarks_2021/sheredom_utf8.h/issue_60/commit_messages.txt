Merge pull request #57 from bitonic/utf8ncmp-safe

exit in `utf8ncmp` if over limit before reading
Fix the utf8casecmp bug with ascii.

Fixes #60.
Fix the utf8casecmp bug with ascii.

Fixes #60.
Fix the utf8casecmp bug in utf8ncasecmp. Related to #60.
Merge pull request #73 from sheredom/fix_utf8ncasecmp

Fix the utf8casecmp bug in utf8ncasecmp. Related to #60.
