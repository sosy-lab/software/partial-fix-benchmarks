Bug with utf8casecmp?
So I got it working by changing the last part of `utf8casecmp` from:

```c
// If they don't match, then we return which of the original's are less
if (src1_orig_cp < src2_orig_cp) {
  return -1;
} else if (src1_orig_cp > src2_orig_cp) {
  return 1;
}
```

To:

```c
// If they don't match, then we return the difference between the characters
return src1_cp - src2_cp;
```

This matches `strcasecmp`'s output.
I'm so sorry I missed this - all my notifications were disable in GitHub at some point and I don't know why, but I missed every single issue on every lib I have :(

Are you happy to file a PR for the above, or do you want me to file it?
You can go ahead.
`utf8ncasecmp` has the same problem and can be fixed the same way.