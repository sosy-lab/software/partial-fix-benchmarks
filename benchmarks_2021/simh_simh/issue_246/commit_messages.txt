TMXR, CONSOLE: Add optional ability to rate limit input data to approximate bps values

A SET CONSOLE SPEED=nnn, where legal values for nnn are common serial
port rates.  The speed value will attempt to limit the input data rates to a
simulator to approximately the specified bits per second.
VAX, PDP11: Add support to, by default, limit console input data to about 9600bps.

This is necessary to avoid kernel type ahead buffer overruns when a user
pastes a chunk of data into a console session as described in issue #246

Other console input speeds can be set with SET CONSOLE SPEED=nnn
PDP11, VAX: Enhance VH, DZ, DL and DCI devices to provide input rate limiting.  Fix #246

Data arriving on simulated serial ports can arrive faster than the OS running
on the simulated system can deliber it to user mode programs.  This happens
when chunks of data are delivered to the mux from a network connection.
This can be due to a file transfer program (kermit) running on the other end
of a network connection and the packet size being delivered exceeds the
simulated OS's type ahead buffer size OR from users who paste arbitrary
blocks of data into a telnet or console session.
VAX, PDP11: Add support to, by default, limit console input data to about 9600bps.

This is necessary to avoid kernel type ahead buffer overruns when a user
pastes a chunk of data into a console session as described in issue #246

Other console input speeds can be set with SET CONSOLE SPEED=nnn
PDP11, VAX: Enhance VH, DZ, DL and DCI devices to provide input rate limiting.  Fix #246

Data arriving on simulated serial ports can arrive faster than the OS running
on the simulated system can deliber it to user mode programs.  This happens
when chunks of data are delivered to the mux from a network connection.
This can be due to a file transfer program (kermit) running on the other end
of a network connection and the packet size being delivered exceeds the
simulated OS's type ahead buffer size OR from users who paste arbitrary
blocks of data into a telnet or console session.
