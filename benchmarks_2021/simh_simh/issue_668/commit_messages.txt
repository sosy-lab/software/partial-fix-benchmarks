SCP: Avoid changing the status after context for unexpected event status return
SCP: Periodically flush open files

As discussed in #668
SCP: Assure that file buffers get flushed during frontpanel API halts

As reported in #668
SCP: Document the -A switch for the ATTACH command.

As mentioned in #668
