CMake cannot find libintl on FreeBSD 10.1
Right, it seems that I did not account the fact that libintl would be installed under /usr/local rather than /usr on FreeBSD. You should be able to compile normally by adding /usr/local to your include path; but I'll add '/usr/local/include' to the include directories.

Thanks! Added `/usr/local/include` in CMakeLists waved that error. Now onto the next one:

``` cmake
[ 27%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/abort.c.o
[ 30%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/report.c.o
[ 33%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/runner.c.o
[ 36%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/worker.c.o
[ 39%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/stats.c.o
[ 42%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/ordered-set.c.o
[ 45%] Building C object Criterion/CMakeFiles/criterion.dir/src/core/theories.c.o
[ 48%] Building C object Criterion/CMakeFiles/criterion.dir/src/compat/pipe.c.o
[ 51%] Building C object Criterion/CMakeFiles/criterion.dir/src/compat/section.c.o
[ 54%] Building C object Criterion/CMakeFiles/criterion.dir/src/compat/process.c.o
[ 57%] Building C object Criterion/CMakeFiles/criterion.dir/src/compat/basename.c.o
[ 60%] Building C object Criterion/CMakeFiles/criterion.dir/src/compat/mockfile.c.o
/root/projects/Criterion/src/compat/mockfile.c:162:9: error: implicit declaration of function 'fopencookie'

      is invalid in C99 [-Werror,-Wimplicit-function-declaration]
    f = fopencookie(cookie, "w+", (cookie_io_functions_t) {
        ^
/root/projects/Criterion/src/compat/mockfile.c:162:36: error: use of undeclared identifier
      'cookie_io_functions_t'
    f = fopencookie(cookie, "w+", (cookie_io_functions_t) {
                                   ^
2 errors generated.
Criterion/CMakeFiles/criterion.dir/build.make:307: recipe for target 'Criterion/CMakeFiles/criterion.dir/src/compat/mock
file.c.o' failed
gmake[2]: *** [Criterion/CMakeFiles/criterion.dir/src/compat/mockfile.c.o] Error 1
CMakeFiles/Makefile2:132: recipe for target 'Criterion/CMakeFiles/criterion.dir/all' failed
gmake[1]: *** [Criterion/CMakeFiles/criterion.dir/all] Error 2
Makefile:136: recipe for target 'all' failed
gmake: *** [all] Error 2
```

This one is new. It seems that the `#ifdef BSD` gets completely ignored, so I'm assuming that `sys/param.h` does not define it.
I guess I'll have to test against `__FreeBSD__`, `__NetBSD__`, `__OpenBSD__`, and `__DragonFly__`.
