import truffleruby patches
@ aardvark179 can go through these patches and think about what it makes sense to upstream. Thanks very much for the offer.
Sorry for the slow reply. It would definitely be useful for nokogiri to take some of the patches. The follow seem like excellent candidates:
1. The changes to `xml_node_set.c`
2. The changes to `xml_document.c`

The changes to `xml_xpath_context.c` were introduced because we wanted the C stack to be unwound correctly if an error was encountered, which was hard for us to guarantee otherwise. It certainly shouldn't hurt to adopt these changes.

The remaining patches are to work round an issue with calling a managed function from native code with varargs. It would be great if nokogiri could put these changes in, probably behind an `#ifdef TRUFFLERUBY`.

The other really useful thing would be to default to using system libraries with TruffleRuby, we don't yet support linking static libraries and have had issues in the past with picking up a the `.so`s from the gem directory.

Looks like patches have moved to https://github.com/oracle/truffleruby/blob/master/lib/truffle/truffle/patches/nokogiri_patches.rb

I'm hoping to find some time to work on this soon, especially now that we've got TruffleRuby CI coverage.
Indeed, I think 3 of these patches are causing the [5 remaining test failures](https://ci.nokogiri.org/teams/nokogiri-core/pipelines/nokogiri/jobs/truffle-stable/builds/2) (from https://github.com/sparklemotion/nokogiri/issues/2089#issuecomment-702264824), @aardvark179 could you check if they are still needed and what changes could be done in Nokogiri to not need them?
For posterity, in case that build log gets deleted, the errors are:

```
1) Failure:
Nokogiri::XML::TestSaxEntityReference#test_more_sax_entity_reference [/tmp/build/4d9a0f57/nokogiri/test/xml/test_entity_reference.rb:209]:
Expected: ["Entity 'bar' not defined"]
Actual: ["Warning."]

2) Failure:
Nokogiri::XML::TestSaxEntityReference#test_more_sax_entity_reference_with_absolute_path [/tmp/build/4d9a0f57/nokogiri/test/xml/test_entity_reference.rb:209]:
Expected: ["Entity 'bar' not defined"]
Actual: ["Warning."]

3) Failure:
Nokogiri::XML::TestSaxEntityReference#test_sax_entity_reference [/tmp/build/4d9a0f57/nokogiri/test/xml/test_entity_reference.rb:196]:
Expected: ["Entity 'bar' not defined"]
Actual: ["Warning."]

4) Failure:
Nokogiri::XML::TestSaxEntityReference#test_sax_entity_reference_with_absolute_path [/tmp/build/4d9a0f57/nokogiri/test/xml/test_entity_reference.rb:196]:
Expected: ["Entity 'bar' not defined"]
Actual: ["Warning."]

5) Failure:
TestXsltTransforms#test_0001_should not crash when given XPath 2.0 features [/tmp/build/4d9a0f57/nokogiri/test/test_xslt_transforms.rb:359]:
Expected /decimal/ to match # encoding: ASCII-8BIT
# valid: true
"Generic errorGeneric errorGeneric error".

```
@aardvark179 and I looked at those failures, and it seems the main issue is that Sulong does not support creating a native function pointer for variadic functions like https://github.com/sparklemotion/nokogiri/blob/d27cb965d40b20c987d84a7db4c1f9fb508acdc3/ext/nokogiri/xml_sax_parser.c#L196-L211

which is stored in native memory and passed to libxml2 in
https://github.com/sparklemotion/nokogiri/blob/d27cb965d40b20c987d84a7db4c1f9fb508acdc3/ext/nokogiri/xml_sax_parser.c#L275

That seems hard to implement, because Sulong uses libffi (via Truffle NFI) for native calls (when calling to libxml2), and libffi does not support varargs stubs at all.
As @eregon says, the current variadic error functions look like they will be very hard to support. Luckily `libxml2` has another way to handle errors that does not require variadic callbacks. I'll try and test how well that approach works and update this issue soon. 
@aardvark179 Can you clarify? I'm cleaning up the error-handling code right now (see https://github.com/sparklemotion/nokogiri/issues/1610, https://github.com/sparklemotion/nokogiri/pull/2096, and https://github.com/sparklemotion/nokogiri/issues/2097) and would be happy to integrate any suggestions you might have.
So it looks like you should be able to remove uses of the generic error handler and simply rely on the structured error handler. I had to make one small change to `test_document.rb` at line 540 in relation to this as you now get an `Nokogiri::XML::XPath::SyntaxError` instead of a Runtime error. Unfortunately `libxslt` doesn't appear to have an equivalent structured error function, so we can't get rid of the problem entirely.
Ah, I see what you're saying. Sure, we can probably drop the generic error handler usage; and I would like to omit the use of varargs in TruffleRuby within Nokogiri's code (rather than via a patch). Let's plan on me doing that as part of the work at https://github.com/sparklemotion/nokogiri/pull/2096 and I'll ping back here when that's ready to go and ask y'all to review it.
@aardvark179 checked and with https://github.com/sparklemotion/nokogiri/pull/2193 + `--use-system-libraries=false`, and removing the patches for Nokogiri (PR to do that automatically upcoming in TruffleRuby), all Nokogiri tests pass!
That means we should be able to have TruffleRuby passing in Nokogiri's CI.

Performance also seems fine when running libxml2/libxslt on Sulong, so I think we might just default to that at some point (use the vendored libxml2 like for CRuby, but no support for precompiled gems yet).
:thinking: Wow. I'll make time this weekend to merge, and update CI and the installation docs.
https://github.com/oracle/truffleruby/commit/9a5b42085174ec7b13b663c1641f74bd43077557 is the PR I was mentioning above in TruffleRuby. That's now merged, so when using `--use-system-libraries=false` the patches have no effect.
I just tried nokogiri 1.11.2 and it works well.
We'll default to the vendored libxml2/libxslt in TruffleRuby 21.1 unless we discover a major issue, that notably avoids some headaches of installing extra system packages which has been quite annoying.
I'm going to close this, then! Thanks for the amazing collaboration!
