format: overhaul test/xml/test_node_set.rb
fix: allow NodeSets to contain Nodes from multiple Documents

Specifically, this means we've added a mark callback for the NodeSet
which marks each of the contained objects.

Previously we skipped explicitly marking the contained objects due to
the assumption that all the nodes would be from the same document as
the NodeSet itself.

Fixes #1952.
fix: allow NodeSets to contain Nodes from multiple Documents

Specifically, this means we've added a mark callback for the NodeSet
which marks each of the contained objects.

Previously we skipped explicitly marking the contained objects due to
the assumption that all the nodes would be from the same document as
the NodeSet itself.

Fixes #1952.
fix: allow NodeSets to contain Nodes from multiple Documents

Specifically, this means we've added a mark callback for the NodeSet
which marks each of the contained objects.

Previously we skipped explicitly marking the contained objects due to
the assumption that all the nodes would be from the same document as
the NodeSet itself.

Fixes #1952.
fix: allow NodeSets to contain Nodes from multiple Documents

Specifically, this means we've added a mark callback for the NodeSet
which marks each of the contained objects.

Previously we skipped explicitly marking the contained objects due to
the assumption that all the nodes would be from the same document as
the NodeSet itself.

Fixes #1952.
Merge pull request #2186 from sparklemotion/1952-node-set-segfault

Allow NodeSet to contain Nodes from multiple Documents (fix for #1952)

---

**What problem is this PR intended to solve?**

fix: allow NodeSets to contain Nodes from multiple Documents

Specifically, this means we've added a mark callback for the NodeSet which marks each of the contained objects.

Previously we skipped explicitly marking the contained objects due to the assumption that all the nodes would be from the same document as the NodeSet itself.

Fixes #1952.


**Have you included adequate test coverage?**

Yes, additional tests created.


**Does this change affect the behavior of either the C or the Java implementations?**

This change allows the C implementation to do what the Java implementation has always allowed.
ci: reduce valgrind suppressions

Since fixing #1952 I've wanted to revisit the valgrind suppressions to
see what's left. These suppressions represent what I saw in docker
images on my dev machine:

- on Ruby 2.7 and 3.0 startup (iseq_peephole_optimize)
- enumerators seem to confuse Valgrind (mark_locations_array/gc_mark_stacked_objects)

ci: add 2.7 valgrind suppressions to ignore enumerator warnings

squash
ci: reduce valgrind suppressions

Since fixing #1952 I've wanted to revisit the valgrind suppressions to
see what's left. These suppressions represent what I saw in docker
images on my dev machine:

- on Ruby 2.7 and 3.0 startup (iseq_peephole_optimize)
- enumerators seem to confuse Valgrind (mark_locations_array/gc_mark_stacked_objects)
ci: reduce valgrind suppressions

Since fixing #1952 I've wanted to revisit the valgrind suppressions to
see what's left. These suppressions represent what I saw in docker
images on my dev machine:

- on Ruby 2.7 and 3.0 startup (iseq_peephole_optimize)
- enumerators seem to confuse Valgrind (mark_locations_array/gc_mark_stacked_objects)
ci: reduce valgrind suppressions

Since fixing #1952 I've wanted to revisit the valgrind suppressions to
see what's left. These suppressions represent what I saw in docker
images on my dev machine:

- on Ruby 2.7 and 3.0 startup (iseq_peephole_optimize)
- enumerators seem to confuse Valgrind (mark_locations_array/gc_mark_stacked_objects)
ci: reduce valgrind suppressions

Since fixing #1952 I've wanted to revisit the valgrind suppressions to
see what's left. I'm leaving only the suppressions I see for Ruby 2.7
and 3.0 startup (iseq_peephole_optimize).
Merge pull request #2209 from sparklemotion/flavorjones-skip-enumerator-in-valgrind

ci: skip Nodeset enumerator test in valgrind

---

Since fixing #1952 I've wanted to revisit the valgrind suppressions to see what's left. I'm leaving only the suppressions I see for Ruby 2.7 and 3.0 startup (iseq_peephole_optimize), and I'm skipping the NodeSet enumerator test since I know it confuses valgrind.
