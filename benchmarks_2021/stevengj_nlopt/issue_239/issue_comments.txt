Error for large dimensionality
the recursive nlopt_qsort_r function blows up the stack as the pivot is bad (last element), its kind of a worst case scenario at the first iteration:
```
#37420 0x00007ffff7ba6296 in nlopt_qsort_r (base_=0x7ffff7e10010, nmemb=87998, size=<optimized out>, thunk=<optimized out>, compar=<optimized out>) at /home/schueller/projects/nlopt/src/util/qsort_r.c:100
#37421 0x00007ffff7ba6296 in nlopt_qsort_r (base_=0x7ffff7e10010, nmemb=87999, size=<optimized out>, thunk=<optimized out>, compar=<optimized out>) at /home/schueller/projects/nlopt/src/util/qsort_r.c:100
#37422 0x00007ffff7ba6296 in nlopt_qsort_r (base_=0x7ffff7e10010, nmemb=88000, size=<optimized out>, thunk=<optimized out>, compar=<optimized out>) at /home/schueller/projects/nlopt/src/util/qsort_r.c:100
#37423 0x00007ffff7b7c418 in sbplx_minimize (n=<optimized out>, f=<optimized out>, f_data=<optimized out>, lb=0x7ffff7f12010, ub=0x7ffff7e66010, x=0x7ffff6cad010, minf=0x7fffffffdc68, xstep0=0x7ffff6c01010, stop=0x7fffffffdb88)
    at /home/schueller/projects/nlopt/src/algs/neldermead/sbplx.c:119
```

it goes fine if I call the system qsort_r:
```
--- a/src/util/qsort_r.c
+++ b/src/util/qsort_r.c
@@ -67,12 +67,12 @@ static void swap(void *a_, void *b_, size_t size)
 
 void nlopt_qsort_r(void *base_, size_t nmemb, size_t size, void *thunk, int (*compar) (void *, const void *, const void *))
 {
-#ifdef HAVE_QSORT_R_damn_it_use_my_own
+#if 1
     /* Even if we could detect glibc vs. BSD by appropriate
        macrology, there is no way to make the calls compatible
        without writing a wrapper for the compar function...screw
        this. */
-    qsort_r(base_, nmemb, size, thunk, compar);
+    qsort_r(base_, nmemb, size, compar, thunk);
 #else
     char *base = (char *) base_;
     if (nmemb < 10) {           /* use O(nmemb^2) algorithm for small enough nmemb */
```
9ff8438a0f8483b6cce8f78bbc18bd7008c5097a throws a ton of memory errors (with the code at https://github.com/stevengj/nlopt/issues/239#issue-387808414):
```
$ valgrind ./tutorial                                                                     
==31310== Memcheck, a memory error detector
==31310== Copyright (C) 2002-2015, and GNU GPL'd, by Julian Seward et al.
==31310== Using Valgrind-3.11.0 and LibVEX; rerun with -h for copyright info
==31310== Command: ./tutorial
==31310== 
==31310== Invalid read of size 8
==31310==    at 0x4E7D69B: p_compare (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x510C1B0: msort_with_tmp.part.0 (msort.c:65)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==  Address 0x60ab3bc is 28 bytes after a block of size 3,520,000 in arena "client"
==31310== 
==31310== Invalid read of size 8
==31310==    at 0x4E7D69B: p_compare (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x510C1B0: msort_with_tmp.part.0 (msort.c:65)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==  Address 0x60ab3b0 is 16 bytes after a block of size 3,520,000 alloc'd
==31310==    at 0x4C2DB8F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==31310==    by 0x4E7D8B0: sbplx_minimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4E9A7D0: nlopt_optimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4009AC: main (in /home/rquey/tmp/tutorial)
==31310== Invalid read of size 8
==31310==    at 0x4E7D69B: p_compare (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x510C1B0: msort_with_tmp.part.0 (msort.c:65)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==  Address 0x60ab3ec is 12 bytes inside an unallocated block of size 601,088 in arena "client"
==31310== 
==31310== Invalid read of size 8
==31310==    at 0x4E7D69B: p_compare (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x510C1B0: msort_with_tmp.part.0 (msort.c:65)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510BEDC: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEDC: msort_with_tmp.part.0 (msort.c:54)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==  Address 0x60ab40c is 44 bytes inside an unallocated block of size 601,088 in arena "client"
==31310== 
==31310== 
==31310== Invalid read of size 8
==31310==    at 0x4E7D69B: p_compare (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x510C1B0: msort_with_tmp.part.0 (msort.c:65)
==31310==    by 0x510BEC6: msort_with_tmp (msort.c:45)
==31310==    by 0x510BEC6: msort_with_tmp.part.0 (msort.c:53)
==31310==    by 0x510C69E: msort_with_tmp (msort.c:45)
==31310==    by 0x510C69E: qsort_r (msort.c:297)
==31310==    by 0x4E7DC92: sbplx_minimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4E9A7D0: nlopt_optimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4009AC: main (in /home/rquey/tmp/tutorial)
==31310==  Address 0x60ab39c is 3,519,996 bytes inside a block of size 3,520,000 alloc'd
==31310==    at 0x4C2DB8F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==31310==    by 0x4E7D8B0: sbplx_minimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4E9A7D0: nlopt_optimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4009AC: main (in /home/rquey/tmp/tutorial)
==31310== 
==31310== Invalid read of size 8
==31310==    at 0x4E7D69B: p_compare (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x510C1B0: msort_with_tmp.part.0 (msort.c:65)
==31310==    by 0x510C69E: msort_with_tmp (msort.c:45)
==31310==    by 0x510C69E: qsort_r (msort.c:297)
==31310==    by 0x4E7DC92: sbplx_minimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4E9A7D0: nlopt_optimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4009AC: main (in /home/rquey/tmp/tutorial)
==31310==  Address 0x60ab3a0 is 0 bytes after a block of size 3,520,000 alloc'd
==31310==    at 0x4C2DB8F: malloc (in /usr/lib/valgrind/vgpreload_memcheck-amd64-linux.so)
==31310==    by 0x4E7D8B0: sbplx_minimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4E9A7D0: nlopt_optimize (in /home/rquey/tmp/libnlopt.so.0)
==31310==    by 0x4009AC: main (in /home/rquey/tmp/tutorial)
==31310== 
found minimum = -9.000000
==31310== 
==31310== HEAP SUMMARY:
==31310==     in use at exit: 28,232,928 bytes in 6 blocks
==31310==   total heap usage: 14 allocs, 8 frees, 63,434,600 bytes allocated
==31310== 
==31310== LEAK SUMMARY:
==31310==    definitely lost: 7,040,224 bytes in 2 blocks
==31310==    indirectly lost: 14,080,000 bytes in 2 blocks
==31310==      possibly lost: 7,040,000 bytes in 1 blocks
==31310==    still reachable: 72,704 bytes in 1 blocks
==31310==         suppressed: 0 bytes in 0 blocks
==31310== Rerun with --leak-check=full to see details of leaked memory
==31310== 
==31310== For counts of detected and suppressed errors, rerun with: -v
==31310== ERROR SUMMARY: 592517 errors from 6 contexts (suppressed: 0 from 0)
```
@rquey could you try the patch at #240 ?
@jschueller Works fine! I also tried with 10<sup>6</sup> variables.