remove colorized build output
force hardware-reset see:https://github.com/texane/stlink/issues/220#issuecomment-234104267
Enable --reset for NC hardware reset line

(Closes #220)
General Project Update

- Added basic security policy
- Updated info on version support for macOS
- Updated tutorial.md
> Added reference for st-info --probe command
> Added section on unknown chip id error
   (Closes #107, Closes #568, Closes #669)
> Added information on HW reset pin issue
   (Closes  #220, Closes #238)
