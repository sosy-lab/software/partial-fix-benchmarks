Incomplete support for STM32G0xx MCUs
It looks like I cannot push a new branch so I cannot create a pull request.
@mrfirmware Fork the repo, push the branch to your fork, then create a pull request from that branch against the upstream repo.
We don't use the fork-and-branch model at work; just branches so I'm not having any luck pushing to my forked, upstream repo. Here's what I have...

$ git remote -v
origin	git@github.com:mrfirmware/stlink.git (fetch)
origin	git@github.com:mrfirmware/stlink.git (push)
upstream	git@github.com:mrfirmware/stlink.git (fetch)
upstream	git@github.com:mrfirmware/stlink.git (push)

I created a branch 'stm32g0xx-improvements'

git push upstream stm32g0xx-improvements

times out with: fatal: Could not read from remote repository.

I love git vs. sccs, cvs, and svn but I also hate it.
@mrfirmware you can also push to your repos master, doesn't need to be a branch. 

(OTOH, that timeout might be your corporate firewall?)
I colleague at work showed me how to get past my authentication issue so I was able to create a PR for my small change set. Hopefully it is useful.
Any progress so far? I'm really hoping to be able to use the tools with G0 chips.
The PR is up.
Closed by #825 and #857.