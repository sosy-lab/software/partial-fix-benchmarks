tests: test_runner, working simple single-ESP "solo" test cases
tests/malloc: Allow malloc to fail when out of RAM, add heap test
cases.

Fixes #76.
brk/malloc: Allow malloc to fail when out of RAM

Fixes #76.
