Further Linux VST3 Progress (#1047)

With this diff, the Limux VST3 works fine in Reaper. Still
a crash on exit. Addresses #514
Status panel cleanups

1: Antialias
2: Center label
3: Don't over-extend save button region
4: Don't mis-toggle tun when no tuning is around

Closes #1048
Status panel and UI cleanups

1: Antialias
2: Center label
3: Don't over-extend save button region
4: Don't mis-toggle tun when no tuning is around
5: Only respond to wheel if the mod control is a metacontroller,
   avoiding a crash when wheeling over non-metacontroller controllers

Closes #1048
Status panel and UI cleanups (#1049)

1: Antialias
2: Center label
3: Don't over-extend save button region
4: Don't mis-toggle tun when no tuning is around
5: Only respond to wheel if the mod control is a metacontroller,
   avoiding a crash when wheeling over non-metacontroller controllers

Closes #1048
