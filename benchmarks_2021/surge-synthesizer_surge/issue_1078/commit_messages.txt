Rebuild controls for a couple of special cases (#1074)

When OSC type changes or envelope mode changes, the results are
really non-local. (OSC type lays out all the sliders again;
envelope mode changes the envelope control switches). To address
this we can force an editor rebuild when these are toggled, kind
of like we do with FX (although using a slightly different API)

Closes #206
Addresses part of #702
Support 'srge' wav metadata

.wav files with no metadata load as one shots and there's a variety
of metadata we support from various other systems, including
the serum and misuse of loop and stuff. But with this diff we
add a 'srge' metadata block which contains explicit information
about table size. Also added a python script to add this.

Addresses #1078
Support 'srge' wav metadata (#1079)

.wav files with no metadata load as one shots and there's a variety
of metadata we support from various other systems, including
the serum and misuse of loop and stuff. But with this diff we
add a 'srge' metadata block which contains explicit information
about table size. Also added a python script to add this.

Addresses #1078
Update wt-tool with some normalizations

wt-tool.py adds a -norm argument which can modify the inbound
i16 wavetables by halving them or setting their peak to 16384
across the set, avoiding the i15 clipping discussed in #1078
Update wt-tool with some normalizations (#1081)

wt-tool.py adds a -norm argument which can modify the inbound
i16 wavetables by halving them or setting their peak to 16384
across the set, avoiding the i15 clipping discussed in #1078
Two small fixes approaching 1.6.2

1. Fix the normalization code in wt-tool.py for some edge cases.
   Addresses #1078

2. Fix the CC menu mapping for an off-by-one. Closes #1082
Two small fixes approaching 1.6.2 (#1088)

1. Fix the normalization code in wt-tool.py for some edge cases.
   Addresses #1078

2. Fix the CC menu mapping for an off-by-one. Closes #1082
