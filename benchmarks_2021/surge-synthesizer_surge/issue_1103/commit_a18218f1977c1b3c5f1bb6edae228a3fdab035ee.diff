diff --git a/src/common/gui/CLFOGui.cpp b/src/common/gui/CLFOGui.cpp
index edd67b488..e0dce59b6 100644
--- a/src/common/gui/CLFOGui.cpp
+++ b/src/common/gui/CLFOGui.cpp
@@ -33,6 +33,19 @@ void drawtri(CRect r, CDrawContext* context, int orientation)
 }
 
 void CLFOGui::draw(CDrawContext* dc)
+{
+   /*
+   ** As of 1.6.2, the linux vectorized drawing is slow and scales imporoperly with zoom, so
+   ** return to the original bitmap drawing until we resolve. See issue #1103.
+   */
+#if LINUX
+   drawBitmap(dc);
+#else
+   drawVectorized(dc);
+#endif
+}
+
+void CLFOGui::drawVectorized(CDrawContext* dc)
 {
    assert(lfodata);
    assert(storage);
@@ -416,6 +429,336 @@ void CLFOGui::draw(CDrawContext* dc)
    setDirty(false);
 }
 
+void CLFOGui::drawBitmap(CDrawContext* dc)
+{
+   assert(lfodata);
+   assert(storage);
+   assert(ss);
+
+   auto size = getViewSize();
+   CRect outer(size);
+   outer.inset(margin, margin);
+   CRect leftpanel(outer);
+   CRect maindisp(outer);
+   leftpanel.right = lpsize + leftpanel.left;
+   maindisp.left = leftpanel.right + 4 + 15;
+   maindisp.top += 1;
+   maindisp.bottom -= 1;
+
+   cdisurf->begin();
+#if MAC
+   cdisurf->clear(0x0090ffff);
+#else
+   cdisurf->clear(0xffff9000);
+#endif
+   int w = cdisurf->getWidth();
+   int h = cdisurf->getHeight();
+
+   if (ss && lfodata->shape.val.i == ls_stepseq)
+   {
+      // I know I could do the math to convert these colors but I would rather leave them as
+      // literals for the compiler so we don't have to shift them at runtime. See issue #141 in
+      // surge github
+#if MAC
+#define PIX_COL(a, b) b
+#else
+#define PIX_COL(a, b) a
+#endif
+      // Step Sequencer Colors. Remember mac is 0xRRGGBBAA and mac is 0xAABBGGRR
+      int stepMarker = PIX_COL(0xff000000, 0x000000ff);
+      int loopRegionHi = PIX_COL(0xffc6e9c4, 0xc4e9c6ff);
+      int loopRegionLo = PIX_COL(0xffb6d9b4, 0xb4d9b6ff);
+      int noLoopHi = PIX_COL(0xffdfdfdf, 0xdfdfdfff);
+      int noLoopLo = PIX_COL(0xffcfcfcf, 0xcfcfcfff);
+      int grabMarker =
+          PIX_COL(0x00087f00, 0x007f08ff); // Surely you can't mean this to be fully transparent?
+                                           // But leave non-mac unch
+
+      for (int i = 0; i < n_stepseqsteps; i++)
+      {
+         CRect rstep(maindisp), gstep;
+         rstep.offset(-size.left - splitpoint, -size.top);
+         rstep.left += scale * i;
+         rstep.right = rstep.left + scale - 1;
+         rstep.bottom -= margin2 + 1;
+         CRect shadow(rstep);
+         shadow.inset(-1, -1);
+         cdisurf->fillRect(shadow, skugga);
+         if (edit_trigmask)
+         {
+            gstep = rstep;
+            rstep.top += margin2;
+            gstep.bottom = rstep.top - 1;
+            gaterect[i] = gstep;
+            gaterect[i].offset(size.left + splitpoint, size.top);
+
+            if (ss->trigmask & (1 << i))
+               cdisurf->fillRect(gstep, stepMarker);
+            else if ((i >= ss->loop_start) && (i <= ss->loop_end))
+               cdisurf->fillRect(gstep, (i & 3) ? loopRegionHi : loopRegionLo);
+            else
+               cdisurf->fillRect(gstep, (i & 3) ? noLoopHi : noLoopLo);
+         }
+         if ((i >= ss->loop_start) && (i <= ss->loop_end))
+            cdisurf->fillRect(rstep, (i & 3) ? loopRegionHi : loopRegionLo);
+         else
+            cdisurf->fillRect(rstep, (i & 3) ? noLoopHi : noLoopLo);
+         steprect[i] = rstep;
+         steprect[i].offset(size.left + splitpoint, size.top);
+         CRect v(rstep);
+         int p1, p2;
+         if (lfodata->unipolar.val.b)
+         {
+            v.top = v.bottom - (int)(v.getHeight() * ss->steps[i]);
+         }
+         else
+         {
+            p1 = v.bottom - (int)((float)0.5f + v.getHeight() * (0.5f + 0.5f * ss->steps[i]));
+            p2 = (v.bottom + v.top) * 0.5;
+            v.top = min(p1, p2);
+            v.bottom = max(p1, p2) + 1;
+         }
+         // if (p1 == p2) p2++;
+         cdisurf->fillRect(v, stepMarker);
+      }
+
+      rect_steps = steprect[0];
+      rect_steps.right = steprect[n_stepseqsteps - 1].right;
+      rect_steps_retrig = gaterect[0];
+      rect_steps_retrig.right = gaterect[n_stepseqsteps - 1].right;
+
+      rect_ls = maindisp;
+      rect_ls.offset(-size.left - splitpoint, -size.top);
+      rect_ls.top = rect_ls.bottom - margin2;
+      rect_le = rect_ls;
+
+      rect_ls.left += scale * ss->loop_start - 1;
+      rect_ls.right = rect_ls.left + margin2;
+      rect_le.right = rect_le.left + scale * (ss->loop_end + 1);
+      rect_le.left = rect_le.right - margin2;
+
+      cdisurf->fillRect(rect_ls, grabMarker);
+      cdisurf->fillRect(rect_le, grabMarker);
+      CRect linerect(rect_ls);
+      linerect.top = maindisp.top - size.top;
+      linerect.right = linerect.left + 1;
+      cdisurf->fillRect(linerect, grabMarker);
+      linerect = rect_le;
+      linerect.top = maindisp.top - size.top;
+      linerect.left = linerect.right - 1;
+      cdisurf->fillRect(linerect, grabMarker);
+
+      rect_ls.offset(size.left + splitpoint, size.top);
+      rect_le.offset(size.left + splitpoint, size.top);
+   }
+   else
+   {
+      pdata tp[n_scene_params];
+      {
+         tp[lfodata->delay.param_id_in_scene].i = lfodata->delay.val.i;
+         tp[lfodata->attack.param_id_in_scene].i = lfodata->attack.val.i;
+         tp[lfodata->hold.param_id_in_scene].i = lfodata->hold.val.i;
+         tp[lfodata->decay.param_id_in_scene].i = lfodata->decay.val.i;
+         tp[lfodata->sustain.param_id_in_scene].i = lfodata->sustain.val.i;
+         tp[lfodata->release.param_id_in_scene].i = lfodata->release.val.i;
+
+         tp[lfodata->magnitude.param_id_in_scene].i = lfodata->magnitude.val.i;
+         tp[lfodata->rate.param_id_in_scene].i = lfodata->rate.val.i;
+         tp[lfodata->shape.param_id_in_scene].i = lfodata->shape.val.i;
+         tp[lfodata->start_phase.param_id_in_scene].i = lfodata->start_phase.val.i;
+         tp[lfodata->deform.param_id_in_scene].i = lfodata->deform.val.i;
+         tp[lfodata->trigmode.param_id_in_scene].i = lm_keytrigger;
+      }
+
+      LfoModulationSource* tlfo = new LfoModulationSource();
+      tlfo->assign(storage, lfodata, tp, 0, ss, true);
+      tlfo->attack();
+      CRect boxo(maindisp);
+      boxo.offset(-size.left - splitpoint, -size.top);
+
+      unsigned int column[512];
+      int column_d[512];
+      int h2 = cdisurf->getHeight();
+      assert(h < 512);
+      float lastval = 0;
+
+      int midline = (int)((float)((0.5f + 0.4f * 0.f) * h));
+      int topline = (int)((float)((0.5f + 0.4f * 1.f) * h));
+      int bottomline = (int)((float)((0.5f + 0.4f * -1.f) * h)) + 1;
+      const int aa_bs = 3;
+      const int aa_samples = 1 << aa_bs;
+      int last_imax = -1, last_imin = -1;
+
+      for (int y = 0; y < h2; y++)
+         column_d[y] = 0;
+
+      for (int x = boxo.left; x < boxo.right; x++)
+      {
+         for (int y = 0; y < h2; y++)
+            column[y] = 0;
+
+         /*for(int i=0; i<16; i++) tlfo->process_block();
+         int p = h - (int)((float)0.5f + h * (0.5f + 0.5f*tlfo->output));
+         cdisurf->setPixel(x,p,0x00000000);*/
+
+         for (int s = 0; s < aa_samples; s++)
+         {
+            tlfo->process_block();
+
+            float val = -tlfo->output;
+            val = (float)((0.5f + 0.4f * val) * h);
+            lastval = val;
+            float v_min = val;
+            float v_max = val;
+
+            v_min = val - 0.5f;
+            v_max = val + 0.5f;
+            int imin = (int)v_min;
+            int imax = (int)v_max;
+            float imax_frac = (v_max - imax);
+            float imin_frac = 1 - (v_min - imin);
+
+            // envelope outline
+            float eval = tlfo->env_val * lfodata->magnitude.val.f;
+            unsigned int ieval1 = (int)((float)((0.5f + 0.4f * eval) * h * 128));
+            unsigned int ieval2 = (int)((float)((0.5f - 0.4f * eval) * h * 128));
+
+            if ((ieval1 >> 7) < (h - 1))
+            {
+               column[ieval1 >> 7] += 128 - (ieval1 & 127);
+               column[(ieval1 >> 7) + 1] += ieval1 & 127;
+            }
+
+            if ((ieval2 >> 7) < (h - 1))
+            {
+               column[ieval2 >> 7] += 128 - (ieval2 & 127);
+               column[(ieval2 >> 7) + 1] += ieval2 & 127;
+            }
+
+            if (imax == imin)
+               imax++;
+
+            if (imin < 0)
+               imin = 0;
+            int dimax = imax, dimin = imin;
+
+            if ((x > boxo.left) || (s > 0))
+            {
+               if (dimin > last_imax)
+                  dimin = last_imax;
+               else if (dimax < last_imin)
+                  dimax = last_imin;
+            }
+            dimin = limit_range(dimin, 0, h);
+            dimax = limit_range(dimax, 0, h);
+
+            // int yp = (int)((float)(size.height() * (-osc->output[block_pos]*0.5+0.5)));
+            // yp = limit_range(yp,0,h-1);
+
+            column[dimin] += ((int)((float)imin_frac * 255.f));
+            column[dimax + 1] += ((int)((float)imax_frac * 255.f));
+            for (int b = (dimin + 1); b < (dimax + 1); b++)
+            {
+               column_d[b & 511] = aa_samples;
+            }
+            last_imax = imax;
+            last_imin = imin;
+
+            for (int y = 0; y < h; y++)
+            {
+               if (column_d[y] > 0)
+                  column[y] += 256;
+               column_d[y]--;
+            }
+         }
+         column[midline] = max((unsigned int)128 << aa_bs, column[midline]);
+         column[topline] = max((unsigned int)32 << aa_bs, column[topline]);
+         column[bottomline] = max((unsigned int)32 << aa_bs, column[bottomline]);
+
+         for (int y = 0; y < h2; y++)
+         {
+            cdisurf->setPixel(x, y,
+                              coltable[min((unsigned int)255, /*(y>>1) +*/ (column[y] >> aa_bs))]);
+         }
+      }
+      delete tlfo;
+   }
+
+   CPoint sp(0, 0);
+   CRect sr(size.left + splitpoint, size.top, size.right, size.bottom);
+   cdisurf->commit();
+   cdisurf->draw(dc, sr, sp);
+
+   /*if(lfodata->shape.val.i != ls_stepseq)
+   {
+           CRect tr(size);
+           tr.x = tr.x2 - 60;
+           tr.y2 = tr.y + 50;
+           CColor ctext = {224,126,0,0xff};
+           dc->setFontColor(ctext);
+           dc->setFont(patchNameFont);
+           dc->drawString("LFO 1",tr,false,kCenterText);
+   }*/
+
+   CColor cskugga = {0x5d, 0x5d, 0x5d, 0xff};
+   CColor cgray = {0x97, 0x98, 0x9a, 0xff};
+   CColor cselected = {0xfe, 0x98, 0x15, 0xff};
+   // CColor blackColor (0, 0, 0, 0);
+   dc->setFrameColor(cskugga);
+   dc->setFont(lfoTypeFont);
+
+   rect_shapes = leftpanel;
+   for (int i = 0; i < n_lfoshapes; i++)
+   {
+      CRect tb(leftpanel);
+      tb.top = leftpanel.top + 10 * i;
+      tb.bottom = tb.top + 10;
+      if (i == lfodata->shape.val.i)
+      {
+         CRect tb2(tb);
+         tb2.left++;
+         tb2.top += 0.5;
+         tb2.inset(2, 1);
+         tb2.offset(0, 1);
+         dc->setFillColor(cselected);
+         dc->drawRect(tb2, kDrawFilled);
+      }
+      // else dc->setFillColor(cgray);
+      // dc->fillRect(tb);
+      shaperect[i] = tb;
+      // tb.offset(0,-1);
+      dc->setFontColor(kBlackCColor);
+      tb.top += 1.6; // now the font is smaller and the box is square, smidge down the text
+      dc->drawString(ls_abberations[i], tb);
+   }
+
+   if (ss && lfodata->shape.val.i == ls_stepseq)
+   {
+      ss_shift_left = leftpanel;
+      ss_shift_left.offset(53, 23);
+      ss_shift_left.right = ss_shift_left.left + 12;
+      ss_shift_left.bottom = ss_shift_left.top + 34;
+
+      dc->setFillColor(cskugga);
+      dc->drawRect(ss_shift_left, kDrawFilled);
+      ss_shift_left.inset(1, 1);
+      ss_shift_left.bottom = ss_shift_left.top + 16;
+
+      dc->setFillColor(cgray);
+      dc->drawRect(ss_shift_left, kDrawFilled);
+      drawtri(ss_shift_left, dc, -1);
+
+      ss_shift_right = ss_shift_left;
+      ss_shift_right.offset(0, 16);
+      dc->setFillColor(cgray);
+      dc->drawRect(ss_shift_right, kDrawFilled);
+      drawtri(ss_shift_right, dc, 1);
+      // ss_shift_left,ss_shift_right;
+   }
+
+   setDirty(false);
+}
+
 enum
 {
    cs_null = 0,
diff --git a/src/common/gui/CLFOGui.h b/src/common/gui/CLFOGui.h
index 0c450d34a..49459a99f 100644
--- a/src/common/gui/CLFOGui.h
+++ b/src/common/gui/CLFOGui.h
@@ -90,6 +90,8 @@ class CLFOGui : public VSTGUI::CControl
       delete cdisurf;
    }
    virtual void draw(VSTGUI::CDrawContext* dc);
+   void drawVectorized(VSTGUI::CDrawContext* dc);
+   void drawBitmap(VSTGUI::CDrawContext* dc);
 
 protected:
    LFOStorage* lfodata;
diff --git a/src/common/gui/COscillatorDisplay.cpp b/src/common/gui/COscillatorDisplay.cpp
index da667965b..211702dd3 100644
--- a/src/common/gui/COscillatorDisplay.cpp
+++ b/src/common/gui/COscillatorDisplay.cpp
@@ -24,6 +24,15 @@ const int wtbheight = 12;
 extern CFontRef displayFont;
 
 void COscillatorDisplay::draw(CDrawContext* dc)
+{
+#if LINUX
+   drawBitmap(dc);
+#else
+   drawVector(dc);
+#endif
+}
+
+void COscillatorDisplay::drawVector(CDrawContext* dc)
 {
    pdata tp[2][n_scene_params]; // 0 is orange, 1 is blue
    Oscillator* osces[2];
@@ -345,6 +354,216 @@ void COscillatorDisplay::draw(CDrawContext* dc)
    setDirty(false);
 }
 
+void COscillatorDisplay::drawBitmap(CDrawContext* dc)
+{
+   pdata tp[n_scene_params];
+   tp[oscdata->pitch.param_id_in_scene].f = 0;
+   for (int i = 0; i < n_osc_params; i++)
+      tp[oscdata->p[i].param_id_in_scene].i = oscdata->p[i].val.i;
+   Oscillator* osc = spawn_osc(oscdata->type.val.i, storage, oscdata, tp);
+
+   cdisurf->begin();
+   cdisurf->clear(0xffffffff);
+
+   int h2 = cdisurf->getHeight();
+   int h = h2;
+   assert(h < 512);
+   if (uses_wavetabledata(oscdata->type.val.i))
+      h -= wtbheight;
+
+   unsigned int column[512];
+   int column_d[512];
+   float lastval = 0;
+   int midline = h >> 1;
+   int topline = midline - 0.4f * h;
+   int bottomline = midline + 0.4f * h;
+   const int aa_bs = 4;
+   const int aa_samples = 1 << aa_bs;
+   int last_imax = -1, last_imin = -1;
+   if (osc)
+   {
+      // srand(2);
+      float disp_pitch_rs = disp_pitch + 12.0 * log2(dsamplerate / 44100.0);
+      bool use_display = osc->allow_display();
+
+      // Mis-install check #2
+      if (uses_wavetabledata(oscdata->type.val.i) && storage->wt_list.size() == 0)
+         use_display = false;
+
+      if (use_display)
+         osc->init(disp_pitch_rs, true);
+      int block_pos = BLOCK_SIZE_OS;
+      for (int y = 0; y < h2; y++)
+         column_d[y] = 0;
+
+      for (int x = 0; x < getWidth(); x++)
+      {
+         for (int y = 0; y < h2; y++)
+            column[y] = 0;
+         for (int s = 0; s < aa_samples; s++)
+         {
+            if (use_display && (block_pos >= BLOCK_SIZE_OS))
+            {
+               if (uses_wavetabledata(oscdata->type.val.i))
+               {
+                  storage->CS_WaveTableData.enter();
+                  osc->process_block(disp_pitch_rs);
+                  block_pos = 0;
+                  storage->CS_WaveTableData.leave();
+               }
+               else
+               {
+                  osc->process_block(disp_pitch_rs);
+                  block_pos = 0;
+               }
+            }
+
+            float val = 0.f;
+            if (use_display)
+               val = -osc->output[block_pos];
+
+            val = (float)((0.5f + 0.4f * val) * h);
+            lastval = val;
+            float v_min = val;
+            float v_max = val;
+
+            v_min = val - 0.5f;
+            v_max = val + 0.5f;
+            int imin = (int)v_min;
+            int imax = (int)v_max;
+            float imax_frac = (v_max - imax);
+            float imin_frac = 1 - (v_min - imin);
+
+            if (imax == imin)
+               imax++;
+
+            if (imin < 0)
+               imin = 0;
+            int dimax = imax, dimin = imin;
+
+            if ((x > 0) || (s > 0))
+            {
+               if (dimin > last_imax)
+                  dimin = last_imax;
+               else if (dimax < last_imin)
+                  dimax = last_imin;
+            }
+            dimin = limit_range(dimin, 0, h - 1);
+            dimax = limit_range(dimax, 0, h - 1);
+
+            // int yp = (int)((float)(size.height() * (-osc->output[block_pos]*0.5+0.5)));
+            // yp = limit_range(yp,0,h-1);
+
+            column[dimin] += ((int)((float)imin_frac * 255.f));
+            column[dimax + 1] += ((int)((float)imax_frac * 255.f));
+            for (int b = (dimin + 1); b < (dimax + 1); b++)
+            {
+               column_d[b & 511] = aa_samples;
+            }
+            last_imax = imax;
+            last_imin = imin;
+            block_pos++;
+            for (int y = 0; y < h; y++)
+            {
+               if (column_d[y] > 0)
+                  column[y] += 256;
+               column_d[y]--;
+            }
+         }
+         column[midline] = max((unsigned int)64 << aa_bs, column[midline]);
+         column[topline] = max((unsigned int)32 << aa_bs, column[topline]);
+         column[bottomline] = max((unsigned int)32 << aa_bs, column[bottomline]);
+         for (int y = 0; y < h2; y++)
+         {
+            cdisurf->setPixel(x, y, coltable[min((unsigned int)255, (column[y] >> aa_bs))]);
+         }
+      }
+      delete osc;
+      // srand( (unsigned)time( NULL ) );
+   }
+   cdisurf->commit();
+   auto size = getViewSize();
+   cdisurf->draw(dc, size);
+
+   if (uses_wavetabledata(oscdata->type.val.i))
+   {
+      CRect wtlbl(size);
+      wtlbl.right -= 1;
+      wtlbl.top = wtlbl.bottom - wtbheight;
+      rmenu = wtlbl;
+      rmenu.inset(14, 0);
+      char wttxt[256];
+
+      storage->CS_WaveTableData.enter();
+
+      int wtid = oscdata->wt.current_id;
+      if ((wtid >= 0) && (wtid < storage->wt_list.size()))
+      {
+         strcpy(wttxt, storage->wt_list.at(wtid).name.c_str());
+      }
+      else if (oscdata->wt.flags & wtf_is_sample)
+      {
+         strcpy(wttxt, "(Patch Sample)");
+      }
+      else
+      {
+         strcpy(wttxt, "(Patch Wavetable)");
+      }
+
+      storage->CS_WaveTableData.leave();
+
+      char* r = strrchr(wttxt, '.');
+      if (r)
+         *r = 0;
+      // VSTGUI::CColor fgcol = cdisurf->int_to_ccol(coltable[255]);
+      VSTGUI::CColor fgcol = {0xff, 0xA0, 0x10, 0xff};
+      dc->setFillColor(fgcol);
+      dc->drawRect(rmenu, kDrawFilled);
+      dc->setFontColor(kBlackCColor);
+      dc->setFont(displayFont);
+      // strupr(wttxt);
+      dc->drawString(wttxt, rmenu, kCenterText, true);
+
+      /*CRect wtlbl_status(size);
+      wtlbl_status.bottom = wtlbl_status.top + wtbheight;
+      dc->setFontColor(kBlackCColor);
+      if(oscdata->wt.flags & wtf_is_sample) dc->drawString("IS
+      SAMPLE",wtlbl_status,false,kRightText);*/
+
+      rnext = wtlbl;
+      rnext.left = rmenu.right; //+ 1;
+      rprev = wtlbl;
+      rprev.right = rmenu.left; //- 1;
+      dc->setFillColor(fgcol);
+      dc->drawRect(rprev, kDrawFilled);
+      dc->drawRect(rnext, kDrawFilled);
+      dc->setFrameColor(kBlackCColor);
+
+      dc->saveGlobalState();
+
+      dc->setDrawMode(kAntiAliasing);
+      dc->setFillColor(kBlackCColor);
+      VSTGUI::CDrawContext::PointList trinext;
+
+      trinext.push_back(VSTGUI::CPoint(134, 170));
+      trinext.push_back(VSTGUI::CPoint(139, 174));
+      trinext.push_back(VSTGUI::CPoint(134, 178));
+      dc->drawPolygon(trinext, kDrawFilled);
+
+      VSTGUI::CDrawContext::PointList triprev;
+
+      triprev.push_back(VSTGUI::CPoint(13, 170));
+      triprev.push_back(VSTGUI::CPoint(8, 174));
+      triprev.push_back(VSTGUI::CPoint(13, 178));
+
+      dc->drawPolygon(triprev, kDrawFilled);
+
+      dc->restoreGlobalState();
+   }
+
+   setDirty(false);
+}
+
 bool COscillatorDisplay::onDrop(VSTGUI::DragEventData data )
 {
    doingDrag = false;
diff --git a/src/common/gui/COscillatorDisplay.h b/src/common/gui/COscillatorDisplay.h
index 42c9a3514..d510dc5a8 100644
--- a/src/common/gui/COscillatorDisplay.h
+++ b/src/common/gui/COscillatorDisplay.h
@@ -18,12 +18,57 @@ class COscillatorDisplay : public VSTGUI::CControl, public VSTGUI::IDropTarget
       this->oscdata = oscdata;
       this->storage = storage;
       controlstate = 0;
+
+      cdisurf = new CDIBitmap(getWidth(), getHeight());
+
+      int bgcol = 0xff161616;
+      int fgcol = 0x00ff9000;
+      float f_bgcol[4], f_fgcol[4];
+      const float sc = (1.f / 255.f);
+      f_bgcol[0] = (bgcol & 0xff) * sc;
+      f_fgcol[0] = (fgcol & 0xff) * sc;
+      f_bgcol[1] = ((bgcol >> 8) & 0xff) * sc;
+      f_fgcol[1] = ((fgcol >> 8) & 0xff) * sc;
+      f_bgcol[2] = ((bgcol >> 16) & 0xff) * sc;
+      f_fgcol[2] = ((fgcol >> 16) & 0xff) * sc;
+
+      f_fgcol[0] = powf(f_fgcol[0], 2.2f);
+      f_fgcol[1] = powf(f_fgcol[1], 2.2f);
+      f_fgcol[2] = powf(f_fgcol[2], 2.2f);
+
+      for (int i = 0; i < 256; i++)
+      {
+         float x = i * sc;
+         unsigned int r =
+             limit_range((int)((float)255.f * (1.f - (1.f - powf(x * f_fgcol[0], 1.f / 2.2f)) *
+                                                         (1.f - f_bgcol[0]))),
+                         0, 255);
+         unsigned int g =
+             limit_range((int)((float)255.f * (1.f - (1.f - powf(x * f_fgcol[1], 1.f / 2.2f)) *
+                                                         (1.f - f_bgcol[1]))),
+                         0, 255);
+         unsigned int b =
+             limit_range((int)((float)255.f * (1.f - (1.f - powf(x * f_fgcol[2], 1.f / 2.2f)) *
+                                                         (1.f - f_bgcol[2]))),
+                         0, 255);
+         unsigned int a = 0xff;
+
+#if MAC
+         // MAC uses a different raw pixel byte order than windows
+         coltable[i] = (b << 8) | (g << 16) | (r << 24) | a;
+#else
+         coltable[i] = r | (g << 8) | (b << 16) | (a << 24);
+#endif
+      }
    }
    virtual ~COscillatorDisplay()
    {
+      delete cdisurf;
    }
    virtual void draw(VSTGUI::CDrawContext* dc);
-   
+   void drawBitmap(VSTGUI::CDrawContext* dc);
+   void drawVector(VSTGUI::CDrawContext* dc);
+
    virtual VSTGUI::DragOperation onDragEnter(VSTGUI::DragEventData data) override
    {
        doingDrag = true;
@@ -76,7 +121,9 @@ class COscillatorDisplay : public VSTGUI::CControl, public VSTGUI::IDropTarget
 
    OscillatorStorage* oscdata;
    SurgeStorage* storage;
-   unsigned controlstate;
+   unsigned int controlstate;
+   unsigned int coltable[256];
+   CDIBitmap* cdisurf;
 
    bool doingDrag = false;
 
