crash while storing preset
By “existing user preset” you mean you save a preset, chose store, edit it, and chose store again with the same name and category? Or you mean you edit a factory preset and save it as a new preset?

Also did you happen to get a stack trace out of either of your hosts?

Thank you
Yes, the first one. No need to edit it, just save it again and it happens.
Sorry I couldn´t  produce now useful stack-trace till now.

Thanks for investigation.
Ok I’ll take a peek when I’m next at my laptop. Thanks!

@tank-trax do you see this behavior in Linux?
Tested with Surge-linux-x64-1.6.1.1 - there it works.
Ahh I have a theory. Hold on. Booting up my linux VM.
OK so between 1.6.1.1 and 1.6.2 we added a code path that prompts you before you overwrite on linux and that uses zenity to prompt for ok/cancel with `zenity --question`

Can you confirm for me that you do or don't have zenity installed? 

Let me tag @jpcima also who wrote the code but I bet I can fix it too just by simulating a missing zenity if JP's not online. But I can super easily reproduce your crash by renaming `zenity` to `qenity` in the C++. Then we get a core dump right away. 

Let me tag this one for 'immediate fix' in our 1.6.2.1 release. Thank you for the report.

OK I've confirmed that if you don't have zenity the execlp falls through and then you get an immediate crash it seems in the child process trying to do some UI thing (my crash backtrace is that the platform xcb handle is bad; but parent has not it seems returned from waitpid so I think it is things going very wrong when excclp can't replace the child process)

Here's a couple of solutions

1. @jpcima or @falkTX can we do anything more defensive in the vfork/exec pattern
2. At startup on linux I could check if zenity is in your path with a system call and if it isn't globaly disable it in user interactions 
3. @spurkopf immediately, you could install zenity :) There are functions in it we do require. But we shouldn't core out if it isn't there.

Thoughts folks?
Replace the `exit` call with `_exit` and the crash will go away.
`exit` will cleanup resources that are also present in the parent process, and things are undefined after this. we need to use `_exit` instead.
Oh super useful. let me check that!
OK @spurkopf I just merged the fix @falkTX suggested to master, along with one other tweak to "fail open". If you build from source you can just pull master and recompile. If you use the built .deb assets it will take about 30 minutes or so for a new nightly to ship. This fix is the first commit today, so any nightly dated Sep-21 or later should be good to go.

Please do let us know if this stops your crash.

But also: some features do require zenity currently. So once you confirm the crash fix, if it is possible on your system, installing zenity in your path will improve your surge user experience.

Best,
That was fast! Yes, I can confirm that this is fixed. Thank you very much baconpaul and falktx!

Unfortunately installing zenity didn´t improve my surge user experience ... it made it crash again. I write a report when I am through it. 

Ok please let us know when you have more visibility in your zenith issue

Does “zenity —question” work in a shell for you? And does your daw have a peculiar path? Those are two things which come to mind.
This fix shipped to the release version with todays 1.6.2.1 release. Closing issue. If you find the other crash please do open a new issue or re-open this one!