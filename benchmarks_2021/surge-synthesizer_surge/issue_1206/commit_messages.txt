Fix a collection of flanger issues (#1712)

Overnight @mkruselj gave a bunch of super useful feedback on
the flanger. This fixes the easy parts of that feedback.

Addresses #1708

Also kills a #1654 booger
Move macOS entirely to CMAKE

As of this commit, macOS does not use premake but instead uses CMAKE
for the builds. Will do linux and windows soon thereafter.

Addresses #1206
Move macOS entirely to CMAKE

As of this commit, macOS does not use premake but instead uses CMAKE
for the builds. Will do linux and windows soon thereafter.

Addresses #1206
Move macOS entirely to CMAKE (#1715)

As of this commit, macOS does not use premake but instead uses CMAKE
for the builds. Will do linux and windows soon thereafter.

Addresses #1206
Move Linux to CMAKE

Remove the need to have premake on Linux. Some functions still
happen in build-linux.sh but the code build is all cmake.

Addresses #1206
Move Linux to CMAKE

Remove the need to have premake on Linux. Some functions still
happen in build-linux.sh but the code build is all cmake.

Addresses #1206
Move Linux to CMAKE (#1716)

Remove the need to have premake on Linux. Some functions still
happen in build-linux.sh but the code build is all cmake.

Addresses #1206
CMake for Windows

Move windows to CMake. Confirmed this builds a VST2 and 3 which loads
in reaper (if you have VSt2 set up obviously)

Addresse #1206
CMake for Windows

Move windows to CMake. Confirmed this builds a VST2 and 3 which loads
in reaper (if you have VSt2 set up obviously)

Addresse #1206
CMake for Windows

Move windows to CMake. Confirmed this builds a VST2 and 3 which loads
in reaper (if you have VSt2 set up obviously)

Addresse #1206
CMake for Windows

Move windows to CMake. Confirmed this builds a VST2 and 3 which loads
in reaper (if you have VSt2 set up obviously)

Addresse #1206
CMake for Windows (#1718)

Move windows to CMake. Confirmed this builds a VST2 and 3 which loads
in reaper (if you have VSt2 set up obviously)

Addresse #1206
Fix slash directions on VST2Path for Azure

Addresses #1206
Fix slash directions on VST2Path for Azure (#1719)

Addresses #1206
Cmake Cleanups continue

- Fix headless
- Make "Release" the default

Addresses #1206
Cmake Cleanups continue

- Fix headless
- Make "Release" the default

Addresses #1206
Cmake Cleanups continue

- Fix headless
- Make "Release" the default

Addresses #1206
Cmake Cleanups continue

- Fix headless
- Make "Release" the default
- Kill the pointless ScalablePiggy complexity which we really don't
  need now we have skins coming

Addresses #1206
Cmake Cleanups continue (#1720)

- Fix headless
- Make "Release" the default
- Kill the pointless ScalablePiggy complexity which we really don't
  need now we have skins coming

Addresses #1206
Fix some errors in build-linux as we slowly cmake transition

Addresses #1206
Fix some errors in build-linux as we slowly cmake transition

Addresses #1206
Fix some errors in build-linux as we slowly cmake transition (#1721)

Addresses #1206
Mac VST3 Export Set

The Mac VST3 was missing an export set which stopped it loading
in the validator and cubase; but not in reaper.

Addresses #1206
Mac VST3 Export Set (#1722)

The Mac VST3 was missing an export set which stopped it loading
in the validator and cubase; but not in reaper.

Addresses #1206
A Few build-linux tweaks

The re-package for VST2 isn't how VST2 quite works; and broke the
build-linux install scripts. Also the make_deb was wrong since
cmake.

Closes #1724
Addresses #1206
A Few build-linux tweaks (#1726)

The re-package for VST2 isn't how VST2 quite works; and broke the
build-linux install scripts. Also the make_deb was wrong since
cmake.

Closes #1724
Addresses #1206
Further work on CMake

- Install and Resource for apple done via cmake. Addresses #1206
- Apple works fine with out of source build
- Make linux VST3 packaging work with out-of-tree builds. Closes #1729
- Include missing header in UserInteractionsLinux. Closes #1729
Further work on CMake

- Install and Resource for apple done via cmake. Addresses #1206
- Apple works fine with out of source build
- Make linux VST3 packaging work with out-of-tree builds. Closes #1729
- Include missing header in UserInteractionsLinux. Closes #1729
Further work on CMake (#1733)

- Install and Resource for apple done via cmake. Addresses #1206
- Apple works fine with out of source build
- Make linux VST3 packaging work with out-of-tree builds. Closes #1729
- Include missing header in UserInteractionsLinux. Closes #1729
CMake Doc

1. Update developer doc
2. Remove premake5.lua

Addresses #1206
CMake Doc (#1735)

1. Update developer doc
2. Remove premake5.lua

Addresses #1206
Last CMake Fixes

1. Remove unused def/exp files
2. Make cmake version understand "release/1.7.foo" branch names

Closes #1206
Last CMake Fixes

1. Remove unused def/exp files
2. Make cmake version understand "release/1.7.foo" branch names

Closes #1206
Last CMake Fixes (#2142)

1. Remove unused def/exp files
2. Make cmake version understand "release/1.7.foo" branch names

Closes #1206
