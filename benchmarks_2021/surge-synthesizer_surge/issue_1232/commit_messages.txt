More VST3 Fake MIDI mapping (#1231)

This stops the metacontroller conflicting with fake midi which
made some synths learn controllers in the 80s for controllers in
the 40s. (that is CC#, not duran duran vs benny goodman).
Fix Automation, VST3, and Layout issues

Fix a variety of issues ahead of 1.6.3

1. Automation of OSC or LFO params for on-display OSC or LFO
   repaints the waveform
2. multi-point vst3 events handled by choosing latest.
3. VST3 supresses non-change events sent in some situations by REAPER
4. Relocate the TIMRBRE button

Closes #1232
Closes #1234
Closes #1083
Fix Automation, VST3, and Layout issues (#1235)

Fix a variety of issues ahead of 1.6.3

1. Automation of OSC or LFO params for on-display OSC or LFO
   repaints the waveform
2. multi-point vst3 events handled by choosing latest.
3. VST3 supresses non-change events sent in some situations by REAPER
4. Relocate the TIMRBRE button

Closes #1232
Closes #1234
Closes #1083
OSC and LFO redraw on midi-cc slider move

If a slider maps to an LFO or OSC which is on display and
is midi cc learned, redraw the osc or lfo display just like
we do with param changes. Refactor the OSC check into a function
like I did for LFO (and should have done for OSC in the first place).

Closes #1232
OSC and LFO redraw on midi-cc slider move (#1237)

If a slider maps to an LFO or OSC which is on display and
is midi cc learned, redraw the osc or lfo display just like
we do with param changes. Refactor the OSC check into a function
like I did for LFO (and should have done for OSC in the first place).

Closes #1232
