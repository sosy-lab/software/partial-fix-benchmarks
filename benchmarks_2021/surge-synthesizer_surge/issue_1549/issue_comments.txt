Showing parameter already on initial mouse down
I think this is the same as #377 and, annoyingly, it is annoying to implement in vstgui

You can right mouse on any control and see the value, although I know that's not what you are asking for.
Oh damn, VSTGUI again 🤡 Well, that idea of the huge display might be very helpful, too. What do you think about it? Zebra deluxe...
Ha - we are at a bit of a crossroads as to what to do with the ui. I’m still in a thinking mode. I have a pre alpha version of surge running with a skinning engine that might help but I’m not quite sure yet the path to release it... 

I also kinda dislike the non locality a lot of synth uis have (mouse here show there type things). But we have lots of room on the screen to show stuff up top so worth considering....
Hi,

if you add a 
`if (listener)` 
`    listener->valueChanged(this);` 
[here](https://github.com/surge-synthesizer/surge/blob/master/src/common/gui/CSurgeSlider.cpp#L565), straight before the return, it seems to display the value then already on initial mousedown. Not sure if everything then still is correct though...
Yeah that might work indeed! 

We would want to bounceValue also when we do (take a look in onMouseMoveDelta) and also test it with the various other flavors (like shift and control drag). But sending a value change when we start a mouse action as opposed to when we micro drag should be OK, as long as it is after beginEdit

If we do this we should test the automation paths also of course.

Happy for you to try that and put a PR together and we can have folks test the nightly. I'm going to wait one or two more days of 1.6.5 being out there before I merge stuff but I think you are right that this would be safe

Thanks so much for looking at this!
if you add the bounceValue your diff as proposed works in every case I tested. So let me sweep that in this afternoon. Thanks!
Good stuff! Right now though, it also shows it when double-clicking to reset a slider value, which is probably not the correct behavior.
Oh sigh I get called twice, once for click a second time for double click. Harumph. Need to think about this one. I may just put a request-to-show into the idle queue but that is bigger surgery.
Why it is a problem, if it is shown while a reset? DELETED, don't follow a bad advice :)
Well I think it's not a major problem. Just an annoyance.
And it would have to show both values if it stayed open and so on. Plus to do that we are in the idle loop anyway so might as well do the more correct thing. Not impossible but needs doing.

Hm, maybe my idea about timeout is quite a nonsense. Might actually disturb a quick workflow using the mouse.
The timeout is what we do on mousewheel actually. So it is definitely not nonsense! Just a timeout to go away is the same code as a timeout to not appear in the first place, roughly, and I think the write answer in this case is the later.
Yeah, but while very quick mouse actions, it might block other sliders in workflow...
Yeah I agree - we don’t want it to popup and stay when you double click. 

My point was you have four options

0: popup on mouse down (what code did earlier)
1: popup and dismiss immdiedately (what code does now)
2; popup and hang around with a timeout (your idea which you now call “nonsense”)
3: delay popup for 20ms on mouse down anticipating a mouse click (roughly the same code as above but not the same UI). I’m proposing we do this one.
4: popup and disappear immediately on mousedown/mouseup, and for a second click within a timeout interval onto the same element, prevent a popup?

I wouldn't be a fan of 3), since it makes the workflow laggy again.
your “#4” is exactly what happens today. And it looks sloppy and bad when you double click.
I mean: I wrote the code that looks sloppy and bad. I’m not blaming you! Just we need a better solution that your number 4 which is exactly the number 1.
but... it does not prevent the popop on the second click currently, or does it?
Yeah no it doesn't