C1-8 controls only report values 0-9 to host now (really: windows w_char sprintfs)
Oh shoot; is it the stringification of the value or the actual value in reaper?
Actual value.

![image](https://user-images.githubusercontent.com/2393720/73858864-626f0480-4839-11ea-8950-9caa68cb0799.png)

OK
Of course, it works on mac
Just a quick test. If you put into latch mode, play, and drag the control in the *surge* ui (not the reaper UI) do you get smooth automation?
It's smooth - just the param value display is quantized to single digit.
OK! This is the exact same problem that made the controls have the name "C" on windows. Basically more wide string shenanigans. Since the actual tracking is smooth I think we can fix this in 1.6.6 timescale. I know where the problem is and can fix it easily.

Thanks!