LFO: Step seq trigger lane UX update
Perfectly clear. May be annoying to fix but if it’s easy I can!
Vincy mentioned something about an annoying flashing that was happening when the trigger lane was first introduced.  Perhaps thats something we can fix with skin colors now or something. IDK. just throwing that out there. 
Yeah I saw that. I think it is just that each click toggles your to both a b or some such and the drag was walking through that. Knowing I am in a drag and applying starting point is the state management I will have to do
Oh i fixed the annoying flash. The drag still not there but the annoying flash bit the dust once i had the vector render correct
OK so here's the thing

I can make this so that a drag across is a toggle

but I'm not sure that is exactly what we want

lemme explain in a gif.

I am presuming that what we would want is to apply all on when we drag? But right now since click is toggle this creates a bunch of clicks on drags == a bunch of toggles

So do we want it to toggle? Set the state that a click in the first would have? Always turn on?

I pushed but didn't merge this in baconpaul/ss-drag-trig-1659 while we figure out what we actually want.

![ssdrag](https://user-images.githubusercontent.com/13387561/83278789-1eb25900-a1a2-11ea-8230-8a4bb4d49cfa.gif)

BTW I *think* what should happen is "the toggle state you would get to where you first click applies"

So if you click on an off and drag all your drags go on.
If you click on an on and drag all your drags go off
If you shift-click on a filter AEG and drag, all your drags to to attack AEG
etc

but before I code that wanted confirmation
I agree, that's the best solution. Also, before implementing independent trigger lanes, if I understand your explanation correctly, that's exactly what happened. (Just tested in 1.6.3 for instance):

![drag](https://user-images.githubusercontent.com/50145178/83281094-93d35d80-a1a5-11ea-99e3-70cfb81f54ce.gif)

So like in the above GIF, the state you would get to if you simply clicked and released should be applied to all of the other ones.
So if you click on an empty one and drag, it fills up everything you drag over to (including the one you clicked before dragging). If you click on a filled on and drag, it clears up everything you drag over to. Same principle would apply for shift clicking or right-clicking I presume (apply the next state to all the steps).
Yeah super useful. OK I have that coded up. Will push it now and close this issue.
Would it be possible to delete the steps as we drag instead of only when releasing? Right now it updates as we drag for adding, but not deleting:

![seq](https://user-images.githubusercontent.com/50145178/83594434-320c4e00-a52d-11ea-8742-cec874ed6b76.gif)

I've looked at the code trying to do it myself but have trouble understanding.
Just going to reopen this instead of creating a new issue.