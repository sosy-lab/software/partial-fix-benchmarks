diff --git a/CMakeLists.txt b/CMakeLists.txt
index 11dd37590..6740686ac 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -46,7 +46,11 @@ set( BUILD_VST3 true )
 if( DEFINED ENV{VST2SDK_DIR} )
   set( BUILD_VST2 true )
   file( TO_CMAKE_PATH "$ENV{VST2SDK_DIR}" VST2SDK_DIR )
-  message( WARNING " Building VST2 using " ${VST2SDK_DIR} " - this is an unsupported configuration" )
+  if( AZURE_PIPELINE )
+    message( ERROR " Building VST2 using " ${VST2SDK_DIR} " - VST2 pipeline builds not allowed" )
+  else()
+    message( WARNING " Building VST2 using " ${VST2SDK_DIR} " - this is an unsupported configuration" )
+  endif()
 else()
   set( BUILD_VST2 false )
 endif()
@@ -70,6 +74,28 @@ endif()
 set(CMAKE_CXX_EXTENSIONS false)
 set(CMAKE_CXX_STANDARD 17)
 
+include(CheckCXXSymbolExists)
+CHECK_CXX_SYMBOL_EXISTS(std::filesystem::path::preferred_separator "filesystem" CXX_STD_FS)
+if( CXX_STD_FS )
+  set( FS_COMPILE_DEFINITIONS USE_STD_FILESYSTEM=1 )
+  message( STATUS "Using std::filesystem" )
+else()
+  CHECK_CXX_SYMBOL_EXISTS(std::experimental::filesystem::path::preferred_separator "experimental/filesystem" CXX_EXP_STD_FS)
+  if( CXX_EXP_STD_FS )
+    set( FS_COMPILE_DEFINITIONS USE_STD_EXPERIMENTAL_FILESYSTEM=1 )
+    message( STATUS "Using std::experimental::filesystem" )
+  else()
+    CHECK_CXX_SYMBOL_EXISTS(std::experimental::filesystem::path::preferred_separator "filesystem" CXX_EXP_STD_FS_FS_HDR)
+    if( CXX_EXP_STD_FS_FS_HDR )
+      set( FS_COMPILE_DEFINITIONS USE_STD_EXPERIMENTAL_FILESYSTEM_FROM_FILESYSTEM=1 )
+      message( STATUS "Using std::experimental::filesystem but from <filesystem> header" )
+    else()
+      set( FS_COMPILE_DEFINITIONS USE_HOMEGROWN_FILESYSTEM=1 )
+      message( STATUS "Using homegrown filesystem" )
+    endif()
+  endif()
+endif()
+
 # Enumerate the sources into groups
 #    SURGE_COMMON_SOURCES
 #    SURGE_GUI_SOURCES
@@ -496,6 +522,7 @@ if( BUILD_AU )
   target_compile_definitions(surge-au
     PRIVATE
     ${OS_COMPILE_DEFINITIONS}
+    ${FS_COMPILE_DEFINITIONS}
     TARGET_AUDIOUNIT=1
     )
 
@@ -566,6 +593,7 @@ if( BUILD_VST3 )
   target_compile_definitions(surge-vst3
     PRIVATE
     ${OS_COMPILE_DEFINITIONS}
+    ${FS_COMPILE_DEFINITIONS}
     TARGET_VST3=1
     )
 
@@ -654,6 +682,7 @@ if( BUILD_VST2 )
   target_compile_definitions(surge-vst2
     PRIVATE
     ${OS_COMPILE_DEFINITIONS}
+    ${FS_COMPILE_DEFINITIONS}
     TARGET_VST2=1
     VSTGUI_ENABLE_DEPRECATED_METHODS=0
     )
@@ -729,6 +758,7 @@ if( BUILD_LV2 )
   target_compile_definitions(surge-lv2
     PRIVATE
     ${OS_COMPILE_DEFINITIONS}
+    ${FS_COMPILE_DEFINITIONS}
     TARGET_LV2=1
     )
 
@@ -803,6 +833,7 @@ if( BUILD_HEADLESS )
   target_compile_definitions(surge-headless
     PRIVATE
     ${OS_COMPILE_DEFINITIONS}
+    ${FS_COMPILE_DEFINITIONS}
     TARGET_HEADLESS=1
     LIBMIDIFILE=1
     )
diff --git a/src/common/ImportFilesystem.h b/src/common/ImportFilesystem.h
new file mode 100644
index 000000000..ba2ee914a
--- /dev/null
+++ b/src/common/ImportFilesystem.h
@@ -0,0 +1,25 @@
+#pragma once
+
+/*
+** This file imports filesystem and sets the fs namespace to the correct
+** thing. In a distant future where we are compiling with all our compilers
+** and sdks set up to have std::filesystem this will go away. But while still
+** supporting VS2017 and macos 10.12 and stuff we need it.
+*/
+
+
+#if USE_STD_FILESYSTEM
+#include <filesystem>
+namespace fs = std::filesystem;
+#elif USE_STD_EXPERIMENTAL_FILESYSTEM
+#include <experimental/filesystem>
+namespace fs = std::experimental::filesystem;
+#elif USE_STD_EXPERIMENTAL_FILESYSTEM_FROM_FILESYSTEM
+#include <filesystem>
+namespace fs = std::experimental::filesystem;
+#elif USE_HOMEGROWN_FILESYSTEM || TARGET_RACK
+#include <filesystem.h>
+namespace fs = std::experimental::filesystem;
+#else
+#error FILESYSTEM is not configured by build system
+#endif
diff --git a/src/common/Parameter.cpp b/src/common/Parameter.cpp
index 018337ea3..0a7560289 100644
--- a/src/common/Parameter.cpp
+++ b/src/common/Parameter.cpp
@@ -1779,11 +1779,11 @@ bool Parameter::set_value_from_string( std::string s )
       {
          ni = std::stoi(c);
       }
-      catch (const std::invalid_argument &e)
+      catch (const std::invalid_argument &)
       {
          ni = val_min.i - 1;   // set value of ni out of range on invalid input
       }
-      catch (const std::out_of_range &e)
+      catch (const std::out_of_range &)
       {
          ni = val_min.i - 1;   // same for out of range input
       }
@@ -1859,7 +1859,7 @@ bool Parameter::set_value_from_string( std::string s )
             {
                oct = std::stoi(s);
             }
-            catch (std::invalid_argument const &e)
+            catch (std::invalid_argument const &)
             {
                oct = -10;   // throw things out of range on invalid input
             }
diff --git a/src/common/SurgeStorage.h b/src/common/SurgeStorage.h
index b704d271a..9e07ece61 100644
--- a/src/common/SurgeStorage.h
+++ b/src/common/SurgeStorage.h
@@ -17,13 +17,7 @@
 #endif
 #include <tinyxml.h>
 
-#if LINUX
-#include <experimental/filesystem>
-#elif MAC || (WINDOWS && TARGET_RACK)
-#include <filesystem.h>
-#else
-#include <filesystem>
-#endif
+#include "ImportFilesystem.h"
 
 #include <fstream>
 #include <iterator>
@@ -32,13 +26,6 @@
 #include "Tunings.h"
 
 
-#if WINDOWS && ( _MSC_VER >= 1920 )
-// vs2019
-namespace fs = std::filesystem;
-#else
-namespace fs = std::experimental::filesystem;
-#endif
-
 #if WINDOWS
 #define PATH_SEPARATOR '\\'
 #else
diff --git a/src/common/SurgeSynthesizerIO.cpp b/src/common/SurgeSynthesizerIO.cpp
index 2e913bd1d..0fdb77df1 100644
--- a/src/common/SurgeSynthesizerIO.cpp
+++ b/src/common/SurgeSynthesizerIO.cpp
@@ -5,13 +5,9 @@
 #include "DspUtilities.h"
 #include <time.h>
 #include <vt_dsp/vt_dsp_endian.h>
-#if LINUX
-#include <experimental/filesystem>
-#elif MAC || TARGET_RACK
-#include <filesystem.h>
-#else
-#include <filesystem>
-#endif
+
+#include "ImportFilesystem.h"
+
 #include <fstream>
 #include <iterator>
 #include "UserInteractions.h"
@@ -20,13 +16,6 @@
 #include "aulayer.h"
 #endif
 
-#if WINDOWS && ( _MSC_VER >= 1920 )
-// vs2019
-namespace fs = std::filesystem;
-#else
-namespace fs = std::experimental::filesystem;
-#endif
-
 #if AU
 #include "aulayer.h"
 #endif
diff --git a/src/common/UserDefaults.cpp b/src/common/UserDefaults.cpp
index 34adaff17..021dab2e7 100644
--- a/src/common/UserDefaults.cpp
+++ b/src/common/UserDefaults.cpp
@@ -9,20 +9,7 @@
 #include <sstream>
 #include <fstream>
 
-#if LINUX
-#include <experimental/filesystem>
-#elif MAC || TARGET_RACK
-#include <filesystem.h>
-#else
-#include <filesystem>
-#endif
-
-#if WINDOWS && ( _MSC_VER >= 1920 )
-// vs2019
-namespace fs = std::filesystem;
-#else
-namespace fs = std::experimental::filesystem;
-#endif
+#include "ImportFilesystem.h"
 
 namespace Surge
 {
diff --git a/src/common/WavSupport.cpp b/src/common/WavSupport.cpp
index 182319c45..f6a3f575c 100644
--- a/src/common/WavSupport.cpp
+++ b/src/common/WavSupport.cpp
@@ -24,20 +24,8 @@
 #include "SurgeStorage.h"
 #include <sstream>
 
-#if LINUX
-#include <experimental/filesystem>
-#elif MAC || TARGET_RACK
-#include <filesystem.h>
-#else
-#include <filesystem>
-#endif
+#include "ImportFilesystem.h"
 
-#if WINDOWS && ( _MSC_VER >= 1920 )
-// vs2019
-namespace fs = std::filesystem;
-#else
-namespace fs = std::experimental::filesystem;
-#endif
 
 // Sigh - lets write a portable ntol by hand
 unsigned int pl_int(char *d)
diff --git a/src/common/gui/COscillatorDisplay.cpp b/src/common/gui/COscillatorDisplay.cpp
index e7e083429..5a2e6ea86 100644
--- a/src/common/gui/COscillatorDisplay.cpp
+++ b/src/common/gui/COscillatorDisplay.cpp
@@ -7,24 +7,11 @@
 #include "unitconversion.h"
 #include "UserInteractions.h"
 #include "guihelpers.h"
-
-#if MAC
-#include "filesystem.h"
-#elif LINUX
-#include "experimental/filesystem"
-#else
-#include "filesystem"
-#endif
 #include "PopupEditorSpawner.h"
 
-using namespace VSTGUI;
+#include "ImportFilesystem.h"
 
-#if WINDOWS && ( _MSC_VER >= 1920 )
-// vs2019
-namespace fs = std::filesystem;
-#else
-namespace fs = std::experimental::filesystem;
-#endif
+using namespace VSTGUI;
 
 const float disp_pitch = 90.15f - 48.f;
 const int wtbheight = 12;
diff --git a/src/common/gui/SkinSupport.cpp b/src/common/gui/SkinSupport.cpp
index 2c05717cf..000aa7d06 100644
--- a/src/common/gui/SkinSupport.cpp
+++ b/src/common/gui/SkinSupport.cpp
@@ -10,21 +10,11 @@
 #include "UIInstrumentation.h"
 #include "CScalableBitmap.h"
 
-#if LINUX
-#include <experimental/filesystem>
-#elif MAC || (WINDOWS && TARGET_RACK)
-#include <filesystem.h>
-#else
-#include <filesystem>
-#endif
+#include "ImportFilesystem.h"
 
 #include <iostream>
 #include <iomanip>
 
-#if !WINDOWS
-namespace fs = std::experimental::filesystem;
-#endif
-
 namespace Surge
 {
 namespace UI
diff --git a/src/common/gui/SurgeGUIEditor.cpp b/src/common/gui/SurgeGUIEditor.cpp
index b05a431d3..d5a877ac5 100644
--- a/src/common/gui/SurgeGUIEditor.cpp
+++ b/src/common/gui/SurgeGUIEditor.cpp
@@ -78,22 +78,7 @@ struct RememberForgetGuard {
 #endif
 
 
-#if LINUX
-#include "vstgui/lib/platform/platform_x11.h"
-#include "vstgui/lib/platform/linux/x11platform.h"
-#include <experimental/filesystem>
-#elif MAC || TARGET_RACK
-#include <filesystem.h>
-#else
-#include <filesystem>
-#endif
-
-#if WINDOWS && ( _MSC_VER >= 1920 )
-// vs2019
-namespace fs = std::filesystem;
-#else
-namespace fs = std::experimental::filesystem;
-#endif
+#include "ImportFilesystem.h"
 
 
 #if LINUX && TARGET_LV2
