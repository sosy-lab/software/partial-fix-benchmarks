Further CMake Improvements (#1976)

1. Installed assets are both build local for out of source builds
   and in a well named directory on all platforms
2. Version generation happens at compile time reliably
3. Strip the linux vst3 dll
4. Improve some versioning schemes
Dynamically expand the InfoBox

Dynamicall expand the InfoBox for longer labels

Closes #1885
Dynamically expand the InfoBox (#1977)

Dynamicall expand the InfoBox for longer labels

Closes #1885
Two Small but Important bug Fixes

1. When we load a patch don't blow away FX modulations!
2. Size the infowindow. Closes #1885

OK maybe one important fix, two small ones :)
Two Small but Important bug Fixes (#2137)

1. When we load a patch don't blow away FX modulations!
2. Size the infowindow. Closes #1885

OK maybe one important fix, two small ones :)
