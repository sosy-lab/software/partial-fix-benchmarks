Surge VST3 GUI lags on reopen in JUCE6 hosts
remind me again how do i get carla with vst3 support installed? Thanks!
on Linux have to add the KXStudio repo and then download that carla-git version
OK the relatively current JUCE6 AudioPluginHost exhibits the same behavior. Nothing to do with Carla (except inasmuch as Carla uses JUCE for VST3 hosting). I can debug in APH easily. 
And I've found it

It seems the order of us creating widgets and the order of the XCB callback means that the frame gets its invalidation message out of order with show sometimes, leaving to the un-refresh frame. There's lots of complicated things I could do to solve this i bet, including debugging that order of creation, but instead what I'm doing is just forcing a call to frame->invalid() the first time through the idle loop, which fixes it. Push incoming. 

Thanks for the report @tank-trax !!
using commit ea02a68 the lag issue has improved in Carla but still exists
I hadn't tried AudioPluginHost prior to the fix but currently there is no lag in APH
I checked my render settings to see if that would improve it and it hadn't
There is no lag in Carla using the VST2

Of note is the GUI paints well on its initial opening. It does not happen when minimized and maximized, only when the cog on Carla is clicked to hide the GUI or it is x'ed out, with the lag happening when the cog is pressed to reinitialize/display the GUI.

![Screenshot_20200621_221841](https://user-images.githubusercontent.com/41337033/85242493-34b3e200-b40d-11ea-9cad-63cbc2e0bf8d.png)

If the mouse is passed over the GUI it paints immediately. Tested at various zoom levels.
Yeah but with the fix this afternoon, it also repaints near immediately (at most 2 idles) for me in Carla-git. Do you not see that?
the redraw thing is better is Carla
however the VST3 does not remember its position
and when opened larger than 100% will straddle both screens
the VST2 when closed and opened will reopen in the place it was when it was closed

at 100% it redraws almost immediately and reopens in the same spot it was when it was closed
at 125% it redraws almost immediately (it's intermittent though) but reopens straddling both screens
at 150% it lags and needs the mouseover to paint the GUI but it does reopen in the same spot it was when it was closed
at 174% (my screen max) it redraws almost immediately but reopens straddling both screens

the opening on two screens though is also intermittent, can't pinpoint the behaviour that decides where it should open
The VST3 API doesn’t give us an option on positioning our screen - the issue you describe is all in the DAW. I don’t think there’s more i can fix in surge here (although if there is I would be happy to do so of course). Wonder if we should open a new issue on KX repo?
I agree. I tested the VST3 versions of ACE, Podolski, Diva and Zebra and they also draw across two monitors. 