Hover over sliders to show modulation source
This is such a great idea. Its too much plumbing to make 1.7.0 at this late stage but I definitely want to add it very soon.
Hmmm what happens if a target parameter has multiple assigned modulators?
It highlights all the assigned modulator to the parameter in question
Since highlight is already part of the routing bar when hovering over tabs, I presume (just a guess here) it could be pretty easy to implement that. And it would fit the theme perfectly and look very nice.
yeah it would not be hard. the only trick is the slider doesn't know the modulation sources so it has to message back to the synth to activate the hover (which is enough plumbing that I'm waiting until after we ship 170)
Do we want this as a togglable feature? I reckon with this enabled and gobs of modulation around, it could be quite epilepsy-inducing...