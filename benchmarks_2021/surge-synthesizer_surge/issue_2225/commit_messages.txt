Fixed mod amt typein for sine FB, certain other % params (#2223)

* also better value reporting for mod range of amplitude/send level ctypes
Fixed the widget disappearing act (#2228)

* Fixed the widget disappearing act
* closes #2225

* Proper init of mouseDowns, remove unnecessary adjunct to onMouseDown condition
Automate Scene Invalidates; Partly back out a245467

Scene Changes didn't reset current_scene and rebuild so
DAW automation didn't update UI. Closes #2226

Also back out a change, reopening #2225
Automate Scene Invalidates; Partly back out a245467 (#2234)

Scene Changes didn't reset current_scene and rebuild so
DAW automation didn't update UI. Closes #2226

Also back out a change, reopening #2225
Fix the double mouse down

The double-mouse-down confuses CHSwitch2. Fix it by
not allowing it if we already have a mouse down.
Requires a >1 button mouse to verify.

Closes #2225
Fix the double mouse down (#2243)

The double-mouse-down confuses CHSwitch2. Fix it by
not allowing it if we already have a mouse down.
Requires a >1 button mouse to verify.

Closes #2225
