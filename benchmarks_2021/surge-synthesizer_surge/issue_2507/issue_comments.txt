Integrate Airwindows
Some internal thoughts

- [x] grouping and presets
- [x] probably want to smooth the params a bit
- [x] @mkruselj to lead the charge to pick which 20 or 30 to include
- [x] Invalidation when you switch needs to happen (so you don't have to do an LFO swapperoonie)
- [x] At least an FX group for "Selector" vs "Params"
- [x] RMB doesn't update
- [x] Data structure to have names for the plugs of course
- [x] Value displays call through (so need an external function on Parameter for building a value with an associated interface)
There are many useful Airwindows plugins, and it's exciting to consider having some as defaults...

Primary among them will be the ability to add a compressor to Surge FX, which is an FX type that I've most wanted to see.

While typing, might be a good time to have a conversation about expanding the number of FX slots as we've discussed in the past. With a suite of Airwindows plugins, it would be extremely useful to have more FX slots to use them.

Anyhow, great idea!
Yeah I actually have this working and it is indeed powerful. And is indeed making me realize we're gonna burn a lot of slots.

I have a separate bit of work going on which will let me add params finally, and then we can expand slot count per that crazy graphic Evil drew over in #2446 

man 1.7.2 is gonna be a big install again. Gotta change that versioning scheme :)
Oh and @Jacky-Ligon if there are particular plugs you think are particularly well suited for inclusion in the surge path, please let us know on this issue. We aren't going to do all 200.
When this is closed make sure to post back on https://github.com/airwindows/airwindows/issues/17
@baconpaul 

Will start with considering the compressors right away.

As I always like to do when I download the Airwindows archive, I import the "what.txt" file into a spreadsheet so that the list can be sorted either by name-description, or newest to oldest.

Here's the most recent import for anyone interested in both Excel and LibreOffice formats:

[20200824 Airwindows Plugins Descriptions.xlsx](https://github.com/surge-synthesizer/surge/files/5120529/20200824.Airwindows.Plugins.Descriptions.xlsx)

[20200824 Airwindows Plugins Descriptions ODS.zip](https://github.com/surge-synthesizer/surge/files/5120530/20200824.Airwindows.Plugins.Descriptions.ODS.zip)

Here are a few initial picks from the dynamics/compressors category:

Top favorites placed after an instance of Surge playing various presets looping in Reaper, whilst listening and viewing their responses with s(M)exoscope, all of which sound quite good for a variety of applications:

- Compresaturator fades between compressing and soft clipping.
- ButterComp2 is improved ButterComp with an output control and sound upgrades.
- OneCornerClip is an ultimate full-bandwidth clipper.
- ADClip7 is the ultimate Airwindows loudness maximizer/ peak control.
- Logical4 is a classic 2-buss compressor.
- Pressure4 is a compressor adjustable between vari-mu and ‘new york’ peak-retaining behaviors.

Honorable Mentions and also quite good:

- Mojo is a biggenator that also works as a loudenator.
- BlockParty is like a moderately saturated analog limiter.
- Pop is a crazy overcompressor with a very exaggerated sound.
- VariMu is a more organic variation on Pressure (a compressor)

Airwindows, as we know, also has a quite large range of saturation, character and tape emulations, many of which are quite useful, but here, I've primarily focused on considering the better of the dynamics processors. 

Will be looking forward to what others have selected, to see if there are any we mutually agree upon. Very glad to see some of these getting added to Surge, and will definitely followup on his GitHub page when we're done.
Yeah ButterComp2 is already in. Along with DustBunny, UnBox, Focus BrassRider and Melt. 

Melt is crazy as is UnBox

This is gonna be cool.
I'm definitely gonna need to build in grouping of some sort in the UI for the selector too.
Interesting. Just installed your picks and will check them out soon. All sound very interesting.

Probably mentioned at some point in the past, that IMHO, it would be really useful, if in the User Data, FXSettings directory, when saving custom user FX presets, they could be written into FX specific plugin directories, as opposed to everything getting written into the root of FXSettings...

For example, all Reverb1 and Reverb2 presets, in discrete dedicated folders: Reverb1 and Reverb2.

FYI/FWIW: In the Surge sound-design work here, I'm constantly creating and saving new presets, and already have a large number of them, and wish they could be categorized according to their FX type. Would be hugely helpful for preset organization and management, and especially, where one might need to rename, or otherwise modify, them afterwards.

How would you characterize the level of difficulty in doing this? 
Oh that's super easy. Couple of hours of work.
Many thanks for the consideration. Again, it would be hugely helpful for the ever expanding number of custom FX presets.
Made a separate issue for it but will get to it in the 172 push.
Wow this is some crazy nice news!  VERY much looking forward to try these out and see how they work in surge!
https://github.com/baconpaul/airwindows/tree/master/plugins/WinVST/DeEss

that one gotta be integrated into surge, remember our deesser discussion ? :)
Oh ha. Sure. Its' super easy to do now so lets just make a list and pick the ones we want.
ED's faves:

Apicolypse
BassDrive
BitGlitter
BlockParty
Cojones
Density
DeRez2
Drive
Fracture
HardVacuum
NC-17
PocketVerbs (can never have enough reverb colors!)
Point (transient designer)
Pop
Pyewacket
SingleEndedTriode
StarChild
Surge (well we just gotta have it, don't we? 🙂)
ToTape6
VoiceOfTheStarship (constant noise source with built in filter and 16 types of noise, we need to add Output slider to this one)



Welp, can't really help it, there's just so many useful distortion colors in there. So I guess let's put a limit at 50 types? And yes, we definitely need to have submenus on this one, for clearer categorization!
No hard limit on count - lets have the ones we want.
But yes we will need grouping. Lemme build that in.
Can we consolidate yours and jackets list with some proposed groups?
OK reading Jacky's spreadsheet and list, Evils list, and perusing the set myself, and making sure to include DeBess (the new DeEss) I get the following list as a proposal. Obviously would alphabetize and the like.

EDIT: replaced with sorted version

```
Dynamics
    ADClip7 
    BlockParty
    BlockParty
    ButterComp2
    Compresaturator 
    Logical4 
    Mojo 
    OneCornerClip 
    Point
    Pop
    Pressure4 
    PyeWacket
    Surge
    VariMu


LoFi and Noise
    BitGlitter
    CrunchyGrooveWear  
    DeRez2
    DeckWrecka
    DustBunny
    GrooveWear
    Noise
    VoiceOfTheStarship

Reverb and Diffusion
    BrightAmbience2
    Hombre
    Melt
    PocketVerbs
    StarChild

Saturation
    Apicolypse
    BassDrive
    Cojones
    Density
    Drive
    Focus
    Fracture
    HardVacuum
    Loud
    NC-17
    PurestDrive
    Spiral
    Sprial2
    TubeLaw
    UnBox

Special
    BrassRider
    DeBess
    Single Ended Triode
Tape
    Iron Oxide 5
    ToTape6
```
DeEss is to me better than DeBess but YMMV. 
Including both is trivially easy. Will do that.
@baconpaul 

Very impressive that you've implemented this so quickly.

Surge-NIGHTLY-2020-08-25-37a691b | Win-10, VST3i, Reaper

Noticing here a bit of an odd behavior with:

The slot that the AW module is assigned to has no icon, and appears black. With this, it's difficult to understand if it's dis/enabled.

The FX Selector kind of hangs, and required moving the control with a mouse click and movement, then releasing the mouse fully, before another selection can be made.
Is there a better place to consolidate beta-test reports on the new AW plugins feature than this particular issue? 

Let me know, and I'll post them where ever is best.

@baconpaul 
Yes sorry we know about both of those - I mentioned it on discord

The icon will get fixed
for the slider let me strongly recommend you use the RMB menu on the slider. We are going to replace that slider widget with a menu widget (that's #2509) shortly. The menu is already there so best way to use this at current nightly is right mouse to select effect.

(Since the UI has to rebuild when you change AW sub-effect dragging the slider is annoying since it rebuilds on each move; the same is not true for the menu which can show you all the options at once).

And this is a perfect place for them. Between here and discord we will get them all.
OK thanks for the info.
Update the nightly and the slider is a more explicit menu now btw
**Surge-NIGHTLY-2020-08-26-88c4b19 | Windows 10 Pro, x64, VST3i, Reaper**

Some potentially interesting AW related things found:

1. No slider appears on the plugin UI.
![image](https://user-images.githubusercontent.com/66483816/91431639-a6097a00-e82e-11ea-9b8f-d9ba7bd874a0.png)

2. When AW presets are saved into: C:\Users\Username\Documents\Surge\FXSettings, they don't appear to restore their settings as saved. 

Saved state with Boost slider: .333, Enhance slider: .333
![image](https://user-images.githubusercontent.com/66483816/91432007-2203c200-e82f-11ea-8696-976f69256d36.png)

Reloaded preset state:
![image](https://user-images.githubusercontent.com/66483816/91432232-6f802f00-e82f-11ea-9957-c942d9b6e115.png)

Attached is the above (and the first) I tested saving/loading for ADClip7 for your review.
[ADC7 1.zip](https://github.com/surge-synthesizer/surge/files/5135950/ADC7.1.zip)

Likewise for another randomly selected, with first slider saved at .5, and second at 1.0, which reloads with sliders zeroed.

Reloaded state:
![image](https://user-images.githubusercontent.com/66483816/91432886-6348a180-e830-11ea-96ed-bc9b9c8f41e0.png)

[GrooveWear 1.zip](https://github.com/surge-synthesizer/surge/files/5135875/GrooveWear.1.zip)

Non-reproducible asides, is that Reaper crashed a few times in initially testing the AW plugins when clicking on the FX Name encircled in the screenshot below, as well as locking the UI a couple of times, such that new global Surge presets could not be loaded, without first restarting Surge entirely. 

![image](https://user-images.githubusercontent.com/66483816/91434126-5b89fc80-e832-11ea-8b9c-203fca2aee9a.png)

As new builds are available I should be available to test them in upcoming days, and, @baconpaul, feel free to let me know about anything in particular I should check for with regard to the new Airwindows features.
Thank you @Jacky-Ligon - I have an idea why save/restore is broken. I tested it then had to restructure the code afterwards. Apologies for that

On your first item: That's intentional. For some discrete choices we've introduced the ability to replace a slider with a button. So far it's there and in the flanger. Still need some dark skin hover items. That crash, though, shouldn't happen. Le me take a look. at that.
@baconpaul 

**Surge-NIGHTLY-2020-08-27-611832e | Windows 10 Pro x64, VST3i, Reaper**

Really loving the new per-FX preset folder enhancement, and even with the two AW FX I tested, the .SRGX files are saving the slider settings correctly into the Airwindows FX directory (confirmed by opening the .SRGX files in Notepad), however, they still aren't being recalled in the FX slots as they were saved, and as before mentioned, the slider controls are still defaulting to zero upon reloading saved presets.

Attached here are two test cases for your review, and as you'll see when you inspect the .SRGX files, the settings are getting saved, but are, as above, not restoring as saved upon reloading the .SRGX.

[Airwindows.zip](https://github.com/surge-synthesizer/surge/files/5138158/Airwindows.zip)

Worth noting however, is that while AW FX settings are being saved and restored correctly at the Surge **.FXP** level, when those very same AW FX settings are saved as **.SRGX** presets into the Airwindows-specific FX folder, even here, upon reloading, their sliders are defaulting to a zeroed state.

Mentioning these two scenarios, because I've both been doing a cleanup of the FX directory and moving .SRGX FX into their own folder types, as well as testing a few AW FX from within existing .FXP presets.
@baconpaul

**Surge-NIGHTLY-2020-08-27-611832e | Windows 10 Pro x64, VST3i, Reaper**

As can be seen in the below LICEcap GIF, there are a few small QC issues with typeins, such as the "edit coming soon" / still accepts typeins, that we've seen occasionally along the way in beta-testing.

![LICEcap](https://user-images.githubusercontent.com/66483816/91484510-59965c80-e877-11ea-9e8c-f243be7dd964.gif)


Great thank you The restore and typein are next in mu queue to fix. 
OK I have fixed both of these problems and when #2522 is merged it will generate a new nightly. 
**Surge-NIGHTLY-2020-08-27-4dd4eaa | Win-10 x64, VST3i, Reaper**

Just a quick reply to confirm that Airwindows save/restore and typeins are now working.

Thanks for the fix! 

Very exciting to have these available in Surge, as well as the new FX preset management features.
Great and glad to hear it. And man these sound good!

There's a few additions we need to make. The typins are all input 0,1 even though the display is different (that's #2525) and I need to do a better job of detecting if we are hand editing or upstreaming so I can set reasonable defaults (#2526) so still some changes which will require a final re-test but I think the basic integration in this issue is in good shape, so let me close this.
Ah this is so cool! I was actually wondering if this would be possible with Airwindows plugins and Surge one day, so cool to see you've implemented it.

I don't know if it's too late to make suggestions but here are a 3 great ones that add some useful features.
Could be helpful if it's possible to implement down the road.

**Capacitor:** very smooth sounding hi and low pass filters with no resonance in one plugin, useful here because it's easy to make very gnarly sounds in Surge so having some simple high and low pass filters in the effects section could be very nice. 
 
**CStrip:** it's a channel strip, has a three band eq, a compressor, and also a one slider gate (which we don't have elsewhere in Surge) could be very useful for fine tuning percussive sounds and the like. Having all these control in one place is really helpful and also Chris put a lot of time into tuning all the parameters to make them work well together.

**Slew and/or Slew 2:** different types of slew limiting tools that are very useful, start to clamp down more on the high end. Hard to explain what they do but also super useful for taming harsh sounds. Slew one sounds more dirty and obvious and Slew2 is more transparent
They are easy to add and notes the time to do it before we memorialize a release 
Lemme reopen this and peek
There’s one constraint which meant 2 couldn’t work. I don’t think they include these ones 
And glad you like them!
Do we really need Capacitor if we're gonna add Surge's own filter as FX?
Probably not, but Capacitor is a particularly smooth sounding filter, could be nice to have anyway if it isn't too time consuming.
Takes about 2 minutes to add one. I'm in favor of having a few more if they are useful in a synth context.
@mortfell unfortunately CStrip is one of the ones which won't work. It has more parameters than we have available in our FX Slots. There were a couple like this and that's one.
Added capacitor and the slews yesterday so re-closing this

There's still some open issues in the milestone for airwindows (one bug, one feature) but those are separate. No more work to be done on this ticket.
ok great thanks Paul, i'll try testing them out more