FX Presets in per-FX folder
I also think that all user presets should then also be in Presets folder first, rather than flat in Documents\Surge.

This might be a bit of a problem considering a bunch of users already have a bunch of presets in flat Documents\Surge... but maybe we can move them in a subfolder in the installer? Hmmm...
Was actually wondering about moving the ones already made here in the user-data FXSettings flat directory into FX-specific directories after this change.

Luckily, as I've been working and creating custom FX presets, in many cases, to differentiate them from each other, I've been using some naming conventions (often prefixes such as R1, R2, etc.) that will help to move them into contextual directories.

Among the really nice things about writing them into discrete FX type folders, is that they will all be neatly compartmentalized, and will require no special naming conventions to tell them apart. Really looking forward to this, and thanks for considering it.

With the addition of the Airwindows plugins, this kind of FX preset directory system will become extremely useful for managing the complexity that will surely come from having many more options.
yeah the change I'll make here basically is "recurse from the FXPresets folder arbitrarily far" and then also "default to making a directory per". So you can arrange however you want really.

The AirWindows is gonna be interesting. Do we want to have a bunch of presets in configuration.xml? We will find out for sure!
Yeah AW presets are gonna be a field trip!

Oh also, just to be exactly clear - individual per-FX preset folders should be _within_ the current FXPresets folder!
of course
Just dl'd the new build with the AW plugins (very impressive that happened so quickly), and on the way to check it now...

Will his new per-FX type directory system be in place in this build?

@baconpaul 
nope. haven't coded it yet. but will get to it soon enough.
This should be in an upcoming nightly.
Thanks for this excellent enhancement @baconpaul !

Was able to move, rename, and tweak the .SRGX files for all FX presets that had been created through the year.

This will be great for building up an even more expansive FX directory going forward, and having them compartmentalized by FX type will make them so much easier to manage. Thanks again!

Did find some AW issues, and will report in that issue soon.
Glad it works for you! It's a good idea. 

I recurse all the subdirectories by the way so you can move anything anywhere you want really. The default write location isn't special. So if you want to make a folder called "blimblam" and put 7 .srgfx there it will just work.