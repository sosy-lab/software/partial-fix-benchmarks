diff --git a/CMakeLists.txt b/CMakeLists.txt
index 21b904149..f020ae5fa 100644
--- a/CMakeLists.txt
+++ b/CMakeLists.txt
@@ -279,7 +279,7 @@ set(SURGE_GUI_SOURCES
   src/common/gui/CVerticalLabel.cpp
   src/common/gui/CursorControlGuard.cpp
   src/common/gui/SkinFontLoader.cpp
-  src/common/gui/CSurgeHyperlink.cpp)
+  src/common/gui/CSurgeHyperlink.cpp src/common/ModulatorPresetManager.cpp src/common/ModulatorPresetManager.h)
 
 set(SURGE_VST3_SOURCES
   src/vst3/SurgeVst3Processor.cpp
diff --git a/src/common/ModulatorPresetManager.cpp b/src/common/ModulatorPresetManager.cpp
new file mode 100644
index 000000000..0b51b6332
--- /dev/null
+++ b/src/common/ModulatorPresetManager.cpp
@@ -0,0 +1,257 @@
+/*
+** Surge Synthesizer is Free and Open Source Software
+**
+** Surge is made available under the Gnu General Public License, v3.0
+** https://www.gnu.org/licenses/gpl-3.0.en.html
+**
+** Copyright 2004-2020 by various individuals as described by the Git transaction log
+**
+** All source at: https://github.com/surge-synthesizer/surge.git
+**
+** Surge was a commercial product from 2004-2018, with Copyright and ownership
+** in that period held by Claes Johanson at Vember Audio. Claes made Surge
+** open source in September 2018.
+*/
+
+
+#include "ModulatorPresetManager.h"
+#include <iostream>
+#include "DebugHelpers.h"
+#include "SurgeStorage.h"
+#include "tinyxml/tinyxml.h"
+
+namespace Surge
+{
+namespace ModulatorPreset
+{
+
+const static std::string PresetDir = "Modulator Presets";
+const static std::string PresetXtn = ".modpreset";
+
+void savePresetToUser( const fs::path & location, SurgeStorage *s, int scene, int lfoid )
+{
+   auto lfo = &(s->getPatch().scene[scene].lfo[lfoid]);
+   int shapev = lfo->shape.val.i;
+
+   auto containingPath = fs::path{ string_to_path( s->userDataPath ) / fs::path{ PresetDir }};
+
+   if( shapev == ls_mseg )
+      containingPath = containingPath / fs::path{ "MSEGs" };
+   else if( shapev == ls_stepseq )
+      containingPath = containingPath / fs::path{ "StepSequences" };
+   else
+      containingPath = containingPath / fs::path{ "Lfos" };
+
+   fs::create_directories(containingPath);
+   auto fullLocation = fs::path( { containingPath / location }).replace_extension( fs::path{ PresetXtn } );
+
+   TiXmlDeclaration decl("1.0", "UTF-8", "yes");
+
+   TiXmlDocument doc;
+   doc.InsertEndChild(decl);
+
+   TiXmlElement lfox( "lfo" );
+   lfox.SetAttribute("shape", shapev );
+
+   TiXmlElement params( "params" );
+   for( auto curr = &(lfo->rate); curr <= &(lfo->release); ++curr )
+   {
+      // shape is odd
+      if( curr == & ( lfo->shape ) )
+      {
+         continue;
+      }
+
+      // OK the internal name has "lfo7_" at the top or what not. We need this
+      // loadable into any LFO so...
+      std::string in( curr->get_internal_name());
+      auto p = in.find("_");
+      in = in.substr(p+1);
+      TiXmlElement pn( in );
+
+
+      if( curr->valtype == vt_float )
+         pn.SetDoubleAttribute("v", curr->val.f );
+      else
+         pn.SetAttribute( "i", curr->val.i );
+      pn.SetAttribute( "temposync", curr->temposync );
+      pn.SetAttribute( "deform_type", curr->deform_type );
+
+      params.InsertEndChild(pn);
+   }
+   lfox.InsertEndChild(params);
+
+   if( shapev == ls_mseg )
+   {
+      TiXmlElement ms( "mseg" );
+      s->getPatch().msegToXMLElement(&(s->getPatch().msegs[scene][lfoid]), ms );
+      lfox.InsertEndChild( ms );
+   }
+   if( shapev == ls_stepseq )
+   {
+      TiXmlElement ss( "sequence" );
+      s->getPatch().stepSeqToXmlElement(&(s->getPatch().stepsequences[scene][lfoid]), ss, true );
+      lfox.InsertEndChild( ss );
+   }
+
+   doc.InsertEndChild(lfox);
+
+   if( ! doc.SaveFile(fullLocation) )
+   {
+      // uhh ... do somethign I guess?
+      std::cout << "Could not save" << std::endl;
+   }
+}
+
+/*
+ * Given a compelted path, load the preset into our storage
+ */
+void loadPresetFrom( const fs::path &location, SurgeStorage *s, int scene, int lfoid )
+{
+   auto lfo = &(s->getPatch().scene[scene].lfo[lfoid]);
+   // ToDo: Inverse of above
+   TiXmlDocument doc;
+   doc.LoadFile(location);
+   auto lfox = TINYXML_SAFE_TO_ELEMENT(doc.FirstChildElement("lfo"));
+   if (!lfox)
+   {
+      std::cout << "Unable to find LFO node in document" << std::endl;
+      return;
+   }
+
+   int shapev = 0;
+   if( lfox->QueryIntAttribute("shape", &shapev) != TIXML_SUCCESS )
+   {
+      std::cout << "Bad shape" << std::endl;
+      return;
+   }
+   lfo->shape.val.i = shapev;
+
+   auto params = TINYXML_SAFE_TO_ELEMENT(lfox->FirstChildElement("params"));
+   if (!params)
+   {
+      std::cout << "NO PARAMS" << std::endl;
+      return;
+   }
+   for( auto curr = &(lfo->rate); curr <= &(lfo->release); ++curr )
+   {
+      // shape is odd
+      if (curr == &(lfo->shape))
+      {
+         continue;
+      }
+
+      // OK the internal name has "lfo7_" at the top or what not. We need this
+      // loadable into any LFO so...
+      std::string in(curr->get_internal_name());
+      auto p = in.find("_");
+      in = in.substr(p + 1);
+      auto valNode = params->FirstChildElement(in.c_str());
+      if (valNode)
+      {
+         double v;
+         int q;
+         if( curr->valtype == vt_float )
+         {
+            if (valNode->QueryDoubleAttribute("v", &v) == TIXML_SUCCESS)
+            {
+               curr->val.f = v;
+            }
+         }
+         else
+         {
+            if( valNode->QueryIntAttribute("i", &q) == TIXML_SUCCESS )
+            {
+               curr->val.i = q;
+            }
+         }
+
+
+
+         if (valNode->QueryIntAttribute("temposync", &q) == TIXML_SUCCESS)
+            curr->temposync = q;
+         if (valNode->QueryIntAttribute("deform_type", &q) == TIXML_SUCCESS)
+            curr->deform_type = q;
+      }
+   }
+
+   if (shapev == ls_mseg)
+   {
+      auto msn = lfox->FirstChildElement("mseg");
+      if( msn )
+         s->getPatch().msegFromXMLElement(&(s->getPatch().msegs[scene][lfoid]), msn);
+   }
+
+   if( shapev == ls_stepseq )
+   {
+      auto msn = lfox->FirstChildElement("sequence");
+      if( msn )
+         s->getPatch().stepSeqFromXmlElement(&(s->getPatch().stepsequences[scene][lfoid]), msn);
+   }
+}
+
+
+std::vector<Category> getPresets( SurgeStorage *s )
+{
+   // Do a dual directory traversal of factory and user data with the fs::directory_iterator stuff looking for .lfopreset
+   auto factoryPath = fs::path( { string_to_path(s->datapath) / fs::path{ PresetDir } } );
+   auto userPath = fs::path( { string_to_path(s->userDataPath) / fs::path{ PresetDir } } );
+
+   std::vector<Category> res;
+
+   for( int i=0; i<2; ++i )
+   {
+      auto p = (i ? userPath : factoryPath);
+      bool isU = i;
+      try
+      {
+         std::string currentCategoryName = "";
+         Category currentCategory;
+         for (auto& d : fs::recursive_directory_iterator(p))
+         {
+            // blah
+            auto base = fs::path(d).stem();
+            auto fn = fs::path(d).filename();
+            if (path_to_string(base) == path_to_string(fn))
+            {
+               if (currentCategory.presets.size() > 0)
+               {
+                  std::sort(currentCategory.presets.begin(), currentCategory.presets.end(),
+                            [](const Preset& a, const Preset& b) { return a.name < b.name; });
+                  res.push_back(currentCategory);
+               }
+               currentCategoryName = path_to_string(base);
+               currentCategory = Category();
+               currentCategory.name = currentCategoryName;
+               currentCategory.isUserCategory = isU;
+               continue;
+            }
+
+            Preset p;
+            p.name = path_to_string(base);
+            p.path = fs::path(d);
+            currentCategory.presets.push_back(p);
+         }
+         if (currentCategory.presets.size() > 0)
+         {
+            std::sort(currentCategory.presets.begin(), currentCategory.presets.end(),
+                      [](const Preset& a, const Preset& b) { return a.name < b.name; } );
+            res.push_back(currentCategory);
+         }
+      }
+      catch (const fs::filesystem_error& e)
+      {
+         // That's OK!
+      }
+   }
+   std::sort( res.begin(), res.end(), [](const Category &a, const Category &b ) {
+               if (a.isUserCategory == b.isUserCategory)
+                  return a.name < b.name;
+               if (a.isUserCategory)
+                  return true;
+               return false;
+               });
+    return res;
+}
+}
+}
diff --git a/src/common/ModulatorPresetManager.h b/src/common/ModulatorPresetManager.h
new file mode 100644
index 000000000..3837dca79
--- /dev/null
+++ b/src/common/ModulatorPresetManager.h
@@ -0,0 +1,55 @@
+/*
+** Surge Synthesizer is Free and Open Source Software
+**
+** Surge is made available under the Gnu General Public License, v3.0
+** https://www.gnu.org/licenses/gpl-3.0.en.html
+**
+** Copyright 2004-2020 by various individuals as described by the Git transaction log
+**
+** All source at: https://github.com/surge-synthesizer/surge.git
+**
+** Surge was a commercial product from 2004-2018, with Copyright and ownership
+** in that period held by Claes Johanson at Vember Audio. Claes made Surge
+** open source in September 2018.
+*/
+
+#pragma once
+
+#include "filesystem/import.h"
+#include <vector>
+class SurgeStorage;
+
+
+namespace Surge
+{
+namespace ModulatorPreset
+{
+/*
+ * Given a storage, scene, and LFO, stream stream it to a file relative to the location
+ * in the user directory LFO presets area
+ */
+void savePresetToUser( const fs::path & location, SurgeStorage *s, int scene, int lfo );
+
+/*
+ * Given a compelted path, load the preset into our storage
+ */
+void loadPresetFrom( const fs::path &location, SurgeStorage *s, int scene, int lfo );
+
+/*
+ * What are the presets we have? In some form of category order
+ */
+struct Preset {
+   std::string name;
+   fs::path path;
+};
+
+struct Category {
+   std::string name;
+   bool isUserCategory;
+   std::vector<Preset> presets;
+};
+
+std::vector<Category> getPresets( SurgeStorage *s );
+
+}
+}
\ No newline at end of file
diff --git a/src/common/SurgePatch.cpp b/src/common/SurgePatch.cpp
index 077f5043a..d39c39dd4 100644
--- a/src/common/SurgePatch.cpp
+++ b/src/common/SurgePatch.cpp
@@ -1527,45 +1527,8 @@ void SurgePatch::load_xml(const void* data, int datasize, bool is_preset)
           (p->QueryIntAttribute("i", &lfo) == TIXML_SUCCESS) && within_range(0, sc, 1) &&
           within_range(0, lfo, n_lfos - 1))
       {
-         if (p->QueryDoubleAttribute("shuffle", &d) == TIXML_SUCCESS)
-            stepsequences[sc][lfo].shuffle = (float)d;
-         if (p->QueryIntAttribute("loop_start", &j) == TIXML_SUCCESS)
-            stepsequences[sc][lfo].loop_start = j;
-         if (p->QueryIntAttribute("loop_end", &j) == TIXML_SUCCESS)
-            stepsequences[sc][lfo].loop_end = j;
-         if (p->QueryIntAttribute("trigmask", &j) == TIXML_SUCCESS)
-            stepsequences[sc][lfo].trigmask = j;
-
-         if (p->QueryIntAttribute("trigmask_0to15", &j ) == TIXML_SUCCESS )
-         {
-            stepsequences[sc][lfo].trigmask &= 0xFFFFFFFFFFFF0000;
-            j &= 0xFFFF;
-            stepsequences[sc][lfo].trigmask |= j;
-         };
-         if (p->QueryIntAttribute("trigmask_16to31", &j ) == TIXML_SUCCESS )
-         {
-            stepsequences[sc][lfo].trigmask &= 0xFFFFFFFF0000FFFF;
-            j &= 0xFFFF;
-            uint64_t jl = (uint64_t)j;
-            stepsequences[sc][lfo].trigmask |= jl << 16;
-         };
-         if (p->QueryIntAttribute("trigmask_32to47", &j ) == TIXML_SUCCESS )
-         {
-            stepsequences[sc][lfo].trigmask &= 0xFFFF0000FFFFFFFF;
-            j &= 0xFFFF;
-            uint64_t jl = (uint64_t)j;
-            stepsequences[sc][lfo].trigmask |= jl << 32;
-         };
-         
-         for (int s = 0; s < n_stepseqsteps; s++)
-         {
-            char txt[256];
-            sprintf(txt, "s%i", s);
-            if (p->QueryDoubleAttribute(txt, &d) == TIXML_SUCCESS)
-               stepsequences[sc][lfo].steps[s] = (float)d;
-            else
-               stepsequences[sc][lfo].steps[s] = 0.f;
-         }
+         stepSeqFromXmlElement(&(stepsequences[sc][lfo]), p );
+
       }
       p = TINYXML_SAFE_TO_ELEMENT(p->NextSibling("sequence"));
    }
@@ -1590,67 +1553,8 @@ void SurgePatch::load_xml(const void* data, int datasize, bool is_preset)
       auto mi = 0;
       if( p->QueryIntAttribute( "i", &v ) == TIXML_SUCCESS ) mi = v;
       auto *ms = &( msegs[sc][mi] );
-      ms->n_activeSegments = 0;
-      if( p->QueryIntAttribute( "activeSegments", &v ) == TIXML_SUCCESS ) ms->n_activeSegments = v;
-      if( p->QueryIntAttribute("endpointMode", &v) == TIXML_SUCCESS )
-         ms->endpointMode = (MSEGStorage::EndpointMode)v;
-      else
-         ms->endpointMode = MSEGStorage::EndpointMode::FREE;
-      if( p->QueryIntAttribute( "loopMode", &v ) == TIXML_SUCCESS )
-         ms->loopMode = (MSEGStorage::LoopMode)v;
-      else
-         ms->loopMode = MSEGStorage::LoopMode::LOOP;
-      if( p->QueryIntAttribute( "loopStart", &v ) == TIXML_SUCCESS )
-         ms->loop_start = v;
-      else
-         ms->loop_start = -1;
-
-      if( p->QueryIntAttribute( "loopEnd", &v ) == TIXML_SUCCESS )
-         ms->loop_end = v;
-      else
-         ms->loop_end = -1;
-
 
-      auto segs = TINYXML_SAFE_TO_ELEMENT(p->FirstChild( "segments" ));
-      if( segs )
-      {
-         auto seg = TINYXML_SAFE_TO_ELEMENT(segs->FirstChild( "segment" ) );
-         int idx = 0;
-         while( seg )
-         {
-            double d;
-#define MSGF(x) if( seg->QueryDoubleAttribute( #x, &d ) == TIXML_SUCCESS ) ms->segments[idx].x = d;
-            MSGF( duration );
-            MSGF( v0 );
-            MSGF( cpduration );
-            MSGF( cpv );
-            MSGF( nv1 );
-            
-            int t = 0;
-            if( seg->QueryIntAttribute( "type", &v ) == TIXML_SUCCESS ) t = v;
-            ms->segments[idx].type = (MSEGStorage::segment::Type)t;
-
-            if( seg->QueryIntAttribute( "useDeform", &v) == TIXML_SUCCESS )
-               ms->segments[idx].useDeform = v;
-            else
-               ms->segments[idx].useDeform = true;
-
-            if( seg->QueryIntAttribute( "invertDeform", &v) == TIXML_SUCCESS )
-               ms->segments[idx].invertDeform = v;
-            else
-               ms->segments[idx].invertDeform = false;
-
-            seg = TINYXML_SAFE_TO_ELEMENT( seg->NextSibling( "segment" ) );
-            
-            idx++;
-         }
-         if( idx != ms->n_activeSegments )
-         {
-            std::cout << "BAD RESTORE " << _D(idx) << _D(ms->n_activeSegments) << std::endl;
-         }
-      }
-      // Rebuild cache
-      Surge::MSEG::rebuildCache(ms);
+      msegFromXMLElement(ms, p );
       p = TINYXML_SAFE_TO_ELEMENT( p->NextSibling( "mseg" ) );
    }
 
@@ -2003,31 +1907,8 @@ unsigned int SurgePatch::save_xml(void** data) // allocates mem, must be freed b
             p.SetAttribute("scene", sc);
             p.SetAttribute("i", l);
 
-            for (int s = 0; s < n_stepseqsteps; s++)
-            {
-               sprintf(txt, "s%i", s);
-               if (stepsequences[sc][l].steps[s] != 0.f)
-                  p.SetAttribute(txt, float_to_str(stepsequences[sc][l].steps[s], txt2));
-            }
+            stepSeqToXmlElement(&(stepsequences[sc][l]), p, l < n_lfos_voice );
 
-            p.SetAttribute("loop_start", stepsequences[sc][l].loop_start);
-            p.SetAttribute("loop_end", stepsequences[sc][l].loop_end);
-            p.SetAttribute("shuffle", float_to_str(stepsequences[sc][l].shuffle, txt2));
-            if (l < n_lfos_voice )
-            {
-               uint64_t ttm = stepsequences[sc][l].trigmask;
-
-               // collapse in case an old surge loads this
-               uint64_t old_ttm = ( ttm & 0xFFFF ) | ( ( ttm >> 16 ) & 0xFFFF ) | ( ( ttm >> 32 ) & 0xFFFF );
-               
-               p.SetAttribute("trigmask", old_ttm);
-
-               p.SetAttribute("trigmask_0to15", ttm & 0xFFFF );
-               ttm = ttm >> 16;
-               p.SetAttribute("trigmask_16to31", ttm & 0xFFFF );
-               ttm = ttm >> 16;
-               p.SetAttribute("trigmask_32to47", ttm & 0xFFFF );
-            }
             ss.InsertEndChild(p);
          }
       }
@@ -2046,28 +1927,7 @@ unsigned int SurgePatch::save_xml(void** data) // allocates mem, must be freed b
             p.SetAttribute("i", l);
 
             auto *ms = &(msegs[sc][l]);
-            p.SetAttribute( "activeSegments", ms->n_activeSegments );
-            p.SetAttribute( "endpointMode", ms->endpointMode );
-            p.SetAttribute( "loopMode", ms->loopMode );
-            p.SetAttribute( "loopStart", ms->loop_start);
-            p.SetAttribute( "loopEnd", ms->loop_end );
-
-            TiXmlElement segs( "segments" );
-            for( int s=0; s<ms->n_activeSegments; ++s )
-            {
-               TiXmlElement seg( "segment" );
-               seg.SetDoubleAttribute( "duration", ms->segments[s].duration );
-               seg.SetDoubleAttribute( "v0", ms->segments[s].v0 );
-               seg.SetDoubleAttribute( "nv1", ms->segments[s].nv1 );
-               seg.SetDoubleAttribute( "cpduration", ms->segments[s].cpduration );
-               seg.SetDoubleAttribute( "cpv", ms->segments[s].cpv );
-               seg.SetAttribute( "type", (int)( ms->segments[s].type ));
-               seg.SetAttribute( "useDeform", (int)(ms->segments[s].useDeform));
-               seg.SetAttribute( "invertDeform", (int)(ms->segments[s].invertDeform));
-               segs.InsertEndChild( seg );
-            }
-            p.InsertEndChild(segs);
-            
+            msegToXMLElement(ms, p );
             mseg.InsertEndChild( p );
          }
       }
@@ -2210,3 +2070,170 @@ unsigned int SurgePatch::save_xml(void** data) // allocates mem, must be freed b
    *data = d;
    return s.size();
 }
+
+void SurgePatch::msegToXMLElement(MSEGStorage* ms, TiXmlElement& p) const
+{
+   p.SetAttribute( "activeSegments", ms->n_activeSegments );
+   p.SetAttribute( "endpointMode", ms->endpointMode );
+   p.SetAttribute( "loopMode", ms->loopMode );
+   p.SetAttribute( "loopStart", ms->loop_start);
+   p.SetAttribute( "loopEnd", ms->loop_end );
+
+   TiXmlElement segs( "segments" );
+   for( int s=0; s<ms->n_activeSegments; ++s )
+   {
+      TiXmlElement seg( "segment" );
+      seg.SetDoubleAttribute( "duration", ms->segments[s].duration );
+      seg.SetDoubleAttribute( "v0", ms->segments[s].v0 );
+      seg.SetDoubleAttribute( "nv1", ms->segments[s].nv1 );
+      seg.SetDoubleAttribute( "cpduration", ms->segments[s].cpduration );
+      seg.SetDoubleAttribute( "cpv", ms->segments[s].cpv );
+      seg.SetAttribute( "type", (int)( ms->segments[s].type ));
+      seg.SetAttribute( "useDeform", (int)(ms->segments[s].useDeform));
+      seg.SetAttribute( "invertDeform", (int)(ms->segments[s].invertDeform));
+      segs.InsertEndChild( seg );
+   }
+   p.InsertEndChild(segs);
+}
+
+void SurgePatch::msegFromXMLElement(MSEGStorage* ms, TiXmlElement* p) const
+{
+   int v;
+   ms->n_activeSegments = 0;
+   if( p->QueryIntAttribute( "activeSegments", &v ) == TIXML_SUCCESS ) ms->n_activeSegments = v;
+   if( p->QueryIntAttribute("endpointMode", &v) == TIXML_SUCCESS )
+      ms->endpointMode = (MSEGStorage::EndpointMode)v;
+   else
+      ms->endpointMode = MSEGStorage::EndpointMode::FREE;
+   if( p->QueryIntAttribute( "loopMode", &v ) == TIXML_SUCCESS )
+      ms->loopMode = (MSEGStorage::LoopMode)v;
+   else
+      ms->loopMode = MSEGStorage::LoopMode::LOOP;
+   if( p->QueryIntAttribute( "loopStart", &v ) == TIXML_SUCCESS )
+      ms->loop_start = v;
+   else
+      ms->loop_start = -1;
+
+   if( p->QueryIntAttribute( "loopEnd", &v ) == TIXML_SUCCESS )
+      ms->loop_end = v;
+   else
+      ms->loop_end = -1;
+
+
+   auto segs = TINYXML_SAFE_TO_ELEMENT(p->FirstChild( "segments" ));
+   if( segs )
+   {
+      auto seg = TINYXML_SAFE_TO_ELEMENT(segs->FirstChild( "segment" ) );
+      int idx = 0;
+      while( seg )
+      {
+         double d;
+#define MSGF(x) if( seg->QueryDoubleAttribute( #x, &d ) == TIXML_SUCCESS ) ms->segments[idx].x = d;
+         MSGF( duration );
+         MSGF( v0 );
+         MSGF( cpduration );
+         MSGF( cpv );
+         MSGF( nv1 );
+
+         int t = 0;
+         if( seg->QueryIntAttribute( "type", &v ) == TIXML_SUCCESS ) t = v;
+         ms->segments[idx].type = (MSEGStorage::segment::Type)t;
+
+         if( seg->QueryIntAttribute( "useDeform", &v) == TIXML_SUCCESS )
+            ms->segments[idx].useDeform = v;
+         else
+            ms->segments[idx].useDeform = true;
+
+         if( seg->QueryIntAttribute( "invertDeform", &v) == TIXML_SUCCESS )
+            ms->segments[idx].invertDeform = v;
+         else
+            ms->segments[idx].invertDeform = false;
+
+         seg = TINYXML_SAFE_TO_ELEMENT( seg->NextSibling( "segment" ) );
+
+         idx++;
+      }
+      if( idx != ms->n_activeSegments )
+      {
+         std::cout << "BAD RESTORE " << _D(idx) << _D(ms->n_activeSegments) << std::endl;
+      }
+   }
+   // Rebuild cache
+   Surge::MSEG::rebuildCache(ms);
+
+}
+
+void SurgePatch::stepSeqToXmlElement(StepSequencerStorage* ss, TiXmlElement& p, bool streamMask) const
+{
+   char txt[256], txt2[256];
+   for (int s = 0; s < n_stepseqsteps; s++)
+   {
+      sprintf(txt, "s%i", s);
+      if (ss->steps[s] != 0.f)
+         p.SetAttribute(txt, float_to_str(ss->steps[s], txt2));
+   }
+
+   p.SetAttribute("loop_start", ss->loop_start);
+   p.SetAttribute("loop_end", ss->loop_end);
+   p.SetAttribute("shuffle", float_to_str(ss->shuffle, txt2));
+   if(streamMask)
+   {
+      uint64_t ttm = ss->trigmask;
+
+      // collapse in case an old surge loads this
+      uint64_t old_ttm = ( ttm & 0xFFFF ) | ( ( ttm >> 16 ) & 0xFFFF ) | ( ( ttm >> 32 ) & 0xFFFF );
+
+      p.SetAttribute("trigmask", old_ttm);
+
+      p.SetAttribute("trigmask_0to15", ttm & 0xFFFF );
+      ttm = ttm >> 16;
+      p.SetAttribute("trigmask_16to31", ttm & 0xFFFF );
+      ttm = ttm >> 16;
+      p.SetAttribute("trigmask_32to47", ttm & 0xFFFF );
+   }
+}
+
+void SurgePatch::stepSeqFromXmlElement(StepSequencerStorage* ss, TiXmlElement* p) const
+{
+   double d;
+   int j;
+   if (p->QueryDoubleAttribute("shuffle", &d) == TIXML_SUCCESS)
+      ss->shuffle = (float)d;
+   if (p->QueryIntAttribute("loop_start", &j) == TIXML_SUCCESS)
+      ss->loop_start = j;
+   if (p->QueryIntAttribute("loop_end", &j) == TIXML_SUCCESS)
+      ss->loop_end = j;
+   if (p->QueryIntAttribute("trigmask", &j) == TIXML_SUCCESS)
+      ss->trigmask = j;
+
+   if (p->QueryIntAttribute("trigmask_0to15", &j ) == TIXML_SUCCESS )
+   {
+      ss->trigmask &= 0xFFFFFFFFFFFF0000;
+      j &= 0xFFFF;
+      ss->trigmask |= j;
+   };
+   if (p->QueryIntAttribute("trigmask_16to31", &j ) == TIXML_SUCCESS )
+   {
+      ss->trigmask &= 0xFFFFFFFF0000FFFF;
+      j &= 0xFFFF;
+      uint64_t jl = (uint64_t)j;
+      ss->trigmask |= jl << 16;
+   };
+   if (p->QueryIntAttribute("trigmask_32to47", &j ) == TIXML_SUCCESS )
+   {
+      ss->trigmask &= 0xFFFF0000FFFFFFFF;
+      j &= 0xFFFF;
+      uint64_t jl = (uint64_t)j;
+      ss->trigmask |= jl << 32;
+   };
+
+   for (int s = 0; s < n_stepseqsteps; s++)
+   {
+      char txt[256];
+      sprintf(txt, "s%i", s);
+      if (p->QueryDoubleAttribute(txt, &d) == TIXML_SUCCESS)
+         ss->steps[s] = (float)d;
+      else
+         ss->steps[s] = 0.f;
+   }
+}
\ No newline at end of file
diff --git a/src/common/SurgeStorage.h b/src/common/SurgeStorage.h
index 1feaf9fdf..117690220 100644
--- a/src/common/SurgeStorage.h
+++ b/src/common/SurgeStorage.h
@@ -806,6 +806,12 @@ class SurgePatch
    unsigned int save_xml(void** data);
    unsigned int save_RIFF(void** data);
 
+   // Factor these so the LFO Preset Mechanism can use them also
+   void msegToXMLElement( MSEGStorage *ms, TiXmlElement &parent ) const;
+   void msegFromXMLElement( MSEGStorage *ms, TiXmlElement *parent ) const;
+   void stepSeqToXmlElement( StepSequencerStorage *ss, TiXmlElement &parent, bool streamMask ) const;
+   void stepSeqFromXmlElement( StepSequencerStorage *ss, TiXmlElement *parent ) const;
+
    void load_patch(const void* data, int size, bool preset);
    unsigned int save_patch(void** data);
 
diff --git a/src/common/gui/SurgeGUIEditor.cpp b/src/common/gui/SurgeGUIEditor.cpp
index 18966c8ea..98da7e679 100644
--- a/src/common/gui/SurgeGUIEditor.cpp
+++ b/src/common/gui/SurgeGUIEditor.cpp
@@ -43,6 +43,7 @@
 #include "UIInstrumentation.h"
 #include "guihelpers.h"
 #include "DebugHelpers.h"
+#include "ModulatorPresetManager.h"
 
 #include <iostream>
 #include <iomanip>
@@ -4724,13 +4725,50 @@ void SurgeGUIEditor::showSettingsMenu(CRect &menuRect)
 
 VSTGUI::COptionMenu *SurgeGUIEditor::makeLfoMenu(VSTGUI::CRect &menuRect)
 {
+   int currentLfoId = modsource_editor[current_scene] - ms_lfo1;
+
+   int shapev = synth->storage.getPatch().scene[current_scene].lfo[currentLfoId].shape.val.i;
+   std::string what = "Lfo";
+   if( ls_mseg == shapev )
+      what = "MSEG";
+   if( ls_stepseq == shapev)
+      what = "StepSeq";
+
    COptionMenu* lfoSubMenu =
        new COptionMenu(menuRect, 0, 0, 0, 0, VSTGUI::COptionMenu::kNoDrawStyle);
-   addCallbackMenu(lfoSubMenu, "[?] LFO Presets", [](){} );
+   addCallbackMenu(lfoSubMenu, "[?] Modulator Presets", [](){} ); // FIXME - help needed
+   lfoSubMenu->addSeparator();
+   addCallbackMenu( lfoSubMenu,
+                   Surge::UI::toOSCaseForMenu("Save " + what + " As..."),
+                   [this, currentLfoId, what](){
+                      // Prompt for a name
+                      promptForMiniEdit("preset", "Enter " + what + " Preset Name", "Please Name Preset",
+                                        CPoint( -1, -1 ),
+                                        [this, currentLfoId](const std::string &s ) {
+                                           Surge::ModulatorPreset::savePresetToUser(string_to_path(s),
+                                                                                  &(this->synth->storage),
+                                                                                  current_scene,
+                                                                                  currentLfoId );
+                                        });
+                      // and save
+                   }
+                   );
+
    lfoSubMenu->addSeparator();
-   addCallbackMenu(lfoSubMenu, "Menu", [](){} );
-   addCallbackMenu(lfoSubMenu, "Coming", [](){} );
-   addCallbackMenu(lfoSubMenu, "Soon", [](){} );
+   auto presetCategories = Surge::ModulatorPreset::getPresets(&(synth->storage));
+   for( auto const &cat : presetCategories )
+   {
+      COptionMenu *catSubMenu = new COptionMenu( menuRect, 0, 0, 0, 0, VSTGUI::COptionMenu::kNoDrawStyle);
+      for( auto const &p : cat.presets )
+         addCallbackMenu(catSubMenu,
+                         Surge::UI::toOSCaseForMenu(p.name),
+                         [this, p, currentLfoId](){
+                            Surge::ModulatorPreset::loadPresetFrom(p.path, &(this->synth->storage), current_scene, currentLfoId );
+                            this->synth->refresh_editor = true;
+                         });
+      lfoSubMenu->addEntry(catSubMenu, cat.name.c_str());
+      catSubMenu->forget();
+   }
    return lfoSubMenu;
 }
 
