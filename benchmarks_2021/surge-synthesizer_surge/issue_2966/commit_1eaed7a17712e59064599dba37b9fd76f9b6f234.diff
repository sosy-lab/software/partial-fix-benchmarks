diff --git a/src/common/SurgeStorage.h b/src/common/SurgeStorage.h
index 63f0f9921..ef91735d2 100644
--- a/src/common/SurgeStorage.h
+++ b/src/common/SurgeStorage.h
@@ -765,6 +765,7 @@ struct MSEGStorage {
    std::array<float, max_msegs> segmentStart, segmentEnd;
    float durationToLoopEnd;
    float durationLoopStartToLoopEnd;
+   float envelopeModeDuration = -1;
 
    static constexpr float minimumDuration = 0.0;
 };
diff --git a/src/common/dsp/MSEGModulationHelper.cpp b/src/common/dsp/MSEGModulationHelper.cpp
index 8e77f5c58..6df1c8c16 100644
--- a/src/common/dsp/MSEGModulationHelper.cpp
+++ b/src/common/dsp/MSEGModulationHelper.cpp
@@ -41,7 +41,7 @@ void rebuildCache( MSEGStorage *ms )
 
       if (nextseg >= ms->n_activeSegments)
       {
-         if (ms->endpointMode == MSEGStorage::EndpointMode::LOCKED)
+         if (ms->endpointMode == MSEGStorage::EndpointMode::LOCKED || ms->editMode == MSEGStorage::LFO)
             ms->segments[i].nv1 = ms->segments[0].v0;
       }
       else
@@ -56,6 +56,19 @@ void rebuildCache( MSEGStorage *ms )
    }
 
    ms->totalDuration = totald;
+   if( ms->editMode == MSEGStorage::ENVELOPE )
+      ms->envelopeModeDuration = totald;
+
+   if( ms->editMode == MSEGStorage::LFO && totald != 1.0 )
+   {
+      if( fabs( totald - 1.0 ) > 1e-5 )
+      {
+         // FIXME: Should never happen but WHAT TO DO HERE!
+         // std::cout << "SOFTWARE ERROR" << std::endl;
+      }
+      ms->totalDuration = 1.0;
+      ms->segmentEnd[ms->n_activeSegments-1] = 1.0;
+   }
 
    for (int i = 0; i < ms->n_activeSegments; ++i)
    {
@@ -704,7 +717,9 @@ void insertBefore( MSEGStorage *ms, float t ) {
 }
 
 void extendTo( MSEGStorage *ms, float t, float nv ) {
+   if( ms->editMode == MSEGStorage::LFO ) return;
    if( t < ms->totalDuration ) return;
+
    nv = limit_range( nv, -1.f, 1.f );
 
    // This will extend the loop end if it is on the last point; but we want that
@@ -724,7 +739,7 @@ void extendTo( MSEGStorage *ms, float t, float nv ) {
    ms->segments[sn].cpv = 0;
    ms->segments[sn].nv1 = nv;
 
-   if( ms->endpointMode == MSEGStorage::EndpointMode::LOCKED )
+   if( ms->endpointMode == MSEGStorage::EndpointMode::LOCKED || ms->editMode == MSEGStorage::LFO )
    {
       // The first point has to match where I just clicked. Adjust it and its control point
       float cpdratio = 0.5;
@@ -884,7 +899,7 @@ void scaleValues(MSEGStorage *ms, float factor)
    for (int i = 0; i < ms->n_activeSegments; i++)
       ms->segments[i].v0 *= factor;
 
-   if (ms->endpointMode == MSEGStorage::EndpointMode::FREE)
+   if (ms->endpointMode == MSEGStorage::EndpointMode::FREE && ms->editMode != MSEGStorage::LFO)
       ms->segments[ms->n_activeSegments - 1].nv1 *= factor;
 
    Surge::MSEG::rebuildCache(ms);
@@ -920,7 +935,7 @@ void mirrorMSEG(MSEGStorage *ms)
       ms->segments[h].v0 = ms->segments[h].nv1;
 
    // special case end node in start/end unlinked mode
-   if (ms->endpointMode == MSEGStorage::EndpointMode::FREE)
+   if (ms->endpointMode == MSEGStorage::EndpointMode::FREE && ms->editMode != MSEGStorage::LFO)
       ms->segments[ms->n_activeSegments - 1].nv1 = v0;
 
    // adjust curvature to flip it the other way around for curve types that need this
@@ -943,5 +958,136 @@ void mirrorMSEG(MSEGStorage *ms)
    Surge::MSEG::rebuildCache(ms);
 }
 
+void modifyEditMode(MSEGStorage *ms, MSEGStorage::EditMode em )
+{
+   if( em == ms->editMode ) return;
+   float targetDuration = 1.0;
+   if( ms->editMode == MSEGStorage::LFO && em == MSEGStorage::ENVELOPE )
+   {
+      if( ms->envelopeModeDuration > 0 )
+         targetDuration = ms->envelopeModeDuration;
+   }
+
+   float durRatio = targetDuration / ms->totalDuration;
+   for( auto &s : ms->segments )
+      s.duration *= durRatio;
+   ms->editMode = em;
+   rebuildCache(ms);
+}
+
+void adjustDurationInternal( MSEGStorage *ms, int idx, float d, float snapResolution, float upperBound = 0)
+{
+   if( snapResolution <= 0 )
+   {
+      ms->segments[idx].duration = std::max( 0.f, ms->segments[idx].duration + d );
+   }
+   else
+   {
+      ms->segments[idx].dragDuration = std::min( 0.f, ms->segments[idx].dragDuration + d );
+      auto target = (float)(round( ( ms->segmentStart[idx] + ms->segments[idx].dragDuration ) / snapResolution ) * snapResolution ) - ms->segmentStart[idx];
+      if( upperBound > 0 && target > upperBound )
+         target = ms->segments[idx].duration;
+      if( target < MSEGStorage::minimumDuration )
+         target = ms->segments[idx].duration;
+      ms->segments[idx].duration = target;
+   }
+}
+
+void adjustDurationShiftingSubsequent(MSEGStorage* ms, int idx, float dx, float snap)
+{
+   if( ms->editMode == MSEGStorage::LFO )
+   {
+      if( ms->segmentEnd[idx] + dx > 1.0 )
+         dx = 1.0 - ms->segmentEnd[idx];
+      if( ms->segmentEnd[idx] + dx < 0.0 )
+         dx = ms->segmentEnd[idx];
+      if( -dx > ms->segments[idx].duration )
+         dx = -ms->segments[idx].duration;
+   }
+
+   if (idx >= 0)
+   {
+      auto rcv = 0.5;
+      if( ms->segments[idx].duration > 0 )
+          rcv = ms->segments[idx].cpduration / ms->segments[idx].duration;
+      adjustDurationInternal(ms, idx, dx, snap);
+      ms->segments[idx].cpduration = ms->segments[idx].duration * rcv;
+   }
+
+   if( ms->editMode == MSEGStorage::LFO )
+   {
+      if( dx > 0 )
+      {
+         /*
+          * Collapse the subsequent by 0 to squash me in
+          */
+         float toConsume = dx;
+         for (int i = ms->n_activeSegments - 1; i > idx && toConsume > 0; --i)
+         {
+            if (ms->segments[i].duration >= toConsume)
+            {
+               ms->segments[i].duration -= toConsume;
+               toConsume = 0;
+            }
+            else
+            {
+               toConsume -= ms->segments[i].duration;
+               ms->segments[i].duration = 0;
+            }
+         }
+      }
+      else
+      {
+         /*
+          * Need to expand the back
+          */
+         ms->segments[ms->n_activeSegments-1].duration -= dx;
+      }
+   }
+
+   rebuildCache(ms);
+}
+
+void adjustDurationConstantTotalDuration( MSEGStorage *ms, int idx, float dx, float snap )
+{
+   int prior = idx, next = idx + 1;
+
+   if (prior >= 0 &&
+       (ms->segments[prior].duration + dx) <= MSEGStorage::minimumDuration &&
+       dx < 0)
+      dx = 0;
+   if (next < ms->n_activeSegments &&
+       (ms->segments[next].duration - dx) <= MSEGStorage::minimumDuration &&
+       dx > 0)
+      dx = 0;
+
+   auto csum = 0.f, pd = 0.f;
+   if (prior >= 0)
+   {
+      csum += ms->segments[prior].duration;
+      pd = ms->segments[prior].duration;
+   }
+   if (next < ms->n_activeSegments)
+      csum += ms->segments[next].duration;
+   if (prior >= 0)
+   {
+      auto rcv = 0.5;
+      if( ms->segments[prior].duration > 0 )
+          rcv = ms->segments[prior].cpduration / ms->segments[prior].duration;
+      adjustDurationInternal(ms, prior, dx, snap, csum);
+      ms->segments[prior].cpduration = ms->segments[prior].duration * rcv;
+      pd = ms->segments[prior].duration;
+   }
+   if (next < ms->n_activeSegments)
+   {
+      auto rcv =
+          ms->segments[next].cpduration / ms->segments[next].duration;
+
+      // offsetDuration( ms->segments[next].duration, -dx );
+      ms->segments[next].duration = csum - pd;
+      ms->segments[next].cpduration = ms->segments[next].duration * rcv;
+   }
+   rebuildCache(ms);
+}
 }
 }
diff --git a/src/common/dsp/MSEGModulationHelper.h b/src/common/dsp/MSEGModulationHelper.h
index e06c7b41b..6f600bf46 100644
--- a/src/common/dsp/MSEGModulationHelper.h
+++ b/src/common/dsp/MSEGModulationHelper.h
@@ -52,6 +52,9 @@ namespace Surge
       void deleteSegment( MSEGStorage *s, float t );
       void deleteSegment( MSEGStorage *s, int idx );
 
+      void adjustDurationShiftingSubsequent( MSEGStorage *s, int idx, float dx, float snap = 0);
+      void adjustDurationConstantTotalDuration( MSEGStorage *s, int idx, float dx, float snap = 0);
+
       void resetControlPoint( MSEGStorage *s, float t );
       void resetControlPoint( MSEGStorage *s, int idx );
       void constrainControlPointAt( MSEGStorage *s, int idx );
@@ -60,5 +63,7 @@ namespace Surge
       void scaleValues(MSEGStorage* s, float factor);
       void setAllDurationsTo(MSEGStorage* s, float value);
       void mirrorMSEG(MSEGStorage* s);
+
+      void modifyEditMode( MSEGStorage *s, MSEGStorage::EditMode mode );
    }
 }
diff --git a/src/common/gui/MSEGEditor.cpp b/src/common/gui/MSEGEditor.cpp
index 8ac38bd50..ece71e9e7 100644
--- a/src/common/gui/MSEGEditor.cpp
+++ b/src/common/gui/MSEGEditor.cpp
@@ -202,26 +202,6 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
              };
    }
 
-   void offsetDuration( float &v, float d ) {
-      v = std::max( MSEGStorage::minimumDuration, v + d );
-   }
-   void adjustDuration( int idx, float d, float snapResolution, float upperBound)
-   {
-      if( snapResolution <= 0 )
-      {
-         offsetDuration(ms->segments[idx].duration, d);
-      }
-      else
-      {
-         offsetDuration( ms->segments[idx].dragDuration, d );
-         auto target = (float)(round( ( ms->segmentStart[idx] + ms->segments[idx].dragDuration ) / snapResolution ) * snapResolution ) - ms->segmentStart[idx];
-         if( upperBound > 0 && target > upperBound )
-            target = ms->segments[idx].duration;
-         if( target < MSEGStorage::minimumDuration )
-            target = ms->segments[idx].duration;
-         ms->segments[idx].duration = target;
-      }
-   }
    void offsetValue( float &v, float d ) {
       v = limit_range( v + d, -1.f, 1.f );
    }
@@ -336,56 +316,16 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
             hotzones.push_back(h);
          };
 
-         auto timeConstraint = [&](int prior, int next, float dx) {
+         auto timeConstraint = [&](int prior, float dx) {
             switch (this->timeEditMode)
             {
             case DRAW:
                break;
             case SHIFT:
-               if (prior >= 0)
-               {
-                  auto rcv =
-                      this->ms->segments[prior].cpduration / this->ms->segments[prior].duration;
-                  adjustDuration(prior, dx, eds->hSnap, 0);
-                  this->ms->segments[prior].cpduration = this->ms->segments[prior].duration * rcv;
-               }
+               Surge::MSEG::adjustDurationShiftingSubsequent(this->ms, prior, dx, eds->hSnap);
                break;
             case SINGLE:
-               if (prior >= 0 &&
-                   (this->ms->segments[prior].duration + dx) <= MSEGStorage::minimumDuration &&
-                   dx < 0)
-                  dx = 0;
-               if (next < ms->n_activeSegments &&
-                   (this->ms->segments[next].duration - dx) <= MSEGStorage::minimumDuration &&
-                   dx > 0)
-                  dx = 0;
-
-               auto csum = 0.f, pd = 0.f;
-               if (prior >= 0)
-               {
-                  csum += ms->segments[prior].duration;
-                  pd = ms->segments[prior].duration;
-               }
-               if (next < ms->n_activeSegments)
-                  csum += ms->segments[next].duration;
-               if (prior >= 0)
-               {
-                  auto rcv =
-                      this->ms->segments[prior].cpduration / this->ms->segments[prior].duration;
-                  adjustDuration(prior, dx, eds->hSnap, csum);
-                  this->ms->segments[prior].cpduration = this->ms->segments[prior].duration * rcv;
-                  pd = ms->segments[prior].duration;
-               }
-               if (next < ms->n_activeSegments)
-               {
-                  auto rcv =
-                      this->ms->segments[next].cpduration / this->ms->segments[next].duration;
-
-                  // offsetDuration( this->ms->segments[next].duration, -dx );
-                  this->ms->segments[next].duration = csum - pd;
-                  this->ms->segments[next].cpduration = this->ms->segments[next].duration * rcv;
-               }
-
+               Surge::MSEG::adjustDurationConstantTotalDuration(this->ms, prior, dx, eds->hSnap);
                break;
             }
          };
@@ -400,7 +340,7 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
 
                 if (i != 0)
                 {
-                   timeConstraint(i - 1, i, dx / tscale);
+                   timeConstraint(i - 1, dx / tscale);
                 }
              });
 
@@ -552,7 +492,7 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
             rectForPoint( tpx(ms->totalDuration), ms->segments[ms->n_activeSegments-1].nv1, /* which is [0].v0 in lock mode only */
                          hotzone::SEGMENT_ENDPOINT,
                           [this, vscale, tscale, unipolarFactor](float dx, float dy, const CPoint &where) {
-                             if( ms->endpointMode == MSEGStorage::EndpointMode::FREE )
+                             if( ms->endpointMode == MSEGStorage::EndpointMode::FREE && ms->editMode != MSEGStorage::LFO )
                              {
                                 float d = -2 * dy / vscale;
                                 float snapResolution = eds->vSnap * unipolarFactor;
@@ -575,15 +515,19 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
                              // We need to deal with the cpduration also
                              auto cpv = this->ms->segments[ms->n_activeSegments-1].cpduration / this->ms->segments[ms->n_activeSegments-1].duration;
 
-                             if( !this->getViewSize().pointInside(where))
+                             // Don't allow endpoint time adjust in LFO mode
+                             if( this->ms->editMode == MSEGStorage::ENVELOPE )
                              {
-                                auto howFar = where.x - this->getViewSize().right;
-                                if( howFar > 0 )
-                                   dx *= howFar * 0.1; // this is really just a speedup as our axes shrinks. Just a fudge
-                             }
+                                if (!this->getViewSize().pointInside(where))
+                                {
+                                   auto howFar = where.x - this->getViewSize().right;
+                                   if (howFar > 0)
+                                      dx *= howFar * 0.1; // this is really just a speedup as our axes shrinks. Just a fudge
+                                }
 
-                             adjustDuration(ms->n_activeSegments-1, dx/tscale, eds->hSnap, 0 );
-                             this->ms->segments[ms->n_activeSegments-1].cpduration = this->ms->segments[ms->n_activeSegments-1].duration * cpv;
+                                Surge::MSEG::adjustDurationShiftingSubsequent(
+                                    ms, ms->n_activeSegments - 1, dx / tscale, eds->hSnap);
+                             }
                           } );
          }
       }
@@ -778,12 +722,18 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
             if( es.lastEval >= 0 && es.lastEval <= ms->n_activeSegments - 1 && ms->segments[es.lastEval].type == MSEGStorage::segment::Type::BROWNIAN )
                vdef = v;
 
-            if( es.lastEval != priorEval )
+            int compareWith = es.lastEval;
+            if( up > ms->totalDuration )
+               compareWith = ms->n_activeSegments - 1;
+
+            if( compareWith != priorEval )
             {
                // OK so make sure that priorEval nv1 is in there
                path->addLine(i, valpx(ms->segments[priorEval].nv1));
-               for( int ns=priorEval + 1; ns <= es.lastEval; ns++ )
-                  path->addLine( i, valpx( ms->segments[ns].v0));
+               for( int ns=priorEval + 1; ns <= compareWith; ns++ )
+               {
+                  path->addLine(i, valpx(ms->segments[ns].v0));
+               }
                priorEval = es.lastEval;
             }
 
@@ -1545,7 +1495,8 @@ struct MSEGCanvas : public CControl, public Surge::UI::SkinConsumingComponent, p
                                  this->modelChanged();
                              }
                          });
-         cm->setChecked( this->ms->endpointMode == MSEGStorage::EndpointMode::LOCKED );
+         cm->setChecked( this->ms->endpointMode == MSEGStorage::EndpointMode::LOCKED || this->ms->editMode == MSEGStorage::LFO);
+         cm->setEnabled( this->ms->editMode != MSEGStorage::LFO );
  
          settingsMenu->addSeparator();
  
@@ -1627,7 +1578,8 @@ void MSEGControlRegion::valueChanged( CControl *p )
    case tag_edit_mode:
    {
       int m = val > 0.5 ? 1 : 0;
-      ms->editMode = (MSEGStorage::EditMode)m;
+      auto editMode = (MSEGStorage::EditMode)m;
+      Surge::MSEG::modifyEditMode(this->ms, editMode );
       canvas->modelChanged();
 
       break;
