MouseWheel on filter Subtype (#3325)

Closes #3324
Filter Indirection and Rename step 1: Tests

This commit streams a bunch of SV14 patches with each filter
configuration available as of 6571f4a, makes a file with all
filters set to the type/subtype, streams it, comments out that
streamer, checkjs in the patches, and then loads it and
makes sure it loads OK. This is groundwork for changing the
enum / name of the filters and making sure we can test
old filters load.

Step along the way on #3006
Filter Indirection and Rename step 1: Tests (#3327)

This commit streams a bunch of SV14 patches with each filter
configuration available as of 6571f4a, makes a file with all
filters set to the type/subtype, streams it, comments out that
streamer, checkjs in the patches, and then loads it and
makes sure it loads OK. This is groundwork for changing the
enum / name of the filters and making sure we can test
old filters load.

Step along the way on #3006
Checkpoint on way to #3006
Split all the Filters and adjust stremaing

1. Split filters by types more granularly
2. Stream accordingly (including ff_revision bump)
3. Adjust stremaing regtests

Addresses #3006
Split all the Filters and adjust stremaing and UI

1. Split filters by types more granularly
2. Stream accordingly (including ff_revision bump)
3. Adjust stremaing regtests
4. Make a streaming re-order map and apply it to the UI
5. Various fixes, renames, and restructures to make it hang together

This is a big change.

Closes #3006
Split all the Filters and adjust stremaing and UI (#3329)

1. Split filters by types more granularly
2. Stream accordingly (including ff_revision bump)
3. Adjust stremaing regtests
4. Make a streaming re-order map and apply it to the UI
5. Various fixes, renames, and restructures to make it hang together

This is a big change.

Closes #3006
