VST3 in some DAWs don’t disable scene width properly when changing filter config
OK this i s avery different bug. For some reason the param UI updates are getting surpassed sometimes. Even in Reaper. Will need a bit of time to fix this one.
Looks like the issue I described over on Discord is the same as this one.
![RealProb](https://user-images.githubusercontent.com/13387561/99836113-9949a900-2b33-11eb-86c8-9cc61bb916a6.gif)

Right so here's a picture in live (look! I'm running live!) on mac vst3. If I click the value it works fine. But if I automate it with a continuous automator it doesn't. So clearly my 'set value of filter config to 0.742' is correctly setting the code path to change the config but not the code path to modify the width slider.

Now I know that should be able to knock this one down pretty easily.
It seems that not every automation change live sends comes through line 797 of SGE; but they do not seem to. I wonder why.
OK I now know exactly why this is happening. A fix is a teensy bit tricky but short version is

1: Live sends the VST3 changes a lot and that fills up our change buffer due to bug 1 in setParameter01. Basically the loop to see if the param refresh is true should be < 0 || == index
2: When the change buffer fills up we use a different code path to refresh everything (that was the "korridor automation stops at 8 fix") but that code path doesn't update sliders. That's bug 2 in SGE and is a wee bit trickier.
3: When you fix #1, it seems the width activation is off by one. That is, it comes on on D2 and RNG not on < > and what not.

But I'll have a fix to this one sometime today. Have caught it in the debugger with a clear explanation of the bugs!
OK so have 1 and 2 fixed; now just 3 left which I think is a round vs floor type thing.  Off for the afternoon but will get this one closed today.