Add Callibrated Makeup Gain for NL Filters (#3428)

Add a commit to (1) generate normalization coefficients by
comparing with other filters and (2) use them.

Co-authored-by: jatinchowdhury18 <jatinchowdhury18@users.noreply.github.com>
SubDirectories in Presets Menu Work

Closes #3392
SubDirectories in Presets Menu Work

Closes #3392
SubDirectories in Presets Menu Work (#3429)

Closes #3392
Handle the case of modpreset with no presets but subdirs

We would add categories only if we found a preset; but that
meant pure-directory organization would be mis-leveled

Closes #3392
Handle the case of modpreset with no presets but subdirs (#3679)

We would add categories only if we found a preset; but that
meant pure-directory organization would be mis-leveled

Closes #3392
