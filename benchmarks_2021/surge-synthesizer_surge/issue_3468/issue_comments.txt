Oscillator drift - init at random value
So to do this, in SurgeSuperOscillator

1. Initialize driftlfo2[i] (which is the state variable) - you can leave driftlfo[i] alone
2. The range of these things is roughly 0.00001/sqrt(0.00001) in a step which is 0.0031 so the range within a few hundred steps assuming root n motion would be 0.03 or so. Sure why not. So probably initialize to something like 0.05 * ( rand()/(float)(RAND_MAX) - 0.5f )
3. I don't think we need to make this an option. 
> "I don't think we need to make this an option."

I actually like the current behavior, where it starts out in-tune and then drifts out of tune as the note is held. (So staccato sounds are not really affected.) 

I'm afraid that starting from a random offset will cause it to be too noticeable and sound out-of-tune, especially if the same note is triggered rapidly in succession.
OK lemme reopen it and we can make an option then
Never seen this argued for any other synth, but ok...

@williamfields did you actually try it as it is now? It is extremely subtle and never sounds out-of-tune.
I don't feel too strongly about it, so feel free to disagree. I know we don't want too many options either. I just figure Claes must have had a reason for implementing it like that, and it has served me well so far.
Happy to add options and you are right we should not change the sound of patches. We will add an option which is off by default in old patches and on in new
@williamfields the next nightly willl have this as an option in the drift RMB menu
Old patches (streamed with 1.8 or earlier) will default it to 'start at zero'
New patches and sessions will default it to 'start at non-zero'
Thanks for bugging us on this one!