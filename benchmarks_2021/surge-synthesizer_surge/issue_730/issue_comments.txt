feature request: method for drawing ramps into stepsequencer
@esaruoho you saw this from the manual right ? 
Holding down shift while drawing will quantize the values to 1/12th steps, hence if the LFO is used to modulate pitch by an octave, each step will represent a semitone.
i definitely advocate .. User presets load and save for Step seq envs. Also Rnd .. 
copy and paste.. too 

Yeah that’s all useful; lemme bump it into the 1.6.n group
This is how Kontakt does it: right mouse drag gives you a line overlay and when you release it you get your linear ramp!

![Kontakt table linear ramp](https://user-images.githubusercontent.com/2393720/83451262-e9826100-a456-11ea-9fe8-7d65f105bb57.gif)

that's exactly what we want yeah.
I wanted that feature for years, Would be very useful!
![stepline](https://user-images.githubusercontent.com/13387561/83472808-15511700-a456-11ea-88e3-352a96c9c83d.gif)

merging now. This is what happens when you RMB-drag across the step sequencer.
Just a quick reopen to update the spec of the suggested behavior: 

- [x] ramp should only be applied on RMB mouse up
- [x] if while dragging the ramp we return the endpoint back to the starting step, ramp shouldn't be drawn at all and table should not be updated

Really cool! However, on my system, steps don't update in real-time (like that GIF you sent above), only when releasing right mouse drag.
Yeah we changed it overnight to be non-destructive.