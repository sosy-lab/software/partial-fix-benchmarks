diff --git a/src/common/dsp/AdsrEnvelope.h b/src/common/dsp/AdsrEnvelope.h
index e07ce2fe4..30a04a240 100644
--- a/src/common/dsp/AdsrEnvelope.h
+++ b/src/common/dsp/AdsrEnvelope.h
@@ -145,7 +145,13 @@ class AdsrEnvelope : public ModulationSource
       if (lc[mode].b)
       {
          /*
-         ** This is the "analog" mode of the envelope
+         ** This is the "analog" mode of the envelope. If you are unclear what it is doing
+         ** because of the SSE the algo is pretty simple; charge up and discharge a capacitor
+         ** with a gate. charge until you hit 1, discharge while the gate is open floored at
+         ** the Sustain; then release.
+         **
+         ** There is, in src/headless/UnitTests.cpp in the "Clone the Analog" section, 
+         ** a non-SSE implementation of this which makes it much easier to understand.
          */
          const float v_cc = 1.5f;
 
@@ -314,6 +320,8 @@ class AdsrEnvelope : public ModulationSource
       }
    }
 
+   int getEnvState() { return envstate; }
+   
 private:
    ADSRStorage* adsr = nullptr;
    SurgeVoiceState* state = nullptr;
diff --git a/src/headless/UnitTests.cpp b/src/headless/UnitTests.cpp
index 3be2d7e52..b9a4e8ebd 100644
--- a/src/headless/UnitTests.cpp
+++ b/src/headless/UnitTests.cpp
@@ -1,6 +1,7 @@
 #include <iostream>
 #include <iomanip>
 #include <sstream>
+#include <algorithm>
 
 #include "HeadlessUtils.h"
 #include "Player.h"
@@ -10,6 +11,7 @@
 #define CATCH_CONFIG_RUNNER
 #include "catch2.hpp"
 
+
 TEST_CASE( "We can read a collection of wavetables", "[wav]" )
 {
    /*
@@ -467,6 +469,386 @@ TEST_CASE( "lipol_ps class", "[dsp]" )
       
 }
 
+void copyScenedataSubset(SurgeStorage *storage, int scene, int start, int end) {
+   int s = storage->getPatch().scene_start[scene];
+   for(int i=start; i<end; ++i )
+   {
+      storage->getPatch().scenedata[scene][i-s].i =
+         storage->getPatch().param_ptr[i]->val.i;
+   }
+}
+
+void setupStorageRanges(Parameter *start, Parameter *endIncluding,
+                        int &storage_id_start, int &storage_id_end) {
+   int min_id = 100000, max_id = -1;
+   Parameter *oap = start;
+   while( oap <= endIncluding )
+   {
+      if( oap->id >= 0 )
+      {
+         if( oap->id > max_id ) max_id = oap->id;
+         if( oap->id < min_id ) min_id = oap->id;
+      }
+      oap++;
+   }
+   
+   storage_id_start = min_id;
+   storage_id_end = max_id + 1;
+}
+
+TEST_CASE( "ADSR Envelope Behaviour", "[mod]" )
+{
+   
+   std::shared_ptr<SurgeSynthesizer> surge( Surge::Headless::createSurge(44100) );
+   REQUIRE( surge.get() );
+
+   /*
+   ** OK so lets set up a pretty simple setup 
+   */
+
+   auto runAdsr = [surge](float a, float d, float s, float r,
+                          int a_s, int d_s, int r_s,
+                          bool isAnalog,
+                          float releaseAfter, float runUntil )
+                     {
+                        auto* adsrstorage = &(surge->storage.getPatch().scene[0].adsr[0]);
+                        std::shared_ptr<AdsrEnvelope> adsr( new AdsrEnvelope() );
+                        adsr->init( &(surge->storage), adsrstorage, surge->storage.getPatch().scenedata[0], nullptr );
+                        REQUIRE( adsr.get() );
+                        
+                        int ids, ide;
+                        setupStorageRanges(&(adsrstorage->a), &(adsrstorage->mode), ids, ide );
+                        REQUIRE( ide > ids );
+                        REQUIRE( ide >= 0 );
+                        REQUIRE( ids >= 0 );
+                        
+                        auto svn = [](Parameter *p, float vn)
+                                      {
+                                         p->set_value_f01( p->value_to_normalized( limit_range( vn, p->val_min.f, p->val_max.f ) ) );
+                                      };
+                        
+                        auto inverseEnvtime = [](float desiredTime)
+                                                 {
+                                                    // 2^x = desired time
+                                                    auto x = log(desiredTime)/log(2.0);
+                                                    return x;
+                                                 };
+                        
+                        svn(&(adsrstorage->a), inverseEnvtime(a));
+                        svn(&(adsrstorage->d), inverseEnvtime(d));
+                        svn(&(adsrstorage->s), s);
+                        svn(&(adsrstorage->r), inverseEnvtime(r));
+                        
+                        svn(&(adsrstorage->a_s), a_s);
+                        svn(&(adsrstorage->d_s), d_s);
+                        svn(&(adsrstorage->r_s), r_s);
+                        
+                        adsrstorage->mode.val.b = isAnalog;
+                        
+                        copyScenedataSubset(&(surge->storage), 0, ids, ide);
+                        adsr->attack();
+
+                        bool released = false;
+                        int i = 0;
+                        std::vector<std::pair<float,float>> res;
+                        res.push_back(std::make_pair(0.f, 0.f));
+                        
+                        while( true )
+                        {
+                           auto t = 1.0 * (i+1) * BLOCK_SIZE * dsamplerate_inv;
+                           i++;
+                           if( t > runUntil || runUntil < 0 )
+                              break;
+                           
+                           if( t > releaseAfter && ! released )
+                           {
+                              adsr->release();
+                              released = true;
+                           }
+                           
+                           adsr->process_block();
+                           res.push_back( std::make_pair( (float)t, adsr->output ) );
+                           if( false && i > 270 && i < 290 )
+                              std::cout << i << " " << t << " " << adsr->output << " " << adsr->getEnvState() 
+                                        << std::endl;
+                        }
+                        return res;
+                     };
+
+   auto detectTurnarounds = [](std::vector<std::pair<float,float>> data) {
+                               auto pv = -1000.0;
+                               int dir = 1;
+                               std::vector<std::pair<float,int>> turns;
+                               turns.push_back( std::make_pair( 0, 1 ) );
+                               for( auto &p : data )
+                               {
+                                  auto t = p.first;
+                                  auto v = p.second;
+                                  if( pv >= 0 )
+                                  {
+                                     int ldir = 0;
+                                     if( v > 0.999999f ) ldir = dir; // sometimes we get a double '1'
+                                     if( fabs( v - pv ) < 5e-6 && fabs( v ) < 1e-5) ldir = 0; // bouncing off of 0 is annoying
+                                     else if( fabs( v - pv ) < 5e-7 ) ldir = 0;
+                                     else if( v > pv ) ldir = 1;
+                                     else ldir = -1;
+
+                                     if( v != 1 )
+                                     {
+                                        if( ldir != dir )
+                                        {
+                                           turns.push_back(std::make_pair(t, ldir) );
+                                        }
+                                        dir = ldir;
+                                     }
+                                  }
+                                  pv = v;
+                               }
+                               return turns;
+                            };
+   
+   // With 0 sustain I should decay in decay time
+   auto runCompare = [&](float a, float d, float s, float r, int a_s, int d_s, int r_s,  bool isAnalog )
+                        {
+                           float sustime = 0.1;
+                           float endtime = 0.1;
+                           float totaltime = a + d + sustime + r + endtime;
+                           
+                           auto simple = runAdsr( a, d, s, r, a_s, d_s, r_s, isAnalog, a + d + sustime, totaltime );
+                           auto sturns = detectTurnarounds(simple);
+                           if( false )
+                              std::cout << "ADSR: " << a << " " << d << " " << s << " " << r << " switches: " << a_s << " " << d_s << " " << r_s << std::endl;
+                           if( s == 0 )
+                           {
+                              if( sturns.size() != 3 )
+                              {
+                                 for( auto s : simple )
+                                    std::cout << s.first << " " << s.second << std::endl;
+                                 for( auto s : sturns )
+                                    std::cout << s.first << " " << s.second << std::endl;
+                              }
+                              REQUIRE( sturns.size() == 3 );
+                              REQUIRE( sturns[0].first == 0 );
+                              REQUIRE( sturns[0].second == 1 );
+                              REQUIRE( sturns[1].first == Approx( a ).margin( 0.01 ) );
+                              REQUIRE( sturns[1].second == -1 );
+                              REQUIRE( sturns[2].first == Approx( a + d ).margin( 0.01 ) );
+                              REQUIRE( sturns[2].second == 0 );
+                           }
+                           else
+                           {
+                              if( sturns.size() != 5 )
+                              {
+                                 for( auto s : simple )
+                                    std::cout << s.first << " " << s.second << std::endl;
+                                 for( auto s : sturns )
+                                    std::cout << s.first << " " << s.second << std::endl;
+                              }
+                              REQUIRE( sturns.size() == 5 );
+                              REQUIRE( sturns[0].first == 0 );
+                              REQUIRE( sturns[0].second == 1 );
+                              REQUIRE( sturns[1].first == Approx( a ).margin( 0.01 ) );
+                              REQUIRE( sturns[1].second == -1 );
+                              if( d_s == 0 )
+                              {
+                                 // this equality only holds in the linear case; in the polynomial case you get faster reach to non-zero sustain
+                                 REQUIRE( sturns[2].first == Approx( a + d * ( 1.0 - s ) ).margin( 0.01 ) );
+                              }
+                              else if( a + d * ( 1.0 - s ) > 0.1 && d > 0.05 )
+                              {
+                                 REQUIRE( sturns[2].first < a + d * ( 1.0 - s ) + 0.01 );
+                              }
+                              REQUIRE( sturns[2].second == 0 );
+                              REQUIRE( sturns[3].first == Approx( a + d + sustime ).margin( 0.01 ) );
+                              REQUIRE( sturns[3].second == -1 );
+                              if( r_s == 0 || s > 0.1 && r > 0.05 ) // if we are in the non-linear releases at low sustain we get there early
+                              {
+                                 REQUIRE( sturns[4].first == Approx( a + d + sustime + r ).margin( ( r_s == 0 ? 0.01 : ( r * 0.1 ) ) ) );
+                                 REQUIRE( sturns[4].second == 0 );
+                              }
+                           }
+                        };
+
+   SECTION( "Test the Digital Envelope" )
+   {
+      for( int as=0;as<3;++as )
+         for( int ds=0; ds<3; ++ds )
+            for( int rs=0; rs<3; ++rs )
+            {
+               runCompare( 0.2, 0.3, 0.0, 0.1, as, ds, rs, false );
+               runCompare( 0.2, 0.3, 0.5, 0.1, as, ds, rs, false );
+               
+               for( int rc=0;rc<10; ++rc )
+               {
+                  auto a = rand() * 1.0 / RAND_MAX;
+                  auto d = rand() * 1.0 / RAND_MAX;
+                  auto s = 0.8 * rand() * 1.0 / RAND_MAX + 0.1; // we have tested the s=0 case above
+                  auto r = rand() * 1.0 / RAND_MAX;
+                  runCompare( a, d, s, r, as, ds, rs, false );
+               }
+            }
+   }
+
+   SECTION( "Test the Analog Envelope" )
+   {
+      // OK so we can't check the same thing here since the turns aren't as tight in analog mode
+      // Also the analog ADSR sustains at half the given sustain.
+      auto testAnalog = [&](float a, float d, float s, float r )
+                           {
+                              // std::cout << "ANALOG " << a << " " << d << " " << s << " " << r << std::endl;
+                              auto holdFor = a + d + d + 0.5;
+                              
+                              auto ae = runAdsr( a, d, s, r, 0, 0, 0, true, holdFor, holdFor + 4 * r );
+                              auto aturns = detectTurnarounds(ae);
+
+                              float maxt=0, maxv=0;
+                              float zerot=0;
+                              float valAtRelEnd = -1;
+                              std::vector<float> heldPeriod;
+                              for( auto obs : ae )
+                              {
+                                 //std::cout << obs.first << " " << obs.second << std::endl;
+
+                                 if( obs.first > a + d + d * 0.5 && obs.first < holdFor && s > 0.05 ) // that 0.1 lets the delay ring off
+                                 {
+                                    REQUIRE( obs.second <  s * s * 1.02 );
+                                    heldPeriod.push_back(obs.second);
+                                 }
+                                 if( obs.first > a + d && obs.second < 1e-6 && zerot == 0 )
+                                    zerot = obs.first;
+                                 if( obs.first > holdFor + r && valAtRelEnd < 0 )
+                                    valAtRelEnd = obs.second;
+                                 
+                                 if( obs.second > maxv )
+                                 {
+                                    maxv = obs.second;
+                                    maxt = obs.first;
+                                 }
+                              }
+
+                              // In the held period are we mostly constant
+                              if( heldPeriod.size() > 10 )
+                              {
+                                 float sum = 0;
+                                 for( auto p : heldPeriod )
+                                    sum += p;
+                                 float mean = sum / heldPeriod.size();
+                                 float var = 0;
+                                 for( auto p : heldPeriod )
+                                    var += ( p - mean ) * ( p - mean );
+                                 var /= ( heldPeriod.size() - 1 );
+                                 float stddev = sqrt( var );
+                                 REQUIRE( stddev < d * 5e-3 );
+                              }
+                              REQUIRE( maxt < a );
+                              REQUIRE( maxv > 0.99 );
+                              if( s > 0.05 )
+                              {
+                                 REQUIRE( zerot > holdFor + r );
+                                 REQUIRE( valAtRelEnd < s * 0.025 );
+                              }
+                           };
+
+      testAnalog( 0.1, 0.2, 0.5, 0.1 );
+      testAnalog( 0.1, 0.2, 0.0, 0.1 );
+      for( int rc=0;rc<50; ++rc )
+      {
+         auto a = rand() * 1.0 / RAND_MAX + 0.01;
+         auto d = rand() * 1.0 / RAND_MAX + 0.01;
+         auto s = 0.7 * rand() * 1.0 / RAND_MAX + 0.2; // we have tested the s=0 case above
+         auto r = rand() * 1.0 / RAND_MAX;
+         testAnalog( a, d, s, r);
+      }
+
+   }
+
+
+   /*
+   ** This section recreates the somewhat painful SSE code in readable stuff
+   */
+   SECTION( "Clone the Analog" )
+   {
+      auto analogClone = [](float a_sec, float d_sec, float s, float r_sec, float releaseAfter, float runUntil )
+                            {
+                               float a = log(a_sec)/log(2.0);
+                               float d = log(d_sec)/log(2.0);
+                               float r = log(r_sec)/log(2.0);
+                               
+                               int i = 0;
+                               bool released = false;
+                               std::vector<std::pair<float,float>> res;
+                               res.push_back(std::make_pair(0.f, 0.f));
+
+                               auto v_c1 = 0.f;
+                               auto v_c1_delayed = 0.f;
+                               bool discharge = false;
+                               const float v_cc = 1.5f;
+                               
+                               while( true )
+                               {
+                                  auto t = 1.0 * (i+1) * BLOCK_SIZE * dsamplerate_inv;
+                                  i++;
+                                  if( t > runUntil || runUntil < 0 )
+                                     break;
+                           
+                                  if( t > releaseAfter && ! released )
+                                  {
+                                     released = true;
+                                  }
+
+                                  auto gate = !released;
+                                  auto v_gate = gate ? v_cc : 0.f;
+
+                                  // discharge = _mm_and_ps(_mm_or_ps(_mm_cmpgt_ss(v_c1_delayed, one), discharge), v_gate);
+                                  discharge = ( ( v_c1_delayed > 1 ) || discharge ) && gate;
+                                  v_c1_delayed = v_c1;
+
+                                  auto S = s * s;
+
+                                  auto v_attack = discharge ? 0 : v_gate;
+                                  auto v_decay = discharge ? S : v_cc;
+                                  auto v_release = v_gate;
+
+                                  auto diff_v_a = std::max( 0.f, v_attack  - v_c1 );
+                                  auto diff_v_d = std::min( 0.f, v_decay   - v_c1 );
+                                  auto diff_v_r = std::min( 0.f, v_release - v_c1 );
+
+                                  const float shortest = 6.f;
+                                  const float longest = -2.f;
+                                  const float coeff_offset = 2.f - log( samplerate / BLOCK_SIZE ) / log( 2.f );
+
+                                  float coef_A = pow( 2.f, std::min( 0.f, coeff_offset - a ) );
+                                  float coef_D = pow( 2.f, std::min( 0.f, coeff_offset - d ) );
+                                  float coef_R = pow( 2.f, std::min( 0.f, coeff_offset - r ) );
+
+                                  v_c1 = v_c1 + diff_v_a * coef_A;
+                                  v_c1 = v_c1 + diff_v_d * coef_D;
+                                  v_c1 = v_c1 + diff_v_r * coef_R;
+                                     
+                           
+                                  // adsr->process_block();
+                                  auto output = v_c1;
+                                  if( !gate && !discharge && v_c1 < 1e-6 )
+                                     output = 0;
+                                  
+                                  res.push_back( std::make_pair( (float)t, output ) );
+                               }
+                               return res;
+                            };
+
+      auto surgeA = runAdsr( 0.1, 0.2, 0.5, 0.1, 0, 0, 0, true, .5, 1.0 );
+      auto replA  = analogClone( 0.1, 0.2, 0.5, 0.1, 0.5, 1.0 );
+
+      REQUIRE( surgeA.size() == replA.size() );
+
+      for( auto i=0; i<surgeA.size(); ++i )
+      {
+         REQUIRE( replA[i].second == Approx( surgeA[i].second ).margin( 1e-6 ) );
+      }
+   }
+
+}
+
 
 
 int runAllTests(int argc, char **argv)
