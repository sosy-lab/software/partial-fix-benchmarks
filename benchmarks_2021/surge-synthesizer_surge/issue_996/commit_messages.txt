Support a few more wave files (#993)

None of the wave file metadata is well documented but
from folks giving me sample files I can figure out roughly
what the synths may intend in their own format and take good
guesses, here for a sample from Rapid and a sample from Hive
leading to supporting smpl with nloops = 0 -> 2048 and
a uhWT data block -> 2048
Prototype of displaying osc mods

I am not sure this is a good idea but want to keep this code in case
we change our mind, so tag it associated with #996 and keep the
branch up on github
Display OSC Mods with an animation

When modulating the OSC show an animation which gives you an indication
of the effect of that modulation as a separate blue line.

Closes #996
Display OSC Mods with an animation (#998)

When modulating the OSC show an animation which gives you an indication
of the effect of that modulation as a separate blue line.

Closes #996
Scene and Voice show in OSC modulation

The first version of OSC animation only used voice level modulations
not voice and scene, which is obviusly wrong.

Closes #996. Again.
Scene and Voice show in OSC modulation (#1003)

The first version of OSC animation only used voice level modulations
not voice and scene, which is obviusly wrong.

Closes #996. Again.
