zombie processes don't seem to be cleaned up properly
Sadly, 5539fd89be99bcec6d5eeb43c6123954ce59038e doesn't fix the problem - instead it introduces regression, which renders sway useless, since no processes can be launched, keyboard is being held hostage by sway, so that switching to tty is possible only after `SysRq + r`..

There also was some error message about not handling signal correctly after I killed sway from another tty.

Reverting 5539fd89be99bcec6d5eeb43c6123954ce59038e fixes the regression.

Please reopen.

Weird. It looked like a solid solution to me.

Looks like 18f4905e62e1fb2042abd79b2a4c756187e3d506 fixed it for me.

@ddevault SIGCHLD SIG_IGN is inherited through execve on Linux.
Which is one of the reasons why we don't set up a `SIGCHLD` handler.