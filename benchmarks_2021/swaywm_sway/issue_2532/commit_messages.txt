Merge pull request #2526 from ianyfan/commands

Fix moving container to inactive workspace on different output
Fix workspace tabs

When collecting focus to save into the transaction state, the workspace
needs to look in the tiling list only.

As seat_get_focus_inactive_tiling returns any descendant, the list also
needs to be traversed back up to the direct child of the workspace.

Fixes #2532
Fix workspace tabs

When collecting focus to save into the transaction state, the workspace
needs to look in the tiling list only.

As seat_get_focus_inactive_tiling returns any descendant, the list also
needs to be traversed back up to the direct child of the workspace.

Fixes #2532
