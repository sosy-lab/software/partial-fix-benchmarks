diff --git a/include/sway/commands.h b/include/sway/commands.h
index 3ed007635c..ab3eaf6310 100644
--- a/include/sway/commands.h
+++ b/include/sway/commands.h
@@ -261,6 +261,7 @@ sway_cmd output_cmd_enable;
 sway_cmd output_cmd_mode;
 sway_cmd output_cmd_position;
 sway_cmd output_cmd_scale;
+sway_cmd output_cmd_subpixel;
 sway_cmd output_cmd_transform;
 
 sway_cmd seat_cmd_attach;
diff --git a/include/sway/config.h b/include/sway/config.h
index 43ea777884..90950d3d82 100644
--- a/include/sway/config.h
+++ b/include/sway/config.h
@@ -171,6 +171,7 @@ struct output_config {
 	int x, y;
 	float scale;
 	int32_t transform;
+	enum wl_output_subpixel subpixel;
 
 	char *background;
 	char *background_option;
diff --git a/sway/commands/output.c b/sway/commands/output.c
index 40dbf3ca46..44e28512f2 100644
--- a/sway/commands/output.c
+++ b/sway/commands/output.c
@@ -18,6 +18,7 @@ static struct cmd_handler output_handlers[] = {
 	{ "res", output_cmd_mode },
 	{ "resolution", output_cmd_mode },
 	{ "scale", output_cmd_scale },
+	{ "subpixel", output_cmd_subpixel },
 	{ "transform", output_cmd_transform },
 };
 
diff --git a/sway/commands/output/subpixel.c b/sway/commands/output/subpixel.c
new file mode 100644
index 0000000000..7494182bb8
--- /dev/null
+++ b/sway/commands/output/subpixel.c
@@ -0,0 +1,36 @@
+#include <string.h>
+#include "sway/commands.h"
+#include "sway/config.h"
+#include "log.h"
+#include "sway/output.h"
+
+struct cmd_results *output_cmd_subpixel(int argc, char **argv) {
+	if (!config->handler_context.output_config) {
+		return cmd_results_new(CMD_FAILURE, "Missing output config");
+	}
+	if (!argc) {
+		return cmd_results_new(CMD_INVALID, "Missing subpixel argument.");
+	}
+	enum wl_output_subpixel subpixel;
+
+	if (strcmp(*argv, "rgb") == 0) {
+		subpixel = WL_OUTPUT_SUBPIXEL_HORIZONTAL_RGB;
+	} else if (strcmp(*argv, "bgr") == 0) {
+		subpixel = WL_OUTPUT_SUBPIXEL_HORIZONTAL_BGR;
+	} else if (strcmp(*argv, "vrgb") == 0) {
+		subpixel = WL_OUTPUT_SUBPIXEL_VERTICAL_RGB;
+	} else if (strcmp(*argv, "vbgr") == 0) {
+		subpixel = WL_OUTPUT_SUBPIXEL_VERTICAL_BGR;
+	} else if (strcmp(*argv, "none") == 0) {
+		subpixel = WL_OUTPUT_SUBPIXEL_NONE;
+	} else {
+		return cmd_results_new(CMD_INVALID, "Invalid output subpixel.");
+	}
+
+	struct output_config *output = config->handler_context.output_config;
+	config->handler_context.leftovers.argc = argc - 1;
+	config->handler_context.leftovers.argv = argv + 1;
+
+	output->subpixel = subpixel;
+	return NULL;
+}
diff --git a/sway/config/output.c b/sway/config/output.c
index 970764b03c..3bff697cf7 100644
--- a/sway/config/output.c
+++ b/sway/config/output.c
@@ -7,6 +7,7 @@
 #include <unistd.h>
 #include <wlr/types/wlr_output.h>
 #include <wlr/types/wlr_output_layout.h>
+#include <wlr/util/subpixel.h>
 #include "sway/config.h"
 #include "sway/output.h"
 #include "sway/tree/root.h"
@@ -42,6 +43,7 @@ struct output_config *new_output_config(const char *name) {
 	oc->x = oc->y = -1;
 	oc->scale = -1;
 	oc->transform = -1;
+	oc->subpixel = WL_OUTPUT_SUBPIXEL_UNKNOWN;
 	return oc;
 }
 
@@ -64,6 +66,9 @@ void merge_output_config(struct output_config *dst, struct output_config *src) {
 	if (src->scale != -1) {
 		dst->scale = src->scale;
 	}
+	if (src->subpixel != WL_OUTPUT_SUBPIXEL_UNKNOWN) {
+		dst->subpixel = src->subpixel;
+	}
 	if (src->refresh_rate != -1) {
 		dst->refresh_rate = src->refresh_rate;
 	}
@@ -129,10 +134,10 @@ struct output_config *store_output_config(struct output_config *oc) {
 	}
 
 	sway_log(SWAY_DEBUG, "Config stored for output %s (enabled: %d) (%dx%d@%fHz "
-		"position %d,%d scale %f transform %d) (bg %s %s) (dpms %d)",
+		"position %d,%d scale %f subpixel %s transform %d) (bg %s %s) (dpms %d)",
 		oc->name, oc->enabled, oc->width, oc->height, oc->refresh_rate,
-		oc->x, oc->y, oc->scale, oc->transform, oc->background,
-		oc->background_option, oc->dpms_state);
+		oc->x, oc->y, oc->scale, wlr_wl_output_subpixel_to_string(oc->subpixel),
+		oc->transform, oc->background, oc->background_option, oc->dpms_state);
 
 	return oc;
 }
@@ -225,6 +230,11 @@ bool apply_output_config(struct output_config *oc, struct sway_output *output) {
 		sway_log(SWAY_DEBUG, "Set %s scale to %f", oc->name, oc->scale);
 		wlr_output_set_scale(wlr_output, oc->scale);
 	}
+	if (oc && oc->subpixel != WL_OUTPUT_SUBPIXEL_UNKNOWN) {
+		sway_log(SWAY_DEBUG, "Set %s subpixel to %s", oc->name,
+			wlr_wl_output_subpixel_to_string(oc->subpixel));
+		wlr_output_set_subpixel(wlr_output, oc->subpixel);
+	}
 	if (oc && oc->transform >= 0) {
 		sway_log(SWAY_DEBUG, "Set %s transform to %d", oc->name, oc->transform);
 		wlr_output_set_transform(wlr_output, oc->transform);
@@ -293,6 +303,7 @@ static void default_output_config(struct output_config *oc,
 	}
 	oc->x = oc->y = -1;
 	oc->scale = 1;
+	oc->subpixel = WL_OUTPUT_SUBPIXEL_UNKNOWN;
 	oc->transform = WL_OUTPUT_TRANSFORM_NORMAL;
 }
 
diff --git a/sway/meson.build b/sway/meson.build
index 293a4ed203..918fc45648 100644
--- a/sway/meson.build
+++ b/sway/meson.build
@@ -168,6 +168,7 @@ sway_sources = files(
 	'commands/output/mode.c',
 	'commands/output/position.c',
 	'commands/output/scale.c',
+	'commands/output/subpixel.c',
 	'commands/output/transform.c',
 
 	'tree/arrange.c',
