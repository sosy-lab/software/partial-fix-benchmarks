diff --git a/include/sway/input/input-manager.h b/include/sway/input/input-manager.h
index 5107647d1e..c9bd08f0d6 100644
--- a/include/sway/input/input-manager.h
+++ b/include/sway/input/input-manager.h
@@ -44,9 +44,11 @@ void input_manager_configure_xcursor(void);
 
 void input_manager_apply_input_config(struct input_config *input_config);
 
+void input_manager_configure_all_inputs(void);
+
 void input_manager_reset_input(struct sway_input_device *input_device);
 
-void input_manager_reset_all_inputs();
+void input_manager_reset_all_inputs(void);
 
 void input_manager_apply_seat_config(struct seat_config *seat_config);
 
diff --git a/sway/config/output.c b/sway/config/output.c
index 713cd2191c..40e66b2a05 100644
--- a/sway/config/output.c
+++ b/sway/config/output.c
@@ -426,6 +426,9 @@ bool apply_output_config(struct output_config *oc, struct sway_output *output) {
 	}
 
 	if (oc && !oc->enabled) {
+		// Reconfigure all devices, since some config items (like map_to_output)
+		// are dependent on a specific output being online.
+		input_manager_configure_all_inputs();
 		return true;
 	}
 
@@ -478,17 +481,7 @@ bool apply_output_config(struct output_config *oc, struct sway_output *output) {
 		output->max_render_time = oc->max_render_time;
 	}
 
-	// Reconfigure all devices, since input config may have been applied before
-	// this output came online, and some config items (like map_to_output) are
-	// dependent on an output being present.
-	struct sway_input_device *input_device = NULL;
-	wl_list_for_each(input_device, &server.input->devices, link) {
-		struct sway_seat *seat = NULL;
-		wl_list_for_each(seat, &server.input->seats, link) {
-			seat_configure_device(seat, input_device);
-		}
-	}
-
+	input_manager_configure_all_inputs();
 	return true;
 }
 
diff --git a/sway/input/input-manager.c b/sway/input/input-manager.c
index b900f66682..5d401ecaac 100644
--- a/sway/input/input-manager.c
+++ b/sway/input/input-manager.c
@@ -542,6 +542,18 @@ void input_manager_apply_input_config(struct input_config *input_config) {
 	retranslate_keysyms(input_config);
 }
 
+void input_manager_configure_all_inputs(void) {
+	struct sway_input_device *input_device = NULL;
+	wl_list_for_each(input_device, &server.input->devices, link) {
+		sway_input_configure_libinput_device(input_device);
+
+		struct sway_seat *seat = NULL;
+		wl_list_for_each(seat, &server.input->seats, link) {
+			seat_configure_device(seat, input_device);
+		}
+	}
+}
+
 void input_manager_reset_input(struct sway_input_device *input_device) {
 	sway_input_reset_libinput_device(input_device);
 	struct sway_seat *seat = NULL;
@@ -550,7 +562,7 @@ void input_manager_reset_input(struct sway_input_device *input_device) {
 	}
 }
 
-void input_manager_reset_all_inputs() {
+void input_manager_reset_all_inputs(void) {
 	struct sway_input_device *input_device = NULL;
 	wl_list_for_each(input_device, &server.input->devices, link) {
 		input_manager_reset_input(input_device);
@@ -568,7 +580,6 @@ void input_manager_reset_all_inputs() {
 	}
 }
 
-
 void input_manager_apply_seat_config(struct seat_config *seat_config) {
 	sway_log(SWAY_DEBUG, "applying seat config for seat %s", seat_config->name);
 	if (strcmp(seat_config->name, "*") == 0) {
diff --git a/sway/input/libinput.c b/sway/input/libinput.c
index caaba5a193..4ec7288212 100644
--- a/sway/input/libinput.c
+++ b/sway/input/libinput.c
@@ -4,6 +4,7 @@
 #include <wlr/backend/libinput.h>
 #include "log.h"
 #include "sway/config.h"
+#include "sway/output.h"
 #include "sway/input/input-manager.h"
 #include "sway/ipc-server.h"
 
@@ -190,9 +191,24 @@ static bool config_libinput_pointer(struct libinput_device *device,
 	sway_log(SWAY_DEBUG, "config_libinput_pointer('%s' on  '%s')",
 			ic->identifier, device_id);
 	bool changed = false;
-	if (ic->send_events != INT_MIN) {
+
+	if (ic->mapped_to_output &&
+			!output_by_name_or_id(ic->mapped_to_output)) {
+		sway_log(SWAY_DEBUG,
+				"Pointer '%s' is mapped to offline output '%s'; disabling input",
+				ic->identifier, ic->mapped_to_output);
+		changed |= set_send_events(device,
+			LIBINPUT_CONFIG_SEND_EVENTS_DISABLED);
+	} else if (ic->send_events != INT_MIN) {
 		changed |= set_send_events(device, ic->send_events);
+	} else {
+		// Have to reset to the default mode here, otherwise if ic->send_events
+		// is unset and a mapped output just came online after being disabled,
+		// we'd remain stuck sending no events.
+		changed |= set_send_events(device,
+			libinput_device_config_send_events_get_default_mode(device));
 	}
+
 	if (ic->tap != INT_MIN) {
 		changed |= set_tap(device, ic->tap);
 	}
