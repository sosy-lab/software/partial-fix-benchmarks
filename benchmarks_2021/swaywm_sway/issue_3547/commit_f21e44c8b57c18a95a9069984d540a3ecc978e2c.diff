diff --git a/include/sway/tree/container.h b/include/sway/tree/container.h
index 8448d70595..bbe54f9c29 100644
--- a/include/sway/tree/container.h
+++ b/include/sway/tree/container.h
@@ -86,7 +86,10 @@ struct sway_container {
 	double x, y;
 	double width, height;
 	double saved_x, saved_y;
-	double saved_width, saved_height;
+
+	// The share of the space of parent container this container occupies
+	double width_fraction;
+	double height_fraction;
 
 	// These are in layout coordinates.
 	double content_x, content_y;
diff --git a/sway/commands/move.c b/sway/commands/move.c
index 6fd66f2856..58e01ec5be 100644
--- a/sway/commands/move.c
+++ b/sway/commands/move.c
@@ -206,7 +206,7 @@ static void container_move_to_workspace(struct sway_container *container,
 	} else {
 		container_detach(container);
 		container->width = container->height = 0;
-		container->saved_width = container->saved_height = 0;
+		container->width_fraction = container->height_fraction = 0;
 		workspace_add_tiling(workspace, container);
 		container_update_representation(container);
 	}
@@ -234,7 +234,7 @@ static void container_move_to_container(struct sway_container *container,
 	container_detach(container);
 	container_remove_gaps(container);
 	container->width = container->height = 0;
-	container->saved_width = container->saved_height = 0;
+	container->width_fraction = container->height_fraction = 0;
 
 	if (destination->view) {
 		container_add_sibling(destination, container, 1);
diff --git a/sway/tree/arrange.c b/sway/tree/arrange.c
index fc5d49ed0d..a870fd7510 100644
--- a/sway/tree/arrange.c
+++ b/sway/tree/arrange.c
@@ -18,39 +18,49 @@ static void apply_horiz_layout(list_t *children, struct wlr_box *parent) {
 		return;
 	}
 
-	// Count the number of new windows we are resizing
+	// Count the number of new windows we are resizing, and how much space is currently occupied
 	int new_children = 0;
+	double current_width_fraction = 0;
 	for (int i = 0; i < children->length; ++i) {
 		struct sway_container *child = children->items[i];
-		if (child->width <= 0) {
+		current_width_fraction += child->width_fraction;
+		if (child->width_fraction <= 0) {
 			new_children += 1;
 		}
 	}
 
-	// Calculate total width of children
-	double total_width = 0;
+	// Calculate each height fraction
+	double total_width_fraction = 0;
 	for (int i = 0; i < children->length; ++i) {
 		struct sway_container *child = children->items[i];
-		if (child->width <= 0) {
-			if (children->length > new_children) {
-				child->width = parent->width / (children->length - new_children);
+		if (child->width_fraction <= 0) {
+			if (current_width_fraction <= 0) {
+				child->width_fraction = 1.0;
 			} else {
-				child->width = parent->width;
+				if (children->length > new_children) {
+					child->width_fraction = current_width_fraction / (children->length - new_children);
+				} else {
+					child->width_fraction = current_width_fraction;
+				}
 			}
 		}
-		container_remove_gaps(child);
-		total_width += child->width;
+		total_width_fraction += child->width_fraction;
+	}
+	// Normalize width fractions so the sum is 1.0
+	for (int i = 0; i < children->length; ++i) {
+		struct sway_container *child = children->items[i];
+		child->width_fraction /= total_width_fraction;
 	}
-	double scale = parent->width / total_width;
 
 	// Resize windows
 	sway_log(SWAY_DEBUG, "Arranging %p horizontally", parent);
 	double child_x = parent->x;
 	for (int i = 0; i < children->length; ++i) {
 		struct sway_container *child = children->items[i];
+		container_remove_gaps(child);
 		child->x = child_x;
 		child->y = parent->y;
-		child->width = floor(child->width * scale);
+		child->width = floor(child->width_fraction * parent->width);
 		child->height = parent->height;
 		child_x += child->width;
 
@@ -67,40 +77,50 @@ static void apply_vert_layout(list_t *children, struct wlr_box *parent) {
 		return;
 	}
 
-	// Count the number of new windows we are resizing
+	// Count the number of new windows we are resizing, and how much space is currently occupied
 	int new_children = 0;
+	double current_height_fraction = 0;
 	for (int i = 0; i < children->length; ++i) {
 		struct sway_container *child = children->items[i];
-		if (child->height <= 0) {
+		current_height_fraction += child->height_fraction;
+		if (child->height_fraction <= 0) {
 			new_children += 1;
 		}
 	}
 
-	// Calculate total height of children
-	double total_height = 0;
+	// Calculate each height fraction
+	double total_height_fraction = 0;
 	for (int i = 0; i < children->length; ++i) {
 		struct sway_container *child = children->items[i];
-		if (child->height <= 0) {
-			if (children->length > new_children) {
-				child->height = parent->height / (children->length - new_children);
+		if (child->height_fraction <= 0) {
+			if (current_height_fraction <= 0) {
+				child->height_fraction = 1.0;
 			} else {
-				child->height = parent->height;
+				if (children->length > new_children) {
+					child->height_fraction = current_height_fraction / (children->length - new_children);
+				} else {
+					child->height_fraction = current_height_fraction;
+				}
 			}
 		}
-		container_remove_gaps(child);
-		total_height += child->height;
+		total_height_fraction += child->height_fraction;
+	}
+	// Normalize height fractions so the sum is 1.0
+	for (int i = 0; i < children->length; ++i) {
+		struct sway_container *child = children->items[i];
+		child->height_fraction /= total_height_fraction;
 	}
-	double scale = parent->height / total_height;
 
 	// Resize
 	sway_log(SWAY_DEBUG, "Arranging %p vertically", parent);
 	double child_y = parent->y;
 	for (int i = 0; i < children->length; ++i) {
 		struct sway_container *child = children->items[i];
+		container_remove_gaps(child);
 		child->x = parent->x;
 		child->y = child_y;
 		child->width = parent->width;
-		child->height = floor(child->height * scale);
+		child->height = floor(child->height_fraction * parent->height);
 		child_y += child->height;
 
 		// Make last child use remaining height of parent
diff --git a/sway/tree/container.c b/sway/tree/container.c
index 9046ae27de..a09ea5d989 100644
--- a/sway/tree/container.c
+++ b/sway/tree/container.c
@@ -962,8 +962,6 @@ static void container_fullscreen_workspace(struct sway_container *con) {
 
 	con->saved_x = con->x;
 	con->saved_y = con->y;
-	con->saved_width = con->width;
-	con->saved_height = con->height;
 
 	if (con->workspace) {
 		con->workspace->fullscreen = con;
@@ -994,8 +992,6 @@ static void container_fullscreen_global(struct sway_container *con) {
 	root->fullscreen_global = con;
 	con->saved_x = con->x;
 	con->saved_y = con->y;
-	con->saved_width = con->width;
-	con->saved_height = con->height;
 
 	struct sway_seat *seat;
 	wl_list_for_each(seat, &server.input->seats, link) {
@@ -1023,8 +1019,6 @@ void container_fullscreen_disable(struct sway_container *con) {
 		con->x = con->saved_x;
 		con->y = con->saved_y;
 	}
-	con->width = con->saved_width;
-	con->height = con->saved_height;
 
 	if (con->fullscreen_mode == FULLSCREEN_WORKSPACE) {
 		if (con->workspace) {
