diff --git a/include/sway/input/input-manager.h b/include/sway/input/input-manager.h
index e166a2377c..8d4a5b0058 100644
--- a/include/sway/input/input-manager.h
+++ b/include/sway/input/input-manager.h
@@ -62,4 +62,6 @@ struct input_config *input_device_get_config(struct sway_input_device *device);
 
 char *input_device_get_identifier(struct wlr_input_device *device);
 
+const char *input_device_get_type(struct sway_input_device *device);
+
 #endif
diff --git a/sway/config/input.c b/sway/config/input.c
index 63c28635b6..92b7d8dc63 100644
--- a/sway/config/input.c
+++ b/sway/config/input.c
@@ -148,6 +148,8 @@ struct input_config *store_input_config(struct input_config *ic) {
 		merge_wildcard_on_all(ic);
 	}
 
+	bool type_wildcard = strncmp(ic->identifier, "type:", 5) == 0;
+
 	int i = list_seq_find(config->input_configs, input_identifier_cmp,
 			ic->identifier);
 	if (i >= 0) {
@@ -156,7 +158,7 @@ struct input_config *store_input_config(struct input_config *ic) {
 		merge_input_config(current, ic);
 		free_input_config(ic);
 		ic = current;
-	} else if (!wildcard) {
+	} else if (!wildcard && !type_wildcard) {
 		sway_log(SWAY_DEBUG, "Adding non-wildcard input config");
 		i = list_seq_find(config->input_configs, input_identifier_cmp, "*");
 		if (i >= 0) {
@@ -170,10 +172,9 @@ struct input_config *store_input_config(struct input_config *ic) {
 		list_add(config->input_configs, ic);
 	} else {
 		// New wildcard config. Just add it
-		sway_log(SWAY_DEBUG, "Adding input * config");
+		sway_log(SWAY_DEBUG, "Adding input %s config", ic->identifier);
 		list_add(config->input_configs, ic);
 	}
-
 	sway_log(SWAY_DEBUG, "Config stored for input %s", ic->identifier);
 
 	return ic;
diff --git a/sway/input/input-manager.c b/sway/input/input-manager.c
index adb36af937..348e2b0e8c 100644
--- a/sway/input/input-manager.c
+++ b/sway/input/input-manager.c
@@ -71,6 +71,40 @@ char *input_device_get_identifier(struct wlr_input_device *device) {
 	return identifier;
 }
 
+static bool device_is_touchpad(struct sway_input_device *device) {
+	if (device->wlr_device->type != WLR_INPUT_DEVICE_POINTER ||
+			!wlr_input_device_is_libinput(device->wlr_device)) {
+		return false;
+	}
+
+	struct libinput_device *libinput_device =
+		wlr_libinput_get_device_handle(device->wlr_device);
+
+	return libinput_device_config_tap_get_finger_count(libinput_device) > 0;
+}
+
+const char *input_device_get_type(struct sway_input_device *device) {
+	switch (device->wlr_device->type) {
+	case WLR_INPUT_DEVICE_POINTER:
+		if (device_is_touchpad(device)) {
+			return "touchpad";
+		} else {
+			return "pointer";
+		}
+	case WLR_INPUT_DEVICE_KEYBOARD:
+		return "keyboard";
+	case WLR_INPUT_DEVICE_TOUCH:
+		return "touch";
+	case WLR_INPUT_DEVICE_TABLET_TOOL:
+		return "tablet_tool";
+	case WLR_INPUT_DEVICE_TABLET_PAD:
+		return "tablet_pad";
+	case WLR_INPUT_DEVICE_SWITCH:
+		return "switch";
+	}
+	return "unknown";
+}
+
 static struct sway_input_device *input_sway_device_from_wlr(
 		struct wlr_input_device *device) {
 	struct sway_input_device *input_device = NULL;
@@ -785,15 +819,25 @@ void input_manager_configure_xcursor(void) {
 
 struct input_config *input_device_get_config(struct sway_input_device *device) {
 	struct input_config *wildcard_config = NULL;
+	struct input_config *type_config = NULL;
 	struct input_config *input_config = NULL;
 	for (int i = 0; i < config->input_configs->length; ++i) {
 		input_config = config->input_configs->items[i];
 		if (strcmp(input_config->identifier, device->identifier) == 0) {
 			return input_config;
+		} else if (strncmp(input_config->identifier, "type:", 5) == 0) {
+			if (strcmp(input_config->identifier + 5,
+						input_device_get_type(device)) == 0) {
+				type_config = input_config;
+			}
 		} else if (strcmp(input_config->identifier, "*") == 0) {
 			wildcard_config = input_config;
 		}
 	}
 
+	if (type_config != NULL) {
+		sway_log(SWAY_DEBUG, "Using type wildcard for %s", device->identifier);
+		return type_config;
+	}
 	return wildcard_config;
 }
diff --git a/sway/ipc-json.c b/sway/ipc-json.c
index c27068b76b..4ccf6dfd35 100644
--- a/sway/ipc-json.c
+++ b/sway/ipc-json.c
@@ -85,27 +85,6 @@ static const char *ipc_json_output_transform_description(enum wl_output_transfor
 	return NULL;
 }
 
-static const char *ipc_json_device_type_description(struct sway_input_device *device) {
-	switch (device->wlr_device->type) {
-	case WLR_INPUT_DEVICE_POINTER:
-		if (device->wlr_device->pointer->is_touchpad) {
-			return "touchpad";
-		}
-		return "pointer";
-	case WLR_INPUT_DEVICE_KEYBOARD:
-		return "keyboard";
-	case WLR_INPUT_DEVICE_SWITCH:
-		return "switch";
-	case WLR_INPUT_DEVICE_TOUCH:
-		return "touch";
-	case WLR_INPUT_DEVICE_TABLET_TOOL:
-		return "tablet_tool";
-	case WLR_INPUT_DEVICE_TABLET_PAD:
-		return "tablet_pad";
-	}
-	return "unknown";
-}
-
 json_object *ipc_json_get_version(void) {
 	int major = 0, minor = 0, patch = 0;
 	json_object *version = json_object_new_object();
@@ -822,7 +801,7 @@ json_object *ipc_json_describe_input(struct sway_input_device *device) {
 		json_object_new_int(device->wlr_device->product));
 	json_object_object_add(object, "type",
 		json_object_new_string(
-			ipc_json_device_type_description(device)));
+			input_device_get_type(device)));
 
 	if (device->wlr_device->type == WLR_INPUT_DEVICE_KEYBOARD) {
 		struct wlr_keyboard *keyboard = device->wlr_device->keyboard;
