Cursor gets hidden even it is currently being used with a drawing tablet (wacom)
I'm facing the exact same problem here on sway 1.4. Relevant config lines are:
```
seat * hide_cursor 2000
input 1386:890:Wacom_One_by_Wacom_S_Pen map_from_region .0x.1 1x1
```