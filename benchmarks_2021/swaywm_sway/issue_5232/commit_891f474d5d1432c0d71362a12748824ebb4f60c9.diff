diff --git a/include/sway/input/cursor.h b/include/sway/input/cursor.h
index 35738894b0..83469a3edd 100644
--- a/include/sway/input/cursor.h
+++ b/include/sway/input/cursor.h
@@ -84,6 +84,7 @@ struct sway_cursor *sway_cursor_create(struct sway_seat *seat);
  */
 void cursor_rebase(struct sway_cursor *cursor);
 void cursor_rebase_all(void);
+void cursor_update_image(struct sway_cursor *cursor, struct sway_node *node);
 
 void cursor_handle_activity(struct sway_cursor *cursor,
 	enum sway_input_idle_source idle_source);
diff --git a/include/sway/input/seat.h b/include/sway/input/seat.h
index 6d7495dd02..ad7cac7541 100644
--- a/include/sway/input/seat.h
+++ b/include/sway/input/seat.h
@@ -220,6 +220,9 @@ bool seat_is_input_allowed(struct sway_seat *seat, struct wlr_surface *surface);
 
 void drag_icon_update_position(struct sway_drag_icon *icon);
 
+enum wlr_edges find_resize_edge(struct sway_container *cont,
+		struct wlr_surface *surface, struct sway_cursor *cursor);
+
 void seatop_begin_default(struct sway_seat *seat);
 
 void seatop_begin_down(struct sway_seat *seat, struct sway_container *con,
diff --git a/sway/input/cursor.c b/sway/input/cursor.c
index 0649f4688c..de3af0de63 100644
--- a/sway/input/cursor.c
+++ b/sway/input/cursor.c
@@ -212,6 +212,27 @@ void cursor_rebase_all(void) {
 	}
 }
 
+void cursor_update_image(struct sway_cursor *cursor,
+		struct sway_node *node) {
+	if (node && node->type == N_CONTAINER) {
+		// Try a node's resize edge
+		enum wlr_edges edge = find_resize_edge(node->sway_container, NULL, cursor);
+		if (edge == WLR_EDGE_NONE) {
+			cursor_set_image(cursor, "left_ptr", NULL);
+		} else if (container_is_floating(node->sway_container)) {
+			cursor_set_image(cursor, wlr_xcursor_get_resize_name(edge), NULL);
+		} else {
+			if (edge & (WLR_EDGE_LEFT | WLR_EDGE_RIGHT)) {
+				cursor_set_image(cursor, "col-resize", NULL);
+			} else {
+				cursor_set_image(cursor, "row-resize", NULL);
+			}
+		}
+	} else {
+		cursor_set_image(cursor, "left_ptr", NULL);
+	}
+}
+
 static void cursor_hide(struct sway_cursor *cursor) {
 	wlr_cursor_set_image(cursor->cursor, NULL, 0, 0, 0, 0, 0, 0);
 	cursor->hidden = true;
diff --git a/sway/input/seatop_default.c b/sway/input/seatop_default.c
index d20048d58e..9280ffe150 100644
--- a/sway/input/seatop_default.c
+++ b/sway/input/seatop_default.c
@@ -91,7 +91,7 @@ static enum wlr_edges find_edge(struct sway_container *cont,
  * If the cursor is over a _resizable_ edge, return the edge.
  * Edges that can't be resized are edges of the workspace.
  */
-static enum wlr_edges find_resize_edge(struct sway_container *cont,
+enum wlr_edges find_resize_edge(struct sway_container *cont,
 		struct wlr_surface *surface, struct sway_cursor *cursor) {
 	enum wlr_edges edge = find_edge(cont, surface, cursor);
 	if (edge && !container_is_floating(cont) && edge_is_external(cont, edge)) {
@@ -192,37 +192,6 @@ static void state_add_button(struct seatop_default_event *e, uint32_t button) {
 	e->pressed_button_count++;
 }
 
-static void cursor_do_rebase(struct sway_cursor *cursor, uint32_t time_msec,
-		struct sway_node *node, struct wlr_surface *surface,
-		double sx, double sy) {
-	struct wlr_seat *wlr_seat = cursor->seat->wlr_seat;
-	if (surface) {
-		if (seat_is_input_allowed(cursor->seat, surface)) {
-			wlr_seat_pointer_notify_enter(wlr_seat, surface, sx, sy);
-		}
-	} else if (node && node->type == N_CONTAINER) {
-		// Try a node's resize edge
-		enum wlr_edges edge = find_resize_edge(node->sway_container, surface, cursor);
-		if (edge == WLR_EDGE_NONE) {
-			cursor_set_image(cursor, "left_ptr", NULL);
-		} else if (container_is_floating(node->sway_container)) {
-			cursor_set_image(cursor, wlr_xcursor_get_resize_name(edge), NULL);
-		} else {
-			if (edge & (WLR_EDGE_LEFT | WLR_EDGE_RIGHT)) {
-				cursor_set_image(cursor, "col-resize", NULL);
-			} else {
-				cursor_set_image(cursor, "row-resize", NULL);
-			}
-		}
-	} else {
-		cursor_set_image(cursor, "left_ptr", NULL);
-	}
-
-	if (surface == NULL) {
-		wlr_seat_pointer_clear_focus(wlr_seat);
-	}
-}
-
 /*----------------------------------\
  * Functions used by handle_button  /
  *--------------------------------*/
@@ -482,9 +451,14 @@ static void handle_motion(struct sway_seat *seat, uint32_t time_msec,
 		check_focus_follows_mouse(seat, e, node);
 	}
 
-	cursor_do_rebase(cursor, time_msec, node, surface, sx, sy);
-	if (surface && seat_is_input_allowed(cursor->seat, surface)) {
-		wlr_seat_pointer_notify_motion(seat->wlr_seat, time_msec, sx, sy);
+	if (surface) {
+		if (seat_is_input_allowed(seat, surface)) {
+			wlr_seat_pointer_notify_enter(seat->wlr_seat, surface, sx, sy);
+			wlr_seat_pointer_notify_motion(seat->wlr_seat, time_msec, sx, sy);
+		}
+	} else {
+		cursor_update_image(cursor, node);
+		wlr_seat_pointer_clear_focus(seat->wlr_seat);
 	}
 
 	struct sway_drag_icon *drag_icon;
@@ -621,7 +595,15 @@ static void handle_rebase(struct sway_seat *seat, uint32_t time_msec) {
 	double sx = 0.0, sy = 0.0;
 	e->previous_node = node_at_coords(seat,
 			cursor->cursor->x, cursor->cursor->y, &surface, &sx, &sy);
-	cursor_do_rebase(cursor, time_msec, e->previous_node, surface, sx, sy);
+
+	if (surface) {
+		if (seat_is_input_allowed(seat, surface)) {
+			wlr_seat_pointer_notify_enter(seat->wlr_seat, surface, sx, sy);
+		}
+	} else {
+		cursor_update_image(cursor, e->previous_node);
+		wlr_seat_pointer_clear_focus(seat->wlr_seat);
+	}
 }
 
 static const struct sway_seatop_impl seatop_impl = {
