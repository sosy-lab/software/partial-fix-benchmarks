diff --git a/include/sway/input/seat.h b/include/sway/input/seat.h
index ad7cac7541..bf7e571799 100644
--- a/include/sway/input/seat.h
+++ b/include/sway/input/seat.h
@@ -7,6 +7,7 @@
 #include <wlr/util/edges.h>
 #include "sway/config.h"
 #include "sway/input/input-manager.h"
+#include "sway/input/tablet.h"
 #include "sway/input/text_input.h"
 
 struct sway_seat;
@@ -19,6 +20,8 @@ struct sway_seatop_impl {
 			double dx, double dy);
 	void (*axis)(struct sway_seat *seat, struct wlr_event_pointer_axis *event);
 	void (*rebase)(struct sway_seat *seat, uint32_t time_msec);
+	void (*tablet_tool_motion)(struct sway_seat *seat, struct sway_tablet *tablet,
+			struct wlr_tablet_tool *tool, uint32_t time_msec, double dx, double dy);
 	void (*end)(struct sway_seat *seat);
 	void (*unref)(struct sway_seat *seat, struct sway_container *con);
 	void (*render)(struct sway_seat *seat, struct sway_output *output,
@@ -261,6 +264,10 @@ void seatop_button(struct sway_seat *seat, uint32_t time_msec,
 void seatop_motion(struct sway_seat *seat, uint32_t time_msec,
 		double dx, double dy);
 
+void seatop_tablet_tool_motion(struct sway_seat *seat,
+		struct sway_tablet *tablet, struct wlr_tablet_tool *tool, uint32_t time_msec,
+		double dx, double dy);
+
 void seatop_axis(struct sway_seat *seat, struct wlr_event_pointer_axis *event);
 
 void seatop_rebase(struct sway_seat *seat, uint32_t time_msec);
diff --git a/sway/input/cursor.c b/sway/input/cursor.c
index f07685731f..743abb44d1 100644
--- a/sway/input/cursor.c
+++ b/sway/input/cursor.c
@@ -541,25 +541,17 @@ static void handle_tablet_tool_position(struct sway_cursor *cursor,
 	double sx, sy;
 	struct wlr_surface *surface = NULL;
 	struct sway_seat *seat = cursor->seat;
-	struct sway_node *focused_node = node_at_coords(seat, cursor->cursor->x,
-		cursor->cursor->y, &surface, &sx, &sy);
+	node_at_coords(seat, cursor->cursor->x, cursor->cursor->y, &surface, &sx, &sy);
 	struct sway_tablet_tool *sway_tool = tool->data;
 
-	if (!surface || !wlr_surface_accepts_tablet_v2(tablet->tablet_v2, surface)) {
+	if (surface && wlr_surface_accepts_tablet_v2(tablet->tablet_v2, surface)) {
+		seatop_tablet_tool_motion(seat, tablet, tool, time_msec, dx, dy);
+	} else {
 		wlr_tablet_v2_tablet_tool_notify_proximity_out(sway_tool->tablet_v2_tool);
 		pointer_motion(cursor, time_msec, input_device->wlr_device, dx, dy, dx, dy);
-		transaction_commit_dirty();
-		return;
-	}
-
-	wlr_tablet_v2_tablet_tool_notify_proximity_in(sway_tool->tablet_v2_tool,
-		tablet->tablet_v2, surface);
-
-	if (focused_node && config->focus_follows_mouse != FOLLOWS_NO) {
-		seat_set_focus(seat, focused_node);
 	}
 
-	wlr_tablet_v2_tablet_tool_notify_motion(sway_tool->tablet_v2_tool, sx, sy);
+	transaction_commit_dirty();
 }
 
 static void handle_tool_axis(struct wl_listener *listener, void *data) {
diff --git a/sway/input/seat.c b/sway/input/seat.c
index 3659380603..cd3ba9a6bf 100644
--- a/sway/input/seat.c
+++ b/sway/input/seat.c
@@ -1458,6 +1458,16 @@ void seatop_axis(struct sway_seat *seat, struct wlr_event_pointer_axis *event) {
 	}
 }
 
+void seatop_tablet_tool_motion(struct sway_seat *seat,
+		struct sway_tablet *tablet, struct wlr_tablet_tool *tool,
+		uint32_t time_msec, double dx, double dy) {
+	if (seat->seatop_impl->tablet_tool_motion) {
+		seat->seatop_impl->tablet_tool_motion(seat, tablet, tool, time_msec, dx, dy);
+	} else {
+		seatop_motion(seat, time_msec, dx, dy);
+	}
+}
+
 void seatop_rebase(struct sway_seat *seat, uint32_t time_msec) {
 	if (seat->seatop_impl->rebase) {
 		seat->seatop_impl->rebase(seat, time_msec);
diff --git a/sway/input/seatop_default.c b/sway/input/seatop_default.c
index 9280ffe150..edbf8e0282 100644
--- a/sway/input/seatop_default.c
+++ b/sway/input/seatop_default.c
@@ -2,9 +2,11 @@
 #include <float.h>
 #include <libevdev/libevdev.h>
 #include <wlr/types/wlr_cursor.h>
+#include <wlr/types/wlr_tablet_v2.h>
 #include <wlr/types/wlr_xcursor_manager.h>
 #include "sway/input/cursor.h"
 #include "sway/input/seat.h"
+#include "sway/input/tablet.h"
 #include "sway/tree/view.h"
 #include "log.h"
 #if HAVE_XWAYLAND
@@ -471,6 +473,43 @@ static void handle_motion(struct sway_seat *seat, uint32_t time_msec,
 	e->previous_node = node;
 }
 
+static void handle_tablet_tool_motion(struct sway_seat *seat,
+		struct sway_tablet *tablet, struct wlr_tablet_tool *tool,
+		uint32_t time_msec, double dx, double dy) {
+	struct seatop_default_event *e = seat->seatop_data;
+	struct sway_cursor *cursor = seat->cursor;
+
+	struct wlr_surface *surface = NULL;
+	double sx, sy;
+	struct sway_node *node = node_at_coords(seat,
+			cursor->cursor->x, cursor->cursor->y, &surface, &sx, &sy);
+
+	if (node && config->focus_follows_mouse != FOLLOWS_NO) {
+		check_focus_follows_mouse(seat, e, node);
+	}
+
+	if (surface) {
+		if (seat_is_input_allowed(seat, surface)) {
+			struct sway_tablet_tool *sway_tool = tool->data;
+			wlr_tablet_v2_tablet_tool_notify_proximity_in(sway_tool->tablet_v2_tool,
+				tablet->tablet_v2, surface);
+			wlr_tablet_v2_tablet_tool_notify_motion(sway_tool->tablet_v2_tool, sx,
+				sy);
+		}
+	} else {
+		cursor_update_image(cursor, node);
+	}
+
+	struct sway_drag_icon *drag_icon;
+	wl_list_for_each(drag_icon, &root->drag_icons, link) {
+		if (drag_icon->seat == seat) {
+			drag_icon_update_position(drag_icon);
+		}
+	}
+
+	e->previous_node = node;
+}
+
 /*--------------------------------\
  * Functions used by handle_axis  /
  *------------------------------*/
@@ -610,6 +649,7 @@ static const struct sway_seatop_impl seatop_impl = {
 	.button = handle_button,
 	.motion = handle_motion,
 	.axis = handle_axis,
+	.tablet_tool_motion = handle_tablet_tool_motion,
 	.rebase = handle_rebase,
 	.allow_set_cursor = true,
 };
