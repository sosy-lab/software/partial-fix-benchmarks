ipc: invert output transformation when necessary

After swaywm/wlroots#2023, #4996 inverted configuration transformations.
For consistency, we should undo (double-apply) the inversion when
communicating via IPC.

Closes #5356.
tree/view: fix smart gaps when ancestor container is tabbed or stacked

Fixes #5406.
tree/view: fix smart gaps when ancestor container is tabbed or stacked

Fixes #5406.
tree/view: fix smart gaps when ancestor container is tabbed or stacked

Fixes #5406.
tree/view: fix smart gaps when ancestor container is tabbed or stacked

Fixes #5406.
tree/view: fix smart gaps when ancestor container is tabbed or stacked

Fixes #5406.
tree/view: fix smart gaps when ancestor container is tabbed or stacked

Fixes #5406.
