DPMS
I'd like to see a protocol extension written for this, and then a seperate daemon to manage it.

Hmm interesting. That sounds like a better idea. I'll need to look into protocol extensions over the weekend and report back

https://github.com/SirCmpwn/sway/tree/master/protocols

They're pretty straightfoward. Write an XML file describing the interface, add it to CMakeLists.txt, and you'll end up with a header to include.

See also https://github.com/SirCmpwn/sway/blob/master/sway/extensions.c

Thanks for the tip

any news on this?

Nope.

While orbment's core-dpms implements this using own timer and wlc's set_sleep. I think drm has dpms api too actually.
Looking at DRM docs, there is an api for setting dpms on drm_connector, but no way of getting current state. Would using wlc set_sleep approach be preferable?
The question is how to make it possible? 

For now I can advice to switch to tty(n) and it works by default - screen is going off by some time. Why I can't do this in wayland? 

My tty(n) is also in KMS mode (like wayland)... 

So how tty(n) can turn screen off, but wayland can't - if them both in KMS mode? 

What do you think?
I think the situation around this could/should/will change very soon due to #1076 
I see on the status issue for for wlroots ([wlroots/#9](https://github.com/swaywm/wlroots/issues/9)) that DPMS is checked off. I also see that DPMS is an unaddressed bounty problem as well. In a general, overview type answer, given the support in wlroots what needs to happen to bring this to Sway? This question is coming from someone not too familiar with the project or Wayland compositors. If it's not too far beyond my skill set, I wouldn't mind working on this as I am able.
It's not currently a priority in the wlroots port. Let's swing back to it later.