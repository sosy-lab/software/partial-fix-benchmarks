Merge pull request #1298 from emersion/output-dangling-idle-frame

output: remove idle_frame event source when destroying output
backend/drm: don't reset conn->pageflip_pending in drm_connector_cleanup

If a pageflip is pending before cleanup, it's still pending after. This
is used line 1177: drm_connector_cleanup is called and
conn->pageflip_pending is called afterwards.

Fixes #1297
backend/drm: don't reset conn->pageflip_pending in drm_connector_cleanup

If a pageflip is pending before cleanup, it's still pending after. This
is used line 1177: drm_connector_cleanup is called and
conn->pageflip_pending is checked afterwards.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
backend: drm: switch to pageflip_handler_2

atomic and legacy now both pass the backend as the user data for the
pageflip event. We than retrieve the correct connector by matching on
the crtc_id passed to the page_flip_handler2.

Wlroots also requires the DRM_CRTC_IN_VBLANK_EVENT capability now.

Fixes #1297
