Segfault in `drm_connector_set_mode`
Can you compile manually to disable optimizations?

Compiling with ASan (`meson build/ -Db_sanitize=address`) may help too.
Sure!

```
#8  0x00007fb065a0098f in drm_connector_set_mode (output=0x61800000ac80, wlr_mode=0x0) at ../backend/drm/drm.c:653
        conn = 0x61800000ac80
        drm = 0x613000004800
        mode = 0x62300005f900
#9  0x00007fb0659facdf in session_signal (listener=0x613000004890, data=0x614000000040) at ../backend/drm/backend.c:96
        plane = 0x611000002fc0
        conn = 0x61800000ac80
        drm = 0x613000004800
        session = 0x614000000040
#10 0x00007fb065ad89af in wlr_signal_emit_safe (signal=0x614000000048, data=0x614000000040) at ../util/signal.c:29
        pos = 0x613000004890
        l = 0x613000004890
        cursor = {link = {prev = 0x613000004890, next = 0x7ffcb97b4490}, notify = 0x7fb065ad8768 <handle_noop>}
        end = {link = {prev = 0x7ffcb97b4450, next = 0x614000000048}, notify = 0x7fb065ad8768 <handle_noop>}
#11 0x00007fb065a32ee7 in resume_device (msg=0x61800004e880, userdata=0x614000000040, ret_error=0x7ffcb97b4630) at ../backend/session/logind.c:323
        dev = 0x604000001910
        session = 0x614000000040
        ret = 1
        fd = 30
        major = 226
        minor = 0
#12 0x00007fb064c12642 in  () at /usr/lib/libsystemd.so.0
#13 0x00007fb064c12184 in  () at /usr/lib/libsystemd.so.0
#14 0x00007fb064c1215d in  () at /usr/lib/libsystemd.so.0
#15 0x00007fb064c12184 in  () at /usr/lib/libsystemd.so.0
#16 0x00007fb064c1215d in  () at /usr/lib/libsystemd.so.0
#17 0x00007fb064c12184 in  () at /usr/lib/libsystemd.so.0
#18 0x00007fb064c1215d in  () at /usr/lib/libsystemd.so.0
#19 0x00007fb064c12184 in  () at /usr/lib/libsystemd.so.0
#20 0x00007fb064c1215d in  () at /usr/lib/libsystemd.so.0
#21 0x00007fb064c12184 in  () at /usr/lib/libsystemd.so.0
#22 0x00007fb064c122da in  () at /usr/lib/libsystemd.so.0
#23 0x00007fb064c124de in  () at /usr/lib/libsystemd.so.0
#24 0x00007fb064c41b88 in  () at /usr/lib/libsystemd.so.0
#25 0x00007fb064c4ade3 in  () at /usr/lib/libsystemd.so.0
#26 0x00007fb065a33e7a in dbus_event (fd=4, mask=1, data=0x61c000000080) at ../backend/session/logind.c:485
        bus = 0x61c000000080
#27 0x00007fb065b8b7f2 in wl_event_loop_dispatch () at /usr/lib/libwayland-server.so.0
#28 0x00007fb065b8a39c in wl_display_run () at /usr/lib/libwayland-server.so.0
#29 0x0000563f6cc49583 in main (argc=1, argv=0x7ffcb97b52f8) at ../sway/sway/main.c:402
        verbose = 0
        debug = 0
        validate = 0
        allow_unsupported_gpu = 0
        long_options =
            {{name = 0x563f6cc914db "help", has_arg = 0, flag = 0x0, val = 104}, {name = 0x563f6cc94415 "config", has_arg = 1, flag = 0x0, val = 99}, {name = 0x563f6cc914e0 "validate", has_arg = 0, flag = 0x0, val = 67}, {name = 0x563f6cc914e9 "debug", has_arg = 0, flag = 0x0, val = 100}, {name = 0x563f6cc9143f "version", has_arg = 0, flag = 0x0, val = 118}, {name = 0x563f6cc905df "verbose", has_arg = 0, flag = 0x0, val = 86}, {name = 0x563f6cc914ef "get-socketpath", has_arg = 0, flag = 0x0, val = 112}, {name = 0x563f6cc914fe "unsupported-gpu", has_arg = 0, flag = 0x0, val = 117}, {name = 0x563f6cc9150e "my-next-gpu-wont-be-nvidia", has_arg = 0, flag = 0x0, val = 117}, {name = 0x0, has_arg = 0, flag = 0x0, val = 0}}
        config_path = 0x0
        usage = 0x563f6cc91840 "Usage: sway [options] [command]\n\n  -h, --help", ' ' <repeats 13 times>, "Show help message and quit.\n  -c, --config <config>  Specify a config file.\n  -C,--validate         Check the validity of the config file, th"...
        c = <optimized out>
```


@emersion & @ddevault  this issue does not seem to be fixed. It still occurs if I switch between terminal 2 and back.
@emersion I'm sure it's not the correct solution, but adding a `wlr_output_update_enabled(&conn->output, false)` to https://github.com/swaywm/wlroots/blob/master/backend/drm/drm.c#L660 at least fixes the crash on my machine.

When I start sway with this I still have the second external monitor turn on and display essentially garbage (the last terminal output), but then switching back and forth between virtual terminals stops sending output to the second external monitor (the way it probably should be).

I'm thinking there's an issue with properly cleaning up this input when there's a failure in `drm_connector_init_renderer` (and probably also a bug causing that to fail, but even if it does it should be cleaned up).
Can you try https://github.com/swaywm/wlroots/pull/1971?
@emersion I can confirm that the segfault issue is fixed. This still an issue with the second monitor not starting correctly, and a difference in what that monitor shows depending if it's plugged in to start with or after starting sway, but no crashes either way. Thank you!