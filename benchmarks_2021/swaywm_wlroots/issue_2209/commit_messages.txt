render/egl: introduce wlr_egl_unset_current

This function can be called after wlr_egl_make_current to cleanup the
EGL context. This avoids having lingering EGL contexts that make things
work by chance.

Closes: https://github.com/swaywm/wlroots/issues/2197
backend/drm: fix current EGL context on multi-GPU

get_tex_for_bo changes the current EGL context. Rendering operations
must immediately follow drm_surface_make_current.

Closes: https://github.com/swaywm/wlroots/issues/2209
backend/drm: fix current EGL context on multi-GPU

get_tex_for_bo changes the current EGL context. Rendering operations
must immediately follow drm_surface_make_current.

Closes: https://github.com/swaywm/wlroots/issues/2209
backend/drm: don't unset EGL context before swapping buffers

drm_fb_lock_surface swaps buffers. We need to make sure that the EGL
context is current before calling wlr_egl_swap_buffers.

Move the wlr_egl_unset_current call into drm_fb_lock_surface to avoid
other surprises like this.

Fixes: d28a7da95d1a ("backend/drm: add missing wlr_egl_unset_current")
Closes: https://github.com/swaywm/wlroots/issues/2209
