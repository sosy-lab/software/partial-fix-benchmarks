Segfault with latest egl context change
Can you try https://github.com/swaywm/wlroots/pull/2210?
That is hanging. I'll try to get info from log.
Log attached.
[sway.log](https://github.com/swaywm/wlroots/files/4652162/sway.log)

```
00:00:02.470 [ERROR] [EGL] command: eglSwapBuffers, error: 0x300d, message: "eglSwapBuffers"
00:00:02.470 [ERROR] [render/egl.c:449] eglSwapBuffers failed
00:00:02.470 [ERROR] [backend/drm/renderer.c:276] Failed to swap buffers
00:00:02.470 [ERROR] [backend/drm/drm.c:727] Failed to initialize renderer on connector 'eDP-1': initial page-flip failed
00:00:02.470 [ERROR] [backend/drm/drm.c:816] Failed to initialize renderer for plane
00:00:02.471 [sway/config/output.c:423] Failed to commit output eDP-1
```
On exit, I get:
`[backend/drm/atomic.c:36] eDP-1: Atomic commit failed (modset): Permission deniedl page-flip failed`
Abort fixed, but there's still this `eglSwapBuffers` failure to address.
I'm seeing the `eglSwapBuffers` failure and hang on latest master as well. Using a single GPU (Intel Haswell integrated graphics), kernel 5.6.13. Reverting commit d28a7da95d1a ("backend/drm: add missing wlr_egl_unset_current") gets rid of the problem for me.
Can you try https://github.com/swaywm/wlroots/pull/2212?
> Can you try #2212?

It's fixed here with #2212.
I've actually come up with a Better(tm) solution, can you try https://github.com/swaywm/wlroots/pull/2213?
I just ran into this as well. I didn't try #2212, but I can report that #2213 fixes the issue for me.
Same issue on my end, log has similar failings and #2213 fixes it. Dual GPU setup (R9 Fury X + RX 5700 XT - both AMDGPU), Gentoo. Wanted to leave a comment so that there's confirmation from more than one person that #2213 fixes it. 
Another confirmation: #2213 fixes the issue for me.