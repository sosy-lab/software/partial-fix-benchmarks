backend/x11: cursor hidden causes issues when debugging crash with gdb
@emersion mentioned that as a first pass we could XDefineCursor with a fully transparent cursor instead of hiding the cursor outright. That way the cursor is still there, and when not over the window should return to the "regular" cursor.

A second pass could be to try to hook up the full pipeline into XRenderCreateCursor. It's unclear whether this can work directly with DRI3 Pixmaps or even with a Window + Present, or if we just need to read the data out "by hand".

Worth noting that in practice GPUs support only a very limited quantity of cursor sizes, not sure what form the cursor textures come in.