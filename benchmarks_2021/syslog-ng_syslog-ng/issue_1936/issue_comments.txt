SSL "session id context uninitialized" sending logs from Windows with client certs
can you pls check if this experimental patch fixes it?

https://github.com/balabit/syslog-ng/tree/ssl-session-id-context

That still needs some refinement, e.g. to make sure it compiles on older OpenSSL versions.
@bazsi as I have checked the CHANGES file `SSL_CTX_set_session_id_context` was introduced in 0.9.3 and the minimum config for openssl in configure.ac is 0.9.8.

Or are there any other adjustments needed too?
Thanks for the quick turnaround! I tried to compile from git on my CentOS box but too many issues with dependencies, will setup an Ubuntu server to test ASAP, hopefully today or tomorrow. Cheers
You can always try our docker images for centos7.

Just do a

$ dbld/rules image-centos7
$ dbld/rules rpm

From the syslog-ng source tree. You can also run dbld/rules shell to get
inside a container that has all the required dependencies to compile
syslog-ng.

Maybe this helps,
Bazsi

On Mar 26, 2018 15:16, "Jeremy2021" <notifications@github.com> wrote:

Thanks for the quick turnaround! I tried to compile from git on my CentOS
box but too many issues with dependencies, will setup an Ubuntu server to
test ASAP, hopefully today or tomorrow. Cheers

—
You are receiving this because you were mentioned.

Reply to this email directly, view it on GitHub
<https://github.com/balabit/syslog-ng/issues/1936#issuecomment-376161698>,
or mute the thread
<https://github.com/notifications/unsubscribe-auth/AArldqfFG1EAwf8q5JuG0t0XuWKOWn0Pks5tiOoVgaJpZM4S3tQj>
.
