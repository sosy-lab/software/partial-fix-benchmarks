Merge pull request #2103 from MrAnno/gcc-811-warning

Fix a bug in the old LogMessage deserialization
transport/transport-tls.c: Work around a race condition

It appears that there's a race somewhere during reload, that sometimes corrupts
the TLSContext's location (likely the rest too, but location is reused earlier).
As this race likely existed before without ill side effects, as a workaround,
remove the location reporting for now.

It should be re-introduced later, once the correct fix is in place.

This partly addresses #2080, but does not fix it.

Signed-off-by: Gergely Nagy <algernon@balabit.com>
tlscontext: make TLSContext refcounted

fixes: #2080

Signed-off-by: Laszlo Budai <stentor.bgyk@gmail.com>
