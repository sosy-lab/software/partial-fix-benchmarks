Added pico_dhcp_server_destroy() function, allowing the termination of a running DHCP server. Documentation updated accordingly.

Fixed #102
ACK the FIN packet - fixes #136
Avoid mixing up active/passive close, by anticipating the event of a call to pico_socket_close() while in close_wait notification.

Fixes the following:

- The state machine goes in CLOSE_WAIT before notifying the socket interface
- A call to pico_socket_close in the callback will set the state to LAST_ACK
- when returning from the tcp_closewait() function, if the state was altered by the callback, the last incorrect ack is not sent any more (since in LAST_ACK state we are currently waiting for the ACK on our own FIN and we are not supposed to communicate anymore).

Fixed #136
Fixed #134
