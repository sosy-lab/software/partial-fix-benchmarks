diff --git a/modules/pico_aodv.c b/modules/pico_aodv.c
index 0d1f044f9..41394cf36 100644
--- a/modules/pico_aodv.c
+++ b/modules/pico_aodv.c
@@ -17,13 +17,13 @@
 static const struct pico_ip4 HOST_NETMASK = {
     0xffffffff
 };
+static struct pico_ip4 all_bcast = { .addr = 0xFFFFFFFFu };
 
 static const struct pico_ip4 ANY_HOST = {
     0x0
 };
 
 static uint32_t pico_aodv_local_id = 0;
-
 static int aodv_node_compare(void *ka, void *kb)
 {
     struct pico_aodv_node *a = ka, *b = kb;
@@ -65,33 +65,34 @@ static void pico_aodv_set_dev(struct pico_device *dev)
 }
 
 
-static int aodv_peer_refresh(struct pico_aodv_node *node, uint32_t seq)
+static int aodv_peer_refresh(struct pico_aodv_node *node, uint32_t seq, int neighbor)
 {
-    if ((0 == node->valid_dseq) || (pico_seq_compare(seq, node->dseq) > 0)) {
+    if (neighbor || (0 == node->valid_dseq) || (pico_seq_compare(seq, node->dseq) > 0)) {
         node->dseq = seq;
+        node->valid_dseq = 1;
         node->last_seen = PICO_TIME_MS();
         return 0;
     }
     return -1;
 }
 
-static void aodv_elect_route(struct pico_aodv_node *node, union pico_address *gw, int metric, struct pico_device *dev)
+static void aodv_elect_route(struct pico_aodv_node *node, union pico_address *gw, uint8_t metric, struct pico_device *dev)
 {
     metric++;
-    if (metric < node->metric) {
+    if (!node->active || metric < node->metric) {
         pico_ipv4_route_del(node->dest.ip4, HOST_NETMASK, node->metric);
         if (!gw) {
             pico_ipv4_route_add(node->dest.ip4, HOST_NETMASK, ANY_HOST, 1, pico_ipv4_link_by_dev(dev));
             node->metric = 1;
         } else {
-            node->metric = (uint16_t)metric;
+            node->metric = metric;
             pico_ipv4_route_add(node->dest.ip4, HOST_NETMASK, gw->ip4, metric, NULL);
         }
         node->active = 1;
     }
 }
 
-static struct pico_aodv_node *aodv_peer_new(union pico_address *addr)
+static struct pico_aodv_node *aodv_peer_new(const union pico_address *addr)
 {
     struct pico_aodv_node *node = PICO_ZALLOC(sizeof(struct pico_aodv_node));
     if (!node)
@@ -102,30 +103,121 @@ static struct pico_aodv_node *aodv_peer_new(union pico_address *addr)
 }
 
 
-static struct pico_aodv_node *aodv_peer_eval(union pico_address *addr, uint32_t seq)
+static struct pico_aodv_node *aodv_peer_eval(union pico_address *addr, uint32_t seq, int neighbor, int valid_seq)
 {
     struct pico_aodv_node *node = NULL; 
     node = get_node_by_addr(addr);
     if (!node) {
         node = aodv_peer_new(addr);
     }
-    if (node && aodv_peer_refresh(node, long_be(seq)) == 0)
+
+    if (!valid_seq)
+        return node;
+
+    if (node && aodv_peer_refresh(node, long_be(seq), neighbor) == 0)
         return node;
     return NULL;
 }
 
+void aodv_forward(void *pkt, struct pico_msginfo *info, int reply)
+{
+    struct pico_aodv_node *orig;
+    union pico_address orig_addr;
+    struct pico_tree_node *index;
+    struct pico_device *dev;
+    pico_time now;
+    int size;
+
+    printf("Forwarding %s packet\n", reply?"REPLY":"REQUEST");
+
+    if (reply) {
+        struct pico_aodv_rrep *rep = (struct pico_aodv_rrep *)pkt;
+        orig_addr.ip4.addr = rep->dest;
+        rep->hop_count++;
+        size = sizeof(struct pico_aodv_rrep);
+    } else {
+        struct pico_aodv_rreq *req = (struct pico_aodv_rreq *)pkt;
+        orig_addr.ip4.addr = req->orig;
+        req->hop_count++;
+        size = sizeof(struct pico_aodv_rreq);
+    }
+
+    orig = get_node_by_addr(&orig_addr);
+    if (!orig)
+        orig = aodv_peer_new(&orig_addr);
+    if (!orig)
+        return;
+
+    now = PICO_TIME_MS();
+
+    if ( ((orig->fwd_time == 0) || ((now - orig->fwd_time) > AODV_NET_TRAVERSAL_TIME)) && (--info->ttl > 0)) {
+        orig->fwd_time = now;
+        info->dev = NULL;
+        pico_tree_foreach(index, &aodv_devices){
+            dev = index->keyValue;
+            pico_aodv_set_dev(dev);
+            pico_socket_sendto_extended(aodv_socket, pkt, size, &all_bcast, short_be(PICO_AODV_PORT), info);
+            printf("Forwarding %s: complete! ==== \n", reply?"REPLY":"REQUEST");
+        }
+    }
+}
+
+static uint32_t aodv_lifetime(struct pico_aodv_node *node)
+{
+    uint32_t lifetime;
+    pico_time now = PICO_TIME_MS();
+    if (!node->last_seen)
+        node->last_seen = now;
+
+    if ((now - node->last_seen) > AODV_ACTIVE_ROUTE_TIMEOUT)
+        return 0;
+
+    lifetime = AODV_ACTIVE_ROUTE_TIMEOUT - (uint32_t)(now - node->last_seen);
+    return lifetime;
+}
+
+static void aodv_send_reply(struct pico_aodv_node *node, struct pico_aodv_rreq *req, int node_is_local, struct pico_msginfo *info)
+{
+    struct pico_aodv_rrep reply;
+    union pico_address dest;
+    reply.type = AODV_TYPE_RREP;
+    reply.hop_count = 0;
+    reply.dest = req->dest;
+    reply.dseq = req->dseq;
+    reply.orig = req->orig;
+
+    dest.ip4.addr = 0xFFFFFFFF; /* wide broadcast */
+
+    if (short_be(req->req_flags) & AODV_RREQ_FLAG_G)
+        dest.ip4.addr = req->orig;
+    else 
+        pico_aodv_set_dev(info->dev);
+
+    if (node_is_local) {
+        reply.lifetime = long_be(AODV_MY_ROUTE_TIMEOUT);
+        reply.dseq = long_be(++pico_aodv_local_id);
+        pico_socket_sendto(aodv_socket, &reply, sizeof(reply), &dest, short_be(PICO_AODV_PORT));
+    } else if (((short_be(req->req_flags) & AODV_RREQ_FLAG_D) == 0) && (node->valid_dseq)) {
+        reply.lifetime = long_be(aodv_lifetime(node));
+        reply.hop_count = (uint8_t)((uint8_t)reply.hop_count + (uint8_t)node->metric);
+        reply.dseq = long_be(node->dseq);
+        printf("Generating RREQ for node %x, id=%x\n", reply.dest, reply.dseq);
+        pico_socket_sendto(aodv_socket, &reply, sizeof(reply), &dest, short_be(PICO_AODV_PORT));
+    }
+}
+
 /* Parser functions */
 
-static void aodv_recv_valid_rreq(struct pico_aodv_node *node)
+static void aodv_recv_valid_rreq(struct pico_aodv_node *node, struct pico_aodv_rreq *req, struct pico_msginfo *info)
 {
     struct pico_device *dev;
     dev = pico_ipv4_link_find(&node->dest.ip4);
-    if (dev) {
-        /* Case 1: destination is ourselves. Send reply. */
-    } else if (node->active) {
-        /* Case 2: we have a possible route. Send reply. */
+    if (dev || node->active) {
+        /* if destination is ourselves, or we have a possible route: Send reply. */
+        aodv_send_reply(node, req, dev != NULL, info);
     } else {
-        /* Case 3: destination unknown. Evaluate forwarding. */
+        /* destination unknown. Evaluate forwarding. */
+        aodv_forward(req, info, 0);
     }
 }
 
@@ -134,16 +226,25 @@ static void aodv_parse_rreq(union pico_address *from, uint8_t *buf, int len, str
     struct pico_aodv_rreq *req = (struct pico_aodv_rreq *) buf;
     struct pico_aodv_node *node = NULL; 
     union pico_address orig;
+    (void)from;
     if (len != sizeof(struct pico_aodv_rreq))
         return;
 
     orig.ip4.addr = req->orig;
-    node = aodv_peer_eval(&orig, req->oseq);
-    if (!node)
+    node = aodv_peer_eval(&orig, req->oseq, 1, 1); /* Evaluate neighbor. */
+    if (!node) {
+        printf("RREQ: Neighbor is not valid. oseq=%d, stored dseq: %d\n", long_be(req->oseq), node->dseq);
         return;
-    aodv_elect_route(node, from, req->hop_count, msginfo->dev);
+    }
+    aodv_elect_route(node, NULL, 1, msginfo->dev);
 
-    aodv_recv_valid_rreq(node);
+    orig.ip4.addr = req->dest;
+    node = aodv_peer_eval(&orig, req->dseq, 0, !(req->req_flags & short_be(AODV_RREQ_FLAG_U)));
+    if (!node)
+        node = aodv_peer_new(&orig);
+    if (!node)
+        return;
+    aodv_recv_valid_rreq(node, req, msginfo);
 }
 
 static void aodv_parse_rrep(union pico_address *from, uint8_t *buf, int len, struct pico_msginfo *msginfo)
@@ -151,15 +252,24 @@ static void aodv_parse_rrep(union pico_address *from, uint8_t *buf, int len, str
     struct pico_aodv_rrep *rep = (struct pico_aodv_rrep *) buf;
     struct pico_aodv_node *node = NULL; 
     union pico_address dest;
+    struct pico_device *dev = NULL;
     if (len != sizeof(struct pico_aodv_rrep))
         return;
 
     dest.ip4.addr = rep->dest;
-    node = aodv_peer_eval(&dest, rep->dseq);
-    if (!node)
+    dev = pico_ipv4_link_find(&dest.ip4);
+
+    if (dev) /* Our reply packet got rebounced, no useful information here, no need to fwd. */
         return;
-    dest.ip4.addr = node->dest.ip4.addr;
-    aodv_elect_route(node, from, rep->hop_count, msginfo->dev);
+
+    printf("::::::::::::: Parsing RREP for node %08x\n", rep->dest);
+    node = aodv_peer_eval(&dest, rep->dseq, 0, 1);
+    if (node) {
+        printf("::::::::::::: Node found. Electing route and forwarding.\n");
+        dest.ip4.addr = node->dest.ip4.addr;
+        aodv_elect_route(node, from, rep->hop_count, msginfo->dev);
+        aodv_forward(rep, msginfo, 1);
+    }
 }
 
 static void aodv_parse_rerr(union pico_address *from, uint8_t *buf, int len, struct pico_msginfo *msginfo)
@@ -203,6 +313,7 @@ static void pico_aodv_parse(union pico_address *from, uint8_t *buf, int len, str
         /* Type is invalid. Discard silently. */
         return;
     }
+    pico_ipv4_route_add(from->ip4, HOST_NETMASK, ANY_HOST, 1, pico_ipv4_link_by_dev(msginfo->dev));
     aodv_parser[buf[0]].call(from, buf, len, msginfo);
 }
 
@@ -220,34 +331,93 @@ static void pico_aodv_socket_callback(uint16_t ev, struct pico_socket *s)
         if (r <= 0)
             return;
         dbg("Received AODV packet: %d bytes \n", r);
+
         pico_aodv_parse(&from, aodv_pkt, r, &msginfo);
     }
 }
 
 static void aodv_make_rreq(struct pico_aodv_node *node, struct pico_aodv_rreq *req)
 {
+    memset(req, 0, sizeof(struct pico_aodv_rreq));
     req->type = AODV_TYPE_RREQ;
-    req->req_flags |= short_be(AODV_RREQ_FLAG_G); /* RFC3561 $6.3: we SHOULD set G flag as originators */
+
     if (!node->valid_dseq) {
         req->req_flags |= short_be(AODV_RREQ_FLAG_U); /* no known dseq, mark as U */
         req->dseq = 0; /* Unknown */
     } else {
         req->dseq = long_be(node->dseq);
+        req->req_flags |= short_be(AODV_RREQ_FLAG_G); /* RFC3561 $6.3: we SHOULD set G flag as originators */
     }
     /* Hop count = 0; */
-    req->rreq_id = long_be(pico_aodv_local_id);
+    req->rreq_id = long_be(++pico_aodv_local_id);
     req->dest = node->dest.ip4.addr;
     req->oseq = long_be(pico_aodv_local_id);
 }
 
+static void aodv_retrans_rreq(pico_time now, void *arg)
+{
+    struct pico_aodv_node *node = (struct pico_aodv_node *)arg;
+    struct pico_device *dev;
+    struct pico_tree_node *index;
+    static struct pico_aodv_rreq rreq;
+    struct pico_ipv4_link *ip4l = NULL;
+    struct pico_msginfo info = {
+        .dev = NULL, .tos = 0, .ttl = AODV_TTL_START
+    };
+    (void)now;
+
+    memset(&rreq, 0, sizeof(rreq));
+
+    if (node->active) {
+        node->ring_ttl = 0;
+        return;
+    }
+
+    if (node->ring_ttl >= AODV_TTL_THRESHOLD) {
+        node->ring_ttl = AODV_NET_DIAMETER;
+        printf("----------- DIAMETER reached.\n");
+    }
+
+
+    if (node->rreq_retry > AODV_RREQ_RETRIES) {
+        node->rreq_retry = 0;
+        node->ring_ttl = 0;
+        printf("Node is unreachable.\n");
+        return;
+    }
+
+    if (node->ring_ttl == AODV_NET_DIAMETER) {
+        node->rreq_retry++; 
+        printf("Retry #%d\n", node->rreq_retry);
+    }
+
+    aodv_make_rreq(node, &rreq);
+    info.ttl = (uint8_t)node->ring_ttl; 
+    pico_tree_foreach(index, &aodv_devices){
+        dev = index->keyValue;
+        pico_aodv_set_dev(dev);
+        ip4l = pico_ipv4_link_by_dev(dev);
+        if (ip4l) {
+            rreq.orig = ip4l->address.addr;
+            pico_socket_sendto_extended(aodv_socket, &rreq, sizeof(rreq), &all_bcast, short_be(PICO_AODV_PORT), &info);
+        }
+    }
+    if (node->ring_ttl < AODV_NET_DIAMETER)
+        node->ring_ttl += AODV_TTL_INCREMENT;
+    pico_timer_add((pico_time)AODV_RING_TRAVERSAL_TIME(node->ring_ttl), aodv_retrans_rreq, node);
+}
+
 static int aodv_send_req(struct pico_aodv_node *node)
 {
     struct pico_device *dev;
     struct pico_tree_node *index;
-    struct pico_ip4 all_bcast = { .addr = 0xFFFFFFFFu };
     static struct pico_aodv_rreq rreq;
     int n = 0;
     struct pico_ipv4_link *ip4l = NULL;
+    struct pico_msginfo info = {
+        .dev = NULL, .tos = 0, .ttl = AODV_TTL_START
+    };
+    memset(&rreq, 0, sizeof(rreq));
 
     if (pico_tree_empty(&aodv_devices))
         return n;
@@ -257,7 +427,6 @@ static int aodv_send_req(struct pico_aodv_node *node)
         return -1;
     }
 
-    pico_aodv_local_id++;
     aodv_make_rreq(node, &rreq);
     pico_tree_foreach(index, &aodv_devices){
         dev = index->keyValue;
@@ -265,10 +434,11 @@ static int aodv_send_req(struct pico_aodv_node *node)
         ip4l = pico_ipv4_link_by_dev(dev);
         if (ip4l) {
             rreq.orig = ip4l->address.addr;
-            pico_socket_sendto(aodv_socket, &rreq, sizeof(rreq), &all_bcast, short_be(PICO_AODV_PORT));
+            pico_socket_sendto_extended(aodv_socket, &rreq, sizeof(rreq), &all_bcast, short_be(PICO_AODV_PORT), &info);
             n++;
         }
     }
+    pico_timer_add(AODV_PATH_DISCOVERY_TIME, aodv_retrans_rreq, node);
     return n;   
 }
 
@@ -304,14 +474,16 @@ int pico_aodv_add(struct pico_device *dev)
 int pico_aodv_lookup(const union pico_address *addr)
 {
     struct pico_aodv_node *node = get_node_by_addr(addr);
-    if (!node) {
-        node = PICO_ZALLOC(sizeof(struct pico_aodv_node));
-        if (!node)
-            return -1;
-        memcpy(&node->dest, addr, sizeof(union pico_address));
-    }
-    if (aodv_send_req(node) > 0)
+    if (!node)
+        node = aodv_peer_new(addr);
+    if (!node)
+        return -1;
+
+    if (node->ring_ttl < AODV_TTL_START) {
+        node->ring_ttl = AODV_TTL_START;
+        aodv_send_req(node);
         return 0;
+    }
     pico_err = PICO_ERR_EINVAL;
     return -1;
 }
diff --git a/modules/pico_aodv.h b/modules/pico_aodv.h
index fd186603e..7cabc4908 100644
--- a/modules/pico_aodv.h
+++ b/modules/pico_aodv.h
@@ -14,8 +14,7 @@
 #define PICO_AODV_PORT (654)
 
 /* RFC3561 $10 */
-#define AODV_TTL_VALUE(f) (((struct pico_ipv4_hdr *)f->net_hdr)->ttl)
-#define AODV_ACTIVE_ROUTE_TIMEOUT     5000 /* Conservative value for link breakage detection */
+#define AODV_ACTIVE_ROUTE_TIMEOUT     (5000u) /* Conservative value for link breakage detection */
 #define AODV_DELETE_PERIOD            (5 * AODV_ACTIVE_ROUTE_TIMEOUT) /* Recommended value K = 5 */
 #define AODV_ALLOWED_HELLO_LOSS       (4) /* conservative */
 #define AODV_NET_DIAMETER             (35)
@@ -35,7 +34,7 @@
 #define AODV_BLACKLIST_TIMEOUT        (AODV_RREQ_RETRIES * AODV_NET_TRAVERSAL_TIME)
 #define AODV_NEXT_HOP_WAIT            (AODV_NODE_TRAVERSAL_TIME + 10)
 #define AODV_PATH_DISCOVERY_TIME      (2 * AODV_NET_TRAVERSAL_TIME)
-#define AODV_RING_TRAVERSAL_TIME(f)   (2 * AODV_NODE_TRAVERSAL_TIME * (AODV_TTL_VALUE(f) + AODV_TIMEOUT_BUFFER))
+#define AODV_RING_TRAVERSAL_TIME(ttl)   (2 * AODV_NODE_TRAVERSAL_TIME * (ttl + AODV_TIMEOUT_BUFFER))
 /* End section RFC3561 $10 */
 
 
@@ -84,10 +83,13 @@ struct pico_aodv_node
 {
     union pico_address dest;
     uint32_t dseq;  
-    uint16_t metric;
+    uint8_t metric;
     int valid_dseq;
     int active;
     pico_time last_seen;
+    pico_time fwd_time;
+    int ring_ttl;
+    int rreq_retry;
 };
 
 PACKED_STRUCT_DEF pico_aodv_unreachable
diff --git a/modules/pico_ipv4.c b/modules/pico_ipv4.c
index 65d26423d..6f06e0d88 100644
--- a/modules/pico_ipv4.c
+++ b/modules/pico_ipv4.c
@@ -19,6 +19,7 @@
 #include "pico_nat.h"
 #include "pico_igmp.h"
 #include "pico_tree.h"
+#include "pico_aodv.h"
 #include "pico_socket_multicast.h"
 
 #ifdef PICO_SUPPORT_IPV4
@@ -885,9 +886,12 @@ struct pico_ip4 *pico_ipv4_source_find(const struct pico_ip4 *dst)
     rt = route_find(dst);
     if (rt && rt->link) {
         myself = &rt->link->address;
-    } else
+    } else {
+#ifdef PICO_SUPPORT_AODV
+        pico_aodv_lookup((const union pico_address *)dst);
+#endif
         pico_err = PICO_ERR_EHOSTUNREACH;
-
+    }
     return myself;
 }
 
diff --git a/modules/pico_udp.c b/modules/pico_udp.c
index deef46397..678ed919d 100644
--- a/modules/pico_udp.c
+++ b/modules/pico_udp.c
@@ -193,6 +193,10 @@ uint16_t pico_udp_recv(struct pico_socket *s, void *buf, uint16_t len, void *src
             *port = hdr->sport;
         }
 
+        if (msginfo) {
+            pico_udp_get_msginfo(f, msginfo);
+        }
+
         if (f->payload_len > len) {
             memcpy(buf, f->payload, len);
             f->payload += len;
@@ -205,9 +209,6 @@ uint16_t pico_udp_recv(struct pico_socket *s, void *buf, uint16_t len, void *src
             pico_frame_discard(f);
             return ret;
         }
-        if (msginfo) {
-            pico_udp_get_msginfo(f, msginfo);
-        }
     } else return 0;
 }
 
diff --git a/test/picoapp.c b/test/picoapp.c
index e4f09f6ff..6c9f92ec2 100644
--- a/test/picoapp.c
+++ b/test/picoapp.c
@@ -619,8 +619,6 @@ int main(int argc, char **argv)
                     pico_aodv_add(dev);
                 }
 
-                /* TEST */
-                pico_aodv_lookup(&aaa);
 
                 app_noop();
 #endif
