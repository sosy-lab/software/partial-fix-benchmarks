TCP: No RST response to segments with some strange flag combinations
Implemented a TCP flags check mechanism using a new matrix containing all valid flag combinations for each TCP state.
"response from closed socket" robot framework test and Linux autotest passed.

No RST is received. Introduced somewhere between 19 Feb, 9h46 and 20 Feb, 9h41.
