Cannot set owner on continuous aggregation view
@cloud-rocket thank you for the issue. 
Currently [ALTER VIEW only supports WITH option on continuous aggregate views](https://docs.timescale.com/v1.3/api#continuous_aggregate-alter_view) and other options are not available. We will look into this feature request.
Can you describe your use case? Why do you need this feature? 
Thank you.
Because I am creating views as a DB user with my personal username and password and then assigning a generic predefined role used as a role (not as a user).