Add ignore_invalidation_older_than to view

Add ignore_invalidation_older_than parameter to
timescaledb_information.continuous_aggregates view

Fixes #1664
Fix crash when interrupting create_hypertable

When doing a migrate of table data to a hypertable during a call of
create_hypertable, an interruption would cause the signal handling to
call `CopyFromErrorCallback` which would read garbage data because a
heap scan descriptor is read as if it was a `CopyState` structure.

This commit fixes that by not adding a new context to the error context
stack when migrating data.

Fixes #1651
Fixes #1652
Prevent deadlock in create_hypertable

The table, which is transformed into hypertable by create_hypertable,
can have foreign key constraints to other tables. If an application
tries to insert data into both a referenced table and the future
hypertable in the same transaction, it results into a deadlock, since
create_hypertable locks the future hypertable, while transforming it
and migrating existing data into chunks. This commit adds locks on the
referenced tables to prevent from the deadlock.
Fixes #1652.
Prevent deadlock in create_hypertable

The table, which is transformed into hypertable by create_hypertable,
can have foreign key constraints to other tables. If an application
tries to insert data into both a referenced table and the future
hypertable in the same transaction, it results into a deadlock, since
create_hypertable locks the future hypertable, while transforming it
and migrating existing data into chunks. This commit adds locks on the
referenced tables to prevent from the deadlock.
Fixes #1652.
Prevent deadlock in create_hypertable

The table, which is transformed into hypertable by create_hypertable,
can have foreign key constraints to other tables. If an application
tries to insert data into both a referenced table and the future
hypertable in the same transaction, it results into a deadlock, since
create_hypertable locks the future hypertable, while transforming it
and migrating existing data into chunks. This commit adds locks on the
referenced tables to prevent from the deadlock.
Fixes #1652.
