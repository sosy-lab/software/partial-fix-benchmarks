diff --git a/src/chunk.c b/src/chunk.c
index db9dae55e1..6ccc7b810c 100644
--- a/src/chunk.c
+++ b/src/chunk.c
@@ -618,6 +618,54 @@ get_am_name_for_rel(Oid relid)
 }
 #endif
 
+static void
+copy_hypertable_acl_to_relid(Hypertable *ht, Oid relid)
+{
+	HeapTuple ht_tuple;
+	bool is_null;
+	Datum acl_datum;
+	Relation class_rel;
+
+	/* We open it here since there is no point in trying to update the tuples
+	 * if we cannot open the Relation catalog table */
+	class_rel = table_open(RelationRelationId, RowExclusiveLock);
+
+	ht_tuple = SearchSysCache1(RELOID, ObjectIdGetDatum(ht->main_table_relid));
+	Assert(HeapTupleIsValid(ht_tuple));
+
+	/* We only bother about setting the chunk ACL if the hypertable ACL is
+	 * non-null */
+	acl_datum = SysCacheGetAttr(RELOID, ht_tuple, Anum_pg_class_relacl, &is_null);
+	if (!is_null)
+	{
+		HeapTuple chunk_tuple, newtuple;
+		Datum new_val[Natts_pg_class] = { 0 };
+		bool new_null[Natts_pg_class] = { false };
+		bool new_repl[Natts_pg_class] = { false };
+		Acl *acl = DatumGetAclP(acl_datum);
+
+		new_repl[Anum_pg_class_relacl - 1] = true;
+		new_val[Anum_pg_class_relacl - 1] = PointerGetDatum(acl);
+
+		/* Find the tuple for the chunk in `pg_class` */
+		chunk_tuple = SearchSysCache1(RELOID, ObjectIdGetDatum(relid));
+		Assert(HeapTupleIsValid(chunk_tuple));
+
+		/* Update the relacl for the chunk tuple to use the acl from the hypertable */
+		newtuple = heap_modify_tuple(chunk_tuple,
+									 RelationGetDescr(class_rel),
+									 new_val,
+									 new_null,
+									 new_repl);
+		CatalogTupleUpdate(class_rel, &newtuple->t_self, newtuple);
+		heap_freetuple(newtuple);
+		ReleaseSysCache(chunk_tuple);
+	}
+
+	ReleaseSysCache(ht_tuple);
+	table_close(class_rel, RowExclusiveLock);
+}
+
 /*
  * Create a chunk's table.
  *
@@ -687,6 +735,13 @@ ts_chunk_create_table(Chunk *chunk, Hypertable *ht, const char *tablespacename)
 #endif
 	);
 
+	/* Make the newly defined relation visible so that we can update the
+	 * ACL. */
+	CommandCounterIncrement();
+
+	/* Copy acl from hypertable to chunk relation record */
+	copy_hypertable_acl_to_relid(ht, objaddr.objectId);
+
 	/*
 	 * need to create a toast table explicitly for some of the option setting
 	 * to work
diff --git a/src/process_utility.c b/src/process_utility.c
index bc4666882e..beb2b0f82c 100644
--- a/src/process_utility.c
+++ b/src/process_utility.c
@@ -276,6 +276,16 @@ process_add_hypertable(ProcessUtilityArgs *args, Hypertable *ht)
 	args->hypertable_list = lappend_oid(args->hypertable_list, ht->main_table_relid);
 }
 
+static void
+add_chunk_oid(Hypertable *ht, Oid chunk_relid, void *vargs)
+{
+	ProcessUtilityArgs *args = vargs;
+	GrantStmt *stmt = castNode(GrantStmt, args->parsetree);
+	Chunk *chunk = ts_chunk_get_by_relid(chunk_relid, true);
+	RangeVar *rv = makeRangeVar(NameStr(chunk->fd.schema_name), NameStr(chunk->fd.table_name), -1);
+	stmt->objects = lappend(stmt->objects, rv);
+}
+
 static void
 process_altertableschema(ProcessUtilityArgs *args)
 {
@@ -968,6 +978,14 @@ process_drop_tablespace(ProcessUtilityArgs *args)
 	return false;
 }
 
+#if PG11_LT
+#define IS_TABLESPACE(STMT) ((STMT) == ACL_OBJECT_TABLESPACE)
+#define IS_TABLE(STMT) ((STMT) == ACL_OBJECT_RELATION)
+#else
+#define IS_TABLESPACE(STMT) ((STMT) == OBJECT_TABLESPACE)
+#define IS_TABLE(STMT) ((STMT) == OBJECT_TABLE)
+#endif
+
 /*
  * Handle GRANT / REVOKE.
  *
@@ -978,37 +996,34 @@ process_grant_and_revoke(ProcessUtilityArgs *args)
 {
 	GrantStmt *stmt = (GrantStmt *) args->parsetree;
 
-	/*
-	 * Need to apply the REVOKE first to be able to check remaining
-	 * permissions
-	 */
-	prev_ProcessUtility(args);
-
-	/* We only care about revokes and setting privileges on a specific object */
-	if (stmt->is_grant || stmt->targtype != ACL_TARGET_OBJECT)
+	if (IS_TABLESPACE(stmt->objtype) && !stmt->is_grant && stmt->targtype == ACL_TARGET_OBJECT)
+	{
+		prev_ProcessUtility(args);
+		ts_tablespace_validate_revoke(stmt);
 		return true;
+	}
 
-	switch (stmt->objtype)
+	if (IS_TABLE(stmt->objtype) && stmt->targtype == ACL_TARGET_OBJECT)
 	{
-/*
- * PG11 consolidated several ACL_OBJECT_FOO or similar to the already extant
- * OBJECT_FOO see:
- * https://github.com/postgres/postgres/commit/2c6f37ed62114bd5a092c20fe721bd11b3bcb91e
- * so we can't simply #define OBJECT_TABLESPACE ACL_OBJECT_TABLESPACE and have
- * things work correctly for previous versions.
- */
-#if PG11_LT
-		case ACL_OBJECT_TABLESPACE:
-#else
-		case OBJECT_TABLESPACE:
-#endif
-			ts_tablespace_validate_revoke(stmt);
-			break;
-		default:
-			break;
+		Cache *hcache = ts_hypertable_cache_pin();
+		ListCell *cell;
+
+		foreach (cell, stmt->objects)
+		{
+			RangeVar *relation = lfirst_node(RangeVar, cell);
+			Hypertable *ht = ts_hypertable_cache_get_entry_rv(hcache, relation);
+
+			if (ht)
+			{
+				process_add_hypertable(args, ht);
+				foreach_chunk(ht, add_chunk_oid, args);
+			}
+		}
+
+		ts_cache_release(hcache);
 	}
 
-	return true;
+	return false;
 }
 
 static bool
diff --git a/src/telemetry/telemetry.c b/src/telemetry/telemetry.c
index bd724cce93..7cd1fe56e5 100644
--- a/src/telemetry/telemetry.c
+++ b/src/telemetry/telemetry.c
@@ -10,7 +10,6 @@
 #include <commands/extension.h>
 #include <catalog/pg_collation.h>
 #include <utils/builtins.h>
-#include <utils/fmgrprotos.h>
 #include <utils/json.h>
 #include <utils/jsonb.h>
 
@@ -31,6 +30,12 @@
 
 #include "cross_module_fn.h"
 
+#if PG10_LT
+#include <utils/formatting.h>
+#else
+#include <utils/fmgrprotos.h>
+#endif
+
 #define TS_VERSION_JSON_FIELD "current_timescaledb_version"
 #define TS_IS_UPTODATE_JSON_FIELD "is_up_to_date"
 
diff --git a/test/expected/grant_hypertable.out b/test/expected/grant_hypertable.out
new file mode 100644
index 0000000000..753961fb59
--- /dev/null
+++ b/test/expected/grant_hypertable.out
@@ -0,0 +1,248 @@
+-- This file and its contents are licensed under the Apache License 2.0.
+-- Please see the included NOTICE for copyright information and
+-- LICENSE-APACHE for a copy of the license.
+\c :TEST_DBNAME :ROLE_SUPERUSER
+CREATE TABLE conditions(
+    time TIMESTAMPTZ NOT NULL,
+    device INTEGER,
+    temperature FLOAT
+);
+-- Create a hypertable and show that it does not have any privileges
+SELECT * FROM create_hypertable('conditions', 'time', chunk_time_interval => '5 days'::interval);
+ hypertable_id | schema_name | table_name | created 
+---------------+-------------+------------+---------
+             1 | public      | conditions | t
+(1 row)
+
+INSERT INTO conditions
+SELECT time, (random()*30)::int, random()*80 - 40
+FROM generate_series('2018-12-01 00:00'::timestamp, '2018-12-10 00:00'::timestamp, '1h') AS time;
+\z conditions
+                               Access privileges
+ Schema |    Name    | Type  | Access privileges | Column privileges | Policies 
+--------+------------+-------+-------------------+-------------------+----------
+ public | conditions | table |                   |                   | 
+(1 row)
+
+\z _timescaledb_internal.*chunk
+                                          Access privileges
+        Schema         |       Name       | Type  | Access privileges | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table |                   |                   | 
+ _timescaledb_internal | _hyper_1_2_chunk | table |                   |                   | 
+ _timescaledb_internal | _hyper_1_3_chunk | table |                   |                   | 
+(3 rows)
+
+-- Add privileges and show that they propagate to the chunks
+GRANT SELECT, INSERT ON conditions TO PUBLIC;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =ar/super_user                |                   | 
+(1 row)
+
+\z _timescaledb_internal.*chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+ _timescaledb_internal | _hyper_1_2_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+ _timescaledb_internal | _hyper_1_3_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+(3 rows)
+
+-- Create some more chunks and show that they also get the privileges.
+INSERT INTO conditions
+SELECT time, (random()*30)::int, random()*80 - 40
+FROM generate_series('2018-12-10 00:00'::timestamp, '2018-12-20 00:00'::timestamp, '1h') AS time;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =ar/super_user                |                   | 
+(1 row)
+
+\z _timescaledb_internal.*chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+ _timescaledb_internal | _hyper_1_2_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+ _timescaledb_internal | _hyper_1_3_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+ _timescaledb_internal | _hyper_1_4_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+ _timescaledb_internal | _hyper_1_5_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+(5 rows)
+
+-- Revoke one of the privileges and show that it propagate to the
+-- chunks.
+REVOKE INSERT ON conditions FROM PUBLIC;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =r/super_user                 |                   | 
+(1 row)
+
+\z _timescaledb_internal.*chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_2_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_3_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_4_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_5_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+(5 rows)
+
+-- Add some more chunks and show that it inherits the grants from the
+-- hypertable.
+INSERT INTO conditions
+SELECT time, (random()*30)::int, random()*80 - 40
+FROM generate_series('2018-12-20 00:00'::timestamp, '2018-12-30 00:00'::timestamp, '1h') AS time;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =r/super_user                 |                   | 
+(1 row)
+
+\z _timescaledb_internal.*chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_2_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_3_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_4_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_5_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_6_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+ _timescaledb_internal | _hyper_1_7_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+(7 rows)
+
+-- Change grants of one chunk explicitly and check that it is possible
+\z _timescaledb_internal._hyper_1_1_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =r/super_user                 |                   | 
+(1 row)
+
+GRANT UPDATE ON _timescaledb_internal._hyper_1_1_chunk TO PUBLIC;
+\z _timescaledb_internal._hyper_1_1_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =rw/super_user                |                   | 
+(1 row)
+
+REVOKE SELECT ON _timescaledb_internal._hyper_1_1_chunk FROM PUBLIC;
+\z _timescaledb_internal._hyper_1_1_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_1_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =w/super_user                 |                   | 
+(1 row)
+
+-- Check that revoking a permission first on the chunk and then on the
+-- hypertable that was added through the hypertable (INSERT and
+-- SELECT, in this case) still do not copy permissions from the
+-- hypertable (so there should not be a select permission to public on
+-- the chunk but there should be one on the hypertable).
+GRANT INSERT ON conditions TO PUBLIC;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =ar/super_user                |                   | 
+(1 row)
+
+\z _timescaledb_internal._hyper_1_2_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_2_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =ar/super_user                |                   | 
+(1 row)
+
+REVOKE SELECT ON _timescaledb_internal._hyper_1_2_chunk FROM PUBLIC;
+REVOKE INSERT ON conditions FROM PUBLIC;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =r/super_user                 |                   | 
+(1 row)
+
+\z _timescaledb_internal._hyper_1_2_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_2_chunk | table | super_user=arwdDxt/super_user |                   | 
+(1 row)
+
+-- Check that granting permissions through hypertable does not remove
+-- separate grants on chunk.
+GRANT UPDATE ON _timescaledb_internal._hyper_1_3_chunk TO PUBLIC;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =r/super_user                 |                   | 
+(1 row)
+
+\z _timescaledb_internal._hyper_1_3_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_3_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =rw/super_user                |                   | 
+(1 row)
+
+GRANT INSERT ON conditions TO PUBLIC;
+REVOKE INSERT ON conditions FROM PUBLIC;
+\z conditions
+                                     Access privileges
+ Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
+--------+------------+-------+-------------------------------+-------------------+----------
+ public | conditions | table | super_user=arwdDxt/super_user+|                   | 
+        |            |       | =r/super_user                 |                   | 
+(1 row)
+
+\z _timescaledb_internal._hyper_1_3_chunk
+                                                Access privileges
+        Schema         |       Name       | Type  |       Access privileges       | Column privileges | Policies 
+-----------------------+------------------+-------+-------------------------------+-------------------+----------
+ _timescaledb_internal | _hyper_1_3_chunk | table | super_user=arwdDxt/super_user+|                   | 
+                       |                  |       | =rw/super_user                |                   | 
+(1 row)
+
diff --git a/test/expected/rowsecurity-10.out b/test/expected/rowsecurity-10.out
index 39da224bdf..5a6e5e5b52 100644
--- a/test/expected/rowsecurity-10.out
+++ b/test/expected/rowsecurity-10.out
@@ -546,6 +546,7 @@ INSERT INTO document VALUES (11, 33, 1, current_user, 'hoge');
 SET SESSION AUTHORIZATION regress_rls_bob;
 INSERT INTO document VALUES (8, 44, 1, 'regress_rls_bob', 'my third manga'); -- Must fail with unique violation, revealing presence of did we can't see
 ERROR:  duplicate key value violates unique constraint "5_10_document_pkey"
+DETAIL:  Key (did)=(8) already exists.
 SELECT * FROM document WHERE did = 8; -- and confirm we can't see it
  did | cid | dlevel | dauthor | dtitle 
 -----+-----+--------+---------+--------
@@ -4332,7 +4333,7 @@ INSERT INTO r1 SELECT a + 1 FROM r2 RETURNING *; -- OK
 
 UPDATE r1 SET a = r2.a + 2 FROM r2 WHERE r1.a = r2.a RETURNING *; -- OK
 ERROR:  new row for relation "_hyper_23_105_chunk" violates check constraint "constraint_105"
-DETAIL:  Failing row contains (a) = (12).
+DETAIL:  Failing row contains (12).
 DELETE FROM r1 USING r2 WHERE r1.a = r2.a + 2 RETURNING *; -- OK
  a | a 
 ---+---
diff --git a/test/expected/rowsecurity-11.out b/test/expected/rowsecurity-11.out
index 46f9193f24..bab43e3e56 100644
--- a/test/expected/rowsecurity-11.out
+++ b/test/expected/rowsecurity-11.out
@@ -559,6 +559,7 @@ INSERT INTO document VALUES (11, 33, 1, current_user, 'hoge');
 SET SESSION AUTHORIZATION regress_rls_bob;
 INSERT INTO document VALUES (8, 44, 1, 'regress_rls_bob', 'my third manga'); -- Must fail with unique violation, revealing presence of did we can't see
 ERROR:  duplicate key value violates unique constraint "5_10_document_pkey"
+DETAIL:  Key (did)=(8) already exists.
 SELECT * FROM document WHERE did = 8; -- and confirm we can't see it
  did | cid | dlevel | dauthor | dtitle 
 -----+-----+--------+---------+--------
@@ -4439,7 +4440,7 @@ INSERT INTO r1 SELECT a + 1 FROM r2 RETURNING *; -- OK
 
 UPDATE r1 SET a = r2.a + 2 FROM r2 WHERE r1.a = r2.a RETURNING *; -- OK
 ERROR:  new row for relation "_hyper_23_105_chunk" violates check constraint "constraint_105"
-DETAIL:  Failing row contains (a) = (12).
+DETAIL:  Failing row contains (12).
 DELETE FROM r1 USING r2 WHERE r1.a = r2.a + 2 RETURNING *; -- OK
  a | a 
 ---+---
diff --git a/test/expected/rowsecurity-12.out b/test/expected/rowsecurity-12.out
index 4b75f990f0..1e7498dda6 100644
--- a/test/expected/rowsecurity-12.out
+++ b/test/expected/rowsecurity-12.out
@@ -559,6 +559,7 @@ INSERT INTO document VALUES (11, 33, 1, current_user, 'hoge');
 SET SESSION AUTHORIZATION regress_rls_bob;
 INSERT INTO document VALUES (8, 44, 1, 'regress_rls_bob', 'my third manga'); -- Must fail with unique violation, revealing presence of did we can't see
 ERROR:  duplicate key value violates unique constraint "5_10_document_pkey"
+DETAIL:  Key (did)=(8) already exists.
 SELECT * FROM document WHERE did = 8; -- and confirm we can't see it
  did | cid | dlevel | dauthor | dtitle 
 -----+-----+--------+---------+--------
@@ -4633,7 +4634,7 @@ INSERT INTO r1 SELECT a + 1 FROM r2 RETURNING *; -- OK
 
 UPDATE r1 SET a = r2.a + 2 FROM r2 WHERE r1.a = r2.a RETURNING *; -- OK
 ERROR:  new row for relation "_hyper_23_105_chunk" violates check constraint "constraint_105"
-DETAIL:  Failing row contains (a) = (12).
+DETAIL:  Failing row contains (12).
 DELETE FROM r1 USING r2 WHERE r1.a = r2.a + 2 RETURNING *; -- OK
  a | a 
 ---+---
diff --git a/test/expected/rowsecurity-9.6.out b/test/expected/rowsecurity-9.6.out
index de8b74ac8d..d2f6045d2e 100644
--- a/test/expected/rowsecurity-9.6.out
+++ b/test/expected/rowsecurity-9.6.out
@@ -381,6 +381,7 @@ INSERT INTO document VALUES (11, 33, 1, current_user, 'hoge');
 SET SESSION AUTHORIZATION regress_rls_bob;
 INSERT INTO document VALUES (8, 44, 1, 'regress_rls_bob', 'my third manga'); -- Must fail with unique violation, revealing presence of did we can't see
 ERROR:  duplicate key value violates unique constraint "5_10_document_pkey"
+DETAIL:  Key (did)=(8) already exists.
 SELECT * FROM document WHERE did = 8; -- and confirm we can't see it
  did | cid | dlevel | dauthor | dtitle 
 -----+-----+--------+---------+--------
@@ -4349,7 +4350,7 @@ INSERT INTO r1 SELECT a + 1 FROM r2 RETURNING *; -- OK
 
 UPDATE r1 SET a = r2.a + 2 FROM r2 WHERE r1.a = r2.a RETURNING *; -- OK
 ERROR:  new row for relation "_hyper_23_102_chunk" violates check constraint "constraint_102"
-DETAIL:  Failing row contains (a) = (12).
+DETAIL:  Failing row contains (12).
 DELETE FROM r1 USING r2 WHERE r1.a = r2.a + 2 RETURNING *; -- OK
  a | a 
 ---+---
diff --git a/test/sql/CMakeLists.txt b/test/sql/CMakeLists.txt
index 1475696eb7..2a875b8742 100644
--- a/test/sql/CMakeLists.txt
+++ b/test/sql/CMakeLists.txt
@@ -20,6 +20,7 @@ set(TEST_FILES
   dump_meta.sql
   extension.sql
   gapfill.sql
+  grant_hypertable.sql
   hash.sql
   histogram_test.sql
   index.sql
diff --git a/test/sql/grant_hypertable.sql b/test/sql/grant_hypertable.sql
new file mode 100644
index 0000000000..689f4517ae
--- /dev/null
+++ b/test/sql/grant_hypertable.sql
@@ -0,0 +1,75 @@
+-- This file and its contents are licensed under the Apache License 2.0.
+-- Please see the included NOTICE for copyright information and
+-- LICENSE-APACHE for a copy of the license.
+
+\c :TEST_DBNAME :ROLE_SUPERUSER
+
+CREATE TABLE conditions(
+    time TIMESTAMPTZ NOT NULL,
+    device INTEGER,
+    temperature FLOAT
+);
+
+-- Create a hypertable and show that it does not have any privileges
+SELECT * FROM create_hypertable('conditions', 'time', chunk_time_interval => '5 days'::interval);
+INSERT INTO conditions
+SELECT time, (random()*30)::int, random()*80 - 40
+FROM generate_series('2018-12-01 00:00'::timestamp, '2018-12-10 00:00'::timestamp, '1h') AS time;
+\z conditions
+\z _timescaledb_internal.*chunk
+
+-- Add privileges and show that they propagate to the chunks
+GRANT SELECT, INSERT ON conditions TO PUBLIC;
+\z conditions
+\z _timescaledb_internal.*chunk
+
+-- Create some more chunks and show that they also get the privileges.
+INSERT INTO conditions
+SELECT time, (random()*30)::int, random()*80 - 40
+FROM generate_series('2018-12-10 00:00'::timestamp, '2018-12-20 00:00'::timestamp, '1h') AS time;
+\z conditions
+\z _timescaledb_internal.*chunk
+
+-- Revoke one of the privileges and show that it propagate to the
+-- chunks.
+REVOKE INSERT ON conditions FROM PUBLIC;
+\z conditions
+\z _timescaledb_internal.*chunk
+
+-- Add some more chunks and show that it inherits the grants from the
+-- hypertable.
+INSERT INTO conditions
+SELECT time, (random()*30)::int, random()*80 - 40
+FROM generate_series('2018-12-20 00:00'::timestamp, '2018-12-30 00:00'::timestamp, '1h') AS time;
+\z conditions
+\z _timescaledb_internal.*chunk
+
+-- Change grants of one chunk explicitly and check that it is possible
+\z _timescaledb_internal._hyper_1_1_chunk
+GRANT UPDATE ON _timescaledb_internal._hyper_1_1_chunk TO PUBLIC;
+\z _timescaledb_internal._hyper_1_1_chunk
+REVOKE SELECT ON _timescaledb_internal._hyper_1_1_chunk FROM PUBLIC;
+\z _timescaledb_internal._hyper_1_1_chunk
+
+-- Check that revoking a permission first on the chunk and then on the
+-- hypertable that was added through the hypertable (INSERT and
+-- SELECT, in this case) still do not copy permissions from the
+-- hypertable (so there should not be a select permission to public on
+-- the chunk but there should be one on the hypertable).
+GRANT INSERT ON conditions TO PUBLIC;
+\z conditions
+\z _timescaledb_internal._hyper_1_2_chunk
+REVOKE SELECT ON _timescaledb_internal._hyper_1_2_chunk FROM PUBLIC;
+REVOKE INSERT ON conditions FROM PUBLIC;
+\z conditions
+\z _timescaledb_internal._hyper_1_2_chunk
+
+-- Check that granting permissions through hypertable does not remove
+-- separate grants on chunk.
+GRANT UPDATE ON _timescaledb_internal._hyper_1_3_chunk TO PUBLIC;
+\z conditions
+\z _timescaledb_internal._hyper_1_3_chunk
+GRANT INSERT ON conditions TO PUBLIC;
+REVOKE INSERT ON conditions FROM PUBLIC;
+\z conditions
+\z _timescaledb_internal._hyper_1_3_chunk
