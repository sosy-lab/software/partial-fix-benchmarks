ERROR:  permission denied for table _hyper_1_12_chunk
Any update?
It is not very clear how your setup looks, but this looks more like a PostgreSQL issue rather than a TimescaleDB issue.

Have you checked the that you have default permissions set for the schema `_timescaledb_internal` (see [ALTER DEFAULT PRIVILEGES](https://www.postgresql.org/docs/current/sql-alterdefaultprivileges.html)?
@mkindahl I'll check the privileges and update you.
I investigated this a little further. It seems like the permissions does not propagate from hypertables to chunks. This is not a problem when accessing the chunks through the hypertable, since that code avoids the permissions, but when accessing the chunks directly, as `pg_dump` does, it will not work.

```
CREATE TABLE conditions(
    time TIMESTAMPTZ NOT NULL,
    device INTEGER,
    temperature FLOAT
);
SELECT * FROM create_hypertable('conditions', 'time', chunk_time_interval => '7 days'::interval);
 hypertable_id | schema_name | table_name | created 
---------------+-------------+------------+---------
             1 | public      | conditions | t
(1 row)

INSERT INTO conditions
SELECT time, (random()*30)::int, random()*80 - 40
FROM generate_series('2018-12-01 00:00'::timestamp, '2018-12-14 00:00'::timestamp, '1h') AS time;
\z conditions
                               Access privileges
 Schema |    Name    | Type  | Access privileges | Column privileges | Policies 
--------+------------+-------+-------------------+-------------------+----------
 public | conditions | table |                   |                   | 
(1 row)

\z _timescaledb_internal.*chunk
                                          Access privileges
        Schema         |       Name       | Type  | Access privileges | Column privileges | Policies 
-----------------------+------------------+-------+-------------------+-------------------+----------
 _timescaledb_internal | _hyper_1_1_chunk | table |                   |                   | 
 _timescaledb_internal | _hyper_1_2_chunk | table |                   |                   | 
 _timescaledb_internal | _hyper_1_3_chunk | table |                   |                   | 
(3 rows)

GRANT SELECT ON conditions TO PUBLIC;
\z conditions
                                     Access privileges
 Schema |    Name    | Type  |       Access privileges       | Column privileges | Policies 
--------+------------+-------+-------------------------------+-------------------+----------
 public | conditions | table | super_user=arwdDxt/super_user+|                   | 
        |            |       | =r/super_user                 |                   | 
(1 row)

\z _timescaledb_internal.*chunk
                                          Access privileges
        Schema         |       Name       | Type  | Access privileges | Column privileges | Policies 
-----------------------+------------------+-------+-------------------+-------------------+----------
 _timescaledb_internal | _hyper_1_1_chunk | table |                   |                   | 
 _timescaledb_internal | _hyper_1_2_chunk | table |                   |                   | 
 _timescaledb_internal | _hyper_1_3_chunk | table |                   |                   | 
(3 rows)
```

Solution is probably to propagate permissions when doing a `GRANT` or `REVOKE` and to copy the permissions from the hypertable when creating a new chunk.
@mkindahl
I have tried (ALTER DEFAULT PRIVILEGES) on `_timescaledb_internal` and it worked.
Thank You that solved the problem with pg_dump command.
This can be especially nasty if you removed a role with chunk permissions before this bug was fixed; the chunks will retain permissions from nameless roles identified by their former `uid`, which will cause tools like pg_upgrade to fail since it can't grant those permissions in the new instance.

I was bit by this on Timescale Cloud, and after working with their support team, the only solution we came up with was to drop those chunks and re-insert the data into the hypertable. This can be done with a transaction for each chunk by copying the data to a temp table before dropping the chunk; this lets concurrent queries always see complete data.