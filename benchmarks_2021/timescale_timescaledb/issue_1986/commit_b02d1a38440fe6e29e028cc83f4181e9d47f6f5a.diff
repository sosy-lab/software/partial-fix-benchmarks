diff --git a/src/chunk.c b/src/chunk.c
index bd7edb9bdd..c8bab72ed7 100644
--- a/src/chunk.c
+++ b/src/chunk.c
@@ -1075,6 +1075,14 @@ ts_chunk_find_or_create_without_cuts(Hypertable *ht, Hypercube *hc, const char *
 
 	if (NULL == stub)
 	{
+		ScanTupLock tuplock = {
+			.lockmode = LockTupleKeyShare,
+			.waitpolicy = LockWaitBlock,
+		};
+
+		/* Lock all slices that already exist to ensure they remain when we
+		 * commit since we won't create those slices ourselves. */
+		ts_hypercube_find_existing_slices(hc, &tuplock);
 		chunk = chunk_create_from_hypercube_after_lock(ht, hc, schema_name, table_name, NULL);
 
 		if (NULL != created)
diff --git a/src/dimension_slice.c b/src/dimension_slice.c
index d2c763be5e..d15f383a11 100644
--- a/src/dimension_slice.c
+++ b/src/dimension_slice.c
@@ -112,6 +112,12 @@ lock_result_ok_or_abort(TupleInfo *ti, DimensionSlice *slice)
 
 #if PG12_GE
 		case TM_Deleted:
+			ereport(ERROR,
+					(errcode(ERRCODE_LOCK_NOT_AVAILABLE),
+					 errmsg("dimension slice %d deleted by other transaction", slice->fd.id),
+					 errhint("Retry the operation again.")));
+			pg_unreachable();
+			break;
 #endif
 		case TM_Updated:
 			ereport(ERROR,
@@ -146,9 +152,26 @@ static ScanTupleResult
 dimension_vec_tuple_found(TupleInfo *ti, void *data)
 {
 	DimensionVec **slices = data;
-	DimensionSlice *slice = dimension_slice_from_slot(ti->slot);
+	DimensionSlice *slice;
 
-	lock_result_ok_or_abort(ti, slice);
+	switch (ti->lockresult)
+	{
+		case TM_SelfModified:
+		case TM_Ok:
+			break;
+#if PG12_GE
+		case TM_Deleted:
+#endif
+		case TM_Updated:
+			/* Treat as not found */
+			return SCAN_CONTINUE;
+		default:
+			elog(ERROR, "unexpected tuple lock status: %d", ti->lockresult);
+			pg_unreachable();
+			break;
+	}
+
+	slice = dimension_slice_from_slot(ti->slot);
 	*slices = ts_dimension_vec_add_slice(slices, slice);
 
 	return SCAN_CONTINUE;
@@ -528,7 +551,23 @@ dimension_slice_fill(TupleInfo *ti, void *data)
 	bool should_free;
 	HeapTuple tuple = ts_scanner_fetch_heap_tuple(ti, false, &should_free);
 
-	memcpy(&(*slice)->fd, GETSTRUCT(tuple), sizeof(FormData_dimension_slice));
+	switch (ti->lockresult)
+	{
+		case TM_SelfModified:
+		case TM_Ok:
+			memcpy(&(*slice)->fd, GETSTRUCT(tuple), sizeof(FormData_dimension_slice));
+			break;
+#if PG12_GE
+		case TM_Deleted:
+#endif
+		case TM_Updated:
+			/* Same as not found */
+			break;
+		default:
+			elog(ERROR, "unexpected tuple lock status: %d", ti->lockresult);
+			pg_unreachable();
+			break;
+	}
 
 	if (should_free)
 		heap_freetuple(tuple);
@@ -545,7 +584,7 @@ dimension_slice_fill(TupleInfo *ti, void *data)
  * otherwise.
  */
 bool
-ts_dimension_slice_scan_for_existing(DimensionSlice *slice)
+ts_dimension_slice_scan_for_existing(DimensionSlice *slice, ScanTupLock *tuplock)
 {
 	ScanKeyData scankey[3];
 
@@ -573,7 +612,7 @@ ts_dimension_slice_scan_for_existing(DimensionSlice *slice)
 		&slice,
 		1,
 		AccessShareLock,
-		NULL,
+		tuplock,
 		CurrentMemoryContext);
 }
 
@@ -747,7 +786,11 @@ dimension_slice_insert_relation(Relation rel, DimensionSlice *slice)
 /*
  * Insert slices into the catalog.
  *
- * Only slices that don't already exists in the catalog will be inserted.
+ * Only slices that don't already exist in the catalog will be inserted. Note
+ * that all slices that already exist (i.e., have a valid ID) MUST be locked
+ * with a tuple lock (e.g., FOR KEY SHARE) prior to calling this function
+ * since they won't be created. Otherwise it is not possible to guarantee that
+ * all slices still exist once the transaction commits.
  *
  * Returns the number of slices inserted.
  */
@@ -762,9 +805,6 @@ ts_dimension_slice_insert_multi(DimensionSlice **slices, Size num_slices)
 
 	for (i = 0; i < num_slices; i++)
 	{
-		slices[i]->fd.id = 0;
-		ts_dimension_slice_scan_for_existing(slices[i]);
-
 		if (slices[i]->fd.id == 0)
 		{
 			dimension_slice_insert_relation(rel, slices[i]);
diff --git a/src/dimension_slice.h b/src/dimension_slice.h
index 2b129b8c0d..9e05a21560 100644
--- a/src/dimension_slice.h
+++ b/src/dimension_slice.h
@@ -50,7 +50,7 @@ ts_dimension_slice_scan_range_limit(int32 dimension_id, StrategyNumber start_str
 									int limit, ScanTupLock *tuplock);
 extern DimensionVec *ts_dimension_slice_collision_scan_limit(int32 dimension_id, int64 range_start,
 															 int64 range_end, int limit);
-extern bool ts_dimension_slice_scan_for_existing(DimensionSlice *slice);
+extern bool ts_dimension_slice_scan_for_existing(DimensionSlice *slice, ScanTupLock *tuplock);
 extern DimensionSlice *ts_dimension_slice_scan_by_id_and_lock(int32 dimension_slice_id,
 															  ScanTupLock *tuplock,
 															  MemoryContext mctx);
diff --git a/src/hypercube.c b/src/hypercube.c
index 3ade9dd030..47291e9071 100644
--- a/src/hypercube.c
+++ b/src/hypercube.c
@@ -211,6 +211,35 @@ ts_hypercube_from_constraints(ChunkConstraints *constraints, MemoryContext mctx)
 	return hc;
 }
 
+/*
+ * Find slices in the hypercube that already exists in metadata.
+ *
+ * If a slice exists in metadata, the slice ID will be filled in on the
+ * existing slice in the hypercube. Optionally, also lock the slice when
+ * found.
+ */
+int
+ts_hypercube_find_existing_slices(Hypercube *cube, ScanTupLock *tuplock)
+{
+	int i;
+	int num_found = 0;
+
+	for (i = 0; i < cube->num_slices; i++)
+	{
+		/*
+		 * Check if there's already an existing slice with the calculated
+		 * range. If a slice already exists, use that slice's ID instead
+		 * of a new one.
+		 */
+		bool found = ts_dimension_slice_scan_for_existing(cube->slices[i], tuplock);
+
+		if (found)
+			num_found++;
+	}
+
+	return num_found;
+}
+
 /*
  * Calculate the hypercube that encloses the given point.
  *
@@ -278,11 +307,8 @@ ts_hypercube_calculate_from_point(Hyperspace *hs, Point *p, ScanTupLock *tuplock
 			 * Check if there's already an existing slice with the calculated
 			 * range. If a slice already exists, use that slice's ID instead
 			 * of a new one.
-			 *
-			 * The tuples are already locked in
-			 * `chunk_create_from_point_after_lock`, so nothing to do here.
 			 */
-			ts_dimension_slice_scan_for_existing(cube->slices[i]);
+			ts_dimension_slice_scan_for_existing(cube->slices[i], tuplock);
 		}
 	}
 
diff --git a/src/hypercube.h b/src/hypercube.h
index 76bd1f2797..9f911e6769 100644
--- a/src/hypercube.h
+++ b/src/hypercube.h
@@ -30,6 +30,7 @@ extern TSDLLEXPORT Hypercube *ts_hypercube_alloc(int16 num_dimensions);
 extern void ts_hypercube_free(Hypercube *hc);
 extern TSDLLEXPORT void ts_hypercube_add_slice(Hypercube *hc, DimensionSlice *slice);
 extern Hypercube *ts_hypercube_from_constraints(ChunkConstraints *constraints, MemoryContext mctx);
+extern int ts_hypercube_find_existing_slices(Hypercube *cube, ScanTupLock *tuplock);
 extern Hypercube *ts_hypercube_calculate_from_point(Hyperspace *hs, Point *p, ScanTupLock *tuplock);
 extern bool ts_hypercubes_collide(Hypercube *cube1, Hypercube *cube2);
 extern TSDLLEXPORT DimensionSlice *ts_hypercube_get_slice_by_dimension_id(Hypercube *hc,
diff --git a/test/isolation/expected/dropchunks_race.out b/test/isolation/expected/dropchunks_race.out
index 89297f27b6..9234820a8f 100644
--- a/test/isolation/expected/dropchunks_race.out
+++ b/test/isolation/expected/dropchunks_race.out
@@ -1,22 +1,44 @@
-Parsed test spec with 3 sessions
+Parsed test spec with 5 sessions
 
-starting permutation: s1a s2a s3a s3b
+starting permutation: s3_chunks_found_wait s1_drop_chunks s2_drop_chunks s3_chunks_found_release s3_show_missing_slices
+step s3_chunks_found_wait: SELECT debug_waitpoint_enable('drop_chunks_chunks_found');
 debug_waitpoint_enable
 
                
-step s1a: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); <waiting ...>
-step s2a: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); <waiting ...>
-step s3a: SELECT debug_waitpoint_release('drop_chunks_chunks_found');
+step s1_drop_chunks: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); <waiting ...>
+step s2_drop_chunks: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); <waiting ...>
+step s3_chunks_found_release: SELECT debug_waitpoint_release('drop_chunks_chunks_found');
 debug_waitpoint_release
 
                
-step s1a: <... completed>
+step s1_drop_chunks: <... completed>
 count          
 
 1              
-step s2a: <... completed>
-error in steps s3a s1a s2a: ERROR:  some chunks could not be read since they are being concurrently updated
-step s3b: SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice);
+step s2_drop_chunks: <... completed>
+count          
+
+0              
+step s3_show_missing_slices: SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice);
+count          
+
+0              
+
+starting permutation: s4_chunks_dropped_wait s1_drop_chunks s4_insert_new_chunk s4_chunks_dropped_release s3_show_missing_slices
+step s4_chunks_dropped_wait: SELECT debug_waitpoint_enable('drop_chunks_end');
+debug_waitpoint_enable
+
+               
+step s1_drop_chunks: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01');
+count          
+
+1              
+step s4_insert_new_chunk: INSERT INTO dropchunks_race_t1 VALUES ('2020-03-01 10:30', 1, 32.2);
+step s4_chunks_dropped_release: SELECT debug_waitpoint_release('drop_chunks_end');
+debug_waitpoint_release
+
+               
+step s3_show_missing_slices: SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice);
 count          
 
 0              
diff --git a/test/isolation/specs/dropchunks_race.spec.in b/test/isolation/specs/dropchunks_race.spec.in
index cbd8cf0faf..25c8a0d094 100644
--- a/test/isolation/specs/dropchunks_race.spec.in
+++ b/test/isolation/specs/dropchunks_race.spec.in
@@ -8,10 +8,10 @@ setup {
   SELECT create_hypertable('dropchunks_race_t1', 'time', 'device', 2);
   INSERT INTO dropchunks_race_t1 VALUES ('2020-01-03 10:30', 1, 32.2);
 
-  CREATE FUNCTION debug_waitpoint_enable(TEXT) RETURNS VOID LANGUAGE C VOLATILE STRICT
+  CREATE OR REPLACE FUNCTION debug_waitpoint_enable(TEXT) RETURNS VOID LANGUAGE C VOLATILE STRICT
   AS '@TS_MODULE_PATHNAME@', 'ts_debug_waitpoint_enable';
 
-  CREATE FUNCTION debug_waitpoint_release(TEXT) RETURNS VOID LANGUAGE C VOLATILE STRICT
+  CREATE OR REPLACE FUNCTION debug_waitpoint_release(TEXT) RETURNS VOID LANGUAGE C VOLATILE STRICT
   AS '@TS_MODULE_PATHNAME@', 'ts_debug_waitpoint_release';
 }
 
@@ -20,14 +20,26 @@ teardown {
 }
 
 session "s1"
-step "s1a"	{ SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); }
+step "s1_drop_chunks"	{ SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); }
 
 session "s2"
-step "s2a"	{ SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); }
+step "s2_drop_chunks"	{ SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); }
 
 session "s3"
-setup           { SELECT debug_waitpoint_enable('drop_chunks_chunks_found'); }
-step "s3a"      { SELECT debug_waitpoint_release('drop_chunks_chunks_found'); }
-step "s3b" 	{ SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice); }
-
-permutation "s1a" "s2a" "s3a" "s3b"
+step "s3_chunks_found_wait"           { SELECT debug_waitpoint_enable('drop_chunks_chunks_found'); }
+step "s3_chunks_found_release"      { SELECT debug_waitpoint_release('drop_chunks_chunks_found'); }
+step "s3_show_missing_slices"	{ SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice); }
+
+# Test race between drop_chunks and an insert into a new chunk.  The
+# new chunk will share a slice with the chunk that is about to be
+# dropped. The shared slice must persist after drop_chunks completes,
+# or otherwise the new chunk will lack one slice.
+session "s4"
+step "s4_chunks_dropped_wait"         { SELECT debug_waitpoint_enable('drop_chunks_end'); }
+step "s4_chunks_dropped_release"      { SELECT debug_waitpoint_release('drop_chunks_end'); }
+
+session "s5"
+step "s4_insert_new_chunk" { INSERT INTO dropchunks_race_t1 VALUES ('2020-03-01 10:30', 1, 32.2); }
+
+permutation "s3_chunks_found_wait" "s1_drop_chunks" "s2_drop_chunks" "s3_chunks_found_release" "s3_show_missing_slices"
+permutation "s4_chunks_dropped_wait" "s1_drop_chunks" "s4_insert_new_chunk" "s4_chunks_dropped_release" "s3_show_missing_slices"
