Do not crash on broken tables

If a hypertable accidentally broke because a dimension slice is
missing, a segmentation fault will result when an attempt is made to
remove a chunk that references the dimension slice. Instead of crashing
the server, this commit prints a warning that the dimension slice did
not exist and proceed with removing the chunk.
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

If a hypercube is built from constraints in
`ts_hypercube_from_constraints`, which can happen during a run of
`drop_chunks` or when a chunk is explicitly dropped as part of other
operations, dimension slices will be removed and the hypercube will
contain references that are not valid.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slice tuple when scanning

In the function `ts_hypercube_from_constraints` a hypercube is build
from constraints which reference dimension slices in `dimension_slice`.
As part of a run of `drop_chunks` or when a chunk is explicitly dropped
as part of other operations, dimension slices can be removed from this
table causing the dimension slices to be removed, which makes the
hypercube reference non-existent dimension slices which subsequently
causes a crash.

This commit fixes this by adding a tuple lock on the dimension slices
that are used to build the hypercube.

If two `drop_chunks` are running concurrently, there can be a race if
dimension slices are removed as a result removing a chunk. We treat
this case in the same way as if the dimension slice was updated: report
an error that another session locked the tuple.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes sure that, whenever a new chunk is created, any
dimension slices that existed prior to creating the new chunk are
locked to prevent them from being dropped before the chunk-creating
transaction commits.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes sure that, whenever a new chunk is created, any
dimension slices that existed prior to creating the new chunk are
locked to prevent them from being dropped before the chunk-creating
transaction commits.

A prior commit (PR #2150) partially solved this problem, but didn't
lock all the slices. That commit also threw an error when a lock on a
slice could not be taken due to the slice being deleted by another
transaction. This is now fixed to treat that case as a missing slice
instead, causing it to be recreated.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes sure that, whenever a new chunk is created, any
dimension slices that existed prior to creating the new chunk are
locked to prevent them from being dropped before the chunk-creating
transaction commits.

A prior commit (PR #2150) partially solved this problem, but didn't
lock all the slices. That commit also threw an error when a lock on a
slice could not be taken due to the slice being deleted by another
transaction. This is now fixed to treat that case as a missing slice
instead, causing it to be recreated.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes sure that, whenever a new chunk is created, any
dimension slices that existed prior to creating the new chunk are
locked to prevent them from being dropped before the chunk-creating
transaction commits.

A prior commit (PR #2150) partially solved this problem, but didn't
lock all the slices. That commit also threw an error when a lock on a
slice could not be taken due to the slice being deleted by another
transaction. This is now fixed to treat that case as a missing slice
instead, causing it to be recreated.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

The commit adds view `_timescaledb_internal.missing_dimension_slices`
that will show dimension slices that are missing from the dimension
slices table. This is intended for debugging and is an internal
function.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
Repair dimension slice table on update

In #2514 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Lock dimension slices when creating new chunk

This change makes two changes to address issues with processes doing
concurrent inserts and `drop_chunks` calls:

- When a new chunk is created, any dimension slices that existed prior
  to creating the new chunk are locked to prevent them from being
  dropped before the chunk-creating process commits.

- When a chunk is being dropped, concurrent inserts into the chunk
  that is being dropped will try to lock the dimension slices of the
  chunk. In case the locking fails (due to the slices being
  concurrently deleted), the insert process will treat the chunk as
  not existing and will instead recreate it. Previously, the chunk
  slices (and thus chunk) would be found, but the insert would fail
  when committing since the chunk was concurrently deleted.

A prior commit (PR #2150) partially solved a related problem, but
didn't lock all the slices of a chunk. That commit also threw an error
when a lock on a slice could not be taken due to the slice being
deleted by another transaction. This is now changed to treat that case
as a missing slice instead, causing it to be recreated.

Fixes #1986
Repair dimension slice table on update

This is a backport of #2558.

In #2800 a a race condition between inserts and `drop_chunks` is fixed
and this commit will repair the dimension slices table by
re-constructing missing dimension slices from the corresponding
constraint expressions.

Closes #1986
