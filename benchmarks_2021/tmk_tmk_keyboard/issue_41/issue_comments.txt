BootMagic configured accidentally
In devlopment BootMagic can be very confusing. In initial stage of development matrix code might not work well for example I often register all keys down with reading ports reverce logic and this cuases wrongly configured layout , unexistent default layer or even avoiding keyboard coming up.

YOU SHOULD DISABLE BootMagic IN INITIAL STAGE OF DEVELOPMENT.

Added additive salt key to avoid unintentional configuration at boot time. Default salt key is Space.
The key is configurable in config.h. See common/bootmagic.h

Another victims. They seem to use not fixed version. but people tends to press keys accidentally/pointlessly during plugin and enumerating. Also people don't seem to read document. 

More protection may be needed. Recovering with replug will be safe for unintentional users.
Maybe BootMagic should just change configuration but not store it in EEPROM and user should store it with command intentionally?

http://geekhack.org/index.php?topic=35065.msg981639#msg981639
http://geekhack.org/index.php?topic=35065.msg981754#msg981754

i would suggest to disable it in the makefiles. if someone needs that feature in particular he or she can still enable it (maybe more documentation on those options might be usefull).
i my opinion "standard" behaviour should be be standard settings in the makefiles. especially if that feature could mess up how the keyboard works.
