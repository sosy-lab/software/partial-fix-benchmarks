Ada: bad structure's detection
Thank you. I introduced code for skipping "overriding" and "not overriding". See #2383 .
However, Ada parser still cannot capture "Coucou" in the first example input.
This is true even when I delete the "overriding" indicator from the input.

I don't know Ada. So I need your help.
It seems that the block wrapping Coucou is an interface explained in https://en.wikibooks.org/wiki/Ada_Programming/Tasking#Interfaces . Am I correct?
Yes, that's right, and that's also how we define standard tasks and task type in specification (.ads).
Task : 
```Ada
   task Tasche_T is
      entry Coucou
         (Parameters : Parameters_Type);
   end Tasche_T;
```
Task type : 
```Ada
   task type Tasche_T is
      entry Coucou
         (Parameters : Parameters_Type);
   end Tasche_T;
```
```
[jet@living]~/var/ctags% git diff | cat
git diff | cat
diff --git a/parsers/ada.c b/parsers/ada.c
index 22a7dc01..73b4a458 100644
--- a/parsers/ada.c
+++ b/parsers/ada.c
@@ -929,7 +929,9 @@ static adaTokenInfo *adaParseBlock(adaTokenInfo *parent, adaKind kind)
       else if(adaKeywordCmp(ADA_KEYWORD_NEW))
       {
         /* if this is a "new" something then no need to parse */
-        skipPast(";");
+        // skipPast(";");
+		skipPastKeyword(ADA_KEYWORD_WITH);
+		adaParse(ADA_DECLARATIONS, token);
       }
       else
       {
[jet@living]~/var/ctags% make
make
REPOINFO   main/repoinfo.h
make  all-am
make[1]: Entering directory '/home/jet/var/ctags'
REPOINFO   main/repoinfo.h
  CCLD     ctags
make[1]: Leaving directory '/home/jet/var/ctags'
[jet@living]~/var/ctags% ./ctags --kinds-Ada=+E -o - ~/var/ctags/Units/parser-ada.r/ada-overriding.d/input.ads
./ctags --kinds-Ada=+E -o - ~/var/ctags/Units/parser-ada.r/ada-overriding.d/input.ads
Coucou	/home/jet/var/ctags/Units/parser-ada.r/ada-overriding.d/input.ads	/^      entry Coucou;$/;"	E	taskspec:Tasche_T
Input	/home/jet/var/ctags/Units/parser-ada.r/ada-overriding.d/input.ads	/^package Input$/;"	P
Inutile	/home/jet/var/ctags/Units/parser-ada.r/ada-overriding.d/input.ads	/^   procedure Inutile$/;"	R	packspec:Input
Tasche_T	/home/jet/var/ctags/Units/parser-ada.r/ada-overriding.d/input.ads	/^   task type Tasche_T is new Carte_T with$/;"	K	packspec:Input
```
Coucou is captured well. However, the change is too specialized for the target input.
So more work is needed.
Need more code examples ?
> Need more code examples ?

Currently, the examples are enough. Thank you for the offering.
What I'm looking for is spare time that can be used for developing ctags:-P.
See #2401. Now Coucou is captured well.

I would like to extend our units test cases for Ada. Could you help me?

The examples you showed:

```
   task Tasche_T is
      entry Coucou
         (Parameters : Parameters_Type);
   end Tasche_T;
```
and
```
task type Tasche_T is
      entry Coucou
         (Parameters : Parameters_Type);
   end Tasche_T;
```

I would like to get them as full ads files acceptable by a compiler.
See https://github.com/universal-ctags/ctags/issues/1903 .

I guess what you showed is code fragments.
So I guess an Ada compiler may not accept them.
I would like to something compiler acceptable input files.
However, it should be small.

What I know is only C. So I would like to explain what I want in C.

Not acceptable:
```
printf("hello, world\n");
```
Acceptable:
```
#include <stdio.h>
int
main(void)
{
   printf("hello, world\n");
   return 0;
}
```

A one file exemple, in `client.adb` : 

```Ada
with Ada.Text_IO;

procedure Client is

   ---------------------------------------------------------------------------
   --  Define the tasks
   task Ma_Tasche is
      entry Accepter
         (Continuer : Boolean);
   end Ma_Tasche;

   task Mon_Autre_Tasche;

   --  Define body of the tasks

   task body Ma_Tasche is
      Var_Continuer : Boolean := False;
   begin
      Boucle_Principale :
      loop
         accept Accepter
            (Continuer : Boolean)
         do
            Var_Continuer := Continuer;
         end Accepter;
      end loop Boucle_Principale;
   end Ma_Tasche;

   task body Mon_Autre_Tasche is
   begin
      null;
   end Mon_Autre_Tasche;
   ---------------------------------------------------------------------------

begin

   Ada.Text_IO.Put_Line ("Tasks won't stop, kill it with CTRL-C");
   Ma_Tasche.Accepter (Continuer => True);

end Client;
```
You need FSF gcc-ada version or GNAT on the Adacore's website.
Compile it with
* `gcc -c client.adb;` (and link with `gnatbind client; gnatlink client` if you would an exe) with the FSF gcc.
* or `gnatmake client.adb` 
* or create a project file `client.gpr` and compile with `gprbuild -Pclient.gpr`
I'm going to add a multi-file version soon.
The multi file version:

In a file named `client.gpr`
```ada
project Client is

   for Main use ("client.adb");

   for Source_Dirs use ("src/**");
   for Object_Dir  use "obj/";
   for Exec_Dir    use "bin/";

   for Create_Missing_Dirs use "True";

   package Compiler is
      for Default_Switches ("Ada")  use ("-O0", "-Wall");
   end Compiler;

end Client;
```

In a file named `src/client.adb`
```ada
with Ada.Text_IO;
with Mes_Tasches_P;

procedure Client is

begin

   Ada.Text_IO.Put_Line ("Tasks won't stop, kill it with CTRL-C");
   Mes_Tasches_P.Ma_Tasche.Accepter (Continuer => True);

end Client;
```

In a file named `src/mes_tasches_p.ads`
```ada
package Mes_Tasches_P is

   task Ma_Tasche is
      entry Accepter
         (Continuer : Boolean);
   end Ma_Tasche;

   task Mon_Autre_Tasche;

end Mes_Tasches_P;
```

In a file named `src/mes_tasches_p.adb`
```ada
package body Mes_Tasches_P is

   task body Ma_Tasche is
      Var_Continuer : Boolean := False;
   begin
      Boucle_Principale :
      loop
         accept Accepter
            (Continuer : Boolean)
         do
            Var_Continuer := Continuer;
         end Accepter;
      end loop Boucle_Principale;
   end Ma_Tasche;

   task body Mon_Autre_Tasche is
   begin
      null;
   end Mon_Autre_Tasche;

end Mes_Tasches_P;
```

Result of the command `tree .`
```sh
.
├── client.gpr
└── src
    ├── mes_tasches_p.adb
    ├── mes_tasches_p.ads
    └── client.adb
```


Compile it with `gprbuild -Pclient.gpr`
I forgot the task type,task type with discriminant, protected, protected type, protected type with discriminant.

File `mes_tasches_p.ads`
```ada
package Mes_Tasches_P is

   task Ma_Tasche is
      entry Accepter
         (Continuer : Boolean);
   end Ma_Tasche;

   task Mon_Autre_Tasche;

   task type Tasche_Type_1_T;

   Une_Tasche : Tasche_Type_1_T;

   task type Tasche_Type_2_T is
      entry Start;
      entry Lire
         (Donnee : out Integer);
   end Tasche_Type_2_T;

   --  Task type with discriminant.
   task type Tasche_Type_3_T
      --  We could have any number of arguments in discriminant
      --  Work exactly like argument in procedure/function/entry/accept
      (Taille : Integer)
   is
      entry Start;
   end Tasche_Type_3_T;

   --  protected objects.

   protected Objet_Protege is
      entry Demarrer;
      procedure Faire;
      function Tester return Boolean;
   private
      Variable : Boolean := True;
   end Objet_Protege;

   protected type Type_Protege is
      entry Demarrer;
      procedure Faire;
      function Tester return Boolean;
   private
      Variable : Boolean := True;
   end Type_Protege;

   protected type Discriminant_Protege
      (Priorite : Natural)
   is
      entry Demarrer;
      procedure Faire;
      function Tester return Boolean;
   private
      Variable : Boolean := True;
   end Discriminant_Protege;

end Mes_Tasches_P;
```

File `mes_tasches_p.adb`
```ada
package body Mes_Tasches_P is

   ---------------------------------------------------------------------------
   task body Ma_Tasche is
      Var_Continuer : Boolean := False;
   begin
      Boucle_Principale :
      loop
         accept Accepter
            (Continuer : Boolean)
         do
            Var_Continuer := Continuer;
         end Accepter;
      end loop Boucle_Principale;
   end Ma_Tasche;
   ---------------------------------------------------------------------------

   ---------------------------------------------------------------------------
   task body Mon_Autre_Tasche is
   begin
      null;
   end Mon_Autre_Tasche;
   ---------------------------------------------------------------------------

   Une_Autre_Tasche_1 : Tasche_Type_1_T;

   ---------------------------------------------------------------------------
   task body Tasche_Type_1_T is
   begin
      null;
   end Tasche_Type_1_T;
   ---------------------------------------------------------------------------

   ---------------------------------------------------------------------------
   task body Tasche_Type_2_T is
   begin
      null;
   end Tasche_Type_2_T;
   ---------------------------------------------------------------------------

   ---------------------------------------------------------------------------
   task body Tasche_Type_3_T is
   begin
      null;
   end Tasche_Type_3_T;
   ---------------------------------------------------------------------------

   Une_Autre_Tasche_2 : Tasche_Type_2_T;
   Une_Autre_Tasche_3 : Tasche_Type_3_T (Taille => 5);

   ---------------------------------------------------------------------------
   protected body Objet_Protege is
      entry Demarrer
         when Variable
      is
      begin
         null;
      end Demarrer;

      procedure Faire is
      begin
         null;
      end Faire;

      function Tester
         return Boolean
      is
      begin
         return Variable;
      end Tester;
   end Objet_Protege;
   ---------------------------------------------------------------------------

   ---------------------------------------------------------------------------
   protected body Type_Protege is
      entry Demarrer
         when Variable
      is
      begin
         null;
      end Demarrer;

      procedure Faire is
      begin
         null;
      end Faire;

      function Tester
         return Boolean
      is
      begin
         return Variable;
      end Tester;
   end Type_Protege;
   ---------------------------------------------------------------------------

   ---------------------------------------------------------------------------
   protected body Discriminant_Protege is
      entry Demarrer
         when Variable
      is
      begin
         null;
      end Demarrer;

      procedure Faire is
      begin
         null;
      end Faire;

      function Tester
         return Boolean
      is
      begin
         return Variable;
      end Tester;
   end Discriminant_Protege;
   ---------------------------------------------------------------------------

end Mes_Tasches_P;
```

No change in `client.adb` and `client.gpr`.
Thank you. It is successfully built.
I will convert it to a test case.
The test case is integrated. Thank you very much.

I have one more request.
In Units/review-needed.r directory. there is a test case for Ada, generalized_stack.ads.t.
The directory contains test cases keeping as is since we started this project by forking from exuberant-ctags. When forking, I didn't know Ada, so the test case was not tested really; the output of ctags for the input has never been verified yet.

Now, you are here. So I would like you to help me verify the expected output.

```
     1	-- Object-oriented generalized stack.  This illustrates the use of a
     2	-- controlled type, which is one that has construction and destructions.
     3	-- It also shows how to create two related types in the same package so
     4	-- that they can share private declarations.  This sort of thing is
     5	-- accomplished in Java or C++ using nested classes, or using friend
     6	-- declarations in C++.
     7	--
     8	with Ada.Finalization; use Ada.Finalization;
     9	
    10	package GenStack is
    11	    -- This is the stack type.
    12	    type Stack is new Controlled with private;
    13	
    14	    -- This is the base type for nodes.  Client packages must derive their
    15	    -- nodes from StackData.  Since it comes from Controlled, the user can
    16	    -- override the Initialize, Adjust, and Finalize methods as needed.
    17	    type StackData is new Controlled with null record;
    18	
    19	    -- Initialization operations.
    20	    procedure Initialize(S: in out Stack);
    21	    procedure Adjust(S: in out Stack);
    22	    procedure Finalize(S: in out Stack);
    23	
    24	    -- Stack operations.
    25	    procedure Push(S: in out Stack; D: StackData'class);
    26	    procedure Pop(S: in out Stack; D: in out StackData'class);
    27	    procedure Top(S: Stack; Data: in out StackData'class);
    28	    function Empty(S: Stack) return Boolean;
    29	
    30	    private
    31	    -- Pointer to the node type.
    32	    type Node;
    33	    type Node_Ptr is access Node;
    34	
    35	    -- Here is the generalized stack itself.  We would just make it the
    36	    -- pointer itself, but it needs to be a record so it can be in a with.
    37	    type Stack is new Controlled with record
    38	        Head: Node_Ptr;
    39	    end record;
    40	
    41	    -- Now, we need a pointer to the data part.
    42	    type Data_Ptr is access StackData'Class;
    43	
    44	    -- This is the node type.
    45	    type Node is record
    46	        Data: Data_Ptr;
    47	        Next: Node_Ptr;
    48	    end record;
    49	
    50	end GenStack;
```

During working on this pull request, I have studied Ada a bit. So I understand what we should tag mostly. However, there is one I cannot decide whether it should be tagged or not.

See the line 32:
```
    32	    type Node;
```

Do you think we should tag `Node`?
The current implementation of Ada parser doesn't tag it.
So, if your answer is yes, Ada parser has a bug. I would like to fix it.
If your answer is no, I would like to hear the reason. I would like to put what you say to the test case.
Line 32 `type Node;` share the same meaning as `typedef struct s_List List;` in `C`, In fact I think ctags should tag it. Can we add an hint like `type name declaration` next to then name ? The idea is to distinguish it from the full statement line 45.

I didn't realize you have so few exemples, do you want me to write some more? 
Oh, I'm very sorry. u-ctags captures Node at line 32:
```
% u-ctags -n -o - --kinds-Ada='*' input.ads | grep ^Node
Node	input.ads	32;"	T	language:Ada	packspec:GenStack	file:
Node	input.ads	45;"	t	language:Ada	packspec:GenStack	file:
Node_Ptr	input.ads	33;"	t	language:Ada	packspec:GenStack	file:
```
I misread the output.

> I didn't realize you have so few exemples, do you want me to write some more?
Thank you for offering. However,  This time, I got many input from you. So it is enough now.

After adding --kinds-Ada=*, ctags captures Node at line 32.
However,  "Head" at line 38 is not captured though Data and Next at line 46 and line 47 are captured.
One more patch may be needed.
I merged the change for fixing this issue.
Thank you for providing test input and knowledges about Ada.