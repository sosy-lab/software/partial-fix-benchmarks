main: introduce experimental top-level symbol table

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: utilize symbol table to distinguish typedef from port

Close #2413.

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
SystemVerilog: use the symbol table to recognize the types for ports

Close #2413.

The original code could not capture "a" in "mod" in the following input because
the parser didn't recognize "int32_t" before "a" is a name of type:

    typedef bit[31:0] int32_t;
    module mod(
      input bit clk,
      input int32_t a
    );
    endmodule

This change makes the parser look up the cork symbol table when the
parser reads a token with unknown kind. A typedef like int32_t is
stored to the symbol table when making a tag for it. As the result, the
parser can resolve the kind for the token as typedef when parsing
int32_t in "input int32_t a".

Signed-off-by: Masatake YAMATO <yamato@redhat.com>
