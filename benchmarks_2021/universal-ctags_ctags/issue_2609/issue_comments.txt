Hope to add protection when to parse files
Do you think changing the behavior of -L option, skipping the input file with a warning message, fits your purpose?
> Do you think changing the behavior of -L option, skipping the input file with a warning message, fits your purpose?

I think it's ok.
The fix is merged. If you still have the trouble, please, reopen this.
Thank you for reporting.