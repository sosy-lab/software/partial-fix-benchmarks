diff --git a/docs/configuration.md b/docs/configuration.md
index 4cacaec..e66e582 100644
--- a/docs/configuration.md
+++ b/docs/configuration.md
@@ -34,6 +34,10 @@ set of ciphers that suits your needs.
 
 Normally you do not have to change this.
 
+TLSv1.3 uses an indepentent list of ciphers, use ``ciphers_v3`` to change the default value:
+
+    "TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256"
+
 ## Using multiple certificates
 
 To add multiple certificates to the hitch config, simply specify multiple ``pem-file``
diff --git a/hitch.conf.man.rst b/hitch.conf.man.rst
index 09df54d..b92810f 100644
--- a/hitch.conf.man.rst
+++ b/hitch.conf.man.rst
@@ -85,6 +85,8 @@ Chroot directory
 ciphers = ...
 -------------
 
+For TLSv1.1 and TLSv1.2 connections. For TLSv1.3, see ciphers_v3.
+
 List of ciphers to use in the secure communication. Refer to the
 OpenSSL documentation for a complete list of supported ciphers.
 
@@ -95,6 +97,18 @@ related OpenSSL's ciphers.
 
 This option is also available in frontend blocks.
 
+ciphers_v3 = ...
+----------------
+
+See ciphers.
+
+For TLSv1.3 connections, an independent list of ciphers is used. This list
+is made of new ciphers, which are not accepted by TLS1.1 and TLS1.2 and
+vica-versa, the old ciphers of TLS1.1 and TLS1.2 are not accepted by TLSv1.3
+
+To use TLSv1.1, TLSv1.2 along with TLSv1.3, both cipers and ciphers_v3 needs to be
+changed, if the default values are not desired.
+
 daemon = on|off
 ---------------
 
diff --git a/src/cfg_lex.l b/src/cfg_lex.l
index 60c1f8b..f9db343 100644
--- a/src/cfg_lex.l
+++ b/src/cfg_lex.l
@@ -47,6 +47,7 @@ char input_line[512];
 "TLSv1.2"			{ return (TOK_TLSv1_2); }
 "TLSv1.3"			{ return (TOK_TLSv1_3); }
 "ciphers"			{ return (TOK_CIPHERS); }
+"ciphers_v3"			{ return (TOK_CIPHERS_V3); }
 "ssl-engine"			{ return (TOK_SSL_ENGINE); }
 "prefer-server-ciphers"		{ return (TOK_PREFER_SERVER_CIPHERS); }
 "workers"			{ return (TOK_WORKERS); }
diff --git a/src/cfg_parser.y b/src/cfg_parser.y
index 37e01f2..8a91eff 100644
--- a/src/cfg_parser.y
+++ b/src/cfg_parser.y
@@ -26,6 +26,7 @@ void cfg_cert_file_free(struct cfg_cert_file **cfptr);
 int cfg_cert_vfy(struct cfg_cert_file *cf);
 void yyerror(hitch_config *, const char *);
 void cfg_cert_add(struct cfg_cert_file *cf, struct cfg_cert_file **dst);
+int tlsv1_3_enabled(void);
 
 static struct front_arg *cur_fa;
 static struct cfg_cert_file *cur_pem;
@@ -58,7 +59,7 @@ extern char input_line[512];
 %token TOK_SESSION_CACHE TOK_SHARED_CACHE_LISTEN TOK_SHARED_CACHE_PEER
 %token TOK_SHARED_CACHE_IF TOK_PRIVATE_KEY TOK_BACKEND_REFRESH
 %token TOK_OCSP_REFRESH_INTERVAL TOK_PEM_DIR TOK_PEM_DIR_GLOB
-%token TOK_LOG_LEVEL TOK_PROXY_TLV TOK_PROXY_AUTHORITY TOK_TFO
+%token TOK_LOG_LEVEL TOK_PROXY_TLV TOK_PROXY_AUTHORITY TOK_TFO TOK_CIPHERS_V3
 
 %parse-param { hitch_config *cfg }
 
@@ -77,6 +78,7 @@ CFG_RECORD
 	| BACKEND_REC
 	| PEM_FILE_REC
 	| CIPHERS_REC
+	| CIPHERS_V3_REC
 	| TLS_REC
 	| SSL_REC
 	| TLS_PROTOS_REC
@@ -153,6 +155,7 @@ FB_REC
 	| FB_SSL
 	| FB_TLS_PROTOS
 	| FB_CIPHERS
+	| FB_CIPHERS_V3
 	| FB_PREF_SRV_CIPH
 	;
 
@@ -334,6 +337,18 @@ FB_CIPHERS: TOK_CIPHERS '=' STRING {
 	if ($3) cur_fa->ciphers = strdup($3);
 };
 
+FB_CIPHERS_V3: TOK_CIPHERS_V3 '=' STRING {
+	if ($3) {
+		if (!tlsv1_3_enabled()) {
+			fprintf(stderr, "TLSv1.3 ciphers have been specified, but"
+				" the installed OpenSSL version does not support TLSv1.3. "
+				"for '%s'\n", input_line);
+			YYABORT;
+		}
+		cur_fa->ciphers_v3 = strdup($3);
+	}
+};
+
 FB_PREF_SRV_CIPH: TOK_PREFER_SERVER_CIPHERS '=' BOOL {
 	cur_fa->prefer_server_ciphers = $3;
 };
@@ -397,7 +412,14 @@ TLS_PROTO
 	| TOK_TLSv1_0 { cfg->SELECTED_TLS_PROTOS |= TLSv1_0_PROTO; }
 	| TOK_TLSv1_1 { cfg->SELECTED_TLS_PROTOS |= TLSv1_1_PROTO; }
 	| TOK_TLSv1_2 { cfg->SELECTED_TLS_PROTOS |= TLSv1_2_PROTO; }
-	| TOK_TLSv1_3 { cfg->SELECTED_TLS_PROTOS |= TLSv1_3_PROTO; };
+	| TOK_TLSv1_3 {
+	if (!tlsv1_3_enabled()) {
+		fprintf(stderr, "TLSv1.3 has been enabled, but the installed OpenSSL "
+			"version does not support it. for '%s'\n", input_line);
+		YYABORT;
+	}
+	cfg->SELECTED_TLS_PROTOS |= TLSv1_3_PROTO;
+};
 
 SSL_ENGINE_REC: TOK_SSL_ENGINE '=' STRING { if ($3) cfg->ENGINE = strdup($3); };
 
@@ -452,6 +474,17 @@ SNI_NOMATCH_ABORT_REC : TOK_SNI_NOMATCH_ABORT '=' BOOL {
 };
 
 CIPHERS_REC: TOK_CIPHERS '=' STRING { if ($3) cfg->CIPHER_SUITE = strdup($3); };
+CIPHERS_V3_REC: TOK_CIPHERS_V3 '=' STRING {
+	if ($3) {
+		if (!tlsv1_3_enabled()) {
+			fprintf(stderr, "TLSv1.3 ciphers have been specified, but"
+				" the installed OpenSSL version does not support TLSv1.3. "
+				"for '%s'\n", input_line);
+			YYABORT;
+		}
+		cfg->CIPHER_SUITE_V3 = strdup($3);
+	}
+};
 
 USER_REC: TOK_USER '=' STRING {
 	/* XXX: passing an empty string for file */
diff --git a/src/configuration.c b/src/configuration.c
index e9fa850..9bf8a62 100644
--- a/src/configuration.c
+++ b/src/configuration.c
@@ -40,6 +40,8 @@
 
 // BEGIN: configuration parameters
 #define CFG_CIPHERS "ciphers"
+#define CFG_CIPHERS_V3 "ciphers_v3"
+#define CFG_PARAM_CIPHERS_V3 48174
 #define CFG_SSL_ENGINE "ssl-engine"
 #define CFG_PREFER_SERVER_CIPHERS "prefer-server-ciphers"
 #define CFG_BACKEND "backend"
@@ -101,6 +103,9 @@
 #define CFG_DEFAULT_CIPHERS \
 	"EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH"
 
+#define CFG_DEFAULT_CIPHERS_V3 \
+	"TLS_AES_256_GCM_SHA384:TLS_CHACHA20_POLY1305_SHA256:TLS_AES_128_GCM_SHA256"
+
 extern FILE *yyin;
 extern int yyparse(hitch_config *);
 
@@ -202,6 +207,7 @@ config_new(void)
 	r->BACK_PORT			= strdup("8000");
 	r->NCORES			= 1;
 	r->CIPHER_SUITE			= strdup(CFG_DEFAULT_CIPHERS);
+	r->CIPHER_SUITE_V3		= strdup(CFG_DEFAULT_CIPHERS_V3);
 	r->ENGINE			= NULL;
 	r->BACKLOG			= 100;
 	r->SNI_NOMATCH_ABORT		= 0;
@@ -291,6 +297,7 @@ config_destroy(hitch_config *cfg)
 		cfg_cert_file_free(&cfg->CERT_DEFAULT);
 
 	free(cfg->CIPHER_SUITE);
+	free(cfg->CIPHER_SUITE_V3);
 	free(cfg->ENGINE);
 	free(cfg->PIDFILE);
 	free(cfg->OCSP_DIR);
@@ -636,6 +643,16 @@ cfg_cert_add(struct cfg_cert_file *cf, struct cfg_cert_file **dst)
 	HASH_ADD_KEYPTR(hh, *dst, cf->filename, strlen(cf->filename), cf);
 }
 
+int
+tlsv1_3_enabled(void)
+{
+#ifdef SSL_OP_NO_TLSv1_3
+	return (1);
+#else
+	return (0);
+#endif
+}
+
 #ifdef USE_SHARED_CACHE
 /* Parse mcast and ttl options */
 static int
@@ -829,6 +846,10 @@ config_param_validate(char *k, char *v, hitch_config *cfg,
 		if (strlen(v) > 0) {
 			config_assign_str(&cfg->CIPHER_SUITE, v);
 		}
+	} else if (strcmp(k, CFG_CIPHERS_V3) == 0) {
+		if (strlen(v) > 0) {
+			config_assign_str(&cfg->CIPHER_SUITE_V3, v);
+		}
 	} else if (strcmp(k, CFG_SSL_ENGINE) == 0) {
 		if (strlen(v) > 0) {
 			config_assign_str(&cfg->ENGINE, v);
@@ -1202,7 +1223,8 @@ config_print_usage_fd(char *prog, FILE *out)
 	fprintf(out, "\n");
 	fprintf(out, "      --tls                   TLSv1 (default. No SSLv3)\n");
 	fprintf(out, "      --ssl                   SSLv3 (enables SSLv3)\n");
-	fprintf(out, "  -c  --ciphers=SUITE         Sets allowed ciphers (Default: \"%s\")\n", config_disp_str(cfg->CIPHER_SUITE));
+	fprintf(out, "  -c  --ciphers=SUITE         Sets allowed ciphers for TLSv1.1 and TLSv1.2 (Default: \"%s\")\n", config_disp_str(cfg->CIPHER_SUITE));
+	fprintf(out, "      --ciphers_v3=SUITE      Sets allowed ciphers for TLSv1.3 (Default: \"%s\")\n", config_disp_str(cfg->CIPHER_SUITE_V3));
 	fprintf(out, "  -e  --ssl-engine=NAME       Sets OpenSSL engine (Default: \"%s\")\n", config_disp_str(cfg->ENGINE));
 	fprintf(out, "  -O  --prefer-server-ciphers Prefer server list order\n");
 	fprintf(out, "\n");
@@ -1366,6 +1388,7 @@ config_parse_cli(int argc, char **argv, hitch_config *cfg)
 		{ "ssl", 0, &ssl, 1},
 		{ "client", 0, &client, 1},
 		{ CFG_CIPHERS, 1, NULL, 'c' },
+		{ CFG_CIPHERS_V3, 1, NULL, CFG_PARAM_CIPHERS_V3 },
 		{ CFG_PREFER_SERVER_CIPHERS, 0, NULL, 'O' },
 		{ CFG_BACKEND, 1, NULL, 'b' },
 		{ CFG_FRONTEND, 1, NULL, 'f' },
@@ -1469,6 +1492,7 @@ CFG_ARG(CFG_PARAM_SEND_BUFSIZE, CFG_SEND_BUFSIZE);
 CFG_ARG(CFG_PARAM_RECV_BUFSIZE, CFG_RECV_BUFSIZE);
 CFG_ARG(CFG_PARAM_ALPN_PROTOS, CFG_ALPN_PROTOS);
 CFG_ARG('c', CFG_CIPHERS);
+CFG_ARG(CFG_PARAM_CIPHERS_V3, CFG_CIPHERS_V3);
 CFG_ARG('e', CFG_SSL_ENGINE);
 CFG_ARG('b', CFG_BACKEND);
 CFG_ARG('f', CFG_FRONTEND);
diff --git a/src/configuration.h b/src/configuration.h
index 929b7e3..8429766 100644
--- a/src/configuration.h
+++ b/src/configuration.h
@@ -87,6 +87,7 @@ struct front_arg {
 	int			sni_nomatch_abort;
 	int			prefer_server_ciphers;
 	char			*ciphers;
+	char			*ciphers_v3;
 	int			selected_protos;
 	int			mark;
 	UT_hash_handle		hh;
@@ -117,6 +118,7 @@ struct __hitch_config {
 	struct cfg_cert_file	*CERT_FILES;
 	struct cfg_cert_file	*CERT_DEFAULT;
 	char			*CIPHER_SUITE;
+	char			*CIPHER_SUITE_V3;
 	char			*ENGINE;
 	int			BACKLOG;
 #ifdef USE_SHARED_CACHE
diff --git a/src/hitch.c b/src/hitch.c
index c2e3c3a..0bf593d 100644
--- a/src/hitch.c
+++ b/src/hitch.c
@@ -857,6 +857,7 @@ make_ctx_fr(const struct cfg_cert_file *cf, const struct frontend *fr,
 	EVP_PKEY *pkey;
 	int selected_protos = CONFIG->SELECTED_TLS_PROTOS;
 	char *ciphers = CONFIG->CIPHER_SUITE;
+	char *ciphers_v3 = CONFIG->CIPHER_SUITE_V3;
 	int pref_srv_ciphers = CONFIG->PREFER_SERVER_CIPHERS;
 
 	if (fa != NULL) {
@@ -865,6 +866,8 @@ make_ctx_fr(const struct cfg_cert_file *cf, const struct frontend *fr,
 			selected_protos = fa->selected_protos;
 		if (fa->ciphers != NULL)
 			ciphers = fa->ciphers;
+		if (fa->ciphers_v3 != NULL)
+			ciphers_v3 = fa->ciphers_v3;
 		if (fa->prefer_server_ciphers != -1)
 			pref_srv_ciphers = fa->prefer_server_ciphers;
 	}
@@ -914,6 +917,17 @@ make_ctx_fr(const struct cfg_cert_file *cf, const struct frontend *fr,
 		}
 	}
 
+#ifdef SSL_OP_NO_TLSv1_3
+	if (ciphers_v3 != NULL) {
+		if (SSL_CTX_set_ciphersuites(ctx, ciphers_v3) != 1) {
+			log_ssl_error(NULL, "{core} SSL_CTX_set_ciphersuites");
+			return (NULL);
+		}
+	}
+#else
+	(void)ciphers_v3;
+#endif
+
 	if (pref_srv_ciphers)
 		SSL_CTX_set_options(ctx, SSL_OP_CIPHER_SERVER_PREFERENCE);
 
diff --git a/src/tests/test34-tls-ciphers-tls1_3.sh b/src/tests/test34-tls-ciphers-tls1_3.sh
new file mode 100755
index 0000000..f6b2179
--- /dev/null
+++ b/src/tests/test34-tls-ciphers-tls1_3.sh
@@ -0,0 +1,25 @@
+#!/bin/sh
+# Test TLS 1.3 chipers.
+
+. hitch_test.sh
+
+if ! openssl s_client -help 2>&1 | grep -q -e "-tls1_3";
+then
+    skip "Missing TLSv1.3 support"
+fi
+
+# only TLSv1.3
+cat >hitch.cfg <<EOF
+backend = "[hitch-tls.org]:80"
+frontend = "[*]:$LISTENPORT"
+pem-file = "${CERTSDIR}/default.example.com"
+ciphers_v3 = "TLS_CHACHA20_POLY1305_SHA256"
+tls-protos = TLSv1.3
+EOF
+
+start_hitch --config=hitch.cfg
+
+# Get the cipher from hitch conf, not the default
+s_client >s_client.dump
+! grep -q "Cipher is TLS_AES_256_GCM_SHA384" s_client.dump
+run_cmd grep -q "Cipher is TLS_CHACHA20_POLY1305_SHA256" s_client.dump
