Problems with running varnishncsa as non-root on varnish 5.2 RC2
That fix seems to have broken it completely, cant start varnish.

```
Assert error in VSMW_New(), vsmw.c line 291:
  Condition(fd >= 0) not true.
  errno = 13 (Permission denied)

```

Strace:
```
[pid 3875692] mkdir("_.vsm_mgt", 0755)  = 0
[pid 3875692] open("_.vsm_mgt", O_RDONLY) = 8
[pid 3875692] fchmod(8, 0750)           = 0
[pid 3875692] fchown(8, 113, 115)       = 0
[pid 3875692] setresgid(-1, 115, -1)    = 0
[pid 3875692] setresuid(-1, 112, -1)    = 0
[pid 3875692] unlinkat(8, "_.index", 0) = -1 ENOENT (No such file or directory)
[pid 3875692] openat(8, "_.index", O_WRONLY|O_CREAT|O_APPEND, 0640) = -1 EACCES (Permission denied)
[pid 3875692] write(2, "Assert error in VSMW_New(), vsmw"..., 76Assert error in VSMW_New(), vsmw.c line 291:
```

User:
113 = vcache
112 = varnish

_.vsm_mgt gets created with 750 owned by vcache:varnish
