Make this work with llvm's code coverage tools
Verbosity no longer cycles through in varnishstat

Refs #2990
Verbosity no longer cycles through in varnishstat

Refs #2990
Declare varnishstat key bindings in an include table

For starters the include table will be used for two things: handle key
presses in interactive mode and generate the "Key bindings" manual
section. The table is so far built to satisfy those two requirements.

It should then be repurposed for a help screen in interactive mode and
possibly refined to accommodate this third use case.

Refs #2990
Introduce a blank help screen in varnishstat

The plan is to reuse the points window and keep the status window going.

Refs #2990
Generate an array of lines for the varnishstat help

Refs #2990
Draw the varnishstat help screen

Refs #2990
Start interactive varnishstat with relevant verbosity

Where relevant means the highest verbosity when fields are filtered with
-f on the command line. There is still a caveat that this only applies
for the first iteration, and some parameters may conditionally appear
and not be visible when that event occurs (for example, when the child
process starts).

The rebuild variable was used as a bitmap but did not make use of
individual bits. This is now the case, but only for the two actionable
rebuild conditions.

Closes #2990
Introduce a blank help screen in varnishstat

The plan is to reuse the points window and keep the status window going.

Refs #2990
Generate an array of lines for the varnishstat help

Refs #2990
Draw the varnishstat help screen

Refs #2990
Start interactive varnishstat with relevant verbosity

Where relevant means the highest verbosity when fields are filtered with
-f on the command line. There is still a caveat that this only applies
for the first iteration, and some parameters may conditionally appear
and not be visible when that event occurs (for example, when the child
process starts).

The rebuild variable was used as a bitmap but did not make use of
individual bits. This is now the case, but only for the two actionable
rebuild conditions.

Closes #2990
Introduce a blank help screen in varnishstat

The plan is to reuse the points window and keep the status window going.

Refs #2990
Generate an array of lines for the varnishstat help

Refs #2990
Draw the varnishstat help screen

Refs #2990
Start interactive varnishstat with relevant verbosity

Where relevant means the highest verbosity when fields are filtered with
-f on the command line. There is still a caveat that this only applies
for the first iteration, and some parameters may conditionally appear
and not be visible when that event occurs (for example, when the child
process starts).

The rebuild variable was used as a bitmap but did not make use of
individual bits. This is now the case, but only for the two actionable
rebuild conditions.

Closes #2990
Generate an array of lines for the varnishstat help

Refs #2990
Draw the varnishstat help screen

Refs #2990
Start interactive varnishstat with relevant verbosity

Where relevant means the highest verbosity when fields are filtered with
-f on the command line. There is still a caveat that this only applies
for the first iteration, and some parameters may conditionally appear
and not be visible when that event occurs (for example, when the child
process starts).

The rebuild variable was used as a bitmap but did not make use of
individual bits. This is now the case, but only for the two actionable
rebuild conditions.

Closes #2990
Declare varnishstat key bindings in an include table

For starters the include table will be used for two things: handle key
presses in interactive mode and generate the "Key bindings" manual
section. The table is so far built to satisfy those two requirements.

It should then be repurposed for a help screen in interactive mode and
possibly refined to accommodate this third use case.

Refs #2990
Introduce a blank help screen in varnishstat

The plan is to reuse the points window and keep the status window going.

Refs #2990
Generate an array of lines for the varnishstat help

Refs #2990
Draw the varnishstat help screen

Refs #2990
Start interactive varnishstat with relevant verbosity

Where relevant means the highest verbosity when fields are filtered with
-f on the command line. There is still a caveat that this only applies
for the first iteration, and some parameters may conditionally appear
and not be visible when that event occurs (for example, when the child
process starts).

The rebuild variable was used as a bitmap but did not make use of
individual bits. This is now the case, but only for the two actionable
rebuild conditions.

Closes #2990
