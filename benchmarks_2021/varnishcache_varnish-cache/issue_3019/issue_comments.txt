VCL Return + Restart causes assertions in CNT_Embark on 	AZ(req->vcl0);
First note, altough the current vcl either use a direct backend or a dns one using dynamic vmod (ovh cloudstorage) fallback.

the only complex part is the restart to change backend when the original response status is over 399
the minimal vcl provided here do not cause the crash, so, it's not as trivial as hoped, we only saw crash on font files, we're working on reproducing the issue as consistently as possible
Once we have a reproducer, it would be nice to check whether #3011 accidentally fixes this by stashing `vcl0` in the `reqtop`.
our first objective is to allow going live with our deployment, so as esi are not yet used on the new stack, we'll try to put live 6.1, this will allow to confirm the vcl0 is indeed the main culprit and not a side effect.

here is the vcl called. and part of the router : 
the common_purge only implement purge control with acl, so should not be an issue
security_filesystem blocks a set of request or badly percent encoded string, so should not causes issue either.

What i find strange is the panic seems to be in the lookup phase.

The vcl

the router is generated based on the routingKey comment and will only forward matching requests, so not really complex, a minimal router.vcl : 

```
vcl 4.1;

import std;

# Dummy, MUST NOT BE USED
backend default { .host = "127.0.0.1"; }

include "conf/common_security_sni_match.vcl";

sub vcl_recv {
    # Pre-Router Logic
    set req.http.X-Forwarded-Proto = "http";
    if (std.port(server.ip) == 443) {
        set req.http.X-Forwarded-Proto = "https";
    }

    ### clean out requests sent via curls -X mode and LWP
    if (req.url ~ "^http://") {
        set req.url = regsub(req.url, "http://[^/]*", "");
    }

    if (req.http.host == "Domain" || ((req.url ~ "^/+(REGEXP)($|/)" && req.url !~"\.ext") || req.url ~"\.static$")) {
      # From : ../conf-enabled/default_files.vcl # Priority : 600
      return (vcl(vcl_files));
    }
    # Return a 404, unknown Host
    return (synth(404, "Unknown Routing Rule"));
}
```

```
# RoutingKey: req.http.host == "Domain" || ((req.url ~ "^/+(REGEXP)($|/)" && req.url !~"\.ext") || req.url ~"\.static$")
# VclLabel: vcl_files
# Priority: 600

vcl 4.1;

import directors;
import dynamic;
import std;

include "conf/common_purge.vcl";
include "conf/common_security_main.vcl";
include "conf/common_security_system_files.vcl";

# probes
probe static_probe {
    .url = "/static/ping.txt";
    .interval = 5s;
    .timeout = 3s;
    .window = 5;
    .threshold = 3;
}

# backends
backend lighttpd {
    .host = "127.0.0.1";
    .port = "81";
    .max_connections = 500;
    .connect_timeout = 500ms;
    .first_byte_timeout = 25s;
    .between_bytes_timeout = 15s;
    .probe = static_probe;
}

sub vcl_init {
    new ovh_storage = dynamic.director(
        port = "80",
        ttl = 5m
    );

    new director_files = directors.fallback();
    director_files.add_backend(lighttpd);
}

sub vcl_recv {
    # Fallback to OVH Storage
    if (req.restarts == 1) {
      set req.url = regsub(req.url, "^/static/", "/customers/qualifio/static/");
      set req.url = regsub(req.url, "^/fonts/", "/customers/qualifio/fonts/");
      set req.url = "/v1/AUTH_XXXXXXXXXXXXXXXXXXXXXXXXX" + req.url;
      set req.backend_hint = ovh_storage.backend("storage.ZONE.cloud.ovh.net");
      return (hash);
    }

    # Protect access to lighttpd server status
    if (req.url ~ "^/XXXXXXXXXXXXXXXXXX") {
        return (synth(404));
    }

    set req.backend_hint=director_files.backend();

    # Happens before we check if we have this in cache already.
    #
    # Typically you clean up the request here, removing cookies you don't need,
    # rewriting the request, etc.
    set req.url = std.tolower(req.url);
    unset req.http.If-Modified-Since;
    unset req.http.If-None-Match;
}

sub vcl_backend_response {
    #unset beresp.http.Last-Modified;
    if (beresp.http.content-type ~ "text" || beresp.http.content-type ~ "application/java") {
        set beresp.do_gzip = true;
    }
    # Happens after we have read the response headers from the backend.
    #
    # Here you clean the response headers, removing silly Set-Cookie headers
    # and other mistakes your backend does.
    set beresp.http.Access-Control-Allow-Origin = "*";
    set beresp.grace=15s;
    if (bereq.url ~ "\.(js|css|jpg|jpeg|png|gif)(\?.*)?") {
        if (beresp.ttl < 3600s) {
            set beresp.ttl = 10800s;
            set beresp.uncacheable = false;
            /* Remove Expires from backend, it's not long enough */
            unset beresp.http.expires;

            /* Set the clients TTL on this object */
            set beresp.http.Cache-Control = "public, max-age=10800";
        }
        set beresp.grace=20m;
    }
    if (beresp.ttl < 60s) {
        set beresp.grace=60s;
	    set beresp.ttl = 300s;
        set beresp.uncacheable = false;
        /* Remove Expires from backend, it's not long enough */
        unset beresp.http.expires;

        /* Set the clients TTL on this object */
        set beresp.http.Cache-Control = "public, max-age=300";
    }
    set beresp.keep=4h;
}

sub vcl_synth {
    # Restart on 911 to fallback to ovh cloud storage
    if (resp.status == 911) {
      return (restart);
    }
}

# Happens when we have all the pieces we need, and are about to send the
# response to the client.
#
# You can do accounting or modifying the final object here.
sub vcl_deliver {
    if (req.restarts == 0 && resp.status > 399) {
        # Require a restart and fallback to ovh cloud
        # We save the current error_code
        set req.http.X-Original-ErrCode = resp.status;
        set req.http.X-Original-Reason = resp.reason;
        # We restart the query on the cloud storage
        return (synth(911, "storage.zone.cloud.ovh.net"));
    }
    if (req.restarts == 1 && resp.status == 401) {
        # The request come from ovh cloud and is a 401
        # if we have a saved error code, return it
        if (req.http.X-Original-ErrCode) {
            return (synth(req.http.X-Original-ErrCode, req.http.X-Original-Reason));
        }
        return (synth(404, "Not Found"));
    }
    unset resp.http.Last-Modified;
    unset resp.http.Etag;
}

```
What is the output of this command?

```
addr2line -e /usr/sbin/varnishd 0x560e65060dd3 0x560e650563d5 0x560e65056910
```
unfortunately : 
```
addr2line -e /usr/sbin/varnishd 0x560e65060dd3 0x560e650563d5 0x560e65056910
??:0
??:0
??:0
```
And i can't find the debug version of the package cloud .deb
```
Desired=Unknown/Install/Remove/Purge/Hold
| Status=Not/Inst/Conf-files/Unpacked/halF-conf/Half-inst/trig-aWait/Trig-pend
|/ Err?=(none)/Reinst-required (Status,Err: uppercase=bad)
||/ Name           Version         Architecture Description
+++-==============-===============-============-==================================================
ii  varnish        6.2.0-1~stretch amd64        state of the art, high-performance web accelerator
```
Full stack :
`addr2line -e /usr/sbin/varnishd +0x4eec9 +0xB99C0 +0x52E16 +0x7bdd3 +0x713d5 +0x71910`
```
./bin/varnishd/cache/cache_panic.c:624
./lib/libvarnish/vas.c:53
./bin/varnishd/cache/cache_req_fsm.c:1023
./bin/varnishd/http1/cache_http1_fsm.c:415
./bin/varnishd/cache/cache_wrk.c:410
./bin/varnishd/cache/cache_wrk.c:443
```
```
varnishtest "return(vcl) then reembark"

barrier b1 cond 2

server s1 {
        rxreq
        barrier b1 sync
        txresp
} -start

varnish v1 -vcl+backend ""
varnish v1 -cliok "vcl.label lbl vcl1"
varnish v1 -vcl {
        backend be { .host = "${bad_backend}"; }

        sub vcl_recv {
                return (vcl(lbl));
        }
} -start

client c1 {
        txreq
        rxresp
        expect resp.status == 200
} -start

client c2 {
        barrier b1 sync
        txreq
        rxresp
        expect resp.status == 200
} -start

client c1 -wait
client c2 -wait
```
Confirmed panic on 6.2 and master, fixed by #3011.
Ok, we'll test in on our system as soon as possible, (we have multiple issues that required us to rush the move to prod of the varnish), i'll keep you posted to confirm it solves our issue.
I'm fairly confident considering how similar the panic I consistently get with the test case I pasted. You can give it a try yourself before and after patching by running `varnishtest file.vtc` where `file.vtc` contains my test script above.
By the way it would be nice to have some feedback about #3011 running in production :)
We'll probably start deploying 6.1 and shadow the traffic to the test node with the patch, i'll let you know if it's good and we'll check the strategy based on the vcls deployed on the new varnish cluster.
Based on a partial load of traffic (that was able to crash the server), we've got a stable varnish now, so, it's probably good, i'll reopen the bug if it's crashing with more volume
Reopening because we have an actionable test case to reproduce the crash. And we are trying to make progress in #3011, a candidate pull request for solving this.
Ok, so, in production for the last 4 hours, we're getting ~100 qps per node, and no issue (we were crashing the node with ~ 7qps before)
we've been basically running for the last month with increasing traffic on the servers.

all is good no panic and > 200qps as of now and  25 days of varnish uptime

so obviously, it works quite well