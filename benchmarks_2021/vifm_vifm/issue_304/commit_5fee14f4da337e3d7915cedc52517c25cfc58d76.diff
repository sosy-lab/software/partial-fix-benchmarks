diff --git a/ChangeLog b/ChangeLog
index d1690aa6a..28076405f 100644
--- a/ChangeLog
+++ b/ChangeLog
@@ -9,6 +9,10 @@
 	Fixed possible crash on `:highlight clear {pattern}` and highlights not
 	being updated.
 
+	Added support for arbitrary expressions in 'statusline': '%{...}'.
+	e.g. '%{&sort}' evaluates to the value of the sort option. Patch by Dmitry
+	Frank (a.k.a.  dimonomid).
+
 0.9 to 0.9.1-beta
 
 	Added "inode" sorting key, which sorts entries by inode number.  Thanks to
diff --git a/data/man/vifm.1 b/data/man/vifm.1
index f8518cae4..098f03837 100644
--- a/data/man/vifm.1
+++ b/data/man/vifm.1
@@ -4174,6 +4174,8 @@ since it cannot be selected
 .IP \- 2
 %z - short tips/tricks/hints that chosen randomly after one minute period
 .IP \- 2
+%{<expr>} - evaluate arbitrary vifm expression '<expr>', e.g. '&sort'
+.IP \- 2
 all 'rulerformat' macros
 .RE
 
@@ -4183,7 +4185,7 @@ minimum field width if you want field to be right aligned.
 Example:
 
 .EX
-  set statusline="  %t%= %A %10u:%\-7g %15s %20d "
+  set statusline="  %t%= %A %10u:%\-7g %15s %20d %{&sort} "
 .EE
 
 On Windows file properties include the following flags (upper case means flag is
diff --git a/src/ui/statusline.c b/src/ui/statusline.c
index 2250592cd..a86c1255d 100644
--- a/src/ui/statusline.c
+++ b/src/ui/statusline.c
@@ -33,6 +33,8 @@
 #include "../compat/pthread.h"
 #include "../compat/reallocarray.h"
 #include "../engine/mode.h"
+#include "../engine/parsing.h"
+#include "../engine/var.h"
 #include "../modes/modes.h"
 #include "../utils/fs.h"
 #include "../utils/log.h"
@@ -201,7 +203,7 @@ refresh_window(WINDOW *win, int lazily)
 TSTATIC char *
 expand_status_line_macros(view_t *view, const char format[])
 {
-	return expand_view_macros(view, format, "tTfaAugsEdD-xlLSz%[]");
+	return expand_view_macros(view, format, "tTfaAugsEdD-xlLSz%[]{");
 }
 
 /* Expands possibly limited set of view macros.  Returns newly allocated string,
@@ -373,6 +375,48 @@ parse_view_macros(view_t *view, const char **format, const char macros[],
 				LOG_INFO_MSG("Unmatched %%]");
 				ok = 0;
 				break;
+			case '{':
+				{
+					/* Try to find matching closing bracket
+					 * TODO: implement the way to escape it, so that the expr may contain
+					 * closing brackets */
+					const char *e = strchr(*format, '}');
+					char *expr = NULL, *resstr = NULL;
+					var_t res = var_false();
+					ParsingErrors parsing_error;
+
+					/* If there's no matching closing bracket, add the opening one
+					 * literally */
+					if (e == NULL) {
+						strappendch(&result, &len, '%');
+						strappendch(&result, &len, '{');
+						break;
+					}
+
+					/* Create a NULL-terminated copy of the given expr.
+					 * TODO: we could temporarily use buf for that, to avoid extra
+					 * allocation, but explicitly named variable reads better. */
+					expr = calloc(e - (*format) + 1 /* NUL-term */, 1);
+					memcpy(expr, *format, e - (*format));
+
+					/* Try to parse expr, and convert the res to string if succeed. */
+					parsing_error = parse(expr, &res);
+					if(parsing_error == PE_NO_ERROR) {
+						resstr = var_to_string(res);
+					}
+
+					if (resstr != NULL) {
+						copy_str(buf, sizeof(buf), resstr);
+					} else {
+						copy_str(buf, sizeof(buf), "<Invalid expr>");
+					}
+
+					free(resstr);
+					free(expr);
+
+					*format = e + 1 /* closing bracket */;
+				}
+				break;
 
 			default:
 				LOG_INFO_MSG("Unexpected %%-sequence: %%%c", c);
diff --git a/tests/misc/expand_status_line_macros.c b/tests/misc/expand_status_line_macros.c
index 740b495b6..33e93f0b2 100644
--- a/tests/misc/expand_status_line_macros.c
+++ b/tests/misc/expand_status_line_macros.c
@@ -183,7 +183,7 @@ TEST(percent_macro_expanded)
 
 TEST(wrong_macros_ignored)
 {
-	static const char STATUS_CHARS[] = "tTfaAugsEdD-xlLS%[]z";
+	static const char STATUS_CHARS[] = "tTfaAugsEdD-xlLS%[]z{";
 	int i;
 
 	for(i = 1; i <= 255; ++i)
@@ -198,7 +198,7 @@ TEST(wrong_macros_ignored)
 
 TEST(wrong_macros_with_width_field_ignored)
 {
-	static const char STATUS_CHARS[] = "tTfaAugsEdD-xlLS%[]z";
+	static const char STATUS_CHARS[] = "tTfaAugsEdD-xlLS%[]z{";
 	int i;
 
 	for(i = 1; i <= 255; ++i)
@@ -246,5 +246,25 @@ TEST(ignore_mismatched_closing_bracket)
 	ASSERT_EXPANDED_TO("%]", "%]");
 }
 
+TEST(valid_expr)
+{
+	char *v = getenv("HOME");
+	if (v == NULL) {
+		v = "";
+	}
+	ASSERT_EXPANDED_TO("%{$HOME}", v);
+}
+
+TEST(invalid_expr)
+{
+	ASSERT_EXPANDED_TO("%{foobar}", "<Invalid expr>");
+}
+
+TEST(ignore_mismatched_opening_curly_bracket)
+{
+	ASSERT_EXPANDED_TO("<%{>", "<%{>");
+	ASSERT_EXPANDED_TO("<%{abcdef>", "<%{abcdef>");
+}
+
 /* vim: set tabstop=2 softtabstop=2 shiftwidth=2 noexpandtab cinoptions-=(0 : */
 /* vim: set cinoptions+=t0 filetype=c : */
