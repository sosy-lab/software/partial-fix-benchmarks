Server passwords doesn't start with colon with spaced password
(For reference: znc/znc#1188)

@flashcode I fear your fix breaks some IRCds when password without space is used. See https://github.com/hexchat/hexchat/issues/1550 and the easiest fix would probably be https://github.com/irssi/irssi/issues/362#issuecomment-159064710.

OK, I'll update accordingly.

@Mikaela: it should be better with the second commit.
