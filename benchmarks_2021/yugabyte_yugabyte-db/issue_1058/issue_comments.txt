[YSQL] Inserting NULL into column with UNIQUE INDEX causes crash
Note that this is NOT reproducible with YCQL:
```cql
cqlsh> CREATE TABLE k.num2 (pk int PRIMARY KEY, i int, vi bigint) WITH transactions = {'enabled': true};
cqlsh> CREATE UNIQUE INDEX k_num2_vi ON k.num2 (vi);
cqlsh> INSERT INTO k.num2 (pk, i, vi) VALUES (0, 0, NULL);
cqlsh> INSERT INTO k.num2 (pk, i, vi) VALUES (1, 0, NULL);
InvalidRequest: Error from server: code=2200 [Invalid query] message="Execution Error. Duplicate value disallowed by unique index k_num2_vi
INSERT INTO k.num2 (pk, i, vi) VALUES (1, 0, NULL);
       ^^^^
 (error -300)"
```
This was partially fixes as a part of #945 - `NULL`s are now inserted normally but aren't exempt from unique constraints (as they should be per SQL standard)