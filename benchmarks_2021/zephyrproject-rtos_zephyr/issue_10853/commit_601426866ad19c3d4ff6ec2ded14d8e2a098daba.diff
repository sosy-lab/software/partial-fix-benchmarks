diff --git a/include/net/net_pkt.h b/include/net/net_pkt.h
index c4bd075b39f3..c22f23a5f6b7 100644
--- a/include/net/net_pkt.h
+++ b/include/net/net_pkt.h
@@ -99,6 +99,12 @@ struct net_pkt {
 	u16_t appdatalen;
 	u8_t ll_reserve;	/* link layer header length */
 	u8_t ip_hdr_len;	/* pre-filled in order to avoid func call */
+	u8_t transport_proto;	/* Transport protol of data, like
+				 * IPPROTO_TCP or IPPROTO_UDP. This value is
+				 * saved so that we do not need to traverse
+				 * through extension headers (this is mainly
+				 * issue in IPv6).
+				 */
 
 #if defined(CONFIG_NET_TCP)
 	sys_snode_t sent_list;
@@ -263,6 +269,16 @@ static inline void net_pkt_set_ip_hdr_len(struct net_pkt *pkt, u8_t len)
 	pkt->ip_hdr_len = len;
 }
 
+static inline u8_t net_pkt_transport_proto(struct net_pkt *pkt)
+{
+	return pkt->transport_proto;
+}
+
+static inline void net_pkt_set_transport_proto(struct net_pkt *pkt, u8_t proto)
+{
+	pkt->transport_proto = proto;
+}
+
 static inline u8_t *net_pkt_next_hdr(struct net_pkt *pkt)
 {
 	return pkt->next_hdr;
diff --git a/subsys/net/ip/ipv4.c b/subsys/net/ip/ipv4.c
index 96cb308b899a..ee0b95022e30 100644
--- a/subsys/net/ip/ipv4.c
+++ b/subsys/net/ip/ipv4.c
@@ -146,6 +146,8 @@ enum net_verdict net_ipv4_process_pkt(struct net_pkt *pkt)
 		goto drop;
 	}
 
+	net_pkt_set_transport_proto(pkt, hdr->proto);
+
 	switch (hdr->proto) {
 	case IPPROTO_ICMP:
 		verdict = net_icmpv4_input(pkt);
diff --git a/subsys/net/ip/ipv6.c b/subsys/net/ip/ipv6.c
index e7a4ef5860da..166fa5f9bb25 100644
--- a/subsys/net/ip/ipv6.c
+++ b/subsys/net/ip/ipv6.c
@@ -612,6 +612,7 @@ enum net_verdict net_ipv6_process_pkt(struct net_pkt *pkt)
 upper_proto:
 
 	net_pkt_set_ipv6_ext_len(pkt, total_len);
+	net_pkt_set_transport_proto(pkt, next);
 
 	switch (next) {
 	case IPPROTO_ICMPV6:
diff --git a/subsys/net/ip/net_pkt.c b/subsys/net/ip/net_pkt.c
index f755e9dc8ed8..c6f6c9ff7fb9 100644
--- a/subsys/net/ip/net_pkt.c
+++ b/subsys/net/ip/net_pkt.c
@@ -1793,13 +1793,7 @@ static int net_pkt_get_addr(struct net_pkt *pkt, bool is_src,
 	addr->sa_family = family;
 
 	/* Examine the transport protocol */
-	if (IS_ENABLED(CONFIG_NET_IPV6) && family == AF_INET6) {
-		proto = NET_IPV6_HDR(pkt)->nexthdr;
-	} else if (IS_ENABLED(CONFIG_NET_IPV4) && family == AF_INET) {
-		proto = NET_IPV4_HDR(pkt)->proto;
-	} else {
-		return -ENOTSUP;
-	}
+	proto = net_pkt_transport_proto(pkt);
 
 	/* Get the source port from transport protocol header */
 	if (IS_ENABLED(CONFIG_NET_TCP) && proto == IPPROTO_TCP) {
diff --git a/tests/net/utils/src/main.c b/tests/net/utils/src/main.c
index 094523b86e11..2d854735137d 100644
--- a/tests/net/utils/src/main.c
+++ b/tests/net/utils/src/main.c
@@ -1347,12 +1347,14 @@ void test_net_pkt_addr_parse(void)
 	static struct ipv6_test_data {
 		const unsigned char *payload;
 		int payload_len;
+		u8_t proto;
 		struct sockaddr_in6 src;
 		struct sockaddr_in6 dst;
 	} ipv6_test_data_set[] = {
 		{
 			.payload = v6_udp_pkt1,
 			.payload_len = sizeof(v6_udp_pkt1),
+			.proto = IPPROTO_UDP,
 			.src = {
 				.sin6_family = AF_INET6,
 				.sin6_port = htons(5353),
@@ -1386,6 +1388,7 @@ void test_net_pkt_addr_parse(void)
 		{
 			.payload = v6_tcp_pkt1,
 			.payload_len = sizeof(v6_tcp_pkt1),
+			.proto = IPPROTO_TCP,
 			.src = {
 				.sin6_family = AF_INET6,
 				.sin6_port = htons(62032),
@@ -1423,12 +1426,14 @@ void test_net_pkt_addr_parse(void)
 	static struct ipv4_test_data {
 		const unsigned char *payload;
 		int payload_len;
+		u8_t proto;
 		struct sockaddr_in src;
 		struct sockaddr_in dst;
 	} ipv4_test_data_set[] = {
 		{
 			.payload = v4_tcp_pkt1,
 			.payload_len = sizeof(v4_tcp_pkt1),
+			.proto = IPPROTO_TCP,
 			.src = {
 				.sin_family = AF_INET,
 				.sin_port = htons(22),
@@ -1454,6 +1459,7 @@ void test_net_pkt_addr_parse(void)
 		{
 			.payload = v4_udp_pkt1,
 			.payload_len = sizeof(v4_udp_pkt1),
+			.proto = IPPROTO_UDP,
 			.src = {
 				.sin_family = AF_INET,
 				.sin_port = htons(64426),
@@ -1508,6 +1514,7 @@ void test_net_pkt_addr_parse(void)
 
 		net_pkt_set_ip_hdr_len(pkt, sizeof(struct net_ipv6_hdr));
 		net_pkt_set_family(pkt, AF_INET6);
+		net_pkt_set_transport_proto(pkt, data->proto);
 
 		zassert_equal(net_pkt_get_src_addr(pkt,
 						   (struct sockaddr *)&addr,
@@ -1564,6 +1571,7 @@ void test_net_pkt_addr_parse(void)
 
 		net_pkt_set_ip_hdr_len(pkt, sizeof(struct net_ipv4_hdr));
 		net_pkt_set_family(pkt, AF_INET);
+		net_pkt_set_transport_proto(pkt, data->proto);
 
 		zassert_equal(net_pkt_get_src_addr(pkt,
 						   (struct sockaddr *)&addr,
