lib/os: Conditionally eliminate alloca/VLA usage

MISRA rules (see #9892) forbid alloca() and family, even though those
features can be valuable performance and memory size optimizations
useful to Zephyr.

Introduce a MISRA_SANE kconfig, which when true enables a gcc error
condition whenever a variable length array is used.

When enabled, the mempool code will use a theoretical-maximum array
size on the stack instead of one tailored to the current pool
configuration.

The rbtree code will do similarly, but because the theoretical maximum
is quite a bit larger (236 bytes on 32 bit platforms) the array is
placed into struct rbtree instead so it can live in static data (and
also so I don't have to go and retune all the test stack sizes!).
Current code only uses at most two of these (one in the scheduler when
SCHED_SCALABLE is selected, and one for dynamic kernel objects when
USERSPACE and DYNAMIC_OBJECTS are set).  The RB_FOR_EACH() macro also
had alloca() usage, but as nothing in-tree uses it currently it was
just diabled in MISRA mode.

This tunable is false by default, but is selected in a single test (a
subcase of tests/kernel/common) for coverage.  Note that the I2C and
SPI subsystems contain uncorrected VLAs, so a few platforms need to be
blacklisted with a filter.

Signed-off-by: Andy Ross <andrew.j.ross@intel.com>
lib/os: Remove recursion from mempool and rbtree

MISRA rules (see #11425) forbid recursive algorithms.  In the case of
rb_walk(), it's not actually used anywhere but a test right now, so we
can simply disable the API when CONFIG_MISRA_SANE is defined.  Mempool
had a (IMHO, fairly clever) tail recursive loop in bfree_recombine()
which can be trivially transformed into an only slightly uglier
iterative version.

Signed-off-by: Andy Ross <andrew.j.ross@intel.com>
lib/os: Remove recursion from mempool and rbtree

MISRA rules (see #11425) forbid recursive algorithms.  In the case of
rb_walk(), it's not actually used anywhere but a test right now, so we
can simply disable the API when CONFIG_MISRA_SANE is defined.  Mempool
had a (IMHO, fairly clever) tail recursive loop in bfree_recombine()
which can be trivially transformed into an only slightly uglier
iterative version.

Signed-off-by: Andy Ross <andrew.j.ross@intel.com>
lib/os: Remove recursion from mempool and rbtree

MISRA rules (see #11425) forbid recursive algorithms.  In the case of
rb_walk(), it's not actually used anywhere but a test right now, so we
can simply disable the API when CONFIG_MISRA_SANE is defined.  Mempool
had a (IMHO, fairly clever) tail recursive loop in bfree_recombine()
which can be trivially transformed into an only slightly uglier
iterative version.

Signed-off-by: Andy Ross <andrew.j.ross@intel.com>
lib/os: Remove recursion from mempool and rbtree

MISRA rules (see #11425) forbid recursive algorithms.  In the case of
rb_walk(), it's not actually used anywhere but a test right now, so we
can simply disable the API when CONFIG_MISRA_SANE is defined.  Mempool
had a (IMHO, fairly clever) tail recursive loop in bfree_recombine()
which can be trivially transformed into an only slightly uglier
iterative version.

Signed-off-by: Andy Ross <andrew.j.ross@intel.com>
