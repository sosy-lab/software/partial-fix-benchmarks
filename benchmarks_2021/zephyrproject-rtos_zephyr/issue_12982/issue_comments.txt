net: Zephyr drops IPv4 packet with extended MAC frame size
> If Zephyr receives a packet with IPv4 datagram occupying only portion of the MAC frame it drops the packet without responding to it. 

I don't understand this report. Does it say that any IPv4 packet with the size different from 1500 gets dropped?

@tbursztyka with PR #13024 UPD and ICMP work, but TCP fails, captures attached

[ext-frame-tcp-zephyr-pr12982.pcap.gz](https://github.com/zephyrproject-rtos/zephyr/files/2827457/ext-frame-tcp-zephyr-pr12982.pcap.gz)
[ext-frame-tcp-zephyr-05f41.pcap.gz](https://github.com/zephyrproject-rtos/zephyr/files/2827458/ext-frame-tcp-zephyr-05f41.pcap.gz)

Weird, how allocating incoming frame on ethernet driver affects tcp... will check.
@pfalcon no, incoming packet has an ethernet trailer of 472 bytes, this causes failure in IP datagram size  calculations.
> @pfalcon no, incoming packet has an ethernet trailer of 472 bytes, this causes failure in IP datagram size calculations.

In other words, there's a mismatch in IPv4 vs Ethernet packet sizes, and Ethernet packet has "empty" space after the IPv4 packet?

@pfalcon you got that right