TCP: sending lots of data deadlocks with slow peer
CC: @jukkar 
Could you detail your hardware configuration, could you also paste a grep NET zephyr/.config  (from your build directory) ?

There is indeed an issue with lock, however there are also things that can be tweaked to help. Let's see.
Well, the hardware is a SAM E70 Xplained board on the same switch as my PC.

Here is the NET part from my .config. I have omitted the LOG_LEVEL messages to shorten it a litte:
```
CONFIG_NET_L2_ETHERNET=y
# CONFIG_NET_L2_IEEE802154 is not set
# CONFIG_SOC_SERIES_KINETIS_K6X is not set
# CONFIG_SOC_SERIES_KINETIS_KL2X is not set
# CONFIG_SOC_SERIES_KINETIS_KWX is not set
# CONFIG_TELNET_CONSOLE is not set
# CONFIG_NET_LOOPBACK is not set
# CONFIG_NEURAL_NET_ACCEL is not set
# CONFIG_LOG_BACKEND_NET is not set
CONFIG_NET_BUF=y
CONFIG_NET_BUF_USER_DATA_SIZE=4
# CONFIG_NET_BUF_LOG is not set
# CONFIG_NET_BUF_POOL_USAGE is not set
CONFIG_NETWORKING=y
# CONFIG_NET_HOSTNAME_ENABLE is not set
# CONFIG_NET_L2_DUMMY is not set
# CONFIG_NET_L2_BT_SHELL is not set
# CONFIG_NET_L2_ETHERNET_MGMT is not set
# CONFIG_NET_VLAN is not set
CONFIG_NET_ARP=y
CONFIG_NET_ARP_TABLE_SIZE=2
CONFIG_NET_ARP_GRATUITOUS=y
# CONFIG_NET_GPTP is not set
# CONFIG_NET_LLDP is not set
CONFIG_NET_LLDP_CHASSIS_ID="CHASSIS_ID_PLACEHOLDER"
CONFIG_NET_LLDP_PORT_ID="PORT_ID_PLACEHOLDER"
# CONFIG_NET_L2_WIFI_MGMT is not set
# CONFIG_NET_L2_WIFI_SHELL is not set
# CONFIG_NET_L2_CANBUS is not set
CONFIG_NET_INIT_PRIO=90
# CONFIG_NET_IPV6 is not set
CONFIG_NET_IPV4=y
CONFIG_NET_INITIAL_TTL=64
CONFIG_NET_IF_MAX_IPV4_COUNT=1
CONFIG_NET_IF_UNICAST_IPV4_ADDR_COUNT=1
CONFIG_NET_IF_MCAST_IPV4_ADDR_COUNT=1
# CONFIG_NET_ICMPV4_ACCEPT_BROADCAST is not set
# CONFIG_NET_DHCPV4 is not set
# CONFIG_NET_IPV4_AUTO is not set
# CONFIG_NET_SHELL is not set
CONFIG_NET_TC_TX_COUNT=1
CONFIG_NET_TC_RX_COUNT=1
CONFIG_NET_TC_MAPPING_STRICT=y
CONFIG_NET_TX_DEFAULT_PRIORITY=1
CONFIG_NET_IP_ADDR_CHECK=y
CONFIG_NET_MAX_ROUTERS=1
CONFIG_NET_TCP=y
CONFIG_NET_TCP_CHECKSUM=y
CONFIG_NET_TCP_BACKLOG_SIZE=1
CONFIG_NET_TCP_TIME_WAIT_DELAY=250
CONFIG_NET_TCP_ACK_TIMEOUT=1000
CONFIG_NET_TCP_INIT_RETRANSMISSION_TIMEOUT=200
CONFIG_NET_TCP_RETRY_COUNT=9
CONFIG_NET_UDP=y
CONFIG_NET_UDP_CHECKSUM=y
CONFIG_NET_MAX_CONN=4
# CONFIG_NET_CONN_CACHE is not set
CONFIG_NET_MAX_CONTEXTS=6
# CONFIG_NET_CONTEXT_NET_PKT_POOL is not set
CONFIG_NET_CONTEXT_SYNC_RECV=y
CONFIG_NET_CONTEXT_CHECK=y
# CONFIG_NET_CONTEXT_PRIORITY is not set
# CONFIG_NET_CONTEXT_TIMESTAMP is not set
# CONFIG_NET_TEST is not set
# CONFIG_NET_TRICKLE is not set
CONFIG_NET_PKT_RX_COUNT=14
CONFIG_NET_PKT_TX_COUNT=14
CONFIG_NET_BUF_RX_COUNT=36
CONFIG_NET_BUF_TX_COUNT=36
CONFIG_NET_BUF_FIXED_DATA_SIZE=y
# CONFIG_NET_BUF_VARIABLE_DATA_SIZE is not set
CONFIG_NET_BUF_DATA_SIZE=256
CONFIG_NET_DEFAULT_IF_FIRST=y
# CONFIG_NET_DEFAULT_IF_ETHERNET is not set
# CONFIG_NET_PKT_TIMESTAMP is not set
# CONFIG_NET_PROMISCUOUS_MODE is not set
CONFIG_NET_TX_STACK_SIZE=1200
CONFIG_NET_RX_STACK_SIZE=1500
CONFIG_NET_MGMT=y
CONFIG_NET_MGMT_EVENT=y
CONFIG_NET_MGMT_EVENT_STACK_SIZE=512
CONFIG_NET_MGMT_EVENT_THREAD_PRIO=7
CONFIG_NET_MGMT_EVENT_QUEUE_SIZE=2
# CONFIG_NET_MGMT_EVENT_INFO is not set
# CONFIG_NET_DEBUG_MGMT_EVENT_STACK is not set
# CONFIG_NET_STATISTICS is not set
# CONFIG_NET_OFFLOAD is not set
CONFIG_NET_LOG=y
# CONFIG_NET_DEBUG_NET_PKT_ALLOC is not set
CONFIG_NET_DEBUG_NET_PKT_EXTERNALS=0
# CONFIG_NET_DEBUG_NET_PKT_NON_FRAGILE_ACCESS is not set
CONFIG_NET_CONFIG_AUTO_INIT=y
CONFIG_NET_CONFIG_INIT_PRIO=95
CONFIG_NET_CONFIG_INIT_TIMEOUT=30
# CONFIG_NET_CONFIG_NEED_IPV6 is not set
# CONFIG_NET_CONFIG_NEED_IPV4 is not set
CONFIG_NET_CONFIG_SETTINGS=y
CONFIG_NET_CONFIG_MY_IPV4_ADDR="192.168.2.166"
CONFIG_NET_CONFIG_MY_IPV4_NETMASK="255.255.255.0"
CONFIG_NET_CONFIG_MY_IPV4_GW=""
CONFIG_NET_CONFIG_PEER_IPV4_ADDR="192.168.2.45"
CONFIG_NET_SOCKETS=y
CONFIG_NET_SOCKETS_POSIX_NAMES=y
CONFIG_NET_SOCKETS_POLL_MAX=3
# CONFIG_NET_SOCKETS_SOCKOPT_TLS is not set
# CONFIG_NET_SOCKETS_OFFLOAD is not set
# CONFIG_NET_SOCKETS_PACKET is not set
# CONFIG_NET_SOCKETS_CAN is not set
```

I have thought about raising CONFIG_NET_PKT_TX_COUNT and CONFIG_NET_BUF_TX_COUNT, but that will not eliminate the problem. It will just be less likely. Temporarily detaching the PC from the switch should still cause the error.

What I did try was to unlock the mutex while calling `net_pkt_alloc_with_buffer` (both in `context_alloc_pkt` and in `prepare_segment`). Some other places also need locking added or temporarily removed for this to work. The application ran the whole weekend constantly sending >8000 TCP packets per second. I know the approach is ugly, but it does appear to work. Of course it has to be thoroughly checked if there are any side effects when the state of the socket does change during `net_pkt_alloc_with_buffer`.
@tbursztyka Please review priority.
@dgloeck what about unlocking only in context_alloc_pkt()? That could be sufficient. Indeed playing with unlock in tcp side is potentially creating issues (it's already quite unstable...) 
@tbursztyka I tried that at first, but then it got stuck in `prepare_segment`.
Ok, well let me send a "fix" based on you tests
Of related issue, we have functions which take timeout parameter, but call `k_mutex_lock(&context->lock, K_FOREVER);` in them. I.e. even despite timeout parameter, they could deadlock forever (of course, one would hope that deadlock can never happen in them, but issues like this teach otherwise).

@tbursztyka, at first I was surprised how short your patch is. But then I saw that you did not unlock the mutex in `prepare_segment`. I have uploaded my patch to dgloeck/zephyr@e61634cf72abfdb824c96b8924e07f552c210a07. I believe I have covered all places that call into `prepare_segment`. Most of this is untested, though.
prepare_segment should not unlock the lock: the problem, currently, is that caller to prepare_segment might not have locked context. It's an error to unlock a non-locked mutex. (try your patch with CONFIG_ASSERT=y, or directly tests/net/tcp/).

[edit] your patch seems to be doing way more than what we discussed.
@tbursztyka Did you try my patch with CONFIG_ASSERT=y? I added locking to all functions that might eventually call `prepare_segment`:
 - `prepare_segment` is called by `net_tcp_prepare_segment` and `net_tcp_prepare_reset`
 - `net_tcp_prepare_segment` is called by `net_tcp_prepare_ack`, `net_tcp_queue_data`, `queue_fin`, and `send_syn_segment`
 - `net_tcp_prepare_ack` is only called by `send_ack`, which is only called by `tcp_established`, which already has locking
 - `net_tcp_queue_data` is called by `sendto` and `context_sendto_new`
 - `sendto` is only called by `net_context_send`, which already has locking
 - `context_sendto_new` is only called by `net_context_sendto_new`, which already has locking
 - `queue_fin` is only called by `net_tcp_put`, which is only called by `net_context_put`, which already has locking
 - `send_syn_segment` is called by `send_syn` and `send_syn_ack`
 - `send_syn` is only called by `net_tcp_connect`, which is only called by `net_context_connect`, which already has locking
 - `send_syn_ack` is only called by `tcp_syn_rcvd`, where I added locking
 - `net_tcp_prepare_reset` is only called by `send_reset`
 - `send_reset` is called by `backlog_ack_timeout`, `tcp_synack_received`, and `tcp_syn_rcvd` and I added locking to all three functions
 - since `tcp_synack_received` has to be able to run in parallel to `net_tcp_connect`, I temporarily unlock the mutex there
@dgloeck I am fine with your approach, it's just going much further than first thought. It's basically fixing also commit 93e5181fbdf

@jukkar as you introduced the context lock, what do you think about @dgloeck's patch?
> @jukkar as you introduced the context lock, what do you think about @dgloeck's patch?

Looks quite ok to me. Needs a bit more testing. @dgloeck, could you prepare a proper PR for that?
@dgloeck ping 
@tbursztyka is this issue still present?
There has been fixes in TCP side so it is difficult to know if this is really fixed or not. There has not been reports about issues like this lately.
This was not seen in any of our tests.  Can this be closed?
Lowering the priority as it has not been seen in some time.
> There has not been reports about issues like this lately.

There was no reports, because everything stays as it was, the TCP implementation is broken.

This is an old issue and it talks about problems when sending data so just fyi atm. I fixed several issues when receiving lot of incoming data in #23334 and it might show improvements in the sending side too.
This issue has been marked as stale because it has been open (more than) 60 days with no activity. Remove the stale label or add a comment saying that you would like to have the label removed otherwise this issue will automatically be closed in 14 days. Note, that you can always re-open a closed issue at any time.