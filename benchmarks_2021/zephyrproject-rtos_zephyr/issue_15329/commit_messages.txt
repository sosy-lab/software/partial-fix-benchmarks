doc: remove references to IRC

Remove references to IRC and mention Slack instead.

Fixes #15266

Signed-off-by: Anas Nashif <anas.nashif@intel.com>
Bluetooth: GATT: Fix not clearing Client Features

When a device is considered unpaired any configuration set in Client
Features shall also be removed.

Fixes #15329

Signed-off-by: Luiz Augusto von Dentz <luiz.von.dentz@intel.com>
Bluetooth: GATT: Fix not clearing Client Features

When a device is considered unpaired any configuration set in Client
Features shall also be removed.

Fixes #15329

Signed-off-by: Luiz Augusto von Dentz <luiz.von.dentz@intel.com>
Bluetooth: GATT: Fix not clearing Client Features

When a device is considered unpaired any configuration set in Client
Features shall also be removed.

Fixes #15329

Signed-off-by: Luiz Augusto von Dentz <luiz.von.dentz@intel.com>
Bluetooth: GATT: Fix not clearing Client Features

When a device is considered unpaired any configuration set in Client
Features shall also be removed.

Fixes #15329

Signed-off-by: Luiz Augusto von Dentz <luiz.von.dentz@intel.com>
