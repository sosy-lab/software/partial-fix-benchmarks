diff --git a/include/device.h b/include/device.h
index d6cdc21d5aab0..aebc10b93144b 100644
--- a/include/device.h
+++ b/include/device.h
@@ -9,6 +9,7 @@
 #define ZEPHYR_INCLUDE_DEVICE_H_
 
 #include <kernel.h>
+#include <power/power.h>
 
 /**
  * @brief Device Driver APIs
@@ -260,6 +261,9 @@ struct device_config {
 #ifdef CONFIG_DEVICE_POWER_MANAGEMENT
 	int (*device_pm_control)(struct device *device, u32_t command,
 				 void *context, device_pm_cb cb, void *arg);
+#ifdef CONFIG_DEVICE_PM_DISTRIBUTED_METHOD
+	int (*device_pm_notify)(struct device *device, enum power_states state);
+#endif
 	struct device_pm *pm;
 #endif
 	const void *config_info;
diff --git a/kernel/Kconfig.power_mgmt b/kernel/Kconfig.power_mgmt
index 8e02e9c7253c1..598d8986209b6 100644
--- a/kernel/Kconfig.power_mgmt
+++ b/kernel/Kconfig.power_mgmt
@@ -62,3 +62,10 @@ config DEVICE_PM_CENTRAL_METHOD
 	default y
 	help
 	  This option enables the device power management central method.
+
+config DEVICE_PM_DISTRIBUTED_METHOD
+	bool "Device power management distributed method"
+	depends on DEVICE_POWER_MANAGEMENT
+	depends on !DEVICE_PM_CENTRAL_METHOD
+	help
+	  This option enables the device power management distributed method.
diff --git a/subsys/power/device.c b/subsys/power/device.c
index bf763385ac75c..d2a40c87468cd 100644
--- a/subsys/power/device.c
+++ b/subsys/power/device.c
@@ -9,6 +9,7 @@
 #include <string.h>
 #include <soc.h>
 #include <device.h>
+#include <sys/atomic.h>
 #include "policy/pm_policy.h"
 
 #if defined(CONFIG_SYS_POWER_MANAGEMENT)
@@ -69,6 +70,35 @@ void sys_pm_resume_devices(void)
 {
 	sys_pm_set_devices_power_state(DEVICE_PM_ACTIVE_STATE);
 }
+#elif defined(CONFIG_DEVICE_PM_DISTRIBUTED_METHOD)
+static atomic_t suspend_lock;
+
+void sys_pm_suspend_lock_acquire(void)
+{
+	atomic_inc(&suspend_lock);
+}
+
+void sys_pm_suspend_lock_release(void)
+{
+	atomic_dec(&suspend_lock);
+}
+
+bool sys_pm_allow_suspend(void)
+{
+	return (atomic_get(&suspend_lock) == 0);
+}
+
+void sys_pm_power_state_notify_devices(enum power_states state)
+{
+	for (int i = device_count - 1; i >= 0; i--) {
+		int idx = device_ordered_list[i];
+		struct device *device = &pm_device_list[idx];
+
+		if (device->config->device_pm_notify) {
+			device->config->device_pm_notify(device, state);
+		}
+	}
+}
 #endif
 
 void sys_pm_create_device_list(void)
diff --git a/subsys/power/policy/pm_policy.h b/subsys/power/policy/pm_policy.h
index 259eb7c9fa021..51ced6ae9d8ca 100644
--- a/subsys/power/policy/pm_policy.h
+++ b/subsys/power/policy/pm_policy.h
@@ -40,6 +40,30 @@ void sys_pm_resume_devices(void);
  *        power state(clock gate device), given the system PM state.
  */
 bool sys_pm_policy_clock_gate_devices(enum power_states pm_state);
+#elif defined(CONFIG_DEVICE_PM_DISTRIBUTED_METHOD)
+/**
+ * @brief Function to acquire system suspend lock. Any device
+ *        should acquire suspend lock before any transfer.
+ */
+void sys_pm_suspend_lock_acquire(void);
+
+/**
+ * @brief Function to release system suspend lock. Any device
+ *        should release suspend lock after done any transfer.
+ */
+void sys_pm_suspend_lock_release(void);
+
+/**
+ * @brief Function to determine whether to put whole system in
+ *        suspend state based on suspend lock.
+ */
+bool sys_pm_allow_suspend(void);
+
+/**
+ * @brief Function to notify system pm state to the devices in
+ *        PM device list if no any device hold suspend lock.
+ */
+void sys_pm_power_state_notify_devices(enum power_states state)
 #endif
 
 /**
diff --git a/subsys/power/power.c b/subsys/power/power.c
index 38f622a29ef62..407bef4b75fc8 100644
--- a/subsys/power/power.c
+++ b/subsys/power/power.c
@@ -110,6 +110,15 @@ enum power_states _sys_suspend(s32_t ticks)
 			sys_pm_clock_gate_devices();
 		}
 	}
+#elif defined(CONFIG_DEVICE_PM_DISTRIBUTED_METHOD)
+	if (sys_pm_allow_suspend()) {
+		sys_pm_power_state_notify_devices(pm_state);
+	} else {
+		LOG_ERR("some devices don't allow suspend!");
+		sys_pm_notify_power_state_exit(pm_state);
+		pm_state = SYS_POWER_STATE_ACTIVE;
+		return pm_state;
+	}
 #endif
 
 	if (deep_sleep) {
@@ -130,6 +139,8 @@ enum power_states _sys_suspend(s32_t ticks)
 		/* Turn on peripherals and restore device states as necessary */
 		sys_pm_resume_devices();
 	}
+#elif defined(CONFIG_DEVICE_PM_DISTRIBUTED_METHOD)
+	sys_pm_power_state_notify_devices(SYS_POWER_STATE_ACTIVE);
 #endif
 	sys_pm_log_debug_info(pm_state);
 
