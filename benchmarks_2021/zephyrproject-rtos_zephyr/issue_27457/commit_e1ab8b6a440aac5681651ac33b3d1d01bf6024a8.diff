diff --git a/drivers/audio/CMakeLists.txt b/drivers/audio/CMakeLists.txt
index ce2c51a6e0072..a06848ec3678f 100644
--- a/drivers/audio/CMakeLists.txt
+++ b/drivers/audio/CMakeLists.txt
@@ -13,5 +13,6 @@ zephyr_library_sources_ifdef(CONFIG_AUDIO_INTEL_DMIC	decimation/pdm_decim_int32_
 zephyr_library_sources_ifdef(CONFIG_AUDIO_INTEL_DMIC	decimation/pdm_decim_int32_06_4156_5100_010_095.c)
 zephyr_library_sources_ifdef(CONFIG_AUDIO_INTEL_DMIC	decimation/pdm_decim_int32_08_4156_5380_010_090.c)
 zephyr_library_sources_ifdef(CONFIG_AUDIO_INTEL_DMIC	decimation/pdm_decim_table.c)
+zephyr_library_sources_ifdef(CONFIG_PDM_NRFX		pdm/pdm_nrfx.c)
 zephyr_library_sources_ifdef(CONFIG_AUDIO_MPXXDTYY	mpxxdtyy.c)
 zephyr_library_sources_ifdef(CONFIG_AUDIO_MPXXDTYY	mpxxdtyy-i2s.c)
diff --git a/drivers/audio/Kconfig b/drivers/audio/Kconfig
index 4f742599bf43a..aafcf3948a63b 100644
--- a/drivers/audio/Kconfig
+++ b/drivers/audio/Kconfig
@@ -1,6 +1,7 @@
 # Audio Codec configuration options
 
 # Copyright (c) 2018 Intel Corporation
+# Copyright (c) 2020 Oticon A/S
 # SPDX-License-Identifier: Apache-2.0
 
 menuconfig AUDIO
@@ -55,4 +56,19 @@ source "drivers/audio/Kconfig.mpxxdtyy"
 
 endif # AUDIO_DMIC
 
+menuconfig PDM
+	bool "PDM (Pulse Density Modulation) Drivers"
+	help
+	  Enable config options for PDM drivers.
+
+if PDM
+
+module = PDM
+module-str = pdm
+source "subsys/logging/Kconfig.template.log_config"
+
+source "drivers/audio/Kconfig.nrfx_pdm"
+
+endif # PDM
+
 endif # AUDIO
diff --git a/drivers/audio/Kconfig.nrfx_pdm b/drivers/audio/Kconfig.nrfx_pdm
new file mode 100644
index 0000000000000..f116de6ff9e60
--- /dev/null
+++ b/drivers/audio/Kconfig.nrfx_pdm
@@ -0,0 +1,13 @@
+# Copyright (c) 2020, Oticon A/S
+# SPDX-License-Identifier: Apache-2.0
+
+# Workaround for not being able to have commas in macro arguments
+DT_COMPAT_NORDIC_NRF_PDM   := nordic,nrf-pdm
+
+config PDM_NRFX
+	bool "nRF PDM nrfx driver"
+	default y
+	depends on HAS_HW_NRF_PDM && $(dt_nodelabel_has_compat,pdm,$(DT_COMPAT_NORDIC_NRF_PDM))
+    select NRFX_PDM
+    help
+        Enable support for nrfx Hardware PDM driver for nRF52 MCU series.
\ No newline at end of file
diff --git a/drivers/audio/pdm/pdm_nrfx.c b/drivers/audio/pdm/pdm_nrfx.c
new file mode 100644
index 0000000000000..b5850868c11e8
--- /dev/null
+++ b/drivers/audio/pdm/pdm_nrfx.c
@@ -0,0 +1,182 @@
+/*
+ * Copyright (c) 2020 Oticon A/S
+ *
+ * SPDX-License-Identifier: Apache-2.0
+ */
+#include <nrfx_pdm.h>
+#include <drivers/audio/pdm.h>
+#include <hal/nrf_gpio.h>
+#include <stdbool.h>
+
+#define LOG_LEVEL CONFIG_PDM_LOG_LEVEL
+#include <logging/log.h>
+LOG_MODULE_REGISTER(pdm_nrfx);
+
+#define DT_DRV_COMPAT nordic_nrf_pdm
+DEVICE_DECLARE(pdm_0);
+
+#define CLK_PIN       DT_PROP(DT_NODELABEL(pdm), clk_pin)
+#define DIN_PIN       DT_PROP(DT_NODELABEL(pdm), din_pin)
+
+#define PDM_STACK_SIZE 256
+#define PDM_PRIORITY   5
+
+#define PDM_NRFX_NUMBER_OF_BUFFERS 2
+#define PDM_NRFX_BUFFER_SIZE       256
+
+K_THREAD_STACK_DEFINE(pdm_stack, PDM_STACK_SIZE);
+
+struct k_work_q m_pdm_work_q;
+
+struct pdm_nrfx_config {
+	nrfx_pdm_config_t initial_config;
+};
+
+struct pdm_nrfx_data {
+	pdm_data_handler_t data_handler;
+};
+
+struct buffer_released_data {
+	struct k_work work;
+	int16_t buffer[PDM_NRFX_BUFFER_SIZE];
+};
+
+static struct pdm_nrfx_data m_pdm_nrfx_data;
+static struct buffer_released_data m_buff_released;
+static int16_t m_next_buffer[PDM_NRFX_BUFFER_SIZE][PDM_NRFX_NUMBER_OF_BUFFERS];
+static uint8_t m_active_buffer;
+
+
+static void release_buffer(struct k_work *item)
+{
+	struct buffer_released_data *data =
+		CONTAINER_OF(item, struct buffer_released_data, work);
+
+	m_pdm_nrfx_data.data_handler(data->buffer, PDM_NRFX_BUFFER_SIZE);
+}
+
+
+static int pdm_nrfx_set_data_handler(struct device *dev,
+				     pdm_data_handler_t handler_func)
+{
+	if (handler_func == NULL) {
+		LOG_ERR("Data handler cannot be NULL.\n");
+		return -EINVAL;
+	}
+	m_pdm_nrfx_data.data_handler = handler_func;
+	return 0;
+}
+
+
+static void pdm_nrfx_event_handler(nrfx_pdm_evt_t const *p_evt)
+{
+	if (p_evt->error == NRFX_PDM_ERROR_OVERFLOW) {
+		LOG_ERR("Overflow error when handling event.\n");
+		return;
+	}
+
+	/* If a buffer was requested,
+	 * provide a new one, and alternate the active buffer
+	 */
+	if (p_evt->buffer_requested) {
+		nrfx_err_t result = nrfx_pdm_buffer_set(&m_next_buffer[0][m_active_buffer], PDM_NRFX_BUFFER_SIZE);
+		if (result != NRFX_SUCCESS) {
+			LOG_ERR("Failed to set new buffer, error %d.\n", result);
+		}
+		m_active_buffer = (~m_active_buffer) & 0x01;
+	}
+
+	/* If a buffer has been released,
+	 * save it and submit it to the workqueue for the event handler
+	 */
+	if (p_evt->buffer_released != NULL) {
+		uint16_t i;
+		for (i = 0; i < PDM_NRFX_BUFFER_SIZE; i++) {
+			m_buff_released.buffer[i] = p_evt->buffer_released[i];
+		}
+		k_work_submit(&m_buff_released.work);
+	}
+}
+
+
+static int pdm_nrfx_start(struct device *dev)
+{
+	nrfx_err_t result = nrfx_pdm_buffer_set(&m_next_buffer[0][m_active_buffer], PDM_NRFX_BUFFER_SIZE);
+
+	if (result != NRFX_SUCCESS) {
+		LOG_ERR("Failed to set new buffer, error %d.\n", result);
+	}
+	m_active_buffer = (~m_active_buffer) & 0x01;
+	result = nrfx_pdm_start();
+	if (result == NRFX_ERROR_BUSY) {
+		LOG_ERR("Failed to start PDM sampling, device %s is busy.\n", dev->name);
+		return -EBUSY;
+	}
+	return 0;
+}
+
+
+static int pdm_nrfx_stop(struct device *dev)
+{
+	nrfx_err_t result = nrfx_pdm_stop();
+
+	if (result == NRFX_ERROR_BUSY) {
+		LOG_ERR("Failed to stop PDM sampling, device %s is busy.\n", dev->name);
+		return -EBUSY;
+	}
+	return 0;
+}
+
+
+static int pdm_nrfx_init(struct device *dev)
+{
+	const struct pdm_nrfx_config *config = dev->config_info;
+
+	nrfx_err_t result = nrfx_pdm_init(&config->initial_config, pdm_nrfx_event_handler);
+
+	if (result != NRFX_SUCCESS) {
+		LOG_ERR("Failed to initialize device: %s\n", dev->name);
+		return -EBUSY;
+	}
+
+	k_work_q_start(&m_pdm_work_q, pdm_stack, K_THREAD_STACK_SIZEOF(pdm_stack), PDM_PRIORITY);
+	k_work_init(&m_buff_released.work, release_buffer);
+
+	m_active_buffer = 0x00;
+
+	IRQ_CONNECT(DT_INST_IRQN(0), DT_INST_IRQ(0, priority),
+		    nrfx_isr, nrfx_pdm_irq_handler, 0);
+	return 0;
+}
+
+static const struct pdm_driver_api m_pdm_nrfx_api = {
+	.start = pdm_nrfx_start,
+	.stop = pdm_nrfx_stop,
+	.set_data_handler = pdm_nrfx_set_data_handler
+};
+
+static const struct pdm_nrfx_config m_pdm_nrfx_config = {
+	.initial_config = NRFX_PDM_DEFAULT_CONFIG(CLK_PIN, DIN_PIN)
+};
+
+/*
+ * There is only one instance on supported SoCs, so inst is guaranteed
+ * to be 0 if any instance is okay. (We use pdm_0 above, so the driver
+ * is relying on the numeric instance value in a way that happens to
+ * be safe.)
+ *
+ * Just in case that assumption becomes invalid in the future, we use
+ * a BUILD_ASSERT().
+ */
+#define PDM_INIT(inst)						\
+	BUILD_ASSERT((inst) == 0,				\
+		     "multiple instances not supported");	\
+	DEVICE_AND_API_INIT(pdm_0, DT_INST_LABEL(0),		\
+			    pdm_nrfx_init,			\
+			    &m_pdm_nrfx_data,			\
+			    &m_pdm_nrfx_config,			\
+			    POST_KERNEL,			\
+			    CONFIG_KERNEL_INIT_PRIORITY_DEVICE,	\
+			    &m_pdm_nrfx_api);
+
+DT_INST_FOREACH_STATUS_OKAY(PDM_INIT)
diff --git a/include/drivers/audio/pdm.h b/include/drivers/audio/pdm.h
new file mode 100644
index 0000000000000..7517095952d02
--- /dev/null
+++ b/include/drivers/audio/pdm.h
@@ -0,0 +1,140 @@
+/*
+ * Copyright (c) 2020 Oticon A/S.
+ *
+ * SPDX-License-Identifier: Apache-2.0
+ */
+
+/**
+ * @file
+ * @brief Public PDM Driver APIs
+ */
+
+#ifndef ZEPHYR_INCLUDE_DRIVERS_PDM_H_
+#define ZEPHYR_INCLUDE_DRIVERS_PDM_H_
+
+/**
+ * @brief PDM Interface
+ * @defgroup pdm_interface PDM Interface
+ * @ingroup io_interfaces
+ * @{
+ */
+
+#include <errno.h>
+#include <zephyr/types.h>
+#include <stddef.h>
+#include <device.h>
+
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+
+/**
+ * @typedef pdm_start_t
+ * @brief API for starting PDM sampling
+ * See @a pdm_start() for argument description
+ */
+typedef int (*pdm_start_t)(struct device *dev);
+
+/**
+ * @typedef pdm_stop_t
+ * @brief API for stopping PDM sampling
+ * See @a pdm_stop() for argument description
+ */
+typedef int (*pdm_stop_t)(struct device *dev);
+
+/**
+ * @typedef pdm_data_handler_t
+ * @brief User data handler for PDM samples
+ *
+ * @param data      Pointer to the PDM sample
+ * @param data_size Size of PDM sample
+ */
+typedef void (*pdm_data_handler_t)(int16_t *data,
+				   uint16_t data_size);
+
+/**
+ * @typedef pdm_set_data_handler_t
+ * @brief Set the data handler
+ * See @a pdm_set_data_handler_t() for argument description
+ */
+typedef int (*pdm_set_data_handler_t)(struct device *dev,
+				      pdm_data_handler_t handler);
+
+/** @brief PDM driver API definition. */
+__subsystem struct pdm_driver_api {
+	pdm_start_t start;
+	pdm_stop_t stop;
+	pdm_set_data_handler_t set_data_handler;
+};
+
+/**
+ * @brief Function for starting PDM sampling.
+ *
+ * @param dev Pointer to the device structure for the driver instance
+ *
+ * @retval 0 If successful.
+ * @retval Negative errno code if failure.
+ */
+__syscall int pdm_start(struct device *dev);
+
+static inline int z_impl_pdm_start(struct device *dev)
+{
+	struct pdm_driver_api *api;
+
+	api = (struct pdm_driver_api *)dev->driver_api;
+	return api->start(dev);
+}
+
+/**
+ * @brief Function for stopping PDM sampling.
+ *
+ * @param dev Pointer to the device structure for the driver instance.
+ *
+ * @retval 0 If successful.
+ * @retval Negative errno code if failure.
+ */
+
+__syscall int pdm_stop(struct device *dev);
+
+static inline int z_impl_pdm_stop(struct device *dev)
+{
+	struct pdm_driver_api *api;
+
+	api = (struct pdm_driver_api *)dev->driver_api;
+	return api->stop(dev);
+}
+
+/**
+ * @brief Function for providing the user event handler.
+ *
+ * @param dev     Pointer to the device structure for the driver instance.
+ * @param handler Function pointer to the user data handler, cannot be NULL.
+ *
+ * @retval 0 If successful.
+ * @retval Negative errno code if failure.
+ */
+__syscall int pdm_set_data_handler(struct device *dev,
+				   pdm_data_handler_t handler);
+
+static inline int z_impl_pdm_set_data_handler(struct device *dev,
+					      pdm_data_handler_t handler)
+{
+	struct pdm_driver_api *api;
+
+	api = (struct pdm_driver_api *)dev->driver_api;
+	return api->set_data_handler(dev, handler);
+}
+
+
+#ifdef __cplusplus
+}
+#endif
+
+/**
+ * @}
+ */
+
+#include <syscalls/pdm.h>
+
+#endif /* ZEPHYR_INCLUDE_DRIVERS_PDM_H_ */
