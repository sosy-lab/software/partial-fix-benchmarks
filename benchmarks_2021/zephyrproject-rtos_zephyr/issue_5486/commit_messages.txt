Bluetooth: mesh: Account for scan window delaying adv events

New advertising started while scanning is already enabled
would delay the first advertisement event until the end of
the current overlapping scan window in the Zephyr native BLE
controller implementation. Hence, consider this scan window
duration when calculating the advertising stop.

Relates to: #6083

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Relates to: #5486

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Relates to: #5486

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Also, fixed master scheduling by correctly accounting for
the jitter between each master event.

Tested ticker information from tests/bluetooth/shell sample
with 8 master connections is:

	ticker> info
	Query done (0xff, err= 0).
	Tickers: 8.
	Tick: 11068237 (337775787us).
	---------------------
	 id   offset   offset
	      (tick)     (us)
	---------------------
	007 00001044 00031860
	008 00001123 00034271
	009 00001203 00036712
	010 00001282 00039123
	011 00001362 00041564
	012 00001441 00043975
	013 00001520 00046386
	014 00001600 00048828
	---------------------

Relates to: #5486

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Also, fixed master scheduling by correctly accounting for
the jitter between each master event.

Tested ticker information from tests/bluetooth/shell sample
with 8 master connections is:

	ticker> info
	Query done (0xff, err= 0).
	Tickers: 8.
	Tick: 11068237 (337775787us).
	---------------------
	 id   offset   offset
	      (tick)     (us)
	---------------------
	007 00001044 00031860
	008 00001123 00034271
	009 00001203 00036712
	010 00001282 00039123
	011 00001362 00041564
	012 00001441 00043975
	013 00001520 00046386
	014 00001600 00048828
	---------------------

Relates to: #5486

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Also, fixed master scheduling by correctly accounting for
the jitter between each master event.

Relates to: #5486

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Also, fixed master scheduling by correctly accounting for
the jitter between each master event.

Relates to: #5486

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Also, fixed master scheduling by correctly accounting for
the jitter between each master event.

Relates to: #5486

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: controller: Fix incorrect master role scheduling

Revert incorrect calculation introduced in
commit ec5a787da2ab ("Bluetooth: controller: Fix multiple
master role event scheduling") and revert a related
incorrect fix in commit a02606cbf9d2 ("Bluetooth:
controller: Fix missing ticks to us conversion").

Fixes the controller assert in ctrl.c line number 1477. A
64-bit arithmetic took ~35 us in Radio ISR for nRF51 causing
the ISR to take too much time before packet buffer could be
set.

Also, fixed master scheduling by correctly accounting for
the jitter between each master event.

Relates to: #5486

Signed-off-by: Vinayak Kariappa Chettimada <vich@nordicsemi.no>
Bluetooth: Workaround privacy feature issue

This fixes intercompatibility issues with controllers supporting
privacy feature.
Core Spec requires to use network privacy mode as a default when
peer device provides its IRK during bonding when LL Privacy is used,
which is the case for Zephyr. We've seen devices including PTS
which exchanges it's IRK but is not aware about network privacy
mode. This results in Zephyr not able do be reconnect to such bonded
devices.
This workaround sets device privacy mode to be able to reconnect
to such devices.

Fixes #4989
Fixes #5486

Signed-off-by: Mariusz Skamra <mariusz.skamra@codecoup.pl>
Bluetooth: Workaround privacy feature issue

This fixes intercompatibility issues with controllers supporting
privacy feature.
Core Spec requires to use network privacy mode as a default when
peer device provides its IRK during bonding when LL Privacy is used,
which is the case for Zephyr. We've seen devices including PTS
which exchanges it's IRK but is not aware about network privacy
mode. This results in Zephyr not able do be reconnect to such bonded
devices.
This workaround sets device privacy mode to be able to reconnect
to such devices.

Fixes #4989
Fixes #5486

Signed-off-by: Mariusz Skamra <mariusz.skamra@codecoup.pl>
Bluetooth: Workaround privacy feature issue

This fixes intercompatibility issues with controllers supporting
privacy feature.
Core Spec requires to use network privacy mode as a default when
peer device provides its IRK during bonding when LL Privacy is used,
which is the case for Zephyr. We've seen devices including PTS
which exchanges it's IRK but is not aware about network privacy
mode. This results in Zephyr not able do be reconnect to such bonded
devices.
This workaround sets device privacy mode to be able to reconnect
to such devices.

Fixes #4989
Fixes #5486

Signed-off-by: Mariusz Skamra <mariusz.skamra@codecoup.pl>
