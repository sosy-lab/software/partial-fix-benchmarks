Fixed indefinite loop when reaching end of stream during find.
Fixed extract 4gb zip files without zip64 via recovery method. #431
Fixed extract 4gb zip files without zip64 via recovery method. #431
More robust recovery method, seek to next local header and seek backward for data descriptor. #431
Fixed issues finding data descriptor when previous compressed size is small. #431
