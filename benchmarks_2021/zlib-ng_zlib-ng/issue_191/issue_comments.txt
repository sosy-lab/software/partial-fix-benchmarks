MemorySanitizer: use-of-uninitialized-value in deflate_medium.c:289
I think this is duplicate of previous bug... s->strstart is uninitialized...
You are right. It happens in the same function as bug #186 in a very similar piece of code as in deflate_medium.c:259.
s->strstart should be initialized inside lm_init() which gets called in deflateReset() and indirectly from deflateInit2_().