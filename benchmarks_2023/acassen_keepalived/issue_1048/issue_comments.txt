Dbus interface allows overwriting arbitrary files and insecure permissions are used
This is a good catch.

Commit c6247a9 (and 5241e4d) change the default umask to 022 (i.e. removing group and other write permission) and allow the umask both to be specified via a command line option `--umask=`, and in the configuration (global_defs option umask, which can be set symbolically). This seeks to allow backward compatibility as far as possible, while also allowing users who want more security to be able to limit the file access permissions.

Commit 04f2d32 sets the O_NOFOLLOW flag on all files opened for writing (other than files under /proc), so that the filename part of the pathname cannot be a symbolic link. This is in considerably more places than the examples identified in this issue, so the impact is potentially quite wide ranging.

Thank you for the quick and complete fix. I asked MITRE for a CVE and they assigned three:
CVE-2018-19044 for https://github.com/acassen/keepalived/commit/04f2d32871bb3b11d7dc024039952f2fe2750306
CVE-2018-19045 for https://github.com/acassen/keepalived/commit/c6247a9ef2c7b33244ab1d3aa5d629ec49f0a067, https://github.com/acassen/keepalived/commit/5241e4d7b177d0b6f073cfc9ed5444bf51ec89d6
CVE-2018-19046 for the case, that a user already created /tmp/keepalived.data or /tmp/keepalived.stats with mode 666, so it's not covered by the umask fix and should still be fixed
Commits ac8e2ef, 26c8d63 and 17f9441 should fully resolve CVE-2018-19046.

How do we get the information at MITRE regarding these CVEs updates to state that they are all now resolved?
First of all thank you for your quick reaction.

The CVEs are used mainly for tracking. MITRE will not do that, but e.g. SUSE will use these ids to track the state of our packages. Other distributions do the same and provides data to users so they can check if their installation is already fixed. For you this should be done now.
Some other vulnerability databases also update entries with this type of information so it is appreciated!