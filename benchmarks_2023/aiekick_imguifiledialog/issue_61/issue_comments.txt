Items Dissapear after Mouse Release
Hello,

thanks for reported this.

btw not sure if in understand well.

you say, when you double click for open a directory, the frame after, the end of the list is not rendered ?

if this is that its normal.
When you open the dir, ImGuiFileDialog will start to scan the new directory and so empty the current list.
and its happenning during the display, so for avoid unecessary extra code, i decided to break the loop.
so as weel the end of the list is not rendered the next frame, but only the next frame.

is this a problem for you ?

Ok that explains indeed why it happens. It is a problem though, because when I do only a single click (not opening the next directory/file) it also happens. This causes the itemlist to look glitchy every time you click an item.
After your explanation I figured out that it could be avoided by commenting out line #1519 `return true; // needToBreakTheloop`. Maybe this can be moved inside the if statement which checks for double clicks:
```
if (ImGui::IsMouseDoubleClicked(0)) // 0 -> left mouse button double click
{
	m_PathClicked = SelectDirectory(vInfos); 
        return true; // needToBreakTheloop
}
```
so the problem for you is the glitch ?
Yes it is only visual. But gets annoying quickly. I can make a merge request of my tiny modification if you like?
yes please. your solution is working fine
Ok done. See pull request.
thanks you :)
Thanks, problem solved. (=
Unfortunately this issue has resurfaced. It can probably be easily fixed by commenting line 3897 in ImGuiFileDialog.cpp:
```
// return true; // needToBreakTheloop
```
Thanks. 
yes its possible i rewrote it a bit for the new picture system.

thanks
fixed ! :)