Improve CreateNative failure message (#903)

This confuses everyone.
Use natural sorting for map lists (Fixes #892)
Revert "Use natural sorting for map lists (Fixes #892)"

This reverts commit a4e169aa8fb534be30a130654fe589371e6cb935.
Use natural sorting for map lists (Fixes #892)
Use natural sorting for map lists (Fixes #892)
Use natural sorting for map lists (Fixes #892)
Use natural sorting for map lists (Fixes #892) (#907)

This produces consistently better results, especially for games such as L4D(2).
