Spike vs Dragon
The spike kills the dragon zombie now but I need to manually add that -127 it seems. Also just noticed while comparing, it should be shootable after it lands.

Actually all of those large spikes need to be shootable after they land.

The spike killing the dragon is confirmed as working.

I made it so they are all shootable after they land, but when testing they didn't make any kind of noise or shake while doing so.

Ah, right. Can confirm. Shake is part of a larger issue, so I'll mark this as closed.

Reopening this because I noticed a strange bug. After picking up whatever item the newly slain dragon leaves behind (health/ammo/exp) the -127 sign reappears.
