diff --git a/tests/CMakeLists.txt b/tests/CMakeLists.txt
index d17076da5..d82fd56a1 100644
--- a/tests/CMakeLists.txt
+++ b/tests/CMakeLists.txt
@@ -97,6 +97,11 @@ if(AVIF_ENABLE_GTEST)
     target_include_directories(avifalphapremtest PRIVATE ${GTEST_INCLUDE_DIRS})
     add_test(NAME avifalphapremtest COMMAND avifalphapremtest)
 
+    add_executable(avifanimationtest gtest/avifanimationtest.cc)
+    target_link_libraries(avifanimationtest aviftest_helpers ${GTEST_LIBRARIES})
+    target_include_directories(avifanimationtest PRIVATE ${GTEST_INCLUDE_DIRS})
+    add_test(NAME avifanimationtest COMMAND avifanimationtest ${CMAKE_CURRENT_SOURCE_DIR}/data/)
+
     add_executable(avifbasictest gtest/avifbasictest.cc)
     target_link_libraries(avifbasictest aviftest_helpers ${GTEST_BOTH_LIBRARIES})
     target_include_directories(avifbasictest PRIVATE ${GTEST_INCLUDE_DIRS})
diff --git a/tests/data/README.md b/tests/data/README.md
index e00646707..264633af3 100644
--- a/tests/data/README.md
+++ b/tests/data/README.md
@@ -419,6 +419,24 @@ Source: Same pixels as `color_grid_alpha_nogrid.avif` encoded with
 [`cavif-rs`](https://github.com/kornelski/cavif-rs) with the
 [alpha `ispe` fix](https://github.com/kornelski/avif-serialize/pull/4) removed.
 
+## Animated Images
+
+### File [colors-animated-8bpc.avif](colors-animated-8bpc.avif)
+
+![](colors-animated-8bpc.avif)
+
+License: [same as libavif](https://github.com/AOMediaCodec/libavif/blob/main/LICENSE)
+
+Source: Random colors generated with ffmpeg.
+
+An animated AVIF image file with the following attributes:
+* Width: 150
+* Height: 150
+* Alpha present: No
+* YUV Format: 420
+* Repetition Count: 0
+* Frame count: 5
+
 # Other Test Files
 
 ### File [sRGB2014.icc](sRGB2014.icc)
diff --git a/tests/data/colors-animated-8bpc.avif b/tests/data/colors-animated-8bpc.avif
new file mode 100644
index 000000000..afa875d10
Binary files /dev/null and b/tests/data/colors-animated-8bpc.avif differ
diff --git a/tests/gtest/avifanimationtest.cc b/tests/gtest/avifanimationtest.cc
new file mode 100644
index 000000000..4b8905847
--- /dev/null
+++ b/tests/gtest/avifanimationtest.cc
@@ -0,0 +1,46 @@
+// Copyright 2023 Google LLC
+// SPDX-License-Identifier: BSD-2-Clause
+
+#include "avif/avif.h"
+#include "aviftest_helpers.h"
+#include "gtest/gtest.h"
+
+namespace libavif {
+namespace {
+
+// Used to pass the data folder path to the GoogleTest suites.
+const char* data_path = nullptr;
+
+TEST(AvifDecodeTest, AnimatedImage) {
+  if (!testutil::Av1DecoderAvailable()) {
+    GTEST_SKIP() << "AV1 Codec unavailable, skip test.";
+  }
+  const char* file_name = "colors-animated-8bpc.avif";
+  testutil::AvifDecoderPtr decoder(avifDecoderCreate(), avifDecoderDestroy);
+  ASSERT_NE(decoder, nullptr);
+  ASSERT_EQ(avifDecoderSetIOFile(decoder.get(),
+                                 (std::string(data_path) + file_name).c_str()),
+            AVIF_RESULT_OK);
+  ASSERT_EQ(avifDecoderParse(decoder.get()), AVIF_RESULT_OK);
+  EXPECT_EQ(decoder->alphaPresent, AVIF_FALSE);
+  EXPECT_EQ(decoder->imageCount, 5);
+  EXPECT_EQ(decoder->repetitionCount, 0);
+  for (int i = 0; i < 5; ++i) {
+    EXPECT_EQ(avifDecoderNextImage(decoder.get()), AVIF_RESULT_OK);
+  }
+}
+
+}  // namespace
+}  // namespace libavif
+
+int main(int argc, char** argv) {
+  ::testing::InitGoogleTest(&argc, argv);
+  if (argc != 2) {
+    std::cerr << "There must be exactly one argument containing the path to "
+                 "the test data folder"
+              << std::endl;
+    return 1;
+  }
+  libavif::data_path = argv[1];
+  return RUN_ALL_TESTS();
+}
