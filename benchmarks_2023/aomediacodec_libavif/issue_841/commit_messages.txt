Allow alpha to be specified as an auxl of color tiles

Allows images of the following form to be decoded as images with
alpha channel (since the spec does not disallow these):

[primary item grid]
   ^       ^
   |dimg   |dimg
   |       |
[color] [color]
   ^       ^
   |auxl   |auxl
   |       |
[alpha] [alpha]

Fixes Issue #1203.

I have tested this commit with the image attached in issue #1203
and it decodes as intended with the alpha plane.
Use global configs for keyframe interval

Instead of computing the required keyframe interval in libavif
code and passing the "force keyframe" flag on individual frames,
use each encoder's global configuration parameter to set the
maximum keyframe interval.

For libaom: `kf_max_dist`.
For rav1e: `key_frame_interval`.
For svt-av1: `intra_period_length` and `force_key_frames`.

Here's the output obtained from various encoders for encoding an
animated file with 10 frames with different values for
--keyframe. Each line is read as follows:
--keyframe value: list of key frames before => list of keyframes after.

aom:
 --keyframe 0: same before and after.
 --keyframe 1: same before and after.
 --keyframe 2: 1,2,3,4,5,6,7,8,9,19 => 1,3,5,7,9 (the old behavior was not incorrect, but new one is exact).
 --keyframe 5: 1,2,6,7 =>1,6 (the old behavior was not incorrect, but new one is exact).

rav1e:
  * no changes. same output before and after.

svt-av1:
 --keyframe 0: same before and after.
 --keyframe 1: 1 => 1,2,3,4,5,6,7,8,9,10 (old behavior was incorrect).
 --keyframe 2: 1 => 1,3,5,7,9 (old behavior was incorrect).
 --keyframe 5: 1 => 1,6 (old behavior was incorrect).

To sum up:
 * svt-av1 was not producing the intended output with --keyframe
   before this change.
 * libaom had some weirdness in keyframe spacing when using the
   force keyframe flag on each frame and that weirdness goes away
   when using the kf_maxdist configuration (see:
   https://bugs.chromium.org/p/aomedia/issues/detail?id=3445).

The `AVIF_ADD_IMAGE_FLAG_FORCE_KEYFRAME` flag is retained in the
codebase since there are some follow-up changes that will make
use of the flag to synchronize keyframe placement between the
color and alpha plane encoders (Issue #841).
Use global configs for keyframe interval

Instead of computing the required keyframe interval in libavif
code and passing the "force keyframe" flag on individual frames,
use each encoder's global configuration parameter to set the
maximum keyframe interval.

For libaom: `kf_max_dist`.
For rav1e: `key_frame_interval`.
For svt-av1: `intra_period_length` and `force_key_frames`.

Here's the output obtained from various encoders for encoding an
animated file with 10 frames with different values for
--keyframe. Each line is read as follows:
--keyframe value: list of key frames before => list of keyframes after.

aom:
 --keyframe 0: same before and after.
 --keyframe 1: same before and after.
 --keyframe 2: 1,2,3,4,5,6,7,8,9,10 => 1,3,5,7,9 (the old behavior was not incorrect, but new one is exact).
 --keyframe 5: 1,2,6,7 =>1,6 (the old behavior was not incorrect, but new one is exact).

rav1e:
  * no changes. same output before and after.

svt-av1:
 --keyframe 0: same before and after.
 --keyframe 1: 1 => 1,2,3,4,5,6,7,8,9,10 (old behavior was incorrect).
 --keyframe 2: 1 => 1,3,5,7,9 (old behavior was incorrect).
 --keyframe 5: 1 => 1,6 (old behavior was incorrect).

To sum up:
 * svt-av1 was not producing the intended output with --keyframe
   before this change.
 * libaom had some weirdness in keyframe spacing when using the
   force keyframe flag on each frame and that weirdness goes away
   when using the kf_max_dist configuration (see:
   https://bugs.chromium.org/p/aomedia/issues/detail?id=3445).

The `AVIF_ADD_IMAGE_FLAG_FORCE_KEYFRAME` flag is retained in the
codebase since there are some follow-up changes that will make
use of the flag to synchronize keyframe placement between the
color and alpha plane encoders (Issue #841).
Use global configs for keyframe interval

Instead of computing the required keyframe interval in libavif
code and passing the "force keyframe" flag on individual frames,
use each encoder's global configuration parameter to set the
maximum keyframe interval.

For libaom: `kf_max_dist`.
For rav1e: `key_frame_interval`.
For svt-av1: `intra_period_length` and `force_key_frames`.

Here's the output obtained from various encoders for encoding an
animated file with 10 frames with different values for
--keyframe. Each line is read as follows:
--keyframe value: list of key frames before => list of keyframes after.

aom:
 --keyframe 0: same before and after.
 --keyframe 1: same before and after.
 --keyframe 2: 1,2,3,4,5,6,7,8,9,10 => 1,3,5,7,9 (the old behavior was not incorrect, but new one is exact).
 --keyframe 5: 1,2,6,7 =>1,6 (the old behavior was not incorrect, but new one is exact).

rav1e:
  * no changes. same output before and after.

svt-av1:
 --keyframe 0: same before and after.
 --keyframe 1: 1 => 1,2,3,4,5,6,7,8,9,10 (old behavior was incorrect).
 --keyframe 2: 1 => 1,3,5,7,9 (old behavior was incorrect).
 --keyframe 5: 1 => 1,6 (old behavior was incorrect).

To sum up:
 * svt-av1 was not producing the intended output with --keyframe
   before this change.
 * libaom had some weirdness in keyframe spacing when using the
   force keyframe flag on each frame and that weirdness goes away
   when using the kf_max_dist configuration (see:
   https://bugs.chromium.org/p/aomedia/issues/detail?id=3445).

The `AVIF_ADD_IMAGE_FLAG_FORCE_KEYFRAME` flag is retained in the
codebase since there are some follow-up changes that will make
use of the flag to synchronize keyframe placement between the
color and alpha plane encoders (Issue #841).
Use global configs for keyframe interval

Instead of computing the required keyframe interval in libavif
code and passing the "force keyframe" flag on individual frames,
use each encoder's global configuration parameter to set the
maximum keyframe interval.

For libaom: `kf_max_dist`.
For rav1e: `key_frame_interval`.
For svt-av1: `intra_period_length` and `force_key_frames`.

Here's the output obtained from various encoders for encoding an
animated file with 10 frames with different values for
--keyframe. Each line is read as follows:
--keyframe value: list of key frames before => list of keyframes after.

aom:
 --keyframe 0: same before and after.
 --keyframe 1: same before and after.
 --keyframe 2: 1,2,3,4,5,6,7,8,9,10 => 1,3,5,7,9 (the old behavior was not incorrect, but new one is exact).
 --keyframe 5: 1,2,6,7 =>1,6 (the old behavior was not incorrect, but new one is exact).

rav1e:
  * no changes. same output before and after.

svt-av1:
 --keyframe 0: same before and after.
 --keyframe 1: 1 => 1,2,3,4,5,6,7,8,9,10 (old behavior was incorrect).
 --keyframe 2: 1 => 1,3,5,7,9 (old behavior was incorrect).
 --keyframe 5: 1 => 1,6 (old behavior was incorrect).

To sum up:
 * svt-av1 was not producing the intended output with --keyframe
   before this change.
 * libaom had some weirdness in keyframe spacing when using the
   force keyframe flag on each frame and that weirdness goes away
   when using the kf_max_dist configuration (see:
   https://bugs.chromium.org/p/aomedia/issues/detail?id=3445).

The `AVIF_ADD_IMAGE_FLAG_FORCE_KEYFRAME` flag is retained in the
codebase since there are some follow-up changes that will make
use of the flag to synchronize keyframe placement between the
color and alpha plane encoders (Issue #841).
Use global configs for keyframe interval

Instead of computing the required keyframe interval in libavif
code and passing the "force keyframe" flag on individual frames,
use each encoder's global configuration parameter to set the
maximum keyframe interval.

For libaom: `kf_max_dist`.
For rav1e: `key_frame_interval`.
For svt-av1: `intra_period_length` and `force_key_frames`.

Here's the output obtained from various encoders for encoding an
animated file with 10 frames with different values for
--keyframe. Each line is read as follows:
--keyframe value: list of key frames before => list of keyframes after.

aom:
 --keyframe 0: same before and after.
 --keyframe 1: same before and after.
 --keyframe 2: 1,2,3,4,5,6,7,8,9,10 => 1,3,5,7,9 (the old behavior was not incorrect, but new one is exact).
 --keyframe 5: 1,2,6,7 =>1,6 (the old behavior was not incorrect, but new one is exact).

rav1e:
  * no changes. same output before and after.

svt-av1:
 --keyframe 0: same before and after.
 --keyframe 1: 1 => 1,2,3,4,5,6,7,8,9,10 (old behavior was incorrect).
 --keyframe 2: 1 => 1,3,5,7,9 (old behavior was incorrect).
 --keyframe 5: 1 => 1,6 (old behavior was incorrect).

To sum up:
 * svt-av1 was not producing the intended output with --keyframe
   before this change.
 * libaom had some weirdness in keyframe spacing when using the
   force keyframe flag on each frame and that weirdness goes away
   when using the kf_max_dist configuration (see:
   https://bugs.chromium.org/p/aomedia/issues/detail?id=3445).

The `AVIF_ADD_IMAGE_FLAG_FORCE_KEYFRAME` flag is retained in the
codebase since there are some follow-up changes that will make
use of the flag to synchronize keyframe placement between the
color and alpha plane encoders (Issue #841).
Use global configs for keyframe interval

Instead of computing the required keyframe interval in libavif
code and passing the "force keyframe" flag on individual frames,
use each encoder's global configuration parameter to set the
maximum keyframe interval.

For libaom: `kf_max_dist`.
For rav1e: `key_frame_interval`.
For svt-av1: `intra_period_length` and `force_key_frames`.

Here's the output obtained from various encoders for encoding an
animated file with 10 frames with different values for
--keyframe. Each line is read as follows:
--keyframe value: list of key frames before => list of keyframes after.

aom:
 --keyframe 0: same before and after.
 --keyframe 1: same before and after.
 --keyframe 2: 1,2,3,4,5,6,7,8,9,10 => 1,3,5,7,9 (the old behavior was not incorrect, but new one is exact).
 --keyframe 5: 1,2,6,7 =>1,6 (the old behavior was not incorrect, but new one is exact).

rav1e:
  * no changes. same output before and after.

svt-av1:
 --keyframe 0: same before and after.
 --keyframe 1: 1 => 1,2,3,4,5,6,7,8,9,10 (old behavior was incorrect).
 --keyframe 2: 1 => 1,3,5,7,9 (old behavior was incorrect).
 --keyframe 5: 1 => 1,6 (old behavior was incorrect).

To sum up:
 * svt-av1 was not producing the intended output with --keyframe
   before this change.
 * libaom had some weirdness in keyframe spacing when using the
   force keyframe flag on each frame and that weirdness goes away
   when using the kf_max_dist configuration (see:
   https://bugs.chromium.org/p/aomedia/issues/detail?id=3445).

The `AVIF_ADD_IMAGE_FLAG_FORCE_KEYFRAME` flag is retained in the
codebase since there are some follow-up changes that will make
use of the flag to synchronize keyframe placement between the
color and alpha plane encoders (Issue #841).
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
Force keyframe for alpha if color is a keyframe

When the encoder supports it (as of now only libaom does),
disable lagged output when encoding animated images with alpha
channel. When the utput of the color planes is a keyframe, force
a keyframe on the alpha channel as well.

While the spec does not enforce this, and the libavif decoder can
handle files without this requirement just fine, this change is
still an enhancement because most decoders look at whether or not
the color frame is a keyframe to determine seek points.

Fixes: Issue #841
