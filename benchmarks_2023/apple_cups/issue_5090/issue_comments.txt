cupsPrintQuality option in driverless PPDs does not work correctly
Will investigate and potentially fix in a 2.2.x update.

A possible solution is contained in my patch on Issue #5091.

Using numeric values for cupsPrintQuality is not the right approach, see <https://www.cups.org/doc/spec-ppd.html#cupsPrintQuality>.

The code that generates the job ticket already maps cupsPrintQuality strings to their corresponding "print-quality" values, but only if the IPP "print-quality" attribute is not already specified as an option (which will override any cupsPrintQuality selection). It would be useful to see a debug error_log showing the job options in the original print request...

I came to the conclusion by two ways:
1. The source code of CUPS does not contain the word "cupsPrintQuality" (and even "PrintQuality", always used case-insensitive searches) outside of the PPD generator in cups/ppd-cache.c, especially also not in the scheduler or other places in the CUPS library.
2. If I send a job with "-o cupsPrintQuality=High" (and the queue having this option in the PPD and using the "ipp" CUPS backend) the 5th command line argument for filter and backend calls (I assume this is the job ticket as it contains IPPized options like "media") still contains "print-quality=4" and not as expected "print-quality=5". See these lines in error_log: 
```
D [27/Aug/2017:12:43:14 -0300] [Job 3036] job-sheets=none,none
D [27/Aug/2017:12:43:14 -0300] [Job 3036] argv[0]="printer"
D [27/Aug/2017:12:43:14 -0300] [Job 3036] argv[1]="3036"
D [27/Aug/2017:12:43:14 -0300] [Job 3036] argv[2]="till"
D [27/Aug/2017:12:43:14 -0300] [Job 3036] argv[3]="CityMap.pdf"
D [27/Aug/2017:12:43:14 -0300] [Job 3036] argv[4]="1"
D [27/Aug/2017:12:43:14 -0300] [Job 3036] argv[5]="cupsPrintQuality=High finishings=3 media=iso_a4_210x297mm number-up=1 output-bin=face-down print-color-mode=color print-quality=4 sides=one-sided job-uuid=urn:uuid:90b1f210-f699-3666-74c6-e16b648b850e job-originating-host-name=localhost date-time-at-creation= date-time-at-processing= time-at-creation=1503848594 time-at-processing=1503848594 document-name-supplied=CityMap.pdf PageSize=A4"
```
How does this exactly work?
cups/ppd-cache.c's _cupsConvertOptions is responsible for mapping PPD cupsPrintQuality to IPP print-quality. Currently there is no code to map IPP print-quality to PPD cupsPrintQuality (but there should be - will add it...)

But in this case both "print-quality" AND "cupsPrintQuality" have been specified when submitting the job, so "print-quality" is used in the IPP request and "cupsPrintQuality" is used to set the resolution.

Now I have taken your latest GIT snapshot and used its libcups.so.2, with your changes for Issue #5091, overwriting my old experiments. Then I have created a queue with "-m everywhere" to exercise CUPS' PPD generator, checked the PPD whether it contains a "cupsPrintQuality" option and no "print-quality" option. Then I have sent a job with "-o cupsPrintQuality=High to this printer and still get:

```
D [27/Aug/2017:14:53:44 -0300] [Job 3038] job-sheets=none,none
D [27/Aug/2017:14:53:44 -0300] [Job 3038] argv[0]="cdl"
D [27/Aug/2017:14:53:44 -0300] [Job 3038] argv[1]="3038"
D [27/Aug/2017:14:53:44 -0300] [Job 3038] argv[2]="till"
D [27/Aug/2017:14:53:44 -0300] [Job 3038] argv[3]=".gitconfig"
D [27/Aug/2017:14:53:44 -0300] [Job 3038] argv[4]="1"
D [27/Aug/2017:14:53:44 -0300] [Job 3038] argv[5]="cupsPrintQuality=High finishings=3 media=iso_a4_210x297mm number-up=1 output-bin=face-down print-color-mode=color print-quality=4 sides=one-sided job-uuid=urn:uuid:cdccf78b-cd4d-3715-6d07-e95fed3da50b job-originating-host-name=localhost date-time-at-creation= date-time-at-processing= time-at-creation=1503856424 time-at-processing=1503856424 document-name-supplied=.gitconfig PageSize=A4"
```
I have also checked that I have no ~/.cups/lpoptions and /etc/cups/lpoptions files.
[master 40cc612af] Fix the interactions between the "print-quality" and "cupsPrintQuality" options (Issue #5090)

I have applied your changes and done the following:
```
lpadmin -p cdl -E -v ipp://HP5CB901E7DDC7.local:631/ipp/print -m everywhere -o PageSize=A4
lp -d cdl -o cupsPrintQuality=High ~/ghostscript/testfiles/CityMap.pdf
```
The printout came out quickly as with "Normal" quality. Then I checked /var/log/cups/error_log and got
```
D [29/Aug/2017:11:13:27 -0300] [Job 3040] job-sheets=none,none
D [29/Aug/2017:11:13:27 -0300] [Job 3040] argv[0]="cdl"
D [29/Aug/2017:11:13:27 -0300] [Job 3040] argv[1]="3040"
D [29/Aug/2017:11:13:27 -0300] [Job 3040] argv[2]="till"
D [29/Aug/2017:11:13:27 -0300] [Job 3040] argv[3]="CityMap.pdf"
D [29/Aug/2017:11:13:27 -0300] [Job 3040] argv[4]="1"
D [29/Aug/2017:11:13:27 -0300] [Job 3040] argv[5]="cupsPrintQuality=High finishings=3 media=iso_a4_210x297mm number-up=1 output-bin=face-down print-color-mode=color print-quality=4 sides=one-sided job-uuid=urn:uuid:8993b126-2426-32a2-73ff-276a49f59532 job-originating-host-name=localhost date-time-at-creation= date-time-at-processing= time-at-creation=1504016007 time-at-processing=1504016007 document-name-supplied=CityMap.pdf cupsPrintQuality=Normal PageSize=A4"
```
Here one can see that the supplied "cupsPrintQuality=High" but there is still the "print-quality=4" in the 5th command line argument for the filters, which I did not supply. This seems to make the quality staying on "Normal".
You'll notice that cupsPrintQuality appears twice in argv[5], so something is forcing print-quality-default to 4 (which should no longer be applied by default).

Check your printers.conf file for print-quality references - you might need to delete the queue and re-add to fix things...

Now I have removed and re-created the queue, checked whether there are really no lpoptions files and checked printers.conf again. Nowhere any hint that print quality is set to "Normal" (and even if so, printing with "-o cupsPrintQuality=High" should override it.
Printed again and again the same 5th command line argument as before with "cupsPrintQuality=High" in the beginning and "cupsPrintQuality=Normal" and "print-quality=4" in the end. Printout comes with the speed of normal quality.
Now I have done another test, setting "cupsPrintQuality=High" as PPD default:
```
lpadmin -p cdl2 -E -v ipp://HP5CB901E7DDC7.local:631/ipp/print -m everywhere -o PageSize=A4 -o cupsPrintQuality=High
lp -d cdl2 ~/.gitconfig
```
I checked the generated PPD /etc/cups/ppd/cdl2.ppd and it has actually set "*defaultcupsPrintQuality: High".
Looking into error_log one sees that CUPS still insists on "Normal" here:
```
D [29/Aug/2017:13:19:06 -0300] [Job 3042] job-sheets=none,none
D [29/Aug/2017:13:19:06 -0300] [Job 3042] argv[0]="cdl2"
D [29/Aug/2017:13:19:06 -0300] [Job 3042] argv[1]="3042"
D [29/Aug/2017:13:19:06 -0300] [Job 3042] argv[2]="till"
D [29/Aug/2017:13:19:06 -0300] [Job 3042] argv[3]=".gitconfig"
D [29/Aug/2017:13:19:06 -0300] [Job 3042] argv[4]="1"
D [29/Aug/2017:13:19:06 -0300] [Job 3042] argv[5]="finishings=3 media=iso_a4_210x297mm number-up=1 output-bin=face-down print-color-mode=color print-quality=4 sides=one-sided job-uuid=urn:uuid:7399d8b4-7109-3b36-6552-b4ca919d1f8b job-originating-host-name=localhost date-time-at-creation= date-time-at-processing= time-at-creation=1504023546 time-at-processing=1504023546 document-name-supplied=.gitconfig cupsPrintQuality=Normal PageSize=A4"
```
The printer uses Apple Raster for driverless printing and I have checked the PPD, and it actually has a "High" choice for the print quality.
Reopening while I dig into this more deeply...

Well, I just started from scratch and ran git master on Ubuntu and everything seems to be working as advertised when you specify the "print-quality" option, but not when you specify the PPD cupsPrintQuality option.


OK, I think I've cleaned up both the print-quality and cupsPrintQuality paths, and it seems to do the right thing with the printers I've tested with.
I have tested your patches now, once in pure CUPS built from source and once by backporting the two patches of this bug report into the Ubuntu package, and in both cases the cupsPrintQuality option works correctly now. Thank you very much.