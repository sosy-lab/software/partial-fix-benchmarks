diff --git a/.travis.yml b/.travis.yml
index 1da59f2d2..2a856ff36 100644
--- a/.travis.yml
+++ b/.travis.yml
@@ -33,4 +33,4 @@ script:
   - ${CI_BUILD_PREFIX} ./configure --disable-local-libopts > build.log 2>&1 || (cat build.log && exit 1)
   - ${CI_BUILD_PREFIX} make > build.log 2>&1 || (cat build.log && exit 1)
   - make dist > build.log 2>&1 || (cat build.log && exit 1)
-  - sudo make test
+  - sudo make test || (cat test/test.log && exit 1)
diff --git a/docs/CHANGELOG b/docs/CHANGELOG
index 23b08c78a..bf342829a 100644
--- a/docs/CHANGELOG
+++ b/docs/CHANGELOG
@@ -6,6 +6,7 @@
     - CVE-2018-17580 heap-buffer-overflow fast_edit_packet (#485)
     - CVE-2018-17582 heap-buffer-overflow in get_next_packet (#484)
     - Out-of-tree build (#482)
+    - Add ability to change tcp SEQ/ACK numbers (#425)
     - Fails to open tap0 on Zephyr (#411)
     - CVE-2018-13112 heap-buffer-overflow in get_l2len (#477 dup #408)
     - Respect 2nd packet timing (#418)
diff --git a/docs/CREDIT b/docs/CREDIT
index 7af65345f..5250c6f6b 100644
--- a/docs/CREDIT
+++ b/docs/CREDIT
@@ -79,3 +79,6 @@ Pedro Arthur Duarte [aka JEdi] <GitHub @pedroarthur>
 Gabriel Ganne <GitHub @GabrielGanne>
     - Data fuzz rewrite feature
     - Out-of-tree build
+
+Mario D Santana <GitHUB @@mariodsantana>
+    - TCP seq/ack edit
diff --git a/src/tcpedit/Makefile.am b/src/tcpedit/Makefile.am
index 15e9a767e..e2646725c 100644
--- a/src/tcpedit/Makefile.am
+++ b/src/tcpedit/Makefile.am
@@ -7,7 +7,7 @@ BUILT_SOURCES = tcpedit_stub.h
 
 libtcpedit_a_SOURCES = tcpedit.c parse_args.c edit_packet.c \
 	portmap.c dlt.c checksum.c incremental_checksum.c \
-	tcpedit_api.c fuzzing.c
+	tcpedit_api.c fuzzing.c rewrite_sequence.c
 
 manpages: tcpedit.1
 
@@ -28,7 +28,7 @@ noinst_HEADERS = tcpedit.h edit_packet.h portmap.h \
 	tcpedit_stub.h parse_args.h dlt.h checksum.h \
 	incremental_checksum.h tcpedit_api.h \
 	tcpedit_types.h plugins.h plugins_api.h \
-	plugins_types.h fuzzing.h
+	plugins_types.h fuzzing.h rewrite_sequence.h
 
 MOSTLYCLEANFILES = *~
 
diff --git a/src/tcpedit/parse_args.c b/src/tcpedit/parse_args.c
index c4672f2b4..84dfacfb5 100644
--- a/src/tcpedit/parse_args.c
+++ b/src/tcpedit/parse_args.c
@@ -167,6 +167,12 @@ tcpedit_post_args(tcpedit_t *tcpedit) {
         }
     }
 
+    /* --rewrite-sequence */
+    if (HAVE_OPT(REWRITE_SEQUENCE))
+	tcpedit->rewrite_sequence = 1;
+    else
+	tcpedit->rewrite_sequence = 0;
+
     /* TCP/UDP port rewriting */
     if (HAVE_OPT(PORTMAP)) {
         int ct = STACKCT_OPT(PORTMAP);
diff --git a/src/tcpedit/rewrite_sequence.c b/src/tcpedit/rewrite_sequence.c
new file mode 100644
index 000000000..a3d0dab15
--- /dev/null
+++ b/src/tcpedit/rewrite_sequence.c
@@ -0,0 +1,86 @@
+/* $Id$ */
+
+/*
+ *   Copyright (c) 2001-2010 Aaron Turner <aturner at synfin dot net>
+ *   Copyright (c) 2013-2017 Fred Klassen <tcpreplay at appneta dot com> - AppNeta
+ *   Copyright (c) 2017 Mario D. Santana <tcpreplay at elorangutan dot com> - El Orangutan
+ *
+ *   The Tcpreplay Suite of tools is free software: you can redistribute it 
+ *   and/or modify it under the terms of the GNU General Public License as 
+ *   published by the Free Software Foundation, either version 3 of the 
+ *   License, or with the authors permission any later version.
+ *
+ *   The Tcpreplay Suite is distributed in the hope that it will be useful,
+ *   but WITHOUT ANY WARRANTY; without even the implied warranty of
+ *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ *   GNU General Public License for more details.
+ *
+ *   You should have received a copy of the GNU General Public License
+ *   along with the Tcpreplay Suite.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+/*
+ * This file contains routines to manipulate port maps, in which
+ * one port number is mapped to another.
+ */
+#include "config.h"
+#include "defines.h"
+#include "common.h"
+
+#include <stdlib.h>
+#include <string.h>
+#include <errno.h>
+
+#include "tcpreplay.h"
+#include "tcpedit.h"
+#include "rewrite_sequence.h"
+#include "incremental_checksum.h"
+
+
+/**
+ * rewrites the TCP sequence and ack numbers
+ * returns 1 for changes made or 0 for none
+ */
+
+static int
+rewrite_seqs(tcpedit_t *tcpedit, tcp_hdr_t *tcp_hdr)
+{
+    uint32_t newnum;
+
+    while (tcpedit->rewrite_sequence == 1) 
+        tcpedit->rewrite_sequence = rand() * (4294967296 / RAND_MAX);
+    newnum = tcp_hdr->th_seq + tcpedit->rewrite_sequence;
+    csum_replace4(&tcp_hdr->th_sum, tcp_hdr->th_seq, newnum);
+    tcp_hdr->th_seq = newnum;
+    newnum = tcp_hdr->th_ack + tcpedit->rewrite_sequence;
+    csum_replace4(&tcp_hdr->th_sum, tcp_hdr->th_ack, newnum);
+    tcp_hdr->th_ack = newnum;
+    return 0;
+}
+
+
+int
+rewrite_ipv4_sequence(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr)
+{
+    assert(tcpedit);
+    tcp_hdr_t *tcp_hdr = NULL;
+
+    if (*ip_hdr && (*ip_hdr)->ip_p == IPPROTO_TCP) {
+        tcp_hdr = (tcp_hdr_t *)get_layer4_v4(*ip_hdr, 65536);
+        return rewrite_seqs(tcpedit, tcp_hdr);
+    }
+    return 0;
+}
+
+int
+rewrite_ipv6_sequence(tcpedit_t *tcpedit, ipv6_hdr_t **ip6_hdr)
+{
+    assert(tcpedit);
+    tcp_hdr_t *tcp_hdr = NULL;
+
+    if (*ip6_hdr && (*ip6_hdr)->ip_nh == IPPROTO_TCP) {
+        tcp_hdr = (tcp_hdr_t *)get_layer4_v6(*ip6_hdr, 65535);
+        return rewrite_seqs(tcpedit, tcp_hdr);
+    }
+    return 0;
+}
diff --git a/src/tcpedit/rewrite_sequence.h b/src/tcpedit/rewrite_sequence.h
new file mode 100644
index 000000000..ac52532dc
--- /dev/null
+++ b/src/tcpedit/rewrite_sequence.h
@@ -0,0 +1,33 @@
+/* $Id$ */
+
+/*
+ *   Copyright (c) 2001-2010 Aaron Turner <aturner at synfin dot net>
+ *   Copyright (c) 2013-2017 Fred Klassen <tcpreplay at appneta dot com> - AppNeta
+ *   Copyright (c) 2017 Mario D. Santana <tcpreplay at elorangutan dot com> - El Orangutan
+ *
+ *   The Tcpreplay Suite of tools is free software: you can redistribute it 
+ *   and/or modify it under the terms of the GNU General Public License as 
+ *   published by the Free Software Foundation, either version 3 of the 
+ *   License, or with the authors permission any later version.
+ *
+ *   The Tcpreplay Suite is distributed in the hope that it will be useful,
+ *   but WITHOUT ANY WARRANTY; without even the implied warranty of
+ *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
+ *   GNU General Public License for more details.
+ *
+ *   You should have received a copy of the GNU General Public License
+ *   along with the Tcpreplay Suite.  If not, see <http://www.gnu.org/licenses/>.
+ */
+
+#include "tcpedit_types.h"
+
+#ifndef __REWRITE_SEQUENCE_H__
+#define __REWRITE_SEQUENCE_H__
+
+int rewrite_ipv4_sequence(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr);
+int rewrite_ipv6_sequence(tcpedit_t *tcpedit, ipv6_hdr_t **ip_hdr);
+
+int rewrite_ipv4_sequence(tcpedit_t *tcpedit, ipv4_hdr_t **ip_hdr);
+int rewrite_ipv6_sequence(tcpedit_t *tcpedit, ipv6_hdr_t **ip6_hdr);
+
+#endif
diff --git a/src/tcpedit/tcpedit.c b/src/tcpedit/tcpedit.c
index 0c6e65152..4c393975d 100644
--- a/src/tcpedit/tcpedit.c
+++ b/src/tcpedit/tcpedit.c
@@ -37,7 +37,7 @@
 #include "edit_packet.h"
 #include "parse_args.h"
 #include "fuzzing.h"
-
+#include "rewrite_sequence.h"
 
 #include "lib/sll.h"
 #include "dlt.h"
@@ -221,7 +221,11 @@ tcpedit_packet(tcpedit_t *tcpedit, struct pcap_pkthdr **pkthdr,
             if ((retval = rewrite_ipv4_ports(tcpedit, &ip_hdr, (*pkthdr)->caplen)) < 0)
                 return TCPEDIT_ERROR;
         }
+
+        if (tcpedit->rewrite_sequence)
+            rewrite_ipv4_sequence(tcpedit, &ip_hdr);
     }
+
     /* IPv6 edits */
     else if (ip6_hdr != NULL) {
         /* rewrite the hop limit */
@@ -258,6 +262,9 @@ tcpedit_packet(tcpedit_t *tcpedit, struct pcap_pkthdr **pkthdr,
             if ((retval = rewrite_ipv6_ports(tcpedit, &ip6_hdr, (*pkthdr)->caplen)) < 0)
                 return TCPEDIT_ERROR;
         }
+
+        if (tcpedit->rewrite_sequence)
+            rewrite_ipv6_sequence(tcpedit, &ip6_hdr);
     }
 
     if (tcpedit->fuzz_seed != 0) {
diff --git a/src/tcpedit/tcpedit_opts.def b/src/tcpedit/tcpedit_opts.def
index a01347c5e..9f3d970cd 100644
--- a/src/tcpedit/tcpedit_opts.def
+++ b/src/tcpedit/tcpedit_opts.def
@@ -136,6 +136,16 @@ IPv6 Example:
 EOText;
 };
 
+flag = {
+    name        = rewrite-sequence;
+    value       = n;
+    descrip     = "Change TCP sequence (and ACK) numbers";
+    doc         = <<- EOText
+Change all TCP sequence numbers, and related sequence-acknowledgement numbers.
+They will be shifted by a random amount between 1 and 2^32.
+EOText;
+};
+
 flag = {
     name        = skipbroadcast;
     value       = b;
diff --git a/src/tcpedit/tcpedit_types.h b/src/tcpedit/tcpedit_types.h
index 267172e52..635706249 100644
--- a/src/tcpedit/tcpedit_types.h
+++ b/src/tcpedit/tcpedit_types.h
@@ -119,6 +119,9 @@ typedef struct {
 
     /* rewrite ip? */
     bool rewrite_ip;
+
+    /* rewrite seq/ack numbers? */
+    u_int32_t rewrite_sequence;
     
     /* fix IP/TCP/UDP checksums */
     bool fixcsum;
diff --git a/test/Makefile.am b/test/Makefile.am
index 7f8a3f62e..943366a9a 100644
--- a/test/Makefile.am
+++ b/test/Makefile.am
@@ -28,7 +28,7 @@ EXTRA_DIST = test.pcap test.auto_bridge test.auto_client test.auto_router \
 		test.rewrite_vlandel test.rewrite_efcs test.rewrite_1ttl \
 		test.rewrite_2ttl test.rewrite_3ttl test.rewrite_enet_subsmac \
 		test.rewrite_mtutrunc test.rewrite_mac_seed test.rewrite_range_portmap \
-		test.rewrite_mac_seed_keep test.rewrite_l7fuzzing \
+		test.rewrite_mac_seed_keep test.rewrite_l7fuzzing test.rewrite_sequence \
 		test2.rewrite_seed test2.rewrite_portmap test2.rewrite_endpoint \
 		test2.rewrite_pnat test2.rewrite_pad test2.rewrite_trunc \
 		test2.rewrite_mac test2.rewrite_layer2 test2.rewrite_config \
@@ -38,7 +38,7 @@ EXTRA_DIST = test.pcap test.auto_bridge test.auto_client test.auto_router \
 		test2.rewrite_2ttl test2.rewrite_3ttl test.rewrite_tos test2.rewrite_tos \
 		test2.rewrite_enet_subsmac test2.rewrite_mac_seed \
 		test2.rewrite_range_portmap test2.rewrite_mac_seed_keep \
-		test2.rewrite_l7fuzzing
+		test2.rewrite_l7fuzzing test2.rewrite_sequence
 
 test: all
 all: clearlog check tcpprep tcpreplay tcprewrite
@@ -62,7 +62,7 @@ standard: standard_prep $(STANDARD_REWRITE)
 	$(PRINTF) "Warning: only creating %s endian standard test files\n" $(REWRITE_WARN)
 
 standard_prep:
-	$(TCPPREP) -i $(TEST_PCAP) -o test.auto_router -a router 
+	$(TCPPREP) -i $(TEST_PCAP) -o test.auto_router -a router
 	$(TCPPREP) -i $(TEST_PCAP) -o test.auto_bridge -a bridge
 	$(TCPPREP) -i $(TEST_PCAP) -o test.auto_client -a client
 	$(TCPPREP) -i $(TEST_PCAP) -o test.auto_server -a server
@@ -86,6 +86,7 @@ standard_bigendian:
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test.rewrite_tos --tos=50
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test.rewrite_portmap -r 80:8080
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test.rewrite_range_portmap -r 1-100:49148
+	$(TCPREWRITE) -i $(TEST_PCAP) -o test.rewrite_sequence -n
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test.rewrite_endpoint \
 		-e 10.10.0.1:10.10.0.2 -c test.auto_router
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test.rewrite_pnat \
@@ -126,7 +127,8 @@ standard_littleendian:
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_seed -s 55
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_tos --tos=50
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_portmap -r 80:8080
-	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_range_portmap -r 1-100:49148	
+	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_range_portmap -r 1-100:49148
+	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_sequence -n
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_endpoint \
 		-e 10.10.0.1:10.10.0.2 -c test.auto_router
 	$(TCPREWRITE) -i $(TEST_PCAP) -o test2.rewrite_pnat \
@@ -173,7 +175,7 @@ tcprewrite: rewrite_portmap rewrite_range_portmap rewrite_endpoint \
 	rewrite_layer2 rewrite_config rewrite_skip rewrite_dltuser rewrite_dlthdlc \
 	rewrite_vlandel rewrite_efcs rewrite_1ttl rewrite_2ttl rewrite_3ttl \
 	rewrite_tos rewrite_mtutrunc rewrite_enet_subsmac rewrite_mac_seed \
-	rewrite_mac_seed_keep rewrite_l7fuzzing
+	rewrite_mac_seed_keep rewrite_l7fuzzing rewrite_sequence
 
 tcpreplay: replay_basic replay_cache replay_pps replay_rate replay_top \
 	replay_config replay_multi replay_pps_multi replay_precache \
@@ -355,14 +357,25 @@ endif
 rewrite_range_portmap:
 	$(PRINTF) "%s" "[tcprewrite] Portmap range test: "
 	$(PRINTF) "%s\n" "*** [tcprewrite] Portmap range test: " >> test.log
-	$(TCPREWRITE) $(ENABLE_DEBUG) -i test.pcap -o test.$@1 -r 1-100:49148 >> test.log 2>&1
+	$(TCPREWRITE) $(ENABLE_DEBUG) -i $(TEST_PCAP) -o test.$@1 -r 1-100:49148 >> test.log 2>&1
 if WORDS_BIGENDIAN
-	diff test.$@ test.$@1 >> test.log 2>&1
+	diff $(srcdir)/test.$@ test.$@1 >> test.log 2>&1
 else
-	diff test2.$@ test.$@1 >> test.log 2>&1
+	diff $(srcdir)/test2.$@ test.$@1 >> test.log 2>&1
 endif
 	if [ $? ] ; then $(PRINTF) "\t\t\t%s\n" "FAILED"; else $(PRINTF) "\t\t%s\n" "OK"; fi
 
+rewrite_sequence:
+	$(PRINTF) "%s" "[tcprewrite] Rewrite sequence test: "
+	$(PRINTF) "%s\n" "*** [tcprewrite] Resequence test: " >> test.log
+	$(TCPREWRITE) $(ENABLE_DEBUG) -i $(TEST_PCAP) -o test.$@1 -n >> test.log 2>&1
+if WORDS_BIGENDIAN
+	diff $(srcdir)/test.$@ test.$@1 >> test.log 2>&1
+else
+	diff $(srcdir)/test2.$@ test.$@1 >> test.log 2>&1
+endif
+	if [ $? ] ; then $(PRINTF) "\t\t\t%s\n" "FAILED"; else $(PRINTF) "\t\t\t%s\n" "OK"; fi
+
 rewrite_endpoint:
 	$(PRINTF) "%s" "[tcprewrite] Endpoint test: "
 	$(PRINTF) "%s\n" "*** [tcprewrite] Endpoint test: " >> test.log
diff --git a/test/test.rewrite_sequence b/test/test.rewrite_sequence
new file mode 100644
index 000000000..6d98dd6de
Binary files /dev/null and b/test/test.rewrite_sequence differ
diff --git a/test/test2.rewrite_sequence b/test/test2.rewrite_sequence
new file mode 100644
index 000000000..6a4688e04
Binary files /dev/null and b/test/test2.rewrite_sequence differ
