Scrub ruler and play indicator not displaying correctly
This is a regression on 3.1.3
First screen shot, on macOS, with the scrub ruler off.  Observe that the green line does not extend to the top of the white area.

<img width="270" alt="Screen Shot 2022-06-06 at 9 46 37 AM" src="https://user-images.githubusercontent.com/11670369/172173789-473082ca-5dd7-4857-b53f-6b78bf04058c.png">

A second screen shot on macOS, with the toggle down for the scrub ruler.  There is a subtle one or two pixel change in the height of the ruler only.  I can't position my mouse anywhere for the hit test result that changes the play head to the green double-headed scrubbing arrow.  I can do scrubbing if I use the toolbar buttons.

<img width="200" alt="Screen Shot 2022-06-06 at 9 47 06 AM" src="https://user-images.githubusercontent.com/11670369/172174006-5c56c506-8fce-4b8b-9e58-67ecf326e5ff.png">

Compare with how it appears when I build commit ec5f272803ab4c524256569a37e6d8fb7c476187, one before the blamed commit.  As it also appears in 3.1.3.

<img width="270" alt="Screen Shot 2022-06-06 at 10 04 08 AM" src="https://user-images.githubusercontent.com/11670369/172176763-2d85ae94-2224-4cb0-8bf8-1aa1803b8537.png">
<img width="362" alt="Screen Shot 2022-06-06 at 10 03 55 AM" src="https://user-images.githubusercontent.com/11670369/172176759-c9d9cf44-350a-474c-a7db-60d4fd015c5b.png">


> First screen shot, on macOS, with the scrub ruler off. Observe that the green line does not extend to the top of the white area.

@Paul-Licameli  - I reported similar in a different thread some days ago.  And similar happens when recording.   Those are regressions on 3.1.3 and earlier.

_I'll try to find that report - but it may have been on Discord_


I discovered the problem incidentally when rewriting our uses of threads.
The shortened green and red cursor lines that do not reach up to to the Timeline ahould probably be listed as a new bu., do you want me to do that?

Recording looks worse than playback as the red line is ahead of the waveform being drawn.
> The shortened green and red cursor lines that do not reach up to to the Timeline ahould probably be listed as a new bug., do you want me to do that?
> 
> Recording looks worse than playback as the red line is ahead of the waveform being drawn.

I unite it all in one issue because the same commit caused both and that may likely be fixed with a common commit.  There is some problem with 2d screen geometry and maybe the refreshing of windows.

OK - single thread it is.

Here is an example of recording with the latest 3.2.0 alpha:  audacity-win-3.2.0-alpha-20220606+1cc0df3-x64-msvc2022

![image](https://user-images.githubusercontent.com/50205984/172197050-916aaa55-bb3d-4eb0-a9e1-f35f0b166f7b.png)

Now that gap in the horizontal direction might be a distinct problem meriting a different issue, if a bisection determines that this regression happened at a different place in commit history.

@Paul-Licameli   I was right -  I had already logged the gap problem:

 **Recording and Playback cursor in the track no longer span the Clip-handle space** #2870 
@Paul-Licameli . I didn't ask to be assigned to this issue! I'm going to unassign myself.
> @Paul-Licameli . I didn't ask to be assigned to this issue! I'm going to unassign myself.

The assignment was only a convenience for maintaining our projects board, and seeing easily who worked on what by the associated icons.  It implied no demand on my part that you do anything more.  But if you object I won't do this again.

Thank you for the fix!



Because I expect your #2913 to be a fix for this issue also.

> Because I expect your #2913 to be a fix for this issue also.

Alas, it won't be. My fix only changes the location of a track which Audacity provides  to any assistive technology. It fixes an existing bug which has only been shown up by the reparenting of the timeline.
My guess from screen shots was wrong then.  But it looked like the same error in y coordinates.

But having has a quick look, there might be very similar problems causing problems with playback cursor.
In void PlayIndicatorOverlayBase::Draw(OverlayPanel &panel, wxDC &dc), the line:
AColor::Line(dc, mLastIndicatorX, tp->GetRect().GetTop(), mLastIndicatorX, tp->GetRect().GetBottom());

looks like it's wrong, but used to accidentally work before the reparenting. (No testing done to check this!)

I'll take a look
> looks like it's wrong, but used to accidentally work before the reparenting. (No testing done to check this!)

It is and has a simple fix:
https://github.com/audacity/audacity/pull/3159
Testing on w10 with latest master:  audacity-win-3.2.0-alpha-20220704+679d6b6-x64-msvc2022

I confirm that the "gap" is now gone  (looks much better) - behavior is now as it was in 3.1.3 *and earlier)
Verified on MacOS and Windows 11