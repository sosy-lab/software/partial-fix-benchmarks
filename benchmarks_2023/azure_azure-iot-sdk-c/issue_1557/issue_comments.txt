Memory leak in IoTHubDeviceClient_SendReportedState function
Hi @alexmrtn ,
so have automated tests for verifying leaks in device twin operations, and we also modified a sample to try sending reported properties while not connected, and destroying at the end.
On our verification, we let the sample run for a few seconds (successfully sending reports) then disconnected the internet.
There were no memory leaks on that scenario.

**The bug you are reporting is valid**, but it is on a specific scenario where the device client gets destroyed immediately after invoking IoTHubDeviceClient_SendReportedState, which is not a common case. 

We are working to provide a fix though.

Thanks for reporting the issue.
Azure IoT Team.
Hi @alexmrtn ,
the fix for this issue is now checked into master.
Could you please validate it as well?
Thanks,
Azure IoT Team.
Hi @ewertons ,

Thanks for this very quick fix! It is valid and it resolved the leak.
Hi Again,

Reopening as I think something is missing: In my sample the corresponding callback `twinReported` is never called if the client gets destroyed immediately after invoking IoTHubDeviceClient_SendReportedState.

The memory leak from SDK is not happening anymore though. But if one rely on the callback to free memory he had passed through the context callback (same as your implementation in the SDK) This code is never called and memory leak can happen in the user code.

I think all is properly implemented for the LL API, I check your UT that [asserts cbk is executed](https://github.com/Azure/azure-iot-sdk-c/blob/354cd4297897008f203132f78ff573d31739a5a2/iothub_client/tests/iothubclientcore_ll_ut/iothub_client_core_ll_ut.c#L4417) but something seems missing for the convenience API.
This has now been fixed. Thanks for your contribution.
If you would like to follow up about this specific issue (Memory Leak) please reopen this ticker, otherwise feel free to open a new one if you have any other concerns.
Thanks,
Azure IoT Client Team