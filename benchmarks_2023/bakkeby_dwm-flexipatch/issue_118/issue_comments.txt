Can't disable dwmbar & run polybar like it should.
So how do you reproduce this issue? What does your configuration file look like?
thx for the reply.
In flexipatch all I added was anybar & coolautostart, made sure usealtbar is set to 1 and startet dwm with my run script, which starts my polybars. I doubt you want me to show you how flexipatch looks with only those two patches and one edited line in config.h? My own config is basically the same, only with some more patches. showbar, topbar & usealtbar are set to 1 in both configs. And in flexipatch the bars are even on top and on bottom on top of that. What exactly can I show you?
So the reproduce I'd say. Apply anybar & autostart, set showbar, topbar & altbar to 1 & start dwm with a autostart script in which you call polybar.
My polybarscript:
```
killall -q polybar &
while pgrep -u $UID -x polybar >/dev/null; do sleep 0.2; done
polybar -c ~/.config/polybar/gamebar/gamebar bgtop -r &
polybar -c ~/.config/polybar/gamebar/gamebar bgbottom -r &
sleep 0.2
polybar -c ~/.config/polybar/gamebar/gamebar fronttop -r &
polybar -c ~/.config/polybar/gamebar/gamebar frontbottom -r &
```
I also checked the class to get that outa the way and suprise the class is indeed "Polybar".
> and one edited line in config.h

That's most likely your problem. It's been a while since I tried this, but I think you need to disable these patches that are enabled by default.

```c
#define BAR_LTSYMBOL_PATCH 1
#define BAR_STATUS_PATCH 1
#define BAR_TAGS_PATCH 1
#define BAR_WINTITLE_PATCH 1
#define BAR_TITLE_LEFT_PAD_PATCH 1
```

and add a 0 to the barrules variable in the config file just to make it compile.

```c
static const BarRule barrules[] = { 0 };
```

ref.

https://github.com/bakkeby/dwm-flexipatch/blob/23c76d13b5376e78c268006da57d132883d5df9b/patches.def.h#L225-L226
still the same. If I compile my built and restart on point it works like a charm..
Only stuff set to one is
```
#define BAR_ANYBAR_PATCH 1
#define COOL_AUTOSTART_PATCH 1
#define RESTARTSIG_PATCH 1
#define TILE_LAYOUT 1
#define MONOCLE_LAYOUT 1
```
Haven't seen the dwm bar this time but still polybar freaks out somehow..
Do you have `override-redirect = false` in your polybar config?
yes. I'm using this fork https://github.com/mihirlad55/polybar-dwm-module and set it up correctly as one can see in my first screenshot. I would guess there is an issue with your patch as it should accept `override-redirect = false`.
I had a look and here are my findings.

A bare 6.2 dwm has support for a single bar. Applying the [IPC](https://dwm.suckless.org/patches/ipc/) patch and the [anybar](https://dwm.suckless.org/patches/anybar/) patch does not change this. What happens when you use an alternative bar is that the main bar window is destroyed and the external bar (polybar) takes it's place as the monitor's bar. That the bars stack the way they do seems to be merely a side effect of how the anybar patch handles the position of the new bar.

I haven't tried, but I suspect that if you were to kill just one of them (instead of all in one go) then you'd get some weird behaviour like like a blank space where the bar was or potentially bars overlapping or them climbing down the screen.

dwm-flexipatch currently has built-in support for up to two bars per monitor via barmodules. That is similar to the [extrabar](https://dwm.suckless.org/patches/extrabar/) and [dualstatus](https://dwm.suckless.org/patches/dualstatus/) patches. It will switch between top bar and bottom bar depending on the bar number, and as the main bar is typically the first an extra bar would be second (hence you may find that the topbar setting in config.h is reversed). This is also the reason why the bars don't stack as said in your first post. As the bar handling is so different in dwm-flexipatch the support for additional bars is experimental.

I figured out why there would be leftover gaps when you kill polybar. It had partially to do with running more than one external bar, but primarily it was due to some missing cleanup when a bar is removed. I added a fix for this.

If you just run one external bar (and no dwm bar) then if you had topbar = 1 in your config then the external bar would end up at the bottom, and vice versa. Technically this had to do with that the dwm bar actually still exists, it just doesn't show. I worked out that you can actually avoid this bar from being created if you set barrules to be like this:

```c
static const BarRule barrules[] = { { -2 } };
```

The number is for the monitor and I use -2 here because -1 means "all monitors" and 0 means the first monitor. I found there were some edge cases if you run dwm without a bar (some parts naturally assumed that there would be a bar) so I added a couple of fixes for that. There could still be some edge cases that I did not catch though, just keep that in mind.

Finally, I added an extra option for you that should allow you to stack the bars at the top or bottom.

```c
/* Anybar option to stack multiple external bars at the top or at the bottom of the monitor
 * instead of adding a second bar at the opposite side.
 */
#define BAR_ANYBAR_STACK_BARS_PATCH 0
```

I think that's it, let me know if there are any issues.
Now they are on top but they do not stack. they load one after another always below the last one.
it's 4 bars. 2 background, 2 foreground. backgrounds supposed to be underneath foreground obviously. Maybe this wasn't clear. Bars should be able to overlap. That's what I meant with stacking, sry. If u have a look at my first picture u see 2 bars but it's actually 4.
I wish I could help u a bit here but I have zero C knowledge.. =( Thanks so far though, really appreciated. You put a lot of work into this project.
Ah right, yes that's different.

All in all it is not _supposed_ to work like this :)

As outlined before dwm 6.2 (and the anybar patch) is only intended to support a single external bar (in place of the bar that comes with dwm).

The reason why it _seems_ to work with fresh install of just IPC + the anybar patch is because when the bar is first registered with dwm it **keeps** the position that it defined in the polybar config (`offset-x`, `offset-y`).

But what happens if you do `MOD+b` twice to hide the bar and show it again? The bars won't be positioned correctly.

The whole point of the [anybar](https://dwm.suckless.org/patches/anybar/) patch is to enable dwm to _manage_ the external bar as if it were it's own so that commands like `togglebar` work the same way - that obviously means that dwm also controls the position of the bar. And again dwm ultimately only controls the last polybar bar that was created as it only supports one bar.

With the changes to dwm-flexipatch you can now have multiple external bars, and they will disappear and re-appear in the same places when you toggle the bar with `MOD+b` as the window manager controls the position of the position of the bars. In principle if the screen size changes then the bar size should change as well, so I added an option to allow dwm to manage the width of the bar. I presume that not all external bars will handle this well.

```c
/* Anybar option to let dwm manage the width of the bar */
#define BAR_ANYBAR_MANAGE_WIDTH_PATCH 0
```

To avoid confusion I decided to avoid the term "stack" for the option regarding the bar position, so I ended up having it respect the topbar / bottom bar config by default and added a separate option to allow the previous behaviour:

```c
/* Anybar option to place the next bar depending on previous bar's position (top or bottom) */
#define BAR_ANYBAR_TOP_AND_BOTTOM_BARS_PATCH 0
```

---

OK, now what you are doing is something rather extraordinary in that you are running four bars and on top of that you have one overlapping the other with transparency.

I believe that you are looking at the wrong patch for dwm, because as said earlier the point of the [anybar](https://dwm.suckless.org/patches/anybar/) patch is that dwm controls the external bar as if it were it's own. In this case you do not actually want dwm to control the bar - you want to have absolute control the size and position yourself through the polybar configuration file.

I think the [insets](https://dwm.suckless.org/patches/insets/) patch makes more sense in this case. It simply reserves an area of space on the monitor to give room for the external bar(s). The only real difference would be that you can't use `MOD+b` to toggle visibility of the bar(s), but it sounds like you don't actually use this feature much anyway. 

with the insets patch applied, the bars spawn below the inset space. Guess I'll have to stick to my own brew :D

> the reason why it seems to work with fresh install of just IPC + the anybar patch

I have like 15 patches or so applied and it works. =)
If you use the insets patch then you need to disable the anybar patch and probably you need to set `override-redirect = true`.
Oh I though these would work together.. well I rather stick to my 4 bars right now. Thanks very much for trying. Sad that this doesn't seem to work.
So it's not supposed to do it.. but If you can make this work on flexipatch like it works on my build I donate you 10EUR. :D
I tried myself the whole evening and I'm slowly getting insane. I'd really like to switch to flexipatch to not have to watch over my own stuff.
I tried a setup similar to yours where:
   - you have a bare 6.2 dwm and
   - the IPC patch and
   - the anybar patch and
   - in the polybar configuration you define the position of each of the bars and
   - override-redirect is set to **false** and
   - some of the bars overlap
 
When you start the bars they appear at the top, overlapping, as in your previous screenshots. When you hit `MOD+b` to hide the bar it doesn't hide all the bars and when you hit `MOD+b` again to show the bars not all of them are back in the right position.

Does `MOD+b` actually work in your setup?

If it doesn't, then why would you want the anybar patch in the first place? That dwm handles the size and position of the bar as well as being able to toggle the display of the bar on and off is really the only reason for using this patch.

If you use rounded corners in polybar then the bar will also have a hard edge on the right hand side when dwm resizes the bar.

If you want to have control over where the bars are positioned then you can use the insets patch.

Similar setup using the insets patch:
   - you have a bare 6.2 dwm and
   - the IPC patch and
   - the insets patch and
   - in the polybar configuration you define the position of each of the bars and
   - override-redirect is set to **true** and
   - some of the bars overlap

The insets patch makes room for bar(s) at the top and the bars are placed according to the polybar configuration. `MOD+b` has no effect, at the same time it doesn't screw anything up.


Ok now I seem to understand better. I'm a bit dumb oftentimes..
You already figured out I don't use mod+b =)

Well I though I need anybar so dwm handles an external bar, that it only hides the dwm bar with anybar patch.

So.. now I applied insets, ipc, autostart, restartsig, disabled all bar related stuff, set barrules to -2 aaaand... dwm crashes on me.
If I do this on my build, it works.
If I start flexipatch, my bars show up and dwm crashes. If I load into my setup it works no problem.
while compiling it also gives me warning
config.h:455:35: warning: missing braces around initializer [-Wmissing-braces]
static const BarRule barrules[] = { -2 };
                                  pointing to first brace
But I think I always got this warning before when used -2
Yes you would have to have:
```c
static const BarRule barrules[] = { { -2 } };
```

I'll probably come up with a better way of disabling the internal bar at some point.
My bad I forgot the second bracelets. It's still crashing on me tho.. I only have those 4 patches (+ the 2 default layout ones) applied..
I can see my panels for half a second, they seem to be in the right place, overlapping, and then it crashes on me. I sadly don't know how to troubleshoot.
What if you don't launch the panels via the autostart script? Does it crash then?
For reference here is the changes I have made to reproduce this setup. I don't have anything in the autostart script though.


```diff
$ diff config.def.h config.h
437c437
< 	.y = 30,
---
> 	.y = 100,
455,521c455
< static const BarRule barrules[] = {
< 	/* monitor  bar    alignment         widthfunc                drawfunc                clickfunc                name */
< 	#if BAR_STATUSBUTTON_PATCH
< 	{ -1,       0,     BAR_ALIGN_LEFT,   width_stbutton,          draw_stbutton,          click_stbutton,          "statusbutton" },
< 	#endif // BAR_STATUSBUTTON_PATCH
< 	#if BAR_POWERLINE_TAGS_PATCH
< 	{  0,       0,     BAR_ALIGN_LEFT,   width_pwrl_tags,         draw_pwrl_tags,         click_pwrl_tags,         "powerline_tags" },
< 	#endif // BAR_POWERLINE_TAGS_PATCH
< 	#if BAR_TAGS_PATCH
< 	{ -1,       0,     BAR_ALIGN_LEFT,   width_tags,              draw_tags,              click_tags,              "tags" },
< 	#endif // BAR_TAGS_PATCH
< 	#if BAR_TAGGRID_PATCH
< 	{ -1,       0,     BAR_ALIGN_LEFT,   width_taggrid,           draw_taggrid,           click_taggrid,           "taggrid" },
< 	#endif // BAR_TAGGRID_PATCH
< 	#if BAR_SYSTRAY_PATCH
< 	{  0,       0,     BAR_ALIGN_RIGHT,  width_systray,           draw_systray,           click_systray,           "systray" },
< 	#endif // BAR_SYSTRAY_PATCH
< 	#if BAR_LTSYMBOL_PATCH
< 	{ -1,       0,     BAR_ALIGN_LEFT,   width_ltsymbol,          draw_ltsymbol,          click_ltsymbol,          "layout" },
< 	#endif // BAR_LTSYMBOL_PATCH
< 	#if BAR_STATUS2D_PATCH && BAR_STATUSCMD_PATCH
< 	{ 'A',      0,     BAR_ALIGN_RIGHT,  width_status2d,          draw_status2d,          click_statuscmd,         "status2d" },
< 	#elif BAR_STATUS2D_PATCH
< 	{ 'A',      0,     BAR_ALIGN_RIGHT,  width_status2d,          draw_status2d,          click_status2d,          "status2d" },
< 	#elif BAR_POWERLINE_STATUS_PATCH
< 	{  0,       0,     BAR_ALIGN_RIGHT,  width_pwrl_status,       draw_pwrl_status,       click_pwrl_status,       "powerline_status" },
< 	#elif BAR_STATUS_PATCH && BAR_STATUSCMD_PATCH
< 	{  0,       0,     BAR_ALIGN_RIGHT,  width_status,            draw_status,            click_statuscmd,         "status" },
< 	#elif BAR_STATUS_PATCH
< 	{ 'A',      0,     BAR_ALIGN_RIGHT,  width_status,            draw_status,            click_status,            "status" },
< 	#endif // BAR_STATUS2D_PATCH | BAR_STATUSCMD_PATCH
< 	#if XKB_PATCH
< 	{  0,       0,     BAR_ALIGN_RIGHT,  width_xkb,               draw_xkb,               click_xkb,               "xkb" },
< 	#endif // XKB_PATCH
< 	#if BAR_FLEXWINTITLE_PATCH
< 	{ -1,       0,     BAR_ALIGN_NONE,   width_flexwintitle,      draw_flexwintitle,      click_flexwintitle,      "flexwintitle" },
< 	#elif BAR_TABGROUPS_PATCH
< 	{ -1,       0,     BAR_ALIGN_NONE,   width_bartabgroups,      draw_bartabgroups,      click_bartabgroups,      "bartabgroups" },
< 	#elif BAR_AWESOMEBAR_PATCH
< 	{ -1,       0,     BAR_ALIGN_NONE,   width_awesomebar,        draw_awesomebar,        click_awesomebar,        "awesomebar" },
< 	#elif BAR_FANCYBAR_PATCH
< 	{ -1,       0,     BAR_ALIGN_NONE,   width_fancybar,          draw_fancybar,          click_fancybar,          "fancybar" },
< 	#elif BAR_WINTITLE_PATCH
< 	{ -1,       0,     BAR_ALIGN_NONE,   width_wintitle,          draw_wintitle,          click_wintitle,          "wintitle" },
< 	#endif // BAR_TABGROUPS_PATCH | BAR_AWESOMEBAR_PATCH | BAR_FANCYBAR_PATCH | BAR_WINTITLE_PATCH
< 	#if BAR_EXTRASTATUS_PATCH
< 	#if BAR_STATUS2D_PATCH && BAR_STATUSCMD_PATCH
< 	{ 'A',      1,     BAR_ALIGN_CENTER, width_status2d_es,       draw_status2d_es,       click_statuscmd_es,      "status2d_es" },
< 	#elif BAR_STATUS2D_PATCH
< 	{ 'A',      1,     BAR_ALIGN_CENTER, width_status2d_es,       draw_status2d_es,       click_status2d,          "status2d_es" },
< 	#elif BAR_POWERLINE_STATUS_PATCH
< 	{  0,       1,     BAR_ALIGN_RIGHT,  width_pwrl_status_es,    draw_pwrl_status_es,    click_pwrl_status,       "powerline_status" },
< 	#elif BAR_STATUSCMD_PATCH && BAR_STATUS_PATCH
< 	{ 'A',      1,     BAR_ALIGN_CENTER, width_status_es,         draw_status_es,         click_statuscmd_es,      "status_es" },
< 	#elif BAR_STATUS_PATCH
< 	{ 'A',      1,     BAR_ALIGN_CENTER, width_status_es,         draw_status_es,         click_status,            "status_es" },
< 	#endif // BAR_STATUS2D_PATCH | BAR_STATUSCMD_PATCH
< 	#endif // BAR_EXTRASTATUS_PATCH
< 	#if BAR_FLEXWINTITLE_PATCH
< 	#if BAR_WINTITLE_HIDDEN_PATCH
< 	{ -1,       1,  BAR_ALIGN_RIGHT_RIGHT, width_wintitle_hidden, draw_wintitle_hidden,   click_wintitle_hidden,   "wintitle_hidden" },
< 	#endif
< 	#if BAR_WINTITLE_FLOATING_PATCH
< 	{ -1,       1,     BAR_ALIGN_LEFT,   width_wintitle_floating, draw_wintitle_floating, click_wintitle_floating, "wintitle_floating" },
< 	#endif // BAR_WINTITLE_FLOATING_PATCH
< 	#endif // BAR_FLEXWINTITLE_PATCH
< };
---
> static const BarRule barrules[] = { { -2 } };
```

```diff
diff --git a/config.mk b/config.mk
index 1c54ec6..f967836 100644
--- a/config.mk
+++ b/config.mk
@@ -33,8 +33,8 @@ FREETYPEINC = /usr/include/freetype2
 #PANGOLIB = `pkg-config --libs xft pango pangoxft`

 # Uncomment for the ipc patch / IPC_PATCH
-#YAJLLIBS = -lyajl
-#YAJLINC = -I/usr/include/yajl
+YAJLLIBS = -lyajl
+YAJLINC = -I/usr/include/yajl

 # Uncomment this for the rounded corners patch / ROUNDED_CORNERS_PATCH
 #XEXTLIB = -lXext
```

```diff
$ diff patches.def.h patches.h
80c80
< #define BAR_LTSYMBOL_PATCH 1
---
> #define BAR_LTSYMBOL_PATCH 0
119c119
< #define BAR_STATUS_PATCH 1
---
> #define BAR_STATUS_PATCH 0
156c156
< #define BAR_TAGS_PATCH 1
---
> #define BAR_TAGS_PATCH 0
159c159
< #define BAR_WINTITLE_PATCH 1
---
> #define BAR_WINTITLE_PATCH 0
410c410
< #define AUTOSTART_PATCH 0
---
> #define AUTOSTART_PATCH 1
605c605
< #define INSETS_PATCH 0
---
> #define INSETS_PATCH 1
619c619
< #define IPC_PATCH 0
---
> #define IPC_PATCH 1
817c817
< #define RESTARTSIG_PATCH 0
---
> #define RESTARTSIG_PATCH 1
```
If I don't use autostart, it runs fine, but when I run my polybar script manually, it crashes nonetheless.
I run this, dwm dies:
```
killall -q polybar &
while pgrep -u $UID -x polybar >/dev/null; do sleep 0.2; done
polybar -c ~/.config/polybar/gamebar/gamebar bgtop -r &
polybar -c ~/.config/polybar/gamebar/gamebar bgbottom -r &
sleep 0.2
polybar -c ~/.config/polybar/gamebar/gamebar fronttop -r &
polybar -c ~/.config/polybar/gamebar/gamebar frontbottom -r &
```
My setup looks exactly like yours (well instead of .y = 100 it's 68)
Jesus christ man.. I don't know what to do. I'm so close and yet so far xD
And on a sidenote, do you plan on integrating your fullscreen-combo patch too? I like the simple keybinding for fullscreen/fakefullscreen.
Hmm, your polybar has some integration with dwm-msg does it not?
Found that the IPC patch relies on there being a bar, side effect of running dwm without a bar. Can you update and try again?
Well https://github.com/mihirlad55/polybar-dwm-module requires IPC patch so I would guess that there is something going on.
Yes, now it works. :D great. If you send me your paypal I donate you some bucks for your help. =)
> If you send me your paypal I donate you some bucks for your help. =)

That won't be necessary, don't worry about it :)

> And on a sidenote, do you plan on integrating your fullscreen-combo patch too? I like the simple keybinding for fullscreen/fakefullscreen.

That should already be integrated via these options:
   - TOGGLEFULLSCREEN_PATCH
   - FAKEFULLSCREEN_CLIENT_PATCH
 
You just need to re-arrange the keybindings a bit as `MOD+f` is used to set the second (floating) layout as per dwm defaults.
I would insist on a donation but that would be disrespectful. I'd still put something up for people to give something if they want to if I were you. :P
Oh okay, I will try. Thanks so far. =)