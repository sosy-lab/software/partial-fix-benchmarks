fix the close_releases_job test (#476)

On some machines the release might not happen soon enough
after the connection holding the job was closed. Instead of
adding artificial timeout I have added reserving with timeout of 1 second.
Test will pass if the job is released sooner than in 1 second.
It should be enough even for the slowest machines.

Fixes #475
fix how connections are handled in the srv->conns heap

Previously in #453 I have changed the type of position in the heap to size_t.
I have removed the call x->rec(x, -1) for the item being removed from the heap.

That patch has broken the meaning of conn->tickpos var, that used to
contain -1 when conn was not in the srv->conns heap. -1 was assigned
only when conn was created. Later when it was just removed from conns,
it was not ever assigned -1 again.

This change fixes how connections are handled in the srv->conns heap.
New var Conn->in_conns is used to track if conn exists in the heap.
Conn->tickpos is size_it and the connrec function has a prototype
matching the heap's Record prototype.

Fixes #472
fix how connections are handled in the srv->conns heap

Previously in #453 I have changed the type of position in the heap to size_t.
I have removed the call x->rec(x, -1) for the item being removed from the heap.

That patch has broken the meaning of conn->tickpos var, that used to
contain -1 when conn was not in the srv->conns heap. -1 was assigned
only when conn was created. Later when it was just removed from conns,
it was not ever assigned -1 again.

This change fixes how connections are handled in the srv->conns heap.
New var Conn->in_conns is used to track if conn exists in the heap.
Conn->tickpos is size_it and the connrec function has a prototype
matching the heap's Record prototype.

Fixes #472
fix how connections are handled in the srv->conns heap (#478)

Previously in #453 I have changed the type of position in the heap to size_t.
I have removed the call x->rec(x, -1) for the item being removed from the heap.

That patch has broken the meaning of conn->tickpos var, that used to
contain -1 when conn was not in the srv->conns heap. -1 was assigned
only when conn was created. Later when it was just removed from conns,
it was not ever assigned -1 again.

This change fixes how connections are handled in the srv->conns heap.
New var Conn->in_conns is used to track if conn exists in the heap.
Conn->tickpos is size_it and the connrec function has a prototype
matching the heap's Record prototype.

Fixes #472
fix prototypes of the Less and Record typedefs

This change improves readability of the code.
It brings consistency into functions of the binary heap.
Typedef Less was renamed to less_fn, Record to setpos_fn.
connless and connrec were changed to match to the corresponding typdefs.

The rec member of heap was renamed to "setpos".

The job was updated in the same way.

Fixes #472
fix prototypes of the Less and Record typedefs (#480)

This change improves readability of the code.
It brings consistency into functions of the binary heap.
Typedef Less was renamed to less_fn, Record to setpos_fn.
connless and connrec were changed to match to the corresponding typdefs.

The rec member of heap was renamed to "setpos".

The job was updated in the same way.

Fixes #472
