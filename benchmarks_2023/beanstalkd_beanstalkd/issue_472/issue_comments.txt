cast between incompatible function types
The `Record` function type is used by the binary heap to record the new positions of elements whenever they get moved or inserted. (Also I think "Record" is not a great name for this type, sorry.) ~The -1 value is probably coming from heapremove (which uses -1 to indicate the element has been removed).~ [Edit: I was looking at a stale copy of the code, sorry.]

Now that the compilers are being run in a more strict mode, it might be necessary to define connrec more like job_setheappos. Also see the comment on job_setheappos in dat.h.
Thanks @kr, it looks like a good next step to me.