Beta flight 3.0 Yaw jump/P-error windup
Sorry, I don't see the connection between my issue and the PWM mapping. Did this get referenced by accident?

That indeed seems wrong.

Your issue seems like a typical esc desync :(

If one looks at the bb log, the yaw P starts to oscillates with increasing amplitude until yaw P rails (I doesn't seem to be doing anything). It does look like a tuning issue (to high of a yaw P gain), but it is already quite low (4.0). Could it have anything to do with the new 2 DOF PID? E.g. that for some reason the yaw error is larger than it should and then things get out of control?

Your yaw is far over 600deg/sec. It just seems that your copter isnt really capable of doing that. Lower your yaw rates.
Yaw rate of 1.0 and 1.6 is extremally high!

When it goes crazy it yaws at almost twice that (>1000 deg/s). I hope I have time to look into my issues later today. Thanks for your input.

Well your yaw rate is really crazy! You are simply asking your quad to do
something it cant

2016-09-09 14:59 GMT+02:00 Florian notifications@github.com:

> When it goes crazy it yaws at almost twice that (>1000 deg/s). I hope I
> have time to look into my issues later today. Thanks for your input.
> 
> —
> You are receiving this because you modified the open/close state.
> Reply to this email directly, view it on GitHub
> https://github.com/betaflight/betaflight/issues/1129#issuecomment-245905487,
> or mute the thread
> https://github.com/notifications/unsubscribe-auth/AKQlhIatqZUJVn8TquvtDwf2TYxIvx0Rks5qoVgmgaJpZM4J4kht
> .

In https://github.com/betaflight/betaflight/blob/master/src/main/flight/pid.c Line 284:
`axisPID[axis] = lrintf(PTerm + ITerm);`

Should there be a `constrain()`?

No constrain needed. Yaw P has yaw_p_limit ;)

Quads cant really do >1000deg/sec without using full power

I figured out what my problem was: I got confused with the new rate calculations (I had to re-read it several times). What I meant to set is the yaw rc rate, not the yaw srate. That gave me insane yaw rates at full stick, causing all sorts of issues as @borisbstyle pointed out.
