diff --git a/src/deck/core/deck_fw_update.c b/src/deck/core/deck_fw_update.c
index 7e540c5566..c3c1cb0474 100644
--- a/src/deck/core/deck_fw_update.c
+++ b/src/deck/core/deck_fw_update.c
@@ -109,6 +109,8 @@ TESTABLE_STATIC bool handleMemRead(const uint32_t memAddr, const uint8_t readLen
         index += bytesToUse;
     }
 
+    // ToDo: call the read function for the deck if this is called within a deck address space
+
     return true;
 }
 
@@ -118,7 +120,7 @@ static bool handleMemWrite(const uint32_t memAddr, const uint8_t writeLen, const
     if (deckInf) {
         if (deckInf->driver->fwUpdate) {
             const uint32_t uploadAddress = deckNr * DECK_FW_MAX_SIZE;
-            deckInf->driver->fwUpdate->updateFcn(memAddr - uploadAddress, writeLen, buffer);
+            deckInf->driver->fwUpdate->write(memAddr - uploadAddress, writeLen, buffer);
         }
     }
 
diff --git a/src/deck/drivers/src/lighthouse.c b/src/deck/drivers/src/lighthouse.c
index a87ac809e4..48a6c9ba69 100644
--- a/src/deck/drivers/src/lighthouse.c
+++ b/src/deck/drivers/src/lighthouse.c
@@ -72,15 +72,16 @@ static void ledTimerHandle(xTimerHandle timer) {
   lighthouseCoreLedTimer();
 }
 
-static void newFwBlockReceived(const uint32_t vAddr, const uint8_t len, const uint8_t* buffer) {
+static bool write_firmware(const uint32_t vAddr, const uint8_t len, const uint8_t* buffer) {
   // TODO krri write block to flash
+  return false;
 }
 
 static const DeckFwUpdateDef_t fwUpdateDef = {
   // TODO krri set values
   .requiredSize = 1234,
   .requiredHash = 9876,
-  .updateFcn = newFwBlockReceived,
+  .write = write_firmware,
 };
 
 static const DeckDriver lighthouse_deck = {
diff --git a/src/deck/interface/deck_core.h b/src/deck/interface/deck_core.h
index 4eaab53171..46ece25206 100644
--- a/src/deck/interface/deck_core.h
+++ b/src/deck/interface/deck_core.h
@@ -144,11 +144,26 @@ typedef struct deckInfo_s {
 /**
  * @brief Definition of function that is called when a block of a new firmware is uploaded to the deck.
  * The upload will be done in small but continouse pieces.
- * @param vAddr: the address for this block in the virtual deck address space starting at 0
- * @param len: length of the block
- * @param buffer: a buffer with a part of the firmware
+ * @param address: Address where the buffer should be written. The start of the firmware is at address 0.
+ * @param len: Buffer length
+ * @param buffer: Buffer to write in the firmware memory
+ * 
+ * @return True if the buffer could be written successully, false otherwise (if the deck if not in bootloader
+ *         mode for example)
  */
-typedef void (deckFwUpdateFcn)(const uint32_t vAddr, const uint8_t len, const uint8_t* buffer);
+typedef bool (deckFwWrite)(const uint32_t vAddr, const uint8_t len, const uint8_t* buffer);
+
+/**
+ * @brief Definition of function to read the firmware
+ * 
+ * @param addr: Address where the data should be read. The start of the firmware is at address 0.
+ * @param len: Length to read.
+ * @param buffer: Buffer where to output the data
+ * 
+ * @return True if the buffer could be read successully, false otherwise (if the deck if not in bootloader
+ *         mode for example)
+ */
+typedef bool (deckFwRead)(const uint32_t vAddr, const uint8_t len, uint8_t* buffer);
 
 /**
  * @brief This struct defines the firmware required by the deck and the function
@@ -159,8 +174,9 @@ typedef struct deckFwUpdateDef_s {
   uint32_t requiredHash;
   uint32_t requiredSize;
 
-  // Function that will be called when new firmware is uploaded to the deck.
-  deckFwUpdateFcn* updateFcn;
+  // Function that will be called when new firmware is uploaded to or downloaded from the deck.
+  deckFwWrite* write;
+  deckFwRead* read;
 } DeckFwUpdateDef_t;
 
 int deckCount(void);
diff --git a/src/modules/interface/lighthouse/lighthouse_deck_flasher.h b/src/modules/interface/lighthouse/lighthouse_deck_flasher.h
index 46ffd33af8..52a2bfdc80 100644
--- a/src/modules/interface/lighthouse/lighthouse_deck_flasher.h
+++ b/src/modules/interface/lighthouse/lighthouse_deck_flasher.h
@@ -28,4 +28,8 @@
 
 #pragma once
 
+#include <stdbool.h>
+
+void lighthouseDeckFlasherInit();
+
 bool lighthouseDeckFlasherCheckVersionAndBoot();
diff --git a/src/modules/src/lighthouse/lighthouse_core.c b/src/modules/src/lighthouse/lighthouse_core.c
index 0270bc5ac2..3006cffe11 100644
--- a/src/modules/src/lighthouse/lighthouse_core.c
+++ b/src/modules/src/lighthouse/lighthouse_core.c
@@ -136,6 +136,7 @@ static void modifyBit(uint16_t *bitmap, const int index, const bool value) {
 }
 
 void lighthouseCoreInit() {
+  lighthouseDeckFlasherInit();
   lighthousePositionEstInit();
 }
 
diff --git a/src/modules/src/lighthouse/lighthouse_deck_flasher.c b/src/modules/src/lighthouse/lighthouse_deck_flasher.c
index 0e8e202c5b..473b87e5cb 100644
--- a/src/modules/src/lighthouse/lighthouse_deck_flasher.c
+++ b/src/modules/src/lighthouse/lighthouse_deck_flasher.c
@@ -34,6 +34,7 @@
 #include "lh_bootloader.h"
 #include "lighthouse_deck_flasher.h"
 #include "crc32.h"
+#include "mem.h"
 
 #ifdef LH_FLASH_BOOTLOADER
 #include "lh_flasher.h"
@@ -42,6 +43,22 @@
 #define BITSTREAM_CRC 0xe2889216
 #define BITSTREAM_SIZE 104092
 
+static uint32_t getFirmwareSize(void);
+static bool readFirmware(const uint32_t memAddr, const uint8_t readLen, uint8_t* buffer);
+static bool writeFirmware(const uint32_t memAddr, const uint8_t writeLen, const uint8_t* buffer);
+
+static const MemoryHandlerDef_t flasherMemory = {
+  .type = MEM_TYPE_DECK_FW,
+  .getSize = getFirmwareSize,
+  .read = readFirmware,
+  .write = writeFirmware,
+};
+
+void lighthouseDeckFlasherInit()
+{
+  // Register access to the flash in the memory subsystem
+  memoryRegisterHandler(&flasherMemory);
+}
 
 bool lighthouseDeckFlasherCheckVersionAndBoot() {
   lhblInit(I2C1_DEV);
@@ -95,3 +112,18 @@ bool lighthouseDeckFlasherCheckVersionAndBoot() {
   
   return pass;
 }
+
+static uint32_t getFirmwareSize(void)
+{
+  return 256*1024;
+}
+
+static bool readFirmware(const uint32_t memAddr, const uint8_t readLen, uint8_t* buffer)
+{
+  return lhblFlashRead(LH_FW_ADDR + memAddr, readLen, buffer);
+}
+
+static bool writeFirmware(const uint32_t memAddr, const uint8_t writeLen, const uint8_t* buffer)
+{
+  return false;
+}
