diff --git a/src/deck/drivers/interface/lighthouse.h b/src/deck/drivers/interface/lighthouse.h
index 591137d972..87ae2d4445 100644
--- a/src/deck/drivers/interface/lighthouse.h
+++ b/src/deck/drivers/interface/lighthouse.h
@@ -30,7 +30,8 @@
  * lighthouse.h: lighthouse tracking system receiver
  */
 
-#ifndef __LIGHTHOUSE_H__
-#define __LIGHTHOUSE_H__
+#pragma once
 
-#endif // __LIGHTHOUSE_H__
+
+#define LIGHTHOUSE_BITSTREAM_CRC 0xe2889216
+#define LIGHTHOUSE_BITSTREAM_SIZE 104092
diff --git a/src/deck/drivers/src/lighthouse.c b/src/deck/drivers/src/lighthouse.c
index 48a6c9ba69..c2a6166a68 100644
--- a/src/deck/drivers/src/lighthouse.c
+++ b/src/deck/drivers/src/lighthouse.c
@@ -40,6 +40,7 @@
 #include "timers.h"
 
 #include "lighthouse_core.h"
+#include "lighthouse_deck_flasher.h"
 
 // LED timer
 static StaticTimer_t timerBuffer;
@@ -72,16 +73,12 @@ static void ledTimerHandle(xTimerHandle timer) {
   lighthouseCoreLedTimer();
 }
 
-static bool write_firmware(const uint32_t vAddr, const uint8_t len, const uint8_t* buffer) {
-  // TODO krri write block to flash
-  return false;
-}
-
 static const DeckFwUpdateDef_t fwUpdateDef = {
   // TODO krri set values
   .requiredSize = 1234,
   .requiredHash = 9876,
-  .write = write_firmware,
+  .write = lighthouseDeckFlasherWrite,
+  .read = lighthouseDeckFlasherRead,
 };
 
 static const DeckDriver lighthouse_deck = {
diff --git a/src/modules/interface/lighthouse/lighthouse_deck_flasher.h b/src/modules/interface/lighthouse/lighthouse_deck_flasher.h
index 52a2bfdc80..102712a6c1 100644
--- a/src/modules/interface/lighthouse/lighthouse_deck_flasher.h
+++ b/src/modules/interface/lighthouse/lighthouse_deck_flasher.h
@@ -30,6 +30,8 @@
 
 #include <stdbool.h>
 
-void lighthouseDeckFlasherInit();
-
 bool lighthouseDeckFlasherCheckVersionAndBoot();
+
+bool lighthouseDeckFlasherRead(const uint32_t memAddr, const uint8_t readLen, uint8_t* buffer);
+
+bool lighthouseDeckFlasherWrite(const uint32_t memAddr, const uint8_t writeLen, const uint8_t* buffer);
\ No newline at end of file
diff --git a/src/modules/src/lighthouse/lighthouse_core.c b/src/modules/src/lighthouse/lighthouse_core.c
index 3006cffe11..0270bc5ac2 100644
--- a/src/modules/src/lighthouse/lighthouse_core.c
+++ b/src/modules/src/lighthouse/lighthouse_core.c
@@ -136,7 +136,6 @@ static void modifyBit(uint16_t *bitmap, const int index, const bool value) {
 }
 
 void lighthouseCoreInit() {
-  lighthouseDeckFlasherInit();
   lighthousePositionEstInit();
 }
 
diff --git a/src/modules/src/lighthouse/lighthouse_deck_flasher.c b/src/modules/src/lighthouse/lighthouse_deck_flasher.c
index 473b87e5cb..e3fda9252d 100644
--- a/src/modules/src/lighthouse/lighthouse_deck_flasher.c
+++ b/src/modules/src/lighthouse/lighthouse_deck_flasher.c
@@ -31,6 +31,8 @@
 
 #define DEBUG_MODULE "LHFL"
 #include "debug.h"
+
+#include "lighthouse.h"
 #include "lh_bootloader.h"
 #include "lighthouse_deck_flasher.h"
 #include "crc32.h"
@@ -40,25 +42,7 @@
 #include "lh_flasher.h"
 #endif
 
-#define BITSTREAM_CRC 0xe2889216
-#define BITSTREAM_SIZE 104092
-
-static uint32_t getFirmwareSize(void);
-static bool readFirmware(const uint32_t memAddr, const uint8_t readLen, uint8_t* buffer);
-static bool writeFirmware(const uint32_t memAddr, const uint8_t writeLen, const uint8_t* buffer);
-
-static const MemoryHandlerDef_t flasherMemory = {
-  .type = MEM_TYPE_DECK_FW,
-  .getSize = getFirmwareSize,
-  .read = readFirmware,
-  .write = writeFirmware,
-};
-
-void lighthouseDeckFlasherInit()
-{
-  // Register access to the flash in the memory subsystem
-  memoryRegisterHandler(&flasherMemory);
-}
+static bool inBootloaderMode = true;
 
 bool lighthouseDeckFlasherCheckVersionAndBoot() {
   lhblInit(I2C1_DEV);
@@ -90,40 +74,40 @@ bool lighthouseDeckFlasherCheckVersionAndBoot() {
   crc32Context_t crcContext;
   crc32ContextInit(&crcContext);
 
-  for (int i=0; i<=BITSTREAM_SIZE; i+=64) {
-    int length = ((i+64)<BITSTREAM_SIZE)?64:BITSTREAM_SIZE-i;
+  for (int i=0; i<=LIGHTHOUSE_BITSTREAM_SIZE; i+=64) {
+    int length = ((i+64)<LIGHTHOUSE_BITSTREAM_SIZE)?64:LIGHTHOUSE_BITSTREAM_SIZE-i;
     lhblFlashRead(LH_FW_ADDR + i, length, (uint8_t*)deckBitstream);
     crc32Update(&crcContext, deckBitstream, length);
   }
 
   uint32_t crc = crc32Out(&crcContext);
-  bool pass = crc == BITSTREAM_CRC;
+  bool pass = crc == LIGHTHOUSE_BITSTREAM_CRC;
   DEBUG_PRINT("Bitstream CRC32: %x %s\n", (int)crc, pass?"[PASS]":"[FAIL]");
 
   // Launch LH deck FW
   if (pass) {
     DEBUG_PRINT("Firmware version %d verified, booting deck!\n", deckVersion);
     lhblBootToFW();
+    inBootloaderMode = false;
   } else {
     DEBUG_PRINT("The deck bitstream does not match the required bitstream.\n");
-    DEBUG_PRINT("We require lighthouse bitstream of size %d and CRC32 %x.\n", BITSTREAM_SIZE, BITSTREAM_CRC);
+    DEBUG_PRINT("We require lighthouse bitstream of size %d and CRC32 %x.\n", LIGHTHOUSE_BITSTREAM_SIZE, LIGHTHOUSE_BITSTREAM_CRC);
     DEBUG_PRINT("Leaving the deck in bootloader mode ...\n");
   }
   
   return pass;
 }
 
-static uint32_t getFirmwareSize(void)
+bool lighthouseDeckFlasherRead(const uint32_t memAddr, const uint8_t readLen, uint8_t* buffer)
 {
-  return 256*1024;
-}
-
-static bool readFirmware(const uint32_t memAddr, const uint8_t readLen, uint8_t* buffer)
-{
-  return lhblFlashRead(LH_FW_ADDR + memAddr, readLen, buffer);
+  if (inBootloaderMode) {
+    return lhblFlashRead(LH_FW_ADDR + memAddr, readLen, buffer);
+  } else {
+    return false;
+  }
 }
 
-static bool writeFirmware(const uint32_t memAddr, const uint8_t writeLen, const uint8_t* buffer)
+bool lighthouseDeckFlasherWrite(const uint32_t memAddr, const uint8_t writeLen, const uint8_t* buffer)
 {
   return false;
 }
