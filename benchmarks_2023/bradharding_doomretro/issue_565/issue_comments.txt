HR save game little issue
Ah, this is because HR.WAD doesn't have any DEHACKED nor MAPINFO lumps to specify the mapnames, which I guess I don't accommodate for when updating the savegame description. I'll look into it, thanks!
sorry, i forget to mention that i made a little dehacked patch for map names and intermission screens. So actual names appeard at the corresponding maps, but if i save with a different name, at the next map, the save name is reseted to the current map name.
In other wads that doesn't happend at all.
Check this dehacked, if i remember right, is in extended dehacked format.
[hrDeh.zip](https://github.com/bradharding/doomretro/files/4123194/hrDeh.zip)

Oh, yes, that dehacked lump would make a difference. I'll reopen this and look into it further. (At least commit 76616f6fc7422bb54cdfdd0ec7d757ea69f4107b fixed another related problem.)
Hey, Brad, how you doing?
I found this same issue happening in No end in sight. I'm using the lates release, 3.5.5 and taking precaution to not make it crash. And just using the proper files that comes in NEIS.zip: neis.wad and neis.deh.
Its the same related issue, but if you prefer i will open an new issue thread.
Hi @P41R47, sorry for the late reply. When you're saving a game using NEIS.wad, what map are you in, and what exactly are you changing the save description to?
When one first save in Doom Retro, it automatically put the name of the map. In this case, NEIS maps names. Then i change it to whatever come in handy, and when i save again, at the start of the next map, it again changes to the name of the map, and i again have to change it, and so on... This occurs since the first map of the first episode and in every one of it . I change the save description, usually, to my name as i'm not the only playing Doom Retro , or the name of the megawad, NEIS or No end in sight.
Here's what I've tried:

1. Load NEIS.wad.
2. Start a new game
3. In E1M1, go to the savegame menu, select an empty save slot, change the save description (in this case, "TERMINAL") to "BRAD".
4. Finish the map.
5. At the start of E1M2, the existing savegame is autosaved as "BRAD".

This seems to work as expected for me. Are you saying that for you at step 5, the save description is changed to the name of E1M2 (in this case, "SLIME TRAILS")?
In step 5 i made it manually because i not use autosave, and when i try to save it change to the name of the map, but it don't save yet, it stay there, then i have to rewrite the name i put on it, and it save with the name as i branded the save slot. But at the start of the next map, or if i want to save somwehere further into the same map, this happends again, while in other wads i just put the name i want, and save, over and over it without having to rewrite it.
This is how i normally save, and it doesn't change back to the map name in other wads, this only happens , as of now, with hell revealed and NEIS.
Okay, so I've disabled the `autosave` CVAR and repeated steps 1 to 4. At step 5 I manually save. I'm not able to replicate this. The save description does not update to the map name for me, it stays as "BRAD".

A couple of questions:
1) Could you please give me a specific example of what you're entering as the save description?
2) How are loading the wad? Maybe that would be a factor. Are you using the WAD launcher, and if not, what's the command-line?
Oh, really? Then my PC is going NUTS!

In the save description for Hell Revealed, i put that, ''Hell Revealed'' without the 
quotation marks.
Same with NEIS, i put ''No End In Sight''.
As for the command line, i use the following for both wads:

@echo off
start doomretro.exe -iwad doom2.wad -file wads\hr.wad wads\hrmus.wad -deh dehacked\hellrevealed.deh
exit

@echo off
start doomretro.exe -iwad doom.wad -file wads\NEIS.wad wads\prjddoom.wad -deh dehacked\neis.deh
exit
I think I've finally confirmed what's happening. When saving over an existing game (doesn't matter if it's done automatically at the start of a map, or manually through the savegame menu), DOOM Retro checks the existing savegame description. It compares it with the names of all the maps currently available. If you change a savegame description to "Hell Revealed", well, that matches with MAP30's name, and so DOOM Retro assumes you didn't type it manually and then updates it to the current map name.

So, just don't change a savegame description to another map name (even if it happens to share the PWAD's name)!

I will see if it's feasible to match the savegame description with only the map the savegame was saved in, however.
But the same happends in NEIS, even if i put NEIS or No End In Sight.
I can try what you stated with Vile Flesh, a mapset that haves map30 as Hell Revealed, too.

EDIT: i can confirm you that this happends in Vile Flesh if i name the save as Hell Revealed. Also, i confirm that what you stated of Hell Revealed wad is also true, i changed it to another thing and it save.

NEIS has a map called "No End In Sight". I can replicate your issue by using that, but not "NEIS".
Yes, true. Strange, i remmeber putting NEIS and also change, maybe i made a mistake. Sorry about that.
But also, great work, Brad!
You really solved this crime that was here for soo long.
Sorry if this brings you a lot of headache, Brad.
Surely i'm the only fool that stumble with this.
LOL! Not a problem at all, Pablo! I do appreciate the issues that you raise, I really do! And I am working on a change right now, that only checks the savegame description with the name of the map the savegame was saved in, which makes more sense, so this particular issue hasn't been a waste at all. 👍 