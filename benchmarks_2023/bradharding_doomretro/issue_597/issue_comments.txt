Midi issues on Linux
Thanks @brzegorz. Being that I only build for Windows, and have no Linux system, I forgot all about this. I'll look at either having a `s_soundfont` CVAR visible in non-Windows builds only, or at least a warning in the console whenever `SDL_SOUNDFONTS` isn't set.
Gone with the latter. See dd7a87b21c805fbd6990be91732be9cdcf103a7d. Thanks again!
Hi again, thank you for swift action. It seems that I've misunderstood something about my system. I've recompiled doomretro and set $SDL_SOUNDFONTS in my bashrc. However, this doesn't solve the midi issue! I can't get doomretro to print the warning you've added, whether $SDL_SOUNDFONTS is set or not. Too get the midi, I need to run Doom like that:
$ SDL_SOUNDFONTS=$SDL_SOUNDFONTS doomretro ...
So it seems that it's not about environment variable, even though I've thought that that's exactly what I'm setting when doing a "X=Y program_name" invocation.
Thanks for clarifying! I'll look at removing the changes in commit dd7a87b21c805fbd6990be91732be9cdcf103a7d since they aren't necessary then.