Add Upstart job for bumblebeed
Added driver-specific configuration files (GH-26)
(GH-26) Fixed some boolean options to keep expected behavior. Eventually the variables must change names to reflect the configuration names.

To test, build the binaries and just run:
 $ bin/bumblebee -vv -C conf/bumblebee.conf

Won't run the daemon but the configuration parser will be executed.
Look at the dumped configuration and play arround with it.
bumblebee.conf cleanup and build scripts update to match new configuration parser (GH-26)
Split parser order (Closes GH-29, fixes GH-26 conflict)
Load (driver) settings in the right order (GH-26)
