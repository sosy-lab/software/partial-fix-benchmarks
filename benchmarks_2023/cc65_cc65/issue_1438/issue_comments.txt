Odd crash in cc65, related to delayed post-counting
It would - of course - be great if you could identify which change brought that crash...
I will try :)
The cause is that the deferred post-inc/dec for `__fastcall__` function calls always tries to get the expression info for the last parameter that is supposed to be passed via AX/EAX even when there are no parameters at all. This is fixed by PR #1439.
It's still crashing here. The testcase looks like this:
```

/* Issue #1438 fix #1439 - crash in cc65, related to delayed post-counting 

  this is an odd issue, the compile would crash *sometimes*, perhaps in one
  of ten compilation runs.
*/

unsigned short __fastcall__ func2(void)
{
    return 42;
}

void func1(unsigned short *wp)
{
    *wp++ = func2();
}

int main(void)
{
    return 0;
}
```
For testing i use a makefile that has
```
cc65 -o foo bug1438.c
```
in it a few dozen times, it will crash randomly after a couple compilation runs
Doh. False alarm. Just git playing tricks on me again *shakes fist*