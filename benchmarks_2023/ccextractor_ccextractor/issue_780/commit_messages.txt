Add GUI for CCExtractor (GSoC 2017) (#794)
Fixes Undeclared-Variable Warnings in extractor.c
Fixes https://github.com/CCExtractor/ccextractor/issues/780

Signed-off-by: Saksham Gupta <shucon01@gmail.com>
Fixes Undeclared-Variable Warnings in extractor.c (#795)

Fixes https://github.com/CCExtractor/ccextractor/issues/780

Signed-off-by: Saksham Gupta <shucon01@gmail.com>
Fixes Unused-Variables warnings

Partially-Fixes https://github.com/CCExtractor/ccextractor/issues/780
