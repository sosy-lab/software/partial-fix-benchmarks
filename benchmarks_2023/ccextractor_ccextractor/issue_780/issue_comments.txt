Compilation warnings on C++ 11
Oh, man. If you want compiler warnings, there's always been a gazilllion on the mac. (where gazillion is defined as "about 300" currently)

I particularly like the "tautological" ones:
> comparison of constant 256 with expression of type 'unsigned char' is always true [-Wtautological-constant-out-of-range-compare]
>                                         if (c < 256)

@mackworth where are those warnings? If they are in GPAC or any other library we just borrowed probably not worth it to do anything about it (except update the library), but if they are in our code we should definitely fix them.
So, the errors are generated from api/extractors/extractor.c,  src/lib_ccx  src/zlib, and src/zvbi
[ccextractor Errors.zip](https://github.com/CCExtractor/ccextractor/files/1384100/ccextractor.Errors.zip)


@cfsmp3   all the warnings are not eliminated yet , reopen the issue please.
@cfsmp3 Please re-open the issue.

Note to the people working on this issue : ff you are building on Linux using the build scripts located inside the `/linux/` directory, please use `./builddebug` for complete list of warnings being generated.
Just as an update, here's the 3,400 lines of compilation warnings on the current master branch when built on the Mac...  (Reflecting only 290 actual warnings).
[ccextractorWarnings.txt]

(https://github.com/CCExtractor/ccextractor/files/4048936/ccextractorWarnings.txt)

I don't think this is specific to Mac.

On linux I get thousands of lines as well. In fact there are so many warnings that it fills my scrollback buffer.
Many are bogus but there's a few ones that at least show raise some eyebrows:

```
../src/lib_ccx/ts_functions.c:89:47: warning: format specifies type
      'unsigned long' but the argument has type 'uint64_t' (aka
      'unsigned long long') [-Wformat]
        printf("Packet start code prefix: %04lx # ", pes_prefix);
                                          ~~~~~      ^~~~~~~~~~
                                          %04llx
../src/lib_ccx/ts_functions.c:127:10: warning: implicit conversion from
      enumeration type 'enum ccx_bufferdata_type' to different enumeration type
      'enum ccx_stream_type' [-Wenum-conversion]
                return CCX_PES;
```

A good proportion of the warnings have to do with ignoring the return value of `write`.
Maybe we should just disable that specific warning and move on. I don't want to fill the code casts to void or replace calls to write to a function that does the cast, etc. 
I thing it's a good thing that we try to fix compilation warnings because if there are too many warnings (like it is currently), then we just ignore the warnings which can sometimes help
Yes but some warnings are just overkill. Ignoring the return of a function when you're only interested in its secondary effect is totally normal. 
@cfsmp3 i don't see any warnings when built on linux. Is this solved for Linux systems ?
It's heavily dependent on your used compiler version and other parameters, but they're for sure not all fixed yet.
oh i see! yeah i'm using C++14. 
Closing as I don't see warnings on master now.
Just FYI, although they are much less problematic, I still see 397 errors during compilation on my Mac.
Almost all are `-Wpointer-sign` warnings with many different specific types, but a sample is:
```
passing 'u8 *' (aka 'unsigned char *') to parameter of type 'const char *' converts between pointers to integer types with different sign [-Wpointer-sign]
```
There's also 8 `-Wself-assign` warnings such as
```
explicitly assigning value of variable of type 'const uint8_t *' (aka 'const unsigned char *') to itself [-Wself-assign]
```

and a handful of others:
```
unused function 'cpr1204_crc' [-Wunused-function]
unused variable 'last_sub' [-Wunused-variable]

format specifies type 'long' but the argument has type 'LLONG' (aka 'long long') [-Wformat]
format specifies type 'unsigned long' but the argument has type 'uint64_t' (aka 'unsigned long long') [-Wformat]
implicit conversion from enumeration type 'const vbi_modulation' to different enumeration type 'vbi3_modulation' [-Wenum-conversion]
```