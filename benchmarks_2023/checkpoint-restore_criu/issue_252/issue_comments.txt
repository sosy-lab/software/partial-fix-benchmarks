Need support for cgroup2 hierarchies
Cc @cyrillos 
Will take a look, thanks.
Has this been fixed in the 3.2 release?
I fear it hasn't been yet.
I've sent a fix for a hang some time ago, but it hasn't been merged :( I'll ping maintainer.
Should we reopen this and mark as `new-feature`?
Just so we don't forget that we don't handle cgroup2.
@0x7f454c46 , you're right :) I was too fast closing it.
A friendly reminder that this issue had no activity for 30 days.
commit 98e9165f0cbe821995c8903ee24ceb7f1515c2c7
Date:   Fri Jun 19 10:33:49 2020 +0300

    cgroup: Add the initial support for cgroup2

A friendly reminder that this issue had no activity for 30 days.
A friendly reminder that this issue had no activity for 30 days.