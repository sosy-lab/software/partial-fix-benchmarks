Transparent window decorations do not work consistently in openbox
I can't reproduce this. I'm using openbox myself. Could you try building from c451c0805878051310352b2add22593fc4ffd437 instead and see what happens? It would be nice to know if this was a regression from the refactor.

edit: sorry, wrong commit, fixed.

edit: Also, it's sort of hard to see which decorations are transparent and which are opaque in that screenshot. Is it just pcmanfm that has opaque decorations?

It applies randomly to all applications and still occurs after building from that commit.

Also, it seems to occur less frequently with the -S option.

Are you sure it's not the shadow clouding the transparency? Do you get the same effects with shadows turned off?

Yes, as well as with multiple window manager themes.

With compton -e 0.4, these were the results

http://goput.it/o7d.png

With compton -e 0.4 -S, I didn't get an improperly rendered pcmanfm window until opening ~20 windows on the same workspace.

The way pcmanfm behaves is especially odd. If a window is opened and then closed, and another one is opened, it will have an opaque titlebar each time (barring usage of -S), but if a second window is opened alongside the first, subsequent windows will have/but still sometimes not have window decorations.

Hmm, that's very very odd. Are you using openbox 3.5?

edit: Nevermind, guessing you are based on your distro.

I'll have to look into this more. I'll try as hard as I can to reproduce it.

Any luck with c01befec1f39e62589dc4e0bc1c177f85f6ded29 ?

Sorry for the delay.

The problem still occurs with c01befe

@th12h: Please, if you are still active, test if this problem occurs on a build of latest `richardgv-dev` branch. A series of changes has been made since those 8 months, and I've never noticed this problem myself. This issue will be closed if I don't see a reply from you in 3 days.
