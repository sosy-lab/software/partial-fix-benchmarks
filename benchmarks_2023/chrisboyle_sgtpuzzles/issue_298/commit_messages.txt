Bump version.
Fix #298: lib expects existence of stpcpy, so fake it in executable.

(Because the library is built at API >= 21 and the executable isn't.)
#298 wants reopening, x86 devices are crashing. Let's just build with ndk r10c for now.

Reverts 07475df0fda7a6d8861756ed79f13621c02eeee9.
Re #298, implement stpcpy in library rather than executable.

This appears to work on the emulator at API 21 as well as below.

The reported crashes were reproducible on the emulator.
We don't appear to need to reinvent stpcpy any more

#298 was long enough ago that nobody should be building with an NDK that old.
Let's check nothing explodes in the next beta.
