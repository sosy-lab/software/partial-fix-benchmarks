x86/libpuzzles.so: error: undefined reference to 'stpcpy'
Reopening because of crash reports; that fix seems insufficient for x86 devices, on which libpuzzles.so really does need stpcpy to resolve. I could try including an implementation of it within libpuzzles.so, but then where does that leave API-21 devices which do in fact have their own in bionic?

```
java.lang.UnsatisfiedLinkError: dlopen failed: cannot locate symbol "stpcpy" referenced by "libpuzzles.so"...
at java.lang.Runtime.loadLibrary(Runtime.java:364)
at java.lang.System.loadLibrary(System.java:526)
at name.boyle.chris.sgtpuzzles.GamePlay.<clinit>(GamePlay.java:1810)
[etc.]
```

That commit ought to have fixed it; let's see what happens...

Hello Sir @chrisboyle ,
Greetings of  the day.
I Created an android studio project and copied your files into that project.
When I run the game , i encountered error mentioned in popup 
/data/app/name.boyle.chris.sgtpuzzles-1/lib/arm/**libpuzzlesgen-with-pie.so**: open failed ENOENT (NO such file or directory).

I opened apk and found libpuzzles.so but not  libpuzzlesgen-with-pie.so
Please take a look at the file "README-android". It's not 100% up to date
but does include the following few lines about how to build that file:

"IMPORTANT: run the gradle task "buildGameGenerationExecutableDebug",
ideally
before installation (or force a rebuild after). This gives you the
executable
required to generate each new game ("puzzlesgen")."

Versions of this file will be packaged as libpuzzles*.so for installation.

It has so far not been possible to automate inclusion of this
buildGameGenerationExecutableDebug step in the normal build process,
because of limitations in Android's gradle build system.

On Wed, 5 Jul 2017 at 06:56 yapappgagan <notifications@github.com> wrote:

> Hello Sir @chrisboyle <https://github.com/chrisboyle> ,
> Greetings of the day.
> I Created an android studio project and copied your files into that
> project.
> When I run the game , i encountered error mentioned in popup
> /data/app/name.boyle.chris.sgtpuzzles-1/lib/arm/
> *libpuzzlesgen-with-pie.so*: open failed ENOENT (NO such file or
> directory).
>
> I opened apk and found libpuzzles.so but not libpuzzlesgen-with-pie.so
>
> —
> You are receiving this because you were mentioned.
> Reply to this email directly, view it on GitHub
> <https://github.com/chrisboyle/sgtpuzzles/issues/298#issuecomment-313003027>,
> or mute the thread
> <https://github.com/notifications/unsubscribe-auth/AAHxSQBCwrGNO3U6Q16ZDSLJCCmwnNunks5sKyWjgaJpZM4Ff4wd>
> .
>
-- 
Chris Boyle
https://chris.boyle.name/
