diff --git a/src/backend/distributed/shardsplit/shardsplit_decoder.c b/src/backend/distributed/shardsplit/shardsplit_decoder.c
index 59fb3118ffe..82bc1276859 100644
--- a/src/backend/distributed/shardsplit/shardsplit_decoder.c
+++ b/src/backend/distributed/shardsplit/shardsplit_decoder.c
@@ -34,6 +34,10 @@ static Oid FindTargetRelationOid(Relation sourceShardRelation,
 								 HeapTuple tuple,
 								 char *currentSlotName);
 
+static HeapTuple GetTupleForTargetSchema(HeapTuple sourceRelationTuple,
+										 TupleDesc sourceTupleDesc,
+										 TupleDesc targetTupleDesc);
+
 /*
  * Postgres uses 'pgoutput' as default plugin for logical replication.
  * We want to reuse Postgres pgoutput's functionality as much as possible.
@@ -87,12 +91,13 @@ split_change_cb(LogicalDecodingContext *ctx, ReorderBufferTXN *txn,
 	}
 
 	Oid targetRelationOid = InvalidOid;
+	HeapTuple existingTuple = NULL;
 	switch (change->action)
 	{
 		case REORDER_BUFFER_CHANGE_INSERT:
 		{
-			HeapTuple newTuple = &(change->data.tp.newtuple->tuple);
-			targetRelationOid = FindTargetRelationOid(relation, newTuple,
+			existingTuple = &(change->data.tp.newtuple->tuple);
+			targetRelationOid = FindTargetRelationOid(relation, existingTuple,
 													  replicationSlotName);
 			break;
 		}
@@ -100,16 +105,16 @@ split_change_cb(LogicalDecodingContext *ctx, ReorderBufferTXN *txn,
 		/* updating non-partition column value */
 		case REORDER_BUFFER_CHANGE_UPDATE:
 		{
-			HeapTuple newTuple = &(change->data.tp.newtuple->tuple);
-			targetRelationOid = FindTargetRelationOid(relation, newTuple,
+			existingTuple = &(change->data.tp.newtuple->tuple);
+			targetRelationOid = FindTargetRelationOid(relation, existingTuple,
 													  replicationSlotName);
 			break;
 		}
 
 		case REORDER_BUFFER_CHANGE_DELETE:
 		{
-			HeapTuple oldTuple = &(change->data.tp.oldtuple->tuple);
-			targetRelationOid = FindTargetRelationOid(relation, oldTuple,
+			existingTuple = &(change->data.tp.oldtuple->tuple);
+			targetRelationOid = FindTargetRelationOid(relation, existingTuple,
 													  replicationSlotName);
 
 			break;
@@ -129,6 +134,40 @@ split_change_cb(LogicalDecodingContext *ctx, ReorderBufferTXN *txn,
 	}
 
 	Relation targetRelation = RelationIdGetRelation(targetRelationOid);
+
+	/*
+	 * If any columns from source relation have been dropped, then the tuple needs to
+	 * be formatted according to the target relation.
+	 */
+	TupleDesc sourceRelationDesc = RelationGetDescr(relation);
+	TupleDesc targetRelationDesc = RelationGetDescr(targetRelation);
+	if (sourceRelationDesc->natts > targetRelationDesc->natts)
+	{
+		HeapTuple targetRelationTuple = GetTupleForTargetSchema(existingTuple,
+																sourceRelationDesc,
+																targetRelationDesc);
+		switch (change->action)
+		{
+			case REORDER_BUFFER_CHANGE_INSERT:
+			{
+				change->data.tp.newtuple->tuple = *targetRelationTuple;
+				break;
+			}
+
+			case REORDER_BUFFER_CHANGE_UPDATE:
+			{
+				change->data.tp.newtuple->tuple = *targetRelationTuple;
+				break;
+			}
+
+			case REORDER_BUFFER_CHANGE_DELETE:
+			{
+				change->data.tp.oldtuple->tuple = *targetRelationTuple;
+				break;
+			}
+		}
+	}
+
 	pgoutputChangeCB(ctx, txn, targetRelation, change);
 	RelationClose(targetRelation);
 }
@@ -223,3 +262,51 @@ GetHashValueForIncomingTuple(Relation sourceShardRelation,
 
 	return DatumGetInt32(hashedValueDatum);
 }
+
+
+/*
+ * GetTupleForTargetSchema returns a tuple with the schema of the target relation.
+ * If some columns within the source relations are dropped, we would have to reformat
+ * the tuple to match the schema of the target relation.
+ *
+ * Consider the below scenario:
+ * Session1 : Drop column followed by create_distributed_table_concurrently
+ * Session2 : Concurrent insert workload
+ *
+ * The child shards created by create_distributed_table_concurrently will have less columns
+ * than the source shard because some column were dropped.
+ * The incoming tuple from session2 will have more columns as the writes
+ * happened on source shard. But now the tuple needs to be applied on child shard. So we need to format
+ * it according to child schema.
+ */
+static HeapTuple
+GetTupleForTargetSchema(HeapTuple sourceRelationTuple,
+						TupleDesc sourceRelDesc,
+						TupleDesc targetRelDesc)
+{
+	/* Deform the tuple */
+	Datum *oldValues = (Datum *) palloc0(sourceRelDesc->natts * sizeof(Datum));
+	bool *oldNulls = (bool *) palloc0(sourceRelDesc->natts * sizeof(bool));
+	heap_deform_tuple(sourceRelationTuple, sourceRelDesc, oldValues,
+					  oldNulls);
+
+
+	/* Create new tuple by skipping dropped columns */
+	int nextAttributeIndex = 0;
+	Datum *newValues = (Datum *) palloc0(targetRelDesc->natts * sizeof(Datum));
+	bool *newNulls = (bool *) palloc0(targetRelDesc->natts * sizeof(bool));
+	for (int i = 0; i < sourceRelDesc->natts; i++)
+	{
+		if (TupleDescAttr(sourceRelDesc, i)->attisdropped)
+		{
+			continue;
+		}
+
+		newValues[nextAttributeIndex] = oldValues[i];
+		newNulls[nextAttributeIndex] = oldNulls[i];
+		nextAttributeIndex++;
+	}
+
+	HeapTuple targetRelationTuple = heap_form_tuple(targetRelDesc, newValues, newNulls);
+	return targetRelationTuple;
+}
diff --git a/src/test/regress/enterprise_isolation_logicalrep_1_schedule b/src/test/regress/enterprise_isolation_logicalrep_1_schedule
index 96cc9915e18..23ed9373947 100644
--- a/src/test/regress/enterprise_isolation_logicalrep_1_schedule
+++ b/src/test/regress/enterprise_isolation_logicalrep_1_schedule
@@ -8,5 +8,6 @@ test: isolation_cluster_management
 test: isolation_logical_replication_single_shard_commands
 test: isolation_logical_replication_multi_shard_commands
 test: isolation_non_blocking_shard_split
+test: isolation_create_distributed_concurrently_after_drop_column
 test: isolation_non_blocking_shard_split_with_index_as_replicaIdentity
 test: isolation_non_blocking_shard_split_fkey
diff --git a/src/test/regress/expected/isolation_create_distributed_concurrently_after_drop_column.out b/src/test/regress/expected/isolation_create_distributed_concurrently_after_drop_column.out
new file mode 100644
index 00000000000..b1056723816
--- /dev/null
+++ b/src/test/regress/expected/isolation_create_distributed_concurrently_after_drop_column.out
@@ -0,0 +1,95 @@
+Parsed test spec with 3 sessions
+
+starting permutation: s2-print-cluster s3-acquire-advisory-lock s2-begin s1-alter-table s1-set-factor-1 s1-create-distributed-table-concurrently s2-insert s2-end s2-print-cluster s3-release-advisory-lock s2-print-cluster
+step s2-print-cluster:
+ -- row count per shard
+ SELECT
+  nodeport, shardid, success, result
+ FROM
+  run_command_on_placements('observations', 'select count(*) from %s')
+ ORDER BY
+  nodeport, shardid;
+
+nodeport|shardid|success|result
+---------------------------------------------------------------------
+(0 rows)
+
+step s3-acquire-advisory-lock:
+    SELECT pg_advisory_lock(44000, 55152);
+
+pg_advisory_lock
+---------------------------------------------------------------------
+
+(1 row)
+
+step s2-begin:
+    BEGIN;
+
+step s1-alter-table:
+ ALTER TABLE observations DROP COLUMN dummy;
+
+step s1-set-factor-1:
+ SET citus.shard_replication_factor TO 1;
+ SELECT citus_set_coordinator_host('localhost');
+
+citus_set_coordinator_host
+---------------------------------------------------------------------
+
+(1 row)
+
+step s1-create-distributed-table-concurrently:
+ SELECT create_distributed_table_concurrently('observations','tenant_id');
+ <waiting ...>
+step s2-insert: 
+ INSERT INTO observations(tenant_id, payload) SELECT 'tenant_id', jsonb_build_object('name', 29.3);
+ INSERT INTO observations(tenant_id, payload) SELECT 'tenant_id', jsonb_build_object('name', 29.3);
+ INSERT INTO observations(tenant_id, payload) SELECT 'tenant_id', jsonb_build_object('name', 29.3);
+
+step s2-end:
+   COMMIT;
+
+step s2-print-cluster:
+ -- row count per shard
+ SELECT
+  nodeport, shardid, success, result
+ FROM
+  run_command_on_placements('observations', 'select count(*) from %s')
+ ORDER BY
+  nodeport, shardid;
+
+nodeport|shardid|success|result
+---------------------------------------------------------------------
+   57636|1500004|t      |     3
+(1 row)
+
+step s3-release-advisory-lock:
+    SELECT pg_advisory_unlock(44000, 55152);
+
+pg_advisory_unlock
+---------------------------------------------------------------------
+t
+(1 row)
+
+step s1-create-distributed-table-concurrently: <... completed>
+create_distributed_table_concurrently
+---------------------------------------------------------------------
+
+(1 row)
+
+step s2-print-cluster:
+ -- row count per shard
+ SELECT
+  nodeport, shardid, success, result
+ FROM
+  run_command_on_placements('observations', 'select count(*) from %s')
+ ORDER BY
+  nodeport, shardid;
+
+nodeport|shardid|success|result
+---------------------------------------------------------------------
+   57637|1500006|t      |     3
+   57637|1500008|t      |     0
+   57638|1500005|t      |     0
+   57638|1500007|t      |     0
+(4 rows)
+
diff --git a/src/test/regress/spec/isolation_create_distributed_concurrently_after_drop_column.spec b/src/test/regress/spec/isolation_create_distributed_concurrently_after_drop_column.spec
new file mode 100644
index 00000000000..76ffbc0b27d
--- /dev/null
+++ b/src/test/regress/spec/isolation_create_distributed_concurrently_after_drop_column.spec
@@ -0,0 +1,95 @@
+#include "isolation_mx_common.include.spec"
+
+// Test scenario for nonblocking split and concurrent INSERT/UPDATE/DELETE
+//  session s1 - Executes create_distributed_table_concurrently after dropping a column
+//  session s2 - Does concurrent inserts
+//  session s3 - Holds advisory locks
+
+setup
+{
+	SET citus.shard_replication_factor TO 1;
+	CREATE TABLE observations (
+  	tenant_id text not null,
+  	dummy int,
+  	measurement_id bigserial not null,
+  	payload jsonb not null,
+  	observation_time timestamptz not null default now(),
+  	PRIMARY KEY (tenant_id, measurement_id)
+	);
+}
+
+teardown
+{
+    DROP TABLE observations;
+}
+
+session "s1"
+
+step "s1-alter-table"
+{
+	ALTER TABLE observations DROP COLUMN dummy;
+}
+
+step "s1-set-factor-1"
+{
+	SET citus.shard_replication_factor TO 1;
+	SELECT citus_set_coordinator_host('localhost');
+}
+
+step "s1-create-distributed-table-concurrently"
+{
+	SELECT create_distributed_table_concurrently('observations','tenant_id');
+}
+
+session "s2"
+
+step "s2-begin"
+{
+    BEGIN;
+}
+
+step "s2-insert"
+{
+	INSERT INTO observations(tenant_id, payload) SELECT 'tenant_id', jsonb_build_object('name', 29.3);
+	INSERT INTO observations(tenant_id, payload) SELECT 'tenant_id', jsonb_build_object('name', 29.3);
+	INSERT INTO observations(tenant_id, payload) SELECT 'tenant_id', jsonb_build_object('name', 29.3);
+
+}
+
+step "s2-end"
+{
+	  COMMIT;
+}
+
+step "s2-print-cluster"
+{
+	-- row count per shard
+	SELECT
+		nodeport, shardid, success, result
+	FROM
+		run_command_on_placements('observations', 'select count(*) from %s')
+	ORDER BY
+		nodeport, shardid;
+}
+
+
+session "s3"
+
+// this advisory lock with (almost) random values are only used
+// for testing purposes. For details, check Citus' logical replication
+// source code
+step "s3-acquire-advisory-lock"
+{
+    SELECT pg_advisory_lock(44000, 55152);
+}
+
+step "s3-release-advisory-lock"
+{
+    SELECT pg_advisory_unlock(44000, 55152);
+}
+
+// Concurrent Insert with create_distributed_table_concurrently after dropping a column:
+// s3 holds advisory lock -> s1 starts create_distributed_table_concurrently and waits for advisory lock ->
+// s2 concurrently inserts rows -> s3 releases the advisory lock
+// -> s1 complete create_distributed_table_concurrently -> result is reflected in new shards
+permutation "s2-print-cluster" "s3-acquire-advisory-lock" "s2-begin" "s1-alter-table" "s1-set-factor-1" "s1-create-distributed-table-concurrently" "s2-insert" "s2-end" "s2-print-cluster" "s3-release-advisory-lock" "s2-print-cluster"
