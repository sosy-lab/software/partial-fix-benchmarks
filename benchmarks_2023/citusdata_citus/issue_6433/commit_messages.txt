Add missing space to citus.shard_count description (#6464)

DESCRIPTION: Add missing space to citus.shard_count description
Consider all clusters when searching node by id

citus_update_node(), citus_nodename_for_nodeid(), and
citus_nodeport_for_nodeid() functions only checked for nodes in their
own clusters.

Fixes https://github.com/citusdata/citus/issues/6433
Allow citus_update_node() to work with nodes from different clusters

DESCRIPTION: Allow citus_update_node() to work with nodes from different clusters

citus_update_node(), citus_nodename_for_nodeid(), and citus_nodeport_for_nodeid() functions only checked for nodes in their own clusters and hence last two returned NULLs and the first one showed an error is the nodeId was from a different cluster.

Fixes https://github.com/citusdata/citus/issues/6433
Allow citus_update_node() to work with nodes from different clusters (#6466)

DESCRIPTION: Allow citus_update_node() to work with nodes from different clusters

citus_update_node(), citus_nodename_for_nodeid(), and citus_nodeport_for_nodeid() functions only checked for nodes in their own clusters and hence last two returned NULLs and the first one showed an error is the nodeId was from a different cluster.

Fixes https://github.com/citusdata/citus/issues/6433
