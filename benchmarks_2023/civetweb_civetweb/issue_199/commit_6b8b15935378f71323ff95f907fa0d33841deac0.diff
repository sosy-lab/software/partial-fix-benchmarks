diff --git a/LICENSE.md b/LICENSE.md
index be6ae19b9..ab049e3e7 100644
--- a/LICENSE.md
+++ b/LICENSE.md
@@ -11,7 +11,7 @@ Civetweb License
 
 ### Included with all features.
 
-> Copyright (c) 2013-2017 The CivetWeb developers ([CREDITS.md](https://github.com/civetweb/civetweb/blob/master/CREDITS.md))
+> Copyright (c) 2013-2018 The CivetWeb developers ([CREDITS.md](https://github.com/civetweb/civetweb/blob/master/CREDITS.md))
 >
 > Copyright (c) 2004-2013 Sergey Lyubka
 >
@@ -73,7 +73,7 @@ SQLite3 License
 
 http://www.sqlite.org/copyright.html
 
-> 2001 September 15
+> 2001-09-15
 >
 > The author disclaims copyright to this source code.  In place of
 > a legal notice, here is a blessing:
@@ -88,11 +88,11 @@ lsqlite3 License
 
 ### Included only if built with Lua and SQLite support.
 
-> Copyright (C) 2002-2013 Tiago Dionizio, Doug Currie
+> Copyright (C) 2002-2016 Tiago Dionizio, Doug Currie
 > All rights reserved.
 > Author    : Tiago Dionizio <tiago.dionizio@ist.utl.pt>
 > Author    : Doug Currie <doug.currie@alum.mit.edu>
-> Library   : lsqlite3 - a SQLite 3 database binding for Lua 5
+> Library   : lsqlite3 - an SQLite 3 database binding for Lua 5
 >
 > Permission is hereby granted, free of charge, to any person obtaining a copy
 > of this software and associated documentation files (the "Software"), to deal
@@ -120,7 +120,7 @@ Lua File System License
 
 http://keplerproject.github.io/luafilesystem/license.html
 
-> Copyright © 2003 Kepler Project.
+> Copyright Kepler Project 2003 (http://www.keplerproject.org/luafilesystem)
 >
 > Permission is hereby granted, free of charge, to any person obtaining a copy
 > of this software and associated documentation files (the "Software"), to deal
@@ -186,7 +186,7 @@ https://github.com/svaarala/duktape/blob/master/LICENSE.txt
 > 
 > (http://opensource.org/licenses/MIT)
 > 
-> Copyright (c) 2013-2015 by Duktape authors (see AUTHORS.rst)
+> Copyright (c) 2013-2017 by Duktape authors (see AUTHORS.rst)
 >
 > Permission is hereby granted, free of charge, to any person obtaining a copy
 > of this software and associated documentation files (the "Software"), to deal
@@ -206,3 +206,36 @@ https://github.com/svaarala/duktape/blob/master/LICENSE.txt
 > OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 > THE SOFTWARE.
 
+
+
+zlib License
+------
+
+### Included only if built with zlib support.
+
+https://www.zlib.net/zlib_license.html
+
+> zlib.h -- interface of the 'zlib' general purpose compression library
+> version 1.2.11, January 15th, 2017
+>
+> Copyright (C) 1995-2017 Jean-loup Gailly and Mark Adler
+>
+> This software is provided 'as-is', without any express or implied
+> warranty.  In no event will the authors be held liable for any damages
+> arising from the use of this software.
+>
+> Permission is granted to anyone to use this software for any purpose,
+> including commercial applications, and to alter it and redistribute it
+> freely, subject to the following restrictions:
+>
+> 1. The origin of this software must not be misrepresented; you must not
+>    claim that you wrote the original software. If you use this software
+>    in a product, an acknowledgment in the product documentation would be
+>    appreciated but is not required.
+> 2. Altered source versions must be plainly marked as such, and must not be
+>    misrepresented as being the original software.
+> 3. This notice may not be removed or altered from any source distribution.
+>
+> Jean-loup Gailly        Mark Adler
+> jloup@gzip.org          madler@alumni.caltech.edu
+
diff --git a/Makefile b/Makefile
index b1dca543e..ce1848e24 100644
--- a/Makefile
+++ b/Makefile
@@ -83,6 +83,7 @@ ifdef WITH_ALL
   WITH_LUA = 1
   WITH_DUKTAPE = 1
   WITH_SERVER_STATS = 1
+  WITH_ZLIB = 1
   WITH_EXPERIMENTAL = 1
   #WITH_CPP is not defined, ALL means only real features, not wrappers
 endif
@@ -113,9 +114,16 @@ ifdef WITH_DUKTAPE
   include resources/Makefile.in-duktape
 endif
 
+ifdef WITH_COMPRESSION
+  WITH_ZLIB = 1
+endif
+
+ifdef WITH_ZLIB
+  LIBS += -lz
+endif
+
 ifdef WITH_EXPERIMENTAL
   CFLAGS += -DMG_EXPERIMENTAL_INTERFACES
-  LIBS += -lz
 endif
 
 ifdef WITH_IPV6
@@ -206,6 +214,7 @@ help:
 	@echo "   WITH_IPV6=1           with IPV6 support"
 	@echo "   WITH_WEBSOCKET=1      build with web socket support"
 	@echo "   WITH_SERVER_STATS=1   build includes support for server statistics"
+	@echo "   WITH_ZLIB=1           build includes support for on-the-fly compression using zlib"
 	@echo "   WITH_CPP=1            build library with c++ classes"
 	@echo "   WITH_EXPERIMENTAL=1   build with experimental features"
 	@echo "   CONFIG_FILE=file      use 'file' as the config file"
diff --git a/Qt/CivetWeb.pro b/Qt/CivetWeb.pro
index 679806b92..215cf6239 100644
--- a/Qt/CivetWeb.pro
+++ b/Qt/CivetWeb.pro
@@ -48,6 +48,9 @@ INCLUDEPATH +=  \
 DEFINES += USE_LUA
 DEFINES += USE_LUA_SHARED
 LIBS += -llua5.2
+
+DEFINES += USE_ZLIB
+LIBS += -lz
 }
 
 #To build with duktape support:
diff --git a/docs/Building.md b/docs/Building.md
index 1282d3d77..bb7740f8f 100644
--- a/docs/Building.md
+++ b/docs/Building.md
@@ -162,6 +162,8 @@ make build COPT="-DNDEBUG -DNO_CGI"
 | USE_DUKTAPE                | enable server-side JavaScript        |
 |                            | support by including Duktape         |
 | USE_SERVER_STATS           | enable server statistics support     |
+| USE_ZLIB                   | enable on-the-fly compression of     |
+|                            | static content by including zlib     |
 | MG_EXPERIMENTAL_INTERFACES | include experimental interfaces      |
 |                            |                                      |
 | NO_SSL_DL                  | link against system libssl library   |
diff --git a/include/civetweb.h b/include/civetweb.h
index 017101339..843c0ec8a 100644
--- a/include/civetweb.h
+++ b/include/civetweb.h
@@ -94,6 +94,10 @@ enum {
 	/* Will only work, if USE_SERVER_STATS is set. */
 	MG_FEATURES_STATS = 0x100u,
 
+	/* Support on-the-fly compression. */
+	/* Will only work, if USE_ZLIB is set. */
+	MG_FEATURES_COMPRESSION = 0x200u,
+
 	/* Collect server status information. */
 	/* Will only work, if USE_SERVER_STATS is set. */
 	MG_FEATURES_ALL = 0xFFFFu
diff --git a/resources/check_defines.lua b/resources/check_defines.lua
index 7ec4aab86..623f7c4fc 100644
--- a/resources/check_defines.lua
+++ b/resources/check_defines.lua
@@ -1,4 +1,4 @@
-
+#!/usr/bin/lua5.2
 
 usedlines = {c={}, n={}}
 useddefs = {c={}, n={}}
@@ -66,6 +66,7 @@ noifdef(path .. "src/main.c")
 noifdef(path .. "src/md5.inl")
 noifdef(path .. "src/mod_duktape.inl")
 noifdef(path .. "src/mod_lua.inl")
+noifdef(path .. "src/mod_zlib.inl")
 noifdef(path .. "src/sha1.inl")
 noifdef(path .. "src/timer.inl")
 noifdef(path .. "src/wolfssl_extras.inl")
diff --git a/src/civetweb.c b/src/civetweb.c
index c7a2b2468..54fe8d941 100644
--- a/src/civetweb.c
+++ b/src/civetweb.c
@@ -26,6 +26,9 @@
 #pragma GCC diagnostic ignored "-Wunused-macros"
 /* A padding warning is just plain useless */
 #pragma GCC diagnostic ignored "-Wpadded"
+#endif
+
+#if defined(__clang__) /* GCC does not (yet) support this pragma */
 /* We must set some flags for the headers we include. These flags
  * are reserved ids according to C99, so we need to disable a
  * warning for that. */
@@ -68,7 +71,7 @@
 #endif
 #endif
 
-#if defined(__GNUC__) || defined(__MINGW32__)
+#if defined(__clang__)
 /* Enable reserved-id-macro warning again. */
 #pragma GCC diagnostic pop
 #endif
@@ -9212,7 +9215,7 @@ fclose_on_exec(struct mg_file_access *filep, struct mg_connection *conn)
 }
 
 
-#if defined(MG_EXPERIMENTAL_INTERFACES) /* TODO: A new define */
+#if defined(USE_ZLIB)
 #include "mod_zlib.inl"
 #endif
 
@@ -9234,17 +9237,17 @@ handle_static_file_request(struct mg_connection *conn,
 	char gz_path[PATH_MAX];
 	const char *encoding = "";
 	const char *cors1, *cors2, *cors3;
-#if defined(MG_EXPERIMENTAL_INTERFACES)   /* TODO: A new define */
+	int is_head_request;
+#if defined(USE_ZLIB)
 	int allow_on_the_fly_compression = 1; /* TODO: get from config */
-#else
-	int allow_on_the_fly_compression = 0; /* TODO: get from config */
 #endif
-	int is_head_request = !strcmp(conn->request_info.request_method, "HEAD");
 
 	if ((conn == NULL) || (conn->dom_ctx == NULL) || (filep == NULL)) {
 		return;
 	}
 
+	is_head_request = !strcmp(conn->request_info.request_method, "HEAD");
+
 	if (mime_type == NULL) {
 		get_mime_type(conn, path, &mime_vec);
 	} else {
@@ -9262,12 +9265,14 @@ handle_static_file_request(struct mg_connection *conn,
 	conn->status_code = 200;
 	range[0] = '\0';
 
+#if defined(USE_ZLIB)
 	/* if this file is in fact a pre-gzipped file, rewrite its filename
 	 * it's important to rewrite the filename after resolving
 	 * the mime type from it, to preserve the actual file's type */
 	if (!conn->accept_gzip) {
 		allow_on_the_fly_compression = 0;
 	}
+#endif
 
 	if (filep->stat.is_gzipped) {
 		mg_snprintf(conn, &truncated, gz_path, sizeof(gz_path), "%s.gz", path);
@@ -9283,8 +9288,10 @@ handle_static_file_request(struct mg_connection *conn,
 		path = gz_path;
 		encoding = "Content-Encoding: gzip\r\n";
 
+#if defined(USE_ZLIB)
 		/* File is already compressed. No "on the fly" compression. */
 		allow_on_the_fly_compression = 0;
+#endif
 	}
 
 	if (!mg_fopen(conn, path, MG_FOPEN_MODE_READ, filep)) {
@@ -9328,8 +9335,10 @@ handle_static_file_request(struct mg_connection *conn,
 		            filep->stat.size);
 		msg = "Partial Content";
 
+#if defined(USE_ZLIB)
 		/* Do not compress ranges. */
 		allow_on_the_fly_compression = 0;
+#endif
 	}
 
 	hdr = mg_get_header(conn, "Origin");
@@ -9376,6 +9385,7 @@ handle_static_file_request(struct mg_connection *conn,
 	send_static_cache_header(conn);
 	send_additional_header(conn);
 
+#if defined(USE_ZLIB)
 	/* On the fly compression allowed */
 	if (allow_on_the_fly_compression) {
 		/* For on the fly compression, we don't know the content size in
@@ -9383,7 +9393,9 @@ handle_static_file_request(struct mg_connection *conn,
 		(void)mg_printf(conn,
 		                "Content-Encoding: gzip\r\n"
 		                "Transfer-Encoding: chunked\r\n");
-	} else {
+	} else
+#endif
+	{
 		/* Without on-the-fly compression, we know the content-length
 		 * and we can use ranges (with on-the-fly compression we cannot).
 		 * So we send these response headers only in this case. */
@@ -9409,10 +9421,13 @@ handle_static_file_request(struct mg_connection *conn,
 	}
 
 	if (!is_head_request) {
+#if defined(USE_ZLIB)
 		if (allow_on_the_fly_compression) {
 			/* Compress and send */
 			send_compressed_data(conn, filep);
-		} else {
+		} else
+#endif
+		{
 			/* Send file directly */
 			send_file_data(conn, filep, r1, cl);
 		}
@@ -18204,26 +18219,29 @@ mg_check_feature(unsigned feature)
 #if defined(USE_SERVER_STATS)
 	                                    | MG_FEATURES_STATS
 #endif
+#if defined(USE_ZLIB)
+	                                    | MG_FEATURES_COMPRESSION
+#endif
 
 /* Set some extra bits not defined in the API documentation.
  * These bits may change without further notice. */
 #if defined(MG_LEGACY_INTERFACE)
-	                                    | 0x8000u
+	                                    | 0x00008000u
 #endif
 #if defined(MG_EXPERIMENTAL_INTERFACES)
-	                                    | 0x4000u
+	                                    | 0x00004000u
 #endif
 #if defined(MEMORY_DEBUGGING)
-	                                    | 0x1000u
+	                                    | 0x00001000u
 #endif
 #if defined(USE_TIMERS)
-	                                    | 0x0200u
+	                                    | 0x00020000u
 #endif
 #if !defined(NO_NONCE_CHECK)
-	                                    | 0x0400u
+	                                    | 0x00040000u
 #endif
 #if !defined(NO_POPEN)
-	                                    | 0x0800u
+	                                    | 0x00080000u
 #endif
 	    ;
 	return (feature & feature_set);
diff --git a/src/mod_zlib.inl b/src/mod_zlib.inl
index 35a8f1bed..f2216d316 100644
--- a/src/mod_zlib.inl
+++ b/src/mod_zlib.inl
@@ -1,4 +1,8 @@
 /* Experimental implementation for on-the-fly compression */
+#if !defined(USE_ZLIB)
+#error "This file must only be included, if USE_ZLIB is set"
+#endif
+
 #include "zconf.h"
 #include "zlib.h"
 
