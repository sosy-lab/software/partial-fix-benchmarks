int32_t is a long int
Ah yeah, thanks. Should be fixed now!
The correct way of doing this is with the PRId32 macro. On LP64 platforms, coercing to long is inefficient.
Ah yeah, good call. Thanks @davidchisnall!