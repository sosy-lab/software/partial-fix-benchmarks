vmem plugin: Fix pgsteal for newer Linux kernels.

Fixes: #1307
df plugin: skip duplicate entries, fixes "uc_update: Value too old" error

Issue: #1402
df plugin: Fix the duplicate detection.

Not that multiple devices could be mounted at the same mount point ... I think.

Fixes: #1402
df plugin: Fix the duplicate detection.

Not that multiple devices could be mounted at the same mount point ... I think.

Fixes: #1402
df plugin: skip duplicate entries, fixes "uc_update: Value too old" error

Issue: #1402
df plugin: Fix the duplicate detection.

Not that multiple devices could be mounted at the same mount point ... I think.

Fixes: #1402
df: remove legacy code skipping "rootfs" monitoring

3512bb1 added code to skip duplicate reporting of `rootfs` mounted on
`/`. f9c1c5b and f0398d0 added generic code to skip any filesystem
mounted twice.

Depending on the order of the entries in `/etc/mtab`, reporting for `/` was
entirely skipped.

This patch basically reverts the first, non-generic patch, as it's
superseded by the 2 others.

Fixes #1402
df: remove legacy code skipping "rootfs" monitoring

3512bb1 added code to skip duplicate reporting of `rootfs` mounted on `/`.
f9c1c5b and f0398d0 added generic code to skip any volume mounted twice.

Depending on the order of the entries in `/etc/mtab`, reporting for `/` was
entirely skipped.

This patch basically reverts the first, non-generic patch, as it's
superseded by the 2 others.

Fixes #1402
df: remove legacy code skipping "rootfs" monitoring

3512bb1 added code to skip duplicate reporting of `rootfs` mounted on `/`.
f9c1c5b and f0398d0 added generic code to skip any volume mounted twice.

Depending on the order of the entries in `/etc/mtab`, reporting for `/` was
entirely skipped.

This patch basically reverts the first, non-generic patch, as it's
superseded by the 2 others.

Fixes #1402

Signed-off-by: Florian Forster <octo@collectd.org>
df: remove legacy code skipping "rootfs" monitoring

3512bb1 added code to skip duplicate reporting of `rootfs` mounted on `/`.
f9c1c5b and f0398d0 added generic code to skip any volume mounted twice.

Depending on the order of the entries in `/etc/mtab`, reporting for `/` was
entirely skipped.

This patch basically reverts the first, non-generic patch, as it's
superseded by the 2 others.

Fixes #1402

Signed-off-by: Florian Forster <octo@collectd.org>
df: remove legacy code skipping "rootfs" monitoring

3512bb1 added code to skip duplicate reporting of `rootfs` mounted on `/`.
f9c1c5b and f0398d0 added generic code to skip any volume mounted twice.

Depending on the order of the entries in `/etc/mtab`, reporting for `/` was
entirely skipped.

This patch basically reverts the first, non-generic patch, as it's
superseded by the 2 others.

Fixes #1402

Signed-off-by: Florian Forster <octo@collectd.org>
