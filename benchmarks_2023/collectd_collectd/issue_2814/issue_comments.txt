snmp_agent: CI issues - Solaris platform build broken, Coverity Scan reports issues 
Hi,
it will be fixed soon
This still has issue:
```src/snmp_agent.c: In function ‘snmp_agent_fill_index_list’:
src/snmp_agent.c:534:23: error: initialization makes pointer from integer without a cast [-Werror=int-conversion]
       regmatch_t m = {-1, -1};
                       ^
src/snmp_agent.c:534:23: note: (near initialization for ‘m.rm_sp’)
src/snmp_agent.c:534:27: error: initialization makes pointer from integer without a cast [-Werror=int-conversion]
       regmatch_t m = {-1, -1};
                           ^
src/snmp_agent.c:534:27: note: (near initialization for ‘m.rm_ep’)
```
cc @dago

Dagobert, can you please help with this issue?

We have success `make check` on solaris-i386, but some checks fails on solaris-sparc.

Output example: https://buildfarm.opencsw.org/buildbot/builders/collectd-solaris10-sparc/builds/1832/steps/shell_3/logs/stdio
 
Sure. I first updated the location of `test-suite.log`, which seems to have been changed. The failure log is now again correctly grabbed from buildbot: https://buildfarm.opencsw.org/buildbot_admin/builders/collectd-solaris10-sparc/builds/1837/steps/shell_3/logs/test-suite.log
There seems to be only one test failing in `test_plugin_snmp_agent`, however there is a `FAIL: 2` reported, is the check run twice?

The failure is happening in the CPU numbering:
`
not ok 46 - name = "example.com/test_plugin-test_plugin_inst/test_type-vcpu_0-cpu_0", want "example.com/test_plugin-test_plugin_inst/test_type-vcpu_1-cpu_10"
`

Does this ring a bell? I am not too familiar with the test environment of CollectD, if you have a pointer where I should look deeper please let me know.
>  I first updated the location of test-suite.log, which seems to have been changed. 

I noticed that too. Seems to depend on the version of automake.
It seems what `collectd-solaris10-sparc` is proceeded twice per CI check.

https://buildfarm.opencsw.org/buildbot/builders/collectd-solaris10-sparc/builds/1839
https://buildfarm.opencsw.org/buildbot/builders/collectd-solaris10-sparc/builds/1840

I see first completed successfully, but now the second is in progress for same revision and change.
I think we need only one such check. @dago ?

It looks like the GitHub Hook triggered two schedulers in my Buildbot configuration. I reorganized the schedulers and hopefully the issue is fixed, let's keep an eye on it when new commits come in.
Thanks, Dagobert!