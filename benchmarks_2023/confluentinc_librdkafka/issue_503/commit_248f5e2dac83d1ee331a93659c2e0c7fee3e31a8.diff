diff --git a/examples/rdkafka_example.c b/examples/rdkafka_example.c
index fbdcb6576..3f0888382 100644
--- a/examples/rdkafka_example.c
+++ b/examples/rdkafka_example.c
@@ -632,9 +632,10 @@ int main (int argc, char **argv) {
 
 			/* Only query for hi&lo partition watermarks */
 
-			if ((err = rd_kafka_get_offsets(rk, topic, partition,
-							&lo, &hi, 5000))) {
-				fprintf(stderr, "%% get_offsets() failed: %s\n",
+			if ((err = rd_kafka_query_watermark_offsets(
+				     rk, topic, partition, &lo, &hi, 5000))) {
+				fprintf(stderr, "%% query_watermark_offsets() "
+					"failed: %s\n",
 					rd_kafka_err2str(err));
 				exit(1);
 			}
diff --git a/src-cpp/rdkafkacpp.h b/src-cpp/rdkafkacpp.h
index b1178fc5d..23ba63b5d 100644
--- a/src-cpp/rdkafkacpp.h
+++ b/src-cpp/rdkafkacpp.h
@@ -873,16 +873,38 @@ class RD_EXPORT Handle {
 
 
   /**
-   * @brief Get low (oldest/beginning) and high (newest/end) offsets for
-   *        partition.
+   * @brief Query broker for low (oldest/beginning)
+   *        and high (newest/end) offsets for partition.
    *
    * Offsets are returned in \p *low and \p *high respectively.
    *
    * @returns RdKafka::ERR_NO_ERROR on success or an error code on failure.
    */
-  virtual ErrorCode get_offsets (const std::string &topic, int32_t partition,
-				 int64_t *low, int64_t *high,
-				 int timeout_ms) = 0;
+  virtual ErrorCode query_watermark_offsets (const std::string &topic,
+					     int32_t partition,
+					     int64_t *low, int64_t *high,
+					     int timeout_ms) = 0;
+
+  /**
+   * @brief Get last known low (oldest/beginning)
+   *        and high (newest/end) offsets for partition.
+   *
+   * The low offset is updated periodically (if statistics.interval.ms is set)
+   * while the high offset is updated on each fetched message set from the
+   * broker.
+   *
+   * If there is no cached offset (either low or high, or both) then
+   * OFFSET_INVALID will be returned for the respective offset.
+   *
+   * Offsets are returned in \p *low and \p *high respectively.
+   *
+   * @returns RdKafka::ERR_NO_ERROR on success or an error code on failure.
+   *
+   * @remark Shall only be used with an active consumer instance.
+   */
+  virtual ErrorCode get_watermark_offsets (const std::string &topic,
+					   int32_t partition,
+					   int64_t *low, int64_t *high) = 0;
 };
 
 
diff --git a/src-cpp/rdkafkacpp_int.h b/src-cpp/rdkafkacpp_int.h
index e91202eba..34272d2d2 100644
--- a/src-cpp/rdkafkacpp_int.h
+++ b/src-cpp/rdkafkacpp_int.h
@@ -449,12 +449,23 @@ class HandleImpl : virtual public Handle {
   ErrorCode pause (std::vector<TopicPartition*> &partitions);
   ErrorCode resume (std::vector<TopicPartition*> &partitions);
 
-  ErrorCode get_offsets (const std::string &topic, int32_t partition,
-			 int64_t *low, int64_t *high, int timeout_ms) {
-	  return static_cast<RdKafka::ErrorCode>(
-		  rd_kafka_get_offsets(
-			  rk_, topic.c_str(), partition,
-			  low, high, timeout_ms));
+  ErrorCode query_watermark_offsets (const std::string &topic,
+				     int32_t partition,
+				     int64_t *low, int64_t *high,
+				     int timeout_ms) {
+    return static_cast<RdKafka::ErrorCode>(
+        rd_kafka_query_watermark_offsets(
+            rk_, topic.c_str(), partition,
+            low, high, timeout_ms));
+  }
+
+  ErrorCode get_watermark_offsets (const std::string &topic,
+                                   int32_t partition,
+                                   int64_t *low, int64_t *high) {
+    return static_cast<RdKafka::ErrorCode>(
+        rd_kafka_get_watermark_offsets(
+            rk_, topic.c_str(), partition,
+            low, high));
   }
 
 
diff --git a/src/rdkafka.c b/src/rdkafka.c
index d40d565ad..c903373a9 100644
--- a/src/rdkafka.c
+++ b/src/rdkafka.c
@@ -678,7 +678,7 @@ static __inline void rd_kafka_stats_emit_toppar (char **bufp, size_t *sizep,
 		   rktp->rktp_committed_offset,
                    offs.eof_offset,
 		   rktp->rktp_lo_offset,
-		   offs.hi_offset,
+		   rktp->rktp_hi_offset,
                    consumer_lag,
                    rd_atomic64_get(&rktp->rktp_c.tx_msgs),
 		   rd_atomic64_get(&rktp->rktp_c.tx_bytes),
@@ -1750,7 +1750,7 @@ rd_kafka_position (rd_kafka_t *rk,
 
 
 
-struct _get_offsets_state {
+struct _query_wmark_offsets_state {
 	rd_kafka_resp_err_t err;
 	const char *topic;
 	int32_t partition;
@@ -1759,13 +1759,13 @@ struct _get_offsets_state {
 	rd_ts_t ts_end;
 };
 
-static void rd_kafka_get_offsets_resp_cb (rd_kafka_t *rk,
-					  rd_kafka_broker_t *rkb,
-					  rd_kafka_resp_err_t err,
-					  rd_kafka_buf_t *rkbuf,
-					  rd_kafka_buf_t *request,
-					  void *opaque) {
-	struct _get_offsets_state *state = opaque;
+static void rd_kafka_query_wmark_offsets_resp_cb (rd_kafka_t *rk,
+						  rd_kafka_broker_t *rkb,
+						  rd_kafka_resp_err_t err,
+						  rd_kafka_buf_t *rkbuf,
+						  rd_kafka_buf_t *request,
+						  void *opaque) {
+	struct _query_wmark_offsets_state *state = opaque;
 
 	err = rd_kafka_handle_Offset(rk, rkb, err, rkbuf, request,
 				     state->topic, state->partition,
@@ -1792,11 +1792,12 @@ static void rd_kafka_get_offsets_resp_cb (rd_kafka_t *rk,
 
 
 rd_kafka_resp_err_t
-rd_kafka_get_offsets (rd_kafka_t *rk, const char *topic, int32_t partition,
-		      int64_t *low, int64_t *high, int timeout_ms) {
+rd_kafka_query_watermark_offsets (rd_kafka_t *rk, const char *topic,
+				  int32_t partition,
+				  int64_t *low, int64_t *high, int timeout_ms) {
 	rd_kafka_broker_t *rkb;
 	rd_kafka_q_t *replyq;
-	struct _get_offsets_state state;
+	struct _query_wmark_offsets_state state;
 	shptr_rd_kafka_toppar_t *s_rktp;
 	rd_kafka_toppar_t *rktp;
 	rd_ts_t ts_end = rd_clock() +
@@ -1805,7 +1806,7 @@ rd_kafka_get_offsets (rd_kafka_t *rk, const char *topic, int32_t partition,
 	/* Look up toppar so we know which broker to query. */
 	s_rktp = rd_kafka_toppar_get2(rk, topic, partition, 0, 1);
 	if (!s_rktp)
-		return RD_KAFKA_RESP_ERR__INVALID_ARG;
+		return RD_KAFKA_RESP_ERR__UNKNOWN_PARTITION;
 	rktp = rd_kafka_toppar_s2i(s_rktp);
 
 	/* Get toppar's leader broker. */
@@ -1834,7 +1835,8 @@ rd_kafka_get_offsets (rd_kafka_t *rk, const char *topic, int32_t partition,
 	state.ts_end = ts_end;
 
 	rd_kafka_OffsetRequest(rkb, topic, partition, state.offsets, state.cnt,
-			       replyq, rd_kafka_get_offsets_resp_cb, &state);
+			       replyq, rd_kafka_query_wmark_offsets_resp_cb,
+			       &state);
         rd_kafka_broker_destroy(rkb);
 
         /* Wait for reply (or timeout) */
@@ -1867,6 +1869,29 @@ rd_kafka_get_offsets (rd_kafka_t *rk, const char *topic, int32_t partition,
 }
 
 
+rd_kafka_resp_err_t
+rd_kafka_get_watermark_offsets (rd_kafka_t *rk, const char *topic,
+				int32_t partition,
+				int64_t *low, int64_t *high) {
+	shptr_rd_kafka_toppar_t *s_rktp;
+	rd_kafka_toppar_t *rktp;
+
+	s_rktp = rd_kafka_toppar_get2(rk, topic, partition, 0, 1);
+	if (!s_rktp)
+		return RD_KAFKA_RESP_ERR__UNKNOWN_PARTITION;
+	rktp = rd_kafka_toppar_s2i(s_rktp);
+
+	rd_kafka_toppar_lock(rktp);
+	*low = rktp->rktp_lo_offset;
+	*high = rktp->rktp_hi_offset;
+	rd_kafka_toppar_unlock(rktp);
+
+	rd_kafka_toppar_destroy(s_rktp);
+
+	return RD_KAFKA_RESP_ERR_NO_ERROR;
+}
+
+
 /**
  * rd_kafka_poll() (and similar) op callback handler.
  * Will either call registered callback depending on cb_type and op type
diff --git a/src/rdkafka.h b/src/rdkafka.h
index 35d0ebf29..fa08094a7 100644
--- a/src/rdkafka.h
+++ b/src/rdkafka.h
@@ -1397,7 +1397,7 @@ rd_kafka_resume_partitions (rd_kafka_t *rk,
 
 
 /**
- * @brief Get low (oldest/beginning) and high (newest/end) offsets
+ * @brief Query broker for low (oldest/beginning) and high (newest/end) offsets
  *        for partition.
  *
  * Offsets are returned in \p *low and \p *high respectively.
@@ -1405,11 +1405,33 @@ rd_kafka_resume_partitions (rd_kafka_t *rk,
  * @returns RD_KAFKA_RESP_ERR_NO_ERROR on success or an error code on failure.
  */
 RD_EXPORT rd_kafka_resp_err_t
-rd_kafka_get_offsets (rd_kafka_t *rk,
+rd_kafka_query_watermark_offsets (rd_kafka_t *rk,
 		      const char *topic, int32_t partition,
 		      int64_t *low, int64_t *high, int timeout_ms);
 
 
+/**
+ * @brief Get last known low (oldest/beginning) and high (newest/end) offsets
+ *        for partition.
+ *
+ * The low offset is updated periodically (if statistics.interval.ms is set)
+ * while the high offset is updated on each fetched message set from the broker.
+ *
+ * If there is no cached offset (either low or high, or both) then
+ * RD_KAFKA_OFFSET_INVALID will be returned for the respective offset.
+ *
+ * Offsets are returned in \p *low and \p *high respectively.
+ *
+ * @returns RD_KAFKA_RESP_ERR_NO_ERROR on success or an error code on failure.
+ *
+ * @remark Shall only be used with an active consumer instance.
+ */
+RD_EXPORT rd_kafka_resp_err_t
+rd_kafka_get_watermark_offsets (rd_kafka_t *rk,
+				const char *topic, int32_t partition,
+				int64_t *low, int64_t *high);
+
+
 
 /**
  * @brief Free pointer returned by librdkafka
diff --git a/src/rdkafka_broker.c b/src/rdkafka_broker.c
index e7b3d837f..ed9d25e17 100644
--- a/src/rdkafka_broker.c
+++ b/src/rdkafka_broker.c
@@ -2862,13 +2862,15 @@ rd_kafka_fetch_reply_handle (rd_kafka_broker_t *rkb,
                                 continue;
                         }
 
-
-			rd_kafka_toppar_lock(rktp);
-
                         /* Update hi offset to be able to compute
                          * consumer lag. */
                         rktp->rktp_offsets.hi_offset = hdr.HighwaterMarkOffset;
 
+			rd_kafka_toppar_lock(rktp);
+
+			/* High offset for get_watermark_offsets() */
+			rktp->rktp_hi_offset = hdr.HighwaterMarkOffset;
+
 			/* If this is the last message of the queue,
 			 * signal EOF back to the application. */
 			if (hdr.HighwaterMarkOffset ==
diff --git a/src/rdkafka_partition.c b/src/rdkafka_partition.c
index 4814b994f..6da5bda7d 100644
--- a/src/rdkafka_partition.c
+++ b/src/rdkafka_partition.c
@@ -75,8 +75,11 @@ static void rd_kafka_toppar_lag_handle_Offset (rd_kafka_t *rk,
 				     rktp->rktp_rkt->rkt_topic->str,
 				     rktp->rktp_partition,
 				     &Offset, &offset_cnt);
-        if (!err)
+        if (!err) {
+		rd_kafka_toppar_lock(rktp);
                 rktp->rktp_lo_offset = Offset;
+		rd_kafka_toppar_unlock(rktp);
+	}
 
         rktp->rktp_wait_consumer_lag_resp = 0;
 
@@ -148,7 +151,8 @@ shptr_rd_kafka_toppar_t *rd_kafka_toppar_new (rd_kafka_itopic_t *rkt,
 	rktp->rktp_offset_fp = NULL;
         rd_kafka_offset_stats_reset(&rktp->rktp_offsets);
         rd_kafka_offset_stats_reset(&rktp->rktp_offsets_fin);
-        rktp->rktp_lo_offset = RD_KAFKA_OFFSET_INVALID;
+        rktp->rktp_hi_offset = RD_KAFKA_OFFSET_INVALID;
+	rktp->rktp_lo_offset = RD_KAFKA_OFFSET_INVALID;
 	rktp->rktp_app_offset = RD_KAFKA_OFFSET_INVALID;
         rktp->rktp_stored_offset = RD_KAFKA_OFFSET_INVALID;
         rktp->rktp_committed_offset = RD_KAFKA_OFFSET_INVALID;
diff --git a/src/rdkafka_partition.h b/src/rdkafka_partition.h
index 4041a61d3..4b3287f99 100644
--- a/src/rdkafka_partition.h
+++ b/src/rdkafka_partition.h
@@ -130,6 +130,9 @@ struct rd_kafka_toppar_s { /* rd_kafka_toppar_t */
                                                * Updated periodically
                                                * by broker thread.
                                                * Locks: toppar_lock */
+
+	int64_t rktp_hi_offset;              /* Current high offset.
+					      * Locks: toppar_lock */
         int64_t rktp_lo_offset;              /* Current broker low offset.
                                               * This is outside of the stats
                                               * struct due to this field
diff --git a/tests/0006-symbols.c b/tests/0006-symbols.c
index 28eac148f..afc487314 100644
--- a/tests/0006-symbols.c
+++ b/tests/0006-symbols.c
@@ -147,7 +147,8 @@ int main_0006_symbols (int argc, char **argv) {
 		rd_kafka_topic_partition_list_copy(NULL);
 		rd_kafka_topic_partition_list_set_offset(NULL, NULL, 0, 0);
 		rd_kafka_topic_partition_list_find(NULL, NULL, 0);
-		rd_kafka_get_offsets(NULL, NULL, 0, NULL, NULL, 0);
+		rd_kafka_query_watermark_offsets(NULL, NULL, 0, NULL, NULL, 0);
+		rd_kafka_get_watermark_offsets(NULL, NULL, 0, NULL, NULL);
         }
 
 
diff --git a/tests/0031-get_offsets.c b/tests/0031-get_offsets.c
index dd3eba38e..977bfe5f8 100644
--- a/tests/0031-get_offsets.c
+++ b/tests/0031-get_offsets.c
@@ -35,7 +35,7 @@
 
 
 /**
- * Verify that rd_kafka_get_offsets() works.
+ * Verify that rd_kafka_(query|get)_watermark_offsets() works.
  */
 
 
@@ -43,29 +43,71 @@ int main_0031_get_offsets (int argc, char **argv) {
 	const char *topic = test_mk_topic_name(__FUNCTION__, 1);
         const int msgcnt = 100;
 	rd_kafka_t *rk;
-	int64_t low = -1234, high = -1235;
+	rd_kafka_topic_t *rkt;
+	int64_t qry_low = -1234, qry_high = -1235;
+	int64_t get_low = -1234, get_high = -1235;
 	rd_kafka_resp_err_t err;
-	test_timing_t t_get;
+	test_timing_t t_qry, t_get;
+	uint64_t testid;
 
         /* Produce messages */
-        test_produce_msgs_easy(topic, 0, 0, msgcnt);
+        testid = test_produce_msgs_easy(topic, 0, 0, msgcnt);
 
 	/* Get offsets */
 	rk = test_create_consumer(NULL, NULL, NULL, NULL, NULL);
 
-	TIMING_START(&t_get, "get_offsets");
-	err = rd_kafka_get_offsets(rk, topic, 0, &low, &high, 10*1000);
-	TIMING_STOP(&t_get);
+	TIMING_START(&t_qry, "query_watermark_offsets");
+	err = rd_kafka_query_watermark_offsets(rk, topic, 0,
+					       &qry_low, &qry_high, 10*1000);
+	TIMING_STOP(&t_qry);
 	if (err)
-		TEST_FAIL("get_offsets failed: %s\n", rd_kafka_err2str(err));
+		TEST_FAIL("query_watermark_offsets failed: %s\n",
+			  rd_kafka_err2str(err));
 
-	if (low != 0 && high != msgcnt)
+	if (qry_low != 0 && qry_high != msgcnt)
 		TEST_FAIL("Expected low,high %d,%d, but got "
 			  "%"PRId64",%"PRId64,
-			  0, msgcnt, low, high);
+			  0, msgcnt, qry_low, qry_high);
+
+	TEST_SAY("query_watermark_offsets: "
+		 "offsets %"PRId64", %"PRId64"\n", qry_low, qry_high);
+
+	/* Now start consuming to update the offset cache, then query it
+	 * with the get_ API. */
+	rkt = test_create_topic_object(rk, topic, NULL);
+
+	test_consumer_start("get", rkt, 0, RD_KAFKA_OFFSET_BEGINNING);
+	test_consume_msgs("get", rkt, testid, 0, TEST_NO_SEEK,
+			  0, msgcnt, 0);
+	/* After at least one message has been consumed the
+	 * watermarks are cached. */
+
+	TIMING_START(&t_get, "get_watermark_offsets");
+	err = rd_kafka_get_watermark_offsets(rk, topic, 0,
+					     &get_low, &get_high);
+	TIMING_STOP(&t_get);
+	if (err)
+		TEST_FAIL("get_watermark_offsets failed: %s\n",
+			  rd_kafka_err2str(err));
+
+	TEST_SAY("get_watermark_offsets: "
+		 "offsets %"PRId64", %"PRId64"\n", get_low, get_high);
+
+	if (get_high != qry_high)
+		TEST_FAIL("query/get discrepancies: "
+			  "low: %"PRId64"/%"PRId64", high: %"PRId64"/%"PRId64,
+			  qry_low, get_low, qry_high, get_high);
+	if (get_low >= get_high)
+		TEST_FAIL("get_watermark_offsets: "
+			  "low %"PRId64" >= high %"PRId64,
+			  get_low, get_high);
+
+	/* FIXME: We currently dont bother checking the get_low offset
+	 *        since it requires stats to be enabled. */
 
-	TEST_SAY("Got offsets %"PRId64", %"PRId64"\n", low, high);
+	test_consumer_stop("get", rkt, 0);
 
+	rd_kafka_topic_destroy(rkt);
 	rd_kafka_destroy(rk);
 
         return 0;
