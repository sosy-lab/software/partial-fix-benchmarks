cgrp: made wait_coord_q expiry lock-safe
cgrp: fix a number of assign() problems (issues #871, #844, possibly #730)

 * avoid unwanted non-wait-unassign -> init state transition
 * toppars could be lost dangling between desired and unknown state
 * the previous assignment could get in the way of the new assignment
 * OffsetCommit for current assignment was poorly handling lack of coordinator
 * OffsetCommit without any offsets to commit could cause a wait_commit stall
cgrp: dont get stuck in wait-coord state on reoccuring query errors (issue #844)
cgrp: fix a number of assign() problems (issues #871, #844, possibly #730)

 * avoid unwanted non-wait-unassign -> init state transition
 * toppars could be lost dangling between desired and unknown state
 * the previous assignment could get in the way of the new assignment
 * OffsetCommit for current assignment was poorly handling lack of coordinator
 * OffsetCommit without any offsets to commit could cause a wait_commit stall

(cherry picked from commit 2cb7a296ae5eda6317deb8331f0f49f3fa4fd73c)
cgrp: dont get stuck in wait-coord state on reoccuring query errors (issue #844)

(cherry picked from commit 966b6c10a76f9c25b97355db9303f6e6fefed256)
