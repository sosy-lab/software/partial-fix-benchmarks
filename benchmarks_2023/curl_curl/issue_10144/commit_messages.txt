haxproxy: send before TLS handhshake

- reverse order of haproxy and final ssl cfilter

- make haproxy avaiable on PROXY builds, independent of HTTP support as
  it can be used with any protocol.

Reported-by: Sergio-IME on github
Fixes #10165
Closes #10167
socketpair: allow localhost MITM sniffers

Windows allow programs to MITM connections to localhost. The previous
check here would detect that and error out. This new method writes data
to verify the pipe thus allowing MITM.

Reported-by: SerusDev on github
Fixes #10144
socketpair: allow localhost MITM sniffers

Windows allow programs to MITM connections to localhost. The previous
check here would detect that and error out. This new method writes data
to verify the pipe thus allowing MITM.

Reported-by: SerusDev on github
Fixes #10144
Closes #10169
