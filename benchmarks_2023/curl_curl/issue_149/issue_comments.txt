Remove unnecessary null pointer checks
I agree they're unnecessary but also harmless. If you or someone brings a patch to remove them, I'll appreciate it. I'm not gonna spend many cycles of my own to remove these.

Fixed merged in commit 9e661601feba
