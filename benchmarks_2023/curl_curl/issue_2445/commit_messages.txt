detect_proxy: only show proxy use if it had contents
ftplistparser: keep state between invokes

Regression from f786d1f14

Reported-by: wncboy on github
Fixes #2445
ftplistparser: keep state between invokes

Regression from f786d1f14

Reported-by: wncboy on github
Fixes #2445
Closes #2508
ftplistparser: keep state between invokes

Regression from f786d1f14

Reported-by: wncboy on github
Fixes #2445
Closes #2508
ftplistparser: keep state between invokes

Fixes FTP wildcard parsing when doing over a number of read buffers.

Regression from f786d1f14

Reported-by: wncboy on github
Fixes #2445
Closes #2519
ftplistparser: keep state between invokes

Fixes FTP wildcard parsing when doing over a number of read buffers.

Regression from f786d1f14

Reported-by: wncboy on github
Fixes #2445
Closes #2519
ftplistparser: keep state between invokes

Fixes FTP wildcard parsing when done over a number of read buffers.

Regression from f786d1f14

Reported-by: wncboy on github
Fixes #2445
Closes #2526
