diff --git a/src/tool_cb_wrt.c b/src/tool_cb_wrt.c
index 2cb5e1b41e1d78..20b4a849050811 100644
--- a/src/tool_cb_wrt.c
+++ b/src/tool_cb_wrt.c
@@ -26,6 +26,7 @@
 #include "curlx.h"
 
 #include "tool_cfgable.h"
+#include "tool_doswin.h"
 #include "tool_msgs.h"
 #include "tool_cb_wrt.h"
 
@@ -55,12 +56,51 @@ bool tool_create_output_file(struct OutStruct *outs,
   }
 
   /* open file for writing */
+#ifdef WIN32
+  /* Use CreateFile to open the FILE instead of fopen, since the latter sets
+     the archive bit which we don't want to set until we're done writing.
+     https://github.com/curl/curl/issues/3354 */
+  {
+    int fd;
+    HANDLE h = CreateFileA(outs->filename, GENERIC_WRITE, FILE_SHARE_READ,
+                           NULL, OPEN_ALWAYS,
+                           (FILE_ATTRIBUTE_NORMAL | FILE_FLAG_SEQUENTIAL_SCAN),
+                           NULL);
+    if(h == INVALID_HANDLE_VALUE) {
+      warnf(global, "Failed to create the file %s: CreateFile failed.\n",
+            outs->filename);
+      return FALSE;
+    }
+
+    fd = _open_osfhandle((intptr_t)h, (append ? _O_APPEND : 0));
+    if(fd == -1) {
+      warnf(global, "Failed to create the file %s: _open_osfhandle failed.\n",
+            outs->filename);
+      CloseHandle(h);
+      return FALSE;
+    }
+
+    file = fdopen(fd, (append ? "ab" : "wb"));
+    if(!file) {
+      warnf(global, "Failed to create the file %s: %s\n", outs->filename,
+            strerror(errno));
+      CloseHandle(h);
+      return FALSE;
+    }
+
+    /* Disable the archive bit before we write to the file. This is necessary
+       because CreateFile ignores FILE_ATTRIBUTE_NORMAL for existing files. */
+    (void)disable_file_archive_bit(file);
+  }
+#else
   file = fopen(outs->filename, append?"ab":"wb");
   if(!file) {
     warnf(global, "Failed to create the file %s: %s\n", outs->filename,
           strerror(errno));
     return FALSE;
   }
+#endif
+
   outs->s_isreg = TRUE;
   outs->fopened = TRUE;
   outs->stream = file;
diff --git a/src/tool_doswin.c b/src/tool_doswin.c
index f360b92e67973e..3eaf5b901c745a 100644
--- a/src/tool_doswin.c
+++ b/src/tool_doswin.c
@@ -725,6 +725,114 @@ struct curl_slist *GetLoadedModulePaths(void)
   return slist;
 }
 
+const int FileBasicInformation = 4;
+
+typedef struct _FILE_BASIC_INFORMATION {
+  LARGE_INTEGER CreationTime;
+  LARGE_INTEGER LastAccessTime;
+  LARGE_INTEGER LastWriteTime;
+  LARGE_INTEGER ChangeTime;
+  ULONG         FileAttributes;
+} FILE_BASIC_INFORMATION, *PFILE_BASIC_INFORMATION;
+
+typedef LONG (WINAPI *NTQUERYINFORMATIONFILE)(HANDLE FileHandle,
+                                              PVOID IoStatusBlock,
+                                              PVOID FileInformation,
+                                              ULONG FileInformationLength,
+                                              int FileInformationClass);
+
+typedef LONG (WINAPI *NTSETINFORMATIONFILE)(HANDLE FileHandle,
+                                            PVOID IoStatusBlock,
+                                            PVOID FileInformation,
+                                            ULONG FileInformationLength,
+                                            int FileInformationClass);
+
+/* Enable or disable archive bit for FILE by using FILE_ATTRIBUTE_ARCHIVE flag.
+ *
+ * Returns TRUE on success and FALSE on failure. If reading or writing the
+ * archive bit is not supported by the file it is treated as a failure.
+ */
+BOOL set_file_archive_bit(FILE *file, BOOL enable)
+{
+  HANDLE h;
+  NTSETINFORMATIONFILE NtSetInformationFile;
+  NTQUERYINFORMATIONFILE NtQueryInformationFile;
+  void *iostatus;  /* IO_STATUS_BLOCK status output which we don't need */
+  FILE_BASIC_INFORMATION info;
+
+  if(!file)
+    return TRUE;
+
+  NtQueryInformationFile = (NTQUERYINFORMATIONFILE)
+    GetProcAddress(GetModuleHandleA("ntdll"), "NtQueryInformationFile");
+
+  if(!NtQueryInformationFile)
+    return FALSE;
+
+  NtSetInformationFile = (NTSETINFORMATIONFILE)
+    GetProcAddress(GetModuleHandleA("ntdll"), "NtSetInformationFile");
+
+  if(!NtSetInformationFile)
+    return FALSE;
+
+  if(fflush(file))
+    return FALSE;
+
+  h = (HANDLE)_get_osfhandle(fileno(file));
+
+  if(h == INVALID_HANDLE_VALUE)
+    return FALSE;
+
+  if(!FlushFileBuffers(h)) /* may fail on devices */
+    return FALSE;
+
+  iostatus = calloc(128, 1);
+  if(!iostatus)
+    return FALSE;
+
+  /* Read the attributes. Not all files support it so it may fail. */
+  if(NtQueryInformationFile(h, iostatus, &info, sizeof(info),
+                            FileBasicInformation)) {
+   free(iostatus);
+   return FALSE;
+  }
+
+  info.ChangeTime.QuadPart = 0;
+  info.CreationTime.QuadPart = 0;
+  info.LastAccessTime.QuadPart = 0;
+  info.LastWriteTime.QuadPart = 0;
+
+  if(enable) {
+    if(info.FileAttributes == FILE_ATTRIBUTE_NORMAL)
+      info.FileAttributes = 0;
+    info.FileAttributes |= FILE_ATTRIBUTE_ARCHIVE;
+  }
+  else {
+    if(info.FileAttributes == FILE_ATTRIBUTE_NORMAL) {
+      free(iostatus);
+      return TRUE;
+    }
+    info.FileAttributes &= ~FILE_ATTRIBUTE_ARCHIVE;
+    if(!info.FileAttributes)
+      info.FileAttributes = FILE_ATTRIBUTE_NORMAL;
+  }
+
+  /* Write the attributes. Not all files support it so it may fail. */
+  if(NtSetInformationFile(h, iostatus, &info, sizeof(info),
+                         FileBasicInformation)) {
+   free(iostatus);
+   return FALSE;
+  }
+
+  if(!FlushFileBuffers(h)) { /* may fail on devices */
+    free(iostatus);
+    return FALSE;
+  }
+
+  free(iostatus);
+  return TRUE;
+}
+
 #endif /* WIN32 */
 
 #endif /* MSDOS || WIN32 */
diff --git a/src/tool_doswin.h b/src/tool_doswin.h
index 415fac27e0a092..3725701fb3d620 100644
--- a/src/tool_doswin.h
+++ b/src/tool_doswin.h
@@ -62,6 +62,10 @@ CURLcode FindWin32CACert(struct OperationConfig *config,
                          const char *bundle_file);
 struct curl_slist *GetLoadedModulePaths(void);
 
+BOOL set_file_archive_bit(FILE *file, BOOL enable);
+#define enable_file_archive_bit(x) set_file_archive_bit(x, TRUE);
+#define disable_file_archive_bit(x) set_file_archive_bit(x, FALSE);
+
 #endif /* WIN32 */
 
 #endif /* MSDOS || WIN32 */
diff --git a/src/tool_operate.c b/src/tool_operate.c
index e53a9d8676168f..61bc3f3a95e02d 100644
--- a/src/tool_operate.c
+++ b/src/tool_operate.c
@@ -1834,7 +1834,12 @@ static CURLcode operate_do(struct GlobalConfig *global,
 
         /* Close the file */
         if(outs.fopened && outs.stream) {
-          int rc = fclose(outs.stream);
+          int rc;
+#ifdef WIN32
+          /* Enable the archive bit before we close the file */
+          (void)enable_file_archive_bit(outs.stream);
+#endif
+          rc = fclose(outs.stream);
           if(!result && rc) {
             /* something went wrong in the writing process */
             result = CURLE_WRITE_ERROR;
