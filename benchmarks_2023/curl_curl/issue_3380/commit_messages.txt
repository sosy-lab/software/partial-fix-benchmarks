pingpong: change default response timeout to 120 seconds

Previously it was 30 minutes
curl -J: overwrite the destination file

Fixes #3380
curl -J: do not append to the destination file

Fixes #3380
mbedtls: use VERIFYHOST

Previously, VERIFYPEER would enable/disable all checks.

Reported-by: Eric Rosenquist
Fixes #3376
Closes #3380
curl -J: do not append to the destination file

Reported-by: Kamil Dudka
Fixes #3380
Closes #3381
