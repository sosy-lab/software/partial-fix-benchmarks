curl 7.67.0 w/OpenSSL: Missing TLS shutdown from server may cause error
Hm, that change was inadvertent on my behalf back then, as it was only intended to improve the error messages for the syscall case, not actually change behavior... 
Interesting. I think I'm seeing the same error when sending a GET request to api.bitbucket.org with Basic Auth headers. Though the site is hosted on Bluehost, so maybe that's it.

cURL version | 7.67.0 OpenSSL/1.0.2t


Getting same error "CURL Error: 56" on Linux server using curl 7.67.0..
This is a very specific case of error 56 where the error message will say there is "No error" or "Success". For reference please include your curl -V and the URL if it is public.
As is said in the other issue request:

I think it's an error related to WinSockets that the socket is closed while curl / OpenSSH tries to use the socket again. For me the problem only seems to be appearing on POST or custom requests like PUT, DELETE and only if there are two or more requests are send right after the other is finished and got data. If i insert a sleep with at least 0.5 seconds between the return and the second request it works.

If i set the CURLOPT_FRESH_CONNECT option it works for me too. Thats why i think it has something todo with reused sockets which are closed to early.
This issue is for a very specific error, "No error" / "Success" is shown in the error message. As I [mentioned](https://github.com/curl/curl/issues/4409#issuecomment-555715213) in the other thread SYSCALL errors are almost invariably due to server or network issues.
This is the error I receive when making a GET request using Basic Auth headers to api.bitbucket.org from my site. `cURL error 56: OpenSSL SSL_read: Success`

The site is hosted on Bluehost. Similar error if I try the same GET request from another site on same host.

These requests were working fine a few days ago.
Fix landed but leaving open for now to minimize dupes.
Closing now as this is now fixed in a shipped release.
@bagder in which release is this fix included? 7.68.0?
yes, 7.68.0