BINDINGS: update the Lua-cURL URL
nss: load CA certificates even with --insecure

... because they may include an intermediate certificate for a _client_
certificate and the intermediate certificate needs to be presented to
the server, no matter if we verify the peer or not.

Reported-by: thraidh
Closes #851
nss: load CA certificates even with --insecure

... because they may include an intermediate certificate for a client
certificate and the intermediate certificate needs to be presented to
the server, no matter if we verify the peer or not.

Reported-by: thraidh
Closes #851
nss: load CA certificates even with --insecure

... because they may include an intermediate certificate for a client
certificate and the intermediate certificate needs to be presented to
the server, no matter if we verify the peer or not.

Reported-by: thraidh
Closes #851
