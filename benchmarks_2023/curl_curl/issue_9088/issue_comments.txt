URL parser treats blank user as no user (CURLUE_NO_USER)
> Is this expected?

I don't think anyone has thought about it. The entire "set blank user name to trigger X" is a somewhat of a kludge, so it's not always easy to tell how it *should* work!
I don't see any downsides to changing it. The change below fixes it but then will also do it even if the login string is empty, for example `http://@test.com`

~~~diff
diff --git a/lib/url.c b/lib/url.c
index 1114c6c..0ce1aa1 100644
--- a/lib/url.c
+++ b/lib/url.c
@@ -2894,7 +2894,7 @@ CURLcode Curl_parse_login_details(const char *login, const size_t len,
                                  (size_t)(login + len - osep)) - 1 : 0);
 
   /* Allocate the user portion buffer */
-  if(userp && ulen) {
+  if(userp) {
     ubuf = malloc(ulen + 1);
     if(!ubuf)
       result = CURLE_OUT_OF_MEMORY;
~~~