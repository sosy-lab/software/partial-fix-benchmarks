Fix door drawing in fog (#72)

Plus some refactoring
Add blob shadow (fixes #16)
Fix crashes from out-of-bounds shadow drawing (#16)
Add grenade shadows (fixes #16)

Add shadow configuration
Change layout of option menus
