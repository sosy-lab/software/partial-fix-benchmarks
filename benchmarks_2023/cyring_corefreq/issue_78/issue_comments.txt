Optimizations
# Memory
2 Declare only once
https://github.com/cyring/CoreFreq/blob/1940a9acbaa823215f4d259bb034ee88191b6d13/coretypes.h#L383
3. Reduce size to bitmap
https://github.com/cyring/CoreFreq/blob/1940a9acbaa823215f4d259bb034ee88191b6d13/corefreqk.c#L3467
4. Use dynamic memory
https://github.com/cyring/CoreFreq/blob/1940a9acbaa823215f4d259bb034ee88191b6d13/corefreqk.h#L357
Can build with compiler optimization levels 0, 1, 2, and 3
**Regression in Voltage** which computes zero half the time:
https://github.com/cyring/CoreFreq/blob/5e2124470ebd57cc1eb6a7dfc53b16aa659e2d42/corefreqd.c#L3106

Move the `SProc` assignment inside the `while` loop
```C
void Core_Manager(REF *Ref)
{
	SHM_STRUCT		*Shm = Ref->Shm;
	PROC			*Proc = Ref->Proc;
	CORE			**Core = Ref->Core;
	struct PKG_FLIP_FLOP	*PFlip;
	struct FLIP_FLOP	*SProc;
	SERVICE_PROC		localService = {.Proc = -1};
	double			maxRelFreq;
	unsigned int		cpu = 0;

	pthread_t tid = pthread_self();
	Shm->AppSvr = tid;

	if (ServerFollowService(&localService, &Shm->Proc.Service, tid) == 0)
		pthread_setname_np(tid, "corefreqd-pmgr");

	ARG *Arg = calloc(Shm->Proc.CPU.Count, sizeof(ARG));

    while (!BITVAL(Shutdown, 0))
    {	// Loop while all the cpu room bits are not cleared.
	while (!BITVAL(Shutdown, 0)
	    && BITWISEAND(BUS_LOCK, roomCore, roomSeed))
	{
		nanosleep(&Shm->Sleep.pollingWait, NULL);
	}

	Shm->Proc.Toggle = !Shm->Proc.Toggle;
	PFlip = &Shm->Proc.FlipFlop[Shm->Proc.Toggle];

	SProc = &Shm->Cpu[Proc->Service.Core].FlipFlop[ \
					!Shm->Cpu[Proc->Service.Core].Toggle];

	// Reset PTM sensor with the Service Processor.
	PFlip->Thermal.Sensor = SProc->Thermal.Sensor;
```

https://github.com/cyring/CoreFreq/blob/1022407c4c3c95a65d8984462e49ce64bfc75215/corefreqk.c#L5088

Redundant code which tests if the collect timer is bound to the Service Processor ?

Split code in two macro functions:
* Temp for any core
* Temp for Package

A gain of one branch is expected.