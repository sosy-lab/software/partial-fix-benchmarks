sqlite seems to be required in 3.0.1
Are you compiling with http enabled?  sqlite is needed by the http /
dav code too.

On Sat, 29 Apr 2017, at 01:28, Sebastian Hagedorn wrote:
> I'm setting up a new CentOS 7 system to test Cyrus 3.0.1 on. I used
> the tarball and decided to run "configure" without any options to get
> started. I installed additional packages until configure ran through
> without errors. No database backends should be used:> Database support:
>  mysql:              no
>  postgresql:         no
>  sqlite:             no
>  lmdb:               no


> But when I ran make, I got this error:


> /bin/sh ./libtool  --tag=CC   --mode=compile gcc -DHAVE_CONFIG_H -I.
> -I. -I./lib -I. -I./lib -DLIBEXEC_DIR=\"/usr/local/libexec\" -
> DSBIN_DIR=\"/usr/local/sbin\" -DSYSCONF_DIR=\"/etc\" -DHAVE_CONFIG_H
> -fPIC  -fvisibility=hidden  -g -O2 -MT imap/imap_libcyrus_imap_la-
> user.lo -MD -MP -MF imap/.deps/imap_libcyrus_imap_la-user.Tpo -c -o
> imap/imap_libcyrus_imap_la-user.lo `test -f 'imap/user.c' || echo
> './'`imap/user.c libtool: compile:  gcc -DHAVE_CONFIG_H -I. -I. -
> I./lib -I. -I./lib -DLIBEXEC_DIR=\"/usr/local/libexec\" -
> DSBIN_DIR=\"/usr/local/sbin\" -DSYSCONF_DIR=\"/etc\" -DHAVE_CONFIG_H
> -fPIC -fvisibility=hidden -g -O2 -MT imap/imap_libcyrus_imap_la-
> user.lo -MD -MP -MF imap/.deps/imap_libcyrus_imap_la-user.Tpo -c
> imap/user.c  -fPIC -DPIC -o imap/.libs/imap_libcyrus_imap_la-user.o In
> file included from imap/user.c:81:0: ./lib/sqldb.h:47:21: fatal error:
> sqlite3.h: No such file or directory #include <sqlite3.h>
>> So it looks as though sqlite3.h is pulled in regardless of the results
> of configure.> — You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub[1], or mute the
> thread[2].> 

--
  Bron Gondwana
  brong@fastmail.fm



Links:

  1. https://github.com/cyrusimap/cyrus-imapd/issues/1944
  2. https://github.com/notifications/unsubscribe-auth/AABE7QtR431FmTyMIZ1LDg24Zkz0UJiVks5r0gWxgaJpZM4NLonX

Sqlite is used by a bunch of code paths, it's not just a backend for cyrusdb's.  

Still though, if you ran `configure` without arguments, I don't _think_ it should be required by any of the default things.  Although, I know there were some features that used to be default off and are now default enabled in 3.0 -- but I can't remember off the top of my head which they were.  Maybe one of those features uses sqlite?  In which case configure isn't checking/erroring properly...

Or, it could just be that `imap/user.c` is missing suitable ifdefs around its inclusion of `lib/sqldb.h`...
I've had a look and I'm pretty sure that `#include "lib/sqldb.h"` in `imap/user.c` is now redundant.  It used to be needed for deleting calendar alarms when deleting a user (and wasn't correctly ifdef'd), but that API has changed and this include is no longer required at all.

I'm testing this out on my end, but I have sqlite and it's fiddly to remove to check.  @hagedose, can you please try commenting out that include and rebuilding (still without sqlite), and see if it now works?  Cheers
This is upstream on the cyrus-imapd-3.0 and master branches now, but I'd appreciate a confirmation that it works okay :)
Thanks! Well, I can confirm that it fixes the problem at that point, but then there is another one:

```
make[2]: Entering directory `/root/cyrus-imapd-3.0.1'
depbase=`echo imap/sync_support.o | sed 's|[^/]*$|.deps/&|;s|\.o$||'`;\
gcc -DHAVE_CONFIG_H -I.   -I. -I./lib -I. -I./lib -DLIBEXEC_DIR=\"/usr/local/libexec\" -DSBIN_DIR=\"/usr/local/sbin\" -DSYSCONF_DIR=\"/etc\" -DHAVE_CONFIG_H        -fPIC  -g -O2 -MT imap/sync_support.o -MD -MP -MF $depbase.Tpo -c -o imap/sync_support.o imap/sync_support.c &&\
mv -f $depbase.Tpo $depbase.Po
In file included from imap/caldav_alarm.h:49:0,
                 from imap/sync_support.c:63:
./lib/sqldb.h:47:21: fatal error: sqlite3.h: No such file or directory
 #include <sqlite3.h>
```

To be clear, I could easily install sqlite, but either it should be listed as a required component and caught by configure if not present, or it should work without. FWIW, configure didn't enable anything that should require sqlite:

```
Cyrus Server configured components

   event notification: yes
   gssapi:             yes
   autocreate:         no
   idled:              no
   httpd:              no
   kerberos V4:        no
   murder:             no
   nntpd:              no
   replication:        no
   sieve:              yes
   calalarmd:          no
   jmap:               no
   objectstore:        no
   backup:             no

External dependencies:
   ldap:               no
   openssl:            yes
   zlib:               yes
   pcre:               yes
   clamav:             no
   -----------------------
   caringo:            no
   openio:             no
   -----------------------
   nghttp2:            no
   brotli:             no
   xml2:               no
   ical:               no
   icu4c:              no
   shapelib:           no

Database support:
   mysql:              no
   postgresql:         no
   sqlite:             no
   lmdb:               no

Search engine:
   squat:              yes
   sphinx:             no
   xapian:             no
   xapian_flavor:      none

Hardware support:
   SSE4.2:             yes
```
This is really helpful! Will try and look at this one tomorrow, hopefully it's as quick of a fix :)
This one should be fixable by wrapping the `#include "caldav_alarm.h"` line in `imap/sync_support.c` with an ifdef, like this:

```
#ifdef USE_CALALARMD
#include "caldav_alarm.h"
#endif
```

Just testing it out now...
I uninstalled sqlite to test this, and it works for me, and seems to be the last one, so this fix is now pushed.

There's at least one more on the master branch (but not on 3.0) that I'm looking into now....
Actually, I've raised #1947 for dealing with the ones on master-only, cause they're not a trivial fix.
Sorry, I didn't have time to look at this last week. I can confirm that 4127fa7 fixes the issue.

Thanks!