sieve_interface.h: play with #include
This was in fact already partially implemented in 51d28de6f4ebe, I wrote the patch for the 3.0 branch.  Now bc_eval.c has twice `#include "grammar.h"` and this happens, because the `#include`s are not alphabetically ordered.  `#include "varlist.h"` can be removed from `sieve/script.h`.

The function `sieve/grammar.c:is_number()` can be made static, and `sieve/grammar.c:is_identifier()` -- HIDDEN.
In fact 3.0 introduces a regression, as the sieve_interface.h header is unusable in its installed form, which was not the case with 2.5 .
`sieve/rebuild.c:sieve_getbcfname()` and `sieve/rebuild.c:sieve_getscriptfname()` are eligible to be made `static`.