sieve.y: added a comment and fixed a couple of nits
cmd_setacl: respond BAD if rights string contains unrecognised chars

* changes cyrus_acl_setmask to set the mask by reference, and return
  a success/error code
* other code that ignores the return value will get the same behaviour
  as before (which might be wrong, but isn't affecting SETACL)

Fixes #368
cmd_setacl: respond BAD if rights string contains unrecognised chars

* changes cyrus_acl_setmask to set the mask by reference, and return
  a success/error code
* other code that ignores the return value will get the same behaviour
  as before (which might be wrong, but isn't affecting SETACL)

Fixes #368
cmd_setacl: respond BAD if rights string contains unrecognised chars

* changes cyrus_acl_setmask to set the mask by reference, and return
  a success/error code
* other code that ignores the return value will get the same behaviour
  as before (which might be wrong, but isn't affecting SETACL)

Fixes #368
