diff --git a/include/notcurses/notcurses.h b/include/notcurses/notcurses.h
index 7e8fe8d011..fb100251ad 100644
--- a/include/notcurses/notcurses.h
+++ b/include/notcurses/notcurses.h
@@ -586,6 +586,10 @@ typedef struct nccell {
   // are single-byte ASCII-derived values. The XXXXXX is interpreted as a 24-bit
   // index into the egcpool. These pools may thus be up to 16MB.
   //
+  // A pixel graphic (a "sprixel") is indicated by the value 0x02XXXXXX. This
+  // is safe for the same reasons as a spilled EGC. The remaining 24 bits are
+  // an identifier for the sprixel cache, shared across the notcurses context.
+  //
   // The cost of this scheme is that the character 0x01 (SOH) cannot be encoded
   // in a nccell, which is absolutely fine because what 70s horseshit is SOH?
   // It must not be allowed through the API, or havoc will result.
@@ -608,7 +612,7 @@ typedef struct nccell {
   // (channels & 0x0200000000000000ull): blitted to lower-left quadrant
   // (channels & 0x0100000000000000ull): blitted to lower-right quadrant
   // (channels & 0x00ffffff00000000ull): foreground in 3x8 RGB (rrggbb) / pindex
-  // (channels & 0x0000000080000000ull): pixel graphics
+  // (channels & 0x0000000080000000ull): reserved, must be 0
   // (channels & 0x0000000040000000ull): background is *not* "default color"
   // (channels & 0x0000000030000000ull): background alpha (2 bits)
   // (channels & 0x0000000008000000ull): background uses palette index
@@ -739,6 +743,7 @@ cell_wide_left_p(const nccell* c){
 
 // return a pointer to the NUL-terminated EGC referenced by 'c'. this pointer
 // can be invalidated by any further operation on the plane 'n', so...watch out!
+// must not be called on a pixel graphic.
 API const char* cell_extended_gcluster(const struct ncplane* n, const nccell* c);
 
 // copy the UTF8-encoded EGC out of the nccell. the result is not tied to any
diff --git a/src/lib/egcpool.h b/src/lib/egcpool.h
index 59ee2ca478..f1da484d59 100644
--- a/src/lib/egcpool.h
+++ b/src/lib/egcpool.h
@@ -250,10 +250,22 @@ cell_egc_idx(const nccell* c){
   return (htole(c->gcluster) & 0x00fffffflu);
 }
 
+// Is the cell a spilled (more than 4 byte) UTF8 EGC?
+static inline bool
+cell_extended_p(const nccell* c){
+  return (htole(c->gcluster) & htole(0xff000000ul)) == htole(0x01000000ul);
+}
+
+// Is the cell a sprixel?
+static inline bool
+cell_pixels_p(const nccell* c){
+  return (htole(c->gcluster) & htole(0xff000000ul)) == htole(0x02000000ul);
+}
+
 // Is the cell simple (a UTF8-encoded EGC of four bytes or fewer)?
 static inline bool
 cell_simple_p(const nccell* c){
-  return (htole(c->gcluster) & htole(0xff000000ul)) != htole(0x01000000ul);
+  return !cell_pixels_p(c) && !cell_extended_p(c);
 }
 
 // only applies to complex cells, do not use on simple cells
diff --git a/src/lib/internal.h b/src/lib/internal.h
index c97e61f6db..80fb170362 100644
--- a/src/lib/internal.h
+++ b/src/lib/internal.h
@@ -39,12 +39,8 @@ struct ncvisual_details;
 #define CELL_NOBACKGROUND_MASK  0x8700000000000000ull
 
 // Was this glyph drawn as part of an ncvisual? If so, we need to honor
-// blitter stacking rather than the standard trichannel solver (yes, this
-// is the same as CELL_NOBACKGROUND_MASK).
-#define CELL_BLITTERSTACK_MASK  0x8700000000000000ull
-
-// if this bit is set, the cell ought be rasterized in pixel graphics mode.
-#define CELL_PIXEL_GRAPHICS     0x0000000080000000ull
+// blitter stacking rather than the standard trichannel solver.
+#define CELL_BLITTERSTACK_MASK  CELL_NOBACKGROUND_MASK
 
 // we can't define multipart ncvisual here, because OIIO requires C++ syntax,
 // and we can't go throwing C++ syntax into this header. so it goes.
@@ -683,26 +679,6 @@ plane_debug(const ncplane* n, bool details){
 
 void sprixel_free(sprixel* s);
 
-static inline unsigned
-channels_pixel_p(uint64_t channels){
-  return channels & CELL_PIXEL_GRAPHICS;
-}
-
-static inline unsigned
-cell_pixels_p(const nccell* c){
-  return channels_pixel_p(c->channels);
-}
-
-static inline nccell*
-cell_set_pixels(nccell* c, unsigned p){
-  if(p){
-    c->channels |= CELL_PIXEL_GRAPHICS;
-  }else{
-    c->channels &= ~CELL_PIXEL_GRAPHICS;
-  }
-  return c;
-}
-
 static inline void
 pool_release(egcpool* pool, nccell* c){
   if(!cell_simple_p(c)){
@@ -1103,7 +1079,6 @@ pool_blit_direct(egcpool* pool, nccell* c, const char* gcluster, int bytes, int
     return -1;
   }
   c->width = cols;
-  cell_set_pixels(c, 0);
   if(bytes <= 4){
     c->gcluster = 0;
     memcpy(&c->gcluster, gcluster, bytes);
diff --git a/src/lib/kitty.c b/src/lib/kitty.c
index 222bbae074..fa3028edbe 100644
--- a/src/lib/kitty.c
+++ b/src/lib/kitty.c
@@ -137,7 +137,6 @@ int kitty_blit_inner(ncplane* nc, int placey, int placex, int linesize,
     free(buf);
     return -1;
   }
-  cell_set_pixels(c, 1);
   free(buf);
   return 1;
 }
diff --git a/src/lib/render.c b/src/lib/render.c
index d69041b984..2134c5ed3c 100644
--- a/src/lib/render.c
+++ b/src/lib/render.c
@@ -383,7 +383,6 @@ paint(const ncplane* p, struct crender* rvec, int dstleny, int dstlenx,
             targc->stylemask = vis->stylemask;
             targc->width = vis->width;
           }
-          cell_set_pixels(targc, cell_pixels_p(vis));
           crender->p = p;
         }else if(cell_wide_right_p(vis)){
           crender->p = p;
diff --git a/src/lib/sixel.c b/src/lib/sixel.c
index 405d87d063..a13e6dff7b 100644
--- a/src/lib/sixel.c
+++ b/src/lib/sixel.c
@@ -262,7 +262,6 @@ int sixel_blit_inner(ncplane* nc, int placey, int placex, int lenx,
     free(buf);
     return -1;
   }
-  cell_set_pixels(c, 1);
   free(buf);
   return 1;
 }
