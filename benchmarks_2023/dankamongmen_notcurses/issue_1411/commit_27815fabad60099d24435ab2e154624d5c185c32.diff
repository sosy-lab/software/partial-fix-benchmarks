diff --git a/README.md b/README.md
index 2d89d6b6e0..ea752236ef 100644
--- a/README.md
+++ b/README.md
@@ -62,7 +62,7 @@ Wherever possible, Notcurses makes use of the Terminfo library shipped with
 NCURSES, benefiting greatly from its portability and thoroughness.
 
 Notcurses opens up advanced functionality for the interactive user on
-workstations, phones, laptops, and tablets, at the expense of e.g.
+workstations, phones, laptops, and tablets, possibly at the expense of e.g.
 some industrial and retail terminals. Fundamentally, Curses assumes the minimum
 and allows you (with effort) to step up, whereas Notcurses assumes the maximum
 and steps down (by itself) when necessary. The latter approach probably breaks
@@ -208,7 +208,7 @@ to breaking under incorrect `TERM` values. If you're not using `xterm`, your
 
 * **Q:** Can I have Notcurses without this huge multimedia stack? **A:** Yes! Build with `-DUSE_MULTIMEDIA=none`.
 
-* **Q:** In `xterm`, Alt doesn't work as expected. **A:** Check out the `eightBitInput` resource of `xterm`. Add `XTerm*eightBitInput: false` to your `$HOME/.Xresources`, and run `xrdb -a $HOME/.Xresources`.
+* **Q:** In `xterm`, Alt doesn't work as expected. **A:** Check out the `eightBitInput` resource of `xterm`. Add `XTerm*eightBitInput: false` to your `$HOME/.Xresources`, and run e.g. `xrdb -all -merge $HOME/.Xresources`.
 
 * **Q:** Notcurses looks like absolute crap in `screen`. **A:** `screen` doesn't support RGB colors (at least as of 4.08.00); if you have `COLORTERM` defined, you'll have a bad time. If you have a `screen` that was compiled with `--enable-colors256`, try exporting `TERM=screen-256color` as opposed to `TERM=screen`.
 
diff --git a/doc/man/man1/ncplayer.1.md b/doc/man/man1/ncplayer.1.md
index 853bdc8034..6abb4e971e 100644
--- a/doc/man/man1/ncplayer.1.md
+++ b/doc/man/man1/ncplayer.1.md
@@ -8,7 +8,7 @@ ncplayer - Render images and video to a terminal
 
 # SYNOPSIS
 
-**ncplayer** [**-h**] [**-V**] [**-q**] [**-d** ***delaymult***] [**-l** ***loglevel***] [**-b** ***blitter***] [**-s** ***scalemode***] [**-k**] [**-L**] [**-t** ***seconds***] files
+**ncplayer** [**-h**] [**-V**] [**-q**] [**-d** ***delaymult***] [**-l** ***loglevel***] [**-b** ***blitter***] [**-s** ***scalemode***] [**-k**] [**-L**] [**-a** ***align***] [**-t** ***seconds***] files
 
 # DESCRIPTION
 
@@ -33,6 +33,8 @@ be any non-negative number.
 
 **-b** ***blitter***: Blitter, one of **ascii**, **half**, **quad**, **sex**, **braille**, or **pixel**.
 
+**-a** ***type***: Align images on **left**, **center**, or **right**.
+
 **-m margins**: Define rendering margins (see below).
 
 **-L**: Loop frames until a key is pressed. Not supported with **-k**.
@@ -51,7 +53,8 @@ Default margins are all 0 and default scaling is **stretch**. The full
 rendering area will thus be used. Using **-m**, margins can be supplied.
 Provide a single number to set all four margins to the same value, or four
 comma-delimited values for the top, right, bottom, and left margins
-respectively. Negative margins are illegal.
+respectively. Top and bottom margins are ignored when **-k** is used. Negative
+margins are illegal.
 
 Scaling mode **stretch** resizes the object to match the target rendering
 area exactly. Unless a blitter is specified with **-b**, **stretch** will use
diff --git a/include/notcurses/direct.h b/include/notcurses/direct.h
index c5373b0abf..5c3f37adda 100644
--- a/include/notcurses/direct.h
+++ b/include/notcurses/direct.h
@@ -290,9 +290,12 @@ API int ncdirect_stop(struct ncdirect* nc);
 // Render an image using the specified blitter and scaling, but do not write
 // the result. The image may be arbitrarily many rows -- the output will scroll
 // -- but will only occupy the column of the cursor, and those to the right.
-// To actually write (and free) this, invoke ncdirect_raster_frame().
+// To actually write (and free) this, invoke ncdirect_raster_frame(). 'maxx'
+// and 'maxy', if greater than 0, are used for scaling; the terminal's geometry
+// is otherwise used.
 API ALLOC ncdirectv* ncdirect_render_frame(struct ncdirect* n, const char* filename,
-                                           ncblitter_e blitter, ncscale_e scale);
+                                           ncblitter_e blitter, ncscale_e scale,
+                                           int maxy, int maxx);
 
 // Takes the result of ncdirect_render_frame() and writes it to the output.
 API int ncdirect_raster_frame(struct ncdirect* n, ncdirectv* ncdv, ncalign_e align);
diff --git a/src/lib/direct.cpp b/src/lib/direct.cpp
index 34493c1d90..69f12e4a53 100644
--- a/src/lib/direct.cpp
+++ b/src/lib/direct.cpp
@@ -457,7 +457,10 @@ int ncdirect_raster_frame(ncdirect* n, ncdirectv* ncdv, ncalign_e align){
 }
 
 ncdirectv* ncdirect_render_frame(ncdirect* n, const char* file,
-                                 ncblitter_e blitfxn, ncscale_e scale){
+                                 ncblitter_e blitfxn, ncscale_e scale,
+                                 int ymax, int xmax){
+  int dimy = ymax > 0 ? ymax : ncdirect_dim_y(n);
+  int dimx = xmax > 0 ? xmax : ncdirect_dim_x(n);
   struct ncvisual* ncv = ncvisual_from_file(file);
   if(ncv == nullptr){
     return nullptr;
@@ -478,11 +481,11 @@ ncdirectv* ncdirect_render_frame(ncdirect* n, const char* file,
   int disprows, dispcols;
   if(scale != NCSCALE_NONE && scale != NCSCALE_NONE_HIRES){
     if(bset->geom != NCBLIT_PIXEL){
-      dispcols = ncdirect_dim_x(n) * encoding_x_scale(&n->tcache, bset);
-      disprows = ncdirect_dim_y(n) * encoding_y_scale(&n->tcache, bset);
+      dispcols = dimx * encoding_x_scale(&n->tcache, bset);
+      disprows = dimy * encoding_y_scale(&n->tcache, bset);
     }else{
-      dispcols = ncdirect_dim_x(n) * n->tcache.cellpixx;
-      disprows = ncdirect_dim_y(n) * n->tcache.cellpixy;
+      dispcols = dimx * n->tcache.cellpixx;
+      disprows = dimy * n->tcache.cellpixy;
     }
     if(scale == NCSCALE_SCALE || scale == NCSCALE_SCALE_HIRES){
       scale_visual(ncv, &disprows, &dispcols);
@@ -531,7 +534,7 @@ ncdirectv* ncdirect_render_frame(ncdirect* n, const char* file,
 
 int ncdirect_render_image(ncdirect* n, const char* file, ncalign_e align,
                           ncblitter_e blitfxn, ncscale_e scale){
-  auto faken = ncdirect_render_frame(n, file, blitfxn, scale);
+  auto faken = ncdirect_render_frame(n, file, blitfxn, scale, -1, -1);
   if(!faken){
     return -1;
   }
diff --git a/src/player/play.cpp b/src/player/play.cpp
index 469a6b37c0..1aafb87bd0 100644
--- a/src/player/play.cpp
+++ b/src/player/play.cpp
@@ -28,6 +28,7 @@ void usage(std::ostream& o, const char* name, int exitcode){
   o << " -t seconds: delay t seconds after each file\n";
   o << " -l loglevel: integer between 0 and 9, goes to stderr'\n";
   o << " -s scaling: one of 'none', 'hires', 'scale', 'scalehi', or 'stretch'\n";
+  o << " -a type: 'left', 'right', or 'center'\n";
   o << " -b blitter: one of 'ascii', 'half', 'quad', 'sex', 'braille', or 'pixel'\n";
   o << " -m margins: margin, or 4 comma-separated margins\n";
   o << " -d mult: non-negative floating point scale for frame time" << std::endl;
@@ -282,7 +283,8 @@ auto handle_opts(int argc, char** argv, notcurses_options& opts, bool* quiet,
 }
 
 // argc/argv ought already be reduced to only the media arguments
-int direct_mode_player(int argc, char** argv, ncscale_e scalemode, ncblitter_e blitter){
+int direct_mode_player(int argc, char** argv, ncscale_e scalemode,
+                       ncblitter_e blitter, int lmargin, int rmargin){
   Direct dm{};
   if(!dm.canopen_images()){
     std::cerr << "Notcurses was compiled without multimedia support\n";
@@ -295,7 +297,8 @@ int direct_mode_player(int argc, char** argv, ncscale_e scalemode, ncblitter_e b
   {
     for(auto i = 0 ; i < argc ; ++i){
       try{
-        dm.render_image(argv[i], NCALIGN_RIGHT, blitter, scalemode);
+        dm.prep_image(argv[i], blitter, scalemode, -1,
+                      dm.get_dim_x() - (lmargin + rmargin));
       }catch(std::exception& e){
         // FIXME want to stop nc first :/ can't due to stdn, ugh
         std::cerr << argv[i] << ": " << e.what() << "\n";
@@ -323,7 +326,7 @@ auto main(int argc, char** argv) -> int {
   // if -k was provided, we now use direct mode rather than simply not using the
   // alternate screen, so that output is inline with the shell.
   if(ncopts.flags & NCOPTION_NO_ALTERNATE_SCREEN){
-    if(direct_mode_player(argc - nonopt, argv + nonopt, scalemode, blitter)){
+    if(direct_mode_player(argc - nonopt, argv + nonopt, scalemode, blitter, ncopts.margin_l, ncopts.margin_r)){
       return EXIT_FAILURE;
     }
     return EXIT_SUCCESS;
