sprixcells ending prior to the end of the cell cannot be OPAQUE
![2021-04-18-034110_875x745_scrot](https://user-images.githubusercontent.com/143473/115138081-ec972900-9ff7-11eb-85f2-19c12ae64340.png)

augh, this is working fine on kitty, but it sucks with sixel, because now when we fade out the main page, we're having to constantly redraw the sprixel, ugh ugh ugh.
ugh goddamnit, totally lame. i don't want to export kitty vs sixel info to the user api, but that's the only sane place to make a decision here -- we can't have sprixels in general cutting out text underneath them etc. hrmmm, except maybe if we're scaling, we do? that's the only possible path i see out of this, and it's still kinda ghetto. it doesn't help with the `NCSCALE_NONE` case, either.

shit, maybe we just don't fix this for sixel? no, that's no good, it'll just look like a bug.
you could maybe mark them ANNIHILATED. they'd display, but an overwrite would kill them permanently. i don't know. fuck sixel and all its doin's and transpirin's. it doesn't actually look that bad with the `sixel_move()` optimization i just put in.
As @dnkl and I noticed in #1527 (and i pointed out above, heh), this causes flicker in `xray`, where it...*really* shouldn't. `intro` is one thing, but we're not updating any cells behind the central media in `xray`.

i'm gonna comment this out for sixel for the moment, but that sounds like a bug.
when we move, we always want to damage (unless we're OPAQUE on the new cell), but not when we delete. the latter conditional needs to be restricted to move-deletes. but hrmmm, can we really get away with that? let's try combining `xray` and `pixel`; if the changes in the background aren't picked up, we need to expand the damage on delete.