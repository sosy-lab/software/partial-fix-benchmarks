OpenCL tuning for memory issue in _opencl_get_unused_device_mem
I don't get crashes myself, but the system freezes in the darkroom (with "working..." overlaid on the image) and eventually the UI becomes unresponsive and I have to `kill -9` darktable. It happens on fresh edits during UI interaction (so no intensive modules being used) and can be reliably triggered by zooming the image slightly.

We might be better to remove this option for the time being (make it do nothing, and remove it from the prefs dialog) given how close we are to the release.
I'm in Win11 with Nvidia 512.95. I've been using "memory size and transfer" and I had zero problems with GPU.

Yesterday I was exporting ~40 images that used the new GL Highlight. I monitored the GPU memory and usage and used -d perf. I was really impressed. At most the system used 4Gb of memory with the images taking at most 5.3s.  
> I don't get crashes myself, but the system freezes in the darkroom (with "working..." overlaid on the image) and eventually the UI becomes unresponsive and I have to `kill -9` darktable. It happens on fresh edits during UI interaction (so no intensive modules being used) and can be reliably triggered by zooming the image slightly.
> 
> We might be better to remove this option for the time being (make it do nothing, and remove it from the prefs dialog) given how close we are to the release.

Are the system freezes due to GPU? I had a similar issue (slow UI) on the lighttable and the "working...", but it was happening when the system was using the CPU instead of the GPU. I switched to Very Fast GPU to force the use of GPU only. 
No useful logs are output so I can't tell for certain exactly what's causing it, but it only happens when the "tune OpenCL performance" pref is set to "memory size" and not when it's set to "nothing".
In fact the nvidia 515 issues (eltoc didn't mention this but he is also using 515) seem to be linux related, maybe related to kernel interface. Unfortunately i am not able to test this in depth atm as my system doesn't offer the latest driver in "stable" yet due to other problems.

So i propose to extend the tuning menu by adding sort of good-old-headroom entries like

- 400MB, 400MB + transfer, 600MB, 600MB + transfer

so we can offer almost perfect settings also for those with unstable memory testing in an interactive way and will be able to support this hopefully in a more stable way.


Will do a suggesting pr tonight
> No useful logs are output so I can't tell for certain exactly what's causing it, but it only happens when the "tune OpenCL performance" pref is set to "memory size" and not when it's set to "nothing".

That's what aurelienpierre has observed with 510 drivers too. I have **never** observed such an issue myself, could very well be a module taking too much mem and not correctly falling back to cpu instead of proper tiling.

> Are the system freezes due to GPU? I had a similar issue (slow UI) on the lighttable and the "working...", but it was happening when the system was using the CPU instead of the GPU. I switched to Very Fast GPU to force the use of GPU only.

I observed that too. Highlights laplacian with "insane settings" tried to get the GPU but failed and on cpu it sometimes took very long (i observed minutes on 48Mpix images, absolutely crazy.) Also can be seen with D&S
> So i propose to extend the tuning menu by adding sort of good-old-headroom entries like 400MB, 400MB + transfer, 600MB, 600MB + transfer

What's the benefit of this over a separate entry for keying extra headroom manually?

> could very well be a module taking too much mem

Is this likely from the default modules? I'm not using anything heavy here.
Just one menu that can be supported later too in an easy way after the checking memory issue for 515 gets resolved.

> Is this likely from the default modules? I'm not using anything heavy here.

No - not at all. That the 515 driver problem
For me is same than @elstoc, it is not really crashing, but system kinda freezes with "working..." and you can still alt+F4 to quit, but dt is not leaving the memory. Typing _ps aux | grep darktable_ still lists it and you need to kill -9 to get rid of it.

Today I downgraded the nvidia-driver to x11-drivers/nvidia-drivers-470.129.06 and the freeze is gone (I have one pic with method, clearly freezes right away)
> Today I downgraded the nvidia-driver to x11-drivers/nvidia-drivers-470.129.06 and the freeze is gone

Yes i got that from some reports. Unfortunately to do proper tests - the is no 515 driver on fedora yet - i would have to use another distro and that me even more errorprone. So the PR as a stable solution, work on the 515 issues later.
Closing this for now
Re-opened this and renamed the title as there is another report #12030 indicating there is a problem - so far only reported on nvidia 515/516 driver series - in `void _opencl_get_unused_device_mem(const int devid)`
@jenshannoschwalm I will keep commenting in this thread instead of the other one. 

Last night I was thinking the difference between 0 and 1 in the headroom could be something like a division by zero. 

I think the other user was @AxelG-DE . I think he just downgraded and kept the old drivers. I will downgrade, test and then do a clean install of the 516 version to test again.

I have multiple - d opencl from yesterday but not -verbose. I can do that. But maybe next week since I have too many commitments this weekend.

I started to read the release notes from Nvidia for 516 and found some interesting changes and the direction they are going.  Sections 2.7 and 2.8 - https://us.download.nvidia.com/Windows/516.40/516.40-win11-win10-release-notes.pdf
I had a small amount of time to look into this today. -d opencl -d verbose with headroom 0 and headroom at 1 looked identical. I'm still in same build darktable 3.9.0+1752

I then tried using d-all and the only difference using the same image is when darktable is closing. With headroom=0 it stops here:
14.206251 [db maintenance] checking for maintenance, due to rule: 'on close'.
14.206545 [db maintenance] main: [0/826 pages], data: [4/92 pages].
14.206565 [db backup] checking snapshots existence.
14.207598 [db backup] found file: library.db-pre-3.9.0.
14.207670 [db backup] found file: library.db-snp-20220505170344.
14.207732 [db backup] found file: library.db-snp-20220512204238.
14.207793 [db backup] found file: library.db-snp-20220521194446.
14.207852 [db backup] found file: library.db-snp-20220529215401.
14.207911 [db backup] found file: library.db-snp-20220606160338.
14.207969 [db backup] found file: library.db-snp-20220613205105.
14.208467 [db backup] last snap: 20220613205108; curr date: 20220619223205.
14.208546 [undo] clear list for 2047 (length 0)
14.208565 [undo] clear list for 2047 (length 0)
14.333534 [undo] clear list for 2047 (length 0)
14.333560 [undo] clear list for 2047 (length 0)

With headroom=1
8.216293 [undo] clear list for 2047 (length 0)
8.216306 [undo] clear list for 2047 (length 0)
8.295393 [undo] clear list for 2047 (length 0)
8.295419 [undo] clear list for 2047 (length 0)
 [opencl_summary_statistics] device 'NVIDIA GeForce RTX 3060' (0): peak memory usage 1862936176 bytes (1776.6 MB)
 [opencl_summary_statistics] device 'NVIDIA GeForce RTX 3060' (0): 449 out of 449 events were successful and 0 events lost. max event=256
 [opencl_summary_statistics] device 'gfx90c' (1): peak memory usage 0 bytes (0.0 MB)
 [opencl_summary_statistics] device 'gfx90c' (1): NOT utilized

I noticed that with headroom=0, darktable stays running in the background(task manager) using ~260mb of memory and no CPU/GPU. I kill the task and I can run darktable again (database not locked). 

I will try downgrading the nvidia drivers tomorrow or anything else you want me to try. 
Thanks for your efforts! I would appreciate if you could post full log output for both cases: headroom=0 and headroom=400 

Your comments hint to some error while doing opencl cleanup, something i don't understand atm, so i am interested what happens here. -d opencl -d verbose are enough i think
I used the same image. I turned on/off the highlight recovery module with GL using 4 iterations just to force some GPU usage. 

headroom=1
https://pastebin.com/CyBvVsN3

headroom=400
https://pastebin.com/kZzXTF5m

headroom=0
https://pastebin.com/09MQqhdB
Thanks, so far i didn't find a clue what's going wrong...
Just to let you know, have installed 515 here, same issue. Will track this down to understanding and possibly fix. 
Just to share my current observations: 

I've been trying different things to see if I can cause a change. I think I did manage to make it worst via changing the kernel compile options (-cl-fast-relaxed-math). I tried different ones and removing them all (blank). With blank, when I get into the darkroom, it doesnt process and I need to kill the program. I thought I had success when I used -cl-opt-disable, but it fails to compile the kernel, so no GPU.

Most of the time I can work with darktable, but I only fail to close the program.  I have plenty of system memory (16Gb) and GPU memory (12Gb). From all the tests Ive done, I'm not using even a third of the memory.

But this morning I did try something else. I exported an image from the lighttable view and it exported without a problem (on GPU) and darktable did close. If I go into the darkroom, then I get the system to slow down and fail to close. 
@jenshannoschwalm FYI Nvidia released a new driver today, 516.59. I'm on 4.1.0+19~g387e05538 using memory size on and large resources. I modified darktablerc to:

cldevice_v4_nvidiageforcertx3060_id0=0 and the problem returns.
cldevice_v4_nvidiageforcertx3060_id0=1 and no problems. Of course 400 is also no problem. 

Thanks @gi-man for keeping an eye on this :-) I also have continued to analyse the issue, so far i know exactly how to reproduce the issue but working on the involved code lead to nothing yet.

What happens is
1. of course you have to set `cldevice_v4_nvidiageforcertx3060_id0=0` as that is exactly the condition triggering.
2. If so the code can correctly find (an approximation) of the used memory on the device
3. The code in question correctly deallocates all buffers

Now the story gets somehow bad - leading to dt hanging
1. If the pipeline has to allocate a new cl buffer/2d image as in- or output that goes well - a non-NULL val is returned without an error
2. If i try to copy to the new buffer that function never returns so dt will hang

Why is this so - i don't know yet. The 515/6 drivers have a new cuda behind OpenCl, thats likely the reason. But: Why does cpu fallback works in this scenario? I seems to be quite similar as we also allocate buffers and start kernels operating on them, the pipeline never stops while allocation but while executing a kernel but no allocation problem afterwards. ???

I spent a huge amount of time on this, learning many things about CL but substancially leading to nothing on this specific problem. I ordered myself 2 weeks off from this topic and might then try to contact one of nvidias devs on this. 

 
The part that has me thinking the most was the export via the lighttable vs darkroom. The export is a full use of the pipeline and it processing the image in GPU, but no crash or issue if I export from the lighttable without ever going into the darkroom. For now, the headroom greater than 0 works, so no big deal. The performance in my system is working pretty good. 
Ive been using Fedora 36 on my system, but I noticed that nvidia updated the windows driver to 516.94. I switched to windows to test it. It continues to crash if set to zero. 
Thanks. I will reinvestigate later 
I did have the problem you described in the issue, with 515 but not 510. But for me, it's not just darktable, the whole Ubuntu 22.04 gnome-shell froze. The mouse cursor can still move but nothing else on the screen, music still plays, I can still switch to another terminal/console (Ctrl-Alt-F3). When switching back using Ctrl-Alt-F1 or Ctrl-Alt-F2, it gets me to the login screen, and after logging in it's just a black screen with the mouse cursor can still move around. Tried killing darktable from Ctrl-Alt-F3 terminal doesn't help. The only solution for me is to reboot. I also disabled all gnome extensions and deleted ~/.config/darktable and ~/.cache/darktable to start fresh. 

I'm not sure if I should create a new issue, but with 515, I also found the above system freeze happens more often at the end of exporting multiple files (no OpenCL tuning). It has never happened in the middle of exporting, e.g. it can export 7 out of 8 photos just fine, but once it finishes exporting the last one, the freeze would happen. With 510, it happens maybe once every 2-3 weeks, but with 515, it's  once or twice a day. Let me know if there's anything you want me to try isolating since I likely can reproduce it somewhat consistently.

My system is Lenovo Legion 5 Pro, Ryzen 5800H, RTX 3060 6GB on Ubuntu 22.04, 32GB RAM. I pull and compile from master branch every one or two weeks.
I think you should start a new issue for this and try to post some of the log. I've been using nvidia 515 of fedora 36 kde x11 on master for a while without these issues.
@hqhoang absolutely no idea, I didn't have a single dt crash for months now and do I lot of Heav testing new opencl stuff.

The last issues I saw were related to dt trying to use so much of the graphics ram that likely some other apps got problems. Not sure about kde, I don't use it.

Also I am not aware of any other reports like yours right now.

As it happens so often it might be worth to always run dt with - d opencl -d tiling - verbose and open an issue with the log. That might be pretty long but might help.

Or maybe run dt under gdb with the above debug options.

A new issue with that log would likely help. But - no idea right now from my side. 