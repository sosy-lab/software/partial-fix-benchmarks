severe performance degradation 
My current library contains >42,000 images
Could you precise which kernel you use on your Linux system ? I have had some severe performance degradation solved by just upgrade my 5.3.0 kernel (on Ubuntu 19.10) by upgrading to a 5.4.11 kernel (I now use a 5.4.13 with same good performances as the 5.4.11).
And which settings do you have in core options, CPU/GPU/Memory parameters on darktable preferences window ?
kernel-default-5.4.7-1.1.x86_64
memory in megabytes to use for thumbnail cache:  512
number of background threads:  8
host memory limit (in MB) for tilling:  18040
minimum amount of memory (in MB) for a single buffer in tilling:  16

fwiw:  I installed darktable-3.0.0 and a library from backup dated 2019-1130 an observe normal operation, ie: no delays on editing operations.

Ah, so I'm not the only one. Same here, GUI events seem very slow.
I only see the slow-down or delays when editing and only editing actions, not changing images or changing from darkroom to lighttable or back or increasing/decreasing image display size.

But the delays make "master" un-usable.
I can run a debug session of dt while operating in an edit session where I experience the delays if that would help.
i found having a lot of drawn masks (e.g. used for retouch) results in a massive slowdown. Log file is full f processing masks ...

I do have some drawn masks but they exist in the library accessed by dt-3.0.0 and the library accessed by darktable 3.1.0~git426.88bf11bd2, but dt-3.0.0 does not exhibit the crippling delays exhibited in git-master builds.  And the library used by dt-3.0.0 has >300k images.
> I can run a debug session of dt while operating in an edit session where I experience the delays if that would help.

What would certainly help understanding and fixing this issue is to find the first commit exhibiting this slowdown.
I just cleaned my prior versions, earliest master I have is darktable-3.1.0~git360.6d897ea0e and it exhibits the editing delay.  So it was prior to 6d897ea0e.  Perhaps Darix has some earlier but I doubt it.
If someone would build the binaries (+ debuginfo), I can quickly test for you.

openSUSE Tumbleweed 20200121
NVIDIA GF106 [GeForce GTS 450], 390.129
OpenCL on 

tks
fwiw: latest master, darktable-3.1.0~git430.89b003f72, has the same problem/delays

@ptilopteri 
Some library combinations related to GMIC have been as performance breaker (#4114 ).
Have you tried to build dt having uninstall libgmic-dev (to get "GMIC not found" running cmake) to see if you get any difference ?



@phweyland 
I do not build but use binaries built by darix's OBS for master on openSUSE Tumbleweed,
https://download.opensuse.org/repositories/home:/darix:/darktable:/master/openSUSE_Tumbleweed/

tks

@phweyland 
I do not build but use binaries build by darix's OBS for master on openSUSE Tumbleweed,
https://download.opensuse.org/repositories/home:/darix:/darktable:/master/openSUSE_Tumbleweed/

tks

As a small test you could try to locate the file `liblut3d.so` in your darktable installation. Just rename it so something like `liblut3d.so.sav` then start darktable and test the performance. You won't be able to use the 3DLut feature during this. If your tests are finished rename to file back to its original name.
@upegelow 
moved /usr/lib64/darktable/plugins/liblut3d.so to /usr/lib64/darktable/plugins/xxliblut3d.so
started dt from cl
no complaint about missing libut3d.so or anything else
opened image, initiated crop module 
see same delay, cannot move crop selection or alter crop selection until cursor location appears on screen.  And if I then move or alter the crop selection, I have to wait again until the cursor location appears on screen before making another change.
Closed dt, no text appeared on xterm instance where dt was started.

If I begin with a new empty library, dt does not exhibit the editing delay
If I import < 200 images, the delay is not noticable
If I import > 400 images (total in library) the editing delay becomes noticable and eventually the delay is prohibitive.

Maybe to do with the new history?
Is there a lib I can rename to disable the new history functions?

tks
> see same delay,

OK, then we can exclude that it's the same issue or closely related to #4114.
This is an incorrect way to test if gmic affects the issue because it's libdarktable.so who is linked to gmic, not liblut3d.so (which is wrong, but it's another question).
> This is an incorrect way to test if gmic affects the issue because it's libdarktable.so who is linked to gmic,

Interesting. Here gmic is linked in `liblut3d.so`. But that's with the current master. So it seems to have changed.
Only to liblut3d.so and not to libdarktable.so? ldd shows all dependencies, including indirect ones, and all iops are linked to libdarktable.so. lddtree will give you more information on the linkage.
> Only to liblut3d.so and not to libdarktable.so?

Well, at least `ldd libdarktable.so | egrep gmic` is empty, while it shows the link in `liblut3d.so`. lddtree is not available here.
OK, I guess --as-needed linker flag is at work here, which is good. Then it is indeed a correct test. Though to be safe I would also move liblut3d.so completely out of iop directory, not just rename it.
fwiw:  I see the same slow-down with 3.1.0~git449.c5f14d958, but not with 3.0.0~git325.4538201be
@ptilopteri : do you have the same slowdown for all collection kind and sort filter ?

BTW, if you are in a position to update the database, can you try adding an index on the film_rolls as follow:

```
$ sqlite3 ~/.config/darktable/library.db
sqlite> create index film_rools_id_index on film_rolls(id);
```

And then check if the slowdown is still there.
tks, doesn't appear to have made any difference

Let's recap a bit:
- the slowdown is proportional to the number of images in the db
- the 3.0 release do not have this issue
- the 6d897ea (Jan 14th) exhibit the issue

So the regression is between Dec 25th and Jan 14th.

Lot if changes during this period. But since it is proportional to the number of images in the db it could be related to a missing index ? Maybe this is reproducible on your side because you have many tags or metadata?

Let's get some numbers, what are the output for:

```
echo "select count(*) from images" | sqlite3 ~/.config/darktable/library.db

echo "select count(*) from film_rolls" | sqlite3 ~/.config/darktable/library.db

echo "select count(*) from tagged_images" | sqlite3 ~/.config/darktable/library.db

echo "select count(*) from meta_data" | sqlite3 ~/.config/darktable/library.db

echo "select count(*) from history" | sqlite3 ~/.config/darktable/library.db
```
Just another try, can you create the following index on tagged_images and test again:

```
$ sqlite3 ~/.config/darktable/library.db
sqlite> CREATE INDEX tagged_image_img_idx on tagged_images (imgid);
```
I have another doubt but so far I haven't been able to demonstrate it.
I've observed some events triggered more often I would have expected. I think that sometimes, dt can execute some part of code more often than required.
Maybe we should install some counters on internal dt events ? Then we would able to compare them with the db numbers.

> Let's recap a bit:
> 
>     * the slowdown is proportional to the number of images in the db
> 
>     * the 3.0 release do not have this issue
> 
>     * the [6d897ea](https://github.com/darktable-org/darktable/commit/6d897ea0eca02b7f550e6814006fb51f419c2100) (Jan 14th) exhibit the issue
> 
> 
> So the regression is between Dec 25th and Jan 14th.
> 
> Lot if changes during this period. But since it is proportional to the number of images in the db it could be related to a missing index ? Maybe this is reproducible on your side because you have many tags or metadata?
> 
> Let's get some numbers, what are the output for:
> 
> ```
> echo "select count(*) from images" | sqlite3 ~/.config/darktable/library.db
128282
 
> echo "select count(*) from film_rolls" | sqlite3 ~/.config/darktable/library.db
1202
 
> echo "select count(*) from tagged_images" | sqlite3 ~/.config/darktable/library.db
1162519
 
> echo "select count(*) from meta_data" | sqlite3 ~/.config/darktable/library.db
499569

> echo "select count(*) from history" | sqlite3 ~/.config/darktable/library.db
1082760

> Just another try, can you create the following index on tagged_images and test again:
> 
>
> $ sqlite3 ~/.config/darktable/library.db
> sqlite> CREATE INDEX tagged_image_img_idx on tagged_images (imgid);

open darktable -d sql
open existing edited image in darkroom with crop selection active
click mouse inside selected crop area to reposition 
delay from 65.916811 till 68.553314 before crop area can be moved

try new virgin image 
activate crop module
click mouse in crop border to drag
447.344398 until 449.212535 before border will move.

If I start dt:
  darktable --library :memory: 
I can operate normally, can move crop border immediately

delay indicated above is also present for sharpening and dragging the histogram and color balance and ...

tks

Ok, so you have big db indeed and lot of tags !

If you have a good backup can we try removing all tags just to see ?

`echo "delete from tagged_images" | sqlite3 ~/.config/darktable/library.db`

Also you did not answered my question about the current collection filters?
> Ok, so you have big db indeed and lot of tags !
> 
> If you have a good backup can we try removing all tags just to see ?

yes, I separate work from experimenting

> `echo "delete from tagged_images" | sqlite3 ~/.config/darktable/library.db`
> 
> Also you did not answered my question about the current collection filters?

didn't see it or didn't understand.

The current group consists of two images in a separate directory with no other filters.  Does this answer your question?

openSUSE Tumbleweed 20200128
NVIDIA GF106 [GeForce GTS 450], 390.129
darktable 3.1.0~git453.e30fb18d9
darktable-3.0.0-182
OpenCL on 
i7 12-core 36GB

Ran the "delete from tagg.."
initiated: darktable -d sql --library ~/.config/darktable/library.db

performed operations related earlier and see normal/expected performance.  The delay is gone.

What should I do next?

tks
> performed operations related earlier and see normal/expected performance. The delay is gone.

Perfect ! Let me review the SQL commands to see what is done during this operation. We probably miss an index somewhere.
Do you want me to retain the added indicies from previous?
To complete information related to main.tagged_images.
On PR #3361 I've removed the update of main.used_tags table as it was not used anywhere, but I've missed to remove the same table from dt. This has been done on PR #4112.
The main consequence is that main.tagged_images is not recreated any more at start time as when main.used_tags and data.tags were de-synchronized.


@ptilopteri : No, do not retain the index it is of no use given your testing. What would help a lot is this:

- start dt from command line with -d sql
- go to darkroom and make the GUI ready for the slow action
- on the console enter many time ENTER key to separate the group of SQL that will come
- on darkroom to the slow action, only one will suffice
- on the console copy the displayed commands since the empty lines added above

And put the commands here. It will help to find the slow command. Thanks.
>     * start dt from command line with -d sql
>     * go to darkroom and make the GUI ready for the slow action
>     * on the console enter many time ENTER key to separate the group of SQL that will come
>     * on darkroom to the slow action, only one will suffice
>     * on the console copy the displayed commands since the empty lines added above

done, with backup library from before indicies added.
http://wahoo.no-ip.org/~paka/darktable.sql.1.txt

I only made one attempt but the delay was obvious.  Will repeat if necessary.

tks
In tagging there are 3 dictionary views. The suggestions view (+ button activated on the right of export button), inherited from former tagging, a list view (+ button deactivated) and tree view (toggle button on the right of + button). The first one is tiggered on external changes (including dev events), not the last two ones.

Do you still have the delay when you deactivate the + button ?

@phweyland : One of the culprit is a 3 JOIN request:

```
INSERT INTO memory.similar_tags (tagid) 
SELECT DISTINCT TI.tagid FROM main.selected_images AS S 
JOIN main.tagged_images AS TI ON TI.imgid = S.imgid 
JOIN data.tags AS T ON T.id = TI.tagid WHERE TI.tagid NOT IN memory.darktable_tags
```

Do you think we can do better ? The first JOIN is superfluous no? Don't this is even equivalent to:

```
INSERT INTO memory.similar_tags (tagid) 
SELECT DISTINCT TI.tagid FROM main.selected_images AS S, main.tagged_images AS TI, data.tags AS T
WHERE TI.imgid = S.imgid AND T.id = TI.tagid AND TI.tagid NOT IN memory.darktable_tags
```
My SQL is a bit rusty but this looks correct.

Also we are missing some primary key on memory table. Will do a PR for that.
The query you suggest looks ok for me too. Internally it does make the same join. Not sure it would be faster.

I suggest also to move the default tagging view to list or tree view. I let the suggestions view by default, when I've made the changes on tagging, to keep the former behavior. But I think this was not a good idea.
 
@ptilopteri : don't remember... are you in a position to build from source a given branch ?

Alternatively if you can give me access to you library.db and data.db I may reproduce the issue myself and check different options. This would certainly be faster. Let me know what you prefer.
> In tagging there are 3 dictionary views. The suggestions view (+ button activated on the right of export button), inherited from former tagging, a list view (+ button deactivated) and tree view (toggle button on the right of + button). The first one is tiggered on external changes (including dev events), not the last two ones.
> 
> Do you still have the delay when you deactivate the + button ?

Not sure I understand the question.  I have never bothered with tagging other than to assign or remove tags for identifying images.

If you will give more direction, maybe I can test.

> @ptilopteri : don't remember... are you in a position to build from source a given branch ?

Sorry, no.  I use darix's openSUSE OBS master builds.

> Alternatively if you can give me access to you library.db and data.db I may reproduce the issue myself and check different options. This would certainly be faster. Let me know what you prefer.

that will work,
  http://wahoo.no-ip.org/~paka/library.db
  http://wahoo.no-ip.org/~paka/data.db

the log file you have shared says your tagging module is configured by default on the suggestions view. So just disable the + button in tagging module and try to reproduce the delay issue.

![image](https://user-images.githubusercontent.com/23012047/73685274-26fff900-46c6-11ea-8568-8a72c1253d5a.png)

@ptilopteri : can you give me the id of one image that causes issue?
@TurboGit 
they all do but the one I have been testing is 308203

@phweyland 
You have it.  I have never bothered with that parameter, but it removes nearly all the delay.  All remaining is a slight hesitation.

tks
@ptilopteri : I have merged another fix which should speed up things.
@ptilopteri : And yet another fix which should also speed-up things. At this stage I'm closing this issue as I'm confident that we are back to normal speed.
I still see a slight "hesitation" but it appears to be the screen reacting slightly later than the mouse action, if that makes sense.  But darktable is now functional for me where I could not use it before.

tks much
@TurboGit 
REOPEN, the change from git546.0d7f85814 to git547.b447df7c8 brings back the same or similar delay.
> the change from git546.0d7f85814 to git547.b447df7c8 brings back the same or similar delay.

The change here is only removing an SQL statement which was not used and quite CPU intensive. So are you sure you are testing the right thing ?
@TurboGit well, maybe ...
I again have the delay when trying to move the crop selected area
Will run -d sql and come back

delay now is:  35.883866 to 36.675241
not nearly as dramatic but noticeably more

opened same image and tried to move the crop selected area
selected area will not move unless I hold the mouse button until the cursor location appears on screen

> delay now is: 35.883866 to 36.675241

Could you attach the log file ?
@phweyland 
log of delay

27.727797 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:208, function dt_tag_new(): prepare "SELECT id FROM data.tags WHERE name = ?1"
27.727976 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:625, function dt_set_darktable_tags(): prepare "SELECT COUNT(*) FROM memory.darktable_tags"
27.728045 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:894, function dt_tag_get_tags(): prepare "SELECT DISTINCT T.id FROM main.tagged_images AS I JOIN data.tags T on T.id = I.tagid WHERE I.imgid = 308203 AND T.id NOT IN memory.darktable_tags"
27.728282 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:111, function _bulk_add_tags(): prepare "INSERT INTO main.tagged_images (imgid, tagid) VALUES (308203,2)"
27.728419 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:785, function _dt_collection_compute_count(): prepare "SELECT COUNT(DISTINCT mi.id) FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version LIMIT ?1, ?2"
27.796027 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:785, function _dt_collection_compute_count(): prepare "SELECT COUNT(DISTINCT mi.id) FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version LIMIT ?1, ?2"
27.858857 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:785, function _dt_collection_compute_count(): prepare "SELECT COUNT(DISTINCT mi.id) FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version"
27.920421 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:785, function _dt_collection_compute_count(): prepare "SELECT COUNT(DISTINCT mi.id) FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version"
27.982499 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:834, function dt_collection_get(): prepare "SELECT id FROM main.selected_images AS s JOIN (SELECT DISTINCT mi.id FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version) AS mi WHERE mi.id = s.imgid LIMIT -1, ?3"
28.043963 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:1724, function dt_collection_image_offset_with_collection(): prepare "SELECT DISTINCT mi.id FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version"
28.106569 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/collection.c:813, function dt_collection_get_selected_count(): prepare "SELECT COUNT(*) FROM main.selected_images"
28.106683 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:738, function _update_collected_images(): exec "DELETE FROM memory.collected_images"
28.106720 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:741, function _update_collected_images(): exec "DELETE FROM memory.sqlite_sequence WHERE name='collected_images'"
28.106771 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:749, function _update_collected_images(): prepare "INSERT INTO memory.collected_images (imgid) SELECT DISTINCT mi.id FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version LIMIT ?1, ?2"
28.169750 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:759, function _update_collected_images(): prepare "SELECT MIN(rowid) FROM memory.collected_images"
28.169823 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:821, function _update_collected_images(): prepare "SELECT imgid FROM memory.collected_images ORDER BY rowid LIMIT ?1, ?2"
28.169900 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:738, function _update_collected_images(): exec "DELETE FROM memory.collected_images"
28.169932 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:741, function _update_collected_images(): exec "DELETE FROM memory.sqlite_sequence WHERE name='collected_images'"
28.169979 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:749, function _update_collected_images(): prepare "INSERT INTO memory.collected_images (imgid) SELECT DISTINCT mi.id FROM images AS mi WHERE   (flags & 256) != 256  AND  (1=1 AND (film_id IN (SELECT id FROM main.film_rolls WHERE folder LIKE '/home/paka/Pictures/2020/candids/2020.0119.play'))) ORDER BY filename, filename , version LIMIT ?1, ?2"
28.231489 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:759, function _update_collected_images(): prepare "SELECT MIN(rowid) FROM memory.collected_images"
28.231560 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:821, function _update_collected_images(): prepare "SELECT imgid FROM memory.collected_images ORDER BY rowid LIMIT ?1, ?2"
28.231618 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/views/lighttable.c:557, function _view_lighttable_collection_listener_internal(): prepare "SELECT rowid FROM memory.collected_images WHERE imgid = 308203"
28.233998 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/libs/collect.c:1528, function list_view(): prepare "SELECT folder, film_rolls_id, COUNT(*) AS count FROM main.images AS mi JOIN (SELECT id AS film_rolls_id, folder       FROM main.film_rolls)   ON film_id = film_rolls_id  WHERE (1=1) GROUP BY folder ORDER BY film_rolls_id DESC"
28.392613 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/libs/tools/timeline.c:624, function _time_read_bounds_from_collection(): prepare "SELECT db.datetime_taken FROM main.images AS db, memory.collected_images AS col WHERE db.id=col.imgid AND LENGTH(db.datetime_taken) = 19 AND db.datetime_taken > '0001:01:01 00:00:00' ORDER BY db.datetime_taken ASC LIMIT 1"
28.392759 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/libs/tools/timeline.c:645, function _time_read_bounds_from_collection(): prepare "SELECT db.datetime_taken FROM main.images AS db, memory.collected_images AS col WHERE db.id=col.imgid AND LENGTH(db.datetime_taken) = 19 AND db.datetime_taken > '0001:01:01 00:00:00' ORDER BY db.datetime_taken DESC LIMIT 1"
28.393266 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/libs/duplicate.c:372, function _lib_duplicate_init_callback(): prepare "SELECT i.version, i.id, m.value FROM images AS i LEFT JOIN meta_data AS m ON m.id = i.id AND m.key = ?3 WHERE film_id = ?1 AND filename = ?2 ORDER BY i.version"
28.439046 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/image.c:176, function dt_image_film_roll(): prepare "SELECT folder FROM main.film_rolls WHERE id = ?1"
28.439215 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/image.c:219, function dt_image_full_path(): prepare "SELECT folder || '/' || filename FROM main.images i, main.film_rolls f WHERE i.film_id = f.id and i.id = ?1"
28.439461 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/metadata.c:305, function dt_metadata_get_xmp(): prepare "SELECT value FROM main.meta_data WHERE id = ?1 AND key = ?2 ORDER BY value"
28.439537 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/metadata.c:305, function dt_metadata_get_xmp(): prepare "SELECT value FROM main.meta_data WHERE id = ?1 AND key = ?2 ORDER BY value"
28.439633 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/metadata.c:305, function dt_metadata_get_xmp(): prepare "SELECT value FROM main.meta_data WHERE id = ?1 AND key = ?2 ORDER BY value"
28.439725 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:625, function dt_set_darktable_tags(): prepare "SELECT COUNT(*) FROM memory.darktable_tags"
28.439758 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:657, function dt_tag_get_attached(): prepare "SELECT DISTINCT T.id, T.name, T.flags, T.synonyms, 1 AS inb FROM main.tagged_images AS I JOIN data.tags T on T.id = I.tagid WHERE I.imgid = 308203 AND T.id NOT IN memory.darktable_tags ORDER BY T.name"
28.439833 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:1195, function dt_selected_images_count(): prepare "SELECT count(*) FROM main.selected_images"
28.439988 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:625, function dt_set_darktable_tags(): prepare "SELECT COUNT(*) FROM memory.darktable_tags"
28.440022 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:657, function dt_tag_get_attached(): prepare "SELECT DISTINCT T.id, T.name, T.flags, T.synonyms, 1 AS inb FROM main.tagged_images AS I JOIN data.tags T on T.id = I.tagid WHERE I.imgid = 308203 AND T.id NOT IN memory.darktable_tags ORDER BY T.name"
28.440086 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:1195, function dt_selected_images_count(): prepare "SELECT count(*) FROM main.selected_images"
28.440297 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:625, function dt_set_darktable_tags(): prepare "SELECT COUNT(*) FROM memory.darktable_tags"
28.440325 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:1226, function dt_tag_get_with_usage(): prepare "INSERT INTO memory.taglist (id, count) SELECT tagid, COUNT(*) FROM main.tagged_images GROUP BY tagid"
28.527198 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:1195, function dt_selected_images_count(): prepare "SELECT count(*) FROM main.selected_images"
28.527267 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:1237, function dt_tag_get_with_usage(): prepare "SELECT T.name, T.id, MT.count, CT.imgnb, T.flags, T.synonyms FROM data.tags T LEFT JOIN memory.taglist MT ON MT.id = T.id LEFT JOIN (SELECT tagid, COUNT(DISTINCT imgid) AS imgnb FROM main.tagged_images WHERE imgid IN (SELECT imgid FROM main.selected_images) GROUP BY tagid) AS CT ON CT.tagid = T.id WHERE T.id NOT IN memory.darktable_tags ORDER BY T.name "
28.527638 [sql] /home/abuild/rpmbuild/BUILD/darktable-3.1.0~git547.b447df7c8/src/common/tags.c:1270, function dt_tag_get_with_usage(): exec "DELETE FROM memory.taglist"


What I can see:
- total time 0,8 sec
- tag time 0.1 sec
- collection time 0.65 sec

Globally that's better than before, isn't it ?

How many images in "2020.0119.play" ? 
What is the collect filter you have ? 

We have a join on collected image with imgid :

`
SELECT db.datetime_taken FROM main.images AS db, memory.collected_images AS col WHERE db.id=col.imgid AND LENGTH(db.datetime_taken) = 19 AND db.datetime_taken > '0001:01:01 00:00:00' ORDER BY db.datetime_taken ASC LIMIT 1`

But no index on col.imgid so may take some time.
> But no index on col.imgid so may take some time.

first time 0.0001 and second time 0.0005 sec ... not the biggest chunk. What is strange is that the same query is executed twice and the second one is longer (should be in cache, if any, and then faster).

Not sure that has any impact but having twice `filename` may not be necessary:
`ORDER BY filename, filename , version`

Other point. If I'm not mistaken, each collection's query is executed twice...



> What I can see:
> 
>     * total time 0,8 sec
> 
>     * tag time 0.1 sec
> 
>     * collection time 0.65 sec
> 
> 
> Globally that's better than before, isn't it ?
> 
> How many images in "2020.0119.play" ?
2
> What is the collect filter you have ?
only the folder


> 2

!

@turbogit . That's not the solution for that issue, but what is the reason to let trig collection when in darkroom ? Collection should discard the signals if not in lighttable, shouldn't it ?

@phweyland : can you have a look at  #4230?
The largest slow down was the tag-changed signal being raise for every history change due to adding the darktable|changed tag even if it was already present.
@phweyland : it's needed for filmstrip, I think
This should now be fixed. Note that for the lighttable to be more responsive you need to deactivate the display of overlay information. In the darkroom there is no more delay at all.
@TurboGit 
I still have the delay.  Admittedly much improved over earlier, but still definite and disturbing delay.

doing the same action with
  darktable -d sql --library :memory: ./Pictures/some-files
is:  33.226116 33.235922,   0.009806
vs
  darktable -d sql --library :memory: ./Pictures/some-files
is:  34.678946 35.492971,  0.814025

operation was moving predetermined crop area to different location

using library=:memory, action was instantaneous

using library=library.db, trying to move the crop area failed until the screen displayed the cursor location

still quite a dramatic and disturbing difference

tks
@TurboGit 
Updated to git553.ff2862c6b
and indeed the delay appears completely gone
GREAT

tks much for efforts and for bearing with me.
And I have just pushed another fix when overlay is activated. Fact was that many index in the DB were removed during the last migration.