Add xcf export format
Correct reload_defaults in colorin module fixing #4106

The issues descibed in #4106 #4108 #4343 https://discuss.pixls.us/t/jpegs-in-adobergb-too-little-saturated/15785
all are due to incorrect setting of the color profile in reload_defaults.

If there is a known profile (either unknown so far and found by inspecting the file **or** if already
existing in img->profile) we must apply that before the other tests.
Correct reload_defaults in colorin module fixing #4106

The issues descibed in #4106 #4108 #4343 https://discuss.pixls.us/t/jpegs-in-adobergb-too-little-saturated/15785
all are due to incorrect setting of the color profile in reload_defaults.

If there is a known profile (either unknown so far and found by inspecting the file **or** if already
existing in img->profile) we must apply that before the other tests.

(cherry-picked with conflicts resolution).
