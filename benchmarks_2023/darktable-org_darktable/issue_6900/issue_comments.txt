Jpegs in Darkroom show a "failed to read camera white balance information from"...
That fix broke whitebalance for subsampled raws (see canon m-raw/s-raw, e.g. https://raw.pixls.us/data/Canon/EOS%205D%20Mark%20III/5G4A9395.CR2).
```
$ git bisect log
git bisect start
# bad: [955c98e6bb1262d08becfe0ce81226ff8be229c9] Panasonic DC-S5 color matrix. Refs. #6351.
git bisect bad 955c98e6bb1262d08becfe0ce81226ff8be229c9
# good: [258ba57574ec18ef0778eb5da0c73af73db7ab26] [tools] check_camera_support: do close opened handles
git bisect good 258ba57574ec18ef0778eb5da0c73af73db7ab26
# good: [a81599309b9df47fb863b900c89170db0e1feb9b] Handle adding/removing locked_camera list
git bisect good a81599309b9df47fb863b900c89170db0e1feb9b
# skip: [a3f41a4f6d25653fac414a0caabc773c483424e9] iop_layouts: let room for future settings in presets
git bisect skip a3f41a4f6d25653fac414a0caabc773c483424e9
# skip: [c5880160af7103fa8f1f3d7ade263569a16bf7d9] iop layouts : fix glitches in manager dialog
git bisect skip c5880160af7103fa8f1f3d7ade263569a16bf7d9
# skip: [addb94be3b39035567a9751f0053552e3ecbfe48] modulegroups: add 3 tabs preset based on Aurélien proposal in #5322.
git bisect skip addb94be3b39035567a9751f0053552e3ecbfe48
# skip: [c31f8973b93bb1ce8cfe277edcca4419a1a54c4b] iop_layout: reformating
git bisect skip c31f8973b93bb1ce8cfe277edcca4419a1a54c4b
# skip: [03d6154a4b2c036fb7f194624aade32c1ec3ac1d] color picker proxy: minor code refactoring.
git bisect skip 03d6154a4b2c036fb7f194624aade32c1ec3ac1d
# good: [cbb2b1dbfcea60d65ae30c692504e27c8fbb014e] Merge branch 'po/conf_gen' into master
git bisect good cbb2b1dbfcea60d65ae30c692504e27c8fbb014e
# bad: [82a348cd38d9d7f1cc0df972e4b1adaf032a851a] modulegroups: add channle mixex in display referred preset.
git bisect bad 82a348cd38d9d7f1cc0df972e4b1adaf032a851a
# bad: [864bd74a8659843e8bfa24843bd81e5f0417ee5e] Merge pull request #6445 from johnny-bit/conf_hardened
git bisect bad 864bd74a8659843e8bfa24843bd81e5f0417ee5e
# bad: [79df2078e37899c22912d88ebd37ccd39985b825] iop layout : retrieve old layout if needed
git bisect bad 79df2078e37899c22912d88ebd37ccd39985b825
# bad: [5aacde6b83b76436d8a1b0b6681bb36585eaefc0] Merge pull request #6425 from jenshannoschwalm/pref_ppd_new
git bisect bad 5aacde6b83b76436d8a1b0b6681bb36585eaefc0
# good: [49313311df2c7997e6c7191187e40bc4332647f5] Merge pull request #6398 from johnny-bit/issue_1994_hide_iop_presets
git bisect good 49313311df2c7997e6c7191187e40bc4332647f5
# good: [79a0d3bd3dd07b4274e1e586d915635cba245bac] Update French translation.
git bisect good 79a0d3bd3dd07b4274e1e586d915635cba245bac
# bad: [179c0ca4d11df3d395dd25271fe385b6799158bf] More simplified preferences interface.
git bisect bad 179c0ca4d11df3d395dd25271fe385b6799158bf
# bad: [670db7fdd7f55363a792ebc4692e0d42ffb93336] Simplyfied approach for `screen_ppd_overwrite` and `ui/cairo_filter`
git bisect bad 670db7fdd7f55363a792ebc4692e0d42ffb93336
# first bad commit: [670db7fdd7f55363a792ebc4692e0d42ffb93336] Simplyfied approach for `screen_ppd_overwrite` and `ui/cairo_filter`
# bad: [201ef7ef3644a6d32540873a013c43303e243b75] RawSpeed submodule update: SONY ILCE-7C and Panasonic DC-GX7MK3 camera support
git bisect bad 201ef7ef3644a6d32540873a013c43303e243b75
# good: [c6dc8ec179f6a461e47001afefbe02bda0858c01] Updates to pt_BR (user manual)
git bisect good c6dc8ec179f6a461e47001afefbe02bda0858c01
# good: [4af99394dd04e6c96ac2130eb81089814286e2ca] Merge pull request #6673 from dterrahe/cleaninit
git bisect good 4af99394dd04e6c96ac2130eb81089814286e2ca
# skip: [51f313bc94855895880b5dbdd3fc40cbe87ccc71] gradient: fix mask handling when changing rotation in creation mode.
git bisect skip 51f313bc94855895880b5dbdd3fc40cbe87ccc71
# good: [1f624b24763b45ba594adb592136dfc7f23f48ba] remove accidentally reintroduced boilerplate
git bisect good 1f624b24763b45ba594adb592136dfc7f23f48ba
# good: [35ce40a190cb916583e8c35ad43d90f60cb424c8] add some consts
git bisect good 35ce40a190cb916583e8c35ad43d90f60cb424c8
# good: [1396c9b7f8c6ce938f41a55680c144065d658c1e] export: add tooltip for intent.
git bisect good 1396c9b7f8c6ce938f41a55680c144065d658c1e
# bad: [8bbb0846f05490a3c51daf28b46fa70085595633] Correct channel display of JzCzhz hue
git bisect bad 8bbb0846f05490a3c51daf28b46fa70085595633
# bad: [8cb2a567c1987420a0597f0ae2ef6c3313848313] [histogram.c] Added ifdef to check compiler version and only include shared pragma for GCC 9+
git bisect bad 8cb2a567c1987420a0597f0ae2ef6c3313848313
# good: [e8204990d33fa44bdf5f0b6dd64444cd24d43057] Merge branch 'po/fix-circle-ellipse-arrow'
git bisect good e8204990d33fa44bdf5f0b6dd64444cd24d43057
# good: [5d9173d5d1453a077f0faccc73ddfbbdaaf45ed8] iop : add deprected message to the api
git bisect good 5d9173d5d1453a077f0faccc73ddfbbdaaf45ed8
# bad: [34ae2bf1598a1ed15d370e88f11c5da20e2f542c] Merge pull request #6915 from AlicVB/soft-deprecated
git bisect bad 34ae2bf1598a1ed15d370e88f11c5da20e2f542c
# bad: [49904d5a436ba870184d784d85ccbbcce8798222] modulegroups: add consts, and then fix some memory leaks.
git bisect bad 49904d5a436ba870184d784d85ccbbcce8798222
# bad: [27752c6b9f63ecc86453b62fd7c091d4710f886c] temperature.c : prevent D65 coeffs initialization on non-raw images
git bisect bad 27752c6b9f63ecc86453b62fd7c091d4710f886c
# first bad commit: [27752c6b9f63ecc86453b62fd7c091d4710f886c] temperature.c : prevent D65 coeffs initialization on non-raw images

```