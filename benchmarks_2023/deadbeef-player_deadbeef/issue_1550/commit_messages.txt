artwork: extract mp4 cover arts through mp4ff.
artwork: check file extension before attempting to read mp4 files (fixes #1550)
artwork: check file extension before attempting to read mp4 files (fixes #1550)
mp4ff: fixed missing stream initialization in the mp4ff_open_read_coveronly (fixes #1550)
