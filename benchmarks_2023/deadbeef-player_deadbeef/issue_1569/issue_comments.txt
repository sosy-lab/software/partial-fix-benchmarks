artwork plugin is not working on FreeBSD
@novel It's unclear why you need to do what you did.. It looks like it's enough to change the code to always use the return value of dirname.

Can you check if this patch does any good?

```
diff --git a/plugins/artwork/artwork_internal.c b/plugins/artwork/artwork_internal.c
index 3b2d663..719711a 100644
--- a/plugins/artwork/artwork_internal.c
+++ b/plugins/artwork/artwork_internal.c
@@ -123,9 +123,9 @@ int ensure_dir (const char *path)
 {
     char dir[PATH_MAX];
     strcpy (dir, path);
-    dirname (dir);
-    trace ("artwork: ensure folder %s exists\n", dir);
-    return check_dir (dir);
+    char *dname = dirname (dir);
+    trace ("artwork: ensure folder %s exists\n", dname);
+    return check_dir (dname);
 }

 #define BUFFER_SIZE 4096
```

I reproduced this issue, will provide the proper fix.
