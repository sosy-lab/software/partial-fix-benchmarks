wildmidi: replace reallocf with realloc and free (fixes #2173)
cdda: fixed a number of memory errors; fixed combining cdtext with cddb on initial read (fixes #2177)
cdda: fixed cdtext/cddb/placeholder metadata init order (fixes #2177)
