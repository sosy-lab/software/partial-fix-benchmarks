Tagging m4a or converting to m4a eats all the RAM (if > 4G)
I found 2 memory leaks, fixed both. Hope it helps.
Thanks, but unfortunately there's no improvement at all. :(
Well, I guess you weren't able to reproduce it?

I was trying to use valgrind, but at some point OOM reaper kicks in and kills DB.
The strange thing is that any subsequent use of valgrind doesn't result in the memleak anymore.
Without valgrind it's still there, though.
Some kind of a heisenbug, it seems...

EDIT: just checked and changing the metadata using VLC works ok.
EDIT 2: also works fine in DB-1.8.4.

I don't have a linux machine or a working VM at the moment.
I was running memory leaks checking in the mac version, and I found 2 which occurred during m4a tagging.
It is likely that there are more leaks that weren't detected.
It is also likely that this happens with some m4a files of specific structure, which I don't have.
I'll keep the bug open until I can verify in linux environment.
Ok, no worries.
In the meantime attaching a test m4a generated with:
`ffmpeg -f lavfi -i "sine=frequency=1000:duration=5" test.m4a`


[test.m4a.zip](https://github.com/DeaDBeeF-Player/deadbeef/files/5500911/test.m4a.zip)

Thanks for the file.
I still can't identify any new leaks (on mac), but I will take a look again as I regain access to a linux VM.
Confirmed the bug. It happens every time on linux. Virtual memory gets to 4GB and can lead to linux killing some processes. This is not normal, will be working on a fix.
Alright, confirmed the fix, thanks!