mfs: use 121f to initialize new CDS [fixes #1231]
mfs: pc-dos work-around for uniniitialized CDS [fixes #1231]

pc-dos seem to indicate the uninitialized CDS by an empty
path. The flags then contain the random values, and should
be reset to the sane default.
int,mfs: re-fix uniniitialized CDS problem [fixes #1231]

The problem simply doesn't exist if we redirect non-fatfs
devices after config.sys processing.

But for fdpp this is not an option since it doesn't use emufs.com.
So introduce the /all option to emufs.sys to redirect all at once.
int: adjust freedos work-around [#1231]

If the redirection of non-fatfs drives is delayed past config.sys,
then freedos bug inverts, and manifests in "no free drives".
mfs: return true from en/dis even if nothing changed [#1231]

DR-DOS7 doesn't need to re-redirect or enable anything.
It keeps all redirections enabled.
Returning error in that case is wrong and leads to problems.
