dosemu -home broken on most DOSes
Sorry I don't remember this issue. I just looked at my commits in freedos but I don't see anything relevant. I do remember changing the redirector to ask DOS if the drive was in use see  36b381146 but I don't know if that's connected.
At least part of the problem appears to
be because we use uninitialized CDS
when redirecting. In case of PC-DOS,
this seems to give random cds_flags,
which leads to a duplicate redirection
error. I am currently trying to build CDS
with 121f.
But this won't explain the freedos problem...
> At least part of the problem appears to
be because we use uninitialized CDS
when redirecting.

Use it in what way? Don't we just get the CDS entry (which may or may not be initialised) then immediately initialize it with our values?

Since the CDS entry is the initial determinant of whether a specific drive exists in some form, I would expect that at least the flags should be set to some sane value by DOS (even it it's zero), any other CDS entry fields can only be relied upon if the flags indicate they should be. The only occasion I can think of where we query a field that is not indicated by the flags, is when trying to restore a fatfs drive after it's been previously overlaid with MFS  

> I am currently trying to build CDS with 121f.

Don't expect that to work on FreeDOS though, I'm pretty sure it's unimplemented. I could never really understand the purpose of this call though.
> Use it in what way? Don't we just get the CDS entry (which may or may not be initialised) then immediately
> initialize it with our values?

We first check if its already redirected.
Plus we don't initialize it fully, just swap flags.

> Since the CDS entry is the initial determinant of whether a specific drive exists in some form, I would expect
> that at least the flags

But pc-dos just sets the empty path
(0 in first byte of cds), leaving everything
else seemingly uninitialized.

> Don't expect that to work on FreeDOS though

I can call it only if the first byte of CDS is 0.
> We first check if its already redirected.
Plus we don't initialize it fully, just swap flags.

Presumably the just swapping of flags occurs only in the case that the drive is currently fatfs and we want to overlay with MFS version? In the case that a new redirection is being created without an underlying fatfs just setting the flags would seem wrong.

> I can call it only if the first byte of CDS is 0.

So did you try comparing the CDS entry on PC-DOS before and after int2f/121f has been called, if so what did it initialise? 
I fixed 121f in fdpp and applied the mfs
patch to bld_cds branch so that you can
play with it.
I also put a verbose comments of all my
findings about that fn there.

> just setting the flags would seem wrong.

We also set path.
But is this enough?
CDS is a large structure.
But for now I'll apply to devel what you
suggest, as 121f appears to be quite 
bad fn.
OK, I fixed the pc-dos part of the problem
by just re-initializing CDS by hands, as you
suggested.
With old freedos it still doesn't work.
Would be good to eventually find who
fixed it. I have some recollections that
this fix was already popped up here
in a form of a link to freedos's commit
by Bart.
Commit https://github.com/dosemu2/fdpp/commit/062ebeb6a667ead39938c6972c65eaeeef9fed16
probably have something to do with that.
But perhaps not, as in our case freedos
returns valid drive as invalid (free), and
this commit was fixing the opposite problem
when invalid drives were considered valid...
Wouldn't this be simpler, as it seems setting the flags to READY(a.k.a PHYSICAL) might not be a consistent state as we can still exit the function without completing the redirection on various conditions? Or else set the flags to zero instead?
```c
commit 537f337eecb40b338945654084db7f2d6ed63f8e (HEAD -> bob01)
Author: Andrew Bird <ajb@spheresystems.co.uk>
Date:   Fri Jul 31 17:29:14 2020 +0100

    MFS: tweak PC-DOS uninitialised workaround

diff --git a/src/dosext/mfs/mfs.c b/src/dosext/mfs/mfs.c
index 3bde18183..f84952fa3 100644
--- a/src/dosext/mfs/mfs.c
+++ b/src/dosext/mfs/mfs.c
@@ -2585,13 +2585,10 @@ static int RedirectDisk(struct vm86_regs *state, int drive, char *resourceName)
     SETWORD(&(state->eax), DISK_DRIVE_INVALID);
     return FALSE;
   }
-  if (!cds[0]) {
-    /* seems to be the indication of an uninitialized CDS, used by PC-DOS */
-    cds_flags(cds) = CDS_FLAG_READY;
-  }
 
-  /* see if drive is already redirected */
-  if (cds_flags(cds) & CDS_FLAG_REMOTE) {
+  /* See if drive is already redirected but only if the path has been
+   * initialised (PC-DOS has cds[0] == 0 on uninitialised drives) */
+  if (cds[0] && cds_flags(cds) & CDS_FLAG_REMOTE) {
     Debug0((dbg_fd, "duplicate redirection for drive %i\n", drive));
     SETWORD(&(state->eax), DUPLICATE_REDIR);
     return FALSE;
```
But the values are random, for example
I see 0x7661 that already includes CDS_FLAG_READY.
Overwriting one random value with another
doesn't seem to be a big deal, unless you
keep cds_root empty.
Note that also cds_rootlen is random, for
example 19781.

This all seems to happen during config.sys
DEVICE= processing.
Perhaps the better fix is to delay the non-fatfs
redirections to INSTALL= processing.
OK, I applied the new fix.
Now I can see that the same freedos
bug manifests differently between
config and autoexec. With autoexec
it returned back to "no free drives".
And if you do "emufs.sys /all", then
you can see its config.sys form.
I've found out 2 things:
1. DR-DOS7 behaves as fdpp, in that it
doesn't need double-redirect. IIRC you
tested all DOSes and concluded that all
of them require double-redirect, but that's
not so.
2. Novell DOS needs re-redirect from
autoexec. It doesn't buy our INSTALL=
trick.
I'm not sure what you mean by 'double-redirect.', can you elaborate?
It means the need of emufs.com after
config.sys, because the CDS setup is
forgotten.
Could you please check why some
freedos test fails? Can that be locally
reproduced?
I just guessed at what it might be after looking at your recent commits. Just rebuilding and testing here now.
I've just occassinally found the root of
all evil. PC-DOS is not buggy. There was
a buggy lastdrive check in GetCDSInDOS().
I've noticed that because also novell-dos
crashed, but in entirely different manner.
Now there is no more uninitialized CDS,
as we do not access past the array.