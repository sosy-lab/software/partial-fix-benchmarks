diff --git a/src/dosext/dpmi/msdos/callbacks.c b/src/dosext/dpmi/msdos/callbacks.c
index 55b9dbe336..c95322586a 100644
--- a/src/dosext/dpmi/msdos/callbacks.c
+++ b/src/dosext/dpmi/msdos/callbacks.c
@@ -350,7 +350,7 @@ void callbacks_init(unsigned short rmcb_sel, void *(*cbk_args)(int),
 {
     int i;
     for (i = 0; i < MAX_RMCBS; i++) {
-	struct pmaddr_s pma = get_pmcb_handler(rmcb_handlers[i], cbk_args(i),
+	struct pmaddr_s pma = get_pmcb_handler(rmcb_handlers[i], cbk_args,
 		rmcb_ret_handlers[i], i);
 	r_cbks[i] = DPMI_allocate_realmode_callback(pma.selector, pma.offset,
 		rmcb_sel, 0);
diff --git a/src/dosext/dpmi/msdos/msdos.c b/src/dosext/dpmi/msdos/msdos.c
index 1bf1c2a303..40e9f25442 100644
--- a/src/dosext/dpmi/msdos/msdos.c
+++ b/src/dosext/dpmi/msdos/msdos.c
@@ -148,6 +148,10 @@ static char *msdos_seg2lin(uint16_t seg)
     return dosaddr_to_unixaddr(seg << 4);
 }
 
+/* the reason for such getters is to provide relevant data after
+ * client terminates. We can't just save the pointer statically. */
+static void *get_prev_fault(void) { return &MSDOS_CLIENT.prev_fault; }
+
 void msdos_init(int is_32, unsigned short mseg, unsigned short psp)
 {
     unsigned short envp;
@@ -190,8 +194,7 @@ void msdos_init(int is_32, unsigned short mseg, unsigned short psp)
 	    LDT_ENTRIES * LDT_ENTRY_SIZE - 1);
 
     MSDOS_CLIENT.prev_fault = dpmi_get_pm_exc_addr(0xd);
-    pma = get_pm_handler(MSDOS_FAULT, msdos_fault_handler,
-        &MSDOS_CLIENT.prev_fault);
+    pma = get_pm_handler(MSDOS_FAULT, msdos_fault_handler, get_prev_fault);
     desc.selector = pma.selector;
     desc.offset32 = pma.offset;
     dpmi_set_pm_exc_addr(0xd, desc);
@@ -394,6 +397,9 @@ static void rm_int(int intno, u_short flags,
 		 stk, stk_len, stk_used);
 }
 
+static void *get_ldt_alias(void) { return &MSDOS_CLIENT.ldt_alias; }
+static void *get_winos2_alias(void) { return &MSDOS_CLIENT.ldt_alias_winos2; }
+
 static void get_ext_API(sigcontext_t *scp)
 {
     struct pmaddr_s pma;
@@ -401,15 +407,14 @@ static void get_ext_API(sigcontext_t *scp)
     D_printf("MSDOS: GetVendorAPIEntryPoint: %s\n", ptr);
     if (!strcmp("MS-DOS", ptr)) {
 	_LO(ax) = 0;
-	pma = get_pm_handler(API_CALL, msdos_api_call,
-			&MSDOS_CLIENT.ldt_alias);
+	pma = get_pm_handler(API_CALL, msdos_api_call, get_ldt_alias);
 	_es = pma.selector;
 	_edi = pma.offset;
 	_eflags &= ~CF;
     } else if (!strcmp("WINOS2", ptr)) {
 	_LO(ax) = 0;
 	pma = get_pm_handler(API_WINOS2_CALL, msdos_api_winos2_call,
-			&MSDOS_CLIENT.ldt_alias_winos2);
+			get_winos2_alias);
 	_es = pma.selector;
 	_edi = pma.offset;
 	_eflags &= ~CF;
@@ -1508,6 +1513,8 @@ int msdos_pre_extender(sigcontext_t *scp, int intr,
 #define RMSEG_ADR(type, seg, reg)  type(&mem_base[(RMREG(seg) << 4) + \
     RMLWORD(reg)])
 
+static void *get_xms_call(void) { return &MSDOS_CLIENT.XMS_call; }
+
 /*
  * DANG_BEGIN_FUNCTION msdos_post_extender
  *
@@ -1587,7 +1594,7 @@ void msdos_post_extender(sigcontext_t *scp, int intr,
 	case 0x4310: {
 	    struct pmaddr_s pma;
 	    MSDOS_CLIENT.XMS_call = MK_FARt(RMREG(es), RMLWORD(bx));
-	    pma = get_pmrm_handler(XMS_CALL, xms_call, &MSDOS_CLIENT.XMS_call,
+	    pma = get_pmrm_handler(XMS_CALL, xms_call, get_xms_call,
 		    xms_ret);
 	    SET_REG(es, pma.selector);
 	    SET_REG(ebx, pma.offset);
diff --git a/src/dosext/dpmi/msdos/segreg.c b/src/dosext/dpmi/msdos/segreg.c
index 7038de5864..a6950581ad 100644
--- a/src/dosext/dpmi/msdos/segreg.c
+++ b/src/dosext/dpmi/msdos/segreg.c
@@ -205,6 +205,8 @@ void msdos_fault_handler(sigcontext_t *scp, void *arg)
         /* if not handled, we push old addr and return to it */
         DPMI_INTDESC *pma = arg;
         if (is_32) {
+            D_printf("MSDOS: chain exception to %x:%x\n",
+                    pma->selector, pma->offset32);
             *--ssp = pma->selector;
             *--ssp = pma->offset32;
             _esp -= 8;
diff --git a/src/dosext/dpmi/msdoshlp.c b/src/dosext/dpmi/msdoshlp.c
index d9dba03734..98daf6eb54 100644
--- a/src/dosext/dpmi/msdoshlp.c
+++ b/src/dosext/dpmi/msdoshlp.c
@@ -45,19 +45,19 @@
 #define MAX_CBKS 3
 struct msdos_ops {
     void (*fault)(sigcontext_t *scp, void *arg);
-    void *fault_arg;
+    void *(*fault_arg)(void);
     void (*api_call)(sigcontext_t *scp, void *arg);
-    void *api_arg;
+    void *(*api_arg)(void);
     void (*api_winos2_call)(sigcontext_t *scp, void *arg);
-    void *api_winos2_arg;
+    void *(*api_winos2_arg)(void);
     void (*xms_call)(const sigcontext_t *scp,
 	struct RealModeCallStructure *rmreg, void *arg);
-    void *xms_arg;
+    void *(*xms_arg)(void);
     void (*xms_ret)(sigcontext_t *scp,
 	const struct RealModeCallStructure *rmreg);
     void (*rmcb_handler[MAX_CBKS])(sigcontext_t *scp,
 	const struct RealModeCallStructure *rmreg, int is_32, void *arg);
-    void *rmcb_arg[MAX_CBKS];
+    void *(*rmcb_arg[MAX_CBKS])(int i);
     void (*rmcb_ret_handler[MAX_CBKS])(sigcontext_t *scp,
 	struct RealModeCallStructure *rmreg, int is_32);
     u_short cb_es;
@@ -161,7 +161,7 @@ static int get_cb(int num)
 
 struct pmaddr_s get_pmcb_handler(void (*handler)(sigcontext_t *,
 	const struct RealModeCallStructure *, int, void *),
-	void *arg,
+	void *(*arg)(int),
 	void (*ret_handler)(sigcontext_t *,
 	struct RealModeCallStructure *, int),
 	int num)
@@ -177,7 +177,7 @@ struct pmaddr_s get_pmcb_handler(void (*handler)(sigcontext_t *,
 }
 
 struct pmaddr_s get_pm_handler(enum MsdOpIds id,
-	void (*handler)(sigcontext_t *, void *), void *arg)
+	void (*handler)(sigcontext_t *, void *), void *(*arg)(void))
 {
     struct pmaddr_s ret;
     switch (id) {
@@ -209,7 +209,7 @@ struct pmaddr_s get_pm_handler(enum MsdOpIds id,
 
 struct pmaddr_s get_pmrm_handler(enum MsdOpIds id, void (*handler)(
 	const sigcontext_t *, struct RealModeCallStructure *, void *),
-	void *arg,
+	void *(*arg)(void),
 	void (*ret_handler)(
 	sigcontext_t *, const struct RealModeCallStructure *))
 {
@@ -254,11 +254,11 @@ far_t get_exec_helper(void)
 void msdos_pm_call(sigcontext_t *scp, int is_32)
 {
     if (_eip == 1 + DPMI_SEL_OFF(MSDOS_fault)) {
-	msdos.fault(scp, msdos.fault_arg);
+	msdos.fault(scp, msdos.fault_arg());
     } else if (_eip == 1 + DPMI_SEL_OFF(MSDOS_API_call)) {
-	msdos.api_call(scp, msdos.api_arg);
+	msdos.api_call(scp, msdos.api_arg());
     } else if (_eip == 1 + DPMI_SEL_OFF(MSDOS_API_WINOS2_call)) {
-	msdos.api_winos2_call(scp, msdos.api_winos2_arg);
+	msdos.api_winos2_call(scp, msdos.api_winos2_arg());
     } else if (_eip >= 1 + DPMI_SEL_OFF(MSDOS_rmcb_call_start) &&
 	    _eip < 1 + DPMI_SEL_OFF(MSDOS_rmcb_call_end)) {
 	int idx, ret;
@@ -295,7 +295,8 @@ void msdos_pm_call(sigcontext_t *scp, int is_32)
 		    SEL_ADR_CLNT(_es, _edi, is_32);
 	    msdos.cb_es = _es;
 	    msdos.cb_edi = _edi;
-	    msdos.rmcb_handler[idx](scp, rmreg, is_32, msdos.rmcb_arg[idx]);
+	    msdos.rmcb_handler[idx](scp, rmreg, is_32,
+		    msdos.rmcb_arg[idx](idx));
 	}
     } else {
 	error("MSDOS: unknown pm call %#x\n", _eip);
@@ -309,7 +310,7 @@ int msdos_pre_pm(int offs, const sigcontext_t *scp,
     int ret = 0;
     switch (offs) {
     case 0:
-	msdos.xms_call(scp, rmreg, msdos.xms_arg);
+	msdos.xms_call(scp, rmreg, msdos.xms_arg());
 	ret = 1;
 	break;
     default:
diff --git a/src/dosext/dpmi/msdoshlp.h b/src/dosext/dpmi/msdoshlp.h
index 4173a1ca52..8a0ad60be5 100644
--- a/src/dosext/dpmi/msdoshlp.h
+++ b/src/dosext/dpmi/msdoshlp.h
@@ -11,15 +11,15 @@ extern void msdos_pm_call(sigcontext_t *scp, int is_32);
 
 extern struct pmaddr_s get_pmcb_handler(void (*handler)(sigcontext_t *,
 	const struct RealModeCallStructure *, int, void *),
-	void *arg,
+	void *(*arg)(int),
 	void (*ret_handler)(sigcontext_t *,
 	struct RealModeCallStructure *, int),
 	int num);
 extern struct pmaddr_s get_pm_handler(enum MsdOpIds id,
-	void (*handler)(sigcontext_t *, void *), void *arg);
+	void (*handler)(sigcontext_t *, void *), void *(*arg)(void));
 extern struct pmaddr_s get_pmrm_handler(enum MsdOpIds id, void (*handler)(
 	const sigcontext_t *, struct RealModeCallStructure *, void *),
-	void *arg,
+	void *(*arg)(void),
 	void (*ret_handler)(
 	sigcontext_t *, const struct RealModeCallStructure *));
 extern far_t get_lr_helper(far_t rmcb);
