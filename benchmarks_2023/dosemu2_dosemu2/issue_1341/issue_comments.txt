dpmi tsr crash with comcom32
Perhaps it would be good to know what to expect from this program.

If I run it, it displays a line: cs=xxxx ds=xxxx cs_base=xxxx ds_base=xxxx

Nothing else. After that, if I try to start another dpmi prog, I get an "DPMI: unhandled Exception 0d - Terminating Client'
I always get this error then, no matter what command.com is active.



Yes, it prints that, but only if functions
c00 and c01 are available.
So I have no idea how have you started
it, you might be cheating.
> I always get this error then, no matter what command.com is active.

Yes, this seems to be the bug on its own.
I fixed the comcom32-related crashes, but
this one is still here.
Phew, this wasn't easy at all!
You also need to update pmdapi if you
want to test it further.
I've noticed before that when I leave you alone, you come up with a fix for an issue with a really difficult to find cause. I'll try not to bother you too much! :smile:
> I'll try not to bother you too much! smile

By not writing xattrs support? :)
OK, good hint.
xattrs supporting is continuing, but trying to write tests and iron out the wrinkles. https://github.com/andrewbird/dosemu2/tree/travis-ci-46

[pmdapi.exe.gz](https://github.com/dosemu2/dosemu2/files/5778822/pmdapi.exe.gz)
Updated test case.
Is that something that provides a simple pass/fail or other predictable output that would be worth adding to the test suite?
No, not yet.
Maybe eventually.
Wrong fix.
Got it to work for 32bit clients.
For 16bit ones something is still not good...
But getting closer.
Btw, the DPMI-1.0 doc is flawed wrt rsp
functionality:
http://www.delorie.com/djgpp/doc/dpmi/ch4.8.html
It passes client handle in BX, but on
termination call the one also needs the
handle of prev client.
As an extension, I now pass such handle
in CX.
Probably this is the reason 0xc00 is
rarely implemented?
> termination call the one also needs the
> handle of prev client.

This is because the selectors of RSP are
not preserved. They are allocated for every
new client, so the RSP should be able to
set segregs to the values that are valid
in context of the current client. So when the
current client terminates, RSP needs to know
what selectors to load into segregs next time.
And for that it needs the handle for prev client.
Finally I figured out what went wrong.
When chaining to the (prev) 16bit int21 handler,
there can be no such handler, in which
case msdos checked the THUNK_16_32x
and seen the 32bit caller.
Obviously this breaks only when the higher
words of some 32bit regs are garbled, so
win31 was becoming unstable but it was
quite difficult to locate.
Now I disable THUNK_16_32x before every
call-out.
So I don't suppose it can work with hdpmi
for 16bit clients, but for 32bit clients it uses
only documented DPMI API, so should work.
[pmdapi.exe.gz](https://github.com/dosemu2/dosemu2/files/5811957/pmdapi.exe.gz)

Andrew, it would be cool to turn it into a
test-case now, as it finally works again.
This can be done by building and loading
pmdapi before some other DPMI test, for
example ecm test.