dpmi jit: ecm dpmimini.com example crashes
The dpmimini.com in the test suite is still working. From the commit message that introduced it, the imported versions were
~~~
Tests retrieved from:
    https://hg.ulukai.org/ecm/dpmitest/file/42b4f7ec457d
Macros from:
    https://hg.ulukai.org/ecm/lmacros/file/ede277bd4d21
~~~
Did something change that triggered the breakage?
No, this must be the same version. 
I'm configuring from git as `$ ./default-configure -d --prefix=$HOME/local.new --sysconfdir=$HOME/local.new/etc
--mandir=$HOME/local.new/share/man --datarootdir=$HOME/local.new/share fdpp-build-p
ath="$PWD/"../fdpp/fdpp fdpp-include-path="$PWD/"../fdpp/include`

I'll try make clean and then make again.
Cloned the repo in a new directory, same default-configure call, make, make install, same crash.
Updated fdpp and rebuild it and dosemu2, still same crash. 
Problem occurs with fdpp running too.
Can you bisect 56a55c8b..HEAD using the python test script. I've never tried this automatically to try build and test with `git bisect run`, but it would be cool to try.
The test script produces 0 on success and 1 on test failure
~~~
$ python test/test_dos.py PPDOSGITTestCase.test_memory_dpmi_ecm_mini
Test PP-DOS-GIT Memory DPMI (ECM) mini                                           ... ok (  2.54s)

----------------------------------------------------------------------
Ran 1 test in 2.548s

OK
~~~
No need to install after build, it should just work from the build directory
Which python and which additional packages do I need to run the test? 
You need python3 and two extra packages python3-cpuinfo python3-pexpect.

You can use `$_cpuemu=(1)`
Did `pip3 install pexpect` and `pip3 install py-cpuinfo` then used `python3` to run the test.
`~/local.new/bin/dosemu -dumb -K "$PWD" -E "dpmimini.com" -I "cpuemu 1"` works as expected. 
So there is a flaw in my plan, if dosemu.conf options that we need to specify change over the range of the bisect, it's not going to work without manual intervention. Mmm!
Running the test as you indicated results in a timeout. It appears it cannot find a shell executable. Look at the output here: https://ulukai.org/ecm/test_dos.PPDOSGITTestCase.test_memory_dpmi_ecm_mini.xpt
I usually have the PPA package of comcom32 installed, but if you don't have it then the log indicates that you can steer dosemu to the location of yours via
~~~
DOSEMU2_COMCOM_DIR env var for alternative location of comcom32
~~~
Running the test like in `DOSEMU2_FREEDOS_DIR=~/local.new/share/dosemu/freedos python3 test/test_dos.py PPDOSGITTestCase.test_memory_dpmi_ecm_mini` results in a timeout again but this time it appears to be due to FreeCOM prompting for a datetime. Look at https://ulukai.org/ecm/test_dos.2
That'll be because the test for FDPP only installed the config.sys / autoexec.bat files for FDPP and they have different names.
There is an expectation in the test suite that FDPP runs with comcom32, FreeDOS 1.20 runs with FreeCOM, and MS-DOS 6.22 runs with its own command.com etc. Mixing things up would require manual intervention at each bisect stage as the test directory is deleted and recreated on every test. 
Had to prepare comcom33 by `git pull` then `source ~/local/djgpp/setenv` then `make clean` then `make all` then `cp -a comcom32.exe command.com`, then the following command worked:

`DOSEMU2_COMCOM_DIR=~/proj/comcom32 python3 test/test_dos.py PPDOSGITTestCase.test_memory_dpmi_ecm_mini`

It leads to the same failure I reported here to begin with.
> It leads to the same failure I reported here to begin with.

Excellent! So I guess you can write a little script that cleans, configures, builds dosemu and then returns the exit code of the test.

I was going to try to do it here, but systemd keeps 'fixing' the acl on /dev/kvm, so it uses that by default instead of jit.
Renaming /dev/kvm prevented systemd from messing me up, so I can now recreate the problem here. Let's see if we get the same result.
Exported the DOSEMU2_COMCOM_DIR variable. Now running:

```
dosemu2$ git bisect start
dosemu2$ git bisect bad
dosemu2$ git bisect good 56a55
Bisecting: 149 revisions left to test after this (roughly 7 steps)
[120ece5f791b20c533b237272148cab611a78b65] conf: more examples for $_dosemu_paths [
fixes #1340]
dosemu2$ git bisect run ./test.sh 2>&1 | tee bisect.log
running ./test.sh
```

Script file uploaded to http://ulukai.org/ecm/test.sh
BTW, 56a55c8 is the commit where the test was introduced. I have assumed in my bisect run that it was a good commit. That may not be true, as it's a long way after your 8596bd5
Ahh, I guess I was lucky in my assumption. I just had an intermediate test success, so we should get a proper result!
There seems to be a problem running the test on some revisions. Here's the log: https://ulukai.org/ecm/20210101.4

```

None                                                                             ... Traceback (most recent call last):
  File "/home/[...]/proj/newdosemu/dosemu2/test/test_dos.py", line 6111, in <module>
    main()
  File "/home/[...]/proj/newdosemu/dosemu2/test/common_framework.py", line 381, in main
    unittest.main(testRunner=MyTestRunner, verbosity=2)
  File "/home/[...]/local/lib/python3.9/unittest/main.py", line 101, in __init__
    self.runTests()
  File "/home/[...]/local/lib/python3.9/unittest/main.py", line 271, in runTests
    self.result = testRunner.run(self.test)
  File "/home/[...]/local/lib/python3.9/unittest/runner.py", line 176, in run
    test(result)
  File "/home/[...]/local/lib/python3.9/unittest/suite.py", line 84, in __call__
    return self.run(*args, **kwds)
  File "/home/[...]/local/lib/python3.9/unittest/suite.py", line 122, in run
    test(result)
  File "/home/[...]/local/lib/python3.9/unittest/suite.py", line 84, in __call__
    return self.run(*args, **kwds)
  File "/home/[...]/local/lib/python3.9/unittest/suite.py", line 122, in run
    test(result)
  File "/home/[...]/local/lib/python3.9/unittest/case.py", line 653, in __call__
    return self.run(*args, **kwds)
  File "/home/[...]/local/lib/python3.9/unittest/case.py", line 566, in run
    result.startTest(self)
  File "/home/[...]/proj/newdosemu/dosemu2/test/common_framework.py", line 323, in startTest
    name = test.id().replace('__main__', test.pname)
  File "/home/[...]/local/lib/python3.9/unittest/loader.py", line 32, in __getattr__
    return super(_FailedTest, self).__getattr__(name)
AttributeError: 'super' object has no attribute '__getattr__'
```
In the log it appears that the test was actually run 3 times and failed for all of those.
Oh it appears to fail for a different reason than this report's error.
Here's a failure case where the test is run. It appears to fail because fdpp support is not compiled in, and it fails to boot FreeDOS: https://ulukai.org/ecm/20210101.5

```
fdpp support is not compiled in.
ERROR: Directory /home/[...]/proj/newdosemu/dosemu2/etc/fdboot does not exist
```
The fdpp support not compiled in error is, I presume, due to fdpp version mismatch. Does anyone know a git command to checkout the fdpp revision whose datetime matches the most recent commit corresponding to the currently checked out dosemu2 commit?
Testing with a new script: https://ulukai.org/ecm/test2.sh
Had a spurious blank in the default-configure call from the test script.
So I'd just left mine to run and I came back and it had finished. In light of @ecm-pushbx's findings it might not be accurate, but just in case I'll post it here.
~~~
3fad52f5db67e97c03dad0d2fbf58d6261a2b0f3 is the first bad commit
commit 3fad52f5db67e97c03dad0d2fbf58d6261a2b0f3
Author: Stas Sergeev <stsp@users.sourceforge.net>
Date:   Fri Nov 6 21:59:53 2020 +0300

    config: mark native DPMI as insecure [#560]

 etc/dosemu.conf         | 4 ++--
 src/base/emu-i386/cpu.c | 8 +++++++-
 src/base/init/config.c  | 3 +++
 3 files changed, 12 insertions(+), 3 deletions(-)
bisect run success
~~~

An odd thing about FDPP / plugin API mismatch, both appear to be 27
~~~
ajb@polly:/clients/common/dosemu2.git$ git show 56a55c8:src/plugin/fdpp/fdpp.c | grep 'if FDPP_API'
#if FDPP_API_VER != 27
ajb@polly:/clients/common/dosemu2.git$ git show HEAD:src/plugin/fdpp/fdpp.c | grep 'if FDPP_API'
#if FDPP_API_VER != 27
~~~


Yes, its  a jit problem so no wonder it
pointed to that commit.
I hope you are debugging your bisect scripts
which are good to have working. But
the bisect itself is not very valuable now.
I got the same result now:

```
3fad52f5db67e97c03dad0d2fbf58d6261a2b0f3 is the first bad commit
commit 3fad52f5db67e97c03dad0d2fbf58d6261a2b0f3
Author: Stas Sergeev <stsp@users.sourceforge.net>
Date:   Fri Nov 6 21:59:53 2020 +0300

    config: mark native DPMI as insecure [#560]

:040000 040000 f852ac9ec7798ce65665e016713897156181943a c3fc6a2a8da0ae22a441aa631f46dbbfb6666239 M	etc
:040000 040000 5e512fd601b18e1d9fe5ae3e0b2456a0c7e93a40 d7a2c2ad405aa81ee04ae7c363825292dff7071d M	src
bisect run success
```

(From the end of the https://ulukai.org/ecm/bisect3.log file.)


This is the fixed test script. It hardcodes the directories though.

```
#! /bin/bash

export DOSEMU2_COMCOM_DIR=~/proj/comcom32
make clean
(($?)) && exit $?
cd ../fdpp
git checkout "$(git log --until="$(git --git-dir=../dosemu2/.git log -n1 "$(git --git-dir=../dosemu2/.git describe | sed -re 's/^.*-g([a-fA-F0-9]+)$/\1/')" | grep -oE "^Date:.*$" | sed -re 's/^Date:\s+//')" -n1 | grep -oE "^commit .*$" | sed -re 's/^commit\s+//')"
(($?)) && exit $?
make clean
(($?)) && exit $?
./configure
(($?)) && exit $?
make
(($?)) && exit $?
DESTDIR=$HOME/local.new make install
(($?)) && exit $?
cd ../dosemu2
./default-configure -d --prefix=$HOME/local.new --sysconfdir=$HOME/local.new/etc --mandir=$HOME/local.new/share/man --datarootdir=$HOME/local.new/share fdpp-build-path="$(realpath "$PWD/"../fdpp/fdpp)" fdpp-include-path="$(realpath "$PWD/"../fdpp/include)"
(($?)) && exit $?
make
(($?)) && exit $?
python3 test/test_dos.py PPDOSGITTestCase.test_memory_dpmi_ecm_mini
exit $?
```

So did that commit just change the default from CPUVM_NATIVE to CPUVM_EMU? If so I guess the problem could have been present before and we just never hit it.
https://github.com/dosemu2/dosemu2/commit/3fad52f5db67e97c03dad0d2fbf58d6261a2b0f3 linked for convenience 
`git log --until=` should be `git log --all --until=` in my script. 
What I actually wanted to test was https://github.com/dosemu2/dosemu2/commit/80d583e080eff673bd7bc5d259ba125130727275 because I noticed that it didn't set the 86 Mode ds:dx registers. But that was superseded by https://github.com/dosemu2/dosemu2/commit/3e815c0f8c257ea86731e6c427bf95d00b01c4af which fixes that error. Tests indicate it seems to work.
Should now be fixed.
Jit seems rather buggy if, when fixing
one bug, I hit another two. :)
It is fixed when running dpmimini.com in the test or on its own. With lDebugX I still get an error:

```
Welcome to dosemu2!
    Build 2.0pre8-20210102-2627-gb6e05edd2
-g
DPMI entry hooked, new entry=045C:7230
Protected mode breakpoint at 015Ah.
32-bit code segment breakpoint at 016Ch.

Unexpected breakpoint interrupt
AX=0200 BX=00E6 CX=0105 DX=000A SP=FFFC BP=0000 SI=0269 DI=7230
DS=1BC5 ES=2BC6 SS=1BC5 CS=1BC5 IP=013B NV UP EI PL ZR NA PE NC
1BC5:013B 89E5              mov     bp, sp
-g
DPMI: Unhandled Exception 05 - Terminating Client
It is likely that dosemu is unstable now and should be rebooted

Program terminated normally (00FF)
-q
```
Works fine with `-I "cpuemu 1"` again.
Is this again only on jit?
Can you create a test that will not
involve any debugger (for example
embed the break-point 0xcc directly
to the code) and open another ticket?
Putting an int3 opcode and running the test outside a debugger does not crash.
Can you try this:
```
diff --git a/src/base/emu-i386/simx86/econfig.h b/src/base/emu-i386/simx86/econfig.h
index 2aa72dea0..589a1fe84 100644
--- a/src/base/emu-i386/simx86/econfig.h
+++ b/src/base/emu-i386/simx86/econfig.h
@@ -37,7 +37,7 @@
 
 /***************************************************************************/
 
-#undef SINGLESTEP
+#define        SINGLESTEP
 #undef SINGLEBLOCK
 #undef PROFILE
 #undef DBG_TIME
```

It will make things very slow but
lets see if the crash is still there.
Crash disappeared with that change.
Should now be fixed.
I wonder why more and more people
run out of kvm. We even explicitly require
udev of the needed version, which doesn't
seem to help?
I thought we'd established that it was udev that added the systemd hook, but just looking at the Travis Focal install log it still seems to be qemu
~~~
Setting up qemu-system-common (1:4.2-3ubuntu6.10) ...
Created symlink /etc/systemd/system/multi-user.target.wants/qemu-kvm.service → /lib/systemd/system/qemu-kvm.service.
~~~
This service seems to be loading the
kvm module and udev seems to be
setting the permissions.
Can you make a definitive test of that?
I.e. that by default (w/o qemu-system-common)
kvm is unavailable, and if you manually load
the needed module, then the permissions
are properly set?
So I'm confused, I know that removing the qemu-system-common package is not the same as having never installed it (users and groups were created etc) but I expected that the systemd service being removed would have broken it. See

~~~                                                                             
ajb@polly:/clients/common/dosemu2.git$ python test/test_dos.py PPDOSGITTestCase.test_cpu_kvm
Test PP-DOS-GIT CPU test: KVM vm86 + KVM DPMI                                    ... ok (  2.38s)
                                                                                
----------------------------------------------------------------------          
Ran 1 test in 2.388s                                                            
                                                                                
OK                                                                              
~~~                                                                             
~~~                                                                             
ajb@polly:/clients/common/dosemu2.git$ sudo apt remove --purge qemu-system-common
[sudo] password for ajb:                                                        
Reading package lists... Done                                                   
Building dependency tree                                                        
Reading state information... Done                                               
The following packages were automatically installed and are no longer required: 
  augeas-lenses cpu-checker cryptsetup-bin db-util db5.3-util extlinux hfsplus icoutils ipxe-qemu ipxe-qemu-256k-compat-efi-roms kpartx ldmtool libafflib0v5 libaugeas0
  libconfig9 libdate-manip-perl libewf2 libfdt1 libhfsp0 libhivex0 libintl-perl libintl-xs-perl libldm-1.0-0 libpmem1 libslirp0 libspice-server1 libsys-virt-perl libtsk13
  libvirglrenderer1 libwin-hivex-perl libyara3 lsscsi msr-tools ovmf qemu-efi-aarch64 qemu-efi-arm qemu-system-data qemu-system-gui scrub seabios sleuthkit supermin
  zerofree                                                                      
Use 'sudo apt autoremove' to remove them.                                       
The following packages will be REMOVED                                          
  libguestfs-hfsplus* libguestfs-perl* libguestfs-reiserfs* libguestfs-tools* libguestfs-xfs* libguestfs0* qemu-kvm* qemu-system-arm* qemu-system-common* qemu-system-x86*
0 to upgrade, 0 to newly install, 10 to remove and 0 not to upgrade.            
After this operation, 119 MB disk space will be freed.                          
Do you want to continue? [Y/n]                                                  
(Reading database ... 316997 files and directories currently installed.)        
Removing libguestfs-hfsplus:amd64 (1:1.40.2-7ubuntu5) ...                       
Removing libguestfs-tools (1:1.40.2-7ubuntu5) ...                               
Removing libguestfs-perl (1:1.40.2-7ubuntu5) ...                                
Removing libguestfs-reiserfs:amd64 (1:1.40.2-7ubuntu5) ...                      
Removing libguestfs-xfs:amd64 (1:1.40.2-7ubuntu5) ...                           
Removing libguestfs0:amd64 (1:1.40.2-7ubuntu5) ...                              
Removing qemu-kvm (1:4.2-3ubuntu6.10) ...                                       
Removing qemu-system-arm (1:4.2-3ubuntu6.10) ...                                
Removing qemu-system-x86 (1:4.2-3ubuntu6.10) ...                                
Removing qemu-system-common (1:4.2-3ubuntu6.10) ...                             
Processing triggers for libc-bin (2.31-0ubuntu9.1) ...                          
Processing triggers for man-db (2.9.1-1) ...                                    
Processing triggers for doc-base (0.10.9) ...                                   
Processing 1 removed doc-base file...                                           
(Reading database ... 316611 files and directories currently installed.)        
Purging configuration files for qemu-system-common (1:4.2-3ubuntu6.10) ...      
Purging configuration files for qemu-kvm (1:4.2-3ubuntu6.10) ...                
Purging configuration files for qemu-system-x86 (1:4.2-3ubuntu6.10) ...         
Purging configuration files for libguestfs-tools (1:1.40.2-7ubuntu5) ...        
~~~                                                                             
~~~                                                                             
# reboot machine                                                                
~~~                                                                             
                                                                                
~~~                                                                             
$ getfacl /dev/kvm                                                              
getfacl: Removing leading '/' from absolute path names                          
# file: dev/kvm                                                                 
# owner: root                                                                   
# group: kvm                                                                    
user::rw-                                                                       
user:ajb:rw-                                                                    
group::rw-                                                                      
mask::rw-                                                                       
other::---                                                                      
~~~                                                                             
                                                                                
~~~                                                                             
ajb@polly:/clients/common/dosemu2.git$ python test/test_dos.py PPDOSGITTestCase.test_cpu_kvm
Test PP-DOS-GIT CPU test: KVM vm86 + KVM DPMI                                    ... ok (  3.31s)
                                                                                
----------------------------------------------------------------------          
Ran 1 test in 3.321s                                                            
                                                                                
OK                                                                              
~~~                                                                             

~~~
ajb@polly:/clients/common/dosemu2.git$ dpkg -S /lib/systemd/system/qemu-kvm.service
dpkg-query: no path found matching pattern /lib/systemd/system/qemu-kvm.service
~~~
Compiled the new revision. Running the application with two "G" commands to the debugger, no breakpoints specified, works now. However, with "G 16C" it prints funny messages when breaking in protected mode. As usual with `-I "cpuemu 1"` it is fine.

This is what it looks like:

```
dpmitest$ ~/local.new/bin/dosemu -K "$PWD" -dumb -E "debugx.com dpmimini.com"
Compatibility warning: found deprecated setup of dosemu2 pre-alpha version.
    If you do not intend to run such old dosemu2 versions
    (dosemu1 is fine), please do:
        rm ~/.dosemu/drives/*.lnk
    You may also do
        rm -rf ~/.dosemu/drives
    if you dont intend to run dosemu1.
ERROR: KVM: error opening /dev/kvm: No such file or directory
ERROR: alsa_midi (ALSA lib): seq_hw.c:466:(snd_seq_hw_open) : No such file or directory open /dev/snd/seq failed
ERROR: Unable to open console or check with X to evaluate the keyboard map.
Please specify your keyboard map explicitly via the $_layout option. Defaulting to US.
dosemu2 2.0pre8-20210102-2641-g1a8d96ac5 Configured: 2021-01-02 14:32:44 +0100
Get the latest code at http://stsp.github.io/dosemu2
Submit Bugs via https://github.com/dosemu2/dosemu2/issues
Ask for help in mail list: linux-msdos@vger.kernel.org
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, GPL v2 (or any later version) distribution conditions.

FreeDOS kernel - SVN (build 2042 OEM:0xfd) [compiled Jan 24 2020]
Kernel compatibility 7.10 - BORLANDC - 80386 CPU required - FAT32 support

(C) Copyright 1995-2012 Pasquale J. Villani and The FreeDOS Project.
All Rights Reserved. This is free software and comes with ABSOLUTELY NO
WARRANTY; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation;
either version 2, or (at your option) any later version.
C: HD1, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
D: HD2, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
E: HD3, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
F: HD4, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
EMUFS host file and print access available
dosemu XMS 3.0 & UMB support enabled
dosemu EMS driver rev 0.9 installed.
Kernel: allocated 36 Diskbuffers = 19152 Bytes in HMA
emufs: redirector enabled
Welcome to dosemu2!
    Build 2.0pre8-20210102-2641-g1a8d96ac5
-g
DPMI entry hooked, new entry=045C:7AD4
Protected mode breakpoint at 015Ah.
32-bit code segment breakpoint at 016Ch.

Unexpected breakpoint interrupt
AX=0200 BX=00E6 CX=0105 DX=000A SP=FFFC BP=0000 SI=0269 DI=7AD4
DS=1F0F ES=2F10 SS=1F0F CS=1F0F IP=013B NV UP EI PL ZR NA PE NC
1F0F:013B 89E5              mov     bp, sp
-g 16c
The proceed breakpointpermanent breakpoint __ G breakpoint (linear ----_----) cannot be written. cannot be restored to __. No error. (Internal error, report!)
It is read-only.
It is unreachable.
It has been overwritten with __.
Unknown error. (Internal error, report!)
    G breakpoint, linear ----_----, content __ (is at CS:EIP)
 (is at CS:IP)
The G breakpoint list is empty.
LISTAGAINWRTReached limit of repeating disassembly.
Internal error in disassembler!
Internal error in assembler!
Stack overflow occurred, IP=____h, due to expression indirection.
expression parentheses.
expression precedence.
:
: wrt : +
       direct branch target =         string source        =         string destination   =         memory source        =         memory destination   =         memory (unknown)     =         unknown mem ref type = Internal error, inval[more]
```
Last message was from an older version of lDebugX (2019), but the behaviour is the same with the most recent one. Only the exact content of the display changes:

```
$ ~/local.new/bin/dosemu -K "$PWD" -dumb -E debugx.com\ dpmimini.com
Compatibility warning: found deprecated setup of dosemu2 pre-alpha version.
    If you do not intend to run such old dosemu2 versions
    (dosemu1 is fine), please do:
        rm ~/.dosemu/drives/*.lnk
    You may also do
        rm -rf ~/.dosemu/drives
    if you dont intend to run dosemu1.
ERROR: KVM: error opening /dev/kvm: No such file or directory
ERROR: alsa_midi (ALSA lib): seq_hw.c:466:(snd_seq_hw_open) : No such file or directory open /dev/snd/seq failed
ERROR: Unable to open console or check with X to evaluate the keyboard map.
Please specify your keyboard map explicitly via the $_layout option. Defaulting to US.
dosemu2 2.0pre8-20210102-2641-g1a8d96ac5 Configured: 2021-01-02 14:32:44 +0100
Get the latest code at http://stsp.github.io/dosemu2
Submit Bugs via https://github.com/dosemu2/dosemu2/issues
Ask for help in mail list: linux-msdos@vger.kernel.org
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, GPL v2 (or any later version) distribution conditions.

FreeDOS kernel - SVN (build 2042 OEM:0xfd) [compiled Jan 24 2020]
Kernel compatibility 7.10 - BORLANDC - 80386 CPU required - FAT32 support

(C) Copyright 1995-2012 Pasquale J. Villani and The FreeDOS Project.
All Rights Reserved. This is free software and comes with ABSOLUTELY NO
WARRANTY; you can redistribute it and/or modify it under the terms of the
GNU General Public License as published by the Free Software Foundation;
either version 2, or (at your option) any later version.
C: HD1, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
D: HD2, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
E: HD3, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
F: HD4, Pri[ 1], CHS=    0-1-1, start=     0 MB, size=  2000 MB
EMUFS host file and print access available
dosemu XMS 3.0 & UMB support enabled
dosemu EMS driver rev 0.9 installed.
Kernel: allocated 36 Diskbuffers = 19152 Bytes in HMA
emufs: redirector enabled
Welcome to dosemu2!
    Build 2.0pre8-20210102-2641-g1a8d96ac5
-g
DPMI entry hooked, new entry=045C:7230
Protected mode breakpoint at 015Ah.
32-bit code segment breakpoint at 016Ch.

Unexpected breakpoint interrupt
AX=0200 BX=00E6 CX=0105 DX=000A SP=FFFC BP=0000 SI=0269 DI=7230
DS=1BC5 ES=2BC6 SS=1BC5 CS=1BC5 IP=013B NV UP EI PL ZR NA PE NC
1BC5:013B 89E5              mov     bp, sp
-g 16c
The proceed breakpointpermanent breakpoint __ G breakpoint (linear ----_----) cannot be written. cannot be restored to __. No error. (Internal error, report!)
It is read-only.
It is unreachable.
It has been overwritten with __.
Unknown error. (Internal error, report!)
    G breakpoint, linear ----_----, content __ (is at CS:EIP)
 (is at CS:IP)
The G breakpoint list is empty.
LISTAGAINReached limit of repeating disassembly.
Internal error in disassembler!
Inter1st G breakpoint (linear 0001_BDBC) cannot be restored to CC. Unknown error. ([more]
```
Just to confirm, it seems the `qemu-system-common` package is not required to use KVM on Travis as I did this
https://github.com/andrewbird/dosemu2/commit/c40f11a078fcc1166ba34dfa008577d3986b4a03
and the test was fine
https://travis-ci.com/github/andrewbird/dosemu2/jobs/467714872

So I've no idea why people's KVM is not configured for them. :frowning_face: 
The server on which I'm doing most development doesn't have KVM available. 
@ecm-pushbx thanks, I suspected as much. We were concerned that perhaps it was a general problem as recently there seemed to have been quite a few issues reported where digging through the log supplied indicated that KVM wasn't in use.
> However, with "G 16C" it prints funny messages

Please see if SINGLESTEP fixes also that.
It does fix it.
OK, should now hopefully be fixed.
Yes, it appears to be fixed, I tried out dpmimini, dpmims, and dpmipsp in the debugger. Thanks!
For good measure I also went through a quick run of lDebugX debugging lDDebugX debugging dpmipsp, and all seems to be well.