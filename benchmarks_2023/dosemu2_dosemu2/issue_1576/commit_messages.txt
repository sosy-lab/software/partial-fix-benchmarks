video: deinit render before closing back-end

This should fix the slang crash on exit.
pcl: basic asan support [#1576]

asan intercepts swapcontext() and friends.
It therefore can't work with custom context switching.
Use normal swapcontext() when asan is enabled.
mfs: fix asan-reported bug [#1576]
enable asan for debug build [fixes #1576]

This makes the debug build considerably slower.
Revert "mfs: fix asan-reported bug [#1576]"

This reverts commit 79969dc804baa051b9f7646ac2adc8fc41950565.
another is_dos_device() fix [#1576]

Introduce is_dos_device8() wrapper to deal with non-asciiz
char[8] input.
Revert "pcl: basic asan support [#1576]"

This reverts commit 582a2cf772a339bf1cd5d3fa41e3ba436eea2ff5.

Wrong fix. mcontext was broken, so was the crash.
mcontext: inline getmcontext() [fixes #1576]

getmcontext() was/is a Cish wrapper around the asm getcontext code.
It wasn't inlined, and therefore was grabbing the wrong stack frame!
How could this even work. :(
libpcl: enable ucontext [#1576]
signal: drop SS_AUTODISARM after probe [fixes #1576]

It appears asan is checking "signal_stack.ss_flags != SS_DISABLE"
which evaluates to true because of SS_AUTODISARM.

Perhaps @amluto should fix the kernel to not return any flags
when SS_DISABLE is set?
Revert "libpcl: enable ucontext [#1576]"

This reverts commit 0f9a4f2a54079d0014382277f9e2e86d6066fa53.

It was enabled for asan to work, but mcontext was fixed in fad89942.
See #1888
