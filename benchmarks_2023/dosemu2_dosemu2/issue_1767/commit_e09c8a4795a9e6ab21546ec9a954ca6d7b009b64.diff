diff --git a/src/base/video/instremu.c b/src/base/video/instremu.c
index ac442b113..839fa9fa3 100644
--- a/src/base/video/instremu.c
+++ b/src/base/video/instremu.c
@@ -62,6 +62,29 @@
 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 #define DEBUG_INSTR	0	/* (<= 2) instruction emulation */
 
+#define R_LO(a) LO_BYTE_d(a)
+#define R_HI(a) HI_BYTE_d(a)
+#define R_WORD(a) LO_WORD(a)
+#define R_DWORD(a) (*((unsigned *) &(a)))
+#define AL (R_LO(x86->eax))
+#define AH (R_HI(x86->eax))
+#define AX (R_WORD(x86->eax))
+#define BL (R_LO(x86->ebx))
+#define BH (R_HI(x86->ebx))
+#define BX (R_WORD(x86->ebx))
+#define CL (R_LO(x86->ecx))
+#define CH (R_HI(x86->ecx))
+#define CX (R_WORD(x86->ecx))
+#define DL (R_LO(x86->edx))
+#define DH (R_HI(x86->edx))
+#define DX (R_WORD(x86->edx))
+#define SI (R_WORD(x86->esi))
+#define DI (R_WORD(x86->edi))
+#define SP (R_WORD(x86->esp))
+#define BP (R_WORD(x86->ebp))
+#define EFLAGS (R_DWORD(x86->eflags))
+#define FLAGS (R_WORD(EFLAGS))
+
 #define instr_msg(x...) v_printf("instremu: " x)
 
 #define instr_deb(x...) v_printf("instremu: " x)
@@ -72,12 +95,39 @@
 #define instr_deb2(x...)
 #endif
 
+enum {REPNZ = 0, REPZ = 1, REP_NONE = 2};
+
+struct rm {
+  unsigned char *r;
+  dosaddr_t m;
+};
+
+typedef struct x86_regs {
+  unsigned eax, ecx, edx, ebx, esp, ebp, esi, edi;
+  /* this sequence is important because this is the cpu's order and thus
+     gives us an optimization */
+  unsigned eip;
+  unsigned eflags;
+  unsigned es, cs, ss, ds, fs, gs;
+  unsigned cs_base, ds_base, es_base, ss_base, fs_base, gs_base;
+  unsigned seg_base, seg_ss_base;
+  unsigned _32bit:1;	/* 16/32 bit code */
+  unsigned address_size; /* in bytes so either 4 or 2 */
+  unsigned operand_size;
+  unsigned prefixes, rep;
+  unsigned (*instr_read)(struct rm rm);
+  void (*instr_write)(struct rm rm, unsigned u);
+  struct rm (*modrm)(unsigned char *cp, struct x86_regs *x86, int *inst_len);
+} x86_regs;
+
 #if DEBUG_INSTR >= 1
-static const char *seg_txt[7] = { "", "es: ", "cs: ", "ss: ", "ds: ", "fs: ", "gs: " };
-static const char *rep_txt[3] = { "", "repnz ", "repz " };
-static const char *lock_txt[2] = { "", "lock " };
+static char *seg_txt[7] = { "", "es: ", "cs: ", "ss: ", "ds: ", "fs: ", "gs: " };
+static char *rep_txt[3] = { "", "repnz ", "repz " };
+static char *lock_txt[2] = { "", "lock " };
 #endif
 
+static unsigned wordmask[5] = {0,0xff,0xffff,0xffffff,0xffffffff};
+
 static unsigned char it[0x100] = {
   7, 7, 7, 7, 2, 3, 1, 1,    7, 7, 7, 7, 2, 3, 1, 0,
   7, 7, 7, 7, 2, 3, 1, 1,    7, 7, 7, 7, 2, 3, 1, 1,
@@ -102,8 +152,19 @@ static unsigned char it[0x100] = {
 
 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 static unsigned seg, lock, rep;
+#define vga_base vga.mem.bank_base
+#define vga_end (vga_base + vga.mem.bank_len)
+
 static unsigned arg_len(unsigned char *, int);
 
+static unsigned instr_read_word(struct rm rm);
+static unsigned instr_read_dword(struct rm rm);
+static void instr_write_word(struct rm rm, unsigned u);
+static void instr_write_dword(struct rm rm, unsigned u);
+static dosaddr_t sib(unsigned char *cp, x86_regs *x86, int *inst_len);
+static struct rm modrm32(unsigned char *cp, x86_regs *x86, int *inst_len);
+static struct rm modrm16(unsigned char *cp, x86_regs *x86, int *inst_len);
+
 /* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
 
 /*
@@ -290,3 +351,569 @@ static unsigned arg_len(unsigned char *p, int asp)
 
   return u;
 }
+
+/*
+ * Some functions to make using the vga emulation easier.
+ *
+ *
+ */
+
+unsigned instr_read_word(struct rm rm)
+{
+  unsigned u;
+
+  if (rm.r) {
+    memcpy(&u, rm.r, 2);
+    return u;
+  }
+
+  /*
+   * segment wrap-arounds within a data word are not allowed since
+   * at least i286, so no problems here
+   */
+  u = read_word(rm.m);
+
+#if DEBUG_INSTR >= 2
+  instr_deb2("Read word 0x%x", u);
+  if (addr<0x8000000) v_printf(" from address %x\n", addr); else v_printf("\n");
+#endif
+  return u;
+}
+
+unsigned instr_read_dword(struct rm rm)
+{
+  unsigned u;
+
+  if (rm.r) {
+    memcpy(&u, rm.r, 4);
+    return u;
+  }
+
+  /*
+   * segment wrap-arounds within a data word are not allowed since
+   * at least i286, so no problems here
+   */
+  u = read_dword(rm.m);
+
+#if DEBUG_INSTR >= 2
+  instr_deb2("Read word 0x%x", u);
+  if (addr<0x8000000) v_printf(" from address %x\n", addr); else v_printf("\n");
+#endif
+  return u;
+}
+
+void instr_write_word(struct rm rm, unsigned u)
+{
+  if (rm.r) {
+    memcpy(rm.r, &u, 2);
+    return;
+  }
+
+  /*
+   * segment wrap-arounds within a data word are not allowed since
+   * at least i286, so no problems here.
+   * we assume application do not try to mix here
+   */
+
+  write_word(rm.m, u);
+
+#if DEBUG_INSTR >= 2
+  instr_deb2("Write word 0x%x", u);
+  if (dst<0x8000000) v_printf(" at address %x\n", dst); else v_printf("\n");
+#endif
+}
+
+void instr_write_dword(struct rm rm, unsigned u)
+{
+  if (rm.r) {
+    memcpy(rm.r, &u, 4);
+    return;
+  }
+
+  /*
+   * segment wrap-arounds within a data word are not allowed since
+   * at least i286, so no problems here.
+   * we assume application do not try to mix here
+   */
+
+  write_dword(rm.m, u);
+
+#if DEBUG_INSTR >= 2
+  instr_deb2("Write word 0x%x", u);
+  if (dst<0x8000000) v_printf(" at address %x\n", dst); else v_printf("\n");
+#endif
+}
+
+static inline void pop(unsigned *val, x86_regs *x86)
+{
+  struct rm mem = {};
+
+  mem.m = x86->ss_base + (x86->esp & wordmask[(x86->_32bit+1)*2]);
+  if (x86->_32bit)
+    x86->esp += x86->operand_size;
+  else
+    LO_WORD(x86->esp) += x86->operand_size;
+  *val = (x86->operand_size == 4 ? instr_read_dword(mem) : instr_read_word(mem));
+}
+
+/* helper functions/macros reg8/reg/sreg/sib/modrm16/32 for instr_sim
+   for address and register decoding */
+
+#define reg8(reg, x86) (((unsigned char *)(x86))+((reg)&0x3)*4+(((reg)>>2)&1))
+#define reg(reg, x86) ((&(x86)->eax)+((reg)&0x7))
+#define sreg(reg, x86) ((&((x86)->es))+((reg)&0x7))
+#define sreg_idx(reg) (es_INDEX+((reg)&0x7))
+
+dosaddr_t sib(unsigned char *cp, x86_regs *x86, int *inst_len)
+{
+  unsigned addr = 0;
+
+  switch(cp[1] & 0xc0) { /* decode modifier */
+  case 0x40:
+    addr = (int)(signed char)cp[3];
+    break;
+  case 0x80:
+    addr = R_DWORD(cp[3]);
+    break;
+  }
+
+  if ((cp[2] & 0x38) != 0x20) /* index cannot be esp */
+    addr += *reg(cp[2]>>3, x86) << (cp[2] >> 6);
+
+  switch(cp[2] & 0x07) { /* decode address */
+  case 0x00:
+  case 0x01:
+  case 0x02:
+  case 0x03:
+  case 0x06:
+  case 0x07:
+    return (addr + *reg(cp[2], x86) + x86->seg_base);
+  case 0x04: /* esp */
+    return (addr + x86->esp + x86->seg_ss_base);
+  case 0x05:
+    if (cp[1] >= 0x40)
+      return (addr + x86->ebp + x86->seg_ss_base);
+    else {
+      *inst_len += 4;
+      return (addr + R_DWORD(cp[3]) + x86->seg_base);
+    }
+  }
+  return 0; /* keep gcc happy */
+}
+
+struct rm modrm16(unsigned char *cp, x86_regs *x86, int *inst_len)
+{
+  unsigned addr = 0;
+  struct rm rm = {};
+  *inst_len = 0;
+
+  switch(cp[1] & 0xc0) { /* decode modifier */
+  case 0x40:
+    addr = (short)(signed char)cp[2];
+    *inst_len = 1;
+    break;
+  case 0x80:
+    addr = R_WORD(cp[2]);
+    *inst_len = 2;
+    break;
+  case 0xc0:
+    if (cp[0]&1) /*(d)word*/
+      rm.r = (unsigned char *)reg(cp[1], x86);
+    else
+      rm.r = reg8(cp[1], x86);
+    return rm;
+  }
+
+
+  switch(cp[1] & 0x07) { /* decode address */
+  case 0x00:
+    rm.m = (((addr + x86->ebx + x86->esi) & 0xffff) + x86->seg_base);
+    break;
+  case 0x01:
+    rm.m = (((addr + x86->ebx + x86->edi) & 0xffff) + x86->seg_base);
+    break;
+  case 0x02:
+    rm.m = (((addr + x86->ebp + x86->esi) & 0xffff) + x86->seg_ss_base);
+    break;
+  case 0x03:
+    rm.m = (((addr + x86->ebp + x86->edi) & 0xffff) + x86->seg_ss_base);
+    break;
+  case 0x04:
+    rm.m = (((addr + x86->esi) & 0xffff) + x86->seg_base);
+    break;
+  case 0x05:
+    rm.m = (((addr + x86->edi) & 0xffff) + x86->seg_base);
+    break;
+  case 0x06:
+    if (cp[1] >= 0x40)
+      rm.m = (((addr + x86->ebp) & 0xffff) + x86->seg_ss_base);
+    else {
+      *inst_len += 2;
+      rm.m = (R_WORD(cp[2]) + x86->seg_base);
+    }
+    break;
+  case 0x07:
+    rm.m = (((addr + x86->ebx) & 0xffff) + x86->seg_base);
+    break;
+  }
+  return rm;
+}
+
+struct rm modrm32(unsigned char *cp, x86_regs *x86, int *inst_len)
+{
+  unsigned addr = 0;
+  struct rm rm = {};
+  *inst_len = 0;
+
+  switch(cp[1] & 0xc0) { /* decode modifier */
+  case 0x40:
+    addr = (int)(signed char)cp[2];
+    *inst_len = 1;
+    break;
+  case 0x80:
+    addr = R_DWORD(cp[2]);
+    *inst_len = 4;
+    break;
+  case 0xc0:
+    if (cp[0]&1) /*(d)word*/
+      rm.r = ((unsigned char *)reg(cp[1], x86));
+    else
+      rm.r = reg8(cp[1], x86);
+    return rm;
+  }
+
+  switch(cp[1] & 0x07) { /* decode address */
+  case 0x00:
+  case 0x01:
+  case 0x02:
+  case 0x03:
+  case 0x06:
+  case 0x07:
+    rm.m = (addr + *reg(cp[1], x86) + x86->seg_base);
+    break;
+  case 0x04: /* sib byte follows */
+    *inst_len += 1;
+    rm.m = sib(cp, x86, inst_len);
+    break;
+  case 0x05:
+    if (cp[1] >= 0x40)
+      rm.m = (addr + x86->ebp + x86->seg_ss_base);
+    else {
+      *inst_len += 4;
+      rm.m = (R_DWORD(cp[2]) + x86->seg_base);
+    }
+    break;
+  }
+  return rm;
+}
+
+static int handle_prefixes(x86_regs *x86)
+{
+  unsigned eip = x86->eip;
+  int prefix = 0;
+
+  for (;; eip++) {
+    switch(*(unsigned char *)MEM_BASE32(x86->cs_base + eip)) {
+    /* handle (some) prefixes */
+      case 0x26:
+        prefix++;
+        x86->seg_base = x86->seg_ss_base = x86->es_base;
+        break;
+      case 0x2e:
+        prefix++;
+        x86->seg_base = x86->seg_ss_base = x86->cs_base;
+        break;
+      case 0x36:
+        prefix++;
+        x86->seg_base = x86->seg_ss_base = x86->ss_base;
+        break;
+      case 0x3e:
+        prefix++;
+        x86->seg_base = x86->seg_ss_base = x86->ds_base;
+        break;
+      case 0x64:
+        prefix++;
+        x86->seg_base = x86->seg_ss_base = x86->fs_base;
+        break;
+      case 0x65:
+        prefix++;
+        x86->seg_base = x86->seg_ss_base = x86->gs_base;
+        break;
+      case 0x66:
+        prefix++;
+        x86->operand_size = 6 - x86->operand_size;
+        if (x86->operand_size == 4) {
+          x86->instr_read = instr_read_dword;
+          x86->instr_write = instr_write_dword;
+        } else {
+          x86->instr_read = instr_read_word;
+          x86->instr_write = instr_write_word;
+        }
+        break;
+      case 0x67:
+        prefix++;
+        x86->address_size = 6 - x86->address_size;
+        x86->modrm = (x86->address_size == 4 ? modrm32 : modrm16);
+        break;
+      case 0xf2:
+        prefix++;
+        x86->rep = REPNZ;
+        break;
+      case 0xf3:
+        prefix++;
+        x86->rep = REPZ;
+        break;
+      default:
+        return prefix;
+    }
+  }
+  return prefix;
+}
+
+static void prepare_x86(x86_regs *x86)
+{
+  x86->seg_base = x86->ds_base;
+  x86->seg_ss_base = x86->ss_base;
+  x86->address_size = x86->operand_size = (x86->_32bit + 1) * 2;
+
+  x86->modrm = (x86->address_size == 4 ? modrm32 : modrm16);
+  x86->rep = REP_NONE;
+
+  if (x86->operand_size == 4) {
+    x86->instr_read = instr_read_dword;
+    x86->instr_write = instr_write_dword;
+  } else {
+    x86->instr_read = instr_read_word;
+    x86->instr_write = instr_write_word;
+  }
+}
+
+#define M(a) (struct rm){.m = (a)}
+
+static void scp_to_x86_regs(x86_regs *x86, cpuctx_t *scp, int pmode)
+{
+  if(pmode) {
+    x86->eax = _eax;
+    x86->ebx = _ebx;
+    x86->ecx = _ecx;
+    x86->edx = _edx;
+    x86->esi = _esi;
+    x86->edi = _edi;
+    x86->ebp = _ebp;
+    x86->esp = _esp;
+    x86->eip = _eip;
+    x86->eflags = _eflags;
+    x86->cs = _cs;
+    x86->ds = _ds;
+    x86->es = _es;
+    x86->ss = _ss;
+    x86->fs = _fs;
+    x86->gs = _gs;
+    x86->cs_base = GetSegmentBase(_cs);
+    x86->ds_base = GetSegmentBase(_ds);
+    x86->es_base = GetSegmentBase(_es);
+    x86->ss_base = GetSegmentBase(_ss);
+    x86->fs_base = GetSegmentBase(_fs);
+    x86->gs_base = GetSegmentBase(_gs);
+    x86->_32bit = _cs && dpmi_segment_is32(_cs) ? 1 : 0;
+  }
+  else {
+    x86->eax = REG(eax);
+    x86->ebx = REG(ebx);
+    x86->ecx = REG(ecx);
+    x86->edx = REG(edx);
+    x86->esi = REG(esi);
+    x86->edi = REG(edi);
+    x86->ebp = REG(ebp);
+    x86->esp = REG(esp);
+    x86->eip = REG(eip);
+    x86->eflags = REG(eflags);
+    x86->cs = SREG(cs);
+    x86->ds = SREG(ds);
+    x86->es = SREG(es);
+    x86->ss = SREG(ss);
+    x86->fs = SREG(fs);
+    x86->gs = SREG(gs);
+    x86->cs_base = SEGOFF2LINEAR(x86->cs, 0);
+    x86->ds_base = SEGOFF2LINEAR(x86->ds, 0);
+    x86->es_base = SEGOFF2LINEAR(x86->es, 0);
+    x86->ss_base = SEGOFF2LINEAR(x86->ss, 0);
+    x86->fs_base = SEGOFF2LINEAR(x86->fs, 0);
+    x86->gs_base = SEGOFF2LINEAR(x86->gs, 0);
+    x86->_32bit = 0;
+  }
+  prepare_x86(x86);
+}
+
+static void x86_regs_to_scp(x86_regs *x86, cpuctx_t *scp, int pmode)
+{
+  if(pmode) {
+    _cs = x86->cs;
+    _ds = x86->ds;
+    _es = x86->es;
+    _fs = x86->fs;
+    _gs = x86->gs;
+    _ss = x86->ss;
+    _eax = x86->eax;
+    _ebx = x86->ebx;
+    _ecx = x86->ecx;
+    _edx = x86->edx;
+    _esi = x86->esi;
+    _edi = x86->edi;
+    _ebp = x86->ebp;
+    _esp = x86->esp;
+    _eip = x86->eip;
+    _eflags = x86->eflags;
+  }
+  else {
+    REG(eax) = x86->eax;
+    REG(ebx) = x86->ebx;
+    REG(ecx) = x86->ecx;
+    REG(edx) = x86->edx;
+    REG(esi) = x86->esi;
+    REG(edi) = x86->edi;
+    REG(ebp) = x86->ebp;
+    REG(esp) = x86->esp;
+    REG(eip) = x86->eip;
+    REG(eflags) = x86->eflags;
+  }
+}
+
+int decode_modify_segreg_insn(cpuctx_t *scp, int pmode,
+    unsigned int *new_val)
+{
+  struct rm mem = {};
+  unsigned cs;
+  int inst_len, ret = -1;
+  x86_regs x86;
+
+  scp_to_x86_regs(&x86, scp, pmode);
+
+  cs = x86.cs_base;
+  x86.prefixes = handle_prefixes(&x86);
+  x86.eip += x86.prefixes;
+
+  switch(*(unsigned char *)MEM_BASE32(cs + x86.eip)) {
+    case 0x8e:		/* mov segreg,r/m16 */
+      ret = sreg_idx(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3);
+      mem = x86.modrm(MEM_BASE32(cs + x86.eip), &x86, &inst_len);
+      if ((*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) & 0xc0) == 0xc0)  /* compensate for mov r,segreg */
+        memcpy(new_val, reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1), &x86), 2);
+      else
+        *new_val = instr_read_word(mem);
+      x86.eip += inst_len + 2;
+      break;
+
+    case 0xca: /*retf imm 16*/
+    case 0xcb: /*retf*/
+    case 0xcf: /*iret*/
+    {
+      unsigned tmp_eip;
+      pop(&tmp_eip, &x86);
+      pop(new_val, &x86);
+      ret = cs_INDEX;
+      switch (*(unsigned char *)MEM_BASE32(cs + x86.eip)) {
+        case 0xca: /*retf imm 16*/
+	  x86.esp += ((unsigned short *) (MEM_BASE32(cs + x86.eip + 1)))[0];
+	  break;
+        case 0xcf: /*iret*/
+	{
+	  unsigned flags;
+          pop(&flags, &x86);
+	  x86.eflags = flags;
+	  break;
+	}
+      }
+      x86.eip = tmp_eip;
+    }
+    break;
+
+    case 0xea:			/* jmp seg:off16/off32 */
+    {
+      unsigned tmp_eip;
+      tmp_eip = x86.instr_read(M(x86.cs_base + x86.eip + 1));
+      *new_val = instr_read_word(M(x86.cs_base + x86.eip + 1 + x86.operand_size));
+      ret = cs_INDEX;
+      x86.eip = tmp_eip;
+    }
+    break;
+
+    case 0xc4:		/* les */
+      mem = x86.modrm(MEM_BASE32(cs + x86.eip), &x86, &inst_len);
+      *new_val = instr_read_word(M(mem.m+x86.operand_size));
+      if (x86.operand_size == 2)
+	R_WORD(*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86)) = instr_read_word(mem);
+      else
+	*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86) = instr_read_dword(mem);
+      ret = es_INDEX;
+      x86.eip += inst_len + 2;
+      break;
+
+    case 0xc5:		/* lds */
+      mem = x86.modrm(MEM_BASE32(cs + x86.eip), &x86, &inst_len);
+      *new_val = instr_read_word(M(mem.m+x86.operand_size));
+      if (x86.operand_size == 2)
+	R_WORD(*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86)) = instr_read_word(mem);
+      else
+	*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86) = instr_read_dword(mem);
+      ret = ds_INDEX;
+      x86.eip += inst_len + 2;
+      break;
+
+    case 0x07:	/* pop es */
+    case 0x17:	/* pop ss */
+    case 0x1f:	/* pop ds */
+      ret = sreg_idx(*(unsigned char *)MEM_BASE32(cs + x86.eip) >> 3);
+      pop(new_val, &x86);
+      x86.eip++;
+      break;
+
+    case 0x0f:
+      x86.eip++;
+      switch (*(unsigned char *)MEM_BASE32(cs + x86.eip)) {
+        case 0xa1:	/* pop fs */
+        case 0xa9:	/* pop gs */
+	  pop(new_val, &x86);
+	  ret = sreg_idx(*(unsigned char *)MEM_BASE32(cs + x86.eip) >> 3);
+	  x86.eip++;
+	  break;
+
+	case 0xb2:	/* lss */
+	  mem = x86.modrm(MEM_BASE32(cs + x86.eip), &x86, &inst_len);
+	  *new_val = instr_read_word(M(mem.m+x86.operand_size));
+	  if (x86.operand_size == 2)
+	    R_WORD(*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86)) = instr_read_word(mem);
+	  else
+	    *reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86) = instr_read_dword(mem);
+	  ret = ss_INDEX;
+	  x86.eip += inst_len + 2;
+	  break;
+
+	case 0xb4:	/* lfs */
+	  mem = x86.modrm(MEM_BASE32(cs + x86.eip), &x86, &inst_len);
+	  *new_val = instr_read_word(M(mem.m+x86.operand_size));
+	  if (x86.operand_size == 2)
+	    R_WORD(*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86)) = instr_read_word(mem);
+	  else
+	    *reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86) = instr_read_dword(mem);
+	  ret = fs_INDEX;
+	  x86.eip += inst_len + 2;
+	  break;
+
+	case 0xb5:	/* lgs */
+	  mem = x86.modrm(MEM_BASE32(cs + x86.eip), &x86, &inst_len);
+	  *new_val = instr_read_word(M(mem.m+x86.operand_size));
+	  if (x86.operand_size == 2)
+	    R_WORD(*reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86)) = instr_read_word(mem);
+	  else
+	    *reg(*(unsigned char *)MEM_BASE32(cs + x86.eip + 1) >> 3, &x86) = instr_read_dword(mem);
+	  ret = gs_INDEX;
+	  x86.eip += inst_len + 2;
+	  break;
+      }
+      break;
+  }
+
+  x86_regs_to_scp(&x86, scp, pmode);
+  return ret;
+}
diff --git a/src/include/instremu.h b/src/include/instremu.h
index 72e7f865d..0daa3e1a1 100644
--- a/src/include/instremu.h
+++ b/src/include/instremu.h
@@ -2,5 +2,7 @@
 #define INSTREMU_H
 
 int instr_len(unsigned char *ptr, int is_32);
+int decode_modify_segreg_insn(cpuctx_t *scp, int pmode,
+    unsigned int *new_val);
 
 #endif
