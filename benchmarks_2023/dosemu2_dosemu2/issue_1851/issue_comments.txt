separate xms pool
Started to work on that in a branch.
It would be good to implement a
top-down allocation strategy in smalloc
for this. To avoid any chances for
xms to intersect with HX fixed mappings.
Well, not completed.
This all resulted in a "contention"
in mem_base view. So $_ext_mem
was reduced from 8Mb to 3Mb,
$_dpmi_base increased, and so on.

Needs to move this mess away from
mem_base into the lowmem_base
for example, where it should use the
separate smalloc pool. And lin_pool
should be returned to dpmi. If xms
or ext_mem is ever needed to be
paged to mem_base, it should use
the main_pool, not lin_pool.
I probably need to write a test-case
first.
Not completed.
Needs to move ext_mem pool out
of lowmem_base too, as that will
allow to make it even larger.
It can be mapped in via hwram
subsystem by default, but unmapped
if internal xms is used. Then they
will never intersect.