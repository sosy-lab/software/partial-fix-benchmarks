configure: try host asm as cross only on native arches

Even clang being used as asm, doesn't support non-native targets. :(
dpmi: fix realmode exception checking [#2065]

PM interrupt table was checked instead of RM exception table.
cpu: init fpu cwd to 0x3ff [fixes #2065]

This is per that doc:
http://www.website.masmforum.com/tutorials/fptute/fpuchap1.htm
Initial state is to not generate interrupt.
Revert "cpu: init fpu cwd to 0x3ff [fixes #2065]"

This reverts commit c2c9107eb4bce5d967f07c4f1c7e41e44b5492a9.
dpmi: preserve fpu st across client switch [#2065]
cpu: fix fpu irq injection check [#2065]
dpmi: fixes to FPU state switching [#2065]
dpmi: add missing fpu kvm sync [fixes #2065]

On client start, we need to sync the state with kvm, or we
save garbage as an fpu state.
dpmi: yet another forgotten fpu sync [#2065]
