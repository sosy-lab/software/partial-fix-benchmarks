update startup messages
try to fix vm86 regressed by 5ee681a [#209]
simx86: dont set node limit too high [#209]

This avoids the node pool overflow.
fix VIP handling [fixes #209]

We need to break out of the vm86 optimization loop if VIP is
set together with IF.

This is a regression of the following commits:
Commit 66ad92cd as a strange, unchangelogged change, started to clear
VIP on setting VIF.
Commit f2d72617 adds the optimization loop in question, but does
not check the VIF&&VIP condition, presumably because it was already
broken by 66ad92cd and could never actually happen.
The breakage wouldn't be visible if not for the commit 5f263dd6b
that makes it possible for the VIF flag to change in the hlt handler.
fix VIP handling [fixes #209]

We need to break out of the vm86 optimization loop if VIP is
set together with IF.

This is a regression of the following commits:
Commit 66ad92cd as a strange, unchangelogged change, started to clear
VIP on setting VIF.
Commit f2d72617 adds the optimization loop in question, but does
not check the VIF&&VIP condition, presumably because it was already
broken by 66ad92cd and could never actually happen.
Commit 11ae67fd also clears VIP for some odd reason.

The breakage wouldn't be visible if not for the commit 5f263dd6b
that makes it possible for the VIF flag to change in the hlt handler.
vm86: restore VIP cleared by kernel [fixes #209]

Kernel appears to have many bugs with flags handling.
In particular it has a nasty habit of clearing the VIP flag.
This became visible after commit 5ee681a which removed pic_iret() -
pic_iret() had the effect of the VIP flag in most cases.
