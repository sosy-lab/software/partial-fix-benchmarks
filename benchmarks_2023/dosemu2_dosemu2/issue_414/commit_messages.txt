update version to 2.0pre7
fix clang compilation [fixes #414]
sdl: assert on loading syms from X plugin [#414]
sdl: add HAVE_XKB ifdefs [#414]
pci: fix proc parsing [#414]
sdl: avoid unloading X plugin [#414]

SDL unloads X symbols in SDL_X11_UnloadSymbols(), and
for some reason this dlclose()s also our X plugin!
So init X11 support after SDL already initialized video, not before.
use dlerror() for dlsym error logging [#414]
video: validate config options after term fallback [#414]
video: fix dumb terminal fallback [fixes #414]

It should only trigger when terminal mode is requested.
It was wrongly triggering also for sdl.
