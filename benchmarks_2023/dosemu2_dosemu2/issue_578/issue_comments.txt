Mouse not working in terminal mode
Does anyone else see this?
Was unable to reproduce just now :/  

Only tested in Tilix + Gnome-Terminal, with Free Dos EDIT so far.
Me neither (gnome-terminal, xterm), and no one else complained in the past.
Please attach `-D9+km`  log.
[boot.termite.good.log.zip](https://github.com/stsp/dosemu2/files/1810209/boot.termite.good.log.zip)
[boot.xfce.bad.log.zip](https://github.com/stsp/dosemu2/files/1810210/boot.xfce.bad.log.zip)

Should now be fixed.
Not yet.

[boot.termite2.good.log.zip](https://github.com/stsp/dosemu2/files/1813551/boot.termite2.good.log.zip)
[boot.xfce2.bad.log.zip](https://github.com/stsp/dosemu2/files/1813552/boot.xfce2.bad.log.zip)

Please re-do the log.
[boot.xfce3.bad.log.zip](https://github.com/stsp/dosemu2/files/1813981/boot.xfce3.bad.log.zip)
[boot.termite3.good.log.zip](https://github.com/stsp/dosemu2/files/1813983/boot.termite3.good.log.zip)

I just noticed the commit. These are not the logs you're looking for. Try these.

[boot.termite4.good.log.zip](https://github.com/stsp/dosemu2/files/1814549/boot.termite4.good.log.zip)
[boot.xfce4.bad.log.zip](https://github.com/stsp/dosemu2/files/1814550/boot.xfce4.bad.log.zip)

Please compile this small test program:
https://invisible-island.net/ncurses/tctest.html#download
and post its output for those 2 terminals.
Includes XFCE, xterm, Gnome, and termite.

[tctest.zip](https://github.com/stsp/dosemu2/files/1816594/tctest.zip)

There is something wrong with your "Km" setting.
On "bad" terminals it is `:Km=\E[<:\` and on good
`:Km=\E[M:\`, as it should be.
Does something change if you do on bad terminals:
`export TERM=xterm-termite`
before running `dosemu -t`?
Also please produce the tctest output after that
command.
That `export` makes the mouse work in XFCETerm. `tctest` reports the exact file of `tctest.termite.txt` (2881).

Downgrading [ncurses](https://www.archlinux.org/packages/core/x86_64/ncurses/) 6.1 -> 6.0 fixes the problem. Every terminal in the list works.

So not a dosemu problem.
````
misc/terminfo.src
# As an alternative (and fixing the longstanding limitation of X11 mouse
# protocol regarding button-releases), I provided this:
xterm+sm+1006|xterm SGR-mouse,
  kmous=\E[<, XM=\E[?1006;1000%?%p1%{1}%=%th%el%;,
````

````
sed -e 's:E\[<,:E[M,:g' -i 'misc/terminfo.src'
````

Patching this line fixes it but this line wasn't changed from 6.0 -> 6.1. Something else was changed to put this code into the path of xterm-256color and xterm. xterm-termite was not affected.

Every instance of `\E[<` except this one has more that comes after.

Why is `dosemu -t` affected but `mc` and `ranger` are not?

Please see what `infocmp |grep kmous` says, as
it seems `mc` looks for `kmous` (I have no idea how
and why it differs from `Km` etc).
````
% pacman -Q ncurses # not patched
ncurses 6.1-3

% echo $TERM; infocmp |grep kmous
xterm-termite
	kind=\E[1;2B, kmous=\E[M, knp=\E[6~, kpp=\E[5~,

% echo $TERM; infocmp |grep kmous
xterm-256color
	kind=\E[1;2B, kmous=\E[<, knp=\E[6~, kpp=\E[5~,
````

````
% pacman -Q ncurses              
ncurses 6.0+20170902-3

% echo $TERM; infocmp |grep kmous                        :(
xterm-256color
	kind=\E[1;2B, kmous=\E[M, knp=\E[6~, kpp=\E[5~,

% echo $TERM; infocmp |grep kmous
xterm-termite
	kind=\E[1;2B, kmous=\E[M, knp=\E[6~, kpp=\E[5~,
````

OK, it appears SGR is a new xterm mouse protocol
which dosemu2 does not support and mc does.
Your ncurses altered existing Km/kmous setting
instead of providing the new one, so it broke all
older applications. This is a bug that should be
fixed on their side. Of course my understanding
of the matters may be completely wrong.
It's a bit tricky to capture keyboard and mouse responses. I did it by killing `mc` which leaves mouse mode active then I use `od -c` to dump the codes.

````
^[[<0;28;24M^[[<0;28;24m^[[<2;32;24M^[[<2;32;24m
````

Reporting mouse events through `Esc [ <` works.

[Stack Overflow: How to enable extended mouse mode?](https://stackoverflow.com/questions/4681302/how-to-enable-extended-mouse-mode)
[LeoNerd's: Wide mouse support in libvterm / libtermkey](http://leonerds-code.blogspot.co.uk/2012/04/wide-mouse-support-in-libvterm.html)

````
MCR/MCR/MCQ/CM-MMCG+F,MC,MCMCE-MCE-MCE-MCF-MCG-MCG-MCG-MCG-MCG-MCG-MCG-MCG-M
````
Whether the mouse reports `Esc M` or `Esc <` codes depends on the requested mouse mode. How can a single `kmous/Km` handle this?

> Reporting mouse events through Esc [ < works.

Like I said, its a new protocol.
You can change `mouse_xterm.c:105` and replace
last 1003 with 1006 to activate it. But of course
dosemu2 will still not parse it correctly.

> How can a single kmous/Km handle this?

In no way, I suppose.
I think they just broke the compatibility.
Note that previously the new protocol worked
nevertheless in mc as mc had the new value
hardcoded.
So there was hardly any reason for a breakage.
It's not hardcoded in `mc`. I downgraded to 6.0 and now the `mc` mouse events are `Esc M` instead of `Esc <`. A possible explanation is that `mc` looks at `kmous/Km` to pick the mouse mode. Another possibility is that `ncurses` has gained another hack to make `Km` report the right value so long as you read it after choosing the mouse mode.

The reason is clear, the old mode sucks and the new mode doesn't. The problem is how to get everyone on the new mode without breakage.
mc hardcodes it:
https://github.com/MidnightCommander/mc/blob/master/lib/tty/tty.c#L313
Then it tries to enable both protocols, and
is prepared to parse either. Maybe the xterm
itself also looks into a termcap database and
doesn't enable the extended protocol under
some conditions? As you said, the "Km" change
is not new, so maybe something else just
prevents the xterm from enabling new protocol.
This will help you to enable mouse:
`echo -e "\033[?9h\033[?1000h\033[?1002h\033[?1003h"`

You can use 1006 to enable new mode.
`echo -e "\033[?9h\033[?1000h\033[?1002h\033[?1003h\033[?1006h"`

This enables new mode.
You actually need both 1003 and 1006.
Seems 1006 only affects button presses/releases.
In fact I think 1003, while using the new escape
code for you, may still use the old protocol if we
don't use 1006. Please check if this is true. If so,
the only change I'll need to do is to hardcode the
new escape. This is feasible.
Grr, wrong, it doesn't use the new escape
code for you. It only codes it in Km, but
doesn't use. Ignore the prev comment.
Grr, now this shit also plaques fedora...
I thought you asked them to please not
break the existing apps. :)
I implemented SGR support, so should now be fixed.
ncurses actually provided the fall-back descriptions,
for example the one could do
`TERM="xterm-1003" dosemu -t`
to get the mouse working, or
`SLtt_initialize("xterm-1003");`
would do the same thing.
And yet I think this was a very hostile compatibility
break.