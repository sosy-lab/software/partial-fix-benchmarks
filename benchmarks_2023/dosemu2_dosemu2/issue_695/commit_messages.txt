int: fix new warning on 32bit [fixes #696]
MFS: Display disabled drive state in lredir

If Dosemu has a drive configured for redirection, but the CDS flags are
not set for DOS to recognise it, have lredir display the DISABLED
attribute.

Note: This patch removes the CDROM unit number display code from lredir,
as it was ineffective because the corresponding bits were never set in
mfs:GetRedirection().

[Fixes #695]
MFS: Display disabled drive state in lredir

If Dosemu has a drive configured for redirection, but the CDS flags are
not set for DOS to recognise it, have lredir display the DISABLED
attribute.

Note: This patch removes the CDROM unit number display code from lredir,
as it was ineffective because the corresponding bits were never set in
mfs:GetRedirection().

[Fixes #695]
MFS: Display disabled drive state in lredir

If Dosemu has a drive configured for redirection, but the CDS flags are
not set for DOS to recognise it, have lredir display the DISABLED
attribute.

Note: This patch removes the CDROM unit number display code from lredir,
as it was ineffective because the corresponding bits were never set in
mfs:GetRedirection().

[Fixes #695]
Revert "MFS: Display disabled drive state in lredir"

This reverts commit 9a0a9643a2260dfb5882fca25f6c2a4e2e14ea02.

This patch is wrong.
It assumes that cdrom flags are not in effect, but they
actually come from the "read_only" integer field.

I would suggest to not hijack CX for device flags, as it
is documented to be the user-supplied value. We can use
DX for our extensions instead.

Reopens #695
mfs: move device flags to DX [#695]

CX is documented to be the user parameter.
We can't abuse it for our own needs.
cdrom handling is now much cleaner.
MFS: Display disabled drive state in lredir [fixes #695]

1/ Lredir sets the redirector signature in CX when using int21/5f02 so
that mfs.c may return additional info in DX register.

2/ Lredir checks the resource string to ensure it's from mfs.c before
interpreting DX register contents.

3/ If Dosemu has a drive configured for redirection, but the CDS flags
are not set for DOS to recognise it, have lredir display the DISABLED
attribute.
MFS: Display disabled drive state in lredir [fixes #695]

1/ Lredir sets the redirector signature in CX when using int21/5f02 so
that mfs.c may return additional info in DX register.

2/ Lredir checks the resource string to ensure it's from mfs.c before
interpreting DX register contents.

3/ If Dosemu has a drive configured for redirection, but the CDS flags
are not set for DOS to recognise it, have lredir display the DISABLED
attribute.
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) The redirector checks for matching signature before writing to
      the caller's DX return register.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) The redirector checks for matching signature before writing to
      the caller's DX return register.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) The redirector checks for matching signature before writing to
      the caller's DX return register.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) The redirector checks for matching signature before writing to
      the caller's DX return register.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) The redirector checks for matching signature before writing to
      the caller's DX return register.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) The redirector checks for matching signature before writing to
      the caller's DX return register.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) Since RBIL suggests that the caller's DX is destroyed by other
      redirectors, we may use it to return Dosemu specific data.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) Since RBIL suggests that the caller's DX is destroyed by other
      redirectors, we may use it to return Dosemu specific data.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
MFS: Display disabled drive state in lredir

1/ Whilst creating a redirection DoRedirectDevice() needs to determine
   if it can use Dosemu specific Options.
   a) Have com_RedirectDevice() sign the request by placing Dosemu's
      signature in the UserData field (CX register).
   b) When dealing with a request the redirector can use the UserData
      signature to determine if the Options data (DX register) is valid.

2/ Ensure the custom data from client's com_GetRedirection() is valid.
   a) Lredir signs an int21/5f02 request using the CX register, see
      http://www.ctyme.com/intr/rb-3042.htm.
   b) Since RBIL suggests that the caller's DX is destroyed by other
      redirectors, we may use it to return Dosemu specific data.

3/ Ensure the custom data from redirector's GetRedirection() is valid.
   Lredir checks the returned resource string to ensure it's from
   our redirector before interpreting Options data (DX register)
   which holds the dosemu specifics.

4/ Have redirector's GetRedirection() add Options data if Dosemu has a
   drive configured for redirection but the CDS flags are not set for
   DOS to recognise it as valid. Lredir interprets the Options data
   and displays the DISABLED attribute as necessary. [fixes #695]
