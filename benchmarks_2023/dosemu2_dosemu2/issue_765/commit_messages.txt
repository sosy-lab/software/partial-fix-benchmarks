Merge pull request #764 from andrewbird/dosdebug-34

Dosdebug 34
config: disable DPMI DOS support if not enough EMS [#765]

This is instead of exiting dosemu with "DPMI must be disabled" msg.
emm: dont split UMBs over page frame if no UMA pages [#765]
xms: fix source handle check to avoid crash [#765]
fatfs: fix lfn detection [#765]
config: exit if no drives defined [#765]
hma: avoid e_invalidate_full() on init [#765]

It may crash if cpu-emu is not yet inited.
fatfs: introduce pre-boot hook [fixes #765]

This hook is used by fdpp to register the needed callbacks.
