LFN file creation not making file
Would you like to provide the fix as well? :)
I think it changed with your recent int21/7100 changes, so you'll probably have better idea where to look.
Something like this "fixes" it:
```
diff --git a/src/dosext/mfs/lfn.c b/src/dosext/mfs/lfn.c
index bc8b94b6c..6f6cc3c4c 100644
--- a/src/dosext/mfs/lfn.c
+++ b/src/dosext/mfs/lfn.c
@@ -701,6 +701,7 @@ static int make_finddata(const char *fpath, uint8_t attrs,
        return 1;
 }
 
+#include "coopth.h"
 static void call_dos_helper(int ah)
 {
        unsigned int ssp = SEGOFF2LINEAR(_SS, 0);
@@ -711,6 +712,12 @@ static void call_dos_helper(int ah)
        _SP -= 4;
        _CS = LFN_HELPER_SEG;
        _IP = LFN_HELPER_OFF;
+//error("sched\n");
+coopth_sched();
+//error("sched done\n");
+static int cnt;
+if (!cnt++)
+CARRY;
 }
 
 
```

No idea yet what's going on with CF.
I think if it worked before, it was a pure luck.
This `call_dos_helper()` is incompatible with the
current revect code. But if I use `coopth_sched();`
to separate them from each other, something
goes wrong with CF.
Should now be fixed.
But this asm code appears to be very fragile now.
Everything should be re-tested, including the recently
fixed test-cases of @ecm-pushbx.
Mmm, this doesn't seem to have helped. Now all the LFN tests are failing, at least on MS-DOS 6.22
~~~
Test MS-DOS-6.22 MFS LFN directory change current ... FAIL
Test MS-DOS-6.22 MFS LFN directory create ... FAIL
Test MS-DOS-6.22 MFS LFN directory delete ... FAIL
Test MS-DOS-6.22 MFS LFN directory delete not empty ... FAIL
Test MS-DOS-6.22 MFS LFN file append ... FAIL
Test MS-DOS-6.22 MFS LFN file create ... FAIL
Test MS-DOS-6.22 MFS LFN file create readonly ... FAIL
Test MS-DOS-6.22 MFS LFN file find ... FAIL
Test MS-DOS-6.22 MFS LFN file read ... FAIL
Test MS-DOS-6.22 MFS LFN file truncate ... FAIL
Test MS-DOS-6.22 MFS LFN get current directory ... FAIL
Test MS-DOS-6.22 MFS LFN Truename ... FAIL
Test MS-DOS-6.22 MFS lredir2 command redirection ... ok
Test MS-DOS-6.22 MFS SFN directory change current ... ok
Test MS-DOS-6.22 MFS SFN directory create ... ok
Test MS-DOS-6.22 MFS SFN directory delete ... ok
Test MS-DOS-6.22 MFS SFN directory delete not empty ... ok
Test MS-DOS-6.22 MFS SFN file append ... ok
Test MS-DOS-6.22 MFS SFN file create ... ok
Test MS-DOS-6.22 MFS SFN file create readonly ... ok
Test MS-DOS-6.22 MFS SFN file find ... ok
Test MS-DOS-6.22 MFS SFN file read ... ok
Test MS-DOS-6.22 MFS SFN file truncate ... ok
Test MS-DOS-6.22 MFS SFN get current directory ... ok
Test MS-DOS-6.22 MFS SFN Truename ... ok
~~~

I won't be able to test again until monday.
But the test you attached here, does work,
doesnt it?
Not for me, function returns success but the file is created with SFN

~~~
ajb@polly:/clients/common/dosemu2.git$ nosetests -v test/test_bootos.py:MSDOS622TestCase.test_mfs_lfn_file_create
Test MS-DOS-6.22 MFS LFN file create ... FAIL

======================================================================
FAIL: Test MS-DOS-6.22 MFS LFN file create
----------------------------------------------------------------------
Traceback (most recent call last):
  File "/clients/common/dosemu2.git/test/test_bootos.py", line 843, in test_mfs_lfn_file_create
    self._test_mfs_file_write("LFN", "create")
  File "/clients/common/dosemu2.git/test/test_bootos.py", line 838, in _test_mfs_file_write
    self.fail("File not created/opened")
AssertionError: File not created/opened

----------------------------------------------------------------------
Ran 1 test in 3.805s

FAILED (failures=1)
~~~

I have to go now.


No, I checked it on a host FS, and it has full name.
Also `dir /lfn` in freecom displays it properly.
And in the log:
```
MFS: Large file locking start=100000000, len=1
MFS: create succeeds: '/home/stas/.dosemu/drive_c/lfn/verylongname.txt' fd = 0x26
MFS: size = 0x0
MFS: Finished dos_fs_redirect
```
Actually I would be very surprised if it
is created with sfn. The technique is to
store LFN in pre-defined location, and
then SFN create finds it and creates LFN.
So the only way to create SFN-only is to
somehow not find it via `sda_user_stack(sda)`.
Tried on PC-DOS: creates `verylong txt`, which is a bit odd.
Fixed the PC-DOS case.
Yes was in a hurry, the name I got was SFN 'verylong.txt', not the mangled version. Will check later if your pc-dos changed fixed it for me.
Looks to be fixed now, thanks.
~~~
Test MS-DOS-6.22 MFS LFN directory change current ... ok
Test MS-DOS-6.22 MFS LFN directory create ... ok
Test MS-DOS-6.22 MFS LFN directory delete ... ok
Test MS-DOS-6.22 MFS LFN directory delete not empty ... ok
Test MS-DOS-6.22 MFS LFN file append ... ok
Test MS-DOS-6.22 MFS LFN file create ... ok
Test MS-DOS-6.22 MFS LFN file create readonly ... ok
Test MS-DOS-6.22 MFS LFN file find ... ok
Test MS-DOS-6.22 MFS LFN file read ... ok
Test MS-DOS-6.22 MFS LFN file truncate ... ok
Test MS-DOS-6.22 MFS LFN get current directory ... ok
Test MS-DOS-6.22 MFS LFN Truename ... ok
~~~
Thanks for testing.
I wonder why all the hell broke
on this revect code and suddenly
everyone reported bugs on what
was there for over an year already...
Good if such a core int handling is
finally fixed.