Support Eurotronic Spirit (SPZB0001) thermostat (untestet)


Issue https://github.com/dresden-elektronik/deconz-rest-plugin/issues/1098

https://eurotronic.org/produkte/zigbee-heizkoerperthermostat/spirit-zigbee/
Improve support of Eurotronic Spirit thermostat (SPZB0001) (wip)

* Fix creating sensor resources (was skipped when not reading basic cluster manually)
* Fix repeated binding by recording 'local temperature' value
* Refactor thermostat module, keep variables more local and use library zcl attribute code
* Add some notes for polling and manufacturer specific attributes

Issue https://github.com/dresden-elektronik/deconz-rest-plugin/issues/1098
ZCLDB: Add more thermostat attributes

Related: https://github.com/dresden-elektronik/deconz-rest-plugin/issues/1098
Improve support for Eurotronics Spirit SPZB0001 thermostat

Issue https://github.com/dresden-elektronik/deconz-rest-plugin/issues/1098

* ZCLDB: add manufacturer specific attributes from official documentation
* Only use reporting to update attributes
* Fix state.on shall be true, when heating is active
* Update config.heatsetpoint when it is changed manually on the device
* Remove unsupported schedules attributes for this thermostat
Fixed the updating of the current heatpoint for newer Eurotronic Spirit Zigbee thermostats. Newer models must use the 0x0012 Occupied Heating Setpoint to manipulate the set temperature. Writing to the manufacturer specific 0x4003 attribute is not allowed anymore for newer models. This closes issue #1098
Simpeler and more robust but more ugly way to fix the change heating setpoint problem (issue #1098). This fix will always send both codes (0x4003 and 0x0012) to the thermostat one of which will be accepted while the other one fails. Although definitely not a nice fix this currently seems the only way to make the expected behaviour reliable. We have received word from the manufacturer is working on a firmware fix that can be installed OTA but until then this fix needs to remain in place.
Merge pull request #3680 from joukestoel/master

Second attempt to make the heating setpoint changeable for all Eurotronic Spirit Zigbee thermostats (issue #1098)
