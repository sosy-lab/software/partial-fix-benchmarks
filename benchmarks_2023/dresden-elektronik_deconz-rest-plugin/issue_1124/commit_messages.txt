Implement the rest of the capabilities api
Don't poll iCasa Pulse keypads for battery percentage

It appears these keypads report battery percentage periodically after all, slightly under every four hours. see https://github.com/dresden-elektronik/deconz-rest-plugin/issues/1509#issuecomment-493671362 and https://github.com/dresden-elektronik/deconz-rest-plugin/issues/1124.
Support iCasa in-wall switch

See #1124.
Update light_node.cpp

Fix `type` for iCasa in-wall switch, see #1124.
