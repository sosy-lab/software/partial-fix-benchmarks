unaddrs in dyld code accessing the DR library through intermediaries
Still seeing similar things:

```
~~Dr.M~~ Error #2: UNADDRESSABLE ACCESS: reading 0x000e700c-0x000e7010 4 byte(s)
~~Dr.M~~ # 0 libdyld.dylib!NSVersionOfRunTimeLibrary
~~Dr.M~~ # 1 CoreFoundation!_CFExecutableLinkedOnOrAfter
~~Dr.M~~ # 2 CoreFoundation!__CFStringComputeEightBitStringEncoding
~~Dr.M~~ # 3 CoreFoundation!__CFStringCreateImmutableFunnel3
~~Dr.M~~ # 4 CoreFoundation!CFStringCreateWithCString
~~Dr.M~~ # 5 CoreFoundation!_CFBundleResourcesInitialize
~~Dr.M~~ # 6 CoreFoundation!CFBundleGetTypeID
~~Dr.M~~ # 7 CoreFoundation!__CFInitialize
~~Dr.M~~ # 8 dyld!ImageLoaderMachO::doImageInit
~~Dr.M~~ # 9 dyld!ImageLoaderMachO::doInitialization
~~Dr.M~~ #10 dyld!ImageLoader::recursiveInitialization
~~Dr.M~~ #11 dyld!ImageLoader::recursiveInitialization
~~Dr.M~~ #12 dyld!ImageLoader::recursiveInitialization
~~Dr.M~~ #13 dyld!ImageLoader::recursiveInitialization
~~Dr.M~~ #14 dyld!ImageLoader::recursiveInitialization
~~Dr.M~~ #15 dyld!ImageLoader::recursiveInitialization
~~Dr.M~~ #16 dyld!ImageLoader::processInitializers
~~Dr.M~~ #17 dyld!ImageLoader::runInitializers
~~Dr.M~~ #18 dyld!dyld::initializeMainExecutable
~~Dr.M~~ Note: @0:00:21.234 in thread 537360
~~Dr.M~~ Note: instruction: cmp    0x0c(%esi) $0x00000006
```

module load event: "libdynamorio.dylib" 0x000e7000-0x00839000 modid: 3 /Users/cutler/derek/drmemory/git/build_x86_dbg/dynamorio/lib32/debug/libdynamorio.dylib

Hmm, it's "libdyld" instead of "dyld"!
