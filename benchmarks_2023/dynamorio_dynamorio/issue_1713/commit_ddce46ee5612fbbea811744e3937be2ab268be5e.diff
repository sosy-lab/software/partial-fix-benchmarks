diff --git a/api/docs/bt.dox b/api/docs/bt.dox
index faef828df56..4102ebce186 100644
--- a/api/docs/bt.dox
+++ b/api/docs/bt.dox
@@ -749,9 +749,10 @@ selection must be special-cased.
 ********************
 \subsection sec_thumb Thumb Mode Addresses
 
-For 32-bit ARM, target addresses passed as event callbacks or as clean call
-targets should have their least significant bit set to 1 if they need to be
-executed in Thumb mode (#DR_ISA_ARM_THUMB).  Addresses obtained via
+For 32-bit ARM, target addresses passed as event callbacks, as clean
+call targets, or as dr_redirect_execution() targets should have
+their least significant bit set to 1 if they need to be executed
+in Thumb mode (#DR_ISA_ARM_THUMB).  Addresses obtained via
 dr_get_proc_address(), or function pointers at the source code level,
 should automatically have this property.  dr_app_pc_as_jump_target() can
 also be used to construct the proper address from an aligned value.
diff --git a/core/lib/instrument.c b/core/lib/instrument.c
index 78ec2e58503..213c7a91bd3 100644
--- a/core/lib/instrument.c
+++ b/core/lib/instrument.c
@@ -6217,7 +6217,7 @@ dr_redirect_execution(dr_mcontext_t *mcontext)
         trace_abort(dcontext);
     }
 
-    dcontext->next_tag = mcontext->pc;
+    dcontext->next_tag = canonicalize_pc_target(dcontext, mcontext->pc);
     dcontext->whereami = WHERE_FCACHE;
     set_last_exit(dcontext, (linkstub_t *)get_client_linkstub());
     transfer_to_dispatch(dcontext, dr_mcontext_as_priv_mcontext(mcontext),
diff --git a/core/lib/instrument.h b/core/lib/instrument.h
index 445004eb165..a292a4fdd8a 100644
--- a/core/lib/instrument.h
+++ b/core/lib/instrument.h
@@ -5427,6 +5427,10 @@ DR_API
  * (dr_register_exception_event()).  From a signal event callback, use the
  * DR_SIGNAL_REDIRECT return value rather than calling this routine.
  *
+ * \note For ARM, to redirect execution to a Thumb target (#DR_ISA_ARM_THUMB),
+ * set the least significant bit of the mcontext pc to 1. Reference
+ * \ref sec_thumb for more information.
+ *
  * \return false if unsuccessful; if successful, does not return.
  */
 bool
diff --git a/ext/drwrap/drwrap.c b/ext/drwrap/drwrap.c
index ff7364099f4..2e9c3b1526f 100644
--- a/ext/drwrap/drwrap.c
+++ b/ext/drwrap/drwrap.c
@@ -743,8 +743,7 @@ drwrap_skip_call(void *wrapcxt_opaque, void *retval, size_t stdcall_args_size)
 #ifdef X86
     wrapcxt->mc->xsp += stdcall_args_size + sizeof(void*)/*retaddr*/;
 #endif
-    /* be sure to clear LSB */
-    wrapcxt->mc->pc = dr_app_pc_as_load_target(DR_ISA_ARM_THUMB, wrapcxt->retaddr);
+    wrapcxt->mc->pc = wrapcxt->retaddr;
     return true;
 }
 
diff --git a/suite/tests/client-interface/drwrap-test.dll.c b/suite/tests/client-interface/drwrap-test.dll.c
index 1edfe639944..4a18fddb658 100644
--- a/suite/tests/client-interface/drwrap-test.dll.c
+++ b/suite/tests/client-interface/drwrap-test.dll.c
@@ -60,6 +60,9 @@ static int replace_callsite(int *x);
 static uint load_count;
 static bool repeated = false;
 static ptr_uint_t repeat_xsp;
+#ifdef ARM
+static ptr_uint_t repeat_link;
+#endif
 
 static int tls_idx;
 
@@ -360,8 +363,12 @@ wrap_pre(void *wrapcxt, OUT void **user_data)
         dr_fprintf(STDERR, "  <pre-skipme>\n");
         drwrap_skip_call(wrapcxt, (void *) 7, 0);
     } else if (drwrap_get_func(wrapcxt) == addr_repeat) {
+        dr_mcontext_t *mc = drwrap_get_mcontext(wrapcxt);
         dr_fprintf(STDERR, "  <pre-repeat#%d>\n", repeated ? 2 : 1);
-        repeat_xsp = drwrap_get_mcontext(wrapcxt)->xsp;
+        repeat_xsp = mc->xsp;
+#ifdef ARM
+        repeat_link = mc->lr;
+#endif
         if (repeated) /* test changing the arg value on the second pass */
             drwrap_set_arg(wrapcxt, 0, (void *)2);
         CHECK(drwrap_redirect_execution(NULL) != DREXT_SUCCESS,
@@ -400,6 +407,9 @@ wrap_post(void *wrapcxt, void *user_data)
             dr_mcontext_t *mc = drwrap_get_mcontext(wrapcxt);
             mc->pc = addr_repeat;
             mc->xsp = repeat_xsp;
+#ifdef ARM
+            mc->lr = repeat_link;
+#endif
             CHECK(drwrap_redirect_execution(wrapcxt) == DREXT_SUCCESS,
                   "redirect rejected");
             CHECK(drwrap_redirect_execution(wrapcxt) != DREXT_SUCCESS,
