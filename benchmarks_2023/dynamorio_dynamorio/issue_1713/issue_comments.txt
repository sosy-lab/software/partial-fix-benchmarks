Add drwrap_redirect_execution
Could you comment as to why this is needed: why dr_redirect_execution() can't just be used as-is.

The drwrap-test now fails on ARM:

```
in repeatme with arg 3
  <post-repeat#1>
<Source operand #0 has wrong type/size>
<Application /home/derek/dr/git/build_local/suite/tests/bin/client.drwrap-test (21289) DynamoRIO usage error : instr_encode error: no encoding found (see log)>
```
