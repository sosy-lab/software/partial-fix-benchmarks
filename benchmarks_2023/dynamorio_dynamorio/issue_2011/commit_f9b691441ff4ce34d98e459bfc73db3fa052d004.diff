diff --git a/clients/drcachesim/CMakeLists.txt b/clients/drcachesim/CMakeLists.txt
index 601b7f7e034..cedd5958a41 100644
--- a/clients/drcachesim/CMakeLists.txt
+++ b/clients/drcachesim/CMakeLists.txt
@@ -86,7 +86,7 @@ add_library(raw2trace STATIC
 configure_DynamoRIO_standalone(raw2trace)
 target_link_libraries(raw2trace drfrontendlib)
 
-add_executable(drcachesim
+set(drcachesim_srcs
   launcher.cpp
   analyzer.cpp
   analyzer_multi.cpp
@@ -99,6 +99,11 @@ add_executable(drcachesim
   tracer/instru.cpp
   tracer/instru_online.cpp
   )
+if (DEBUG)
+  # We include the invariants analyzer for testing.
+  set(drcachesim_srcs ${drcachesim_srcs} tests/trace_invariants.cpp)
+endif ()
+add_executable(drcachesim ${drcachesim_srcs})
 # In order to embed raw2trace we need to be standalone:
 configure_DynamoRIO_standalone(drcachesim)
 # Link in our tools:
diff --git a/clients/drcachesim/analyzer_multi.cpp b/clients/drcachesim/analyzer_multi.cpp
index 851d7da6140..8e5437e34a5 100644
--- a/clients/drcachesim/analyzer_multi.cpp
+++ b/clients/drcachesim/analyzer_multi.cpp
@@ -42,6 +42,9 @@
 #include "reader/ipc_reader.h"
 #include "tracer/raw2trace_directory.h"
 #include "tracer/raw2trace.h"
+#ifdef DEBUG
+# include "tests/trace_invariants.h"
+#endif
 
 analyzer_multi_t::analyzer_multi_t()
 {
@@ -123,6 +126,18 @@ analyzer_multi_t::create_analysis_tools()
     if (tools[0] == NULL)
         return false;
     num_tools = 1;
+#ifdef DEBUG
+    if (op_test_mode.get_value()) {
+        tools[1] = new trace_invariants_t(op_offline.get_value(), op_verbose.get_value());
+        if (tools[1] != NULL && !*tools[1]) {
+            delete tools[1];
+            tools[1] = NULL;
+        }
+        if (tools[1] == NULL)
+            return false;
+        num_tools = 2;
+    }
+#endif
     return true;
 }
 
diff --git a/clients/drcachesim/common/options.cpp b/clients/drcachesim/common/options.cpp
index 8ae5990eccc..41156f6e95c 100644
--- a/clients/drcachesim/common/options.cpp
+++ b/clients/drcachesim/common/options.cpp
@@ -230,6 +230,12 @@ droption_t<unsigned int> op_verbose
 (DROPTION_SCOPE_ALL, "verbose", 0, 0, 64, "Verbosity level",
  "Verbosity level for notifications.");
 
+#ifdef DEBUG
+droption_t<bool> op_test_mode
+(DROPTION_SCOPE_ALL, "test_mode", false, "Run sanity tests",
+ "Run extra analyses for sanity checks on the trace.");
+#endif
+
 droption_t<std::string> op_dr_root
 (DROPTION_SCOPE_FRONTEND, "dr", "", "Path to DynamoRIO root directory",
  "Specifies the path of the DynamoRIO root directory.");
diff --git a/clients/drcachesim/common/options.h b/clients/drcachesim/common/options.h
index eb6d4fc1873..46c4ba41f9f 100644
--- a/clients/drcachesim/common/options.h
+++ b/clients/drcachesim/common/options.h
@@ -86,6 +86,9 @@ extern droption_t<unsigned int> op_TLB_L2_assoc;
 extern droption_t<std::string> op_TLB_replace_policy;
 extern droption_t<std::string> op_simulator_type;
 extern droption_t<unsigned int> op_verbose;
+#ifdef DEBUG
+extern droption_t<bool> op_test_mode;
+#endif
 extern droption_t<std::string> op_dr_root;
 extern droption_t<bool> op_dr_debug;
 extern droption_t<std::string> op_dr_ops;
diff --git a/clients/drcachesim/common/trace_entry.h b/clients/drcachesim/common/trace_entry.h
index 64d092e323a..49163f11146 100644
--- a/clients/drcachesim/common/trace_entry.h
+++ b/clients/drcachesim/common/trace_entry.h
@@ -146,6 +146,11 @@ typedef enum {
     // An internal value used for online traces and turned by reader_t into
     // either TRACE_TYPE_INSTR or TRACE_TYPE_INSTR_NO_FETCH.
     TRACE_TYPE_INSTR_MAYBE_FETCH,
+
+    // We separate out the x86 sysenter instruction as it has a hardcoded
+    // return point that shows up as a discontinuity in the user mode program
+    // counter execution sequence.
+    TRACE_TYPE_INSTR_SYSENTER,
 } trace_type_t;
 
 // The sub-type for TRACE_TYPE_MARKER.
@@ -169,11 +174,12 @@ typedef enum {
 extern const char * const trace_type_names[];
 
 // Returns whether the type represents an instruction fetch.
-// Deliberately excludes TRACE_TYPE_INSTR_NO_FETCH.
+// Deliberately excludes TRACE_TYPE_INSTR_NO_FETCH and TRACE_TYPE_INSTR_BUNDLE.
 static inline bool
 type_is_instr(const trace_type_t type)
 {
-    return (type >= TRACE_TYPE_INSTR && type <= TRACE_TYPE_INSTR_BUNDLE);
+    return (type >= TRACE_TYPE_INSTR && type <= TRACE_TYPE_INSTR_RETURN) ||
+        type == TRACE_TYPE_INSTR_SYSENTER;
 }
 
 static inline bool
diff --git a/clients/drcachesim/reader/reader.cpp b/clients/drcachesim/reader/reader.cpp
index 51755857f82..d36a169278a 100644
--- a/clients/drcachesim/reader/reader.cpp
+++ b/clients/drcachesim/reader/reader.cpp
@@ -31,7 +31,6 @@
  */
 
 #include <assert.h>
-#include <map>
 #include "reader.h"
 #include "../common/memref.h"
 #include "../common/utils.h"
@@ -117,6 +116,7 @@ reader_t::operator++()
         case TRACE_TYPE_INSTR_DIRECT_CALL:
         case TRACE_TYPE_INSTR_INDIRECT_CALL:
         case TRACE_TYPE_INSTR_RETURN:
+        case TRACE_TYPE_INSTR_SYSENTER:
         case TRACE_TYPE_INSTR_NO_FETCH:
             have_memref = true;
             assert(cur_tid != 0 && cur_pid != 0);
@@ -141,6 +141,7 @@ reader_t::operator++()
             have_memref = true;
             // The trace stream always has the instr fetch first, which we
             // use to compute the starting PC for the subsequent instructions.
+            assert(type_is_instr(cur_ref.instr.type));
             cur_ref.instr.size = input_entry->length[bundle_idx++];
             cur_pc = next_pc;
             cur_ref.instr.addr = cur_pc;
diff --git a/clients/drcachesim/tests/drcachesim-invariants.templatex b/clients/drcachesim/tests/drcachesim-invariants.templatex
new file mode 100644
index 00000000000..154e6da4980
--- /dev/null
+++ b/clients/drcachesim/tests/drcachesim-invariants.templatex
@@ -0,0 +1,2 @@
+.*
+Trace invariant checks passed
diff --git a/clients/drcachesim/tests/trace_invariants.cpp b/clients/drcachesim/tests/trace_invariants.cpp
index 209cc6b1582..3f35fd64a20 100644
--- a/clients/drcachesim/tests/trace_invariants.cpp
+++ b/clients/drcachesim/tests/trace_invariants.cpp
@@ -35,9 +35,11 @@
 #include <iostream>
 #include <string.h>
 
-trace_invariants_t::trace_invariants_t()
+trace_invariants_t::trace_invariants_t(bool offline, unsigned int verbose) :
+    knob_offline(offline), knob_verbose(verbose)
 {
     memset(&prev_instr, 0, sizeof(prev_instr));
+    memset(&prev_marker, 0, sizeof(prev_marker));
 }
 
 trace_invariants_t::~trace_invariants_t()
@@ -47,16 +49,53 @@ trace_invariants_t::~trace_invariants_t()
 bool
 trace_invariants_t::process_memref(const memref_t &memref)
 {
+    if (memref.exit.type == TRACE_TYPE_THREAD_EXIT)
+        thread_exited[memref.exit.tid] = true;
     if (type_is_instr(memref.instr.type) ||
         memref.instr.type == TRACE_TYPE_PREFETCH_INSTR ||
         memref.instr.type == TRACE_TYPE_INSTR_NO_FETCH) {
+        if (knob_verbose >= 3) {
+            std::cerr << "::" << memref.data.pid << ":" << memref.data.tid << ":: " <<
+                " @" << (void *)memref.instr.addr <<
+                ((memref.instr.type == TRACE_TYPE_INSTR_NO_FETCH) ? " non-fetched" : "")
+                << " instr x" << memref.instr.size << "\n";
+        }
         // Invariant: offline traces guarantee that a branch target must immediately
         // follow the branch w/ no intervening trace switch.
-        if (type_is_instr_branch(prev_instr.instr.type)) {
-            assert(prev_instr.instr.tid == memref.instr.tid);
+        if (knob_offline && type_is_instr_branch(prev_instr.instr.type)) {
+            assert(prev_instr.instr.tid == memref.instr.tid ||
+                   // For limited-window traces a thread might exit after a branch.
+                   thread_exited[prev_instr.instr.tid]);
+        }
+        // Invariant: non-explicit control flow (i.e., kernel-mediated) is indicated
+        // by markers.
+        if (prev_instr.instr.addr != 0/*first*/ &&
+            prev_instr.instr.tid == memref.instr.tid &&
+            !type_is_instr_branch(prev_instr.instr.type)) {
+            assert(// Regular fall-through.
+                   (prev_instr.instr.addr + prev_instr.instr.size == memref.instr.addr) ||
+                   // String loop.
+                   (prev_instr.instr.addr == memref.instr.addr &&
+                    memref.instr.type == TRACE_TYPE_INSTR_NO_FETCH) ||
+                   // Kernel-mediated.
+                   (prev_marker.instr.tid == memref.instr.tid &&
+                    (prev_marker.marker.marker_type == TRACE_MARKER_TYPE_KERNEL_EVENT ||
+                     prev_marker.marker.marker_type == TRACE_MARKER_TYPE_KERNEL_XFER)) ||
+                   prev_instr.instr.type == TRACE_TYPE_INSTR_SYSENTER);
         }
         prev_instr = memref;
     }
+    if (memref.marker.type == TRACE_TYPE_MARKER) {
+        if (knob_verbose >= 3) {
+            std::cerr << "::" << memref.data.pid << ":" << memref.data.tid << ":: " <<
+                "marker type " << memref.marker.marker_type <<
+                " value " << memref.marker.marker_value << "\n";
+        }
+        prev_marker = memref;
+        // Clear prev_instr to avoid a branch-gap failure above for things like
+        // wow64 call* NtContinue syscall.
+        memset(&prev_instr, 0, sizeof(prev_instr));
+    }
     return true;
 }
 
diff --git a/clients/drcachesim/tests/trace_invariants.h b/clients/drcachesim/tests/trace_invariants.h
index c38ac5ecec7..74059e4db74 100644
--- a/clients/drcachesim/tests/trace_invariants.h
+++ b/clients/drcachesim/tests/trace_invariants.h
@@ -38,17 +38,22 @@
 
 #include "../analysis_tool.h"
 #include "../common/memref.h"
+#include <unordered_map>
 
 class trace_invariants_t : public analysis_tool_t
 {
  public:
-    trace_invariants_t();
+    trace_invariants_t(bool offline = true, unsigned int verbose = 0);
     virtual ~trace_invariants_t();
     virtual bool process_memref(const memref_t &memref);
     virtual bool print_results();
 
  protected:
+    bool knob_offline;
+    unsigned int knob_verbose;
     memref_t prev_instr;
+    memref_t prev_marker;
+    std::unordered_map<memref_tid_t,bool> thread_exited;
 };
 
 #endif /* _TRACE_INVARIANTS_H_ */
diff --git a/clients/drcachesim/tools/histogram_launcher.cpp b/clients/drcachesim/tools/histogram_launcher.cpp
index 8dd386f6bd6..61a28747a34 100644
--- a/clients/drcachesim/tools/histogram_launcher.cpp
+++ b/clients/drcachesim/tools/histogram_launcher.cpp
@@ -101,7 +101,7 @@ _tmain(int argc, const TCHAR *targv[])
                               op_verbose.get_value());
     std::vector<analysis_tool_t*> tools;
     tools.push_back(tool1);
-    trace_invariants_t tool2;
+    trace_invariants_t tool2(true/*offline*/, op_verbose.get_value());
     if (op_test_mode.get_value()) {
         // We use this launcher to run tests as well:
         tools.push_back(&tool2);
diff --git a/clients/drcachesim/tracer/instru.cpp b/clients/drcachesim/tracer/instru.cpp
index bb4901cdf98..616096c1059 100644
--- a/clients/drcachesim/tracer/instru.cpp
+++ b/clients/drcachesim/tracer/instru.cpp
@@ -54,6 +54,10 @@ instru_t::instr_to_instr_type(instr_t *instr, bool repstr_expanded)
         return TRACE_TYPE_INSTR_INDIRECT_JUMP;
     if (instr_is_cbr(instr))
         return TRACE_TYPE_INSTR_CONDITIONAL_JUMP;
+#ifdef X86
+    if (instr_get_opcode(instr) == OP_sysenter)
+        return TRACE_TYPE_INSTR_SYSENTER;
+#endif
     // i#2051: to satisfy both cache and core simulators we mark subsequent iters
     // of string loops as TRACE_TYPE_INSTR_NO_FETCH, converted from this
     // TRACE_TYPE_INSTR_MAYBE_FETCH by reader_t (since online traces would need
diff --git a/clients/drcachesim/tracer/instru_offline.cpp b/clients/drcachesim/tracer/instru_offline.cpp
index d47222114e5..e98e9373f16 100644
--- a/clients/drcachesim/tracer/instru_offline.cpp
+++ b/clients/drcachesim/tracer/instru_offline.cpp
@@ -387,8 +387,10 @@ offline_instru_t::instrument_instr(void *drcontext, void *tag, void **bb_field,
         if ((ptr_uint_t)*bb_field > MAX_INSTR_COUNT)
             return adjust;
         pc = dr_fragment_app_pc(tag);
-    } else
+    } else {
+        // XXX: For repstr do we want tag insted of skipping rep prefix?
         pc = instr_get_app_pc(app);
+    }
     adjust += insert_save_pc(drcontext, ilist, where, reg_ptr, reg_tmp, adjust,
                            pc, memref_needs_full_info ? 1 : (uint)(ptr_uint_t)*bb_field);
     if (!memref_needs_full_info)
diff --git a/clients/drcachesim/tracer/instru_online.cpp b/clients/drcachesim/tracer/instru_online.cpp
index d71ef8d4d8a..9d7b5c675fd 100644
--- a/clients/drcachesim/tracer/instru_online.cpp
+++ b/clients/drcachesim/tracer/instru_online.cpp
@@ -283,6 +283,7 @@ online_instru_t::instrument_memref(void *drcontext, instrlist_t *ilist, instr_t
         insert_save_type_and_size(drcontext, ilist, where, reg_ptr, reg_tmp,
                                   TRACE_TYPE_INSTR, 0, adjust);
         insert_save_pc(drcontext, ilist, where, reg_ptr, reg_tmp,
+                       // XXX: For repstr do we want tag insted of skipping rep prefix?
                        instr_get_app_pc(app), adjust);
         adjust += sizeof(trace_entry_t);
     }
@@ -310,11 +311,18 @@ online_instru_t::instrument_instr(void *drcontext, void *tag, void **bb_field,
                                   instr_t *app)
 {
     bool repstr_expanded = *bb_field != 0; // Avoid cl warning C4800.
+    app_pc pc = repstr_expanded ? dr_fragment_app_pc(tag) : instr_get_app_pc(app);
+    // To handle zero-iter repstr loops this routine is called at the top of the bb
+    // where "app" is jecxz so we have to hardcode the rep str type and get length
+    // from the tag.
+    ushort type = repstr_expanded ? TRACE_TYPE_INSTR_MAYBE_FETCH :
+        instr_to_instr_type(app, repstr_expanded);
+    ushort size = repstr_expanded ?
+        (ushort)decode_sizeof(drcontext, pc, NULL _IF_X86_64(NULL)) :
+        (ushort)instr_length(drcontext, app);
     insert_save_type_and_size(drcontext, ilist, where, reg_ptr, reg_tmp,
-                              instr_to_instr_type(app, repstr_expanded),
-                              (ushort)instr_length(drcontext, app), adjust);
-    insert_save_pc(drcontext, ilist, where, reg_ptr, reg_tmp,
-                   instr_get_app_pc(app), adjust);
+                              type, size, adjust);
+    insert_save_pc(drcontext, ilist, where, reg_ptr, reg_tmp, pc, adjust);
     return (adjust + sizeof(trace_entry_t));
 }
 
diff --git a/clients/drcachesim/tracer/raw2trace.cpp b/clients/drcachesim/tracer/raw2trace.cpp
index 0b41e4673bd..9e198eb66c3 100644
--- a/clients/drcachesim/tracer/raw2trace.cpp
+++ b/clients/drcachesim/tracer/raw2trace.cpp
@@ -277,28 +277,37 @@ raw2trace_t::unmap_modules(void)
  * Disassembly to fill in instr and memref entries
  */
 
+#define FAULT_INTERRUPTED_BB "INTERRUPTED"
+
+// Returns FAULT_INTERRUPTED_BB if a fault occurred on this memref.
+// Any other non-empty string is a fatal error.
 std::string
 raw2trace_t::append_memref(INOUT trace_entry_t **buf_in, uint tidx, instr_t *instr,
                            opnd_t ref, bool write)
 {
     trace_entry_t *buf = *buf_in;
-    offline_entry_t in_entry;
-    if (!thread_files[tidx]->read((char*)&in_entry, sizeof(in_entry)))
+    // To avoid having to backtrack later, we read 2 ahead to see whether this memref
+    // faulted.  There's a footer so this should always succeed.
+    offline_entry_t in_entry[2];
+    if (!thread_files[tidx]->read((char*)in_entry, sizeof(in_entry)))
         return "Trace ends mid-block";
-    if (in_entry.addr.type != OFFLINE_TYPE_MEMREF &&
-        in_entry.addr.type != OFFLINE_TYPE_MEMREF_HIGH) {
+    if (in_entry[0].addr.type != OFFLINE_TYPE_MEMREF &&
+        in_entry[0].addr.type != OFFLINE_TYPE_MEMREF_HIGH) {
         // This happens when there are predicated memrefs in the bb, or for a
         // zero-iter rep string loop.  For predicated memrefs,
         // they could be earlier, so "instr" may not itself be predicated.
         // XXX i#2015: if there are multiple predicated memrefs, our instr vs
         // data stream may not be in the correct order here.
         VPRINT(4, "Missing memref from predication or 0-iter repstr (next type is 0x"
-               ZHEX64_FORMAT_STRING ")\n", in_entry.combined_value);
-        // Put back the entry.
+               ZHEX64_FORMAT_STRING ")\n", in_entry[0].combined_value);
+        // Put back both entries.
         thread_files[tidx]->seekg(-(std::streamoff)sizeof(in_entry),
                                   thread_files[tidx]->cur);
         return "";
     }
+    // Put back the 2nd entry.
+    thread_files[tidx]->seekg(-(std::streamoff)sizeof(in_entry[0]),
+                              thread_files[tidx]->cur);
     if (instr_is_prefetch(instr)) {
         buf->type = instru_t::instr_to_prefetch_type(instr);
         buf->size = 1;
@@ -313,9 +322,16 @@ raw2trace_t::append_memref(INOUT trace_entry_t **buf_in, uint tidx, instr_t *ins
         buf->size = (ushort) opnd_size_in_bytes(opnd_get_size(ref));
     }
     // We take the full value, to handle low or high.
-    buf->addr = (addr_t) in_entry.combined_value;
+    buf->addr = (addr_t) in_entry[0].combined_value;
     VPRINT(4, "Appended memref to " PFX "\n", (ptr_uint_t)buf->addr);
     *buf_in = ++buf;
+    if (in_entry[1].extended.type == OFFLINE_TYPE_EXTENDED &&
+        in_entry[1].extended.ext == OFFLINE_EXT_TYPE_MARKER &&
+        in_entry[1].extended.valueB == TRACE_MARKER_TYPE_KERNEL_EVENT) {
+        // A signal/exception interrupted the bb after the memref.
+        VPRINT(4, "Signal/exception interrupted the bb\n");
+        return FAULT_INTERRUPTED_BB;
+    }
     return "";
 }
 
@@ -341,6 +357,7 @@ raw2trace_t::append_bb_entries(uint tidx, offline_entry_t *in_entry, OUT bool *h
                (ptr_uint_t)in_entry->pc.modoffs, modvec[in_entry->pc.modidx].path);
     }
     bool skip_icache = false;
+    bool truncated = false; // Whether a fault ended the bb early.
     if (instr_count == 0) {
         // L0 filtering adds a PC entry with a count of 0 prior to each memref.
         skip_icache = true;
@@ -350,7 +367,7 @@ raw2trace_t::append_bb_entries(uint tidx, offline_entry_t *in_entry, OUT bool *h
             instrs_are_separate = true;
     }
     CHECK(!instrs_are_separate || instr_count == 1, "cannot mix 0-count and >1-count");
-    for (uint i = 0; i < instr_count; ++i) {
+    for (uint i = 0; !truncated && i < instr_count; ++i) {
         trace_entry_t *buf = buf_start;
         app_pc orig_pc = decode_pc - modvec[in_entry->pc.modidx].map_base +
             modvec[in_entry->pc.modidx].orig_base;
@@ -403,19 +420,25 @@ raw2trace_t::append_bb_entries(uint tidx, offline_entry_t *in_entry, OUT bool *h
         if ((!instrs_are_separate || skip_icache) &&
             // Rule out OP_lea.
             (instr_reads_memory(instr) || instr_writes_memory(instr))) {
-            for (int i = 0; i < instr_num_srcs(instr); i++) {
-                if (opnd_is_memory_reference(instr_get_src(instr, i))) {
+            for (int j = 0; j < instr_num_srcs(instr); j++) {
+                if (opnd_is_memory_reference(instr_get_src(instr, j))) {
                     std::string error = append_memref(&buf, tidx, instr,
-                                                      instr_get_src(instr, i), false);
-                    if (!error.empty())
+                                                      instr_get_src(instr, j), false);
+                    if (error == FAULT_INTERRUPTED_BB) {
+                        truncated = true;
+                        break;
+                    } else if (!error.empty())
                         return error;
                 }
             }
-            for (int i = 0; i < instr_num_dsts(instr); i++) {
-                if (opnd_is_memory_reference(instr_get_dst(instr, i))) {
+            for (int j = 0; !truncated && j < instr_num_dsts(instr); j++) {
+                if (opnd_is_memory_reference(instr_get_dst(instr, j))) {
                     std::string error = append_memref(&buf, tidx, instr,
-                                                      instr_get_dst(instr, i), true);
-                    if (!error.empty())
+                                                      instr_get_dst(instr, j), true);
+                    if (error == FAULT_INTERRUPTED_BB) {
+                        truncated = true;
+                        break;
+                    } else if (!error.empty())
                         return error;
                 }
             }
diff --git a/clients/drcachesim/tracer/tracer.cpp b/clients/drcachesim/tracer/tracer.cpp
index e08e8b6af95..0e4c64324e5 100644
--- a/clients/drcachesim/tracer/tracer.cpp
+++ b/clients/drcachesim/tracer/tracer.cpp
@@ -117,6 +117,8 @@ typedef struct {
 } per_thread_t;
 
 #define MAX_NUM_DELAY_INSTRS 32
+// Really sizeof(trace_entry_t.length)
+#define MAX_NUM_DELAY_ENTRIES (MAX_NUM_DELAY_INSTRS / sizeof(addr_t))
 /* per bb user data during instrumentation */
 typedef struct {
     app_pc last_app_pc;
@@ -316,6 +318,13 @@ write_trace_data(void *drcontext, byte *towrite_start, byte *towrite_end)
         return atomic_pipe_write(drcontext, towrite_start, towrite_end);
 }
 
+static bool
+is_ok_to_split_before(trace_type_t type)
+{
+    return type_is_instr(type) || type == TRACE_TYPE_INSTR_MAYBE_FETCH ||
+        type == TRACE_TYPE_MARKER || type == TRACE_TYPE_THREAD_EXIT;
+}
+
 static void
 memtrace(void *drcontext, bool skip_size_cap)
 {
@@ -382,12 +391,17 @@ memtrace(void *drcontext, bool skip_size_cap)
                 // XXX i#2638: if we want to support branch target analysis in online
                 // traces we'll need to not split after a branch: either split before
                 // it or one instr after.
-                trace_type_t type = instru->get_entry_type(mem_ref);
-                if (type_is_instr(type) || type == TRACE_TYPE_INSTR_MAYBE_FETCH) {
-                    if ((mem_ref - pipe_start) > ipc_pipe.get_atomic_write_size())
-                        pipe_start = atomic_pipe_write(drcontext, pipe_start, pipe_end);
-                    // Advance pipe_end pointer
+                if (is_ok_to_split_before(instru->get_entry_type(mem_ref))) {
                     pipe_end = mem_ref;
+                    // We check the end of this entry + the max # of delay entries to
+                    // avoid splitting an instr from its subsequent bundle entry.
+                    // An alternative is to have the reader use per-thread state.
+                    if ((mem_ref + (1+MAX_NUM_DELAY_ENTRIES)*instru->sizeof_entry()
+                         - pipe_start) > ipc_pipe.get_atomic_write_size()) {
+                        DR_ASSERT(is_ok_to_split_before(instru->get_entry_type
+                                                        (pipe_start+header_size)));
+                        pipe_start = atomic_pipe_write(drcontext, pipe_start, pipe_end);
+                    }
                 }
             }
         }
@@ -400,10 +414,16 @@ memtrace(void *drcontext, bool skip_size_cap)
             // XXX i#2638: if we want to support branch target analysis in online
             // traces we'll need to not split after a branch by carrying a write-final
             // branch forward to the next buffer.
-            if ((buf_ptr - pipe_start) > ipc_pipe.get_atomic_write_size())
+            if ((buf_ptr - pipe_start) > ipc_pipe.get_atomic_write_size()) {
+                DR_ASSERT(is_ok_to_split_before(instru->get_entry_type
+                                                (pipe_start+header_size)));
                 pipe_start = atomic_pipe_write(drcontext, pipe_start, pipe_end);
-            if ((buf_ptr - pipe_start) > (ssize_t)buf_hdr_slots_size)
+            }
+            if ((buf_ptr - pipe_start) > (ssize_t)buf_hdr_slots_size) {
+                DR_ASSERT(is_ok_to_split_before(instru->get_entry_type
+                                                (pipe_start+header_size)));
                 atomic_pipe_write(drcontext, pipe_start, buf_ptr);
+            }
         }
         data->num_refs += num_refs;
     }
@@ -926,7 +946,8 @@ event_app_instruction(void *drcontext, void *tag, instrlist_t *bb,
     is_memref = instr_reads_memory(instr) || instr_writes_memory(instr);
     // See comment in instrument_delay_instrs: we only want the original string
     // ifetch and not any of the expansion instrs.  We instrument the first
-    // one to handle a zero-iter loop.
+    // one to handle a zero-iter loop.  For offline, we just record the pc; for
+    // online we have to ignore "instr" here in instru_online::instrument_instr().
     if (!ud->repstr || drmgr_is_first_instr(drcontext, instr)) {
         adjust = instrument_instr(drcontext, tag, ud, bb,
                                   instr, reg_ptr, reg_tmp, adjust, instr);
@@ -1045,8 +1066,13 @@ event_kernel_xfer(void *drcontext, const dr_kernel_xfer_info_t *info)
     per_thread_t *data = (per_thread_t *) drmgr_get_tls_field(drcontext, tls_idx);
     trace_marker_type_t marker_type;
     switch (info->type) {
-    case DR_XFER_SIGNAL_DELIVERY:
     case DR_XFER_APC_DISPATCHER:
+        /* Do not bother with a marker for the thread init routine. */
+        if (data->num_refs == 0 &&
+            BUF_PTR(data->seg_base) == data->buf_base + data->init_header_size)
+            return;
+        ANNOTATE_FALLTHROUGH;
+    case DR_XFER_SIGNAL_DELIVERY:
     case DR_XFER_EXCEPTION_DISPATCHER:
     case DR_XFER_RAISE_DISPATCHER:
     case DR_XFER_CALLBACK_DISPATCHER:
@@ -1292,6 +1318,7 @@ init_thread_in_process(void *drcontext)
 
         /* put buf_base to TLS plus header slots as starting buf_ptr */
         BUF_PTR(data->seg_base) = data->buf_base + buf_hdr_slots_size;
+        data->init_header_size = buf_hdr_slots_size;
     }
 
     if (op_L0_filter.get_value()) {
diff --git a/suite/tests/CMakeLists.txt b/suite/tests/CMakeLists.txt
index fb1abaeca15..1373ba2ce08 100644
--- a/suite/tests/CMakeLists.txt
+++ b/suite/tests/CMakeLists.txt
@@ -2585,6 +2585,27 @@ if (CLIENT_INTERFACE)
         set(tool.reuse_time.offline_basedir "${PROJECT_SOURCE_DIR}/clients/drcachesim/tests")
       endif ()
 
+      # We run an app w/ kernel xfers to test trace_invariants online.
+      # Offline invariants are tested in the tool.histogram.offline test below.
+      if (UNIX)
+        set(kernel_xfer_app pthreads.ptsig)
+      else ()
+        if (CLIENT_INTERFACE)
+          set(kernel_xfer_app client.winxfer) # We want threads and xfers.
+        else ()
+          set(kernel_xfer_app win32.winapc)
+        endif ()
+      endif()
+      if (DEBUG) # for -test_mode
+        torunonly_ci(tool.drcachesim.invariants ${kernel_xfer_app} drcachesim
+          "drcachesim-invariants.c" # for templatex basename
+          "-test_mode -ipc_name ${IPC_PREFIX}drtestpipe8" "" "")
+        set(tool.drcachesim.invariants_toolname "drcachesim")
+        set(tool.drcachesim.invariants_basedir
+          "${PROJECT_SOURCE_DIR}/clients/drcachesim/tests")
+        set(tool.drcachesim.invariants_rawtemp ON) # no preprocessor
+      endif ()
+
       # Test offline traces.
       # XXX: we could exclude the pipe files and build the offline trace
       # support by itself for Android.
@@ -2690,13 +2711,20 @@ if (CLIENT_INTERFACE)
       # Test the standalone histogram tool.
       # ${ci_shared_app} is already used for an offline test, and we're deleting a
       # dir with that name, so we run other apps to avoid having to serialize.
-      if (UNIX)
-        set(histo_app pthreads.pthreads) # We want some threads for trace_invariants.
+      # We also want threads and signals for trace_invariants.
+      set(histo_app ${kernel_xfer_app})
+      if (WIN32)
+        # winxfer produces a 500M+ trace and test taking >3mins so we narrow
+        # the window.  2M makes a 50M trace and still hits 4 or 5 of the
+        # 13 markers on my machine but is still a little slow so we cut in half
+        # for 2+ markers.
+        set(histo_ops "-trace_after_instrs 5K -exit_after_tracing 1M")
+        set(tool.histogram.offline_timeout 180)
       else ()
-        set(histo_app common.${control_flags})
-      endif()
+        set(histo_ops "")
+      endif ()
       torunonly_ci(tool.histogram.offline ${histo_app} drcachesim
-        "histogram-offline.c" "-offline" "" "")
+        "histogram-offline.c" "-offline ${histo_ops}" "" "")
       set(tool.histogram.offline_toolname "drcachesim")
       set(tool.histogram.offline_basedir
         "${PROJECT_SOURCE_DIR}/clients/drcachesim/tests")
@@ -3066,7 +3094,7 @@ if (UNIX)
     "-enable_reset -reset_at_fragment_count 100" "")
   tobuild(pthreads.pthreads pthreads/pthreads.c)
   tobuild(pthreads.pthreads_exit pthreads/pthreads_exit.c)
-  tobuild(pthreads.ptsig_FLAKY pthreads/ptsig.c)
+  tobuild(pthreads.ptsig pthreads/ptsig.c)
   if (NOT ANDROID) # FIXME i#1874: failing on Android
     # XXX i#951: pthreads_fork reports leaks on occasion so we mark it FLAKY
     tobuild(pthreads.pthreads_fork_FLAKY pthreads/pthreads_fork.c)
diff --git a/suite/tests/pthreads/ptsig.c b/suite/tests/pthreads/ptsig.c
index 2d23de19e5c..11cc712e09a 100644
--- a/suite/tests/pthreads/ptsig.c
+++ b/suite/tests/pthreads/ptsig.c
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2011-2015 Google, Inc.  All rights reserved.
+ * Copyright (c) 2011-2017 Google, Inc.  All rights reserved.
  * Copyright (c) 2003-2008 VMware, Inc.  All rights reserved.
  * **********************************************************/
 
@@ -43,10 +43,12 @@
 #include <ucontext.h>
 #include <unistd.h>
 #include <assert.h>
+#include <setjmp.h>
 
 volatile double pi = 0.0;  /* Approximation to pi (shared) */
 pthread_mutex_t pi_lock;   /* Lock for above */
 volatile double intervals; /* How many intervals? */
+static SIGJMP_BUF mark;
 
 static void
 signal_handler(int sig, siginfo_t *siginfo, ucontext_t *ucxt)
@@ -65,6 +67,12 @@ signal_handler(int sig, siginfo_t *siginfo, ucontext_t *ucxt)
 #endif
         break;
     }
+    case SIGSEGV:
+#if VERBOSE
+        print("thread %d got SIGSEGV @ "PFX"\n", getpid(), pc);
+#endif
+        SIGLONGJMP(mark, 1);
+        break;
     default:
         assert(0);
     }
@@ -80,14 +88,6 @@ process(void *arg)
 
 #if VERBOSE
     print("thread %s starting\n", id);
-#endif
-    if (id[0] == '1') {
-        intercept_signal(SIGUSR1, (handler_3_t) SIG_IGN, false);
-#if VERBOSE
-        print("thread %d ignoring SIGUSR1\n", getpid());
-#endif
-    }
-#if VERBOSE
     print("thread %d sending SIGUSR1\n", getpid());
 #endif
     kill(getpid(), SIGUSR1);
@@ -137,6 +137,7 @@ main(int argc, char **argv)
     pthread_mutex_init(&pi_lock, NULL);
 
     intercept_signal(SIGUSR1, signal_handler, false);
+    intercept_signal(SIGSEGV, signal_handler, false);
 
     /* Make the two threads */
     if (pthread_create(&thread0, NULL, process, (void *)"0") ||
@@ -156,6 +157,12 @@ main(int argc, char **argv)
     print("thread %d sending SIGUSR1\n", getpid());
 #endif
     kill(getpid(), SIGUSR1);
+#if VERBOSE
+    print("thread %d hitting SIGSEGV\n", getpid());
+#endif
+    if (SIGSETJMP(mark) == 0) {
+        *(int*)42 = 0;
+    }
 
     /* Print the result */
     print("Estimation of pi is %16.15f\n", pi);
