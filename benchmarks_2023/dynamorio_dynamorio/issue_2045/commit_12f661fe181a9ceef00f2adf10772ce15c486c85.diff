diff --git a/clients/drcachesim/analyzer.cpp b/clients/drcachesim/analyzer.cpp
index 8e716b7c35d..a6b23dcc179 100644
--- a/clients/drcachesim/analyzer.cpp
+++ b/clients/drcachesim/analyzer.cpp
@@ -100,6 +100,12 @@ analyzer_t::operator!()
     return !success;
 }
 
+std::string
+analyzer_t::get_error_string()
+{
+    return error_string;
+}
+
 bool
 analyzer_t::start_reading()
 {
diff --git a/clients/drcachesim/analyzer.h b/clients/drcachesim/analyzer.h
index 8c94ebece67..01ff60b68ec 100644
--- a/clients/drcachesim/analyzer.h
+++ b/clients/drcachesim/analyzer.h
@@ -47,10 +47,12 @@ class analyzer_t
 {
  public:
     // Usage: errors encountered during a constructor will set a flag that should
-    // be queried via operator!.
+    // be queried via operator!.  If operator! returns true, get_error_string()
+    // can be used to try to obtain more information.
     analyzer_t();
     virtual ~analyzer_t();
     virtual bool operator!();
+    virtual std::string get_error_string();
 
     // We have two usage models: one where there are multiple tools and the
     // trace iteration is performed by analyzer_t, and another where a single
@@ -83,6 +85,7 @@ class analyzer_t
     bool start_reading();
 
     bool success;
+    std::string error_string;
     reader_t *trace_iter;
     reader_t *trace_end;
     int num_tools;
diff --git a/clients/drcachesim/analyzer_multi.cpp b/clients/drcachesim/analyzer_multi.cpp
index d3c79d74957..851d7da6140 100644
--- a/clients/drcachesim/analyzer_multi.cpp
+++ b/clients/drcachesim/analyzer_multi.cpp
@@ -79,6 +79,15 @@ analyzer_multi_t::analyzer_multi_t()
     } else if (op_infile.get_value().empty()) {
         trace_iter = new ipc_reader_t(op_ipc_name.get_value().c_str());
         trace_end = new ipc_reader_t();
+        if (!*trace_iter) {
+            success = false;
+#ifdef UNIX
+            // This is the most likely cause of the error.
+            // XXX: Even better would be to propagate the mkfifo errno here.
+            error_string = "try removing stale pipe file " +
+                reinterpret_cast<ipc_reader_t*>(trace_iter)->get_pipe_name();
+#endif
+        }
     } else {
 #ifdef HAS_ZLIB
         // Even for uncompressed files, zlib's gzip interface is faster than fstream.
diff --git a/clients/drcachesim/common/named_pipe.h b/clients/drcachesim/common/named_pipe.h
index e711825da76..7281004d753 100644
--- a/clients/drcachesim/common/named_pipe.h
+++ b/clients/drcachesim/common/named_pipe.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2015-2016 Google, Inc.  All rights reserved.
+ * Copyright (c) 2015-2017 Google, Inc.  All rights reserved.
  * **********************************************************/
 
 /*
@@ -66,6 +66,7 @@ class named_pipe_t
  public:
     named_pipe_t();
     bool set_name(const char *name);
+    std::string get_name() const;
     explicit named_pipe_t(const char *name);
     ~named_pipe_t();
 
diff --git a/clients/drcachesim/common/named_pipe_unix.cpp b/clients/drcachesim/common/named_pipe_unix.cpp
index 2a52803cae0..d60c1cf624c 100644
--- a/clients/drcachesim/common/named_pipe_unix.cpp
+++ b/clients/drcachesim/common/named_pipe_unix.cpp
@@ -105,6 +105,12 @@ named_pipe_t::set_name(const char *name)
     return false;
 }
 
+std::string
+named_pipe_t::get_name() const
+{
+    return pipe_name;
+}
+
 bool
 named_pipe_t::create()
 {
diff --git a/clients/drcachesim/common/named_pipe_win.cpp b/clients/drcachesim/common/named_pipe_win.cpp
index 286827e694b..2ae7453d850 100644
--- a/clients/drcachesim/common/named_pipe_win.cpp
+++ b/clients/drcachesim/common/named_pipe_win.cpp
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2015-2016 Google, Inc.  All rights reserved.
+ * Copyright (c) 2015-2017 Google, Inc.  All rights reserved.
  * **********************************************************/
 
 /*
@@ -70,6 +70,12 @@ named_pipe_t::set_name(const char *name)
     return false;
 }
 
+std::string
+named_pipe_t::get_name() const
+{
+    return pipe_name;
+}
+
 bool
 named_pipe_t::create()
 {
diff --git a/clients/drcachesim/launcher.cpp b/clients/drcachesim/launcher.cpp
index b38239bab66..6fbf50fd5d6 100644
--- a/clients/drcachesim/launcher.cpp
+++ b/clients/drcachesim/launcher.cpp
@@ -44,6 +44,8 @@
 # include <sys/types.h>
 # include <sys/wait.h>
 # include <unistd.h>  /* for fork */
+# include <cstdlib>
+# include <signal.h>
 #endif
 
 #include <assert.h>
@@ -71,6 +73,28 @@
     } \
 } while (0)
 
+static analyzer_t *analyzer;
+#ifdef UNIX
+static pid_t child;
+#endif
+
+#ifdef UNIX
+static void
+signal_handler(int sig, siginfo_t *info, void *cxt)
+{
+# define INTERRUPT_MSG "Interrupted: exiting.\n"
+    ssize_t res = write(STDERR_FILENO, INTERRUPT_MSG, sizeof(INTERRUPT_MSG));
+    (void)res; // Work around compiler warnings.
+    // Terminate child in case shell didn't already send this there.
+    // It's up to the child to terminate grandchildren not already notified.
+    kill(child, SIGINT);
+    // Destroy pipe file if it's open.
+    if (analyzer != NULL)
+        delete analyzer;
+    exit(1);
+}
+#endif
+
 #define CLIENT_ID 0
 
 static bool
@@ -158,12 +182,24 @@ _tmain(int argc, const TCHAR *targv[])
     char buf[MAXIMUM_PATH];
     drfront_status_t sc;
     bool is64, is32;
-    analyzer_t *analyzer = NULL;
     std::string tracer_ops;
+    bool have_trace_file;
+
 #ifdef UNIX
-    pid_t child = 0;
+    // We want to clean up the pipe file on control-C.
+    struct sigaction act;
+    act.sa_sigaction = signal_handler;
+    sigfillset(&act.sa_mask); // Block all within handler.
+    act.sa_flags = SA_SIGINFO;
+    int rc = sigaction(SIGINT, &act, NULL);
+    if (rc != 0)
+        NOTIFY(0, "WARNING", "Failed to set up interrupt handler\n");
+#else
+    // We do not bother with SetConsoleCtrlHandler for two reasons:
+    // one, there's no problem to solve like the UNIX fifo file left
+    // behind.  Two, the ^c handler in a new thread is more work to
+    // deal with as it races w/ the main thread.
 #endif
-    bool have_trace_file;
 
 #if defined(WINDOWS) && !defined(_UNICODE)
 # error _UNICODE must be defined
@@ -235,7 +271,9 @@ _tmain(int argc, const TCHAR *targv[])
     } else {
         analyzer = new analyzer_multi_t;
         if (!*analyzer) {
-            FATAL_ERROR("failed to initialize analyzer");
+            std::string error_string = analyzer->get_error_string();
+            FATAL_ERROR("failed to initialize analyzer%s%s",
+                        error_string.empty() ? "" : ": ", error_string.c_str());
         }
     }
 
@@ -248,6 +286,10 @@ _tmain(int argc, const TCHAR *targv[])
         NOTIFY(1, "INFO", "DynamoRIO configuration directory is %s", buf);
 
 #ifdef UNIX
+        // We could try to arrange for the child to auto-exit if the parent dies via
+        // prctl(PR_SET_PDEATHSIG, SIGTERM) on Linux and kqueue on Mac, plus
+        // checking for ppid changes for up front races, but that won't propagate
+        // to grandchildren.
         if (op_offline.get_value())
             child = 0;
         else
diff --git a/clients/drcachesim/reader/ipc_reader.cpp b/clients/drcachesim/reader/ipc_reader.cpp
index b3f50f6e096..8fb70118be1 100644
--- a/clients/drcachesim/reader/ipc_reader.cpp
+++ b/clients/drcachesim/reader/ipc_reader.cpp
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2015-2016 Google, Inc.  All rights reserved.
+ * Copyright (c) 2015-2017 Google, Inc.  All rights reserved.
  * **********************************************************/
 
 /*
@@ -40,7 +40,7 @@
 # include <iostream>
 #endif
 
-ipc_reader_t::ipc_reader_t()
+ipc_reader_t::ipc_reader_t() : creation_success(false)
 {
     /* Empty. */
 }
@@ -48,14 +48,28 @@ ipc_reader_t::ipc_reader_t()
 ipc_reader_t::ipc_reader_t(const char *ipc_name) :
     pipe(ipc_name)
 {
-    /* Empty. */
+    // We create the pipe here so the user can set up a pipe writer
+    // *before* calling the blocking analyzer_t::run().
+    creation_success = pipe.create();
+}
+
+bool
+ipc_reader_t::operator!()
+{
+    return !creation_success;
+}
+
+std::string
+ipc_reader_t::get_pipe_name() const
+{
+    return pipe.get_name();
 }
 
 bool
 ipc_reader_t::init()
 {
     at_eof = false;
-    if (!pipe.create() ||
+    if (!creation_success ||
         !pipe.open_for_read())
         return false;
     pipe.maximize_buffer();
diff --git a/clients/drcachesim/reader/ipc_reader.h b/clients/drcachesim/reader/ipc_reader.h
index f468ea25c9f..c8332774449 100644
--- a/clients/drcachesim/reader/ipc_reader.h
+++ b/clients/drcachesim/reader/ipc_reader.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2015-2016 Google, Inc.  All rights reserved.
+ * Copyright (c) 2015-2017 Google, Inc.  All rights reserved.
  * **********************************************************/
 
 /*
@@ -49,14 +49,17 @@ class ipc_reader_t : public reader_t
     ipc_reader_t();
     explicit ipc_reader_t(const char *ipc_name);
     virtual ~ipc_reader_t();
+    virtual bool operator!();
     // This potentially blocks.
     virtual bool init();
+    std::string get_pipe_name() const;
 
  protected:
     virtual trace_entry_t * read_next_entry();
 
  private:
     named_pipe_t pipe;
+    bool creation_success;
 
     // For efficiency we want to read large chunks at a time.
     // The atomic write size for a pipe on Linux is 4096 bytes but
diff --git a/clients/drcachesim/reader/reader.h b/clients/drcachesim/reader/reader.h
index c9a9d237116..5effb531665 100644
--- a/clients/drcachesim/reader/reader.h
+++ b/clients/drcachesim/reader/reader.h
@@ -69,6 +69,11 @@ class reader_t : public std::iterator<std::input_iterator_tag, memref_t>
 
     virtual reader_t& operator++();
 
+    // Supplied for subclasses that may fail in their constructors.
+    virtual bool operator!() {
+        return false;
+    }
+
     // We do not support the post-increment operator for two reasons:
     // 1) It prevents pure virtual functions here, as it cannot
     //    return an abstract type;
diff --git a/clients/drcachesim/tracer/tracer.cpp b/clients/drcachesim/tracer/tracer.cpp
index 416dbf70daa..a055973ff12 100644
--- a/clients/drcachesim/tracer/tracer.cpp
+++ b/clients/drcachesim/tracer/tracer.cpp
@@ -73,8 +73,12 @@ DR_EXPORT void drmemtrace_client_main(client_id_t id, int argc, const char *argv
         dr_fprintf(STDERR, __VA_ARGS__);   \
 } while (0)
 
+// A clean exit via dr_exit_process() is not supported from init code, but
+// we do want to at least close the pipe file.
 #define FATAL(...) do {                \
     dr_fprintf(STDERR, __VA_ARGS__);   \
+    if (!op_offline.get_value())       \
+        ipc_pipe.close();              \
     dr_abort();                        \
 } while (0)
 
@@ -283,8 +287,9 @@ atomic_pipe_write(void *drcontext, byte *pipe_start, byte *pipe_end)
     ssize_t towrite = pipe_end - pipe_start;
     DR_ASSERT(towrite <= ipc_pipe.get_atomic_write_size() &&
               towrite > (ssize_t)buf_hdr_slots_size);
-    if (ipc_pipe.write((void *)pipe_start, towrite) < (ssize_t)towrite)
-        DR_ASSERT(false);
+    if (ipc_pipe.write((void *)pipe_start, towrite) < (ssize_t)towrite) {
+        FATAL("Fatal error: failed to write to pipe\n");
+    }
     // Re-emit thread entry header
     DR_ASSERT(pipe_end - buf_hdr_slots_size > pipe_start);
     pipe_start = pipe_end - buf_hdr_slots_size;
