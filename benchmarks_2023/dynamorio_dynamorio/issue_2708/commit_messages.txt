i#2720 cpusim tests: add -ignore_all_libs option (#2721)

Adds a new drcpusim option -ignore_all_libs and uses it on cpuid tests to
avoid failures on Travis Mac where system libraries are using recent
instructions.  These cpuid tests are mainly testing cpuid fooling, not
instruction set violations.

Fixes #2720
i#2708 trace xfers: record kernel xfer boundaries in traces

Adds the concept of "marker entries" to drcachesim's traces, which can be
used to store metadata about a location in a trace.

Uses DR's new kernel xfer event to insert trace marker entries on
kernel-mediated control transfers: signal handlers, signal returns, Windows
APCs, exceptions, callbacks, etc.

To help with testing, adds a new tool basic_counts which counts the number
of each trace entry type per thread in the trace.

Adds new online and offline tests that run the basic_counts tool it on the
common.decode-bad test binary.

Fixes #2708
i#2708 trace xfers: record kernel xfer boundaries in traces (#2722)

Adds the concept of "marker entries" to drcachesim's traces, which can be
used to store metadata about a location in a trace.

Uses DR's new kernel xfer event to insert trace marker entries on
kernel-mediated control transfers: signal handlers, signal returns, Windows
APCs, exceptions, callbacks, etc.

To help with testing, adds a new tool basic_counts which counts the number
of each trace entry type per thread in the trace.

Adds new online and offline tests that run the basic_counts tool it on the
common.decode-bad test binary.

Fixes #2708
i#2708 trace xfers: record kernel xfer boundaries in traces (#2722)

Adds the concept of "marker entries" to drcachesim's traces, which can be
used to store metadata about a location in a trace.

Uses DR's new kernel xfer event to insert trace marker entries on
kernel-mediated control transfers: signal handlers, signal returns, Windows
APCs, exceptions, callbacks, etc.

To help with testing, adds a new tool basic_counts which counts the number
of each trace entry type per thread in the trace.

Adds new online and offline tests that run the basic_counts tool it on the
common.decode-bad test binary.

Fixes #2708
i#2011 repstr ifetch: handle zero-iter loops

Adds a missing instruction fetch for zero-iteration rep string loops by
moving the instruction instrumentation to the top of a rep string bb rather
than prior to the single memory reference.

The forthcoming trace_invariants continuous PC checks for #2708 will test
this.

Fixes #2011
i#2011 repstr ifetch: handle zero-iter loops (#2742)

Adds a missing instruction fetch for zero-iteration rep string loops by
moving the instruction instrumentation to the top of a rep string bb rather
than prior to the single memory reference.

The forthcoming trace_invariants continuous PC checks for #2708 will add
an automated test for this.

Fixes #2011
i#2708 trace discontinuity: add test, fix bugs found by test

Extends the trace_invariants test from #2638 to ensure there's no
discontinuity in control flow not indicated by a branch or a kernel xfer
marker.

Adds an online trace_invariants test.

Switches the trace_invariants test to run pthreads.ptsig on UNIX, which is
marked un-FLAKY, to test both threads and signals, and on winxfer on
Windows.

Fixes several issues found by this new test:
+ Adds TRACE_TYPE_INSTR_SYSENTER to mark the PC discontinuity from
  OP_sysenter.
+ Adds proper handling of a mid-bb fault in offline trace conversion by
  looking ahead after each memref to see whether there's a marker.
+ #2011 follow-up: fixes the zero-iter code from 2772b0b which it turns out
  only worked for offline traces.  For online, the top-of-bb instr is
  jecxz, so the instr type and size were wrong.
+ Fixes pipe write splitting to avoid separating an instr from its bundle
  entries.
+ Avoids a marker for a thread init kernel xfer event on Windows.

Fixes #2708
i#2708 trace discontinuity: add test, fix bugs found by test (#2747)

Extends the trace_invariants test from #2638 to ensure there's no
discontinuity in control flow not indicated by a branch or a kernel xfer
marker.

Adds an online trace_invariants test.

Switches the trace_invariants test to run pthreads.ptsig on UNIX, which is
marked un-FLAKY, to test both threads and signals, and on winxfer on
Windows.

Fixes several issues found by this new test:
+ Adds TRACE_TYPE_INSTR_SYSENTER to mark the PC discontinuity from
  OP_sysenter.
+ Adds proper handling of a mid-bb fault in offline trace conversion by
  looking ahead after each memref to see whether there's a marker.
+ #2011 follow-up: fixes the zero-iter code from 2772b0b which it turns out
  only worked for offline traces.  For online, the top-of-bb instr is
  jecxz, so the instr type and size were wrong.
+ Fixes pipe write splitting to avoid separating an instr from its bundle
  entries.
+ Avoids a marker for a thread init kernel xfer event on Windows.

Fixes #2708
i#2708 trace discontinuity: avoid istream::seekg

Commit f9b6914 added an extra peek ahead when processing a memref in
raw2trace, in order to properly handle a signal delivered in the middle of
a bb.  This peek and subsequent putback with istream::seekg ends up causing
performance problems for some istream implementations.  We avoid that by
doing our own buffering of the 1 or 2 entries that we peek in raw2trace.

Issue: #2708
i#2708 trace discontinuity: avoid istream::seekg (#2749)

Commit f9b6914 added an extra peek ahead when processing a memref in
raw2trace, in order to properly handle a signal delivered in the middle of
a bb.  This peek and subsequent putback with istream::seekg ends up causing
performance problems for some istream implementations.  We avoid that by
doing our own buffering of the 1 or 2 entries that we peek in raw2trace.

For unknown reasons this triggers tool.histogram.offline on 32-bit Appveyor full suite to hang.  #2752 covers figuring it out; for a workaround here we shrink its trace size.

Issue: #2708
i#2708 trace discontinuity: allow single lookahead

Commit 85ca26c added internal buffering for peeking and unpeeking in the
thread trace streams, but it failed to handle missing memrefs at the end of
a trace file.  We fix that here, as well as making eof checking more robust
in the presence of the buffering.

Issue: #2708
i#2708 trace discontinuity: allow single lookahead (#2757)

Commit 85ca26c added internal buffering for peeking and unpeeking in the
thread trace streams, but it failed to handle missing memrefs at the end of
a trace file.  We fix that here, as well as making eof checking more robust
in the presence of the buffering.

Issue: #2708
i#2638 core sim: document several recent features

Adds documentation for several recent features geared toward core
simulator support: avoiding thread switch gaps after branches; cpu
markers; kernel xfer markers; and the raw2trace mapping interfaces.

Issue: #2638, #2843, #2708, #2006
i#2638 core sim: document several recent features (#3035)

Adds documentation for several recent features geared toward core
simulator support: avoiding thread switch gaps after branches; cpu
markers; kernel xfer markers; and the raw2trace mapping interfaces.

Issue: #2638, #2843, #2708, #2006
i#2638 core sim: document several recent features (#3035)

Adds documentation for several recent features geared toward core
simulator support: avoiding thread switch gaps after branches; cpu
markers; kernel xfer markers; and the raw2trace mapping interfaces.

Issue: #2638, #2843, #2708, #2006
