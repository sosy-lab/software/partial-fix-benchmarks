diff --git a/core/globals.h b/core/globals.h
index 60e70c01187..8f2b0725690 100644
--- a/core/globals.h
+++ b/core/globals.h
@@ -886,7 +886,7 @@ struct _dcontext_t {
     reg_t sys_param3; /* used for post_system_call */
 #endif
 #ifdef UNIX
-    reg_t sys_param4; /* used for post_system_call i#173 */
+    reg_t sys_param4; /* used for post_system_call i#173 and i#2759 */
     bool sys_was_int; /* was the last system call via do_int_syscall? */
     bool sys_xbp;     /* PR 313715: store orig xbp */
 #    ifdef DEBUG
diff --git a/core/unix/os.c b/core/unix/os.c
index 077ac955fdf..db08343a455 100644
--- a/core/unix/os.c
+++ b/core/unix/os.c
@@ -5139,6 +5139,12 @@ ignorable_system_call_normalized(int num)
     /* syscall changes app's thread register */
     case SYS_set_tls:
     case SYS_cacheflush:
+#    endif
+#    if defined(LINUX)
+    /* syscalls change procsigmask */
+    case SYS_pselect6:
+    case SYS_ppoll:
+    case SYS_epoll_pwait:
 #    endif
         return false;
 #    ifdef LINUX
@@ -7416,6 +7422,72 @@ pre_system_call(dcontext_t *dcontext)
                                      dcontext->sys_num);
         break;
     }
+#    ifdef LINUX
+    case SYS_ppoll: {
+        kernel_sigset_t *sigmask = (kernel_sigset_t *)sys_param(dcontext, 3);
+        size_t sizemask = (size_t)sys_param(dcontext, 4);
+        /* The original app's sigmask parameter is now NULL effectively making the syscall
+         * a non p* version, and the mask's semantics are emulated by DR instead.
+         */
+        dcontext->sys_param3 = (reg_t)sigmask;
+        set_syscall_param(dcontext, 3, (reg_t)NULL);
+        if (!handle_pre_extended_syscall_sigmasks(dcontext, sigmask, sizemask)) {
+            /* In old kernels with sizeof(kernel_sigset_t) != sizemask, we're forcing
+             * failure. We're already violating app transparency in other places in DR.
+             */
+            set_failure_return_val(dcontext, EINVAL);
+            DODEBUG({ dcontext->expect_last_syscall_to_fail = true; });
+            execute_syscall = false;
+        }
+        break;
+    }
+    case SYS_pselect6: {
+        typedef struct {
+            kernel_sigset_t *sigmask;
+            size_t sizemask;
+        } data_t;
+        dcontext->sys_param3 = sys_param(dcontext, 5);
+        data_t *data_param = (data_t *)dcontext->sys_param3;
+        data_t data;
+        /* Refer to comments in SYS_ppoll above. Taking extra steps here due to struct
+         * argument in pselect6.
+         */
+        if (!safe_read(data_param, sizeof(data), &data)) {
+            LOG(THREAD, LOG_SYSCALLS, 2, "\treturning EFAULT to app for pselect6\n");
+            set_failure_return_val(dcontext, EFAULT);
+            DODEBUG({ dcontext->expect_last_syscall_to_fail = true; });
+            execute_syscall = false;
+        } else {
+            dcontext->sys_param4 = (reg_t)data.sigmask;
+            kernel_sigset_t *nullsigmaskptr = NULL;
+            if (!safe_write_ex((void *)&data_param->sigmask, sizeof(data_param->sigmask),
+                               &nullsigmaskptr, NULL)) {
+                set_failure_return_val(dcontext, EFAULT);
+                DODEBUG({ dcontext->expect_last_syscall_to_fail = true; });
+                execute_syscall = false;
+            } else if (!handle_pre_extended_syscall_sigmasks(dcontext, data.sigmask,
+                                                             data.sizemask)) {
+                set_failure_return_val(dcontext, EINVAL);
+                DODEBUG({ dcontext->expect_last_syscall_to_fail = true; });
+                execute_syscall = false;
+            }
+        }
+        break;
+    }
+    case SYS_epoll_pwait: {
+        kernel_sigset_t *sigmask = (kernel_sigset_t *)sys_param(dcontext, 4);
+        size_t sizemask = (size_t)sys_param(dcontext, 5);
+        /* Refer to comments in SYS_ppoll above. */
+        dcontext->sys_param4 = (reg_t)sigmask;
+        set_syscall_param(dcontext, 4, (reg_t)NULL);
+        if (!handle_pre_extended_syscall_sigmasks(dcontext, sigmask, sizemask)) {
+            set_failure_return_val(dcontext, EINVAL);
+            DODEBUG({ dcontext->expect_last_syscall_to_fail = true; });
+            execute_syscall = false;
+        }
+        break;
+    }
+#    endif
 
     /****************************************************************************/
     /* FILES */
@@ -8584,6 +8656,31 @@ post_system_call(dcontext_t *dcontext)
         break;
     }
 #    endif
+#    ifdef LINUX
+    case SYS_ppoll: {
+        handle_post_extended_syscall_sigmasks(dcontext, success);
+        set_syscall_param(dcontext, 3, dcontext->sys_param3);
+        break;
+    }
+    case SYS_pselect6: {
+        typedef struct {
+            kernel_sigset_t *sigmask;
+            size_t sizemask;
+        } data_t;
+        data_t *data_param = (data_t *)dcontext->sys_param3;
+        handle_post_extended_syscall_sigmasks(dcontext, success);
+        if (!safe_write_ex((void *)&data_param->sigmask, sizeof(data_param->sigmask),
+                           &dcontext->sys_param4, NULL)) {
+            LOG(THREAD, LOG_SYSCALLS, 2, "\tEFAULT for pselect6 post syscall\n");
+        }
+        break;
+    }
+    case SYS_epoll_pwait: {
+        handle_post_extended_syscall_sigmasks(dcontext, success);
+        set_syscall_param(dcontext, 4, dcontext->sys_param4);
+        break;
+    }
+#    endif
 
     /****************************************************************************/
     /* FILES */
diff --git a/core/unix/os_private.h b/core/unix/os_private.h
index 3603d16d237..cecb5badaed 100644
--- a/core/unix/os_private.h
+++ b/core/unix/os_private.h
@@ -307,9 +307,20 @@ handle_sigreturn(dcontext_t *dcontext, bool rt);
 bool
 handle_sigreturn(dcontext_t *dcontext, void *ucxt, int style);
 #endif
+
+#ifdef LINUX
+bool
+handle_pre_extended_syscall_sigmasks(dcontext_t *dcontext, kernel_sigset_t *sigmask,
+                                     size_t sizemask);
+
+void
+handle_post_extended_syscall_sigmasks(dcontext_t *dcontext, bool success);
+#endif
+
 bool
 handle_sigaltstack(dcontext_t *dcontext, const stack_t *stack, stack_t *old_stack,
                    reg_t cur_xsp, OUT uint *result);
+
 bool
 handle_sigprocmask(dcontext_t *dcontext, int how, kernel_sigset_t *set,
                    kernel_sigset_t *oset, size_t sigsetsize);
diff --git a/core/unix/signal_linux.c b/core/unix/signal_linux.c
index 889c6258496..7a662866f0e 100644
--- a/core/unix/signal_linux.c
+++ b/core/unix/signal_linux.c
@@ -349,6 +349,32 @@ signalfd_thread_exit(dcontext_t *dcontext, thread_sig_info_t *info)
     TABLE_RWLOCK(sigfd_table, write, unlock);
 }
 
+bool
+handle_pre_extended_syscall_sigmasks(dcontext_t *dcontext, kernel_sigset_t *sigmask,
+                                     size_t sizemask)
+{
+    thread_sig_info_t *info = (thread_sig_info_t *)dcontext->signal_field;
+
+    /* XXX i#2311, #3240: We may currently deliver incorrect signals, because the
+     * native sigprocmask the system call may get interrupted by may not be the same
+     * as the native app expects. In addition to this, the p* variants of above syscalls
+     * are not properly emulated w.r.t. their atomicity setting the sigprocmask and
+     * executing the syscall.
+     */
+    if (sizemask != sizeof(kernel_sigset_t))
+        return false;
+    info->pre_syscall_app_sigprocmask = info->app_sigblocked;
+    signal_set_mask(dcontext, sigmask);
+    return true;
+}
+
+void
+handle_post_extended_syscall_sigmasks(dcontext_t *dcontext, bool success)
+{
+    thread_sig_info_t *info = (thread_sig_info_t *)dcontext->signal_field;
+    signal_set_mask(dcontext, &info->pre_syscall_app_sigprocmask);
+}
+
 ptr_int_t
 handle_pre_signalfd(dcontext_t *dcontext, int fd, kernel_sigset_t *mask, size_t sizemask,
                     int flags)
diff --git a/core/unix/signal_private.h b/core/unix/signal_private.h
index 476d6cadbb8..4d47a11c27e 100644
--- a/core/unix/signal_private.h
+++ b/core/unix/signal_private.h
@@ -430,7 +430,9 @@ typedef struct _thread_sig_info_t {
     kernel_sigset_t app_sigblocked;
     /* for returning the old mask (xref PR 523394) */
     kernel_sigset_t pre_syscall_app_sigblocked;
-    /* for preserving the app memory (xref i#1187) */
+    /* for preserving the app memory (xref i#1187), and for preserving app
+     * mask supporting ppoll, epoll_pwait and pselect
+     */
     kernel_sigset_t pre_syscall_app_sigprocmask;
     /* for alarm signals arriving in coarse units we only attempt to xl8
      * every nth signal since coarse translation is expensive (PR 213040)
@@ -563,7 +565,7 @@ libc_sigismember(const sigset_t *set, int _sig)
 
 /* XXX: how does libc do this? */
 static inline void
-copy_sigset_to_kernel_sigset(sigset_t *uset, kernel_sigset_t *kset)
+copy_sigset_to_kernel_sigset(const sigset_t *uset, kernel_sigset_t *kset)
 {
     int sig;
     kernel_sigemptyset(kset);
diff --git a/suite/tests/CMakeLists.txt b/suite/tests/CMakeLists.txt
index 171e3af3af6..8ea80794086 100644
--- a/suite/tests/CMakeLists.txt
+++ b/suite/tests/CMakeLists.txt
@@ -3121,6 +3121,9 @@ if (UNIX)
   tobuild(linux.sigplain110 linux/sigplain110.c)
   tobuild(linux.sigplain111 linux/sigplain111.c)
   tobuild(linux.sigaction   linux/sigaction.c)
+  if (LINUX)
+    tobuild(linux.syscall_pwait linux/syscall_pwait.c)
+  endif ()
   if (LINUX AND NOT ANDROID) # Only tests RT sigaction which is not supported on Android.
     tobuild(linux.sigaction_nosignals linux/sigaction_nosignals.c)
   endif ()
diff --git a/suite/tests/linux/syscall_pwait.c b/suite/tests/linux/syscall_pwait.c
new file mode 100644
index 00000000000..a329d100892
--- /dev/null
+++ b/suite/tests/linux/syscall_pwait.c
@@ -0,0 +1,388 @@
+/* **********************************************************
+ * Copyright (c) 2018 Google, Inc.  All rights reserved.
+ * **********************************************************/
+
+/*
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of Google, Inc. nor the names of its contributors may be
+ *   used to endorse or promote products derived from this software without
+ *   specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED. IN NO EVENT SHALL VMWARE, INC. OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
+ * DAMAGE.
+ */
+
+/*
+ * Test of ppoll, pselect and epoll_pwait (xref i#2759, i#3240)
+ */
+
+#include "tools.h"
+
+#ifndef LINUX
+#    error Test of Linux-only system calls
+#endif
+
+#include <signal.h>
+#include <stdio.h>
+#include <stdlib.h>
+#include <string.h>
+#include <sys/epoll.h>
+#include <sys/syscall.h>
+#include <unistd.h>
+#include <time.h>
+#include <poll.h>
+
+typedef struct {
+    sigset_t *sigmask;
+    size_t sizemask;
+} data_t;
+
+static struct timespec sleeptime;
+
+static void
+signal_handler(int sig, siginfo_t *siginfo, void *context)
+{
+    print("signal received: %d\n", sig);
+}
+
+bool
+kick_off_child_signals()
+{
+    /* waste some time */
+    nanosleep(&sleeptime, NULL);
+
+    int pid = fork();
+    if (pid < 0) {
+        perror("fork error");
+    } else if (pid == 0) {
+        /* waste some time */
+        nanosleep(&sleeptime, NULL);
+        kill(getppid(), SIGUSR2);
+        /* waste some time */
+        nanosleep(&sleeptime, NULL);
+        kill(getppid(), SIGUSR1);
+        /* waste some time */
+        nanosleep(&sleeptime, NULL);
+        kill(getppid(), SIGUSR1);
+        return true;
+    }
+
+    return false;
+}
+
+int
+main(int argc, char *argv[])
+{
+    struct sigaction act;
+    sigset_t new_set;
+
+    INIT();
+
+    sleeptime.tv_sec = 0;
+    sleeptime.tv_nsec = 500 * 1000 * 1000;
+
+    intercept_signal(SIGUSR1, (handler_3_t)signal_handler, true);
+    intercept_signal(SIGUSR2, (handler_3_t)signal_handler, true);
+    print("handlers for signals: %d, %d\n", SIGUSR1, SIGUSR2);
+    sigemptyset(&new_set);
+    sigaddset(&new_set, SIGUSR1);
+    sigprocmask(SIG_BLOCK, &new_set, NULL);
+    print("signal blocked: %d\n", SIGUSR1);
+
+    sigset_t test_set;
+    sigfillset(&test_set);
+    sigdelset(&test_set, SIGUSR1);
+    sigdelset(&test_set, SIGUSR2);
+
+    if (kick_off_child_signals())
+        return 0;
+
+    int epoll_fd = epoll_create1(EPOLL_CLOEXEC);
+    struct epoll_event events;
+
+    print("Testing epoll_pwait\n");
+
+    int count = 0;
+    while (count++ < 3) {
+        /* XXX i#3240: DR currently does not handle the atomicity aspect of this system
+         * call. Once it does, please include this in this test or add a new test.
+         */
+        sigset_t pre_syscall_set = {
+            0, /* Set padding to 0 so we can use memcmp */
+        };
+        sigprocmask(SIG_SETMASK, NULL, &pre_syscall_set);
+        if (epoll_pwait(epoll_fd, &events, 24, -1, &test_set) == -1) {
+            if (errno != EINTR)
+                perror("expected EINTR");
+        } else {
+            perror("expected interruption of syscall");
+        }
+        sigset_t post_syscall_set = {
+            0, /* Set padding to 0 so we can use memcmp */
+        };
+        sigprocmask(SIG_SETMASK, NULL, &post_syscall_set);
+        if (memcmp(&pre_syscall_set, &post_syscall_set, sizeof(pre_syscall_set)) != 0) {
+            print("sigmask mismatch");
+            exit(1);
+        }
+    }
+
+    if (kick_off_child_signals())
+        return 0;
+
+    print("Testing pselect\n");
+
+    count = 0;
+    while (count++ < 3) {
+        /* XXX i#3240: DR currently does not handle the atomicity aspect of this system
+         * call. Once it does, please include this in this test or add a new test.
+         */
+        sigset_t pre_syscall_set = {
+            0, /* Set padding to 0 so we can use memcmp */
+        };
+        sigprocmask(SIG_SETMASK, NULL, &pre_syscall_set);
+        if (pselect(0, NULL, NULL, NULL, NULL, &test_set) == -1) {
+            if (errno != EINTR)
+                perror("expected EINTR");
+        } else {
+            perror("expected interruption of syscall");
+        }
+        sigset_t post_syscall_set = {
+            0, /* Set padding to 0 so we can use memcmp */
+        };
+        sigprocmask(SIG_SETMASK, NULL, &post_syscall_set);
+        if (memcmp(&pre_syscall_set, &post_syscall_set, sizeof(pre_syscall_set)) != 0) {
+            print("sigmask mismatch");
+            exit(1);
+        }
+    }
+
+    if (kick_off_child_signals())
+        return 0;
+
+    print("Testing ppoll\n");
+
+    count = 0;
+    while (count++ < 3) {
+        /* XXX i#3240: DR currently does not handle the atomicity aspect of this system
+         * call. Once it does, please include this in this test or add a new test.
+         */
+        sigset_t pre_syscall_set = {
+            0, /* Set padding to 0 so we can use memcmp */
+        };
+        sigprocmask(SIG_SETMASK, NULL, &pre_syscall_set);
+        if (ppoll(NULL, 0, NULL, &test_set) == -1) {
+            if (errno != EINTR)
+                perror("expected EINTR");
+        } else {
+            perror("expected interruption of syscall");
+        }
+        sigset_t post_syscall_set = {
+            0, /* Set padding to 0 so we can use memcmp */
+        };
+        sigprocmask(SIG_SETMASK, NULL, &post_syscall_set);
+        if (memcmp(&pre_syscall_set, &post_syscall_set, sizeof(pre_syscall_set)) != 0) {
+            print("sigmask mismatch");
+            exit(1);
+        }
+    }
+
+    print("Testing epoll_pwait failure\n");
+
+    /* XXX: The following failure tests will 'hang' if syscall succeeds, due to the nature
+     * of the syscall. Maybe change this into something that will rather fail immediately.
+     */
+
+    /* waste some time */
+    nanosleep(&sleeptime, NULL);
+
+    if (syscall(SYS_epoll_pwait, epoll_fd, &events, 24LL, -1LL, &test_set, 0) == 0) {
+        print("expected syscall failure");
+        exit(1);
+    } else if (errno != EINVAL) {
+        print("wrong errno code");
+        exit(1);
+    }
+
+    print("Testing pselect failure\n");
+
+    /* waste some time */
+    nanosleep(&sleeptime, NULL);
+
+    data_t data_wrong = { &test_set, 0 };
+
+    if (syscall(SYS_pselect6, 0, NULL, NULL, NULL, NULL, &data_wrong) == 0) {
+        print("expected syscall failure");
+        exit(1);
+    } else if (errno != EINVAL) {
+        print("wrong errno code");
+        exit(1);
+    }
+
+    print("Testing ppoll failure\n");
+
+    /* waste some time */
+    nanosleep(&sleeptime, NULL);
+
+    if (syscall(SYS_ppoll, NULL, 0, NULL, &test_set, 0) == 0) {
+        print("expected syscall failure");
+        exit(1);
+    } else if (errno != EINVAL) {
+        print("wrong errno code");
+        exit(1);
+    }
+
+#if defined(X86) && defined(X64)
+
+    if (kick_off_child_signals())
+        return 0;
+
+    print("Testing epoll_pwait, preserve mask\n");
+
+    count = 0;
+    while (count++ < 3) {
+        int syscall_error = 0;
+        int mask_error = 0;
+        /* Syscall preserves all registers except rax, rcx and r11. Note that we're
+         * clobbering rbx (which is choosen randomly) in order to save the old mask
+         * for a mask check. Upon a syscall, DR will modify the sigmask parameter
+         * of the call and restore it post syscall. The mask check is making sure
+         * that save/restore is done properly.
+         */
+        asm volatile("mov %7, %%rbx\n\t"
+                     "movq %2, %%rax\n\t"
+                     "movq %3, %%rdi\n\t"
+                     "movq %4, %%rsi\n\t"
+                     "movq %5, %%rdx\n\t"
+                     "movq %6, %%r10\n\t"
+                     "movq %7, %%r8\n\t"
+                     "movq %8, %%r9\n\t"
+                     "syscall\n\t"
+                     "mov $0, %0\n\t"
+                     "cmp $-4095, %%rax\n\t"
+                     "jl no_syscall_error%=\n\t"
+                     "mov $1, %0\n\t"
+                     "no_syscall_error%=:\n\t"
+                     "mov $0, %1\n\t"
+                     "cmp %%rbx, %%r8\n\t"
+                     "je no_mask_error%=\n\t"
+                     "mov $1, %1\n\t"
+                     "no_mask_error%=:\n"
+                     /* early-clobber outputs */
+                     : "=&r"(syscall_error), "=&r"(mask_error)
+                     : "r"((int64_t)SYS_epoll_pwait), "rm"((int64_t)epoll_fd),
+                       "rm"(&events), "r"(24LL), "r"(-1LL), "rm"(&test_set),
+                       "r"((int64_t)(_NSIG / 8))
+                     : "rax", "rdi", "rsi", "rdx", "r10", "r8", "r9", "rbx");
+        if (syscall_error == 0)
+            perror("expected syscall error EINTR");
+        if (mask_error == 1) {
+            /* This checks whether DR has properly restored the mask parameter after the
+             * syscall. I.e. internally, DR may choose to change the parameter prior to
+             * the syscall.
+             */
+            perror("expected syscall to preserve mask parameter");
+        }
+    }
+
+    data_t data = { &test_set, _NSIG / 8 };
+
+    if (kick_off_child_signals())
+        return 0;
+
+    print("Testing pselect, preserve mask\n");
+
+    count = 0;
+    while (count++ < 3) {
+        int syscall_error = 0;
+        int mask_error = 0;
+        asm volatile("movq 0(%8), %%rbx\n\t"
+                     "movq %2, %%rax\n\t"
+                     "movq %3, %%rdi\n\t"
+                     "movq %4, %%rsi\n\t"
+                     "movq %5, %%rdx\n\t"
+                     "movq %6, %%r10\n\t"
+                     "movq %7, %%r8\n\t"
+                     "movq %8, %%r9\n\t"
+                     "syscall\n\t"
+                     "mov $0, %0\n\t"
+                     "cmp $-4095, %%rax\n\t"
+                     "jl no_syscall_error%=\n\t"
+                     "mov $1, %0\n\t"
+                     "no_syscall_error%=:\n\t"
+                     "mov $0, %1\n\t"
+                     "cmp 0(%%r9), %%rbx\n\t"
+                     "je no_mask_error%=\n\t"
+                     "mov $1, %1\n\t"
+                     "no_mask_error%=:\n"
+                     /* early-clobber ouputs */
+                     : "=&r"(syscall_error), "=&r"(mask_error)
+                     : "r"((int64_t)SYS_pselect6), "r"(0LL), "rm"(NULL), "rm"(NULL),
+                       "rm"(NULL), "rm"(NULL), "r"(&data)
+                     : "rax", "rdi", "rsi", "rdx", "r10", "r8", "r9", "rbx");
+        if (syscall_error == 0)
+            perror("expected syscall error EINTR");
+        if (mask_error == 1)
+            perror("expected syscall to preserve mask parameter");
+    }
+
+    if (kick_off_child_signals())
+        return 0;
+
+    print("Testing ppoll, preserve mask\n");
+
+    count = 0;
+    while (count++ < 3) {
+        int syscall_error = 0;
+        int mask_error = 0;
+        asm volatile("mov %6, %%rbx\n\t"
+                     "movq %2, %%rax\n\t"
+                     "movq %3, %%rdi\n\t"
+                     "movq %4, %%rsi\n\t"
+                     "movq %5, %%rdx\n\t"
+                     "movq %6, %%r10\n\t"
+                     "movq %7, %%r8\n\t"
+                     "syscall\n\t"
+                     "mov $0, %0\n\t"
+                     "cmp $-4095, %%rax\n\t"
+                     "jl no_syscall_error%=\n\t"
+                     "mov $1, %0\n\t"
+                     "no_syscall_error%=:\n\t"
+                     "mov $0, %1\n\t"
+                     "cmp %%rbx, %%r10\n\t"
+                     "je no_mask_error%=\n\t"
+                     "mov $1, %1\n\t"
+                     "no_mask_error%=:\n"
+                     /* early-clobber outputs */
+                     : "=&r"(syscall_error), "=&r"(mask_error)
+                     : "r"((int64_t)SYS_ppoll), "rm"(NULL), "r"(0LL), "rm"(NULL),
+                       "rm"(&test_set), "r"((int64_t)(_NSIG / 8))
+                     : "rax", "rdi", "rsi", "rdx", "r10", "r8", "rbx");
+        if (syscall_error == 0)
+            perror("expected syscall error EINTR");
+        if (mask_error == 1)
+            perror("expected syscall to preserve mask parameter");
+    }
+
+#endif
+
+    return 0;
+}
diff --git a/suite/tests/linux/syscall_pwait.template b/suite/tests/linux/syscall_pwait.template
new file mode 100644
index 00000000000..e34daa197f4
--- /dev/null
+++ b/suite/tests/linux/syscall_pwait.template
@@ -0,0 +1,31 @@
+handlers for signals: 10, 12
+signal blocked: 10
+Testing epoll_pwait
+signal received: 12
+signal received: 10
+signal received: 10
+Testing pselect
+signal received: 12
+signal received: 10
+signal received: 10
+Testing ppoll
+signal received: 12
+signal received: 10
+signal received: 10
+Testing epoll_pwait failure
+Testing pselect failure
+Testing ppoll failure
+#if defined(X86) && defined(X64)
+Testing epoll_pwait, preserve mask
+signal received: 12
+signal received: 10
+signal received: 10
+Testing pselect, preserve mask
+signal received: 12
+signal received: 10
+signal received: 10
+Testing ppoll, preserve mask
+signal received: 12
+signal received: 10
+signal received: 10
+#endif
