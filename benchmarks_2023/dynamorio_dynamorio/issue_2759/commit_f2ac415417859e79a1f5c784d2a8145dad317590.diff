diff --git a/core/dispatch.c b/core/dispatch.c
index 94add26fd01..2814a056836 100644
--- a/core/dispatch.c
+++ b/core/dispatch.c
@@ -1993,7 +1993,18 @@ handle_system_call(dcontext_t *dcontext)
             dcontext->sys_param1 = (reg_t)dcontext->next_tag;
             LOG(THREAD, LOG_SYSCALLS, 3, "for sigreturn, set sys_param1 to " PFX "\n",
                 dcontext->sys_param1);
+        } else if (is_prace_syscall(dcontext)) {
+            /* Convert to system call that does not change the signal mask (poll, select,
+             * epoll_wait), as we've already emulated that. XXX i#2311: we may currently
+             * deliver incorrect signals, because the native sigprocmask the system call
+             * may get interrupted by may not be the same as the native app expects. In
+             * addition to this, the p* variants of above syscalls are not properly emu-
+             * lated w.r.t. their atomicity setting the sigprocmask and executing the
+             * syscall. */
+            priv_mcontext_t *mc = get_mcontext(dcontext);
+            MCXT_SYSNUM_REG(mc) = convert_to_non_prace_syscall(dcontext);
         }
+
 #else
         if (use_prev_dcontext) {
             /* get the current, but now swapped out, dcontext */
diff --git a/core/unix/os.c b/core/unix/os.c
index ff3cf5e3810..47499750168 100644
--- a/core/unix/os.c
+++ b/core/unix/os.c
@@ -5138,6 +5138,12 @@ ignorable_system_call_normalized(int num)
     /* syscall changes app's thread register */
     case SYS_set_tls:
     case SYS_cacheflush:
+#    endif
+#    if defined(LINUX)
+    /* syscalls change procsigmask */
+    case SYS_pselect6:
+    case SYS_ppoll:
+    case SYS_epoll_pwait:
 #    endif
         return false;
 #    ifdef LINUX
@@ -5520,6 +5526,45 @@ was_sigreturn_syscall(dcontext_t *dcontext)
     return is_sigreturn_syscall_number(dcontext->sys_num);
 }
 
+int
+convert_to_non_prace_syscall_number(int sysnum)
+{
+#    if defined(LINUX)
+    if (sysnum == SYS_ppoll)
+        return SYS_poll;
+    if (sysnum == SYS_pselect6)
+        return SYS_select;
+    if (sysnum == SYS_epoll_pwait)
+        return SYS_epoll_wait;
+#    endif
+    ASSERT_NOT_REACHED();
+    return -1;
+}
+
+int
+convert_to_non_prace_syscall(dcontext_t *dcontext)
+{
+    priv_mcontext_t *mc = get_mcontext(dcontext);
+    return convert_to_non_prace_syscall_number(MCXT_SYSNUM_REG(mc));
+}
+
+bool
+is_prace_syscall_number(int sysnum)
+{
+#    ifdef LINUX
+    return sysnum == SYS_ppoll || sysnum == SYS_pselect6 || sysnum == SYS_epoll_pwait;
+#    else
+    return false;
+#    endif
+}
+
+bool
+is_prace_syscall(dcontext_t *dcontext)
+{
+    priv_mcontext_t *mc = get_mcontext(dcontext);
+    return is_prace_syscall_number(MCXT_SYSNUM_REG(mc));
+}
+
 /* process a signal this process/thread is sending to itself */
 static void
 handle_self_signal(dcontext_t *dcontext, uint sig)
@@ -7690,6 +7735,21 @@ pre_system_call(dcontext_t *dcontext)
 #        endif
 #    endif
 
+#    if defined(LINUX)
+    case SYS_ppoll: {
+        handle_pre_prace_sigmasks(dcontext, (const sigset_t *)sys_param(dcontext, 3));
+        break;
+    }
+    case SYS_pselect6: {
+        handle_pre_prace_sigmasks(dcontext, (const sigset_t *)sys_param(dcontext, 5));
+        break;
+    }
+    case SYS_epoll_pwait: {
+        handle_pre_prace_sigmasks(dcontext, (const sigset_t *)sys_param(dcontext, 4));
+        break;
+    }
+#    endif
+
     default: {
 #    ifdef LINUX
         execute_syscall = handle_restartable_region_syscall_pre(dcontext);
@@ -8682,6 +8742,12 @@ post_system_call(dcontext_t *dcontext)
         break;
 #    endif
 
+#    ifdef LINUX
+    case SYS_pselect6:
+    case SYS_ppoll:
+    case SYS_epoll_pwait: handle_post_prace_sigmasks(dcontext, success);
+#    endif
+
     default:
 #    ifdef LINUX
         handle_restartable_region_syscall_post(dcontext, success);
diff --git a/core/unix/os_exports.h b/core/unix/os_exports.h
index 79126ecdf4e..c2072425b48 100644
--- a/core/unix/os_exports.h
+++ b/core/unix/os_exports.h
@@ -336,6 +336,11 @@ bool
 was_sigreturn_syscall(dcontext_t *dcontext);
 bool
 ignorable_system_call(int num, instr_t *gateway, dcontext_t *dcontext_live);
+bool
+is_prace_syscall(dcontext_t *dcontext);
+
+int
+convert_to_non_prace_syscall(dcontext_t *dcontext);
 
 bool
 kernel_is_64bit(void);
diff --git a/core/unix/os_private.h b/core/unix/os_private.h
index 3603d16d237..89c0f52eddd 100644
--- a/core/unix/os_private.h
+++ b/core/unix/os_private.h
@@ -250,6 +250,12 @@ os_walk_address_space(memquery_iter_t *iter, bool add_modules);
 bool
 is_sigreturn_syscall_number(int sysnum);
 
+int
+convert_to_non_prace_syscall_number(int sysnum);
+
+bool
+is_prace_syscall_number(int sysnum);
+
 /* in signal.c */
 struct _kernel_sigaction_t;
 typedef struct _kernel_sigaction_t kernel_sigaction_t;
@@ -307,9 +313,19 @@ handle_sigreturn(dcontext_t *dcontext, bool rt);
 bool
 handle_sigreturn(dcontext_t *dcontext, void *ucxt, int style);
 #endif
+
+#ifdef LINUX
+void
+handle_pre_prace_sigmasks(dcontext_t *dcontext, const sigset_t *mask);
+
+void
+handle_post_prace_sigmasks(dcontext_t *dcontext, bool success);
+#endif
+
 bool
 handle_sigaltstack(dcontext_t *dcontext, const stack_t *stack, stack_t *old_stack,
                    reg_t cur_xsp, OUT uint *result);
+
 bool
 handle_sigprocmask(dcontext_t *dcontext, int how, kernel_sigset_t *set,
                    kernel_sigset_t *oset, size_t sigsetsize);
diff --git a/core/unix/signal_linux.c b/core/unix/signal_linux.c
index 889c6258496..0697ec6858a 100644
--- a/core/unix/signal_linux.c
+++ b/core/unix/signal_linux.c
@@ -349,6 +349,27 @@ signalfd_thread_exit(dcontext_t *dcontext, thread_sig_info_t *info)
     TABLE_RWLOCK(sigfd_table, write, unlock);
 }
 
+void
+handle_pre_prace_sigmasks(dcontext_t *dcontext, const sigset_t *mask)
+{
+    thread_sig_info_t *info = (thread_sig_info_t *)dcontext->signal_field;
+    kernel_sigset_t set;
+    sigset_t safe_set;
+    info->pre_syscall_app_sigprocmask = info->app_sigblocked;
+
+    if (mask != NULL && safe_read(mask, sizeof(safe_set), &safe_set)) {
+        copy_sigset_to_kernel_sigset((sigset_t *)&safe_set, &set);
+        signal_set_mask(dcontext, &set);
+    }
+}
+
+void
+handle_post_prace_sigmasks(dcontext_t *dcontext, bool success)
+{
+    thread_sig_info_t *info = (thread_sig_info_t *)dcontext->signal_field;
+    signal_set_mask(dcontext, &info->pre_syscall_app_sigprocmask);
+}
+
 ptr_int_t
 handle_pre_signalfd(dcontext_t *dcontext, int fd, kernel_sigset_t *mask, size_t sizemask,
                     int flags)
diff --git a/core/unix/signal_private.h b/core/unix/signal_private.h
index 476d6cadbb8..805d10ca234 100644
--- a/core/unix/signal_private.h
+++ b/core/unix/signal_private.h
@@ -430,7 +430,8 @@ typedef struct _thread_sig_info_t {
     kernel_sigset_t app_sigblocked;
     /* for returning the old mask (xref PR 523394) */
     kernel_sigset_t pre_syscall_app_sigblocked;
-    /* for preserving the app memory (xref i#1187) */
+    /* for preserving the app memory (xref i#1187), and for preserving app
+     * mask supporting ppoll, epoll_pwait and pselect */
     kernel_sigset_t pre_syscall_app_sigprocmask;
     /* for alarm signals arriving in coarse units we only attempt to xl8
      * every nth signal since coarse translation is expensive (PR 213040)
@@ -563,7 +564,7 @@ libc_sigismember(const sigset_t *set, int _sig)
 
 /* XXX: how does libc do this? */
 static inline void
-copy_sigset_to_kernel_sigset(sigset_t *uset, kernel_sigset_t *kset)
+copy_sigset_to_kernel_sigset(const sigset_t *uset, kernel_sigset_t *kset)
 {
     int sig;
     kernel_sigemptyset(kset);
