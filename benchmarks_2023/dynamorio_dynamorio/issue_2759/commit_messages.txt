i#3203: Add a microbenchmark to test the cache miss analyzer (#3229)

Adds a microbenchmark to test the newly added LLC miss analyzer.

Issue #3203
i#2759 ppoll, pselect, epoll_pwait: add support

Add support for above system calls, saving, restoring sigprocmask.

Fixes #2759
i#2759 ppoll, pselect, epoll_pwait: add support (#3238)

Add emulation support for ppoll, pselect and epoll_pwait system calls, saving, restoring sigprocmask.

Add test for system calls.

Issue: #2759, #3240
i#2759 api.ibl-stress test: checklevel only debug

Disables checklevel option for release build regression.

Issue: #2759
