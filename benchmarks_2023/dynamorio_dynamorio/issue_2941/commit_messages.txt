i#3278 windows file path: temporarily strip dot/slash in path (#3279)

Temporarily strip path such that Windows loader can handle it (xref DrMi#2138).

Issue: #3278
i#2941 blah: blah

blah

Fixes #2941
i#2941: properly recognize DR scratch register slot in drreg

Fix drreg to properly recognize and ignore the 3rd DR slot if a spill is detected but not restore is found. This does not fix #2933 as it is still possible to break drreg with a client that is using DR spill slots.

Issue: #2933
Fixes #2941
i#2941 recreate_app_state spill/restore: properly recognize DR scratch register slot in drreg (#3301)

Fix drreg to properly recognize and ignore the 3rd DR slot if a spill is detected but not restore is found. This does not fix #2933 as it is still possible to break drreg with a client that is using DR spill slots.

Add test for above.

Issue: #2941, #2933, #511
i#2941 recreate_app_state spill/restore: robustly recognize hidden slot used by DR

Deterministically fix the hidden slot to number <num_spill_slots> regardless of whether DR slots are ordered low to high or high to low.

Issue: #2941, #2933
i#2941 recreate_app_state spill/restore: robustly recognize non-public DR slot used by DR mangling (#3308)

Deterministically set the non-public DR slots used by DR mangling to number <num_spill_slots> in drreg, regardless of whether DR slots are ordered low to high or high to low.

Adds subtest to drreg-test that is temporarily disabled due to #3312.

Issue: #2941, #2933, #3312
i#2941 recreate_app_state spill/restore: test restore of non-public DR slot in mangling

Adds test to check whether drreg ignores restoring non-public slot of DR used for mangling. DR secretly violates transparency in extreme cases, if scratch reg is optimized into using app's dead reg of rip-rel instruction. This patch adds an option to DR used by this test to avoid this.

Issue: #2941, #2933
i#2941 recreate_app_state spill/restore: test restore of non-public DR slot in mangling (#3313)

Adds test to check whether drreg ignores restoring non-public slot of DR used for mangling. DR secretly violates transparency in extreme cases if scratch reg is optimized into using app's dead reg of rip-rel instruction.

Issue: #2941, #2933
i#3307 mangling epilogue asynch interrupt: fix rip-rel and syscall mangling case

Fixes xl8 if an asynch signal arrives in epilogue of rip-rel and syscall mangling.

Adds a test. Please note that this test does not properly test this feature on tested machines yet and needs improvement.

Issue: #3307
Fixes #2941
i#3307 mangling epilogue asynch interrupt: fix rip-rel mangling case (#3318)

Supports xl8 of points in epilogue of rip-rel mangling, caused by an asynch signal. Adds support to advance the program counter properly as described in xref #3307 for the simplest case, which is rip-rel mangled instruction not part of another mangled region. These simple mangling epilogue instructions are marked with a new flag, and checks have been added to constrain this to the simplest case.

Adds 2 tests. The test client.mangle_suspend reliably exposes bug as described above.

Issue: #3307
Fixes #2941
