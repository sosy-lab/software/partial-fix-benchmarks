i#3201: Fix MacOS build failure. (#3215)

Fixes a MacOS build failure introduced in 55c78c48 which changed the memquery_library_bounds API.

Issue: #3201
i#3203: Add a basic last-level cache (LLC) miss analyzer (#3205)

The analyzer first identifies load instructions that suffer from significant last-level cache (LLC) misses. The analyzer then analyzes the cache line address streams for each of those load instructions in search for patterns that can be used for SW prefetching. The analyzer here can only find simple strides. Analyses for more complex patterns can be added in the future.
A subsequent PR will connect the changes here to the front-end via new runtime options.

Issue #3203
i#3203: Add the interface for the cache miss analyzer (#3222)

An interface is added for the cache miss analyzer added in PR# 3205. The interface allows the user to invoke the analyzer using command-line parameters.

Issue #3203
i#3203: Add the interface for the cache miss analyzer (#3222)

An interface is added for the cache miss analyzer added in PR# 3205. The interface allows the user to invoke the analyzer using command-line parameters.

Issue #3203
i#3203: Add a microbenchmark to test the cache miss analyzer (#3229)

Adds a microbenchmark to test the newly added LLC miss analyzer.

Issue #3203
i#3203: Add an example in the documentation for the cache miss analyzer (#3231)

Adds an example in the documentation for the cache miss analyzer to show how to run the analyzer on a test program.

Issue #3203
