Avoid blind syslog during init (#4729)

Avoids printing of an internal warning during early initialization for
single-bitwidth setups regardless of -stderr_mask by moving options
init even earlier.

To avoid DR heap init messing up the app's brk setup, moves heap init
out of the options init and into the later half.  This undoes the
early heap init from PR #4726, which is worked around by switching to
a stack buffer for -arch_init.  This seems safer in any case, delaying
heap init and client lib loads until after the app's interpreter is
loaded.

Moves the 1config file deletion from d_r_config_init() to -config_heap_init(),
after any potential reload_dynamorio().

Issue: #4719
i#4731: Properly handle non-empty segment gaps in raw2trace

Switches the module_mapper_t used by raw2trace to explicitly track
individual segments, rather than collapsing them into the single
whole-module mmap, in order to handle objects like vdso allocated in
gaps between the segments.

Tested on a proprietary system where vdso was in the middle of ld.so,
causing the opcode_mix tool to crash.  (Creating a regression test
would be complex, requiring synthesizing ELF files with the required
characteristics.)

Fixes #4731
i#4731: Properly handle non-empty segment gaps in raw2trace (#4733)

Switches the module_mapper_t used by raw2trace to explicitly track
individual segments, rather than collapsing them into the single
whole-module mmap, in order to handle objects like vdso allocated in
gaps between the segments.

Tested on a proprietary system where vdso was in the middle of ld.so,
causing the opcode_mix tool to crash.  (Creating a regression test
would be complex, requiring synthesizing ELF files with the required
characteristics.)

Fixes #4731
i#4731: Fix DR_MAPEXE_SKIP_WRITABLE for cross-arch

Fixes a problem where the final-segment check for
DR_MAPEXE_SKIP_WRITABLE fails to check the memory size and assumes it
is just the page extension of the file size.

This fixes an assert coming from PR #4733 when run on x86 on AArch64
traces.

Fixes #4731
i#4731: Fix DR_MAPEXE_SKIP_WRITABLE for cross-arch (#4738)

Fixes a problem where the final-segment check for
DR_MAPEXE_SKIP_WRITABLE fails to check the memory size and assumes it
is just the page extension of the file size.

This fixes an assert coming from PR #4733 when run on x86 on AArch64
traces.

Fixes #4731
