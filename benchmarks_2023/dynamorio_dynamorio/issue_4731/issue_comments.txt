CRASH in raw2trace module mapping when VDSO in between segments of a library
To clarify: this bug does not affect basic post-processing of raw trace files, since that uses module indices stored in the trace.  It only affects uses of the module mapping interface exposed by the raw2trace code: which is opcode_mix and view and other analysis tools such as for core simulator integration.
There was a problem with #4733: an assert I added there sometimes fires when processing an ARM trace on x86:
```
ASSERT FAILURE: clients/drcachesim/tracer/raw2trace.cpp:377: off_end || info.start - modvec_[info.containing_index].orig_seg_base + info.size <= modvec_[info.containing_index].total_map_size ()
```

The details for one case:
```
off_end=0 cur=0x4a0000-0x4b0000 containing=0x400000-0x4a0000 tot size=712704 ==0xae000

  0,   0, 0x0000000000400000, 0x00000000004a0000, 0x0000000000400678, 0000000000000000, v#1,0, xx
  1,   0, 0x00000000004a0000, 0x00000000004b0000, 0x0000000000400678, 0000000000094e80, v#1,0, xx
  2,   2, 0x0000000072000000, 0x0000000072030000, 0x0000000072008710, 0000000000000000, v#1,0, aa

Program Headers:
  Type           Offset             VirtAddr           PhysAddr
                 FileSiz            MemSiz              Flags  Align
  LOAD           0x0000000000000000 0x0000000000400000 0x0000000000400000
                 0x00000000000946e4 0x00000000000946e4  R E    0x10000
  LOAD           0x0000000000094e80 0x00000000004a4e80 0x00000000004a4e80
                 0x0000000000002bb8 0x0000000000009000  RW     0x10000
```

So the map size 0xae000 must be 0x4e80+0x9000=0xde80.
Why didn't it align to 0xb0000?
Is it that DR's code is aligning to x86's PAGE_SIZE instead of the ELF
alignment?
But even so: in skipping the +w segment shouldn't it use the prior end
which would be 0a495000 if x86-aligned?  I think b/c it only doesn't map if
the seg end matches the lib end.

After `s/PAGE_SIZE/prog_hdr->p_align/` in drlibc_module_elf.c the assert goes
away and we're mapping just the rx:
```
off_end=1 cur=0x4a0000-0x4b0000 containing=0x400000-0x4a0000 tot size=655360 = 0xa0000
```

> After s/PAGE_SIZE/prog_hdr->p_align/ in drlibc_module_elf.c the assert goes
away and we're mapping just the rx:

That leads to a rabbit hole of problems: on one system, libdynamorio.so's segments all have 2MB p_align, yet the kernel completely ignores that, causing a mismatch in DR's early injection relocation and other actions and resulting crashes and asserts.

Going to have to look for a different solution.  Even though DR's ELF code seems to be missing corner case handling by ignoring p_align, the situation is apparently complex with some kernel ignoring it (and some loaders??).  Maybe best to leave the page alignment, even though it's the *wrong* page size (xref earlier discussions of storing the page size in the trace header??), and try to get the skip-writable to do fuzzy matching: or else relax the assert and hope nobody dereferences off the end.
Filed #4737 on the p_align issues.  Falling back on just trying to fix the final-segment skip for writable segment skipping.