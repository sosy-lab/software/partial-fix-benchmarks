diff --git a/ext/drwrap/drwrap.c b/ext/drwrap/drwrap.c
index 653650505d6..4eb3989df63 100644
--- a/ext/drwrap/drwrap.c
+++ b/ext/drwrap/drwrap.c
@@ -885,6 +885,12 @@ static dr_emit_flags_t
 drwrap_event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
                        bool for_trace, bool translating, void *user_data);
 
+/* This version takes a separate "instr" and "where" for use with drbbdup. */
+static dr_emit_flags_t
+drwrap_event_bb_insert_where(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
+                             instr_t *where, bool for_trace, bool translating,
+                             void *user_data);
+
 static void
 drwrap_event_module_unload(void *drcontext, const module_data_t *info);
 
@@ -946,9 +952,11 @@ drwrap_init(void)
     drmgr_init();
     if (!drmgr_register_bb_app2app_event(drwrap_event_bb_app2app, &pri_replace))
         return false;
-    if (!drmgr_register_bb_instrumentation_event(drwrap_event_bb_analysis,
-                                                 drwrap_event_bb_insert, &pri_insert))
-        return false;
+    if (!TEST(DRWRAP_INVERT_CONTROL, global_flags)) {
+        if (!drmgr_register_bb_instrumentation_event(drwrap_event_bb_analysis,
+                                                     drwrap_event_bb_insert, &pri_insert))
+            return false;
+    }
     if (!drmgr_register_restore_state_ex_event(drwrap_event_restore_state_ex))
         return false;
 
@@ -963,7 +971,9 @@ drwrap_init(void)
                       false /*!str_dup*/, false /*!synch*/, post_call_entry_free, NULL,
                       NULL);
     post_call_rwlock = dr_rwlock_create();
-    wrap_lock = dr_recurlock_create();
+    /* This lock may have been set up by drwrap_set_global_flags() (in this thread). */
+    if (wrap_lock == NULL)
+        wrap_lock = dr_recurlock_create();
     drmgr_register_module_unload_event(drwrap_event_module_unload);
 
     tls_idx = drmgr_register_tls_field();
@@ -1005,13 +1015,28 @@ drwrap_exit(void)
     if (count != 0)
         return;
 
+    if (!TEST(DRWRAP_INVERT_CONTROL, global_flags)) {
+        if (!drmgr_unregister_bb_instrumentation_event(drwrap_event_bb_analysis))
+            ASSERT(false, "failed to unregister in drwrap_exit");
+    }
     if (!drmgr_unregister_bb_app2app_event(drwrap_event_bb_app2app) ||
-        !drmgr_unregister_bb_instrumentation_event(drwrap_event_bb_analysis) ||
         !drmgr_unregister_restore_state_ex_event(drwrap_event_restore_state_ex) ||
         !drmgr_unregister_module_unload_event(drwrap_event_module_unload) ||
+        !drmgr_unregister_thread_init_event(drwrap_thread_init) ||
+        !drmgr_unregister_thread_exit_event(drwrap_thread_exit) ||
         !drmgr_unregister_tls_field(tls_idx))
         ASSERT(false, "failed to unregister in drwrap_exit");
 
+#ifdef WINDOWS
+    if (sysnum_NtContinue != -1) {
+        if (!dr_unregister_filter_syscall_event(drwrap_event_filter_syscall) ||
+            !drmgr_unregister_pre_syscall_event(drwrap_event_pre_syscall))
+            ASSERT(false, "failed to unregister in drwrap_exit");
+    }
+    if (!drmgr_unregister_exception_event(drwrap_event_exception))
+        ASSERT(false, "failed to unregister in drwrap_exit");
+#endif
+
     if (dr_is_detaching()) {
         memset(&drwrap_stats, 0, sizeof(drwrap_stats_t));
         for (int i = 0; i < POSTCALL_CACHE_SIZE; i++) {
@@ -1030,6 +1055,7 @@ drwrap_exit(void)
     hashtable_delete(&post_call_table);
     dr_rwlock_destroy(post_call_rwlock);
     dr_recurlock_destroy(wrap_lock);
+    wrap_lock = NULL; /* For early drwrap_set_global_flags() after re-attach. */
     drmgr_exit();
 
     while (post_call_notify_list != NULL) {
@@ -1085,7 +1111,21 @@ drwrap_set_global_flags(drwrap_global_flags_t flags)
 {
     drwrap_global_flags_t old_flags;
     bool res;
+    /* This can be called prior to drwrap_init().
+     * We only support this being done in the single DR-initialization thread.
+     */
+    if (wrap_lock == NULL) {
+        ASSERT(dr_atomic_load32(&drwrap_init_count) == 0,
+               "Unsupported race between drwrap_init() and drwrap_set_global_flags()");
+        wrap_lock = dr_recurlock_create();
+    }
     dr_recurlock_lock(wrap_lock);
+    if (dr_atomic_load32(&drwrap_init_count) > 0 &&
+        /* After drwrap_init() was called, control inversion cannot be changed. */
+        (flags & DRWRAP_INVERT_CONTROL) != (global_flags & DRWRAP_INVERT_CONTROL)) {
+        dr_recurlock_unlock(wrap_lock);
+        return false;
+    }
     /* if anyone asks for safe, be safe.
      * since today the only 2 flags ask for safe, we can accomplish that
      * by simply or-ing in each request.
@@ -1099,6 +1139,28 @@ drwrap_set_global_flags(drwrap_global_flags_t flags)
     return res;
 }
 
+DR_EXPORT
+dr_emit_flags_t
+drwrap_invoke_analysis(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
+                       bool translating, OUT void **user_data)
+{
+    ASSERT(TEST(DRWRAP_INVERT_CONTROL, global_flags),
+           "must set DRWRAP_INVERT_CONTROL to call drwrap_invoke_analysis");
+    return drwrap_event_bb_analysis(drcontext, tag, bb, for_trace, translating,
+                                    user_data);
+}
+
+DR_EXPORT
+dr_emit_flags_t
+drwrap_invoke_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
+                     instr_t *where, bool for_trace, bool translating, void *user_data)
+{
+    ASSERT(TEST(DRWRAP_INVERT_CONTROL, global_flags),
+           "must set DRWRAP_INVERT_CONTROL to call drwrap_invoke_app2app");
+    return drwrap_event_bb_insert_where(drcontext, tag, bb, inst, where, for_trace,
+                                        translating, user_data);
+}
+
 /***************************************************************************
  * FUNCTION REPLACING
  */
@@ -1236,6 +1298,12 @@ DR_EXPORT
 bool
 drwrap_replace(app_pc original, app_pc replacement, bool override)
 {
+    if (TEST(DRWRAP_INVERT_CONTROL, global_flags)) {
+        /* Not supported in this mode since drbbdup does not support a separate
+         * app2app per case.
+         */
+        return false;
+    }
     return drwrap_replace_common(&replace_table, original, replacement, override, false);
 }
 
@@ -1244,6 +1312,12 @@ bool
 drwrap_replace_native(app_pc original, app_pc replacement, bool at_entry,
                       uint stack_adjust, void *user_data, bool override)
 {
+    if (TEST(DRWRAP_INVERT_CONTROL, global_flags)) {
+        /* Not supported in this mode since drbbdup does not support a separate
+         * app2app per case.
+         */
+        return false;
+    }
     bool res = false;
     replace_native_t *rn;
     if (stack_adjust > max_stack_adjust ||
@@ -2299,9 +2373,16 @@ drwrap_event_bb_analysis(void *drcontext, void *tag, instrlist_t *bb, bool for_t
     return DR_EMIT_DEFAULT;
 }
 
+/* This version takes a separate "instr" and "where" for use with drbbdup.
+ * The separate "where" handles cases such as with drbbdup's final app
+ * instruction (which cannot be duplicated into each case) or with
+ * emulation where the instruction "inst" to monitor is distinct from
+ * the location "where" to insert instrumentation.
+ */
 static dr_emit_flags_t
-drwrap_event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
-                       bool for_trace, bool translating, void *user_data)
+drwrap_event_bb_insert_where(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
+                             instr_t *where, bool for_trace, bool translating,
+                             void *user_data)
 {
     dr_emit_flags_t res = DR_EMIT_DEFAULT;
     /* XXX: if we had dr_bbs_cross_ctis() query (i#427) we could just check 1st instr
@@ -2335,7 +2416,7 @@ drwrap_event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *ins
             ? (DR_CLEANCALL_NOSAVE_FLAGS | DR_CLEANCALL_NOSAVE_XMM_NONPARAM)
             : 0;
         flags |= DR_CLEANCALL_READS_APP_CONTEXT | DR_CLEANCALL_WRITES_APP_CONTEXT;
-        dr_insert_clean_call_ex(drcontext, bb, inst, (void *)drwrap_in_callee, flags,
+        dr_insert_clean_call_ex(drcontext, bb, where, (void *)drwrap_in_callee, flags,
                                 IF_X86_ELSE(2, 3), OPND_CREATE_INTPTR((ptr_int_t)arg1),
                                 /* pass in xsp to avoid dr_get_mcontext */
                                 opnd_create_reg(DR_REG_XSP)
@@ -2344,20 +2425,20 @@ drwrap_event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *ins
     dr_recurlock_unlock(wrap_lock);
 
     if (post_call_lookup_for_instru(instr_get_app_pc(inst) /*normalized*/)) {
-        drwrap_insert_post_call(drcontext, bb, inst, pc);
+        drwrap_insert_post_call(drcontext, bb, where, pc);
     }
 
     if (dr_fragment_app_pc(tag) == (app_pc)replace_retaddr_sentinel) {
-        drwrap_insert_post_call(drcontext, bb, inst, pc);
+        drwrap_insert_post_call(drcontext, bb, where, pc);
         /* The post-call C code put the real retaddr into the DR slot that will be
          * used by dr_redirect_native_target().
          */
         app_pc tgt = dr_redirect_native_target(drcontext);
         reg_id_t scratch = RETURN_POINT_SCRATCH_REG;
         instrlist_insert_mov_immed_ptrsz(drcontext, (ptr_int_t)tgt,
-                                         opnd_create_reg(scratch), bb, inst, NULL, NULL);
+                                         opnd_create_reg(scratch), bb, where, NULL, NULL);
         instrlist_meta_preinsert(
-            bb, inst, XINST_CREATE_jump_reg(drcontext, opnd_create_reg(scratch)));
+            bb, where, XINST_CREATE_jump_reg(drcontext, opnd_create_reg(scratch)));
         /* This unusual transition confuses DR trying to stitch blocks together into
          * a trace.
          */
@@ -2384,6 +2465,16 @@ drwrap_event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *ins
     return res;
 }
 
+static dr_emit_flags_t
+drwrap_event_bb_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
+                       bool for_trace, bool translating, void *user_data)
+{
+    ASSERT(!TEST(DRWRAP_INVERT_CONTROL, global_flags),
+           "should not get here if DRWRAP_INVERT_CONTROL is set");
+    return drwrap_event_bb_insert_where(drcontext, tag, bb, inst, inst, for_trace,
+                                        translating, user_data);
+}
+
 static void
 drwrap_event_module_unload(void *drcontext, const module_data_t *info)
 {
diff --git a/ext/drwrap/drwrap.h b/ext/drwrap/drwrap.h
index d70b982f3e6..f1c87a9f1bc 100644
--- a/ext/drwrap/drwrap.h
+++ b/ext/drwrap/drwrap.h
@@ -1,5 +1,5 @@
 /* **********************************************************
- * Copyright (c) 2010-2021 Google, Inc.   All rights reserved.
+ * Copyright (c) 2010-2022 Google, Inc.   All rights reserved.
  * **********************************************************/
 
 /* drwrap: DynamoRIO Function Wrapping and Replacing Extension
@@ -66,6 +66,9 @@ DR_EXPORT
  * normally) but each call must be paired with a corresponding call to
  * drwrap_exit().
  *
+ * Some drwrap behavior (such as #DRWRAP_INVERT_CONTROL) must be set by calling
+ * drwrap_set_global_flags() *before* calling this routine.
+ *
  * \return whether successful.
  */
 bool
@@ -78,6 +81,42 @@ DR_EXPORT
 void
 drwrap_exit(void);
 
+/***************************************************************************
+ * CONTROL INVERSION
+ */
+
+DR_EXPORT
+/**
+ * When #drwrap_global_flags_t #DRWRAP_INVERT_CONTROL is set, the user
+ * must call this function from a drmgr analysis event handler
+ * (typically registered with
+ * drmgr_register_bb_instrumentation_event() or if using drbbdup with
+ * the analyze_case_ex field in #drbbdup_options_t).  It is up to the user
+ * to control the ordering, since the priority #DRMGR_PRIORITY_INSERT_DRWRAP
+ * will not apply.
+ */
+dr_emit_flags_t
+drwrap_invoke_analysis(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
+                       bool translating, OUT void **user_data);
+
+DR_EXPORT
+/**
+ * When #drwrap_global_flags_t #DRWRAP_INVERT_CONTROL is set, the user
+ * must call this function from a drmgr insertion event handler
+ * (typically registered with
+ * drmgr_register_bb_instrumentation_event() or if using drbbdup with
+ * the instrument_instr_ex field in #drbbdup_options_t).  It is up to the user
+ * to control the ordering, since the priority #DRMGR_PRIORITY_INSERT_DRWRAP
+ * will not apply.
+ * The separate "where" handles cases such as with drbbdup's final app
+ * instruction (which cannot be duplicated into each case) or with
+ * emulation where the instruction "inst" to monitor is distinct from
+ * the location "where" to insert instrumentation.
+ */
+dr_emit_flags_t
+drwrap_invoke_insert(void *drcontext, void *tag, instrlist_t *bb, instr_t *inst,
+                     instr_t *where, bool for_trace, bool translating, void *user_data);
+
 /***************************************************************************
  * FUNCTION REPLACING
  */
@@ -136,6 +175,8 @@ DR_EXPORT
  * DRMGR_PRIORITY_APP2APP_DRWRAP and its name is
  * DRMGR_PRIORITY_NAME_DRWRAP.
  *
+ * \note Not supported if #DRWRAP_INVERT_CONTROL is set.
+ *
  * \return whether successful.
  */
 bool
@@ -243,6 +284,8 @@ DR_EXPORT
  *
  * \note Far calls are not supported.
  *
+ * \note Not supported if #DRWRAP_INVERT_CONTROL is set.
+ *
  * \return whether successful.
  */
 bool
@@ -751,6 +794,21 @@ typedef enum {
      * Once set, this flag cannot be unset.
      */
     DRWRAP_FAST_CLEANCALLS = 0x08,
+    /**
+     * This flag must only be set before calling drwrap_init().  If set, drwrap will not
+     * register for the drmgr analysis or insertion events.  The user must instead
+     * explicitly invoke drwrap_invoke_analysis() and drwrap_invoke_insert() from its
+     * own handler for those events.  This "inverted control" mode is provided for
+     * better compatibility with drbbdup where the user wishes to only perform wrapping
+     * in a subset of the drbbdup cases.
+     *
+     * Only wrapping is supported this way: drwrap_replace() and drwrap_replace_native()
+     * are not supported when this flag is set.
+     *
+     * As this is a global change in how drwrap operates, be careful that its use
+     * does not conflict with drwrap uses in any libraries or joint clients.
+     */
+    DRWRAP_INVERT_CONTROL = 0x10,
 } drwrap_global_flags_t;
 
 DR_EXPORT
diff --git a/suite/tests/CMakeLists.txt b/suite/tests/CMakeLists.txt
index b7963c8ef65..05f1942aee9 100644
--- a/suite/tests/CMakeLists.txt
+++ b/suite/tests/CMakeLists.txt
@@ -2594,6 +2594,15 @@ use_DynamoRIO_extension(client.drbbdup-analysis-test.dll drmgr)
 use_DynamoRIO_extension(client.drbbdup-analysis-test.dll drreg)
 use_DynamoRIO_extension(client.drbbdup-analysis-test.dll drbbdup)
 
+tobuild_appdll(client.drbbdup-drwrap-test client-interface/drbbdup-drwrap-test.c)
+get_target_path_for_execution(bbdupwrap_libpath
+  client.drbbdup-drwrap-test.appdll "${location_suffix}")
+tobuild_ci(client.drbbdup-drwrap-test client-interface/drbbdup-drwrap-test.c "" ""
+  "${bbdupwrap_libpath}")
+use_DynamoRIO_extension(client.drbbdup-drwrap-test.dll drmgr)
+use_DynamoRIO_extension(client.drbbdup-drwrap-test.dll drbbdup)
+use_DynamoRIO_extension(client.drbbdup-drwrap-test.dll drwrap)
+
 if (ARM)
   tobuild_ci(client.predicate-test client-interface/predicate-test.c "" "" "")
   use_DynamoRIO_extension(client.predicate-test.dll drmgr)
diff --git a/suite/tests/client-interface/drbbdup-drwrap-test.appdll.c b/suite/tests/client-interface/drbbdup-drwrap-test.appdll.c
new file mode 100644
index 00000000000..e6bc576396e
--- /dev/null
+++ b/suite/tests/client-interface/drbbdup-drwrap-test.appdll.c
@@ -0,0 +1,80 @@
+/* **********************************************************
+ * Copyright (c) 2011-2022 Google, Inc.  All rights reserved.
+ * **********************************************************/
+
+/*
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of Google, Inc. nor the names of its contributors may be
+ *   used to endorse or promote products derived from this software without
+ *   specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED. IN NO EVENT SHALL GOOGLE, INC. OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
+ * DAMAGE.
+ */
+
+/* Test wrapping functionality using a library w/ exported routines
+ * so they're easy for the client to locate.
+ */
+
+#include "tools.h"
+
+int EXPORT
+wrapme(int x)
+{
+    print("%s: arg %d\n", __FUNCTION__, x);
+    return x;
+}
+
+void
+run_tests(void)
+{
+    print("first wrapme returned %d\n", wrapme(2));
+    /* Signal the client to switch modes. */
+#ifdef WINDOWS
+    __nop();
+    __nop();
+    __nop();
+    __nop();
+#else
+    __asm__ __volatile__("nop; nop; nop; nop");
+#endif
+    print("second wrapme returned %d\n", wrapme(2));
+}
+
+#ifdef WINDOWS
+BOOL APIENTRY
+DllMain(HANDLE hModule, DWORD reason_for_call, LPVOID Reserved)
+{
+    switch (reason_for_call) {
+    case DLL_PROCESS_ATTACH: run_tests(); break;
+    case DLL_PROCESS_DETACH: break;
+    case DLL_THREAD_ATTACH: break;
+    case DLL_THREAD_DETACH: break;
+    }
+    return TRUE;
+}
+#else
+int __attribute__((constructor)) so_init(void)
+{
+    run_tests();
+    return 0;
+}
+#endif
diff --git a/suite/tests/client-interface/drbbdup-drwrap-test.c b/suite/tests/client-interface/drbbdup-drwrap-test.c
new file mode 100644
index 00000000000..73c12d4f0b8
--- /dev/null
+++ b/suite/tests/client-interface/drbbdup-drwrap-test.c
@@ -0,0 +1,75 @@
+/* **********************************************************
+ * Copyright (c) 2011-2022 Google, Inc.  All rights reserved.
+ * **********************************************************/
+
+/*
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of Google, Inc. nor the names of its contributors may be
+ *   used to endorse or promote products derived from this software without
+ *   specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED. IN NO EVENT SHALL GOOGLE, INC. OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
+ * DAMAGE.
+ */
+
+#include "tools.h"
+#ifdef UNIX
+#    include "dlfcn.h"
+#endif
+
+static void
+load_library(const char *path)
+{
+#ifdef WINDOWS
+    HANDLE lib = LoadLibrary(path);
+    if (lib == NULL) {
+        print("error loading library %s\n", path);
+    } else {
+        print("loaded library\n");
+        FreeLibrary(lib);
+    }
+#else
+    void *lib = dlopen(path, RTLD_LAZY | RTLD_LOCAL);
+    if (lib == NULL) {
+        print("error loading library %s: %s\n", path, dlerror());
+    } else {
+        print("loaded library\n");
+        dlclose(lib);
+    }
+#endif
+}
+
+int
+main(int argc, char *argv[])
+{
+#ifdef WINDOWS
+    load_library("client.drbbdup-drwrap-test.appdll.dll");
+#else
+    /* We don't have "." on LD_LIBRARY_PATH path so we take in abs path */
+    if (argc < 2) {
+        print("need to pass in lib path\n");
+        return 1;
+    }
+    load_library(argv[1]);
+#endif
+    print("thank you for testing the client interface\n");
+    return 0;
+}
diff --git a/suite/tests/client-interface/drbbdup-drwrap-test.dll.c b/suite/tests/client-interface/drbbdup-drwrap-test.dll.c
new file mode 100644
index 00000000000..6e1c68dd442
--- /dev/null
+++ b/suite/tests/client-interface/drbbdup-drwrap-test.dll.c
@@ -0,0 +1,179 @@
+/* **********************************************************
+ * Copyright (c) 2015-2022 Google, Inc.  All rights reserved.
+ * **********************************************************/
+
+/*
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions are met:
+ *
+ * * Redistributions of source code must retain the above copyright notice,
+ *   this list of conditions and the following disclaimer.
+ *
+ * * Redistributions in binary form must reproduce the above copyright notice,
+ *   this list of conditions and the following disclaimer in the documentation
+ *   and/or other materials provided with the distribution.
+ *
+ * * Neither the name of Google, Inc. nor the names of its contributors may be
+ *   used to endorse or promote products derived from this software without
+ *   specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
+ * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED. IN NO EVENT SHALL GOOGLE, INC. OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
+ * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
+ * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
+ * DAMAGE.
+ */
+
+/* Tests the drbbdup extension when combined with drrwap but using drwrap
+ * in only a subset of the cases.
+ */
+
+#include "dr_api.h"
+#include "drmgr.h"
+#include "drbbdup.h"
+#include "drwrap.h"
+#include "client_tools.h"
+#include <string.h>
+
+/* We assume the app is single-threaded for this test. */
+static uintptr_t case_encoding = 0;
+
+static uintptr_t
+set_up_bb_dups(void *drbbdup_ctx, void *drcontext, void *tag, instrlist_t *bb,
+               bool *enable_dups, bool *enable_dynamic_handling, void *user_data)
+{
+    drbbdup_status_t res;
+
+    CHECK(enable_dups != NULL, "should not be NULL");
+    CHECK(enable_dynamic_handling != NULL, "should not be NULL");
+
+    res = drbbdup_register_case_encoding(drbbdup_ctx, 1);
+    CHECK(res == DRBBDUP_SUCCESS, "failed to register case 1");
+
+    *enable_dups = true;
+    *enable_dynamic_handling = false; /* disable dynamic handling */
+    return 0;                         /* return default case */
+}
+
+static dr_emit_flags_t
+event_analyse_case(void *drcontext, void *tag, instrlist_t *bb, bool for_trace,
+                   bool translating, uintptr_t encoding, void *user_data,
+                   void *orig_analysis_data, void **case_analysis_data)
+{
+    if (encoding == 0) {
+        int consec_nop_count = 0;
+        for (instr_t *inst = instrlist_first_app(bb); inst != NULL;
+             inst = instr_get_next_app(inst)) {
+            if (instr_get_opcode(inst) == OP_nop)
+                ++consec_nop_count;
+            else {
+                if (consec_nop_count == 4) {
+                    /* Time to switch modes. */
+                    case_encoding = 1;
+                }
+                consec_nop_count = 0;
+            }
+        }
+        return DR_EMIT_DEFAULT;
+    }
+    if (encoding == 1) {
+        return drwrap_invoke_analysis(drcontext, tag, bb, for_trace, translating,
+                                      case_analysis_data);
+    }
+    return DR_EMIT_DEFAULT;
+}
+
+static dr_emit_flags_t
+event_instrument_instr(void *drcontext, void *tag, instrlist_t *bb, instr_t *instr,
+                       instr_t *where, bool for_trace, bool translating,
+                       uintptr_t encoding, void *user_data, void *orig_analysis_data,
+                       void *case_analysis_data)
+{
+    if (encoding == 1) {
+        return drwrap_invoke_insert(drcontext, tag, bb, instr, where, for_trace,
+                                    translating, case_analysis_data);
+    }
+    return DR_EMIT_DEFAULT;
+}
+
+static void
+wrap_pre(void *wrapcxt, OUT void **user_data)
+{
+    bool ok;
+    CHECK(wrapcxt != NULL && user_data != NULL, "invalid arg");
+    ok = drwrap_set_arg(wrapcxt, 0, (void *)42);
+    CHECK(ok, "set_arg error");
+}
+
+static void
+wrap_post(void *wrapcxt, void *user_data)
+{
+    /* Nothing yet. */
+}
+
+static void
+event_module_load(void *drcontext, const module_data_t *mod, bool loaded)
+{
+    if (strstr(dr_module_preferred_name(mod), "client.drbbdup-drwrap-test.appdll.") ==
+        NULL)
+        return;
+    app_pc target = (app_pc)dr_get_proc_address(mod->handle, "wrapme");
+    CHECK(target != NULL, "cannot find lib export");
+    bool res = drwrap_wrap(target, wrap_pre, wrap_post);
+    CHECK(res, "wrap failed");
+}
+
+static void
+event_exit(void)
+{
+    bool res = drmgr_unregister_module_load_event(event_module_load);
+    CHECK(res, "drmgr_unregister_event_module_load failed");
+    drwrap_exit();
+    drbbdup_status_t status = drbbdup_exit();
+    CHECK(status == DRBBDUP_SUCCESS, "drbbdup exit failed");
+    drmgr_exit();
+}
+
+DR_EXPORT void
+dr_init(client_id_t id)
+{
+    drmgr_init();
+
+    drbbdup_options_t opts = { 0 };
+    opts.struct_size = sizeof(drbbdup_options_t);
+    opts.set_up_bb_dups = set_up_bb_dups;
+    opts.analyze_case_ex = event_analyse_case;
+    opts.instrument_instr_ex = event_instrument_instr;
+    opts.runtime_case_opnd = OPND_CREATE_ABSMEM(&case_encoding, OPSZ_PTR);
+    opts.atomic_load_encoding = false;
+    opts.max_case_encoding = 1;
+    opts.non_default_case_limit = 1;
+
+    drbbdup_status_t status = drbbdup_init(&opts);
+    CHECK(status == DRBBDUP_SUCCESS, "drbbdup init failed");
+
+    dr_register_exit_event(event_exit);
+
+    /* Make sure requesting inversion fails *after* drwrap_init().
+     * This also stresses drwrap re-attach via init;exit;init.
+     */
+    bool res = drwrap_init();
+    CHECK(res, "drwrap_init failed");
+    res = drwrap_set_global_flags(DRWRAP_INVERT_CONTROL);
+    CHECK(!res, "DRWRAP_INVERT_CONTROL after drwrap_init should fail");
+    drwrap_exit();
+
+    res = drwrap_set_global_flags(DRWRAP_INVERT_CONTROL);
+    CHECK(res, "DRWRAP_INVERT_CONTROL failed");
+    res = drwrap_init();
+    CHECK(res, "drwrap_init failed");
+
+    res = drmgr_register_module_load_event(event_module_load);
+    CHECK(res, "drmgr_register_event_module_load failed");
+}
diff --git a/suite/tests/client-interface/drbbdup-drwrap-test.expect b/suite/tests/client-interface/drbbdup-drwrap-test.expect
new file mode 100644
index 00000000000..37d19d87f7d
--- /dev/null
+++ b/suite/tests/client-interface/drbbdup-drwrap-test.expect
@@ -0,0 +1,6 @@
+wrapme: arg 2
+first wrapme returned 2
+wrapme: arg 42
+second wrapme returned 42
+loaded library
+thank you for testing the client interface
