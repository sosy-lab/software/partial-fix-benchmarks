i#5352 perf regression: Point at 9.0.1 release (#5355)

We put out a 9.0.1 bugfix release for the #5352 performance regression
on Windows.  Here we update the direct download links.

Also updates the changelog, which includes a couple of minor features
added since 9.0.0.  We stick with 9.0.1 despite that for simplicity.
The only compatibility change affects just those building DR from
sources and so is less relevant for versioning.

Issue: #5352
i#5356: Add drwrap control inversion option

Adds a new global flag to drwrap which inverts control such that the
client must explicitly invoke drwrap's app2app and instrumentation
handlers.  This lets a client use drbbdup with drwrap only applying to
a subset of the cases.

Given missing pieces in drbbdup, only drwrap wrapping is supported
this way: not drwrap replacing.  (For full support, we would need to
add app2app event support inside drbbdup, as well as support for
returning other than DR_EMIT_DEFAULT for the various instrumentation
handlers.)

Adds a new test drbbdup-drwrap-test which wraps a function in only one
of its drbbdup cases.

The new test includes a check for drwrap_set_global_flags() being
called after drwrap_init() by having a
drwrap_init;drwrap_exit;drwrap_init sequence.  This revealed several
missing unregister calls in drwrap_exit, which we fix here.  These are
not assumed to affect full re-attach since drmgr resets its state
completely; they cause a use-after-free here because drwrap but not
drmgr was reset and we had two thread exit handlers.

Issue: #3995, #5356
Fixes #5356
i#5356 bbdup-wrap: Add extended drbbdup instrumentation callback

Adds a new field to the drbbdup options that points at an
instrumentation handler that takes in the "for_trace" and
"translating" parameters and returns dr_emit_flags_t.  Either the
original or this one must be set.

Adds a simple sanity check of the parameters.  Testing the return
value is difficult, unfortunately.

Issue: #5356, #3995, #4134
i#5356 bbdup-wrap: Add extended drbbdup instrumentation callbacks (#5361)

Adds new fields to the drbbdup options that point at a
case analysis handler and an instrumentation handler
that both take in the "for_trace" and "translating" parameters
and return dr_emit_flags_t.  For each, only one of the
original or the new extended version must be set.

Adds a simple sanity check of the parameters.  Testing the return
value is difficult, unfortunately.

Issue: #5356, #3995, #4134
i#5356 bbdup-wrap: Add extended drbbdup instrumentation callbacks (#5361)

Adds new fields to the drbbdup options that point at a
case analysis handler and an instrumentation handler
that both take in the "for_trace" and "translating" parameters
and return dr_emit_flags_t.  For each, only one of the
original or the new extended version must be set.

Adds a simple sanity check of the parameters.  Testing the return
value is difficult, unfortunately.

Issue: #5356, #3995, #4134
i#5356 bbdup-drwrap: Add drwrap control inversion option (#5357)

Adds a new global flag to drwrap which inverts control such that the
client must explicitly invoke drwrap's analysis and instrumentation
handlers.  This lets a client use drbbdup with drwrap only applying to
a subset of the cases.

Given missing pieces in drbbdup, only drwrap wrapping is supported
this way: not drwrap replacing.  (For full support, we would need to
add app2app event support inside drbbdup cases.)  When the inversion
flag is set, replacing explicitly fails.

Adds a new test drbbdup-drwrap-test which wraps a function in only one
of its drbbdup cases.

The new test includes a check for drwrap_set_global_flags() being
called after drwrap_init() by having a
drwrap_init;drwrap_exit;drwrap_init sequence.  This revealed several
missing unregister calls in drwrap_exit, which we fix here.  These are
not assumed to affect full re-attach since drmgr resets its state
completely; they cause a use-after-free here because drwrap but not
drmgr was reset and we had two thread exit handlers.

Issue: #3995, #5356
i#5356 bbdup-drwrap: Add drwrap cleanup-only support

To best handle wrapping functions with drwrap in only some drbbdup
instrumentation cases, drwrap provides a cleanup-only instrumentation
feature which is called in drbbdup cases without wrapping.  This
ensures that key cleanup like restoring replaced return addresses is
performed without worrying about sychronously clearing state in all
threads on a drbbdup case transition.

To avoid having 2 analysis functions on top of the now 2 insert
functions, removes the analysis code completely as it was empty.

Adds a test which mis-behaves without this feature.

Issue: #5356, #3995
i#5356 bbdup-drwrap: Add drwrap cleanup-only support (#5374)

To best handle wrapping functions with drwrap in only some drbbdup
instrumentation cases, drwrap now provides a cleanup-only instrumentation
feature which is called in drbbdup cases without wrapping.  This
ensures that key cleanup like restoring replaced return addresses is
performed without worrying about sychronously clearing state in all
threads on a drbbdup case transition.

To avoid having 2 analysis functions on top of the now 2 insert
functions, removes the analysis code completely as it was empty.

Adds a test which mis-behaves without this feature.

Issue: #5356, #3995
i#5356: Add drbbdup support for meta bbs (#5381)

Adds drbbdup support for basic blocks consisting solely of meta instructions. The hash-table manager no longer uses PCs of the first app instructions inside basic blocks as keys. Instead, tags of basic blocks are now used.

The PR also adds tests.

Issue: #5356
