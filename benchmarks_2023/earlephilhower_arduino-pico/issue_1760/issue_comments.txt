Starting with 3.3.0, hardware SPI crashes when invoked from different parts of the program
Sorry, but without an MCVE (minimum code to reproduce the failure) and more detail on the failure there's really nothing to do here.  Can you make a failing sample that works without anything attached (as I don't have any LCD displays lying around)?  And can you capture the failure mode, too?

Using a Picoprobe and then `gdb` to dump the stack trace when things hang (assuming they really hang) would help, even if you can't make a small test case.

FWIW, if you check the changelogs, then going from 3.2.2 to 3.3.0 had no SPI changes in the core whatsoever.  There was a Pico SDK 1.5.0 -> 1.5.1 minor update merged https://github.com/raspberrypi/pico-sdk/compare/2ccab115de0d42d31d6611cca19ef0cd0d2ccaa7...6a7db34ff63345a7badec79ebea3aaef1712f374

Only thing there related to SPI is https://github.com/raspberrypi/pico-sdk/commit/0121007c853a36cebcf78e591437d6c79c964d4b which doesn't seem offensive.
Also, if you're saying that things run but the SPI is not working to communicate to your device, capturing the SPI bus in enough detail to see what's going on would be helpful, too.  I.e. is it in the wrong format, is the clock frequency off, etc.?
Thanks for the quick response and the suggestions for further investigation.  I've  changed the board package between 3.2.2 and 3.3.0 several times, with always the same result.  Since U8g2 is a quite large and complex library,  building a minimal code to reproduce the error will take a while.  The typical sequence of the program is : 
 - initialization of SPI and the display hardware by U8g2
 - output of graphical data to the display by the library, which are displayed correctly
 - initiating another SPI transfer from a separate subroutine , commands and data are 
   transferred correctly 
 - SPI transfer ends 
 - the next time the library sends data , it is displayed correctly when the board package 3.2.2 is installed. Otherwise, no data is recognized by the display . It seems though, that the program keeps running, I will try to verify that.  


When you do your own SPI transaction, what is the exact sequence of commands you're doing?  Do you change the clock, phase, polarity, etc?  

Looking at the Pico SDK there may be some issues if things like the polarity or phase are changed between SPI transactions.  https://github.com/raspberrypi/pico-sdk/issues/868
Another idea, just to isolate it to the SDK, would be to use the `libpico.a` file from 3.2.2 and copy it over the one installed for `3.3.0`.  If things pass w/core 3.3.0 + libpico 3.2.2, then it's definitely related to the SDK in some way.  
Thanks for the suggestion, I'll try that.  After reviewing the changes to the SPI module in the SDK, that seems to be a hot lead. 
 The separate SPI transfer is done with the same parameters as the one from the library , otherwise the display wouldn't work ( with 3.2.2). The commands are byte sequences to initiate i.e. scrolling of the display, they work as supposed.   
One other question, do you have a real CS, or is it tied high all the time?  They disable/change settings/re-enable the SPI hardware with the patch and maybe that gives a blip on the clock pin causing everything afterward to get corrupted since the receiving HW can't ever recover sync (no CS deassertion)?
And one more thing, can you enable SPI debugging and capture the logs on failure?  `Tools->Debug Port->Serial` then `Tools->Debug Level->SPI`.  If the failure happens right after reset the USB port may not yet be up so you may need to add a `delay(5000);` to the beginning of your `setup()` to give it time.
I've  pasted the libpico.a from 3.2.2 onto  the 3.3.0 package, without success.  :(  I'll try your other suggestions. 
> I've pasted the libpico.a from 3.2.2 onto the 3.3.0 package, without success. :( I'll try your other suggestions.

Argh, I see that there are lots of inline functions in the Pico SDK's SPI header so this makes sense...the functions aren't actually built into the libpico.a.  The debug traces should be interesting, at least!
I've switched  the SPI debug on and found, that the separate SPI transfers used different modes. I corrected that, with no visible difference in both the 3.2.2 and 3.3.0 environment. So this seems not to be the cause of the problem. From the other debug output, I couldn't recognize a difference - the system seems to shovel data into the SPI module, but the display controller seems to not recognize that as valid. I also checked the SPI clock with an oscilloscope and found it to be 4 MHz in both cases.  
Is there an easy way to monitor the SPI control registers in runtime ? 
The CS is active, the display only recognises incoming commands after a change of CS from H to L .  On the scope, this signal also looks the same in both cases.  So , up to now, still a mystery . At least, the program works in the 3.2.2 environment.  :)
I still think it's related to the SDK SPI change, so would you be able to run `git master` and try the change in #1762?

You could also install 3.6.0 and manually copy the `SPI.cpp` file diffs.  This avoids the call to `spi_set_format` which was changed in 1.5.1 SDK to do the SPI disable/re-enable.
I installed 3.6.0 and manually changed the SPI.cpp  , according to your suggestion. Everything is running fine now. Thank you very much for your help and your  dedication to this very useful project. Wish you all the best, Georg
After another review of the checks done, the most probable cause of the problem is an unintentional change of the bit sequence of the transferred data. The Futaba display requires the serial data to be transmitted LSB first. The hardware SPI module of the RP2040 only supports MSB first transfers. The bit sequence reversal is done by software in the SDK, if needed. The spi_set_format  function of the 1.5.1 SDK  seems to interfere with that.  Bytes arriving  MSB first will of course not be recognized by the display controller. 
Sorry, but now I'm just confused.  It was working with the change and now it's not?  What changed again?

There is NO LSB first support in the SDK.  See https://www.raspberrypi.com/documentation/pico-sdk/hardware.html#rpip5aa22e7dbad796bb41ce .  The SPI class handles it in software.  If you're using the raw SDK spi-* functions then your code needs to do the bit reversal.  If you're using the SPI class, it's taken care of in the transfer methods.
With v 3.6.0, and the modified SPI.cpp , there is no problem anymore.   My speculations about the bit sequence could be wrong of course, but this would be consistent with  the other observations.  Guess there are more ways to corrupt a SPI setup though ...  Thanks again