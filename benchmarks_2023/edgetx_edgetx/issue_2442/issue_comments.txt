Portrait screen - spacing and update tweaks
The channel monitor is also not centered, and there are big gaps. Maybe a one column version would be better.
![image](https://user-images.githubusercontent.com/36520141/194406263-8bb847b5-f752-45f8-ba38-a7fb6af2046f.png)

I actually think it would be better to increase the number of channels shown to 16, and reduce the number of tabs, but sure will look at single column as would be easier to implement. 
> The channel monitor is also not centered, and there are big gaps. Maybe a one column version would be better. 

Just looking at this - the off-centering is partly deliberate - there are icons shown on the left side for reversing and channel override... it looks like they got pushed too far to the left though at some point though 😮 

![image](https://user-images.githubusercontent.com/5500713/194753427-d4405f56-f47d-494e-87e7-b93015dd46ba.png)

Check out https://github.com/EdgeTX/edgetx/pull/2441#issuecomment-1272523991 to see what it looks like now.

> there are icons shown on the left side for reversing and channel override...

These icons really don't look good. Maybe we could just use another color for "override" channels? (`WARNING` red?).
For the channel reverse, frankly, I don't think you need anything: you just see that it moves the opposite way...
Yeah, there are probably better ways to do it. The icons *are* aligned now though... I couldn't leave them where they were since I was fiddling around there. 😆 
The file listing did not make use of full screen.

![IMG 019](https://user-images.githubusercontent.com/18414861/196338367-a2bf95d5-8925-47d4-9011-0071f88b858c.png)

The area in the bottom is used to display model images, if you go into MODELS folder. This is similar to the empty right side for horizontal screen.
Button alignment was fixed in a 2.9 PR