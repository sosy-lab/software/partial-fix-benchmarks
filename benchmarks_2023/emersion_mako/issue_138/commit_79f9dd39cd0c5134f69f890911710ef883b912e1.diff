diff --git a/config.c b/config.c
index e9679491..d35d7a57 100644
--- a/config.c
+++ b/config.c
@@ -508,6 +508,13 @@ static bool apply_style_option(struct mako_style *style, const char *name,
 	return false;
 }
 
+bool apply_global_option(struct mako_config *config, const char *name,
+		const char *value) {
+	struct mako_criteria *global = global_criteria(config);
+	return apply_style_option(&global->style, name, value) ||
+		apply_config_option(config, name, value);
+}
+
 static bool file_exists(const char *path) {
 	return path && access(path, R_OK) != -1;
 }
@@ -676,9 +683,6 @@ int parse_config_arguments(struct mako_config *config, int argc, char **argv) {
 		{0},
 	};
 
-	struct mako_criteria *root_criteria =
-		wl_container_of(config->criteria.next, root_criteria, link);
-
 	optind = 1;
 	char *config_arg = NULL;
 	while (1) {
@@ -713,8 +717,7 @@ int parse_config_arguments(struct mako_config *config, int argc, char **argv) {
 		}
 
 		const char *name = long_options[option_index].name;
-		if (!apply_style_option(&root_criteria->style, name, optarg)
-				&& !apply_config_option(config, name, optarg)) {
+		if (!apply_global_option(config, name, optarg)) {
 			fprintf(stderr, "Failed to parse option '%s'\n", name);
 			return -1;
 		}
diff --git a/dbus/mako.c b/dbus/mako.c
index bf649954..226e3657 100644
--- a/dbus/mako.c
+++ b/dbus/mako.c
@@ -193,17 +193,7 @@ static int handle_list_notifications(sd_bus_message *msg, void *data,
 	return 0;
 }
 
-static int handle_reload(sd_bus_message *msg, void *data,
-		sd_bus_error *ret_error) {
-	struct mako_state *state = data;
-
-	if (reload_config(&state->config, state->argc, state->argv) != 0) {
-		sd_bus_error_set_const(
-				ret_error, "fr.emersion.Mako.InvalidConfig",
-				"Unable to parse configuration file");
-		return -1;
-	}
-
+static void reapply_config(struct mako_state *state) {
 	struct mako_notification *notif;
 	wl_list_for_each(notif, &state->notifications, link) {
 		// Reset the notifications' grouped state so that if criteria have been
@@ -226,6 +216,41 @@ static int handle_reload(sd_bus_message *msg, void *data,
 	}
 
 	set_dirty(state);
+}
+
+static int handle_reload(sd_bus_message *msg, void *data,
+		sd_bus_error *ret_error) {
+	struct mako_state *state = data;
+
+	if (reload_config(&state->config, state->argc, state->argv) != 0) {
+		sd_bus_error_set_const(
+				ret_error, "fr.emersion.Mako.InvalidConfig",
+				"Unable to parse configuration file");
+		return -1;
+	}
+
+	reapply_config(state);
+
+	return sd_bus_reply_method_return(msg, "");
+}
+
+static int handle_set_config_option(sd_bus_message *msg, void *data,
+		sd_bus_error *ret_error) {
+	struct mako_state *state = data;
+
+	const char *name = NULL, *value = NULL;
+	int ret = sd_bus_message_read(msg, "ss", &name, &value);
+	if (ret < 0) {
+		return ret;
+	}
+
+	if (!apply_global_option(&state->config, name, value)) {
+		sd_bus_error_set_const(ret_error, "fr.emersion.Mako.InvalidConfig",
+			"Failed to apply configuration option");
+		return -1;
+	}
+
+	reapply_config(state);
 
 	return sd_bus_reply_method_return(msg, "");
 }
@@ -237,6 +262,7 @@ static const sd_bus_vtable service_vtable[] = {
 	SD_BUS_METHOD("InvokeAction", "us", "", handle_invoke_action, SD_BUS_VTABLE_UNPRIVILEGED),
 	SD_BUS_METHOD("ListNotifications", "", "aa{sv}", handle_list_notifications, SD_BUS_VTABLE_UNPRIVILEGED),
 	SD_BUS_METHOD("Reload", "", "", handle_reload, SD_BUS_VTABLE_UNPRIVILEGED),
+	SD_BUS_METHOD("SetConfigOption", "ss", "", handle_set_config_option, SD_BUS_VTABLE_UNPRIVILEGED),
 	SD_BUS_VTABLE_END
 };
 
diff --git a/include/config.h b/include/config.h
index a3abc9c4..7f95a51a 100644
--- a/include/config.h
+++ b/include/config.h
@@ -100,5 +100,7 @@ bool apply_superset_style(
 int parse_config_arguments(struct mako_config *config, int argc, char **argv);
 int load_config_file(struct mako_config *config, char *config_arg);
 int reload_config(struct mako_config *config, int argc, char **argv);
+bool apply_global_option(struct mako_config *config, const char *name,
+	const char *value);
 
 #endif
diff --git a/makoctl b/makoctl
index c5956ea3..aa3120d8 100755
--- a/makoctl
+++ b/makoctl
@@ -12,12 +12,13 @@ usage() {
 	echo "                           notification action to be invoked"
 	echo "  list                     List notifications"
 	echo "  reload                   Reload the configuration file"
+	echo "  set <key>=<value>        Set a global configuration option"
 	echo "  help                     Show this help"
 }
 
 call() {
 	busctl -j --user call org.freedesktop.Notifications /fr/emersion/Mako \
-		fr.emersion.Mako "$@"
+		fr.emersion.Mako -- "$@"
 }
 
 if [ $# -eq 0 ] || [ $# -gt 5 ]; then
@@ -81,6 +82,19 @@ case "$1" in
 "reload")
 	call Reload
 	;;
+"set")
+	if [ $# -lt 2 ]; then
+		echo >&2 "makoctl: missing argument for 'set'"
+		exit 1
+	fi
+	name="${2%%'='*}"
+	value="${2#*'='}"
+	if [ "$2" = "$name" ]; then
+		echo >&2 "makoctl: missing '=' in argument for 'set'"
+		exit 1
+	fi
+	call SetConfigOption "ss" "$name" "$value"
+	;;
 "help"|"--help"|"-h")
 	usage
 	;;
diff --git a/makoctl.1.scd b/makoctl.1.scd
index f6a4baef..1d2d2fb9 100644
--- a/makoctl.1.scd
+++ b/makoctl.1.scd
@@ -47,6 +47,11 @@ Sends IPC commands to the running mako daemon via dbus.
 *reload*
 	Reloads the configuration file.
 
+*set* <key>=<value>
+	Set a global configuration option.
+
+	Reloading the config will discard options set like this.
+
 *help, -h, --help*
 	Show help message and quit.
 
