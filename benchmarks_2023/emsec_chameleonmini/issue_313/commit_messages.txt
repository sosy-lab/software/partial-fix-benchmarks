A few changes to logging timing for testing
Stashing code that correctly goes through the nfc-anticol utility ; Still need to handle #313
Stashing code that correctly makes it through to complete RATS ; Now need to work on #313
First order approximation to solving issue #313 ; Needs testing
Progress towards #313 ... Needs further testing
Fixing issue with trailing CRCA missing from #313
AuthLegacy(0x0A) works using 3DES enc/dec modes -- Makefile no testing -- For #313
Bug fixes to observations in #313
Fixing bug where we must first auth with the legacy command mentioned in #313
Fixing legacy auth algorithm from 3K3DES -> 2K3DES (cf. #313)
Addressing the ACK/NAK keep-alive exchanges from some NXP readers noted by \@lvandenb in #313
Adding in preliminary AES transfer functions -- Trying to save space for more where it can be pruned -- III -- cf. #313
*Should* finally be working now -- Problem was that the SETTINGS memory remapped the FLASH memory space used by avr-gcc\'s __flash to a custom .flashdata section after the LUFA build by a second call to avr-objcopy -- Clever way to maximize slot space, but the previous code was not aware of that causing apparently quasi-non-deterministic behavior with some readers -- Cf. #313 and #315
Working ISO authentication on the PM3 : cf. #313
Incorporating bug fixes by @colinoflynn at https://github.com/colinoflynn/ChameleonMini/commits/desfire-fixes (see message in #313)
Fix Makefile bugs noticed in #313
Attempt to fix bug with `pm3 --> hf mfdes info` reported in #313
Updates to make/build scripts ; Partial fixes to @colinoflynn\'s notes in #313 (success with \'hf mfdes info -- Still debugging auth exchanges)
Finalizing the fixes to #313 to verify PM3 support
Fixing Linux build errors noticed by @colinoflynn in \#313
ACR122U USB/PCSC reader stopped being responsive after all of the PM3 compat changes: Attempt to fix this problem (PM3 command support verified as still working) -- Cf. #313
Squashed commit of the following:

commit 9fbb7fbbf0b029320798a841d61148f927cf230b
Author: Fabian <fabian@kasper-oswald.de>
Date:   Wed Sep 21 11:06:58 2022 +0200

    fix #325

    * remove MemoryClear from ConfigurationSetById

commit 62e2f71e3345c217ddd0b1a3e04128626a591f8c
Merge: f5c1347 d5d36fe
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Mon Aug 15 10:45:10 2022 +0200

    Merge pull request #323 from maxieds/ExternalUSBReadersCompat

    DESfire emulation support: Updated support for PM3 and better compatibility with external USB readers

commit d5d36fe61d2d36699a724c043a7fe60be5c868d4
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Wed Aug 3 18:03:28 2022 -0500

    Update BuilingFirmwareBinariesFromSource.md

commit 46a3cc8f8dabe42ab798df58b5d61022ea27f8e7
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jul 26 01:40:27 2022 -0400

    Small changes to the NAK/ACK return size (4 bits versus 1 byte)

commit eb0d5fd8a771d76a0569e8eb6cada164e5e8331f
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Tue Jul 26 00:02:15 2022 -0400

    Update DESFireSupportReadme.md

commit 3a91394f12280c0ee80373502182f6bef3e12379
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 20:39:50 2022 -0400

    One more small change to resetting the ATQA value automatically depending on whether the UID is known to be randomly generated

commit 2257c4226272f9936b549129bc6b7e9c994bfe3c
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 20:31:29 2022 -0400

    Cleaning up modifications used to test the development code

commit cddb9bdbf8b84be1efab2192c19159639b8368b2
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 19:41:50 2022 -0400

    Update DESFireSupportReadme.md

commit 63e74f99149e77e11f79818ae08d5b4f732adfeb
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 19:42:29 2022 -0400

    Changes to ISO14443A-4 handlers confirmed to work with the ACS ACR-122U external USB reader ; Updated docs and source code

commit 32277f06e85d2279a0a8d4fe006e8e7c2c33c688
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 19:15:46 2022 -0400

    Update DESFireSupportReadme.md

commit f9e9018199de471316eef1236b096f22ee92896d
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 18:42:50 2022 -0400

    Update DESFireSupportReadme.md

commit 8fe0e66867ca615d33e682380c41fa022fe40635
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Mon Jul 25 18:40:06 2022 -0400

    Update DESFireSupportReadme.md

commit 15be871bb2339f1e6ce43023bace3f6286c5a1a5
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Jul 23 00:16:22 2022 -0400

    Restore point for changes to the CL1/CL2 exchanges in the anticollision for DF ISO14443A-4 support

commit 2a42b31b49f538be581cc49ce9ff08b25712d096
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Sat Jul 23 00:12:24 2022 -0400

    Update DESFireSupportReadme.md

commit f5c13475748e3c6574e328c344775f2af7ea7ff7
Author: Fabian <fabian@kasper-oswald.de>
Date:   Wed Jul 20 13:42:28 2022 +0200

    fix github action name

commit 78a108bbedba5fba4096f050640703e84329f763
Author: Fabian <fabian@kasper-oswald.de>
Date:   Wed Jul 20 13:32:06 2022 +0200

    change automated builds

    * make all builds all ISO14443A configs
    * all ISO15693 configs are automatically build with make iso15693

commit 66c2e8bab8434cf646e5b95bb7d2cdc1196e7cc1
Merge: 3c01a6c aa20be6
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Wed Jul 20 13:08:23 2022 +0200

    Merge pull request #322 from maxieds/DESFireNFCExternalUSBReaderPatches-LibNFCTestCode

    DESFire emulation support: Bug fixes, improvements and updated LibNFC test code

commit aa20be65665d863fbcf670b67cb80cda7cf39e13
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jul 20 06:14:27 2022 -0400

    Current firmware builds tested with PM3 and LibNFC test code ; Still takes too long to verify the file mgmt and data manip programs

commit ffb6683c08b162a8fc94dfdbbd7f7df506165679
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Wed Jul 20 05:59:02 2022 -0400

    Update DESFireSupportReadme.md

commit b79d96476a342098a1e50009487410fe54b22a9f
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Wed Jul 20 03:25:21 2022 -0400

    Update DESFireSupportReadme.md

commit 5cd6773ae58656ab926b9308dbfce8570a00b2f5
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jul 20 03:00:17 2022 -0400

    Restore point for many incremental updates, bug fixes and documentation changes

commit 126189aa0396fd1d2d529f1fd3f780b748611e71
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jul 19 22:00:24 2022 -0400

    Verified ISODES and legacy DES auth schemes work ; AES-128 auth support is verified with the PM3

commit a050d049f4cfebaca08e164546c4754ca3bd8a21
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jul 19 20:54:09 2022 -0400

    Multiple code cleanup changes to TransferState -- Enc of transfers is handled by the APDU pre/post process functions -- Restore point for previous functionality

commit 871451aeda7584da78a1f0a15739233322be2234
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Tue Jul 19 19:14:45 2022 -0400

    Update DESFireSupportReadme.md

commit 512eff38b3fd402d7bbd57d82e133d4d272c038d
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jul 19 18:53:18 2022 -0400

    Fixing PM3 AES-128 authentication bug

commit 3d86fe4c75e766db797d505a98da70a806dc0d2c
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jul 19 17:46:08 2022 -0400

    Updating TDEA (x3) crypto code to handle uneven buffer sizes ; Changes to DESFire auth instructions reinit / (in)validate state logic

commit 5c894b8463a9541721fb3e3998e61e9fcab2c342
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jul 19 15:24:27 2022 -0400

    Updating the AES128 enc/dec code to support ECB mode (default) and handle uneven buffer sizes that are not a multiple of 16

commit ccdc36ece17394913bbba608eb63ecad284f7b5d
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jul 17 16:54:43 2022 -0400

    Removed old AES128 support with AVR libs in place of openssl/EVP ; Bug fix for ISODES auth in the firmware to keep session IV state with multiple auths

commit 3078e7f373cf4fa6f790f81fc3d3f3699c965977
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jul 17 14:23:02 2022 -0400

    Stashing working LibNFC test code -- ISO auth is working

commit e7790dceede8c292e761a813d167540ae2e07542
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jul 17 06:56:28 2022 -0400

    Updates to LibNFC test code (ISO auth works) ; Untested changes to fw source to support recall of header data from EEPROM on power cycle (need to test) ; Other misc minor modifications to stash as a restore point

commit 1c2cf3a414e290b247f294db38bd83c1d8a90166
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Sun Jul 17 02:09:53 2022 -0400

    Update DESFireSupportReadme.md

commit 306865e367e44abda00824b189ae0ccb015c71ab
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jul 17 01:55:21 2022 -0400

    New DF_ENCMODE command to set ECB/CBC crypto modes ; Incremental changes to LibNFC test code ; Incomplete docs to edit elsewhere

commit 3a89c4baaaee926e383250f11b44e0251a030764
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Sat Jul 16 20:57:25 2022 -0400

    Update BuilingFirmwareBinariesFromSource.md

commit 197b958636a8ab8c80eb43b9d9ed3ab8e1a10431
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Sat Jul 16 20:55:20 2022 -0400

    Update and rename BuilingFromSource.md to BuilingFirmwareBinariesFromSource.md

commit 5de0aae751a43318558cf5abe64a4f099dffc013
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Sat Jul 16 20:54:59 2022 -0400

    New misc-tags target to BuildScripts/custom_build_targets.mk

commit e07823ef735578bcb0e9955f74eb60320d390262
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Jul 16 20:39:06 2022 -0400

    Stashing in progress changes to the DESFire LibNFC test code ; Adding incomplete documentation for custom build targets

commit 3d19776ca5dad8226cd0c79e680a402c8c737fba
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Sat Jul 16 20:27:39 2022 -0400

    Update DESFireSupportReadme.md

commit 4cfea3deeb2fda39da11d80ab3029b84124304eb
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Jul 15 19:10:30 2022 -0400

    Saving work on the LibNFC testing code for DESFire builds

commit d297a0e4fc5d3b00ec20aa3c46cf5145293efc31
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Jul 15 11:04:24 2022 -0400

    Restoring full log buffer space to the default (non dev) desfire target

commit bfac98020d009667e03ce8cd1afef0b49e73ccd9
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Jul 15 10:20:39 2022 -0400

    Testing code updates with the PM3 -- a few small changes

commit 53de26e3f7a1a6692cbba22533197892f044c154
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Jul 15 08:50:10 2022 -0400

    Tidying up code, build script fixes for no bc command, preliminary attempts to get CONFIG=MF_DESFIRE compatibility with extrernal USB readers using 'pcsc_spy -v'

commit 3c01a6caa06aa7472fb17a483eef9390c1e71c5c
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Fri Jul 8 13:09:53 2022 +0200

    Update firmware-desfire-push.yml

commit bc333f9dd1df72d1becfc1186acfac9dc1c8f094
Merge: 99dceff 37be68d
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Fri Jul 8 12:24:22 2022 +0200

    Merge pull request #319 from maxieds/DESFire-AuthISO-Patch

    DESFire emulation support: Bug, stability and reliability fixes and PM3 compatible ISO authentication

commit 37be68d2bfab6e6cfeb9d081d303cae9170cefff
Merge: bc8057b 05eeda6
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Jul 1 08:44:33 2022 -0400

    Merge branch 'DESFire-AuthISO-Patch' of https://github.com/maxieds/ChameleonMini into DESFire-AuthISO-Patch

commit bc8057bfe7c5f1972c94204c3bfaadaf444006da
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Jul 1 08:44:21 2022 -0400

    Several fixes to responsiveness and frozen behavior noted in PR #319

commit 93e77a27cdbac96b34a84a3fae0049f73d797f67
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Thu Jun 30 22:42:22 2022 -0400

    Stashing changes to DESFire code -- No PSTR wrappers on command names as this doesn't change the ELF application size

commit 807d4ac37e83fa24c4678f2828ea688e5eaa7303
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Thu Jun 30 22:24:07 2022 -0400

    Stashing DESFire updates before tinkering with saving space with PSTR wrappers around the terminal command names

commit 05eeda659d0f140287df9311e76e1f2e5289b39e
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Wed Jun 29 13:12:55 2022 +0200

    Update Log.py

    change DESFire Generic Error decoder

commit 99dceff96e22176322651a69e9b2a4fb13779664
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Wed Jun 29 11:50:28 2022 +0200

    Update ISO15693-A.c

    add missing VICINITY support statement

commit cda26ee2c7430f1a48e1a66809c0aea22d31923a
Merge: c3e1dab b6f4094
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 29 05:44:30 2022 -0400

    Merge branch 'DESFire-AuthISO-Patch' of https://github.com/maxieds/ChameleonMini into DESFire-AuthISO-Patch

commit c3e1dab8f6c2bd0dc4a2a1c0a0809ef5b4523185
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 29 05:44:22 2022 -0400

    Updating the FLASH_DATA_ADDR value to the correct value for 6 slots (versus standard 8)

commit b6f40946c59fa9695327ea39c8de268db070eaef
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Wed Jun 29 11:36:35 2022 +0200

    add missing sources for ISO15693_SNIFF

commit 3d2a8a67330dcaa1c98205561a08a86ad7e1f628
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Wed Jun 29 11:24:39 2022 +0200

    add missing EM4233 statement

commit 40da00ae69f3b77327e3c4aa61c194fdccf8f2b0
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Wed Jun 29 11:12:29 2022 +0200

    fix typos

commit 688b4942b4452b5626fc50f98ea8010bb9c93ff9
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 29 04:50:39 2022 -0400

    More typ corrections and build script bug fixes annotated in #319

commit 7f37bfb43719043d230b49d2185a921a7f086fe0
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jun 28 11:59:03 2022 -0400

    Updating source after the feedback from @fptrs in PR #319

commit b378b4e3318f43ec8196475843a9d355dac48788
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Wed Jun 22 21:00:18 2022 -0400

    Delete DESFire_example.contents

    This image is out of date -- Created using much older firmware sources. It eventually would be nice to replace it with a better dump using PR #319 sources (or more recent).

commit 3806bcf05a2001732f73effcabbc543d98f78714
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Wed Jun 22 21:00:08 2022 -0400

    Delete DESFire_example.dmp

    This image is out of date -- Created using much older firmware sources. It eventually would be nice to replace it with a better dump using PR #319 sources (or more recent).

commit d29f08793cf55c3a20a1252d0cc253658775d335
Merge: 149599e 0291b9b
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Thu Jun 16 20:17:00 2022 -0400

    Merge branch 'master' into DESFire-AuthISO-Patch

commit 149599e2ac38d7caa26055ffdee1920beda5b3ca
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Thu Jun 16 02:32:57 2022 -0400

    Cleaning up some old commented out macro definitions

commit bbc489ee10425c0d0110ff8cf51f3cc6c1796042
Author: Maxie D. Schmidt <maxieds@gmail.com>
Date:   Thu Jun 16 02:31:32 2022 -0400

    Typo corrected

commit 39de8cc51c4d5b949166749fac9ec839a9ac4967
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 15 18:54:58 2022 -0400

    ACR122U USB/PCSC reader stopped being responsive after all of the PM3 compat changes: Attempt to fix this problem (PM3 command support verified as still working) -- Cf. #313

commit 1ce7b55ac18b357d04c8920c7dc0872a7233c163
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 15 12:20:25 2022 -0400

    Fixing the terminal (echo) printing on MacOS following the last commit for Linux users

commit 68a12e673c362b8b153db39658bba77b0ef88511
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 15 11:42:43 2022 -0400

    Fixing Linux build errors noticed by @colinoflynn in \#313

commit 7b4cdd86d137d82fd9a5b235dd166fdceff13649
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Jun 7 21:56:24 2022 -0400

    Fixing some make build script bugs

commit 024c699df66c90bea4160449c45d692a86f386e3
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Jun 4 16:13:01 2022 -0400

    Finalizing the fixes to #313 to verify PM3 support

commit f9c1dabf3c8d3bda98f6127d7cf43daa5b1a4f50
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Thu Jun 2 00:40:53 2022 -0400

    Updates to make/build scripts ; Partial fixes to @colinoflynn\'s notes in #313 (success with \'hf mfdes info -- Still debugging auth exchanges)

commit 8e77d7877a0fb9913473f27167de17f6e2b57555
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 1 09:04:53 2022 -0400

    More refinements and efforts to make build output cleaner to read

commit 8a823c80c60fb3e217b0e019f3c77b9416d35bf9
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Jun 1 00:39:24 2022 -0400

    A few improvements to Makefile and build scripts (cf. previous issue #283)

commit 8e25538d83bf5119a1b8f62e7c65e55506b06fc9
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri May 13 08:27:42 2022 -0400

    Attempt to fix bug with `pm3 --> hf mfdes info` reported in #313

commit 83ce182cb8f748ff9dd774b11e7356fd3c98ad6d
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed May 11 00:43:28 2022 -0400

    Fix Makefile bugs noticed in #313

commit 453678907cab17ade7037480bc5e1f93f9ff0bef
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Apr 22 13:43:40 2022 -0400

    Incorporating bug fixes by @colinoflynn at https://github.com/colinoflynn/ChameleonMini/commits/desfire-fixes (see message in #313)

commit 19e0d1cda43c8da7e6274ecd3dcae47cbfee31ba
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Mar 30 12:46:06 2022 -0400

    Stashing LibNFC external USB reader test code online to test on Linux (builds on MacOS)

commit f4faaa75cb744a2af0ea73ee34042f2f8a270748
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Mar 30 10:00:26 2022 -0400

    Working ISO authentication on the PM3 : cf. #313

commit 45a6c3fb025187644a0b8f09e8c8b0aa73deee77
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Mar 29 23:30:46 2022 -0400

    Stashing more working code to test PM3 compatibility (ISO/EV1 auth)

commit 7ecf3cb18cdaceadfbb1bdecb68e1eee346bc47d
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Mar 26 18:25:54 2022 -0400

    Stuck on debugging issue with PM3 -- Posted to discord -- awaiting fix before this can get fixed

commit fecf5cbea23ac9646ffa7dc8ec96a6c7950ec2df
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Mar 26 10:44:07 2022 -0400

    More progress towards PM3 compatible auth II

commit b8732616d0958e92bd5cb3f62bec9718dc93024c
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Mar 26 10:23:55 2022 -0400

    More progress towards PM3 compatible auth

commit bccf5792d140597ca2f994f3c16256b8484d5242
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Mar 22 06:36:24 2022 -0400

    Proposed PM3 ISO auth compliant fwmod -- pending testing

commit 1be76882044528e4a9d5e53234c94713521e92bf
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Mar 21 21:05:16 2022 -0400

    Adding more complete support for PM3 ISO auth (stashing incremental changes as reference point -- II)

commit bb7d46f208082b1502296971595e09682bcf5bcc
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Mar 21 18:34:11 2022 -0400

    Adding more complete support for PM3 ISO auth (stashing incremental changes as reference point)

commit 0291b9bd1b3759db897db98d9b3fae8cc897e922
Author: Fabian <fabian@kasper-oswald.de>
Date:   Thu Mar 17 12:04:40 2022 +0100

    update Doxygen

commit aafc5ab2e24a484eecb80df4459ba9c2b16346a6
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Wed Mar 16 03:03:03 2022 -0400

    Stashing more incremental updates to the code to get/verify PM3 compatibility

commit 64d1fff60c8237363c37193b5f329fc4023eb8ac
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Tue Mar 15 17:48:29 2022 -0400

    Stashing incremental changes from Arch for compilation on Mac

commit 371eacafb6c3ff63b565e6f0b076ca45cfb654b4
Merge: 94aeb3c 14537ac
Author: fptrs <48245105+fptrs@users.noreply.github.com>
Date:   Tue Mar 1 15:31:32 2022 +0100

    Merge pull request #318 from cacke-r/master

    AUTOCALIBRATE command for Sniff-ISO15693 application

commit 14537ac0ce81ee8df99f93469033f56c8d2dbee1
Merge: a212bbb c47974e
Author: cacke-r <96484810+cacke-r@users.noreply.github.com>
Date:   Thu Feb 24 15:40:22 2022 +0100

    Merge pull request #2 from cacke-r/fix_autothreshold

    Sniff15693: Autocalibrate: Detect error case

commit c47974e44d5f7f6c4d3ab2672c4f20acf5f9d722
Author: cacke-r <cresch@gmx.de>
Date:   Wed Feb 23 21:03:17 2022 +0100

    Sniff15693: Autocalibrate: Detect error case

    Detect the case, that we never receive a valid card frame.
    Restore original/recent threshold in this case.

    Signed-off-by: cacke-r <cresch@gmx.de>

commit a212bbb313e89662c93dceea6080899c7b58973b
Author: cacke-r <cresch@gmx.de>
Date:   Tue Feb 15 20:56:36 2022 +0100

    Chamtool: Add Autothreshold command

    Signed-off-by: cacke-r <cresch@gmx.de>

commit c68604002dbc9eea2af99fddc4e413d1706cabd4
Author: cacke-r <cresch@gmx.de>
Date:   Tue Feb 15 20:15:15 2022 +0100

    Commands: Autocalibrate: Dont execute if ISO15693 codec uses autothreshold

    Signed-off-by: cacke-r <cresch@gmx.de>

commit b8fec22458281a4395b0df505b5689dbaa4d2987
Author: cacke-r <cresch@gmx.de>
Date:   Mon Feb 14 21:01:46 2022 +0100

    Terminal: Add commands to control Sniff15693 autothreshold feature

    Signed-off-by: cacke-r <cresch@gmx.de>

commit b04351b1afa002166cdba3c8f51e76f3b866e247
Author: cacke-r <cresch@gmx.de>
Date:   Wed Feb 9 21:46:30 2022 +0100

    Sniff15693: Add CRC Check on received data

    Signed-off-by: cacke-r <cresch@gmx.de>

commit ad1643482a0c478ee8415927721607d963a585e7
Author: cacke-r <cresch@gmx.de>
Date:   Tue Feb 15 20:07:25 2022 +0100

    Sniff15693: Implementation of Autocalibration command

    Signed-off-by: cacke-r <cresch@gmx.de>

commit 821de780abe4b6db1e76cf1cf5cbe5cbe73be289
Author: cacke-r <cresch@gmx.de>
Date:   Tue Feb 15 20:00:22 2022 +0100

    SniffISO15693: Add functions to codec to enable autocalibration

    Signed-off-by: cacke-r <cresch@gmx.de>

commit 97d7ad8bf0ecc3b8364de31187471d4a506ee120
Author: cacke-r <cresch@gmx.de>
Date:   Sun Jan 30 21:21:39 2022 +0100

    Hook Sniff15693 App to the AutoCalibration command

    Signed-off-by: cacke-r <cresch@gmx.de>

commit 5f8d0ec71c046a39f53e2245e21f98c9f4de94bc
Author: cacke-r <cresch@gmx.de>
Date:   Sun Jan 30 21:20:38 2022 +0100

    Add Autocalibrate command to chamtool

    Signed-off-by: cacke-r <cresch@gmx.de>

commit 8f5053f7b9598a6b26869920650f2fe54cfb8eb5
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Feb 14 12:54:05 2022 -0500

    *Should* finally be working now -- Problem was that the SETTINGS memory remapped the FLASH memory space used by avr-gcc\'s __flash to a custom .flashdata section after the LUFA build by a second call to avr-objcopy -- Clever way to maximize slot space, but the previous code was not aware of that causing apparently quasi-non-deterministic behavior with some readers -- Cf. #313 and #315

commit 2d547404c28f24d988d0b36066042ae55fd2d1f2
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Feb 14 12:32:10 2022 -0500

    Separate section to store in flash not working -- Beginning new approach to put reduced size massive structure into plain old data mem

commit 138a174e56f0e076e307c4c200724005d7d0f7a0
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Feb 14 08:19:23 2022 -0500

    Swapping out previous __flash settings for commands lookup table into last half of LOG_FRAM (code and Makefile defines -- may still need to extract this new section using avr-objcopy -- pending testing)

commit ac265b4ca6626a8f19f9d6960287b9852397bfe3
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Feb 13 03:54:01 2022 -0500

    Out of hell ... Everything seems to be working again with the MEMORY_LIMITED_TESTING define set in the Makefile :)

commit 65766e1ee401433e7b26cf42aa7d3cd2d6009d48
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Feb 13 02:41:53 2022 -0500

    Incremental backup -- Auth schemes should be working -- New CommMode checksum functions DO NOT fit into text/data (yet)

commit bf1eadc337c39b75793472c2dd24ff7ee81868b1
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Feb 13 00:29:24 2022 -0500

    Making a sane backup point while prfusely debugging

commit a24fbe85c0d088a04989a1b15cd3cec23fd7e3bb
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 21:47:54 2022 -0500

    Preliminary (partial) support for more CommModes -- This is going to need substantial testing -- III

commit 74f216f51aef16e69668f4eb467aa1a69e77807c
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 19:15:57 2022 -0500

    Preliminary (partial) support for more CommModes -- This is going to need substantial testing -- II

commit e9437a67391b1900fff780c45b89c1a2022ae366
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 18:52:10 2022 -0500

    Preliminary (partial) support for more CommModes -- This is going to need substantial testing

commit e9ce4ac76607354bf22574e80636c9ead13486d1
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 14:43:01 2022 -0500

    Untested CMAC implementation for CommMode=FULL exchanges (Enciphered+CMAC'ed data)

commit de798fb01fcfe1a55e27b287f07b153178fd70e8
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 10:59:20 2022 -0500

    Adding in preliminary AES transfer functions -- Trying to save space for more where it can be pruned -- III -- cf. #313

commit a0cb74cac8eea0c2b6e881e67d717df5f09aea1d
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 10:20:10 2022 -0500

    Adding in preliminary AES transfer functions -- Trying to save space for more where it can be pruned -- II

commit 583bb9362b062c115f829b9f4fb13f454f1bbe21
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 10:07:37 2022 -0500

    Adding in preliminary AES transfer functions -- Trying to save space for more where it can be pruned

commit 397478608e107d37db61e67cf0e9d19542850162
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 08:47:04 2022 -0500

    Finding other places to squeeze space for the DESFire config (Log and terminal buffers stored on the stack -- adding buffer full messages for INFO)

commit a4672ffd1edb2f80729faf858470ab70a3711664
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 08:34:27 2022 -0500

    Finding other places to squeeze space for the DESFire config (Log and terminal buffers stored on the stack)

commit 9ee32362b214940c86ae79d40c99318575f03bf1
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 08:02:54 2022 -0500

    Addressing the ACK/NAK keep-alive exchanges from some NXP readers noted by \@lvandenb in #313

commit b6be1b815999a15de364ad616702626435c02547
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 05:32:37 2022 -0500

    Space saving defines to remove currently unused crypto exchange functionality (will revisit when get to this subproject)

commit 20a6960fcad7e00e95e152396e8fb4e6fd6be59b
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 04:41:40 2022 -0500

    Fixing legacy auth algorithm from 3K3DES -> 2K3DES (cf. #313)

commit 1644a9e0aeaaedba0e169215f152ee0b37a80b7f
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 00:40:13 2022 -0500

    Making sure the auth AES and auth ISO handlers keep the chain of prior legacy auths intact

commit bcf68d068763ff6914632d1b7fc50eabcae45a13
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sat Feb 12 00:27:24 2022 -0500

    Last minute modifications to DES/3DES enc/dec routines for data that is not a multiple of the resp. block size

commit e15f8c71d7863bd6065bf396f92dc03bf2abc116
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Feb 11 22:51:37 2022 -0500

    Fixing bug where we must first auth with the legacy command mentioned in #313

commit e22b286c9eff4428629760a3d5031cd8bdc7de91
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Feb 11 17:43:41 2022 -0500

    Removing default testing nonce B from auth commands ; Running make style

commit ba3c73629ce83ca791c9b926c2999fbb6614c4d2
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Feb 11 17:42:20 2022 -0500

    Bug fixes to observations in #313

commit a02f2141e5e5c7ed4231ac1e79c6b19e5524b819
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Fri Feb 4 22:17:00 2022 -0500

    Working AES128 auth and ISO auth ; Still need to debug the legacy auth

commit b451e1212be5410566a3f43414f43288c8d7c0a1
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Thu Feb 3 06:52:33 2022 -0500

    Working AES128 auth ; Nearly working ISO auth (still debugging) -- Stashing copy for reference

commit 71762595efc9e655aecdee45af12087c0a7bdd12
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jan 31 12:54:23 2022 -0500

    Stashing incremental testing release code

commit ca0afad50f4efac9365ac1f617462373db5da455
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jan 31 10:54:37 2022 -0500

    Adding support for non-wrapped native commands (I believe)

commit 281d3c7de8211a7481633e152ad7c7d9d90426f6
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jan 31 01:23:50 2022 -0500

    AuthLegacy(0x0A) works using 3DES enc/dec modes -- Makefile no testing -- For #313

commit 195e60018cadf5e6060bbfdaddc6122fd36c901d
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Mon Jan 31 01:22:07 2022 -0500

    AuthLegacy(0x0A) works using 3DES enc/dec modes

commit 6ee19583512fd946a85521b7811603d5fb057d88
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jan 30 23:03:07 2022 -0500

    AuthISO(0x1A) works tentatively (a little slowly) with the LibNFC test code

commit d20cac46c29fb90fa87e06429dfa9040323c9b0b
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jan 30 15:45:27 2022 -0500

    More testing (still doesn't make it through round2 on the Chameleon)

commit 1e542ecf69522913b53db3113f035161c8217f04
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jan 30 14:53:16 2022 -0500

    Re-implementing the 3DES crypto with CBC

commit fba0ee3f633cb91e54e70487f3f181ebe49a0658
Author: Maxie Dion Schmidt <maxieds@gmail.com>
Date:   Sun Jan 30 12:34:25 2022 -0500

    Debugging ISO authenticate command (0x1a)
