README: clarify supported versions
repmgr: in switchover mode, prevent checks for local config file if not provided

In switchover mode, if no remote repmgr config file is provided with `-C`,
repmgr attempts to look for a file with the same path as the local
file (provided with `-f/--config-file`). However if this was not specified,
repmgr would execute `ls` with an empty filepath on the remote host, which
appeared to succeed, causing subsequent remote repmgr command executions
to fail as a blank value was provided for `-f/--config-file`.

Fixes GitHub #229.
repmgr: in standby switchover, quote file paths in remotely executed commands

Per suggestion from GitHub user sebasmannem (#229)
