intervals don't work as expected
_From [AaronMatthewBrown](https://code.google.com/u/116933825573928182564/) on December 18, 2013 07:26:07_

A different possible fix. This sets the non-blocking mode on the sockets, and disables the sigalarm mode. The numbers come out on the nose with this, though I'm not sure if it breaks anything else.

**Attachment:** [socket_nonblocking.patch](http://code.google.com/p/iperf/issues/detail?id=125#hc1)

_From [ssahani@redhat.com](https://code.google.com/u/ssahani@redhat.com/) on December 18, 2013 08:05:37_

setnonblocking already in net.c

_From [jef.poskanzer](https://code.google.com/u/114501281018969491281/) on December 18, 2013 08:08:45_

Yeah - not currently called anywhere.

Going non-blocking and getting rid of sigalrm mode is a not-obviously-bad idea.  In addition to looking for things it breaks, we'd have to look carefully at the performance.

_From [ssahani@redhat.com](https://code.google.com/u/ssahani@redhat.com/) on December 18, 2013 08:11:03_

Talking about the interrupted system call , the signals are handled by  signal sys call . 

 Looking at  sigaction(2) man page  can we think that setting the SA_RESTART flag is simpler that handling system call interruption. The documentation says that setting it will make certain system calls automatically restartable across signals.
more information in man 7 signal. which has list of syscall automatically restarted.

It would be a simpler implementation .

_From [AaronMatthewBrown](https://code.google.com/u/116933825573928182564/) on December 18, 2013 08:13:57_

The problem is SA_RESTART flag apparently gets auto-set if you use 'signal' because in doing an strace, it's auto-restarting anyway. The restarting is actually the problem, because when a network is lossy, the write can hang for an indeterminate period of time which is problematic when you're trying to make sure you're printing out results at specific intervals.

_From [ssahani@redhat.com](https://code.google.com/u/ssahani@redhat.com/) on December 18, 2013 08:45:23_

the Nwrite function actually calculating how much is written and restarting the write again if it's interrupted if I am not wrong.

```
         switch (errno) {
            case EINTR:
            return count - nleft;
```

Currently sockets are not non-blocked yes write wound hang.

_From [AaronMatthewBrown](https://code.google.com/u/116933825573928182564/) on December 18, 2013 08:52:02_

The EINTR doesn't actually come out though because 'write' gets auto-restarted. Even if you do enable interrupting, there are 2 situations where it will still fail:

1) if any data has been written, the write returns the amount written, not -1/EINTR
2) if the signal occurs in-between calls to write, the EINTR code-path won't get called. I noticed this occur with some frequency even if i had it try to interrupt the calls to write. I'm not sure if this is a property of linux delaying the signal until the syscall is finished, or what, but it'd happen a number of times during every run I tried.

_From [ssahani@redhat.com](https://code.google.com/u/ssahani@redhat.com/) on December 18, 2013 09:09:20_

looking at the manual man 7 signal

" If  a  blocked  call to one of the following interfaces is interrupted by a signal handler, then the call will be automatically restarted after the signal handler returns if
       the SA_RESTART flag was used; otherwise the call will fail with the error EINTR:"

  There is noway to specify SA_RESTART in signal system call. 
1. Correct. But need to check out the errno value  if it's interrupted .. how ever not sure  will debug it.

_From [bltierney@es.net](https://code.google.com/u/117296441546344075535/) on December 18, 2013 13:59:17_

**Labels:** Milestone-3.1a1  

_From [AaronMatthewBrown](https://code.google.com/u/116933825573928182564/) on February 21, 2014 12:26:30_

Here's an updated patch that also rips out the sigalrm code. In testing locally, things seem to work as expected (e.g. even on very lossy connections, the intervals happen as expected instead of quasi-randomly). Would it be possible to get this tested on the 40G testbed? I'm curious what impact the change from sigalrm to select might have.

**Attachment:** [iperf3_nonblocking_mode.patch](http://code.google.com/p/iperf/issues/detail?id=125#hc10)

Just to clarify:  That commit was to a branch, not the mainline.  I'll merge it after some testing and/or tweaking.

So far so good testing on ESnet 100G testbed.  No significant performance differences noted in some admittedly brief testing.

Interval timers do behave much more sanely with (pathological) 10% packet loss configured using `tc qdisc add dev eth40 root netem loss 10%`.  Before revision 081ba8e:

```
[bmah@nersc-diskpt-6 ~]$ iperf3 -c 192.168.101.9 
Connecting to host 192.168.101.9, port 5201
[  4] local 192.168.101.8 port 41175 connected to 192.168.101.9 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.05   sec  1.25 MBytes  9.98 Mbits/sec   19   43.7 KBytes       
[  4]   1.05-2.10   sec  2.25 MBytes  18.0 Mbits/sec   28   35.0 KBytes       
[  4]   2.10-3.07   sec  1.12 MBytes  9.74 Mbits/sec   18   8.74 KBytes       
[  4]   3.07-4.08   sec  2.25 MBytes  18.6 Mbits/sec   25   17.5 KBytes       
[  4]   4.08-5.51   sec  2.50 MBytes  14.7 Mbits/sec   37   26.2 KBytes       
[  4]   5.51-6.00   sec  2.25 MBytes  38.5 Mbits/sec   21   17.5 KBytes       
[  4]   6.00-8.01   sec   512 KBytes  2.09 Mbits/sec   10   35.0 KBytes       
[  4]   8.01-8.01   sec  0.00 Bytes  0.00 bits/sec    0   35.0 KBytes       
[  4]   8.01-9.07   sec  1.62 MBytes  12.9 Mbits/sec   26   35.0 KBytes       
[  4]   9.07-10.07  sec   512 KBytes  4.17 Mbits/sec   11   43.7 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.07  sec  14.2 MBytes  11.9 Mbits/sec  195             sender
[  4]   0.00-10.07  sec  14.1 MBytes  11.8 Mbits/sec                  receiver

iperf Done.
```

After revision 081ba8e:

```
[bmah@nersc-diskpt-6 ~]$ ./iperf3 -c 192.168.101.9 
Connecting to host 192.168.101.9, port 5201
[  4] local 192.168.101.8 port 41173 connected to 192.168.101.9 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec  1.67 MBytes  14.0 Mbits/sec   21   43.7 KBytes       
[  4]   1.00-2.00   sec  4.29 MBytes  36.0 Mbits/sec   38   17.5 KBytes       
[  4]   2.00-3.00   sec  3.50 MBytes  29.4 Mbits/sec   28   26.2 KBytes       
[  4]   3.00-4.00   sec   734 KBytes  6.01 Mbits/sec   12   26.2 KBytes       
[  4]   4.00-5.00   sec  1.39 MBytes  11.7 Mbits/sec   23   35.0 KBytes       
[  4]   5.00-6.00   sec   489 KBytes  4.01 Mbits/sec   16   35.0 KBytes       
[  4]   6.00-7.00   sec   384 KBytes  3.15 Mbits/sec    9   17.5 KBytes       
[  4]   7.00-8.00   sec  6.10 MBytes  51.2 Mbits/sec   51   8.74 KBytes       
[  4]   8.00-9.00   sec   795 KBytes  6.51 Mbits/sec   15   26.2 KBytes       
[  4]   9.00-10.00  sec   620 KBytes  5.08 Mbits/sec   11   17.5 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  19.9 MBytes  16.7 Mbits/sec  224             sender
[  4]   0.00-10.00  sec  19.6 MBytes  16.4 Mbits/sec                  receiver

iperf Done.
```

I merged the features/issue-125 branch to the master (mainline) codeline in revision 54f2264730f54817e4ccfdcc3f871b3de6643641.  I probably could have done a better job in the commit message, but I don't think I can go back and edit it.

Closing this bug as fixed.

This code change seems to have broken UDP tests, which now emit many lines of the form:

```
iperf3: OUT OF ORDER - incoming packet = 1 and received packet = 528351093 AND SP = 5
```

I'm testing a fix.  Basically we want to only set non-blocking mode on sockets on the sending side, but not on the receiver side.  (Usually this means only the client should set non-blocking mode, unless `--reverse` is being used, in which case it's only the server that should do this.)

Leaving this issue open for now while doing more testing on the (possible) fix I just committed.

@bltierney and I would like to see if we can get this into an upcoming 3.0.x maintenance release.

Testing tip of master on the ESnet 100G testbed (same conditions as 17 April tests)...TCP intervals work well:

```
[bmah@nersc-diskpt-6 ~]$ ./iperf3 -c 192.168.101.9 
Connecting to host 192.168.101.9, port 5201
[  4] local 192.168.101.8 port 46303 connected to 192.168.101.9 port 5201
[ ID] Interval           Transfer     Bandwidth       Retr  Cwnd
[  4]   0.00-1.00   sec  2.94 MBytes  24.6 Mbits/sec   25   52.4 KBytes       
[  4]   1.00-2.00   sec  2.21 MBytes  18.5 Mbits/sec   32   17.5 KBytes       
[  4]   2.00-3.00   sec  2.75 MBytes  23.0 Mbits/sec   17    114 KBytes       
[  4]   3.00-4.00   sec  3.22 MBytes  27.0 Mbits/sec   39   26.2 KBytes       
[  4]   4.00-5.00   sec  1.85 MBytes  15.5 Mbits/sec   24   17.5 KBytes       
[  4]   5.00-6.00   sec  1.79 MBytes  15.0 Mbits/sec   26   8.74 KBytes       
[  4]   6.00-7.00   sec  4.01 MBytes  33.6 Mbits/sec   33   35.0 KBytes       
[  4]   7.00-8.00   sec   306 KBytes  2.51 Mbits/sec   11   26.2 KBytes       
[  4]   8.00-9.00   sec  2.27 MBytes  19.0 Mbits/sec   20   87.4 KBytes       
[  4]   9.00-10.00  sec  1.71 MBytes  14.3 Mbits/sec   29   35.0 KBytes       
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Retr
[  4]   0.00-10.00  sec  23.0 MBytes  19.3 Mbits/sec  256             sender
[  4]   0.00-10.00  sec  23.0 MBytes  19.3 Mbits/sec                  receiver

iperf Done.
```

UDP tests exhibit no regressions (seen from the receiver):

```
Accepted connection from 192.168.101.8, port 46304
[  5] local 192.168.101.9 port 5201 connected to 192.168.101.8 port 59423
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  5]   0.00-1.00   sec   104 KBytes   852 Kbits/sec  6.374 ms  2/15 (13%)  
[  5]   1.00-2.00   sec   120 KBytes   983 Kbits/sec  2.426 ms  1/16 (6.2%)  
[  5]   2.00-3.00   sec   104 KBytes   852 Kbits/sec  1.055 ms  3/16 (19%)  
[  5]   3.00-4.00   sec   104 KBytes   852 Kbits/sec  0.462 ms  3/16 (19%)  
[  5]   4.00-5.00   sec   120 KBytes   983 Kbits/sec  0.181 ms  1/16 (6.2%)  
[  5]   5.00-6.00   sec   112 KBytes   918 Kbits/sec  0.078 ms  2/16 (12%)  
[  5]   6.00-7.00   sec   112 KBytes   918 Kbits/sec  0.040 ms  2/16 (12%)  
[  5]   7.00-8.00   sec   120 KBytes   983 Kbits/sec  0.023 ms  1/16 (6.2%)  
[  5]   8.00-9.00   sec   120 KBytes   983 Kbits/sec  0.018 ms  1/16 (6.2%)  
[  5]   9.00-10.00  sec   112 KBytes   918 Kbits/sec  0.014 ms  2/16 (12%)  
[  5]  10.00-10.04  sec  0.00 Bytes  0.00 bits/sec  0.014 ms  0/0 (-nan%)  
- - - - - - - - - - - - - - - - - - - - - - - - -
[ ID] Interval           Transfer     Bandwidth       Jitter    Lost/Total Datagrams
[  5]   0.00-10.04  sec  1.24 MBytes  1.04 Mbits/sec  0.014 ms  18/159 (11%)  
```

Tested with SCTP as well (results not shown).

This looks good enough to merge.

Merge to the 3.0-STABLE branch complete and lightly tested.  Closing this issue.
