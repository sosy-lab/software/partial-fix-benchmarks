Regen.
Check HAVE_STDINT_H before trying to include <stdint.h>.

Partial fix for compilation on some Solaris versions.  Fixes #175.

Submitted by:	@marksolaris
Use HAVE_STDINT_H to prevent a recurrence of #175.
