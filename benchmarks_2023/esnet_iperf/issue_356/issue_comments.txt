Verify --window parameter settings
For a related problem (found using iperf3, but unrelated to iperf3), see [this](https://stackoverflow.com/questions/31546835/tcp-receiving-window-size-higher-than-net-core-rmem-max/35438236).

This is basically a suggestion to read back the socket buffer sizes after setting them to be sure they're actually the requested value.  We should do this for both the sending and receiving sides of the socket.  Testing some code to do this...it's pretty straightforward.

Awesome, thank you!
Re-open so I remember to merge this after it's baked a bit.
Merged!
