#if DHCPS_DEBUG is missing in dhcpserver.c (IDFGH-557)
Hi @luc-github,
We already know the problem, and we are planing to add it in v4.0. Thanks . 
@zhangyanjiaoesp 3.1.3 esp-idf is just released, may I know when V4.0 is planned ?

Is there a reason to wait for a major release to add the define line?

**#if DHCPS_DEBUG**
        DHCPS_LOG("dhcps: send_offer>>udp_sendto result %x\n", SendOffer_err_t);
**#endif**
@igrr Thank you
I think there is another one  looking at code https://github.com/espressif/esp-idf/blob/27cc0d1f91e16980698aac1d1448b131a158f2c1/components/lwip/apps/dhcpserver/dhcpserver.c#L604
but never saw it in output, so not sure it can be a problem

@luc-github I'm sorry that I didn't check all the "DEBUG_LOG" code, this place need to be changed also. Thanks for your reminding. 