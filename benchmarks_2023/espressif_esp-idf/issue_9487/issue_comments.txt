Crash when accessing flash while using RMT and -Os compiler flag (IDFGH-7974)
Thanks for reporting, we can reproduce the issue with your code. Will fix it ASAP.
BTW, calling RMT functions while the flash cache is disabled is not guaranteed to be safe, as these public APIs are not placed in IRAM.
How certain are you that the issue is really fixed?

In IDF 4.4.4 I still get a
> Guru Meditation Error: Core  0 panic'ed (Cache disabled but cached memory region accessed). 

> 0x40163211: rmt_ll_enable_tx_err_interrupt at esp-idf/components/hal/esp32/include/hal/rmt_ll.h:252
> 0x400825f1: shared_intr_isr at esp-idf/components/esp_hw_support/intr_alloc.c:407
> 0x40082ff5: _xt_lowint1 at esp-idf/components/freertos/port/xtensa/xtensa_vectors.S:1114
> 0x4000c050: ?? ??:0
> 0x40008544: ?? ??:0
> 0x40086d35: delay_us at esp-idf/components/spi_flash/spi_flash_os_func_app.c:148
> 0x4009b932: spi_flash_chip_generic_wait_idle at esp-idf/components/spi_flash/spi_flash_chip_generic.c:455
> 0x4009b841: spi_flash_chip_generic_page_program at esp-idf/components/spi_flash/spi_flash_chip_generic.c:253
> 0x4009b34e: spi_flash_chip_generic_write at esp-idf/components/spi_flash/spi_flash_chip_generic.c:278
> 0x40086c40: esp_flash_write at esp-idf/components/spi_flash/esp_flash_api.c:907
> 0x400f41d1: esp_partition_write at esp-idf/components/spi_flash/partition.c:466
> 0x400f306d: esp_ota_write at esp-idf/components/app_update/esp_ota_ops.c:240

`rmt_ll_enable_tx_err_interrupt` is called `rmt_driver_install` which is not expected to run in ISR. Another place can be `rmt_set_err_intr_en`  Maybe you're calling `rmt_set_err_intr_en` in the ISR callback?
That is strange. I do not use rmt_set_err_intr_en at all. rmt_driver_install is only called at start and rmt_driver_uninstall is not used at all.

I suspect that -Os makes the dump unreliable and it's in fact another function like rmt_ll_get_tx_end_interrupt_status, rmt_ll_clear_tx_end_interrupt, etc. which are called from the isr and not properly inlined.
rmt_ll_enable_tx_err_interrupt and
rmt_ll_enable_rx_err_interrupt
are basically the same function. Could it be that the compiler combines them (uses only one of them)? -Os does crazy things. But also rmt_ll_enable_rx_err_interrupt is used only in rmt_set_err_intr_en, rmt_driver_uninstall and rmt_driver_install (via rmt_hal_rx_channel_reset)...

Anyway, if I inline rmt_ll_enable_tx_err_interrupt using `__attribute__((always_inline))` the crash seems to go away...
...but a new crash appears:

0x401631e9: rmt_ll_enable_rx_end_interrupt at esp-idf/components/hal/esp32/include/hal/rmt_ll.h:246
0x40082ff5: _xt_lowint1 at esp-idf/components/freertos/port/xtensa/xtensa_vectors.S:1114
0x40008544: ?? ??:0
0x40086d35: delay_us at esp-idf/components/spi_flash/spi_flash_os_func_app.c:148
0x4009b932: spi_flash_chip_generic_wait_idle at esp-idf/components/spi_flash/spi_flash_chip_generic.c:455
0x4009b841: spi_flash_chip_generic_page_program at esp-idf/components/spi_flash/spi_flash_chip_generic.c:253
0x4009b34e: spi_flash_chip_generic_write at esp-idf/components/spi_flash/spi_flash_chip_generic.c:278
0x40086c40: esp_flash_write at esp-idf/components/spi_flash/esp_flash_api.c:907
0x400f41e1: esp_partition_write at esp-idf/components/spi_flash/partition.c:466
0x400f307d: esp_ota_write at esp-idf/components/app_update/esp_ota_ops.c:240

I did what I did back in August with version 4.4.1: I properly inlined all functions in
https://github.com/espressif/esp-idf/blob/release/v4.4/components/hal/esp32/include/hal/rmt_ll.h
using `__attribute__((always_inline))`.

Now I'm monitoring if the problem is gone again.
No problems so far after inlining.

What were the changes you made to the 4.4 branch?
Looking at the history of these files makes me think the fix might have somehow gotten lost.
https://github.com/espressif/esp-idf/commits/release/v4.4/components/hal/esp32/include/hal/rmt_ll.h
https://github.com/espressif/esp-idf/commits/release/v4.4/components/driver/rmt.c
I'm encountering a similar problem (slightly different trace) in version 4.4.2 and 4.4.4. I'm calling rmt_driver_install with the ESP_INTR_FLAG_IRAM flag. 
Using __attribute__((always_inline)) on rmt_ll_get_tx_err_interrupt_status seems to help me. 
But when looking into the driver I noticed that it looks like the tx_buf is allocated in the wrong way in rmt_translator_init().

>  if (p_rmt_obj[channel]->intr_alloc_flags & ESP_INTR_FLAG_IRAM) {
            p_rmt_obj[channel]->tx_buf = (rmt_item32_t *)malloc(block_size);
        } else {
            p_rmt_obj[channel]->tx_buf = (rmt_item32_t *)heap_caps_calloc(1, block_size, MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT);
        }

should it not be the other way around?

>  if (!(p_rmt_obj[channel]->intr_alloc_flags & ESP_INTR_FLAG_IRAM)) {
            p_rmt_obj[channel]->tx_buf = (rmt_item32_t *)malloc(block_size);
        } else {
            p_rmt_obj[channel]->tx_buf = (rmt_item32_t *)heap_caps_calloc(1, block_size, MALLOC_CAP_INTERNAL | MALLOC_CAP_8BIT);
        }

I don't know if this solves any issues yet, but it does not look correct to me.  
Please reopen.
@emmpe Thanks for reporting, yeah, it's an issue, we will fix it ASAP and backport to release/4.4

@kriegste You're correct, the fix is not backported to 4.4 release yet (it should exist since 5.0). We will backport this fix to release/4.4 soon.