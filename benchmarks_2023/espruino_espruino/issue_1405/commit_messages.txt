Merge pull request #1407 from espruino/MaBecker-patch-1

add ESP8266 #1380, #1358
Avoid double on('connect') for non-HTTP connects

Fixes #1405
Avoid double on('connect') for non-HTTP connects

Fixes #1405
Fix large HTTP headers receive.

Added a test which sends HTTP headers which take more than
MSS bytes (536) and therefore the httpParseHeaders() needs to be
called later again after the next packet arrives.

Fixes #1405
Fix receiving of large HTTP headers

Added a test which sends HTTP headers which take more than
MSS bytes (536) and therefore the httpParseHeaders() needs to be
called later again after the next packet arrives.

Fixes #1405
Fix receiving of large HTTP headers

Added a test which sends HTTP headers which take more than
MSS bytes (536) and therefore the httpParseHeaders() needs to be
called later again after the next packet arrives.

Fixes #1405
Fix receiving of large HTTP headers

Added a test which sends HTTP headers which take more than
MSS bytes (536) and therefore the httpParseHeaders() needs to be
called later again after the next packet arrives.

Fixes #1405
