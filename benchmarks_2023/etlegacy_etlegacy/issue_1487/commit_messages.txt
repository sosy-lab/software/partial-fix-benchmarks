cgame: fix bullet water splash and particle impact range visibility

* Add waterSplashIsSprite field in impact particle definition
* Fix water splash light color
game: added dynamite chaining, refs #1487
game: fixed dynamite chain check on secondary objectives, refs #1487

The dyna chaining feature checks if dyna are set on the same objective
to ensure only relevant dyna are exploded in case two objectives are
close to one another.

A condition error was introduced in vanilla, but this was inconsequential
on that codebase. Years later, the onobjective parameter was never set
on secondary objective. The dyna chain feature was however still working,
since the check was comparing 'empty' to 'empty' in G_RadiusDamage()
and etpro_RadiusDamage().

Properly fixing the condition also avoid issues in the dynamite
indicator feature implementation.
game: add dyna chaining free, refs #1487

Add the possibility to remove the dynamtite in chain instead of explosing them.
g_dynamiteChaining cvar is add to control the chaining behaviour (explosion or free).
game: fix dynamite chaining deleting non armed dynamites, refs #1487 (#1716)

* game: fix dynamite chaining deleting non armed dynamites

* game: fix dynamite chaining deleting non armed dynamites, missed 1 check

* game: fix dynamite chaining deleting non armed dynamites, missed 2nd check
game: fix dynamite chaining on multi-stage objs, refs #1487
game: fix dynamite chaining on multi-stage objs, refs #1487
game: removed g_dynamiteChaining cvar, refs #1487

Use the dynamite chain free behaviour by default.
