renderercommon: fixed backward compatibility, refs #1510 #1457 #1028

glconfig_t is referred in cgame (vidWidth, vidHeight, windowsAspect).
Keep the obsolete items in order to keep the correct index ordering.

This allows old versions to connect to newer mod versions.
cgame: don't draw FT icon if disguised enemy use player uniform, refs #1503
cgame: don't display FT icon with 1 member in FT, refs #1503
cgame: fix a case where FT icon should not be draw

Fix the case where the disguised uniform is from a FT member and the player is not in a FT or in a different FT
