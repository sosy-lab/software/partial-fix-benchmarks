Emitter: Condense define/class block setup.
Allow Hack-inspired shorthand constructors. (fixes #279).
Push Tainted outside of core (fixes #279).

I've adjusted dyna_tools.py to generate a different set of extras for
native classes (DYNA_ID_x to get the id when doing dynaload, and only
the ID_x. No INIT_ macro).

I'm pushing `Tainted` outside of core, because I don't see a critical
use for it being there. It makes more sense in apache's mod_lily.

Most importantly, `mod_lily` no longer uses parser internals.
