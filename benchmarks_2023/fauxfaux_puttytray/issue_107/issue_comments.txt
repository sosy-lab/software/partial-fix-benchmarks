Always allow "Restart" menu item
Will be included in p0.63-t020 and higher.

I forgot to say thank you for this.  Looking forward to the next beta relase.

This is broken in p0.64-t023. "Restart Session" does not show up until it detects that the connection has broken. Perhaps it's because of [this](https://github.com/FauxFaux/PuTTYTray/commit/a7c6cd79067f687f989c3204afdcc1c03e27ddc2#diff-88776592a9f52e2b3f2939253efbc5ecL383)?

@FauxFaux Any news on this? Still the same in p0.65-t025.

Good spot, @nyuszika7h.  Fixed again!
