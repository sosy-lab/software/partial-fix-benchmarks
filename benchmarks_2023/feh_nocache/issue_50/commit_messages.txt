Merge pull request #43 from shadjiiski/fix-maxfd-off-by-1

Fix off by one issue with NOCACHE_MAX_FDS
Document that this tool is perhaps not the best thing to be using in 2022

Addresses the comment in issue #50.
Document how to run a process in a cgroup

Addresses the documentation gap mentioned in issue #50.
Add a bool block_signals for the cleanup codepath

Currently set to true for all callers, so this doesn’t change behavior.

Issue #50
Block signals one time only for final destroy() call

Previously, we’d block/unblock signals once for every potential FD.
(That’s expensive, e.g. on my system `/proc/sys/fs/file-max` is 780838.)

Reduces the major portion of overhead mentioned in issue #50.
Keep track of the max FD we have ever stored info for, and don’t iterate beyond it on cleanup

This is an optimization that yields a slight performance benefit for
calling tools that do very little (like /bin/true).

Addressing the performance issue #50.
Add some comments about cgroup use to the man page

Issue #50.
