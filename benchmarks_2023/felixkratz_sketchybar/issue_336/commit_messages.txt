add string property to all texts to avoid table ambiguity
fix memory leak when bar is not sticky (#336)
fix window retain problems (#336)
less aggressive display event handling (#336)
reintroduce transactional window ordering on Sonoma (#336)
