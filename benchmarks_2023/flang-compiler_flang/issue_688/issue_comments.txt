[ICE][OpenMP] taskloop in subroutine causes ICE in flang2
Hi Kiran,

This patch was committed only on your fork of this repo, IMHO this ticket should not be closed...

I was testing out the github actions workflow for setting up a CI. Merged the fix for this issue in my fork to test triggering the CI which seems to have closed this issue. Reopening as suggested by @pawosm-arm.