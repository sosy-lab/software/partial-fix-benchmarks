Zebra is ignoring IPv6 link-local addresses on recent Linux kernels
This bug was introduced by a patch from Vivek, I'll ask him to look at it real quick
Apparently this issue is a false alarm.

Vivek's change to ignore IPv6 addresses with the IFA_F_DADFAILED/IFA_F_TENTATIVE flags is correct as these addresses can not be used nor bound to.

I did some more testing here and the problem only happens on a very specific scenario. When you run a VirtualBox VM using a bridged network adapter based on a wlan interface, all IPv6 addresses added to this virtual adapter fail in the IPv6 DAD check (and thus zebra will correctly ignore them). The same doesn't happen if we use the bridge mode based on a wired ethernet interface.

Please see the output below (enp0s3 is bridged on a wlan interface and enp0s8 is bridged on a wired ethernet interface):
```
# ip -6 addr add 3000::1/64 dev enp0s3
# ip -6 addr add 4000::1/64 dev enp0s8
# ip -6 addr show scope global
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP qlen 1000
    inet6 3000::1/64 scope global tentative dadfailed 
       valid_lft forever preferred_lft forever
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 state UP qlen 1000
    inet6 4000::1/64 scope global 
       valid_lft forever preferred_lft forever
```

Something really obscure is going on here but the problem is definitely not in zebra. And using different kernel versions doesn't change anything, just the test environment.
My results vary on a ubuntu 16.10 VM I just installed:

dsa@ubuntu-1610:~$ uname -a
Linux ubuntu-1610 4.8.0-22-generic #24-Ubuntu SMP Sat Oct 8 09:15:00 UTC 2016 x86_64 x86_64 x86_64 GNU/Linux

This interface is bridged to my Wi-Fi and pulls a dhcp address from the router (actual addresses obscured, but it is a valid address):
dsa@ubuntu-1610:~$ ip addr sh enp0s8
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:3d:4d:e3 brd ff:ff:ff:ff:ff:ff
    inet 172.16.xx.xx/24 brd 172.16.xx.255 scope global enp0s8
       valid_lft forever preferred_lft forever
    inet6 2601:282:800:xxxx:xxxx:xxxx:xxxx:xxxx/64 scope global mngtmpaddr dynamic
       valid_lft 600sec preferred_lft 300sec
    inet6 fe80::a00:27ff:fe3d:4de3/64 scope link
       valid_lft forever preferred_lft forever

This is using Virtual Box on a Mac running 10.12.2.
@rwestphal ok to close this bug then?
@donaldsharp sure.