diff --git a/bgpd/bgp_vty.c b/bgpd/bgp_vty.c
index c9bd0f148aea..f9b7064d15cc 100644
--- a/bgpd/bgp_vty.c
+++ b/bgpd/bgp_vty.c
@@ -4655,12 +4655,11 @@ DEFUN (neighbor_description,
 
 DEFUN (no_neighbor_description,
        no_neighbor_description_cmd,
-       "no neighbor <A.B.C.D|X:X::X:X|WORD> description [LINE]",
+       "no neighbor <A.B.C.D|X:X::X:X|WORD> description",
        NO_STR
        NEIGHBOR_STR
        NEIGHBOR_ADDR_STR2
-       "Neighbor specific description\n"
-       "Up to 80 characters describing this neighbor\n")
+       "Neighbor specific description\n")
 {
 	int idx_peer = 2;
 	struct peer *peer;
@@ -4674,6 +4673,11 @@ DEFUN (no_neighbor_description,
 	return CMD_SUCCESS;
 }
 
+ALIAS(no_neighbor_description, no_neighbor_description_comment_cmd,
+      "no neighbor <A.B.C.D|X:X::X:X|WORD> description LINE...",
+      NO_STR NEIGHBOR_STR NEIGHBOR_ADDR_STR2
+      "Neighbor specific description\n"
+      "Up to 80 characters describing this neighbor\n")
 
 /* Neighbor update-source. */
 static int peer_update_source_vty(struct vty *vty, const char *peer_str,
@@ -13035,6 +13039,7 @@ void bgp_vty_init(void)
 	/* "neighbor description" commands. */
 	install_element(BGP_NODE, &neighbor_description_cmd);
 	install_element(BGP_NODE, &no_neighbor_description_cmd);
+	install_element(BGP_NODE, &no_neighbor_description_comment_cmd);
 
 	/* "neighbor update-source" commands. "*/
 	install_element(BGP_NODE, &neighbor_update_source_cmd);
diff --git a/bgpd/bgpd.c b/bgpd/bgpd.c
index 33c1f80a635a..3839eccbb58b 100644
--- a/bgpd/bgpd.c
+++ b/bgpd/bgpd.c
@@ -1493,10 +1493,12 @@ void bgp_peer_conf_if_to_su_update(struct peer *peer)
 	 * needed.
 	 */
 	if (peer_addr_updated) {
-		if (peer->password && prev_family == AF_UNSPEC)
+		if (CHECK_FLAG(peer->flags, PEER_FLAG_PASSWORD)
+		    && prev_family == AF_UNSPEC)
 			bgp_md5_set(peer);
 	} else {
-		if (peer->password && prev_family != AF_UNSPEC)
+		if (CHECK_FLAG(peer->flags, PEER_FLAG_PASSWORD)
+		    && prev_family != AF_UNSPEC)
 			bgp_md5_unset(peer);
 		peer->su.sa.sa_family = AF_UNSPEC;
 		memset(&peer->su.sin6.sin6_addr, 0, sizeof(struct in6_addr));
@@ -2229,9 +2231,8 @@ int peer_delete(struct peer *peer)
 		bgp_unlink_nexthop_by_peer(peer);
 
 	/* Password configuration */
-	if (peer->password) {
+	if (CHECK_FLAG(peer->flags, PEER_FLAG_PASSWORD)) {
 		XFREE(MTYPE_PEER_PASSWORD, peer->password);
-		peer->password = NULL;
 
 		if (!accept_peer && !BGP_PEER_SU_UNSPEC(peer)
 		    && !CHECK_FLAG(peer->sflags, PEER_STATUS_GROUP))
@@ -2461,8 +2462,9 @@ static void peer_group2peer_config_copy(struct peer_group *group,
 	}
 
 	/* password apply */
-	if (conf->password && !peer->password)
-		peer->password = XSTRDUP(MTYPE_PEER_PASSWORD, conf->password);
+	if (!CHECK_FLAG(peer->flags_override, PEER_FLAG_PASSWORD))
+		PEER_STR_ATTR_INHERIT(peer, group, password,
+				      MTYPE_PEER_PASSWORD);
 
 	if (!BGP_PEER_SU_UNSPEC(peer))
 		bgp_md5_set(peer);
@@ -3840,6 +3842,7 @@ static const struct peer_flag_action peer_flag_action_list[] = {
 	{PEER_FLAG_ROUTEADV, 0, peer_change_none},
 	{PEER_FLAG_TIMER, 0, peer_change_none},
 	{PEER_FLAG_TIMER_CONNECT, 0, peer_change_none},
+	{PEER_FLAG_PASSWORD, 0, peer_change_none},
 	{0, 0, 0}};
 
 static const struct peer_flag_action peer_af_flag_action_list[] = {
@@ -5421,55 +5424,69 @@ int peer_local_as_unset(struct peer *peer)
 /* Set password for authenticating with the peer. */
 int peer_password_set(struct peer *peer, const char *password)
 {
-	struct listnode *nn, *nnode;
+	struct peer *member;
+	struct listnode *node, *nnode;
 	int len = password ? strlen(password) : 0;
 	int ret = BGP_SUCCESS;
 
 	if ((len < PEER_PASSWORD_MINLEN) || (len > PEER_PASSWORD_MAXLEN))
 		return BGP_ERR_INVALID_VALUE;
 
-	if (peer->password && strcmp(peer->password, password) == 0
-	    && !CHECK_FLAG(peer->sflags, PEER_STATUS_GROUP))
+	/* Set flag and configuration on peer. */
+	peer_flag_set(peer, PEER_FLAG_PASSWORD);
+	if (peer->password && strcmp(peer->password, password) == 0)
 		return 0;
-
-	if (peer->password)
-		XFREE(MTYPE_PEER_PASSWORD, peer->password);
-
+	XFREE(MTYPE_PEER_PASSWORD, peer->password);
 	peer->password = XSTRDUP(MTYPE_PEER_PASSWORD, password);
 
+	/* Check if handling a regular peer. */
 	if (!CHECK_FLAG(peer->sflags, PEER_STATUS_GROUP)) {
+		/* Send notification or reset peer depending on state. */
 		if (BGP_IS_VALID_STATE_FOR_NOTIF(peer->status))
 			bgp_notify_send(peer, BGP_NOTIFY_CEASE,
 					BGP_NOTIFY_CEASE_CONFIG_CHANGE);
 		else
 			bgp_session_reset(peer);
 
+		/*
+		 * Attempt to install password on socket and skip peer-group
+		 * mechanics.
+		 */
 		if (BGP_PEER_SU_UNSPEC(peer))
 			return BGP_SUCCESS;
-
 		return (bgp_md5_set(peer) >= 0) ? BGP_SUCCESS
 						: BGP_ERR_TCPSIG_FAILED;
 	}
 
-	for (ALL_LIST_ELEMENTS(peer->group->peer, nn, nnode, peer)) {
-		if (peer->password && strcmp(peer->password, password) == 0)
+	/*
+	 * Set flag and configuration on all peer-group members, unless they are
+	 * explicitely overriding peer-group configuration.
+	 */
+	for (ALL_LIST_ELEMENTS(peer->group->peer, node, nnode, member)) {
+		/* Skip peers with overridden configuration. */
+		if (CHECK_FLAG(member->flags_override, PEER_FLAG_PASSWORD))
 			continue;
 
-		if (peer->password)
-			XFREE(MTYPE_PEER_PASSWORD, peer->password);
-
-		peer->password = XSTRDUP(MTYPE_PEER_PASSWORD, password);
+		/* Skip peers with the same password. */
+		if (member->password && strcmp(member->password, password) == 0)
+			continue;
 
-		if (BGP_IS_VALID_STATE_FOR_NOTIF(peer->status))
-			bgp_notify_send(peer, BGP_NOTIFY_CEASE,
+		/* Set flag and configuration on peer-group member. */
+		SET_FLAG(member->flags, PEER_FLAG_PASSWORD);
+		if (member->password)
+			XFREE(MTYPE_PEER_PASSWORD, member->password);
+		member->password = XSTRDUP(MTYPE_PEER_PASSWORD, password);
+
+		/* Send notification or reset peer depending on state. */
+		if (BGP_IS_VALID_STATE_FOR_NOTIF(member->status))
+			bgp_notify_send(member, BGP_NOTIFY_CEASE,
 					BGP_NOTIFY_CEASE_CONFIG_CHANGE);
 		else
-			bgp_session_reset(peer);
+			bgp_session_reset(member);
 
-		if (!BGP_PEER_SU_UNSPEC(peer)) {
-			if (bgp_md5_set(peer) < 0)
-				ret = BGP_ERR_TCPSIG_FAILED;
-		}
+		/* Attempt to install password on socket. */
+		if (!BGP_PEER_SU_UNSPEC(member) && bgp_md5_set(member) < 0)
+			ret = BGP_ERR_TCPSIG_FAILED;
 	}
 
 	return ret;
@@ -5477,47 +5494,63 @@ int peer_password_set(struct peer *peer, const char *password)
 
 int peer_password_unset(struct peer *peer)
 {
-	struct listnode *nn, *nnode;
+	struct peer *member;
+	struct listnode *node, *nnode;
 
-	if (!peer->password && !CHECK_FLAG(peer->sflags, PEER_STATUS_GROUP))
+	if (!CHECK_FLAG(peer->flags, PEER_FLAG_PASSWORD))
 		return 0;
 
+	/* Inherit configuration from peer-group if peer is member. */
+	if (peer_group_active(peer)) {
+		peer_flag_inherit(peer, PEER_FLAG_PASSWORD);
+		PEER_STR_ATTR_INHERIT(peer, peer->group, password,
+				      MTYPE_PEER_PASSWORD);
+	} else {
+		/* Otherwise remove flag and configuration from peer. */
+		peer_flag_unset(peer, PEER_FLAG_PASSWORD);
+		XFREE(MTYPE_PEER_PASSWORD, peer->password);
+	}
+
+	/* Check if handling a regular peer. */
 	if (!CHECK_FLAG(peer->sflags, PEER_STATUS_GROUP)) {
+		/* Send notification or reset peer depending on state. */
 		if (BGP_IS_VALID_STATE_FOR_NOTIF(peer->status))
 			bgp_notify_send(peer, BGP_NOTIFY_CEASE,
 					BGP_NOTIFY_CEASE_CONFIG_CHANGE);
 		else
 			bgp_session_reset(peer);
 
-		if (peer->password)
-			XFREE(MTYPE_PEER_PASSWORD, peer->password);
-
-		peer->password = NULL;
-
+		/* Attempt to uninstall password on socket. */
 		if (!BGP_PEER_SU_UNSPEC(peer))
 			bgp_md5_unset(peer);
 
+		/* Skip peer-group mechanics for regular peers. */
 		return 0;
 	}
 
-	XFREE(MTYPE_PEER_PASSWORD, peer->password);
-	peer->password = NULL;
-
-	for (ALL_LIST_ELEMENTS(peer->group->peer, nn, nnode, peer)) {
-		if (!peer->password)
+	/*
+	 * Remove flag and configuration from all peer-group members, unless
+	 * they are explicitely overriding peer-group configuration.
+	 */
+	for (ALL_LIST_ELEMENTS(peer->group->peer, node, nnode, member)) {
+		/* Skip peers with overridden configuration. */
+		if (CHECK_FLAG(member->flags_override, PEER_FLAG_PASSWORD))
 			continue;
 
-		if (BGP_IS_VALID_STATE_FOR_NOTIF(peer->status))
-			bgp_notify_send(peer, BGP_NOTIFY_CEASE,
+		/* Remove flag and configuration on peer-group member. */
+		UNSET_FLAG(member->flags, PEER_FLAG_PASSWORD);
+		XFREE(MTYPE_PEER_PASSWORD, member->password);
+
+		/* Send notification or reset peer depending on state. */
+		if (BGP_IS_VALID_STATE_FOR_NOTIF(member->status))
+			bgp_notify_send(member, BGP_NOTIFY_CEASE,
 					BGP_NOTIFY_CEASE_CONFIG_CHANGE);
 		else
-			bgp_session_reset(peer);
-
-		XFREE(MTYPE_PEER_PASSWORD, peer->password);
-		peer->password = NULL;
+			bgp_session_reset(member);
 
-		if (!BGP_PEER_SU_UNSPEC(peer))
-			bgp_md5_unset(peer);
+		/* Attempt to uninstall password on socket. */
+		if (!BGP_PEER_SU_UNSPEC(member))
+			bgp_md5_unset(member);
 	}
 
 	return 0;
@@ -6970,13 +7003,9 @@ static void bgp_config_write_peer_global(struct vty *vty, struct bgp *bgp,
 	}
 
 	/* password */
-	if (peer->password) {
-		if (!peer_group_active(peer) || !g_peer->password
-		    || strcmp(peer->password, g_peer->password) != 0) {
-			vty_out(vty, " neighbor %s password %s\n", addr,
-				peer->password);
-		}
-	}
+	if (peergroup_flag_check(peer, PEER_FLAG_PASSWORD))
+		vty_out(vty, " neighbor %s password %s\n", addr,
+			peer->password);
 
 	/* neighbor solo */
 	if (CHECK_FLAG(peer->flags, PEER_FLAG_LONESOUL)) {
diff --git a/bgpd/bgpd.h b/bgpd/bgpd.h
index cc5f8e057af0..c17ddcc041b2 100644
--- a/bgpd/bgpd.h
+++ b/bgpd/bgpd.h
@@ -898,6 +898,7 @@ struct peer {
 #define PEER_FLAG_ROUTEADV                  (1 << 17) /* route advertise */
 #define PEER_FLAG_TIMER                     (1 << 18) /* keepalive & holdtime timers */
 #define PEER_FLAG_TIMER_CONNECT             (1 << 19) /* connect timer */
+#define PEER_FLAG_PASSWORD                  (1 << 20) /* password */
 
 	/* outgoing message sent in CEASE_ADMIN_SHUTDOWN notify */
 	char *tx_shutdown_message;
diff --git a/tests/bgpd/test_peer_attr.c b/tests/bgpd/test_peer_attr.c
index 5e09bf61a91d..6a978d87f825 100644
--- a/tests/bgpd/test_peer_attr.c
+++ b/tests/bgpd/test_peer_attr.c
@@ -210,6 +210,12 @@ static struct test_peer_attr test_peer_attrs[] = {
 		.o.invert_peer = true,
 		.o.use_iface_peer = true,
 	},
+	{
+		.cmd = "description",
+		.peer_cmd = "description FRR Peer",
+		.group_cmd = "description FRR Group",
+		.type = PEER_AT_GLOBAL_CUSTOM,
+	},
 	{
 		.cmd = "disable-connected-check",
 		.u.flag = PEER_FLAG_DISABLE_CONNECTED_CHECK,
@@ -220,11 +226,33 @@ static struct test_peer_attr test_peer_attrs[] = {
 		.u.flag = PEER_FLAG_DONT_CAPABILITY,
 		.type = PEER_AT_GLOBAL_FLAG,
 	},
+	{
+		.cmd = "ebgp-multihop",
+		.peer_cmd = "ebgp-multihop 10",
+		.group_cmd = "ebgp-multihop 20",
+		.type = PEER_AT_GLOBAL_CUSTOM,
+	},
 	{
 		.cmd = "enforce-first-as",
 		.u.flag = PEER_FLAG_ENFORCE_FIRST_AS,
 		.type = PEER_AT_GLOBAL_FLAG,
 	},
+	{
+		.cmd = "local-as",
+		.peer_cmd = "local-as 1",
+		.group_cmd = "local-as 2",
+		.type = PEER_AT_GLOBAL_CUSTOM,
+	},
+	{
+		.cmd = "local-as 1 no-prepend",
+		.u.flag = PEER_FLAG_LOCAL_AS_NO_PREPEND,
+		.type = PEER_AT_GLOBAL_FLAG,
+	},
+	{
+		.cmd = "local-as 1 no-prepend replace-as",
+		.u.flag = PEER_FLAG_LOCAL_AS_REPLACE_AS,
+		.type = PEER_AT_GLOBAL_FLAG,
+	},
 	{
 		.cmd = "override-capability",
 		.u.flag = PEER_FLAG_OVERRIDE_CAPABILITY,
@@ -235,11 +263,22 @@ static struct test_peer_attr test_peer_attrs[] = {
 		.u.flag = PEER_FLAG_PASSIVE,
 		.type = PEER_AT_GLOBAL_FLAG,
 	},
+	{
+		.cmd = "password",
+		.peer_cmd = "password FRR-Peer",
+		.group_cmd = "password FRR-Group",
+		.type = PEER_AT_GLOBAL_CUSTOM,
+	},
 	{
 		.cmd = "shutdown",
 		.u.flag = PEER_FLAG_SHUTDOWN,
 		.type = PEER_AT_GLOBAL_FLAG,
 	},
+	{
+		.cmd = "solo",
+		.u.flag = PEER_FLAG_LONESOUL,
+		.type = PEER_AT_GLOBAL_FLAG,
+	},
 	{
 		.cmd = "strict-capability-match",
 		.u.flag = PEER_FLAG_STRICT_CAP_MATCH,
@@ -259,6 +298,18 @@ static struct test_peer_attr test_peer_attrs[] = {
 		.u.flag = PEER_FLAG_TIMER_CONNECT,
 		.type = PEER_AT_GLOBAL_FLAG,
 	},
+	{
+		.cmd = "ttl-security hops",
+		.peer_cmd = "ttl-security hops 10",
+		.group_cmd = "ttl-security hops 20",
+		.type = PEER_AT_GLOBAL_CUSTOM,
+	},
+	{
+		.cmd = "update-source",
+		.peer_cmd = "update-source 255.255.255.1",
+		.group_cmd = "update-source 255.255.255.2",
+		.type = PEER_AT_GLOBAL_CUSTOM,
+	},
 
 	/* Address Family Attributes */
 	{
