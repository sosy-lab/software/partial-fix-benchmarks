diff --git a/bgpd/bgp_community.c b/bgpd/bgp_community.c
index 43138b82f68f..b034ec9f7bf9 100644
--- a/bgpd/bgp_community.c
+++ b/bgpd/bgp_community.c
@@ -28,6 +28,7 @@
 
 #include "bgpd/bgp_memory.h"
 #include "bgpd/bgp_community.h"
+#include "bgpd/bgp_community_alias.h"
 
 /* Hash of community attribute. */
 static struct hash *comhash;
@@ -292,7 +293,7 @@ static void set_community_string(struct community *com, bool make_json)
 			len += strlen(" no-peer");
 			break;
 		default:
-			len += strlen(" 65536:65535");
+			len = BUFSIZ;
 			break;
 		}
 	}
@@ -450,7 +451,7 @@ static void set_community_string(struct community *com, bool make_json)
 			val = comval & 0xFFFF;
 			char buf[32];
 			snprintf(buf, sizeof(buf), "%u:%d", as, val);
-			strlcat(str, buf, len);
+			strlcat(str, bgp_community2alias(buf), len);
 			if (make_json) {
 				json_string = json_object_new_string(buf);
 				json_object_array_add(json_community_list,
diff --git a/bgpd/bgp_community_alias.c b/bgpd/bgp_community_alias.c
new file mode 100644
index 000000000000..cfbcfac512c8
--- /dev/null
+++ b/bgpd/bgp_community_alias.c
@@ -0,0 +1,128 @@
+/* BGP community, large-community aliasing.
+ *
+ * Copyright (C) 2021 Donatas Abraitis <donatas.abraitis@gmail.com>
+ *
+ * This file is part of FRRouting (FRR).
+ *
+ * FRR is free software; you can redistribute it and/or modify it under the
+ * terms of the GNU General Public License as published by the Free Software
+ * Foundation; either version 2, or (at your option) any later version.
+ *
+ * FRR is distributed in the hope that it will be useful, but WITHOUT ANY
+ * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
+ * details.
+ *
+ * You should have received a copy of the GNU General Public License along
+ * with this program; see the file COPYING; if not, write to the Free Software
+ * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
+ */
+
+#include "hash.h"
+#include "memory.h"
+#include "linklist.h"
+
+#include "bgpd/bgpd.h"
+#include "bgpd/bgp_community_alias.h"
+
+struct list *bgp_community_alias_list = NULL;
+
+void bgp_community_alias_list_free(void)
+{
+	struct community_alias *ca;
+	struct listnode *node, *nnode;
+
+	for (ALL_LIST_ELEMENTS(bgp_community_alias_list, node, nnode, ca)) {
+		listnode_delete(bgp_community_alias_list, ca);
+		XFREE(MTYPE_COMMUNITY_ALIAS_STR, ca->community);
+		XFREE(MTYPE_COMMUNITY_ALIAS_STR, ca->alias);
+		XFREE(MTYPE_COMMUNITY_ALIAS, ca);
+	}
+}
+
+void bgp_community_alias_list_add_entry(const char *community,
+					const char *alias)
+{
+	struct community_alias *ca;
+
+	ca = XCALLOC(MTYPE_COMMUNITY_ALIAS, sizeof(struct community_alias));
+
+	if (community && alias) {
+		ca->community = XSTRDUP(MTYPE_COMMUNITY_ALIAS_STR, community);
+		ca->alias = XSTRDUP(MTYPE_COMMUNITY_ALIAS_STR, alias);
+	}
+
+	listnode_add(bgp_community_alias_list, ca);
+}
+
+bool bgp_community_alias_list_remove_entry(const char *community)
+{
+	struct community_alias *ca;
+	struct listnode *node, *nnode;
+
+	for (ALL_LIST_ELEMENTS(bgp_community_alias_list, node, nnode, ca)) {
+		if (strncmp(ca->community, community, strlen(community)) == 0) {
+			listnode_delete(bgp_community_alias_list, ca);
+			XFREE(MTYPE_COMMUNITY_ALIAS_STR, ca->community);
+			XFREE(MTYPE_COMMUNITY_ALIAS_STR, ca->alias);
+			XFREE(MTYPE_COMMUNITY_ALIAS, ca);
+			return true;
+		}
+	}
+
+	return false;
+}
+
+bool bgp_community_alias_list_has_entry(const char *community,
+					const char *alias)
+{
+	struct community_alias *ca;
+	struct listnode *node, *nnode;
+
+	for (ALL_LIST_ELEMENTS(bgp_community_alias_list, node, nnode, ca)) {
+		/* Comparing strlen in addition to make sure we don't
+		 * hit a trap between community and large communities. E.g:
+		 * community       65001:1
+		 * large-community 65001:1:1
+		 */
+		if (strlen(ca->community) == strlen(community)
+		    && strncmp(ca->community, community, strlen(community)) == 0
+		    && strncmp(ca->alias, alias, strlen(community)) == 0)
+			return true;
+
+		/* Community found, but alias name is different */
+		if (strlen(ca->community) == strlen(community)
+		    && strncmp(ca->community, community, strlen(community))
+			       == 0) {
+			bgp_community_alias_list_remove_entry(ca->community);
+			return false;
+		}
+	}
+
+	return false;
+}
+
+const char *bgp_community2alias(char *community)
+{
+	struct community_alias *ca;
+	struct listnode *node, *nnode;
+
+	for (ALL_LIST_ELEMENTS(bgp_community_alias_list, node, nnode, ca)) {
+		if (strlen(ca->community) == strlen(community)
+		    && strncmp(ca->community, community, strlen(community))
+			       == 0)
+			return ca->alias;
+	}
+
+	return community;
+}
+
+void bgp_community_alias_list_write(struct vty *vty)
+{
+	struct community_alias *ca;
+	struct listnode *node, *nnode;
+
+	for (ALL_LIST_ELEMENTS(bgp_community_alias_list, node, nnode, ca))
+		vty_out(vty, "bgp community alias %s %s\n", ca->community,
+			ca->alias);
+}
diff --git a/bgpd/bgp_community_alias.h b/bgpd/bgp_community_alias.h
new file mode 100644
index 000000000000..d2b600d88b42
--- /dev/null
+++ b/bgpd/bgp_community_alias.h
@@ -0,0 +1,43 @@
+/* BGP community, large-community aliasing.
+ *
+ * Copyright (C) 2021 Donatas Abraitis <donatas.abraitis@gmail.com>
+ *
+ * This file is part of FRRouting (FRR).
+ *
+ * FRR is free software; you can redistribute it and/or modify it under the
+ * terms of the GNU General Public License as published by the Free Software
+ * Foundation; either version 2, or (at your option) any later version.
+ *
+ * FRR is distributed in the hope that it will be useful, but WITHOUT ANY
+ * WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
+ * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
+ * details.
+ *
+ * You should have received a copy of the GNU General Public License along
+ * with this program; see the file COPYING; if not, write to the Free Software
+ * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
+ */
+
+#ifndef _QUAGGA_BGP_COMMUNITY_ALIAS_H
+#define _QUAGGA_BGP_COMMUNITY_ALIAS_H
+
+struct community_alias {
+	/* Human readable community string */
+	char *community;
+
+	/* Human readable community alias */
+	char *alias;
+};
+
+extern struct list *bgp_community_alias_list;
+
+extern void bgp_community_alias_list_free(void);
+extern void bgp_community_alias_list_add_entry(const char *community,
+					       const char *alias);
+extern bool bgp_community_alias_list_remove_entry(const char *community);
+extern bool bgp_community_alias_list_has_entry(const char *community,
+					       const char *alias);
+extern const char *bgp_community2alias(char *community);
+extern void bgp_community_alias_list_write(struct vty *vty);
+
+#endif /* _QUAGGA_BGP_COMMUNITY_ALIAS_H */
diff --git a/bgpd/bgp_lcommunity.c b/bgpd/bgp_lcommunity.c
index 88a85c979e74..ff34937efc73 100644
--- a/bgpd/bgp_lcommunity.c
+++ b/bgpd/bgp_lcommunity.c
@@ -30,6 +30,7 @@
 
 #include "bgpd/bgpd.h"
 #include "bgpd/bgp_lcommunity.h"
+#include "bgpd/bgp_community_alias.h"
 #include "bgpd/bgp_aspath.h"
 
 /* Hash of community attribute. */
@@ -213,7 +214,7 @@ static void set_lcommunity_string(struct lcommunity *lcom, bool make_json)
 	}
 
 	/* 1 space + lcom->size lcom strings + null terminator */
-	size_t str_buf_sz = (LCOMMUNITY_STRLEN * lcom->size) + 2;
+	size_t str_buf_sz = BUFSIZ;
 	str_buf = XCALLOC(MTYPE_LCOMMUNITY_STR, str_buf_sz);
 
 	for (i = 0; i < lcom->size; i++) {
@@ -231,7 +232,7 @@ static void set_lcommunity_string(struct lcommunity *lcom, bool make_json)
 		snprintf(lcsb, sizeof(lcsb), "%u:%u:%u", global, local1,
 			 local2);
 
-		len = strlcat(str_buf, lcsb, str_buf_sz);
+		len = strlcat(str_buf, bgp_community2alias(lcsb), str_buf_sz);
 		assert((unsigned int)len < str_buf_sz);
 
 		if (make_json) {
diff --git a/bgpd/bgp_main.c b/bgpd/bgp_main.c
index ea74a82cec55..932e2bb49a90 100644
--- a/bgpd/bgp_main.c
+++ b/bgpd/bgp_main.c
@@ -66,6 +66,7 @@
 #include "bgpd/bgp_evpn_mh.h"
 #include "bgpd/bgp_nht.h"
 #include "bgpd/bgp_routemap_nb.h"
+#include "bgpd/bgp_community_alias.h"
 
 #ifdef ENABLE_BGP_VNC
 #include "bgpd/rfapi/rfapi_backend.h"
@@ -250,6 +251,8 @@ static __attribute__((__noreturn__)) void bgp_exit(int status)
 	/* reverse community_list_init */
 	community_list_terminate(bgp_clist);
 
+	bgp_community_alias_list_free();
+
 	bgp_vrf_terminate();
 #ifdef ENABLE_BGP_VNC
 	vnc_zebra_destroy();
diff --git a/bgpd/bgp_memory.c b/bgpd/bgp_memory.c
index 36bdc05eb784..9b46ac361ab3 100644
--- a/bgpd/bgp_memory.c
+++ b/bgpd/bgp_memory.c
@@ -68,6 +68,8 @@ DEFINE_MTYPE(BGPD, AS_FILTER_STR, "BGP AS filter str");
 DEFINE_MTYPE(BGPD, COMMUNITY, "community");
 DEFINE_MTYPE(BGPD, COMMUNITY_VAL, "community val");
 DEFINE_MTYPE(BGPD, COMMUNITY_STR, "community str");
+DEFINE_MTYPE(BGPD, COMMUNITY_ALIAS, "community alias");
+DEFINE_MTYPE(BGPD, COMMUNITY_ALIAS_STR, "community alias str");
 
 DEFINE_MTYPE(BGPD, ECOMMUNITY, "extcommunity");
 DEFINE_MTYPE(BGPD, ECOMMUNITY_VAL, "extcommunity val");
diff --git a/bgpd/bgp_memory.h b/bgpd/bgp_memory.h
index 29923424e33c..93e92ce7ea63 100644
--- a/bgpd/bgp_memory.h
+++ b/bgpd/bgp_memory.h
@@ -64,6 +64,8 @@ DECLARE_MTYPE(AS_FILTER_STR);
 DECLARE_MTYPE(COMMUNITY);
 DECLARE_MTYPE(COMMUNITY_VAL);
 DECLARE_MTYPE(COMMUNITY_STR);
+DECLARE_MTYPE(COMMUNITY_ALIAS);
+DECLARE_MTYPE(COMMUNITY_ALIAS_STR);
 
 DECLARE_MTYPE(ECOMMUNITY);
 DECLARE_MTYPE(ECOMMUNITY_VAL);
diff --git a/bgpd/bgp_route.c b/bgpd/bgp_route.c
index f56c866de5a1..24617c953473 100644
--- a/bgpd/bgp_route.c
+++ b/bgpd/bgp_route.c
@@ -51,6 +51,7 @@
 #include "bgpd/bgp_aspath.h"
 #include "bgpd/bgp_regex.h"
 #include "bgpd/bgp_community.h"
+#include "bgpd/bgp_community_alias.h"
 #include "bgpd/bgp_ecommunity.h"
 #include "bgpd/bgp_lcommunity.h"
 #include "bgpd/bgp_clist.h"
diff --git a/bgpd/bgp_vty.c b/bgpd/bgp_vty.c
index 1e465d2620f9..f053aa940451 100644
--- a/bgpd/bgp_vty.c
+++ b/bgpd/bgp_vty.c
@@ -45,6 +45,7 @@
 #include "bgpd/bgp_attr.h"
 #include "bgpd/bgp_aspath.h"
 #include "bgpd/bgp_community.h"
+#include "bgpd/bgp_community_alias.h"
 #include "bgpd/bgp_ecommunity.h"
 #include "bgpd/bgp_lcommunity.h"
 #include "bgpd/bgp_damp.h"
@@ -1499,6 +1500,26 @@ void cli_show_router_bgp_router_id(struct vty *vty, struct lyd_node *dnode,
 	vty_out(vty, " bgp router-id %s\n", yang_dnode_get_string(dnode, NULL));
 }
 
+DEFPY(bgp_community_alias, bgp_community_alias_cmd,
+      "[no$no] bgp community alias WORD$community WORD$alias", NO_STR BGP_STR
+      "Add community specific parameters\n"
+      "Create an alias for a community\n"
+      "Community (AA:BB or AA:BB:CC)\n"
+      "Alias name\n")
+{
+	if (!bgp_community_alias_list)
+		bgp_community_alias_list = list_new();
+
+	if (no) {
+		bgp_community_alias_list_remove_entry(community);
+	} else {
+		if (!bgp_community_alias_list_has_entry(community, alias))
+			bgp_community_alias_list_add_entry(community, alias);
+	}
+
+	return CMD_SUCCESS;
+}
+
 DEFPY (bgp_global_suppress_fib_pending,
        bgp_global_suppress_fib_pending_cmd,
        "[no] bgp suppress-fib-pending",
@@ -17363,6 +17384,8 @@ int bgp_config_write(struct vty *vty)
 		vty_out(vty, "\n");
 	}
 
+	bgp_community_alias_list_write(vty);
+
 	if (bm->wait_for_fib)
 		vty_out(vty, "bgp suppress-fib-pending\n");
 
@@ -17919,6 +17942,9 @@ void bgp_vty_init(void)
 	install_element(CONFIG_NODE, &bgp_set_route_map_delay_timer_cmd);
 	install_element(CONFIG_NODE, &no_bgp_set_route_map_delay_timer_cmd);
 
+	/* bgp community alias */
+	install_element(CONFIG_NODE, &bgp_community_alias_cmd);
+
 	/* global bgp update-delay command */
 	install_element(CONFIG_NODE, &bgp_global_update_delay_cmd);
 	install_element(CONFIG_NODE, &no_bgp_global_update_delay_cmd);
diff --git a/bgpd/subdir.am b/bgpd/subdir.am
index 07e71ba60120..431185d33dbd 100644
--- a/bgpd/subdir.am
+++ b/bgpd/subdir.am
@@ -57,6 +57,7 @@ bgpd_libbgp_a_SOURCES = \
 	bgpd/bgp_bfd.c \
 	bgpd/bgp_clist.c \
 	bgpd/bgp_community.c \
+	bgpd/bgp_community_alias.c \
 	bgpd/bgp_conditional_adv.c \
 	bgpd/bgp_damp.c \
 	bgpd/bgp_debug.c \
@@ -137,6 +138,7 @@ noinst_HEADERS += \
 	bgpd/bgp_bfd.h \
 	bgpd/bgp_clist.h \
 	bgpd/bgp_community.h \
+	bgpd/bgp_community_alias.h \
 	bgpd/bgp_conditional_adv.h \
 	bgpd/bgp_damp.h \
 	bgpd/bgp_debug.h \
