vli-usbhub: Fix the quirk for the legacy VIA 813 chip
fu-engine: Atomically reload configuration files w/ modify-remote

Relying upon the inotify event is racy for installed tests and has
been leading to some failures.

Fixes: https://github.com/fwupd/fwupd/issues/1648
fu-engine: Atomically reload configuration files w/ modify-remote

Relying upon the inotify event is racy for installed tests and has
been leading to some failures.

Fixes: https://github.com/fwupd/fwupd/issues/1648
fu-remote-list: emit a changed signal when modifying a remote

This should notify the daemon of changes before the inotify letting
the data store get reloaded and fix autopkgtest race conditions.
Fixes: #1648
fu-remote-list: emit a changed signal when modifying a remote

This should notify the daemon of changes before the inotify letting
the data store get reloaded and fix autopkgtest race conditions.
Fixes: #1648
