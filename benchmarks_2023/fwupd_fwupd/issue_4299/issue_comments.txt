autopkgtests are failing in Ubuntu and Debian for 1.7.5
In the failure I specifically noticed:
```
Mismatched daemon and client, use fwupdmgr instead
```

Which makes me wonder if we just need to be using `--force` in the p2p test or detect `FWUPD_DBUS_SOCKET` and skip that test (someone would really have have to go out of their way to launch that way and probably doesn't need that infro).
I also am surprised we aren't catching this in CI.
Not only that, but this:

> The daemon has loaded 3rd party code and is no longer supported by the upstream developers!

You able to get the daemon logs on a failing system?
That's just the test plugin. Daemon logs are in the autopkgtest run. I'll try to see if this also is easy to reproduce in unstable in a container. I would think it is.
@ycheng can you reproduce this?
I tried in a jammy lxc container, it doesn't repro for me there, but maybe that's because I have real systemd in lxc but autopkgtest doesn't?
I can reproduce on focal host, the command to setup is:

$ sudo apt-get install autopkgtest lxc-utils lxc-template # once
$ sudo autopkgtest-build-lxc ubuntu jammy # once
$ cd fwupd
$ sudo LC_ALL=C autopkgtest -U --shell-fail -- lxc autopkgtest-jammy

If one test fails, it will give you a shell to debug.

Can you apply the PR I sent up for review and see if it fixes it then?
Backported the patches from the PR into 1.7.5-2 along with that container virtualization thing, but now it's failing because of that I think:
```
Executing: fwupd/fwupdmgr-p2p.test
Error: GDBus.Error:org.freedesktop.DBus.Error.TimedOut: Failed to activate service 'org.freedesktop.fwupd': timed out (service_start_timeout=25000ms)
```

```
Executing: fwupd/fwupdmgr.test
Failed to connect to daemon: Error calling StartServiceByName for org.freedesktop.fwupd: Timeout was reached
Feb 18 21:11:11 ci-049-40846784 systemd[1]: Firmware update daemon was skipped because of a failed condition check (ConditionVirtualization=!container).
FAIL: fwupd/fwupdmgr.test (Child process exited with code 1)
```

https://ci.debian.net/data/autopkgtest/testing/amd64/f/fwupd/19347340/log.gz

@simondeziel I backported your fix since you initially filed it in debian I figured you wanted it sooner. I think your fix might have caused the failure though :/
I can reproduce the same failure as run autopkgtest in ubuntu/focal host and debian/unstable/lxc autopkgtest testbed.
Can you confirm if we back out that container fix it helps?
> Can you confirm if we back out that container fix it helps?

Don’t really understand what you want me to do, yet… will try to investigate…
I'm pretty sure this will fix it:
```
ice.in b/data/fwupd.service.in
index 0095c5b5..6bdf3943 100644
--- a/data/fwupd.service.in
+++ b/data/fwupd.service.in
@@ -3,7 +3,6 @@ Description=Firmware update daemon
 Documentation=https://fwupd.org/
 After=dbus.service
 Before=display-manager.service
-ConditionVirtualization=!container

 [Service]
 Type=dbus
```

Anyone think of a cleaner way to do that such that we can still let autopkgtest run?  I guess there is always sed in the autopkgtest job script.
#4310 hopefully solves it.
> @simondeziel I backported your fix since you initially filed it in debian I figured you wanted it sooner. I think your fix might have caused the failure though :/

Oops, sorry for this and thanks for the autopkgtest workaround!