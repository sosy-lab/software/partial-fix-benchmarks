diff --git a/plugins/dell/README.md b/plugins/dell/README.md
index 9fbefa43cdb..c1225bef249 100644
--- a/plugins/dell/README.md
+++ b/plugins/dell/README.md
@@ -50,8 +50,7 @@ If you don't want or need this functionality you can use the
 
 ## Devices powered by the Dell Plugin
 
-The Dell plugin creates device nodes for PC's that have switchable TPMs as
-well as the Type-C docks (WD15/TB16).
+The Dell plugin creates device nodes for PC's with upgradable TPMs.
 
 These device nodes can be flashed using UEFI capsule but don't
 use the ESRT table to communicate device status or version information.
@@ -59,103 +58,6 @@ use the ESRT table to communicate device status or version information.
 This is intentional behavior because more complicated decisions need to be made
 on the OS side to determine if the devices should be offered to flash.
 
-## Switchable TPM Devices
-
-Machines with switchable TPMs can operate in both TPM 1.2 and TPM 2.0 modes.
-Switching modes will require flashing an alternative firmware and clearing the
-contents of the TPM.
-
-Machines that offer this functionality will display two devices in
-`fwupdmgr get-devices` output.
-
-Example (from a *Precision 5510*):
-
-```text
-Precision 5510 TPM 1.2
-  Guid:                 b2088ba1-51ae-514e-8f0a-64756c6e4ffc
-  DeviceID:             DELL-b2088ba1-51ae-514e-8f0a-64756c6e4ffclu
-  Plugin:               dell
-  Flags:                internal|allow-offline|require-ac
-  Version:              5.81.0.0
-  Created:              2016-07-19
-
-Precision 5510 TPM 2.0
-  Guid:                 475d9bbd-1b7a-554e-8ca7-54985174a962
-  DeviceID:             DELL-475d9bbd-1b7a-554e-8ca7-54985174a962lu
-  Plugin:               dell
-  Flags:                internal|require-ac|locked
-  Created:              2016-07-19
-```
-
-In this example, the TPM is currently operating in **TPM 1.2 mode**.  Any
-firmware updates posted to *LVFS* for TPM 1.2 mode will be applied.
-
-### Switching TPM Modes
-
-In order to be offered to switch the TPM to **TPM 2.0 mode**, the virtual device
-representing the *TPM 2.0 mode* will need to be unlocked.
-
-```# fwupdmgr unlock DELL-475d9bbd-1b7a-554e-8ca7-54985174a962lu```
-
-If the TPM is currently *owned*, an error will be displayed such as this one:
-
- ERROR: Precision 5510 TPM 1.2 is currently OWNED. Ownership must be removed to switch modes.
-
-TPM Ownership can be cleared from within the BIOS setup menus.
-
-If the unlock process was successful, then the devices will be modified:
-
-```text
-Precision 5510 TPM 1.2
-  Guid:                 b2088ba1-51ae-514e-8f0a-64756c6e4ffc
-  DeviceID:             DELL-b2088ba1-51ae-514e-8f0a-64756c6e4ffclu
-  Plugin:               dell
-  Flags:                internal|require-ac
-  Version:              5.81.0.0
-  Created:              2016-07-19
-
-Precision 5510 TPM 2.0
-  Guid:                 475d9bbd-1b7a-554e-8ca7-54985174a962
-  DeviceID:             DELL-475d9bbd-1b7a-554e-8ca7-54985174a962lu
-  Plugin:               dell
-  Flags:                internal|allow-offline|require-ac
-  Version:              0.0.0.0
-  Created:              2016-07-19
-  Modified:             2016-07-19
-```
-
-Now the firmware for TPM 2.0 mode can be pulled down from LVFS and flashed:
-
-```shell
-# fwupdmgr update
-```
-
-Upon the next reboot, the new TPM firmware will be flashed.  If the firmware is
-*not offered from LVFS*, then switching modes may not work on this machine.
-
-After updating the output from ```# fwupdmgr get-devices```  will reflect the
-new mode.
-
-```text
-Precision 5510 TPM 2.0
-  Guid:                 475d9bbd-1b7a-554e-8ca7-54985174a962
-  DeviceID:             DELL-475d9bbd-1b7a-554e-8ca7-54985174a962lu
-  Plugin:               dell
-  Flags:                internal|allow-offline|require-ac
-  Version:              1.3.0.1
-  Created:              2016-07-20
-
-Precision 5510 TPM 1.2
-  Guid:                 b2088ba1-51ae-514e-8f0a-64756c6e4ffc
-  DeviceID:             DELL-b2088ba1-51ae-514e-8f0a-64756c6e4ffclu
-  Plugin:               dell
-  Flags:                internal|require-ac|locked
-  Created:              2016-07-20
-```
-
-Keep in mind that **TPM 1.2** and **TPM 2.0** will require different userspace
-tools.
-
 ## External Interface Access
 
 This plugin requires read/write access to `/dev/wmi/dell-smbios` and `/sys/bus/platform/devices/dcdbas`.
diff --git a/plugins/dell/fu-dell-plugin.c b/plugins/dell/fu-dell-plugin.c
index d27d5faa203..3b15c3b804d 100644
--- a/plugins/dell/fu-dell-plugin.c
+++ b/plugins/dell/fu-dell-plugin.c
@@ -24,7 +24,6 @@ struct _FuDellPlugin {
 	FuDellSmiObj *smi_obj;
 	guint16 fake_vid;
 	guint16 fake_pid;
-	gboolean can_switch_modes;
 	gboolean capsule_supported;
 };
 
@@ -57,14 +56,6 @@ struct da_structure {
 	guint8 *tokens;
 } __attribute__((packed));
 
-/**
- * Devices that should allow modeswitching
- */
-static guint16 tpm_switch_allowlist[] = {
-    0x06F2, 0x06F3, 0x06DD, 0x06DE, 0x06DF, 0x06DB, 0x06DC, 0x06BB, 0x06C6, 0x06BA, 0x06B9, 0x05CA,
-    0x06C7, 0x06B7, 0x06E0, 0x06E5, 0x06D9, 0x06DA, 0x06E4, 0x0704, 0x0720, 0x0730, 0x0758, 0x0759,
-    0x075B, 0x07A0, 0x079F, 0x07A4, 0x07A5, 0x07A6, 0x07A7, 0x07A8, 0x07A9, 0x07AA, 0x07AB, 0x07B0,
-    0x07B1, 0x07B2, 0x07B4, 0x07B7, 0x07B8, 0x07B9, 0x07BE, 0x07BF, 0x077A, 0x07CF};
 /**
  * Dell device types to run
  */
@@ -168,8 +159,7 @@ fu_dell_plugin_inject_fake_data(FuPlugin *plugin,
 				guint32 *output,
 				guint16 vid,
 				guint16 pid,
-				guint8 *buf,
-				gboolean can_switch_modes)
+				guint8 *buf)
 {
 	FuDellPlugin *self = FU_DELL_PLUGIN(plugin);
 	if (!self->smi_obj->fake_smbios)
@@ -179,7 +169,6 @@ fu_dell_plugin_inject_fake_data(FuPlugin *plugin,
 	self->fake_vid = vid;
 	self->fake_pid = pid;
 	self->smi_obj->fake_buffer = buf;
-	self->can_switch_modes = TRUE;
 }
 
 static gboolean
@@ -411,19 +400,12 @@ fu_dell_plugin_detect_tpm(FuPlugin *plugin, GError **error)
 	FuDellPlugin *self = FU_DELL_PLUGIN(plugin);
 	FuContext *ctx = fu_plugin_get_context(plugin);
 	const gchar *tpm_mode;
-	const gchar *tpm_mode_alt;
 	guint16 system_id = 0;
-	gboolean can_switch_modes = FALSE;
-	g_autofree gchar *pretty_tpm_name_alt = NULL;
 	g_autofree gchar *pretty_tpm_name = NULL;
-	g_autofree gchar *tpm_guid_raw_alt = NULL;
-	g_autofree gchar *tpm_guid_alt = NULL;
 	g_autofree gchar *tpm_guid = NULL;
 	g_autofree gchar *tpm_guid_raw = NULL;
-	g_autofree gchar *tpm_id_alt = NULL;
 	g_autofree gchar *version_str = NULL;
 	struct tpm_status *out = NULL;
-	g_autoptr(FuDevice) dev_alt = NULL;
 	g_autoptr(FuDevice) dev = NULL;
 	g_autoptr(GError) error_tss = NULL;
 
@@ -466,10 +448,8 @@ fu_dell_plugin_detect_tpm(FuPlugin *plugin, GError **error)
 	/* test TPM mode to determine current mode */
 	if (((out->status & TPM_TYPE_MASK) >> 8) == TPM_1_2_MODE) {
 		tpm_mode = "1.2";
-		tpm_mode_alt = "2.0";
 	} else if (((out->status & TPM_TYPE_MASK) >> 8) == TPM_2_0_MODE) {
 		tpm_mode = "2.0";
-		tpm_mode_alt = "1.2";
 	} else {
 		g_set_error_literal(error,
 				    G_IO_ERROR,
@@ -479,34 +459,19 @@ fu_dell_plugin_detect_tpm(FuPlugin *plugin, GError **error)
 	}
 
 	system_id = fu_dell_get_system_id(plugin);
-	if (self->smi_obj->fake_smbios) {
-		can_switch_modes = self->can_switch_modes;
-	} else if (system_id == 0) {
+	if (!self->smi_obj->fake_smbios && system_id == 0) {
 		g_set_error_literal(error, G_IO_ERROR, G_IO_ERROR_NOT_SUPPORTED, "no system ID");
 		return FALSE;
 	}
 
-	for (guint i = 0; i < G_N_ELEMENTS(tpm_switch_allowlist); i++) {
-		if (tpm_switch_allowlist[i] == system_id) {
-			can_switch_modes = TRUE;
-		}
-	}
-
 	tpm_guid_raw = g_strdup_printf("%04x-%s", system_id, tpm_mode);
 	tpm_guid = fwupd_guid_hash_string(tpm_guid_raw);
 
-	tpm_guid_raw_alt = g_strdup_printf("%04x-%s", system_id, tpm_mode_alt);
-	tpm_guid_alt = fwupd_guid_hash_string(tpm_guid_raw_alt);
-	tpm_id_alt = g_strdup_printf("DELL-%s" G_GUINT64_FORMAT, tpm_guid_alt);
-
-	g_debug("Creating primary TPM GUID %s and secondary TPM GUID %s",
-		tpm_guid_raw,
-		tpm_guid_raw_alt);
+	g_debug("Creating TPM GUID %s", tpm_guid_raw);
 	version_str = fu_version_from_uint32(out->fw_version, FWUPD_VERSION_FORMAT_QUAD);
 
 	/* make it clear that the TPM is a discrete device of the product */
 	pretty_tpm_name = g_strdup_printf("TPM %s", tpm_mode);
-	pretty_tpm_name_alt = g_strdup_printf("TPM %s", tpm_mode_alt);
 
 	/* build Standard device nodes */
 	dev = fu_device_new(ctx);
@@ -544,42 +509,6 @@ fu_dell_plugin_detect_tpm(FuPlugin *plugin, GError **error)
 				      "TpmFamily",
 				      fu_device_get_metadata(dev, "TpmFamily"));
 
-	/* build alternate device node */
-	if (can_switch_modes) {
-		dev_alt = fu_device_new(ctx);
-		fu_device_set_id(dev_alt, tpm_id_alt);
-		fu_device_add_instance_id(dev_alt, tpm_guid_raw_alt);
-		fu_device_set_vendor(dev, "Dell Inc.");
-		fu_device_add_vendor_id(dev, "PCI:0x1028");
-		fu_device_set_name(dev_alt, pretty_tpm_name_alt);
-		fu_device_set_summary(dev_alt, "Alternate mode for platform TPM device");
-		fu_device_add_flag(dev_alt, FWUPD_DEVICE_FLAG_INTERNAL);
-		fu_device_add_flag(dev_alt, FWUPD_DEVICE_FLAG_REQUIRE_AC);
-		fu_device_add_flag(dev_alt, FWUPD_DEVICE_FLAG_LOCKED);
-		fu_device_add_icon(dev_alt, "computer");
-		fu_device_set_alternate_id(dev_alt, fu_device_get_id(dev));
-		fu_device_set_metadata(dev_alt,
-				       FU_DEVICE_METADATA_UEFI_DEVICE_KIND,
-				       "dell-tpm-firmware");
-		fu_device_add_parent_guid(dev_alt, tpm_guid);
-
-		/* If TPM is not owned and at least 1 flash left allow mode switching
-		 *
-		 * Mode switching is turned on by setting flashes left on alternate
-		 * device.
-		 */
-		if ((out->status & TPM_OWN_MASK) == 0 && out->flashes_left > 0) {
-			fu_device_set_flashes_left(dev_alt, out->flashes_left);
-		} else {
-			fu_device_set_update_error(dev_alt,
-						   "mode switch disabled due to TPM ownership");
-		}
-		if (!fu_device_setup(dev_alt, error))
-			return FALSE;
-		fu_plugin_device_register(plugin, dev_alt);
-	} else
-		g_debug("System %04x does not offer TPM modeswitching", system_id);
-
 	return TRUE;
 }
 
@@ -612,7 +541,6 @@ static void
 fu_dell_plugin_to_string(FuPlugin *plugin, guint idt, GString *str)
 {
 	FuDellPlugin *self = FU_DELL_PLUGIN(plugin);
-	fu_string_append_kb(str, idt, "CanSwitchModes", self->can_switch_modes);
 	fu_string_append_kb(str, idt, "CapsuleSupported", self->capsule_supported);
 }
 
diff --git a/plugins/dell/fu-plugin-dell.h b/plugins/dell/fu-plugin-dell.h
index 36987aaf69f..d197a65392a 100644
--- a/plugins/dell/fu-plugin-dell.h
+++ b/plugins/dell/fu-plugin-dell.h
@@ -15,8 +15,7 @@ fu_dell_plugin_inject_fake_data(FuPlugin *plugin,
 				guint32 *output,
 				guint16 vid,
 				guint16 pid,
-				guint8 *buf,
-				gboolean can_switch_modes);
+				guint8 *buf);
 
 gboolean
 fu_dell_plugin_detect_tpm(FuPlugin *plugin, GError **error);
diff --git a/plugins/dell/fu-self-test.c b/plugins/dell/fu-self-test.c
index f7d745af5a3..445c403258a 100644
--- a/plugins/dell/fu-self-test.c
+++ b/plugins/dell/fu-self-test.c
@@ -108,7 +108,7 @@ fu_dell_plugin_tpm_func(gconstpointer user_data)
 
 	/* inject fake data (no TPM) */
 	tpm_out.ret = -2;
-	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL, FALSE);
+	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL);
 	ret = fu_dell_plugin_detect_tpm(self->plugin_dell, &error);
 	g_assert_error(error, G_IO_ERROR, G_IO_ERROR_NOT_SUPPORTED);
 	g_assert_false(ret);
@@ -125,27 +125,16 @@ fu_dell_plugin_tpm_func(gconstpointer user_data)
 	tpm_out.fw_version = 0;
 	tpm_out.status = TPM_EN_MASK | (TPM_1_2_MODE << 8);
 	tpm_out.flashes_left = 0;
-	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL, TRUE);
+	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL);
 	ret = fu_dell_plugin_detect_tpm(self->plugin_dell, &error);
 	g_assert_true(ret);
-	g_assert_cmpint(devices->len, ==, 2);
-
-	/* make sure 2.0 is locked */
-	device_v20 = _find_device_by_name(devices, "TPM 2.0");
-	g_assert_nonnull(device_v20);
-	g_assert_true(fu_device_has_flag(device_v20, FWUPD_DEVICE_FLAG_LOCKED));
+	g_assert_cmpint(devices->len, ==, 1);
 
 	/* make sure not allowed to flash 1.2 */
 	device_v12 = _find_device_by_name(devices, "TPM 1.2");
 	g_assert_nonnull(device_v12);
 	g_assert_false(fu_device_has_flag(device_v12, FWUPD_DEVICE_FLAG_UPDATABLE));
 
-	/* try to unlock 2.0 */
-	ret = fu_plugin_runner_unlock(self->plugin_uefi_capsule, device_v20, &error);
-	g_assert_error(error, FWUPD_ERROR, FWUPD_ERROR_NOT_SUPPORTED);
-	g_assert_false(ret);
-	g_clear_error(&error);
-
 	/* cleanup */
 	g_ptr_array_set_size(devices, 0);
 
@@ -157,7 +146,7 @@ fu_dell_plugin_tpm_func(gconstpointer user_data)
 	 */
 	tpm_out.status = TPM_EN_MASK | TPM_OWN_MASK | (TPM_1_2_MODE << 8);
 	tpm_out.flashes_left = 125;
-	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL, TRUE);
+	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL);
 	ret = fu_dell_plugin_detect_tpm(self->plugin_dell, &error);
 	g_assert_no_error(error);
 	g_assert_true(ret);
@@ -167,12 +156,6 @@ fu_dell_plugin_tpm_func(gconstpointer user_data)
 	g_assert_nonnull(device_v12);
 	g_assert_false(fu_device_has_flag(device_v12, FWUPD_DEVICE_FLAG_UPDATABLE));
 
-	/* try to unlock 2.0 */
-	device_v20 = _find_device_by_name(devices, "TPM 2.0");
-	g_assert_nonnull(device_v20);
-	ret = fu_plugin_runner_unlock(self->plugin_uefi_capsule, device_v20, &error);
-	g_assert_error(error, FWUPD_ERROR, FWUPD_ERROR_NOT_SUPPORTED);
-	g_assert_false(ret);
 	g_clear_error(&error);
 
 	/* cleanup */
@@ -186,27 +169,15 @@ fu_dell_plugin_tpm_func(gconstpointer user_data)
 	 */
 	tpm_out.status = TPM_EN_MASK | (TPM_1_2_MODE << 8);
 	tpm_out.flashes_left = 125;
-	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL, TRUE);
+	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL);
 	ret = fu_dell_plugin_detect_tpm(self->plugin_dell, &error);
 	g_assert_no_error(error);
 	g_assert_true(ret);
 
-	/* make sure allowed to flash 1.2 but not 2.0 */
+	/* make sure allowed to flash */
 	device_v12 = _find_device_by_name(devices, "TPM 1.2");
 	g_assert_nonnull(device_v12);
 	g_assert_true(fu_device_has_flag(device_v12, FWUPD_DEVICE_FLAG_UPDATABLE));
-	device_v20 = _find_device_by_name(devices, "TPM 2.0");
-	g_assert_nonnull(device_v20);
-	g_assert_false(fu_device_has_flag(device_v20, FWUPD_DEVICE_FLAG_UPDATABLE));
-
-	/* try to unlock 2.0 */
-	ret = fu_plugin_runner_unlock(self->plugin_uefi_capsule, device_v20, &error);
-	g_assert_no_error(error);
-	g_assert_true(ret);
-
-	/* make sure no longer allowed to flash 1.2 but can flash 2.0 */
-	g_assert_false(fu_device_has_flag(device_v12, FWUPD_DEVICE_FLAG_UPDATABLE));
-	g_assert_true(fu_device_has_flag(device_v20, FWUPD_DEVICE_FLAG_UPDATABLE));
 
 	/* cleanup */
 	g_ptr_array_set_size(devices, 0);
@@ -219,18 +190,15 @@ fu_dell_plugin_tpm_func(gconstpointer user_data)
 	 */
 	tpm_out.status = TPM_EN_MASK | (TPM_2_0_MODE << 8);
 	tpm_out.flashes_left = 1;
-	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL, TRUE);
+	fu_dell_plugin_inject_fake_data(self->plugin_dell, (guint32 *)&tpm_out, 0, 0, NULL);
 	ret = fu_dell_plugin_detect_tpm(self->plugin_dell, &error);
 	g_assert_no_error(error);
 	g_assert_true(ret);
 
-	/* make sure allowed to flash 2.0 but not 1.2 */
+	/* make sure allowed to flash */
 	device_v20 = _find_device_by_name(devices, "TPM 2.0");
 	g_assert_nonnull(device_v20);
 	g_assert_true(fu_device_has_flag(device_v20, FWUPD_DEVICE_FLAG_UPDATABLE));
-	device_v12 = _find_device_by_name(devices, "TPM 1.2");
-	g_assert_nonnull(device_v12);
-	g_assert_false(fu_device_has_flag(device_v12, FWUPD_DEVICE_FLAG_UPDATABLE));
 
 	/* ensure flags set */
 	ret = fu_device_probe(device_v20, &error);
