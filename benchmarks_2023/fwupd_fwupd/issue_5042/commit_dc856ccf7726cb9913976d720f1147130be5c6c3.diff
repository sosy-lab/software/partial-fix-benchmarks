diff --git a/contrib/ci/check-headers.py b/contrib/ci/check-headers.py
index 82c0e18e5a5..897b27e129f 100755
--- a/contrib/ci/check-headers.py
+++ b/contrib/ci/check-headers.py
@@ -66,7 +66,7 @@ def test_files() -> int:
         if (
             fn.startswith("plugins")
             and not fn.endswith("self-test.c")
-            and not fn.endswith("-tool.c")
+            and not fn.endswith("tool.c")
         ):
             for include in includes:
                 # check for using private header use in plugins
diff --git a/contrib/migrate.py b/contrib/migrate.py
index 83770305bc2..f4e90a42f03 100755
--- a/contrib/migrate.py
+++ b/contrib/migrate.py
@@ -125,7 +125,7 @@
             "fu_common_get_volume_by_device": "fu_volume_new_by_device",
             "fu_common_get_volume_by_devnum": "fu_volume_new_by_devnum",
             "fu_common_get_esp_for_path": "fu_volume_new_esp_for_path",
-            "fu_common_get_esp_default": "fu_volume_new_esp_default",
+            "fu_common_get_esp_default": "fu_context_get_esp_volumes",
             "fu_smbios_to_string": "fu_firmware_to_string",
             "fu_i2c_device_read_full": "fu_i2c_device_read",
             "fu_i2c_device_write_full": "fu_i2c_device_write",
diff --git a/data/daemon.conf b/data/daemon.conf
index 6ac825249cf..64fed17b397 100644
--- a/data/daemon.conf
+++ b/data/daemon.conf
@@ -60,6 +60,10 @@ TrustedUids=
 # config level. e.g. `vendor-factory-2021q1`
 HostBkc=
 
+# The EFI system partition (ESP) path used if UDisks is not available
+# or if this partition is not mounted at /boot/efi, /boot, or /efi
+#EspLocation=
+
 # these are only required when the SMBIOS or Device Tree data is invalid or missing
 #Manufacturer=
 #ProductName=
diff --git a/docs/env.md b/docs/env.md
index 8abc4adcb96..794c0de1a2c 100644
--- a/docs/env.md
+++ b/docs/env.md
@@ -87,7 +87,6 @@ for details.
 * `FWUPD_SYSFSFWDIR`
 * `FWUPD_SYSFSSECURITYDIR`
 * `FWUPD_SYSFSTPMDIR`
-* `FWUPD_UEFI_ESP_PATH`
 * `HOME`
 * `RUNTIME_DIRECTORY`
 * `SNAP`
diff --git a/libfwupdplugin/fu-context-private.h b/libfwupdplugin/fu-context-private.h
index 41c0e0cf8fa..5e14bbd166b 100644
--- a/libfwupdplugin/fu-context-private.h
+++ b/libfwupdplugin/fu-context-private.h
@@ -9,6 +9,7 @@
 #include "fu-context.h"
 #include "fu-hwids.h"
 #include "fu-quirks.h"
+#include "fu-volume.h"
 
 FuContext *
 fu_context_new(void);
@@ -32,3 +33,5 @@ void
 fu_context_add_udev_subsystem(FuContext *self, const gchar *subsystem);
 GPtrArray *
 fu_context_get_udev_subsystems(FuContext *self);
+void
+fu_context_add_esp_volume(FuContext *self, FuVolume *volume);
diff --git a/libfwupdplugin/fu-context.c b/libfwupdplugin/fu-context.c
index a3a17718f06..58144383d3b 100644
--- a/libfwupdplugin/fu-context.c
+++ b/libfwupdplugin/fu-context.c
@@ -12,6 +12,7 @@
 #include "fu-context-private.h"
 #include "fu-hwids.h"
 #include "fu-smbios-private.h"
+#include "fu-volume.h"
 
 /**
  * FuContext:
@@ -28,6 +29,7 @@ typedef struct {
 	GHashTable *runtime_versions;
 	GHashTable *compile_versions;
 	GPtrArray *udev_subsystems;
+	GPtrArray *esp_volumes;
 	GHashTable *firmware_gtypes;
 	GHashTable *hwid_flags; /* str: */
 	FuBatteryState battery_state;
@@ -867,6 +869,102 @@ fu_context_has_flag(FuContext *context, FuContextFlags flag)
 	return (priv->flags & flag) > 0;
 }
 
+/**
+ * fu_context_add_esp_volume:
+ * @context: a #FuContext
+ * @volume: a #FuVolume
+ *
+ * Adds an ESP volume location.
+ *
+ * Since: 1.8.5
+ **/
+void
+fu_context_add_esp_volume(FuContext *self, FuVolume *volume)
+{
+	FuContextPrivate *priv = GET_PRIVATE(self);
+
+	g_return_if_fail(FU_IS_CONTEXT(self));
+	g_return_if_fail(FU_IS_VOLUME(volume));
+
+	/* check for dupes */
+	for (guint i = 0; i < priv->esp_volumes->len; i++) {
+		FuVolume *volume_tmp = g_ptr_array_index(priv->esp_volumes, i);
+		if (g_strcmp0(fu_volume_get_id(volume_tmp), fu_volume_get_id(volume)) == 0) {
+			g_debug("not adding duplicate volume %s", fu_volume_get_id(volume));
+			return;
+		}
+	}
+
+	/* add */
+	g_ptr_array_add(priv->esp_volumes, g_object_ref(volume));
+}
+
+/**
+ * fu_context_get_esp_volumes:
+ * @context: a #FuContext
+ * @error: (nullable): optional return location for an error
+ *
+ * Finds all volumes that could be an ESP.
+ *
+ * The volumes are cached and so subsequent calls to this function will be much faster.
+ *
+ * Returns: (transfer container) (element-type FuVolume): a #GPtrArray, or %NULL if no ESP was found
+ *
+ * Since: 1.8.5
+ **/
+GPtrArray *
+fu_context_get_esp_volumes(FuContext *self, GError **error)
+{
+	FuContextPrivate *priv = GET_PRIVATE(self);
+	g_autoptr(GError) error_bdp = NULL;
+	g_autoptr(GError) error_esp = NULL;
+	g_autoptr(GPtrArray) volumes_bdp = NULL;
+	g_autoptr(GPtrArray) volumes_esp = NULL;
+
+	g_return_val_if_fail(FU_IS_CONTEXT(self), NULL);
+	g_return_val_if_fail(error == NULL || *error == NULL, NULL);
+
+	/* cached result */
+	if (priv->esp_volumes->len > 0)
+		return g_ptr_array_ref(priv->esp_volumes);
+
+	/* ESP */
+	volumes_esp = fu_volume_new_by_kind(FU_VOLUME_KIND_ESP, &error_esp);
+	if (volumes_esp == NULL) {
+		g_debug("%s", error_esp->message);
+	} else {
+		for (guint i = 0; i < volumes_esp->len; i++) {
+			FuVolume *vol = g_ptr_array_index(volumes_esp, i);
+			fu_context_add_esp_volume(self, vol);
+		}
+	}
+
+	/* BDP */
+	volumes_bdp = fu_volume_new_by_kind(FU_VOLUME_KIND_BDP, &error_bdp);
+	if (volumes_bdp == NULL) {
+		g_debug("%s", error_bdp->message);
+	} else {
+		for (guint i = 0; i < volumes_bdp->len; i++) {
+			FuVolume *vol = g_ptr_array_index(volumes_bdp, i);
+			g_autofree gchar *type = fu_volume_get_id_type(vol);
+			if (g_strcmp0(type, "vfat") != 0)
+				continue;
+			if (!fu_volume_is_internal(vol))
+				continue;
+			fu_context_add_esp_volume(self, vol);
+		}
+	}
+
+	/* nothing found */
+	if (priv->esp_volumes->len == 0) {
+		g_set_error_literal(error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND, "No ESP or BDP found");
+		return NULL;
+	}
+
+	/* success */
+	return g_ptr_array_ref(priv->esp_volumes);
+}
+
 static void
 fu_context_get_property(GObject *object, guint prop_id, GValue *value, GParamSpec *pspec)
 {
@@ -931,6 +1029,7 @@ fu_context_finalize(GObject *object)
 	g_object_unref(priv->host_bios_settings);
 	g_hash_table_unref(priv->firmware_gtypes);
 	g_ptr_array_unref(priv->udev_subsystems);
+	g_ptr_array_unref(priv->esp_volumes);
 
 	G_OBJECT_CLASS(fu_context_parent_class)->finalize(object);
 }
@@ -1044,6 +1143,7 @@ fu_context_init(FuContext *self)
 	priv->firmware_gtypes = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, NULL);
 	priv->quirks = fu_quirks_new();
 	priv->host_bios_settings = fu_bios_settings_new();
+	priv->esp_volumes = g_ptr_array_new_with_free_func((GDestroyNotify)g_object_unref);
 }
 
 /**
diff --git a/libfwupdplugin/fu-context.h b/libfwupdplugin/fu-context.h
index 53c4587f177..e88607e4343 100644
--- a/libfwupdplugin/fu-context.h
+++ b/libfwupdplugin/fu-context.h
@@ -123,3 +123,6 @@ gboolean
 fu_context_get_bios_setting_pending_reboot(FuContext *self);
 FwupdBiosSetting *
 fu_context_get_bios_setting(FuContext *self, const gchar *name);
+
+GPtrArray *
+fu_context_get_esp_volumes(FuContext *self, GError **error) G_GNUC_WARN_UNUSED_RESULT;
diff --git a/libfwupdplugin/fu-volume.c b/libfwupdplugin/fu-volume.c
index 6d21a00a76e..f13536271b1 100644
--- a/libfwupdplugin/fu-volume.c
+++ b/libfwupdplugin/fu-volume.c
@@ -445,7 +445,16 @@ fu_volume_locker(FuVolume *self, GError **error)
 					 error);
 }
 
-/* private */
+/**
+ * fu_volume_new_from_mount_path:
+ * @mount_path: a mount path, typically a string
+ *
+ * Creates a volume with a specified mount path. Usually used internally or from the self tests.
+ *
+ * Returns: (transfer full): a #FuVolume
+ *
+ * Since: 1.8.5
+ **/
 FuVolume *
 fu_volume_new_from_mount_path(const gchar *mount_path)
 {
@@ -647,7 +656,9 @@ fu_volume_new_by_devnum(guint32 devnum, GError **error)
  * fu_volume_new_esp_default:
  * @error: (nullable): optional return location for an error
  *
- * Gets the platform default ESP
+ * Gets the platform default ESP.
+ *
+ * NOTE: This has been deprecated, please use fu_context_get_esp_volumes() instead.
  *
  * Returns: (transfer full): a volume, or %NULL if the ESP was not found
  *
@@ -656,7 +667,6 @@ fu_volume_new_by_devnum(guint32 devnum, GError **error)
 FuVolume *
 fu_volume_new_esp_default(GError **error)
 {
-	const gchar *path_tmp;
 	gboolean has_internal = FALSE;
 	g_autoptr(GPtrArray) volumes_fstab = g_ptr_array_new();
 	g_autoptr(GPtrArray) volumes_mtab = g_ptr_array_new();
@@ -666,11 +676,6 @@ fu_volume_new_esp_default(GError **error)
 
 	g_return_val_if_fail(error == NULL || *error == NULL, NULL);
 
-	/* for the test suite use local directory for ESP */
-	path_tmp = g_getenv("FWUPD_UEFI_ESP_PATH");
-	if (path_tmp != NULL)
-		return fu_volume_new_from_mount_path(path_tmp);
-
 	volumes = fu_volume_new_by_kind(FU_VOLUME_KIND_ESP, &error_local);
 	if (volumes == NULL) {
 		g_debug("%s, falling back to %s", error_local->message, FU_VOLUME_KIND_BDP);
@@ -755,8 +760,11 @@ fu_volume_new_esp_for_path(const gchar *esp_path, GError **error)
 	basename = g_path_get_basename(esp_path);
 	for (guint i = 0; i < volumes->len; i++) {
 		FuVolume *vol = g_ptr_array_index(volumes, i);
-		g_autofree gchar *vol_basename =
-		    g_path_get_basename(fu_volume_get_mount_point(vol));
+		const gchar *mount_point = fu_volume_get_mount_point(vol);
+		g_autofree gchar *vol_basename = NULL;
+		if (mount_point == NULL)
+			continue;
+		vol_basename = g_path_get_basename(mount_point);
 		if (g_strcmp0(basename, vol_basename) == 0)
 			return g_object_ref(vol);
 	}
@@ -767,60 +775,3 @@ fu_volume_new_esp_for_path(const gchar *esp_path, GError **error)
 		    esp_path);
 	return NULL;
 }
-
-/**
- * fu_volume_new_by_esp:
- * @error: (nullable): optional return location for an error
- *
- * Finds all volumes that could be an ESP.
- *
- * Returns: (transfer container) (element-type FuVolume): a #GPtrArray, or %NULL if no ESP was found
- *
- * Since: 1.8.5
- **/
-GPtrArray *
-fu_volume_new_by_esp(GError **error)
-{
-	g_autoptr(GError) error_bdp = NULL;
-	g_autoptr(GError) error_esp = NULL;
-	g_autoptr(GPtrArray) volumes_bdp = NULL;
-	g_autoptr(GPtrArray) volumes_esp = NULL;
-	g_autoptr(GPtrArray) volumes =
-	    g_ptr_array_new_with_free_func((GDestroyNotify)g_object_unref);
-
-	/* ESP */
-	volumes_esp = fu_volume_new_by_kind(FU_VOLUME_KIND_ESP, &error_esp);
-	if (volumes_esp == NULL) {
-		g_debug("%s", error_esp->message);
-	} else {
-		for (guint i = 0; i < volumes_esp->len; i++) {
-			FuVolume *vol = g_ptr_array_index(volumes_esp, i);
-			g_ptr_array_add(volumes, g_object_ref(vol));
-		}
-	}
-
-	/* BDP */
-	volumes_bdp = fu_volume_new_by_kind(FU_VOLUME_KIND_BDP, &error_bdp);
-	if (volumes_bdp == NULL) {
-		g_debug("%s", error_bdp->message);
-	} else {
-		for (guint i = 0; i < volumes_bdp->len; i++) {
-			FuVolume *vol = g_ptr_array_index(volumes_bdp, i);
-			g_autofree gchar *type = fu_volume_get_id_type(vol);
-			if (g_strcmp0(type, "vfat") != 0)
-				continue;
-			if (!fu_volume_is_internal(vol))
-				continue;
-			g_ptr_array_add(volumes, g_object_ref(vol));
-		}
-	}
-
-	/* nothing found */
-	if (volumes->len == 0) {
-		g_set_error_literal(error, G_IO_ERROR, G_IO_ERROR_NOT_FOUND, "No ESP or BDP found");
-		return NULL;
-	}
-
-	/* success */
-	return g_steal_pointer(&volumes);
-}
diff --git a/libfwupdplugin/fu-volume.h b/libfwupdplugin/fu-volume.h
index bf0f60e786e..7984ba1cf85 100644
--- a/libfwupdplugin/fu-volume.h
+++ b/libfwupdplugin/fu-volume.h
@@ -60,7 +60,7 @@ FuVolume *
 fu_volume_new_by_devnum(guint32 devnum, GError **error) G_GNUC_WARN_UNUSED_RESULT;
 FuVolume *
 fu_volume_new_esp_for_path(const gchar *esp_path, GError **error) G_GNUC_WARN_UNUSED_RESULT;
+
+G_DEPRECATED_FOR(fu_context_get_esp_volumes)
 FuVolume *
 fu_volume_new_esp_default(GError **error) G_GNUC_WARN_UNUSED_RESULT;
-GPtrArray *
-fu_volume_new_by_esp(GError **error) G_GNUC_WARN_UNUSED_RESULT;
diff --git a/libfwupdplugin/fwupdplugin.map b/libfwupdplugin/fwupdplugin.map
index c7f3182949c..c9116ef7261 100644
--- a/libfwupdplugin/fwupdplugin.map
+++ b/libfwupdplugin/fwupdplugin.map
@@ -1100,7 +1100,9 @@ LIBFWUPDPLUGIN_1.8.5 {
   global:
     fu_backend_load;
     fu_backend_save;
+    fu_context_add_esp_volume;
     fu_context_add_flag;
+    fu_context_get_esp_volumes;
     fu_context_has_flag;
     fu_device_add_backend_tag;
     fu_device_get_backend_tags;
@@ -1125,6 +1127,6 @@ LIBFWUPDPLUGIN_1.8.5 {
     fu_usb_device_fw_ds20_new;
     fu_usb_device_ms_ds20_get_type;
     fu_usb_device_ms_ds20_new;
-    fu_volume_new_by_esp;
+    fu_volume_new_from_mount_path;
   local: *;
 } LIBFWUPDPLUGIN_1.8.4;
diff --git a/libfwupdplugin/meson.build b/libfwupdplugin/meson.build
index 9505104c586..4d0ea901b83 100644
--- a/libfwupdplugin/meson.build
+++ b/libfwupdplugin/meson.build
@@ -209,6 +209,7 @@ fwupdplugin_headers_private = [
   'fu-usb-device-fw-ds20.h',
   'fu-usb-device-ms-ds20.h',
   'fu-usb-device-private.h',
+  'fu-volume-private.h',
   fwupdplugin_version_h,
 ]
 
diff --git a/plugins/dell/fu-self-test.c b/plugins/dell/fu-self-test.c
index 5e1fbcb0579..661399dbb1b 100644
--- a/plugins/dell/fu-self-test.c
+++ b/plugins/dell/fu-self-test.c
@@ -15,6 +15,7 @@
 #include "fu-device-private.h"
 #include "fu-plugin-dell.h"
 #include "fu-plugin-private.h"
+#include "fu-volume-private.h"
 
 typedef struct {
 	FuPlugin *plugin_uefi_capsule;
@@ -511,6 +512,14 @@ fu_test_self_init(FuTest *self)
 	g_autoptr(GError) error = NULL;
 	g_autofree gchar *pluginfn_uefi = NULL;
 	g_autofree gchar *pluginfn_dell = NULL;
+	g_autofree gchar *sysfsdir = NULL;
+	g_autoptr(FuVolume) volume = NULL;
+
+	/* override ESP */
+	sysfsdir = fu_path_from_kind(FU_PATH_KIND_SYSFSDIR_FW);
+	volume = fu_volume_new_from_mount_path(sysfsdir);
+	g_assert_nonnull(volume);
+	fu_context_add_esp_volume(ctx, volume);
 
 	/* do not save silo */
 	ret = fu_context_load_quirks(ctx, FU_QUIRKS_LOAD_FLAG_NO_CACHE, &error);
@@ -559,7 +568,6 @@ G_DEFINE_AUTOPTR_CLEANUP_FUNC(FuTest, fu_test_self_free)
 int
 main(int argc, char **argv)
 {
-	g_autofree gchar *sysfsdir = NULL;
 	g_autofree gchar *testdatadir = NULL;
 	g_autoptr(FuTest) self = g_new0(FuTest, 1);
 
@@ -571,8 +579,6 @@ main(int argc, char **argv)
 	(void)g_setenv("FWUPD_SYSFSFWATTRIBDIR", testdatadir, TRUE);
 
 	/* change behavior */
-	sysfsdir = fu_path_from_kind(FU_PATH_KIND_SYSFSDIR_FW);
-	(void)g_setenv("FWUPD_UEFI_ESP_PATH", sysfsdir, TRUE);
 	(void)g_setenv("FWUPD_UEFI_TEST", "1", TRUE);
 	(void)g_setenv("FWUPD_DELL_FAKE_SMBIOS", "1", FALSE);
 
diff --git a/plugins/lenovo-thinklmi/fu-self-test.c b/plugins/lenovo-thinklmi/fu-self-test.c
index 402431f7d5f..357164be806 100644
--- a/plugins/lenovo-thinklmi/fu-self-test.c
+++ b/plugins/lenovo-thinklmi/fu-self-test.c
@@ -15,6 +15,7 @@
 #include "fu-context-private.h"
 #include "fu-device-private.h"
 #include "fu-plugin-private.h"
+#include "fu-volume-private.h"
 
 typedef struct {
 	FuContext *ctx;
@@ -38,9 +39,17 @@ fu_test_self_init(FuTest *self, GError **error_global)
 	g_autoptr(GError) error = NULL;
 	g_autofree gchar *pluginfn_uefi = NULL;
 	g_autofree gchar *pluginfn_lenovo = NULL;
+	g_autofree gchar *sysfsdir = NULL;
+	g_autoptr(FuVolume) volume = NULL;
 
 	g_test_expect_message("FuBiosSettings", G_LOG_LEVEL_WARNING, "*KERNEL*BUG*");
 
+	/* override ESP */
+	sysfsdir = fu_path_from_kind(FU_PATH_KIND_SYSFSDIR_FW);
+	volume = fu_volume_new_from_mount_path(sysfsdir);
+	g_assert_nonnull(volume);
+	fu_context_add_esp_volume(ctx, volume);
+
 	ret = fu_context_load_quirks(ctx,
 				     FU_QUIRKS_LOAD_FLAG_NO_CACHE | FU_QUIRKS_LOAD_FLAG_NO_VERIFY,
 				     &error);
@@ -191,7 +200,6 @@ G_DEFINE_AUTOPTR_CLEANUP_FUNC(FuTest, fu_test_self_free)
 int
 main(int argc, char **argv)
 {
-	g_autofree gchar *sysfsdir = NULL;
 	g_autofree gchar *testdatadir = NULL;
 	g_autofree gchar *confdir = NULL;
 	g_autofree gchar *test_dir = NULL;
@@ -210,8 +218,6 @@ main(int argc, char **argv)
 	(void)g_setenv("FWUPD_SYSFSFWDIR", testdatadir, TRUE);
 
 	/* change behavior of UEFI plugin for test mode */
-	sysfsdir = fu_path_from_kind(FU_PATH_KIND_SYSFSDIR_FW);
-	(void)g_setenv("FWUPD_UEFI_ESP_PATH", sysfsdir, TRUE);
 	(void)g_setenv("FWUPD_UEFI_TEST", "1", TRUE);
 
 	/* to load daemon.conf */
diff --git a/plugins/uefi-capsule/README.md b/plugins/uefi-capsule/README.md
index d43c4debdd8..311650f5ff0 100644
--- a/plugins/uefi-capsule/README.md
+++ b/plugins/uefi-capsule/README.md
@@ -106,7 +106,7 @@ support on the device by using the `unlock` command.
 Since version 1.1.0 fwupd will autodetect the ESP if it is mounted on
 `/boot/efi`, `/boot`, or `/efi`, and UDisks is available on the system. In
 other cases the mount point of the ESP needs to be manually specified using the
-option *OverrideESPMountPoint* in `/etc/fwupd/uefi_capsule.conf`.
+option *EspLocation* in `/etc/fwupd/daemon.conf`.
 
 Setting an invalid directory will disable the fwupd plugin.
 
diff --git a/plugins/uefi-capsule/fu-plugin-uefi-capsule.c b/plugins/uefi-capsule/fu-plugin-uefi-capsule.c
index 11c361baed4..dd5e891657c 100644
--- a/plugins/uefi-capsule/fu-plugin-uefi-capsule.c
+++ b/plugins/uefi-capsule/fu-plugin-uefi-capsule.c
@@ -500,6 +500,16 @@ fu_plugin_uefi_capsule_load_config(FuPlugin *plugin, FuDevice *device)
 		fu_device_add_private_flag(device, FU_UEFI_DEVICE_FLAG_FALLBACK_TO_REMOVABLE_PATH);
 }
 
+static FuVolume *
+fu_plugin_uefi_capsule_get_default_esp(FuPlugin *plugin, GError **error)
+{
+	g_autoptr(GPtrArray) esp_volumes = NULL;
+	esp_volumes = fu_context_get_esp_volumes(fu_plugin_get_context(plugin), error);
+	if (esp_volumes == NULL)
+		return NULL;
+	return g_object_ref(g_ptr_array_index(esp_volumes, 0));
+}
+
 static void
 fu_plugin_uefi_capsule_register_proxy_device(FuPlugin *plugin, FuDevice *device)
 {
@@ -511,7 +521,7 @@ fu_plugin_uefi_capsule_register_proxy_device(FuPlugin *plugin, FuDevice *device)
 	dev = fu_uefi_backend_device_new_from_dev(FU_UEFI_BACKEND(priv->backend), device);
 	fu_plugin_uefi_capsule_load_config(plugin, FU_DEVICE(dev));
 	if (priv->esp == NULL)
-		priv->esp = fu_volume_new_esp_default(&error_local);
+		priv->esp = fu_plugin_uefi_capsule_get_default_esp(plugin, &error_local);
 	if (priv->esp == NULL) {
 		fu_device_inhibit(device, "no-esp", error_local->message);
 	} else {
@@ -860,7 +870,7 @@ fu_plugin_uefi_capsule_coldplug(FuPlugin *plugin, FuProgress *progress, GError *
 	fu_progress_add_step(progress, FWUPD_STATUS_LOADING, 1, "setup-bgrt");
 
 	if (priv->esp == NULL) {
-		priv->esp = fu_volume_new_esp_default(&error_udisks2);
+		priv->esp = fu_plugin_uefi_capsule_get_default_esp(plugin, &error_udisks2);
 		if (priv->esp == NULL) {
 			fu_plugin_add_flag(plugin, FWUPD_PLUGIN_FLAG_ESP_NOT_FOUND);
 			fu_plugin_add_flag(plugin, FWUPD_PLUGIN_FLAG_CLEAR_UPDATABLE);
diff --git a/plugins/uefi-capsule/fu-uefi-tool.c b/plugins/uefi-capsule/fu-uefi-tool.c
index 78d07133dbf..5852d3d3abc 100644
--- a/plugins/uefi-capsule/fu-uefi-tool.c
+++ b/plugins/uefi-capsule/fu-uefi-tool.c
@@ -72,6 +72,8 @@ main(int argc, char *argv[])
 	g_autoptr(FuUtilPrivate) priv = g_new0(FuUtilPrivate, 1);
 	g_autoptr(GError) error = NULL;
 	g_autoptr(GPtrArray) devices = NULL;
+	g_autoptr(GPtrArray) esp_volumes = NULL;
+	g_autoptr(FuContext) ctx = fu_context_new();
 	g_autoptr(FuVolume) esp = NULL;
 	const GOptionEntry options[] = {
 	    {"verbose",
@@ -234,19 +236,23 @@ main(int argc, char *argv[])
 
 	/* override the default ESP path */
 	if (esp_path != NULL) {
-		esp = fu_volume_new_esp_for_path(esp_path, &error);
-		if (esp == NULL) {
+		g_autoptr(FuVolume) volume = NULL;
+		volume = fu_volume_new_esp_for_path(esp_path, &error);
+		if (volume == NULL) {
 			/* TRANSLATORS: ESP is EFI System Partition */
 			g_print("%s: %s\n", _("ESP specified was not valid"), error->message);
 			return EXIT_FAILURE;
 		}
-	} else {
-		esp = fu_volume_new_esp_default(&error);
-		if (esp == NULL) {
-			g_printerr("failed: %s\n", error->message);
-			return EXIT_FAILURE;
-		}
+		fu_context_add_esp_volume(ctx, volume);
+	}
+
+	/* get the default ESP only */
+	esp_volumes = fu_context_get_esp_volumes(ctx, &error);
+	if (esp_volumes == NULL) {
+		g_printerr("%s\n", error->message);
+		return EXIT_FAILURE;
 	}
+	esp = g_object_ref(g_ptr_array_index(esp_volumes, 0));
 
 	/* show the debug action_log from the last attempted update */
 	if (action_log) {
@@ -275,7 +281,6 @@ main(int argc, char *argv[])
 	}
 
 	if (action_list || action_supported || action_info) {
-		g_autoptr(FuContext) ctx = fu_context_new();
 		g_autoptr(FuBackend) backend = fu_uefi_backend_new(ctx);
 		g_autoptr(FuProgress) progress = fu_progress_new(G_STRLOC);
 		g_autoptr(GError) error_local = NULL;
@@ -395,7 +400,6 @@ main(int argc, char *argv[])
 
 	/* apply firmware updates */
 	if (apply != NULL) {
-		g_autoptr(FuContext) ctx = fu_context_new();
 		g_autoptr(FuBackend) backend = fu_uefi_backend_new(ctx);
 		g_autoptr(FuProgress) progress = fu_progress_new(G_STRLOC);
 		g_autoptr(FuUefiDevice) dev = NULL;
diff --git a/plugins/uefi-capsule/uefi_capsule.conf b/plugins/uefi-capsule/uefi_capsule.conf
index 1fc5ef4d28e..6ab87e82598 100644
--- a/plugins/uefi-capsule/uefi_capsule.conf
+++ b/plugins/uefi-capsule/uefi_capsule.conf
@@ -9,6 +9,8 @@
 
 # the EFI system partition (ESP) path used if UDisks is not available
 # or if this partition is not mounted at /boot/efi, /boot, or /efi
+#
+# NOTE: this is now unused; use the ESPMountPoint value in daemon.conf
 #OverrideESPMountPoint=
 
 # amount of free space required on the ESP, for example using 0x2000000 for 32Mb
diff --git a/plugins/uefi-dbx/fu-dbxtool.c b/plugins/uefi-dbx/fu-dbxtool.c
index 7a258f998e4..ddd94622b0f 100644
--- a/plugins/uefi-dbx/fu-dbxtool.c
+++ b/plugins/uefi-dbx/fu-dbxtool.c
@@ -15,6 +15,7 @@
 #include <stdlib.h>
 #include <unistd.h>
 
+#include "fu-context-private.h"
 #include "fu-uefi-dbx-common.h"
 
 /* custom return code */
@@ -95,8 +96,10 @@ main(int argc, char *argv[])
 	gboolean verbose = FALSE;
 	g_autofree gchar *dbxfile = NULL;
 	g_autoptr(GError) error = NULL;
+	g_autoptr(FuContext) ctx = fu_context_new();
 	g_autoptr(GOptionContext) context = NULL;
 	g_autofree gchar *tmp = NULL;
+	g_autofree gchar *esp_path = NULL;
 	const GOptionEntry options[] = {
 	    {"verbose",
 	     'v',
@@ -139,6 +142,15 @@ main(int argc, char *argv[])
 	     N_("Specify the dbx database file"),
 	     /* TRANSLATORS: command argument: uppercase, spaces->dashes */
 	     N_("FILENAME")},
+	    {"esp-path",
+	     'p',
+	     G_OPTION_FLAG_NONE,
+	     G_OPTION_ARG_STRING,
+	     &esp_path,
+	     /* TRANSLATORS: command line option */
+	     N_("Override the default ESP path"),
+	     /* TRANSLATORS: command argument: uppercase, spaces->dashes */
+	     N_("PATH")},
 	    {"force",
 	     'f',
 	     0,
@@ -178,6 +190,18 @@ main(int argc, char *argv[])
 		g_log_set_handler(G_LOG_DOMAIN, G_LOG_LEVEL_DEBUG, fu_util_ignore_cb, NULL);
 	}
 
+	/* override the default ESP path */
+	if (esp_path != NULL) {
+		g_autoptr(FuVolume) volume = NULL;
+		volume = fu_volume_new_esp_for_path(esp_path, &error);
+		if (volume == NULL) {
+			/* TRANSLATORS: ESP is EFI System Partition */
+			g_print("%s: %s\n", _("ESP specified was not valid"), error->message);
+			return EXIT_FAILURE;
+		}
+		fu_context_add_esp_volume(ctx, volume);
+	}
+
 	/* list contents, either of the existing system, or an update */
 	if (action_list || action_version) {
 		guint cnt = 1;
@@ -285,7 +309,8 @@ main(int argc, char *argv[])
 		if (!force) {
 			/* TRANSLATORS: ESP refers to the EFI System Partition */
 			g_print("%s\n", _("Validating ESP contents…"));
-			if (!fu_uefi_dbx_signature_list_validate(FU_EFI_SIGNATURE_LIST(dbx_update),
+			if (!fu_uefi_dbx_signature_list_validate(ctx,
+								 FU_EFI_SIGNATURE_LIST(dbx_update),
 								 &error)) {
 				g_printerr("%s: %s\n",
 					   /* TRANSLATORS: something with a blocked hash exists
diff --git a/plugins/uefi-dbx/fu-uefi-dbx-common.c b/plugins/uefi-dbx/fu-uefi-dbx-common.c
index 731b4a36f53..b7f291439e2 100644
--- a/plugins/uefi-dbx/fu-uefi-dbx-common.c
+++ b/plugins/uefi-dbx/fu-uefi-dbx-common.c
@@ -80,10 +80,10 @@ fu_uefi_dbx_signature_list_validate_volume(FuEfiSignatureList *siglist,
 }
 
 gboolean
-fu_uefi_dbx_signature_list_validate(FuEfiSignatureList *siglist, GError **error)
+fu_uefi_dbx_signature_list_validate(FuContext *ctx, FuEfiSignatureList *siglist, GError **error)
 {
 	g_autoptr(GPtrArray) volumes = NULL;
-	volumes = fu_volume_new_by_esp(error);
+	volumes = fu_context_get_esp_volumes(ctx, error);
 	if (volumes == NULL)
 		return FALSE;
 	for (guint i = 0; i < volumes->len; i++) {
diff --git a/plugins/uefi-dbx/fu-uefi-dbx-common.h b/plugins/uefi-dbx/fu-uefi-dbx-common.h
index c844470d0c5..af7390dd289 100644
--- a/plugins/uefi-dbx/fu-uefi-dbx-common.h
+++ b/plugins/uefi-dbx/fu-uefi-dbx-common.h
@@ -11,4 +11,4 @@
 gchar *
 fu_uefi_dbx_get_authenticode_hash(const gchar *fn, GError **error);
 gboolean
-fu_uefi_dbx_signature_list_validate(FuEfiSignatureList *siglist, GError **error);
+fu_uefi_dbx_signature_list_validate(FuContext *ctx, FuEfiSignatureList *siglist, GError **error);
diff --git a/plugins/uefi-dbx/fu-uefi-dbx-device.c b/plugins/uefi-dbx/fu-uefi-dbx-device.c
index ed1275824f0..01107c10300 100644
--- a/plugins/uefi-dbx/fu-uefi-dbx-device.c
+++ b/plugins/uefi-dbx/fu-uefi-dbx-device.c
@@ -73,6 +73,7 @@ fu_uefi_dbx_device_set_version_number(FuDevice *device, GError **error)
 static FuFirmware *
 fu_uefi_dbx_prepare_firmware(FuDevice *device, GBytes *fw, FwupdInstallFlags flags, GError **error)
 {
+	FuContext *ctx = fu_device_get_context(device);
 	g_autoptr(FuFirmware) siglist = fu_efi_signature_list_new();
 
 	/* parse dbx */
@@ -82,7 +83,9 @@ fu_uefi_dbx_prepare_firmware(FuDevice *device, GBytes *fw, FwupdInstallFlags fla
 	/* validate this is safe to apply */
 	if ((flags & FWUPD_INSTALL_FLAG_FORCE) == 0) {
 		//		fu_progress_set_status(progress, FWUPD_STATUS_DEVICE_VERIFY);
-		if (!fu_uefi_dbx_signature_list_validate(FU_EFI_SIGNATURE_LIST(siglist), error)) {
+		if (!fu_uefi_dbx_signature_list_validate(ctx,
+							 FU_EFI_SIGNATURE_LIST(siglist),
+							 error)) {
 			g_prefix_error(error,
 				       "Blocked executable in the ESP, "
 				       "ensure grub and shim are up to date: ");
diff --git a/src/fu-config.c b/src/fu-config.c
index 815abb21104..0aa702eb51b 100644
--- a/src/fu-config.c
+++ b/src/fu-config.c
@@ -32,6 +32,7 @@ struct _FuConfig {
 	guint64 archive_size_max;
 	guint idle_timeout;
 	gchar *host_bkc;
+	gchar *esp_location;
 	gboolean update_motd;
 	gboolean enumerate_all_devices;
 	gboolean ignore_power;
@@ -61,6 +62,7 @@ fu_config_reload(FuConfig *self, GError **error)
 	g_auto(GStrv) plugins = NULL;
 	g_autofree gchar *domains = NULL;
 	g_autofree gchar *host_bkc = NULL;
+	g_autofree gchar *esp_location = NULL;
 	g_autoptr(GKeyFile) keyfile = g_key_file_new();
 	g_autoptr(GError) error_update_motd = NULL;
 	g_autoptr(GError) error_ignore_power = NULL;
@@ -241,6 +243,11 @@ fu_config_reload(FuConfig *self, GError **error)
 	if (host_bkc != NULL && host_bkc[0] != '\0')
 		self->host_bkc = g_steal_pointer(&host_bkc);
 
+	/* fetch hardcoded ESP mountpoint */
+	esp_location = g_key_file_get_string(keyfile, "fwupd", "EspLocation", NULL);
+	if (esp_location != NULL && esp_location[0] != '\0')
+		self->esp_location = g_steal_pointer(&esp_location);
+
 	/* get trusted uids */
 	g_array_set_size(self->trusted_uids, 0);
 	uids = g_key_file_get_string_list(keyfile,
@@ -444,6 +451,13 @@ fu_config_get_host_bkc(FuConfig *self)
 	return self->host_bkc;
 }
 
+const gchar *
+fu_config_get_esp_location(FuConfig *self)
+{
+	g_return_val_if_fail(FU_IS_CONFIG(self), NULL);
+	return self->esp_location;
+}
+
 static void
 fu_config_class_init(FuConfigClass *klass)
 {
@@ -499,6 +513,7 @@ fu_config_finalize(GObject *obj)
 	g_ptr_array_unref(self->uri_schemes);
 	g_array_unref(self->trusted_uids);
 	g_free(self->host_bkc);
+	g_free(self->esp_location);
 
 	G_OBJECT_CLASS(fu_config_parent_class)->finalize(obj);
 }
diff --git a/src/fu-config.h b/src/fu-config.h
index 015296acd25..1d5fcad2be5 100644
--- a/src/fu-config.h
+++ b/src/fu-config.h
@@ -46,3 +46,5 @@ gboolean
 fu_config_get_show_device_private(FuConfig *self);
 const gchar *
 fu_config_get_host_bkc(FuConfig *self);
+const gchar *
+fu_config_get_esp_location(FuConfig *self);
diff --git a/src/fu-engine.c b/src/fu-engine.c
index e1cc1654f45..d577bd4a1ca 100644
--- a/src/fu-engine.c
+++ b/src/fu-engine.c
@@ -4114,6 +4114,18 @@ static void
 fu_engine_config_changed_cb(FuConfig *config, FuEngine *self)
 {
 	fu_idle_set_timeout(self->idle, fu_config_get_idle_timeout(config));
+
+	/* allow changing the hardcoded ESP location */
+	if (fu_config_get_esp_location(config) != NULL) {
+		g_autoptr(GError) error = NULL;
+		g_autoptr(FuVolume) vol = NULL;
+		vol = fu_volume_new_esp_for_path(fu_config_get_esp_location(config), &error);
+		if (vol == NULL) {
+			g_warning("not adding changed EspLocation: %s", error->message);
+		} else {
+			fu_context_add_esp_volume(self->ctx, vol);
+		}
+	}
 }
 
 static void
@@ -7565,6 +7577,7 @@ fu_engine_backends_coldplug(FuEngine *self, FuProgress *progress)
 gboolean
 fu_engine_load(FuEngine *self, FuEngineLoadFlags flags, FuProgress *progress, GError **error)
 {
+	FuPlugin *plugin_uefi;
 	FuQuirksLoadFlags quirks_flags = FU_QUIRKS_LOAD_FLAG_NONE;
 	GPtrArray *guids;
 	const gchar *host_emulate = g_getenv("FWUPD_HOST_EMULATE");
@@ -7644,6 +7657,15 @@ fu_engine_load(FuEngine *self, FuEngineLoadFlags flags, FuProgress *progress, GE
 	}
 	fu_progress_step_done(progress);
 
+	/* set the hardcoded ESP */
+	if (fu_config_get_esp_location(self->config) != NULL) {
+		g_autoptr(FuVolume) vol = NULL;
+		vol = fu_volume_new_esp_for_path(fu_config_get_esp_location(self->config), error);
+		if (vol == NULL)
+			return FALSE;
+		fu_context_add_esp_volume(self->ctx, vol);
+	}
+
 	/* read remotes */
 	if (flags & FU_ENGINE_LOAD_FLAG_REMOTES) {
 		FuRemoteListLoadFlags remote_list_flags = FU_REMOTE_LIST_LOAD_FLAG_NONE;
@@ -7698,6 +7720,17 @@ fu_engine_load(FuEngine *self, FuEngineLoadFlags flags, FuProgress *progress, GE
 	}
 	fu_progress_step_done(progress);
 
+	/* migrate per-plugin settings into daemon.conf */
+	plugin_uefi = fu_plugin_list_find_by_name(self->plugin_list, "uefi_capsule", NULL);
+	if (plugin_uefi != NULL) {
+		const gchar *tmp = fu_plugin_get_config_value(plugin_uefi, "OverrideESPMountPoint");
+		if (tmp != NULL && g_strcmp0(tmp, fu_config_get_esp_location(self->config)) != 0) {
+			g_debug("migrating OverrideESPMountPoint=%s to EspLocation", tmp);
+			if (!fu_config_set_key_value(self->config, "EspLocation", tmp, error))
+				return FALSE;
+		}
+	}
+
 	/* set up idle exit */
 	if ((flags & FU_ENGINE_LOAD_FLAG_NO_IDLE_SOURCES) == 0)
 		fu_idle_set_timeout(self->idle, fu_config_get_idle_timeout(self->config));
diff --git a/src/fu-tool.c b/src/fu-tool.c
index 275297252a7..f52b5026dcb 100644
--- a/src/fu-tool.c
+++ b/src/fu-tool.c
@@ -2996,14 +2996,15 @@ fu_util_security(FuUtilPrivate *priv, gchar **values, GError **error)
 }
 
 static FuVolume *
-fu_util_prompt_for_volume(GError **error)
+fu_util_prompt_for_volume(FuUtilPrivate *priv, GError **error)
 {
+	FuContext *ctx = fu_engine_get_context(priv->engine);
 	FuVolume *volume;
 	guint idx;
 	g_autoptr(GPtrArray) volumes = NULL;
 
 	/* exactly one */
-	volumes = fu_volume_new_by_esp(error);
+	volumes = fu_context_get_esp_volumes(ctx, error);
 	if (volumes == NULL)
 		return NULL;
 	if (volumes->len == 1) {
@@ -3037,7 +3038,7 @@ static gboolean
 fu_util_esp_mount(FuUtilPrivate *priv, gchar **values, GError **error)
 {
 	g_autoptr(FuVolume) volume = NULL;
-	volume = fu_util_prompt_for_volume(error);
+	volume = fu_util_prompt_for_volume(priv, error);
 	if (volume == NULL)
 		return FALSE;
 	return fu_volume_mount(volume, error);
@@ -3047,7 +3048,7 @@ static gboolean
 fu_util_esp_unmount(FuUtilPrivate *priv, gchar **values, GError **error)
 {
 	g_autoptr(FuVolume) volume = NULL;
-	volume = fu_util_prompt_for_volume(error);
+	volume = fu_util_prompt_for_volume(priv, error);
 	if (volume == NULL)
 		return FALSE;
 	return fu_volume_unmount(volume, error);
@@ -3061,7 +3062,7 @@ fu_util_esp_list(FuUtilPrivate *priv, gchar **values, GError **error)
 	g_autoptr(FuVolume) volume = NULL;
 	g_autoptr(GPtrArray) files = NULL;
 
-	volume = fu_util_prompt_for_volume(error);
+	volume = fu_util_prompt_for_volume(priv, error);
 	if (volume == NULL)
 		return FALSE;
 	locker = fu_volume_locker(volume, error);
