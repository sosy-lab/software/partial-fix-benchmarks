unable to install update with ESP on raid1 
```
fwupdtool esp-list --verbose
02:57:02:0107 FuDebug              Verbose debugging enabled (on console 1)
02:57:02:0119 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme0n1p1, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0120 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme1n1p3, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0121 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme1n1p2, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0123 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme1n1p1, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0125 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme0n1p3, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0126 FuVolume             device /org/freedesktop/UDisks2/block_devices/sda1, type: 0fc63daf-8483-4772-8e79-3d69d8477de4, internal: 0, fs: xfs
02:57:02:0128 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme0n1p2, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0129 FuVolume             no volumes of type c12a7328-f81f-11d2-ba4b-00a0c93ec93b
02:57:02:0141 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme0n1p1, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0143 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme1n1p3, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0145 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme1n1p2, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0147 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme1n1p1, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0148 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme0n1p3, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0149 FuVolume             device /org/freedesktop/UDisks2/block_devices/sda1, type: 0fc63daf-8483-4772-8e79-3d69d8477de4, internal: 0, fs: xfs
02:57:02:0151 FuVolume             device /org/freedesktop/UDisks2/block_devices/nvme0n1p2, type: a19d880f-05fc-4d3b-a006-743f0f84911e, internal: 1, fs: linux_raid_member
02:57:02:0152 FuVolume             no volumes of type ebd0a0a2-b9e5-4433-87c0-68b6b72699c7
No ESP or BDP found

```
I didn't know you could have a ESP on RAID tbh. @vathpela do you think firmware updates are going to work in reality? I suppose the dbx checker should respect `OverrideESPMountPoint=/boot/efi` regardless....
@superm1 I guess there are a few ways we could solve this:

 * Add a `fu_plugin_get_config_value_full()` that allows us to specify the plugin name, e.g. `fu_plugin_get_config_value_full(plugin, "uefi-capsule", "OverrideESPMountPoint")` which is a bit eww from a layers point of view, although preserves forward and backwards compat
 * Add a `OverrideESPMountPoint` option to daemon.conf and migrate the value in `uefi_capsule.conf` at startup and then have something like `fu_context_get_esp_volume**s**()`
 * Read the `OverrideESPMountPoint` in `uefi_capsule` and set the environment value `FWUPD_UEFI_ESP_PATH` so we can use something like `fu_path_from_kind(FU_PATH_KIND_CUSTOM_ESP_MOUNTPOINT)` -- this is a bit eww as setting env variables at runtime leads to all kinds of funky as getenv returns a pointer, not a dup'd string.

I'm erring on the 2nd option, as this would also mean we could remove `fu_volume_new_by_esp()` and deprecate `fu_volume_new_esp_default()` and `fu_volume_new_esp_for_path()`. Comments welcome!
#2 is my gut too, thanks!
> I didn't know you could have a ESP on RAID tbh. @vathpela do you think firmware updates are going to work in reality? I suppose the dbx checker should respect `OverrideESPMountPoint=/boot/efi` regardless....

In general I think it should work fine; while ESP on RAID 1 is a gigantic hack, it basically works so long as the bootloader doesn't do any writing to the drive. So assuming it's not degraded, updates just need to boot one or the other.
A lot of BIOS vendors have an assumption that they can write to the ESP.  I've seen recovery BIOS image end up there and log dumps end up there.  If the ESP is actually supposed to be RAIDed between two disks this sounds like a recipe for disaster to me.  Wouldn't the BIOS essentially degrade the array?
> A lot of BIOS vendors have an assumption that they can write to the ESP. I've seen recovery BIOS image end up there and log dumps end up there. If the ESP is actually supposed to be RAIDed between two disks this sounds like a recipe for disaster to me. Wouldn't the BIOS essentially degrade the array?

Yeah - for sure if you need this functionality the right thing to do is BIOS RAID, but IBVs continue to make that a value add that isn't in most builds, so people do OS-level RAID instead.

That said, the failure mode for e.g. writing logs out to the ESP is that you can't find the logs, but redundant booting still works.