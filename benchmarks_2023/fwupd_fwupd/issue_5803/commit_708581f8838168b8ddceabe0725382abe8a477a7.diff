diff --git a/libfwupdplugin/fu-efivar.h b/libfwupdplugin/fu-efivar.h
index 893a19a4f2a..48ba5319ef9 100644
--- a/libfwupdplugin/fu-efivar.h
+++ b/libfwupdplugin/fu-efivar.h
@@ -15,6 +15,7 @@
 #define FU_EFIVAR_GUID_SECURITY_DATABASE  "d719b2cb-3d3a-4596-a3bc-dad00e67656f"
 #define FU_EFIVAR_GUID_UX_CAPSULE	  "3b8c8162-188c-46a4-aec9-be43f1d65697"
 #define FU_EFIVAR_GUID_EFI_CAPSULE_REPORT "39b68c46-f7fb-441b-b6ec-16b0f69821f3"
+#define FU_EFIVAR_GUID_SHIM		  "605dab50-e046-4300-abb6-3dd810dd8b23"
 
 #define FU_EFIVAR_ATTR_NON_VOLATILE			     (1 << 0)
 #define FU_EFIVAR_ATTR_BOOTSERVICE_ACCESS		     (1 << 1)
diff --git a/plugins/uefi-capsule/fu-uefi-bootmgr.c b/plugins/uefi-capsule/fu-uefi-bootmgr.c
index a223cc2802e..f2e32ea9357 100644
--- a/plugins/uefi-capsule/fu-uefi-bootmgr.c
+++ b/plugins/uefi-capsule/fu-uefi-bootmgr.c
@@ -249,6 +249,162 @@ fu_uefi_setup_bootnext_with_loadopt(FuEfiLoadOption *loadopt, GError **error)
 	return TRUE;
 }
 
+static GHashTable *
+fu_uefi_bootmgr_parse_csv(const gchar *data, gsize size, GError **error)
+{
+	g_auto(GStrv) lines = NULL;
+	g_autoptr(GHashTable) hash = NULL;
+	g_autofree gchar *nt_data = NULL;
+
+	hash = g_hash_table_new_full(g_str_hash, g_str_equal, g_free, g_free);
+
+	lines = fu_strsplit(data, size, "\n", -1);
+
+	for (guint i = 0; lines[i] != NULL; i++) {
+		g_auto(GStrv) columns = NULL;
+		guint64 generation;
+		g_autofree guint *generation_bucket = NULL;
+
+		if (lines[i][0] == '\0') {
+			/* ignore empty lines */
+			break;
+		}
+
+		columns = g_strsplit(lines[i], ",", 3);
+		if (g_strv_length(columns) < 2) {
+			g_set_error(error,
+				    FWUPD_ERROR,
+				    FWUPD_ERROR_INVALID_FILE,
+				    "invalid CSV data: not enough columns");
+			return NULL;
+		}
+
+		if (!fu_strtoull(columns[1], &generation, 0, G_MAXUINT, error)) {
+			g_set_error(error,
+				    FWUPD_ERROR,
+				    FWUPD_ERROR_INVALID_FILE,
+				    "invalid generation in sbat level for entry %s",
+				    columns[0]);
+			return NULL;
+		}
+		generation_bucket = g_new0(guint, 1);
+		*generation_bucket = (guint)generation;
+		g_hash_table_insert(hash,
+				    g_strdup(columns[0]),
+				    g_steal_pointer(&generation_bucket));
+	}
+
+	return g_steal_pointer(&hash);
+}
+
+static gboolean
+fu_uefi_bootmgr_shim_is_safe(const gchar *source_shim, GError **error)
+{
+	const gchar *sbatlevel_content = NULL;
+	gsize sbatlevel_size = 0;
+	gsize current_sbatlevel_size = 0;
+	GHashTableIter iter;
+	gpointer key, value;
+	g_autofree gchar *sbatlevel_path = NULL;
+	g_autofree gchar *current_sbatlevel = NULL;
+	g_autoptr(GHashTable) shim_hash = NULL;
+	g_autoptr(GHashTable) current_hash = NULL;
+	g_autoptr(FuFirmware) shim = NULL;
+	g_autoptr(FuFirmware) sbatlevel_section = NULL;
+	g_autoptr(FuFirmware) previous_sbatlevel = NULL;
+	g_autoptr(GError) error_local = NULL;
+	g_autoptr(GBytes) blob = NULL;
+	g_autoptr(GBytes) sbatlevel_bytes = NULL;
+
+	shim = fu_pefile_firmware_new();
+	blob = fu_bytes_get_contents(source_shim, error);
+	if (!blob)
+		return FALSE;
+	if (!fu_firmware_parse(shim, blob, FWUPD_INSTALL_FLAG_NONE, error))
+		return FALSE;
+
+	sbatlevel_section = fu_firmware_get_image_by_id(shim, ".sbatlevel", &error_local);
+	if (sbatlevel_section == NULL) {
+		g_debug("No sbatlevel section was found.");
+		/* if there is no .sbatlevel section, then it will not update, it should be safe */
+		if (g_error_matches(error_local, FWUPD_ERROR, FWUPD_ERROR_NOT_FOUND))
+			return TRUE;
+		g_propagate_error(error, g_steal_pointer(&error_local));
+	}
+
+	previous_sbatlevel = fu_firmware_get_image_by_id(sbatlevel_section, "previous", error);
+	if (previous_sbatlevel == NULL)
+		return FALSE;
+
+	sbatlevel_bytes = fu_firmware_get_bytes(previous_sbatlevel, error);
+	if (sbatlevel_bytes == NULL)
+		return FALSE;
+
+	sbatlevel_content = g_bytes_get_data(sbatlevel_bytes, &sbatlevel_size);
+
+	shim_hash = fu_uefi_bootmgr_parse_csv(sbatlevel_content, sbatlevel_size, error);
+	if (shim_hash == NULL)
+		return FALSE;
+
+	/* not safe if variable is not set but new shim would set it */
+	if (!fu_efivar_get_data(FU_EFIVAR_GUID_SHIM,
+				"SbatLevelRT",
+				(guint8 **)&current_sbatlevel,
+				&current_sbatlevel_size,
+				NULL,
+				NULL))
+		return FALSE;
+
+	current_hash = fu_uefi_bootmgr_parse_csv(current_sbatlevel, current_sbatlevel_size, error);
+	if (current_hash == NULL)
+		return FALSE;
+
+	/*
+	 * For every new shim entry, we need a matching entry in the
+	 * current sbatlevel. That is the entry of the shim is not
+	 * newer than current sbatlevel.
+	 *
+	 * The opposite way might work (for example shim's latest
+	 * sbatlevel matches) or not (shim is too old), but it will
+	 * not brick the current OS.
+	 */
+
+	g_hash_table_iter_init(&iter, shim_hash);
+	while (g_hash_table_iter_next(&iter, &key, &value)) {
+		const gchar *shim_entry = (const gchar *)key;
+		const guint *shim_generation = (const guint *)value;
+		const guint *current_generation;
+
+		current_generation = (const guint *)g_hash_table_lookup(current_hash, key);
+
+		if (current_generation == NULL) {
+			g_set_error(error,
+				    FWUPD_ERROR,
+				    FWUPD_ERROR_INVALID_FILE,
+				    "shim sbatlevel for %s has a bricking update for entry %s "
+				    "(missing entry in current UEFI variable)",
+				    source_shim,
+				    shim_entry);
+
+			return FALSE;
+		}
+
+		if (*current_generation < *shim_generation) {
+			g_set_error(error,
+				    FWUPD_ERROR,
+				    FWUPD_ERROR_INVALID_FILE,
+				    "sbatlevel for shim %s has a bricking update for entry "
+				    "%s (newer generation)",
+				    source_shim,
+				    shim_entry);
+
+			return FALSE;
+		}
+	}
+
+	return TRUE;
+}
+
 gboolean
 fu_uefi_bootmgr_bootnext(FuVolume *esp,
 			 const gchar *description,
@@ -286,6 +442,8 @@ fu_uefi_bootmgr_bootnext(FuVolume *esp,
 		source_shim = fu_uefi_get_built_app_path("shim", NULL);
 		if (source_shim != NULL) {
 			if (!fu_uefi_esp_target_verify(source_shim, esp, shim_app)) {
+				if (!fu_uefi_bootmgr_shim_is_safe(source_shim, error))
+					return FALSE;
 				if (!fu_uefi_esp_target_copy(source_shim, esp, shim_app, error))
 					return FALSE;
 			}
