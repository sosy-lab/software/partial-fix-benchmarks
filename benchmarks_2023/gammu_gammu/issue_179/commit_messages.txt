Recognize AT+CIND responses

We still don't parse them, but can properly skip them.

Signed-off-by: Michal Čihař <michal@cihar.com>
Enable encoded USDD for SIMCOM_SIM300D

Fixes #179

Signed-off-by: Michal Čihař <michal@cihar.com>
Add (failing) tests for USSD parsing

Issue #178, #179

Signed-off-by: Michal Čihař <michal@cihar.com>
Fix parsing of USSD responses

The USSD response should be HEX encoded when using HEX encoding on all
devices. On the other side it should be connection charset encoded if
using GSM charset.

Fixes #178
Fixes #179

Signed-off-by: Michal Čihař <michal@cihar.com>
