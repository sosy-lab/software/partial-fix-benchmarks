Let -X -Y also handle multipliers and not just divisors (#6305)

* Let -X -Y also handle multipliers and not just divisors

This PR extends the modern form of -X -Y to allow not just fractions of previous plot dimensions w and h to be used but also arbitrary factors.

* Fix docs
Increase the memory allocation for voronoi polygon coordinates

See #6306 for background.  It is hard to follow what I wrote some years ago regarding building these polygons, but hopefully this change will prevent us from running out of memory - after all these are small polygons.
Increase the memory allocation for voronoi polygon coordinates (#6310)

* Increase the memory allocation for voronoi polygon coordinates

See #6306 for background.  It is hard to follow what I wrote some years ago regarding building these polygons, but hopefully this change will prevent us from running out of memory - after all these are small polygons.

* Update gmt_support.c
Set correct allocation size

While it does not fix the problem in #6306, we should use the right allocation count in the assert statement.
Set correct allocation size (#6331)

While it does not fix the problem in #6306, we should use the right allocation count in the assert statement.
Improve the voronoi polygon building

See #6306 fro background.
Addressing Voronoi polygons in triangulate -Qn (#6334)

* Improve the voronoi polygon building

See #6306 fro background.

* Update gmt_support.c

* Update gmt_support.c

* Update gmt_support.c

* Update gmt_support.c

* Update triangulate.c
