gmt surface gave segfault with pixel-node registration but not gridline-node registration, only on M1 architecture
I test on linux with gmt 6.5 (dev veserion) and it worked for both cases. No one could try with the dev version? 

Reproducible on my M1 MacBook. Crashes in quick _sort_r_ which is a GNU code we provide.  But I no longer remember why. man _qsort_ on my mac shows _qsort_r_.  I will try to re-learn why we did that and why it is always included in the CMakeList.txt set up.
I think we needed a reentrant version
Yes, but is that not qsort_r? 
Don’t remember much too but maybe something after that stupid bug in MSVC that they refused to accept as bug. 
Confirmed. I reverted to a previous branch and the crash went way.
Here are the lines that cause the crash in master
```
julia> l1 = gmt("project -C22/49 -E-60/-20 -G20 -Q");
julia> l2 = gmt("project -C0/-60 -E-60/-30 -G20 -Q");
julia> gmtspatial((l1, l2), I=:e, F="l");
```

If I blindly change the #ifdef logic and change line 4826 to (instead of #ifdef)
```
#ifndef QSORT_R_THUNK_FIRST
```
then the crash goes way.
Hm, will need to stick in some pragma warnings to see which branch it goes into.  The original  **surface** case works OK before **and** after your change?
Didn’t try, but it cannot make any difference. My change affects only the order of arguments for that particular function. 
Could perhaps @anbj can give master a spin and try the surface commands at top.  We believe we have done this correctly for Linux but without building and running we do not know.
Will do. Probably later today. 
I built *master* and ran the commands given in the first post without problems:

```
$ gmt surface xy_vert.txt -Gvert.grd -R-125.8/-122.6/39.3/41.7 -I0.0064 -rg
$ gmt surface xy_vert.txt -Gvert.grd -R-125.8/-122.6/39.3/41.7 -I0.0064 -rp
```
Thanks, was that a BDS or another Linux?
Sorry. Yes Linux. Debian 11. 
Bump (already sedimenting under the minor issues)
Can we close this?