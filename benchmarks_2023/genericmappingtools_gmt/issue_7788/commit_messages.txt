Bump actions/checkout from 3.6.0 to 4.0.0 (#7785)

Bumps [actions/checkout](https://github.com/actions/checkout) from 3.6.0 to 4.0.0.
- [Release notes](https://github.com/actions/checkout/releases)
- [Changelog](https://github.com/actions/checkout/blob/main/CHANGELOG.md)
- [Commits](https://github.com/actions/checkout/compare/v3.6.0...v4.0.0)

---
updated-dependencies:
- dependency-name: actions/checkout
  dependency-type: direct:production
  update-type: version-update:semver-major
...

Signed-off-by: dependabot[bot] <support@github.com>
Co-authored-by: dependabot[bot] <49699333+dependabot[bot]@users.noreply.github.com>
WIP Treat stroke symbols like other symbols

Refer to #7788 for background.  This WIP PR covers psxy and seems to work as explained.  Will do the same for psxyz.
Treat stroke symbols like other symbols (#7789)

* WIP Treat stroke symbols like other symbols

Refer to #7788 for background.  This WIP PR covers psxy and seems to work as explained.  Will do the same for psxyz.

* Same for psxyz

they share the same stroke symbols.

* Only override color it one has been set

* Update the docs for strokable symbols

* Add a constant for the default size to pen width factor

* Update gmt_constants.h

* Fix color assignment if -G or -C given

* Introduce new default MAP_STROKE_WIDTH

* Minor fixes

* Deal with resets after reading symbols from data file

* Fix some PS and scripts

* More PS updates

* Update images.dvc

* fix output name and 2 PS files

* Update psevents.dvc

* Fix inverted CPT

* Increase RMS threshold due to architectures

* typos

running spellchecker
Use MAP_SYMBOL_STROKE_FACTOR instead of MAP_STROKE_WIDTH

See #7788 for discussion at end.  Note the internal variable is map_stroke_width. wo questions:

1. Rename map_stroke_width to map_stroke_factor
2. Consider SCALE instead of FACTOR?
Use MAP_SYMBOL_PEN_SCALE instead of MAP_STROKE_WIDTH (#7822)

* Use MAP_SYMBOL_STROKE_FACTOR instead of MAP_STROKE_WIDTH

See #7788 for discussion at end.  Note the internal variable is map_stroke_width. wo questions:

1. Rename map_stroke_width to map_stroke_factor
2. Consider SCALE instead of FACTOR?

See also here (#7822) how we came to MAP_SYMBOL_PEN_SCALE

* finalize names
