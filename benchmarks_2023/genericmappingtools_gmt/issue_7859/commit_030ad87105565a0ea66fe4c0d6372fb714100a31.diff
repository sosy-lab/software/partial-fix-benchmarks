diff --git a/src/gmt_api.c b/src/gmt_api.c
index d0b3138ca37..fa074ce02c3 100644
--- a/src/gmt_api.c
+++ b/src/gmt_api.c
@@ -9610,7 +9610,8 @@ void * GMT_Read_Data (void *V_API, unsigned int family, unsigned int method, uns
 				/* Maybe using the API without a module call first so server has not been refreshed yet */
 				gmt_refresh_server (API);
 			}
-			gmt_set_unspecified_remote_registration (API, &input);	/* Same, this call otherwise only happens with modules */
+			if (gmt_set_unspecified_remote_registration (API, &input))   /* If argument is a remote file name then this handles any missing registration _p|_g */
+				GMT_Report (API, GMT_MSG_DEBUG, "Revised remote file name to %s\n", input);
 			first = gmt_download_file_if_not_found (API->GMT, input, 0);	/* Deal with downloadable GMT data sets first */
 			strncpy (file, &input[first], PATH_MAX-1);
 			if ((k_data = gmt_remote_no_extension (API, input)) != GMT_NOTSET)	/* A remote @earth_relief_xxm|s grid without extension */
@@ -15997,7 +15998,8 @@ int GMT_Get_FilePath (void *V_API, unsigned int family, unsigned int direction,
 		return GMT_NOERROR;
 	}
 
-	if ((mode & GMT_FILE_CHECK) == 0) gmt_set_unspecified_remote_registration (API, file_ptr);	/* Complete remote filenames without registration information */
+	if ((mode & GMT_FILE_CHECK) == 0 && gmt_set_unspecified_remote_registration (API, file_ptr))	/* Complete remote filenames without registration information */
+		GMT_Report (API, GMT_MSG_DEBUG, "Revised remote file name to %s\n", file_ptr);
 
 	gmt_filename_get (file);	/* Replace any ASCII 29 with spaces (if filename had spaces they may now be ASCII 29) */
 
diff --git a/src/gmt_init.c b/src/gmt_init.c
index b2dadaac21f..0afa79e4987 100644
--- a/src/gmt_init.c
+++ b/src/gmt_init.c
@@ -8937,6 +8937,24 @@ bool gmt_check_region (struct GMT_CTRL *GMT, double wesn[]) {
 		return ((wesn[XLO] >= wesn[XHI] || wesn[YLO] >= wesn[YHI]));
 }
 
+GMT_LOCAL unsigned int gmtinit_might_be_remotefile (char *file) {
+	bool quote = false;	/* We are outside any quoted text */
+	size_t k;
+	static char *text_escapes = "~%:;+-#_!.@[";	/* If any of these follow leading @ it is pstext junk passed as file */
+	if (strchr (file, '@') == NULL) return GMT_IS_NOT_REMOTE;	/* No @ anywhere */
+	if (gmt_M_file_is_memory (file)) return GMT_IS_NOT_REMOTE;	/* Not a remote file but a memory reference */
+	if (file[0] == '@') {
+		if (file[1] && strchr (text_escapes, file[1])) return GMT_FILE_IS_INVALID;	/* text junk not a file */
+		return GMT_IS_REMOTE;	/* Definitively a remote file */
+	}
+	/* Get here when a @ is not in the first position. Return true unless @ is inside quotes */
+	for (k = 0; k < strlen (file); k++) {
+		if (file[k] == '\"' || file[k] == '\'') quote = !quote;
+		if (file[k] == '@' && !quote) return GMT_IS_REMOTE;	/* Found an unquoted at-symbol */
+	}
+	return GMT_IS_NOT_REMOTE;	/* Nothing */
+}
+
 /*! . */
 int gmt_parse_R_option (struct GMT_CTRL *GMT, char *arg) {
 	unsigned int i, icol, pos, error = 0, n_slash = 0, first = 0, x_type, y_type;
@@ -8986,12 +9004,22 @@ int gmt_parse_R_option (struct GMT_CTRL *GMT, char *arg) {
 	got_r = (strstr (item, "+r") != NULL);
 	got_country = (got_r || (strstr (item, "+R") != NULL));	/* May have given DCW (true of +R, maybe if +r since the latter also means oblique) */
 
+	if (gmtinit_might_be_remotefile (item)) {	/* Must check if registration is specified; if not add it */
+		char *tmp = strdup (item);
+		gmt_refresh_server (GMT->parent);
+		if (gmt_set_unspecified_remote_registration (GMT->parent, &tmp)) {	/* If argument is a remote file name then this handles any missing registration _p|_g */
+			GMT_Report (GMT->parent, GMT_MSG_DEBUG, "Option -R: Revised remote file name argument %s to %s\n", item, tmp);
+			strcpy (item, tmp);
+			gmt_M_str_free (tmp);
+		}
+	}
+
 	strncpy (GMT->common.R.string, item, GMT_LEN256-1);	/* Verbatim copy */
 
 	if (gmt_remote_dataset_id (GMT->parent, item) != GMT_NOTSET) {	/* Silly, but user set -R@earth_relief_xxy or similar */
 		/* These are always -Rd */
-		GMT->common.R.wesn[XLO] = -180.0, GMT->common.R.wesn[XHI] = 180.0;
-		GMT->common.R.wesn[YLO] = -90.0;	GMT->common.R.wesn[YHI] = +90.0;
+		GMT->common.R.wesn[XLO] = -180.0;	GMT->common.R.wesn[XHI] = +180.0;
+		GMT->common.R.wesn[YLO] =  -90.0;	GMT->common.R.wesn[YHI] =  +90.0;
 		gmt_set_geographic (GMT, GMT_IN);
 		GMT->current.io.geo.range = GMT_IS_M180_TO_P180_RANGE;
 		return (GMT_NOERROR);
@@ -15256,24 +15284,6 @@ GMT_LOCAL bool gmtinit_mapproject_needs_RJ (struct GMTAPI_CTRL *API, struct GMT_
 	return (true);	/* We get here when a classic command like "gmt mapproject -R -J file" in modern mode looks like "gmt mapproject file" and thus -R -J is required */
 }
 
-GMT_LOCAL unsigned int gmtinit_might_be_remotefile (char *file) {
-	bool quote = false;	/* We are outside any quoted text */
-	size_t k;
-	static char *text_escapes = "~%:;+-#_!.@[";	/* If any of these follow leading @ it is pstext junk passed as file */
-	if (strchr (file, '@') == NULL) return GMT_IS_NOT_REMOTE;	/* No @ anywhere */
-	if (gmt_M_file_is_memory (file)) return GMT_IS_NOT_REMOTE;	/* Not a remote file but a memory reference */
-	if (file[0] == '@') {
-		if (file[1] && strchr (text_escapes, file[1])) return GMT_FILE_IS_INVALID;	/* text junk not a file */
-		return GMT_IS_REMOTE;	/* Definitively a remote file */
-	}
-	/* Get here when a @ is not in the first position. Return true unless @ is inside quotes */
-	for (k = 0; k < strlen (file); k++) {
-		if (file[k] == '\"' || file[k] == '\'') quote = !quote;
-		if (file[k] == '@' && !quote) return GMT_IS_REMOTE;	/* Found an unquoted at-symbol */
-	}
-	return GMT_IS_NOT_REMOTE;	/* Nothing */
-}
-
 /*! . */
 GMT_LOCAL int gmtinit_compare_resolutions (const void *point_1, const void *point_2) {
 	/* Sorts differences from desired nodes-per-degree from small to big  */
@@ -15402,7 +15412,8 @@ struct GMT_CTRL *gmt_init_module (struct GMTAPI_CTRL *API, const char *lib_name,
 				gmt_refresh_server (API);	/* Refresh hash and info tables as needed */
 				remote_first = false;
 			}
-			gmt_set_unspecified_remote_registration (API, &(opt->arg));	/* If argument is a remote file name then this handles any missing registration _p|_g */
+			if (gmt_set_unspecified_remote_registration (API, &(opt->arg)))	/* If argument is a remote file name then this handles any missing registration _p|_g */
+				GMT_Report (API, GMT_MSG_DEBUG, "Revised remote file name to %s\n", opt->arg);
 		}
 	}
 
diff --git a/src/gmt_prototypes.h b/src/gmt_prototypes.h
index a2d1ab7755a..b2bd8dd76c6 100644
--- a/src/gmt_prototypes.h
+++ b/src/gmt_prototypes.h
@@ -207,7 +207,7 @@ EXTERN_MSC int gmt_set_remote_and_local_filenames (struct GMT_CTRL *GMT, const c
 EXTERN_MSC int gmt_remote_dataset_id (struct GMTAPI_CTRL *API, const char *file);
 EXTERN_MSC int gmt_file_is_a_tile (struct GMTAPI_CTRL *API, const char *file, unsigned int where);
 EXTERN_MSC int gmt_remote_no_extension (struct GMTAPI_CTRL *API, const char *file);
-EXTERN_MSC void gmt_set_unspecified_remote_registration (struct GMTAPI_CTRL *API, char **file);
+EXTERN_MSC int gmt_set_unspecified_remote_registration (struct GMTAPI_CTRL *API, char **file);
 EXTERN_MSC char ** gmt_get_dataset_tiles (struct GMTAPI_CTRL *API, double wesn[], int k_data, unsigned int *n_tiles, bool *need_filler);
 EXTERN_MSC bool gmt_file_is_tiled_list (struct GMTAPI_CTRL *API, const char *file, int *ID, char *wetdry, char *region_type);
 EXTERN_MSC int gmt_download_tiles (struct GMTAPI_CTRL *API, char *list, unsigned int mode);
diff --git a/src/gmt_remote.c b/src/gmt_remote.c
index 5dd1f3641e4..0c7c8b79c49 100644
--- a/src/gmt_remote.c
+++ b/src/gmt_remote.c
@@ -523,18 +523,19 @@ bool gmt_file_is_cache (struct GMTAPI_CTRL *API, const char *file) {
 	return true;
 }
 
-void gmt_set_unspecified_remote_registration (struct GMTAPI_CTRL *API, char **file_ptr) {
+int gmt_set_unspecified_remote_registration (struct GMTAPI_CTRL *API, char **file_ptr) {
 	/* If a remote file is missing _g or _p we find which one we should use and revise the file accordingly.
 	 * There are a few different scenarios where this can happen:
 	 * 1. Users of GMT <= 6.0.0 are used to say earth_relief_01m. These will now get p.
-	 * 2. Users who do not care about registration.  If so, they get p if available. */
+	 * 2. Users who do not care about registration.  If so, they get p if available.
+	 * We return 1 if filename was changed, otherwise 0. */
 	char newfile[GMT_LEN256] = {""}, dir[GMT_LEN128] = {""}, reg[2] = {'p', 'g'};
 	char *file = NULL, *infile = NULL, *c = NULL, *p = NULL, *q = NULL;
-	int k_data, k, kstart = 0, kstop = 2, kinc = 1;
+	int k_data, k, kstart = 0, kstop = 2, kinc = 1, reg_added = 0;
 	size_t L;
-	if (file_ptr == NULL || (file = *file_ptr) == NULL || file[0] == '\0') return;
-	if (gmt_M_file_is_memory (file)) return;	/* Not a remote file for sure */
-	if (file[0] != '@') return;
+	if (file_ptr == NULL || (file = *file_ptr) == NULL || file[0] == '\0') return 0;
+	if (gmt_M_file_is_memory (file)) return 0;	/* Not a remote file for sure */
+	if (file[0] != '@') return 0;
 	infile = strdup (file);
 	if ((c = strchr (infile, '+')))	/* Got modifiers, probably from grdimage or similar, chop off for now */
 		c[0] = '\0';
@@ -547,7 +548,7 @@ void gmt_set_unspecified_remote_registration (struct GMTAPI_CTRL *API, char **fi
 	strncpy (dir, API->remote_info[k_data].dir, L);	dir[L] = '\0';	/* Duplicate dir without slash */
 	p = strrchr (dir, '/') + 1;	/* Start of final subdirectory (skipping over the slash we found) */
 	q = strstr (file, p);	/* Start of the file name (most likely the same as &file[1] but we want to make sure) */
-	if (q == NULL) return;	/* Should never happen but definitively nothing more to do here - just a safety valve */
+	if (q == NULL) return 0;	/* Should never happen but definitively nothing more to do here - just a safety valve */
 	q += strlen (p);	/* Move to the end of family name after which any registration codes would be found */
 	if (strstr (q, "_p") || strstr (q, "_g")) goto clean_up;	/* Already have the registration codes */
 	if (API->use_gridline_registration) {	/* Switch order so checking for g first, then p */
@@ -571,11 +572,13 @@ void gmt_set_unspecified_remote_registration (struct GMTAPI_CTRL *API, char **fi
 			*file_ptr = strdup (newfile);
 			API->remote_id = k_data;
 			GMT_Report (API, GMT_MSG_DEBUG, "Input remote grid modified to have registration: %s\n", newfile);
+			reg_added = 1;
 			goto clean_up;
 		}
 	}
 clean_up:
 	gmt_M_str_free (infile);
+	return (reg_added);
 }
 
 int gmt_remote_no_extension (struct GMTAPI_CTRL *API, const char *file) {
