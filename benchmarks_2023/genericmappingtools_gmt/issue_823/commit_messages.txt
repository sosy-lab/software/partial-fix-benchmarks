Let grd2cpt and makecpt set current CPT (#822)

These creators of CPTs now set the output as the current CPT in modern mode.
Let makecpt and grd2cpt get -H under modern mode

See #823 for context.
Update modern mode script given -H option to makecpt/grd2cpt (#827)

* Let makecpt and grd2cpt get -H under modern mode

See #823 for context.

* Using -H

* Update psevents.c

Forgot about pssvents.

* Update modern scripts
Wrap makecpt (#329)

* Wrap makecpt

Initial commit for wrapping the makecpt function raised at #214, tentatively implemented here under the mathops.py file which will hold functions for "Mathematical operations on tables or grids", but with documentation placed under 'Plotting'. Original GMT `makecpt` documentation can be found at https://docs.generic-mapping-tools.org/latest/makecpt.html.

Tests are stored under test_makecpt.py, and there are now some basic tests ensuring that we can change the color when plotting points and grids. Current implementation uses 'cmap' and 'series' as aliases for 'C' and 'T' respectively.

* Enable makecpt to save to a cpt file via output (H) parameter

Allow makecpt to save the generated color palette table to a file via the output (H) argument. The 'H' setting in GMT upstream (see https://docs.generic-mapping-tools.org/latest/makecpt.html#h) is actually a flag to force the creation of an output (to stdout) in modern mode. Here we use it to set the filename too.

See also https://github.com/GenericMappingTools/gmt/pull/827 and https://github.com/GenericMappingTools/gmt/issues/823

* Add truncate (G) alias for makecpt

With rainbow cmap checks to test various zlow/zhigh combinations.

* Add reverse (I) alias for makecpt

Used 'earth' cmap to test various reversed colormap examples.

* Alias continuous (Z) for makecpt

Included one test to create a continuous cpt from blue to white. Also updated link to the full list of GMT's color palette tables due to documentation reorganization during https://github.com/GenericMappingTools/gmt/pull/1594.
Add a gallery example for logo (#823)

Co-authored-by: Will Schlitzer <schlitzer90@gmail.com>
Co-authored-by: Dongdong Tian <seisman.info@gmail.com>
