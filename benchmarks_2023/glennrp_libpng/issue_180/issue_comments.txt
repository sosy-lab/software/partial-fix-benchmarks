Possibly unnecessary condition check
I'm not sure about the original intent, but certainly as it stands the test
is rendundant.  Fixed in libpng16 branch.

Problem seems to exist only in the libpng16 branch.
Reopening this issue because there are two more checks that could be omitted.

File pngrutil.c, line 1588:
`else if (size > 0)`
Unsigned variable is not equal to 0, therefore the condition is always true. Though it could be left as a comment.

File _pngwrite.c_, line 1943:
`int write_16bit = linear && !colormap && (display->convert_to_8bit == 0);`
_!colormap_ was already evaluated in _linear_ variable.
Agreed about !colormap.
The "size > 0" gave some trouble with Coverity so I'm not so sure about that.
I'll make a Coverity run with the change now and see what happens.
Looking at the old code in _pngrutil.c_ it might be expected to see:
 ```
#ifndef __COVERITY__
                                     errmsg = png_ptr->zstream.msg;
#endif
                                 }
                                else
                                    errmsg = "truncated";
```