diff --git a/libglusterfs/src/glusterfs/iobuf.h b/libglusterfs/src/glusterfs/iobuf.h
index 9c6fda6b03c..e3e657c6dd1 100644
--- a/libglusterfs/src/glusterfs/iobuf.h
+++ b/libglusterfs/src/glusterfs/iobuf.h
@@ -64,9 +64,9 @@ struct iobuf {
 
     void *ptr; /* usable memory region by the consumer */
 
-    void *free_ptr; /* in case of stdalloc, this is the
-                       one to be freed */
     size_t page_size;
+    char free_ptr[]; /* in case of stdalloc, this is the
+                       one to be freed */
 };
 
 struct iobuf_arena {
diff --git a/libglusterfs/src/iobuf.c b/libglusterfs/src/iobuf.c
index b45167c152f..8415c2aec26 100644
--- a/libglusterfs/src/iobuf.c
+++ b/libglusterfs/src/iobuf.c
@@ -431,7 +431,6 @@ static void
 __iobuf_free(struct iobuf *iobuf)
 {
     LOCK_DESTROY(&iobuf->lock);
-    GF_FREE(iobuf->free_ptr);
     GF_FREE(iobuf);
 }
 
@@ -441,7 +440,6 @@ iobuf_get_from_stdalloc(struct iobuf_pool *iobuf_pool, const size_t page_size)
     struct iobuf *iobuf = NULL;
     struct iobuf_arena *iobuf_arena = NULL;
     struct iobuf_arena *trav = NULL;
-    int ret = -1;
 
     /* The first arena in the 'MAX-INDEX' will always be used for misc */
     list_for_each_entry(trav, &iobuf_pool->arenas[IOBUF_ARENA_MAX_INDEX], list)
@@ -450,30 +448,19 @@ iobuf_get_from_stdalloc(struct iobuf_pool *iobuf_pool, const size_t page_size)
         break;
     }
 
-    iobuf = GF_CALLOC(1, sizeof(*iobuf), gf_common_mt_iobuf);
+    iobuf = GF_CALLOC(1,
+                      sizeof(*iobuf) + ((page_size + GF_IOBUF_ALIGN_SIZE) - 1),
+                      gf_common_mt_iobuf);
     if (!iobuf)
-        goto out;
-
-    /* 4096 is the alignment */
-    iobuf->free_ptr = GF_CALLOC(1, ((page_size + GF_IOBUF_ALIGN_SIZE) - 1),
-                                gf_common_mt_char);
-    if (!iobuf->free_ptr)
-        goto out;
+        return NULL;
 
-    iobuf->ptr = GF_ALIGN_BUF(iobuf->free_ptr, GF_IOBUF_ALIGN_SIZE);
     iobuf->iobuf_arena = iobuf_arena;
-    iobuf->page_size = page_size;
     LOCK_INIT(&iobuf->lock);
-
     /* Hold a ref because you are allocating and using it */
     GF_ATOMIC_INIT(iobuf->ref, 1);
 
-    ret = 0;
-out:
-    if (ret && iobuf) {
-        __iobuf_free(iobuf);
-        iobuf = NULL;
-    }
+    iobuf->ptr = GF_ALIGN_BUF(iobuf->free_ptr, GF_IOBUF_ALIGN_SIZE);
+    iobuf->page_size = page_size;
 
     return iobuf;
 }
@@ -483,30 +470,18 @@ iobuf_get_from_small(const size_t page_size)
 {
     struct iobuf *iobuf = NULL;
 
-    iobuf = GF_MALLOC(sizeof(*iobuf), gf_common_mt_iobuf);
-    if (!iobuf)
-        goto err;
-
-    iobuf->free_ptr = GF_MALLOC(page_size, gf_common_mt_iobuf_pool);
-    if (!iobuf->free_ptr)
-        goto err;
+    iobuf = GF_MALLOC(sizeof(*iobuf) + page_size, gf_common_mt_iobuf);
+    if (caa_unlikely(!iobuf))
+        return NULL;
 
-    iobuf->ptr = iobuf->free_ptr;
-    iobuf->page_size = page_size;
     INIT_LIST_HEAD(&iobuf->list);
     iobuf->iobuf_arena = NULL;
     LOCK_INIT(&iobuf->lock);
     /* Hold a ref because you are allocating and using it */
     GF_ATOMIC_INIT(iobuf->ref, 1);
-
+    iobuf->ptr = iobuf->free_ptr;
+    iobuf->page_size = page_size;
     return iobuf;
-err:
-    if (iobuf) {
-        GF_FREE(iobuf->free_ptr);
-        GF_FREE(iobuf);
-    }
-
-    return NULL;
 }
 
 struct iobuf *
@@ -586,17 +561,6 @@ iobuf_get_page_aligned(struct iobuf_pool *iobuf_pool, size_t page_size,
     iobuf = iobuf_get2(iobuf_pool, req_size);
     if (!iobuf)
         return NULL;
-    /* If std allocation was used, then free_ptr will be non-NULL. In this
-     * case, we do not want to modify the original free_ptr.
-     * On the other hand, if the buf was gotten through the available
-     * arenas, then we use iobuf->free_ptr to store the original
-     * pointer to the offset into the mmap'd block of memory and in turn
-     * reuse iobuf->ptr to hold the page-aligned address. And finally, in
-     * iobuf_put(), we copy iobuf->free_ptr into iobuf->ptr - back to where
-     * it was originally when __iobuf_get() returned this iobuf.
-     */
-    if (!iobuf->free_ptr)
-        iobuf->free_ptr = iobuf->ptr;
     iobuf->ptr = GF_ALIGN_BUF(iobuf->ptr, align_size);
 
     return iobuf;
@@ -645,11 +609,6 @@ __iobuf_put(struct iobuf *iobuf, struct iobuf_arena *iobuf_arena)
     list_del_init(&iobuf->list);
     iobuf_arena->active_cnt--;
 
-    if (iobuf->free_ptr) {
-        iobuf->ptr = iobuf->free_ptr;
-        iobuf->free_ptr = NULL;
-    }
-
     list_add(&iobuf->list, &iobuf_arena->passive_list);
     iobuf_arena->passive_cnt++;
 
