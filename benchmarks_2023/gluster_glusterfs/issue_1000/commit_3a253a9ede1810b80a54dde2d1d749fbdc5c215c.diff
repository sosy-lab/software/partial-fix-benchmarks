diff --git a/libglusterfs/src/mem-pool.c b/libglusterfs/src/mem-pool.c
index 0657c82afb9..ee62460f06e 100644
--- a/libglusterfs/src/mem-pool.c
+++ b/libglusterfs/src/mem-pool.c
@@ -11,6 +11,7 @@
 #include "glusterfs/mem-pool.h"
 #include "glusterfs/common-utils.h"  // for GF_ASSERT, gf_thread_cr...
 #include "glusterfs/globals.h"       // for xlator_t, THIS
+#include "glusterfs/syscall.h"
 #include <stdlib.h>
 #include <stdarg.h>
 
@@ -395,17 +396,8 @@ typedef struct {
     unsigned int n_cold_lists;
 } sweep_state_t;
 
-enum init_state {
-    GF_MEMPOOL_INIT_NONE = 0,
-    GF_MEMPOOL_INIT_EARLY,
-    GF_MEMPOOL_INIT_LATE,
-    GF_MEMPOOL_INIT_DESTROY
-};
-
-static enum init_state init_done = GF_MEMPOOL_INIT_NONE;
-static pthread_mutex_t init_mutex = PTHREAD_MUTEX_INITIALIZER;
-static unsigned int init_count = 0;
 static pthread_t sweeper_tid;
+static int sweeper_pipe[2];
 
 static bool
 collect_garbage(sweep_state_t *state, per_thread_pool_list_t *pool_list)
@@ -463,29 +455,43 @@ pool_sweeper(void *arg)
     pending = true;
 
     for (;;) {
+        struct pollfd pfd = {.fd = sweeper_pipe[0], .events = POLLHUP};
+
         /* If we know there's pending work to do (or it's the first run), we
          * do collect garbage more often. */
-        sleep(pending ? POOL_SWEEP_SECS / 5 : POOL_SWEEP_SECS);
+        int ret = poll(
+            &pfd, 1, (pending ? POOL_SWEEP_SECS / 5 : POOL_SWEEP_SECS) * 1000);
+
+        if (ret == 1 && (pfd.revents & POLLHUP)) {
+            /* Peer has closed the write end of the pipe. */
+            sys_close(sweeper_pipe[0]);
+            break;
+        } else if (ret < 0) {
+            if (errno == EINTR)
+                /* Restart because event-poll.c do this at EINTR as well. */
+                continue;
+            else
+                GF_ABORT("unexpected error in memsweep thread (%s)",
+                         strerror(errno));
+        }
 
-        (void)pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
         state.n_cold_lists = 0;
         pending = false;
 
         /* First pass: collect stuff that needs our attention. */
-        (void)pthread_mutex_lock(&pool_lock);
+        pthread_mutex_lock(&pool_lock);
         list_for_each_entry(pool_list, &pool_threads, thr_list)
         {
             if (collect_garbage(&state, pool_list)) {
                 pending = true;
             }
         }
-        (void)pthread_mutex_unlock(&pool_lock);
+        pthread_mutex_unlock(&pool_lock);
 
         /* Second pass: free cold objects from live pools. */
         for (i = 0; i < state.n_cold_lists; ++i) {
             free_obj_list(state.cold_lists[i]);
         }
-        (void)pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
     }
 
     return NULL;
@@ -558,8 +564,6 @@ mem_pools_preinit(void)
 
     pool_list_size = sizeof(per_thread_pool_list_t) +
                      sizeof(per_thread_pool_t) * (NPOOLS - 1);
-
-    init_done = GF_MEMPOOL_INIT_EARLY;
 }
 
 static __attribute__((destructor)) void
@@ -578,58 +582,42 @@ mem_pools_postfini(void)
      *       so the memory will be released anyway by the system. */
 }
 
-/* Call mem_pools_init() once threading has been configured completely. This
- * prevent the pool_sweeper thread from getting killed once the main() thread
- * exits during deamonizing. */
+static void
+mem_pools_init_once(void)
+{
+    if (pipe(sweeper_pipe))
+        GF_ABORT("can't create memsweep thread control pipe");
+    if (gf_thread_create(&sweeper_tid, NULL, pool_sweeper, NULL, "memsweep"))
+        GF_ABORT("can't create memsweep thread");
+}
+
 void
 mem_pools_init(void)
 {
-    pthread_mutex_lock(&init_mutex);
-    if ((init_count++) == 0) {
-        (void)gf_thread_create(&sweeper_tid, NULL, pool_sweeper, NULL,
-                               "memsweep");
+    static pthread_once_t init_once = PTHREAD_ONCE_INIT;
+    pthread_once(&init_once, mem_pools_init_once);
+}
 
-        init_done = GF_MEMPOOL_INIT_LATE;
-    }
-    pthread_mutex_unlock(&init_mutex);
+static void
+mem_pools_fini_once(void)
+{
+    /* Closing the write end causes POLLHUP at the read end. */
+    sys_close(sweeper_pipe[1]);
+
+    pthread_join(sweeper_tid, NULL);
+
+    /* There could be threads still running in some cases, so we can't
+     * destroy pool_lists in use. We can also not destroy unused
+     * pool_lists because some allocated objects may still be pointing
+     * to them. */
+    mem_pool_thread_destructor(NULL);
 }
 
 void
 mem_pools_fini(void)
 {
-    pthread_mutex_lock(&init_mutex);
-    switch (init_count) {
-        case 0:
-            /*
-             * If init_count is already zero (as e.g. if somebody called this
-             * before mem_pools_init) then the sweeper was probably never even
-             * started so we don't need to stop it. Even if there's some crazy
-             * circumstance where there is a sweeper but init_count is still
-             * zero, that just means we'll leave it running. Not perfect, but
-             * far better than any known alternative.
-             */
-            break;
-        case 1: {
-            /* if mem_pools_init() was not called, sweeper_tid will be invalid
-             * and the functions will error out. That is not critical. In all
-             * other cases, the sweeper_tid will be valid and the thread gets
-             * stopped. */
-            (void)pthread_cancel(sweeper_tid);
-            (void)pthread_join(sweeper_tid, NULL);
-
-            /* There could be threads still running in some cases, so we can't
-             * destroy pool_lists in use. We can also not destroy unused
-             * pool_lists because some allocated objects may still be pointing
-             * to them. */
-            mem_pool_thread_destructor(NULL);
-
-            init_done = GF_MEMPOOL_INIT_DESTROY;
-            /* Fall through. */
-        }
-        default:
-            --init_count;
-    }
-    pthread_mutex_unlock(&init_mutex);
+    static pthread_once_t fini_once = PTHREAD_ONCE_INIT;
+    pthread_once(&fini_once, mem_pools_fini_once);
 }
 
 void
