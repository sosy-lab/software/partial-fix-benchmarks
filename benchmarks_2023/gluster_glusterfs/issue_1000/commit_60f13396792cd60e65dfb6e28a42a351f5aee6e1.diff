diff --git a/libglusterfs/src/common-utils.c b/libglusterfs/src/common-utils.c
index d5fca2f56c1..efc67012d15 100644
--- a/libglusterfs/src/common-utils.c
+++ b/libglusterfs/src/common-utils.c
@@ -4124,6 +4124,76 @@ gf_thread_create_detached(pthread_t *thread, void *(*start_routine)(void *),
     return ret;
 }
 
+int
+gf_periodic_thread_create(gf_periodic_thread_t *ctrl,
+                          const pthread_attr_t *attr,
+                          void *(*start_routine)(void *),
+                          void *arg, const char *name, ...)
+{
+    int ret;
+    va_list args;
+
+    ctrl->run = _gf_false;
+    ctrl->quit = _gf_false;
+
+    ret = pthread_mutex_init(&ctrl->lock, NULL);
+    if (ret)
+        return ret;
+    ret = pthread_cond_init(&ctrl->cond, NULL);
+    if (ret)
+        return ret;
+
+    va_start(args, name);
+    ret = gf_thread_vcreate(&ctrl->thread, attr, start_routine, arg, name,
+                            args);
+    va_end(args);
+
+    if (ret) {
+        pthread_mutex_destroy(&ctrl->lock);
+        pthread_cond_destroy(&ctrl->cond);
+    } else
+        ctrl->run = _gf_true;
+    return ret;
+}
+
+void
+gf_periodic_thread_destroy(gf_periodic_thread_t *ctrl)
+{
+    pthread_mutex_lock(&ctrl->lock);
+    pthread_cond_signal(&ctrl->cond);
+    ctrl->quit = _gf_true;
+    pthread_mutex_unlock(&ctrl->lock);
+
+    pthread_join(ctrl->thread, NULL);
+    ctrl->run = _gf_false;
+
+    pthread_mutex_destroy(&ctrl->lock);
+    pthread_cond_destroy(&ctrl->cond);
+}
+
+gf_boolean_t
+gf_periodic_thread_quit(gf_periodic_thread_t *ctrl, time_t timeout)
+{
+    gf_boolean_t quit;
+    struct timespec ts;
+
+    timespec_now_realtime(&ts);
+    ts.tv_sec += timeout;
+
+    pthread_mutex_lock(&ctrl->lock);
+    pthread_cond_timedwait(&ctrl->cond, &ctrl->lock, &ts);
+    quit = ctrl->quit;
+    pthread_mutex_unlock(&ctrl->lock);
+
+    return quit;
+}
+
+gf_boolean_t
+gf_periodic_thread_alive(gf_periodic_thread_t *ctrl)
+{
+    return ctrl->run && pthread_kill(ctrl->thread, 0) == 0;
+}
+
 int
 gf_skip_header_section(int fd, int header_len)
 {
diff --git a/libglusterfs/src/glusterfs/common-utils.h b/libglusterfs/src/glusterfs/common-utils.h
index 55cb4b3706b..dbf78d5746a 100644
--- a/libglusterfs/src/glusterfs/common-utils.h
+++ b/libglusterfs/src/glusterfs/common-utils.h
@@ -1114,6 +1114,31 @@ gf_thread_set_name(pthread_t thread, const char *name, ...)
 
 void
 gf_thread_set_vname(pthread_t thread, const char *name, va_list args);
+
+typedef struct gf_periodic_thread {
+    pthread_t thread;
+    gf_boolean_t run;
+    gf_boolean_t quit;
+    pthread_cond_t cond;
+    pthread_mutex_t lock;
+} gf_periodic_thread_t;
+
+int
+gf_periodic_thread_create(gf_periodic_thread_t *ctrl,
+                          const pthread_attr_t *attr,
+                          void *(*start_routine)(void *),
+                          void *arg, const char *name, ...)
+    __attribute__((__format__(__printf__, 5, 6)));
+
+void
+gf_periodic_thread_destroy(gf_periodic_thread_t *ctrl);
+
+gf_boolean_t
+gf_periodic_thread_alive(gf_periodic_thread_t *ctrl);
+
+gf_boolean_t
+gf_periodic_thread_quit(gf_periodic_thread_t *ctrl, time_t secs);
+
 gf_boolean_t
 gf_is_pid_running(int pid);
 gf_boolean_t
diff --git a/libglusterfs/src/libglusterfs.sym b/libglusterfs/src/libglusterfs.sym
index 8e9527bee8c..d9b495dc34c 100644
--- a/libglusterfs/src/libglusterfs.sym
+++ b/libglusterfs/src/libglusterfs.sym
@@ -646,6 +646,10 @@ _gf_msg
 _gf_msg_nomem
 gf_nwrite
 gf_path_strip_trailing_slashes
+gf_periodic_thread_alive
+gf_periodic_thread_create
+gf_periodic_thread_destroy
+gf_periodic_thread_quit
 gf_print_trace
 gf_proc_dump_add_section
 gf_proc_dump_info
diff --git a/xlators/debug/io-stats/src/io-stats.c b/xlators/debug/io-stats/src/io-stats.c
index f9fa49d5d4d..3693045b68f 100644
--- a/xlators/debug/io-stats/src/io-stats.c
+++ b/xlators/debug/io-stats/src/io-stats.c
@@ -163,9 +163,7 @@ struct ios_conf {
     struct ios_stat_head list[IOS_STATS_TYPE_MAX];
     struct ios_stat_head thru_list[IOS_STATS_THRU_MAX];
     int32_t ios_dump_interval;
-    pthread_t dump_thread;
-    gf_boolean_t dump_thread_should_die;
-    gf_boolean_t dump_thread_running;
+    gf_periodic_thread_t dump_thread;
     gf_lock_t ios_sampling_lock;
     int32_t ios_sample_interval;
     int32_t ios_sample_buf_size;
@@ -3010,20 +3008,10 @@ conditional_dump(dict_t *dict, char *key, data_t *value, void *data)
     return 0;
 }
 
-int
-_ios_destroy_dump_thread(struct ios_conf *conf)
-{
-    conf->dump_thread_should_die = _gf_true;
-    if (conf->dump_thread_running) {
-        (void)pthread_cancel(conf->dump_thread);
-        (void)pthread_join(conf->dump_thread, NULL);
-    }
-    return 0;
-}
-
-void *
-_ios_dump_thread(xlator_t *this)
+static void *
+ios_dump_thread(void *arg)
 {
+    xlator_t *this = arg;
     struct ios_conf *conf = NULL;
     FILE *stats_logfp = NULL;
     FILE *samples_logfp = NULL;
@@ -3037,7 +3025,6 @@ _ios_dump_thread(xlator_t *this)
     char *instance_name;
     gf_boolean_t log_stats_fopen_failure = _gf_true;
     gf_boolean_t log_samples_fopen_failure = _gf_true;
-    int old_cancel_type;
 
     conf = this->private;
     gf_log(this->name, GF_LOG_INFO,
@@ -3094,12 +3081,9 @@ _ios_dump_thread(xlator_t *this)
         goto out;
     }
     while (1) {
-        if (conf->dump_thread_should_die)
+        if (gf_periodic_thread_quit(&conf->dump_thread,
+                                    conf->ios_dump_interval))
             break;
-        (void)pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS,
-                                    &old_cancel_type);
-        sleep(conf->ios_dump_interval);
-        (void)pthread_setcanceltype(PTHREAD_CANCEL_DEFERRED, &old_cancel_type);
         /*
          * It's not clear whether we should reopen this each time, or
          * just hold it open and rewind/truncate on each iteration.
@@ -3130,7 +3114,6 @@ _ios_dump_thread(xlator_t *this)
         }
     }
 out:
-    conf->dump_thread_running = _gf_false;
     gf_log(this->name, GF_LOG_INFO, "IO stats dump thread terminated");
     return NULL;
 }
@@ -3739,12 +3722,9 @@ reconfigure(xlator_t *this, dict_t *options)
     GF_OPTION_RECONF("ios-dump-interval", conf->ios_dump_interval, options,
                      int32, out);
     if ((old_dump_interval <= 0) && (conf->ios_dump_interval > 0)) {
-        conf->dump_thread_running = _gf_true;
-        conf->dump_thread_should_die = _gf_false;
-        ret = gf_thread_create(&conf->dump_thread, NULL,
-                               (void *)&_ios_dump_thread, this, "iosdump");
+        ret = gf_periodic_thread_create(&conf->dump_thread, NULL,
+                                        ios_dump_thread, this, "iosdump");
         if (ret) {
-            conf->dump_thread_running = _gf_false;
             gf_log(this ? this->name : "io-stats", GF_LOG_ERROR,
                    "Failed to start thread"
                    "while reconfigure. Returning %d",
@@ -3752,7 +3732,8 @@ reconfigure(xlator_t *this, dict_t *options)
             goto out;
         }
     } else if ((old_dump_interval > 0) && (conf->ios_dump_interval == 0)) {
-        _ios_destroy_dump_thread(conf);
+        if (gf_periodic_thread_alive(&conf->dump_thread))
+            gf_periodic_thread_destroy(&conf->dump_thread);
     }
 
     GF_OPTION_RECONF("ios-sample-interval", conf->ios_sample_interval, options,
@@ -3835,7 +3816,8 @@ ios_conf_destroy(struct ios_conf *conf)
         return;
 
     ios_destroy_top_stats(conf);
-    _ios_destroy_dump_thread(conf);
+    if (gf_periodic_thread_alive(&conf->dump_thread))
+        gf_periodic_thread_destroy(&conf->dump_thread);
     ios_destroy_sample_buf(conf->ios_sample_buf);
     LOCK_DESTROY(&conf->lock);
     gf_dnscache_deinit(conf->dnscache);
@@ -3999,12 +3981,9 @@ init(xlator_t *this)
 
     this->private = conf;
     if (conf->ios_dump_interval > 0) {
-        conf->dump_thread_running = _gf_true;
-        conf->dump_thread_should_die = _gf_false;
-        ret = gf_thread_create(&conf->dump_thread, NULL,
-                               (void *)&_ios_dump_thread, this, "iosdump");
+        ret = gf_periodic_thread_create(&conf->dump_thread, NULL,
+                                        ios_dump_thread, this, "iosdump");
         if (ret) {
-            conf->dump_thread_running = _gf_false;
             gf_log(this ? this->name : "io-stats", GF_LOG_ERROR,
                    "Failed to start thread"
                    "in init. Returning %d",
diff --git a/xlators/storage/posix/src/posix-common.c b/xlators/storage/posix/src/posix-common.c
index 2a0bece5070..22503b5d473 100644
--- a/xlators/storage/posix/src/posix-common.c
+++ b/xlators/storage/posix/src/posix-common.c
@@ -1127,7 +1127,6 @@ posix_init(xlator_t *this)
         }
     }
 
-    _private->health_check_active = _gf_false;
     GF_OPTION_INIT("health-check-interval", _private->health_check_interval,
                    time, out);
     GF_OPTION_INIT("health-check-timeout", _private->health_check_timeout, time,
@@ -1259,7 +1258,6 @@ void
 posix_fini(xlator_t *this)
 {
     struct posix_private *priv = this->private;
-    gf_boolean_t health_check = _gf_false;
     glusterfs_ctx_t *ctx = this->ctx;
     uint32_t count;
     int ret = 0;
@@ -1267,12 +1265,6 @@ posix_fini(xlator_t *this)
 
     if (!priv)
         return;
-    LOCK(&priv->lock);
-    {
-        health_check = priv->health_check_active;
-        priv->health_check_active = _gf_false;
-    }
-    UNLOCK(&priv->lock);
 
     if (priv->dirfd >= 0) {
         sys_close(priv->dirfd);
@@ -1286,10 +1278,8 @@ posix_fini(xlator_t *this)
         }
     }
 
-    if (health_check) {
-        (void)gf_thread_cleanup_xint(priv->health_check);
-        priv->health_check = 0;
-    }
+    if (gf_periodic_thread_alive(&priv->health_check))
+        gf_periodic_thread_destroy(&priv->health_check);
 
     if (priv->janitor) {
         /*TODO: Make sure the synctask is also complete */
diff --git a/xlators/storage/posix/src/posix-helpers.c b/xlators/storage/posix/src/posix-helpers.c
index bacba9b91ce..87e5d99abd7 100644
--- a/xlators/storage/posix/src/posix-helpers.c
+++ b/xlators/storage/posix/src/posix-helpers.c
@@ -2109,7 +2109,7 @@ posix_health_check_thread_proc(void *data)
     glusterfs_ctx_t *ctx = THIS->ctx;
     char file_path[PATH_MAX];
 
-    /* prevent races when the interval is updated */
+    /* Prevent races when the interval is updated. */
     if (interval == 0)
         goto out;
 
@@ -2122,41 +2122,17 @@ posix_health_check_thread_proc(void *data)
                  "interval = %d seconds",
                  file_path, interval);
     while (1) {
-        /* aborting sleep() is a request to exit this thread, sleep()
-         * will normally not return when cancelled */
-        ret = sleep(interval);
-        if (ret > 0)
+        if (gf_periodic_thread_quit(&priv->health_check, interval))
             break;
-        /* prevent thread errors while doing the health-check(s) */
-        pthread_setcancelstate(PTHREAD_CANCEL_DISABLE, NULL);
 
         /* Do the health-check.*/
-        ret = posix_fs_health_check(this, file_path);
-        if (ret < 0 && priv->health_check_active)
-            goto abort;
-        if (!priv->health_check_active)
-            goto out;
-        pthread_setcancelstate(PTHREAD_CANCEL_ENABLE, NULL);
+        if (posix_fs_health_check(this, file_path) < 0)
+            break;
     }
 
 out:
     gf_msg_debug(this->name, 0, "health-check thread exiting");
 
-    LOCK(&priv->lock);
-    {
-        priv->health_check_active = _gf_false;
-    }
-    UNLOCK(&priv->lock);
-
-    return NULL;
-
-abort:
-    LOCK(&priv->lock);
-    {
-        priv->health_check_active = _gf_false;
-    }
-    UNLOCK(&priv->lock);
-
     /* health-check failed */
     gf_msg(this->name, GF_LOG_EMERG, 0, P_MSG_HEALTHCHECK_FAILED,
            "health-check failed, going down");
@@ -2225,33 +2201,23 @@ posix_spawn_health_check_thread(xlator_t *xl)
 
     LOCK(&priv->lock);
     {
-        /* cancel the running thread  */
-        if (priv->health_check_active == _gf_true) {
-            ret = pthread_cancel(priv->health_check);
-            if (ret != 0) {
-                gf_msg(xl->name, GF_LOG_ERROR, ret, P_MSG_PTHREAD_CANCEL_FAILED,
-                       "Failed to send cancellation to health-check thread");
-                ret = -1;
-                goto unlock;
-            }
-            priv->health_check_active = _gf_false;
-        }
+        /* Stop the currently running thread, if any. */
+        if (gf_periodic_thread_alive(&priv->health_check))
+            gf_periodic_thread_destroy(&priv->health_check);
 
-        /* prevent scheduling a check in a tight loop */
+        /* Prevent scheduling a check in a tight loop. */
         if (priv->health_check_interval == 0)
             goto unlock;
 
-        ret = gf_thread_create(&priv->health_check, NULL,
-                               posix_health_check_thread_proc, xl, "posixhc");
+        ret = gf_periodic_thread_create(&priv->health_check, NULL,
+                                        posix_health_check_thread_proc,
+                                        xl, "posixhc");
         if (ret) {
             priv->health_check_interval = 0;
-            priv->health_check_active = _gf_false;
             gf_msg(xl->name, GF_LOG_ERROR, errno, P_MSG_HEALTHCHECK_FAILED,
                    "unable to setup health-check thread");
             goto unlock;
         }
-
-        priv->health_check_active = _gf_true;
     }
 unlock:
     UNLOCK(&priv->lock);
diff --git a/xlators/storage/posix/src/posix.h b/xlators/storage/posix/src/posix.h
index 77830d710c6..385fb95b6a9 100644
--- a/xlators/storage/posix/src/posix.h
+++ b/xlators/storage/posix/src/posix.h
@@ -199,7 +199,7 @@ struct posix_private {
     time_t health_check_interval;
     /* seconds to sleep to wait for aio write finish for health checks */
     time_t health_check_timeout;
-    pthread_t health_check;
+    gf_periodic_thread_t health_check;
 
     double disk_reserve;
     pthread_t disk_space_check;
@@ -236,7 +236,6 @@ struct posix_private {
     gf_boolean_t janitor_task_stop;
 
     char disk_unit;
-    gf_boolean_t health_check_active;
     gf_boolean_t update_pgfid_nlinks;
     gf_boolean_t gfid2path;
     /* node-uuid in pathinfo xattr */
