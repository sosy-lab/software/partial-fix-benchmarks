diff --git a/xlators/cluster/dht/src/dht-common.h b/xlators/cluster/dht/src/dht-common.h
index fd43d9ec475..baada2e2503 100644
--- a/xlators/cluster/dht/src/dht-common.h
+++ b/xlators/cluster/dht/src/dht-common.h
@@ -64,6 +64,19 @@ typedef int (*dht_refresh_layout_unlock)(call_frame_t *frame, xlator_t *this,
 
 typedef int (*dht_refresh_layout_done_handle)(call_frame_t *frame);
 
+struct dht_layout_entry {
+    int err; /* 0 = normal
+                -1 = dir exists and no xattr
+                >0 = dir lookup failed with errno
+             */
+    uint32_t start;
+    uint32_t stop;
+    uint32_t commit_hash;
+    xlator_t *xlator;
+};
+
+typedef struct dht_layout_entry dht_layout_entry_t;
+
 struct dht_layout {
     int spread_cnt; /* layout spread count per directory,
                        is controlled by 'setxattr()' with
@@ -88,16 +101,7 @@ struct dht_layout {
     int type;
     gf_atomic_t ref; /* use with dht_conf_t->layout_lock */
     uint32_t search_unhashed;
-    struct {
-        int err; /* 0 = normal
-                    -1 = dir exists and no xattr
-                    >0 = dir lookup failed with errno
-                 */
-        uint32_t start;
-        uint32_t stop;
-        uint32_t commit_hash;
-        xlator_t *xlator;
-    } list[];
+    dht_layout_entry_t list[];
 };
 typedef struct dht_layout dht_layout_t;
 
@@ -1199,7 +1203,7 @@ void
 dht_log_new_layout_for_dir_selfheal(xlator_t *this, loc_t *loc,
                                     dht_layout_t *layout);
 
-int
+void
 dht_layout_sort(dht_layout_t *layout);
 
 int
diff --git a/xlators/cluster/dht/src/dht-layout.c b/xlators/cluster/dht/src/dht-layout.c
index 2dbd9f39607..8df4cbd9388 100644
--- a/xlators/cluster/dht/src/dht-layout.c
+++ b/xlators/cluster/dht/src/dht-layout.c
@@ -369,34 +369,6 @@ dht_layout_merge(xlator_t *this, dht_layout_t *layout, xlator_t *subvol,
     return ret;
 }
 
-void
-dht_layout_entry_swap(dht_layout_t *layout, int i, int j)
-{
-    uint32_t start_swap = 0;
-    uint32_t stop_swap = 0;
-    uint32_t commit_hash_swap = 0;
-    xlator_t *xlator_swap = 0;
-    int err_swap = 0;
-
-    start_swap = layout->list[i].start;
-    stop_swap = layout->list[i].stop;
-    xlator_swap = layout->list[i].xlator;
-    err_swap = layout->list[i].err;
-    commit_hash_swap = layout->list[i].commit_hash;
-
-    layout->list[i].start = layout->list[j].start;
-    layout->list[i].stop = layout->list[j].stop;
-    layout->list[i].xlator = layout->list[j].xlator;
-    layout->list[i].err = layout->list[j].err;
-    layout->list[i].commit_hash = layout->list[j].commit_hash;
-
-    layout->list[j].start = start_swap;
-    layout->list[j].stop = stop_swap;
-    layout->list[j].xlator = xlator_swap;
-    layout->list[j].err = err_swap;
-    layout->list[j].commit_hash = commit_hash_swap;
-}
-
 void
 dht_layout_range_swap(dht_layout_t *layout, int i, int j)
 {
@@ -412,11 +384,6 @@ dht_layout_range_swap(dht_layout_t *layout, int i, int j)
     layout->list[j].start = start_swap;
     layout->list[j].stop = stop_swap;
 }
-static int64_t
-dht_layout_entry_cmp_volname(dht_layout_t *layout, int i, int j)
-{
-    return (strcmp(layout->list[i].xlator->name, layout->list[j].xlator->name));
-}
 
 gf_boolean_t
 dht_is_subvol_in_layout(dht_layout_t *layout, xlator_t *xlator)
@@ -435,58 +402,36 @@ dht_is_subvol_in_layout(dht_layout_t *layout, xlator_t *xlator)
     return _gf_false;
 }
 
-static int64_t
-dht_layout_entry_cmp(dht_layout_t *layout, int i, int j)
+static int
+dht_layout_entry_cmp(const void *p, const void *q)
 {
-    int64_t diff = 0;
+    const dht_layout_entry_t *x = p, *y = q;
 
-    /* swap zero'ed out layouts to front, if needed */
-    if (!layout->list[j].start && !layout->list[j].stop) {
-        diff = (int64_t)layout->list[i].stop - (int64_t)layout->list[j].stop;
-        goto out;
-    }
-    diff = (int64_t)layout->list[i].start - (int64_t)layout->list[j].start;
-
-out:
-    return diff;
+    /* Swap zero'ed out layouts to front if needed. */
+    return (!y->start && !y->stop) ? (x->stop - y->stop) :
+        (x->start - y->start);
 }
 
-int
-dht_layout_sort(dht_layout_t *layout)
+static int
+dht_layout_entry_cmp_volname(const void *p, const void *q)
 {
-    int i = 0;
-    int j = 0;
-    int64_t ret = 0;
-
-    /* TODO: O(n^2) -- bad bad */
+    const dht_layout_entry_t *x = p, *y = q;
 
-    for (i = 0; i < layout->cnt - 1; i++) {
-        for (j = i + 1; j < layout->cnt; j++) {
-            ret = dht_layout_entry_cmp(layout, i, j);
-            if (ret > 0)
-                dht_layout_entry_swap(layout, i, j);
-        }
-    }
+    return strcmp(x->xlator->name, y->xlator->name);
+}
 
-    return 0;
+void
+dht_layout_sort(dht_layout_t *layout)
+{
+    qsort(layout->list, layout->cnt, sizeof(dht_layout_entry_t),
+          dht_layout_entry_cmp);
 }
 
 void
 dht_layout_sort_volname(dht_layout_t *layout)
 {
-    int i = 0;
-    int j = 0;
-    int64_t ret = 0;
-
-    /* TODO: O(n^2) -- bad bad */
-
-    for (i = 0; i < layout->cnt - 1; i++) {
-        for (j = i + 1; j < layout->cnt; j++) {
-            ret = dht_layout_entry_cmp_volname(layout, i, j);
-            if (ret > 0)
-                dht_layout_entry_swap(layout, i, j);
-        }
-    }
+    qsort(layout->list, layout->cnt, sizeof(dht_layout_entry_t),
+          dht_layout_entry_cmp_volname);
 }
 
 void
@@ -617,12 +562,7 @@ dht_layout_normalize(xlator_t *this, loc_t *loc, dht_layout_t *layout)
     uint32_t misc = 0, missing_dirs = 0;
     char gfid[GF_UUID_BUF_SIZE] = {0};
 
-    ret = dht_layout_sort(layout);
-    if (ret == -1) {
-        gf_smsg(this->name, GF_LOG_WARNING, 0, DHT_MSG_LAYOUT_SORT_FAILED,
-                NULL);
-        goto out;
-    }
+    dht_layout_sort(layout);
 
     gf_uuid_unparse(loc->gfid, gfid);
 
@@ -652,7 +592,6 @@ dht_layout_normalize(xlator_t *this, loc_t *loc, dht_layout_t *layout)
             ret += missing_dirs;
     }
 
-out:
     return ret;
 }
 
diff --git a/xlators/cluster/dht/src/dht-messages.h b/xlators/cluster/dht/src/dht-messages.h
index 601f8dad78b..a7cfb551a30 100644
--- a/xlators/cluster/dht/src/dht-messages.h
+++ b/xlators/cluster/dht/src/dht-messages.h
@@ -43,7 +43,7 @@ GLFS_MSGID(
     DHT_MSG_OPEN_FD_FAILED, DHT_MSG_SET_INODE_CTX_FAILED,
     DHT_MSG_UNLOCKING_FAILED, DHT_MSG_DISK_LAYOUT_NULL, DHT_MSG_SUBVOL_INFO,
     DHT_MSG_CHUNK_SIZE_INFO, DHT_MSG_LAYOUT_FORM_FAILED, DHT_MSG_SUBVOL_ERROR,
-    DHT_MSG_LAYOUT_SORT_FAILED, DHT_MSG_REGEX_INFO, DHT_MSG_FOPEN_FAILED,
+    DHT_MSG_REGEX_INFO, DHT_MSG_FOPEN_FAILED,
     DHT_MSG_SET_HOSTNAME_FAILED, DHT_MSG_BRICK_ERROR, DHT_MSG_SYNCOP_FAILED,
     DHT_MSG_MIGRATE_INFO, DHT_MSG_SOCKET_ERROR, DHT_MSG_CREATE_FD_FAILED,
     DHT_MSG_READDIR_ERROR, DHT_MSG_CHILD_LOC_BUILD_FAILED,
@@ -336,7 +336,6 @@ GLFS_MSGID(
 #define DHT_MSG_COMPUTE_HASH_FAILED_STR "hash computation failed"
 #define DHT_MSG_INVALID_DISK_LAYOUT_STR                                        \
     "Invalid disk layout: Catastrophic error layout with unknown type found"
-#define DHT_MSG_LAYOUT_SORT_FAILED_STR "layout sort failed"
 #define DHT_MSG_ANOMALIES_INFO_STR "Found anomalies"
 #define DHT_MSG_XATTR_DICT_NULL_STR "xattr dictionary is NULL"
 #define DHT_MSG_DISK_LAYOUT_MISSING_STR "Disk layout missing"
diff --git a/xlators/cluster/dht/src/dht-selfheal.c b/xlators/cluster/dht/src/dht-selfheal.c
index cc1c2736465..301a0f09f2c 100644
--- a/xlators/cluster/dht/src/dht-selfheal.c
+++ b/xlators/cluster/dht/src/dht-selfheal.c
@@ -125,10 +125,9 @@ dht_selfheal_dir_finish(call_frame_t *frame, xlator_t *this, int ret,
     return 0;
 }
 
-int
+static int
 dht_refresh_layout_done(call_frame_t *frame)
 {
-    int ret = -1;
     dht_layout_t *refreshed = NULL, *heal = NULL;
     dht_local_t *local = NULL;
     dht_need_heal_t should_heal = NULL;
@@ -142,12 +141,7 @@ dht_refresh_layout_done(call_frame_t *frame)
     healer = local->selfheal.healer;
     should_heal = local->selfheal.should_heal;
 
-    ret = dht_layout_sort(refreshed);
-    if (ret == -1) {
-        gf_smsg(frame->this->name, GF_LOG_WARNING, 0,
-                DHT_MSG_LAYOUT_SORT_FAILED, NULL);
-        goto err;
-    }
+    dht_layout_sort(refreshed);
 
     if (should_heal(frame, &heal, &refreshed)) {
         healer(frame, &local->loc, heal);
@@ -162,10 +156,6 @@ dht_refresh_layout_done(call_frame_t *frame)
     }
 
     return 0;
-
-err:
-    dht_selfheal_dir_finish(frame, frame->this, -1, 1);
-    return 0;
 }
 
 int
