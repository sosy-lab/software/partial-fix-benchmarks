diff --git a/configure.ac b/configure.ac
index 19f16571fea..45282b9696f 100644
--- a/configure.ac
+++ b/configure.ac
@@ -1112,6 +1112,11 @@ if test "x${have_posix_fallocate}" = "xyes"; then
    AC_DEFINE(HAVE_POSIX_FALLOCATE, 1, [define if posix_fallocate exists])
 fi
 
+AC_CHECK_FUNC([open_memstream], [have_open_memstream=yes])
+if test "x${have_open_memstream}" = "xyes"; then
+   AC_DEFINE(HAVE_OPEN_MEMSTREAM, 1, [define if open_memstream exists])
+fi
+
 # On fedora-29, copy_file_range syscall and the libc API both are present.
 # Whereas, on some machines such as centos-7, RHEL-7, the API is not there.
 # Only the system call is present. So, this change is to determine whether
diff --git a/libglusterfs/src/glusterfs/strfd.h b/libglusterfs/src/glusterfs/strfd.h
index 861cd02e005..33b450dde16 100644
--- a/libglusterfs/src/glusterfs/strfd.h
+++ b/libglusterfs/src/glusterfs/strfd.h
@@ -11,15 +11,22 @@
 #ifndef _STRFD_H
 #define _STRFD_H
 
+/* This is a stack-allocated object used to collect
+   (mostly by concatenation) strings with all usual
+   C burden like strlen(), realloc() etc. hidden. */
+
 typedef struct {
-    void *data;
-    size_t alloc_size;
+    char *data;
     size_t size;
-    off_t pos;
+#ifdef HAVE_OPEN_MEMSTREAM
+    FILE *fp;
+#else
+    size_t alloc_size;
+#endif /* HAVE_OPEN_MEMSTREAM */
 } strfd_t;
 
-strfd_t *
-strfd_open();
+int
+strfd_open(strfd_t *strfd);
 
 int
 strprintf(strfd_t *strfd, const char *fmt, ...)
@@ -28,7 +35,7 @@ strprintf(strfd_t *strfd, const char *fmt, ...)
 int
 strvprintf(strfd_t *strfd, const char *fmt, va_list ap);
 
-int
+void
 strfd_close(strfd_t *strfd);
 
 #endif
diff --git a/libglusterfs/src/strfd.c b/libglusterfs/src/strfd.c
index 8a2580edc85..74ab6c232f2 100644
--- a/libglusterfs/src/strfd.c
+++ b/libglusterfs/src/strfd.c
@@ -15,14 +15,41 @@
 #include "glusterfs/strfd.h"
 #include "glusterfs/common-utils.h"
 
-strfd_t *
-strfd_open()
+#ifdef HAVE_OPEN_MEMSTREAM
+
+int
+strfd_open(strfd_t *strfd)
 {
-    strfd_t *strfd = NULL;
+    memset(strfd, 0, sizeof(strfd_t));
+    strfd->fp = open_memstream(&strfd->data, &strfd->size);
+    return strfd->fp ? 0 : -1;
+}
 
-    strfd = GF_CALLOC(1, sizeof(*strfd), gf_common_mt_strfd_t);
+int
+strvprintf(strfd_t *strfd, const char *fmt, va_list ap)
+{
+    int size = vfprintf(strfd->fp, fmt, ap);
+    /* According to the manual, flushing is required to update 'data'
+       and 'size' fields which are directly accessed by strfd users. */
+    fflush(strfd->fp);
+    return size;
+}
 
-    return strfd;
+void
+strfd_close(strfd_t *strfd)
+{
+    fclose(strfd->fp);
+    /* Not GF_FREE because was not GF_ALLOC'ed. */
+    free(strfd->data);
+}
+
+#else /* not HAVE_OPEN_MEMSTREAM */
+
+int
+strfd_open(strfd_t *strfd)
+{
+    memset(strfd, 0, sizeof(strfd_t));
+    return 0;
 }
 
 int
@@ -70,6 +97,14 @@ strvprintf(strfd_t *strfd, const char *fmt, va_list ap)
     return size;
 }
 
+void
+strfd_close(strfd_t *strfd)
+{
+    GF_FREE(strfd->data);
+}
+
+#endif /* HAVE_OPEN_MEMSTREAM */
+
 int
 strprintf(strfd_t *strfd, const char *fmt, ...)
 {
@@ -82,12 +117,3 @@ strprintf(strfd_t *strfd, const char *fmt, ...)
 
     return ret;
 }
-
-int
-strfd_close(strfd_t *strfd)
-{
-    GF_FREE(strfd->data);
-    GF_FREE(strfd);
-
-    return 0;
-}
diff --git a/xlators/meta/src/meta-defaults.c b/xlators/meta/src/meta-defaults.c
index 91c328473f8..ca307c80ebc 100644
--- a/xlators/meta/src/meta-defaults.c
+++ b/xlators/meta/src/meta-defaults.c
@@ -239,8 +239,8 @@ meta_default_readlink(call_frame_t *frame, xlator_t *this, loc_t *loc,
                       size_t size, dict_t *xdata)
 {
     struct meta_ops *ops = NULL;
-    strfd_t *strfd = NULL;
     struct iatt iatt = {};
+    strfd_t strfd;
     int len = -1;
 
     ops = meta_ops_get(loc->inode, this);
@@ -249,23 +249,22 @@ meta_default_readlink(call_frame_t *frame, xlator_t *this, loc_t *loc,
         return 0;
     }
 
-    strfd = strfd_open();
-    if (!strfd) {
+    if (strfd_open(&strfd)) {
         META_STACK_UNWIND(readlink, frame, -1, ENOMEM, 0, 0, 0);
         return 0;
     }
 
-    ops->link_fill(this, loc->inode, strfd);
+    ops->link_fill(this, loc->inode, &strfd);
 
     meta_iatt_fill(&iatt, loc->inode, IA_IFLNK);
 
-    if (strfd->data) {
-        len = strlen(strfd->data);
-        META_STACK_UNWIND(readlink, frame, len, 0, strfd->data, &iatt, xdata);
+    if (strfd.data) {
+        len = strfd.size;
+        META_STACK_UNWIND(readlink, frame, len, 0, strfd.data, &iatt, xdata);
     } else
         META_STACK_UNWIND(readlink, frame, -1, ENODATA, 0, 0, 0);
 
-    strfd_close(strfd);
+    strfd_close(&strfd);
 
     return 0;
 }
diff --git a/xlators/meta/src/meta-helpers.c b/xlators/meta/src/meta-helpers.c
index 554c0f731f8..16b6f811724 100644
--- a/xlators/meta/src/meta-helpers.c
+++ b/xlators/meta/src/meta-helpers.c
@@ -251,8 +251,8 @@ int
 meta_file_fill(xlator_t *this, fd_t *fd)
 {
     meta_fd_t *meta_fd = NULL;
-    strfd_t *strfd = NULL;
     struct meta_ops *ops = NULL;
+    strfd_t strfd;
     int ret = 0;
 
     meta_fd = meta_fd_get(fd, this);
@@ -262,27 +262,24 @@ meta_file_fill(xlator_t *this, fd_t *fd)
     if (meta_fd->data)
         return meta_fd->size;
 
-    strfd = strfd_open();
-    if (!strfd)
+    if (strfd_open(&strfd))
         return -1;
 
     ops = meta_ops_get(fd->inode, this);
     if (!ops) {
-        strfd_close(strfd);
+        strfd_close(&strfd);
         return -1;
     }
 
     if (ops->file_fill)
-        ret = ops->file_fill(this, fd->inode, strfd);
+        ret = ops->file_fill(this, fd->inode, &strfd);
 
-    if (ret >= 0) {
-        meta_fd->data = strfd->data;
-        meta_fd->size = strfd->size;
-
-        strfd->data = NULL;
+    if (ret > 0) {
+        meta_fd->data = gf_strdup(strfd.data);
+        meta_fd->size = strfd.size;
     }
 
-    strfd_close(strfd);
+    strfd_close(&strfd);
 
     return meta_fd->size;
 }
