diff --git a/libglusterfs/src/glusterfs/inode.h b/libglusterfs/src/glusterfs/inode.h
index 1e806c23b8e..e97d1a3e112 100644
--- a/libglusterfs/src/glusterfs/inode.h
+++ b/libglusterfs/src/glusterfs/inode.h
@@ -285,9 +285,6 @@ inode_ctx_put(inode_t *inode, xlator_t *this, uint64_t v)
 gf_boolean_t
 __is_root_gfid(uuid_t gfid);
 
-void
-__inode_table_set_lru_limit(inode_table_t *table, uint32_t lru_limit);
-
 void
 inode_table_set_lru_limit(inode_table_t *table, uint32_t lru_limit);
 
@@ -297,18 +294,12 @@ inode_ctx_merge(fd_t *fd, inode_t *inode, inode_t *linked_inode);
 int
 inode_is_linked(inode_t *inode);
 
-void
-inode_set_need_lookup(inode_t *inode, xlator_t *this);
-
 gf_boolean_t
 inode_needs_lookup(inode_t *inode, xlator_t *this);
 
 int
 inode_has_dentry(inode_t *inode);
 
-size_t
-inode_ctx_size(inode_t *inode);
-
 void
 inode_find_directory_name(inode_t *inode, const char **name);
 
diff --git a/libglusterfs/src/inode.c b/libglusterfs/src/inode.c
index d0395088f07..1a1d2d20724 100644
--- a/libglusterfs/src/inode.c
+++ b/libglusterfs/src/inode.c
@@ -205,14 +205,11 @@ __dentry_unhash(dentry_t *dentry)
 static void
 dentry_destroy(dentry_t *dentry)
 {
-    if (!dentry)
-        return;
-
-    GF_FREE(dentry->name);
-    dentry->name = NULL;
-    mem_put(dentry);
-
-    return;
+    if (dentry) {
+        GF_FREE(dentry->name);
+        dentry->name = NULL;
+        mem_put(dentry);
+    }
 }
 
 static dentry_t *
@@ -470,15 +467,11 @@ __inode_get_xl_index(inode_t *inode, xlator_t *xlator)
 {
     int set_idx = inode_get_ctx_index(inode->table, xlator);
 
-    if ((inode->_ctx[set_idx].xl_key != NULL) &&
-        (inode->_ctx[set_idx].xl_key != xlator)) {
-        set_idx = -1;
-        goto out;
-    }
-
-    inode->_ctx[set_idx].xl_key = xlator;
+    if (inode->_ctx[set_idx].xl_key == NULL)
+        inode->_ctx[set_idx].xl_key = xlator;
+    else if (inode->_ctx[set_idx].xl_key != xlator)
+        return -1;
 
-out:
     return set_idx;
 }
 
@@ -566,11 +559,6 @@ __inode_ref(inode_t *inode, bool is_invalidate)
     int index = 0;
     xlator_t *this = NULL;
 
-    if (!inode)
-        return NULL;
-
-    this = THIS;
-
     /*
      * Root inode should always be in active list of inode table. So unrefs
      * on root inode are no-ops. If we do not allow unrefs but allow refs,
@@ -579,10 +567,10 @@ __inode_ref(inode_t *inode, bool is_invalidate)
      * in inode table increases which is wrong. So just keep the ref
      * count as 1 always
      */
-    if (__is_root_gfid(inode->gfid) && inode->ref)
-        return inode;
-
-    if (!inode->ref) {
+    if (inode->ref) {
+        if (__is_root_gfid(inode->gfid))
+            return inode;
+    } else {
         if (inode->in_invalidate_list) {
             inode->in_invalidate_list = false;
             inode->table->invalidate_size--;
@@ -601,6 +589,8 @@ __inode_ref(inode_t *inode, bool is_invalidate)
         }
     }
 
+    this = THIS;
+
     inode->ref++;
 
     index = __inode_get_xl_index(inode, this);
@@ -733,11 +723,10 @@ inode_new(inode_table_t *table)
             GF_ASSERT(!inode->in_lru_list);
             inode->in_lru_list = _gf_true;
             __inode_ref(inode, false);
+            /* let the dummy, 'unlinked' inodes have root as namespace */
+            inode->ns_inode = __inode_ref(table->root, _gf_false);
         }
         pthread_mutex_unlock(&table->lock);
-
-        /* let the dummy, 'unlinked' inodes have root as namespace */
-        inode->ns_inode = inode_ref(table->root);
     }
 
     return inode;
@@ -777,22 +766,17 @@ __inode_ref_reduce_by_n(inode_t *inode, uint64_t nref)
     return inode;
 }
 
-static inode_t *
+static void
 inode_forget_atomic(inode_t *inode, uint64_t nlookup)
 {
     uint64_t inode_lookup = 0;
 
-    if (!inode)
-        return NULL;
-
     if (nlookup == 0) {
         GF_ATOMIC_INIT(inode->nlookup, 0);
     } else {
         inode_lookup = GF_ATOMIC_FETCH_SUB(inode->nlookup, nlookup);
         GF_ASSERT(inode_lookup >= nlookup);
     }
-
-    return inode;
 }
 
 dentry_t *
@@ -933,7 +917,7 @@ __is_root_gfid(uuid_t gfid)
     return _gf_false;
 }
 
-inode_t *
+static inode_t *
 __inode_find(inode_table_t *table, uuid_t gfid, const int hash)
 {
     inode_t *inode = NULL;
@@ -1077,7 +1061,10 @@ __inode_link(inode_t *inode, inode_t *parent, const char *name,
             dentry->parent = __inode_ref(parent, false);
             GF_ATOMIC_INC(parent->kids);
             list_add(&dentry->inode_list, &link_inode->dentry_list);
-            link_inode->ns_inode = __inode_ref(parent->ns_inode, false);
+            if (parent->ns_inode)
+                link_inode->ns_inode = __inode_ref(parent->ns_inode, false);
+            else
+                link_inode->ns_inode = NULL;
 
             if (old_inode && __is_dentry_cyclic(dentry)) {
                 errno = ELOOP;
@@ -1269,8 +1256,6 @@ static dentry_t *
 __inode_unlink(inode_t *inode, inode_t *parent, const char *name)
 {
     dentry_t *dentry = NULL;
-    char pgfid[64] = {0};
-    char gfid[64] = {0};
 
     dentry = __dentry_search_for_inode(inode, parent->gfid, name);
 
@@ -1278,10 +1263,11 @@ __inode_unlink(inode_t *inode, inode_t *parent, const char *name)
     if (dentry) {
         dentry = __dentry_unset(dentry);
     } else {
+        char pgfid[64];
         gf_smsg("inode", GF_LOG_WARNING, 0, LG_MSG_DENTRY_NOT_FOUND,
                 "parent-gfid=%s name=%s gfid%s",
-                uuid_utoa_r(parent->gfid, pgfid), name,
-                uuid_utoa_r(inode->gfid, gfid), NULL);
+                uuid_utoa_r(parent->gfid, pgfid), name, uuid_utoa(inode->gfid),
+                NULL);
     }
 
     return dentry;
@@ -1351,7 +1337,8 @@ inode_rename(inode_table_t *table, inode_t *srcdir, const char *srcname,
     /* free the old dentry */
     dentry_destroy(dentry);
 
-    inode_table_prune(table);
+    if (table)
+        inode_table_prune(table);
 
     return 0;
 }
@@ -1390,6 +1377,7 @@ inode_parent(inode_t *inode, uuid_t pargfid, const char *name)
     inode_t *parent = NULL;
     inode_table_t *table = NULL;
     dentry_t *dentry = NULL;
+    gf_boolean_t search_for_inode = _gf_false;
 
     if (!inode) {
         gf_msg_callingfn(THIS->name, GF_LOG_WARNING, 0, LG_MSG_INODE_NOT_FOUND,
@@ -1399,19 +1387,22 @@ inode_parent(inode_t *inode, uuid_t pargfid, const char *name)
 
     table = inode->table;
 
+    if (pargfid && !gf_uuid_is_null(pargfid) && name)
+        search_for_inode = _gf_true;
+
     pthread_mutex_lock(&table->lock);
     {
-        if (pargfid && !gf_uuid_is_null(pargfid) && name) {
+        if (search_for_inode) {
             dentry = __dentry_search_for_inode(inode, pargfid, name);
         } else {
             dentry = __dentry_search_arbit(inode);
         }
 
-        if (dentry)
+        if (dentry) {
             parent = dentry->parent;
-
-        if (parent)
-            __inode_ref(parent, false);
+            if (parent)
+                __inode_ref(parent, false);
+        }
     }
     pthread_mutex_unlock(&table->lock);
 
@@ -1560,19 +1551,12 @@ inode_path(inode_t *inode, const char *name, char **bufp)
     return ret;
 }
 
-void
-__inode_table_set_lru_limit(inode_table_t *table, uint32_t lru_limit)
-{
-    table->lru_limit = lru_limit;
-    return;
-}
-
 void
 inode_table_set_lru_limit(inode_table_t *table, uint32_t lru_limit)
 {
     pthread_mutex_lock(&table->lock);
     {
-        __inode_table_set_lru_limit(table, lru_limit);
+        table->lru_limit = lru_limit;
     }
     pthread_mutex_unlock(&table->lock);
 
@@ -1595,9 +1579,6 @@ inode_table_prune(inode_table_t *table)
     uint64_t nlookup = 0;
     int64_t lru_size = 0;
 
-    if (!table)
-        return -1;
-
     INIT_LIST_HEAD(&purge);
 
     pthread_mutex_lock(&table->lock);
@@ -1686,17 +1667,14 @@ __inode_table_init_root(inode_table_t *table)
         0,
     };
 
-    if (!table)
-        return;
-
     root = inode_create(table);
 
     list_add(&root->list, &table->lru);
     table->lru_size++;
     root->in_lru_list = _gf_true;
 
-    iatt.ia_gfid[15] = 1;
     iatt.ia_ino = 1;
+    iatt.ia_gfid[15] = 1;
     iatt.ia_type = IA_IFDIR;
 
     __inode_link(root, NULL, NULL, &iatt, 0);
@@ -1763,13 +1741,14 @@ inode_table_with_invalidator(uint32_t lru_limit, xlator_t *xl,
     if (!new->dentry_pool)
         goto out;
 
-    new->inode_hash = (void *)GF_CALLOC(
-        new->inode_hashsize, sizeof(struct list_head), gf_common_mt_list_head);
+    new->inode_hash = (void *)GF_MALLOC(
+        new->inode_hashsize * sizeof(struct list_head), gf_common_mt_list_head);
     if (!new->inode_hash)
         goto out;
 
-    new->name_hash = (void *)GF_CALLOC(
-        new->dentry_hashsize, sizeof(struct list_head), gf_common_mt_list_head);
+    new->name_hash = (void *)GF_MALLOC(
+        new->dentry_hashsize * sizeof(struct list_head),
+        gf_common_mt_list_head);
     if (!new->name_hash)
         goto out;
 
@@ -1832,70 +1811,6 @@ inode_table_new(uint32_t lru_limit, xlator_t *xl, uint32_t dentry_hashsize,
                                         dentry_hashsize, inode_hashsize);
 }
 
-int
-inode_table_ctx_free(inode_table_t *table)
-{
-    int ret = 0;
-    inode_t *del = NULL;
-    inode_t *tmp = NULL;
-    int purge_count = 0;
-    int lru_count = 0;
-    int active_count = 0;
-    xlator_t *this = NULL;
-    int itable_size = 0;
-
-    if (!table)
-        return -1;
-
-    this = THIS;
-
-    pthread_mutex_lock(&table->lock);
-    {
-        list_for_each_entry_safe(del, tmp, &table->purge, list)
-        {
-            if (del->_ctx) {
-                __inode_ctx_free(del);
-                purge_count++;
-            }
-        }
-
-        list_for_each_entry_safe(del, tmp, &table->lru, list)
-        {
-            if (del->_ctx) {
-                __inode_ctx_free(del);
-                lru_count++;
-            }
-        }
-
-        /* should the contexts of active inodes be freed?
-         * Since before this function being called fds would have
-         * been migrated and would have held the ref on the new
-         * inode from the new inode table, the older inode would not
-         * be used.
-         */
-        list_for_each_entry_safe(del, tmp, &table->active, list)
-        {
-            if (del->_ctx) {
-                __inode_ctx_free(del);
-                active_count++;
-            }
-        }
-    }
-    pthread_mutex_unlock(&table->lock);
-
-    ret = purge_count + lru_count + active_count;
-    itable_size = table->active_size + table->lru_size + table->purge_size;
-    gf_msg_callingfn(this->name, GF_LOG_INFO, 0, LG_MSG_INODE_CONTEXT_FREED,
-                     "total %d (itable size: "
-                     "%d) inode contexts have been freed (active: %d, ("
-                     "active size: %d), lru: %d, (lru size: %d),  purge: "
-                     "%d, (purge size: %d))",
-                     ret, itable_size, active_count, table->active_size,
-                     lru_count, table->lru_size, purge_count,
-                     table->purge_size);
-    return ret;
-}
-
 void
 inode_table_destroy_all(glusterfs_ctx_t *ctx)
 {
@@ -1948,9 +1863,6 @@ inode_table_destroy(inode_table_t *inode_table)
      * Not sure which is the approach to be taken, going by approach 2.
      */
 
-    /* Approach 3:
-     * ret = inode_table_ctx_free (inode_table);
-     */
     pthread_mutex_lock(&inode_table->lock);
     {
         inode_table->cleanup_started = _gf_true;
@@ -2077,19 +1989,6 @@ inode_from_path(inode_table_t *itable, const char *path)
     return inode;
 }
 
-void
-inode_set_need_lookup(inode_t *inode, xlator_t *this)
-{
-    uint64_t need_lookup = LOOKUP_NEEDED;
-
-    if (!inode || !this)
-        return;
-
-    inode_ctx_set(inode, this, &need_lookup);
-
-    return;
-}
-
 /* Function behaviour:
  * Function return true if inode_ctx is not present,
  * or value stored in inode_ctx is LOOKUP_NEEDED.
@@ -2692,46 +2591,6 @@ inode_table_dump_to_dict(inode_table_t *itable, char *prefix, dict_t *dict)
     return;
 }
 
-size_t
-inode_ctx_size(inode_t *inode)
-{
-    int i = 0;
-    size_t size = 0;
-    xlator_t *xl = NULL, *old_THIS = NULL;
-
-    if (!inode)
-        goto out;
-
-    LOCK(&inode->lock);
-    {
-        for (i = 0; i < inode->table->ctxcount; i++) {
-            if (!inode->_ctx[i].xl_key)
-                continue;
-
-            xl = (xlator_t *)(long)inode->_ctx[i].xl_key;
-
-            /* If inode ref is taken when THIS is global xlator,
-             * the ctx xl_key is set, but the value is NULL.
-             * For global xlator the cbks can be NULL, hence check
-             * for the same */
-            if (!xl->cbks)
-                continue;
-
-            if (xl->cbks->ictxsize) {
-                if (!old_THIS)
-                    old_THIS = THIS;
-                THIS = xl;
-                size += xl->cbks->ictxsize(xl, inode);
-                THIS = old_THIS;
-            }
-        }
-    }
-    UNLOCK(&inode->lock);
-
-out:
-    return size;
-}
-
 /* *
  * This function finds name of the inode, if it has dentry. The dentry will be
  * created only if inode_link happens with valid parent and name. And this
diff --git a/libglusterfs/src/libglusterfs.sym b/libglusterfs/src/libglusterfs.sym
index 4dc7f2eccc2..a3250bf5cf8 100644
--- a/libglusterfs/src/libglusterfs.sym
+++ b/libglusterfs/src/libglusterfs.sym
@@ -769,10 +769,8 @@ __inode_ctx_set1
 inode_ctx_set1
 __inode_ctx_set2
 inode_ctx_set2
-inode_ctx_size
 inode_dump
 inode_dump_to_dict
-__inode_find
 inode_find
 inode_find_directory_name
 inode_forget
@@ -795,15 +793,12 @@ inode_ref_reduce_by_n
 inode_rename
 inode_resolve
 inode_set_namespace_inode
-inode_set_need_lookup
-inode_table_ctx_free
 inode_table_destroy
 inode_table_destroy_all
 inode_table_dump
 inode_table_dump_to_dict
 inode_table_new
 inode_table_with_invalidator
-__inode_table_set_lru_limit
 inode_table_set_lru_limit
 inode_unlink
 inode_unref
