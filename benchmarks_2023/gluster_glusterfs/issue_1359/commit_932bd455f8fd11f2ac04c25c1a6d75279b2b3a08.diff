diff --git a/libglusterfs/src/glusterfs/mem-pool.h b/libglusterfs/src/glusterfs/mem-pool.h
index 0fd1214e27d..e5b3276d047 100644
--- a/libglusterfs/src/glusterfs/mem-pool.h
+++ b/libglusterfs/src/glusterfs/mem-pool.h
@@ -202,6 +202,24 @@ gf_memdup(const void *src, size_t size)
     return dup_mem;
 }
 
+#ifdef GF_DISABLE_MEMPOOL
+
+/* No-op memory pool enough to fit current API without massive redesign. */
+
+struct mem_pool {
+    unsigned long sizeof_type;
+};
+
+#define mem_pools_init()                                                       \
+    do {                                                                       \
+    } while (0)
+#define mem_pools_fini()                                                       \
+    do {                                                                       \
+    } while (0)
+#define mem_pool_thread_destructor(pool_list) (void)pool_list
+
+#else /* !GF_DISABLE_MEMPOOL */
+
 /* kind of 'header' for the actual mem_pool_shared structure, this might make
  * it possible to dump some more details in a statedump */
 struct mem_pool {
@@ -210,10 +228,10 @@ struct mem_pool {
     unsigned long count; /* requested pool size (unused) */
     char *name;
     char *xl_name;
-    gf_atomic_t active; /* current allocations */
+    gf_atomic_t active;     /* current allocations */
 #ifdef DEBUG
-    gf_atomic_t hit;  /* number of allocations served from pt_pool */
-    gf_atomic_t miss; /* number of std allocs due to miss */
+    gf_atomic_t hit;        /* number of allocations served from pt_pool */
+    gf_atomic_t miss;       /* number of std allocs due to miss */
 #endif
     struct list_head owner; /* glusterfs_ctx_t->mempool_list */
     glusterfs_ctx_t *ctx;   /* take ctx->lock when updating owner */
@@ -287,6 +305,10 @@ void
 mem_pools_init(void); /* start the pool_sweeper thread */
 void
 mem_pools_fini(void); /* cleanup memory pools */
+void
+mem_pool_thread_destructor(per_thread_pool_list_t *pool_list);
+
+#endif /* GF_DISABLE_MEMPOOL */
 
 struct mem_pool *
 mem_pool_new_fn(glusterfs_ctx_t *ctx, unsigned long sizeof_type,
@@ -308,9 +330,6 @@ mem_get0(struct mem_pool *pool);
 void
 mem_pool_destroy(struct mem_pool *pool);
 
-void
-mem_pool_thread_destructor(per_thread_pool_list_t *pool_list);
-
 void
 gf_mem_acct_enable_set(void *ctx);
 
diff --git a/libglusterfs/src/mem-pool.c b/libglusterfs/src/mem-pool.c
index 1a87d277cc9..2d5a12b0a00 100644
--- a/libglusterfs/src/mem-pool.c
+++ b/libglusterfs/src/mem-pool.c
@@ -362,6 +362,30 @@ __gf_free(void *free_ptr)
     FREE(ptr);
 }
 
+#if defined(GF_DISABLE_MEMPOOL)
+
+struct mem_pool *
+mem_pool_new_fn(glusterfs_ctx_t *ctx, unsigned long sizeof_type,
+                unsigned long count, char *name)
+{
+    struct mem_pool *new;
+
+    new = GF_MALLOC(sizeof(struct mem_pool), gf_common_mt_mem_pool);
+    if (!new)
+        return NULL;
+
+    new->sizeof_type = sizeof_type;
+    return new;
+}
+
+void
+mem_pool_destroy(struct mem_pool *pool)
+{
+    GF_FREE(pool);
+}
+
+#else /* !GF_DISABLE_MEMPOOL */
+
 static pthread_mutex_t pool_lock = PTHREAD_MUTEX_INITIALIZER;
 static struct list_head pool_threads;
 static pthread_mutex_t pool_free_lock = PTHREAD_MUTEX_INITIALIZER;
@@ -371,7 +395,6 @@ static size_t pool_list_size;
 
 static __thread per_thread_pool_list_t *thread_pool_list = NULL;
 
-#if !defined(GF_DISABLE_MEMPOOL)
 #define N_COLD_LISTS 1024
 #define POOL_SWEEP_SECS 30
 
@@ -617,21 +640,29 @@ mem_pools_fini(void)
     pthread_mutex_unlock(&init_mutex);
 }
 
-#else
 void
-mem_pools_init(void)
-{
-}
-void
-mem_pools_fini(void)
-{
-}
-void
-mem_pool_thread_destructor(per_thread_pool_list_t *pool_list)
+mem_pool_destroy(struct mem_pool *pool)
 {
-}
+    if (!pool)
+        return;
 
-#endif
+    /* remove this pool from the owner (glusterfs_ctx_t) */
+    LOCK(&pool->ctx->lock);
+    {
+        list_del(&pool->owner);
+    }
+    UNLOCK(&pool->ctx->lock);
+
+    /* free this pool, but keep the mem_pool_shared */
+    GF_FREE(pool);
+
+    /*
+     * Pools are now permanent, so the mem_pool->pool is kept around. All
+     * of the objects *in* the pool will eventually be freed via the
+     * pool-sweeper thread, and this way we don't have to add a lot of
+     * reference-counting complexity.
+     */
+}
 
 struct mem_pool *
 mem_pool_new_fn(glusterfs_ctx_t *ctx, unsigned long sizeof_type,
@@ -700,21 +731,6 @@ mem_pool_new_fn(glusterfs_ctx_t *ctx, unsigned long sizeof_type,
     return new;
 }
 
-void *
-mem_get0(struct mem_pool *mem_pool)
-{
-    void *ptr = mem_get(mem_pool);
-    if (ptr) {
-#if defined(GF_DISABLE_MEMPOOL)
-        memset(ptr, 0, mem_pool->sizeof_type);
-#else
-        memset(ptr, 0, AVAILABLE_SIZE(mem_pool->pool->power_of_two));
-#endif
-    }
-
-    return ptr;
-}
-
 per_thread_pool_list_t *
 mem_get_pool_list(void)
 {
@@ -823,6 +839,23 @@ mem_get_from_pool(struct mem_pool *mem_pool)
     return retval;
 }
 
+#endif /* GF_DISABLE_MEMPOOL */
+
+void *
+mem_get0(struct mem_pool *mem_pool)
+{
+    void *ptr = mem_get(mem_pool);
+    if (ptr) {
+#if defined(GF_DISABLE_MEMPOOL)
+        memset(ptr, 0, mem_pool->sizeof_type);
+#else
+        memset(ptr, 0, AVAILABLE_SIZE(mem_pool->pool->power_of_two));
+#endif
+    }
+
+    return ptr;
+}
+
 void *
 mem_get(struct mem_pool *mem_pool)
 {
@@ -897,27 +930,3 @@ mem_put(void *ptr)
     }
 #endif /* GF_DISABLE_MEMPOOL */
 }
-
-void
-mem_pool_destroy(struct mem_pool *pool)
-{
-    if (!pool)
-        return;
-
-    /* remove this pool from the owner (glusterfs_ctx_t) */
-    LOCK(&pool->ctx->lock);
-    {
-        list_del(&pool->owner);
-    }
-    UNLOCK(&pool->ctx->lock);
-
-    /* free this pool, but keep the mem_pool_shared */
-    GF_FREE(pool);
-
-    /*
-     * Pools are now permanent, so the mem_pool->pool is kept around. All
-     * of the objects *in* the pool will eventually be freed via the
-     * pool-sweeper thread, and this way we don't have to add a lot of
-     * reference-counting complexity.
-     */
-}
diff --git a/libglusterfs/src/statedump.c b/libglusterfs/src/statedump.c
index 68e01104023..c1aa9b69aa2 100644
--- a/libglusterfs/src/statedump.c
+++ b/libglusterfs/src/statedump.c
@@ -349,26 +349,13 @@ gf_proc_dump_mem_info_to_dict(dict_t *dict)
 void
 gf_proc_dump_mempool_info(glusterfs_ctx_t *ctx)
 {
+#ifdef GF_DISABLE_MEMPOOL
+    gf_proc_dump_write("built with --disable-mempool", " so no memory pools");
+#else
     struct mem_pool *pool = NULL;
 
     gf_proc_dump_add_section("mempool");
 
-#if defined(OLD_MEM_POOLS)
-    list_for_each_entry(pool, &ctx->mempool_list, global_list)
-    {
-        gf_proc_dump_write("-----", "-----");
-        gf_proc_dump_write("pool-name", "%s", pool->name);
-        gf_proc_dump_write("hot-count", "%d", pool->hot_count);
-        gf_proc_dump_write("cold-count", "%d", pool->cold_count);
-        gf_proc_dump_write("padded_sizeof", "%lu", pool->padded_sizeof_type);
-        gf_proc_dump_write("alloc-count", "%" PRIu64, pool->alloc_count);
-        gf_proc_dump_write("max-alloc", "%d", pool->max_alloc);
-
-        gf_proc_dump_write("pool-misses", "%" PRIu64, pool->pool_misses);
-        gf_proc_dump_write("cur-stdalloc", "%d", pool->curr_stdalloc);
-        gf_proc_dump_write("max-stdalloc", "%d", pool->max_stdalloc);
-    }
-#else
     LOCK(&ctx->lock);
     {
         list_for_each_entry(pool, &ctx->mempool_list, owner)
@@ -388,15 +375,13 @@ gf_proc_dump_mempool_info(glusterfs_ctx_t *ctx)
         }
     }
     UNLOCK(&ctx->lock);
-
-    /* TODO: details of (struct mem_pool_shared) pool->pool */
-#endif
+#endif /* GF_DISABLE_MEMPOOL */
 }
 
 void
 gf_proc_dump_mempool_info_to_dict(glusterfs_ctx_t *ctx, dict_t *dict)
 {
-#if defined(OLD_MEM_POOLS)
+#ifndef GF_DISABLE_MEMPOOL
     struct mem_pool *pool = NULL;
     char key[GF_DUMP_MAX_BUF_LEN] = {
         0,
@@ -407,51 +392,47 @@ gf_proc_dump_mempool_info_to_dict(glusterfs_ctx_t *ctx, dict_t *dict)
     if (!ctx || !dict)
         return;
 
-    list_for_each_entry(pool, &ctx->mempool_list, global_list)
+    LOCK(&ctx->lock);
     {
-        snprintf(key, sizeof(key), "pool%d.name", count);
-        ret = dict_set_str(dict, key, pool->name);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.hotcount", count);
-        ret = dict_set_int32(dict, key, pool->hot_count);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.coldcount", count);
-        ret = dict_set_int32(dict, key, pool->cold_count);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.paddedsizeof", count);
-        ret = dict_set_uint64(dict, key, pool->padded_sizeof_type);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.alloccount", count);
-        ret = dict_set_uint64(dict, key, pool->alloc_count);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.max_alloc", count);
-        ret = dict_set_int32(dict, key, pool->max_alloc);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.max-stdalloc", count);
-        ret = dict_set_int32(dict, key, pool->max_stdalloc);
-        if (ret)
-            return;
-
-        snprintf(key, sizeof(key), "pool%d.pool-misses", count);
-        ret = dict_set_uint64(dict, key, pool->pool_misses);
-        if (ret)
-            return;
-        count++;
+        list_for_each_entry(pool, &ctx->mempool_list, owner)
+        {
+            int64_t active = GF_ATOMIC_GET(pool->active);
+
+            snprintf(key, sizeof(key), "pool%d.name", count);
+            ret = dict_set_str(dict, key, pool->name);
+            if (ret)
+                goto out;
+
+            snprintf(key, sizeof(key), "pool%d.active-count", count);
+            ret = dict_set_uint64(dict, key, active);
+            if (ret)
+                goto out;
+
+            snprintf(key, sizeof(key), "pool%d.sizeof-type", count);
+            ret = dict_set_uint64(dict, key, pool->sizeof_type);
+            if (ret)
+                goto out;
+
+            snprintf(key, sizeof(key), "pool%d.padded-sizeof", count);
+            ret = dict_set_uint64(dict, key, 1 << pool->pool->power_of_two);
+            if (ret)
+                goto out;
+
+            snprintf(key, sizeof(key), "pool%d.size", count);
+            ret = dict_set_uint64(dict, key,
+                                  (1 << pool->pool->power_of_two) * active);
+            if (ret)
+                goto out;
+
+            snprintf(key, sizeof(key), "pool%d.shared-pool", count);
+            ret = dict_set_static_ptr(dict, key, pool->pool);
+            if (ret)
+                goto out;
+        }
     }
-    ret = dict_set_int32(dict, "mempool-count", count);
-#endif
+out:
+    UNLOCK(&ctx->lock);
+#endif /* !GF_DISABLE_MEMPOOL */
 }
 
 void
