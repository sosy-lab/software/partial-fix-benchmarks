why not use JumpConsistentHash  to replace SuperFastHash to choose glusterfsd provided service
Hi @perrynzhou. If computing the remainder doesn't provide a balanced distribution, it means that the SuperFastHash() function doesn't generate good hashes. Do you have any test results where you see these effects ?

I've done a quick test by generating 10.000.000 gfid's and computing % 3, and the results seem quite balanced:

    Result = 0: 3.334.716
    Result = 1: 3.331.409
    Result = 2: 3.333.875

This is less than 0.1% deviation from the expected value.
> Hi @perrynzhou. If computing the remainder doesn't provide a balanced distribution, it means that the SuperFastHash() function doesn't generate good hashes. Do you have any test results where you see these effects ?
> 
> I've done a quick test by generating 10.000.000 gfid's and computing % 3, and the results seem quite balanced:
> 
> ```
> Result = 0: 3.334.716
> Result = 1: 3.331.409
> Result = 2: 3.333.875
> ```
> 
> This is less than 0.1% deviation from the expected value.

@xhernandez  we set cluster.read-hash-mode  to 3 in prod env on glusterfs 7.2;obviously we found that the IO bandwidth is far inferior to setting cluster.read-hash-mode to 1.we storge over 1000000000 files
@perrynzhou 3 is AFR_READ_POLICY_LESS_LOAD.  If you are interested in AFR_READ_POLICY_GFID_PID_HASH, you need to set it to 2. Although, I see that there is a bug in AFR_READ_POLICY_GFID_PID_HASH, where we are not copying the gfid into `uuid_t gfid_copy` before adding the pid of the client.
> @perrynzhou 3 is AFR_READ_POLICY_LESS_LOAD. If you are interested in AFR_READ_POLICY_GFID_PID_HASH, you need to set it to 2. Although, I see that there is a bug in AFR_READ_POLICY_GFID_PID_HASH, where we are not copying the gfid into `uuid_t gfid_copy` before adding the pid of the client.

@itisravi  can fixed it whith that way? init buffer,covert uuid to string and append pid  as SuperFastHash first parameter, then compute?

```
int
afr_hash_child(afr_read_subvol_args_t *args, afr_private_t *priv,
               unsigned char *readable)
{
    uuid_t gfid_copy = {
        0,
    };
    pid_t pid;
    int child = -1;
    char buffer[128] = {'\0'};
    switch (priv->hash_mode) {
        case AFR_READ_POLICY_FIRST_UP:
            break;
        case AFR_READ_POLICY_GFID_HASH:
            gf_uuid_copy(gfid_copy, args->gfid);
            child = SuperFastHash((char *)gfid_copy, sizeof(gfid_copy)) %
                    priv->child_count;
            break;
        case AFR_READ_POLICY_GFID_PID_HASH:
            if (args->ia_type != IA_IFDIR) {
                /*
                 * Why getpid?  Because it's one of the cheapest calls
                 * available - faster than gethostname etc. - and
                 * returns a constant-length value that's sure to be
                 * shorter than a UUID. It's still very unlikely to be
                 * the same across clients, so it still provides good
                 * mixing.  We're not trying for perfection here. All we
                 * need is a low probability that multiple clients
                 * won't converge on the same subvolume.
                 */
                pid = getpid();
                
                uuid_unparse(args->gfid,(char *)&buffer);
                snprintf((char *)&buffer,128-strlen((char *)&buffer),"-%ld",pid)
                
            }
            child = (SuperFastHash((char *)&buffer, sizeof(buffer)) +pid/priv->child_count) %
                    priv->child_count;
            break;
        case AFR_READ_POLICY_LESS_LOAD:
            child = afr_least_pending_reads_child(priv, readable);
            break;
        case AFR_READ_POLICY_LEAST_LATENCY:
            child = afr_least_latency_child(priv, readable);
            break;
        case AFR_READ_POLICY_LOAD_LATENCY_HYBRID:
            child = afr_least_latency_times_pending_reads_child(priv, readable);
            break;
    }

    return child;
}
```
Something simple like this:
```
diff --git a/xlators/cluster/afr/src/afr-common.c b/xlators/cluster/afr/src/afr-common.c
index 4c8fa31b6..1b996758c 100644
--- a/xlators/cluster/afr/src/afr-common.c
+++ b/xlators/cluster/afr/src/afr-common.c
@@ -2271,6 +2271,7 @@ afr_hash_child(afr_read_subvol_args_t *args, afr_private_t *priv,
                  * need is a low probability that multiple clients
                  * won't converge on the same subvolume.
                  */
+                uuid_copy (gfid_copy, args->gfid);
                 pid = getpid();
                 memcpy(gfid_copy, &pid, sizeof(pid));
             }
```

Would you like to send a patch with this change?

But anyway for your original query, @xhernandez 's test above indicates equal distribution (I don't think the above diff should make any difference) for , so maybe there isn't any need for  SuperFastHash() to be replaced with hash_jump_consistent().
> But anyway for your original query, @xhernandez 's test above indicates equal distribution (I don't think the above diff should make any difference) for , so maybe there isn't any need for SuperFastHash() to be replaced with hash_jump_consistent().

My test was only a small program that was doing SuperFastHash on many gfid's. I didn't use the actual code from this function and I didn't see that the gfid was not used actually. I was only testing whether SuperFastHash distribution was good.

But in this case the problem is real. When AFR_READ_POLICY_GFID_PID_HASH is used, the same child will be selected for all files, since the client pid is always the same.

We need to apply your patch, @itisravi. Probably this would fix the distribution problems @perrynzhou saw when using policy 2.

Only one suggestion: maybe instead of overwriting with the pid, we should xor its contents to keep randomness as high as possible.
>But in this case the problem is real. When AFR_READ_POLICY_GFID_PID_HASH is used, the same child will be selected for all files, since the client pid is always the same.

@xhernandez  Ah, you are right!
@perrynzhou  Can you test the patch by including Xavi's XOR suggestion and send it for review?
> @perrynzhou 3 is AFR_READ_POLICY_LESS_LOAD. If you are interested in AFR_READ_POLICY_GFID_PID_HASH, you need to set it to 2. Although, I see that there is a bug in AFR_READ_POLICY_GFID_PID_HASH, where we are not copying the gfid into `uuid_t gfid_copy` before adding the pid of the client.

OTOH, I'm not sure we need to copy args->gfid into gfid_copy when using AFR_READ_POLICY_GFID_HASH - I suspect we can use it directly?
> OTOH, I'm not sure we need to copy args->gfid into gfid_copy when using AFR_READ_POLICY_GFID_HASH - I suspect we can use it directly?

It looks like SuperFastHash() modifies the input...
> > But in this case the problem is real. When AFR_READ_POLICY_GFID_PID_HASH is used, the same child will be selected for all files, since the client pid is always the same.
> 
> @xhernandez Ah, you are right!
> @perrynzhou Can you test the patch by including Xavi's XOR suggestion and send it for review?

@itisravi  yes,i can test this patch by including Xavi's XOR  and review it in our env。after xor that  look like this?


> > OTOH, I'm not sure we need to copy args->gfid into gfid_copy when using AFR_READ_POLICY_GFID_HASH - I suspect we can use it directly?
> 
> It looks like SuperFastHash() modifies the input...

That's very surprising. Perhaps it's changing the pointer but not the data?
> > > OTOH, I'm not sure we need to copy args->gfid into gfid_copy when using AFR_READ_POLICY_GFID_HASH - I suspect we can use it directly?
> > 
> > 
> > It looks like SuperFastHash() modifies the input...
> 
> That's very surprising. Perhaps it's changing the pointer but not the data?

@mykaul @itisravi  are you sure SuperFastHash change  input pointer or input data?
  >  That's very surprising. Perhaps it's changing the pointer but not the data?

> @mykaul @itisravi are you sure SuperFastHash change input pointer or input data? i test that,it never change input pointer and data,at same time,all about data that as SuperFastHash first parameter is read,never do operations like data[index]=xxx

You are right, it is only advancing the pointer, not the data : `data += 2 * sizeof(uint16_t);`




> > That's very surprising. Perhaps it's changing the pointer but not the data?
> 
> > @mykaul @itisravi are you sure SuperFastHash change input pointer or input data? i test that,it never change input pointer and data,at same time,all about data that as SuperFastHash first parameter is read,never do operations like data[index]=xxx
> 
> You are right, it is only advancing the pointer, not the data : `data += 2 * sizeof(uint16_t);`

@itisravi change data pointer,but that only effect within SuperFastHash ，It's ok
> after xor that look like this?

@perrynzhou  I think you missed mentioning the change. You can directly send the patch out on gerrit and reviews can happen there.
> > after xor that look like this?
> 
> @perrynzhou I think you missed mentioning the change. You can directly send the patch out on gerrit and reviews can happen there.

The following document has the steps. You can skip running regression part as it happens automatically when you submit the change.
https://docs.gluster.org/en/latest/Developer-guide/Simplified-Development-Workflow/
A patch https://review.gluster.org/25057 has been posted that references this issue.

fixed AFR_READ_POLICY_GFID_PID_HASH policy bug,that used gfid and pid not just using client pid to compute hash

Change-Id: Ib927a770a486c95e4b157e76ba96e9904d1a9716
Fixes: #1499


> > > after xor that look like this?
> > 
> > 
> > @perrynzhou I think you missed mentioning the change. You can directly send the patch out on gerrit and reviews can happen there.
> 
> The following document has the steps. You can skip running regression part as it happens automatically when you submit the change.
> https://docs.gluster.org/en/latest/Developer-guide/Simplified-Development-Workflow/

@pranithk @itisravi  already pushed.
A patch https://review.gluster.org/25062 has been posted that references this issue.

replace uuid_copy to gf_uuid_copy and gfid_copy xor client pid

Change-Id: Ie692bcb4e8058fd183713fe62d08d1a812988b04
Fixes: #1499


@itisravi please review change that replace uuid_copy to gf_uuid_copy and gfid_copy used xor whih client of pid
@perrynzhou  Could you use the same change ID `Change-Id: Ib927a770a486c95e4b157e76ba96e9904d1a9716
`  when re-working the same patch?  That way, the same patch https://review.gluster.org/25057 will be updated.

A patch https://review.gluster.org/25063 has been posted that references this issue.

gfid_copy used xor with client pid,remove memcoy that no need anymore

Change-Id: I3a6c31898ada1702824e5b7e6db3b24a273e848e
Fixes: #1499


@itisravi  so sorry,i don't know how to used same change id for git commit,that may be each git commit can change chang-id? i already push changes again.
@perrynzhou  Please research on `git commit --amend`.  Once the editor opens, you can manually replace the change-id. 
> git commit --amend

@itisravi thank you very much.
@perrynzhou Welcome, will you be updating https://review.gluster.org/#/c/25057/?
> @perrynzhou Welcome, will you be updating https://review.gluster.org/#/c/25057/?
@itisravi  yes,i already update.

@perrynzhou Thanks! Just to confirm, you tested your latest patch with AFR_READ_POLICY_GFID_PID_HASH and you do see reads being distributed right?  
@itisravi  we build with source ,test it ,that Take effect
@itisravi  i has doubt. why need   " if (args->ia_type != IA_IFDIR) " condition for AFR_READ_POLICY_GFID_PID_HASH option?
@itisravi  i test io  throughput with diff read-hash-mode.read-hash-mode=2 is less than read-hash-mode=1.
- read-hash-mode=1:https://github.com/perrynzhou/glusterfs-issue-logs/blob/master/cluster.read-hash-mode%3D1.md
- read-hash-mode=2:https://github.com/perrynzhou/glusterfs-issue-logs/blob/master/cluster.read-hash-mode%3D2.md

- gluster build version
```
root@172.25.78.11 ~ $ gluster --version
glusterfs 9dev
Repository revision: git://git.gluster.org/glusterfs.git
Copyright (c) 2006-2016 Red Hat, Inc. <https://www.gluster.org/>
GlusterFS comes with ABSOLUTELY NO WARRANTY.
It is licensed to you under your choice of the GNU Lesser
General Public License, version 3 or any later version (LGPLv3
or later), or the GNU General Public License, version 2 (GPLv2),
in all cases as published by the Free Software Foundation.
```
> @itisravi i has doubt. why need " if (args->ia_type != IA_IFDIR) " condition for AFR_READ_POLICY_GFID_PID_HASH option?

Seems to be added in https://review.gluster.org/#/c/glusterfs/+/9332/
> i test io throughput with

I do not understand why `read` column is zero in cluster.read-hash-mode=2.md for all 3 bricks. But patch looks okay, I will test it once and merge it.
> > @itisravi i has doubt. why need " if (args->ia_type != IA_IFDIR) " condition for AFR_READ_POLICY_GFID_PID_HASH option?
> 
> Seems to be added in https://review.gluster.org/#/c/glusterfs/+/9332/
> 
> > i test io throughput with
> 
> I do not understand why `read` column is zero in cluster.read-hash-mode=2.md for all 3 bricks. But patch looks okay, I will test it once and merge it.

@itisravi thank you,yes,we test for write data to volume,so read colume is zero.
Re-opening for backports
A patch https://review.gluster.org/25090 has been posted that references this issue.

fixed AFR_READ_POLICY_GFID_PID_HASH policy bug

Change-Id: Ib927a770a486c95e4b157e76ba96e9904d1a9716
Fixes: #1499
Signed-off-by: perrynzhou <perrynzhou@gmail.com>
(cherry picked from commit 268faabed00995537394c04ac168c018167fbe27)


A patch https://review.gluster.org/25091 has been posted that references this issue.

fixed AFR_READ_POLICY_GFID_PID_HASH policy bug

Change-Id: Ib927a770a486c95e4b157e76ba96e9904d1a9716
Fixes: #1499
Signed-off-by: perrynzhou <perrynzhou@gmail.com>
(cherry picked from commit 268faabed00995537394c04ac168c018167fbe27)


@perrynzhou Please don't close the issue. The backports need to go in.
Thank you for your contributions.
Noticed that this issue is not having any activity in last ~6 months! We are marking this issue as stale because it has not had recent activity.
It will be closed in 2 weeks if no one responds with a comment here.

Closing this issue as there was no update since my last update on issue. If this is an issue which is still valid, feel free to open it.
