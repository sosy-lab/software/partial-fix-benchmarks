MAINTAINERS: Updated Email address (#1567)

Change-Id: I867809cc2015869e480389caef22695862ccda9a
Signed-off-by: Aravinda Vishwanathapura <aravinda@kadalu.io>
configure.ac: Add --enable-brickmux (#1572)

Introduce an --enable-brickmux configure option to
enable brick_mux as a default option.

Fixes: gluster#1569
Change-Id: Id0c61ee3500630889e2cb2b5816cd40f68d47c94
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
configure.ac: Add --enable-brickmux (#1572)

Introduce an --enable-brickmux configure option to
enable brick_mux as a default option.

Fixes: gluster#1569
Change-Id: Id0c61ee3500630889e2cb2b5816cd40f68d47c94
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
glusterd: Resolve clang format issue (#1622)

In the commit 8d54899724a31f29848e1461f68ce2cf40585056 clang
format issue was introduced in patch but clang check was not
complained at the time of running regression.

Fixes: #1569
Change-Id: Ib1fb039ebe3c77f39b8c686eb4699327256ac494
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
glusterd: Resolve clang format issue (#1622)

In the commit 8d54899724a31f29848e1461f68ce2cf40585056 clang
format issue was introduced in patch but clang check was not
complained at the time of running regression.

Fixes: #1569
Change-Id: Ib1fb039ebe3c77f39b8c686eb4699327256ac494
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
