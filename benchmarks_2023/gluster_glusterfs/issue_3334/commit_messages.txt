[posix.c] Coverity: Handle forward NULL

This patch addresses the coverity forward null error.

CID: 14760{19,20,21}

Signed-off-by: black.dragon74 <nickk.2974@gmail.com>
rpc: glfs_fini takes too much time and slows down qemu-img

glfs_fini takes time (20s) and slows down qemu-img, the time
is taken by due to timer_thread behavior. The timer thread
reregister function unless the timer event has not been
removed from a list.To avoid the delay the even needs
to add only if cleanup has not started.In case if cleanup
has started there is no need to add the event again in
timer thread list.

Fixes: #3334
Change-Id: I179c63c656c2917de9eb2bb44664453a1b6fc471
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
rpc: avoid restarting the bail out timer after initiating shut down (#3340)

glfs_fini takes time (20s) and slows down qemu-img, the time
is taken by due to timer_thread behavior. The timer thread
reregister function unless the timer event has not been
removed from a list.To avoid the delay the even needs
to add only if cleanup has not started.In case if cleanup
has started there is no need to add the event again in
timer thread list.

Fixes: #3334
Change-Id: I179c63c656c2917de9eb2bb44664453a1b6fc471
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
rpc: avoid restarting the bail out timer after initiating shut down

glfs_fini takes time (20s) and slows down qemu-img, the time
is taken by due to timer_thread behavior. The timer thread
reregister function unless the timer event has not been
removed from a list.To avoid the delay the even needs
to add only if cleanup has not started.In case if cleanup
has started there is no need to add the event again in
timer thread list.

> Fixes: #3334
> Change-Id: I179c63c656c2917de9eb2bb44664453a1b6fc471
> Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
> (Reviewed on upstream link https://github.com/gluster/glusterfs/pull/3340)

Change-Id: I58e1ed875399f49bc93d69be5975f2559f3785e9
Updates: #3334
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
rpc: avoid restarting the bail out timer after initiating shut down

glfs_fini takes time (20s) and slows down qemu-img, the time
is taken by due to timer_thread behavior. The timer thread
reregister function unless the timer event has not been
removed from a list.To avoid the delay the even needs
to add only if cleanup has not started.In case if cleanup
has started there is no need to add the event again in
timer thread list.

Fixes: #3334
Change-Id: I179c63c656c2917de9eb2bb44664453a1b6fc471
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
rpc: avoid restarting the bail out timer after initiating shut down (#3341)

glfs_fini takes time (20s) and slows down qemu-img, the time
is taken by due to timer_thread behavior. The timer thread
reregister function unless the timer event has not been
removed from a list.To avoid the delay the even needs
to add only if cleanup has not started.In case if cleanup
has started there is no need to add the event again in
timer thread list.

> Fixes: #3334
> Change-Id: I179c63c656c2917de9eb2bb44664453a1b6fc471
> Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
> (Reviewed on upstream link https://github.com/gluster/glusterfs/pull/3340)

Change-Id: I58e1ed875399f49bc93d69be5975f2559f3785e9
Updates: #3334
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
rpc: avoid restarting the bail out timer after initiating shut down (#3349)

glfs_fini takes time (20s) and slows down qemu-img, the time
is taken by due to timer_thread behavior. The timer thread
reregister function unless the timer event has not been
removed from a list.To avoid the delay the even needs
to add only if cleanup has not started.In case if cleanup
has started there is no need to add the event again in
timer thread list.

Fixes: #3334
Change-Id: I179c63c656c2917de9eb2bb44664453a1b6fc471
Signed-off-by: Mohit Agrawal <moagrawa@redhat.com>
