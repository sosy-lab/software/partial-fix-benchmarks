diff --git a/tests/basic/distribute/dht_seek_test.t b/tests/basic/distribute/dht_seek_test.t
new file mode 100644
index 00000000000..cbc892281eb
--- /dev/null
+++ b/tests/basic/distribute/dht_seek_test.t
@@ -0,0 +1,43 @@
+#!/bin/bash
+
+. $(dirname $0)/../../include.rc
+. $(dirname $0)/../../volume.rc
+. $(dirname $0)/../../dht.rc
+
+TESTS_EXPECTED_IN_LOOP=57
+# Initialize
+#------------------------------------------------------------
+cleanup;
+
+# Start glusterd
+TEST glusterd;
+TEST pidof glusterd;
+TEST $CLI volume info;
+
+# Create a volume
+TEST $CLI volume create $V0 $H0:$B0/${V0}{1,2,3};
+
+# Verify volume creation
+EXPECT "$V0" volinfo_field $V0 'Volume Name';
+EXPECT 'Created' volinfo_field $V0 'Status';
+
+# Start volume and verify successful start
+TEST $CLI volume start $V0;
+EXPECT 'Started' volinfo_field $V0 'Status';
+TEST glusterfs --volfile-id=$V0 --volfile-server=$H0 --entry-timeout=0 $M0;
+
+for i in {1..20}; do
+    TEST dd if=/dev/urandom of=${M0}/file.${i} bs=1k count=1 seek=128
+done
+
+TEST mkdir $M0/dst
+
+for i in {1..20}; do
+    TEST cp --sparse=always ${M0}/file.${i} ${M0}/dst
+done 
+
+for i in {1..20}; do
+   TEST cmp ${M0}/file.${i} ${M0}/dst/file.${i}
+done
+
+cleanup;
diff --git a/xlators/cluster/dht/src/dht-common.h b/xlators/cluster/dht/src/dht-common.h
index a402ad7e7f1..db3825208eb 100644
--- a/xlators/cluster/dht/src/dht-common.h
+++ b/xlators/cluster/dht/src/dht-common.h
@@ -64,19 +64,6 @@ typedef int (*dht_refresh_layout_unlock)(call_frame_t *frame, xlator_t *this,
 
 typedef int (*dht_refresh_layout_done_handle)(call_frame_t *frame);
 
-struct dht_layout_entry {
-    int err; /* 0 = normal
-                -1 = dir exists and no xattr
-                >0 = dir lookup failed with errno
-             */
-    uint32_t start;
-    uint32_t stop;
-    uint32_t commit_hash;
-    xlator_t *xlator;
-};
-
-typedef struct dht_layout_entry dht_layout_entry_t;
-
 struct dht_layout {
     int spread_cnt; /* layout spread count per directory,
                        is controlled by 'setxattr()' with
@@ -101,7 +88,16 @@ struct dht_layout {
     int type;
     gf_atomic_t ref; /* use with dht_conf_t->layout_lock */
     uint32_t search_unhashed;
-    dht_layout_entry_t list[];
+    struct {
+        int err; /* 0 = normal
+                    -1 = dir exists and no xattr
+                    >0 = dir lookup failed with errno
+                 */
+        uint32_t start;
+        uint32_t stop;
+        uint32_t commit_hash;
+        xlator_t *xlator;
+    } list[];
 };
 typedef struct dht_layout dht_layout_t;
 
@@ -536,6 +532,7 @@ struct dht_conf {
     int32_t refresh_interval;
     gf_lock_t subvolume_lock;
     time_t last_stat_fetch;
+    gf_lock_t layout_lock;
     dict_t *leaf_to_subvol;
     void *private; /* Can be used by wrapper xlators over
                       dht */
@@ -618,8 +615,6 @@ struct dht_conf {
     gf_boolean_t do_weighting;
 
     gf_boolean_t randomize_by_gfid;
-
-    gf_boolean_t ensure_durability;
 };
 typedef struct dht_conf dht_conf_t;
 
@@ -714,21 +709,25 @@ typedef struct dht_fd_ctx {
 #define DHT_STACK_UNWIND(fop, frame, params...)                                \
     do {                                                                       \
         dht_local_t *__local = NULL;                                           \
+        xlator_t *__xl = NULL;                                                 \
         if (frame) {                                                           \
+            __xl = frame->this;                                                \
             __local = frame->local;                                            \
             frame->local = NULL;                                               \
         }                                                                      \
         STACK_UNWIND_STRICT(fop, frame, params);                               \
-        dht_local_wipe(__local);                                               \
+        dht_local_wipe(__xl, __local);                                         \
     } while (0)
 
 #define DHT_STACK_DESTROY(frame)                                               \
     do {                                                                       \
         dht_local_t *__local = NULL;                                           \
+        xlator_t *__xl = NULL;                                                 \
+        __xl = frame->this;                                                    \
         __local = frame->local;                                                \
         frame->local = NULL;                                                   \
         STACK_DESTROY(frame->root);                                            \
-        dht_local_wipe(__local);                                               \
+        dht_local_wipe(__xl, __local);                                         \
     } while (0)
 
 #define DHT_UPDATE_TIME(ctx_sec, ctx_nsec, new_sec, new_nsec, post)            \
@@ -790,6 +789,9 @@ dht_layout_dir_mismatch(xlator_t *this, dht_layout_t *layout, xlator_t *subvol,
 xlator_t *
 dht_linkfile_subvol(xlator_t *this, inode_t *inode, struct iatt *buf,
                     dict_t *xattr);
+int
+dht_linkfile_unlink(call_frame_t *frame, xlator_t *this, xlator_t *subvol,
+                    loc_t *loc);
 
 int
 dht_layouts_init(xlator_t *this, dht_conf_t *conf);
@@ -811,7 +813,7 @@ int
 dht_deitransform(xlator_t *this, uint64_t y, xlator_t **subvol);
 
 void
-dht_local_wipe(dht_local_t *local);
+dht_local_wipe(xlator_t *this, dht_local_t *local);
 dht_local_t *
 dht_local_init(call_frame_t *frame, loc_t *loc, fd_t *fd, glusterfs_fop_t fop);
 int
@@ -866,9 +868,9 @@ int
 dht_layout_set(xlator_t *this, inode_t *inode, dht_layout_t *layout);
 ;
 void
-dht_layout_unref(dht_layout_t *layout);
+dht_layout_unref(xlator_t *this, dht_layout_t *layout);
 dht_layout_t *
-dht_layout_ref(dht_layout_t *layout);
+dht_layout_ref(xlator_t *this, dht_layout_t *layout);
 int
 dht_layout_index_for_subvol(dht_layout_t *layout, xlator_t *subvol);
 xlator_t *
@@ -974,6 +976,10 @@ int32_t
 dht_open(call_frame_t *frame, xlator_t *this, loc_t *loc, int32_t flags,
          fd_t *fd, dict_t *xdata);
 
+int32_t
+dht_seek(call_frame_t *frame, xlator_t *this, fd_t *fd, off_t offset,
+         gf_seek_what_t what, dict_t *xdata);
+
 int32_t
 dht_readv(call_frame_t *frame, xlator_t *this, fd_t *fd, size_t size,
           off_t offset, uint32_t flags, dict_t *xdata);
@@ -1147,7 +1153,9 @@ gf_defrag_start(void *this);
 
 int32_t
 gf_defrag_handle_hardlink(xlator_t *this, loc_t *loc, int *fop_errno);
-
+int
+dht_migrate_file(xlator_t *this, loc_t *loc, xlator_t *from, xlator_t *to,
+                 int flag, int *fop_errno);
 int
 dht_inode_ctx_layout_get(inode_t *inode, xlator_t *this,
                          dht_layout_t **layout_int);
@@ -1202,7 +1210,7 @@ void
 dht_log_new_layout_for_dir_selfheal(xlator_t *this, loc_t *loc,
                                     dht_layout_t *layout);
 
-void
+int
 dht_layout_sort(dht_layout_t *layout);
 
 int
@@ -1382,4 +1390,8 @@ dht_dir_layout_error_check(xlator_t *this, inode_t *inode);
 
 int
 dht_inode_ctx_mdsvol_set(inode_t *inode, xlator_t *this, xlator_t *mds_subvol);
+
+int
+dht_seek_cbk(call_frame_t *frame, void *cookie, xlator_t *this, int op_ret,
+             int op_errno, off_t offset, dict_t *xdata);
 #endif /* _DHT_H */
diff --git a/xlators/cluster/dht/src/dht-helper.c b/xlators/cluster/dht/src/dht-helper.c
index d42d39a3382..49ce4403a93 100644
--- a/xlators/cluster/dht/src/dht-helper.c
+++ b/xlators/cluster/dht/src/dht-helper.c
@@ -382,6 +382,14 @@ dht_check_and_open_fd_on_subvol_complete(int ret, call_frame_t *frame,
                        local->key, local->fd, local->rebalance.lock_cmd,
                        &local->rebalance.flock, local->xattr_req);
             break;
+
+        case GF_FOP_SEEK:
+            STACK_WIND_COOKIE(frame, dht_seek_cbk, subvol, subvol,
+                              subvol->fops->seek, local->fd,
+                              local->rebalance.offset, local->rebalance.flags,
+                              local->xattr_req);
+            break;
+
         default:
             gf_smsg(this->name, GF_LOG_ERROR, 0, DHT_MSG_UNKNOWN_FOP, "fd=%p",
                     fd, "gfid=%s", uuid_utoa(fd->inode->gfid), "name=%s",
@@ -710,7 +718,7 @@ dht_deitransform(xlator_t *this, uint64_t y, xlator_t **subvol_p)
 }
 
 void
-dht_local_wipe(dht_local_t *local)
+dht_local_wipe(xlator_t *this, dht_local_t *local)
 {
     int i = 0;
 
@@ -728,7 +736,7 @@ dht_local_wipe(dht_local_t *local)
         inode_unref(local->inode);
 
     if (local->layout) {
-        dht_layout_unref(local->layout);
+        dht_layout_unref(this, local->layout);
         local->layout = NULL;
     }
 
@@ -758,12 +766,12 @@ dht_local_wipe(dht_local_t *local)
         dict_unref(local->xdata);
 
     if (local->selfheal.layout) {
-        dht_layout_unref(local->selfheal.layout);
+        dht_layout_unref(this, local->selfheal.layout);
         local->selfheal.layout = NULL;
     }
 
     if (local->selfheal.refreshed_layout) {
-        dht_layout_unref(local->selfheal.refreshed_layout);
+        dht_layout_unref(this, local->selfheal.refreshed_layout);
         local->selfheal.refreshed_layout = NULL;
     }
 
@@ -950,7 +958,7 @@ dht_subvol_get_hashed(xlator_t *this, loc_t *loc)
 
 out:
     if (layout) {
-        dht_layout_unref(layout);
+        dht_layout_unref(this, layout);
     }
 
     return subvol;
@@ -975,7 +983,7 @@ dht_subvol_get_cached(xlator_t *this, inode_t *inode)
 
 out:
     if (layout) {
-        dht_layout_unref(layout);
+        dht_layout_unref(this, layout);
     }
 
     return subvol;
@@ -1380,7 +1388,7 @@ dht_migration_complete_check_task(void *data)
 
     /* update local. A layout is set in inode-ctx in lookup already */
 
-    dht_layout_unref(local->layout);
+    dht_layout_unref(this, local->layout);
 
     local->layout = dht_layout_get(frame->this, inode);
     local->cached_subvol = dst_node;
@@ -1792,40 +1800,18 @@ dht_inode_ctx_layout_set(inode_t *inode, xlator_t *this,
 {
     dht_inode_ctx_t *ctx = NULL;
     int ret = -1;
-    uint64_t ctx_int = 0;
-    dht_layout_t *old_layout = NULL;
-
-    LOCK(&inode->lock);
-    {
-        ret = __inode_ctx_get(inode, this, &ctx_int);
-        if (!ret) {
-            ctx = (dht_inode_ctx_t *)(uintptr_t)ctx_int;
-            if (ctx) {
-                old_layout = ctx->layout;
-                ctx->layout = layout_int;
-            }
-        }
 
-        if (!ctx) {
-            ctx = GF_CALLOC(1, sizeof(*ctx), gf_dht_mt_inode_ctx_t);
-            if (ctx) {
-                ctx->layout = layout_int;
-                ctx_int = (long)ctx;
-                ret = __inode_ctx_set0(inode, this, &ctx_int);
-                if (ret)
-                    GF_FREE(ctx);
-            } else {
-                ret = -1;
-            }
-        }
-        if (!ret && layout_int)
-            dht_layout_ref(ctx->layout);
+    ret = dht_inode_ctx_get(inode, this, &ctx);
+    if (!ret && ctx) {
+        ctx->layout = layout_int;
+    } else {
+        ctx = GF_CALLOC(1, sizeof(*ctx), gf_dht_mt_inode_ctx_t);
+        if (!ctx)
+            return ret;
+        ctx->layout = layout_int;
     }
-    UNLOCK(&inode->lock);
 
     ret = dht_inode_ctx_set(inode, this, ctx);
-    if (old_layout)
-        dht_layout_unref(old_layout);
 
     return ret;
 }
diff --git a/xlators/cluster/dht/src/dht-inode-read.c b/xlators/cluster/dht/src/dht-inode-read.c
index dbb8070b0da..c665172a5b9 100644
--- a/xlators/cluster/dht/src/dht-inode-read.c
+++ b/xlators/cluster/dht/src/dht-inode-read.c
@@ -1656,3 +1656,126 @@ dht_finodelk(call_frame_t *frame, xlator_t *this, const char *volume, fd_t *fd,
 
     return 0;
 }
+
+static int
+dht_seek2(xlator_t *subvol, call_frame_t *frame, int ret)
+{
+    dht_local_t *local = NULL;
+    int op_errno = EINVAL;
+    off_t offset = 0;
+
+    if (!frame)
+        goto out;
+
+    local = frame->local;
+    op_errno = local->op_errno;
+    offset = local->rebalance.offset;
+
+    if (we_are_not_migrating(ret)) {
+        /* This DHT layer is not migrating the file */
+        DHT_STACK_UNWIND(seek, frame, -1, local->op_errno, 0, NULL);
+        return 0;
+    }
+
+    if (subvol == NULL)
+        goto out;
+
+    local->call_cnt = 2;
+    STACK_WIND_COOKIE(frame, dht_seek_cbk, subvol, subvol, subvol->fops->seek,
+                      local->fd, offset, local->rebalance.flags,
+                      local->xattr_req);
+    return 0;
+
+out:
+    DHT_STACK_UNWIND(seek, frame, -1, op_errno, 0, NULL);
+    return 0;
+}
+
+int
+dht_seek_cbk(call_frame_t *frame, void *cookie, xlator_t *this, int op_ret,
+             int op_errno, off_t offset, dict_t *xdata)
+{
+    dht_local_t *local = NULL;
+    xlator_t *prev = NULL;
+    int ret = 0;
+
+    local = frame->local;
+    prev = cookie;
+
+    /* lseek fails with EBADF if dht has not yet opened the fd
+     * on the cached subvol. This could happen if the file was migrated
+     * and a lookup updated the cached subvol in the inode ctx.
+     * We only check once as this could be a valid bad fd error.
+     */
+
+    if (dht_check_remote_fd_failed_error(local, op_ret, op_errno)) {
+        ret = dht_check_and_open_fd_on_subvol(this, frame);
+        if (ret)
+            goto out;
+        return 0;
+    }
+
+    local->op_errno = op_errno;
+    if ((op_ret == -1) && !dht_inode_missing(op_errno)) {
+        gf_msg_debug(this->name, op_errno, "subvolume %s returned -1",
+                     prev->name);
+        goto out;
+    }
+
+    if ((op_ret == -1) && ((op_errno == ENXIO) || (op_errno == EOVERFLOW)))
+        goto out;
+
+    if (!op_ret || (local->call_cnt != 1))
+        goto out;
+
+    /* rebalance would have happened */
+    local->rebalance.target_op_fn = dht_seek2;
+    ret = dht_rebalance_complete_check(this, frame);
+    if (!ret)
+        return 0;
+
+out:
+    DHT_STACK_UNWIND(seek, frame, op_ret, op_errno, offset, xdata);
+
+    return 0;
+}
+
+int
+dht_seek(call_frame_t *frame, xlator_t *this, fd_t *fd, off_t offset,
+         gf_seek_what_t what, dict_t *xdata)
+{
+    xlator_t *subvol = NULL;
+    dht_local_t *local = NULL;
+    int op_errno = EINVAL;
+
+    local = dht_local_init(frame, NULL, fd, GF_FOP_SEEK);
+    if (!local) {
+        op_errno = ENOMEM;
+        goto err;
+    }
+
+    subvol = local->cached_subvol;
+    if (!subvol) {
+        gf_msg_debug(this->name, 0, "no cached subvolume for fd=%p", fd);
+        op_errno = EINVAL;
+        goto err;
+    }
+
+    if (xdata)
+        local->xattr_req = dict_ref(xdata);
+
+    local->rebalance.offset = offset;
+    local->rebalance.flags = what;
+    local->call_cnt = 1;
+
+    STACK_WIND_COOKIE(frame, dht_seek_cbk, subvol, subvol, subvol->fops->seek,
+                      fd, local->rebalance.offset, local->rebalance.flags,
+                      local->xattr_req);
+
+    return 0;
+
+err:
+    DHT_STACK_UNWIND(seek, frame, -1, op_errno, offset, xdata);
+
+    return 0;
+}
diff --git a/xlators/cluster/dht/src/dht.c b/xlators/cluster/dht/src/dht.c
index 53de8292704..5f6e994475a 100644
--- a/xlators/cluster/dht/src/dht.c
+++ b/xlators/cluster/dht/src/dht.c
@@ -72,6 +72,7 @@ struct xlator_fops fops = {
     .finodelk = dht_finodelk,
     .lk = dht_lk,
     .lease = dht_lease,
+    .seek = dht_seek,
 
     /* Inode write operations */
     .fremovexattr = dht_fremovexattr,
