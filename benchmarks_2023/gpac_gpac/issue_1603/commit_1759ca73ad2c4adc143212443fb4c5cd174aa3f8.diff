diff --git a/applications/gpac/main.c b/applications/gpac/main.c
index 9b075b5023..db95e1d283 100644
--- a/applications/gpac/main.c
+++ b/applications/gpac/main.c
@@ -52,6 +52,9 @@ static u32 help_flags = 0;
 static const char *make_fileio(const char *inargs, const char **out_arg, Bool is_input, GF_Err *e);
 static void cleanup_file_io();
 
+//coverage for custom filters
+static GF_Filter *load_custom_filter(GF_FilterSession *sess, char *opts, GF_Err *e);
+
 FILE *sidebar_md=NULL;
 static FILE *helpout = NULL;
 
@@ -1799,6 +1802,7 @@ static int gpac_main(int argc, char **argv)
 		}
 		else if (!strcmp(arg, "-ltf")) {
 			load_test_filters = GF_TRUE;
+		} else if (!strncmp(arg, "-lcf", 4)) {
 		} else if (!strcmp(arg, "-stats")) {
 			dump_stats = GF_TRUE;
 		} else if (!strcmp(arg, "-graph")) {
@@ -1995,6 +1999,10 @@ static int gpac_main(int argc, char **argv)
 			i++;
 			f_loaded = GF_TRUE;
 		}
+		else if (!strncmp(arg, "-lcf", 4)) {
+			f_loaded = GF_TRUE;
+			filter = load_custom_filter(session, arg+4, &e);
+		}
 		//appart from the above src/dst, other args starting with - are not filters
 		else if (arg[0]=='-') {
 			if (!strcmp(arg, "-xopt")) has_xopt = GF_TRUE;
@@ -4165,3 +4173,49 @@ static void cleanup_file_io()
 	all_gfio_defined = NULL;
 }
 
+static GF_Err cust_configure_pid(GF_Filter *filter, GF_FilterPid *pid, Bool is_remove)
+{
+	GF_FilterEvent evt;
+	if (is_remove) return GF_OK;
+
+	GF_FEVT_INIT(evt, GF_FEVT_PLAY, pid);
+	gf_filter_pid_send_event(pid, &evt);
+	gf_filter_pid_set_framing_mode(pid, GF_TRUE);
+	return GF_OK;
+}
+static GF_Err cust_process(GF_Filter *filter)
+{
+	u32 i;
+	for (i=0; i<gf_filter_get_ipid_count(filter); i++) {
+		GF_FilterPid *pid = gf_filter_get_ipid(filter, i);
+		const char *name = gf_filter_pid_get_name(pid);
+		while (1) {
+			GF_FilterPacket *pck = gf_filter_pid_get_packet(pid);
+			if (!pck) break;
+			fprintf(stderr, "PID %s Got packet CTS "LLU"\n", name, gf_filter_pck_get_cts(pck));
+			gf_filter_pid_drop_packet(pid);
+		}
+	}
+	return GF_OK;
+}
+
+static GF_Filter *load_custom_filter(GF_FilterSession *sess, char *opts, GF_Err *e)
+{
+	GF_Filter *f = gf_fs_new_filter(sess, "custom", e);
+	if (!f) return NULL;
+
+	*e = gf_filter_push_caps(f, GF_PROP_PID_STREAM_TYPE, &PROP_UINT(GF_STREAM_VISUAL), NULL, GF_CAPS_INPUT, 0);
+	if (*e) return NULL;
+	*e = gf_filter_set_configure_ckb(f, cust_configure_pid);
+	if (*e) return NULL;
+	*e = gf_filter_set_process_ckb(f, cust_process);
+	if (*e) return NULL;
+	*e = gf_filter_set_process_event_ckb(f, NULL);
+	if (*e) return NULL;
+	*e = gf_filter_set_reconfigure_output_ckb(f, NULL);
+	if (*e) return NULL;
+	*e = gf_filter_set_probe_data_cbk(f, NULL);
+	if (*e) return NULL;
+
+	return f;
+}
diff --git a/include/gpac/filters.h b/include/gpac/filters.h
index 84536b84c6..9d5cf26fa9 100644
--- a/include/gpac/filters.h
+++ b/include/gpac/filters.h
@@ -602,6 +602,8 @@ GF_Err gf_fs_load_script(GF_FilterSession *session, const char *jsfile);
 /*! @} */
 
 
+
+
 /*!
 \addtogroup fs_props Filter Properties
 \ingroup filters_grp
@@ -1773,6 +1775,9 @@ typedef enum
 	/*! Indicates the filter requires graph resolver (typically because it creates new destinations/sinks at run time)*/
 	GF_FS_REG_REQUIRES_RESOLVER = 1<<11,
 
+
+	/*! flag dynamically set at runtime for custom filters*/
+	GF_FS_REG_CUSTOM = 0x40000000,
 	/*! flag dynamically set at runtime for registries loaded through shared libraries*/
 	GF_FS_REG_DYNLIB = 0x80000000
 } GF_FSRegisterFlags;
@@ -3849,6 +3854,78 @@ This is typically used by sink filters to decide if they can hold references to
 */
 Bool gf_filter_pck_is_blocking_ref(GF_FilterPacket *pck);
 
+/*! @} */
+
+
+/*!
+\addtogroup fs_props Filter Properties
+\ingroup filters__cust_grp
+\brief Custom Filter
+
+Custom filters are filters created by the app with no associated registry.
+The app is responsible for assigning capabilities to the filter, and setting callback functions.
+Each callback is optionnal, but a custom filter should at least have a process callback, and a configure_pid callback if not a source filter.
+
+Custom filters do not have any arguments exposed, and cannot be selected for sink or source filters.
+If your app requires custom I/Os for source or sinks, use \ref GF_FileIO.
+@{
+ */
+
+/*! Loads custom filter
+\param session filter session
+\param name name of filter to use - optional, may be NULL
+\param e set to the error code if any - optional, may be NULL
+\return filter or NULL if error
+*/
+GF_Filter *gf_fs_new_filter(GF_FilterSession *session, const char *name, GF_Err *e);
+
+/*! Push a new capability for a custom filter
+\param filter the target filter
+\param code the capability code - cf \ref GF_FilterCapability
+\param value the capability value - cf \ref GF_FilterCapability
+\param name the capability name - cf \ref GF_FilterCapability
+\param flags the capability flags - cf \ref GF_FilterCapability
+\param priority the capability priority - cf \ref GF_FilterCapability
+\return error if any
+ */
+GF_Err gf_filter_push_caps(GF_Filter *filter, u32 code, GF_PropertyValue *value, const char *name, u32 flags, u8 priority);
+
+/*! Set the process function for  a custom filter
+\param filter the target filter
+\param process_cbk the process callback, may be NULL -  cf process in  \ref __gf_filter_register
+\return error if any
+ */
+GF_Err gf_filter_set_process_ckb(GF_Filter *filter, GF_Err (*process_cbk)(GF_Filter *filter) );
+
+/*! Set the PID configuration function for  a custom filter
+\param filter the target filter
+\param configure_cbk the configure callback, may be NULL -  cf configure_pid in \ref __gf_filter_register
+\return error if any
+ */
+GF_Err gf_filter_set_configure_ckb(GF_Filter *filter, GF_Err (*configure_cbk)(GF_Filter *filter, GF_FilterPid *PID, Bool is_remove) );
+
+/*! Set the process event function for  a custom filter
+\param filter the target filter
+\param process_event_cbk the process event callback, may be NULL -  cf process_event in \ref __gf_filter_register
+\return error if any
+ */
+GF_Err gf_filter_set_process_event_ckb(GF_Filter *filter, Bool (*process_event_cbk)(GF_Filter *filter, const GF_FilterEvent *evt) );
+
+/*! Set the reconfigure output function for  a custom filter
+\param filter the target filter
+\param reconfigure_output_cbk the reconfigure callback, may be NULL -  cf reconfigure_output_cbk in \ref __gf_filter_register
+\return error if any
+ */
+GF_Err gf_filter_set_reconfigure_output_ckb(GF_Filter *filter, GF_Err (*reconfigure_output_cbk)(GF_Filter *filter, GF_FilterPid *PID) );
+
+/*! Set the data prober function for  a custom filter
+\param filter the target filter
+\param probe_data_cbk the data prober callback , may be NULL-  cf probe_data in \ref __gf_filter_register
+\return error if any
+ */
+GF_Err gf_filter_set_probe_data_cbk(GF_Filter *filter, const char * (*probe_data_cbk)(const u8 *data, u32 size, GF_FilterProbeScore *score) );
+
+
 /*! @} */
 
 #ifdef __cplusplus
diff --git a/share/doc/idl/filtersession.idl b/share/doc/idl/filtersession.idl
index 7ee8e554f1..f7ad5bbf82 100644
--- a/share/doc/idl/filtersession.idl
+++ b/share/doc/idl/filtersession.idl
@@ -115,6 +115,14 @@ boolean fire_event(FilterEvent evt, optional JSFSFilter *filter=null, optional b
 \param enable enables reporting if true*/
 void reporting(boolean enable);
 
+/*! creates a new custom JS filter. This filter will share the same script context as the filter session script - see \ref JSFilter
+\note A custom filter cannot use default arguments, and will not have the initialize callback function called.
+
+\param name name for the filter
+\return new filter
+*/
+JSFilter new_filter(DOMString name=NULL);
+
 /*! number of filters in the session - see \ref gf_fs_get_filters_count. 
 This number is only valid when session is locked*/
 attribute long nb_filters;
diff --git a/src/filter_core/filter.c b/src/filter_core/filter.c
index f719a0af40..c9135040d9 100644
--- a/src/filter_core/filter.c
+++ b/src/filter_core/filter.c
@@ -503,6 +503,12 @@ void gf_filter_del(GF_Filter *filter)
 		gf_free(filter->iname);
 #endif
 
+	if (filter->freg && (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		if (! (filter->freg->flags & GF_FS_REG_SCRIPT))
+			if (filter->forced_caps) gf_free( (void *) filter->forced_caps);
+		gf_free( (char *) filter->freg->name);
+		gf_free( (void *) filter->freg);
+	}
 	gf_free(filter);
 }
 
@@ -3905,3 +3911,81 @@ GF_Err gf_filter_set_event_target(GF_Filter *filter, Bool enable_events)
 	return GF_OK;
 }
 
+GF_EXPORT
+GF_Err gf_filter_push_caps(GF_Filter *filter, u32 code, GF_PropertyValue *value, const char *name, u32 flags, u8 priority)
+{
+	u32 nb_caps;
+	GF_FilterCapability *caps;
+	if (! (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to push cap on non custom filter %s\n", filter->freg->name));
+		return GF_BAD_PARAM;
+	}
+	caps = (GF_FilterCapability *)filter->forced_caps;
+	nb_caps = filter->nb_forced_caps;
+	caps = gf_realloc(caps, sizeof(GF_FilterCapability)*(nb_caps+1) );
+	if (!caps) return GF_OUT_OF_MEM;
+	caps[nb_caps].code = code;
+	caps[nb_caps].val = *value;
+	caps[nb_caps].name = name ? gf_strdup(name) : NULL;
+	caps[nb_caps].priority = priority;
+	caps[nb_caps].flags = flags;
+	filter->nb_forced_caps++;
+	filter->forced_caps = caps;
+	return GF_OK;
+}
+
+GF_EXPORT
+GF_Err gf_filter_set_process_ckb(GF_Filter *filter, GF_Err (*process_cbk)(GF_Filter *filter) )
+{
+	if (! (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to assign filter callback on non custom filter %s\n", filter->freg->name));
+		return GF_BAD_PARAM;
+	}
+	((GF_FilterRegister *) filter->freg)->process = process_cbk;
+	return GF_OK;
+}
+
+
+GF_EXPORT
+GF_Err gf_filter_set_configure_ckb(GF_Filter *filter, GF_Err (*configure_cbk)(GF_Filter *filter, GF_FilterPid *PID, Bool is_remove) )
+{
+	if (! (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to assign filter callback on non custom filter %s\n", filter->freg->name));
+		return GF_BAD_PARAM;
+	}
+	((GF_FilterRegister *) filter->freg)->configure_pid = configure_cbk;
+	return GF_OK;
+}
+
+GF_EXPORT
+GF_Err gf_filter_set_process_event_ckb(GF_Filter *filter, Bool (*process_event_cbk)(GF_Filter *filter, const GF_FilterEvent *evt) )
+{
+	if (! (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to assign filter callback on non custom filter %s\n", filter->freg->name));
+		return GF_BAD_PARAM;
+	}
+	((GF_FilterRegister *) filter->freg)->process_event = process_event_cbk;
+	return GF_OK;
+}
+
+GF_EXPORT
+GF_Err gf_filter_set_reconfigure_output_ckb(GF_Filter *filter, GF_Err (*reconfigure_output_cbk)(GF_Filter *filter, GF_FilterPid *PID) )
+{
+	if (! (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to assign filter callback on non custom filter %s\n", filter->freg->name));
+		return GF_BAD_PARAM;
+	}
+	((GF_FilterRegister *) filter->freg)->reconfigure_output = reconfigure_output_cbk;
+	return GF_OK;
+}
+
+GF_EXPORT
+GF_Err gf_filter_set_probe_data_cbk(GF_Filter *filter, const char * (*probe_data_cbk)(const u8 *data, u32 size, GF_FilterProbeScore *score) )
+{
+	if (! (filter->freg->flags & GF_FS_REG_CUSTOM)) {
+		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to assign filter callback on non custom filter %s\n", filter->freg->name));
+		return GF_BAD_PARAM;
+	}
+	((GF_FilterRegister *) filter->freg)->probe_data = probe_data_cbk;
+	return GF_OK;
+}
diff --git a/src/filter_core/filter_pid.c b/src/filter_core/filter_pid.c
index a391851c15..7e6bc1f889 100644
--- a/src/filter_core/filter_pid.c
+++ b/src/filter_core/filter_pid.c
@@ -2400,7 +2400,7 @@ static GF_FilterRegDesc *gf_filter_reg_build_graph(GF_List *links, const GF_Filt
 	GF_FilterRegDesc *reg_desc = NULL;
 	const GF_FilterCapability *caps = freg->caps;
 	nb_caps = freg->nb_caps;
-	if (dst_filter && (freg->flags & GF_FS_REG_SCRIPT)) {
+	if (dst_filter && (freg->flags & (GF_FS_REG_SCRIPT|GF_FS_REG_CUSTOM))) {
 		caps = dst_filter->forced_caps;
 		nb_caps = dst_filter->nb_forced_caps;
 	}
@@ -2660,7 +2660,7 @@ static void gf_filter_pid_resolve_link_dijkstra(GF_FilterPid *pid, GF_Filter *ds
 			disable_filter = GF_TRUE;
 		}
 		//freg shall be instantiated
-		else if ((freg->flags & (GF_FS_REG_EXPLICIT_ONLY|GF_FS_REG_SCRIPT)) && (freg != pid->filter->freg) && (freg != dst->freg) ) {
+		else if ((freg->flags & (GF_FS_REG_EXPLICIT_ONLY|GF_FS_REG_SCRIPT|GF_FS_REG_CUSTOM)) && (freg != pid->filter->freg) && (freg != dst->freg) ) {
 			assert(freg != dst->freg);
 			disable_filter = GF_TRUE;
 		}
diff --git a/src/filter_core/filter_session.c b/src/filter_core/filter_session.c
index d15e9b0703..21c44055d6 100644
--- a/src/filter_core/filter_session.c
+++ b/src/filter_core/filter_session.c
@@ -3139,6 +3139,34 @@ void gf_fs_check_graph_load(GF_FilterSession *fsess, Bool for_load)
 	}
 }
 
+GF_EXPORT
+GF_Filter *gf_fs_new_filter(GF_FilterSession *fsess, const char *name, GF_Err *e)
+{
+	GF_Filter *f;
+	char szRegName[25];
+	GF_FilterRegister *reg;
+
+	GF_SAFEALLOC(reg, GF_FilterRegister);
+	if (!reg) return NULL;
+
+	reg->flags = 0;
+#ifndef GPAC_DISABLE_DOC
+	reg->author = "custom";
+	reg->description = "custom";
+	reg->help = "custom";
+#endif
+	reg->version = "custom";
+	sprintf(szRegName, "custom%p", reg);
+	reg->name = gf_strdup(name ? name : szRegName);
+	reg->flags = GF_FS_REG_CUSTOM | GF_FS_REG_EXPLICIT_ONLY;
+
+	f = gf_filter_new(fsess, reg, NULL, NULL, 0, e, NULL, GF_FALSE);
+	if (!f) return NULL;
+	if (name)
+		gf_filter_set_name(f, name);
+	return f;
+}
+
 #ifndef GPAC_DISABLE_3D
 
 
diff --git a/src/filter_core/filter_session_js.c b/src/filter_core/filter_session_js.c
index e6e5186e1d..bb60d7e335 100644
--- a/src/filter_core/filter_session_js.c
+++ b/src/filter_core/filter_session_js.c
@@ -1076,6 +1076,24 @@ static JSValue jsfs_reporting(JSContext *ctx, JSValueConst this_val, int argc, J
 	return JS_UNDEFINED;
 }
 
+JSValue jsfilter_initialize_custom(GF_Filter *filter, JSContext *ctx);
+
+static JSValue jsfs_new_filter(JSContext *ctx, JSValueConst this_val, int argc, JSValueConst *argv)
+{
+	GF_Filter *f;
+	GF_Err e;
+	const char *name = NULL;
+	GF_FilterSession *fs = JS_GetOpaque(this_val, fs_class_id);
+	if (!fs) return JS_EXCEPTION;
+	if (argc) name = JS_ToCString(ctx, argv[0]);
+
+	f = gf_fs_new_filter(fs, name, &e);
+	if (name) JS_FreeCString(ctx, name);
+	if (!f) return js_throw_err(ctx, e);
+
+	return jsfilter_initialize_custom(f, ctx);
+}
+
 
 static const JSCFunctionListEntry fs_funcs[] = {
     JS_CGETSET_MAGIC_DEF_ENUM("nb_filters", jsfs_prop_get, NULL, JSFS_NB_FILTERS),
@@ -1096,6 +1114,7 @@ static const JSCFunctionListEntry fs_funcs[] = {
     JS_CFUNC_DEF("add_filter", 0, jsfs_add_filter),
     JS_CFUNC_DEF("fire_event", 0, jsfs_fire_event),
     JS_CFUNC_DEF("reporting", 0, jsfs_reporting),
+    JS_CFUNC_DEF("new_filter", 0, jsfs_new_filter),
 
 };
 
diff --git a/src/filters/jsfilter.c b/src/filters/jsfilter.c
index a310cd427f..a19dc1bc74 100644
--- a/src/filters/jsfilter.c
+++ b/src/filters/jsfilter.c
@@ -122,6 +122,7 @@ typedef struct
 	const char *js;
 
 	GF_Filter *filter;
+	Bool is_custom;
 
 	JSContext *ctx;
 
@@ -2023,7 +2024,9 @@ static JSValue jsf_pid_get_packet(JSContext *ctx, JSValueConst this_val, int arg
 	GF_JSPckCtx *pckctx;
 	GF_JSPidCtx *pctx = JS_GetOpaque(this_val, jsf_pid_class_id);
     if (!pctx) return JS_EXCEPTION;
-
+	if (!pctx->jsf->filter->in_process)
+		return js_throw_err_msg(ctx, GF_BAD_PARAM, "Filter %s attempt to query packet outside process callback not allowed!\n", pctx->jsf->filter->name);
+		
     pck = gf_filter_pid_get_packet(pctx->pid);
 	if (!pck) return JS_NULL;
 
@@ -2055,6 +2058,8 @@ static JSValue jsf_pid_drop_packet(JSContext *ctx, JSValueConst this_val, int ar
 	GF_JSPckCtx *pckctx;
 	GF_JSPidCtx *pctx = JS_GetOpaque(this_val, jsf_pid_class_id);
     if (!pctx) return JS_EXCEPTION;
+	if (!pctx->jsf->filter->in_process)
+		return js_throw_err_msg(ctx, GF_BAD_PARAM, "Filter %s attempt to drop packet outside process callback not allowed!\n", pctx->jsf->filter->name);
 
 	if (!pctx->pck_head) {
 		if (gf_filter_pid_get_packet_count(pctx->pid)) {
@@ -2270,6 +2275,9 @@ static JSValue jsf_pid_new_packet(JSContext *ctx, JSValueConst this_val, int arg
 	GF_JSPidCtx *pctx = JS_GetOpaque(this_val, jsf_pid_class_id);
 
     if (!pctx) return JS_EXCEPTION;
+	if (!pctx->jsf->filter->in_process)
+		return js_throw_err_msg(ctx, GF_BAD_PARAM, "Filter %s attempt to create a new packet outside process callback not allowed!\n", pctx->jsf->filter->name);
+
 
 	pckc = gf_list_pop_back(pctx->jsf->pck_res);
 	if (!pckc) {
@@ -3587,6 +3595,9 @@ static JSValue jsf_pck_send(JSContext *ctx, JSValueConst this_val, int argc, JSV
 	GF_FilterPacket *pck;
 	GF_JSPckCtx *pckctx = JS_GetOpaque(this_val, jsf_pck_class_id);
     if (!pckctx || !pckctx->pck) return JS_EXCEPTION;
+	if (! pckctx->jspid->jsf->filter->in_process)
+		return js_throw_err_msg(ctx, GF_BAD_PARAM, "Filter %s attempt to send packet outside process callback not allowed!\n", pckctx->jspid->jsf->filter->name);
+
     pck = pckctx->pck;
     if (!JS_IsUndefined(pckctx->data_ab)) {
     	JS_FreeValue(ctx, pckctx->data_ab);
@@ -4055,7 +4066,7 @@ void js_load_constants(JSContext *ctx, JSValue global_obj)
 
 }
 
-static GF_Err jsfilter_initialize(GF_Filter *filter)
+static GF_Err jsfilter_initialize_ex(GF_Filter *filter, JSContext *custom_ctx)
 {
 	u8 *buf;
 	u32 buf_len;
@@ -4066,32 +4077,42 @@ static GF_Err jsfilter_initialize(GF_Filter *filter)
     JSRuntime *rt;
 	GF_JSFilterCtx *jsf = gf_filter_get_udta(filter);
 
+	if (custom_ctx) {
+		GF_SAFEALLOC(jsf, GF_JSFilterCtx);
+		filter->filter_udta = jsf;
+	}
+
 	jsf->filter = filter;
 	jsf->pids = gf_list_new();
 	jsf->pck_res = gf_list_new();
-	jsf->log_name = gf_strdup("JSF");
+	jsf->log_name = gf_strdup(custom_ctx ? filter->name : "JSF");
 
-	if (!jsf->js) {
-		GF_LOG(GF_LOG_ERROR, GF_LOG_SCRIPT, ("[JSF] Missing script file\n"));
-		return GF_BAD_PARAM;
-	}
-	if (!gf_file_exists(jsf->js)) {
-		GF_LOG(GF_LOG_ERROR, GF_LOG_SCRIPT, ("[JSF] Script file %s does not exist\n", jsf->js));
-		return GF_BAD_PARAM;
-	}
-	jsf->filter_obj = JS_UNDEFINED;
+	if (custom_ctx) {
+		jsf->ctx = custom_ctx;
+		jsf->is_custom = GF_TRUE;
+		global_obj = JS_GetGlobalObject(jsf->ctx);
+	} else {
+		if (!jsf->js) {
+			GF_LOG(GF_LOG_ERROR, GF_LOG_SCRIPT, ("[JSF] Missing script file\n"));
+			return GF_BAD_PARAM;
+		}
+		if (!gf_file_exists(jsf->js)) {
+			GF_LOG(GF_LOG_ERROR, GF_LOG_SCRIPT, ("[JSF] Script file %s does not exist\n", jsf->js));
+			return GF_BAD_PARAM;
+		}
+		jsf->filter_obj = JS_UNDEFINED;
 
-	jsf->ctx = gf_js_create_context();
-	if (!jsf->ctx) {
-		GF_LOG(GF_LOG_ERROR, GF_LOG_SCRIPT, ("[JSF] Failed to load QuickJS context\n"));
-		return GF_IO_ERR;
+		jsf->ctx = gf_js_create_context();
+		if (!jsf->ctx) {
+			GF_LOG(GF_LOG_ERROR, GF_LOG_SCRIPT, ("[JSF] Failed to load QuickJS context\n"));
+			return GF_IO_ERR;
+		}
+		JS_SetContextOpaque(jsf->ctx, jsf);
+		global_obj = JS_GetGlobalObject(jsf->ctx);
+		js_load_constants(jsf->ctx, global_obj);
 	}
-	JS_SetContextOpaque(jsf->ctx, jsf);
 	rt = JS_GetRuntime(jsf->ctx);
 
-    global_obj = JS_GetGlobalObject(jsf->ctx);
-
-	js_load_constants(jsf->ctx, global_obj);
 
 
 	//initialize filter class and create a single filter object in global scope
@@ -4101,7 +4122,8 @@ static GF_Err jsfilter_initialize(GF_Filter *filter)
 	jsf->filter_obj = JS_NewObjectClass(jsf->ctx, jsf_filter_class_id);
     JS_SetPropertyFunctionList(jsf->ctx, jsf->filter_obj, jsf_filter_funcs, countof(jsf_filter_funcs));
     JS_SetOpaque(jsf->filter_obj, jsf);
-    JS_SetPropertyStr(jsf->ctx, global_obj, "filter", jsf->filter_obj);
+    if (!custom_ctx)
+		JS_SetPropertyStr(jsf->ctx, global_obj, "filter", jsf->filter_obj);
 
 	//initialize filter instance class
 	JS_NewClassID(&jsf_filter_inst_class_id);
@@ -4125,9 +4147,13 @@ static GF_Err jsfilter_initialize(GF_Filter *filter)
     JS_SetPropertyFunctionList(jsf->ctx, pck_proto, jsf_pck_funcs, countof(jsf_pck_funcs));
     JS_SetClassProto(jsf->ctx, jsf_pck_class_id, pck_proto);
 
-	JS_SetPropertyStr(jsf->ctx, global_obj, "_gpac_log_name", JS_NewString(jsf->ctx, gf_file_basename(jsf->js) ) );
+    if (!custom_ctx)
+		JS_SetPropertyStr(jsf->ctx, global_obj, "_gpac_log_name", JS_NewString(jsf->ctx, gf_file_basename(jsf->js) ) );
+
     JS_FreeValue(jsf->ctx, global_obj);
 
+    if (custom_ctx) return GF_OK;
+
 
 	//load script
 	GF_Err e = gf_file_load_data(jsf->js, &buf, &buf_len);
@@ -4176,6 +4202,12 @@ static GF_Err jsfilter_initialize(GF_Filter *filter)
 	return GF_OK;
 }
 
+
+static GF_Err jsfilter_initialize(GF_Filter *filter)
+{
+	return jsfilter_initialize_ex(filter, NULL);
+}
+
 static void jsfilter_finalize(GF_Filter *filter)
 {
 	u32 i, count;
@@ -4208,7 +4240,8 @@ static void jsfilter_finalize(GF_Filter *filter)
 	if (jsf->unload_session_api)
 		gf_fs_unload_script(filter->session, jsf->ctx);
 
-	gf_js_delete_context(jsf->ctx);
+	if (!jsf->is_custom)
+		gf_js_delete_context(jsf->ctx);
 
 	while (gf_list_count(jsf->pids)) {
 		GF_JSPidCtx *pctx = gf_list_pop_back(jsf->pids);
@@ -4243,6 +4276,8 @@ static void jsfilter_finalize(GF_Filter *filter)
 	if (jsf->caps) gf_free(jsf->caps);
 }
 
+
+
 static GF_Err jsfilter_update_arg(GF_Filter *filter, const char *arg_name, const GF_PropertyValue *new_val)
 {
 	GF_JSFilterCtx *jsf = gf_filter_get_udta(filter);
@@ -4426,6 +4461,24 @@ const GF_FilterRegister *jsfilter_register(GF_FilterSession *session)
 	return &JSFilterRegister;
 }
 
+
+JSValue jsfilter_initialize_custom(GF_Filter *filter, JSContext *ctx)
+{
+	GF_JSFilterCtx *jsf;
+	GF_Err e = jsfilter_initialize_ex(filter, ctx);
+	if (e) return js_throw_err(ctx, e);
+	jsf = gf_filter_get_udta(filter);
+	((GF_FilterRegister *) filter->freg)->finalize = jsfilter_finalize;
+	((GF_FilterRegister *) filter->freg)->process = jsfilter_process;
+	((GF_FilterRegister *) filter->freg)->configure_pid = jsfilter_configure_pid;
+	((GF_FilterRegister *) filter->freg)->process_event = jsfilter_process_event;
+//	((GF_FilterRegister *) filter->freg)->reconfigure_output = jsfilter_reconfigure_output;
+//	((GF_FilterRegister *) filter->freg)->probe_data = jsfilter_probe_data;
+	//signal reg is script, so we don't free the filter reg caps as with custom filters
+	((GF_FilterRegister *) filter->freg)->flags |= GF_FS_REG_SCRIPT;
+	return jsf->filter_obj;
+}
+
 #else
 
 const GF_FilterRegister *jsfilter_register(GF_FilterSession *session)
diff --git a/testsuite b/testsuite
index 3d94f8f139..99862af44f 160000
--- a/testsuite
+++ b/testsuite
@@ -1 +1 @@
-Subproject commit 3d94f8f139fa8ecfdb44fdf91c7d7650071c80da
+Subproject commit 99862af44fc8be9d508a4ce5f270efb43d1c68fb
