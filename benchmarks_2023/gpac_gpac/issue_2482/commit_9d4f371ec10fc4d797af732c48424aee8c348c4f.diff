diff --git a/include/gpac/internal/isomedia_dev.h b/include/gpac/internal/isomedia_dev.h
index d3322ed251..c6cea7f895 100644
--- a/include/gpac/internal/isomedia_dev.h
+++ b/include/gpac/internal/isomedia_dev.h
@@ -1157,6 +1157,8 @@ typedef struct
 	u32 r_FirstSampleInEntry;
 	u32 r_currentEntryIndex;
 	u64 r_CurrentDTS;
+	//when removing samples, this is the DTS of first sample after all removed samples
+	u64 cumulated_start_dts;
 
 	//stats for read
 	u32 max_ts_delta;
@@ -2638,6 +2640,7 @@ typedef struct
 	u8 *moof_data;
 	u32 moof_data_len, trun_ref_size;
 
+	GF_List *trun_list;
 } GF_MovieFragmentBox;
 
 
diff --git a/include/gpac/isomedia.h b/include/gpac/isomedia.h
index 8b14a9b1ad..d000149a2f 100644
--- a/include/gpac/isomedia.h
+++ b/include/gpac/isomedia.h
@@ -4257,7 +4257,7 @@ GF_Err gf_isom_reset_tables(GF_ISOFile *isom_file, Bool reset_sample_count);
 
 /*! sets the offset for parsing from the input buffer to 0 (used to reclaim input buffer)
 \param isom_file the target ISO file
-\param top_box_start set to the byte offset in the source buffer of the first top level box
+\param top_box_start set to the byte offset in the source buffer of the first top level box, may be NULL
 \return error if any
 */
 GF_Err gf_isom_reset_data_offset(GF_ISOFile *isom_file, u64 *top_box_start);
diff --git a/include/gpac/mpd.h b/include/gpac/mpd.h
index e1f94e098c..14571cc943 100644
--- a/include/gpac/mpd.h
+++ b/include/gpac/mpd.h
@@ -82,7 +82,7 @@ typedef enum
 \param bandwidth bandwidth used for the representation
 \param segment_number number of the target segment
 \param use_segment_timeline indicates if segmentTimeline is used for segment addressing in the MPD
-\param forced_template if true, do not append extension or missing $Number$ or $Time$  when resolving template
+\param forced if true, do not append extension or missing $Number$ or $Time$  when resolving template
 \return error if any
 */
 GF_Err gf_media_mpd_format_segment_name(GF_DashTemplateSegmentType seg_type, Bool is_bs_switching, char *segment_name, const char *rep_id, const char *base_url, const char *seg_rad_name, const char *seg_ext, u64 start_time, u32 bandwidth, u32 segment_number, Bool use_segment_timeline, Bool forced);
diff --git a/share/doc/man/gpac-filters.1 b/share/doc/man/gpac-filters.1
index 2e0832e12c..c8646a291f 100644
--- a/share/doc/man/gpac-filters.1
+++ b/share/doc/man/gpac-filters.1
@@ -5937,15 +5937,17 @@ The pipeline flush is signaled as EOS while keeping the stream active.
 .br
 This is typically needed for mux filters waiting for EOS to flush their data.
 .br
-Warning: Usage of  .I sigflush may not be properly supported by some filters.
+  
 .br
 If .I marker is set, the following strings (all 8-bytes 0 terminator) will be scanned:
 .br
-* `GPACPIF`: triggers a pipeline flush event after the marker
+* `GPACPIF`: triggers a pipeline flush event
+.br
+* `GPACPIR`: triggers a reconfiguration of the format (used to signal mux type changes)
 .br
-* `GPACPIR`: triggers a reconfigration of the format after the marker
+The .I marker mode should be used carefully as it will slow down pipe processing (higher CPU usage and delayed output).
 .br
-The marker mode should be used carefully as it will slow down pipe processing (higher CPU usage and delayed output).
+Warning: Usage of pipeline flushing may not be properly supported by some filters.
 .br
   
 .br
@@ -5972,6 +5974,10 @@ mkp (bool, default: false):    create pipe if not found
 .br
 sigflush (bool, default: false): signal end of stream upon pipe close - cf filter help
 .br
+marker (bool, default: false): inspect payload for flush and reconfigure signals - cf filter help
+.br
+bpcnt (uint, default: 0):      number of broken pipe allowed before exiting, 0 means forever
+.br
 
 .br
 .SH pout
diff --git a/src/filter_core/filter_pid.c b/src/filter_core/filter_pid.c
index ee78fd4057..ba73c59c3f 100644
--- a/src/filter_core/filter_pid.c
+++ b/src/filter_core/filter_pid.c
@@ -6645,7 +6645,12 @@ void gf_filter_pid_set_eos(GF_FilterPid *pid)
 		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to signal EOS on input PID %s in filter %s\n", pid->pid->name, pid->filter->name));
 		return;
 	}
-	if (pid->has_seen_eos) return;
+	//don't resend EOS if not keepalive - in keepalive we need to reevaluate and potentially trigger eos from filters
+	if (pid->has_seen_eos && !pid->eos_keepalive) {
+		return;
+	}
+	//reset eos keepalive at each first eos signal. If a source pid is in keepalive, we propagate below
+	pid->eos_keepalive = GF_FALSE;
 
 	GF_LOG(GF_LOG_INFO, GF_LOG_FILTER, ("EOS signaled on PID %s in filter %s\n", pid->name, pid->filter->name));
 	//we create a fake packet for eos signaling
@@ -9140,6 +9145,7 @@ void gf_filter_pid_send_flush(GF_FilterPid *pid)
 		GF_LOG(GF_LOG_ERROR, GF_LOG_FILTER, ("Attempt to signal flush on input PID %s in filter %s\n", pid->pid->name, pid->filter->name));
 		return;
 	}
-	pid->eos_keepalive = GF_TRUE;
 	gf_filter_pid_set_eos(pid);
+	//set keepalive once eos has been called
+	pid->eos_keepalive = GF_TRUE;
 }
diff --git a/src/filters/in_pipe.c b/src/filters/in_pipe.c
index 49e3431e0a..517c9bc1bb 100644
--- a/src/filters/in_pipe.c
+++ b/src/filters/in_pipe.c
@@ -57,8 +57,8 @@ typedef struct
 	char *src;
 	char *ext;
 	char *mime;
-	u32 block_size;
-	Bool blk, ka, mkp, sigflush;
+	u32 block_size, bpcnt;
+	Bool blk, ka, mkp, sigflush, marker;
 
 	u32 read_block_size;
 	//only one output pid declared
@@ -322,6 +322,9 @@ static Bool pipein_process_event(GF_Filter *filter, const GF_FilterEvent *evt)
 static void pipein_pck_destructor(GF_Filter *filter, GF_FilterPid *pid, GF_FilterPacket *pck)
 {
 	GF_PipeInCtx *ctx = (GF_PipeInCtx *) gf_filter_get_udta(filter);
+	u32 size;
+	gf_filter_pck_get_data(pck, &size);
+	ctx->buffer[size] = ctx->store_char;
 	ctx->pck_out = GF_FALSE;
 	//ready to process again
 	gf_filter_post_process_task(filter);
@@ -354,7 +357,6 @@ static GF_Err pipein_process(GF_Filter *filter)
 		total_read = ctx->left_over;
 		ctx->left_over = 0;
 		ctx->copy_offset = 0;
-		ctx->buffer[0] = PIPE_FLUSH_MARKER[0];
 	}
 	if (ctx->has_recfg) {
 		ctx->do_reconfigure = GF_TRUE;
@@ -443,6 +445,13 @@ static GF_Err pipein_process(GF_Filter *filter)
 				}
 				else if (error == ERROR_BROKEN_PIPE) {
 					if (ctx->ka) {
+						if (ctx->bpcnt) {
+							ctx->bpcnt--;
+							if (!ctx->bpcnt) {
+								gf_filter_pid_set_eos(ctx->pid);
+								return GF_EOS;
+							}
+						}
 						GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[PipeIn] Pipe closed by remote side, reopening!\n"));
 						CloseHandle(ctx->pipe);
 						ctx->pipe = INVALID_HANDLE_VALUE;
@@ -484,6 +493,16 @@ static GF_Err pipein_process(GF_Filter *filter)
 				} else {
 					//set keepalive eos
 					if (ctx->ka && ctx->bytes_read && ctx->sigflush && ctx->pid) {
+						if (ctx->bpcnt) {
+							ctx->bpcnt--;
+							if (!ctx->bpcnt) {
+								GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[PipeIn] exiting keep-alive mode\n"));
+								ctx->bytes_read = 0;
+								ctx->ka = GF_FALSE;
+								gf_filter_pid_set_eos(ctx->pid);
+								return GF_EOS;
+							}
+						}
 						gf_filter_pid_send_flush(ctx->pid);
 						ctx->bytes_read = 0;
 					}
@@ -508,14 +527,13 @@ static GF_Err pipein_process(GF_Filter *filter)
 	ctx->nb_empty = 0;
 
 	Bool has_marker=GF_FALSE;
-	if (ctx->sigflush) {
+	if (ctx->marker) {
 		u8 *start = ctx->buffer;
 		u32 avail = nb_read;
 		if (nb_read<8) {
 			ctx->left_over = nb_read;
 			nb_read = 0;
 			ctx->copy_offset = 0;
-			ctx->store_char = ctx->buffer[0];
 		}
 		while (nb_read) {
 			u8 *m = memchr(start, PIPE_FLUSH_MARKER[0], avail);
@@ -525,7 +543,6 @@ static GF_Err pipein_process(GF_Filter *filter)
 				ctx->left_over = remain;
 				nb_read -= remain;
 				ctx->copy_offset = m - (u8*)ctx->buffer;
-				ctx->store_char = ctx->buffer[nb_read];
 				break;
 			}
 			if (!memcmp(m, PIPE_FLUSH_MARKER, 8)) {
@@ -533,6 +550,7 @@ static GF_Err pipein_process(GF_Filter *filter)
 				nb_read = m - (u8*)ctx->buffer;
 				ctx->copy_offset = nb_read+8;
 				has_marker = GF_TRUE;
+				GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[PipeIn] Found flush marker\n"));
 				break;
 			}
 			if (!memcmp(m, PIPE_RECFG_MARKER, 8)) {
@@ -540,6 +558,7 @@ static GF_Err pipein_process(GF_Filter *filter)
 				nb_read = m - (u8*)ctx->buffer;
 				ctx->copy_offset = nb_read+8;
 				ctx->has_recfg = GF_TRUE;
+				GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[PipeIn] Found reconfig marker\n"));
 				break;
 			}
 			start = m+1;
@@ -551,11 +570,14 @@ static GF_Err pipein_process(GF_Filter *filter)
 		if (has_marker) {
 			gf_filter_pid_send_flush(ctx->pid);
 		}
-		if (!ctx->bytes_read) gf_filter_ask_rt_reschedule(filter, 10000);
-		else gf_filter_ask_rt_reschedule(filter, 1000);
+		if (!ctx->bytes_read)
+			gf_filter_ask_rt_reschedule(filter, 10000);
+		else if (!total_read)
+			gf_filter_ask_rt_reschedule(filter, 1000);
 		return GF_OK;
 	}
 
+	ctx->store_char = ctx->buffer[nb_read];
 	ctx->buffer[nb_read] = 0;
 	if (!ctx->pid || ctx->do_reconfigure) {
 		GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[PipeIn] configuring stream %d probe bytes\n", nb_read));
@@ -571,7 +593,7 @@ static GF_Err pipein_process(GF_Filter *filter)
 	pck = gf_filter_pck_new_shared(ctx->pid, ctx->buffer, nb_read, pipein_pck_destructor);
 	if (!pck) return GF_OUT_OF_MEM;
 
-	GF_LOG(GF_LOG_DEBUG, GF_LOG_MMIO, ("[PipeIn] sending %d bytes\n", nb_read));
+	GF_LOG(GF_LOG_DEBUG, GF_LOG_MMIO, ("[PipeIn] Got %d bytes\n", nb_read));
 	gf_filter_pck_set_framing(pck, ctx->is_first, ctx->is_end);
 	gf_filter_pck_set_sap(pck, GF_FILTER_SAP_1);
 
@@ -604,6 +626,9 @@ static const GF_FilterArgs PipeInArgs[] =
 	{ OFFS(ka), "keep-alive pipe when end of input is detected", GF_PROP_BOOL, "false", NULL, GF_FS_ARG_HINT_ADVANCED},
 	{ OFFS(mkp), "create pipe if not found", GF_PROP_BOOL, "false", NULL, 0},
 	{ OFFS(sigflush), "signal end of stream upon pipe close - cf filter help", GF_PROP_BOOL, "false", NULL, GF_FS_ARG_HINT_ADVANCED},
+	{ OFFS(marker), "inspect payload for flush and reconfigure signals - cf filter help", GF_PROP_BOOL, "false", NULL, GF_FS_ARG_HINT_ADVANCED},
+	{ OFFS(bpcnt), "number of broken pipe allowed before exiting, 0 means forever", GF_PROP_UINT, "0", NULL, GF_FS_ARG_HINT_EXPERT},
+
 	{0}
 };
 
@@ -648,11 +673,12 @@ GF_FilterRegister PipeInRegister = {
 		"  \n"
 		"The pipeline flush is signaled as EOS while keeping the stream active.\n"
 		"This is typically needed for mux filters waiting for EOS to flush their data.\n"
-		"Warning: Usage of  [-sigflush]() may not be properly supported by some filters.\n"
+		"  \n"
 		"If [-marker]() is set, the following strings (all 8-bytes `0` terminator) will be scanned:\n"
-		"- `GPACPIF`: triggers a pipeline flush event after the marker\n"
-		"- `GPACPIR`: triggers a reconfiguration of the format after the marker (used to signal mux type changes)\n"
-		"The marker mode should be used carefully as it will slow down pipe processing (higher CPU usage and delayed output).\n"
+		"- `GPACPIF`: triggers a pipeline flush event\n"
+		"- `GPACPIR`: triggers a reconfiguration of the format (used to signal mux type changes)\n"
+		"The [-marker]() mode should be used carefully as it will slow down pipe processing (higher CPU usage and delayed output).\n"
+		"Warning: Usage of pipeline flushing may not be properly supported by some filters.\n"
 		"  \n"
 		"The pipe input can be created in blocking mode or non-blocking mode.\n"
 	"")
diff --git a/src/filters/mux_isom.c b/src/filters/mux_isom.c
index 3cbb62cf99..3597d029b8 100644
--- a/src/filters/mux_isom.c
+++ b/src/filters/mux_isom.c
@@ -5634,7 +5634,7 @@ static GF_Err mp4_mux_initialize_movie(GF_MP4MuxCtx *ctx)
 
 		pck = gf_filter_pid_get_packet(tkw->ipid);
 		if (!pck) {
-			if (gf_filter_pid_is_eos(tkw->ipid)) {
+			if (gf_filter_pid_is_eos(tkw->ipid) && !gf_filter_pid_is_flush_eos(tkw->ipid)) {
 				if (tkw->dgl_copy) {
 					gf_filter_pck_discard(tkw->dgl_copy);
 					tkw->dgl_copy = NULL;
@@ -6241,7 +6241,7 @@ static GF_Err mp4_mux_process_fragmented(GF_Filter *filter, GF_MP4MuxCtx *ctx)
 		nb_eos=0;
 		for (i=0; i<count; i++) {
 			TrackWriter *tkw = gf_list_get(ctx->tracks, i);
-			if (gf_filter_pid_is_eos(tkw->ipid)) {
+			if (gf_filter_pid_is_eos(tkw->ipid) && !gf_filter_pid_is_flush_eos(tkw->ipid)) {
 				nb_eos ++;
 			}
 		}
@@ -6928,7 +6928,7 @@ static void mp4_mux_config_timing(GF_MP4MuxCtx *ctx)
 		}
 
 		if (!pck) {
-			if (gf_filter_pid_is_eos(tkw->ipid)) {
+			if (gf_filter_pid_is_eos(tkw->ipid) && !gf_filter_pid_is_flush_eos(tkw->ipid)) {
 				if (tkw->cenc_state==CENC_NEED_SETUP)
 					mp4_mux_cenc_update(ctx, tkw, NULL, CENC_CONFIG, 0, 0);
 
@@ -7184,7 +7184,7 @@ GF_Err mp4_mux_process(GF_Filter *filter)
 		}
 
 		if (!pck) {
-			if (gf_filter_pid_is_eos(tkw->ipid)) {
+			if (gf_filter_pid_is_eos(tkw->ipid) && !gf_filter_pid_is_flush_eos(tkw->ipid)) {
 				tkw->suspended = GF_FALSE;
 				nb_eos++;
 			}
diff --git a/src/filters/out_audio.c b/src/filters/out_audio.c
index c11c0bd7bc..83b1b8bab6 100644
--- a/src/filters/out_audio.c
+++ b/src/filters/out_audio.c
@@ -299,7 +299,8 @@ static u32 aout_fill_output(void *ptr, u8 *buffer, u32 buffer_size)
 		GF_FilterPacket *pck = gf_filter_pid_get_packet(ctx->pid);
 		if (!pck) {
 			if (gf_filter_pid_is_eos(ctx->pid)) {
-				ctx->is_eos = GF_TRUE;
+				if (!gf_filter_pid_is_flush_eos(ctx->pid))
+					ctx->is_eos = GF_TRUE;
 			} else if (!is_first_pck) {
 				GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[AudioOut] buffer underflow\n"));
 			}
diff --git a/src/filters/out_pipe.c b/src/filters/out_pipe.c
index a1c07cc7a4..3e75ca9c45 100644
--- a/src/filters/out_pipe.c
+++ b/src/filters/out_pipe.c
@@ -314,7 +314,7 @@ static void pipeout_finalize(GF_Filter *filter)
 #define PIPE_FLUSH_MARKER	"GPACPIF"
 static void pout_write_marker(GF_PipeOutCtx *ctx)
 {
-	if (ctx->marker && gf_filter_pid_is_flush_eos(ctx->pid)) {
+	if (ctx->marker) {
 		u32 nb_write;
 #ifdef WIN32
 		if (! WriteFile(ctx->pipe, PIPE_FLUSH_MARKER, 8, (LPDWORD) &nb_write, NULL)) {
@@ -328,7 +328,7 @@ static void pout_write_marker(GF_PipeOutCtx *ctx)
 			return;
 		}
 #endif
-		GF_LOG(GF_LOG_DEBUG, GF_LOG_MMIO, ("[PipeOut] Wrote marker\n"));
+		GF_LOG(GF_LOG_INFO, GF_LOG_MMIO, ("[PipeOut] Wrote flush marker\n"));
 	}
 }
 
@@ -396,7 +396,6 @@ static GF_Err pipeout_process(GF_Filter *filter)
 			}
 		}
 	}
-	pout_write_marker(ctx);
 
 	pck_data = gf_filter_pck_get_data(pck, &pck_size);
 	if (
diff --git a/src/filters/out_video.c b/src/filters/out_video.c
index 0ad77f98a9..33f0166a74 100644
--- a/src/filters/out_video.c
+++ b/src/filters/out_video.c
@@ -1730,7 +1730,7 @@ static GF_Err vout_process(GF_Filter *filter)
 
 	pck = gf_filter_pid_get_packet(ctx->pid);
 	if (!pck) {
-		if (gf_filter_pid_is_eos(ctx->pid)) {
+		if (gf_filter_pid_is_eos(ctx->pid) && !gf_filter_pid_is_flush_eos(ctx->pid)) {
 			if (!ctx->aborted) {
 				GF_FilterEvent evt;
 				GF_FEVT_INIT(evt, GF_FEVT_STOP, ctx->pid);
diff --git a/src/isomedia/box_code_base.c b/src/isomedia/box_code_base.c
index d44e5b9382..914da5ac3e 100644
--- a/src/isomedia/box_code_base.c
+++ b/src/isomedia/box_code_base.c
@@ -3788,6 +3788,7 @@ void moof_box_del(GF_Box *s)
 		}
 		gf_list_del(ptr->emsgs);
 	}
+	gf_list_del(ptr->trun_list);
 	gf_free(ptr);
 }
 
diff --git a/src/isomedia/isom_read.c b/src/isomedia/isom_read.c
index 01c65961bd..8b0f4bd463 100644
--- a/src/isomedia/isom_read.c
+++ b/src/isomedia/isom_read.c
@@ -3142,6 +3142,7 @@ GF_Err gf_isom_reset_data_offset(GF_ISOFile *movie, u64 *top_box_start)
 	for (i=0; i<count; i++) {
 		GF_TrackBox *tk = gf_list_get(movie->moov->trackList, i);
 		tk->first_traf_merged = GF_FALSE;
+		tk->Media->information->sampleTable->TimeToSample->cumulated_start_dts = 0;
 	}
 #endif
 	return GF_OK;
diff --git a/src/isomedia/movie_fragments.c b/src/isomedia/movie_fragments.c
index 0514f32031..6b7d382368 100644
--- a/src/isomedia/movie_fragments.c
+++ b/src/isomedia/movie_fragments.c
@@ -929,25 +929,21 @@ GF_Err gf_isom_write_compressed_box(GF_ISOFile *mov, GF_Box *root_box, u32 repl_
 void flush_ref_samples(GF_ISOFile *movie, u64 *out_seg_size, Bool use_seg_marker)
 {
 	u32 i=0;
-	u32 traf_count = movie->in_sidx_write ? 0 : gf_list_count(movie->moof->TrackList);
-	for (i=0; i<traf_count; i++) {
-		GF_TrackFragmentBox *traf = gf_list_get(movie->moof->TrackList, i);
-		u32 j, run_count = gf_list_count(traf->TrackRuns);
-		if (!run_count) continue;
-		for (j=0; j<run_count; j++) {
-			GF_TrackFragmentRunBox *trun = (GF_TrackFragmentRunBox *)gf_list_get(traf->TrackRuns, j);
-			u32 s_count = gf_list_count(trun->sample_refs);
-			while (s_count) {
-				if (!use_seg_marker && movie->on_last_block_start && (i+1==traf_count) && (j+1==run_count) && (s_count==1)) {
-					movie->on_last_block_start(movie->on_block_out_usr_data);
-				}
-				GF_TrafSampleRef *sref = gf_list_pop_front(trun->sample_refs);
-				movie->on_block_out(movie->on_block_out_usr_data, sref->data, sref->len, sref->ref, sref->ref_offset);
-				if (out_seg_size) *out_seg_size += sref->len;
-				if (!sref->ref) gf_free(sref->data);
-				gf_free(sref);
-				s_count--;
+	if (movie->in_sidx_write) return;
+	u32 trun_count = gf_list_count(movie->moof->trun_list);
+	for (i=0; i<trun_count; i++) {
+		GF_TrackFragmentRunBox *trun = gf_list_get(movie->moof->trun_list, i);
+		u32 s_count = gf_list_count(trun->sample_refs);
+		while (s_count) {
+			if (!use_seg_marker && movie->on_last_block_start && (i+1==trun_count) && (s_count==1)) {
+				movie->on_last_block_start(movie->on_block_out_usr_data);
 			}
+			GF_TrafSampleRef *sref = gf_list_pop_front(trun->sample_refs);
+			movie->on_block_out(movie->on_block_out_usr_data, sref->data, sref->len, sref->ref, sref->ref_offset);
+			if (out_seg_size) *out_seg_size += sref->len;
+			if (!sref->ref) gf_free(sref->data);
+			gf_free(sref);
+			s_count--;
 		}
 	}
 }
@@ -2843,6 +2839,10 @@ GF_Err gf_isom_fragment_add_sample_ex(GF_ISOFile *movie, GF_ISOTrackID TrackID,
 		//if we use data caching, create a bitstream
 		if (traf->DataCache)
 			trun->cache = gf_bs_new(NULL, 0, GF_BITSTREAM_WRITE);
+
+		//remember the order in which we created truns for reference flushing (unused otherwise)
+		if (!movie->moof->trun_list) movie->moof->trun_list = gf_list_new();
+		gf_list_add(movie->moof->trun_list, trun);
 	}
 
 	memset(&ent, 0, sizeof(GF_TrunEntry));
diff --git a/src/isomedia/stbl_read.c b/src/isomedia/stbl_read.c
index 21e737e824..67d4ff6666 100644
--- a/src/isomedia/stbl_read.c
+++ b/src/isomedia/stbl_read.c
@@ -227,7 +227,7 @@ GF_Err stbl_GetSampleDTS_and_Duration(GF_TimeToSampleBox *stts, u32 SampleNumber
 	return GF_OK;
 
 found:
-	(*DTS) = stts->r_CurrentDTS + j * (u64) ent->sampleDelta;
+	(*DTS) = stts->r_CurrentDTS + j * (u64) ent->sampleDelta + stts->cumulated_start_dts;
 	if (duration) *duration = ent->sampleDelta;
 	return GF_OK;
 }
diff --git a/src/isomedia/stbl_write.c b/src/isomedia/stbl_write.c
index 73647c8835..002ad08582 100644
--- a/src/isomedia/stbl_write.c
+++ b/src/isomedia/stbl_write.c
@@ -1108,6 +1108,8 @@ GF_Err stbl_RemoveDTS(GF_SampleTableBox *stbl, u32 sampleNumber, u32 nb_samples,
 		stts->nb_entries = 0;
 		stts->r_FirstSampleInEntry = stts->r_currentEntryIndex = 0;
 		stts->r_CurrentDTS = 0;
+		if (nb_samples>1)
+			stts->cumulated_start_dts += stts->entries[0].sampleDelta;
 		return GF_OK;
 	}
 	//we're removing the last sample
@@ -1115,6 +1117,8 @@ GF_Err stbl_RemoveDTS(GF_SampleTableBox *stbl, u32 sampleNumber, u32 nb_samples,
 		ent = &stts->entries[stts->nb_entries-1];
 		ent->sampleCount--;
 		if (!ent->sampleCount) stts->nb_entries--;
+		if (nb_samples>1)
+			stts->cumulated_start_dts += ent->sampleDelta;
 	} else {
 		u64 *DTSs, curDTS;
 		u32 i, j, k, sampNum;
@@ -1138,8 +1142,12 @@ GF_Err stbl_RemoveDTS(GF_SampleTableBox *stbl, u32 sampleNumber, u32 nb_samples,
 					} else {
 						DTSs[sampNum-k] = curDTS;
 					}
-				} else if (sampNum >= nb_samples) {
-					DTSs[sampNum - nb_samples] = curDTS;
+				} else {
+					if (sampNum >= nb_samples) {
+						DTSs[sampNum - nb_samples] = curDTS;
+					} else if (sampNum + 1 == nb_samples) {
+						stts->cumulated_start_dts += curDTS+ent->sampleDelta;
+					}
 				}
 				curDTS += ent->sampleDelta;
 				sampNum ++;
@@ -1163,7 +1171,12 @@ GF_Err stbl_RemoveDTS(GF_SampleTableBox *stbl, u32 sampleNumber, u32 nb_samples,
 			if (stbl->SampleSize->sampleCount == 2) {
 				stts->entries[0].sampleDelta = LastAUDefDuration;
 			} else {
-				stts->entries[0].sampleDelta = (u32) DTSs[1] /*- DTSs[0]*/;
+				if (tot_samples>1) {
+					stts->entries[0].sampleDelta = (u32) (DTSs[1] - DTSs[0]);
+				} else {
+					//special case if we remove all but one sample, compute delta based on last DTS
+					stts->entries[0].sampleDelta = (u32) (curDTS - DTSs[0]);
+				}
 			}
 		} else {
 			sampNum = 0;
diff --git a/testsuite b/testsuite
index eba7046749..2ee35db8cc 160000
--- a/testsuite
+++ b/testsuite
@@ -1 +1 @@
-Subproject commit eba70467496e31482a03738c60d2060822945780
+Subproject commit 2ee35db8cc24c6b9ccf852890b8d7633a4fa8b4e
