swf_init_decompress: check dst_size min size (fixes #2618)
ac3dmx: add remain size check (fixes #2627)
fix 5692dc7: make test more strict (#2627)
