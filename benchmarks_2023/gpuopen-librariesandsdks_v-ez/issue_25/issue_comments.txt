Disabling vsync does not work on surfaces who doesn't support immediate mode
What platform and hardware are you using?
Windows 10 with the NVIDIA Geforce GTX 1070
I'll try to reproduce on my end as this seems like it could be a bug in NVIDIA's driver when using FIFO mode.
Thanks soconne,
i have another question. Is it possible to use the validation layers effectively when using vez? I have them enabled, but because vez is built as an dll i guess they are not working properly.
If you enable the validation layers the same way they’re enabled in the examples it should work.  I’ve had problems with them when installing a new Vulkan SDK over an existing version.  Uninstalling all Vulkan SDKs and then reinstalling only the latest has fixed the issue for me in the past.
Nope they doesn't work for me, even though i reinstalled 3x with different versions.
I had VezFramebufferCreateInfo.layers set to 0, which i spent several hours of debugging trying to figure out what's wrong... xD
VK_PRESENT_MODE_FIFO_RELAXED_KHR  does not disable vsync. As far as i understand it, it stills waits for every vsync cycle to update the screen, but with the exception if there's not an image in the queue, meaning the application might be too slow, a new request will be displayed immediately, resulting in visible tearing.
I need to measure the speed of my application, but it's not possible with FIFO_RELAXED.
Unfortunately my gpu doesn't support immediate mode anymore (it did a while ago), so i have to use VK_PRESENT_MODE_MAILBOX_KHR. Please give me an option to use this mode (preferable with tripple buffering). In my opinion the VK_PRESENT_MODE_MAILBOX_KHR is the best mode (with tripple buffering) anyway on desktop.  
Rather than explicitly exposing a minimum image count in the API, I have added the tripleBuffer field to VezSwapchainCreateInfo.  I have also fixed the issue where MAILBOX was not being used for disabled vsync.  Let me know if this fixes your issue.
Thanks and yea i think it would if you would have been commited the header file (Swapchain.h) aswell xD I checked the commit, you just changed Swapchain.cpp ;-)

// Offtopic
How long do you think it will take to add suport for individual ubo/ssbo members? I'm at a point where i really need it, otherwise i can't continue. I`ll do it myself if you have no time, but of course i prefer an official solution. :-)
VEZ.h was missing from the commit.  It's now in.