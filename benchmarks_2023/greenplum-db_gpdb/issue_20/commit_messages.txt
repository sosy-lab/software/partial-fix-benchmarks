Don't link strlcpy.o into mock test programs.

Now that we no longer forcibly compile strlcpy.c, and only compile and link
it into strlcpy.o into libpgport when needed, this is no longer needed.
Furthermore, it caused the linking to fail on platforms that don't need
strlcpy.o, like OS X.
Have "configure" fail if GPOPT/GPOS header files are not present (#20).
Revert "Have "configure" fail if GPOPT/GPOS header files are not present (#20)."

This reverts commit c742462eae8e03e5e23381c3cf2b9713f0bb04a1.
Have "configure" fail if GPOPT/GPOS header files are not present (#20).
Update config to include gpopt and gpos header files.
Have "configure" fail if GPOPT/GPOS header files are not present (#20).
Revert "Have "configure" fail if GPOPT/GPOS header files are not present (#20)."

This reverts commit c742462eae8e03e5e23381c3cf2b9713f0bb04a1.
Have "configure" fail if GPOPT/GPOS header files are not present (#20).
Update config to include gpopt and gpos header files.
Use col position from table descriptor for index key retrieval

Index keys are relative to the relation and the list of columns in
`pdrgpcr` is relative to the table descriptor and does not include any
dropped columns. Consider the case where we had 20 columns in a table. We
create an index that covers col # 20 as one of its keys. Then we drop
columns 10 through 15. Now the index key still points to col #20 but the
column ref list in `pdrgpcr` will only have 15 elements in it and cause
ORCA to crash with an `Out of Bounds` exception when
`CLogical::PosFromIndex()` gets called.

This commit fixes that issue by using the index key as the position to
retrieve the column from the relation. Use the column's `attno` to get
the column's current position relative to the table descriptor `ulPos`.
Then uses this `ulPos` to retrieve the `CColRef` of the index key
column as shown below:

```
CColRef *pcr = (*pdrgpcr)[ulPos];
```

We also added the 2 test cases below to test for the above condition:

1. `DynamicIndexGetDroppedCols`
2. `LogicalIndexGetDroppedCols`

Both of the above test cases use a table with 30 columns; create a btree
index on 6 columns and then drop 7 columns so the table only has 23
columns.

This commit also bumps ORCA to version 2.43.1
Remove redundant index on pg_stat_last_operation.

It had two indexes:

* pg_statlastop_classid_objid_index on (classid oid_ops, objid oid_ops), and
* pg_statlastop_classid_objid_staactionname_index on (classid oid_ops, objid
  oid_ops, staactionname name_ops)

The first one is completely redundant with the second one. Remove it.

Fixes assertion failure https://github.com/greenplum-db/gpdb/issues/6362.
The assertion was added in PostgreSQL 9.1, commit d2f60a3ab0. The failure
happened on "VACUUM FULL pg_stat_last_operation", if the VACUUM FULL
itself added a new row to the table. The insertion also inserted entries
in the indexes, which tripped the assertion that checks that you don't try
to insert entries into an index that's currently being reindexed, or
pending reindexing:

> (gdb) bt
> #0  0x00007f02f5189783 in __select_nocancel () from /lib64/libc.so.6
> #1  0x0000000000be76ef in pg_usleep (microsec=30000000) at pgsleep.c:53
> #2  0x0000000000ad75aa in elog_debug_linger (edata=0x11bf760 <errordata>) at elog.c:5293
> #3  0x0000000000acdba4 in errfinish (dummy=0) at elog.c:675
> #4  0x0000000000acc3bf in ExceptionalCondition (conditionName=0xc15798 "!(!ReindexIsProcessingIndex(((indexRelation)->rd_id)))", errorType=0xc156ef "FailedAssertion",
>     fileName=0xc156d0 "indexam.c", lineNumber=215) at assert.c:46
> #5  0x00000000004fded5 in index_insert (indexRelation=0x7f02f6b6daa0, values=0x7ffdb43915e0, isnull=0x7ffdb43915c0 "", heap_t_ctid=0x240bd64, heapRelation=0x24efa78,
>     checkUnique=UNIQUE_CHECK_YES) at indexam.c:215
> #6  0x00000000005bda59 in CatalogIndexInsert (indstate=0x240e5d0, heapTuple=0x240bd60) at indexing.c:136
> #7  0x00000000005bdaaa in CatalogUpdateIndexes (heapRel=0x24efa78, heapTuple=0x240bd60) at indexing.c:162
> #8  0x00000000005b2203 in MetaTrackAddUpdInternal (classid=1259, objoid=6053, relowner=10, actionname=0xc51543 "VACUUM", subtype=0xc5153b "REINDEX", rel=0x24efa78,
>     old_tuple=0x0) at heap.c:744
> #9  0x00000000005b229d in MetaTrackAddObject (classid=1259, objoid=6053, relowner=10, actionname=0xc51543 "VACUUM", subtype=0xc5153b "REINDEX") at heap.c:773
> #10 0x00000000005b2553 in MetaTrackUpdObject (classid=1259, objoid=6053, relowner=10, actionname=0xc51543 "VACUUM", subtype=0xc5153b "REINDEX") at heap.c:856
> #11 0x00000000005bd271 in reindex_index (indexId=6053, skip_constraint_checks=1 '\001') at index.c:3741
> #12 0x00000000005bd418 in reindex_relation (relid=6052, flags=2) at index.c:3870
> #13 0x000000000067ba71 in finish_heap_swap (OIDOldHeap=6052, OIDNewHeap=16687, is_system_catalog=1 '\001', swap_toast_by_content=0 '\000', swap_stats=1 '\001',
>     check_constraints=0 '\000', is_internal=1 '\001', frozenXid=821, cutoffMulti=1) at cluster.c:1667
> #14 0x0000000000679ed5 in rebuild_relation (OldHeap=0x7f02f6b7a6f0, indexOid=0, verbose=0 '\000') at cluster.c:648
> #15 0x0000000000679913 in cluster_rel (tableOid=6052, indexOid=0, recheck=0 '\000', verbose=0 '\000', printError=1 '\001') at cluster.c:461
> #16 0x0000000000717580 in vacuum_rel (onerel=0x0, relid=6052, vacstmt=0x2533c38, lmode=8, for_wraparound=0 '\000') at vacuum.c:2315
> #17 0x0000000000714ce7 in vacuumStatement_Relation (vacstmt=0x2533c38, relid=6052, relations=0x24c12f8, bstrategy=0x24c1220, do_toast=1 '\001', for_wraparound=0 '\000',
>     isTopLevel=1 '\001') at vacuum.c:787
> #18 0x0000000000714303 in vacuum (vacstmt=0x2403260, relid=0, do_toast=1 '\001', bstrategy=0x24c1220, for_wraparound=0 '\000', isTopLevel=1 '\001') at vacuum.c:337
> #19 0x0000000000969cd2 in standard_ProcessUtility (parsetree=0x2403260, queryString=0x24027e0 "vacuum full;", context=PROCESS_UTILITY_TOPLEVEL, params=0x0, dest=0x2403648,
>     completionTag=0x7ffdb4392550 "") at utility.c:804
> #20 0x00000000009691be in ProcessUtility (parsetree=0x2403260, queryString=0x24027e0 "vacuum full;", context=PROCESS_UTILITY_TOPLEVEL, params=0x0, dest=0x2403648,
>     completionTag=0x7ffdb4392550 "") at utility.c:373

In this scenario, we had just reindexed one of the indexes of
pg_stat_last_operation, and the metatrack update of that tried to insert a
row into the same table. But the second index in the table was pending
reindexing, which triggered the assertion.

After removing the redundant index, pg_stat_last_operation has only one
index, and that scenario no longer happens. This is a bit fragile fix,
because the problem will reappear as soon as you add a second index on the
table. But we have no plans of doing that, and I believe no harm would be
done in production builds with assertions disabled, anyway. So this will
do for now.

Reviewed-by: Ashwin Agrawal <aagrawal@pivotal.io>
Reviewed-by: Shaoqi Bai <sbai@pivotal.io>
Reviewed-by: Jamie McAtamney <jmcatamney@pivotal.io>
Try to fix qp_functions_in_subquery_constant hang issue

After reproduced on my laptop and checked the log, my theory is the
cancel happens at `postgres.c:5196` where I changed, but it doesn't
cancel the MPP operation because `DoingCommandRead` is true.

Push this to see if it works, then polish, then even a PR.

The plan, which is the same as 6X:
```
regression=# EXPLAIN SELECT * FROM foo, (SELECT func1_read_int_sql_stb(func2_read_int_stb(5)) from foo) r order by 1,2,3;
                                               QUERY PLAN
--------------------------------------------------------------------------------------------------------
 Gather Motion 3:1  (slice1; segments: 3)  (cost=10000000063.75..10000000066.00 rows=100 width=12)
   Merge Key: foo.a, foo.b
   ->  Sort  (cost=10000000063.75..10000000064.00 rows=34 width=12)
         Sort Key: foo.a, foo.b
         ->  Nested Loop  (cost=10000000000.00..10000000060.42 rows=34 width=12)
               ->  Seq Scan on foo  (cost=0.00..3.10 rows=4 width=8)
               ->  Materialize  (cost=0.00..3.65 rows=10 width=0)
                     ->  Broadcast Motion 3:3  (slice2; segments: 3)  (cost=0.00..3.50 rows=10 width=0)
                           ->  Seq Scan on foo foo_1  (cost=0.00..3.10 rows=4 width=0)
 Optimizer: Postgres query optimizer
(10 rows)
```

The QE errors out:
```
11855 2020-03-30 23:13:44.848707 CST,"adam","regression",p68223,th503250368,"127.0.0.1","51055",2020-03-30 23:13:35 CST,0,con216,cmd306,seg1,slice1,,,sx1,"LOG","00000","An exception was encountered during       the execution of statement: SELECT * FROM foo, (SELECT func1_read_int_sql_stb(func2_read_int_stb(a)) from foo) r order by 1,2,3;",,,,,,,0,,,,
11856 2020-03-30 23:13:44.858214 CST,"adam","regression",p68223,th503250368,"127.0.0.1","51055",2020-03-30 23:13:35 CST,0,con216,,seg1,,,,,"LOG","00000","Process interrupt for 'query cancel pending' (post      gres.c:5196)",,,,,,,0,,"postgres.c",3906,
```

Another QE hangs:
```
    frame #3: 0x000000010582d1e4 postgres`WaitLatchOrSocket(latch=0x0000000105e8c260, wakeEvents=25, sock=-1, timeout=250, wait_event_info=134217765) at latch.c:407:7
    frame #4: 0x0000000105b4bf74 postgres`receiveChunksUDPIFC(pTransportStates=0x00007fa1f982b380, pEntry=0x00000001146e50c8, motNodeID=2, srcRoute=0x00007ffeeaa42334, conn=0x0000000000000000) at ic_udpifc.c:3800:10
    frame #5: 0x0000000105b4c9a6 postgres`RecvTupleChunkFromAnyUDPIFC_Internal(transportStates=0x00007fa1f982b380, motNodeID=2, srcRoute=0x00007ffeeaa42334) at ic_udpifc.c:3915:11
    frame #6: 0x0000000105b48de3 postgres`RecvTupleChunkFromAnyUDPIFC(transportStates=0x00007fa1f982b380, motNodeID=2, srcRoute=0x00007ffeeaa42334) at ic_udpifc.c:3935:12
    frame #7: 0x0000000105b34553 postgres`processIncomingChunks(mlStates=0x00007fa1fc814b48, transportStates=0x00007fa1f982b380, pMNEntry=0x00007fa1fc814c58, motNodeID=2, srcRoute=-100) at cdbmotion.c:665:12
    frame #8: 0x0000000105b341f0 postgres`RecvTupleFrom(mlStates=0x00007fa1fc814b48, transportStates=0x00007fa1f982b380, motNodeID=2, srcRoute=-100) at cdbmotion.c:621:3
    frame #9: 0x00000001055ceb0d postgres`execMotionUnsortedReceiver(node=0x00007fa1fa80d650) at nodeMotion.c:319:10
    frame #10: 0x00000001055cdd90 postgres`ExecMotion(pstate=0x00007fa1fa80d650) at nodeMotion.c:144:12
    frame #11: 0x000000010556e139 postgres`ExecProcNodeGPDB(node=0x00007fa1fa80d650) at execProcnode.c:678:11
    frame #12: 0x00000001055a4462 postgres`ExecProcNode(node=0x00007fa1fa80d650) at executor.h:264:9
    frame #13: 0x00000001055a3dac postgres`ExecMaterial(pstate=0x00007fa1fa018b58) at nodeMaterial.c:198:15
    frame #14: 0x000000010556e139 postgres`ExecProcNodeGPDB(node=0x00007fa1fa018b58) at execProcnode.c:678:11
    frame #15: 0x00000001055aeb22 postgres`ExecProcNode(node=0x00007fa1fa018b58) at executor.h:264:9
    frame #16: 0x00000001055ae743 postgres`ExecNestLoop_guts(pstate=0x00007fa1fa018650) at nodeNestloop.c:253:20
    frame #17: 0x00000001055adf85 postgres`ExecNestLoop(pstate=0x00007fa1fa018650) at nodeNestloop.c:380:11
    frame #18: 0x000000010556e139 postgres`ExecProcNodeGPDB(node=0x00007fa1fa018650) at execProcnode.c:678:11
    frame #19: 0x00000001055b4be2 postgres`ExecProcNode(node=0x00007fa1fa018650) at executor.h:264:9
    frame #20: 0x00000001055b4146 postgres`ExecSort(pstate=0x00007fa1fa018358) at nodeSort.c:196:11
    frame #21: 0x000000010556e139 postgres`ExecProcNodeGPDB(node=0x00007fa1fa018358) at execProcnode.c:678:11
    frame #22: 0x000000010556d3be postgres`ExecProcNodeFirst(node=0x00007fa1fa018358) at execProcnode.c:643:9
    frame #23: 0x00000001055cef92 postgres`ExecProcNode(node=0x00007fa1fa018358) at executor.h:264:9
    frame #24: 0x00000001055ced45 postgres`execMotionSender(node=0x00007fa1fa018048) at nodeMotion.c:219:20
    frame #25: 0x00000001055cddf2 postgres`ExecMotion(pstate=0x00007fa1fa018048) at nodeMotion.c:183:10
    frame #26: 0x000000010556e139 postgres`ExecProcNodeGPDB(node=0x00007fa1fa018048) at execProcnode.c:678:11
    frame #27: 0x000000010556d3be postgres`ExecProcNodeFirst(node=0x00007fa1fa018048) at execProcnode.c:643:9
    frame #28: 0x0000000105565842 postgres`ExecProcNode(node=0x00007fa1fa018048) at executor.h:264:9
    frame #29: 0x00000001055613d0 postgres`ExecutePlan(estate=0x00007fa1fa017d48, planstate=0x00007fa1fa018048, use_parallel_mode=false, operation=CMD_SELECT, sendTuples=false, numberTuples=0, direction=ForwardScanDirection, dest=0x00007fa1f9836590, execute_once=false) at execMain.c:2664:10
    frame #30: 0x0000000105560fcb postgres`standard_ExecutorRun(queryDesc=0x00007fa1f981e948, direction=ForwardScanDirection, count=0, execute_once=false) at execMain.c:880:4
    frame #31: 0x0000000105560b51 postgres`ExecutorRun(queryDesc=0x00007fa1f981e948, direction=ForwardScanDirection, count=0, execute_once=false) at execMain.c:761:3
    frame #32: 0x0000000105876409 postgres`PortalRunSelect(portal=0x00007fa1fc037348, forward=true, count=0, dest=0x00007fa1f9836590) at pquery.c:1101:4
    frame #33: 0x0000000105875dd2 postgres`PortalRun(portal=0x00007fa1fc037348, count=9223372036854775807, isTopLevel=true, run_once=false, dest=0x00007fa1f9836590, altdest=0x00007fa1f9836590, completionTag="") at pquery.c:939:18
    frame #34: 0x0000000105870280 postgres`exec_mpp_query(query_string="SELECT * FROM foo, (SELECT func1_sql_setint_imm(func2_read_int_stb(5)) from foo) r order by 1,2,3;", serializedPlantree="Z\x02\x01", serializedPlantreelen=2259, serializedQueryDispatchDesc="\x0e", serializedQueryDispatchDesclen=384) at postgres.c:1422:10
    frame #35: 0x000000010586da7b postgres`PostgresMain(argc=1, argv=0x00007fa1fc00bb98, dbname="regression", username="adam") at postgres.c:5401:7
```
