'aggregates' regression test failing with debug-enabled ORCA
Thanks for reporting @hlinnaka there seems to be a problem in CTranslatorRelcacheToDXL since it tells that a boolean bucket has three distinct values:

```
      <dxl:ColumnStatistics Mdid="1.16390.1.0.0" Name="b1" Width="1.000000" NullFreq="0.333333" NdvRemain="0.000000" FreqRemain="0.000000" ColStatsMissing="false">
        <dxl:StatsBucket Frequency="0.666667" DistinctValues="3.000000">
          <dxl:LowerBound Closed="true" TypeMdid="0.16.1.0" IsNull="false" IsByValue="true" Value="false"/>
          <dxl:UpperBound Closed="true" TypeMdid="0.16.1.0" IsNull="false" IsByValue="true" Value="true"/>
        </dxl:StatsBucket>
      </dxl:ColumnStatistics>
```