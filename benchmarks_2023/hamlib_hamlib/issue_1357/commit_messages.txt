Add BACKLIGHT to IC7300
Add get_powerstat to  IC9700 which speeds up start when auto_power_on is detected
Fix parm_gran
https://github.com/Hamlib/Hamlib/issues/1357
Fix Yaesu gran_parm
https://github.com/Hamlib/Hamlib/issues/1357
Fix parm_gran for Kenwood
https://github.com/Hamlib/Hamlib/issues/1357
