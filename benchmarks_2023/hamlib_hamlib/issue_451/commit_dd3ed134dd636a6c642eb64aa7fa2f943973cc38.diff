diff --git a/doc/man1/rotctl.1 b/doc/man1/rotctl.1
index 669ad3c8e..74e067868 100644
--- a/doc/man1/rotctl.1
+++ b/doc/man1/rotctl.1
@@ -384,8 +384,8 @@ are returned as double precision floating point values.
 Move the rotator in a specific direction at the given rate.
 .IP
 .RI \(aq Direction \(aq
-is an integer defined as \(oq2\(cq = Up, \(oq4\(cq = Down, \(oq8\(cq = Left,
-and \(oq16\(cq = Right.
+is an integer or keyword defined as \(oq2\(cq = UP, \(oq4\(cq = DOWN, \(oq8\(cq = LEFT or CCW
+and \(oq16\(cq = RIGHT or CW
 .IP
 .RI \(aq Speed \(aq
 is an integer between 1 and 100.
diff --git a/include/hamlib/rotlist.h b/include/hamlib/rotlist.h
index 582e9152e..2dfd01204 100644
--- a/include/hamlib/rotlist.h
+++ b/include/hamlib/rotlist.h
@@ -325,7 +325,8 @@
 #define ROT_BACKEND_M2 "m2"
 //! @endcond
 #define ROT_MODEL_RC2800 ROT_MAKE_MODEL(ROT_M2, 1)
-#define ROT_MODEL_RC2800AZ ROT_MAKE_MODEL(ROT_M2, 2)
+#define ROT_MODEL_RC2800_EARLY_AZ ROT_MAKE_MODEL(ROT_M2, 2)
+#define ROT_MODEL_RC2800_EARLY_AZEL ROT_MAKE_MODEL(ROT_M2, 3)
 
 
 /**
diff --git a/rotators/m2/rc2800.c b/rotators/m2/rc2800.c
index ecbb08282..80b405121 100644
--- a/rotators/m2/rc2800.c
+++ b/rotators/m2/rc2800.c
@@ -39,7 +39,6 @@
 
 #define CR "\r"
 #define LF "\x0a"
-#define LFCR "\x0a\r"
 
 #define BUFSZ 128
 
@@ -248,21 +247,45 @@ rc2800_rot_set_position(ROT *rot, azimuth_t az, elevation_t el)
 
     rig_debug(RIG_DEBUG_TRACE, "%s called: %f %f\n", __func__, az, el);
 
-    num_sprintf(cmdstr, "A%3.0f"LFCR, az);
+    if (rot->caps->rot_model == ROT_MODEL_RC2800_EARLY_AZ)
+    {
+	// we only do azimuth and this is the old protocol
+	// we have to switch modes and then send azimuth
+	// an extra CR gives us a response to expect
+    	num_sprintf(cmdstr, "A\r%.0f\r\r", az);
+    }
+    else 
+    {
+        // does the new protocol use decimal points?
+	// we'll assume no for now
+    	num_sprintf(cmdstr, "A%0f"CR, az);
+    }
     retval1 = rc2800_transaction(rot, cmdstr, NULL, 0);
 
-    if (rot->caps->rot_model == ROT_MODEL_RC2800)
+    if (rot->caps->rot_type == ROT_TYPE_AZIMUTH)
     {
-        /* do not overwhelm the MCU? */
-        hl_usleep(200 * 1000);
+        return retval1;
+    }
 
-        num_sprintf(cmdstr, "E%3.0f"LFCR, el);
-        retval2 = rc2800_transaction(rot, cmdstr, NULL, 0);
+    /* do not overwhelm the MCU? */
+    hl_usleep(200 * 1000);
 
-        if (retval1 == retval2)
-        {
-            return retval1;
-        }
+    if (rot->caps->rot_model == ROT_MODEL_RC2800_EARLY_AZEL)
+    {
+	// this is the old protocol
+	// we have to switch modes and then send azimuth
+	// an extra CR gives us a response to expect
+    	num_sprintf(cmdstr, "E\r%.0f\r\r", el);
+    }
+    else 
+    {
+        num_sprintf(cmdstr, "E%.0f"CR, el);
+    }
+    retval2 = rc2800_transaction(rot, cmdstr, NULL, 0);
+
+    if (retval1 == retval2)
+    {
+        return retval1;
     }
 
     return (retval1 != RIG_OK ? retval1 : retval2);
@@ -278,6 +301,8 @@ rc2800_rot_get_position(ROT *rot, azimuth_t *az, elevation_t *el)
 
     rig_debug(RIG_DEBUG_TRACE, "%s called\n", __func__);
 
+    *el = 0;
+
     retval = rc2800_transaction(rot, "A" CR, posbuf, sizeof(posbuf));
 
     if (retval != RIG_OK || strlen(posbuf) < 5)
@@ -349,6 +374,11 @@ rc2800_rot_stop(ROT *rot)
 
     if (retval != RIG_OK) { rig_debug(RIG_DEBUG_VERBOSE, "%s: az S command failed?\n", __func__); }
 
+    if (rot->caps->rot_type == ROT_TYPE_AZIMUTH)
+    {
+        return retval;
+    }
+
     /* do not overwhelm the MCU? */
     hl_usleep(200 * 1000);
 
@@ -383,7 +413,7 @@ const struct rot_caps rc2800_rot_caps =
     .mfg_name =       "M2",
     .version =        "20201130",
     .copyright =      "LGPL",
-    .status =         RIG_STATUS_STABLE,
+    .status =         RIG_STATUS_BETA,
     .rot_type =       ROT_TYPE_AZEL,
     .port_type =      RIG_PORT_SERIAL,
     .serial_rate_min  = 9600,
@@ -407,10 +437,11 @@ const struct rot_caps rc2800_rot_caps =
     .stop         = rc2800_rot_stop,
 };
 
+// below tested on RC2800P-A
 const struct rot_caps rc2800az_rot_caps =
 {
-    ROT_MODEL(ROT_MODEL_RC2800AZ),
-    .model_name =     "RC2800AZ",
+    ROT_MODEL(ROT_MODEL_RC2800_EARLY_AZ),
+    .model_name =     "RC2800_EARLY_AZ",
     .mfg_name =       "M2",
     .version =        "20201130",
     .copyright =      "LGPL",
@@ -438,6 +469,37 @@ const struct rot_caps rc2800az_rot_caps =
     .stop         = rc2800_rot_stop,
 };
 
+const struct rot_caps rc2800azel_rot_caps =
+{
+    ROT_MODEL(ROT_MODEL_RC2800_EARLY_AZEL),
+    .model_name =     "RC2800_EARLY_AZEL",
+    .mfg_name =       "M2",
+    .version =        "20201130",
+    .copyright =      "LGPL",
+    .status =         RIG_STATUS_STABLE,
+    .rot_type =       ROT_TYPE_AZEL,
+    .port_type =      RIG_PORT_SERIAL,
+    .serial_rate_min  = 9600,
+    .serial_rate_max  = 9600,
+    .serial_data_bits = 8,
+    .serial_stop_bits = 1,
+    .serial_parity    = RIG_PARITY_NONE,
+    .serial_handshake = RIG_HANDSHAKE_NONE,
+    .write_delay      = 0,
+    .post_write_delay = 0,
+    .timeout          = 1000,
+    .retry            = 3,
+
+    .min_az =     0.0,
+    .max_az =     360.0,
+    .min_el =     0.0,
+    .max_el =     180.0,
+
+    .get_position = rc2800_rot_get_position,
+    .set_position = rc2800_rot_set_position,
+    .stop         = rc2800_rot_stop,
+};
+
 /* ************************************************************************* */
 
 DECLARE_INITROT_BACKEND(m2)
@@ -446,6 +508,7 @@ DECLARE_INITROT_BACKEND(m2)
 
     rot_register(&rc2800_rot_caps);
     rot_register(&rc2800az_rot_caps);
+    rot_register(&rc2800azel_rot_caps);
 
     return RIG_OK;
 }
