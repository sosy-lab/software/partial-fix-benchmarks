diff --git a/include/hamlib/rig.h b/include/hamlib/rig.h
index 276a872e7..af26c1341 100644
--- a/include/hamlib/rig.h
+++ b/include/hamlib/rig.h
@@ -1966,7 +1966,7 @@ enum rig_caps_cptr_e {
  *
  */
 //! @cond Doxygen_Suppress
-extern int rig_get_caps_int(rig_model_t rig_model, enum rig_caps_int_e rig_caps);
+extern long rig_get_caps_int(rig_model_t rig_model, enum rig_caps_int_e rig_caps);
 
 /**
  * \brief Function to return char pointer value from rig->caps
@@ -2308,6 +2308,10 @@ extern HAMLIB_EXPORT(int)
 rig_get_vfo HAMLIB_PARAMS((RIG *rig,
                            vfo_t *vfo));
 
+extern HAMLIB_EXPORT(int)
+rig_get_vfo_info HAMLIB_PARAMS((RIG *rig,
+                           vfo_t vfo, freq_t *freq, rmode_t *mode, pbwidth_t *width));
+
 extern HAMLIB_EXPORT(int)
 netrigctl_get_vfo_mode HAMLIB_PARAMS((RIG *rig));
 
@@ -2783,6 +2787,11 @@ rig_set_vfo_callback HAMLIB_PARAMS((RIG *,
                                     vfo_cb_t,
                                     rig_ptr_t));
 
+extern HAMLIB_EXPORT(int)
+rig_get_vfo_info_callback HAMLIB_PARAMS((RIG *,
+                                          vfo_cb_t,
+                                          rig_ptr_t));
+
 extern HAMLIB_EXPORT(int)
 rig_set_ptt_callback HAMLIB_PARAMS((RIG *,
                                     ptt_cb_t,
@@ -2944,6 +2953,7 @@ extern HAMLIB_EXPORT(int) rig_get_cache_timeout_ms(RIG *rig, hamlib_cache_t sele
 extern HAMLIB_EXPORT(int) rig_set_cache_timeout_ms(RIG *rig, hamlib_cache_t selection, int ms);
 
 extern HAMLIB_EXPORT(int) rig_set_vfo_opt(RIG *rig, int status);
+extern HAMLIB_EXPORT(int) rig_get_vfo_info(RIG *rig, vfo_t vfo, freq_t *freq, rmode_t *mode, pbwidth_t *width);
 
 
 typedef unsigned long rig_useconds_t;
diff --git a/src/misc.c b/src/misc.c
index b8c2a66e9..45619c6ff 100644
--- a/src/misc.c
+++ b/src/misc.c
@@ -2140,8 +2140,13 @@ void *rig_get_function_ptr(rig_model_t rig_model,
 }
 
 // negative return indicates error
-// watch out for integer values that may be negative
-int rig_get_caps_int(rig_model_t rig_model, enum rig_caps_int_e rig_caps)
+/**
+ * \brief Get integer/long instead of using rig->caps
+ *  watch out for integer values that may be negative -- if needed must change hamlib
+ * \param RIG* and rig_caps_int_e
+ * \return the corresponding long value -- -RIG_EINVAL is the only error possible
+ */
+long rig_get_caps_int(rig_model_t rig_model, enum rig_caps_int_e rig_caps)
 {
     const struct rig_caps *caps = rig_get_caps(rig_model);
 
diff --git a/src/rig.c b/src/rig.c
index 65b594b49..c85ac212a 100644
--- a/src/rig.c
+++ b/src/rig.c
@@ -5550,6 +5550,37 @@ const char *HAMLIB_API rig_get_info(RIG *rig)
     RETURNFUNC(rig->caps->get_info(rig));
 }
 
+/**
+ * \brief get freq/mode/width for requested VFO
+ * \param rig   The rig handle
+ * \param vfo   The VFO to get
+ *
+ *  Gets the current VFO information. The VFO can be RIG_VFO_A, RIG_VFO_B, RIG_VFO_C
+ *  for VFOA, VFOB, VFOC respectively or RIG_VFO_MEM for Memory mode.
+ *  Supported VFOs depends on rig capabilities.
+ *
+ * \RETURNFUNC(RIG_OK) if the operation has been successful, otherwise
+ * a negative value if an error occurred (in which case use rigerror(return)
+ * for error message).
+ *
+ */
+int HAMLIB_API rig_get_vfo_info(RIG *rig, vfo_t vfo, freq_t *freq, rmode_t *mode, pbwidth_t *width)
+{
+    int retcode;
+
+    ENTERFUNC;
+    rig_debug(RIG_DEBUG_VERBOSE, "%s called vfo=%s\n", __func__, rig_strvfo(vfo));
+
+    if (CHECK_RIG_ARG(rig))
+    {
+        RETURNFUNC(-RIG_EINVAL);
+    }
+
+    retcode = rig_get_freq(rig,vfo,freq);
+    if (retcode != RIG_OK) RETURNFUNC(retcode);
+    retcode = rig_get_mode(rig,vfo,mode,width);
+    RETURNFUNC(retcode);
+}
 
 /**
  * \brief get the Hamlib license
diff --git a/tests/rigctl_parse.c b/tests/rigctl_parse.c
index 2b39527b9..363500e4e 100644
--- a/tests/rigctl_parse.c
+++ b/tests/rigctl_parse.c
@@ -166,6 +166,7 @@ declare_proto_rig(set_mode);
 declare_proto_rig(get_mode);
 declare_proto_rig(set_vfo);
 declare_proto_rig(get_vfo);
+declare_proto_rig(get_vfo_info);
 declare_proto_rig(set_ptt);
 declare_proto_rig(get_ptt);
 declare_proto_rig(get_ptt);
@@ -330,6 +331,7 @@ static struct test_table test_list[] =
     { 0x8f, "dump_state",       ACTION(dump_state),     ARG_OUT | ARG_NOVFO },
     { 0xf0, "chk_vfo",          ACTION(chk_vfo),        ARG_NOVFO, "ChkVFO" },   /* rigctld only--check for VFO mode */
     { 0xf2, "set_vfo_opt",      ACTION(set_vfo_opt),    ARG_NOVFO | ARG_IN, "Status" }, /* turn vfo option on/off */
+    { 0xf3, "get_vfo_info",     ACTION(get_vfo_info),   ARG_NOVFO | ARG_IN1 | ARG_OUT3, "VFO", "Freq", "Mode", "Width" }, /* turn vfo option on/off */
     { 0xf1, "halt",             ACTION(halt),           ARG_NOVFO },   /* rigctld only--halt the daemon */
     { 0x8c, "pause",            ACTION(pause),          ARG_IN, "Seconds" },
     { 0x00, "", NULL },
@@ -2180,6 +2182,40 @@ declare_proto_rig(get_vfo)
     return status;
 }
 
+/* '\get_vfo_info' */
+declare_proto_rig(get_vfo_info)
+{
+    int retval;
+
+    ENTERFUNC;
+    if (!strcmp(arg1, "?"))
+    {
+        char s[SPRINTF_MAX_SIZE];
+        rig_sprintf_vfo(s, rig->state.vfo_list);
+        fprintf(fout, "%s\n", s);
+        return RIG_OK;
+    }
+
+    vfo = rig_parse_vfo(arg1);
+    freq_t freq=0;
+    rmode_t mode=RIG_MODE_NONE;
+    pbwidth_t width = 0;
+    retval = rig_get_vfo_info(rig, vfo, &freq, &mode, &width);
+
+    rig_debug(RIG_DEBUG_ERR,"%s: vfo=%s\n", __func__, rig_strvfo(vfo));
+    if ((interactive && prompt) || (interactive && !prompt && ext_resp))
+    {
+        fprintf(fout,"%s: %.0f\n", cmd->arg1, freq);
+        fprintf(fout,"%s: %s\n", cmd->arg2, rig_strrmode(mode));
+        fprintf(fout,"%s: %d\n", cmd->arg3, (int)width);
+    }
+    else
+    {
+        fprintf(fout,"%.0f\n%s\n%d\n", freq, rig_strrmode(mode), (int)width);
+    }
+    RETURNFUNC(retval);
+}
+
 
 /* 'T' */
 declare_proto_rig(set_ptt)
