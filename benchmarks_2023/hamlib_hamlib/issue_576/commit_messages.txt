Update comments
Reverse RTTY and RTTYR in kenwood.c mode table
It was backwards.  RTTY is LSB (ie FSKR) and RTTYR is USB (ie FSK)
So RTTYR is mode 6 and RTTY is mode 9
https://github.com/Hamlib/Hamlib/issues/576
Revert kenwood RTTY/RTTYR and fix k3.c's DT mode to match it
https://github.com/Hamlib/Hamlib/issues/576
