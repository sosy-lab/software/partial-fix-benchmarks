CLEANUP: mux-fcgi: Don't needlessly store result of data/trailers parsing

Return values of fcgi_strm_parse_data() and fcgi_strm_parse_trailers() are
no longer checked. Thus it is useless to store it.

This patch should fix the issues #1269 and #1268.
BUILD: fix compilation for OpenSSL-3.0.0-alpha17

Some changes in the OpenSSL syntax API broke this syntax:
  #if SSL_OP_NO_TLSv1_3

OpenSSL made this change which broke our usage in commit f04bb0bce490de847ed0482b8ec9eabedd173852:

-# define SSL_OP_NO_TLSv1_3                               (uint64_t)0x20000000
+#define SSL_OP_BIT(n)  ((uint64_t)1 << (uint64_t)n)
+# define SSL_OP_NO_TLSv1_3                               SSL_OP_BIT(29)

Which can't be evaluated by the preprocessor anymore.
This patch replace the test by an openssl version test.

This fix part of #1276 issue.
CI: github: switch to OpenSSL 3.0.0

Switch the OpenSSL 3.0.0alpha17 version to the final 3.0.0 release.

Part of OpenSSL 3.0.0 portage. (ticket #1276)
BUILD: fix compilation for OpenSSL-3.0.0-alpha17

Some changes in the OpenSSL syntax API broke this syntax:
  #if SSL_OP_NO_TLSv1_3

OpenSSL made this change which broke our usage in commit f04bb0bce490de847ed0482b8ec9eabedd173852:

-# define SSL_OP_NO_TLSv1_3                               (uint64_t)0x20000000
+#define SSL_OP_BIT(n)  ((uint64_t)1 << (uint64_t)n)
+# define SSL_OP_NO_TLSv1_3                               SSL_OP_BIT(29)

Which can't be evaluated by the preprocessor anymore.
This patch replace the test by an openssl version test.

This fix part of #1276 issue.

(cherry picked from commit f22b032956bc492dcf47b2a909f91a6fb2c6e49b)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
CI: github: switch to OpenSSL 3.0.0

Switch the OpenSSL 3.0.0alpha17 version to the final 3.0.0 release.

Part of OpenSSL 3.0.0 portage. (ticket #1276)

(cherry picked from commit bc2b96c2de9edf9754178dd011bbead0657a0b5f)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
BUILD: fix compilation for OpenSSL-3.0.0-alpha17

Some changes in the OpenSSL syntax API broke this syntax:
  #if SSL_OP_NO_TLSv1_3

OpenSSL made this change which broke our usage in commit f04bb0bce490de847ed0482b8ec9eabedd173852:

-# define SSL_OP_NO_TLSv1_3                               (uint64_t)0x20000000
+#define SSL_OP_BIT(n)  ((uint64_t)1 << (uint64_t)n)
+# define SSL_OP_NO_TLSv1_3                               SSL_OP_BIT(29)

Which can't be evaluated by the preprocessor anymore.
This patch replace the test by an openssl version test.

This fix part of #1276 issue.

(cherry picked from commit f22b032956bc492dcf47b2a909f91a6fb2c6e49b)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
(cherry picked from commit e8a8aa4ed2cf5421c26b0be981e14c711cb465f3)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
CI: github: switch to OpenSSL 3.0.0

Switch the OpenSSL 3.0.0alpha17 version to the final 3.0.0 release.

Part of OpenSSL 3.0.0 portage. (ticket #1276)

(cherry picked from commit bc2b96c2de9edf9754178dd011bbead0657a0b5f)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
(cherry picked from commit 7ac94ab11d0050b43912318ac5b467460a5fa10d)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUILD: fix compilation for OpenSSL-3.0.0-alpha17

Some changes in the OpenSSL syntax API broke this syntax:
  #if SSL_OP_NO_TLSv1_3

OpenSSL made this change which broke our usage in commit f04bb0bce490de847ed0482b8ec9eabedd173852:

-# define SSL_OP_NO_TLSv1_3                               (uint64_t)0x20000000
+#define SSL_OP_BIT(n)  ((uint64_t)1 << (uint64_t)n)
+# define SSL_OP_NO_TLSv1_3                               SSL_OP_BIT(29)

Which can't be evaluated by the preprocessor anymore.
This patch replace the test by an openssl version test.

This fix part of #1276 issue.

(cherry picked from commit f22b032956bc492dcf47b2a909f91a6fb2c6e49b)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
(cherry picked from commit e8a8aa4ed2cf5421c26b0be981e14c711cb465f3)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit e5f6e07640efc5bf1a9564451a7a15be0e881221)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
CI: github: switch to OpenSSL 3.0.0

Switch the OpenSSL 3.0.0alpha17 version to the final 3.0.0 release.

Part of OpenSSL 3.0.0 portage. (ticket #1276)

(cherry picked from commit bc2b96c2de9edf9754178dd011bbead0657a0b5f)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
(cherry picked from commit 7ac94ab11d0050b43912318ac5b467460a5fa10d)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit e1f9b3e4139344123a729f70996657d5d2836f4e)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUILD: fix compilation for OpenSSL-3.0.0-alpha17

Some changes in the OpenSSL syntax API broke this syntax:
  #if SSL_OP_NO_TLSv1_3

OpenSSL made this change which broke our usage in commit f04bb0bce490de847ed0482b8ec9eabedd173852:

-# define SSL_OP_NO_TLSv1_3                               (uint64_t)0x20000000
+#define SSL_OP_BIT(n)  ((uint64_t)1 << (uint64_t)n)
+# define SSL_OP_NO_TLSv1_3                               SSL_OP_BIT(29)

Which can't be evaluated by the preprocessor anymore.
This patch replace the test by an openssl version test.

This fix part of #1276 issue.

(cherry picked from commit f22b032956bc492dcf47b2a909f91a6fb2c6e49b)
Signed-off-by: William Lallemand <wlallemand@haproxy.org>
(cherry picked from commit e8a8aa4ed2cf5421c26b0be981e14c711cb465f3)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit e5f6e07640efc5bf1a9564451a7a15be0e881221)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit aa502c52a1b5f6c9ca2de2580bfcb26ed0e88787)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
