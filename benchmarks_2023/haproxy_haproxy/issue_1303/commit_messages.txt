CLEANUP: backend: remove impossible case of round-robin + consistent hash

In 1.4, consistent hashing was brought by commit 6b2e11be1 ("[MEDIUM]
backend: implement consistent hashing variation") which took care of
replacing all direct calls to map_get_server_rr() with an alternate
call to chash_get_next_server() if consistent hash was being used.

One of them, however, cannot happen because a preliminary test for
static round-robin is being done prior to the call, so we're certain
that if it matches it cannot use a consistent hash tree.

Let's remove it.
MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules

If it possible to set source IP/Port from "tcp-request connection",
"tcp-request session" and "http-request" rules but not from "tcp-request
content" rules. There is no reason for this limitation and it may be a
problem for anyone wanting to call a lua fetch to dynamically set source
IP/Port from a TCP proxy. Indeed, to call a lua fetch, we must have a
stream. And there is no stream when "tcp-request connection/session" rules
are evaluated.

Thanks to this patch, "set-src" and "set-src-port" action are now supported
by "tcp_request content" rules.

This patch is related to the issue #1303. It may be backported to all stable
versions.
MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules

If it possible to set source IP/Port from "tcp-request connection",
"tcp-request session" and "http-request" rules but not from "tcp-request
content" rules. There is no reason for this limitation and it may be a
problem for anyone wanting to call a lua fetch to dynamically set source
IP/Port from a TCP proxy. Indeed, to call a lua fetch, we must have a
stream. And there is no stream when "tcp-request connection/session" rules
are evaluated.

Thanks to this patch, "set-src" and "set-src-port" action are now supported
by "tcp_request content" rules.

This patch is related to the issue #1303. It may be backported to all stable
versions.

(cherry picked from commit 19bbbe05629ea947dd60d5b96d96f0066b047b97)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules

If it possible to set source IP/Port from "tcp-request connection",
"tcp-request session" and "http-request" rules but not from "tcp-request
content" rules. There is no reason for this limitation and it may be a
problem for anyone wanting to call a lua fetch to dynamically set source
IP/Port from a TCP proxy. Indeed, to call a lua fetch, we must have a
stream. And there is no stream when "tcp-request connection/session" rules
are evaluated.

Thanks to this patch, "set-src" and "set-src-port" action are now supported
by "tcp_request content" rules.

This patch is related to the issue #1303. It may be backported to all stable
versions.

(cherry picked from commit 19bbbe05629ea947dd60d5b96d96f0066b047b97)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 40f95c49bbbc79f99fe4d1554c092d0764606a46)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 78f4613532d41b133ed6d02d1b8ae3d50ed6ab72)
[cf: changes applied in src/proto_tcp.c instead of src/tcp_act.c]
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit afad5c385c146be1d5e1a50c6e3bd317301aa27f)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules

If it possible to set source IP/Port from "tcp-request connection",
"tcp-request session" and "http-request" rules but not from "tcp-request
content" rules. There is no reason for this limitation and it may be a
problem for anyone wanting to call a lua fetch to dynamically set source
IP/Port from a TCP proxy. Indeed, to call a lua fetch, we must have a
stream. And there is no stream when "tcp-request connection/session" rules
are evaluated.

Thanks to this patch, "set-src" and "set-src-port" action are now supported
by "tcp_request content" rules.

This patch is related to the issue #1303. It may be backported to all stable
versions.

(cherry picked from commit 19bbbe05629ea947dd60d5b96d96f0066b047b97)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 40f95c49bbbc79f99fe4d1554c092d0764606a46)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules

If it possible to set source IP/Port from "tcp-request connection",
"tcp-request session" and "http-request" rules but not from "tcp-request
content" rules. There is no reason for this limitation and it may be a
problem for anyone wanting to call a lua fetch to dynamically set source
IP/Port from a TCP proxy. Indeed, to call a lua fetch, we must have a
stream. And there is no stream when "tcp-request connection/session" rules
are evaluated.

Thanks to this patch, "set-src" and "set-src-port" action are now supported
by "tcp_request content" rules.

This patch is related to the issue #1303. It may be backported to all stable
versions.

(cherry picked from commit 19bbbe05629ea947dd60d5b96d96f0066b047b97)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 40f95c49bbbc79f99fe4d1554c092d0764606a46)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 78f4613532d41b133ed6d02d1b8ae3d50ed6ab72)
[cf: changes applied in src/proto_tcp.c instead of src/tcp_act.c]
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
Revert "MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules"

This reverts commit 19bbbe05629ea947dd60d5b96d96f0066b047b97.

For now, set-src/set-src-port actions are directly performed on the client
connection. Using these actions at the stream level is really a problem with
HTTP connection (See #90) because all requests are affected by this change
and not only the current request. And it is worse with the H2, because
several requests can set their source address into the same connection at
the same time.

It is already an issue when these actions are called from "http-request"
rules. It is safer to wait a bit before adding the support to "tcp-request
content" rules. The solution is to be able to set src/dst address on the
stream and not on the connection when the action if performed from the L7
level..

Reverting the above commit means the issue #1303 is no longer fixed.

This patch must be backported in all branches containing the above commit
(as far as 2.0 for now).
Revert "MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules"

This reverts commit 19bbbe05629ea947dd60d5b96d96f0066b047b97.

For now, set-src/set-src-port actions are directly performed on the client
connection. Using these actions at the stream level is really a problem with
HTTP connection (See #90) because all requests are affected by this change
and not only the current request. And it is worse with the H2, because
several requests can set their source address into the same connection at
the same time.

It is already an issue when these actions are called from "http-request"
rules. It is safer to wait a bit before adding the support to "tcp-request
content" rules. The solution is to be able to set src/dst address on the
stream and not on the connection when the action if performed from the L7
level..

Reverting the above commit means the issue #1303 is no longer fixed.

This patch must be backported in all branches containing the above commit
(as far as 2.0 for now).

(cherry picked from commit 23048875a4eacf5d7d4450d677cb077e67778b95)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
Revert "MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules"

This reverts commit 19bbbe05629ea947dd60d5b96d96f0066b047b97.

For now, set-src/set-src-port actions are directly performed on the client
connection. Using these actions at the stream level is really a problem with
HTTP connection (See #90) because all requests are affected by this change
and not only the current request. And it is worse with the H2, because
several requests can set their source address into the same connection at
the same time.

It is already an issue when these actions are called from "http-request"
rules. It is safer to wait a bit before adding the support to "tcp-request
content" rules. The solution is to be able to set src/dst address on the
stream and not on the connection when the action if performed from the L7
level..

Reverting the above commit means the issue #1303 is no longer fixed.

This patch must be backported in all branches containing the above commit
(as far as 2.0 for now).

(cherry picked from commit 23048875a4eacf5d7d4450d677cb077e67778b95)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 390f49477159de53d0506cd52bd6ed323febde0a)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 637b026540de4a328da6a214dc29e00906317cdc)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
Revert "MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules"

This reverts commit 19bbbe05629ea947dd60d5b96d96f0066b047b97.

For now, set-src/set-src-port actions are directly performed on the client
connection. Using these actions at the stream level is really a problem with
HTTP connection (See #90) because all requests are affected by this change
and not only the current request. And it is worse with the H2, because
several requests can set their source address into the same connection at
the same time.

It is already an issue when these actions are called from "http-request"
rules. It is safer to wait a bit before adding the support to "tcp-request
content" rules. The solution is to be able to set src/dst address on the
stream and not on the connection when the action if performed from the L7
level..

Reverting the above commit means the issue #1303 is no longer fixed.

This patch must be backported in all branches containing the above commit
(as far as 2.0 for now).

(cherry picked from commit 23048875a4eacf5d7d4450d677cb077e67778b95)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 390f49477159de53d0506cd52bd6ed323febde0a)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
Revert "MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules"

This reverts commit 19bbbe05629ea947dd60d5b96d96f0066b047b97.

For now, set-src/set-src-port actions are directly performed on the client
connection. Using these actions at the stream level is really a problem with
HTTP connection (See #90) because all requests are affected by this change
and not only the current request. And it is worse with the H2, because
several requests can set their source address into the same connection at
the same time.

It is already an issue when these actions are called from "http-request"
rules. It is safer to wait a bit before adding the support to "tcp-request
content" rules. The solution is to be able to set src/dst address on the
stream and not on the connection when the action if performed from the L7
level..

Reverting the above commit means the issue #1303 is no longer fixed.

This patch must be backported in all branches containing the above commit
(as far as 2.0 for now).

(cherry picked from commit 23048875a4eacf5d7d4450d677cb077e67778b95)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 390f49477159de53d0506cd52bd6ed323febde0a)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 637b026540de4a328da6a214dc29e00906317cdc)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit fcc1dd5fed7e7228c6004ce3a52efc5d39e62006)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
MINOR: tcp-act: Add set-src/set-src-port for "tcp-request content" rules

This patch was reverted because it was inconsitent to change connection
addresses at stream level. Especially in HTTP because all requests was
affected by this change and not only the current one. In HTTP/2, it was
worse. Several streams was able to change the connection addresses at the
same time.

It is no longer an issue, thanks to recent changes. With multi-level client
source and destination addresses, it is possible to limit the change to the
current request. Thus this patch can be reintroduced.

If it possible to set source IP/Port from "tcp-request connection",
"tcp-request session" and "http-request" rules but not from "tcp-request
content" rules. There is no reason for this limitation and it may be a
problem for anyone wanting to call a lua fetch to dynamically set source
IP/Port from a TCP proxy. Indeed, to call a lua fetch, we must have a
stream. And there is no stream when "tcp-request connection/session" rules
are evaluated.

Thanks to this patch, "set-src" and "set-src-port" action are now supported
by "tcp_request content" rules.

This patch is related to the issue #1303.
