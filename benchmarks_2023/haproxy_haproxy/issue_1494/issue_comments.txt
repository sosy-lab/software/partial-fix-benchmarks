2.4.9 SSL related crash?
Very interesting, thank you Christian! It looks amazingly close to a private report we've got in 2.5 and could confirm that something related to SSL that changed between 2.4 and 2.5 was indeed related to a fix backported between 2.4.7 and 2.4.9. For now we don't know but are actively tracking this one. Some of us may come back asking for extra info soon. Cc @wlallemand @pierrecdn 

Continuing the conversation here.
For now, I didn't get any new coredump after 3 days running the 2.4.9. So, the issue we observed may be unrelated to this one, or we still have to wait a bit more so that it reappears.
OK thank you Pierre. This will probably arrange William but not the rest of us :-)
@idl0r few questions:

* do you reproduce this problem often?
*  Did you use your configuration with a previous haproxy version which did not triggered this problem? We had few changes in the SSL stack in the maintenance branch.
*   Are you using SSL only on the frontend side or is it used on the backends? 
> @idl0r few questions:
> 
> * do you reproduce this problem often?

No, the one reported here was the only one until now.

> * Did you use your configuration with a previous haproxy version which did not triggered this problem? We had few changes in the SSL stack in the maintenance branch.

Yep, 1:1 the same config with 2.2.17 and lower for quite some months.

> * Are you using SSL only on the frontend side or is it used on the backends?

99% is frontend only but there are some with backend SSL as well.


@idl0r would you be willing to share your coredump and your configuration privately? That would be really useful. if so, email me at wlallemand [at] haproxy [dot] com
Just for the record: William got something via mail :)
Christian, we do both have your archive and will look at it next week as we've been quite busy with other bugs already. Thanks!
No rush! It crashed only twice so far, on ~26 LBs since we have upgraded to 2.4.9 so it's a rather rare crash.
Happy christmas!
In the end it was an bug in SPOE causing a use-after-free and corrupting random memory locations.
Yup, that seems to be it. No more crashes yet! Thanks, guys!
I guess it's ok to close this issue then. In the worst case, I'll re-open it :P
Great news! However let's not close before we've backported it completely, or we may forget to backport the fix far enough.