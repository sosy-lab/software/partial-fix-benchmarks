PHP-FPM in combination with gzip fails to return response in some cases
Indeed, I confirm the issue. I'll fix it soon.
I pushed two patches that should fix the issue. I'll backport it soon.
Perfect, so it should be part 2.5.6, right? I will keep my eyes on the releases :+1:
Yes it will be shipped with the next stable releases.
I can confirm that I no longer have issues with the latest release, thank you again for the quick fix 👍 
Thanks. I'm reopening the issue because the fix must still be backported as far as 2.2.