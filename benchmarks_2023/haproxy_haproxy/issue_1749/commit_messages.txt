BUG/MINOR: quic: Wrong reuse of fulfilled dgram RX buffer

After having fulfilled a buffer, then marked it as full, we must
consume the remaining space. But to do that, and not to erase the
already existing data, we must check there is not remaining data in
after the tail of the buffer (between the tail and the head).
This is done adding a condition to test that adding the number of
bytes from the remaining contiguous space to the tail does not
pass the wrapping postion in the buffer.

Must be backported to 2.6.
BUG/MAJOR: quic: Big RX dgrams leak when fulfilling a buffer

When entering quic_sock_fd_iocb() I/O handler which is responsible
of recvfrom() datagrams, the first thing which is done it to try
to reuse a dgram object containing metadata about the received
datagrams which has been consumed by the connection thread.
If this object could not be used for any reason, so when we
"goto out" of this function, we must release the memory allocated
for this objet, if not it will leak. Most of the time, this happened
when we fulfilled a buffer as reported in GH #1749 by Tristan. This is why we
added a pool_free() call just before the out label. We mark <new_dgram> as NULL
when it successfully could be used.

Thank you for Tristan and Willy for their participation on this issue.

Must be backported to 2.6.
BUG/MAJOR: quic: Big RX dgrams leak with POST requests

This previous commit:
 "BUG/MAJOR: Big RX dgrams leak when fulfilling a buffer"
partially fixed an RX dgram memleak. There is a missing break in the loop which
looks for the first datagram attached to an RX buffer dgrams list which may be
reused (because consumed by the connection thread). So when several dgrams were
consumed by the connection thread and are present in the RX buffer list, some are
leaked because never reused for ever. They are removed for their list.

Furthermore, as commented in this patch, there is always at least one dgram
object attached to an RX dgrams list, excepted the first time we enter this
I/O handler function for this RX buffer. So, there is no need to use a loop
to lookup and reuse the first datagram in an RX buffer dgrams list.

This isssue was reproduced with quiche client with plenty of POST requests
(100000 streams):
 cargo run --bin quiche-client -- https://127.0.0.1:8080/helloworld.html
 --no-verify -n 100000 --method POST --body /var/www/html/helloworld.html
and could be reproduce with GET request.

This bug was reported by Tristan in GH #1749.

Must be backported to 2.6.
BUG/MAJOR: quic: Big RX dgrams leak when fulfilling a buffer

When entering quic_sock_fd_iocb() I/O handler which is responsible
of recvfrom() datagrams, the first thing which is done it to try
to reuse a dgram object containing metadata about the received
datagrams which has been consumed by the connection thread.
If this object could not be used for any reason, so when we
"goto out" of this function, we must release the memory allocated
for this objet, if not it will leak. Most of the time, this happened
when we fulfilled a buffer as reported in GH #1749 by Tristan. This is why we
added a pool_free() call just before the out label. We mark <new_dgram> as NULL
when it successfully could be used.

Thank you for Tristan and Willy for their participation on this issue.

Must be backported to 2.6.

(cherry picked from commit 19ef6369b5539daa9da63195df630b08e4b0cccd)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUG/MAJOR: quic: Big RX dgrams leak with POST requests

This previous commit:
 "BUG/MAJOR: Big RX dgrams leak when fulfilling a buffer"
partially fixed an RX dgram memleak. There is a missing break in the loop which
looks for the first datagram attached to an RX buffer dgrams list which may be
reused (because consumed by the connection thread). So when several dgrams were
consumed by the connection thread and are present in the RX buffer list, some are
leaked because never reused for ever. They are removed for their list.

Furthermore, as commented in this patch, there is always at least one dgram
object attached to an RX dgrams list, excepted the first time we enter this
I/O handler function for this RX buffer. So, there is no need to use a loop
to lookup and reuse the first datagram in an RX buffer dgrams list.

This isssue was reproduced with quiche client with plenty of POST requests
(100000 streams):
 cargo run --bin quiche-client -- https://127.0.0.1:8080/helloworld.html
 --no-verify -n 100000 --method POST --body /var/www/html/helloworld.html
and could be reproduce with GET request.

This bug was reported by Tristan in GH #1749.

Must be backported to 2.6.

(cherry picked from commit 2bed1f166e0f4fd865007a8ad86be7263b019427)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
