MINOR: dynbuf: switch allocation and release to macros to better track users

When building with DEBUG_MEM_STATS, we only see b_alloc() and b_free() as
users of the "buffer" pool, because all call places rely on these more
convenient functions. It's annoying because it makes it very hard to see
which parts of the code are consuming buffers.

By switching the b_alloc() and b_free() inline functions to macros, we
can now finally track the users of struct buffer, e.g:

  mux_h1.c:513            P_FREE  size:   1275002880  calls:     38910  size/call:  32768 buffer
  mux_h1.c:498           P_ALLOC  size:   1912438784  calls:     58363  size/call:  32768 buffer
  stream.c:763            P_FREE  size:   4121493504  calls:    125778  size/call:  32768 buffer
  stream.c:759            P_FREE  size:   2061697024  calls:     62918  size/call:  32768 buffer
  stream.c:742           P_ALLOC  size:   3341123584  calls:    101963  size/call:  32768 buffer
  stream.c:632            P_FREE  size:   1275068416  calls:     38912  size/call:  32768 buffer
  stream.c:631            P_FREE  size:    637435904  calls:     19453  size/call:  32768 buffer
  channel.h:850          P_ALLOC  size:   4116480000  calls:    125625  size/call:  32768 buffer
  channel.h:850          P_ALLOC  size:       720896  calls:        22  size/call:  32768 buffer
  dynbuf.c:55             P_FREE  size:        65536  calls:         2  size/call:  32768 buffer

Let's do this since it doesn't change anything for the output code
(beyond adding the call places). Interestingly the code even got
slightly smaller now.
BUG/MINOR: mux-h1: Do not send a last null chunk on body-less answers

HEAD answers should not contain any body data. Currently when a
"transfer-encoding: chunked" header is returned, a last null-chunk is added to
the answer. Some clients choke on it and fail when trying to reuse the
connection. Check that the response should not be body-less before sending the
null-chunk.

This patch should fix #1932. It must be backported as far as 2.4.
BUG/MINOR: mux-h1: Do not send a last null chunk on body-less answers

HEAD answers should not contain any body data. Currently when a
"transfer-encoding: chunked" header is returned, a last null-chunk is added to
the answer. Some clients choke on it and fail when trying to reuse the
connection. Check that the response should not be body-less before sending the
null-chunk.

This patch should fix #1932. It must be backported as far as 2.4.

(cherry picked from commit 226082d13a0d0b83114e933b3a63916b18f9824b)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUG/MINOR: mux-h1: Do not send a last null chunk on body-less answers

HEAD answers should not contain any body data. Currently when a
"transfer-encoding: chunked" header is returned, a last null-chunk is added to
the answer. Some clients choke on it and fail when trying to reuse the
connection. Check that the response should not be body-less before sending the
null-chunk.

This patch should fix #1932. It must be backported as far as 2.4.

(cherry picked from commit 226082d13a0d0b83114e933b3a63916b18f9824b)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 611bfc4356544a44a1fc11024eee3eac4de1293d)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 01740c1794ed9b1dfdc90d7e03e3214cc81c16bc)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
BUG/MINOR: mux-h1: Do not send a last null chunk on body-less answers

HEAD answers should not contain any body data. Currently when a
"transfer-encoding: chunked" header is returned, a last null-chunk is added to
the answer. Some clients choke on it and fail when trying to reuse the
connection. Check that the response should not be body-less before sending the
null-chunk.

This patch should fix #1932. It must be backported as far as 2.4.

(cherry picked from commit 226082d13a0d0b83114e933b3a63916b18f9824b)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
(cherry picked from commit 611bfc4356544a44a1fc11024eee3eac4de1293d)
Signed-off-by: Christopher Faulet <cfaulet@haproxy.com>
