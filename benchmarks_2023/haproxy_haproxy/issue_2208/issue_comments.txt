The response status message of code 302 says `Moved Temporarily`.
Indeed, It is a cut/paste error and it exists since a while. I will fix it. Thanks !
For the record, the commit: e3e4e0006
https://github.com/haproxy/haproxy/blob/e3e4e00063f359d6113144bddff67d3557a9eedb/src/h1_htx.c#L262

Maybe this line should also be changed too.

Indeed ! I missed this one. Thanks !
Fixed now. Thanks !
The fixes are not backported yet to all versions. Thus I'm reopening the  issue. This help us to track pending bkackports.