ssl-min-ver SSLv3 does not work; min-ver still TLSv1.0 without force-sslv3
Hello,

This is a duplicate of #595, it was not backported to 1.8 yet.
Hmmm, I think the scope is larger than the linked issue suggests.  At least in severity; I'm unable to select SSLv3 on the bind line itself.  If I disable defaults entirely..  still no SSLv3.
Most likely your underlying OpenSSL library has been compiled without support for SSLv3.

Are you able to use SSLv3 with openssl command line?
when you run `haproxy -vv`, does it show sslv3 support ?
in your above output - it does.
if that could happen when openssl lib is built with sslv3 support, it looks like a bug
This is activated at build, it could be available in the .h during the build and then disabled in openssl.cnf for example.
But the fact that it worked with force-sslv3 shows that there is a problem, I'll take a look at the code.
Hmm, this is build time setting, not runtime?
> Hmm, this is build time setting, not runtime?

Some parts of this code are built depending on macros.

I think I found the problem with the help of @jmagnin.

With OpenSSL >= 1.1.0, there is an API which sets the minimum and maximum SSL version to use on a bind line, it was implemented as ssl-min-ver and ssl-max-ver in HAProxy. However, to be compatible with previous version of OpenSSL it is emulated by putting the right SSL options.

We can't reproduce the problem with standard OpenSSL versions, but it seems that Amazon patched the SSL_CTX_new() function with a SSL_OP_NO_SSLv3 option. Which explains the difference.


[0001-BUG-MINOR-ssl-fix-ssl-min-max-ver-with-openssl-1.1.0.patch.txt](https://github.com/haproxy/haproxy/files/4765852/0001-BUG-MINOR-ssl-fix-ssl-min-max-ver-with-openssl-1.1.0.patch.txt)

The attached patch should fix the issue.
I was able to test the patch on an amazon linux ami and can confim it fixes the issue.
I tossed in that line addition to the beginning of the CONF_TLSV loop in 1.8.25 and I've observed the same no-sslv3 behavior.  So it's either pebkac or my obvious lack of domain knowledge.

I have as well confirmed desired operation with 2.2-dev9.
OK it was pebkac for sure.  `make PREFIX=/usr install` cleaned all that up.  Pathing is hard.  Confirmed working in 1.8.25, thank you all very much.
@in4mer thanks for confirming, and thanks for reporting this problem. I will re open this issue and it will be closed when the fix is backported to all affected branches.
Ah thanks, sorry for the confusion.  Here's the 1.8.25 patch backport for posterity.

```diff --git a/src/ssl_sock.c b/src/ssl_sock.c
index fa37bc0..b6f5080 100644
--- a/src/ssl_sock.c
+++ b/src/ssl_sock.c
@@ -3846,7 +3846,11 @@ ssl_sock_initial_ctx(struct bind_conf *bind_conf)
        /* find min, max and holes */
        min = max = CONF_TLSV_NONE;
        hole = 0;
-       for (i = CONF_TLSV_MIN; i <= CONF_TLSV_MAX; i++)
+       for (i = CONF_TLSV_MIN; i <= CONF_TLSV_MAX; i++) {
+               /* clear every version flags in case SSL_CTX_new()
+                * returns an SSL_CTX with disabled versions */
+               SSL_CTX_clear_options(ctx, methodVersions[i].option);
+
                /*  version is in openssl && version not disable in configuration */
                if (methodVersions[i].option && !(flags & methodVersions[i].flag)) {
                        if (min) {
@@ -3867,6 +3871,7 @@ ssl_sock_initial_ctx(struct bind_conf *bind_conf)
                        if (min)
                                hole = i;
                }
+       }
        if (!min) {
                ha_alert("Proxy '%s': all SSL/TLS versions are disabled for bind '%s' at [%s:%d].\n",
                         bind_conf->frontend->id, bind_conf->arg, bind_conf->file, bind_conf->line);
```