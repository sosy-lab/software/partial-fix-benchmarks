diff --git a/include/tidy.h b/include/tidy.h
index 2f3dabd11..8a30d9fd0 100755
--- a/include/tidy.h
+++ b/include/tidy.h
@@ -631,6 +631,9 @@ TIDY_EXPORT Bool TIDY_CALL tidyInitSink( TidyOutputSink* sink,
 TIDY_EXPORT void TIDY_CALL tidyPutByte( TidyOutputSink* sink, uint byteValue );
 
 
+/****************
+   Errors
+****************/
 /** Callback to filter messages by diagnostic level:
 **  info, warning, etc.  Just set diagnostic output 
 **  handler to redirect all diagnostics output.  Return true
@@ -656,6 +659,18 @@ TIDY_EXPORT int TIDY_CALL     tidySetErrorBuffer( TidyDoc tdoc, TidyBuffer* errb
 /** Set error sink to given generic sink */
 TIDY_EXPORT int TIDY_CALL     tidySetErrorSink( TidyDoc tdoc, TidyOutputSink* sink );
 
+
+/****************
+   Printing
+****************/
+/** Callback to track the progress of the pretting printing process.
+**
+*/
+typedef void (TIDY_CALL *TidyPPProgress)( TidyDoc tdoc, uint line, uint col, uint destLine );
+
+TIDY_EXPORT Bool TIDY_CALL   tidySetPrettyPrinterCallback( TidyDoc tdoc,
+                                                  TidyPPProgress callback );
+
 /** @} end IO group */
 
 /* TODO: Catalog all messages for easy translation
diff --git a/src/pprint.c b/src/pprint.c
index 20900c7e3..bb7ecccba 100644
--- a/src/pprint.c
+++ b/src/pprint.c
@@ -312,6 +312,7 @@ void TY_(InitPrintBuf)( TidyDocImpl* doc )
     InitIndent( &doc->pprint.indent[0] );
     InitIndent( &doc->pprint.indent[1] );
     doc->pprint.allocator = doc->allocator;
+    doc->pprint.line = 0;
 }
 
 void TY_(FreePrintBuf)( TidyDocImpl* doc )
@@ -613,6 +614,7 @@ static void WrapLine( TidyDocImpl* doc )
         TY_(WriteChar)( '\\', doc->docOut );
 
     TY_(WriteChar)( '\n', doc->docOut );
+    pprint->line++;
     ResetLineAfterWrap( pprint );
 }
 
@@ -666,6 +668,7 @@ static void WrapAttrVal( TidyDocImpl* doc )
         TY_(WriteChar)( ' ', doc->docOut );
 
     TY_(WriteChar)( '\n', doc->docOut );
+    pprint->line++;
     ResetLineAfterWrap( pprint );
 }
 
@@ -686,7 +689,7 @@ static void PFlushLineImpl( TidyDocImpl* doc )
 
     for ( i = 0; i < pprint->linelen; ++i )
         TY_(WriteChar)( pprint->linebuf[i], doc->docOut );
-    
+
     if ( IsInString(pprint) )
         TY_(WriteChar)( '\\', doc->docOut );
     ResetLine( pprint );
@@ -701,6 +704,7 @@ void TY_(PFlushLine)( TidyDocImpl* doc, uint indent )
         PFlushLineImpl( doc );
 
     TY_(WriteChar)( '\n', doc->docOut );
+    pprint->line++;
     pprint->indent[ 0 ].spaces = indent;
 }
 
@@ -713,6 +717,7 @@ static void PCondFlushLine( TidyDocImpl* doc, uint indent )
          PFlushLineImpl( doc );
 
          TY_(WriteChar)( '\n', doc->docOut );
+         pprint->line++;
          pprint->indent[ 0 ].spaces = indent;
     }
 }
@@ -733,6 +738,7 @@ void TY_(PFlushLineSmart)( TidyDocImpl* doc, uint indent )
     /* Issue #228 - cfgBool( doc, TidyVertSpace ); */
     if(TidyAddVS) {
         TY_(WriteChar)( '\n', doc->docOut );
+        pprint->line++;
     }
 
     pprint->indent[ 0 ].spaces = indent;
@@ -749,6 +755,7 @@ static void PCondFlushLineSmart( TidyDocImpl* doc, uint indent )
          /* Issue #228 - cfgBool( doc, TidyVertSpace ); */
          if(TidyAddVS) {
             TY_(WriteChar)( '\n', doc->docOut );
+            pprint->line++;
          }
 
          pprint->indent[ 0 ].spaces = indent;
@@ -2115,6 +2122,11 @@ void TY_(PPrintTree)( TidyDocImpl* doc, uint mode, uint indent, Node *node )
     if ( node == NULL )
         return;
 
+    if (doc->progressCallback)
+    {
+        doc->progressCallback( tidyImplToDoc(doc), node->line, node->column, doc->pprint.line + 1 );
+    }
+
     if (node->type == TextNode)
     {
         PPrintText( doc, mode, indent, node );
@@ -2379,6 +2391,11 @@ void TY_(PPrintXMLTree)( TidyDocImpl* doc, uint mode, uint indent, Node *node )
     if (node == NULL)
         return;
 
+    if (doc->progressCallback)
+    {
+        doc->progressCallback( tidyImplToDoc(doc), node->line, node->column, doc->pprint.line + 1 );
+    }
+    
     if ( node->type == TextNode)
     {
         PPrintText( doc, mode, indent, node );
diff --git a/src/pprint.h b/src/pprint.h
index 4b36a2177..8d473a413 100644
--- a/src/pprint.h
+++ b/src/pprint.h
@@ -53,6 +53,7 @@ typedef struct _TidyPrintImpl
     uint lbufsize;
     uint linelen;
     uint wraphere;
+    uint line;
   
     uint ixInd;
     TidyIndent indent[2];  /* Two lines worth of indent state */
diff --git a/src/tidy-int.h b/src/tidy-int.h
index f4a4703d8..51bcf3bbc 100755
--- a/src/tidy-int.h
+++ b/src/tidy-int.h
@@ -59,6 +59,7 @@ struct _TidyDocImpl
     TidyReportFilter    mssgFilt;
     TidyReportFilter2   mssgFilt2;
     TidyOptCallback     pOptCallback;
+    TidyPPProgress      progressCallback;
 
     /* Parse + Repair Results */
     uint                optionErrors;
diff --git a/src/tidylib.c b/src/tidylib.c
index 0d7dc8837..851a752c0 100755
--- a/src/tidylib.c
+++ b/src/tidylib.c
@@ -752,6 +752,19 @@ int TIDY_CALL    tidySetErrorSink( TidyDoc tdoc, TidyOutputSink* sink )
     return -EINVAL;
 }
 
+/* Use TidyPPProgress to monitor the progress of the pretty printer.
+ */
+Bool TIDY_CALL        tidySetPrettyPrinterCallback(TidyDoc tdoc, TidyPPProgress callback)
+{
+    TidyDocImpl* impl = tidyDocToImpl( tdoc );
+    if ( impl )
+    {
+        impl->progressCallback = callback;
+        return yes;
+    }
+    return no;
+}
+
 
 /* Document info */
 int TIDY_CALL        tidyStatus( TidyDoc tdoc )
