	* elf.c (elf_add): Close descriptor if we use a debugfile.
	* btest.c (check_open_files): New static function.
	(main): Call check_open_files.
Copy changes from GCC:

2018-04-17  Ian Lance Taylor  <iant@golang.org>

	* backtrace.c (backtrace_full): When testing whether we can
	allocate memory, call mmap directly, and munmap the memory.

2018-04-04  Jakub Jelinek  <jakub@redhat.com>

	PR other/85161
	* elf.c (elf_zlib_fetch): Fix up predefined macro names in test for
	big endian, only use 32-bit loads if endianity macros are predefined
	and indicate big or little endian.

2018-02-15  Jakub Jelinek  <jakub@redhat.com>

	PR other/82368
	* elf.c (SHT_PROGBITS): Undefine and define.

2018-02-14  Jakub Jelinek  <jakub@redhat.com>

	PR other/82368
	* elf.c (EM_PPC64, EF_PPC64_ABI): Undefine and define.
	(struct elf_ppc64_opd_data): New type.
	(elf_initialize_syminfo): Add opd argument, handle symbols
	pointing into the PowerPC64 ELFv1 .opd section.
	(elf_add): Read .opd section on PowerPC64 ELFv1, pass pointer
	to structure with .opd data to elf_initialize_syminfo.

2018-01-19  Tony Reix  <tony.reix@atos.net>

	* xcoff.c (xcoff_incl_compare): New function.
	(xcoff_incl_search): New function.
	(xcoff_process_linenos): Use bsearch to find include file.
	(xcoff_initialize_fileline): Sort include file information.

Fixes #13
	* backtrace.c: Revert last change.  Don't call mmap
	directly.

Updates #13
