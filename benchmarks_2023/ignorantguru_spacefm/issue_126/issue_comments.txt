Can't Compile 0.7.9 - make: *** [all-recursive] Error 1
Sounds like this line needs to be added in the top section of main-window.c for slackware:

```
#include <glib-object.h>
```

I've made this change to the next branch.

should be fixed in 0.7.10

Nope still problem;

main-window.c: In function 'main_task_view_new':
main-window.c:4975:30: error: 'G_VALUE_INIT' undeclared (first use in this function)
main-window.c:4975:30: note: each undeclared identifier is reported only once for each function it appears in
make[1]: **\* [spacefm-main-window.o] Error 1
make[1]: Leaving directory `/tmp/sar/spacefm-0.7.10/src'
make: **\* [all-recursive] Error 1

Here's the config.log if needed;

http://pastebin.ca/2174776

THANKS

The G_VALUE_INIT macro requires glib >=2.30.  Perhaps that was the issue.  I have replaced it with an explicit value in the next branch.

Ok I changed it like this and it compiled;

 /\* wrap to multiple lines
                GValue val = { 0 };   // G_VALUE_INIT (glib>=2.30) caused to slackware issue ?
                g_value_init (&val, G_TYPE_CHAR);
                g_value_set_char (&val, 100);  // set to width of cell?
                g_object_set_property (G_OBJECT (renderer), "wrap-width", &val);
                g_value_unset (&val);
                */
                GValue val = { 0 };   // G_VALUE_INIT (glib>=2.30) caused to slackware issue ?
                g_value_init (&val, G_TYPE_CHAR);
                g_value_set_char (&val, PANGO_ELLIPSIZE_MIDDLE);
                g_object_set_property (G_OBJECT (renderer), "ellipsize", &val);
                g_value_unset (&val);

So I guess I'm good for now?

THANKS

I can not find the main-window.c file, Can you tell the file position?
very thanks

@shanghailangke:  If you open the source archive of spacefm, main-window.c is in the src/ subdirectory [here](https://github.com/IgnorantGuru/spacefm/blob/master/src/main-window.c).  To obtain a source archive (eg spacefm-0.8.6.tar.xz) go to [packages](https://github.com/IgnorantGuru/spacefm/tree/master/packages), click on a filename, and click 'View Raw'.
