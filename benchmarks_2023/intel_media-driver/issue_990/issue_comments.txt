VAConfigAttribPredictionDirection query failed on VMEPAK mode.
thanks reporting, will file PR to fix it soon
I checked the newest code in master, and find #1009 does not fix all problems on TGL. Though, the ICL can works well.

```
VAStatus MediaLibvaCapsG12::QuerySurfaceAttributes(
        VAConfigID configId,
        VASurfaceAttrib *attribList,
        uint32_t *numAttribs)
{
   .......

    if(entrypoint == VAEntrypointEncSliceLP)
    {
     .....

        else if (IsHevcProfile(profile))
        {
            attrib.type = (VAConfigAttribType) VAConfigAttribPredictionDirection;
            attrib.value = VA_PREDICTION_DIRECTION_PREVIOUS | VA_PREDICTION_DIRECTION_FUTURE | VA_PREDICTION_DIRECTION_BI_NOT_EMPTY;
            (*attribList)[attrib.type] = attrib.value;
        }
    }


Only in VDENC mode, this attribute is set, but in VMEPAK mode, this attribute is still unset.

And so far as I know, on TGL, the VMEPAK mode  also does not support P frame, so this still cause our pipeline hang:

gst-launch-1.0 -v -f videotestsrc num-buffers=100 ! capsfilter caps=video/x-raw,format=NV12,width=400,height=320 ! vaapih265enc ! video/x-h265,profile=main ! h265parse ! filesink location=out.265







I can not find the reopen option, @XinfengZhang please reopen it.
@HeJunyan , please double confirm it, the code you copied should be old , VME & VDEnc is same on TGL, support I B Lowdelay B
@HeJunyan , could you help to check whether #1021 fix the issue?
It works for me on my TGL, please help to merge it.