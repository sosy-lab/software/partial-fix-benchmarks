Merge pull request #270 from iPlug2/windows-app-hi-dpi

The APP shouldn't know about high-dpi - that's for the graphics layer…
fix #245 cairo quartz assertion
Revert "fix #245 cairo quartz assertion"

This reverts commit f2db196e2013a8507a5451ca06410d3b4bc41d1c.
