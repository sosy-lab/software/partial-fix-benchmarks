ping -I device_name device_ip_address (self) does not work
I guess cancelling socket option when it does not work is an option. See commit 4082ba9e780320e3c6e31a21c7d7ad164c229b5b.
Merged: cc44f4c5c2e72f7aa833658a6978d924ff9e059d
Hello,

I am encountering an issue similar to this using release s20190709, but with IPv6. I've confirmed that the packets are actually appearing on the loopback interface with tcpdump, and that specifying the device as e.g. ping -6 fe80::92e2:baff:fe08:3738%eth0 works, but ping -6 -I eth0 fe80::92e2:baff:fe08:3738 does not. Both ping options work for me on s20180629. I've also tried writing a patch similar to [cc44f4c](https://github.com/iputils/iputils/commit/cc44f4c5c2e72f7aa833658a6978d924ff9e059d), but it didn't affect the result.

Is there a known problem with this behavior for IPv6? I don't see a related open issue except maybe https://github.com/iputils/iputils/issues/198.
Hi,

I found it doesn't work when net.ipv4.ping_group_range sysctl is enabled. Since systemd 243, the ping_group_range sysctl is enabled:

root@qemux86-64:~# cat /usr/lib/sysctl.d/50-default.conf | grep "ping_group_range"
-net.ipv4.ping_group_range = 0 2147483647
root@qemux86-64:~# 
root@qemux86-64:~# cat /proc/sys/net/ipv4/ping_group_range 
0       2147483647
root@qemux86-64:~# 

Then the ping program will create ping (icmp) socket rather than raw socket. However, it doesn't work with -I dev option:
root@qemux86-64:~# 
root@qemux86-64:~# cat /proc/sys/net/ipv4/ping_group_range 
0       2147483647
root@qemux86-64:~# 
root@qemux86-64:~# ping -6 -c3 -I eth0 fe80::5054:ff:fe12:3402
ping: Warning: source address might be selected on device other than: eth0
PING fe80::5054:ff:fe12:3402(fe80::5054:ff:fe12:3402) from :: eth0: 56 data bytes

^C
--- fe80::5054:ff:fe12:3402 ping statistics ---
3 packets transmitted, 0 received, 100% packet loss, time 2028ms

root@qemux86-64:~# 

When I disabled the sysctl, it can work:
root@qemux86-64:~# sysctl -w "net.ipv4.ping_group_range=1 0"
net.ipv4.ping_group_range = 1 0
root@qemux86-64:~# 
root@qemux86-64:~# ping -6 -c3 -I eth0 fe80::5054:ff:fe12:3402
ping: Warning: source address might be selected on device other than: eth0
PING fe80::5054:ff:fe12:3402(fe80::5054:ff:fe12:3402) from :: eth0: 56 data bytes
64 bytes from fe80::5054:ff:fe12:3402%eth0: icmp_seq=1 ttl=64 time=0.035 ms
64 bytes from fe80::5054:ff:fe12:3402%eth0: icmp_seq=2 ttl=64 time=0.010 ms
64 bytes from fe80::5054:ff:fe12:3402%eth0: icmp_seq=3 ttl=64 time=0.010 ms

--- fe80::5054:ff:fe12:3402 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2061ms
rtt min/avg/max/mdev = 0.010/0.018/0.035/0.011 ms
root@qemux86-64:~# 

BTW kernel in v5.2 got related fix [19e4e768064a ipv4: Fix raw socket lookup for local traffic](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=19e4e768064a87b073a4b4c138b55db70e0cfb9f).
@ValZapod thanks! Yep, fix became a regression, how usual in kernel development.
I plan to have a look into the issue, if there is a way to fix ping with kernel without fix.
I don't see [these changes](https://kernelnewbies.org/Linux_5.5#Networking) as a problem:
* size 256 length is definitely not a problem (`char *p, *addr = strdup(optarg);` handling `-I` in https://github.com/iputils/iputils/blob/master/ping/ping.c#L388). BTW: I'd expect something like `getconf ARG_MAX`, which is 2097152 on my system, would fail, but in the reality is a bit more than half: 

```
$ ping  8.8.8.8 -I $(printf 'x%.0s' {1..131610})
bash: /bin/ping: Argument list too long

$ ping  8.8.8.8 -I $(printf 'x%.0s' {1..130610})
... xxxxxxxxxxxxxxxxxxxxxxxxxxxx: No such device
```
* What could cause troubles on interface being addressed by more names?

=> remember, Linux cares a lot on compatibility with user space.