gctest hangs sometimes with AO emulation on Linux
Latest build: https://travis-ci.org/github/ivmai/bdwgc/jobs/709584324
Source: master (43e1d537d)
Latest build: https://app.travis-ci.com/github/ivmai/bdwgc/jobs/532963020
Source: https://github.com/ivmai/bdwgc/commit/94a502e6d3adabe865c568110a97058b967228c6
Latest build: https://app.travis-ci.com/github/ivmai/bdwgc/jobs/538136367
Source: https://github.com/ivmai/bdwgc/commit/b2f34974abe6ede3ef91112921d3036801bf331d (master)
Source: master (b41a290)
Build: https://app.travis-ci.com/github/ivmai/bdwgc/jobs/598169279
How to: ./configure --without-libatomic-ops --enable-gc-assertions --enable-cplusplus --enable-static && make -j check CONF_CFLAGS="-D _FORTIFY_SOURCE=2 -D AO_USE_PTHREAD_DEFS"

Source: master (132eb0b)
Build: https://app.travis-ci.com/github/ivmai/bdwgc/jobs/598851924