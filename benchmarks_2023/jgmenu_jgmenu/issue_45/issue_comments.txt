$XDG_CONFIG_DIRS can be a list of directories
Thanks. Good point. I will put that right.
I think that's fixed it. I've done some testing as I've gone, but am grateful for any feedback on how it behaves on your system.
NixOS looks interesting btw.
@johanmalm Thanks for looking at it.

It still does not work. It seems that some shell scripts still uses `$XDG_CONFIG_DIRS` as a single directory, instead of a list of possibly several directories.

After adding `-x` to the first line of `jgmenu_run` and `jgmenu_xdg.sh` I can trace some shell commands.

Checking the value of `$XDG_CONFIG_DIRS`:
```
✓ 0 [170526] 10:10:30 romildo@jrm /alt/projects/jgmenu (master)* 0
$ echo $XDG_CONFIG_DIRS
/etc/per-user-pkgs/romildo/etc/xdg:/home/romildo/.nix-profile/etc/xdg:/nix/var/nix/profiles/default/etc/xdg:/run/current-system/sw/etc/xdg
```

Running `jgmenu_run xdg` fails:
```
✓ 0 [170526] 10:10:41 romildo@jrm /alt/projects/jgmenu (master)* 0
$ PATH=.:$PATH ./jgmenu_run xdg
+ : /home/romildo/src/jgmenu
+ export PATH=/home/romildo/src/jgmenu:.:/nix/store/j77whxr5hw0cm4ibzq92p41xxl9ldgii-pkg-config-0.29.2/bin:/nix/store/avkk0fxind45lzhc4khf6w3kzkvqflp5-libxml2-2.9.5-dev/bin:/nix/store/v6d01121v0d89kgfwrbnx5072adrbjsn-libxml2-2.9.5-bin/bin:/nix/store/50msiz3ias6c554pcn5jqdnz1wrailhg-cairo-1.14.10-dev/bin:/nix/store/g9r5acpb8z02rl2hrdc48ydn8wdkzk6z-freetype-2.7.1-dev/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/n9nvbbalvrinp18ainacfdp4r6jl9zd3-libpng-apng-1.6.34-dev/bin:/nix/store/k0hkkr6il7h06ls98rqh0kal7gv1vm5n-fontconfig-2.12.1-bin/bin:/nix/store/b5kfr19m10awkxi142xazp34pn3hhl4s-expat-2.2.5-dev/bin:/nix/store/5bglj2xcnyh54c6jp13mcyrdg2cgc0nq-glib-2.54.1-dev/bin:/nix/store/s22z47andfnqb2yra50jpd83pyi6a1i0-libdrm-2.4.84-bin/bin:/nix/store/fy80flz98lzidwyk6ls866f621qbaq46-harfbuzz-1.5.1-dev/bin:/nix/store/6waj6v242hrsdsr6i3l2ax3p61qwqka3-graphite2-1.3.6/bin:/nix/store/a2nqdmbckdw67jbbnn9vqkdmb94yskii-pango-1.40.12-bin/bin:/nix/store/argz59mn588mxh8dcflgks0hq39xizwc-gdk-pixbuf-2.36.7-dev/bin:/nix/store/slp284nc7w7lz88ik173xdg89q1y2ybv-libjpeg-turbo-1.5.2-bin/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/nix/store/ybf05ljgjvayzhbdwpk5f4jf5ypl79x6-libtiff-4.0.8-bin/bin:/nix/store/jhszcq2jq3mbzv8ld1pmaf7kvyhhpfnx-jasper-2.0.14-bin/bin:/nix/store/79vqhc9q70irzl8911adm8scng9ckk8l-gdk-pixbuf-2.36.7/bin:/nix/store/hlv4dn9bgj9x84rilmr78jzw7fsp7295-librsvg-2.40.19/bin:/nix/store/yjh21h38fmw3i4h2b5r557p4ggbvrd0v-gcc-wrapper-6.4.0/bin:/nix/store/dpza5shcrnf4m8rjsbxb34hf1vnppbbp-gcc-6.4.0/bin:/nix/store/yha4q7zgm6y0ci0b57a8hbmzwyvv1w44-binutils-2.28.1/bin:/nix/store/7vqbmwjv8acb3afqvd9bip13xh6f12ps-glibc-2.26-75-bin/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/yhlvb9aiya369qn2r5jz9k1q6qqg5m3w-patchelf-0.9/bin:/nix/store/azzl0bvmzsqc999mg9rgk37pr64wfdz0-paxctl-0.9/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/rix7drclds895majp75f4imqh09w9bvb-findutils-4.6.0/bin:/nix/store/jxa512mq3hyrbqwdhlkc7q1xxh538x4z-diffutils-3.6/bin:/nix/store/arcyymlgk8nh0flbb4hb2m287byfswkr-gnused-4.4/bin:/nix/store/jnflx7xcqjkn7lkkr8hiv0y284fdz0qf-gnugrep-3.1/bin:/nix/store/1yqmx3rl2hmz78992i9k8l4y88r42gm1-gawk-4.2.0/bin:/nix/store/7lx6bha6l22vi5h9c31nri1p7rqs7v1d-gnutar-1.29/bin:/nix/store/smx6lirinf04qkm5i9hxrfg9psl7jlsz-gzip-1.8/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/caq1vyjkcgfrnxkynizn1bxw1vwazl0n-gnumake-4.2.1/bin:/nix/store/4ada72n7785wwazv42fhsnxjvilaa3aj-bash-4.4-p12/bin:/nix/store/v06ybx67cflsmcgjhs260wzclglrjg0p-patch-2.7.5/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/home/romildo/bin:/run/wrappers/bin:/etc/per-user-pkgs/romildo/bin:/home/romildo/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin
+ PATH=/home/romildo/src/jgmenu:.:/nix/store/j77whxr5hw0cm4ibzq92p41xxl9ldgii-pkg-config-0.29.2/bin:/nix/store/avkk0fxind45lzhc4khf6w3kzkvqflp5-libxml2-2.9.5-dev/bin:/nix/store/v6d01121v0d89kgfwrbnx5072adrbjsn-libxml2-2.9.5-bin/bin:/nix/store/50msiz3ias6c554pcn5jqdnz1wrailhg-cairo-1.14.10-dev/bin:/nix/store/g9r5acpb8z02rl2hrdc48ydn8wdkzk6z-freetype-2.7.1-dev/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/n9nvbbalvrinp18ainacfdp4r6jl9zd3-libpng-apng-1.6.34-dev/bin:/nix/store/k0hkkr6il7h06ls98rqh0kal7gv1vm5n-fontconfig-2.12.1-bin/bin:/nix/store/b5kfr19m10awkxi142xazp34pn3hhl4s-expat-2.2.5-dev/bin:/nix/store/5bglj2xcnyh54c6jp13mcyrdg2cgc0nq-glib-2.54.1-dev/bin:/nix/store/s22z47andfnqb2yra50jpd83pyi6a1i0-libdrm-2.4.84-bin/bin:/nix/store/fy80flz98lzidwyk6ls866f621qbaq46-harfbuzz-1.5.1-dev/bin:/nix/store/6waj6v242hrsdsr6i3l2ax3p61qwqka3-graphite2-1.3.6/bin:/nix/store/a2nqdmbckdw67jbbnn9vqkdmb94yskii-pango-1.40.12-bin/bin:/nix/store/argz59mn588mxh8dcflgks0hq39xizwc-gdk-pixbuf-2.36.7-dev/bin:/nix/store/slp284nc7w7lz88ik173xdg89q1y2ybv-libjpeg-turbo-1.5.2-bin/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/nix/store/ybf05ljgjvayzhbdwpk5f4jf5ypl79x6-libtiff-4.0.8-bin/bin:/nix/store/jhszcq2jq3mbzv8ld1pmaf7kvyhhpfnx-jasper-2.0.14-bin/bin:/nix/store/79vqhc9q70irzl8911adm8scng9ckk8l-gdk-pixbuf-2.36.7/bin:/nix/store/hlv4dn9bgj9x84rilmr78jzw7fsp7295-librsvg-2.40.19/bin:/nix/store/yjh21h38fmw3i4h2b5r557p4ggbvrd0v-gcc-wrapper-6.4.0/bin:/nix/store/dpza5shcrnf4m8rjsbxb34hf1vnppbbp-gcc-6.4.0/bin:/nix/store/yha4q7zgm6y0ci0b57a8hbmzwyvv1w44-binutils-2.28.1/bin:/nix/store/7vqbmwjv8acb3afqvd9bip13xh6f12ps-glibc-2.26-75-bin/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/yhlvb9aiya369qn2r5jz9k1q6qqg5m3w-patchelf-0.9/bin:/nix/store/azzl0bvmzsqc999mg9rgk37pr64wfdz0-paxctl-0.9/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/rix7drclds895majp75f4imqh09w9bvb-findutils-4.6.0/bin:/nix/store/jxa512mq3hyrbqwdhlkc7q1xxh538x4z-diffutils-3.6/bin:/nix/store/arcyymlgk8nh0flbb4hb2m287byfswkr-gnused-4.4/bin:/nix/store/jnflx7xcqjkn7lkkr8hiv0y284fdz0qf-gnugrep-3.1/bin:/nix/store/1yqmx3rl2hmz78992i9k8l4y88r42gm1-gawk-4.2.0/bin:/nix/store/7lx6bha6l22vi5h9c31nri1p7rqs7v1d-gnutar-1.29/bin:/nix/store/smx6lirinf04qkm5i9hxrfg9psl7jlsz-gzip-1.8/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/caq1vyjkcgfrnxkynizn1bxw1vwazl0n-gnumake-4.2.1/bin:/nix/store/4ada72n7785wwazv42fhsnxjvilaa3aj-bash-4.4-p12/bin:/nix/store/v06ybx67cflsmcgjhs260wzclglrjg0p-patch-2.7.5/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/home/romildo/bin:/run/wrappers/bin:/etc/per-user-pkgs/romildo/bin:/home/romildo/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin
+ JGMENU_LOCKFILE=/home/romildo/.jgmenu-lockfile
+ test 1 -lt 1
+ test xdg = --help
+ test xdg = --exec-path
+ cmd=xdg
+ cmds='jgmenu-xdg jgmenu-xdg.sh jgmenu-xdg.py'
+ shift
+ for c in ${cmds}
+ type jgmenu-xdg
+ for c in ${cmds}
+ type jgmenu-xdg.sh
+ exec jgmenu-xdg.sh
+ test -z /etc/per-user-pkgs/romildo/etc/xdg:/home/romildo/.nix-profile/etc/xdg:/nix/var/nix/profiles/default/etc/xdg:/run/current-system/sw/etc/xdg
+ config_dirs=/etc/per-user-pkgs/romildo/etc/xdg:/home/romildo/.nix-profile/etc/xdg:/nix/var/nix/profiles/default/etc/xdg:/run/current-system/sw/etc/xdg
+ test -z gnome-
+ file_list=/etc/per-user-pkgs/romildo/etc/xdg:/home/romildo/.nix-profile/etc/xdg:/nix/var/nix/profiles/default/etc/xdg:/run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ menu_file=
+ for f in ${file_list}
+ test -e /etc/per-user-pkgs/romildo/etc/xdg:/home/romildo/.nix-profile/etc/xdg:/nix/var/nix/profiles/default/etc/xdg:/run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ test -z
+ printf '%s\n' 'fatal: cannot find .menu file'
fatal: cannot find .menu file
+ exit 1
```

Running `jgmenu_run xdg` with `$XDG_CONFIG_DIRS` set to a single directory succeeds:
```
✗ 1 [170527] 10:11:52 romildo@jrm /alt/projects/jgmenu (master)* 0
$ PATH=.:$PATH XDG_CONFIG_DIRS=/run/current-system/sw/etc/xdg ./jgmenu_run xdg
+ : /home/romildo/src/jgmenu
+ export PATH=/home/romildo/src/jgmenu:.:/nix/store/j77whxr5hw0cm4ibzq92p41xxl9ldgii-pkg-config-0.29.2/bin:/nix/store/avkk0fxind45lzhc4khf6w3kzkvqflp5-libxml2-2.9.5-dev/bin:/nix/store/v6d01121v0d89kgfwrbnx5072adrbjsn-libxml2-2.9.5-bin/bin:/nix/store/50msiz3ias6c554pcn5jqdnz1wrailhg-cairo-1.14.10-dev/bin:/nix/store/g9r5acpb8z02rl2hrdc48ydn8wdkzk6z-freetype-2.7.1-dev/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/n9nvbbalvrinp18ainacfdp4r6jl9zd3-libpng-apng-1.6.34-dev/bin:/nix/store/k0hkkr6il7h06ls98rqh0kal7gv1vm5n-fontconfig-2.12.1-bin/bin:/nix/store/b5kfr19m10awkxi142xazp34pn3hhl4s-expat-2.2.5-dev/bin:/nix/store/5bglj2xcnyh54c6jp13mcyrdg2cgc0nq-glib-2.54.1-dev/bin:/nix/store/s22z47andfnqb2yra50jpd83pyi6a1i0-libdrm-2.4.84-bin/bin:/nix/store/fy80flz98lzidwyk6ls866f621qbaq46-harfbuzz-1.5.1-dev/bin:/nix/store/6waj6v242hrsdsr6i3l2ax3p61qwqka3-graphite2-1.3.6/bin:/nix/store/a2nqdmbckdw67jbbnn9vqkdmb94yskii-pango-1.40.12-bin/bin:/nix/store/argz59mn588mxh8dcflgks0hq39xizwc-gdk-pixbuf-2.36.7-dev/bin:/nix/store/slp284nc7w7lz88ik173xdg89q1y2ybv-libjpeg-turbo-1.5.2-bin/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/nix/store/ybf05ljgjvayzhbdwpk5f4jf5ypl79x6-libtiff-4.0.8-bin/bin:/nix/store/jhszcq2jq3mbzv8ld1pmaf7kvyhhpfnx-jasper-2.0.14-bin/bin:/nix/store/79vqhc9q70irzl8911adm8scng9ckk8l-gdk-pixbuf-2.36.7/bin:/nix/store/hlv4dn9bgj9x84rilmr78jzw7fsp7295-librsvg-2.40.19/bin:/nix/store/yjh21h38fmw3i4h2b5r557p4ggbvrd0v-gcc-wrapper-6.4.0/bin:/nix/store/dpza5shcrnf4m8rjsbxb34hf1vnppbbp-gcc-6.4.0/bin:/nix/store/yha4q7zgm6y0ci0b57a8hbmzwyvv1w44-binutils-2.28.1/bin:/nix/store/7vqbmwjv8acb3afqvd9bip13xh6f12ps-glibc-2.26-75-bin/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/yhlvb9aiya369qn2r5jz9k1q6qqg5m3w-patchelf-0.9/bin:/nix/store/azzl0bvmzsqc999mg9rgk37pr64wfdz0-paxctl-0.9/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/rix7drclds895majp75f4imqh09w9bvb-findutils-4.6.0/bin:/nix/store/jxa512mq3hyrbqwdhlkc7q1xxh538x4z-diffutils-3.6/bin:/nix/store/arcyymlgk8nh0flbb4hb2m287byfswkr-gnused-4.4/bin:/nix/store/jnflx7xcqjkn7lkkr8hiv0y284fdz0qf-gnugrep-3.1/bin:/nix/store/1yqmx3rl2hmz78992i9k8l4y88r42gm1-gawk-4.2.0/bin:/nix/store/7lx6bha6l22vi5h9c31nri1p7rqs7v1d-gnutar-1.29/bin:/nix/store/smx6lirinf04qkm5i9hxrfg9psl7jlsz-gzip-1.8/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/caq1vyjkcgfrnxkynizn1bxw1vwazl0n-gnumake-4.2.1/bin:/nix/store/4ada72n7785wwazv42fhsnxjvilaa3aj-bash-4.4-p12/bin:/nix/store/v06ybx67cflsmcgjhs260wzclglrjg0p-patch-2.7.5/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/home/romildo/bin:/run/wrappers/bin:/etc/per-user-pkgs/romildo/bin:/home/romildo/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin
+ PATH=/home/romildo/src/jgmenu:.:/nix/store/j77whxr5hw0cm4ibzq92p41xxl9ldgii-pkg-config-0.29.2/bin:/nix/store/avkk0fxind45lzhc4khf6w3kzkvqflp5-libxml2-2.9.5-dev/bin:/nix/store/v6d01121v0d89kgfwrbnx5072adrbjsn-libxml2-2.9.5-bin/bin:/nix/store/50msiz3ias6c554pcn5jqdnz1wrailhg-cairo-1.14.10-dev/bin:/nix/store/g9r5acpb8z02rl2hrdc48ydn8wdkzk6z-freetype-2.7.1-dev/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/n9nvbbalvrinp18ainacfdp4r6jl9zd3-libpng-apng-1.6.34-dev/bin:/nix/store/k0hkkr6il7h06ls98rqh0kal7gv1vm5n-fontconfig-2.12.1-bin/bin:/nix/store/b5kfr19m10awkxi142xazp34pn3hhl4s-expat-2.2.5-dev/bin:/nix/store/5bglj2xcnyh54c6jp13mcyrdg2cgc0nq-glib-2.54.1-dev/bin:/nix/store/s22z47andfnqb2yra50jpd83pyi6a1i0-libdrm-2.4.84-bin/bin:/nix/store/fy80flz98lzidwyk6ls866f621qbaq46-harfbuzz-1.5.1-dev/bin:/nix/store/6waj6v242hrsdsr6i3l2ax3p61qwqka3-graphite2-1.3.6/bin:/nix/store/a2nqdmbckdw67jbbnn9vqkdmb94yskii-pango-1.40.12-bin/bin:/nix/store/argz59mn588mxh8dcflgks0hq39xizwc-gdk-pixbuf-2.36.7-dev/bin:/nix/store/slp284nc7w7lz88ik173xdg89q1y2ybv-libjpeg-turbo-1.5.2-bin/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/nix/store/ybf05ljgjvayzhbdwpk5f4jf5ypl79x6-libtiff-4.0.8-bin/bin:/nix/store/jhszcq2jq3mbzv8ld1pmaf7kvyhhpfnx-jasper-2.0.14-bin/bin:/nix/store/79vqhc9q70irzl8911adm8scng9ckk8l-gdk-pixbuf-2.36.7/bin:/nix/store/hlv4dn9bgj9x84rilmr78jzw7fsp7295-librsvg-2.40.19/bin:/nix/store/yjh21h38fmw3i4h2b5r557p4ggbvrd0v-gcc-wrapper-6.4.0/bin:/nix/store/dpza5shcrnf4m8rjsbxb34hf1vnppbbp-gcc-6.4.0/bin:/nix/store/yha4q7zgm6y0ci0b57a8hbmzwyvv1w44-binutils-2.28.1/bin:/nix/store/7vqbmwjv8acb3afqvd9bip13xh6f12ps-glibc-2.26-75-bin/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/yhlvb9aiya369qn2r5jz9k1q6qqg5m3w-patchelf-0.9/bin:/nix/store/azzl0bvmzsqc999mg9rgk37pr64wfdz0-paxctl-0.9/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/rix7drclds895majp75f4imqh09w9bvb-findutils-4.6.0/bin:/nix/store/jxa512mq3hyrbqwdhlkc7q1xxh538x4z-diffutils-3.6/bin:/nix/store/arcyymlgk8nh0flbb4hb2m287byfswkr-gnused-4.4/bin:/nix/store/jnflx7xcqjkn7lkkr8hiv0y284fdz0qf-gnugrep-3.1/bin:/nix/store/1yqmx3rl2hmz78992i9k8l4y88r42gm1-gawk-4.2.0/bin:/nix/store/7lx6bha6l22vi5h9c31nri1p7rqs7v1d-gnutar-1.29/bin:/nix/store/smx6lirinf04qkm5i9hxrfg9psl7jlsz-gzip-1.8/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/caq1vyjkcgfrnxkynizn1bxw1vwazl0n-gnumake-4.2.1/bin:/nix/store/4ada72n7785wwazv42fhsnxjvilaa3aj-bash-4.4-p12/bin:/nix/store/v06ybx67cflsmcgjhs260wzclglrjg0p-patch-2.7.5/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/home/romildo/bin:/run/wrappers/bin:/etc/per-user-pkgs/romildo/bin:/home/romildo/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin
+ JGMENU_LOCKFILE=/home/romildo/.jgmenu-lockfile
+ test 1 -lt 1
+ test xdg = --help
+ test xdg = --exec-path
+ cmd=xdg
+ cmds='jgmenu-xdg jgmenu-xdg.sh jgmenu-xdg.py'
+ shift
+ for c in ${cmds}
+ type jgmenu-xdg
+ for c in ${cmds}
+ type jgmenu-xdg.sh
+ exec jgmenu-xdg.sh
+ test -z /run/current-system/sw/etc/xdg
+ config_dirs=/run/current-system/sw/etc/xdg
+ test -z gnome-
+ file_list=/run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ menu_file=
+ for f in ${file_list}
+ test -e /run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ menu_file=/run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ break
+ test -z /run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ jgmenu_run parse-xdg /run/current-system/sw/etc/xdg/menus/gnome-applications.menu
+ jgmenu
+ : /home/romildo/src/jgmenu
+ export PATH=/home/romildo/src/jgmenu:/home/romildo/src/jgmenu:.:/nix/store/j77whxr5hw0cm4ibzq92p41xxl9ldgii-pkg-config-0.29.2/bin:/nix/store/avkk0fxind45lzhc4khf6w3kzkvqflp5-libxml2-2.9.5-dev/bin:/nix/store/v6d01121v0d89kgfwrbnx5072adrbjsn-libxml2-2.9.5-bin/bin:/nix/store/50msiz3ias6c554pcn5jqdnz1wrailhg-cairo-1.14.10-dev/bin:/nix/store/g9r5acpb8z02rl2hrdc48ydn8wdkzk6z-freetype-2.7.1-dev/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/n9nvbbalvrinp18ainacfdp4r6jl9zd3-libpng-apng-1.6.34-dev/bin:/nix/store/k0hkkr6il7h06ls98rqh0kal7gv1vm5n-fontconfig-2.12.1-bin/bin:/nix/store/b5kfr19m10awkxi142xazp34pn3hhl4s-expat-2.2.5-dev/bin:/nix/store/5bglj2xcnyh54c6jp13mcyrdg2cgc0nq-glib-2.54.1-dev/bin:/nix/store/s22z47andfnqb2yra50jpd83pyi6a1i0-libdrm-2.4.84-bin/bin:/nix/store/fy80flz98lzidwyk6ls866f621qbaq46-harfbuzz-1.5.1-dev/bin:/nix/store/6waj6v242hrsdsr6i3l2ax3p61qwqka3-graphite2-1.3.6/bin:/nix/store/a2nqdmbckdw67jbbnn9vqkdmb94yskii-pango-1.40.12-bin/bin:/nix/store/argz59mn588mxh8dcflgks0hq39xizwc-gdk-pixbuf-2.36.7-dev/bin:/nix/store/slp284nc7w7lz88ik173xdg89q1y2ybv-libjpeg-turbo-1.5.2-bin/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/nix/store/ybf05ljgjvayzhbdwpk5f4jf5ypl79x6-libtiff-4.0.8-bin/bin:/nix/store/jhszcq2jq3mbzv8ld1pmaf7kvyhhpfnx-jasper-2.0.14-bin/bin:/nix/store/79vqhc9q70irzl8911adm8scng9ckk8l-gdk-pixbuf-2.36.7/bin:/nix/store/hlv4dn9bgj9x84rilmr78jzw7fsp7295-librsvg-2.40.19/bin:/nix/store/yjh21h38fmw3i4h2b5r557p4ggbvrd0v-gcc-wrapper-6.4.0/bin:/nix/store/dpza5shcrnf4m8rjsbxb34hf1vnppbbp-gcc-6.4.0/bin:/nix/store/yha4q7zgm6y0ci0b57a8hbmzwyvv1w44-binutils-2.28.1/bin:/nix/store/7vqbmwjv8acb3afqvd9bip13xh6f12ps-glibc-2.26-75-bin/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/yhlvb9aiya369qn2r5jz9k1q6qqg5m3w-patchelf-0.9/bin:/nix/store/azzl0bvmzsqc999mg9rgk37pr64wfdz0-paxctl-0.9/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/rix7drclds895majp75f4imqh09w9bvb-findutils-4.6.0/bin:/nix/store/jxa512mq3hyrbqwdhlkc7q1xxh538x4z-diffutils-3.6/bin:/nix/store/arcyymlgk8nh0flbb4hb2m287byfswkr-gnused-4.4/bin:/nix/store/jnflx7xcqjkn7lkkr8hiv0y284fdz0qf-gnugrep-3.1/bin:/nix/store/1yqmx3rl2hmz78992i9k8l4y88r42gm1-gawk-4.2.0/bin:/nix/store/7lx6bha6l22vi5h9c31nri1p7rqs7v1d-gnutar-1.29/bin:/nix/store/smx6lirinf04qkm5i9hxrfg9psl7jlsz-gzip-1.8/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/caq1vyjkcgfrnxkynizn1bxw1vwazl0n-gnumake-4.2.1/bin:/nix/store/4ada72n7785wwazv42fhsnxjvilaa3aj-bash-4.4-p12/bin:/nix/store/v06ybx67cflsmcgjhs260wzclglrjg0p-patch-2.7.5/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/home/romildo/bin:/run/wrappers/bin:/etc/per-user-pkgs/romildo/bin:/home/romildo/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin
+ PATH=/home/romildo/src/jgmenu:/home/romildo/src/jgmenu:.:/nix/store/j77whxr5hw0cm4ibzq92p41xxl9ldgii-pkg-config-0.29.2/bin:/nix/store/avkk0fxind45lzhc4khf6w3kzkvqflp5-libxml2-2.9.5-dev/bin:/nix/store/v6d01121v0d89kgfwrbnx5072adrbjsn-libxml2-2.9.5-bin/bin:/nix/store/50msiz3ias6c554pcn5jqdnz1wrailhg-cairo-1.14.10-dev/bin:/nix/store/g9r5acpb8z02rl2hrdc48ydn8wdkzk6z-freetype-2.7.1-dev/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/n9nvbbalvrinp18ainacfdp4r6jl9zd3-libpng-apng-1.6.34-dev/bin:/nix/store/k0hkkr6il7h06ls98rqh0kal7gv1vm5n-fontconfig-2.12.1-bin/bin:/nix/store/b5kfr19m10awkxi142xazp34pn3hhl4s-expat-2.2.5-dev/bin:/nix/store/5bglj2xcnyh54c6jp13mcyrdg2cgc0nq-glib-2.54.1-dev/bin:/nix/store/s22z47andfnqb2yra50jpd83pyi6a1i0-libdrm-2.4.84-bin/bin:/nix/store/fy80flz98lzidwyk6ls866f621qbaq46-harfbuzz-1.5.1-dev/bin:/nix/store/6waj6v242hrsdsr6i3l2ax3p61qwqka3-graphite2-1.3.6/bin:/nix/store/a2nqdmbckdw67jbbnn9vqkdmb94yskii-pango-1.40.12-bin/bin:/nix/store/argz59mn588mxh8dcflgks0hq39xizwc-gdk-pixbuf-2.36.7-dev/bin:/nix/store/slp284nc7w7lz88ik173xdg89q1y2ybv-libjpeg-turbo-1.5.2-bin/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/nix/store/ybf05ljgjvayzhbdwpk5f4jf5ypl79x6-libtiff-4.0.8-bin/bin:/nix/store/jhszcq2jq3mbzv8ld1pmaf7kvyhhpfnx-jasper-2.0.14-bin/bin:/nix/store/79vqhc9q70irzl8911adm8scng9ckk8l-gdk-pixbuf-2.36.7/bin:/nix/store/hlv4dn9bgj9x84rilmr78jzw7fsp7295-librsvg-2.40.19/bin:/nix/store/yjh21h38fmw3i4h2b5r557p4ggbvrd0v-gcc-wrapper-6.4.0/bin:/nix/store/dpza5shcrnf4m8rjsbxb34hf1vnppbbp-gcc-6.4.0/bin:/nix/store/yha4q7zgm6y0ci0b57a8hbmzwyvv1w44-binutils-2.28.1/bin:/nix/store/7vqbmwjv8acb3afqvd9bip13xh6f12ps-glibc-2.26-75-bin/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/yhlvb9aiya369qn2r5jz9k1q6qqg5m3w-patchelf-0.9/bin:/nix/store/azzl0bvmzsqc999mg9rgk37pr64wfdz0-paxctl-0.9/bin:/nix/store/0abay33zgwqgchdzzq6h88ac4963smwf-coreutils-8.28/bin:/nix/store/rix7drclds895majp75f4imqh09w9bvb-findutils-4.6.0/bin:/nix/store/jxa512mq3hyrbqwdhlkc7q1xxh538x4z-diffutils-3.6/bin:/nix/store/arcyymlgk8nh0flbb4hb2m287byfswkr-gnused-4.4/bin:/nix/store/jnflx7xcqjkn7lkkr8hiv0y284fdz0qf-gnugrep-3.1/bin:/nix/store/1yqmx3rl2hmz78992i9k8l4y88r42gm1-gawk-4.2.0/bin:/nix/store/7lx6bha6l22vi5h9c31nri1p7rqs7v1d-gnutar-1.29/bin:/nix/store/smx6lirinf04qkm5i9hxrfg9psl7jlsz-gzip-1.8/bin:/nix/store/fw1ixc551hm7mvpfxcmlzr6y1x3ck2hv-bzip2-1.0.6.0.1-bin/bin:/nix/store/caq1vyjkcgfrnxkynizn1bxw1vwazl0n-gnumake-4.2.1/bin:/nix/store/4ada72n7785wwazv42fhsnxjvilaa3aj-bash-4.4-p12/bin:/nix/store/v06ybx67cflsmcgjhs260wzclglrjg0p-patch-2.7.5/bin:/nix/store/r556bjkg62fh4mxpfqpa9rpjcvbylg6z-xz-5.2.3-bin/bin:/home/romildo/bin:/run/wrappers/bin:/etc/per-user-pkgs/romildo/bin:/home/romildo/.nix-profile/bin:/nix/var/nix/profiles/default/bin:/run/current-system/sw/bin
+ JGMENU_LOCKFILE=/home/romildo/.jgmenu-lockfile
+ test 2 -lt 1
+ test parse-xdg = --help
+ test parse-xdg = --exec-path
+ cmd=parse-xdg
+ cmds='jgmenu-parse-xdg jgmenu-parse-xdg.sh jgmenu-parse-xdg.py'
+ shift
+ for c in ${cmds}
+ type jgmenu-parse-xdg
+ exec jgmenu-parse-xdg /run/current-system/sw/etc/xdg/menus/gnome-applications.menu
info: parsing tint2 config file '/home/romildo/.config/tint2/tint2rc'
info: got font from xsettings
info: set font to 'Sans 14'
info: got icon theme from xsettings
info: set icon theme to 'Papirus'
warning: cannot find icon /home/romildo/jd2/.install4j/JDownloader2.png
^C
info: caught SIGTERM or SIGINT
info: cleaning up...
+ exit 0
```
Sorry. I should have been a bit more thorough last night.
We could do a quick fix in jgmenu-xdg.sh along the lines of `IFS=:
for s in $XDG_...` 
However, I'm more inclined to re-write it in C and put it in jgmenu-parse-xdg.c
I think it would be tidier in the long run.

The jgmenu-xdg.sh wrapper is a relic from the really early versions of jgmenu before it became a long-running application. I intend to overhaul the CLI before v1.0 and will probably get rid of jgmenu-xdg.sh at that point.

Do say if you seek a quick implementation (fix). Otherwise I'll work on it at my own pace.
`jgmenu` has been added to the NixOS package collection (https://github.com/NixOS/nixpkgs/pull/31669). Because of that I think a quick fix in the script is welcome. Yet I think re-writing it in C is very reasonable.

I am not using it very much yet. I have been just trying it, as it has been mentioned in one of the latest releases of tint2.
I have re-written it in C.
Again, would appreciate if you were able to test it on your system.
I have tried a few combinations of
`jgmenu_run xdg`
and
`jgmenu --csv-cmd="jgmenu_run parse-xdg"`
with various XDG_CONFIG_{DIRS,HOME} values such as "a:bb:c:$HOME/my_special_menu_file:d:e"
Please say if you wish me to up-version.
It works now. Thanks.
![screen](https://user-images.githubusercontent.com/1217934/33226454-d634a672-d16c-11e7-84ef-d1d9e4f9cb74.png)

Some items appear repeatedly many times. But this also happens with pmenu. Later on I will take a closer look at it.

I new release is welcome.
I've released v0.7.5

I'll close this issue, but would be very grateful for any help in tracking down the cause of the multiple entries (separate issue?)

This is what it looks like for me with `jgmenu_run xdg`

![2017-11-25-164726_1024x600_scrot](https://user-images.githubusercontent.com/6955353/33234330-d9c6b554-d21c-11e7-8876-7b859af2fdd5.png)
