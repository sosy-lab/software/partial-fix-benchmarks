chore(vscode): add large file test
feat(tool): introduce blurring capability

The blur algorithm is largely inspired from Kristian Høgsberg & Chris
Wilson in [this file](https://www.cairographics.org/cookbook/blur.c/)

Closes #17
feat(tool): introduce blurring capability

The blur algorithm is largely inspired from Kristian Høgsberg & Chris
Wilson in [this file](https://www.cairographics.org/cookbook/blur.c/)

Closes #17
feat(tool): introduce blurring capability

The blur algorithm is largely inspired from Kristian Høgsberg & Chris
Wilson in [this file](https://www.cairographics.org/cookbook/blur.c/)

Closes #17
feat(tool): introduce blurring capability

The blur algorithm is largely inspired from Kristian Høgsberg & Chris
Wilson in [this file](https://www.cairographics.org/cookbook/blur.c/)

Closes #17
feat(tool): introduce blurring capability

The blur algorithm is largely inspired from Kristian Høgsberg & Chris
Wilson in [this file](https://www.cairographics.org/cookbook/blur.c/)

Closes #17
feat(blur): use rect blur instead of brush

Use a rectangle to blur instead of brushes.

Does not improve the performance that much, will come in a later patch.

Will fix the UX of the feature though.

Closes #17
feat(blur): use rect blur instead of brush

Use a rectangle to blur instead of brushes.

Does not improve the performance that much, will come in a later patch.

Will fix the UX of the feature though.

Closes #17
feat(blur): use rect blur instead of brush

Use a rectangle to blur instead of brushes.

Does not improve the performance that much, will come in a later patch.

Will fix the UX of the feature though.

Closes #17
