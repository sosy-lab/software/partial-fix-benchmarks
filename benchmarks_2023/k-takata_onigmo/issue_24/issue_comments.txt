segmation fault occurs when many groups are used 
It seems that b9fba1d causes random crashes with multithread settings.

Oops, still SEGV occurs.
There were two causes with this issue.
1. Using `alloca()` may cause stack overflow when allocating very large area. (Already fixed.)
2. Group number is managed by `MemNumType` which is `short int`. Thus, the number become negative if the number of group exceeds 32767. It may cause access errors. (Not fixed yet.)

Should Onigmo support more than 32767 groups?

Now the maximum number of capture groups is explicitly limited to 32767 (`ONIG_MAX_CAPTURE_GROUP_NUM`).
