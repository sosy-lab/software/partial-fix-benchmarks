Merge branch 'master' of https://github.com/kfix/ddcctl
add some debug output when acquiring a display's IO service port
I'm not sure the 10.11 workaround is always working
DisplayRequest: send to every matching display

If a there are multiple identical displays, then send the request to all
of them instead of just the first one. For those setups this outcome is
better than not working at all.

Workaround for #17
gather a couple of IOFB fields in DEBUG builds

relates to #17
refactor IOFramebuffer discovery (#87)

maybe-fixes #17

* stop passing CGDisplay ID's to all the DDC commands, just use IOFramebuffers' service ports

* simply the main if-mountain-else

* weak link to CGDisplayIOServicePort and always use it if it was linked by dyld

* filter fallback IOFramebufferPortFromCGDisplayID(CGDirectDisplayID matches against location info from WindowServer's prefs
