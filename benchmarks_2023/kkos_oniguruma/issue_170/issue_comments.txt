Code to sumbit
PRINT_UNSUPPORT has been renamed to ONIGURUMA_UNSUPPORTED_PRINT.

I accepted ONIGURUMA_SYS_UEFI, but after reconsideration I don't accept this name.
Regular expressions libraries should know nothing about UEFI.
For example, I want to change the name to ONIG_NO_STANDARD_C_HEADERS.
For that reason, I reduced the range of ONIGURUMA_SYS_UEFI with some rewriting that seemed OK.
Currently, there is only one ONIGURUMA_SYS_UEFI in regint.h.
Do you have any comments on the name?

And today I have reduced the direct inclusion of system header files and moved them into ONIGURUMA_SYS_UEFI.
What remains now is:
```
$ grep "#include\s*<" *.h
onigposix.h:#include <stddef.h>
regenc.h:#include <stddef.h>
(* exclude regint.h)

$ grep "#include\s*<" *.c
regposerr.c:#include <string.h>
regposerr.c:#include <stdio.h>
(* exclude mktable.c)
```
Which of these files should not be included in SYS_UEFI?

All OS based header files should be in SYS_UEFI, including the files you list.
And there is another thing I want to raise. Please see below.

In regint.h

#ifndef xmemset
#define xmemset     memset
#endif

#ifndef xmemcpy
#define xmemcpy     memcpy
#endif

#ifndef xmemmove
#define xmemmove    memmove
#endif


memset, memcpy and memmove are Windows based API. Can you also include them into SYS_UEFI?
Or the name you raise ONIG_NO_STANDARD_C_HEADERS. This name is OK to me.
I have implemented your request.
```
ONIGURUMA_SYS_UEFI -> ONIG_NO_STANDARD_C_HEADERS
ONIGURUMA_UNSUPPORTED_PRINT -> ONIG_NO_PRINT
```
