Rule that specifies "title" seems to affect non-matching windows
So the issue is not really that the title filter doesn't apply, however, when a window has no title at all the check basically says "this window doesn't to have a title so we cannot perform a title check". I've changed the code so that if a window reports no title at all, we treat it as having the empty string as its title.
Excellent! Thank you @koekeishiya. I'll try it out today. 
Can confirm that #707 fixes the issue I was having: Chrome windows with empty titles are no longer matched by rules that specify a title regex. 👍 