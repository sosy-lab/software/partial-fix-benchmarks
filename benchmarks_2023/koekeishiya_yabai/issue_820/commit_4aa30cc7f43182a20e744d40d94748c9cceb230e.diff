diff --git a/CHANGELOG.md b/CHANGELOG.md
index cf6f9ea6..c84dd602 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -8,6 +8,7 @@ This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.htm
 ### Changed
 - Fixed an issue that in rare occasions caused yabai to freeze when focusing an inactve space on an inactive display [#1309](https://github.com/koekeishiya/yabai/issues/1309).
 - Fixed an issue that caused a window to incorrectly become focused when assigned to a space through rules [#1370](https://github.com/koekeishiya/yabai/issues/1370)
+- Fixed an issue that caused windows to snap back to its original position when moved between displays (to an inactive space) using mission-control [#820](https://github.com/koekeishiya/yabai/issues/820)
 
 ## [4.0.1] - 2022-05-17
 ### Changed
diff --git a/src/display_manager.c b/src/display_manager.c
index 39b6405f..76394dde 100644
--- a/src/display_manager.c
+++ b/src/display_manager.c
@@ -4,7 +4,7 @@ extern int g_connection;
 
 bool display_manager_query_displays(FILE *rsp)
 {
-    uint32_t count = 0;
+    int count;
     uint32_t *display_list = display_manager_active_display_list(&count);
     if (!display_list) return false;
 
@@ -146,7 +146,7 @@ uint32_t display_manager_last_display_id(void)
 
 uint32_t display_manager_find_closest_display_in_direction(uint32_t source_did, int direction)
 {
-    uint32_t display_count;
+    int display_count;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
     if (!display_list) return 0;
 
@@ -253,18 +253,18 @@ bool display_manager_display_is_animating(uint32_t did)
     return result;
 }
 
-uint32_t display_manager_active_display_count(void)
+int display_manager_active_display_count(void)
 {
     uint32_t count;
     CGGetActiveDisplayList(0, NULL, &count);
-    return count;
+    return (int)count;
 }
 
-uint32_t *display_manager_active_display_list(uint32_t *count)
+uint32_t *display_manager_active_display_list(int *count)
 {
     int display_count = display_manager_active_display_count();
     uint32_t *result = ts_alloc_aligned(sizeof(uint32_t), display_count);
-    CGGetActiveDisplayList(display_count, result, count);
+    CGGetActiveDisplayList(display_count, result, (uint32_t*)count);
     return result;
 }
 
diff --git a/src/display_manager.h b/src/display_manager.h
index af7cbd3a..3470f29e 100644
--- a/src/display_manager.h
+++ b/src/display_manager.h
@@ -53,8 +53,8 @@ int display_manager_dock_orientation(void);
 CGRect display_manager_dock_rect(void);
 bool display_manager_active_display_is_animating(void);
 bool display_manager_display_is_animating(uint32_t did);
-uint32_t display_manager_active_display_count(void);
-uint32_t *display_manager_active_display_list(uint32_t *count);
+int display_manager_active_display_count(void);
+uint32_t *display_manager_active_display_list(int *count);
 void display_manager_focus_display_with_point(uint32_t did, CGPoint point, bool update_cursor_position);
 void display_manager_focus_display(uint32_t did, uint64_t sid);
 bool display_manager_begin(struct display_manager *dm);
diff --git a/src/space_manager.c b/src/space_manager.c
index cf8fea64..2d984b82 100644
--- a/src/space_manager.c
+++ b/src/space_manager.c
@@ -67,7 +67,7 @@ bool space_manager_query_spaces_for_display(FILE *rsp, uint32_t did)
 
 bool space_manager_query_spaces_for_displays(FILE *rsp)
 {
-    uint32_t display_count;
+    int display_count;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
     if (!display_list) return false;
 
@@ -883,7 +883,7 @@ void space_manager_mark_spaces_invalid_for_display(struct space_manager *sm, uin
 
 void space_manager_mark_spaces_invalid(struct space_manager *sm)
 {
-    uint32_t display_count = 0;
+    int display_count;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
     if (!display_list) return;
 
@@ -976,7 +976,7 @@ void space_manager_init(struct space_manager *sm)
 
     table_init(&sm->view, 23, hash_view, compare_view);
 
-    uint32_t display_count;
+    int display_count;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
     if (!display_list) return;
 
diff --git a/src/window_manager.c b/src/window_manager.c
index e860424d..549ce49c 100644
--- a/src/window_manager.c
+++ b/src/window_manager.c
@@ -61,7 +61,7 @@ void window_manager_query_windows_for_display(FILE *rsp, uint32_t did)
 
 void window_manager_query_windows_for_displays(FILE *rsp)
 {
-    uint32_t display_count = 0;
+    int display_count = 0;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
 
     int space_count = 0;
@@ -1031,7 +1031,7 @@ struct window *window_manager_create_and_add_window(struct space_manager *sm, st
 
 static uint32_t *window_manager_application_window_list(struct application *application, int *window_count)
 {
-    uint32_t display_count;
+    int display_count;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
     if (!display_list) return NULL;
 
@@ -1612,11 +1612,13 @@ static void window_manager_validate_windows_on_space(struct space_manager *sm, s
             struct window *window = window_manager_find_window(wm, view_window_list[i]);
             if (!window) continue;
 
-            space_manager_untile_window(sm, view, window);
+            view_remove_window_node(view, window);
             window_manager_remove_managed_window(wm, window->id);
             window_manager_purify_window(wm, window);
         }
     }
+
+    view_flush(view);
 }
 
 static void window_manager_check_for_windows_on_space(struct space_manager *sm, struct window_manager *wm, uint64_t sid, uint32_t *window_list, int window_count)
@@ -1643,17 +1645,15 @@ static void window_manager_check_for_windows_on_space(struct space_manager *sm,
 
 void window_manager_validate_and_check_for_windows_on_space(struct space_manager *sm, struct window_manager *wm, uint64_t sid)
 {
-    int window_count;
+    int window_count = 0;
     uint32_t *window_list = space_window_list(sid, &window_count, false);
-    if (!window_list) return;
-
     window_manager_validate_windows_on_space(sm, wm, sid, window_list, window_count);
     window_manager_check_for_windows_on_space(sm, wm, sid, window_list, window_count);
 }
 
 void window_manager_correct_for_mission_control_changes(struct space_manager *sm, struct window_manager *wm)
 {
-    uint32_t display_count = 0;
+    int display_count;
     uint32_t *display_list = display_manager_active_display_list(&display_count);
     if (!display_list) return;
 
@@ -1665,12 +1665,12 @@ void window_manager_correct_for_mission_control_changes(struct space_manager *sm
         if (!space_list) continue;
 
         uint64_t sid = display_space_id(did);
-        for (int i = 0; i < space_count; ++i) {
-            if (space_list[i] == sid) {
+        for (int j = 0; j < space_count; ++j) {
+            if (space_list[j] == sid) {
                 window_manager_validate_and_check_for_windows_on_space(&g_space_manager, &g_window_manager, sid);
                 space_manager_refresh_view(sm, sid);
             } else {
-                space_manager_mark_view_invalid(sm, space_list[i]);
+                space_manager_mark_view_invalid(sm, space_list[j]);
             }
         }
     }
