set_exception_handler does not work.
I had a play around ... I expected that it was because $this is destroyed before the handler is invoked so set handler to a static method in another class, no dice. Even stranger, at the level of zend there is zend_try/zend_catch, they do what you expect them to do ... in my catch block at the level of zend where I should be able to manage that exception, there is no exception set ...

There are other possibilities ... zend's got exception hooks, but you get passed exceptions regardless of if they are being caught or not ... 

It's 7am and I'm not properly awake yet ... I'll come back to this in a few hours ...

ok let me explain why we need this to act like it does without using a thread. We want to run a legacy app within a thread context. Some of them using set_exception_handler to catch uncaught exceptions and output a styled error page. We want to grab these styled error pages from output buffer by doing:

``` php
ob_start();
require $scriptFilename;
$outputBuffer = ob_get_clean();
```

Now if no exception handler will handle those uncaught exceptions we wont get that styled error pages from the legacy app, because the exception handler will not be called. We can catch that exception by doing a try / catch over require, but that's not a solution for the problem itself.

Maybe now you're fully awake and can come back to this in a few hours ;)
Thanks!

Sorry chaps, I'm super busy at the minute ... I have looked and had a play around but nothing good to say yet ...

All right! Thanks for you time Joe.

@zelgerj does the behavior change if you use `&$this` like the following?

``` php
set_exception_handler(array(&$this, "handleException"));
```

@rdlowrey no it doesn't... it also does not work using static class like this:

``` php
<?php

class ExceptionHandler
{
    static public function handle(Exception $e)
    {
        var_dump($e);
    }
}

class ExceptionThread extends Thread
{
    public function run()
    {
        set_exception_handler(array("ExceptionHandler", "handle"));
        throw new Exception();
    }
}

$t = new ExceptionThread();
$t->start();
```

woops, my fix messes up normal exception handling ... have to think again ...
