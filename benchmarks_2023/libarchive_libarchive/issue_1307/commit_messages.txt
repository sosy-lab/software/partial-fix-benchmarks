Redo fix for #1302 in a way that archive_write_client_open()
correctly frees its allocations in an error case.

Reverts 5e270715b51d199467195b56f77e21cb8bb1d642
Fixed endian problem in lha UTF-16 encoding.

A patch to fix #1307
Merge pull request #1308 from Claybird/fix_lha_endian

Fixed endian problem in lha UTF-16 encoding.
Fixes #1307
LHA reader UTF-16: Encode directory separator with archive_le16enc()

Wrap to 80 characters.
Fixes #1307
