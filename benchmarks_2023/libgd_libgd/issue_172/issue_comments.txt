Zero-height rectangle has whiskers
I am not totally sure it is still here as I can reproduce it using 2.2 or master. I however added the tests using the php respective test, adding two red dots up the edges of the rectangle to quickly visually check what or where it would be wrong.

@cmb69 you have access to this repo now, if you feel like syncing php with gd, go ahead.

On the php implementation as of now, I am not sure about the new implementation,  especially the parts related to:

```
    x1h = x1; x1v = x1; y1h = y1; y1v = y1; x2h = x2; x2v = x2; y2h = y2; y2v = y2;
```

could be done only if the coordinates have been swapped.

Also seems to be used only in the last if else case. We should sync both implementations for easier maintenance. 

@pierrejoye wrote:

> I am not totally sure it is still here as I can reproduce it using 2.2 or master.

I assume you mean that you can _not_ reproduce it.

> you have access to this repo now, if you feel like syncing php with gd, go ahead.

I'll try to. Have to build libgd first (only did that with the bundled GD until now).

@pierrejoye I've fixed the test, but now it fails for me. Indeed, I see the whiskers below the horizontal rectangle in the red circle (the whiskers above the rectangle are overlaid by the red dots). Can you please verify the issue?

Thanks for testing!

Can you post the image pls?

I will try again, wondering why it passes for me :)

My result:

![github_bug_00172 c_41_out](https://cloud.githubusercontent.com/assets/2306138/15902530/a116dba4-2da8-11e6-9f82-eb8ca1d53f3b.png)

From looking at [the code](https://github.com/libgd/libgd/blob/gd-2.2.1/src/gd.c#L2320-L2321) there is most certainly an issue: if y1==y2 then there will be 3px long vertical lines drawn.

Thanks for testing.

I simplified the test while being at it.

The implementation is also a bit more clear now, handling simple (for operations) cases first.
