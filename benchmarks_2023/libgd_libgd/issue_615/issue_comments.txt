gdImageStringFT() fails for empty strings as of libgd 2.3.0
Forwarded from https://bugs.php.net/79415.
The problem is that [textLayout()](https://github.com/libgd/libgd/blob/188aa091e1c87ded33707f738753430dcd848bee/src/gdft.c#L445) returns 0 to signal failure, but also if an empty string is passed, although the latter is not necessarily a failure.
I have no idea about how to fix this issue. Could your please offer some tips about the solution? @cmb69 
@cmb69 can you please have a look, it seems your patch is not enough, perhaps related to some library version.

`gdImageStringFT` fails with "`Problem doing text layout`"
Is `ssize_t` and `SSIZE_MAX` available on that platform? Do you have a build with or without libraqm?
> Do you have a build with or without libraqm?

Yes, this is related to libraqm, see pr #649 