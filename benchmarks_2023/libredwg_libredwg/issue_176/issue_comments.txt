Several bugs found by fuzzing
**2.Crafted input will lead to Memory allocation failed in dwg_decode_LWPOLYLINE_private (src/dwg.spec:4105)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000003%2Csig:06%2Csrc:000000%2Cop:flip1%2Cpos:43812
ASAN says:
```
==25389==ERROR: AddressSanitizer failed to allocate 0xc9459a000 (54028509184) bytes of LargeMmapAllocator (error code: 12)
==25389==Process memory map follows:
	0x00007fff7000-0x00008fff7000
	0x00008fff7000-0x02008fff7000
	0x02008fff7000-0x10007fff8000
	0x5557879cb000-0x555788ebf000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x5557890bf000-0x5557890d7000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x5557890d7000-0x5557891bb000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x5557891bb000-0x5557891bf000
	....
	0x7fbd7b8fb000-0x7fbd7dc4d000
	0x7fbd7dc4d000-0x7fbd7dc64000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7fbd7dc64000-0x7fbd7de63000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7fbd7de63000-0x7fbd7de64000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7fbd7de64000-0x7fbd7de65000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7fbd7de65000-0x7fbd7de7f000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7fbd7de7f000-0x7fbd7e07e000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7fbd7e07e000-0x7fbd7e07f000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7fbd7e07f000-0x7fbd7e080000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7fbd7e080000-0x7fbd7e084000
	0x7fbd7e084000-0x7fbd7e08b000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7fbd7e08b000-0x7fbd7e28a000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7fbd7e28a000-0x7fbd7e28b000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7fbd7e28b000-0x7fbd7e28c000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7fbd7e28c000-0x7fbd7e28f000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7fbd7e28f000-0x7fbd7e48e000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7fbd7e48e000-0x7fbd7e48f000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7fbd7e48f000-0x7fbd7e490000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7fbd7e490000-0x7fbd7e677000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7fbd7e677000-0x7fbd7e877000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7fbd7e877000-0x7fbd7e87b000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7fbd7e87b000-0x7fbd7e87d000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7fbd7e87d000-0x7fbd7e881000
	0x7fbd7e881000-0x7fbd7ea1e000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7fbd7ea1e000-0x7fbd7ec1d000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7fbd7ec1d000-0x7fbd7ec1e000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7fbd7ec1e000-0x7fbd7ec1f000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7fbd7ec1f000-0x7fbd7ed6f000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7fbd7ed6f000-0x7fbd7ef6f000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7fbd7ef6f000-0x7fbd7ef72000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7fbd7ef72000-0x7fbd7ef75000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7fbd7ef75000-0x7fbd7fbda000
	0x7fbd7fbda000-0x7fbd7fc01000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7fbd7fc6d000-0x7fbd7fdf7000
	0x7fbd7fdf7000-0x7fbd7fe01000
	0x7fbd7fe01000-0x7fbd7fe02000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7fbd7fe02000-0x7fbd7fe03000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7fbd7fe03000-0x7fbd7fe04000
	0x7ffe9b3e2000-0x7ffe9b403000	[stack]
	0x7ffe9b56a000-0x7ffe9b56d000	[vvar]
	0x7ffe9b56d000-0x7ffe9b56f000	[vdso]
	0xffffffffff600000-0xffffffffff601000	[vsyscall]
==25389==End of process memory map.
==25389==AddressSanitizer CHECK failed: ../../../../src/libsanitizer/sanitizer_common/sanitizer_common.cc:118 "((0 && "unable to mmap")) != (0)" (0x0, 0x0)
    #0 0x7fbd7ed08c02  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xe9c02)
    #1 0x7fbd7ed27595 in __sanitizer::CheckFailed(char const*, int, char const*, unsigned long long, unsigned long long) (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x108595)
    #2 0x7fbd7ed12492  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xf3492)
    #3 0x7fbd7ed1e8a5  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xff8a5)
    #4 0x7fbd7ec4b8f1  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2c8f1)
    #5 0x7fbd7ec4604b  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2704b)
    #6 0x7fbd7ecfdd00 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded00)
    #7 0x55578802f729 in dwg_decode_LWPOLYLINE_private ../../src/dwg.spec:4105
    #8 0x555788038884 in dwg_decode_LWPOLYLINE ../../src/dwg.spec:4029
    #9 0x5557883c72f6 in dwg_decode_add_object ../../src/decode.c:4905
    #10 0x5557883daf28 in decode_R13_R2000 ../../src/decode.c:1216
    #11 0x55578840d61a in dwg_decode ../../src/decode.c:239
    #12 0x555787daff7a in dwg_read_file ../../src/dwg.c:206
    #13 0x555787dae32d in main ../../programs/dwg2dxf.c:255
    #14 0x7fbd7e4b1b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #15 0x555787daf2e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)
```
**3.Crafted input will lead to Memory allocation failed in decode_3dsolid (src/dwg.spec:1801)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000006%2Csig:06%2Csrc:000000%2Cop:flip1%2Cpos:46085
ASAN says:
```
==833==ERROR: AddressSanitizer failed to allocate 0x7f940f9000 (547944894464) bytes of LargeMmapAllocator (error code: 12)
==833==Process memory map follows:
	0x00007fff7000-0x00008fff7000
	0x00008fff7000-0x02008fff7000
	0x02008fff7000-0x10007fff8000
	0x562cd50b0000-0x562cd65a4000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x562cd67a4000-0x562cd67bc000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x562cd67bc000-0x562cd68a0000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x562cd68a0000-0x562cd68a4000
	....
	0x7f7e1e3c9000-0x7f7e2071b000
	0x7f7e2071b000-0x7f7e20732000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f7e20732000-0x7f7e20931000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f7e20931000-0x7f7e20932000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f7e20932000-0x7f7e20933000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f7e20933000-0x7f7e2094d000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f7e2094d000-0x7f7e20b4c000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f7e20b4c000-0x7f7e20b4d000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f7e20b4d000-0x7f7e20b4e000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f7e20b4e000-0x7f7e20b52000
	0x7f7e20b52000-0x7f7e20b59000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f7e20b59000-0x7f7e20d58000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f7e20d58000-0x7f7e20d59000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f7e20d59000-0x7f7e20d5a000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f7e20d5a000-0x7f7e20d5d000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f7e20d5d000-0x7f7e20f5c000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f7e20f5c000-0x7f7e20f5d000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f7e20f5d000-0x7f7e20f5e000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f7e20f5e000-0x7f7e21145000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f7e21145000-0x7f7e21345000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f7e21345000-0x7f7e21349000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f7e21349000-0x7f7e2134b000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f7e2134b000-0x7f7e2134f000
	0x7f7e2134f000-0x7f7e214ec000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f7e214ec000-0x7f7e216eb000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f7e216eb000-0x7f7e216ec000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f7e216ec000-0x7f7e216ed000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f7e216ed000-0x7f7e2183d000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f7e2183d000-0x7f7e21a3d000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f7e21a3d000-0x7f7e21a40000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f7e21a40000-0x7f7e21a43000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f7e21a43000-0x7f7e226a8000
	0x7f7e226a8000-0x7f7e226cf000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f7e2273b000-0x7f7e228c5000
	0x7f7e228c5000-0x7f7e228cf000
	0x7f7e228cf000-0x7f7e228d0000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f7e228d0000-0x7f7e228d1000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f7e228d1000-0x7f7e228d2000
	0x7ffec2458000-0x7ffec2479000	[stack]
	0x7ffec24f6000-0x7ffec24f9000	[vvar]
	0x7ffec24f9000-0x7ffec24fb000	[vdso]
	0xffffffffff600000-0xffffffffff601000	[vsyscall]
==833==End of process memory map.
==833==AddressSanitizer CHECK failed: ../../../../src/libsanitizer/sanitizer_common/sanitizer_common.cc:118 "((0 && "unable to mmap")) != (0)" (0x0, 0x0)
    #0 0x7f7e217d6c02  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xe9c02)
    #1 0x7f7e217f5595 in __sanitizer::CheckFailed(char const*, int, char const*, unsigned long long, unsigned long long) (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x108595)
    #2 0x7f7e217e0492  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xf3492)
    #3 0x7f7e217ec8a5  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xff8a5)
    #4 0x7f7e217198f1  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2c8f1)
    #5 0x7f7e2171404b  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2704b)
    #6 0x7f7e217cbd00 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded00)
    #7 0x562cd58a523f in decode_3dsolid ../../src/dwg.spec:1801
    #8 0x562cd58c27dc in dwg_decode_REGION_private ../../src/dwg.spec:2042
    #9 0x562cd58c3123 in dwg_decode_REGION ../../src/dwg.spec:2040
    #10 0x562cd5aa9c06 in dwg_decode_add_object ../../src/decode.c:4741
    #11 0x562cd5abff28 in decode_R13_R2000 ../../src/decode.c:1216
    #12 0x562cd5af261a in dwg_decode ../../src/decode.c:239
    #13 0x562cd5494f7a in dwg_read_file ../../src/dwg.c:206
    #14 0x562cd549332d in main ../../programs/dwg2dxf.c:255
    #15 0x7f7e20f7fb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #16 0x562cd54942e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)
```
**4.Crafted input will lead to Memory allocation failed in dwg_decode_HATCH_private (src/dwg.spec:3774)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000009%2Csig:06%2Csrc:000000%2Cop:flip1%2Cpos:47573
ASAN says:
```
==27930==ERROR: AddressSanitizer failed to allocate 0xc7b60b000 (53609541632) bytes of LargeMmapAllocator (error code: 12)
==27930==Process memory map follows:
	0x00007fff7000-0x00008fff7000
	0x00008fff7000-0x02008fff7000
	0x02008fff7000-0x10007fff8000
	0x5572ffa32000-0x557300f26000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x557301126000-0x55730113e000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x55730113e000-0x557301222000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x557301222000-0x557301226000
	....
	0x7f1f63cfe000-0x7f1f66050000
	0x7f1f66050000-0x7f1f66067000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f1f66067000-0x7f1f66266000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f1f66266000-0x7f1f66267000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f1f66267000-0x7f1f66268000	/lib/x86_64-linux-gnu/libgcc_s.so.1
	0x7f1f66268000-0x7f1f66282000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f1f66282000-0x7f1f66481000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f1f66481000-0x7f1f66482000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f1f66482000-0x7f1f66483000	/lib/x86_64-linux-gnu/libpthread-2.27.so
	0x7f1f66483000-0x7f1f66487000
	0x7f1f66487000-0x7f1f6648e000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f1f6648e000-0x7f1f6668d000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f1f6668d000-0x7f1f6668e000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f1f6668e000-0x7f1f6668f000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f1f6668f000-0x7f1f66692000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f1f66692000-0x7f1f66891000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f1f66891000-0x7f1f66892000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f1f66892000-0x7f1f66893000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f1f66893000-0x7f1f66a7a000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f1f66a7a000-0x7f1f66c7a000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f1f66c7a000-0x7f1f66c7e000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f1f66c7e000-0x7f1f66c80000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f1f66c80000-0x7f1f66c84000
	0x7f1f66c84000-0x7f1f66e21000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f1f66e21000-0x7f1f67020000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f1f67020000-0x7f1f67021000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f1f67021000-0x7f1f67022000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f1f67022000-0x7f1f67172000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f1f67172000-0x7f1f67372000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f1f67372000-0x7f1f67375000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f1f67375000-0x7f1f67378000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f1f67378000-0x7f1f67fdd000
	0x7f1f67fdd000-0x7f1f68004000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f1f68070000-0x7f1f681fa000
	0x7f1f681fa000-0x7f1f68204000
	0x7f1f68204000-0x7f1f68205000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f1f68205000-0x7f1f68206000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f1f68206000-0x7f1f68207000
	0x7fffaaf12000-0x7fffaaf33000	[stack]
	0x7fffaafac000-0x7fffaafaf000	[vvar]
	0x7fffaafaf000-0x7fffaafb1000	[vdso]
	0xffffffffff600000-0xffffffffff601000	[vsyscall]
==27930==End of process memory map.
==27930==AddressSanitizer CHECK failed: ../../../../src/libsanitizer/sanitizer_common/sanitizer_common.cc:118 "((0 && "unable to mmap")) != (0)" (0x0, 0x0)
    #0 0x7f1f6710bc02  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xe9c02)
    #1 0x7f1f6712a595 in __sanitizer::CheckFailed(char const*, int, char const*, unsigned long long, unsigned long long) (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x108595)
    #2 0x7f1f67115492  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xf3492)
    #3 0x7f1f671218a5  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xff8a5)
    #4 0x7f1f6704e8f1  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2c8f1)
    #5 0x7f1f6704904b  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2704b)
    #6 0x7f1f67100d00 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded00)
    #7 0x5573003a30c8 in dwg_decode_HATCH_private ../../src/dwg.spec:3774
    #8 0x5573003ca0fb in dwg_decode_HATCH ../../src/dwg.spec:3641
    #9 0x557300430ce6 in dwg_decode_add_object ../../src/decode.c:4908
    #10 0x557300441f28 in decode_R13_R2000 ../../src/decode.c:1216
    #11 0x55730047461a in dwg_decode ../../src/decode.c:239
    #12 0x5572ffe16f7a in dwg_read_file ../../src/dwg.c:206
    #13 0x5572ffe1532d in main ../../programs/dwg2dxf.c:255
    #14 0x7f1f668b4b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #15 0x5572ffe162e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)
```
**5.Double-free in dwg_free (src/free.c:837)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000002%2Csig:06%2Csrc:000093%2Cop:havoc%2Crep:8
ASAN says:
```
==10100==ERROR: AddressSanitizer: attempting double-free on 0x603000000040 in thread T0:
    #0 0x7f6b9a47a7b8 in __interceptor_free (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xde7b8)
    #1 0x55b7ed1d1623 in dwg_free ../../src/free.c:837
    #2 0x55b7eca06193 in main ../../programs/dwg2dxf.c:342
    #3 0x7f6b99c2eb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #4 0x55b7eca072e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)

0x603000000040 is located 0 bytes inside of 32-byte region [0x603000000040,0x603000000060)
freed by thread T0 here:
    #0 0x7f6b9a47a7b8 in __interceptor_free (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xde7b8)
    #1 0x55b7ece0c199 in dwg_decode_handleref ../../src/decode.c:3689

previously allocated by thread T0 here:
    #0 0x7f6b9a47ad38 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded38)
    #1 0x55b7ece0bcbd in dwg_decode_handleref ../../src/decode.c:3669
    #2 0x10effffffffff  (<unknown module>)

SUMMARY: AddressSanitizer: double-free (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xde7b8) in __interceptor_free
==10100==ABORTING

```
**6.Heap-use-after-free in resolve_objectref_vector (src/decode.c:1434)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000006%2Csig:06%2Csrc:000308%2B000975%2Cop:splice%2Crep:64
ASAN says:
```
==25001==ERROR: AddressSanitizer: heap-use-after-free on address 0x603000000058 at pc 0x55e4585b3686 bp 0x7ffc01783e70 sp 0x7ffc01783e60
READ of size 8 at 0x603000000058 thread T0
    #0 0x55e4585b3685 in resolve_objectref_vector ../../src/decode.c:1434
    #1 0x55e458b6fa26 in decode_R13_R2000 ../../src/decode.c:1421
    #2 0x55e458b9e61a in dwg_decode ../../src/decode.c:239
    #3 0x55e458540f7a in dwg_read_file ../../src/dwg.c:206
    #4 0x55e45853f32d in main ../../programs/dwg2dxf.c:255
    #5 0x7f0bd1d26b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #6 0x55e4585402e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)

0x603000000058 is located 24 bytes inside of 32-byte region [0x603000000040,0x603000000060)
freed by thread T0 here:
    #0 0x7f0bd25727b8 in __interceptor_free (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xde7b8)
    #1 0x55e458945199 in dwg_decode_handleref ../../src/decode.c:3689

previously allocated by thread T0 here:
    #0 0x7f0bd2572d38 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded38)
    #1 0x55e458944cbd in dwg_decode_handleref ../../src/decode.c:3669
    #2 0x10effffffffff  (<unknown module>)

SUMMARY: AddressSanitizer: heap-use-after-free ../../src/decode.c:1434 in resolve_objectref_vector
Shadow bytes around the buggy address:
  0x0c067fff7fb0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7fc0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7fd0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c067fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c067fff8000: fa fa fd fd fd fd fa fa fd fd fd[fd]fa fa 00 00
  0x0c067fff8010: 00 00 fa fa 00 00 00 00 fa fa 00 00 00 00 fa fa
  0x0c067fff8020: fd fd fd fd fa fa fd fd fd fd fa fa 00 00 00 02
  0x0c067fff8030: fa fa 00 00 00 02 fa fa 00 00 00 03 fa fa fa fa
  0x0c067fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c067fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==25001==ABORTING
```
**7.Heap-buffer-overflow in decode_R13_R2000 (src/decode.c:1315)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000007%2Csig:06%2Csrc:000316%2B000377%2Cop:splice%2Crep:128
ASAN says:
```
==17430==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x6130000001b0 at pc 0x5647424fb76d bp 0x7ffdbd213570 sp 0x7ffdbd213560
READ of size 8 at 0x6130000001b0 thread T0
    #0 0x5647424fb76c in decode_R13_R2000 ../../src/decode.c:1315
    #1 0x56474251561a in dwg_decode ../../src/decode.c:239
    #2 0x564741eb7f7a in dwg_read_file ../../src/dwg.c:206
    #3 0x564741eb632d in main ../../programs/dwg2dxf.c:255
    #4 0x7f94848bcb96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #5 0x564741eb72e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)

0x6130000001b0 is located 8 bytes to the right of 360-byte region [0x613000000040,0x6130000001a8)
allocated by thread T0 here:
    #0 0x7f9485108d38 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded38)
    #1 0x5647424dea59 in decode_R13_R2000 ../../src/decode.c:861

SUMMARY: AddressSanitizer: heap-buffer-overflow ../../src/decode.c:1315 in decode_R13_R2000
Shadow bytes around the buggy address:
  0x0c267fff7fe0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c267fff7ff0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c267fff8000: fa fa fa fa fa fa fa fa 00 00 00 00 00 00 00 00
  0x0c267fff8010: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c267fff8020: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c267fff8030: 00 00 00 00 00 fa[fa]fa fa fa fa fa fa fa fa fa
  0x0c267fff8040: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c267fff8050: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c267fff8060: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c267fff8070: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c267fff8080: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==17430==ABORTING
```
**8.Crafted input will lead to Memory allocation failed in decode_preR13_section (src/decode.c:315)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000008%2Csig:06%2Csrc:000768%2B000486%2Cop:splice%2Crep:128
ASAN says:
```
==30021==ERROR: AddressSanitizer failed to allocate 0x1138003000 (73953980416) bytes of LargeMmapAllocator (error code: 12)
==30021==Process memory map follows:
	0x00007fff7000-0x00008fff7000
	0x00008fff7000-0x02008fff7000
	0x02008fff7000-0x10007fff8000
	0x555c6a254000-0x555c6b748000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x555c6b948000-0x555c6b960000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	0x555c6b960000-0x555c6ba44000	/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf
	....
	0x7f552f888000-0x7f552f88f000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f552f88f000-0x7f552fa8e000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f552fa8e000-0x7f552fa8f000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f552fa8f000-0x7f552fa90000	/lib/x86_64-linux-gnu/librt-2.27.so
	0x7f552fa90000-0x7f552fa93000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f552fa93000-0x7f552fc92000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f552fc92000-0x7f552fc93000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f552fc93000-0x7f552fc94000	/lib/x86_64-linux-gnu/libdl-2.27.so
	0x7f552fc94000-0x7f552fe7b000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f552fe7b000-0x7f553007b000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f553007b000-0x7f553007f000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f553007f000-0x7f5530081000	/lib/x86_64-linux-gnu/libc-2.27.so
	0x7f5530081000-0x7f5530085000
	0x7f5530085000-0x7f5530222000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f5530222000-0x7f5530421000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f5530421000-0x7f5530422000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f5530422000-0x7f5530423000	/lib/x86_64-linux-gnu/libm-2.27.so
	0x7f5530423000-0x7f5530573000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f5530573000-0x7f5530773000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f5530773000-0x7f5530776000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f5530776000-0x7f5530779000	/usr/lib/x86_64-linux-gnu/libasan.so.4.0.0
	0x7f5530779000-0x7f55313de000
	0x7f55313de000-0x7f5531405000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f5531471000-0x7f55315fb000
	0x7f55315fb000-0x7f5531605000
	0x7f5531605000-0x7f5531606000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f5531606000-0x7f5531607000	/lib/x86_64-linux-gnu/ld-2.27.so
	0x7f5531607000-0x7f5531608000
	0x7fff8fdef000-0x7fff8fe10000	[stack]
	0x7fff8fea3000-0x7fff8fea6000	[vvar]
	0x7fff8fea6000-0x7fff8fea8000	[vdso]
	0xffffffffff600000-0xffffffffff601000	[vsyscall]
==30021==End of process memory map.
==30021==AddressSanitizer CHECK failed: ../../../../src/libsanitizer/sanitizer_common/sanitizer_common.cc:118 "((0 && "unable to mmap")) != (0)" (0x0, 0x0)
    #0 0x7f553050cc02  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xe9c02)
    #1 0x7f553052b595 in __sanitizer::CheckFailed(char const*, int, char const*, unsigned long long, unsigned long long) (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x108595)
    #2 0x7f5530516492  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xf3492)
    #3 0x7f55305228a5  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xff8a5)
    #4 0x7f553044f8f1  (/usr/lib/x86_64-linux-gnu/libasan.so.4+0x2c8f1)
    #5 0x7f5530501f07 in realloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xdef07)
    #6 0x555c6a380d1f in decode_preR13_section ../../src/decode.c:315
    #7 0x555c6a9fadca in decode_preR13 ../../src/decode.c:708
    #8 0x555c6ac9634f in dwg_decode ../../src/decode.c:235
    #9 0x555c6a638f7a in dwg_read_file ../../src/dwg.c:206
    #10 0x555c6a63732d in main ../../programs/dwg2dxf.c:255
    #11 0x7f552fcb5b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #12 0x555c6a6382e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)
```
**9.Heap-buffer-overflow in decode_preR13_section (src/decode.c:350)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000024%2Csig:06%2Csrc:002283%2B001622%2Cop:splice%2Crep:64
ASAN says:
```
==31479==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x629000004e14 at pc 0x55713d105f18 bp 0x7ffefe359270 sp 0x7ffefe359260
WRITE of size 4 at 0x629000004e14 thread T0
    #0 0x55713d105f17 in decode_preR13_section ../../src/decode.c:350
    #1 0x55713d77fdca in decode_preR13 ../../src/decode.c:708
    #2 0x55713da1b34f in dwg_decode ../../src/decode.c:235
    #3 0x55713d3bdf7a in dwg_read_file ../../src/dwg.c:206
    #4 0x55713d3bc32d in main ../../programs/dwg2dxf.c:255
    #5 0x7fcb2bc53b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #6 0x55713d3bd2e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)

0x629000004e14 is located 20 bytes to the right of 19456-byte region [0x629000000200,0x629000004e00)
allocated by thread T0 here:
    #0 0x7fcb2c49fb50 in __interceptor_malloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xdeb50)
    #1 0x55713d19562d in decode_preR13_entities ../../src/decode.c:4356

SUMMARY: AddressSanitizer: heap-buffer-overflow ../../src/decode.c:350 in decode_preR13_section
Shadow bytes around the buggy address:
  0x0c527fff8970: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c527fff8980: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c527fff8990: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c527fff89a0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0c527fff89b0: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0c527fff89c0: fa fa[fa]fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c527fff89d0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c527fff89e0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c527fff89f0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c527fff8a00: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0c527fff8a10: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==31479==ABORTING
```
**10.Heap-buffer-overflow in bit_search_sentinel (src/bits.c:1825)**
PoC: https://github.com/linhlhq/research/blob/master/PoCs/libreDWG/id:000041%2Csig:06%2Csrc:003308%2Cop:havoc%2Crep:16
ASAN says:
```
==32322==ERROR: AddressSanitizer: heap-buffer-overflow on address 0x7f2638fc53de at pc 0x562bfa36216a bp 0x7ffc67dcbdc0 sp 0x7ffc67dcbdb0
READ of size 1 at 0x7f2638fc53de thread T0
    #0 0x562bfa362169 in bit_search_sentinel ../../src/bits.c:1825
    #1 0x562bfa9264ac in decode_R13_R2000 ../../src/decode.c:1389
    #2 0x562bfa95261a in dwg_decode ../../src/decode.c:239
    #3 0x562bfa2f4f7a in dwg_read_file ../../src/dwg.c:206
    #4 0x562bfa2f332d in main ../../programs/dwg2dxf.c:255
    #5 0x7f2637792b96 in __libc_start_main (/lib/x86_64-linux-gnu/libc.so.6+0x21b96)
    #6 0x562bfa2f42e9 in _start (/home/user/linhlhq/libredwg/obj-afl/programs/dwg2dxf+0x3e42e9)

0x7f2638fc53de is located 0 bytes to the right of 400350-byte region [0x7f2638f63800,0x7f2638fc53de)
allocated by thread T0 here:
    #0 0x7f2637fded38 in __interceptor_calloc (/usr/lib/x86_64-linux-gnu/libasan.so.4+0xded38)
    #1 0x562bfa2f4e61 in dat_read_file ../../src/dwg.c:69
    #2 0x562bfa2f4e61 in dwg_read_file ../../src/dwg.c:199

SUMMARY: AddressSanitizer: heap-buffer-overflow ../../src/bits.c:1825 in bit_search_sentinel
Shadow bytes around the buggy address:
  0x0fe5471f0a20: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0fe5471f0a30: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0fe5471f0a40: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0fe5471f0a50: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
  0x0fe5471f0a60: 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
=>0x0fe5471f0a70: 00 00 00 00 00 00 00 00 00 00 00[06]fa fa fa fa
  0x0fe5471f0a80: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0fe5471f0a90: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0fe5471f0aa0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0fe5471f0ab0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
  0x0fe5471f0ac0: fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa fa
Shadow byte legend (one shadow byte represents 8 application bytes):
  Addressable:           00
  Partially addressable: 01 02 03 04 05 06 07
  Heap left redzone:       fa
  Freed heap region:       fd
  Stack left redzone:      f1
  Stack mid redzone:       f2
  Stack right redzone:     f3
  Stack after return:      f5
  Stack use after scope:   f8
  Global redzone:          f9
  Global init order:       f6
  Poisoned by user:        f7
  Container overflow:      fc
  Array cookie:            ac
  Intra object redzone:    bb
  ASan internal:           fe
  Left alloca redzone:     ca
  Right alloca redzone:    cb
==32322==ABORTING
```
I can repro all
I'll ignore the preR13 error (id 000008), as this code is not stable yet, and disabled in releases.
Yes, when you fix all the bugs in the release I will come back.
Done in the `smoke/fuzz` branch
Did you fix all the bugs I reported above? Except for bug **8. lead to Memory allocation failed in decode_preR13_section**
All fixed now
yeah, that was awesome. Can you help me request cve for the above bugs? Thank you.
You cannot CVE's for the preR13 bugs, because that code is disabled with releases. 
@rurban So are there any other bugs you can help me with? With bugs from 1 to 7 I report above.
@rurban Can you cliff notes if any of them were in active code and link to the fixing commits?
It appears these were assigned:
- [CVE-2019-20009](https://nvd.nist.gov/vuln/detail/CVE-2019-20009)
- [CVE-2019-20010](https://nvd.nist.gov/vuln/detail/CVE-2019-20010)
- [CVE-2019-20011](https://nvd.nist.gov/vuln/detail/CVE-2019-20011)
- [CVE-2019-20012](https://nvd.nist.gov/vuln/detail/CVE-2019-20012)
- [CVE-2019-20013](https://nvd.nist.gov/vuln/detail/CVE-2019-20013)
- [CVE-2019-20014](https://nvd.nist.gov/vuln/detail/CVE-2019-20014)
- [CVE-2019-20015](https://nvd.nist.gov/vuln/detail/CVE-2019-20015)
Thanks, I'll have to mark them as fixed by the latest release 0.9.3
@rurban Can I request cve for preR13 bugs? Or can you request for me? Thanks.
These cannot be CVE's as this code is disabled in releases.

linhlhq <notifications@github.com> schrieb am Mi., 1. Jan. 2020, 11:21:

> @rurban <https://github.com/rurban> Can I request cve for preR13 bugs? Or
> can you request for me? Thanks.
>
> —
> You are receiving this because you were mentioned.
> Reply to this email directly, view it on GitHub
> <https://github.com/LibreDWG/libredwg/issues/176?email_source=notifications&email_token=AAAKGUKXAWN4WBUHYJMUTPLQ3RVBZA5CNFSM4J62I26KYY3PNVWWK3TUL52HS4DFVREXG43VMVBW63LNMVXHJKTDN5WW2ZLOORPWSZGOEH5CARI#issuecomment-570040389>,
> or unsubscribe
> <https://github.com/notifications/unsubscribe-auth/AAAKGULY6L3JRIK6R662AMLQ3RVBZANCNFSM4J62I26A>
> .
>

Verified them to be fixed with 3f515d52954e2ec009a2508d3d5be04e3bcc094b (for the upcoming 0.10) also