json: fix LTYPE.dashes_r11 size

fixed size 12 for json also.
Fixes oss-fuzz #53617
dxf: NOD must be the first OBJECTS item

not just the first DICTIONARY.
Fixes part 1 of GH #536
dxf: fix EED 1004 with empty value

need to print the GROUP 1004, even with empty value.
Fixes GH #536

DXFB has a better logic: do{} while, and is not affected.
dxf: fix EED 1004 with empty value

need to print the GROUP 1004, even with empty value.
Fixes GH #536

DXFB has a better logic: do{} while, and is not affected.
