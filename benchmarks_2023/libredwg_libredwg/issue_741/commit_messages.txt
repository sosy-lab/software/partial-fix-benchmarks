fix 32-bit handle sizes

32bit did not zero the higher bits, leading
to too high handle values with the add api.
dwgadd: scanf float before int

Fixes GH #741
dwgadd: improve int vs float decision

still without checking the target type.
Fixes GH #741
WIP dwgadd: improve int vs float decision

still without checking the target type.
Fixes GH #741
WIP dwgadd: improve int vs float decision

now also with checking the target size. (stack-overflows)
Fixes GH #741
dwgadd: improve int vs float decision

now also with checking the target size and type. (stack-overflows)
Fixes GH #741
dwgadd: fix int vs float decisions

now also with checking the target size and type. (stack-overflows)
Fixes GH #741
