diff --git a/src/bits.c b/src/bits.c
index 100785d9d6..a110bf53cb 100644
--- a/src/bits.c
+++ b/src/bits.c
@@ -2394,14 +2394,20 @@ bit_write_TU16 (Bit_Chain *restrict dat, BITCODE_TU restrict chain)
     bit_write_RS (dat, chain[i]);
 }
 
-/** String16: Write ASCII prefixed by a RS length
+/** String16: Write ASCII/Unicode prefixed by a RS length,
+    up-converts to TU16
  */
 void
 bit_write_T16 (Bit_Chain *restrict dat, BITCODE_T16 restrict chain)
 {
-  size_t i, length;
+  size_t length;
   if (chain)
-    length = strlen (chain);
+    {
+      if (IS_FROM_TU (dat))
+        length = bit_wcs2len ((BITCODE_TU)chain);
+      else
+        length = strlen (chain);
+    }
   else
     length = 0;
   if (length > INT16_MAX)
@@ -2411,12 +2417,61 @@ bit_write_T16 (Bit_Chain *restrict dat, BITCODE_T16 restrict chain)
       length = INT16_MAX;
       chain[INT16_MAX - 1] = '\0';
     }
-  bit_write_RS (dat, (BITCODE_RS)(length));
-  for (i = 0; i < length; i++)
-    bit_write_RC (dat, chain[i]);
+  else if (length == 0)
+    bit_write_RS (dat, 0);
+  else if (dat->version >= R_2007)
+    {
+      if (!IS_FROM_TU (dat))
+        { // convert to unicode, expand \\U+
+          BITCODE_TU wstr = bit_utf8_to_TU (chain, 0);
+          size_t wlen = bit_wcs2len (wstr) + 1;
+          bit_write_RS (dat, (BITCODE_RS)length);
+          for (size_t i = 0; i <= length; i++)
+            bit_write_RS (dat, wstr[i]);
+        }
+      else
+        {
+          bit_write_RS (dat, (BITCODE_RS)length + 1);
+          for (size_t i = 0; i <= length; i++)
+            bit_write_RS (dat, chain[i]);
+        }
+    }
+  else
+    {
+      if (IS_FROM_TU (dat))
+        {
+          // convert from unicode to ascii via utf8
+          char dest[1024];
+          char *u8 = bit_convert_TU ((BITCODE_TU)chain);
+          bit_utf8_to_TV (dest, (unsigned char *)u8, 1024, strlen (u8),
+                          0, dat->codepage);
+          length = strlen (dest);
+          if (length)
+            {
+              bit_write_RS (dat, (BITCODE_RS)(length + 1));
+              for (size_t i = 0; i <= length; i++)
+                bit_write_RC (dat, dest[i]);
+            }
+          else
+            bit_write_RS (dat, 0);
+          free (u8);
+        }
+      else
+        {
+          if (length)
+            {
+              bit_write_RS (dat, (BITCODE_RS)(length + 1));
+              for (size_t i = 0; i <= length; i++)
+                bit_write_RC (dat, chain[i]);
+            }
+          else
+            bit_write_RS (dat, 0);
+        }
+    }
 }
 
 // ASCII/UCS-2 string with a RL size (not length)
+// TODO: also upconvert to TU32
 void
 bit_write_T32 (Bit_Chain *restrict dat, BITCODE_T32 restrict chain)
 {
diff --git a/src/dec_macros.h b/src/dec_macros.h
index 846865d22f..080ea61513 100644
--- a/src/dec_macros.h
+++ b/src/dec_macros.h
@@ -562,30 +562,39 @@
     LOG_TRACE ("\n")                                                          \
     LOG_TRACE_TF ((BITCODE_RC*)_obj->nam, (int)len);                          \
   }
-#define FIELD_T16(nam, dxf) FIELDG (nam, T16, dxf)
-#define FIELD_TU16(nam, dxf)                                                  \
-  {                                                                           \
-    _obj->nam = bit_read_TU16 (dat);                                          \
-    LOG_TRACE_TU (#nam, FIELD_VALUE (nam), dxf);                              \
-  }
-#define FIELD_TU32(nam, dxf)                                                  \
+#define FIELD_T16(nam, dxf)                                                   \
+    if (dat->from_version < R_2007)                                           \
+      {                                                                       \
+        FIELDG (nam, T16, dxf);                                               \
+      }                                                                       \
+    else                                                                      \
+      {                                                                       \
+        _obj->nam = (BITCODE_T16)bit_read_TU16 (dat);                         \
+        LOG_TRACE_TU (#nam, FIELD_VALUE (nam), dxf);                          \
+      }
+#define FIELD_T32(nam, dxf)                                                   \
   {                                                                           \
-    _obj->nam = bit_read_TU32 (dat);                                          \
     if (dat->from_version < R_2007)                                           \
       {                                                                       \
-        LOG_TRACE (#nam ": \"%s\" [TU32 %d]\n", _obj->nam, dxf)               \
+        FIELDG (nam, T32, dxf);                                               \
       }                                                                       \
     else                                                                      \
       {                                                                       \
+        _obj->nam = bit_read_TU32 (dat);                                      \
         LOG_TRACE_TU (#nam, FIELD_VALUE (nam), dxf)                           \
       }                                                                       \
   }
-#define FIELD_T32(nam, dxf)                                                   \
+#define FIELD_TU16(nam, dxf)                                                  \
+  {                                                                           \
+    _obj->nam = bit_read_TU16 (dat);                                          \
+    LOG_TRACE_TU (#nam, FIELD_VALUE (nam), dxf);                              \
+  }
+#define FIELD_TU32(nam, dxf)                                                  \
   {                                                                           \
-    _obj->nam = bit_read_T32 (dat);                                           \
+    _obj->nam = bit_read_TU32 (dat);                                          \
     if (dat->from_version < R_2007)                                           \
       {                                                                       \
-        LOG_TRACE (#nam ": \"%s\" [T32 %d]\n", _obj->nam, dxf);               \
+        LOG_TRACE (#nam ": \"%s\" [TU32 %d]\n", _obj->nam, dxf)               \
       }                                                                       \
     else                                                                      \
       {                                                                       \
diff --git a/src/encode.c b/src/encode.c
index 8f96d163ea..a0dd210441 100644
--- a/src/encode.c
+++ b/src/encode.c
@@ -6594,11 +6594,11 @@ downconvert_TABLESTYLE (Dwg_Object *restrict obj)
   _obj->rowstyles[0].unit_type =
   _obj->sty.cellstyle.content_format.value_unit_type;
   {
-    char *u8 = bit_convert_TU
-  (_obj->sty.cellstyle.content_format.value_format_string); size_t destlen =
-  strlen (u8) + 1; char *dest = malloc (destlen);
+    char *u8 = bit_convert_TU (_obj->sty.cellstyle.content_format.value_format_string);
+    size_t destlen = strlen (u8) + 1;
+    char *dest = malloc (destlen);
     _obj->rowstyles[0].format_string = bit_utf8_to_TV (dest, u8, destlen,
-  destlen - 1, 0, dwg->header.codepage);
+                                         destlen - 1, 0, dwg->header.codepage);
   }
   */
   if (!_obj->rowstyles[0].num_borders)
diff --git a/src/template.spec b/src/template.spec
index a5d5e44239..fe8d53ae5b 100644
--- a/src/template.spec
+++ b/src/template.spec
@@ -18,10 +18,5 @@
 
 #include "spec.h"
 
-PRE (R_2007) {
-  FIELD_T16 (description, 0);
-}
-LATER_VERSIONS {
-  FIELD_CAST (description, TU16, T16, 0);
-}
+FIELD_T16 (description, 0);
 FIELD_RS (MEASUREMENT, 0); // copied to header_vars
