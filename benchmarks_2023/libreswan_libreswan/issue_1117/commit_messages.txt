connections: tweak is-a-template logging
testing: make xauth-pluto-14 and ikev1-l2tp-03-two-interfaces WIP

see #1116 and #1117
routing: drop hack letting a template establish

see #1116 and #1117
routing: add hacks for ikev1 establishing templates

see ikev1-l2tp-03-two-interfaces #693 #1117
testing: mark ikev1-l2tp-03-two-interfaces as goodish

see #693 #1117
connections: <that>subnet=vnet:... only implies <this> is a template

... not <that> is also a template

fix #1117
connections: <that>subnet=vnet:... only implies <this> is a template

... not <that> is also a template

fix #1117
routing: drop bogus ESTABLISH_INBOUND, ROUTED_ONDEMAND, TEMPLATE

no longer happens #1117
