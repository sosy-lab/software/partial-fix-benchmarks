SDL_RenderSetLogicalSize stretches render with SDL_WINDOW_ALLOW_HIGHDPI when window is resized and moved
I've also run into this while testing `testwm2` with `--resizable --logical 640x480 --allow-highdpi --renderer opengl` (recent `main` - d713a68071fe3ad8a60be3d92054a04325c5dfd8 , and macOS 10.15.7). Edit: I added `--renderer opengl` to the above command as it only repros with that renderer.

1. Resize the window horizontally a bit to cause letterboxing on the left/right: 
<img width="810" alt="image" src="https://user-images.githubusercontent.com/239161/143551068-c3c3226d-2a6f-4b88-a478-15f7c0b438b6.png">

2. Move the window by dragging the titlebar. The letterboxing will disappear and the contents will be stretched:

<img width="806" alt="image" src="https://user-images.githubusercontent.com/239161/143551265-fc27cb72-e116-4036-90ed-e35cd984345f.png">


I can't reproduce it on linux, maybe a specific sequence on macosx.
maybe you can try to use this PR https://github.com/libsdl-org/SDL/pull/5003 : it fixes an issue with scale and viewport, but I don't see why it would solve this one. but who knows..

Can you enable the log, maybe there are some different event occuring:

`./testwm2  --resizable --logical 640x480 --allow-highdpi --info all`







eventually also changing the renderer : 
```
--renderer software
--renderer metal
--renderer opengl
```

Tried different backends:

- `--renderer software`: doesn't reproduce
- `--renderer metal`: doesn't reproduce
- `--renderer opengl`: reproduces

Seems like the bug is specific to macOS + OpenGL. Will try the other ideas.
There was nothing interesting in the `--info all` output, and the results were the same with  #5003

However, I did find a workaround: in sdl_render_gl.c:

```
static void
SetDrawState(GL_RenderData *data, const SDL_RenderCommand *cmd, const GL_Shader shader)
{
    const SDL_BlendMode blend = cmd->data.draw.blend;

    if (1) { //data->drawstate.viewport_dirty) { // run this block with the glViewport() call, etc., unconditionally
```

So it seems like moving the window is causing some OpenGL state (viewport?) to change without SDL's knowledge.

More practical workaround:

```
diff --git a/src/render/opengl/SDL_render_gl.c b/src/render/opengl/SDL_render_gl.c
index 07280865e..dbfcc414c 100644
--- a/src/render/opengl/SDL_render_gl.c
+++ b/src/render/opengl/SDL_render_gl.c
@@ -1165,14 +1165,20 @@ GL_RunCommandQueue(SDL_Renderer * renderer, SDL_RenderCommand *cmd, void *vertic
             data->drawstate.viewport_dirty = SDL_TRUE;  // if the window dimensions changed, invalidate the current viewport, etc.
             data->drawstate.cliprect_dirty = SDL_TRUE;
             data->drawstate.drawablew = w;
             data->drawstate.drawableh = h;
         }
     }
 
+#ifdef __MACOSX__
+    // On macOS, moving the window seems to invalidate the OpenGL viewport state,
+    // so don't bother trying to persist it across frames; always reset it.
+    // Workaround for: https://github.com/libsdl-org/SDL/issues/1504
+    data->drawstate.viewport_dirty = SDL_TRUE;
+#endif
 
     while (cmd) {
         switch (cmd->command) {
             case SDL_RENDERCMD_SETDRAWCOLOR: {
                 const Uint8 r = cmd->data.color.r;
                 const Uint8 g = cmd->data.color.g;
                 const Uint8 b = cmd->data.color.b;
```

I'll make a PR.
Ah cool, I had run into this issue in the past but didn’t have a simple reproducer. I similarly worked around it by using a Metal renderer instead of OpenGL on macOS.
My mistake on the close/reopen, looking at this on a phone...

I'm trying to look over the history but it seems like this got fixed then immediately reopened - what happened with this one?
I think I have a fix for this... it seems like a good idea to expand the size changed event handler to include certain moves as well:

https://github.com/libsdl-org/SDL/blob/main/src/render/SDL_render.c#L701

This would fix both the macOS viewport as well as the scenario where the window gets moved to a display with a new DPI. I think this gets partially updated in the renderer but reusing the size-changed drawable size check could prevent issues with multimonitor setups in particular. FNA does a similar thing to address a similar bug to this.