Wrong SDL_RenderGetClipRect/SDL_RenderSetClipRect behaviour
I thought we fixed this; let me see what we did there.
I really thought we tracked this state separately to avoid float issues, but maybe I dreamed that...?

The quick fix is probably just to use SDL_round instead of SDL_floor.  @1bsyl, is that okay?
Yeah, it looks like we used to use `SDL_ceil()`, and that changed in https://github.com/libsdl-org/SDL/commit/590a5469eddc0c29a012305b69a2d4965ee771b0 but `SDL_round()` seems reasonable.
I have also reproduced it. (so this bug hasn't been fixed by increasing the precision to float SDL_FRect)
It doesn't seem to be a regression, because I still get it reproduced in all previous SDL2 versions.

(https://github.com/libsdl-org/SDL/commit/590a5469eddc0c29a012305b69a2d4965ee771b0 doesn't make a difference because it's about the "SetClipRect" but the lost of precision could be in GetClipRect") also. it was fixing a real artefact.

my testcase: 
[main_bug_5547_cliprect_scale.c.txt](https://github.com/libsdl-org/SDL/files/8507385/main_bug_5547_cliprect_scale.c.txt)

doing a quick test, It seems this would be fixed by updating the precision from float to double.
eg: 
adding a *internal* SDL_DRect  (same as SDL_FRect but with double).  to store the clip rect / viewport.
and the scope of this is only SDL_render.c


Okay, let's not create SDL_DRect, but that seems like a good change.
previously, 
we changed cliprect/viewport to float
https://github.com/libsdl-org/SDL/commit/e14d10263cc7c8ab2be8758d7c347b3365f9a62c
https://github.com/libsdl-org/SDL/commit/a0818a63e3ec9d9bafbb4118ecb7404d4ac2baff

I mean, creating SDL_DRect is fine, but I don't want to expose it in the API and add a whole new set of functions to work with them.
yes of course !
Can you confirm this is still fixed after https://github.com/libsdl-org/SDL/commit/b9fe6ba0e726c5d6aee8bd40ab4165630e33edb6?
yes,  testing seems it's ok !
Great!
On the forums, Mykola wrote:
> Maybe, but for now I just need that after SDL_RenderSetClipRect SDL_RenderGetClipRect returns the same value. This we achieve by replacing SDL_floor with SDL_round.

Is he talking about inside SDL_render.c, or in his code? Do we need to make further changes here?


I think the current behavior is ok.

Fix of [80b396a](https://github.com/1bsyl/SDL/commit/80b396a77cd6fae0d4a3e68f73242878427b7884) (switching to double precision) is  necessary because, it should be possible to do:
whatever the scale set with SDL_SetRenderScale.
if you set a clip rect with SDL_SetClipRect (int), then SD_GetClipRect should be able to give back the exact same SDL_rect.
It's only achievable by increasing the internal precision to store the product (int coordinate * float scale) without loss. (eg with size of double).

The other scenario will always be broken, if you do:
SDL_SetRenderScale
SDL_SetClipRect
Change to another scale. SDL_SetRenderScale( other scale ) // internally the clip is scaled.
GetClipRect // here you get the clip casted to (int)
SetClipRect // set a different clip that what was internally.

GetClipRect then SetClip won't actually set back exactly the same clip state. 

The work-around:
1) Use the SDL_round inside the SDL_GetClipRect() seem to work in some case, but won't work for other case. Ant the clip rect is not representative of the real clip/viewport that would get used in rendering.

2) Adding GetClipF SetClipF:  the single precision isn't actually enough.







Okay, great.