SDL_RenderSetClipRect BUG
A negative width or height is always an invalid rectangle, and can't be used for clipping. What is the bug here?
> 

but direct3d9 and direct3d12 
if<0  then =0
direct3d9 and opengl present different results
Ah good point. 

This is fixed, thanks!
I think>=0 is more appropriate 😁
What does setting a clip rect of width or height 0 do on the various backends?
> What does setting a clip rect of width or height 0 do on the various backends?

My English is poor, and I can't describe it in English, just to make it unable to display any content

Just suggestions
I'm not at all familiar with the code, but I can at least confirm that it being `rect->w > 0 && rect->h > 0` instead of `rect->w >= 0 && rect->h >= 0` causes a variety of issues for Wesnoth (https://github.com/wesnoth/wesnoth/issues/7864).

Using x11 driver and OpenGL renderer.
The "fix" has broken my GUI code. As commented by the guy above me, it should be >= 0 not > 0
A w/h value of 0 should be valid and should totally disable the drawing and is required when coding GUI. It was working perfectly in previous versions.
@Pentarctagon, @lukegrahamSydney, are #8228 and #8229 the change you are asking for? Please try whichever one is appropriate for the branch of SDL your games are using (I know Wesnoth is SDL 2, I don't know what  @lukegrahamSydney is developing) and confirm whether it fixes the issue. Or if that change is not what you wanted, please propose a different PR.
Yep, that's the change that fixed Wesnoth's issues when I tested previously.