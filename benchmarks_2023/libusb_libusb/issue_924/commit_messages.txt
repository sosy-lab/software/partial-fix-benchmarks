core: allow libusb_set_option on the default context before libusb_init

This commit updates libusb_set_option to save the options if setting them on the default
context. This ensures the options 1) can be set before libusb_init(NULL, ...), and 2)
are honored even if the default context is destroyed and re-created.

Signed-off-by: Nathan Hjelm <hjelmn@google.com>
core: ensure that all devices are properly cleaned up on libusb_exit

When cleaning up the context on libusb_exit the last step is to to call
hotplug_exit. This function took one pass over the devices and released any
devices where the reference count had reached 0. All remaining devices
were assumed to have leaked references and a warning message was printed
out. Unfortunately, this cleanup was too simplistic. It ignored the
references created when a device was the parent of another device (which
takes a reference). This reference is released when the child device is
released.

To ensure that we do not erroneously warn about devices with leftover
references the code now loops over the device list until no more devices
have a single reference. This ensures that we will eventually remove
all devices with no external references.

Fixes #924

Signed-off-by: Nathan Hjelm <hjelmn@google.com>
core: really fix dangling devices

The prior fix made an incorrect assumption about the reference counting of
parent devices. This CL fixes the issue by checking if the parent device
should be removed from the list as well. This is needed as when the child
device is released the parent may also be released.

Closes #924

Signed-off-by: Nathan Hjelm <hjelmn@google.com>
darwin: release device parent reference when re-enumerating device

This commit fixes a dangling device reference that can occur when a device is
being re-enumerated. The problem is the code was unconditionally re-caching the
parent device which then takes a reference on the parent device. If the field
was already populated this reference was not needed. To ensure the code works
even if the parent device also changes during the re-enumeration the code
release the parent reference and grabs a fresh reference.

Fixes #924

Signed-off-by: Nathan Hjelm <hjelmn@google.com>
