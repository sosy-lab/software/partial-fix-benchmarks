doc: fix unescaped character

This was causing a warning during the documentation build.

PR-URL: https://github.com/libuv/libuv/pull/2797
Reviewed-By: Ben Noordhuis <info@bnoordhuis.nl>
Reviewed-By: Colin Ihrig <cjihrig@gmail.com>
win: work around QueryPerformanceCounter() turning backwards

This is not a great solution to the referenced bug, but it seems fair
as libuv is intended to be a platform abstraction library that (also)
deals with OS bugs.

Adding a regression test does not seem feasible.

Fixes: https://github.com/libuv/libuv/issues/1633
win, util: rearrange uv_hrtime

Rearrange math operations in uv_hrtime. This is a workaround for a
probable compiler bug in VS2019.

Fixes: https://github.com/libuv/libuv/issues/1633
win, util: rearrange uv_hrtime

Rearrange math operations in uv_hrtime. This is a workaround for a
probable compiler bug in VS2019.

Fixes: https://github.com/libuv/libuv/issues/1633

PR-URL: https://github.com/libuv/libuv/pull/2866
Reviewed-By: Anna Henningsen <anna@addaleax.net>
Reviewed-By: Ben Noordhuis <info@bnoordhuis.nl>
win, util: rearrange uv_hrtime

Rearrange math operations in uv_hrtime. This is a workaround for a
probable compiler bug in VS2019.

Fixes: https://github.com/libuv/libuv/issues/1633

PR-URL: https://github.com/libuv/libuv/pull/2866
Reviewed-By: Anna Henningsen <anna@addaleax.net>
Reviewed-By: Ben Noordhuis <info@bnoordhuis.nl>
