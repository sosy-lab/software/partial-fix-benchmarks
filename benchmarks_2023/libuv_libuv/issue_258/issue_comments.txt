test: tcp_oob occassionally times out
@bnoordhuis how do you build io.js on air? It must take ages.

I'm a very patient person.  Also, ccache.

@bnoordhuis where have you been before. do you know how much knee overheat pain you might solved if you'd let me know about ccache earlier!

This is a dtruss output:

```
pipe(0x10AAEBB70, 0x7FFF92FAF1B4, 0x7FFF5555F5A0)        = 3 0
ioctl(0x3, 0x20006601, 0x4)      = 0 0
ioctl(0x4, 0x20006601, 0x0)      = 0 0
write(0x4, "*\0", 0x1)       = 1 0
kqueue(0x4, 0x7FFF5555F9FF, 0x1)         = 5 0
ioctl(0x5, 0x20006601, 0x0)      = 0 0
pipe(0x5, 0x20006601, 0x0)       = 6 0
ioctl(0x6, 0x20006601, 0x7)      = 0 0
ioctl(0x7, 0x20006601, 0x0)      = 0 0
ioctl(0x6, 0x8004667E, 0x7FFF5555FA64)       = 0 0
ioctl(0x7, 0x8004667E, 0x7FFF5555FA64)       = 0 0
pipe(0x7, 0x8004667E, 0x7FFF5555FA64)        = 8 0
ioctl(0x8, 0x20006601, 0x9)      = 0 0
ioctl(0x9, 0x20006601, 0x0)      = 0 0
ioctl(0x8, 0x8004667E, 0x7FFF5555FA14)       = 0 0
ioctl(0x9, 0x8004667E, 0x7FFF5555FA14)       = 0 0
open("/\0", 0x0, 0xC)        = 10 0
ioctl(0xA, 0x20006601, 0x0)      = 0 0
socket(0x2, 0x1, 0x0)        = 11 0
ioctl(0xB, 0x8004667E, 0x7FFF5555FA64)       = 0 0
ioctl(0xB, 0x20006601, 0x0)      = 0 0
setsockopt(0xB, 0xFFFF, 0x1022)      = 0 0
setsockopt(0xB, 0xFFFF, 0x100)       = 0 0
setsockopt(0xB, 0xFFFF, 0x4)         = 0 0
bind(0xB, 0x7FFF5555FB38, 0x10)      = 0 0
listen(0xB, 0x1, 0x10)       = 0 0
socket(0x2, 0x1, 0x0)        = 12 0
ioctl(0xC, 0x8004667E, 0x7FFF5555FA64)       = 0 0
ioctl(0xC, 0x20006601, 0x0)      = 0 0
setsockopt(0xC, 0xFFFF, 0x1022)      = 0 0
setsockopt(0xC, 0x6, 0x1)        = 0 0
setsockopt(0xC, 0xFFFF, 0x100)       = 0 0
connect(0xC, 0x7FFF5555FB38, 0x10)       = -1 Err#36
kevent(0x5, 0x7FFF55557A90, 0x4)         = 2 0
getsockopt(0xC, 0xFFFF, 0x1007)      = 0 0
accept(0xB, 0x0, 0x0)        = 13 0
ioctl(0xD, 0x20006601, 0x0)      = 0 0
ioctl(0xD, 0x8004667E, 0x7FFF55557924)       = 0 0
setsockopt(0xD, 0xFFFF, 0x100)       = 0 0
ioctl(0xC, 0x8004667E, 0x7FFF55557924)       = 0 0
sendto(0xC, 0x10A6F8B48, 0x5)        = 5 0
sendto(0xC, 0x10A6F8B48, 0x5)        = 5 0
ioctl(0xC, 0x8004667E, 0x7FFF55557924)       = 0 0
kevent(0x5, 0x7FFF55557A90, 0x1)         = 1 0
kevent(0x5, 0x7FFF55557A70, 0x1)         = 0 0
```

It seems that `sendto` does not trigger kevent. Digging in more.

@bnoordhuis I think kernel is our enemy here:
- https://github.com/opensource-apple/xnu/blob/10.10/bsd/kern/uipc_socket.c#L5473-L5482
- https://github.com/opensource-apple/xnu/blob/10.10/bsd/kern/uipc_socket.c#L5503-L5508

See, basically it sets EV_OOBAND in pt1 and fall to pt2, returning 0 and ignoring `kevent()`. I guess that what we are hitting is a situation where kernel handles two packets in one pass. I wonder if we could just run `sendto` in a loop in another thread, until the `kevent()` will be triggered. This sounds nasty, but I don't see how we could workaround it.

What do you think?

Hm... it may work with SS_RCVATMARK, actually. Will try to figure out the way to set it.

Oh, this is internal thing. I guess we are fighting TCP RFC then :)

@bnoordhuis does this patch help?

``` diff
diff --git a/test/test-tcp-oob.c b/test/test-tcp-oob.c
index fc011ee..a051bcf 100644
--- a/test/test-tcp-oob.c
+++ b/test/test-tcp-oob.c
@@ -84,14 +84,14 @@ static void connection_cb(uv_stream_t* handle, int status) {
    * triggering `kevent()` for the first one
    */
   do {
-    r = send(fd, "hello", 5, MSG_OOB);
+    r = send(fd, "h", 1, MSG_OOB);
   } while (r < 0 && errno == EINTR);
-  ASSERT(5 == r);
+  ASSERT(1 == r);

   do {
-    r = send(fd, "hello", 5, MSG_OOB);
+    r = send(fd, "h", 1, MSG_OOB);
   } while (r < 0 && errno == EINTR);
-  ASSERT(5 == r);
+  ASSERT(1 == r);

   ASSERT(0 == uv_stream_set_blocking((uv_stream_t*) &client_handle, 0));
 }
```

@saghul: @indutny's patch definitely helps. I could't reproduce with it.

Want me to send a PR?

@indutny I'll land it for you

Landed in 3346082. Thanks @indutny for the patch and @Fishrock123 for testing!

Yay, thank y'all!

This change actually causes tcp_oob to timeout on my system (Linux Mint 17).  The current v1.x has the timeout, but commit 2eb1c18 works.

Can this issue be reopenned, or is a new one needed?

That commit just changes the README, it has no effect in the code being run. Are you sure?

@saghul I think @coreyfarrell ment `git checkout 2eb1c18`

Yes, if I checkout 2eb1c18 (currently HEAD~1) the test works, latest v1.x fails.

Looks like I now get it as well :-S What the...

I'm getting it consistently on Linux too now, unless I revert 3346082.

@bnoordhuis same here :-( Looks like what fixes on OSX breaks it on Linux, for some reason... I can revert and then we reevaluate. Are you ok with that?

Sounds like a plan.

Done in 3e405d9

Closing because I haven't seen this test fail in a long time.  Can reopen if it's still an issue.