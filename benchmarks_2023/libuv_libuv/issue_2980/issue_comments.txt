uv__threadpool_cleanup non-op on Windows?
c8abb29f68abf64bacf2dc5a483eedab84b52428 changed this in 2014, but since we’re using `__attribute__((destructor))` on `uv_library_shutdown()`, I’m assuming that this isn’t being automatically run on Windows anyway.

I think just removing the `#ifdef` should be fine.
https://github.com/libuv/libuv/commit/c8abb29f68abf64bacf2dc5a483eedab84b52428 is one of two commits from https://github.com/joyent/libuv/pull/1381.
@addaleax :
> I think just removing the #ifdef should be fine.

Yeah, not seeing anything in local testing that would suggest otherwise. I'll get a PR opened.
This issue has been automatically marked as stale because it has not had recent activity. It will be closed if no further activity occurs. Thank you for your contributions.
