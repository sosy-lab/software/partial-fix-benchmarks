Multiple crashes when using vips to convert images
Hello @HongxuChen, thank you for these images. I'll investigate. 
I fixed the TIFF one, thanks for that. It was failing to check for non-byte multiple sample sizes for some types of tiff file.

The sigtrap ones are caused by your environment, I think. You have set the environment variable `G_DEBUG` to `fatal-warnings` and that's making it fail on the first warning message. If you undefine that variable, it'll exit with an error code instead.

The Radiance stuff looks serious, I'll do that next. The code is a copy-paste from Greg Ward's Radiance project (with permission). Have you fuzzed that?

https://www.radiance-online.org

If I find something I can send a note, if you like.
I will follow your suggestions to reset `G_DEBUG`. 
I haven't fuzzed on Radiance; and thank your for your information about that!
Our fuzzer detected a few other crashes with the same settings against commit 0077017a, POCs are [in this directory](https://github.com/ntu-sec/pocs/tree/master/libvips-0077017a/crashes). A difference from the previous build is that I also used the deb package `libmatio4:amd64` from Ubuntu 18.04.

I'm not familiar with glib, but on my machine, when I unset G_DEBUG or set it to other options like gc-friendly, it still caused SIGTRAP. Since it can be definitely resolved, I simply ignored them.

(I also accidentally found another issue regarding input file name parameter, please see #1040).
I checked back with radiance and they seem to have mostly rewritten their code. I'm copy-pasting in their new stuff at the moment and hopefully that'll address that set of issues. It's a shame they don't have a load library I can simply link against.

Could you test with `copy` rather than `im_copy`? The `im_` prefix is part of the vips7 compatibility layer. Remove that and you'll hit the more common (and more complex) vips8 path.
I pasted in the latest stable radiance code, but at least this example hbo_radiance.c_389_1 still crashes. 

The problem comes from globmatch() expanding into a 64-character buffer here:

https://radiance-online.org/cgi-bin/viewcvs.cgi/ray/src/common/header.c?view=markup#l231

which is usually allocated on the stack.

I mailed the rad list about this, I'll see if there's interest in fixing it.
Ok, we'll test on the latest version with `copy` to see what happens.
I think that's everything fixed. Or at least if it fails now, it should be somewhere else!

Thank you for submitting this issue, @HongxuChen 
That's great and my pleasure! Will double check tomorrow.
OK, let's make a new issue when / if you find more crashes.
Oh, maybe I found the SIGTRAP thing.

If you build libvips with debug enabled, it will abort() on the first warning message. Perhaps you configured with `--debug=yes`?
Yes, I configured with debug option.
Files below still cause out-of-bound write on my machine as of 5fc8bf54:

https://github.com/ntu-sec/pocs/blob/master/libvips-35658082/crashes/invalids/hbo_radiance.c_728_1?raw=true
https://github.com/ntu-sec/pocs/blob/master/libvips-35658082/crashes/invalids/hbo_radiance.c_728_2?raw=true

and also files in https://github.com/ntu-sec/pocs/tree/master/libvips-0077017a/crashes (I additionally linked /usr/lib/x86_64-linux-gnu/libmatio.so.4).

Ah, the matio ones, I'd forgotten, sorry. I'll look at the rad stuff again too.
OK, fixed a couple more, thanks!

The matlab ones seem to be in libmatio. If I run hbo_matlab.c_119_1.mat in valgrind, for example, I see:

```
#8  0x0000000004fe567d in read_new (
    filename=0x1922b580 "hbo_matlab.c_119_1.mat", out=0x1922bbf0)
    at matlab.c:119
119			if( !(read->var = Mat_VarReadNextInfo( read->mat )) ) {
(gdb) bt
#0  0x0000000005a7de97 in __GI_raise (sig=sig@entry=6)
    at ../sysdeps/unix/sysv/linux/raise.c:51
#1  0x0000000005a7f801 in __GI_abort () at abort.c:79
#2  0x0000000005ac8897 in __libc_message (action=action@entry=(do_abort | do_backtrace), fmt=fmt@entry=0x5bf5988 "*** %s ***: %s terminated\n")
    at ../sysdeps/posix/libc_fatal.c:181
#3  0x0000000005b73cff in __GI___fortify_fail_abort (need_backtrace=need_backtrace@entry=true, msg=msg@entry=0x5bf5905 "buffer overflow detected")
    at fortify_fail.c:33
#4  0x0000000005b73d21 in __GI___fortify_fail (msg=msg@entry=0x5bf5905 "buffer overflow detected") at fortify_fail.c:44
#5  0x0000000005b71a10 in __GI___chk_fail () at chk_fail.c:28
#6  0x0000000005b7215d in __fread_chk (ptr=<optimised out>, ptrlen=<optimised out>, size=4, n=134217730, stream=0x1922db00) at fread_chk.c:39
#7  0x0000000009d77c06 in  () at /usr/lib/x86_64-linux-gnu/libmatio.so.4
#8  0x0000000004fe567d in read_new (filename=0x1922b580 "hbo_matlab.c_119_1.mat", out=0x1922bbf0) at matlab.c:119
```

hbo_matlab.c_285_1.mat is:

```
(gdb) bt
#0  0x0000000009d71c2f in  () at /usr/lib/x86_64-linux-gnu/libmatio.so.4
#1  0x0000000009d7210c in  () at /usr/lib/x86_64-linux-gnu/libmatio.so.4
#2  0x0000000009d7c943 in Mat_VarReadDataAll ()
```

That libmatio tracker is here:

https://sourceforge.net/p/matio/discussion/

Shall I try to open an issue, or would you rather do it?
Yes, it should be a libmatio issue. You can open an issue there (honestly speaking we are not familiar with either libmatio or libvips).
OK, and I'll link here.

Hopefully this issue is done now.
:ok_hand: 