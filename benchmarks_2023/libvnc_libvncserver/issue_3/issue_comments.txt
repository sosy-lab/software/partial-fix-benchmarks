Fixes for multithreaded Windows applications
How about opening a pull request instead?

Can you please explain what to do? I'm new here and not familiar with this development platform. What kind of file can be used for it? I have a "unified diff" lying around here and it should apply to version 0.9.9 and the latest development sources.

You could paste your diff into a comment, between the usual triple backticks and marking the pasted text as 'diff'; see https://help.github.com/articles/github-flavored-markdown for details.

If you want to learn how to use Git or GitHub efficiently, I encourage you to look at https://help.github.com/

It still looks complicated to me. If there's no easy way to submit a patch, can you tell me where I can send it to?

> It still looks complicated to me.

Is it really complicated to paste it into a comment here? ;-)

Alternatively, there is a mailing list: libvncserver-common@lists.sourceforge.net

@vruppert it's all explained here: https://help.github.com/articles/using-pull-requests

Hi!

Am 02.09.2014 13:27, schrieb Christian Beier:

> @vruppert https://github.com/vruppert it's all explained here:
> https://help.github.com/articles/using-pull-requests

Now I'll send the patch to the maling list and I hope that the people 
think it's useful.

Thanks Volker

@vruppert Please use a pull request instead.

For interested parties: my response to Volker's mail is archived [here](http://sourceforge.net/p/libvncserver/mailman/message/32790733/). There are substantial problems with the patch, hence I asked to provide it as a PR so that we can communicate about, and work on, the patch efficiently.

This seems irrelevant anymore, as the MSVC adjustments already include using critical sections

@danielgindi Where? Grepping for 'CriticalSection' in the repo only yields libvncclient/tls_openssl.c ...

It's actually `CRITICAL_SECTION` :-)
And yes, that is the only place where it is relevant. As other appearances of `mutex` in the code are not compiled for WIN32.
The fact that both `libvncclient` and `libvncserver` now compiles on MSVC proves that it is true!

a cursory glance at the code appears to disagree with that. many files in libvncserver use rfb.h macros LOCK and UNLOCK which are defined as no-ops if libpthread is not available.

@0maxxam0 has funded $20.00 to this issue.

---
- Submit pull request via [IssueHunt](https://issuehunt.io/repos/18277887/issues/3) to receive this reward.
- Want to contribute? Chip in to this issue via [IssueHunt](https://issuehunt.io/repos/18277887/issues/3).
- Checkout the [IssueHunt Issue Explorer](https://issuehunt.io/issues) to see more funded issues.
- Need help from developers? [Add your repository](https://issuehunt.io/r/new) on IssueHunt to raise funds.
Mentioned patch by @vruppert can be found [here](https://sourceforge.net/p/libvncserver/mailman/attachment/5406062D.4060606%40t-online.de/1/)
https://stackoverflow.com/questions/800383/what-is-the-difference-between-mutex-and-critical-section 
https://cmake.org/cmake/help/v3.2/module/FindThreads.html
[@bk138](https://issuehunt.io/u/bk138) has rewarded $18.00 to [@bk138](https://issuehunt.io/u/bk138). [See it on IssueHunt](https://issuehunt.io/repos/18277887/issues/3)

- :moneybag: Total deposit: $20.00
- :tada: Repository reward(0%): $0.00
- :wrench: Service fee(10%): $2.00
Complicated process, that issuehunt :sweat_smile: 