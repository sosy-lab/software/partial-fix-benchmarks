MAP_32BIT on SSE based platforms
Fixed as of above commit. Feel free to reopen the issue if not fixed.
Seems to be still an issue:

```
[xgemm]$ lscpu
Architecture:                    x86_64
CPU op-mode(s):                  32-bit, 64-bit
Byte Order:                      Little Endian
Address sizes:                   39 bits physical, 48 bits virtual
CPU(s):                          4
On-line CPU(s) list:             0-3
Thread(s) per core:              1
Core(s) per socket:              4
Socket(s):                       1
NUMA node(s):                    1
Vendor ID:                       GenuineIntel
CPU family:                      6
Model:                           122
Model name:                      Intel(R) Celeron(R) N4100 CPU @ 1.10GHz
Stepping:                        1
CPU MHz:                         1555.928
CPU max MHz:                     2400.0000
CPU min MHz:                     800.0000
BogoMIPS:                        2188.80
L1d cache:                       96 KiB
L1i cache:                       128 KiB
L2 cache:                        4 MiB
NUMA node0 CPU(s):               0-3
Vulnerability Itlb multihit:     Not affected
Vulnerability L1tf:              Not affected
Vulnerability Mds:               Not affected
Vulnerability Meltdown:          Mitigation; PTI
Vulnerability Spec store bypass: Mitigation; Speculative Store Bypass disabled via prctl and seccomp
Vulnerability Spectre v1:        Mitigation; usercopy/swapgs barriers and __user pointer sanitization
Vulnerability Spectre v2:        Mitigation; Enhanced IBRS, IBPB conditional, RSB filling
Vulnerability Srbds:             Not affected
Vulnerability Tsx async abort:   Not affected
Flags:                           fpu vme de pse tsc msr pae mce cx8 apic sep mtrr pge mca cmov pat pse36 clflush dts acpi mmx fxsr sse sse2 ss ht tm pbe syscall nx pdpe1gb rdtscp lm consta
                                 nt_tsc art arch_perfmon pebs bts rep_good nopl xtopology nonstop_tsc cpuid aperfmperf tsc_known_freq pni pclmulqdq dtes64 ds_cpl est tm2 ssse3 sdbg cx16 xt
                                 pr pdcm sse4_1 sse4_2 x2apic movbe popcnt tsc_deadline_timer aes xsave rdrand lahf_lm 3dnowprefetch cpuid_fault cat_l2 pti cdp_l2 ssbd ibrs ibpb stibp ibrs
                                 _enhanced fsgsbase tsc_adjust smep erms mpx rdt_a rdseed smap clflushopt intel_pt sha_ni xsaveopt xsavec xgetbv1 xsaves dtherm ida arat pln pts umip rdpid
                                 md_clear arch_capabilities
[xgemm]$ uname -a
Linux 5.10.7-200.fc33.x86_64 #1 SMP Tue Jan 12 20:20:11 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
[xgemm]$ LIBXSMM_VERBOSE=-1 ./kernel 32 32 32 32 32 32 1 1 0 0 0 0 nopf SP nobr 1 0 10 0
------------------------------------------------
RUNNING (32x32) X (32x32) = (32x32), SP, BR=1
------------------------------------------------
LIBXSMM: prepared to pin threads.
LIBXSMM-JIT-DUMP(ptr:file)**0x7f030fc7c000** : libxsmm_wsm_f32_nn_32x32x32_32_32_32_a1_b1_p0_br0_uh0_si0_tc-abid_avnni0_bvnni0_cvnni0_decompress_A0_spfactor1.mxm
function pointer address: 7f030fc7c000
0.000878s for creating jit
0.000179s for C
3.657624 GFLOPS for C
0.000078s for libxsmm
8.451047 GFLOPS for libxsmm
max. error: 0.000000
------------------------------------------------


 Total Max Error 0.000000


LIBXSMM_VERSION: master-1.16.1-1114 (25183322)
WSM/SP      TRY    JIT    STA    COL
   0..13      0      0      0      0
  14..23      0      0      0      0
  24..64      1      1      0      0
Registry and code: 14 MB + 8 KB (gemm=1)
Uptime: 0.007136 s
```

Ok, the pointer is clearly above the 32-bit range. I will try to reproduce and fix it.
Fixed and verified on Celeron J3455.