diff --git a/src/libxsmm_malloc.c b/src/libxsmm_malloc.c
index 5c6acaf55..02831d7a8 100644
--- a/src/libxsmm_malloc.c
+++ b/src/libxsmm_malloc.c
@@ -1145,7 +1145,7 @@ LIBXSMM_API_INTERN int internal_xfree(const void* memory, internal_malloc_info_t
     }
   }
 #if !defined(LIBXSMM_BUILD)
-  else if ((LIBXSMM_VERBOSITY_WARN <= libxsmm_verbosity || 0 > libxsmm_verbosity) /* library code is expected to be mute */
+  else if ((LIBXSMM_VERBOSITY_WARN <= libxsmm_verbosity || 0 > libxsmm_verbosity)
     && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
   {
     fprintf(stderr, "LIBXSMM WARNING: attempt to release memory from non-matching implementation!\n");
@@ -2195,25 +2195,48 @@ LIBXSMM_API_INTERN int libxsmm_malloc_xattrib(void* buffer, int flags, size_t si
 #if defined(_WIN32)
       /* TODO: implement memory protection under Microsoft Windows */
 #else
-      const int result_mprotect = mprotect(buffer, size/*entire memory region*/, PROT_READ);
-#   if (defined(__APPLE__) && defined(__arm64__)) || 1
-      static int error_once = 0;
-      LIBXSMM_UNUSED(result_mprotect);
-      if ( 0 != libxsmm_verbosity /* library code is expected to be mute */
-        && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
-      {
-        fprintf(stderr, "LIBXSMM ERROR: failed to mark executable buffer as read-only!\n");
+      const int result_mprotect = mprotect(buffer, size, PROT_READ);
+# if defined(__APPLE__) && defined(__arm64__)
+      if (EXIT_SUCCESS != result_mprotect) {
+        static int error_once = 0;
+        if ((LIBXSMM_VERBOSITY_HIGH <= libxsmm_verbosity || 0 > libxsmm_verbosity)
+          && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
+        {
+          fprintf(stderr, "LIBXSMM WARNING: failed to mark buffer as read-only!\n");
+        }
       }
-#   else
+# else
       result = result_mprotect;
-#   endif
+# endif
 #endif
     }
     else { /* executable buffer requested */
 #if defined(_WIN32)
       /* TODO: implement memory protection under Microsoft Windows */
-#else
-      result = mprotect(buffer, size/*entire memory region*/, PROT_READ | PROT_EXEC);
+#else /* treat memory protection errors as soft error; ignore return value */
+# if defined(__APPLE__) && defined(__arm64__)
+      if (0 == (LIBXSMM_MALLOC_FLAG_W & flags)) {
+        pthread_jit_write_protect_np(1/*true*/);
+      }
+# else
+      const int result_mprotect = mprotect(buffer, size, PROT_READ | PROT_EXEC);
+      if (EXIT_SUCCESS != result_mprotect) {
+        static int error_once = 0;
+        if (0 != libxsmm_se) { /* hard-error in case of SELinux */
+          if (0 != libxsmm_verbosity /* library code is expected to be mute */
+            && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
+          {
+            fprintf(stderr, "LIBXSMM ERROR: failed to allocate an executable buffer!\n");
+          }
+          result = result_mprotect;
+        }
+        else if ((LIBXSMM_VERBOSITY_HIGH <= libxsmm_verbosity || 0 > libxsmm_verbosity) /* library code is expected to be mute */
+          && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
+        {
+          fprintf(stderr, "LIBXSMM WARNING: read-only request for JIT-buffer failed!\n");
+        }
+      }
+# endif
 #endif
     }
   }
@@ -2240,15 +2263,8 @@ LIBXSMM_API_INTERN int libxsmm_malloc_attrib(void** memory, int flags, const cha
     if (0 == (LIBXSMM_MALLOC_FLAG_W & flags) || 0 != (LIBXSMM_MALLOC_FLAG_X & flags)) {
       const size_t alignment = (size_t)(((const char*)(*memory)) - ((const char*)buffer));
       const size_t alloc_size = apply_size + alignment;
-      int xattrib_result;
       if (0 == (LIBXSMM_MALLOC_FLAG_X & flags)) { /* data-buffer; non-executable */
-        xattrib_result = libxsmm_malloc_xattrib(buffer, flags, alloc_size);
-        if (EXIT_SUCCESS != xattrib_result
-          && (LIBXSMM_VERBOSITY_HIGH <= libxsmm_verbosity || 0 > libxsmm_verbosity)
-          && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
-        {
-          fprintf(stderr, "LIBXSMM WARNING: marking buffer as read-only failed!\n");
-        }
+        result = libxsmm_malloc_xattrib(buffer, flags, alloc_size);
       }
       else { /* executable buffer requested */
         void *const code_ptr = (NULL != info->reloc ? ((void*)(((char*)info->reloc) + alignment)) : *memory);
@@ -2316,29 +2332,8 @@ LIBXSMM_API_INTERN int libxsmm_malloc_attrib(void** memory, int flags, const cha
             /* info size minus actual hash value */
             (unsigned int)(((char*)&info->hash) - ((char*)info))));
 #   endif
-# endif   /* treat memory protection errors as soft error; ignore return value */
-# if defined(__APPLE__) && defined(__arm64__)
-          if (0 == (LIBXSMM_MALLOC_FLAG_W & flags)) {
-            pthread_jit_write_protect_np(1/*true*/);
-          }
-# else
-          xattrib_result = libxsmm_malloc_xattrib(buffer, flags, alloc_size);
-          if (EXIT_SUCCESS != xattrib_result) {
-            if (0 != libxsmm_se) { /* hard-error in case of SELinux */
-              if (0 != libxsmm_verbosity /* library code is expected to be mute */
-                && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
-              {
-                fprintf(stderr, "LIBXSMM ERROR: failed to allocate an executable buffer!\n");
-              }
-              result = xattrib_result;
-            }
-            else if ((LIBXSMM_VERBOSITY_HIGH <= libxsmm_verbosity || 0 > libxsmm_verbosity) /* library code is expected to be mute */
-              && 1 == LIBXSMM_ATOMIC_ADD_FETCH(&error_once, 1, LIBXSMM_ATOMIC_RELAXED))
-            {
-              fprintf(stderr, "LIBXSMM WARNING: read-only request for JIT-buffer failed!\n");
-            }
-          }
 # endif
+          result = libxsmm_malloc_xattrib(buffer, flags, alloc_size);
         }
 #endif
       }
