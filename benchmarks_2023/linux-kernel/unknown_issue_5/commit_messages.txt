scripts/kernel-doc: optionally treat warnings as errors

The kbuild bot recently added the W=1 option, which triggered
documentation cleanups to squelch hundreds of kernel-doc warnings.

To make sure new kernel contributions don't add regressions to
kernel-doc descriptors, this patch suggests an option to treat
warnings as errors in CI/automated tests.

A -Werror command-line option is added to the kernel-doc script. When
this option is set, the script will return the number of warnings
found. The caller can then treat this positive return value as an
error and stop the build.

Using this command line option is however not straightforward when the
kernel-doc script is called from other scripts. To align with typical
kernel compilation or documentation generation, the Werror option is
also set by checking the KCFLAGS environment variable, or if
KDOC_WERROR is defined, as in the following examples:

KCFLAGS="-Wall -Werror" make W=1 sound/
KCFLAGS="-Wall -Werror" make W=1 drivers/soundwire/
KDOC_WERROR=1 make htmldocs

Note that in the last example the documentation build does not stop,
only an additional log is provided.

Credits to Randy Dunlap for suggesting the use of environment variables.

Suggested-by: Randy Dunlap <rdunlap@infradead.org>
Signed-off-by: Pierre-Louis Bossart <pierre-louis.bossart@linux.intel.com>
Link: https://lore.kernel.org/r/20200728162040.92467-1-pierre-louis.bossart@linux.intel.com
Signed-off-by: Jonathan Corbet <corbet@lwn.net>


-----------------------------------------------------------------------------------------------------------------

scripts/kernel-doc: match -Werror flag strictly

In our CI testing, we use some commands as below to only turn a specific
type of warnings into errors, but we notice that kernel-doc warnings
are also turned into errors unexpectedly.

$ make KCFLAGS="-Werror=return-type" W=1 kernel/fork.o

kernel/fork.c:1406: warning: Function parameter or member 'mm' not described in 'set_mm_exe_file'
kernel/fork.c:1406: warning: Function parameter or member 'new_exe_file' not described in 'set_mm_exe_file'
kernel/fork.c:1441: warning: Function parameter or member 'mm' not described in 'replace_mm_exe_file'
kernel/fork.c:1441: warning: Function parameter or member 'new_exe_file' not described in 'replace_mm_exe_file'
kernel/fork.c:1491: warning: Function parameter or member 'mm' not described in 'get_mm_exe_file'
kernel/fork.c:1510: warning: Function parameter or member 'task' not described in 'get_task_exe_file'
kernel/fork.c:1534: warning: Function parameter or member 'task' not described in 'get_task_mm'
kernel/fork.c:2109: warning: bad line:
kernel/fork.c:2130: warning: Function parameter or member 'ret' not described in '__pidfd_prepare'
kernel/fork.c:2130: warning: Excess function parameter 'pidfd' description in '__pidfd_prepare'
kernel/fork.c:2179: warning: Function parameter or member 'ret' not described in 'pidfd_prepare'
kernel/fork.c:2179: warning: Excess function parameter 'pidfd' description in 'pidfd_prepare'
kernel/fork.c:3195: warning: expecting prototype for clone3(). Prototype was for sys_clone3() instead
13 warnings as Errors
make[3]: *** [scripts/Makefile.build:243: kernel/fork.o] Error 13
make[3]: *** Deleting file 'kernel/fork.o'
make[2]: *** [scripts/Makefile.build:480: kernel] Error 2
make[1]: *** [/root/linux/Makefile:1913: .] Error 2
make: *** [Makefile:234: __sub-make] Error 2

>From the git history, commit 2c12c8103d8f ("scripts/kernel-doc:
optionally treat warnings as errors") introduces a new command-line
option to make kernel-doc warnings into errors. It can also read the
KCFLAGS environment variable to decide whether to turn this option on,
but the regex used for matching may not be accurate enough. It can match
both "-Werror" and "-Werror=<diagnostic-type>", so the option is turned
on by mistake in the latter case.

Fix this by strictly matching the flag "-Werror": there must be a space
or start of string in the front, and a space or end of string at the
end. This can handle all the following cases correctly:

KCFLAGS="-Werror" make W=1 [MATCH]
KCFLAGS="-Werror=return-type" make W=1 [NO MATCH]
KCFLAGS="-Wcomment -Werror -Wundef" make W=1 [MATCH]
KCFLAGS="-Wcomment -Werror=return-type -Wundef" make W=1 [NO MATCH]

Fixes: 2c12c8103d8f ("scripts/kernel-doc: optionally treat warnings as errors")
Signed-off-by: Yujie Liu <yujie.liu@intel.com>
Signed-off-by: Jonathan Corbet <corbet@lwn.net>
Message-ID: <20231019095637.2471840-1-yujie.liu@intel.com>


-----------------------------------------------------------------------------------------------------------------

scripts/kernel-doc: Fix the regex for matching -Werror flag

Swarup reported a "make htmldocs" warning:

  Variable length lookbehind is experimental in regex;
  marked by <-- HERE in m/(?<=^|\s)-Werror(?=$|\s)
  <-- HERE / at ./scripts/kernel-doc line 188.

Akira managed to reproduce it by perl v5.34.0.

On second thought, it is not necessary to have the complicated
"lookahead and lookbehind" things, and the regex can be simplified.

Generally, the kernel-doc warnings should be considered as errors only
when "-Werror" flag is set in KCFLAGS, but not when
"-Werror=<diagnostic-type>" is set, which means there needs to be a
space or start of string before "-Werror", and a space or end of string
after "-Werror".

The following cases have been tested to work as expected:

* kernel-doc warnings are considered as errors:

  $ KCFLAGS="-Werror" make W=1
  $ KCFLAGS="-Wcomment -Werror" make W=1
  $ KCFLAGS="-Werror -Wundef" make W=1
  $ KCFLAGS="-Wcomment -Werror -Wundef" make W=1

* kernel-doc warnings remain as warnings:

  $ KCFLAGS="-Werror=return-type" make W=1
  $ KCFLAGS="-Wcomment -Werror=return-type" make W=1
  $ KCFLAGS="-Werror=return-type -Wundef" make W=1
  $ KCFLAGS="-Wcomment -Werror=return-type -Wundef" make W=1

The "Variable length lookbehind is experimental in regex" warning is
also resolved by this patch.

Fixes: 91f950e8b9d8 ("scripts/kernel-doc: match -Werror flag strictly")
Reported-by: Swarup Laxman Kotiaklapudi <swarupkotikalapudi@gmail.com>
Signed-off-by: Yujie Liu <yujie.liu@intel.com>
Closes: https://lore.kernel.org/r/20231028182231.123996-1-swarupkotikalapudi@gmail.com/
Reviewed-by: Akira Yokosawa <akiyks@gmail.com>
Signed-off-by: Jonathan Corbet <corbet@lwn.net>
Link: https://lore.kernel.org/r/20231030085404.3343403-1-yujie.liu@intel.com
