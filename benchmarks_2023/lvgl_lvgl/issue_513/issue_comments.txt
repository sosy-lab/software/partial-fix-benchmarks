Many Warning
hi,
maybe you should use gcc tool chain?
Keil is an integrated development tool, can't replace the compiler。

Can you consider modifying the code and removing the default branch?

Hi
you can ignor warnings like those "..\lvgl\lv_objx\lv_ta.c(767): warning: #111-D: statement is unreachable"
@chenshijianworkgit I have never used Keil, but is there no way to turn off these warnings?

For GCC (and probably other compilers), when optimization is enabled, this kind of duplication is automatically eliminated so it's more of a clarity question.
Maybe the function can be changed from:
```c
lv_style_t * lv_ta_get_style(const lv_obj_t * ta, lv_ta_style_t type)
{
    lv_ta_ext_t * ext = lv_obj_get_ext_attr(ta);

    switch(type) {
        case LV_TA_STYLE_BG:
            return lv_page_get_style(ta, LV_PAGE_STYLE_BG);
        case LV_TA_STYLE_SB:
            return lv_page_get_style(ta, LV_PAGE_STYLE_SB);
        case LV_TA_STYLE_CURSOR:
            return ext->cursor.style;
        default:
            return NULL;
    }

    /*To avoid warning*/
    return NULL;
}
```

to:
```c
lv_style_t * lv_ta_get_style(const lv_obj_t * ta, lv_ta_style_t type)
{
    lv_style_t * style = NULL;
    lv_ta_ext_t * ext = lv_obj_get_ext_attr(ta);

    switch(type) {
        case LV_TA_STYLE_BG:
            style = lv_page_get_style(ta, LV_PAGE_STYLE_BG);
            break;
        case LV_TA_STYLE_SB:
            style = lv_page_get_style(ta, LV_PAGE_STYLE_SB);
            break;
        case LV_TA_STYLE_CURSOR:
            style = ext->cursor.style;
            break;
        default:
            break;
    }

    return style;
}
```
For keil, try using **#pragma  diag_suppress 188** or **#pragma  diag_suppress 111**
> Maybe the function can be changed from:
> 
> ```c
> lv_style_t * lv_ta_get_style(const lv_obj_t * ta, lv_ta_style_t type)
> {
>     lv_ta_ext_t * ext = lv_obj_get_ext_attr(ta);
> 
>     switch(type) {
>         case LV_TA_STYLE_BG:
>             return lv_page_get_style(ta, LV_PAGE_STYLE_BG);
>         case LV_TA_STYLE_SB:
>             return lv_page_get_style(ta, LV_PAGE_STYLE_SB);
>         case LV_TA_STYLE_CURSOR:
>             return ext->cursor.style;
>         default:
>             return NULL;
>     }
> 
>     /*To avoid warning*/
>     return NULL;
> }
> ```
> 
> to:
> 
> ```c
> lv_style_t * lv_ta_get_style(const lv_obj_t * ta, lv_ta_style_t type)
> {
>     lv_style_t * style = NULL;
>     lv_ta_ext_t * ext = lv_obj_get_ext_attr(ta);
> 
>     switch(type) {
>         case LV_TA_STYLE_BG:
>             style = lv_page_get_style(ta, LV_PAGE_STYLE_BG);
>             break;
>         case LV_TA_STYLE_SB:
>             style = lv_page_get_style(ta, LV_PAGE_STYLE_SB);
>             break;
>         case LV_TA_STYLE_CURSOR:
>             style = ext->cursor.style;
>             break;
>         default:
>             break;
>     }
> 
>     return style;
> }
> ```

yes，this method can be used
> For keil, try using **#pragma diag_suppress 188** or **#pragma diag_suppress 111**

I tested it，add "#pragma diag_suppress 111" can remove the warning message。
It is recommended to use the C47D code, so that no error will be reported in any compiler.
@kisvegabor, do you agree to change the `lv_ta_get_style` function to the new proposal?, i can open a PR for it.
Hi,

My attempt was to create a safer code where there is an explicit return at the end.
However, if you all say that the `return style;` version would be better I accept it.

@C47D 
So yes, I agree, please create a PR. Thank you very much in advance!


@kisvegabor Done at #522.

The `style` variable will be returned at the end of the `lv_ta_get_style` function, it does exactly the same as before, this change also makes `lv_ta_get_style` return in a single point, see [coding-tip-have-a-single-exit-point](https://www.tomdalling.com/blog/coding-tips/coding-tip-have-a-single-exit-point/).

Any thoughts @embeddedt, @chenshijianworkgit, @RocFan, @turoksama?
Sorry, I closed the issue by mistake.

@C47D Merged, thank you!

Based on this update, if you find related issues, please create further Pull requests.
Thanks @kisvegabor, i wonder if static analysis of the code can help us to find "issues" like this.
@C47D I'm sure it could help. I assume we can find tools on GitHub marketplace too. Do you have experience in this filed?
Not really, i'm trying to get familiar with clang sanitizers at the moment.
Hi，
Looking at the latest main branch code only modified the warning of the lv_ta_get_style function, and other functions I have listed here.

lv_arc_get_style
lv_bar_get_style
lv_btn_get_style
lv_calendar_get_style
lv_btnm_get_style
lv_cb_get_style
lv_cb_design
lv_ddlist_get_style
lv_kb_get_style
lv_list_get_style
lv_mbox_get_style
lv_preload_get_style
lv_page_get_style
lv_slider_get_style
lv_roller_get_style
lv_sw_get_style
lv_tabview_get_style
lv_win_get_style
lv_obj_get_child
lv_obj_get_child_back
hex_char_to_num
lv_img_color_format_get_px_size
lv_img_color_format_is_chroma_keyed
lv_img_color_format_has_alpha

Fixed all the functions except one, `lv_cb_design`:

```c
static bool lv_cb_design(lv_obj_t * cb, const lv_area_t * mask, lv_design_mode_t mode)
{
    if(mode == LV_DESIGN_COVER_CHK) {
        /*Return false if the object is not covers the mask_p area*/
        return ancestor_bg_design(cb, mask, mode);
    } else if(mode == LV_DESIGN_DRAW_MAIN || mode == LV_DESIGN_DRAW_POST) {
        lv_cb_ext_t * cb_ext = lv_obj_get_ext_attr(cb);
        lv_btn_ext_t * bullet_ext = lv_obj_get_ext_attr(cb_ext->bullet);

        /*Be sure the state of the bullet is the same as the parent button*/
        bullet_ext->state = cb_ext->bg_btn.state;

        return ancestor_bg_design(cb, mask, mode);

    } else {
        return ancestor_bg_design(cb, mask, mode);
    }

    return true;
}
```

i think the following code is correct:
```c
static bool lv_cb_design(lv_obj_t * cb, const lv_area_t * mask, lv_design_mode_t mode)
{
    bool result = true;

    if(mode == LV_DESIGN_COVER_CHK) {
        /*Return false if the object is not covers the mask_p area*/
        result = ancestor_bg_design(cb, mask, mode);
    } else if(mode == LV_DESIGN_DRAW_MAIN || mode == LV_DESIGN_DRAW_POST) {
        lv_cb_ext_t * cb_ext = lv_obj_get_ext_attr(cb);
        lv_btn_ext_t * bullet_ext = lv_obj_get_ext_attr(cb_ext->bullet);
        /*Be sure the state of the bullet is the same as the parent button*/
        bullet_ext->state = cb_ext->bg_btn.state;
        result = ancestor_bg_design(cb, mask, mode);
    } else {
        result = ancestor_bg_design(cb, mask, mode);
    }

    return result;
}
```

@kisvegabor can you help me to confirm this assumption so i can do another PR?

I also notices some switch statements where we can use a fall-through and avoid having lots of cases, but i didn't wanted to change a lot of code, because i don't have how to probe it.

Thanks in advance.
@C47D It looks good! Thank you!
Hi，
1.The Lv_btnm_get_style function LV_BTNM_STYLE_BTN_REL branch is missing "=" symbol
2.The warning for the lv_img_src_get_type function has not been removed.

the other is perfect. Thank you.
`lv_img_src_get_type` was not in the first list but here's how i tried to fix it:

Original version:
```c
lv_img_src_t lv_img_src_get_type(const void * src)
{
    if(src == NULL) return LV_IMG_SRC_UNKNOWN;
    const uint8_t * u8_p = src;

    /*The first byte shows the type of the image source*/
    if(u8_p[0] >= 0x20 && u8_p[0] <= 0x7F) {
        return LV_IMG_SRC_FILE;     /*If it's an ASCII character then it's file name*/
    } else if(u8_p[0] >= 0x80) {
        return LV_IMG_SRC_SYMBOL;   /*Symbols begins after 0x7F*/
    } else {
        return LV_IMG_SRC_VARIABLE; /*`lv_img_dsc_t` is design to the first byte < 0x20*/
    }

    LV_LOG_WARN("lv_img_src_get_type: unknown image type");
    return LV_IMG_SRC_UNKNOWN;
}
```

New:
```c
lv_img_src_t lv_img_src_get_type(const void * src)
{
    lv_img_src_t img_src_type = LV_IMG_SRC_UNKNOWN;

    if(src == NULL) return img_src_type;
    const uint8_t * u8_p = src;

    /*The first byte shows the type of the image source*/
    if(u8_p[0] >= 0x20 && u8_p[0] <= 0x7F) {
        img_src_type = LV_IMG_SRC_FILE;     /*If it's an ASCII character then it's file name*/
    } else if(u8_p[0] >= 0x80) {
        img_src_type = LV_IMG_SRC_SYMBOL;   /*Symbols begins after 0x7F*/
    } else {
        img_src_type = LV_IMG_SRC_VARIABLE; /*`lv_img_dsc_t` is design to the first byte < 0x20*/
    }

    if (LV_IMG_SRC_UNKNOWN == img_src_type) {
        LV_LOG_WARN("lv_img_src_get_type: unknown image type");
    }

    return img_src_type;
}
```

And also added the missing `=` in lv_btnm_get_style function.

Both commits in PR #539
@C47D Thank you, merged!
I already fixed the missing `=` in `dev-5.3` yesterday but still not in `master.`
@C47D 
Thanks, the problem is solved perfectly, the lvgl library will get better and better.
@chenshijianworkgit Thanks for reporting the issues you have, hope my silly mistakes didn't disturb your project, are there more warnings to solve?
@C47D 
Compile with the latest master branch's lvgl library in the keil environment without any warning.
@chenshijianworkgit 
Glad to hear that the warnings disappeared!

@C47D
Well done! :) 