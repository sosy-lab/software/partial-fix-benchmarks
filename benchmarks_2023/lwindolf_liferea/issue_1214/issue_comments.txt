Liferea 1.14.2 crash in conf_get_bool_value_from_schema()
Liferea no longer starts for me, and I believe I get the same crash.
Built https://github.com/lwindolf/liferea/commit/dc44aff6c54662c8e3a50825c1889854c1cef975
Here's a backtrace with debug symbols:

```
#0  0x000055555557b282 in conf_get_bool_value_from_schema (gsettings=<optimized out>, gsettings@entry=0x0, key=key@entry=0x5555555d030a "enable-reader-mode", value=value@entry=0x54) at ../conf.c:258
#1  0x00005555555aba83 in liferea_browser_update (browser=0x0, mode=1) at liferea_browser.c:727
#2  0x00005555555a9977 in itemview_update () at itemview.c:297
#3  0x0000555555588dc1 in itemlist_merge_itemset (itemSet=itemSet@entry=0x5555569edda0) at ../itemlist.c:299
#4  0x0000555555588ff5 in itemlist_load (node=node@entry=0x555555e50590) at ../itemlist.c:354
#5  0x00005555555851b1 in feedlist_selection_changed (obj=<optimized out>, nodeId=0x555555731dd0 "llmbfss", data=<optimized out>) at ../feedlist.c:585
#9  0x00007ffff15f3b9c in <emit signal 0x5555555c54be "selection-changed" on instance 0x555555b7e9c0 [FeedListView]>
    (instance=0x555555b7e9c0, detailed_signal=detailed_signal@entry=0x5555555c54be "selection-changed") at ../../../gobject/gsignal.c:3595
    #6  0x00007ffff15d3802 in g_closure_invoke (closure=0x5555563deb90, return_value=0x0, n_param_values=2, param_values=0x7fffffffb0c0, invocation_hint=0x7fffffffb040) at ../../../gobject/gclosure.c:810
    #7  0x00007ffff15e7814 in signal_emit_unlocked_R
    (node=node@entry=0x555555b6af90, detail=detail@entry=0, instance=instance@entry=0x555555b7e9c0, emission_return=emission_return@entry=0x0, instance_and_params=instance_and_params@entry=0x7fffffffb0c0)
    at ../../../gobject/gsignal.c:3743
    #8  0x00007ffff15f2bbe in g_signal_emit_valist (instance=instance@entry=0x555555b7e9c0, signal_id=signal_id@entry=289, detail=detail@entry=0, var_args=var_args@entry=0x7fffffffb2f8)
    at ../../../gobject/gsignal.c:3499
#10 0x00005555555a737d in feed_list_view_selection_changed_cb (selection=<optimized out>, data=<optimized out>) at feed_list_view.c:134
#11 0x00007ffff15d3a56 in _g_closure_invoke_va (closure=0x555555b99550, return_value=0x0, instance=0x5555558cb4d0, args=0x7fffffffb6d0, n_params=0, param_types=0x0) at ../../../gobject/gclosure.c:873
#12 0x00007ffff15f2b48 in g_signal_emit_valist (instance=0x5555558cb4d0, signal_id=<optimized out>, detail=0, var_args=var_args@entry=0x7fffffffb6d0) at ../../../gobject/gsignal.c:3408
#13 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#14 0x00007ffff1b7a986 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#15 0x00007ffff1b7d3f8 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#16 0x00007ffff15d3a56 in _g_closure_invoke_va (closure=0x5555556a1830, return_value=0x0, instance=0x555555b303c0, args=0x7fffffffba80, n_params=0, param_types=0x0) at ../../../gobject/gclosure.c:873
#17 0x00007ffff15f2b48 in g_signal_emit_valist (instance=0x555555b303c0, signal_id=<optimized out>, detail=0, var_args=var_args@entry=0x7fffffffba80) at ../../../gobject/gsignal.c:3408
#18 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#19 0x00007ffff1b9600a in gtk_widget_grab_focus () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#20 0x00007ffff1b72cc9 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#21 0x00007ffff1be7542 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#22 0x00007ffff15d3965 in _g_closure_invoke_va (closure=0x5555556a1a80, return_value=0x7fffffffbd10, instance=0x555555b303c0, args=0x7fffffffbde0, n_params=1, param_types=0x5555556a1ad0)
    at ../../../gobject/gclosure.c:873
#23 0x00007ffff15f1df1 in g_signal_emit_valist (instance=0x555555b303c0, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffbde0) at ../../../gobject/gsignal.c:3408
#24 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#25 0x00007ffff1b96d48 in gtk_widget_child_focus () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#26 0x00007ffff1acf69c in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#27 0x00007ffff1be7542 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#28 0x00007ffff15d3965 in _g_closure_invoke_va (closure=0x5555556a1a80, return_value=0x7fffffffc0a0, instance=0x555555b28370, args=0x7fffffffc170, n_params=1, param_types=0x5555556a1ad0)
    at ../../../gobject/gclosure.c:873
#29 0x00007ffff15f1df1 in g_signal_emit_valist (instance=0x555555b28370, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffc170) at ../../../gobject/gsignal.c:3408
#30 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#31 0x00007ffff1b96d48 in gtk_widget_child_focus () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#32 0x00007ffff197c767 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#33 0x00007ffff1a80c1b in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#34 0x00007ffff1be7542 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#35 0x00007ffff15d3965 in _g_closure_invoke_va (closure=0x5555556a1a80, return_value=0x7fffffffc480, instance=0x555555b22220, args=0x7fffffffc550, n_params=1, param_types=0x5555556a1ad0)
    at ../../../gobject/gclosure.c:873
#36 0x00007ffff15f1df1 in g_signal_emit_valist (instance=0x555555b22220, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffc550) at ../../../gobject/gsignal.c:3408
#37 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#38 0x00007ffff1b96d48 in gtk_widget_child_focus () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#39 0x00007ffff197c767 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#40 0x00007ffff1be7542 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#41 0x00007ffff15d3965 in _g_closure_invoke_va (closure=0x5555556a1a80, return_value=0x7fffffffc840, instance=0x555555b1e140, args=0x7fffffffc910, n_params=1, param_types=0x5555556a1ad0)
    at ../../../gobject/gclosure.c:873
#42 0x00007ffff15f1df1 in g_signal_emit_valist (instance=0x555555b1e140, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffc910) at ../../../gobject/gsignal.c:3408
#43 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#44 0x00007ffff1b96d48 in gtk_widget_child_focus () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#45 0x00007ffff1baee8c in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#46 0x00007ffff1be7542 in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#47 0x00007ffff15d3a56 in _g_closure_invoke_va (closure=0x5555556a1a80, return_value=0x7fffffffcbd0, instance=0x555555b082c0, args=0x7fffffffcca0, n_params=1, param_types=0x5555556a1ad0)
    at ../../../gobject/gclosure.c:873
#48 0x00007ffff15f1df1 in g_signal_emit_valist (instance=0x555555b082c0, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffcca0) at ../../../gobject/gsignal.c:3408
#49 0x00007ffff15f30f3 in g_signal_emit (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
#50 0x00007ffff1b96d48 in gtk_widget_child_focus () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#51 0x00007ffff1baedcd in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#52 0x00007ffff1baf08a in  () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#56 0x00007ffff15f30f3 in <emit signal ??? on instance 0x555555b082c0 [GtkApplicationWindow]> (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>) at ../../../gobject/gsignal.c:3555
    #53 0x00007ffff15d3802 in g_closure_invoke (closure=0x55555569dfa0, return_value=0x0, n_param_values=1, param_values=0x7fffffffcfa0, invocation_hint=0x7fffffffcf20) at ../../../gobject/gclosure.c:810
    #54 0x00007ffff15e7962 in signal_emit_unlocked_R (node=node@entry=0x55555569eba0, detail=detail@entry=0, instance=instance@entry=0x555555b082c0, emission_return=emission_return@entry=0x0, instance_and_params=instance_and_params@entry=0x7fffffffcfa0) at ../../../gobject/gsignal.c:3673
    #55 0x00007ffff15f2bbe in g_signal_emit_valist (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffd150) at ../../../gobject/gsignal.c:3499
#57 0x00007ffff1b9b1ce in gtk_widget_show () at /lib/x86_64-linux-gnu/libgtk-3.so.0
#58 0x00005555555aeaa1 in liferea_shell_restore_state (overrideWindowState=0x0) at liferea_shell.c:1204
#59 liferea_shell_create (app=app@entry=0x555555620120 [LifereaApplication], overrideWindowState=0x0, pluginsDisabled=0) at liferea_shell.c:1409
#60 0x000055555558a2da in on_app_activate (gtk_app=0x555555620120 [LifereaApplication], user_data=<optimized out>) at ../liferea_application.c:120
#64 0x00007ffff15f30f3 in <emit signal ??? on instance 0x555555620120 [LifereaApplication]> (instance=instance@entry=0x555555620120, signal_id=<optimized out>, detail=detail@entry=0) at ../../../gobject/gsignal.c:3555
    #61 0x00007ffff15d3802 in g_closure_invoke (closure=0x5555556228e0, return_value=0x0, n_param_values=1, param_values=0x7fffffffd510, invocation_hint=0x7fffffffd490) at ../../../gobject/gclosure.c:810
    #62 0x00007ffff15e7814 in signal_emit_unlocked_R (node=node@entry=0x55555561a700, detail=detail@entry=0, instance=instance@entry=0x555555620120, emission_return=emission_return@entry=0x0, instance_and_params=instance_and_params@entry=0x7fffffffd510) at ../../../gobject/gsignal.c:3743
    #63 0x00007ffff15f2bbe in g_signal_emit_valist (instance=<optimized out>, signal_id=<optimized out>, detail=<optimized out>, var_args=var_args@entry=0x7fffffffd6c0) at ../../../gobject/gsignal.c:3499
#65 0x00007ffff1701a23 in g_application_activate (application=application@entry=0x555555620120 [LifereaApplication]) at ../../../gio/gapplication.c:2303
#66 0x00007ffff1701d98 in g_application_real_local_command_line (application=0x555555620120 [LifereaApplication], arguments=0x7fffffffd828, exit_status=0x7fffffffd824) at ../../../gio/gapplication.c:1139
#67 0x00007ffff1701f52 in g_application_run (application=0x555555620120 [LifereaApplication], argc=argc@entry=1, argv=argv@entry=0x7fffffffd988) at ../../../gio/gapplication.c:2528
#68 0x000055555558a588 in liferea_application_new (argc=1, argv=0x7fffffffd988) at ../liferea_application.c:354
#69 0x00007ffff1156083 in __libc_start_main (main=0x555555578e70 <main>, argc=1, argv=0x7fffffffd988, init=<optimized out>, fini=<optimized out>, rtld_fini=<optimized out>, stack_end=0x7fffffffd978) at ../csu/libc-start.c:308
#70 0x0000555555578f1e in _start ()
```
The reporter of the Debian bug [1032989](https://bugs.debian.org/1032989) confirmed that the solution for issue #1212 didn't solve their issue and that it looks more like this one.
I don't know what caused it, but I suspect the commit ed860fa somehow triggered (`on_window_resize_cb`) the `itemview_set_layout()` at the wrong time in the wrong way. As a result, leaving the `itemview->htmlview` to NULL.

Try revert the mentioned commit and see if that helps. Else git bisect from the 1.4.0 to 1.4.1 might help, there are not much commits in between. 
@mozbugbox You're correct. Reverting that commit gets Liferea running again.
The submitter of Debian bug [1032989](https://bugs.debian.org/1032989) confirms that reverting commit https://github.com/lwindolf/liferea/commit/ed860fa49f61978f3e32eac10e18007e52e886a6 prevents liferea from crashing on his system.
Fixed by reverting ed860fa
Look like the real culprit of the bug was that the `feedlist_create` should be called after the `itemview_set_layout` call:

https://github.com/lwindolf/liferea/blob/a1f7ce15aea453e66674515729f912c93b000567/src/ui/liferea_shell.c#L1385-L1411

Before the `itemview_set_layout` was called, the `itemview->htmlview` was uninitialized, `feedlist_create` would cause the crash by loading item into the webview.

However, by putting `liferea_shell_restore_state (overrideWindowState);` into step 9, the `itermview_set_layout` was called by accident, on window resize. Thus initialized the `htmlview` before `feedlist_create` was called, avoided the crash.

I believe the proper fix should be  move the first half of `step 11` to the front of `step 10`. Or initialize `itemview->htmlview` in the `itemview_create()` call if nothing else in the `itemview_set_layout` was absolutely necessary. 
Liferea 1.4.3 crashes immediately (core dump).
It is unbelievable. Why does it every of those crashes in the last 3 releases happen with everyone else's system, but not mine. I dare no to do another release. It'll just crash again anyway.
@lwindolf I completely understand the frustration. Since I've been able to reproduce all of these crashes, I am willing to test things before a new release as long as you let me know.

The issue mentioned above is probably #1217 as I can reproduce that one too.
@tatokis @sunwire @mozbugbox Can you please test the code in #1221 ?


> @tatokis @sunwire @mozbugbox Can you please test the code in #1221 ?

Tested (latest https://github.com/lwindolf/liferea/commit/91241bb0f0abb00ac82c31ebe07e98808a7f86c3 + #1221), works excellent.
UPDATE
There are some minor issues https://github.com/lwindolf/liferea/pull/1221#issuecomment-1490468753
Additional issues fixed for 1.4.4 with #1221 