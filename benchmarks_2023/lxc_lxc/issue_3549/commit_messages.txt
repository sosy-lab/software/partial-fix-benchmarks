Merge pull request #3555 from brauner/2020-10-16/seccomp

seccomp: fix compilation on powerpc
conf: don't send invalid file descriptor on error

Closes: #3549.
Signed-off-by: Christian Brauner <christian.brauner@ubuntu.com>
start: improve devpts fd sending

Closes: #3549.
Signed-off-by: Christian Brauner <christian.brauner@ubuntu.com>
start: improve devpts fd sending

Closes: #3549.
Signed-off-by: Christian Brauner <christian.brauner@ubuntu.com>
start: improve devpts fd sending

Closes: #3549.
Signed-off-by: Christian Brauner <christian.brauner@ubuntu.com>
conf: do not hang when no pty devices are requested

Closes: #3549
Signed-off-by: Christian Brauner <christian.brauner@ubuntu.com>
