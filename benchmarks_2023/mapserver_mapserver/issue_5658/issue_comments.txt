SLD - Missing escaped characters when converting OGC propertyIsLike to Regex
thanks @rouault !
Actually, the pr #5660 introduced a regression in regards to how the regexp is built / character are escaped, I reproduced using a shapefile to simplify ; using the following layer entry:

```
	LAYER
            NAME "surval_30140_all_point_19_12_2017"
        TYPE POINT
	DEBUG 5
        DUMP TRUE
        STATUS OFF
        EXTENT -180 -90 180 90
        UNITS DD
        DATA surval/surval 
	    PROJECTION
                "init=epsg:4326"
            END
	    ENCODING UTF-8 
            METADATA
                wms_title "surval_30140_all_point_19_12_2017"
                wms_name "surval_30140_all_point_19_12_2017"
                wms_srs "EPSG:4326"
                wms_connectiontimeout "120"
                wms_server_version "1.1.1"
                wms_include_items "all"
                gml_include_items "all"		
	        wms_attribution_title ""
	        wms_attribution_onlineresource ""
                wms_metadataurl_format "text/xml"
                wms_metadataurl_type "TC211"
            END
        CLASS
            NAME ""
            STYLE
             	SYMBOL "circle"
		MINSIZE 5
		MAXSIZE 20
		COLOR 0 92 230
            END
	    LABEL
                COLOR 0 92 230
                POSITION auto
                SIZE small
	    END
        END
    END	
```

Here is the shape mentioned: [surval.zip](https://github.com/mapserver/mapserver/files/2746291/surval.zip)

Calling mapserver using:

```
/usr/lib/cgi-bin/mapserv QUERY_STRING='MAP=/root/resources/version-shapefile/mapfile.map
&SERVICE=WMS
&VERSION=1.3.0
&REQUEST=GetMap
&FORMAT=image%2Fpng
&TRANSPARENT=true
&LAYERS=surval_30140_all_point_19_12_2017
&STYLES=default
&SLD=http://172.17.0.1/mantis-44221.xml
&CRS=EPSG%3A3857&WIDTH=2036&HEIGHT=750&BBOX=-10014840.59554642%2C765984.6328891446%2C9897433.320100393%2C8098264.663286176'
```

The SLD passed as parameter looking like:

```xml
          <ogc:Filter xmlns:xs="http://www.w3.org/2001/XMLSchema">
            <ogc:PropertyIsLike escapeChar="\" matchCase="false" singleChar="?" wildCard="*">
              <ogc:PropertyName>SUPPORTS_N</ogc:PropertyName>
                   <ogc:Literal>*Support : Eau - Niveau : Surface-Fond ?profondeur &lt;3 m?*</ogc:Literal>
            </ogc:PropertyIsLike>
          </ogc:Filter>
```

Breaking in gdb in the `ms_regcomp()` function, before the PR being applied, analysing the memory gives the following hexdump (xxd is a specific macro I defined in my .gdbinit):

```
(gdb) xxd 0x55555578fab0 512
0000000: 2e2a 5375 7070 6f72 7420 3a20 4561 7520 .*Support : Eau 
0000010: 2d20 4e69 7665 6175 203a 2053 7572 6661 - Niveau : Surfa
0000020: 6365 2d46 6f6e 6420 2e70 726f 666f 6e64 ce-Fond .profond
0000030: 6575 7220 3c33 206d 2e2e 2a00 4d50 2054 eur <3 m..*.MP T
```

After the PR:

```
(gdb) xxd 0x64f740 512
0000000: 2e2a 5375 7070 6f72 7420 3a20 4561 7520 .*Support : Eau 
0000010: 5c2d 204e 6976 6561 7520 3a20 5375 7266 \- Niveau : Surf
0000020: 6163 655c 2d46 6f6e 6420 2e70 726f 666f ace\-Fond .profo
0000030: 6e64 6575 7220 5c3c 3320 6d2e 2e2a 0000 ndeur \<3 m..*..
```

the characters "-" and "<" are escaped. Normally in our SLD filter, we make use of parenthesis instead of the "?" char, which does not work in both version, so for now I was using '?' instead. From my understanding, I can sum up the 2 problems encountered here:

* Is it necessary to escape both previous characters  ('-' and '<') ?
* Why is the filter not working when containing parenthesis in both cases (before and after the PR) ?

I have not been able to reopen the current issue, I can open a new one if necessary, and/or provide more information to ease reproduction of the problem.
ping @rouault 