Rumble not working for non-XInput gamepads #601
Issues with non-ASCII chars in path or filename #604

Windows fopen and fstream do not support UTF-8 paths.

gimx.exe issues:
* failure to write log file
* failure to read macros

gimx-launcher issues:
* failure to save and restore choices
* failure to read gimx exit status
* failure to download configurations
* failure to download updates (if temp dir was modified)
Issues with non-ASCII chars in path or filename #604
Issues with non-ASCII chars in path or filename #604
Issues with non-ASCII chars in path or filename #604

* on Windows, use _wopendir, _wclosedir, _wreaddir, and _wstat
* this fixes issues when reading macro files
Issues with non-ASCII chars in path or filename #604

* Fix memleak in last commit
Issues with non-ASCII chars in path or filename #604

* fix last commit
