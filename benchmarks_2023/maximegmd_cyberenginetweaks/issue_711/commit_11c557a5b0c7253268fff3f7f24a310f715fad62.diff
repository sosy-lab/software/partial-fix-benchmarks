diff --git a/src/Options.cpp b/src/Options.cpp
index f140f9bd..764edf8d 100644
--- a/src/Options.cpp
+++ b/src/Options.cpp
@@ -138,7 +138,6 @@ Options::Options(Paths& aPaths)
 
     set_default_logger(CreateLogger(m_paths.CETRoot() / "cyber_engine_tweaks.log", "main"));
 
-
     Log::Info("Cyber Engine Tweaks is starting...");
 
     GameImage.Initialize();
@@ -148,9 +147,9 @@ Options::Options(Paths& aPaths)
         Log::Info("CET version {} [{}]", CET_BUILD_COMMIT, CET_BUILD_BRANCH);
         auto [major, minor] = GameImage.GetVersion();
         Log::Info("Game version {}.{:02d}", major, minor);
-        Log::Info("Root path: \"{}\"", aPaths.GameRoot().string());
-        Log::Info("Cyber Engine Tweaks path: \"{}\"", aPaths.CETRoot().string());
-        Log::Info("Lua scripts search path: \"{}\"", aPaths.ModsRoot().string());
+        Log::Info("Root path: \"{}\"", UTF16ToUTF8(aPaths.GameRoot().native()));
+        Log::Info("Cyber Engine Tweaks path: \"{}\"", UTF16ToUTF8(aPaths.CETRoot().native()));
+        Log::Info("Lua scripts search path: \"{}\"", UTF16ToUTF8(aPaths.ModsRoot().native()));
 
         if (GameImage.GetVersion() != GameImage.GetSupportedVersion())
         {
diff --git a/src/Options.h b/src/Options.h
index 4ccd67f2..0f246db1 100644
--- a/src/Options.h
+++ b/src/Options.h
@@ -8,11 +8,11 @@ struct Options
 {
     Options(Paths& aPaths);
     ~Options() = default;
-   
+
     void Load();
     void Save();
-    void ResetToDefaults(); 
-    
+    void ResetToDefaults();
+
     Image GameImage;
     bool PatchEnableDebug{ false };
     bool PatchRemovePedestrians{ false };
diff --git a/src/Utils.cpp b/src/Utils.cpp
index e40d9b6d..56941aaf 100644
--- a/src/Utils.cpp
+++ b/src/Utils.cpp
@@ -7,16 +7,12 @@
 
 void ltrim(std::string& s)
 {
-    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) {
-        return !std::isspace(ch);
-        }));
+    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](unsigned char ch) { return !std::isspace(ch); }));
 }
 
 void rtrim(std::string& s)
 {
-    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
-        return !std::isspace(ch);
-        }).base(), s.end());
+    s.erase(std::find_if(s.rbegin(), s.rend(), [](unsigned char ch) { return !std::isspace(ch); }).base(), s.end());
 }
 
 void trim(std::string& s)
@@ -25,7 +21,35 @@ void trim(std::string& s)
     rtrim(s);
 }
 
-template <typename Mutex>
+std::string UTF16ToUTF8(std::wstring_view utf16)
+{
+    const auto utf16Length = static_cast<int>(utf16.length());
+    const auto utf8Length = WideCharToMultiByte(CP_UTF8, 0, utf16.data(), utf16Length, nullptr, 0, nullptr, nullptr);
+    if (!utf8Length)
+        return {};
+
+    std::string utf8;
+    utf8.resize(utf8Length);
+    WideCharToMultiByte(CP_UTF8, 0, utf16.data(), utf16Length, utf8.data(), utf8Length, nullptr, nullptr);
+
+    return utf8;
+}
+
+std::wstring UTF8ToUTF16(std::string_view utf8)
+{
+    const auto utf8Length = static_cast<int>(utf8.length());
+    const auto utf16Length = MultiByteToWideChar(CP_UTF8, 0, utf8.data(), utf8Length, nullptr, 0);
+    if (!utf16Length)
+        return {};
+
+    std::wstring utf16;
+    utf16.resize(utf16Length);
+    MultiByteToWideChar(CP_UTF8, 0, utf8.data(), utf8Length, utf16.data(), utf16Length);
+
+    return utf16;
+}
+
+template<typename Mutex>
 class CustomSink : public spdlog::sinks::base_sink<Mutex>
 {
 public:
@@ -33,14 +57,15 @@ class CustomSink : public spdlog::sinks::base_sink<Mutex>
         : spdlog::sinks::base_sink<Mutex>()
         , m_sinkItHandler(aSinkItHandler)
         , m_flushHandler(aFlushHandler)
-    {}
+    {
+    }
 
 protected:
     void sink_it_(const spdlog::details::log_msg& msg) override
     {
         if (!m_sinkItHandler)
             return;
-            
+
         spdlog::memory_buf_t formatted;
         spdlog::sinks::base_sink<Mutex>::formatter_->format(msg, formatted);
         m_sinkItHandler(fmt::to_string(formatted));
@@ -53,36 +78,39 @@ class CustomSink : public spdlog::sinks::base_sink<Mutex>
     }
 
 private:
-
-    std::function<void(const std::string&)> m_sinkItHandler{ nullptr };
-    std::function<void()> m_flushHandler{ nullptr };
+    std::function<void(const std::string&)> m_sinkItHandler{nullptr};
+    std::function<void()> m_flushHandler{nullptr};
 };
 
-template <typename Mutex>
-spdlog::sink_ptr CreateCustomSink(std::function<void(const std::string&)> aSinkItHandler, std::function<void()> aFlushHandler)
+template<typename Mutex>
+spdlog::sink_ptr CreateCustomSink(std::function<void(const std::string&)> aSinkItHandler,
+                                  std::function<void()> aFlushHandler)
 {
     return std::make_shared<CustomSink<Mutex>>(aSinkItHandler, aFlushHandler);
 }
 
-spdlog::sink_ptr CreateCustomSinkST(std::function<void(const std::string&)> aSinkItHandler, std::function<void()> aFlushHandler)
+spdlog::sink_ptr CreateCustomSinkST(std::function<void(const std::string&)> aSinkItHandler,
+                                    std::function<void()> aFlushHandler)
 {
     return CreateCustomSink<spdlog::details::null_mutex>(aSinkItHandler, aFlushHandler);
 }
 
-spdlog::sink_ptr CreateCustomSinkMT(std::function<void(const std::string&)> aSinkItHandler, std::function<void()> aFlushHandler)
+spdlog::sink_ptr CreateCustomSinkMT(std::function<void(const std::string&)> aSinkItHandler,
+                                    std::function<void()> aFlushHandler)
 {
     return CreateCustomSink<std::mutex>(aSinkItHandler, aFlushHandler);
 }
 
-std::shared_ptr<spdlog::logger> CreateLogger(const std::filesystem::path& aPath, const std::string& aID, spdlog::sink_ptr aExtraSink, const std::string& aPattern)
+std::shared_ptr<spdlog::logger> CreateLogger(const std::filesystem::path& aPath, const std::string& aID,
+                                             spdlog::sink_ptr aExtraSink, const std::string& aPattern)
 {
     auto existingLogger = spdlog::get(aID);
     if (existingLogger)
         return existingLogger;
 
-    const auto rotSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(aPath.string(), 1048576 * 5, 3);
+    const auto rotSink = std::make_shared<spdlog::sinks::rotating_file_sink_mt>(aPath.native(), 1048576 * 5, 3);
     rotSink->set_pattern(aPattern);
-    auto logger = std::make_shared<spdlog::logger>(aID, spdlog::sinks_init_list{ rotSink });
+    auto logger = std::make_shared<spdlog::logger>(aID, spdlog::sinks_init_list{rotSink});
 
     if (aExtraSink)
         logger->sinks().emplace_back(aExtraSink);
diff --git a/src/Utils.h b/src/Utils.h
index f152a026..3baaaa66 100644
--- a/src/Utils.h
+++ b/src/Utils.h
@@ -4,6 +4,9 @@ void ltrim(std::string& s);
 void rtrim(std::string& s);
 void trim(std::string& s);
 
+std::string UTF16ToUTF8(std::wstring_view utf16);
+std::wstring UTF8ToUTF16(std::string_view utf8);
+
 spdlog::sink_ptr CreateCustomSinkST(std::function<void(const std::string&)> aSinkItHandler, std::function<void()> aFlushHandler = nullptr);
 spdlog::sink_ptr CreateCustomSinkMT(std::function<void(const std::string&)> aSinkItHandler, std::function<void()> aFlushHandler = nullptr);
 std::shared_ptr<spdlog::logger> CreateLogger(const std::filesystem::path& aPath, const std::string& aID, spdlog::sink_ptr aExtraSink = nullptr, const std::string& aPattern = "[%Y-%m-%d %H:%M:%S UTC%z] [%l] [%!] %v");
diff --git a/src/d3d12/D3D12_Functions.cpp b/src/d3d12/D3D12_Functions.cpp
index 9157645c..a0679833 100644
--- a/src/d3d12/D3D12_Functions.cpp
+++ b/src/d3d12/D3D12_Functions.cpp
@@ -1,18 +1,19 @@
 #include <stdafx.h>
 
 #include "D3D12.h"
+#include "Options.h"
+#include "Utils.h"
 
 #include <kiero/kiero.h>
 #include <imgui_impl/dx12.h>
 #include <imgui_impl/win32.h>
 
-#include <window/Window.h>
-#include "Options.h"
+#include <window/window.h>
 
 bool D3D12::ResetState(bool aClearDownlevelBackbuffers)
 {
     if (m_initialized)
-    {   
+    {
         m_initialized = false;
         ImGui_ImplDX12_Shutdown();
         ImGui_ImplWin32_Shutdown();
@@ -46,7 +47,7 @@ bool D3D12::Initialize(IDXGISwapChain* apSwapChain)
         return false;
     }
 
-    if (m_initialized) 
+    if (m_initialized)
     {
         IDXGISwapChain3* pSwapChain3{ nullptr };
         if (FAILED(apSwapChain->QueryInterface(IID_PPV_ARGS(&pSwapChain3))))
@@ -62,9 +63,9 @@ bool D3D12::Initialize(IDXGISwapChain* apSwapChain)
         {
             DXGI_SWAP_CHAIN_DESC sdesc;
             m_pdxgiSwapChain->GetDesc(&sdesc);
-            
+
             if (hWnd != sdesc.OutputWindow)
-                Log::Warn("D3D12::Initialize() - output window of current swap chain does not match hooked window! Currently hooked to {0} while swap chain output window is {1}.", reinterpret_cast<void*>(hWnd), reinterpret_cast<void*>(sdesc.OutputWindow));            
+                Log::Warn("D3D12::Initialize() - output window of current swap chain does not match hooked window! Currently hooked to {0} while swap chain output window is {1}.", reinterpret_cast<void*>(hWnd), reinterpret_cast<void*>(sdesc.OutputWindow));
         }
 
         return true;
@@ -84,7 +85,7 @@ bool D3D12::Initialize(IDXGISwapChain* apSwapChain)
 
     DXGI_SWAP_CHAIN_DESC sdesc;
     m_pdxgiSwapChain->GetDesc(&sdesc);
-    
+
     if (hWnd != sdesc.OutputWindow)
         Log::Warn("D3D12::Initialize() - output window of current swap chain does not match hooked window! Currently hooked to {0} while swap chain output window is {1}.", reinterpret_cast<void*>(hWnd), reinterpret_cast<void*>(sdesc.OutputWindow));
 
@@ -123,7 +124,7 @@ bool D3D12::Initialize(IDXGISwapChain* apSwapChain)
         Log::Error("D3D12::Initialize() - failed to create SRV descriptor heap!");
         return ResetState();
     }
-    
+
     for (auto& context : m_frameContexts)
         if (FAILED(m_pd3d12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&context.CommandAllocator))))
         {
@@ -176,7 +177,7 @@ bool D3D12::InitializeDownlevel(ID3D12CommandQueue* apCommandQueue, ID3D12Resour
     }
 
     auto cmdQueueDesc = apCommandQueue->GetDesc();
-    if(cmdQueueDesc.Type != D3D12_COMMAND_LIST_TYPE_DIRECT) 
+    if(cmdQueueDesc.Type != D3D12_COMMAND_LIST_TYPE_DIRECT)
     {
         Log::Warn("D3D12::InitializeDownlevel() - ignoring command queue - invalid type of command list!");
         return false;
@@ -186,7 +187,7 @@ bool D3D12::InitializeDownlevel(ID3D12CommandQueue* apCommandQueue, ID3D12Resour
 
     auto st2DDesc = apSourceTex2D->GetDesc();
     m_outSize = { static_cast<LONG>(st2DDesc.Width), static_cast<LONG>(st2DDesc.Height) };
-    
+
     if (hWnd != ahWindow)
         Log::Warn("D3D12::InitializeDownlevel() - current output window does not match hooked window! Currently hooked to {0} while current output window is {1}.", reinterpret_cast<void*>(hWnd), reinterpret_cast<void*>(ahWindow));
 
@@ -237,7 +238,7 @@ bool D3D12::InitializeDownlevel(ID3D12CommandQueue* apCommandQueue, ID3D12Resour
         Log::Error("D3D12::InitializeDownlevel() - failed to create SRV descriptor heap!");
         return ResetState();
     }
-    
+
     for (auto& context : m_frameContexts)
     {
         if (FAILED(m_pd3d12Device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, IID_PPV_ARGS(&context.CommandAllocator))))
@@ -295,7 +296,7 @@ bool D3D12::InitializeImGui(size_t aBuffersCounts)
         config.OversampleH = config.OversampleV = 1;
         config.PixelSnapH = true;
         io.Fonts->AddFontDefault(&config);
-        
+
         if (!m_options.FontPath.empty())
         {
             std::filesystem::path fontPath(m_options.FontPath);
@@ -328,7 +329,7 @@ bool D3D12::InitializeImGui(size_t aBuffersCounts)
                     case MAKELANGID(LANG_CHINESE, SUBLANG_CHINESE_SIMPLIFIED):
                         cpGlyphRanges = io.Fonts->GetGlyphRangesChineseSimplifiedCommon();
                         break;
-                        
+
                     case MAKELANGID(LANG_KOREAN, SUBLANG_DEFAULT):
                         cpGlyphRanges = io.Fonts->GetGlyphRangesKorean();
                         break;
@@ -360,8 +361,9 @@ bool D3D12::InitializeImGui(size_t aBuffersCounts)
                     cpGlyphRanges = io.Fonts->GetGlyphRangesThai();
                 else if (m_options.FontGlyphRanges == "Vietnamese")
                     cpGlyphRanges = io.Fonts->GetGlyphRangesVietnamese();
+
                 ImFont* pFont =
-                    io.Fonts->AddFontFromFileTTF(fontPath.string().c_str(), m_options.FontSize, nullptr, cpGlyphRanges);
+                    io.Fonts->AddFontFromFileTTF(UTF16ToUTF8(fontPath.native()).c_str(), m_options.FontSize, nullptr, cpGlyphRanges);
 
                 if (pFont != nullptr)
                 {
@@ -370,8 +372,8 @@ bool D3D12::InitializeImGui(size_t aBuffersCounts)
             }
         }
     }
-    
-    if (!ImGui_ImplWin32_Init(m_window.GetWindow())) 
+
+    if (!ImGui_ImplWin32_Init(m_window.GetWindow()))
     {
         Log::Error("D3D12::InitializeImGui() - ImGui_ImplWin32_Init call failed!");
         return false;
@@ -387,7 +389,7 @@ bool D3D12::InitializeImGui(size_t aBuffersCounts)
         return false;
     }
 
-    if (!ImGui_ImplDX12_CreateDeviceObjects(m_pCommandQueue)) 
+    if (!ImGui_ImplDX12_CreateDeviceObjects(m_pCommandQueue))
     {
         Log::Error("D3D12::InitializeImGui() - ImGui_ImplDX12_CreateDeviceObjects call failed!");
         ImGui_ImplDX12_Shutdown();
diff --git a/src/imgui_impl/win32.cpp b/src/imgui_impl/win32.cpp
index 83e05d3e..21ed9b8b 100644
--- a/src/imgui_impl/win32.cpp
+++ b/src/imgui_impl/win32.cpp
@@ -16,6 +16,7 @@
 #include "win32.h"
 
 #include "CET.h"
+#include "Utils.h"
 
 // CHANGELOG
 // (minor and older changes stripped away, please see git history for details)
@@ -68,7 +69,7 @@ bool ImGui_ImplWin32_Init(HWND ahWnd)
     io.ImeWindowHandle = ahWnd;
 
     // Setup ini path
-    g_LayoutPath = (CET::Get().GetPaths().CETRoot() / "layout.ini").string();
+    g_LayoutPath = UTF16ToUTF8((CET::Get().GetPaths().CETRoot() / "layout.ini").native());
     io.IniFilename = g_LayoutPath.c_str();
 
     // Keyboard mapping. ImGui will use those indices to peek into the io.KeysDown[] array that we will update during the application lifetime.
diff --git a/src/overlay/widgets/TweakDBEditor.cpp b/src/overlay/widgets/TweakDBEditor.cpp
index 70efd043..4bee944b 100644
--- a/src/overlay/widgets/TweakDBEditor.cpp
+++ b/src/overlay/widgets/TweakDBEditor.cpp
@@ -1,16 +1,18 @@
 #include <stdafx.h>
 
+#include "TweakDBEditor.h"
+#include "HelperWidgets.h"
+#include "Utils.h"
+
 #include <RED4ext/Scripting/Natives/Generated/Color.hpp>
 #include <RED4ext/Scripting/Natives/Generated/EulerAngles.hpp>
 #include <RED4ext/Scripting/Natives/Generated/Quaternion.hpp>
 #include <RED4ext/Scripting/Natives/Generated/Vector2.hpp>
 #include <RED4ext/Scripting/Natives/Generated/Vector3.hpp>
 
-#include "HelperWidgets.h"
 #include <CET.h>
 #include <reverse/TweakDB/TweakDB.h>
 
-#include "TweakDBEditor.h"
 
 bool TweakDBEditor::s_recordsFilterIsRegex = false;
 bool TweakDBEditor::s_flatsFilterIsRegex = false;
@@ -1977,7 +1979,7 @@ void TweakDBEditor::DrawAdvancedTab()
         ImGui::PopStyleColor(2);
         ImGui::TextUnformatted("1) Download and unpack 'Metadata'");
         ImGui::TextUnformatted("2) Copy 'tweakdb.str' to 'plugins\\cyber_engine_tweaks\\tweakdb.str'");
-        std::string cetDir = CET::Get().GetPaths().CETRoot().string();
+        const auto cetDir = UTF16ToUTF8(CET::Get().GetPaths().CETRoot().native());
         ImGui::Text("Full path: %s", cetDir.c_str());
         if (ImGui::Button("3) Load tweakdb.str"))
         {
@@ -2008,7 +2010,7 @@ void TweakDBEditor::DrawAdvancedTab()
         ImGui::InputText("##wkitLink", pLink, sizeof(pLink) - 1, ImGuiInputTextFlags_ReadOnly);
         ImGui::PopStyleColor(2);
         ImGui::TextUnformatted("1) Download and put 'usedhashes.kark' in 'plugins\\cyber_engine_tweaks\\'");
-        std::string cetDir = CET::Get().GetPaths().CETRoot().string();
+        const auto cetDir = UTF16ToUTF8(CET::Get().GetPaths().CETRoot().native());
         ImGui::Text("Full path: %s", cetDir.c_str());
         if (ImGui::Button("3) Load usedhashes.kark"))
         {
diff --git a/src/scripting/LuaSandbox.cpp b/src/scripting/LuaSandbox.cpp
index 05616ff9..2b102c70 100644
--- a/src/scripting/LuaSandbox.cpp
+++ b/src/scripting/LuaSandbox.cpp
@@ -225,13 +225,14 @@ void LuaSandbox::InitializeExtraLibsForSandbox(Sandbox& aSandbox) const
     const auto cLoadTexture = [cSBRootPath, sbStateView](const std::string& acPath) -> std::tuple<std::shared_ptr<Texture>, sol::object> {
         auto absPath = absolute(cSBRootPath / acPath).make_preferred();
 
-        const auto cRelPathStr = relative(absPath, cSBRootPath).string();
-        if (!exists(absPath) || !is_regular_file(absPath) || (cRelPathStr.find("..") != std::string::npos))
+        const auto cRelPathStr = relative(absPath, cSBRootPath).native();
+        if (!exists(absPath) || !is_regular_file(absPath) || (cRelPathStr.find(L"..") != std::wstring::npos))
             return std::make_tuple(nullptr, make_object(sbStateView, "Invalid path!"));
 
-        auto texture = Texture::Load(absPath.string());
+        const auto absPathUTF8 = UTF16ToUTF8(absPath.native());
+        auto texture = Texture::Load(absPathUTF8);
         if (!texture)
-            return std::make_tuple(nullptr, make_object(sbStateView, "Failed to load '" + absPath.string() + "'"));
+            return std::make_tuple(nullptr, make_object(sbStateView, "Failed to load '" + absPathUTF8 + "'"));
 
         return std::make_tuple(texture, sol::nil);
     };
@@ -257,7 +258,7 @@ void LuaSandbox::InitializeDBForSandbox(Sandbox& aSandbox) const
     }
     sbEnv["sqlite3"] = sqlite3Copy;
 
-    sbEnv["db"] = luaView["sqlite3"]["open"]((cSBRootPath / "db.sqlite3").string());
+    sbEnv["db"] = luaView["sqlite3"]["open"](UTF16ToUTF8((cSBRootPath / "db.sqlite3").native()));
 }
 
 void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& acName)
@@ -292,13 +293,13 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
         auto absPath = absolute(cSBRootPath / acPath).make_preferred();
         if (!exists(absPath) || !is_regular_file(absPath))
             absPath += ".lua";
-        const auto cRelPathStr = relative(absPath, cSBRootPath).string();
-        if (!exists(absPath) || !is_regular_file(absPath) || (cRelPathStr.find("..") != std::string::npos))
+        const auto cRelPathStr = relative(absPath, cSBRootPath).native();
+        if (!exists(absPath) || !is_regular_file(absPath) || (cRelPathStr.find(L"..") != std::wstring::npos))
             return std::make_tuple(sol::nil, make_object(sbStateView, "Invalid path!"));
 
         std::ifstream ifs(absPath);
         const std::string cScriptString((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
-        return cLoadString(cScriptString, "@" + absPath.string());
+        return cLoadString(cScriptString, "@" + UTF16ToUTF8(absPath.native()));
     };
     sbEnv["loadfile"] = cLoadFile;
 
@@ -330,18 +331,18 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
                 absPath = absPath3;
             }
         }
-        const auto cRelPathStr = relative(absPath, cSBRootPath).string();
-        if ((cRelPathStr.find("..") != std::string::npos))
+        const auto cRelPathStr = relative(absPath, cSBRootPath).native();
+        if ((cRelPathStr.find(L"..") != std::wstring::npos))
             return std::make_tuple(sol::nil, make_object(sbStateView, "Invalid path!"));
 
-        const auto cKey = absPath.string();
+        const auto cKey = UTF16ToUTF8(absPath.native());
         const auto cExistingModule = this->m_modules.find(cKey);
         if (cExistingModule != this->m_modules.end())
             return std::make_tuple(cExistingModule->second, sol::nil);
 
         std::ifstream ifs(absPath);
         const std::string cScriptString((std::istreambuf_iterator<char>(ifs)), std::istreambuf_iterator<char>());
-        auto res = cLoadString(cScriptString, "@" + absPath.string());
+        auto res = cLoadString(cScriptString, "@" + UTF16ToUTF8(absPath.native()));
         auto obj = std::get<0>(res);
         if (obj == sol::nil)
             return res;
@@ -381,8 +382,8 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
         sol::state_view sv = sbStateView;
 
         auto absPath = absolute(cSBRootPath / acPath).make_preferred();
-        const auto cRelPathStr = relative(absPath, cSBRootPath).string();
-        if (!exists(absPath) || !is_directory(absPath) || (cRelPathStr.find("..") != std::string::npos))
+        const auto cRelPathStr = relative(absPath, cSBRootPath).native();
+        if (!exists(absPath) || !is_directory(absPath) || (cRelPathStr.find(L"..") != std::wstring::npos))
             return sol::nil;
 
         sol::table res(sv, sol::create);
@@ -390,7 +391,7 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
         for (const auto& file : std::filesystem::directory_iterator(absPath))
         {
             sol::table item(sv, sol::create);
-            item["name"] = relative(file.path(), absPath).string();
+            item["name"] = UTF16ToUTF8(relative(file.path(), absPath).native());
             item["type"] = (file.is_directory()) ? ("directory") : ("file");
             res[index++] = item;
         }
@@ -410,17 +411,17 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
         ioSB["lines"] = [cIO, cSBRootPath](const std::string& acPath)
         {
             const auto cAbsPath = absolute(cSBRootPath / acPath).make_preferred();
-            const auto cRelPathStr = relative(cAbsPath, cSBRootPath).string();
-            if (cRelPathStr.find("..") == std::string::npos)
-                return cIO["lines"](cAbsPath.string());
+            const auto cRelPathStr = relative(cAbsPath, cSBRootPath).native();
+            if (cRelPathStr.find(L"..") == std::wstring::npos)
+                return cIO["lines"](UTF16ToUTF8(cAbsPath.native()));
             return cIO["lines"](""); // simulate invalid input even though it may be valid - we dont want mod access outside!
         };
         const auto cOpenWithMode = [cIO, cSBRootPath](const std::string& acPath, const std::string& acMode)
         {
             const auto cAbsPath = absolute(cSBRootPath / acPath).make_preferred();
-            const auto cRelPathStr = relative(cAbsPath, cSBRootPath).string();
-            if (cRelPathStr.find("..") == std::string::npos && (cRelPathStr != "db.sqlite3"))
-                return cIO["open"](cAbsPath.string(), acMode);
+            const auto cRelPathStr = relative(cAbsPath, cSBRootPath).native();
+            if (cRelPathStr.find(L"..") == std::string::npos && (cRelPathStr != L"db.sqlite3"))
+                return cIO["open"](UTF16ToUTF8(cAbsPath.native()), acMode);
             return cIO["open"]("", acMode); // simulate invalid input even though it may be valid - we dont want mod access outside!
         };
         auto cOpenDefault = [cOpenWithMode](const std::string& acPath)
@@ -438,16 +439,16 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
         osSB["rename"] = [cOS, cSBRootPath](const std::string& acOldPath, const std::string& acNewPath) -> std::tuple<sol::object, std::string>
         {
             const auto cAbsOldPath = absolute(cSBRootPath / acOldPath).make_preferred();
-            const auto cRelOldPathStr =  relative(cAbsOldPath, cSBRootPath).string();
-            if (!exists(cAbsOldPath) || (cRelOldPathStr.find("..") != std::string::npos) || (cRelOldPathStr == "db.sqlite3"))
+            const auto cRelOldPathStr =  relative(cAbsOldPath, cSBRootPath).native();
+            if (!exists(cAbsOldPath) || (cRelOldPathStr.find(L"..") != std::wstring::npos) || (cRelOldPathStr == L"db.sqlite3"))
                 return std::make_tuple(sol::nil, "Argument oldpath is invalid!");
 
             const auto cAbsNewPath = absolute(cSBRootPath / acNewPath).make_preferred();
-            const auto cRelNewPathStr =  relative(cAbsNewPath, cSBRootPath).string();
-            if (cRelNewPathStr.find("..") != std::string::npos)
+            const auto cRelNewPathStr =  relative(cAbsNewPath, cSBRootPath).native();
+            if (cRelNewPathStr.find(L"..") != std::wstring::npos)
                 return std::make_tuple(sol::nil, "Argument newpath is invalid!");
 
-            const auto cResult = cOS["rename"](cAbsOldPath.string(), cAbsNewPath.string());
+            const auto cResult = cOS["rename"](UTF16ToUTF8(cAbsOldPath.native()), UTF16ToUTF8(cAbsNewPath.native()));
             if (cResult.valid())
                 return std::make_tuple(cResult.get<sol::object>(), "");
             return std::make_tuple(cResult.get<sol::object>(0), cResult.get<std::string>(1));
@@ -455,11 +456,11 @@ void LuaSandbox::InitializeIOForSandbox(Sandbox& aSandbox, const std::string& ac
         osSB["remove"] = [cOS, cSBRootPath](const std::string& acPath) -> std::tuple<sol::object, std::string>
         {
             const auto cAbsPath = absolute(cSBRootPath / acPath).make_preferred();
-            const auto cRelPathStr = relative(cAbsPath, cSBRootPath).string();
-            if (!exists(cAbsPath) || (cRelPathStr.find("..") != std::string::npos) || (cRelPathStr == "db.sqlite3"))
+            const auto cRelPathStr = relative(cAbsPath, cSBRootPath).native();
+            if (!exists(cAbsPath) || (cRelPathStr.find(L"..") != std::wstring::npos) || (cRelPathStr == L"db.sqlite3"))
                 return std::make_tuple(sol::nil, "Argument path is invalid!");
 
-            const auto cResult = cOS["remove"](cAbsPath.string());
+            const auto cResult = cOS["remove"](UTF16ToUTF8(cAbsPath.native()));
             if (cResult.valid())
                 return std::make_tuple(cResult.get<sol::object>(), "");
             return std::make_tuple(cResult.get<sol::object>(0), cResult.get<std::string>(1));
diff --git a/src/scripting/LuaSandbox.h b/src/scripting/LuaSandbox.h
index 32f3f2f9..2ba14c68 100644
--- a/src/scripting/LuaSandbox.h
+++ b/src/scripting/LuaSandbox.h
@@ -12,15 +12,15 @@ struct LuaSandbox
     void ResetState();
 
     size_t CreateSandbox(const std::filesystem::path& acPath = "", const std::string& acName = "", bool aEnableExtraLibs = true, bool aEnableDB = true, bool aEnableIO = true, bool aEnableLogger = true);
-    
+
     sol::protected_function_result ExecuteFile(const std::string& acPath);
     sol::protected_function_result ExecuteString(const std::string& acString);
-    
+
     Sandbox& operator[](size_t aID);
     const Sandbox& operator[](size_t aID) const;
 
     [[nodiscard]] TiltedPhoques::Locked<sol::state, std::recursive_mutex> GetState() const;
-    
+
 private:
 
     void InitializeExtraLibsForSandbox(Sandbox& aSandbox) const;
diff --git a/src/scripting/ScriptContext.cpp b/src/scripting/ScriptContext.cpp
index be252046..47154356 100644
--- a/src/scripting/ScriptContext.cpp
+++ b/src/scripting/ScriptContext.cpp
@@ -1,6 +1,7 @@
 #include <stdafx.h>
 
 #include "ScriptContext.h"
+#include "Utils.h"
 
 #include <CET.h>
 
@@ -84,7 +85,7 @@ ScriptContext::ScriptContext(LuaSandbox& aLuaSandbox, const std::filesystem::pat
 
         m_vkBindInfos.emplace_back(VKBindInfo{vkBind});
     };
-    
+
     env["registerInput"] = [this, &aLuaSandbox](const std::string& acID, const std::string& acDescription, sol::function aCallback) {
         if (acID.empty() ||
             (std::ranges::find_if(acID, [](char c) { return !(isalpha(c) || isdigit(c) || c == '_'); }) != acID.cend()))
@@ -117,7 +118,7 @@ ScriptContext::ScriptContext(LuaSandbox& aLuaSandbox, const std::filesystem::pat
     try
     {
         const auto path = acPath / "init.lua";
-        const auto result = sb.ExecuteFile(path.string());
+        const auto result = sb.ExecuteFile(UTF16ToUTF8(path.native()));
 
         if (result.valid())
         {
@@ -186,7 +187,7 @@ void ScriptContext::TriggerOnDraw() const
 
     TryLuaFunction(m_logger, m_onDraw);
 }
-    
+
 void ScriptContext::TriggerOnOverlayOpen() const
 {
     auto state = m_sandbox.GetState();
diff --git a/src/scripting/ScriptStore.cpp b/src/scripting/ScriptStore.cpp
index 13b0a15c..f01cf3e2 100644
--- a/src/scripting/ScriptStore.cpp
+++ b/src/scripting/ScriptStore.cpp
@@ -1,6 +1,7 @@
 #include <stdafx.h>
 
 #include "ScriptStore.h"
+#include "Utils.h"
 
 ScriptStore::ScriptStore(LuaSandbox& aLuaSandbox, const Paths& aPaths, VKBindings& aBindings)
     : m_sandbox(aLuaSandbox)
@@ -11,6 +12,9 @@ ScriptStore::ScriptStore(LuaSandbox& aLuaSandbox, const Paths& aPaths, VKBinding
 
 void ScriptStore::LoadAll()
 {
+    // set current path for following scripts to our ModsPath
+    current_path(m_paths.ModsRoot());
+
     m_vkBindInfos.clear();
     m_contexts.clear();
     m_sandbox.ResetState();
@@ -37,7 +41,7 @@ void ScriptStore::LoadAll()
         }
 
         fPath = absolute(fPath);
-        auto fPathStr = fPath.string();
+        auto fPathStr = UTF16ToUTF8(fPath.native());
 
         if (!exists(fPath / "init.lua"))
         {
@@ -45,7 +49,7 @@ void ScriptStore::LoadAll()
             continue;
         }
 
-        auto name = file.path().filename().string();
+        auto name = UTF16ToUTF8(file.path().filename().native());
         if (name.find('.') != std::string::npos)
         {
             consoleLogger->info("Ignoring directory containing '.', as this is reserved character! ('{}')", fPathStr);
diff --git a/xmake.lua b/xmake.lua
index 2bf2c21e..9776802a 100644
--- a/xmake.lua
+++ b/xmake.lua
@@ -37,11 +37,11 @@ target("RED4ext.SDK")
   on_install(function() end)
 
 target("cyber_engine_tweaks")
-    add_defines("WIN32_LEAN_AND_MEAN", "NOMINMAX", "WINVER=0x0601", "SOL_ALL_SAFETIES_ON", "SOL_LUAJIT=1", "SPDLOG_WCHAR_TO_UTF8_SUPPORT", "IMGUI_USER_CONFIG=\""..imguiUserConfig.."\"") -- WINVER=0x0601 == Windows 7, we need this specified now for some reason
+    add_defines("WIN32_LEAN_AND_MEAN", "NOMINMAX", "WINVER=0x0601", "SOL_ALL_SAFETIES_ON", "SOL_LUAJIT=1", "SPDLOG_WCHAR_TO_UTF8_SUPPORT", "SPDLOG_WCHAR_FILENAMES", "SPDLOG_WCHAR_SUPPORT", "IMGUI_USER_CONFIG=\""..imguiUserConfig.."\"") -- WINVER=0x0601 == Windows 7xmake
     set_pcxxheader("src/stdafx.h")
     set_kind("shared")
     set_filename("cyber_engine_tweaks.asi")
-    add_files("src/**.c", "src/**.cpp")
+    add_files("src/**.cpp")
     add_headerfiles("src/**.h", "build/CETVersion.h")
     add_includedirs("src/", "build/")
     add_syslinks("User32", "Version", "d3d11")
