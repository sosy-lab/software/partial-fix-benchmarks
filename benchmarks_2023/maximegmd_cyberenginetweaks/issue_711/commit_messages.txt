Fix #709 - LUA_PATH overriding pachage.path
Fix wrongly manipulating paths

- add UTF16ToUTF8 and UTF8ToUTF16 conversion helpers
- fix initialization failing when path contains non-ascii characters #705
- fix wrongly sending ascii strings to imgui in some places (imgui can only work with UTF-8)

New issue discovered during working on this which needs to be looked at - mods won't load when path has non-ascii characters included! #711
Fix non-unicode paths Lua issues (fixes #711 for good)
Fix wrongly manipulating paths

- add UTF16ToUTF8 and UTF8ToUTF16 conversion helpers
- fix initialization failing when path contains non-ascii characters #705
- fix wrongly sending ascii strings to imgui in some places (imgui can only work with UTF-8)

New issue discovered during working on this which needs to be looked at - mods won't load when path has non-ascii characters included! #711
Fix non-unicode paths Lua issues (fixes #711 for good)
Fix wrongly manipulating paths

- add UTF16ToUTF8 and UTF8ToUTF16 conversion helpers
- fix initialization failing when path contains non-ascii characters #705
- fix wrongly sending ascii strings to imgui in some places (imgui can only work with UTF-8)

New issue discovered during working on this which needs to be looked at - mods won't load when path has non-ascii characters included! #711
Fix non-unicode paths Lua issues (fixes #711 for good)
