UWP has no lstrlenW function
I was having the same issue, check:

https://github.com/ARMmbed/mbedtls/issues/1227#issue-283783664


Hi @borrrden , @erossetto ,

There's a fix pending for this issue in https://github.com/ARMmbed/mbedtls/pull/1453 . Unfortunately, that pull request does many things, got delayed several times and has accumulated a lot of conflicts. We don't have much Windows expertise on the team, and we don't have much time to spend on it.

Would you mind helping us with this by contributing a fix?

* The smaller the code change, the easier it is to review.
* Please make sure not to break support for older Windows environments. We currently support Visual Studio back to Visual Studio 2013.
* That cmake command line is very helpful. We should add a Windows build to our CI that catches this problem. What versions of Visual Studio, cmake and other tools does it require? We currently have up to Visual Studio 2017. Would we need to install a more recent version?
