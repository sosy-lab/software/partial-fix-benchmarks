diff --git a/ice.c b/ice.c
index 3ea98c1899..81d89b5901 100644
--- a/ice.c
+++ b/ice.c
@@ -316,8 +316,12 @@ typedef struct janus_ice_queued_packet {
 } janus_ice_queued_packet;
 /* A few static, fake, messages we use as a trigger: e.g., to start a
  * new DTLS handshake, hangup a PeerConnection or close a handle */
-static janus_ice_queued_packet janus_ice_dtls_handshake,
-	janus_ice_hangup_peerconnection, janus_ice_detach_handle;
+static janus_ice_queued_packet
+	janus_ice_start_gathering,
+	janus_ice_add_candidates,
+	janus_ice_dtls_handshake,
+	janus_ice_hangup_peerconnection,
+	janus_ice_detach_handle;
 
 /* Janus NACKed packet we're tracking (to avoid duplicates) */
 typedef struct janus_ice_nacked_packet {
@@ -470,8 +474,11 @@ static inline void janus_ice_free_rtp_packet(janus_rtp_packet *pkt) {
 }
 
 static void janus_ice_free_queued_packet(janus_ice_queued_packet *pkt) {
-	if(pkt == NULL || pkt == &janus_ice_dtls_handshake ||
-			pkt == &janus_ice_hangup_peerconnection || pkt == &janus_ice_detach_handle) {
+	if(pkt == NULL || pkt == &janus_ice_start_gathering ||
+			pkt == &janus_ice_add_candidates ||
+			pkt == &janus_ice_dtls_handshake ||
+			pkt == &janus_ice_hangup_peerconnection ||
+			pkt == &janus_ice_detach_handle) {
 		return;
 	}
 	g_free(pkt->data);
@@ -609,6 +616,15 @@ static void janus_plugin_session_dereference(janus_plugin_session *plugin_sessio
 }
 
 
+static void janus_ice_clear_queued_candidates(janus_ice_handle *handle) {
+	if(handle == NULL || handle->queued_candidates == NULL) {
+		return;
+	}
+	while(g_async_queue_length(handle->queued_candidates) > 0) {
+		(void)g_async_queue_try_pop(handle->queued_candidates);
+	}
+}
+
 static void janus_ice_clear_queued_packets(janus_ice_handle *handle) {
 	if(handle == NULL || handle->queued_packets == NULL) {
 		return;
@@ -1175,6 +1191,7 @@ janus_ice_handle *janus_ice_handle_create(void *core_session, const char *opaque
 	handle->handle_id = handle_id;
 	handle->app = NULL;
 	handle->app_handle = NULL;
+	handle->queued_candidates = g_async_queue_new();
 	handle->queued_packets = g_async_queue_new();
 	janus_mutex_init(&handle->mutex);
 	janus_session_handles_insert(session, handle);
@@ -1316,6 +1333,10 @@ static void janus_ice_handle_free(const janus_refcount *handle_ref) {
 	janus_ice_handle *handle = janus_refcount_containerof(handle_ref, janus_ice_handle, ref);
 	/* This stack can be destroyed, free all the resources */
 	janus_mutex_lock(&handle->mutex);
+	if(handle->queued_candidates != NULL) {
+		janus_ice_clear_queued_candidates(handle);
+		g_async_queue_unref(handle->queued_candidates);
+	}
 	if(handle->queued_packets != NULL) {
 		janus_ice_clear_queued_packets(handle);
 		g_async_queue_unref(handle->queued_packets);
@@ -1415,6 +1436,7 @@ static void janus_ice_webrtc_free(janus_ice_handle *handle) {
 		}
 	}
 	handle->pending_trickles = NULL;
+	janus_ice_clear_queued_candidates(handle);
 	g_free(handle->rtp_profile);
 	handle->rtp_profile = NULL;
 	g_free(handle->local_sdp);
@@ -3173,6 +3195,20 @@ void janus_ice_candidates_to_sdp(janus_ice_handle *handle, janus_sdp_mline *mlin
 	g_slist_free(candidates);
 }
 
+void janus_ice_add_remote_candidate(janus_ice_handle *handle, NiceCandidate *c) {
+	JANUS_LOG(LOG_VERB, "[%"SCNu64"] Queueing candidate %p\n", handle->handle_id, c);
+	if(handle->queued_candidates != NULL)
+		g_async_queue_push(handle->queued_candidates, c);
+	if(handle->queued_packets != NULL) {
+#if GLIB_CHECK_VERSION(2, 46, 0)
+		g_async_queue_push_front(handle->queued_packets, &janus_ice_add_candidates);
+#else
+		g_async_queue_push(handle->queued_packets, &janus_ice_add_candidates);
+#endif
+		g_main_context_wakeup(handle->mainctx);
+	}
+}
+
 void janus_ice_setup_remote_candidates(janus_ice_handle *handle, guint stream_id, guint component_id) {
 	if(!handle || !handle->agent)
 		return;
@@ -3202,32 +3238,22 @@ void janus_ice_setup_remote_candidates(janus_ice_handle *handle, guint stream_id
 	/* Add all candidates */
 	NiceCandidate *c = NULL;
 	GSList *gsc = component->candidates;
-	gchar *rufrag = NULL, *rpwd = NULL;
 	while(gsc) {
 		c = (NiceCandidate *) gsc->data;
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"] >> Remote Stream #%d, Component #%d\n", handle->handle_id, c->stream_id, c->component_id);
-		if(c->username && !rufrag)
-			rufrag = c->username;
-		if(c->password && !rpwd)
-			rpwd = c->password;
-		gchar address[NICE_ADDRESS_STRING_LEN];
-		nice_address_to_string(&(c->addr), (gchar *)&address);
-		gint port = nice_address_get_port(&(c->addr));
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"]   Address:    %s:%d\n", handle->handle_id, address, port);
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"]   Priority:   %d\n", handle->handle_id, c->priority);
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"]   Foundation: %s\n", handle->handle_id, c->foundation);
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"]   Username:   %s\n", handle->handle_id, c->username);
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"]   Password:   %s\n", handle->handle_id, c->password);
+		JANUS_LOG(LOG_VERB, "[%"SCNu64"] Queueing candidate %p (startup)\n", handle->handle_id, c);
+		if(handle->queued_candidates != NULL)
+			g_async_queue_push(handle->queued_candidates, c);
 		gsc = gsc->next;
 	}
-	gint added = nice_agent_set_remote_candidates(handle->agent, stream_id, component_id, component->candidates);
-	if(added < 0 || (guint)added < g_slist_length(component->candidates)) {
-		JANUS_LOG(LOG_ERR, "[%"SCNu64"] Failed to set remote candidates :-( (added %u, expected %u)\n",
-			handle->handle_id, (guint)added, g_slist_length(component->candidates));
-	} else {
-		JANUS_LOG(LOG_VERB, "[%"SCNu64"] Remote candidates set!\n", handle->handle_id);
-		component->process_started = TRUE;
+	if(handle->queued_packets != NULL) {
+#if GLIB_CHECK_VERSION(2, 46, 0)
+		g_async_queue_push_front(handle->queued_packets, &janus_ice_add_candidates);
+#else
+		g_async_queue_push(handle->queued_packets, &janus_ice_add_candidates);
+#endif
+		g_main_context_wakeup(handle->mainctx);
 	}
+	component->process_started = TRUE;
 }
 
 int janus_ice_setup_local(janus_ice_handle *handle, int offer, int audio, int video, int data, int trickle) {
@@ -3501,7 +3527,8 @@ int janus_ice_setup_local(janus_ice_handle *handle, int offer, int audio, int vi
 	/* FIXME: libnice supports this since 0.1.0, but the 0.1.3 on Fedora fails with an undefined reference! */
 	nice_agent_set_port_range(handle->agent, handle->stream_id, 1, rtp_range_min, rtp_range_max);
 #endif
-	if(!nice_agent_gather_candidates(handle->agent, handle->stream_id)) {
+	/* Gather now only if we're doing hanf-trickle */
+	if(!janus_full_trickle_enabled && !nice_agent_gather_candidates(handle->agent, handle->stream_id)) {
 		JANUS_LOG(LOG_ERR, "[%"SCNu64"] Error gathering candidates...\n", handle->handle_id);
 		janus_flags_clear(&handle->webrtc_flags, JANUS_ICE_HANDLE_WEBRTC_HAS_AGENT);
 		janus_ice_webrtc_hangup(handle, "Gathering error");
@@ -3525,6 +3552,14 @@ int janus_ice_setup_local(janus_ice_handle *handle, int offer, int audio, int vi
 		return -1;
 	}
 	janus_refcount_increase(&component->dtls->ref);
+	/* If we're doing full-tricke, start gathering asynchronously */
+	if(janus_full_trickle_enabled) {
+#if GLIB_CHECK_VERSION(2, 46, 0)
+		g_async_queue_push_front(handle->queued_packets, &janus_ice_start_gathering);
+#else
+		g_async_queue_push(handle->queued_packets, &janus_ice_start_gathering);
+#endif
+	}
 	return 0;
 }
 
@@ -3948,7 +3983,35 @@ static gboolean janus_ice_outgoing_traffic_handle(janus_ice_handle *handle, janu
 	janus_session *session = (janus_session *)handle->session;
 	janus_ice_stream *stream = handle->stream;
 	janus_ice_component *component = stream ? stream->component : NULL;
-	if(pkt == &janus_ice_dtls_handshake) {
+	if(pkt == &janus_ice_start_gathering) {
+		/* Start gathering candidates */
+		if(!nice_agent_gather_candidates(handle->agent, handle->stream_id)) {
+			JANUS_LOG(LOG_ERR, "[%"SCNu64"] Error gathering candidates...\n", handle->handle_id);
+			janus_ice_webrtc_hangup(handle, "ICE gathering error");
+		}
+		return G_SOURCE_CONTINUE;
+	} else if(pkt == &janus_ice_add_candidates) {
+		/* There are remote candidates pending, add them now */
+		GSList *candidates = NULL;
+		NiceCandidate *c = NULL;
+		while((c = g_async_queue_try_pop(handle->queued_candidates)) != NULL) {
+			JANUS_LOG(LOG_VERB, "[%"SCNu64"] Processing candidate %p\n", handle->handle_id, c);
+			candidates = g_slist_append(candidates, c);
+		}
+		guint count = g_slist_length(candidates);
+		if(stream != NULL && component != NULL && count > 0) {
+			int added = nice_agent_set_remote_candidates(handle->agent, stream->stream_id, component->component_id, candidates);
+			if(added < 0 || (guint)added != count) {
+				JANUS_LOG(LOG_WARN, "[%"SCNu64"] Failed to add some remote candidates (added %u, expected %u)\n",
+					handle->handle_id, added, count);
+			} else {
+				JANUS_LOG(LOG_VERB, "[%"SCNu64"] %d remote %s added\n", handle->handle_id,
+					count, (count > 1 ? "candidates" : "candidate"));
+			}
+		}
+		g_slist_free(candidates);
+		return G_SOURCE_CONTINUE;
+	} else if(pkt == &janus_ice_dtls_handshake) {
 		if(!janus_is_webrtc_encryption_enabled()) {
 			JANUS_LOG(LOG_WARN, "[%"SCNu64"] WebRTC encryption disabled, skipping DTLS handshake\n", handle->handle_id);
 			janus_ice_dtls_handshake_done(handle, component);
diff --git a/ice.h b/ice.h
index fb9689964b..151daa14bf 100644
--- a/ice.h
+++ b/ice.h
@@ -329,6 +329,8 @@ struct janus_ice_handle {
 	const gchar *hangup_reason;
 	/*! \brief List of pending trickle candidates (those we received before getting the JSEP offer) */
 	GList *pending_trickles;
+	/*! \brief Queue of remote candidates that still need to be processed */
+	GAsyncQueue *queued_candidates;
 	/*! \brief Queue of events in the loop and outgoing packets to send */
 	GAsyncQueue *queued_packets;
 	/*! \brief Count of the recent SRTP replay errors, in order to avoid spamming the logs */
@@ -656,6 +658,10 @@ int janus_ice_setup_local(janus_ice_handle *handle, int offer, int audio, int vi
  * @param[in] stream_id The stream ID of the candidate to add to the SDP
  * @param[in] component_id The component ID of the candidate to add to the SDP */
 void janus_ice_candidates_to_sdp(janus_ice_handle *handle, janus_sdp_mline *mline, guint stream_id, guint component_id);
+/*! \brief Method to queue a remote candidate for processing
+ * @param[in] handle The Janus ICE handle this method refers to
+ * @param[in] c The remote NiceCandidate to process */
+void janus_ice_add_remote_candidate(janus_ice_handle *handle, NiceCandidate *c);
 /*! \brief Method to handle remote candidates and start the connectivity checks
  * @param[in] handle The Janus ICE handle this method refers to
  * @param[in] stream_id The stream ID of the candidate to add to the SDP
diff --git a/janus.c b/janus.c
index 4635e6e32c..c4f5ba1966 100644
--- a/janus.c
+++ b/janus.c
@@ -3521,6 +3521,7 @@ json_t *janus_plugin_handle_sdp(janus_plugin_session *plugin_session, janus_plug
 	}
 	if(!updating && !janus_ice_is_full_trickle_enabled()) {
 		/* Wait for candidates-done callback */
+		int waiting = 0;
 		while(ice_handle->cdone < 1) {
 			if(ice_handle == NULL || janus_flags_is_set(&ice_handle->webrtc_flags, JANUS_ICE_HANDLE_WEBRTC_STOP)
 					|| janus_flags_is_set(&ice_handle->webrtc_flags, JANUS_ICE_HANDLE_WEBRTC_ALERT)) {
@@ -3528,13 +3529,16 @@ json_t *janus_plugin_handle_sdp(janus_plugin_session *plugin_session, janus_plug
 				janus_sdp_destroy(parsed_sdp);
 				return NULL;
 			}
-			JANUS_LOG(LOG_VERB, "[%"SCNu64"] Waiting for candidates-done callback...\n", ice_handle->handle_id);
-			g_usleep(10000);
 			if(ice_handle->cdone < 0) {
 				JANUS_LOG(LOG_ERR, "[%"SCNu64"] Error gathering candidates!\n", ice_handle->handle_id);
 				janus_sdp_destroy(parsed_sdp);
 				return NULL;
 			}
+			if((waiting % 500) == 0) {
+				JANUS_LOG(LOG_VERB, "[%"SCNu64"] Waiting for candidates-done callback...\n", ice_handle->handle_id);
+			}
+			waiting++;
+			g_usleep(1000);
 		}
 	}
 	/* Anonymize SDP */
diff --git a/sdp.c b/sdp.c
index 3c6a97b6e2..f149a29445 100644
--- a/sdp.c
+++ b/sdp.c
@@ -392,7 +392,7 @@ int janus_sdp_process(void *ice_handle, janus_sdp *remote_sdp, gboolean update)
 			/* If this is a renegotiation, check if this is an ICE restart */
 			if((ruser && stream->ruser && strcmp(ruser, stream->ruser)) ||
 					(rpass && stream->rpass && strcmp(rpass, stream->rpass))) {
-				JANUS_LOG(LOG_WARN, "[%"SCNu64"] ICE restart detected\n", handle->handle_id);
+				JANUS_LOG(LOG_INFO, "[%"SCNu64"] ICE restart detected\n", handle->handle_id);
 				janus_flags_clear(&handle->webrtc_flags, JANUS_ICE_HANDLE_WEBRTC_ALL_TRICKLES);
 				janus_flags_set(&handle->webrtc_flags, JANUS_ICE_HANDLE_WEBRTC_ICE_RESTART);
 			}
@@ -881,15 +881,8 @@ int janus_sdp_parse_candidate(void *ice_stream, const char *candidate, int trick
 							JANUS_LOG(LOG_VERB, "[%"SCNu64"] ICE already started for this component, setting candidates we have up to now\n", handle->handle_id);
 							janus_ice_setup_remote_candidates(handle, component->stream_id, component->component_id);
 						} else {
-							GSList *candidates = NULL;
-							candidates = g_slist_append(candidates, c);
-							if(nice_agent_set_remote_candidates(handle->agent, stream->stream_id, component->component_id, candidates) < 1) {
-								JANUS_LOG(LOG_ERR, "[%"SCNu64"] Failed to add trickle candidate :-( (%s)\n",
-									handle->handle_id, candidate);
-							} else {
-								JANUS_LOG(LOG_HUGE, "[%"SCNu64"] Trickle candidate added!\n", handle->handle_id);
-							}
-							g_slist_free(candidates);
+							/* Queue the candidate, we'll process it in the loop */
+							janus_ice_add_remote_candidate(handle, c);
 						}
 					} else {
 						/* ICE hasn't started yet: to make sure we're not stuck, also check if we stopped processing the SDP */
@@ -901,14 +894,8 @@ int janus_sdp_parse_candidate(void *ice_stream, const char *candidate, int trick
 								JANUS_LOG(LOG_VERB, "[%"SCNu64"] SDP processed but ICE not started yet for this component, setting candidates we have up to now\n", handle->handle_id);
 								janus_ice_setup_remote_candidates(handle, component->stream_id, component->component_id);
 							} else {
-								GSList *candidates = NULL;
-								candidates = g_slist_append(candidates, c);
-								if(nice_agent_set_remote_candidates(handle->agent, stream->stream_id, component->component_id, candidates) < 1) {
-									JANUS_LOG(LOG_ERR, "[%"SCNu64"] Failed to add trickle candidate :-(\n", handle->handle_id);
-								} else {
-									JANUS_LOG(LOG_HUGE, "[%"SCNu64"] Trickle candidate added!\n", handle->handle_id);
-								}
-								g_slist_free(candidates);
+								/* Queue the candidate, we'll process it in the loop */
+								janus_ice_add_remote_candidate(handle, c);
 							}
 						} else {
 							/* Still processing the offer/answer: queue the trickle candidate for now, we'll process it later */
