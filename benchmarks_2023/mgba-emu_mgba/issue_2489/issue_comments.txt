Blending with white/black should not be applied to semi-transparent sprites when target 1 excludes sprites and target 2 includes disabled backgrounds
I believe I hit the same or a similar bug a while back... in fact I didn't realise it was a bug, and thought it was the other emulators that were doing it wrong :sweat_smile: 

Here's an example:
https://exelo.tl/files/misc/goodboy-blend-demo.gba

mGBA behaviour:
![image](https://user-images.githubusercontent.com/569607/198857570-c1d0717e-2652-4dfc-83df-affc6888b2c4.png)

hardware behaviour:
![image](https://user-images.githubusercontent.com/569607/198857573-1ccacdaf-aaef-48bc-903a-0230a22516fb.png)

In my code I have:
* set the BLDCNT mode to darkening and opt _all_ layers into _both_ targets
* enable the "blend" effect on a cluster of sprites which form a blue rectangle
* transition BLDY fade value from 16 (fully dark) to 0 (fully visible)
* transition BLDALPHA coefficients from 0:16 (sprites fully transparent) to 12:4 (sprites at around ~80% opacity)
[Another test ROM](https://github.com/mgba-emu/mgba/files/9895380/blending.zip).

Blending weights can be changed with the keypad.

![blending](https://user-images.githubusercontent.com/13147012/198868743-cd12fdc5-824d-41c1-bb8d-4765110cb917.png)

@exelotl that, if it is a bug (please check on hardware), is unrelated to this bug. This bug is about erroneously taking a disabled background into the blending check.
@GValiente What was that screenshot taken on? It doesn't look like that on mGBA either before or after the fix for the OP issue. If that's how it looks on hardware, it might be the same as exelotl's issue, which is not the same as the OP's issue.

I'm still testing my fix though, just to make sure it doesn't regress anything.
The GL renderer appears to still get this wrong so I'm reopening it until I fix it there too