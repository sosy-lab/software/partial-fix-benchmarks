mpy-cross needs to know sizeof(nlr_buf_t) on target when generating native code
> The first one can partially be calculated from `native_arch`, but you also need to know whether soft floating point is enabled.

IIRC `MP_NATIVE_ARCH_ARMV7EMSP` and `MP_NATIVE_ARCH_ARMV7EMDP` specify single and double precision FP support (they are currently unused because native code doesn't use any FP instructions).

> But it looks like we also need flags for: pystack enabled, settrace enabled, stackless enabled

Good point, I guess that's all part of the ABI.  For settrace and stackless they aren't supported/needed by native code so one option here would be to define a `mp_native_code_state_t` which only has what native needs, and should not have any optional entries.

The nlr buf issue was fixed by b596638b9b28975adee4a06a92497b5d9dbba34c.

The other issues still need to be fixed, and #5120 enables tests for it.
The remaining issue was fixed by 1762990579ff8ddc285e24492626fe40b6f2621d