py: Revert change of include, "" back to <> for mpconfigport.h.
Add __assert_func

* issue #692
Add copysignf

* Fix #692
Merge pull request #692 from tannewt/latest_m4

Add support for the Metro M4 Rev F and make the SPI flash code more robust
