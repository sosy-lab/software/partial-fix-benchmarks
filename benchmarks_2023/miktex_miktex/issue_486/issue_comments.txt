Luatex cannot find input file when using texify on windows
This has been fixed.
I'm using MiKTeX on Windows 10 and I've been having a similar issue when the path to my tex file contains spaces. I reinstalled and updated MiKTeX (2.9.7400) today, and I was able to reproduce this issue almost identically, with the added requirement that at least one of the parent directories of the file or the file itself has a space in its name. Otherwise, texify works as expected. Again, it only happens when the luatex engine option is specified in texify, and it also seems to be an issue with the mishandling of backslashes in the path name.

The log files seem pretty similar, but here they are in case they are useful.
## Log

### Texify output

~~~~
D:\Users\Directory with spaces>texify -b -p --engine="luatex" Standalone.tex
This is LuaHBTeX, Version 1.12.0 (MiKTeX 2.9.7400 64-bit)
 restricted system commands enabled.
! Undefined control sequence.
<*> "D:/Users/Directory with spaces\Standalone
                                           .tex"
! I can't find file `D:/Users/Directory with spaces.tex'.
<*> "D:/Users/Directory with spaces\Standalone.tex"

(Press Enter to retry, or Control-Z to exit)
Please type another input file name: ^Z

! Emergency stop.
<*> "D:/Users/Directory with spaces\Standalone.tex"

 270 words of node memory still in use:
   1 hlist, 39 glue_spec nodes
   avail lists: 2:12,3:1,4:1,5:3
!  ==> Fatal error occurred, no output PDF file produced!
Transcript written on texput.log.

Sorry, but "MiKTeX Compiler Driver" did not succeed.

The log file hopefully contains the information to get MiKTeX going again:

  C:\Users\username\AppData\Local\MiKTeX\2.9\miktex\log\texify.log
~~~~

### Luatex log
~~~~
2020-04-20 11:49:08,683-0400 INFO  lualatex - this process (6300) started by 'texify' with command line: lualatex --interaction=scrollmode "D:/Users/Directory with spaces\Standalone.tex"
2020-04-20 11:49:12,208-0400 INFO  lualatex - this process (6300) finishes with exit code 1
2020-04-20 11:49:12,209-0400 ERROR miktex.core - still open: C:\Users\awaw6\AppData\Local\MiKTeX\2.9\miktex/data/le/luahbtex\lualatex.fmt
~~~~

### Texify log
~~~~
2020-04-20 11:49:08,467-0400 INFO  texify - this process (10168) started by 'cmd' with command line: texify -b -p --engine=luatex Standalone.tex
2020-04-20 11:49:08,514-0400 INFO  texify.core - start process: lualatex --interaction=scrollmode "D:/Users/Directory with spaces\Standalone.tex"
2020-04-20 11:49:12,251-0400 FATAL texify.core - TeX engine failed for some reason (see log file).
2020-04-20 11:49:12,251-0400 FATAL texify.core - Data: 
2020-04-20 11:49:12,251-0400 FATAL texify.core - Source: Programs\MiKTeX\texify\mcd.cpp:1528
2020-04-20 11:49:12,253-0400 FATAL texify - TeX engine failed for some reason (see log file).
2020-04-20 11:49:12,253-0400 FATAL texify - Info: 
2020-04-20 11:49:12,253-0400 FATAL texify - Source: Programs\MiKTeX\texify\mcd.cpp
2020-04-20 11:49:12,253-0400 FATAL texify - Line: 1528
~~~~

### initexmf-report
[initexmf-report.txt](https://github.com/MiKTeX/miktex/files/4504680/initexmf-report.txt)
### Simple input file
~~~~latex
\documentclass{article} \title{Title} \author{Your Name} \begin{document} \maketitle \section{Introduction} This is where you will write your content. \end{document}
~~~~
Thank you, will try to reproduce it...
Thank you. MiKTeX-LuaTeX now accepts path names with spaces.