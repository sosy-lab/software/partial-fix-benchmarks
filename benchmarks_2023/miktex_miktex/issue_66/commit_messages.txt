Bug fix: internal error (#68)
Don't finish setup in portable mode (#66)
MiKTeX-Portable: fix miktexstartup.ini location (#66)
