miniupnpd/netfilter_nft: add debug messages about lease timestamps/duration

in order to debug issue #466
miniupnpd: improve AddAnyPortMapping()

try with next port when  -3 permission check failed

see #465
miniupnpd: AddAnyPortMapping() tries port above and below requested port

fixes #465
if the requested port is n, it will tries successively :
n, n+1, n-1, n+2, n-2, n+3, n-3, etc.
AddAnyPortMapping(): Only try allowed ports

build an array of all allowed ports.
should fix #465
Fix undefined behaviour: shifting signed int by 31 place

see #465

     #0 0x555719469ec5 in AddAnyPortMapping.cfi /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnpsoap.c:703:42
     #1 0x5557194705a7 in ExecuteSoapAction /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnpsoap.c:2335:5
 SUMMARY: UndefinedBehaviorSanitizer: undefined-behavior upnpsoap.c:703:42 in
