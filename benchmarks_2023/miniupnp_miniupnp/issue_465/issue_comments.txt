Could you slightly change the behavior of AddAnyPortMapping()
@emojifreak do you think it will be useful to also check ports below the initial value ?
like if 10000 is requested and not available, we try 10001, then 9999, then 10002, then 9998, etc.
> @emojifreak do you think it will be useful to also check ports below the initial value ?
> like if 10000 is requested and not available, we try 10001, then 9999, then 10002, then 9998, etc.

Yes, it seems more useful than only trying larger values from the initially given one.
You attached milestone_2_2 tag. **IF** it means that this is planned to be dealt in mid-term and **not short-term**, then a (not the) right solution seems to try ports only within `allowed` range of ports, instead of trying near values to a given initial one by a client application.

I will try to respond to your other comments within two or three hours (i.e. before I'm going to sleep).
> **IF** it means that this is planned to be dealt in mid-term and **not short-term**, then a (not the) right solution seems to try ports only within `allowed` range of ports, instead of trying near values to a given initial one by a client application.

I was somewhat wrong in my last comment above. As `upnp_redirect()` calls the permission check at its very beginning, simply calling `upnp_redirect()` at ports of the initial vaue, the initial value ± 1, the initial value ± 2, ... in that order will simply realize what I subggested above...

@emojifreak could you test the new implementation of AddAnyPortMapping(), please ?
@miniupnp 
Hi, thank you for your work. The behavior seems somewhat strange, as below:

The following is fine...

```
$ ./miniupnp/miniupnpc/upnpc-static -n 192.168.1.76 65534 65534 TCP 1800
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:34321/30652ad1/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:34321/30652ad1/ctl/IPConn
Local LAN ip address : 192.168.1.76
ExternalIPAddress = 153.240.174.134
InternalIP:Port = 192.168.1.76:65534
external 153.240.174.134:65231 TCP is redirected to internal 192.168.1.76:65534 (duration=1799)
```

The next command fails. It seems that the server takes too long time and the client timed out...

```
$ ./miniupnp/miniupnpc/upnpc-static -n 192.168.1.76 2000 2000 TCP 1800
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:34321/30652ad1/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:34321/30652ad1/ctl/IPConn
Local LAN ip address : 192.168.1.76
ExternalIPAddress = 153.240.174.134
AddAnyPortMapping(2000, 2000, 192.168.1.76) failed with code -3 (Miniupnpc HTTP error)
GetSpecificPortMappingEntry() failed with code -3 (Miniupnpc HTTP error)
```

Under the following situation:

```
$ ./miniupnp/miniupnpc/upnpc-static -L
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:34321/30652ad1/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:34321/30652ad1/ctl/IPConn
Local LAN ip address : 192.168.1.76
 i protocol exPort->inAddr:inPort description remoteHost leaseTime
 0 TCP 65231->192.168.1.76:65534 'libminiupnpc' '' 1279
 1 TCP 65216->192.168.1.76:65200 'libminiupnpc' '' 1414
```

The next command overwrites port mapping of 65216. Is it OK??

```
$ ./miniupnp/miniupnpc/upnpc-static -n 192.168.1.76 65100 65100 TCP 1800
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:34321/30652ad1/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:34321/30652ad1/ctl/IPConn
Local LAN ip address : 192.168.1.76
ExternalIPAddress = 153.240.174.134
InternalIP:Port = 192.168.1.76:65100
external 153.240.174.134:65216 TCP is redirected to internal 192.168.1.76:65100 (duration=1799)
ryutaroh@ryutaroh-CFSZ6-1L:~$ ./miniupnp/miniupnpc/upnpc-static -L
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:34321/30652ad1/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:34321/30652ad1/ctl/IPConn
Local LAN ip address : 192.168.1.76
 i protocol exPort->inAddr:inPort description remoteHost leaseTime
 0 TCP 65231->192.168.1.76:65534 'libminiupnpc' '' 1232
 1 TCP 65216->192.168.1.76:65100 'libminiupnpc' '' 1784
```


What's the log on miniupnpd side ?
I did

```
$ upnpc -n 192.168.1.72 40000 40000 TCP 1800
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2019 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:40645/34ac528c/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:40645/34ac528c/ctl/IPConn
Local LAN ip address : 192.168.1.72
ExternalIPAddress = 153.240.174.134
AddAnyPortMapping(40000, 40000, 192.168.1.72) failed with code -3 (Miniupnpc HTTP error)
GetSpecificPortMappingEntry() failed with code -3 (Miniupnpc HTTP error)
```

The following server log looks relevant...:

```
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 31105->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 48896->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 31104->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 48897->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 31103->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 48898->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: miniupnpd[77844]: redirection permission check failed for 31102->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: redirection permission check failed for 31170->192.168.1.72:40000 TCP
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
Jun 21 19:19:42 router1 miniupnpd[77844]: UPnP permission rule 1 matched : port mapping rejected
```

The same lines repeat roughly 25000 times, then I see

```
Jun 21 19:20:03 router1 systemd-journald[264]: Suppressed 98140 messages from miniupnpd.service
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 systemd-journald[264]: Suppressed 7805 messages from miniupnpd.service
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ifindex = 3  192.168.1.2
Jun 21 19:20:03 router1 miniupnpd[77844]: level=0 type=8
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ST: uuid:54d043c5-1006-4e89-8328-70e51c13db0b (ver=54)
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: SSDP M-SEARCH from 192.168.1.1:63720 ST: uuid:54d043c5-1006-4e89-8328-70e51c13db0b
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: ssdp:uuid (IGD) found
Jun 21 19:20:03 router1 miniupnpd[77844]: miniupnpd[77844]: SendSSDPResponse(): 0 bytes to 192.168.1.1:63720 ST: HTTP/1.1 200 OK
```




I think I need to add a method to check that is possible to find an authorized external port for the internal host:port
My config has been always:

```
allow 65216-65231 192.168.1.0/24 0-65535
deny 0-65535 0.0.0.0/0 0-65535
```

though I don't know if it is relevant to this issue...
@emojifreak could you test with commit 18a6ab0  ? (branch `issue-465`)
Yes, please give me 1 or 2 days.
I had the following error...:

```
ryutaroh@ryutaroh-CFSZ6-1L:~/miniupnp/miniupnpc$ ./upnpc-static  -n 192.168.1.75 40000 40000 TCP 1800
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:44505/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:44505/ctl/IPConn
Local LAN ip address : 192.168.1.75
ExternalIPAddress = 153.240.174.134
AddAnyPortMapping(40000, 40000, 192.168.1.75) failed with code -3 (Miniupnpc HTTP error)
GetSpecificPortMappingEntry() failed with code -3 (Miniupnpc HTTP error)
```

Strangely, I had no relevant log at the server side:

```
Oct 18 12:43:58 router1 systemd[1]: Starting UPnP Internet Gateway Device Daemon...
Oct 18 12:43:58 router1 nft_init.sh[41728]: create nat table
Oct 18 12:43:58 router1 nft_init.sh[41728]: create chain in nat table
Oct 18 12:43:58 router1 nft_init.sh[41728]: create pcp peer chain in nat table
Oct 18 12:43:58 router1 nft_init.sh[41728]: create filter table
Oct 18 12:43:58 router1 nft_init.sh[41728]: create chain in filter table
Oct 18 12:43:58 router1 systemd[1]: Started UPnP Internet Gateway Device Daemon.
Oct 18 12:43:58 router1 miniupnpd[41735]: HTTP listening on port 44505
Oct 18 12:43:58 router1 miniupnpd[41735]: HTTP IPv6 address given to control points : [2400:4050:2ba1:ac00:99:f0ae:8600:2c00]
Oct 18 12:43:58 router1 miniupnpd[41735]: Listening for NAT-PMP/PCP traffic on port 5351
Oct 18 12:43:58 router1 miniupnpd[41735]: PCPSendUnsolicitedAnnounce() IPv6 sendto(): Cannot assign requested address
```
@emojifreak are you running `miniupnpd` with the `-d` option to get more detailed logs ?
@emojifreak  I think I spotted the bug. Please test with 6b2070c6e977716c1a13eba52e8c9bed43cf8843
@miniupnp Thank you for your investigation. Please give me 1 or 2 days.
@miniupnp Thanks, this issue seems fixed, but another issue was exposed as below.

```
$ ./upnpc-static  -n 192.168.1.75 40000 40000 TCP 1800
upnpc : miniupnpc library test client, version 2.1.
 (c) 2005-2020 Thomas Bernard.
Go to http://miniupnp.free.fr/ or https://miniupnp.tuxfamily.org/
for more information.
List of UPNP devices found on the network :
 desc: http://192.168.1.2:35435/rootDesc.xml
 st: urn:schemas-upnp-org:device:InternetGatewayDevice:1

Found valid IGD : http://192.168.1.2:35435/ctl/IPConn
Local LAN ip address : 192.168.1.75
ExternalIPAddress = 153.240.174.134
InternalIP:Port = 192.168.1.75:40000
external 153.240.174.134:65216 TCP is redirected to internal 192.168.1.75:40000 (duration=1799)
```

Worked fine as above. But the server log showed an UBSan error as below:

```
Oct 25 10:57:04 router1 systemd[1]: Started UPnP Internet Gateway Device Daemon.
Oct 25 10:57:04 router1 miniupnpd[86901]: HTTP listening on port 35435
Oct 25 10:57:04 router1 miniupnpd[86901]: HTTP IPv6 address given to control points : [2400:4050:2ba1:ac00:99:f0ae:8600:2c00]
Oct 25 10:57:04 router1 miniupnpd[86901]: Listening for NAT-PMP/PCP traffic on port 5351
Oct 25 10:57:04 router1 miniupnpd[86901]: PCPSendUnsolicitedAnnounce() IPv6 sendto(): Cannot assign requested address
Oct 25 10:59:04 router1 miniupnpd[86901]: upnpsoap.c:703:42: runtime error: left shift of 1 by 31 places cannot be represented in type 'int'
Oct 25 10:59:05 router1 miniupnpd[86901]:     #0 0x555719469ec5 in AddAnyPortMapping.cfi /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnpsoap.c:703:42
Oct 25 10:59:05 router1 miniupnpd[86901]:     #1 0x5557194705a7 in ExecuteSoapAction /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnpsoap.c:2335:5
Oct 25 10:59:05 router1 miniupnpd[86901]:     #2 0x55571945bd5d in ProcessHTTPPOST_upnphttp /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnphttp.c:496:4
Oct 25 10:59:05 router1 miniupnpd[86901]:     #3 0x555719456cd3 in ProcessHttpQuery_upnphttp /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnphttp.c:840:3
Oct 25 10:59:05 router1 miniupnpd[86901]:     #4 0x5557194529af in Process_upnphttp /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/upnphttp.c:980:5
Oct 25 10:59:05 router1 miniupnpd[86901]:     #5 0x555719446352 in main /home/ryutaroh/miniupnpd-1018/miniupnp/miniupnpd/miniupnpd.c:2913:6
Oct 25 10:59:05 router1 miniupnpd[86901]:     #6 0x7fd19fc6dcc9 in __libc_start_main csu/../csu/libc-start.c:308:16
Oct 25 10:59:05 router1 miniupnpd[86901]:     #7 0x555719399879 in _start (/usr/sbin/miniupnpd+0x82879)
Oct 25 10:59:05 router1 miniupnpd[86901]: SUMMARY: UndefinedBehaviorSanitizer: undefined-behavior upnpsoap.c:703:42 in
```


@emojifreak it should be fixed with 90259ae
Yes, everything works fine as expected!