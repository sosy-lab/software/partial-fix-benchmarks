tweak and guard DPI change handling (#470)
stretching narrow characters that are expected wide (#123)
revert change (¬#123) that spoiled wide character display (#570)
expand characters as appropriate for wide display (#123, /#570)
