Stretch narrow characters that are meant to be wide
Original comment by `andy.koppe` on 20 Jun 2009 at 6:56
- Changed state: **Accepted**

Original comment by `andy.koppe` on 24 Oct 2010 at 9:41
- Added labels: **Priority-Low**
- Removed labels: **Priority-Medium**

```
Yes, right. Stretching or centering them in a two-cell space.
```

Original comment by `towom...@googlemail.com` on 31 May 2012 at 11:30

Implemented (except for VT100 wide lines) and released in 2.4.1.

The tweak had to be reverted for now as it spoils wide character display (#570).

Released in 2.6.2.
