Ligature support
This may be beyond the Windows API currently used for text output (ExtTextOut), see #430 and https://github.com/mintty/mintty/issues/438#issuecomment-232723547.
Please describe more precisely what you are missing and what you see (e.g. rendered "fl" desired to be ligature <fl>?).

Fira Code has some crazy ligatures for programming while remaining a
monospace font. -> is a ligature, so is ==.

The issue is that the raw characters are displayed and not the ligatures.
So you are not asking about any Unicode ligatures but rather font-specific ligatures.
I am not aware which Windows API, if any, would be able to handle them implicitly.
So it might be necessary to handle font information explicitly in mintty to achieve that.
You are welcome to make a proposal how to implement that.

This functionality may be available with the [Uniscribe API](https://msdn.microsoft.com/en-us/library/windows/desktop/dd374093%28v=vs.85%29.aspx).
Handling the complexity of that API is however beyond my current ambition to enhance mintty.

So here's your homework :):
- Either find any application with source code that implements this, find how it detects which character pairs have ligatures (mapping them to font glyphs).
- Or find out how the TrueType cmap tables can be retrieved using the Windows API and which table contains the ligature information.

@be5invis: taking over this issue here...

> You cannot enable ligatures unless you switch from GDI to DirectWrite or FreeType.

FreeType is an external library, and DirectWrite as well as Uniscribe are of deterring complexity. I've experimented with Uniscribe, though, and it does not seem to support ligatures (so I guess notepad uses DirectWrite...).

_But:_ The information must be available somewhere in the font file. And using a mapping table like 
('-', '>', '⟶') mintty could output those glyphs by glyph index rather than character code, like it does for right-to-left text already.

@mintty Uniscribe does not support enabling arbitrary OpenType feature tag unless it is one of the “required” tags for typesetting complex scripts (like Devanagari).

@be5invis Thanks for the confirmation.
What about the ligature information; isn't it available as a "table" in TrueType? Any idea how to access it?

@mintty It’s a **feature** located in `GSUB` table, and it is corresponded to a list of **lookups**. For most programming fonts, the feature is tagged as `calt`.

Hmm, using a `ttfdump` tool, Iosevka doesn't seem to have any `calt` entries, and Fira Code has only 16 of them, looking as mysterious as this:

```
'calt'
FeatureT07b6
                      ; FeatureRecord[17]
...
Feature FeatureT07b6
NULL
3
25
26
27
```

Probably not worth the effort...

@mintty To avoid some compatibility issues under Linux, Iosevka Term turned off ligatures by default.
A feature is (usually) an array of lookup indexes, indicates if this feature is enabled, which of the lookups should be set to active.

"By default" means the information is not contained in the font?

Whatever, I think I'll just close this issue unless someone enthusiastic about the feature will
- demonstrate how it would be useful for at least a few fonts, and not for just 16 characters of one strange font (just look at the ugly 'm' of Fira Code!)
- provide code how to retrieve a suitable mapping table from a TrueType font (starting with the GDI function `GetFontData`) because I'm not going to dig into the incredibly over-engineered insanity of the TrueType format myself (that person would gain a place on the mintty credits page, of course)

@mintty Iosevka Term does not have any ligature data, while Iosevka does.

In most cases, the `calt` is implemented as a contextual substitution, which means that it will try to match a glyph sequence, and apply some other lookups (usually a single substitution, or a multiple-to-one substitution) to the specified position of the match. It is really hard to interpret it, especially you are on GDI.

> - provide code how to retrieve a suitable mapping table from a TrueType font 

or make an implementation proposal to use DirectWrite, see #430

DirectWrite has GDI compatible mode. If you use it, you don't need to switch all graphics code to Direct2D.
Vim 8.0 also uses it: https://github.com/vim/vim/blob/master/src/gui_dwrite.cpp

In referring issue #605, I tried to evaluate Uniscribe for this purpose.
About DirectWrite, thanks to @k-takata for referring to that example, but I'm not going to reverse-engineer 900 lines of code to understand that nightmare of an API, sorry.
If anyone comes up with a half-way working demo solution using either Uniscribe or DirectWrite, that will help.

Maybe @koron can help figuring this out for mintty, too. Would also be a major break-though for vim users (like me) that rather use the command line on windows (ie. cygwin). Sorry for bumping this issue: still one year later there seems to be no actual terminal (that can e.g. run tmux) on windows with ligature support. 
Mintty already uses Uniscribe. I don't think there is additional benefit by using DirectWrite with GDI-compatible mode.
I think @riedel was referring to ligature support, not DirectWrite.
Uniscribe does not seem to provide it, at least not implicitly.
Oh, I misunderstood that Uniscribe provides (some kind of) ligature support.
@k-takata: you are right: tt seems that uniscribe supports ligatures, but not the type of OpenType ligatures eg. used by Fira Code.

This uniscribe tutorial shows that `ScriptStingOut `does not work: https://www.catch22.net/tuts/introduction-uniscribe .  

https://maxradi.us/documents/uniscribe/ talks about font-specific ligature and how to use the more complex workflow using `ScriptShape` to compute ligatures, but I think this also does not apply to Opentype ligatures.  ~~Butit seems unlikely that this API level works any better.~~ 

Edit 2: This slightly more complex API seems to do the trick and works with Fira Code: https://www.catch22.net/tuts/drawing-styled-text-uniscribe

Edit 1: Windows offers some OpenType specific uniscribe functions:  https://msdn.microsoft.com/en-us/library/windows/desktop/dd317792(v=vs.85).aspx
I accidentally just saw that sometimes ligatures are actually working: 

![firacode](https://user-images.githubusercontent.com/566485/32134178-cea1bf66-bbe7-11e7-8eca-cca2e6bd6835.png)

What's the actual output rendered here? Can you provide a screen log please (e.g. mintty -l)?
I ran `mintty -l x.txt sh -c "head -204 Developer_Space%2FStandards.mw|tail -1"`, which gives you only the relevant line. I confirmed that it renders the ligature (only in the beginning but not the end of the line)

This is the output:
[x.txt](https://github.com/mintty/mintty/files/1463778/x.txt)

I was on the train writing a document at the time and wasn't looking any further. Now I looked into it  because it worked for none of the other lines, which seemed strange: it seems that the long dash (hex sequence 0xe28094) in the line triggers rendering the ligatures (others ligatures from fira code work as well).  Should be easy to replicate...
This is a miracle of Uniscribe, I don't think mintty has control over it. However, some observations:
* with `-o FontRender=textout`, no ligatures are applied
* both a wrapped line (longer than the screen width) and the em dash are needed to trigger the leading ligature
* if the line is shortened to screen width, there is a break before the dash, i.e. mintty outputs the line in chunks (as can be seen with #define debug_win_text in wintext.c), and no ligature is applied if the chunk is only "=== Information technology "
Update: a wrapped line is not needed to produce ligatures. At least one more character of some kind (non-ASCII?) after the initial === seems to be necessary. Exact conditions remain mysterious. Scrolling may alter the display (ligature/non-ligature). The whole scenario is chaotic and not likely to be controllable (with Uniscribe).
I have to withdraw my previous assessment this would be "a miracle of Uniscribe".
Actually, there was some presumed "optimization" in mintty which applied a fallback from Uniscribe to TextOut for ASCII-only chunks, thus obstructing the application of ligatures for ASCII characters (unless combined with non-ASCII characters in a chunk, therefore the strange observations above).

The "optimization" was based on an assumed performance penalty of Uniscribe (of < 5%) which can, however, not be reproduced. I'm uploading a fix.
There is still a gap in ligature support while you are on a ligature with the cursor, and also the refresh if you leave the ligature clear from the cursor is not always immediate. 
Thanks! Had to try it out immediately. Works nicely for many programming ligatures now. Somehow as you see in the image some Fira Code ligatures seem to be different such as `->` and `=>`. As you described also the others also only work if the are redrawn together, but this is already pretty cool!

![ligatures](https://user-images.githubusercontent.com/566485/34486806-c85ab3b0-efd1-11e7-86c4-a8f8e4f02514.png)
vs.
![ligatures-electron](https://user-images.githubusercontent.com/566485/34487570-2c2cb340-efd5-11e7-97cd-6d8f704c9ca1.png)

```
const func = (a, b, c, d, e, f, s, n) => {
    if (a++ && b-- || c >= d || e <= f) {      
    }
    if (s === 'string' && n !== 999) {
    }
    if (a == b && c != d)
    
    a <- b <-- c <== d

    a <| b |> c -> d ==> e 
    
  }
```

I've tweaked output chunk breaking to keep longer chunks together as relevant for ligatures.
E.g. `:=` is now applied, but the ligatures with `<` or `>` and dashes are not applied, although they are now output in one chunk (term.c).
How and by what criteria would font rendering actually distinguish between different semantics of character sequences?
Notepad with Fira Code combines `<=` to `≤` not `⇐`.
Released 2.8.3.
As a vim user I noticed that the characters revert to non-ligature versions when the cursor moves over them. However, pressing Ctrl-L in normal mode causes a re-render and I can see the ligatures again. 
Firacode readme says mintty doesn't support ligatures, so I'm going to open a [pull request](https://github.com/tonsky/FiraCode/pull/622) for them. 

![image](https://user-images.githubusercontent.com/988845/42267157-f9c593c2-7f80-11e8-8ef7-a4c1713ac41c.png)

> characters revert to non-ligature versions when the cursor moves over them

Output is in chunks, separated e.g. at changing character attributes, and Windows ligature composition is applicable only to chunks.
I had considered to add support for character sequences with different background (which includes the cursor position). But I wasn't sure it's worth the effort or even really desirable;
if the cursor is within a ligature, you have the option to change one of the ligature base characters - isn't it better in that case to actually see the individual components?
> if the cursor is within a ligature, you have the option to change one of the ligature base characters - isn't it better in that case to actually see the individual components?

Considering the philosophy of ligatures, which is only concerned with better readability, I'd say it isn't worth the effort as I shouldn't be moving my cursor over those characters while reading in the first place. 
FWIW, in qterminal when you type ligatures followed by a space, e.g. `-> ` then it renders the ligature. I'm not sure if there's a reasonable way to trigger re-rendering the text that the cursor has moved over - or even the line that the cursor is on, but that would certainly help.
By what criteria should mintty decide to trigger redisplay?
As this is additional runtime effort (and potential screen flickering), it should not be done unconditionally.
Pending a design proposal, I'll not work on this for the next release which is about soon.
Released 2.9.2.
I think it makes sense to start a conforming implementation (like konsole+firacode) and copy what it does.
I'd prefer if you comment on the current improvement first.

Then - conforming to what? - it may make sense to outline additional requirements/wishes.

Next steps could be a design proposal and then possibly some implementation. I doubt it makes much sense to clone code from a foreign project as the way output is rendered to the screen is likely to vary considerably.

Of course, when I use windows again, I will comment on it.

Just to clarify, by `conforming` I just meant `with ligature support` and
by  suggest'ng konsole as a reference implementation I just meant that
whenever you want to make a decision like whether we want to see the
sequence when cursor is over the glyph, it would make sense to me to just
use some other project as a reference about what to do (not the code)

On Thu, 4 Oct 2018 at 14:36, mintty <notifications@github.com> wrote:

> I'd prefer if you comment on the current improvement first.
>
> Then - conforming to what? - it may make sense to outline additional
> requirements/wishes.
>
> Next steps could be a design proposal and then possibly some
> implementation. I doubt it makes much sense to clone code from a foreign
> project as the way output is rendered to the screen is likely to vary
> considerably.
>
> —
> You are receiving this because you commented.
> Reply to this email directly, view it on GitHub
> <https://github.com/mintty/mintty/issues/601#issuecomment-426985895>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AA8WrduBsAHywF7AbbjqWE_wmQ_PEZ_kks5uhfKvgaJpZM4KSPgF>
> .
>

Released 2.9.3.
`LigaturesSupport=2` works great for me!  Awesome!

As mentioned before an issue for me is the support for those ligatures with a combination of `>`,`=` and `-` in FiraCode:
```
<-< <<- <-- <- <-> -> --> ->> >->
<=< <<= <==    <=> => ==> =>> >=>
>>= >>- >- -< -<< =<<
</ <!--  </>  --> />
<= >= 
```

What puzzles me is that ligatures containing very similar characters render quite nicely for me, such as:
```
     <~~ <~ <~> ~~ ~> ~~>
     == != === !== =/=
     -- ---  
```
(complete list: https://github.com/tonsky/FiraCode/blob/master/showcases/showcases.txt)

I think @mintty also alluded to that fact of partial support in his comments towards https://github.com/tonsky/FiraCode/pull/622
> I've tweaked output chunk breaking to keep longer chunks together as relevant for ligatures.
> E.g. `:=` is now applied, but the ligatures with `<` or `>` and dashes are not applied, although they are now output in one chunk (term.c).

May I ask something about this January post? What's unique with ligatures with `<` or `>`, and will Mintty support them?
Ligatures are actually applied by Windows, as provided by the font.
Mintty support is limited to avoiding unnecessary splitting of output, e.g. by redrawing parts of the line that would not otherwise need to be redrawn. This is why currently ligatures are not supported if they comprise character with different background colour or underline attributes, including the cursor position.
So does a normal `<` or `>` sign always have different attributes? Why are these two signs so special?
They are not really special, but somewhat ambiguous if ligatures are available e.g. for `<=` which could become either `≤` or `⇐`. However, if there is ever a choice between such alternatives, it would be made either by the font or by the Windows framework (Uniscribe), not by mintty.
Actually, the previous comment applies e.g. to `<=` but not to `<-`. The subtle background is that `-` has a different bidi class, so mintty breaks its output there, in order to make sure right-to-left output is properly separated. If that's your use case, please state that explicitly, and maybe provide some more test cases for which you are missing ligatures. However, I don't see right now how that could be handled easily.
I understand there are good reasons not to do ligatures for things like '=>' because of all the use cases you want to support, but I would like to have them in a private fork for my personal use.  I am building mintty myself and have poked around in the code trying to figure out how to enable the extra ligatures I'd like, but I'm not having much luck.  It seems like it should be in src/term.c somewhere, but the changes I have tried so far (including turning on verbose debug output) haven't worked.  Can you point me in the right direction?  Thanks!
As a special hack for `<-`, `->` and other combinations with `-`, you could try to disable line
    {0x002D, 0x002D, ES},
in file bidiclasses.t. If that's what you want, we could consider to reduce the number of cases of setting `break_run` in term.c, particularly depending on `bidi_class` in lines that do not set `has_rtl`.
Thanks, but removing that line didn't seem to change things.  I pulled the latest code, did 'make clean && make', I assume that's all that is needed.  I'm sure the font family supports ligatures for '->' ... if I open the test file I am using in Notepad the arrow is displayed.
I verified that "<-" is in fact handed over to Uniscribe for output in one bunch, even without that patch, actually.
Maybe some parameters to the Uniscribe functions (wintext.c) would make a difference?
hey, @mintty 👋 Mintty is awsome! I've been trying your terminal out and it mostly checks all my list for a terminal. The last is Ligature Support which it seems that mintty actually supports.

I try to edit the shortcut 
`C:\Users\username\AppData\Local\wsltty\bin\mintty.exe -o Padding=200 -o LigaturesSupport=2 --WSL= --configdir="C:\Users\username\AppData\Roaming\wsltty" -~  -`

Am I doing it correctly? I'm using [Cascadia Code](https://github.com/microsoft/cascadia-code) which supports ligature, but it seems that it doesn't work.


The release page says:
> As in 1911.20, this release includes Cascadia Mono, a version of Cascadia that doesn't have ligatures
Hi @mintty, thanks for getting back with me. Happy Holidays! 🤗 I'm not using `Cascadia Mono`, I'm using `Cascadia Code` which does include ligatures.

As you see in the release, there are six assets, 
1. Cascadia -> with ligatures
2. Cascadia Mono -> without ligatures
3. Cascadia Mono PL -> without ligatures but with patched with powerline.
4. Cascadia PL -> with ligatures patched with powerlines
5. Source Code in .zip format
6. Source Code in tar.gz format

I'm using no. 1.
![Alt text](https://monosnap.com/image/N9c7Is7WmQjVNsHwIokz4u5Bvb5cbf)
Install Cascadia Code, set it default in mintty, run this:

    mintty.exe -o LigaturesSupport=1 -e wslbridge2.exe

Works in my case.
Yes, it works. If you experience any special cases that don't, describe them.
If I use what @Biswa96 suggested I get an error:
![Alt text](https://monosnap.com/image/zcIIXKALWqCyVwDxOL2RST5S5Ia6HO)

I tried playing with the settings thru the shortcut:
`C:\Users\username\AppData\Local\wsltty\bin\mintty.exe -o Padding=200 -o LigaturesSupport=1 --WSL= --configdir="C:\Users\username\AppData\Roaming\wsltty" -~  -`

It somewhat works with that configuration but it's missing converting some ligatures.
 
For eg.:
`=>` and `->` doesn't work/transform.

![Alt text](https://monosnap.com/image/mtE3Swj9SqsZnvv4Htks0KCfzsXgP0)

*edit*: I just realized I'm using `wsltty`. But it does use `mintty` under the hood.



I tried using Fira Code, which doesn't work either. I've also tried: LigaturesSupport=2 (which I found in this PR https://github.com/mintty/mintty/commit/f35aa2aa72295f979dd90250c98fa7a4dc9e1c2e)

**Using Fira Code:**
![Alt text](https://monosnap.com/image/bpflJvRvVPICxqRfLwH5zaTFq9tS02)
OK, your report was it doesn't work. As you see in your line (foo == true), it does work.
Please check the discussion above for specific combinations that are not combined into a ligature.
I might add a summary to the documentation.
oh okay @mintty sorry about that, so basically `mintty` has partial support of ligatures? Looking forward to the summary thanks again for the great work. 🙏🏻
> I verified that "<-" is in fact handed over to Uniscribe for output in one bunch, even without that patch, actually.
> Maybe some parameters to the Uniscribe functions (wintext.c) would make a difference?

Sorry for the slow response, life got in the way.

I finally got it to work!  I played around with the Uniscribe parameters as you suggested, and the extra ligatures I wanted appeared when I passed in some different flags for the `SCRIPT_CONTROL` parameter in `ScriptStringAnalyse`:
```
  SCRIPT_CONTROL sctrl;
  ZeroMemory(&sctrl, sizeof(SCRIPT_CONTROL));
  sctrl.fMergeNeutralItems = 1;
  HRESULT hr = ScriptStringAnalyse(hdc, psz, cch, 0, -1,
    // could | SSA_FIT and use `width` (from win_text) instead of MAXLONG
    // to justify to monospace cell widths;
    // SSA_LINK is needed for Hangul and default-size CJK
    SSA_GLYPHS | SSA_FALLBACK | SSA_LINK, MAXLONG,
    &sctrl, NULL, dxs, NULL, NULL, &ssa);
```
Is this something you would want to incorporate, or should I maintain my own fork?
Add another bit setting to enable this in LigaturesSupport? 
Thanks for digging this out. No forks, please, I'll add that to the code. But it can only be an option, due to the ambiguity discussed before, see the marked case in the screenshot:
![grafik](https://user-images.githubusercontent.com/12740416/74602384-2729cc80-50a8-11ea-9263-b9dd51ddb76e.png)
Any idea how to tackle this (and other?) cases?
Should it be a distinct option or a bitmask variation as Brian suggested?
Thanks for adding it as an option, I didn't want to have to keep up with all your releases.

Regarding the ambiguity, I imagine the common case of, say, '<=' is less-than-or-equal-to, not double-arrow-pointing-left.  The only way I see around it is if you think it is worthwhile to write code to scan blocks, potentially break them up, and feed them to Uniscribe with different options.  I suggest starting out with everything on or off, and see if many people ask for fine-grained control.  FWIW, all the other programs I have tried go the less-than-or-equal-to glyph route.

For the option, you could hedge your bet about people wanting more control and have another option that is a bitmask.  One bit for 'all ligatures on', and in the future other bits could be 'double-arrow-pointing-left on'.
I wouldn't know how to persuade Uniscribe to render <= as left-arrow.

The current option is about interactive line rendering support (i.e. rendering while typing), which isn't yet perfect actually.
The new option would be about the range of ligatures to enable, so a bit different - maybe it should be a separate option `Ligatures` then.
Released 3.1.4.
Nice, is wsltty going to be updated? (Also you forgot to actually release 3.1.0.3.)
There is a bug with crash potential in the wslbridge gateway. Waiting for its fix.