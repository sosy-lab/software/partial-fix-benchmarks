Cursor color became BLACK after switching to tmux
If I just `cat` your log to the terminal, nothing happens to the cursor colour.
However, it does contain an OSC 112 sequence, which *resets* the cursor colour to its default.
So apparently your cursor colour before running whatever is not your default cursor colour.
I guess you're setting cursor colour with OSC 12, maybe in your shell profile.
Thanks for pointing out the possible reason of the problem. After some investigation, I found that it's not caused by some of my personal configurations, but rather a replicable problem. Here's how to replicate:

1. Make sure MS Traditional Chinese IME has been installed on a Windows 10 machine.
2. Download Cygwin x64 (currently 3.0.7).
3. Search for tmux to install (currently 2.6-1).
4. Run tmux and turn on the IME. The cursor's color would be altered. (Or perhaps, been "reset")

I also observed OSC 112 in another test environment ([log0943.log](https://github.com/mintty/mintty/files/3855635/log0943.log)), but I'm not sure whether the problem came from Cygwin or tmux. I am also wondering if there's a way to temporarily fix it.
Current cygwin version is 3.0.7.
I confirm that tmux resets the cursor colour to its configured default by sending OSC 112 to the terminal. It does not set the cursor colour with OSC 12 before, so that's kind of unmotivated and I consider it a bug. Please report to tmux.
Also, check out option SuppressOSC for a workaround.
My mistake, it's Cygwin 3.0.7 installed by an 2.897 installer. 
So it turns out that it's actually Microsoft IME that is buggy.....

![curs](https://user-images.githubusercontent.com/23246033/69010266-0793e280-0999-11ea-8df7-e6dab3f41a8c.gif)

Switching back to English mode somehow makes the cursor white again. Strange.
I'll attach some logs here. [log1](https://github.com/mintty/mintty/files/3855796/log1.log) [log2](https://github.com/mintty/mintty/files/3855797/log2.log)

~Since `SuppressOSC=yes` will crash the terminal when trying to run tmux~, I would suggest of running `echo -e "\e]12;white\a"` or changing to another IME provider.
Crashing on SuppressOSC=yes is certainly an embarrasing bug, thanks for the report. But see the manual, values are actually a list of numbers, for the OSC codes to suppress. SuppressOSC=112 would be your catch.
The additional logs do not help. Switching IME does not generate any trace in the screen log. However, IMECursorColour is another config item to look at; you hadn't said before that you're using an IME. So please configure another IMECursorColour and see what's changing. Also please answer my question above, whether any OSC 12 code is emitted in your profiles (.profile, .bash_profile, .bashrc, or .whatever-shellrc).
Sorry, I had set `SuppressOSC` to an incorrect value type.  After setting `SuppressOSC=112`, the cursor color no longer switch to black again.

Regarding to your former questions, I think I already answered them [here](https://github.com/mintty/mintty/issues/942#issuecomment-554749610), but I can restate them again. There are no OSC 12 generating code found in my profiles. Furthermore, I have tried installing a fresh new Cygwin on a different Windows computer, and they produced the same result. What they have in common is the default MS Traditional Chinese IME.

By setting `IMECursorColour=255,0,0`, the cursor color became _red_ when I turned IME on, and resumed to _white_ when I turned it off. Therefore, setting `IMECursorColour=255,255,255` is also a feasible solution aside from `SuppressOSC` to the MS IME bug. ([red.log](https://github.com/mintty/mintty/files/3950009/red2.log))
As stated before, this is a tmux bug, and I've opened an issue there.
Thanks for your assistance!
may  be change xterm-vt220
> may be change xterm-vt220

Please explain this comment.
The tmux fix should meanwhile be available in a release, so no workaround should be needed anymore (if tmux is up-to-date). Did you actually *observer* the suggested change to help for an older version of tmux?
add to .minttyrc
```
IMECursorColour=255,0,0
SuppressOSC=112
```

* ime off white
* ime on red


```
$ cat .minttyrc
Font=MyricaM M
FontHeight=12
Locale=ja_JP
Charset=UTF-8
# Start emacs without going through tmux.
# Term=xterm-256color
Term=xterm

IMECursorColour=255,0,0
SuppressOSC=112

$ tmux -V
tmux 2.6

$ cygcheck -c cygwin
Cygwin Package Information
Package              Version        Status
cygwin               3.1.5-1        OK

$ grep -P "256|color" ~/.tmux.conf
set-option -g default-terminal screen-256color
set -g terminal-overrides 'xterm:colors=256'
```

mistake .mintty
thanks

Current tmux release is 3.1 and once you've upgraded the workaround should no longer be necessary.
Also: Please check whether `Term=xterm-256color` is really needed for your workaround (I doubt it). If not, please remove it from the workaround in your comment above, for reference to others that may come across this issue.
please reopen this bug, it is not fixed

i use flat-ui theme, and IMECursorColour=255,0,0 fix the problem, i suppose neither CursorColour nor IMECursorColour is included in theme. Options->Looks->"Cursor..." configure for non-IME cursor only, should we need a "IME Cursor..." beside it
The original report was quite clearly identified to be a tmux issue, so this issue is closed.
If you think you have new insights, or maybe a different but related issue, please describe it reproducibly, with a minimal test case.
Did you actually check whether `SuppressOSC=112` is a workaround for you? And what's your tmux version?
Thanks for the conclusion of the thread
tmux is buggy but mintty too
tested both with tmux-2.6, tmux-3.1b
mintty/wsltty 3.3.0

with tmux-2.6 i'm not sure but i may have randomly get cursor stuck with one color (the IME one), switching on/off IME won't change its color. it seems happen after i exit/detach tmux IME on mode, it won't switch back to off. but after i do "apt upgrade" the problem seems gone, version however stay the same 2.6. maybe it get fixed on ubuntu? i don't know as i'm not sure if tmux is back-port patched by them

with both tmux-2.6, tmux-3.1b, with absent of IMECursorColour SuppressOSC setting:
i can reproduce the problem on mintty/wsltty 3.3.0

with both tmux-2.6, tmux-3.1b + IMECursorColour:
cursor change according to IME on/off perfectly (except maybe for tmux-2.6 issue mention above it stay at IMECursorColour after tmux exit)

with both tmux-2.6, tmux-3.1b + SuppressOSC=112:
cursor color stay regardless of IME on/off perfectly, not going black

with both tmux-2.6, tmux-3.1b + no IMECursorColour + no SuppressOSC:
turning on IME make cursor goes black, which i suppose IMECursorColour is black by default in mintty

this is why i think "missing UI setting for IMECursorColour" is a bug because it default to black.

BTW: auto switching color according to IME is a feature and a good one, please don't disable it. most vim+IME user would love to keep it. since both vim and IME are dual mode by itself, combining to 4 states make it worse, it's good to know the state by looking at cursor color. and yes i hide IME status floating bar completely
On OSC 112 "reset cursor colour", mintty would set undefined IME colour to an invalid value, effectively applying transparent (not black) cursor colour. Fixed.
Released 3.4.0.