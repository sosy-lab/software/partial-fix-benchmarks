Audio distortion when starting playback
The only way to address this easily today is to increase the prebuffer size (using the [`prefill` consumer property](https://www.mltframework.org/doxygen/properties.html)). That increases the number of frames rendered before starting playback. That increases the latency in the user experience. Do you want to wait a little for it start playing when you choose to start playing? If not someone else can try to add some code that will request the consumer to do some prebufferring in the background whenever seeking occurs. I will not be working on that.

I had looked into this in the past. If I am remembering correctly, I think there might be an opportunity for improvement.

In Shotcut, we [set the buffer and prefill to 25](https://github.com/mltframework/shotcut/blob/master/src/glwidget.cpp#L702).

In Shotcut, when we [pause](https://github.com/mltframework/shotcut/blob/master/src/mltcontroller.cpp#L255) or [seek](https://github.com/mltframework/shotcut/blob/master/src/mltcontroller.cpp#L446) we set the producer speed to 0. 

In MLT, when the producer speed is zero, the prepare_next() function[ does not advance the frame number](
https://github.com/mltframework/mlt/blob/master/src/framework/mlt_producer.c#L602).

This means, when you pause the player and seek to frame 0, the consumer buffer will prefill with 25 instances of frame 0. When you proceed to play, the consumer has to throw away 24 frames (all frame 0) and then wait for the producer to decode and send frame 1.

I think it would be a good improvement if the consumer buffer were prefilled with frames 0-24 when paused.
> In Shotcut, we [set the buffer and prefill to 25](https://github.com/mltframework/shotcut/blob/master/src/glwidget.cpp#L702).

Incorrect, prefill is set to fps/25 - a low value but one that increases with frame rate.

> Incorrect, prefill is set to fps/25 - a low value but one that increases with frame rate.

Thanks for the clarification. I see that now. As a test, I changed prefill to be the same as the buffer size and I do not see any improvement in the startup distortion.

I think my observation still stands - anything prefilled in the consumer buffer is just the same frame over and over, not consecutive frames. I think we could have a snappier startup if the consumer were prefilled with "x" number of consecutive frames.
> As a test, I changed prefill to be the same as the buffer size and I do not see any improvement in the startup distortion.

Maybe you changed the wrong line, for example, the line you referred to above was wrong unless you are using a DeckLink.


> Maybe you changed the wrong line, for example, the line you referred to above was wrong unless you are using a DeckLink.

Good eye. But I did set the correct line in my test.

The problem is that the SDL consumer never calls mlt_consumer_start(). As a result, [the preroll is never enabled](https://github.com/mltframework/mlt/blob/master/src/framework/mlt_consumer.c#L566). Without preroll enabled, the mlt_consumer_rt_frame() bypasses the prefill parameter and releases a frame [as soon as the queue is > 1](https://github.com/mltframework/mlt/blob/master/src/framework/mlt_consumer.c#L1533).

Here is a simple patch that infers a pause or seek and forces the preroll:

```
diff --git a/src/framework/mlt_consumer.c b/src/framework/mlt_consumer.c
index 9891e858..d39d0ae6 100644
--- a/src/framework/mlt_consumer.c
+++ b/src/framework/mlt_consumer.c
@@ -914,6 +914,8 @@ static void *consumer_read_ahead_thread( void *arg )
                }
 
                // Determine if we started, resumed, or seeked
+               if ( pos != last_pos + 1 && priv->speed )
+                       priv->preroll = 1;
                if ( pos != last_pos + 1 )
                        start_pos = pos;
                last_pos = pos;
@@ -1530,25 +1532,33 @@ mlt_frame mlt_consumer_rt_frame( mlt_consumer self )
        }
        else if ( priv->real_time == 1 || priv->real_time == -1 )
        {
+               int buffer = mlt_properties_get_int( properties, "buffer" );
+               int prefill = mlt_properties_get_int( properties, "prefill" );
+               int preroll_size = prefill > 0 && prefill < buffer ? prefill : buffer;
                int size = 1;
 
                if ( priv->preroll )
                {
-                       int buffer = mlt_properties_get_int( properties, "buffer" );
-                       int prefill = mlt_properties_get_int( properties, "prefill" );
 #ifndef _WIN32
                        consumer_read_ahead_start( self );
 #endif
                        if ( buffer > 1 && priv->speed )
-                               size = prefill > 0 && prefill < buffer ? prefill : buffer;
+                               size = preroll_size;
                        priv->preroll = 0;
                }
 
                // Get frame from queue
                pthread_mutex_lock( &priv->queue_mutex );
                mlt_log_timings_begin();
-               while( priv->ahead && mlt_deque_count( priv->queue ) < size )
+               while( priv->ahead && mlt_deque_count( priv->queue ) < size ) {
                        pthread_cond_wait( &priv->queue_cond, &priv->queue_mutex );
+                       if ( priv->preroll )
+                       {
+                               if ( buffer > 1 && priv->speed )
+                                       size = preroll_size;
+                               priv->preroll = 0;
+                       }
+               }
                frame = mlt_deque_pop_front( priv->queue );
                mlt_log_timings_end( NULL, "wait_for_frame_queue" );
                pthread_cond_broadcast( &priv->queue_cond );
```

In my test case, with a "prefill" of 8 and this patch above, I get a distortion free start with only a short delay after pressing the play button.

I do not like unnecessary user options. But I suppose we could add a player option to let the user choose the prefill.

A more complex option would be to have the consumer prefill consecutive frames while it is paused so that they are ready to go as soon as the user starts playback. I see now that the consumer logic is not designed to do that.


consumers are not supposed to call `mlt_consumer_start()`. The applications call that to start playback/processing. If you [search the code](https://github.com/mltframework/mlt/search?q=mlt_consumer_start) you see the only consumers that call this are special ones that encapsulate other consumers. So, your change might be an improvement, but I think it needs a better explanation. 
The application calls `mlt_consumer_start`, which sets `priv->preroll = 1` and tells the consumer to start, which typically starts its "consumer" thread that starts calling `mlt_consumer_rt_frame`. The first call to `mlt_consumer_rt_frame` starts the base mlt_consumer render ("read_ahead") thread and preroll.

Maybe what you are trying to say is that when paused and seeking, `priv->preroll` is not set? Or there is some condition where preroll is defeated when starting playback?

> with a "prefill" of 8

With what frame rate?
Thank you both for taking this up. 
  
Would it be possible to get audio pre-fill “preferred”? In detail: If an effect is enabled and playback is started the audio playback is as accurate as possible while video frames get dropped in favor of a clear audio playback.
  
Do you saw the other point I mentioned: click in timeline plays 1 frame of audio?

> Maybe what you are trying to say is that when paused and seeking, priv->preroll is not set? Or there is some condition where preroll is defeated when starting playback?

Yes. It is defeated by [this line](https://github.com/mltframework/mlt/blob/master/src/framework/mlt_consumer.c#L485). I guess we never call mlt_consumer_stop() in Shotcut when pausing or seeking.

>> with a "prefill" of 8

> With what frame rate?

The framerate was 30. But I do not think the prefill should be a function of the framerate. I expect the startup time would vary depending on the complexity of the file encoding and the maximum time to seek/decode a frame plus the time delay of filter processing which is project dependent. 

> Do you saw the other point I mentioned: click in timeline plays 1 frame of audio?

In Shotcut, disable Settings->Player->Scrub Audio

> In Shotcut, disable Settings->Player->Scrub Audio

Yes, when Scrub Audio is disabled -> click in the timeline plays now no 1 frame of audio
Thanks, Brian, this is working good in my testing and committed.
This change was reverted as it was causing deadlocks. This area of code is quite tricky. Using a prefill value of 8 still helps a lot though.
Brian, I had to revert the prefill=8 change in Shotcut for another deadlock. This one cannot be reproduced as described on the mailing list by trying to start playback quickly after seeking almost like triggering play while scrubbing. Rather, for this one, I have Video Mode set to Automatic (not sure if that is required), open a clip for playback in Source. Then, while it is still playing open a timeline project. Now, mlt_consumer is deadlocked in a problem similar to what I saw with the MLT patch. I suspect there is a race condition we are narrowly avoiding in the current situation (prefill <= 3, no MLT change). And if I can ever figure it out, then these changes might actually work.
 
I agree with reverting the changes. I assigned this one to myself and I aspire to return to it in the future.
Would it make sense that prefill is project dependent? Meaning a project with 50fps has more prefill then a project with 25fps.
Yes. But there might still be other factors like the file encoding and seeking complexity. Maybe an application would choose to make it a use option - but I do not think we would do that in Shotcut.
This is working for me now without deadlocks. It would be great for others to test it as well.
Yes, Brian, it is working good for me!