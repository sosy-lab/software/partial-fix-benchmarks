Encoding failure when using many scans (>60)
This seems to have been closed accidentally as the commit is unrelated. Anyway, I think this behavior is related to the hard-coded arrays of size 64 in https://github.com/mozilla/mozjpeg/blob/fd569212597dcc249752bd38ea58a4e2072da24f/jcmaster.h#L35-L37

@kornelski please reopen?