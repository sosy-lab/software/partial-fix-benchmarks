options: fix channel map options sometimes failing

Channel amp otpions were not copied correctly: it copied the size of a
pointer to struct chmap, not the struct itself. Since mp_chmap is
currently 9 bytes, this meant the last channel entry was not copied
correctly on 64 bit systems, leading to very strange failures. It could
be triggered especially when using the client API, because the client
API always copies options on access (mpv command line options tend to
work directly on options).
ao_coreaudio: initialize fetched properties to zeros

Should hopefully fix #1249 and #1279
coreaudio: reject descriptions with too many channels

This is a fix attempt for #1279 and #1249.
coreaudio: don't output too many channel descriptions

for #1279 and #1249
