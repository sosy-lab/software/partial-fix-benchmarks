--tid option gets crashed with event record
Here is the recorded data - [uftrace.data.issue-659.tar.gz](https://github.com/namhyung/uftrace/files/2779471/uftrace.data.issue-659.tar.gz)


The segfault doesn't happen if `--no-event` is given together during record.
```c
1707│
1708│                 /* calculate duration from now on */
1709│                 for (i = 0; i < task->stack_count; i++) {
1710│                         fstack = &task->func_stack[i];
1711│
1712├───────────────────────> fstack->total_time = rstack->time;  /* start time */
1713│                         fstack->child_time = 0;
1714│                         fstack->valid = true;
1715│                 }
1716│
```
```
(gdb) r
Starting program: /home/honggyu/usr/bin/uftrace replay --tid 178331 --no-pager
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/lib/x86_64-linux-gnu/libthread_db.so.1".
# DURATION     TID     FUNCTION
            [178331] | main() {
  29.034 us [178331] |   /* linux:schedule */
            [178331] |   clang::driver::Driver::ExecuteCompilation() {
            [178331] |     clang::driver::Compilation::ExecuteJobs() {
            [178331] |       clang::driver::Compilation::ExecuteCommand() {
            [178331] |         clang::driver::Command::Execute() {
            [178331] |           llvm::sys::ExecuteAndWait() {
            [178331] |             Execute() {
            [178331] |               posix_spawn() {

Program received signal SIGSEGV, Segmentation fault.
0x0000000000441af8 in fstack_account_time (task=0x186d958) at /home/honggyu/work/uftrace/git/uftrace/utils/fstack.c:1712

(gdb) bt
#0  0x0000000000441af8 in fstack_account_time (task=0x186d958) at /home/honggyu/work/uftrace/git/uftrace/utils/fstack.c:1712
#1  0x000000000044263a in __fstack_consume (task=0x186d958, kernel=0x0, cpu=0) at /home/honggyu/work/uftrace/git/uftrace/utils/fstack.c:1951
#2  0x00000000004426be in fstack_consume (handle=0x7fffffffdec0, task=0x186d958) at /home/honggyu/work/uftrace/git/uftrace/utils/fstack.c:1973
#3  0x000000000043f522 in fstack_skip (handle=0x7fffffffdec0, task=0x186d830, curr_depth=0, opts=0x7fffffffe100) at /home/honggyu/work/uftrace/git/uftrace/utils/fstack.c:782
#4  0x000000000041fd23 in print_graph_rstack (handle=0x7fffffffdec0, task=0x186d830, opts=0x7fffffffe100) at /home/honggyu/work/uftrace/git/uftrace/cmds/replay.c:958
#5  0x0000000000420591 in command_replay (argc=0, argv=0x7fffffffe370, opts=0x7fffffffe100) at /home/honggyu/work/uftrace/git/uftrace/cmds/replay.c:1139
#6  0x000000000040759b in main (argc=0, argv=0x7fffffffe370) at /home/honggyu/work/uftrace/git/uftrace/uftrace.c:1067

(gdb) p fstack
$1 = (struct fstack *) 0x0
```
Could you please test [check/event-fix](https://github.com/namhyung/uftrace/compare/check/event-fix)?

```diff
diff --git a/utils/perf.c b/utils/perf.c
index 14fa9b32..cbbdbc05 100644
--- a/utils/perf.c
+++ b/utils/perf.c
@@ -298,6 +298,7 @@ static int read_perf_event(struct uftrace_data *handle,
                           struct uftrace_perf_reader *perf)
 {
        struct perf_event_header h;
+       struct uftrace_task_reader *task;
        union {
                struct perf_context_switch_event cs;
                struct perf_task_event t;
@@ -393,7 +394,8 @@ static int read_perf_event(struct uftrace_data *handle,
                goto again;
        }
 
-       if (unlikely(find_task(&handle->sessions, perf->tid) == NULL))
+       task = get_task_handle(handle, perf->tid);
+       if (unlikely(task == NULL || task->fp == NULL))
                goto again;
 
        perf->type = h.type;
@@ -561,6 +563,9 @@ void process_perf_event(struct uftrace_data *handle)
                rec = get_perf_record(handle, perf);
                task = get_task_handle(handle, perf->tid);
 
+               if (unlikely(task == NULL || task->fp == NULL))
+                       continue;
+
                if (perf->type == PERF_RECORD_COMM) {
                        rec->more = 1;
                        args.args = NULL;
```
It works fine now. Thanks!
Hmm.. 9200e7c makes `update_perf_task_comm()` not to properly update task comm name.
So the output of `uftrace info` doesn't show updated comm name.
By the way, would it be better to create another issue rather than reopening this again?
If it doesn't crash anymore, yes.
Created another issue at #662.