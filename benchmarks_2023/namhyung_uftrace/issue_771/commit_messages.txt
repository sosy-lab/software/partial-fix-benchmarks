Merge branch 'size-filter'

Add -Z/--size-filter option to disable dynamic tracing for functions
smaller than the given SIZE (in bytes).  This is an effort to reduce
overhead of tracing by skipping (probably) not important functions.

Signed-off-by: Namhyung Kim <namhyung@gmail.com>
record: Disable linux:schedule event for caller filter

The caller filter is to focus tracing onto given functions.  So
disable schedule events to remove noise in the output.

Fixed: #771

Signed-off-by: Namhyung Kim <namhyung@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands when caller filter is used.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands when caller filter is used.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands when caller filter is used.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands when caller filter is used.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands when caller filter is used.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
uftrace: Apply --no-sched when caller filter is used

There was no way to disable schedule event for analysis commands but
--no-sched can do this now.  Let's simply enable it for both record and
other analysis commands when caller filter is used.

Fixed: #771, #1173

Signed-off-by: Honggyu Kim <honggyu.kp@gmail.com>
