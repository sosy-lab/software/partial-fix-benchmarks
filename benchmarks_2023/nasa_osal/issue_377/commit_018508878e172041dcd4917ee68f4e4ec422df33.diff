diff --git a/src/unit-tests/oscore-test/CMakeLists.txt b/src/unit-tests/oscore-test/CMakeLists.txt
index 0ed5ba38b..eea988489 100644
--- a/src/unit-tests/oscore-test/CMakeLists.txt
+++ b/src/unit-tests/oscore-test/CMakeLists.txt
@@ -4,6 +4,7 @@ set(TEST_MODULE_FILES
   ut_oscore_binsem_test.c    
   ut_oscore_misc_test.c   
   ut_oscore_queue_test.c  
+  ut_oscore_select_test.c  
   ut_oscore_countsem_test.c  
   ut_oscore_mutex_test.c  
   ut_oscore_task_test.c   
diff --git a/src/unit-tests/oscore-test/ut_oscore_select_test.c b/src/unit-tests/oscore-test/ut_oscore_select_test.c
new file mode 100644
index 000000000..fe7fe56ff
--- /dev/null
+++ b/src/unit-tests/oscore-test/ut_oscore_select_test.c
@@ -0,0 +1,149 @@
+/*================================================================================*
+** File:  ut_oscore_select_test.c
+** Owner: Chris Knight
+** Date:  March 2020
+**================================================================================*/
+
+/*--------------------------------------------------------------------------------*
+** Includes
+**--------------------------------------------------------------------------------*/
+
+#include "ut_oscore_select_test.h"
+
+/*--------------------------------------------------------------------------------*
+** Macros
+**--------------------------------------------------------------------------------*/
+
+#define UT_SELECT_FN "/cf/select_test.tmp"
+
+/*--------------------------------------------------------------------------------*
+** Data types
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** External global variables
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** Global variables
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** External function prototypes
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** Local function prototypes
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** Local function definitions
+**--------------------------------------------------------------------------------*/
+
+char *fsAddrPtr = NULL;
+static int32 setup_file(void)
+{
+    OS_mkfs(fsAddrPtr, "/ramdev3", " ", 512, 20);
+    OS_mount("/ramdev3", "/drive3");
+    return OS_creat("/drive3/select_test.txt", OS_READ_WRITE);
+}
+
+static void teardown_file(int32 fd)
+{
+    OS_close(fd);
+    OS_remove("/drive3/select_test.txt");
+    OS_unmount("/drive3");
+    OS_rmfs("/ramdev3");
+}
+
+/*--------------------------------------------------------------------------------*
+** Syntax: OS_SelectFdZero, OS_SelectFdAdd, OS_SelectFdClear, OS_SelectFdIsSet
+** Purpose: Configure file descriptor set for select
+** Parameters: To-be-filled-in
+** Returns: OS_INVALID_POINTER if the pointer passed in is null
+**          OS_SUCCESS if succeeded
+**--------------------------------------------------------------------------------*/
+void UT_os_select_fd_test(void)
+{
+    OS_FdSet FdSet;
+    int32 fd = setup_file();
+
+    UtAssert_Simple(OS_SelectFdZero(&FdSet) != OS_ERR_NOT_IMPLEMENTED);
+    UtAssert_Simple(OS_SelectFdAdd(&FdSet, 0) != OS_ERR_NOT_IMPLEMENTED);
+    UtAssert_Simple(OS_SelectFdAdd(&FdSet, 0) != OS_ERR_NOT_IMPLEMENTED);
+
+    UtAssert_Simple(OS_SelectFdZero(NULL) == OS_INVALID_POINTER);
+    UtAssert_Simple(OS_SelectFdAdd(NULL, 0) == OS_INVALID_POINTER);
+    UtAssert_Simple(OS_SelectFdClear(NULL, 0) == OS_INVALID_POINTER);
+    UtAssert_Simple(OS_SelectFdIsSet(NULL, 0) == false);
+
+    OS_SelectFdZero(&FdSet);
+    OS_SelectFdAdd(&FdSet, fd);
+    UtAssert_Simple(OS_SelectFdIsSet(&FdSet, fd));
+
+    OS_SelectFdZero(&FdSet);
+    OS_SelectFdAdd(&FdSet, fd);
+    OS_SelectFdClear(&FdSet, fd);
+    UtAssert_Simple(!OS_SelectFdIsSet(&FdSet, fd));
+
+    teardown_file(fd);
+}
+
+/*--------------------------------------------------------------------------------*
+** Syntax: int32 OS_SelectSingle(uint32 objid, uint32 *StateFlags, int32 msecs);
+** Purpose: Select on a single file descriptor
+** Parameters: To-be-filled-in
+** Returns: OS_INVALID_POINTER if the pointer passed in is null
+**          OS_SUCCESS if succeeded
+**--------------------------------------------------------------------------------*/
+void UT_os_select_single_test(void)
+{
+    uint32 StateFlags;
+    int32 fd = setup_file();
+
+    UtAssert_Simple(OS_SelectSingle(fd, &StateFlags, 0) != OS_ERR_NOT_IMPLEMENTED);
+
+    UtAssert_Simple(OS_SelectSingle(fd, NULL, 0) != OS_SUCCESS);
+
+    StateFlags = OS_STREAM_STATE_WRITABLE;
+    UtAssert_Simple(OS_SelectSingle(fd, &StateFlags, 0) == OS_SUCCESS && StateFlags & OS_STREAM_STATE_WRITABLE);
+
+    StateFlags = OS_STREAM_STATE_READABLE;
+    UtAssert_Simple(OS_SelectSingle(fd, &StateFlags, 1) == OS_SUCCESS);
+
+    teardown_file(fd);
+}
+
+/*--------------------------------------------------------------------------------*
+** Syntax: int32 OS_SelectMultiple(OS_FdSet *ReadSet, OS_FdSet *WriteSet, int32 msecs)
+** Purpose: Select on a multiple file descriptors
+** Parameters: To-be-filled-in
+** Returns: OS_INVALID_POINTER if the pointer passed in is null
+**          OS_SUCCESS if succeeded
+**--------------------------------------------------------------------------------*/
+void UT_os_select_multi_test(void)
+{
+    OS_FdSet ReadSet, WriteSet;
+    int32 fd = setup_file();
+
+    UtAssert_Simple(OS_SelectMultiple(&ReadSet, &WriteSet, 1) != OS_ERR_NOT_IMPLEMENTED);
+
+    OS_SelectFdZero(&WriteSet);
+    OS_SelectFdAdd(&WriteSet, fd);
+    UtAssert_Simple(OS_SelectMultiple(NULL, &WriteSet, 1) == OS_SUCCESS);
+
+    OS_SelectFdZero(&ReadSet);
+    OS_SelectFdAdd(&ReadSet, fd);
+    UtAssert_Simple(OS_SelectMultiple(&ReadSet, NULL, 1) == OS_SUCCESS);
+
+    OS_SelectFdZero(&ReadSet);
+    OS_SelectFdAdd(&ReadSet, fd);
+    OS_SelectFdZero(&WriteSet);
+    UtAssert_Simple(OS_SelectMultiple(&ReadSet, &WriteSet, 0) == OS_SUCCESS);
+
+    teardown_file(fd);
+}
+
+/*================================================================================*
+** End of File: ut_oscore_queue_test.c
+**================================================================================*/
diff --git a/src/unit-tests/oscore-test/ut_oscore_select_test.h b/src/unit-tests/oscore-test/ut_oscore_select_test.h
new file mode 100644
index 000000000..0b41a3162
--- /dev/null
+++ b/src/unit-tests/oscore-test/ut_oscore_select_test.h
@@ -0,0 +1,46 @@
+/*================================================================================*
+** File:  ut_oscore_select_test.h
+** Owner: Chris Knight
+** Date:  March 2020
+**================================================================================*/
+
+#ifndef _UT_OSCORE_SELECT_TEST_H_
+#define _UT_OSCORE_SELECT_TEST_H_
+
+/*--------------------------------------------------------------------------------*
+** Includes
+**--------------------------------------------------------------------------------*/
+
+#include "ut_os_support.h"
+
+/*--------------------------------------------------------------------------------*
+** Macros
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** Data types
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** External global variables
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** Global variables
+**--------------------------------------------------------------------------------*/
+
+/*--------------------------------------------------------------------------------*
+** Function prototypes
+**--------------------------------------------------------------------------------*/
+
+void UT_os_select_fd_test(void);
+void UT_os_select_single_test(void);
+void UT_os_select_multi_test(void);
+
+/*--------------------------------------------------------------------------------*/
+
+#endif  /* _UT_OSCORE_SELECT_TEST_H_ */
+
+/*================================================================================*
+** End of File: ut_oscore_select_test.h
+**================================================================================*/
diff --git a/src/unit-tests/oscore-test/ut_oscore_test.c b/src/unit-tests/oscore-test/ut_oscore_test.c
index 6b7b75ae7..dd912aad7 100644
--- a/src/unit-tests/oscore-test/ut_oscore_test.c
+++ b/src/unit-tests/oscore-test/ut_oscore_test.c
@@ -205,6 +205,10 @@ void UtTest_Setup(void)
     UtTest_Add(UT_os_queue_get_id_by_name_test, NULL, NULL, "OS_QueueGetIdByName");
     UtTest_Add(UT_os_queue_get_info_test, NULL, NULL, "OS_QueueGetInfo");
 
+    UtTest_Add(UT_os_select_fd_test, NULL, NULL, "OS_SelectFd");
+    UtTest_Add(UT_os_select_single_test, NULL, NULL, "OS_SelectSingle");
+    UtTest_Add(UT_os_select_multi_test, NULL, NULL, "OS_SelectMultiple");
+
     UtTest_Add(
             NULL,
             UT_os_init_task_misc,
diff --git a/src/unit-tests/oscore-test/ut_oscore_test.h b/src/unit-tests/oscore-test/ut_oscore_test.h
index 14d790ca2..f7a2598f3 100644
--- a/src/unit-tests/oscore-test/ut_oscore_test.h
+++ b/src/unit-tests/oscore-test/ut_oscore_test.h
@@ -17,6 +17,7 @@
 #include "ut_oscore_countsem_test.h"
 #include "ut_oscore_mutex_test.h"
 #include "ut_oscore_queue_test.h"
+#include "ut_oscore_select_test.h"
 #include "ut_oscore_task_test.h"
 #include "ut_oscore_interrupt_test.h"
 #include "ut_oscore_exception_test.h"
