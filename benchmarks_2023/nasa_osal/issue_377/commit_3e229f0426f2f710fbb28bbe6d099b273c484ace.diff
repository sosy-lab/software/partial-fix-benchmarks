diff --git a/src/tests/select-test/select-test.c b/src/tests/select-test/select-test.c
new file mode 100644
index 000000000..62db4a067
--- /dev/null
+++ b/src/tests/select-test/select-test.c
@@ -0,0 +1,377 @@
+/*
+ *  NASA Docket No. GSC-18,370-1, and identified as "Operating System Abstraction Layer"
+ *
+ *  Copyright (c) 2019 United States Government as represented by
+ *  the Administrator of the National Aeronautics and Space Administration.
+ *  All Rights Reserved.
+ *
+ *  Licensed under the Apache License, Version 2.0 (the "License");
+ *  you may not use this file except in compliance with the License.
+ *  You may obtain a copy of the License at
+ *
+ *    http://www.apache.org/licenses/LICENSE-2.0
+ *
+ *  Unless required by applicable law or agreed to in writing, software
+ *  distributed under the License is distributed on an "AS IS" BASIS,
+ *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ *  See the License for the specific language governing permissions and
+ *  limitations under the License.
+ */
+
+/*
+ * Filename: select-test.c
+ *
+ * Purpose: This file contains functional tests for "osapi-select"
+ *
+ */
+
+#include <stdio.h>
+#include <string.h>
+#include <stdlib.h>
+#include "common_types.h"
+#include "osapi.h"
+#include "utassert.h"
+#include "uttest.h"
+#include "utbsp.h"
+
+osal_id_t     s_task_id;
+osal_id_t     s2_task_id;
+osal_id_t     s3_task_id;
+osal_id_t     s_socket_id;
+osal_id_t     s2_socket_id;
+osal_id_t     s3_socket_id;
+osal_id_t     c_socket_id;
+osal_id_t     c2_socket_id;
+osal_id_t     c3_socket_id;
+OS_SockAddr_t s_addr;
+OS_SockAddr_t s2_addr;
+OS_SockAddr_t s3_addr;
+OS_SockAddr_t c_addr;
+OS_SockAddr_t c2_addr;
+OS_SockAddr_t c3_addr;
+
+/* *************************************** MAIN ************************************** */
+
+char *           fsAddrPtr = NULL;
+static osal_id_t setup_file(void)
+{
+    osal_id_t id;
+    OS_mkfs(fsAddrPtr, "/ramdev3", "RAM3", 512, 20);
+    OS_mount("/ramdev3", "/drive3");
+    OS_OpenCreate(&id, "/drive3/select_test.txt", OS_FILE_FLAG_CREATE | OS_FILE_FLAG_TRUNCATE, OS_READ_ONLY);
+    return id;
+}
+
+void Setup_Server(void)
+{
+    int32 expected;
+    int32 actual;
+
+    /*
+     * Set up a server
+     */
+
+    /* Open a server socket */
+    s_socket_id = OS_OBJECT_ID_UNDEFINED;
+    expected    = OS_SUCCESS;
+    actual      = OS_SocketOpen(&s_socket_id, OS_SocketDomain_INET, OS_SocketType_STREAM);
+    UtAssert_True(actual == expected, "OS_SocketOpen() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(OS_ObjectIdDefined(s_socket_id), "s_socket_id (%lu) != 0", OS_ObjectIdToInteger(s_socket_id));
+
+    /* Initialize server address */
+    actual = OS_SocketAddrInit(&s_addr, OS_SocketDomain_INET);
+    UtAssert_True(actual == expected, "OS_SocketAddrInit() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set server port */
+    actual = OS_SocketAddrSetPort(&s_addr, 9997);
+    UtAssert_True(actual == expected, "OS_SocketAddrSetPort() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set server address */
+    actual = OS_SocketAddrFromString(&s_addr, "127.0.0.1");
+    UtAssert_True(actual == expected, "OS_SocketAddrFromString() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Bind server socket to server address */
+    actual = OS_SocketBind(s_socket_id, &s_addr);
+    UtAssert_True(actual == expected, "OS_SocketBind() (%ld) == OS_SUCCESS", (long)actual);
+}
+
+void Setup_Client(void)
+{
+    int32 expected;
+    int32 actual;
+
+    /*
+     * Set up a client
+     */
+
+    /* Open a client socket */
+    expected    = OS_SUCCESS;
+    c_socket_id = OS_OBJECT_ID_UNDEFINED;
+
+    actual = OS_SocketOpen(&c_socket_id, OS_SocketDomain_INET, OS_SocketType_STREAM);
+    UtAssert_True(actual == expected, "OS_SocketOpen() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(OS_ObjectIdDefined(c_socket_id), "c_socket_id (%lu) != 0", OS_ObjectIdToInteger(c_socket_id));
+
+    /* Initialize client address */
+    actual = OS_SocketAddrInit(&c_addr, OS_SocketDomain_INET);
+    UtAssert_True(actual == expected, "OS_SocketAddrInit() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set client port */
+    actual = OS_SocketAddrSetPort(&c_addr, 9996);
+    UtAssert_True(actual == expected, "OS_SocketAddrSetPort() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set client address */
+    actual = OS_SocketAddrFromString(&c_addr, "127.0.0.1");
+    UtAssert_True(actual == expected, "OS_SocketAddrFromString() (%ld) == OS_SUCCESS", (long)actual);
+}
+
+void Server_Fn(void)
+{
+    osal_id_t     connsock_id = OS_OBJECT_ID_UNDEFINED;   
+    OS_SockAddr_t addr;  
+
+    /* Accept incoming connections */
+    OS_SocketAccept(s_socket_id, &connsock_id, &addr, OS_PEND);  
+
+    OS_TaskDelay(200);
+
+    OS_close(s_socket_id);
+    OS_close(connsock_id);
+
+} /* end Server_Fn */
+
+void TestSelectSingleRead(void)
+{
+    /*
+     * Test Case For:
+     * int32 OS_SelectSingle(uint32 objid, uint32 *StateFlags, int32 msecs);
+     */
+    int32  expected;
+    int32  actual;    
+
+    Setup_Server();
+    Setup_Client();
+
+    expected = OS_SUCCESS;
+
+    /*
+     * Create a server thread, and connect client from
+     * this thread to server thread and verify connection
+     */
+
+    /* Create a server task/thread */
+    int32 status = OS_TaskCreate(&s_task_id, "Server", Server_Fn, OSAL_TASK_STACK_ALLOCATE, OSAL_SIZE_C(16384),
+                           OSAL_PRIORITY_C(50), 0);
+    UtAssert_True(status == OS_SUCCESS, "OS_TaskCreate() (%ld) == OS_SUCCESS", (long)status);
+
+    /* Connect to a server */
+    actual = OS_SocketConnect(c_socket_id, &s_addr, 10);
+    UtAssert_True(actual == expected, "OS_SocketConnect() (%ld) == OS_SUCCESS", (long)actual);   
+
+    uint32 StateFlags;
+    expected = OS_ERROR_TIMEOUT;
+    StateFlags = OS_STREAM_STATE_READABLE;
+    actual = OS_SelectSingle(c_socket_id, &StateFlags, 100);
+
+    /* Verify Outputs */
+    UtAssert_True(actual == expected, "OS_SelectSingle() (%ld) == OS_ERROR_TIMEOUT", (long)actual);
+    UtAssert_True(StateFlags == 0, "OS_SelectSingle() (%d) == None", StateFlags);
+
+
+    expected = OS_SUCCESS;
+    StateFlags = OS_STREAM_STATE_READABLE;
+    actual = OS_SelectSingle(c_socket_id, &StateFlags, 300);
+
+    /* Verify Outputs */
+    UtAssert_True(actual == expected, "OS_SelectSingle() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(StateFlags == OS_STREAM_STATE_READABLE, "OS_SelectSingle() (%d) == OS_STREAM_STATE_READABLE", StateFlags);  
+    
+}
+
+void Setup_Server2(void)
+{
+    int32 expected;
+    int32 actual;
+
+    /*
+     * Set up a server
+     */
+
+    /* Open a server socket */
+    s2_socket_id = OS_OBJECT_ID_UNDEFINED;
+    expected    = OS_SUCCESS;
+    actual      = OS_SocketOpen(&s2_socket_id, OS_SocketDomain_INET, OS_SocketType_STREAM);
+    UtAssert_True(actual == expected, "OS_SocketOpen() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(OS_ObjectIdDefined(s2_socket_id), "s2_socket_id (%lu) != 0", OS_ObjectIdToInteger(s2_socket_id));
+
+    /* Initialize server address */
+    actual = OS_SocketAddrInit(&s2_addr, OS_SocketDomain_INET);
+    UtAssert_True(actual == expected, "OS_SocketAddrInit() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set server port */
+    actual = OS_SocketAddrSetPort(&s2_addr, 9998);
+    UtAssert_True(actual == expected, "OS_SocketAddrSetPort() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set server address */
+    actual = OS_SocketAddrFromString(&s2_addr, "127.0.0.1");
+    UtAssert_True(actual == expected, "OS_SocketAddrFromString() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Bind server socket to server address */
+    actual = OS_SocketBind(s2_socket_id, &s2_addr);
+    UtAssert_True(actual == expected, "OS_SocketBind() (%ld) == OS_SUCCESS", (long)actual);
+}
+
+void Setup_Client2(void)
+{
+    int32 expected;
+    int32 actual;
+
+    /*
+     * Set up a client
+     */
+
+    /* Open a client socket */
+    expected    = OS_SUCCESS;
+    c2_socket_id = OS_OBJECT_ID_UNDEFINED;
+
+    actual = OS_SocketOpen(&c2_socket_id, OS_SocketDomain_INET, OS_SocketType_STREAM);
+    UtAssert_True(actual == expected, "OS_SocketOpen() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(OS_ObjectIdDefined(c2_socket_id), "c2_socket_id (%lu) != 0", OS_ObjectIdToInteger(c2_socket_id));
+
+    /* Initialize client address */
+    actual = OS_SocketAddrInit(&c2_addr, OS_SocketDomain_INET);
+    UtAssert_True(actual == expected, "OS_SocketAddrInit() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set client port */
+    actual = OS_SocketAddrSetPort(&c2_addr, 9995);
+    UtAssert_True(actual == expected, "OS_SocketAddrSetPort() (%ld) == OS_SUCCESS", (long)actual);
+
+    /* Set client address */
+    actual = OS_SocketAddrFromString(&c2_addr, "127.0.0.1");
+    UtAssert_True(actual == expected, "OS_SocketAddrFromString() (%ld) == OS_SUCCESS", (long)actual);
+}
+
+void Server_Fn2(void)
+{
+    osal_id_t     connsock_id = OS_OBJECT_ID_UNDEFINED;
+   
+    OS_SockAddr_t addr;  
+
+    /* Accept incoming connections */
+    OS_SocketAccept(s2_socket_id, &connsock_id, &addr, OS_PEND);  
+
+    OS_TaskDelay(1000);
+
+    OS_close(s2_socket_id);
+    OS_close(connsock_id);
+
+} /* end Server_Fn */
+
+void TestSelectMultipleRead(void)
+{
+    /*
+     * Test Case For:
+     * int32 OS_SelectMultiple(OS_FdSet *ReadSet, OS_FdSet *WriteSet, int32 msecs);
+     */
+    OS_FdSet ReadSet;
+    OS_FdSet WriteSet;
+    int32    expected = OS_SUCCESS;
+    int32    actual;
+    int32    status;
+
+    OS_SelectFdZero(&ReadSet);
+    OS_SelectFdZero(&WriteSet);
+
+    Setup_Server();
+    Setup_Client();
+    Setup_Server2();
+    Setup_Client2();
+
+    expected = OS_SUCCESS;
+
+    /*
+     * Create a server thread, and connect client from
+     * this thread to server thread and verify connection
+     */
+
+    /* Create a server task/thread */
+    status = OS_TaskCreate(&s_task_id, "Server", Server_Fn, OSAL_TASK_STACK_ALLOCATE, OSAL_SIZE_C(16384),
+                           OSAL_PRIORITY_C(50), 0);
+    UtAssert_True(status == OS_SUCCESS, "OS_TaskCreate() (%ld) == OS_SUCCESS", (long)status);
+
+    /* Connect to a server */
+    actual = OS_SocketConnect(c_socket_id, &s_addr, 10);
+    UtAssert_True(actual == expected, "OS_SocketConnect() (%ld) == OS_SUCCESS", (long)actual);
+
+    status = OS_TaskCreate(&s2_task_id, "Server2", Server_Fn2, OSAL_TASK_STACK_ALLOCATE, OSAL_SIZE_C(16384),
+                           OSAL_PRIORITY_C(50), 0);
+    UtAssert_True(status == OS_SUCCESS, "OS_TaskCreate() (%ld) == OS_SUCCESS", (long)status);
+
+    /* Connect to a server */
+    actual = OS_SocketConnect(c2_socket_id, &s2_addr, 10);
+    UtAssert_True(actual == expected, "OS_SocketConnect() (%ld) == OS_SUCCESS", (long)actual);
+
+    OS_SelectFdAdd(&ReadSet, c_socket_id);
+    OS_SelectFdAdd(&ReadSet, c2_socket_id);
+
+    actual = OS_SelectFdIsSet(&ReadSet, c_socket_id);
+    UtAssert_True(OS_SelectFdIsSet(&ReadSet, c_socket_id), "OS_SelectFdIsSet(1) == true");
+    UtAssert_True(OS_SelectFdIsSet(&ReadSet, c2_socket_id), "OS_SelectFdIsSet(1) == true");
+
+    actual = OS_SelectMultiple(&ReadSet, &WriteSet, 500);
+    /* Verify Outputs */
+    UtAssert_True(actual == expected, "OS_SelectMultiple() (%ld) == OS_SUCCESS", (long)actual);
+
+    UtAssert_True(OS_SelectFdIsSet(&ReadSet, c_socket_id), "OS_SelectFdIsSet(1) == true");
+    UtAssert_True(!OS_SelectFdIsSet(&ReadSet, c2_socket_id), "OS_SelectFdIsSet(2) == false");
+}
+
+void TestSingleSelectFile(void)
+{
+    int32    expected = OS_SUCCESS;
+    int32    actual;
+    uint32 StateFlags;
+    osal_id_t fd = setup_file();
+    
+    expected = OS_SUCCESS;
+    StateFlags = OS_STREAM_STATE_READABLE;
+    actual = OS_SelectSingle(fd, &StateFlags, 100);
+
+    /* Verify Outputs */
+    UtAssert_True(actual == expected, "OS_SelectSingle() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(StateFlags == OS_STREAM_STATE_READABLE, "OS_SelectSingle() (%d) == OS_STREAM_STATE_READABLE", StateFlags);
+
+
+    StateFlags = OS_STREAM_STATE_WRITABLE;
+    actual = OS_SelectSingle(fd, &StateFlags, 100);
+
+    /* Verify Outputs */
+    UtAssert_True(actual == expected, "OS_SelectSingle() (%ld) == OS_SUCCESS", (long)actual);
+    UtAssert_True(StateFlags == OS_STREAM_STATE_WRITABLE, "OS_SelectSingle() (%d) == OS_STREAM_STATE_WRITABLE", StateFlags);
+
+   
+    expected = OS_ERROR_TIMEOUT;
+    StateFlags = OS_STREAM_STATE_BOUND;
+    actual = OS_SelectSingle(fd, &StateFlags, 100);
+
+    /* Verify Outputs */
+    UtAssert_True(actual == expected, "OS_SelectSingle() (%ld) == OS_ERROR_TIMEOUT", (long)actual);
+    UtAssert_True(StateFlags == 0, "OS_SelectSingle() (%d) == None", StateFlags);
+}
+
+void UtTest_Setup(void)
+{
+    if (OS_API_Init() != OS_SUCCESS)
+    {
+        UtAssert_Abort("OS_API_Init() failed");
+    }
+
+    /*
+     * Register the test setup and check routines in UT assert
+     */
+
+    UtTest_Add(TestSelectSingleRead, NULL, NULL, "TestSelectSingleRead");
+    UtTest_Add(TestSelectMultipleRead, NULL, NULL, "TestSelectMultipleRead");
+    UtTest_Add(TestSingleSelectFile, NULL, NULL, "TestSingleSelectFile");
+
+}
\ No newline at end of file
