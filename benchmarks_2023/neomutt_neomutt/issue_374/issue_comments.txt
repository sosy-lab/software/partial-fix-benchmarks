Lists, queues, and the like
The one large issue with using either the FreeBSD or Linux versions of queues is portability. We can't expect these headers to exist on all the possible systems. And maintaining our own copy of these is a bad idea.

In general, I am not fond of using a macro based approach for anything. Modern compilers are smart enough to inline code when needed. If we can find a good library that provides these, something we can statically / dynamically link to, sure, it makes sense to look into it.
mutt has this approach of having portability always in mind, without having external deps, meaning to reimplement stuff like that.

Here we can have two approaches: either we reimplement our own (current approach), or deal with all the existing versions (your suggestion). If we go with platform approaches, we need to check the availability to all our best effort targets (linux, *BSD, darwin, solaris), and offer conditional inclusion of the matching headers.

Otherwise, we can extract that stuff out of `muttlib.c` into a list+queues compilation unit, and write tests for those. That kind of structures and algorithms are the easy pieces to unit test.
I would definitely *not* reimplement anything. I'll just import queue.h from BSD or Linux.

> And maintaining our own copy of these is a bad idea.

Why? Those have been in use for several (twenty?) years, they are definitely stable.
Here, "maintaining" would really not be much more than "check perhaps once a year if there have been big improvement upstream"

As for portability, please note that those headers are 100% self-contained. (Yes, BSD imports `sys/cdefs.h`, but please don't bring it up as an argument :)
As for the "macros are bad" argument, yeah I agree in most cases, but you can't do composition in C (object orientation, if you like) without them *or* without doing everything by hand.

Look at GObject..
I'm ok with either solution, as long as it's fully covering our use cases: traditional single link lists and single linked heap, with all the list/heap methods implemented on them. (I did not check the full implementation of the linux/bsd ones).

About object composition, AFAICT, we only care about a string datatype in mutt lists, so it's not a first class reason to switch.
By object composition here I mean the composition of a list data structure (`next`, possibly `prev` links) and the operations to work on them with our data structures (`HEADER`, ...)
[aside]

> you can't do composition in C

C11's anonymous structs allow you to compose (no we're not moving to C11 :-)
@guyzmo, I've never heard of such a think as "single linked heap". I know what a singly linked list is, and what a heap is. The latter is a tree in which certain conditions are met.
From this typedef struct list_t HEAP; I get that you mean nothing different than a list where you insert and remove at the head. Is that it, or am I missing something?
yes, it's exactly that: all I mean is a LIFO implemented with a linked list. You can implement it with a tree (useful for searches) as well if you'd like but that's not our need.

Though, I've just looked at wikipedia, and I got lost in translation, a LIFO really is a stack and we should rename all the functions I introduced as `s/heap/stack/`. Keeping the name as is will only cause confusion with the heap datastructure.

Using the typedef is only useful to discriminate the intention of usage of the datastructure. A `list_t` should behave like a list, and a `stack_t` like a stack (most importantly `push`, `pop` — `peek` is a nice to have to make it complete — and for our special use here: `contains`).

*mea culpa* 😖
I see. I don't think we should have two separate datastructures then.. perhaps the intent should be expressed by the variable name, e.g., `LIST *att_list;` vs. `LIST *rc_stack`. But we can have this discussion separately :)
> About object composition, AFAICT, we only care about a string datatype in mutt lists, so it's not a first class reason to switch.

@guyzmo  oh, now I see what you mean.. it's true that `LIST` is only a linked list of strings, but there are a lot of other datastructures that have a `next` pointer in them. The benefit of a generic approach to node-based datastructures would be quite huge :)
Do we have a list of the structures with a `next` pointer in them? I think there are quite a few of them, and that we would benefit from a generic, battle-proofed solution.
```
$ grep '\* \?next' mutt.h                                                                                                   
  struct list_t *next;
  struct rx_list_t *next;
  struct spam_list_t *next;
  struct alias *next;
  struct parameter *next;
  struct body *next;            /* next attachment in the list */
  THREAD *next;
  struct group_context_t *next;
  struct pattern_t *next;
```

First 2-sec glance shows this. If we unify handling of these, we're a good step forwards :)
with a bit of vim-fu, searching for `/*next/`, with the position at `line:column`:

```
 buffy.h        | 33:18   | struct buffy_t *next;          | single link list
 color.c        | 60:21   | struct color_list *next;       | single link list
 crypt-gpgme.c  | 93:22   | struct crypt_cache *next;      | single link list
 crypt-gpgme.c  | 106:24  | struct crypt_keyinfo *next;    | single link list
 hash.h         | 26:20   | struct hash_elem *next;        | single link list
 hook.c         | 43:15   | struct hook *next;             | single link list
 keymap.h       | 41:19   | struct keymap_t *next;         | single link list
 mh.c           | 80:18   | struct maildir *next;          | single link list
 mutt_curses.h  | 160:21  | struct color_line *next;       | single link list
 mutt.h         | 640:17  | struct list_t *next;           | single link list
 mutt.h         | 648:20  | struct rx_list_t *next;        | single link list
 mutt.h         | 656:22  | struct spam_list_t *next;      | single link list
 mutt.h         | 707:16  | struct alias *next;            | single link list (with weird self pointer)
 mutt.h         | 758:20  | struct parameter *next;        | single link list
 mutt.h         | 809:15  | struct body *next;             | single link list (but not only)
 mutt.h         | 962:10  | THREAD *next;                  | tree+list (parent/next/prev/child)
 mutt.h         | 986:26  | struct group_context_t *next;  | single link list
 mutt.h         | 1000:20 | struct pattern_t *next;        | tree (next/child)
 mutt_notmuch.c | 101:18  | struct uri_tag *next;          | single link list
 mutt_notmuch.c | 116:20  | struct nm_hdrtag *next;        | single link list
 mutt_socket.h  | 44:22   | struct _connection *next;      | single link list
 pgpkey.c       | 50:20   | struct pgp_cache *next;        | single link list
 pgplib.h       | 27:24   | struct pgp_signature *next;    | single link list
 pgplib.h       | 46:22   | struct pgp_keyinfo *next;      | tree (parent/next)
 pgplib.h       | 56:18   | struct pgp_uid *next;          | tree (parent/next)
 query.c        | 38:16   | struct query *next;            | single link list
 rfc2231.c      | 53:8    | *next;                         | single link list
 rfc822.h       | 44:20   | struct address_t *next;        | single link list
 score.c        | 34:18   | struct score_t *next;          | single link list
 smime.h        | 32:20   | struct smime_key *next;        | single link list
```

for the how, I used a plugin `BufGrep` which printed all that in the quickfix window, then I use `easyalign` to align on `|`. Then I looked at each occurence to check what structure it really is.
So for all the real linked lists we can have a type-agnostic struct like:

```c
typedef struct list_t {
  struct list_t next;
  void *payload; /* (or data or thing or bazinga) */
} list_t;
```

and for each of the pure single linked list above, make them into a type without self reference, and use the canonical set of functions or macros to init it and apply the needed algorithms on them. (we could be using the sys headers, or our own cooked thing).

for how to do it, I guess we should go iteratively, refactoring a struct per commit.
I will soon come up with a proof of concept
Not likely to come up with anything soon-ish. I don't think this is a priority, at least for now. I'll close this.
it would have been nice to at least have a PoC of what you had in mind
for replacement of the lists in mutt, either in a branch, or as a gist. 😉

Otherwise I agree, that's a long term goal, and might be an "easy" task
to give to a newcomer, given that the way to go is shown.


I have had some time to come back at this and have committed a proof of concept by converting two fields. Feedback is welcome.
A few more comments:

- I have chosen to use a singly-linked tail queue because the overhead is minimal and it allows O(1) insertion at both ends.
- Lists are no longer pointers to the first element, but represented by a separate head structure. This allows for fewer malloc/free's and fewer checks for NULL.
- Naming currently sucks, I don't like `STailQHead` and `STailQNode`. If we agree on using this as a replacement for the current `List`, the plan is to convert them all (perhaps in more passes) and then rename them back to `ListHead` and `ListNode`, or even simply `List`. Functions will also be renamed to their original names (`mutt_free_stailq` -> `mutt_free_list`).
I've taken a look at your work so far and it's very nice.
I traced through a couple of specific examples and familiarised myself with all the macros you've used so far.

We'll need to start thinking about how we test these changes -- it could take a while.

For now, please keep on fixing.
I'll close this and handle the conversion of any additional datastructure to queue.h in a separate issue.