Sprite editor select tool jumbles the selection content when moved
fixed here 38d4ed7
Hmm, your fix resets the selection when you drag outside of canvas, which can be an annoying behavior when you select close to the border and you accidentally slip out.
This is why I was checking that the left mouse button wasn't pressed as a condition for drag release (albeit not on the appropriate rect I suppose).

Also, there's one corner case isn't fixed by either solution : when the user moves the selection while dragging.

Please reopen.
@nesbox The head https://github.com/ddelemeny/TIC-80/commit/c2ee5b9ac3d9180831e8020867ebc6fbd2286fe5 referenced above tries to wrap it all up (based on your solution). Please have a look.
yes, you are right, it resets the selection
So, I reverted my previous fix and just disabled keyboard handling when you dragging a selection rectangle. It looks much simpler than other solutions 9352a37.
Thank you for your help.
@nesbox I confirm the corrupting behavior is fixed in all cases I could test.
I think preventing keyboard moves when dragging is a good call (and your location to do it for it is better than the one I chose). 

However, in my opinion the select tool should follow this rule :
>  **The drag state should always be released when the mouse button is released**, no matter if the pointer is inside or outside of canvas. 

With your last fix, you kept the original behavior : the drag state is kept active if you release the mouse button out of canvas, and it will stay active until the user brings the pointer back in canvas.
That's way less critical than corrupting the sprite of course, but I think it's a quirk that deserves to be fixed as well.

Please _(sorry)_ consider implementing out-of-canvas release. 
I combined all the solutions in one here 0717a19
You are hard-bitten :) Thanks
Haha, I'm a simple person : I see a nail sticking out of a board, I hit the nail with a hammer until it's not sticking out of the board.

This specific nail is now perfectly leveled (as far as I can tell). Thank you!