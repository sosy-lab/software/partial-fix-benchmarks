Music() function with tempo parameters
Also, you can add a channel mute function.  Unless, of course, it overloads the system.
Added music tempo/speed parameters to the RAM here 8c1e0ae (the wiki will be updated after 0.90 release)
so, music `tempo`, `speed` can be poked by `0x14E04/0x14E05` addr now

```
+-----------------------------------+
|           96KB RAM LAYOUT         |
+-------+-------------------+-------+
| ADDR  | INFO              | BYTES |
+-------+-------------------+-------+
           ...
| 14E04 | MUSIC PARAMETERS  | 2     |
           ...
+-------+-------------------+-------+
```

> Also, you can add a channel mute function. Unless, of course, it overloads the system.

Try to poke a sound register volume to mute the channel https://github.com/nesbox/TIC-80/wiki/RAM#sound-registers

Thank you.
>  (the wiki will be updated after 0.90 release)

I've been trying to keep it updated as we go and just adding notes on versioning when appropriate. Wrong idea?
Not at all, good idea :)
This is just a reminder for me to check the wiki before the release...
Thank you.


I reverted tempo/speed parameters in the RAM because it breaks compatibility with some carts, this one for example https://tic80.com/play?cart=479
160b29a
And I'm going to add speed/tempo to the music() API, reopening...
Added tempo/speed parameters to the `music(track=-1 frame=-1 row=-1 loop=true sustain=false tempo=-1 speed=-1)` api 39db700