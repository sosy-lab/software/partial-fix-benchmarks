Netdata stops monitoring restarted docker containers
I can't reproduce this.
I installed docker, run a container, it showed up.
Stopped the container, it stopped on netdata.
Restarted the container, it restarted refreshing on netdata, showing a gap for the time it was stopped.

netdata will create a new container entry, if docker changed the name of the container. Did your container change name?

When containers change name, you can just refresh the dashboard (hit F5 on your browser). Both the old and the new containers will show up. The old will be stopped. The new will be running.

I am also having trouble reproducing this with regularity. In testing, everything has been working, but I have noticed this a few times. I can can clarify what happened this morning to see if it helps; the container in question was stopped, removed, the image was updated, and the container was recreated and run. When I submitted this issue, the container was running without issue, but Netdata showed it as stopped. I did the same thing again just now (stop, remove, recreate), and Netdata began updating the data again.

hm... Could you please check that you have an error log like this: https://github.com/firehol/netdata/blob/25b14cd1959a66bd92e8bba1117cdbe8e659a902/src/sys_fs_cgroup.c#L719

i.e.: `Control group with chart id XXXXXX already exists with id YYYYYY and is enabled. Disabling cgroup with id ZZZZZ'`

Yes, I do have this entry from the time when the container was restarted:

```
Control group with chart id 'cloudprint' already exists with id '/docker/60a975ea404b51688d762a9d0699ab168b68ea972457ef19c92b86ff63f1036d' and is enabled. Disabling cgroup with id '/docker/30d9b465df4291701d0226078b0bf50297ac604d27c949445af032dae5e73b51'
```

ok. So, when 2 docker containers come with the same name, netdata now prefers to keep the first one and disables the new one.

I will make another check: it will keep the first one, only if it being collected. Otherwise it will keep the second.

Making a PR now...

merged it.
Could you please test it now works as expected?

It still seems to be doing the same thing. I updated Netdata, recreated a container, and it did not continue collecting data. I recreated the container again, and it started again. Then I recreated it again, and it stopped collecting data. I am not getting the log entry as I was before. Is there some timing aspect to it?

yes, it looks for new containers every 10 seconds by default. So, you have to wait 10 seconds for it to detect a new container exists.

It is controlled by netdata.conf:

```
[plugin:cgroups]
    check for new cgroups every = 10
```

Check also the error log. Every time a new container with overlapping name is detected, something gets printed. This states the decision made by netdata. There will be no error log only if the started container has the same docker id with the old one.

So there needs to be a 10 second delay between the removal and recreation of the container? Or I need to wait 10 seconds to see it reappear? Because it does not begin collecting container data, even after several hours. 

The containers always have new Docker IDs. The only thing I see listed in the error log is this, and that container ID is accurate for the new container, for which data is not being collected:

```
Running API command: /containers/b0c8153dacc81f6dd59509c94b51b61b39a9f81d30473f4693bb23d1a1189022/json
Docker container 'b0c8153dacc81f6dd59509c94b51b61b39a9f81d30473f4693bb23d1a1189022' is named 'flexget'
/usr/libexec/netdata/plugins.d/cgroup-name.sh: cgroup 'docker_b0c8153dacc81f6dd59509c94b51b61b39a9f81d30473f4693bb23d1a1189022' is called 'flexget'
```

This is a flow:
1. run the container. Make sure netdata collects data properly.
2. stop the container. Verify netdata stopped collecting data (the charts stopped).
3. start the container. Wait up to 10 seconds for netdata to show data from this container on the dashboard.

I just verified this works on my installation.

It still is not working on every other recreate. Here is the sequence I am using:
1. `docker stop ...`
2. `docker rm ...`
3. `docker pull ...`
4. `docker run ...`

What is happening for me is that I can run the above sequence, and Netdata will stop registering data, despite the fact that the container is running again (or rather, a new container is running with the same name). It will not resume registering data until I run the above sequence again, and specifically, the second time must be a number of seconds after the first. Waiting 10 seconds seems to work. 

If it still does not work, do this:
1. edit netdata.conf and set `debug flags = 0x00100000` (uncomment the line too). This will enable lot's of cgroups debugging output on `/var/log/netdata/debug.log`.
2. note the docker id of the container running.
3. restart netdata
4. verify it collects data for the container
5. stop the container
6. start the container - note its new docker id.
7. wait 20 seconds.
8. stop netdata - comment out the `debug flags` and restart it.
9. post here:
   - the old docker id
   - the new docker id
   - the file `/var/log/netdata/debug.log`

I just verified 3 times that your sequence works.

wait. I think I managed to re-produce it.

Okay, so I managed to get the information requested, but even after wiping the logs just before beginning, the debug log is over 3000 lines. How do you want me to post it?

no need yet for the logs.
Please check it works now.

That did it. It is working properly now.

ok. It was a timing issue. If docker was fast enough to stop and start the container within the timeframe netdata required to detect that one was stopped and another was started, we were ended up with a container removed and a container disabled.

Now, it doesn't try to detect that. It just marks the duplicate container as duplicate, and when the older is removed, it enables any duplicate waiting. So it always works.

Thanks for reporting this. Nasty bug...

Great, thank you very much for the help.
