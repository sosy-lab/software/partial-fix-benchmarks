diff --git a/conf/Makefile.am b/conf/Makefile.am
index 0704a5cc3f..2d5947dd5d 100644
--- a/conf/Makefile.am
+++ b/conf/Makefile.am
@@ -26,10 +26,14 @@ clean-local:
 #%-spellchecked: % Makefile.am $(top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
 #	$(MAKE) -s -f $(top_builddir)/docs/Makefile SPELLCHECK_SRC_ONE="$<" SPELLCHECK_DIR="$(srcdir)" $@
 
-.sample.sample-spellchecked: Makefile.am $(top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
+# NOTE: Portable suffix rules do not allow prerequisites, so we shim them here
+# by a wildcard target in case the make implementation can put the two together.
+*-spellchecked: Makefile.am $(top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
+
+.sample.sample-spellchecked:
 	$(MAKE) -s -f $(top_builddir)/docs/Makefile SPELLCHECK_SRC_ONE="$<" SPELLCHECK_DIR="$(srcdir)" $@
 
-.in.in-spellchecked: Makefile.am $(top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
+.in.in-spellchecked:
 	$(MAKE) -s -f $(top_builddir)/docs/Makefile SPELLCHECK_SRC_ONE="$<" SPELLCHECK_DIR="$(srcdir)" $@
 
 spellcheck spellcheck-interactive spellcheck-sortdict:
diff --git a/docs/ci-farm-lxc-setup.txt b/docs/ci-farm-lxc-setup.txt
index e992d00bca..f9c9d78b90 100644
--- a/docs/ci-farm-lxc-setup.txt
+++ b/docs/ci-farm-lxc-setup.txt
@@ -181,8 +181,8 @@ Shepherd the herd
 :; for ALTROOT in /srv/libvirt/rootfs/*/rootfs/ ; do \
     echo "=== $ALTROOT :" >&2; \
     chroot "$ALTROOT" apt-get install sudo bash ; \
-    grep eth0 "$ALTROOT/etc/issue" || { echo '\S{NAME} \S{VERSION_ID} \n \l@\b ; Current IP(s): \4{eth0} \4{eth1} \4{eth2} \4{eth3}' >> "$ALTROOT/etc/issue" ) ; \
-    grep eth0 "$ALTROOT/etc/issue.net" || { echo '\S{NAME} \S{VERSION_ID} \n \l@\b ; Current IP(s): \4{eth0} \4{eth1} \4{eth2} \4{eth3}' >> "$ALTROOT/etc/issue.net" ) ; \
+    grep eth0 "$ALTROOT/etc/issue" || ( echo '\S{NAME} \S{VERSION_ID} \n \l@\b ; Current IP(s): \4{eth0} \4{eth1} \4{eth2} \4{eth3}' >> "$ALTROOT/etc/issue" ) ; \
+    grep eth0 "$ALTROOT/etc/issue.net" || ( echo '\S{NAME} \S{VERSION_ID} \n \l@\b ; Current IP(s): \4{eth0} \4{eth1} \4{eth2} \4{eth3}' >> "$ALTROOT/etc/issue.net" ) ; \
     groupadd -R "$ALTROOT" -g 399 abuild ; \
     useradd -R "$ALTROOT" -u 399 -g abuild -M -N -s /bin/bash abuild \
     || useradd -R "$ALTROOT" -u 399 -g 399 -M -N -s /bin/bash abuild \
@@ -255,3 +255,176 @@ This can be achieved with e.g.:
 
 TODO: Test and document a working NAT and firewall setup for this, to allow
 SSH access to the containers via dedicated TCP ports exposed on the host.
+
+
+Connecting Jenkins to the containers
+~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
+
+To cooperate with the jenkins-dynamatrix driving NUT CI builds, each build
+environment should be exposed as an individual agent with labels describing
+its capabilities.
+
+Labels
+^^^^^^
+
+Emulated-CPU container builds are CPU-intensive, so for them we define as
+few capabilities as possible: here CI is more interested in checking how
+binaries behave on those CPUs, *not* in checking the quality of recipes
+(distcheck, Make implementations, etc.), shell scripts or documentation,
+which is more efficient to test on native platforms.
+
+Still, we are interested in results from different compiler suites, so
+specify at least one version of each.
+
+NOTE: Currently the NUT Jenkinsfile-dynamatrix only looks at various
+`COMPILER` variants for this use-case, disregarding the versions and
+just using one that the environment defaults to.
+
+The reduced set of labels for QEMU workers looks like:
+
+------
+qemu-nut-builder qemu-nut-builder:alldrv
+NUT_BUILD_CAPS=drivers:all NUT_BUILD_CAPS=cppunit
+OS_FAMILY=linux OS_DISTRO=debian11 GCCVER=10 CLANGVER=11
+COMPILER=GCC COMPILER=CLANG
+ARCH64=ppc64le ARCH_BITS=64
+------
+
+For contrast, a "real" build agent's set of labels, depending on
+presence or known lack of some capabilities, looks like:
+------
+doc-builder nut-builder nut-builder:alldrv
+NUT_BUILD_CAPS=docs:man NUT_BUILD_CAPS=docs:all
+NUT_BUILD_CAPS=drivers:all NUT_BUILD_CAPS=cppunit=no
+OS_FAMILY=bsd OS_DISTRO=freebsd12 GCCVER=10 CLANGVER=10
+COMPILER=GCC COMPILER=CLANG
+ARCH64=amd64 ARCH_BITS=64
+SHELL_PROGS=sh SHELL_PROGS=dash SHELL_PROGS=zsh SHELL_PROGS=bash
+SHELL_PROGS=csh SHELL_PROGS=tcsh SHELL_PROGS=busybox
+MAKE=make MAKE=gmake
+------
+
+Generic agent attributes
+^^^^^^^^^^^^^^^^^^^^^^^^
+
+* Name: e.g. `ci-debian-altroot--jenkins-debian10-arm64` (note the
+  pattern for "Conflicts With" detailed below)
+
+* Remote root directory: preferably unique per agent, to avoid surprises;
+  e.g.: `/home/abuild/jenkins-nut-altroots/jenkins-debian10-armel`
+
+* Usage: "Only build jobs with label expressions matching this node"
+
+* Node properties / Environment variables:
+
+** `PATH+LOCAL` => `/usr/lib/ccache`
+
+Where to run agent.jar
+^^^^^^^^^^^^^^^^^^^^^^
+
+Depending on circumstances of the container, there are several options
+available to the NUT CI farm:
+
+* Java can run in the container, efficiently (native CPU, different distro)
+  => the container may be exposed as a standalone host for direct SSH access
+  (usually by NAT, exposing SSH on a dedicated port of the host; or by first
+  connecting the Jenkins controller with the host as an SSH Build Agent, and
+  then calling SSH to the container as a prefix for running the agent), so
+  ultimately the build `agent.jar` would run in the container.
+  Filesystem for the `abuild` account may be or not be shared with the host.
+
+* Java can not run in the container (crashes on emulated CPU, or is too old
+  in the agent container's distro -- currently Jenkins requires JRE 8+, but
+  eventually will require 11+) => the agent would run on the host, and then
+  the host would `ssh` or `chroot` (networking not required, but bind-mount
+  of `/home/abuild` and maybe other paths from host would be needed) called
+  for executing `sh` steps in the container environment. Either way, home
+  directory of the `abuild` account is maintained on the host and shared with
+  the guest environment, user and group IDs should match.
+
+* Java is inefficient in the container (operations like un-stashing the source
+  succeed but take minutes instead of seconds) => either of the above
+
+Methods below involving SSH assume that you have configured a password-less
+key authentication from the host machine to the `abuild` account in container.
+This can be an `ssh-keygen` result posted into `authorized_keys`, or a trusted
+key passed by a chain of ssh agents from a Jenkins credential for connection
+to the container-hoster into the container.
+
+* For passing the agent through an SSH connection from host to container,
+  so that the `agent.jar` runs inside the container environment, configure:
+
+** Launch method: "Agents via SSH"
+
+** Host, Credentials, Port: as suitable for accessing the container hoster
+
+** Prefix Start Agent Command: content depends on the container name,
+   but generally looks like the example below to report some info about
+   the final target platform (and make sure `java` is usable) in the
+   agent's log. Note that it ends with un-closed quote and a space char:
++
+------
+ssh jenkins-debian10-amd64 '( java -version & uname -a ; getconf LONG_BIT; getconf WORD_BIT; wait ) && 
+------
+
+** Suffix Start Agent Command: a single quote to close the text opened above:
+------
+'
+------
+
+
+* The other option is to run the `agent.jar` on the host, for all the
+  network and filesystem magic the agent does, and only execute shell
+  steps in the container. The solution relies on overridden `sh` step
+  implementation in the jenkins-dynamatrix shared library that uses a
+  magic `CI_WRAP_SH` environment variable to execute a pipe into the
+  container. Such pipes can be `ssh` or `chroot` with appropriate host
+  setup described above.
++
+NOTE: In case of ssh piping, remember that the container's
+`/etc/ssh/sshd_config` should `AcceptEnv *` and the SSH
+server should be restarted after such change.
+
+** Launch method: "Agents via SSH"
+
+** Host, Credentials, Port: as suitable for accessing the container hoster
+
+** Prefix Start Agent Command: content depends on the container name,
+   but generally looks like the example below to report some info about
+   the final target platform (and make sure it is accessible) in the
+   agent's log. Note that it ends with a space char:
++
+------
+echo PING > /dev/tcp/jenkins-debian11-ppc64el/22 && ssh jenkins-debian11-ppc64el 'uname -a ; getconf LONG_BIT; getconf WORD_BIT; true;' && 
+------
+
+** Suffix Start Agent Command: empty
+
+* Node properties / Environment variables:
+
+** `CI_WRAP_SH` => `ssh -o SendEnv='*' "jenkins-debian11-ppc64el" /bin/sh -xe `
+
+
+Sequentializing the stress
+^^^^^^^^^^^^^^^^^^^^^^^^^^
+
+Another aspect of farm management is that emulation is a slow and intensive
+operation, so we can not run all agents and execute builds at the same time.
+
+The current solution relies on https://github.com/jenkinsci/jenkins/pull/5764
+to allow build agents to conflict with each other -- one picks up a job from
+the queue and blocks others from starting; when it is done, another may start.
+
+Containers can be configured with "Availability => On demand", with shorter
+cycle to switch over faster (the core code sleeps a minute between attempts):
+
+* In demand delay: `0`;
+
+* Idle delay: `0` (Jenkins may change it to `1`);
+
+* Conflicts with: `^ci-debian-altroot--.*$` assuming that is the pattern
+  for agent definitions in Jenkins -- not necessarily linked to hostnames.
+
+Also, the "executors" count should be reduced to the amount of compilers
+in that system (usually 2) and so avoid extra stress of scheduling too many
+emulated-CPU builds at once.
diff --git a/docs/contact-closure.txt b/docs/contact-closure.txt
index 80578bcd0f..044039e772 100644
--- a/docs/contact-closure.txt
+++ b/docs/contact-closure.txt
@@ -79,10 +79,14 @@ Outgoing lines:
 
 Incoming lines:
 
-- line_ol   = flag that appears for on line / on battery
-- val_ol    = value of that flag when the UPS is on battery
-- line_bl   = flag that appears for low battery / battery OK
-- val_bl    = value of that flag when the battery is low
+- line_ol     = flag that appears for on line / on battery
+- val_ol      = value of that flag when the UPS is on battery
+- line_bl     = flag that appears for low battery / battery OK
+- val_bl      = value of that flag when the battery is low
+- line_rb     = flag that appears for battery health
+- val_rb      = value of that flag when the battery needs a replacement
+- line_bypass = flag that appears for battery bypass / battery protection active
+- val_bypass  = value of that flag when the battery is bypassed / missing
 
 This may seem a bit confusing to have two variables per value that
 we want to read, but here's how it works.  If you set line_ol to
diff --git a/docs/man/Makefile.am b/docs/man/Makefile.am
index eddd6ed186..b430b94391 100644
--- a/docs/man/Makefile.am
+++ b/docs/man/Makefile.am
@@ -909,7 +909,10 @@ clean-local:
 #%-spellchecked: % Makefile.am $(top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
 #	$(MAKE) -s -f $(top_builddir)/docs/Makefile SPELLCHECK_SRC_ONE="$<" SPELLCHECK_DIR="$(srcdir)" $@
 
-.txt.txt-spellchecked: Makefile.am $(abs_top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
+# NOTE: Portable suffix rules do not allow prerequisites, so we shim them here
+# by a wildcard target in case the make implementation can put the two together.
+*.txt-spellchecked: Makefile.am $(abs_top_srcdir)/docs/Makefile.am $(abs_srcdir)/$(NUT_SPELL_DICT)
+.txt.txt-spellchecked:
 	$(MAKE) -s -f $(top_builddir)/docs/Makefile SPELLCHECK_SRC_ONE="$<" SPELLCHECK_DIR="$(srcdir)" $@
 
 spellcheck spellcheck-interactive spellcheck-sortdict:
diff --git a/docs/man/genericups.txt b/docs/man/genericups.txt
index 127dfb7b39..b948d0e3dc 100644
--- a/docs/man/genericups.txt
+++ b/docs/man/genericups.txt
@@ -18,7 +18,8 @@ kind of interface where basic high/low signals are provided to indicate
 status.  This kind of UPS can only report line power and battery status.
 
 This means that you will only get the essentials in ups.status: OL, OB,
-and LB.  Anything else requires a smarter UPS.
+and LB (some UPSes may also support RB and BYPASS).  Anything else requires
+a smarter UPS.
 
 CABLING
 -------
@@ -95,6 +96,10 @@ OL:: On line (no power failure) (opposite of OB - on battery)
 
 LB:: Low battery
 
+RB:: Replace battery
+
+BYPASS:: Battery bypass active or no battery installed
+
 SD:: Shutdown load
 
 CP:: Cable power (must be present for cable to have valid reading)
@@ -109,8 +114,15 @@ RNG:: Ring indicate.  Received from the UPS.
 
 DTR:: Data Terminal Ready.  Sent by the PC.
 
+DSR:: Data Set Ready.  Received from the UPS.
+
 ST:: Send a BREAK on the transmit data line
 
+NULL:: Disable this signal.  Disabled signal will always be low except for OL
+       which will always be high.
+
+none:: Alias to `NULL` which matches some other documentation.
+
 A "-" in front of a signal name (like -RNG) means that the indicated
 condition is signaled with an active low signal.  For example, [LB=-RNG]
 means the battery is low when the ring indicate line goes low, and that
@@ -219,6 +231,11 @@ UPS TYPES
 
      [CP=RTS] [OL=CTS] [LB=-DCD] [SD=DTR]
 
+23 = Generic FTTx (Fiber to the x) battery backup
+     with 4-wire telemetry interface
+
+     [CP=RTS] [OL=CTS] [LB=-DCD] [RB=-RNG] [BYPASS=-DSR] [SD=none]
+
 SIMILAR MODELS
 --------------
 
diff --git a/docs/nut-qa.txt b/docs/nut-qa.txt
index 147e56d9b6..8d237d182d 100644
--- a/docs/nut-qa.txt
+++ b/docs/nut-qa.txt
@@ -66,11 +66,20 @@ trees are reported on the link:http://lists.alioth.debian.org/mailman/listinfo/n
 mailing list.
 ////////////////////////////////////////////////////////////////////////////////
 
-- link:http://buildbot.networkupstools.org/public/nut/[Buildbot] and
-link:https://travis-ci.org/networkupstools/nut/builds[Travis CI] to automate the
-compile/test cycle. Any build failure is caught early, and fixed quickly. Also
-we get to see if incoming pull requests (as well as Git branch HEADs) do not have
-code (or recipe) that is instantly faulty and can not build on even one platform.
+- link:http://buildbot.networkupstools.org/public/nut/[Buildbot]
+  and the new dedicated Jenkins incarnation of the NUT CI Farm with "legacy UI"
+  for link:https://ci.networkupstools.org/job/nut/job/nut/job/master/[main branch]
+  and link:https://ci.networkupstools.org/job/nut/job/nut/view/change-requests/[PRs],
+  also accessible at the slower but slicker-looking Blue Ocean user interface for
+  link:https://ci.networkupstools.org/blue/organizations/jenkins/nut%2Fnut/activity/[activity],
+  link:https://ci.networkupstools.org/blue/organizations/jenkins/nut%2Fnut/branches/[branches]
+  and link:https://ci.networkupstools.org/blue/organizations/jenkins/nut%2Fnut/pr/[PRs],
+  are all used to automate the compile/test cycle, using numerous platforms,
+  target distributions, C/C++ revisions, compiler toolkits and make program
+  implementations. Any build failure is caught early, and fixed quickly.
+  Also we get to see if incoming pull requests (as well as Git branch HEADs)
+  do not have code (or recipe) that is instantly faulty and can not build on
+  one of the platforms we track even with relaxed warnings.
 
 ////////////////////////////////////////////////////////////////////////////////
  reported through the
diff --git a/docs/nut.dict b/docs/nut.dict
index 91f1233bb7..82e0fa9f0e 100644
--- a/docs/nut.dict
+++ b/docs/nut.dict
@@ -1,4 +1,4 @@
-personal_ws-1.1 en 2782 utf-8
+personal_ws-1.1 en 2808 utf-8
 AAS
 ACFAIL
 ACFREQ
@@ -39,6 +39,7 @@ AVR
 AVRLCD
 AWG
 Ablerex
+AcceptEnv
 ActivePower
 AdMiN
 Affero
@@ -159,6 +160,7 @@ CERTREQUEST
 CERTVERIF
 CEST
 CHRG
+CLANGVER
 CLI
 CLOCAL
 CMDDESC
@@ -328,6 +330,7 @@ FREHn
 FRELn
 FRU
 FTS
+FTTx
 FTUPS
 FV
 Faber
@@ -353,6 +356,7 @@ FreqSens
 Frolov
 FullLoad
 Fuß
+GCCVER
 GES
 GETADDRINFO
 GKrellM
@@ -784,6 +788,7 @@ PPDnnn
 PPP
 PPPPPPPPPP
 PR'ed
+PROGS
 PRs
 PSA
 PSD
@@ -1024,8 +1029,10 @@ Schonefeld
 Schroder
 Sekury
 Selinger
+SendEnv
 Senoidal
 Sep
+Sequentializing
 Serv
 Shara
 Shaul
@@ -1140,6 +1147,7 @@ UB
 UBD
 UBR
 UDP
+UI
 UID
 UIDA
 UINT
@@ -1173,6 +1181,7 @@ UTC
 UTalk
 UUU
 UX
+Ubuntu
 Ulf
 Uncomment
 Unices
@@ -1310,6 +1319,8 @@ alloc
 allowfrom
 altinterface
 altpidpath
+altroot
+altroots
 amd
 anded
 aod
@@ -1329,6 +1340,7 @@ argc
 argparse
 args
 argv
+armel
 armhf
 asapm
 ascii
@@ -1424,6 +1436,7 @@ boostvoolts
 bootable
 bp
 br
+bsd
 bsv
 bt
 bti
@@ -1571,6 +1584,7 @@ ddl
 de
 deUNV
 debian
+debootstrap
 debouncing
 deci
 decrement
@@ -1667,6 +1681,7 @@ energysave
 english
 enum
 envvar
+envvars
 ep
 epdu
 epodebounce
@@ -1726,6 +1741,7 @@ forcessl
 formatconfig
 formatparam
 fp
+freebsd
 freeipmi
 freqsensitivity
 frob
@@ -1753,6 +1769,7 @@ getDevice
 getTrackingResult
 getValue
 getVariable
+getconf
 getent
 getenv
 getopt
@@ -1792,6 +1809,7 @@ hidparser
 hidups
 highbattery
 highfrequency
+hoster
 hostname
 hostnames
 hostsfile
@@ -1893,6 +1911,7 @@ kadets
 kaminski
 kde
 keyclick
+keygen
 keyout
 killall
 killpower
@@ -1906,6 +1925,7 @@ lan
 langid
 lasaine
 ld
+le
 len
 lf
 libaugeas
@@ -2195,12 +2215,14 @@ oq
 os
 ostream
 otherprotocols
+outliers
 pF
 paramkeywords
 parsability
 parsable
 parseconf
 passname
+passphrase
 passthrough
 passwd
 passwordlevel
@@ -2464,6 +2486,7 @@ srcdir
 srv
 srw
 ss
+sshd
 ssl
 stan
 startIP
@@ -2597,6 +2620,7 @@ uA
 uD
 uM
 ua
+ubuntu
 uc
 ucb
 udev
@@ -2606,6 +2630,7 @@ ugen
 ukUNV
 ul
 un
+uname
 uncomment
 unconfigured
 undefine
@@ -2748,6 +2773,7 @@ xa
 xaabbcc
 xcalloc
 xd
+xe
 xferdelay
 xff
 xfff
diff --git a/drivers/apc-mib.c b/drivers/apc-mib.c
index 0989821a16..64c1ad6897 100644
--- a/drivers/apc-mib.c
+++ b/drivers/apc-mib.c
@@ -26,7 +26,7 @@
 
 #include "apc-mib.h"
 
-#define APCC_MIB_VERSION	"1.3"
+#define APCC_MIB_VERSION	"1.4"
 
 #define APC_UPS_DEVICE_MODEL	".1.3.6.1.4.1.318.1.1.1.1.1.1.0"
 /* FIXME: Find a better oid_auto_check vs sysOID for this one? */
@@ -365,6 +365,8 @@ static snmp_info_t apcc_mib[] = {
 	{ "input.transfer.high", ST_FLAG_STRING | ST_FLAG_RW, 3, ".1.3.6.1.4.1.318.1.1.1.5.2.2.0", "", SU_TYPE_INT | SU_FLAG_OK, NULL },
 	{ "input.transfer.reason", ST_FLAG_STRING, 1, APCC_OID_TRANSFERREASON, "", SU_TYPE_INT | SU_FLAG_OK, apcc_transfer_reasons },
 	{ "input.sensitivity", ST_FLAG_STRING | ST_FLAG_RW, 1, APCC_OID_SENSITIVITY, "", SU_TYPE_INT | SU_FLAG_OK, apcc_sensitivity_modes },
+	{ "ups.power", 0, 1, ".1.3.6.1.4.1.318.1.1.1.4.2.9.0", "", SU_FLAG_OK, NULL },
+	{ "ups.realpower", 0, 1, ".1.3.6.1.4.1.318.1.1.1.4.2.8.0", "", SU_FLAG_OK, NULL },
 	{ "ups.status", ST_FLAG_STRING, SU_INFOSIZE, APCC_OID_POWER_STATUS, "OFF",
 		SU_FLAG_OK | SU_STATUS_PWR, apcc_pwr_info },
 	{ "ups.status", ST_FLAG_STRING, SU_INFOSIZE, APCC_OID_BATT_STATUS, "",
diff --git a/drivers/genericups.c b/drivers/genericups.c
index a9e23efafb..dd126208ea 100644
--- a/drivers/genericups.c
+++ b/drivers/genericups.c
@@ -25,7 +25,7 @@
 #include "nut_stdint.h"
 
 #define DRIVER_NAME	"Generic contact-closure UPS driver"
-#define DRIVER_VERSION	"1.36"
+#define DRIVER_VERSION	"1.37"
 
 /* driver description structure */
 upsdrv_info_t upsdrv_info = {
@@ -83,6 +83,11 @@ static void parse_output_signals(const char *value, int *line)
 		fatalx(EXIT_FAILURE, "Can't override output with DSR (not an output)");
 	}
 
+	if (strstr(value, "NULL") || strstr(value, "none")) {
+		upsdebugx(3, "%s: disable", __func__);
+		*line = 0;
+	}
+
 	if(*line == old_line) {
 		upslogx(LOG_NOTICE, "%s: output overrides specified, but no effective difference - check for typos?", __func__);
 	}
@@ -156,6 +161,12 @@ static void parse_input_signals(const char *value, int *line, int *val)
 		fatalx(EXIT_FAILURE, "Can't override input with ST (not an input)");
 	}
 
+	if (strstr(value, "NULL") || strstr(value, "none")) {
+		*line = 0;
+		*val = 0;
+		upsdebugx(3, "%s: disable", __func__);
+	}
+
 	if((*line == old_line) && (*val == old_val)) {
 		upslogx(LOG_NOTICE, "%s: input overrides specified, but no effective difference - check for typos?", __func__);
 	}
@@ -188,12 +199,22 @@ void upsdrv_initinfo(void)
 		parse_input_signals(v, &upstab[upstype].line_bl, &upstab[upstype].val_bl);
 		upsdebugx(2, "parse_input_signals: LB overridden with %s\n", v);
 	}
+
+	if ((v = getval("RB")) != NULL) {
+		parse_input_signals(v, &upstab[upstype].line_rb, &upstab[upstype].val_rb);
+		upsdebugx(2, "parse_input_signals: RB overridden with %s\n", v);
+	}
+
+	if ((v = getval("BYPASS")) != NULL) {
+		parse_input_signals(v, &upstab[upstype].line_bypass, &upstab[upstype].val_bypass);
+		upsdebugx(2, "parse_input_signals: BYPASS overridden with %s\n", v);
+	}
 }
 
 /* normal idle loop - keep up with the current state of the UPS */
 void upsdrv_updateinfo(void)
 {
-	int	flags, ol, bl, ret;
+	int	flags, ol, bl, rb, bypass, ret;
 
 	ret = ioctl(upsfd, TIOCMGET, &flags);
 
@@ -204,8 +225,13 @@ void upsdrv_updateinfo(void)
 		return;
 	}
 
+	/* Always online when OL is disabled */
 	ol = ((flags & upstab[upstype].line_ol) == upstab[upstype].val_ol);
-	bl = ((flags & upstab[upstype].line_bl) == upstab[upstype].val_bl);
+
+	/* Always have the flags cleared when other status flags are disabled */
+	bl = upstab[upstype].line_bl != 0 && ((flags & upstab[upstype].line_bl) == upstab[upstype].val_bl);
+	rb = upstab[upstype].line_rb != 0 && ((flags & upstab[upstype].line_rb) == upstab[upstype].val_rb);
+	bypass = upstab[upstype].line_bypass != 0 && ((flags & upstab[upstype].line_bypass) == upstab[upstype].val_bypass);
 
 	status_init();
 
@@ -219,9 +245,17 @@ void upsdrv_updateinfo(void)
 		status_set("OB");	/* on battery */
 	}
 
+	if (rb) {
+		status_set("RB");	/* replace battery */
+	}
+
+	if (bypass) {
+		status_set("BYPASS");	/* battery bypass */
+	}
+
 	status_commit();
 
-	upsdebugx(5, "ups.status: %s %s\n", ol ? "OL" : "OB", bl ? "BL" : "");
+	upsdebugx(5, "ups.status: %s %s %s %s\n", ol ? "OL" : "OB", bl ? "BL" : "", rb ? "RB" : "", bypass ? "BYPASS" : "");
 
 	ser_comm_good();
 	dstate_dataok();
@@ -331,6 +365,8 @@ void upsdrv_makevartable(void)
 	addvar(VAR_VALUE, "CP", "Override cable power setting");
 	addvar(VAR_VALUE, "OL", "Override on line signal");
 	addvar(VAR_VALUE, "LB", "Override low battery signal");
+	addvar(VAR_VALUE, "RB", "Override replace battery signal");
+	addvar(VAR_VALUE, "BYPASS", "Override battery bypass signal");
 	addvar(VAR_VALUE, "SD", "Override shutdown setting");
 	addvar(VAR_VALUE, "sdtime", "Hold time for shutdown value (seconds)");
 }
diff --git a/drivers/genericups.h b/drivers/genericups.h
index d4d7f357bf..f4c3a45a73 100644
--- a/drivers/genericups.h
+++ b/drivers/genericups.h
@@ -27,6 +27,8 @@ static struct {
 	int	line_norm;
 	int	line_ol, val_ol;
 	int	line_bl, val_bl;
+	int	line_rb, val_rb;
+	int	line_bypass, val_bypass;
 	int	line_sd;
 }	upstab[] =
 {
@@ -37,6 +39,8 @@ static struct {
 	  TIOCM_DTR | TIOCM_RTS,	/* cable power: DTR + RTS	*/
 	  TIOCM_CTS, 0,			/* online: CTS off		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CD on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_RTS			/* shutdown: RTS		*/
 	},
 
@@ -47,6 +51,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_RNG, 0,			/* online: RNG off		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CD on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_RTS 			/* shutdown: RTS		*/
 	},
 
@@ -57,6 +63,8 @@ static struct {
 	  TIOCM_RTS,			/* cable power: RTS		*/
 	  TIOCM_CTS, 0,			/* online: CTS off		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CD on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR|TIOCM_RTS		/* shutdown: DTR + RTS		*/
 	},
 
@@ -67,6 +75,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, 0,			/* online: CTS off		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CD on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR | TIOCM_RTS		/* shutdown: DTR + RTS		*/
 	},
 
@@ -77,6 +87,8 @@ static struct {
 	  TIOCM_RTS,			/* cable power: RTS		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  0				/* shutdown: none		*/
 	},
 
@@ -87,6 +99,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR | TIOCM_RTS		/* shutdown: DTR + RTS		*/
 	},
 
@@ -97,6 +111,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_RTS			/* shutdown: set RTS		*/
 	},
 
@@ -107,6 +123,8 @@ static struct {
 	  TIOCM_RTS,			/* cable power: RTS		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR			/* shutdown: set DTR		*/
 	},
 
@@ -117,6 +135,8 @@ static struct {
           TIOCM_DTR,			/* cable power: DTR		*/
           TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
           TIOCM_CD, 0,			/* low battery: CD off		*/
+          0, 0,				/* replace battery: none	*/
+          0, 0,				/* battery bypass: none		*/
           -1				/* shutdown: unknown		*/
         },
 
@@ -127,6 +147,8 @@ static struct {
 	  0,				/* cable power: none		*/
 	  TIOCM_CD, 0,			/* online: CD off		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* low battery: CTS on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_RTS			/* shutdown: RTS		*/
 	},
 
@@ -137,6 +159,8 @@ static struct {
 	  TIOCM_RTS,                    /* cable power: RTS             */
 	  TIOCM_CTS, TIOCM_CTS,         /* online: CTS on               */
 	  TIOCM_CD, 0,                  /* low battery: CD off          */
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR                     /* shutdown: DTR                */
 	},
 
@@ -147,6 +171,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, 0,			/* online: CTS off		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_ST			/* shutdown: ST			*/
 	},
 
@@ -157,6 +183,8 @@ static struct {
 	  TIOCM_RTS,			/* cable power: RTS		*/
 	  TIOCM_CTS, 0,			/* online: CTS off		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CD on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR			/* shutdown: raise DTR		*/
 	},
 
@@ -167,6 +195,8 @@ static struct {
 	  TIOCM_DTR | TIOCM_RTS,	/* cable power: DTR + RTS	*/
 	  TIOCM_CD, TIOCM_CD,		/* On-line : DCD on		*/
 	  TIOCM_CTS, 0,			/* Battery low: CTS off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_ST			/* shutdown: TX BREA		*/
 	},
 
@@ -177,6 +207,8 @@ static struct {
 	   TIOCM_DTR,			/* cable power: DTR		*/
 	   TIOCM_CD, TIOCM_CD,		/* online: CD on		*/
 	   TIOCM_CTS, 0,		/* low battery: CTS off		*/
+	   0, 0,				/* replace battery: none	*/
+	   0, 0,				/* battery bypass: none		*/
 	   TIOCM_RTS			/* shutdown: raise RTS		*/
 	},
 
@@ -187,6 +219,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_ST			/* shutdown: ST (break)		*/
 	},
 
@@ -197,6 +231,8 @@ static struct {
 	  TIOCM_DTR | TIOCM_RTS,	/* cable power: DTR + RTS	*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  -1				/* shutdown: unknown		*/
 	},
 
@@ -207,6 +243,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  -1				/* shutdown: unknown		*/
 	},
 
@@ -217,6 +255,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CAR on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  -1				/* shutdown: none		*/
 	},
 
@@ -227,6 +267,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: DCD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_RTS			/* shutdown: set RTS		*/
 	},
 
@@ -238,6 +280,8 @@ static struct {
 	  TIOCM_DTR,			/* cable power: DTR		*/
 	  TIOCM_CTS, 0,			/* online: CTS off		*/
 	  TIOCM_CD, TIOCM_CD,		/* low battery: CD on		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_ST			/* shutdown: ST (break)		*/
 	},
 
@@ -249,6 +293,8 @@ static struct {
 	  TIOCM_RTS,                    /* cable power: RTS		*/
 	  TIOCM_CTS, TIOCM_CTS,         /* online: CTS on		*/
 	  TIOCM_CD, 0,                  /* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_RTS | TIOCM_DTR         /* shutdown: RTS+DTR		*/
 	},
 
@@ -259,9 +305,23 @@ static struct {
 	  TIOCM_RTS,			/* cable power: RTS		*/
 	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
 	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  0, 0,				/* replace battery: none	*/
+	  0, 0,				/* battery bypass: none		*/
 	  TIOCM_DTR			/* shutdown: DTR		*/
 	},
 
+	/* Type 23 */
+	{ "Generic",
+	  "Generic FTTx Battery Backup",
+	  "FTTx (Fiber to the x) battery backup with 4-wire telemetry interface",
+	  TIOCM_RTS,			/* cable power: RTS		*/
+	  TIOCM_CTS, TIOCM_CTS,		/* online: CTS on		*/
+	  TIOCM_CD, 0,			/* low battery: CD off		*/
+	  TIOCM_RI, 0,			/* replace battery: RI off	*/
+	  TIOCM_DSR, 0,			/* battery bypass: DSR off	*/
+	  0				/* shutdown: none		*/
+	},
+
 	/* add any new entries directly above this line */
 
 	{ NULL,
@@ -270,6 +330,8 @@ static struct {
 	  0,
 	  0, 0,
 	  0, 0,
+	  0, 0,
+	  0, 0,
 	  0
 	}
 };
diff --git a/scripts/DMF/dmfsnmp/apc-mib.dmf b/scripts/DMF/dmfsnmp/apc-mib.dmf
index 1caea8a973..39405af81d 100644
--- a/scripts/DMF/dmfsnmp/apc-mib.dmf
+++ b/scripts/DMF/dmfsnmp/apc-mib.dmf
@@ -89,6 +89,8 @@
 		<snmp_info default="" flag_ok="yes" multiplier="3.0" name="input.transfer.high" oid=".1.3.6.1.4.1.318.1.1.1.5.2.2.0" power_status="yes" string="yes" writable="yes"/>
 		<snmp_info default="" flag_ok="yes" lookup="apcc_transfer_reasons" multiplier="1.0" name="input.transfer.reason" oid="1.3.6.1.4.1.318.1.1.1.3.2.5.0" power_status="yes" string="yes"/>
 		<snmp_info default="" flag_ok="yes" lookup="apcc_sensitivity_modes" multiplier="1.0" name="input.sensitivity" oid=".1.3.6.1.4.1.318.1.1.1.5.2.7.0" power_status="yes" string="yes" writable="yes"/>
+		<snmp_info default="" flag_ok="yes" multiplier="1.0" name="ups.power" oid=".1.3.6.1.4.1.318.1.1.1.4.2.9.0"/>
+		<snmp_info default="" flag_ok="yes" multiplier="1.0" name="ups.realpower" oid=".1.3.6.1.4.1.318.1.1.1.4.2.8.0"/>
 		<snmp_info default="OFF" flag_ok="yes" lookup="apcc_pwr_info" multiplier="128.0" name="ups.status" oid=".1.3.6.1.4.1.318.1.1.1.4.1.1.0" power_status="yes" string="yes"/>
 		<snmp_info battery_status="yes" default="" flag_ok="yes" lookup="apcc_batt_info" multiplier="128.0" name="ups.status" oid=".1.3.6.1.4.1.318.1.1.1.2.1.1.0" string="yes"/>
 		<snmp_info calibration="yes" default="" flag_ok="yes" lookup="apcc_cal_info" multiplier="128.0" name="ups.status" oid=".1.3.6.1.4.1.318.1.1.1.7.2.6.0" string="yes"/>
@@ -177,6 +179,6 @@
 		<snmp_info command="yes" default="3" flag_ok="yes" multiplier="1.0" name="calibrate.stop" oid=".1.3.6.1.4.1.318.1.1.1.7.2.5.0"/>
 		<snmp_info command="yes" default="2" flag_ok="yes" multiplier="1.0" name="reset.input.minmax" oid=".1.3.6.1.4.1.318.1.1.1.9.1.1.0"/>
 	</snmp>
-	<mib2nut auto_check=".1.3.6.1.4.1.318.1.1.1.1.1.1.0" mib_name="apcc" name="apc" oid=".1.3.6.1.4.1.318.1.1.1.1.1.1.0" power_status=".1.3.6.1.4.1.318.1.1.1.4.1.1.0" snmp_info="apcc_mib" version="1.3"/>
+	<mib2nut auto_check=".1.3.6.1.4.1.318.1.1.1.1.1.1.0" mib_name="apcc" name="apc" oid=".1.3.6.1.4.1.318.1.1.1.1.1.1.0" power_status=".1.3.6.1.4.1.318.1.1.1.4.1.1.0" snmp_info="apcc_mib" version="1.4"/>
 </nut>
 
