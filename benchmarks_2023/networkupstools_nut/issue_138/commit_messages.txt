Fix USB permission issues related to Linux / udev

Rename udev rules file to 62-nut-usbups.rules, to prevent NUT USB
privileges from being overwritten

Closes #140
Remove redundant usb_set_altinterface(), unless user requests it

Closes networkupstools/nut#138
Remove redundant usb_set_altinterface(), unless user requests it

Adds flag/value to USB driver options.

Closes networkupstools/nut#138
Remove redundant usb_set_altinterface(), unless user requests it

Adds flag/value to USB driver options.

Closes networkupstools/nut#138
Merge pull request #138 from jimklimov/FTY-remerge-20210922
