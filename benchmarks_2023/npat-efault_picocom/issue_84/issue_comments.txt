Improve BSD support for platform specific implementations
Go ahead, if the changes are rather simple. 

Otherwise, I would be reluctant to add code for platforms I don't actually use. Esp. for stuff like custom baudrates, which is a "fringe" feature, anyway. I would leave this to someone who *actually* uses the platform, *and* actually needs the feature. 

But again, if changes are simple, go ahead...  
  
It's more about testing if the FreeBsd implementation also works with those other Bsd variants than really implementation. So I expect just some `#ifdef` adjustments and doc updates if the tests succeeds.
That's ok
First refactoring

First, I'd do minor refactoring, moving that `#if defined(__linux__) && defined(USE_CUSTOM_BAUD)` ... stuff out of `term.c` and put it to `termios2.h` / `custbaud_bsd.h` headers. 
Moving that platform specific `#ifdefs` to the platform specific headers helps removing duplicate complex `#ifdef` constructs. 
The both platform specific headers then do `#define HAS_CUSTOM_BAUD` to signal that custom baudrate support is implemented for the target platform.
I'd tested all above mentioned platforms with both `USE_CUSTOM_BAUD` unset and set to check if that refactoring don't raises unexpected issues.

→ f9e0b4c8a23951b711ba91caa1beef20ac5b5f7e

No! I don't want so many changes. Especially, I don't want `term.c` to include both `custbaud_bsd.h` and `termios2.h` (confusing to inc. something `_bsd.h` when building under non-bsd). 

Is anything more than adding a couple of more or's for other systems required?

    #if (defined (__FreeBSD__) || defined(__APPLE__)) && defined(USE_CUSTOM_BAUD)
    /* only for FreeBSD and macOS (Tiger and above) */
    #include "custbaud_bsd.h"
    #endif

If we latter see that things get too complicated, then we may refactor...
To make things a little easier, you could do something like (in `term.c`):

    #ifdef USE_CUSTOM_BAUD
    #if defined (__linux__)
    #include "termios2.h"
    #elif defined (__FreeBSD__) || defined (__APPLE__)
    #include "custbaud_bsd.h"
    #else
    #error "USE_CUSTOM_BAUD not supported in this system"
    #endif
    #endif /* of USE_CUSTOM_BAUD */

Ok, so I'll move that `#ifdef's` from `termios2.h` and `custbaud_bsd.h` back to `term.c`.
Modem control commit looks good. Do a pull request when ready.
Just rename `bsd.txt` to something more specific (i don't know... like: `modem_control_bsd.txt`)

    Ok, so I'll move that #ifdef's from termios2.h and custbaud_bsd.h back to term.c.

Yes, and remove the `HAS_CUSTOM_BAUD` stuff
I'll currently test around custom baudrates for the different bsd variant. Have a couple of embedded and bare metal developers which need this frequently.

Why dislike `HAS_CUSTOM_BAUD`? It signals that there is a custom specific implementation for custom baudrate support for the current build target.
... doing a lot of work in my spare time to have those features for an increasing number of target systems and even making sure to don't break build for other platforms.
> Why dislike HAS_CUSTOM_BAUD? It signals that there is a custom specific implementation for custom baudrate support for the current build target.

Bacause: (a) Minimal changes, and (b), we already have `USE_CUSTOM_BAUD`. Don't need to add another macro. If there is no support for a system, and `USE_CUSTOM_BAUD` is defined, picocom should fail to compile. 

See also my previous comment:

https://github.com/npat-efault/picocom/issues/84#issuecomment-357767693

The idea is to test if custom-baudrate stuff works with the other BSDs, and if necessary to make the *minimal* changes (an `#ifdef`, here and there, if required) to make it work. Not to refactor the way the custom-baudrate code is conditionally compiled.

Sorry for sounding pedantic, but I try to keep things contained. 
Hmm, we basically have analogous situation for manual handshake control. For some platforms it works, for others not. But it build for all platforms without the need for a switch controlled by Makefile.

Why going different way for custom baudrate support?

The final idea behind that newly introduced `HAS_CUSTOM_BAUD` is to ''maybe'' have `USE_CUSTOM_BAUD` switched on by default in future (or remove it and adjust the platform dependency a bit closer to the way as manual handshake control goes).

The final benefit is, that distribution maintainers don't need to check for Makefile modifications and we hopefully have a picocom with custom baudrate support in all linux distros in future.
Maybe,  but this is not the point of this issue! 

Personally, I consider custom-baudrate support a fringe feature. At least under Linux, it requires ugly hacks, and officially, it is *not supported* by `libc`. Regardless, we do provide support for it, but not by default. All the user needs to do, is to uncomment a couple of lines in the Makefile, so it's not a big deal. 

Anyway, maybe sometime in the future I will change my mind about this (I do see the advantages), but this decision has to be explicit and it is *not* something I'll to do today.

The point of this issue was *not* to change how picocom handles custom baudrate and whether it is enabled by default. The point was to check if the code compiles and works for other BSDs.

This and only this.

As for why I do not want to enable custom baudrate by default... 

As I said, under Linux (which is the primary supported platform) it requires ugly hacks, and officially it is **not** supported by GLIBC. I have not tested it under all architectures, and there is a very real chance that it may *not* work---or even compile---for some of them (it will, most likely, work for most, but I *cannot* be sure). 

I consider very important to have a picocom that, by default, compiles *reliably* with a simple `make`. *Even* if some features are disabled (and custom baudrate support is something rarely required).  

So the current situation is a compromise of this (and other, which I will not get into here) factors. 

But, as I said in my previous comment, this issue is *unrelated with any of these*. Let's keep it this way.

Ok, you're the boss. Have not realized that the linux implementation is such hacky. I'll turn it back and follow your [comment](https://github.com/npat-efault/picocom/issues/84#issuecomment-357767693).
Thanks!

I will also open an issue to discuss how we could enable USE_CUSTOM_BAUD automatically on specific OSes / versions / and architectures where we are certain it works. 

I do have some ideas on this, but let's keep things separate...
Hmm, thinking about how to rename `bsd.txt` as it not just contain only info about handshake lines but also for custom baudrate support.
Hmm, I don't know, maybe `bsd_notes.txt` (if it is bsd-specific)? 
Take a look at commit 8ca2e5a10fade43e5dad88b976d36269dfe4206c

It improves the build support for custom baudrates. On some systems, custom baudrate support gets enabled by default.

Tested on linux and seems to work. Do a quick test on a couple of the BSDs.

Thanks
Test environment:

- VirtualBox host Kubuntu 16.04
- FreeBsd 11.0 (VM)
- OpenBsd 6.2 (VM)
- NetBsd 7.1.1 (VM)
- Dragonfly 5.0.2 (VM)
- OSX 10.11.6 El Capitan (native on Macbook Pro)
- Kubuntu 16.04 (native on Macbook Pro)
- all above on Intel x86 64 Bit

Tested build with:

- make all
- CPPFLAGS=-DUSE_CUSTOM_BAUD make all
- CPPFLAGS=-DNO_CUSTOM_BAUD make all

Just tested build behaviour, not communication. Just checked for `picocom --help` and output of `USE_CUSTOM_BAUD is enabled`. All worked (build) as expected.

Default build `make all`:
Except of NetBsd, all targets build by default with custom baudrate enabled. NetBsd build with custom baudrate disabed by default.

Build with `USE_CUSTOM_BAUD` explicitely set:
NetBsd failed build with: `#error "USE_CUSTOM_BAUD not supported on this system!"`.
All others build with with custom baudrate enabled.

Build with `NO_CUSTOM_BAUD` explicitely set:
All targets build with no custom baudrate support.

Thanks! 

In 2fa6a1d7a2cb48746e845980a050e49f6a0e6773 I changed a bit the set of systems where `USE_CUSTOM_BAUD` is enabled by default. I try to be very conservative, so it is enabled by default only for:

- Linux, kernels versions > 2.6.0, x86 & x86_64 (principal platform, we know it works)
- FreeBSD, OpenBSD, DragonflyBSD (because there is really no special code there, so even if it may not always actually work, it will always compile and cause no problems when used with standard baudrate values---I think)

Also allowed NetBSD to be compiled with custom baudrate support (if `USE_CUSTOM_BAUDRATE` is defined explicitly). It may not currently work, but it is mostly an issue of driver / subsystem support and not an issue of interface (i.e. it may work on future releases).

Perhaps we should also enable by default for *specific versions* of OSX / macOS, but I don't like to do so for everything APPLE.

As we have not tested custom baudrate on iPhone, maybe something like this:

    #if defined __APPLE__
      #include <TargetConditionals.h>
      #if TARGET_OS_MAC
        ...
      #endif
    #endif

and even something from `AvailabilityMacros.h` to check against the OSX / macOS version. Seems that `MAC_OS_X_VERSION_MIN_REQUIRED` was made for this.
But I might check if it at least will _build_ for iPhone target.
Yes, a conditional on `TARGET_OS` and `MAC_OS_X_VERSION_MIN_REQUIRED` seems like a good idea...

Again, we do not need to enable-by-default on the widest range of systems. Only those we *know* it works and causes no problems.
