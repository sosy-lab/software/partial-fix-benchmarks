Added missing check to prevent crashes
QUIC: add basic support for fragmented Client Hello

Only in-order and non overlapping fragments are handled
See #1195
QUIC: add basic support for fragmented Client Hello (#1216)

Only in-order and non overlapping fragments are handled
See #1195
Trying to improve QUIC reassembler (#1195) (#1489)

* handling QUIC out-of-order fragments

* minor fix

* updated quic_frags_ch_out_of_order_same_packet_craziness.pcapng.out

* quic test: buf_len + last_pos

* QUIC: comment update in __reassemble function and minor change is_ch_complete function
QUIC: handle retransmissions and overlapping fragments in reassembler (#1195) (#1498)

* QUIC: handle retransmissions and overlapping fragments in reassembler

* Trigger CI

* minor fix: parentheses

* Changing ndpi_malloc to ndpi_calloc

* fix memory leak

* quic_reasm_buf calloc to malloc

* change order of is_ch_complete && is_reasm_buf_complete call

* is_reasm_buf_complete: added handling for case where frame size is not multiple of 8

* add extra check
