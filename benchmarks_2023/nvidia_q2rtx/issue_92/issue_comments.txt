Crash in monster_use due to null activator
Further testing seems to indicate this is generally related to the use of notarget mode.
@apanteleev Is it right this was closed? The cited commit doesn't appear to be related to the issue.
(But the commit message contains an issue number, probably for q2pro.)
No, this is GitHub thinking it's smart. Reopening.
Fixed in https://github.com/NVIDIA/Q2RTX/commit/9ea8b5fc7c8cd8c47f30da1438f23de4b8fa45a4, closing. Thanks!