How to add fonts to u8G2
None of the existing fonts is big enough?
I can try to render it even bigger, however there are some internal restrictions for the size of individual glyphs.

What about this:
![https://github.com/olikraus/u8g2/wiki/fntpic/u8g2_font_logisoso62_tn.png](https://github.com/olikraus/u8g2/wiki/fntpic/u8g2_font_logisoso62_tn.png)


of course you can generate the internal format from any bdf file. And bdf itself can be generated from any TTF.

Some information is here:
https://github.com/olikraus/u8g2/wiki/internal#ttf-to-bdf

The conversion tool (bdf to .c code) is here:
https://github.com/olikraus/u8g2/tree/master/tools/font/bdfconv

I do this in ubuntu only, but i guess you should be able to compile it also under windows.
Hi Oliver,

Nice font, any chance of going to maybe 78 high?

Kind regards

Gavin Brown

On 12 December 2016 at 22:35, olikraus <notifications@github.com> wrote:

> None of the existing fonts is big enough?
> I can try to render it even bigger, however there are some internal
> restrictions for the size of individual glyphs.
>
> What about this:
> [image:
> https://github.com/olikraus/u8g2/wiki/fntpic/u8g2_font_logisoso62_tn.png]
> <https://github.com/olikraus/u8g2/wiki/fntpic/u8g2_font_logisoso62_tn.png>
>
> of course you can generate the internal format from any bdf file. And bdf
> itself can be generated from any TTF.
>
> —
> You are receiving this because you authored the thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/olikraus/u8g2/issues/105#issuecomment-266406622>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AXScJzwKyLmrUZBml-p7aZ5XA9oYJAuBks5rHTFkgaJpZM4LKZTX>
> .
>

"-p 110" gives the desired 78 pixel height

```
	-../otf2bdf/otf2bdf -a -r 72 -p 110 -o tmp.bdf ../ttf/Logisoso.ttf
	./bdfconv -v -f 1 -m '32,42-57' tmp.bdf -o u8g2_font_logisoso_tn.c  -n u8g2_font_logisoso_tn -d ../bdf/helvB18.bdf 
```

```
/*
  Fontname: -FreeType-Logisoso-Medium-R-Normal--110-1100-72-72-P-127-ISO10646-1
  Copyright: Created by Mathieu Gabiot with FontForge 2.0 (http://fontforge.sf.net) - Brussels - 2009
  Glyphs: 17/527
  BBX Build Mode: 0
*/
const uint8_t u8g2_font_logisoso_tn[1294] U8G2_FONT_SECTION("u8g2_font_logisoso_tn") = 
  "\21\0\5\5\6\7\6\7\7-]\0\373N\354M\0\0\0\0\0\4\365 \7\0\0\4\312\1*B"
  "\241\10\265\307\333\241\317\203\303\71\316P\12s\230\202\234\15E\34B\21_\370\200 \272\262\241\213M/"
  "\202\321\233\330\205\266\322\211\17\10\341\23\212\70\204\62\16r\42\205\71LQ\206s\234\1\36\372\274\6\0"
  "+\24\235G}\306\225\62\325\277\364\201\377\7\26\245L\365/\1,\26\312#\335q\7A\216\201\204$"
  "t\2E\14\63\234\261#\14\0-\12\235B\315\306\301\377\7\26.\23\212\42\5r\7A\216\201\204$"
  "t\2\21G!\6\0/d\245\323\4\312\267\242WZ\321+\255\350\225V\364J+z\321\312V\364\242"
  "\225\255\350E+[\321\213V\364J+z\245\25\275\322\212^iE\257\264\242\27\255lE/Z\331\212"
  "^\264\262\25\275he+z\321\212^iE\257\264\242WZ\321\213V\266\242\27\255lE/Z\331\212"
  "^\264\262\25\275he+z\321\0\60V\247\263\4\306_\262\37;I\13fm[]\372\316\7\202\363"
  "\1\241\70\247)\314b\6\323\226\261\274E(Q\11KT\202\62\223\240L\372\377\377\377\377\63\11K"
  "T\204\22\25\261\274E\60m!\314b\210s\32\363\1\301| @_\352\326\326\262g\315\311>;y"
  "\0\61#\223\63\6\312\223:\264:,\323\24\207<\3\12\37\370\3P\200\206S\26\244$\365\377\377\377"
  "\377\377\377\3\62x\247\323\4\312_\262\37;I\13fm[]\362\320\7\202\363\1\241\70\247)\314b"
  "\6\323\226\261\274E(Q\11KT\202\42\225\240L\372IE(Q\21I\134\4\22\231Q\314\205\14\224"
  ")\1e\366\262\231\275\332\314^\66\243\231\275lF\63{\331\314^\66\243\231\275lF\63{\331\314^"
  "\66\243\231\275lF\63{\331\214f\66\243\231\275lF\63{\331\214f\366\7\376\377\200\4\63c\247\323"
  "\374\311\301\377? \355e\63\232\331\313f\64\263\227\315\354\325f\366j\63{\331\214f\366\262\31\315l"
  "f;\371\301\221\225\352D\251Y\311J;\233\341\14_\372\322U>\361JW\377\307\225\256 a*\246"
  "\210\13\71\340\62\226\306\210f\65\303\61\17\371\300`> \234/ukk\331\263\346t\33_H\0\64"
  "Z\247\263\4\312\233\322\325\270\322\325\270\322\325\270\322\325\270\322U|\341JW\361\205+]\305\27\256t"
  "\25W\272\32W\272\32W\214\322\24\243\64\305(M\61\12S\216\302\224\243\60\345(L\71\312R\220\262"
  "\24\244,\5)KA\212R\222\242\224\244\250\17\374\377\1i*]\375\12\0\65f\247\263\4\306\301"
  "\377?\300u\365\377W\222\247\210\207u\352C\37\10\316\7\4\363\201\241|\200 \37 \310\7\212\1\35"
  "G\70\213\21M[\4\363\226\260D%(R\11\312L\202\62\251\251t\365\377:\65i\63\11\212T\204"
  "\22\25\261\274E\60m!\314b\210s\234\362\1\301| @_\352\326\326\262w\311\311>;y\0\66"
  "\200\247\263\4\306\37\302\37;I\13fm[]\372\316\7\202\363\1\241\70\307!\314b\6\323\226\261\274"
  "E(Q\11KT\202\62\351\272\372\317 O\21\17\353\224\227>\20\234\17\10\346\3C\371\300P>@"
  "\220\17\24\3\62\316pT#\232\266\10\346-a\201KX\242\22\24\251\4e\322\377\237T\302\22\25\241"
  "DE,o\21L[\10\263\30\342\234\306|@\60\37\10\220\247\272\265\265\354Ys\262\317N\36\0\67"
  "\134\247\263\4\306\301\7\216\360\201\377\37\240\342\22\224\270\4%.A\201\213P\340\42\24\270\10\5.B"
  "y\313P\336\62\22\270\360e/|m/|m/|m/|\265\231\275\360\325^\370\332^\370\332^"
  "\370\332^\370\332^\370j\63{\341\253\315\354\205\257\366\302\327\366\302\327\366\302W\22\0\70\216\247\323\4"
  "\312_\262\37;I\13fm[]\372\316\7\202\363\1\241\70\247)\314b\6\323\226\261\274E(Q\11"
  "KT\202\42\225\240L\372\237T\302\22\25\241DE,o\21L[\10\263\30\342\234\306|@\60\37\10"
  "\320\227\272\265\261mu\311C\37\10\316\7\204\342\30\247\60\213\31L[\306\362\26\241D%,Q\11\212"
  "T\202\62\351R\11KT\204\22\25\261\274E\60m!\314b\210s\32\363\1\301| @_\352\326"
  "\326\262g\315\311>;y\0\71\201\247\263\4\306_\262\37;\311\353em[]\372\316\7\202\363\1\241"
  "\70\247)\314b\6\323\226\261\274E(Q\11KT\202\42\225\240L\372\377\223V\134\202\22\227\260<&"
  "\60\255\21\314r\204s\240\361\201b|\240 \37 \312\7\6\363\1\341| @_\362\26$\24\227\30"
  "\245\253\235\232\264\231\4E*B\211\212X\336\42\230\266\20f\61\304\71N\371\200`>\20\240/u"
  "kk\331\273\344d\237\235<\0\0\0\0";


```

i have also added this font to the u8g2 library.
Thanks Oliver, much appreciated.

On 14 December 2016 at 16:00, olikraus <notifications@github.com> wrote:

> i have also added this font to the u8g2 library.
>
> —
> You are receiving this because you authored the thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/olikraus/u8g2/issues/105#issuecomment-266943293>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AXScJ0kFzJpC1KXhypqnwqy3aqrAS2PRks5rH3f0gaJpZM4LKZTX>
> .
>

Hi Oliver,

Playing around with the new font. Looks good, except it looks like there
are a few ? chars been added to the file that affect formatting

Any chance of putting up a text file of the codes for the font please?



On 15 December 2016 at 07:01, Gavin Brown <gavin.brown@secureetran.com>
wrote:

> Thanks Oliver, much appreciated.
>
> On 14 December 2016 at 16:00, olikraus <notifications@github.com> wrote:
>
>> i have also added this font to the u8g2 library.
>>
>> —
>> You are receiving this because you authored the thread.
>> Reply to this email directly, view it on GitHub
>> <https://github.com/olikraus/u8g2/issues/105#issuecomment-266943293>, or mute
>> the thread
>> <https://github.com/notifications/unsubscribe-auth/AXScJ0kFzJpC1KXhypqnwqy3aqrAS2PRks5rH3f0gaJpZM4LKZTX>
>> .
>>
>
>

oh, i see the problem. Here it is. It still was there in my temp dir of my hard disk :-)

[u8g2_font_logisoso_tn.txt](https://github.com/olikraus/u8g2/files/658182/u8g2_font_logisoso_tn.txt)

Oliver, your wayyy tooo fast!!!

Thanks for being so supportive.

The new size is exactly what I needed.

Thanks a million!

Kind regards

Gavin Brown

On 17 December 2016 at 07:38, olikraus <notifications@github.com> wrote:

> u8g2_font_logisoso_tn.txt
> <https://github.com/olikraus/u8g2/files/658182/u8g2_font_logisoso_tn.txt>
>
> —
> You are receiving this because you authored the thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/olikraus/u8g2/issues/105#issuecomment-267690423>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AXScJ1hWzcMe7fVdxgpio5Qad1d-nR_sks5rIva9gaJpZM4LKZTX>
> .
>

:-)

todo: run font build
Please explain in more detail how to convert the font to .c code
A little bit is described in the FAQ. What exactly do you want to know?
I want to create my own font. I figured out that it is written in the file u8g2_fonts.c and u8g2.h. But I do not understand how to write font in c. code (convert).
For example:
const uint8_t u8g2_font_logisoso_tn[1294] U8G2_FONT_SECTION("u8g2_font_logisoso_tn") = 
  "\21\0\5\5\6\7\6\7\7-]\0\373N\354M\0\0\0\0\0\4\365 \7\0\0\4\312\1*B"
  "\241\10\265\307\333\241\317\203\303\71\316P\12s\230\202\234\15E\34B\21_\370\200 \272\262\241\213M/"
.......................................
.......................................
Your font must be available in Adobe BDF Format. The use bdfconv to create the c code. Alternatively you can attach the bdf file here and I do the conversion.
For font creation use fontforge or fony.exe. Fony.exe is a lightwight  windows font editor (http://hukka.ncn.fi/?fony).
I downloaded bdfconv.exe. At startup, the program window is immediately closed. I have Windows 7. How to use bdfconv.exe?
It requires commandline options as described in the FAQ: https://github.com/olikraus/u8g2/blob/master/doc/faq.txt

Mate this is not me.... Someone is impersonating my account.

On 12 May 2017 at 18:03, markot500 <notifications@github.com> wrote:

> Please explain in more detail how to convert the font to .c code
>
> —
> You are receiving this because you authored the thread.
> Reply to this email directly, view it on GitHub
> <https://github.com/olikraus/u8g2/issues/105#issuecomment-301011223>, or mute
> the thread
> <https://github.com/notifications/unsubscribe-auth/AXScJz9GwtKhgXOI_O9jYIfReSwcXV3-ks5r5BI0gaJpZM4LKZTX>
> .
>

@GavinBrownAus: Unfortunately @markot500 has reused this issue. Maybe you want to unsubscribe here.
Does not work. For the sample, I downloaded the font. At the command line wrote.

C:\Font\bdfconv.exe -f 1 -m '32-255' -n myfont -o C:\Font\myfont.c C:\Font\Arialbd.bdf

Got a file

/*
  Fontname: -FreeType-Arial-Bold-R-Normal--12-120-75-75-P-65-ISO8859-1
  Copyright: Typeface © The Monotype Corporation plc. Data © The Monotype Corporation plc/Type Solutions Inc. 1990-1992. All Rights Reserved
  Glyphs: 0/191
  BBX Build Mode: 0
*/
const uint8_t myfont[27] U8G2_FONT_SECTION("myfont") = 
  "\0\0\2\2\0\0\0\0\0\0\0\0\0\12\375\12\375\0\0\0\0\0\2\0\0\0";

What am I doing wrong. The font file is attached.
[Arialbd.txt](https://github.com/olikraus/u8g2/files/995859/Arialbd.txt)
Maybe bdfconv.exe is outdated or wrong. Where did you download bdfconv.exe?

The the same args from above the linux version of bdfconv create the attached c.code.

[myfont.txt](https://github.com/olikraus/u8g2/files/995878/myfont.txt)

I took it from here. https://github.com/olikraus/u8g2/tree/master/tools/font/bdfconv
Hmmm... I probably need to recreate the exe. 
I have updated bdfconv.exe.
Maybe you can test again.
I tried the new bdfconv.exe. Same. Does not want to translate to .c code.

const uint8_t myfont[27] U8G2_FONT_SECTION("myfont") = 
  "\0\0\2\2\0\0\0\0\0\314\320\324\330\12\375\12\375\0\0\0\0\0\2\0\0\0";

Maybe I'm writing the wrong command.

C:\Font\bdfconv.exe -f 1 -m '32-255' -n myfont -o C:\Font\myfont.c C:\Font\Arialbd.bdf
Strange. I have to analyse this. 
Tried on Windows 7 and Windows XP. The result is the same.
I think the problem are the single quotes. You must use:
C:\Font\bdfconv.exe -f 1 -m "32-255" -n myfont -o C:\Font\myfont.c C:\Font\Arialbd.bdf

Happened! Thank you!
But why in your FAQ ' ' but not " "?
on linux ' works fine.
Once again many thanks. Please make a note in the FAQ for Windows.
 olikraus, tell me please, how to translate font Cyrillic into c. code? What command line?
You want to use a external cyrillic font? U8g2 already includes some fonts, which could be used directly.

I figured it out. Instead of 255, write 1251.
I need a font with a height of 8 pixels. Only the letters need. That would take less memory.
I am converting a ardunio sketch from OpenGLCD and have a font file from OpenGLCD can I install this into Uu8g2.  Copied pasted the font file below.
Help greatly appreciated.
Regards
MakkyB



/*
 *
 * cou32
 *
 * created with FontCreator
 * written by F. Maximilian Thiele
 *
 * http://www.apetech.de/fontCreator
 * me@apetech.de
 *
 * File Name           : cou32.h
 * Date                : 15.02.2014
 * Font size in bytes  : 3522
 * Font width          : 10
 * Font height         : 24
 * Font first char     : 46
 * Font last char      : 58
 * Font used chars     : 12
 *
 * The font data are defined as
 *
 * struct _FONT_ {
 *     uint16_t   font_Size_in_Bytes_over_all_included_Size_it_self;
 *     uint8_t    font_Width_in_Pixel_for_fixed_drawing;
 *     uint8_t    font_Height_in_Pixel_for_all_characters;
 *     unit8_t    font_First_Char;
 *     uint8_t    font_Char_Count;
 *
 *     uint8_t    font_Char_Widths[font_Last_Char - font_First_Char +1];
 *                  // for each character the separate width in pixels,
 *                  // characters < 128 have an implicit virtual right empty row
 *
 *     uint8_t    font_data[];
 *                  // bit field of all characters
 */

#include <inttypes.h>
#include <avr/pgmspace.h>

#ifndef COU32_H
#define COU32_H

#define COU32_WIDTH 10
#define COU32_HEIGHT 24

GLCDFONTDECL(cou32) = {
    0x0D, 0xC2, // size
    0x0A, // width
    0x18, // height
    0x2E, // first char
    0x0C, // char count
    
    // char widths
    0x06, 0x0C, 0x0D, 0x0C, 0x0C, 0x0D, 0x0D, 0x0D, 0x0D, 0x0D, 
    0x0D, 0x0D, 
    
    // font data
    0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x1C, 0x3E, 0x3E, 0x1C, 0x00, // 46
    0x00, 0xF8, 0xFC, 0x8E, 0x06, 0x06, 0x06, 0x8E, 0xFC, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x03, 0x03, 0x03, 0x03, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, // 47
    0x00, 0xC0, 0xF0, 0x78, 0x1C, 0x0C, 0x0C, 0x0C, 0x1C, 0x38, 0xF0, 0xE0, 0x00, 0x7E, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0xFF, 0x00, 0x07, 0x0F, 0x1E, 0x38, 0x30, 0x30, 0x30, 0x38, 0x3C, 0x1F, 0x07, 0x01, // 48
    0x10, 0x18, 0x18, 0x18, 0x1C, 0xFC, 0xFC, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xFF, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x30, 0x30, 0x30, 0x30, 0x30, 0x3F, 0x3F, 0x30, 0x30, 0x30, 0x30, 0x30, // 49
    0x30, 0x78, 0x38, 0x1C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1C, 0x38, 0xF8, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x80, 0xC0, 0xE0, 0x70, 0x3C, 0x1E, 0x0F, 0x03, 0x38, 0x3C, 0x3E, 0x3F, 0x33, 0x31, 0x30, 0x30, 0x30, 0x30, 0x38, 0x3C, // 50
    0x00, 0x10, 0x18, 0x1C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1C, 0x78, 0xF8, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x08, 0x1C, 0x1C, 0x1E, 0x3F, 0xF3, 0xE1, 0xC0, 0x18, 0x18, 0x38, 0x38, 0x30, 0x30, 0x30, 0x30, 0x38, 0x3C, 0x1F, 0x0F, 0x03, // 51
    0x00, 0x00, 0x00, 0x00, 0x80, 0xE0, 0x78, 0x1C, 0xFC, 0xFC, 0x00, 0x00, 0x00, 0xE0, 0xF0, 0xBC, 0x8F, 0x83, 0x81, 0x80, 0x80, 0xFF, 0xFF, 0x80, 0x80, 0x80, 0x01, 0x01, 0x01, 0x01, 0x01, 0x31, 0x31, 0x31, 0x3F, 0x3F, 0x31, 0x31, 0x00, // 52
    0x00, 0xFC, 0xFC, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x08, 0x00, 0x00, 0x0F, 0x0F, 0x0C, 0x0E, 0x06, 0x06, 0x06, 0x0E, 0x1C, 0xFC, 0xF8, 0xE0, 0x18, 0x38, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x38, 0x3C, 0x1F, 0x0F, 0x03, // 53
    0x00, 0x00, 0xC0, 0xF0, 0x70, 0x38, 0x1C, 0x1C, 0x0C, 0x0C, 0x0C, 0x0C, 0x00, 0xF8, 0xFF, 0xFF, 0x39, 0x1C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1C, 0xF8, 0xF0, 0xC0, 0x00, 0x07, 0x0F, 0x1C, 0x38, 0x30, 0x30, 0x30, 0x30, 0x38, 0x1F, 0x0F, 0x03, // 54
    0x38, 0x3C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x1C, 0x9C, 0xFC, 0xFC, 0x08, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xF0, 0xFC, 0x3F, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x20, 0x3C, 0x3F, 0x0F, 0x01, 0x00, 0x00, 0x00, 0x00, // 55
    0x00, 0xF0, 0xF8, 0x18, 0x0C, 0x0C, 0x0C, 0x0C, 0x0C, 0x1C, 0xF8, 0xF0, 0xC0, 0x80, 0xE3, 0xE7, 0x7E, 0x3C, 0x18, 0x18, 0x18, 0x3C, 0x3E, 0xE7, 0xE7, 0x80, 0x03, 0x0F, 0x1F, 0x38, 0x30, 0x30, 0x30, 0x30, 0x30, 0x38, 0x1F, 0x0F, 0x07, // 56
    0xC0, 0xF0, 0xF8, 0x3C, 0x1C, 0x0C, 0x0C, 0x0C, 0x1C, 0x78, 0xF0, 0xE0, 0x00, 0x03, 0x0F, 0x1F, 0x38, 0x30, 0x70, 0x70, 0x30, 0x30, 0x9C, 0xFF, 0xFF, 0x1F, 0x00, 0x20, 0x30, 0x30, 0x30, 0x30, 0x38, 0x1C, 0x1E, 0x0F, 0x07, 0x01, 0x00 // 57
    
};

#endif



Can you export / provide the font as BDF file? Otherwise porting the font is not possible.
Hi Oliver, 

I would like to ask you for create a font what includes all of the special Hungarian characters.
Like:  "Ő,Ű,Ö,Ü,Í,Á,É "
I have checked all of the fonts in the u8g2 but non of the includes this character = Ű ű 
It woud be wery nice to have this char.
Can you help me in this?? 
Thanks in advance. 



This are codes &#x0170; and &#x0171;
Yes, 
oh,  it got converted to glyphs automatically... ok, codes hex 170 and 171. i will create anew issue for this.

The fonts are included in the .1 prerelease:
https://github.com/olikraus/U8g2_Arduino/archive/master.zip
Hi Oliver,

Thanks for all the work to create the u8g2 library. I have created a TTF font (digits, period and colon, only) and converted it to C. Am reading through these comments and your FAQs on how to add the font to u8g2. 

Could you verify the steps? 
1) convert to BDF 
2) Use BDF converter (with the correct command line) to convert to u8g2. 
3) Paste the results into the .ino sketch (in setup? or the library?). 
4) To call the font,  u8g2.setFont(u8g2_font_9x15_mn); //with the correct title

You mentioned in an earlier post that you could convert a BDF file. I don't have access to a Windows computer, however, I have converted the font to BDF--would it be possible for you to do the C (u8g2) conversion?

The font is 0-9, a period and a colon.
[Ford_Speedometer-25.bdf.txt](https://github.com/olikraus/u8g2/files/1050062/Ford_Speedometer-25.bdf.txt)


bdfconv is a linux and windows commandline tool.The source will nicely compile on any linux machine.
You can paste the code into any .ino or .c/.cpp file. The code is just a plain c array.


[Ford_Speedometer-25.c.txt](https://github.com/olikraus/u8g2/files/1050162/Ford_Speedometer-25.c.txt)
![bdf](https://cloud.githubusercontent.com/assets/1084992/26759656/5beaf6fa-4904-11e7-961e-beb966c68e83.png)

Looks like the 8 is a little bit to high.

ok, i fixed the "8" in the bdf file...

![bdf](https://cloud.githubusercontent.com/assets/1084992/26759667/ec89ec8e-4904-11e7-9d67-a9f54d5589e9.png)

I think this looks better.

For reference: 
./bdfconv -v -f 1 Ford_Speedometer-25.bdf.txt -o Ford_Speedometer-25.c.txt -n ford_speedometer_25 -d ../bdf/helvB18.bdf
[Ford_Speedometer-25.c.txt](https://github.com/olikraus/u8g2/files/1050165/Ford_Speedometer-25.c.txt)
[Ford_Speedometer-25.bdf.txt](https://github.com/olikraus/u8g2/files/1050166/Ford_Speedometer-25.bdf.txt)

Thanks! And thanks for fixing the 8. 
This should be germain to the thread: 
How does one compile the bdfconf.exe file in linux--specifically, the ubuntu terminal running on 64 bit architecture?
I got the following error:

arminjahr@ubuntu:~$ ./bdfconv.exe
bash: ./bdfconv.exe: cannot execute binary file: Exec format error

I did some checking and found the following information:
arminjahr@ubuntu:~$ file bdfconv.exe
bdfconv.exe: PE32 executable (console) Intel 80386, for MS Windows

arminjahr@ubuntu:~$ uname -mo
x86_64 GNU/Linux

arminjahr@ubuntu:~$ uname -a
Linux ubuntu 4.8.0-52-generic #55~16.04.1-Ubuntu SMP Fri Apr 28 14:36:29 UTC 2017 x86_64 x86_64 x86_64 GNU/Linux

Looks like the file runs on 32 bit architecture while my version of ubuntu runs on 64 bit. I've installed sudo apt-get install gcc-multilib g++-multilib to work with 32 bit, however, I am uncertain as to what to do next.
bdfconv.exe is indeed the windows executable. On linux, you have to type "make" and then execute "./bdfconv" (without .exe)
I want to pass on that bdfconv works on a mac platform running parallels with Windows 10.  The trickiest part was getting the files to be visible to Windows.

On the Win/Mac platform, I found this was the easiest when the files were located in the shared OneDrive folder. I used the command (from the FAQ), c:\Users\arminjahr\OneDrive>start bdfconv.exe -f 1 -m "32-255" -n MyFontName -o MyC_output.c MyBDF_input.bdf and everything worked perfectly. When the process was finished, there was a new file with the .c extension in OneDrive. I opened it in a text editor, copied and pasted the contents into the .ino sketch, and celebrated when the new font showed up on my OLED screen.

Best part about the conversion process... the brilliant white flash that occurs when the font is compiled. 

A separate question: How does one design a font style to be transparent or opaque? I've noticed that some are, by default, opaque (e.g. u8g2_xxx_mx) and others are transparent (e.g. u8g2_xxx_tx). What do you set within the font family to make this difference? (I see that within the sketch, the u8g2_SetFontMode command can be used to make fonts transparent or opaque)

Thanks for all of your work, detailed documentation and help! Now I can work on different point sizes for the vintage Ford font family!

Thanks again,
Armin Jahr
Font render modes: All font types (tx, mx, hx) will work in both font render modes (solid/transparent). However tx will have some artefacts in solid mode and mx/hx will waste flashrom space in transparent mode. The difference is, that tx fonts do not contain the complete background information, which saves space.
The type of font is selected with the -b option in bdfconv:
-b 0  tx
-b 1  hx
-b 2  mx


Ah, I see. Add the appropriate flag -b 1 (2, or 3) to the bdfconv command line, compile the code and enjoy the results. It worked perfectly. Thx! Now that everything is working, I'll stop bugging you :) When I finish the font, I'll let you know. So far, I have a 25 point and a 9 point version of the numerals. I'll be working on the alphabet in the days ahead. It is based on a narrow, blocky "art deco" font that was used on the 1949-1951 vintage Ford F-1 dash boards. 
If you want to make your font public, then I can also add your font to u8g2... 
Hello olikraus.

I have a problem with adding a font.
1) converted the .otf font to .ttf format then to .bdf using fony
2) .bdf font using "bdfconv" converted to ".c"
command "bdfconv -f 1 -m" 32-255 "-n u8g2_font_gotthard -o gotthard.c gotthard.bdf" (using windows)
3) the resulting ".c" file was opened with notepad, copied the contents to the end of the file "... / u8g2 / src / clib / u8g2_fonts.c" (also opened with notepad), saved the changes.

Nothing works :(
Arduino IDE doesn't compile line of code:
"u8g2.setFont (u8g2_font_gotthard);"
Throwing out an error:
"exit status 1
'u8g2_font_gotthard' was not declared in this scope "

Please tell me what am I doing wrong?

my files: https://yadi.sk/d/hQweuNe3oERAjA?w=1
Copy the font c code at the beginning of your .ino file instead. Will this help?
> Copy the font c code at the beginning of your .ino file instead. Will this help?

Yes, it works. So simple. Thank You.

![2020-10-16 16-34-55](https://user-images.githubusercontent.com/72743272/96239975-eb4a5c80-0fd2-11eb-8682-1c4e8719baf8.JPG)

Wow, looks great. Nice :-)


Reviving this thread, because this is about where I'm stuck, too.
I have my font.c file, in the same directory as the .ino, created by bdfconv compiled on Linux.
Here the font is declared as an uint8_t type (which is byte type as declared in Arduino.h, which is also included in the sketch)
I include it with #include "myfont.c"

However, on compilation of the ino I get a type error:
sketch/SairaCondensed_Regular_16.c:7:7: error: unknown type name 'uint8_t'
 const uint8_t SairaCondensed_Regular_16[2451] U8G2_FONT_SECTION("SairaCondensed_Regular_16") = 
       ^
sketch/SairaCondensed_Regular_16.c:7:47: error: expected '=', ',', ';', 'asm' or '__attribute__' before 'U8G2_FONT_SECTION'
 const uint8_t SairaCondensed_Regular_16[2451] U8G2_FONT_SECTION("SairaCondensed_Regular_16") = 

I thought Arduino.h took care of this declaration. Any ideas?

Thanks!
You should also include "U8g2lib.h".
Hello Oli,

That is obviously included, right after Arduino.h. Otherwise the display wouldn't work at all, would it?
The library works great with the included fonts (I'm using an SPI-connected SSD1309 on an ESP32 board)
My display declaration is this, and I'm currently using the "Hello World" example to keep things simple.

> U8G2_SSD1309_128X64_NONAME0_F_4W_SW_SPI u8g2(U8G2_R0, /* clock=*/ 18, /* data=*/ 23, /* cs=*/ 22, /* dc=*/ 21, /* reset=*/ 19); 

It's just the use of my own font that's causing the error when I try to include the .c-file. To squeeze a few more characters into the display I need to use a condensed font, that's why I want to go with my own.
I think I figured it out, it works if I add "#include <u8g2.h>" to the .c-file itself.
On another note, a lot of really nice ttf-fonts look very ugly when rasterized and scaled to 16pt or something ;-)
Hello Olikraus,
There are several fonts that I like, but they are from the previous version of u8glib. They look like:
const u8g_fntpgm_uint8_t my5x7rus[2004] U8G_FONT_SECTION(".progmem.my5x7rus") = {
  0,6,6,0,255,7,1,147,3,35,32,255,255,6,255,7,
  255,0,0,0,4,0,0,1,7,7,2,0,255,128,128,128,...
I do not have the original .bdf file. There are only .h files Is it possible to convert fonts from u8glib to u8g2?
> I think I figured it out, it works if I add "#include <u8g2.h>" to the .c-file itself.
> On another note, a lot of really nice ttf-fonts look very ugly when rasterized and scaled to 16pt or something ;-)

Conversation is done by otf2bdf. I decided not to touch any results.
> Hello Olikraus,
> There are several fonts that I like, but they are from the previous version of u8glib. They look like:
> const u8g_fntpgm_uint8_t my5x7rus[2004] U8G_FONT_SECTION(".progmem.my5x7rus") = {
> 0,6,6,0,255,7,1,147,3,35,32,255,255,6,255,7,
> 255,0,0,0,4,0,0,1,7,7,2,0,255,128,128,128,...
> I do not have the original .bdf file. There are only .h files Is it possible to convert fonts from u8glib to u8g2?

I always require the bdf file.
> Conversation is done by otf2bdf. I decided not to touch any results.

Oh, I was just mentioning a fact. No offense meant, none at all!

Got it. I will use other fonts. Thanks!
> > Conversation is done by otf2bdf. I decided not to touch any results.
> 
> Oh, I was just mentioning a fact. No offense meant, none at all!

No problem. 