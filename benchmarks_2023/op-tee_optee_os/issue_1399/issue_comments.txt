link static library for TA
Hi @yuhsien-chen,
Your TA is trying to write a 64-bit value to a memory address that is not 8-byte aligned. Try re-building your static library with `-mno-unaligned-access` if you can.
I don't have the source code for the library, so I can't re-build it.
Very appreciate for your reply. 
I'll try other way to do it. Thanks.
I was updated the version of static library(vendor provided), and I encountered another problem.
When my TA call functions in the static library, 
the Trust OS show message "TEE-CORE: Unknown relocation type 257", and the CA received an error code "0xffff0005" means "TEEC_ERROR_BAD_FORMAT".
I found that code in /optee_os/core/arch/arm/kernel/elf_load.c.
What's the message means?
Thanks.
Relocation types are described in http://infocenter.arm.com/help/topic/com.arm.doc.ihi0056b/IHI0056B_aaelf64.pdf
In this case it's a `R_AARCH64_ABS64`. I'm not sure what code would result in this. I haven't run across it in our TAs. Possibly it's due to how that library is compiled.

It shouldn't be very hard to implement, considering that we support `R_ARM_ABS32` for 32-bit ELFs.
~@yuhsien-chen could you try this patch, please? https://github.com/jforissier/optee_os/commit/74e20a1a8dc43981e7b3e6b03a9ebc29a18e95f5
As I said in the commit message, it's completely untested, but if you can confirm it works for you then I will create a pull request.~ **Update** 29-Mar-2017: this patch is incorrect, see https://github.com/OP-TEE/optee_os/pull/1419 for the (hopefully) correct version.

Hi @jforissier ,
It works for me to execute my TA, even it going to "alignment fault", but your patch addressed "R_AARCH64_ABS64 not support" problem, thanks!
I'll try other way to address the "alignment fault". Thanks.
https://github.com/OP-TEE/optee_os/pull/1419
I implemented a simple library to cause alignment fault.
And I tried build with "-mno-unaligned-access" option, got
"error: unrecognized command line option '-mno-alignment-access".
I found it was the gcc option for ARM, and the option for ARM64 is "-mstrict-align".
But I built with it, and still get alignment fault.
Any advice will be appreciate.

Below is the simple library code.

libMM.h
```c
#ifndef _LIB_MM_H
#define _LIB_MM_H

int test_unalign_access2(uint8_t a);

#endif
```

libMM.c
```c
#include <stdint.h>

#include "libMM.h"

int test_unalign_access2(uint8_t a)
{
	uint32_t* pMyPointer = (uint32_t*)(&a);

	*pMyPointer += 2;

	return *pMyPointer;
}
```

makefile
```make
ANDROID_NDK_BIN := /tmp/my-android-toolchain/arm64/bin

CC := $(ANDROID_NDK_BIN)/aarch64-linux-android-gcc
CPP := $(ANDROID_NDK_BIN)/aarch64-linux-android-g++
AR := $(ANDROID_NDK_BIN)/aarch64-linux-android-ar

CFLAGS := -I../include -I/tmp/my-android-toolchain/arm64/sysroot/usr/include
CFLAGS += -static -Wall -O3 -mstrict-align

LIBS := -L/tmp/my-android-toolchain/arm64/sysroot/usr/lib

# Rule for make c++ object file
.cpp.o:
	$(CPP) $(CFLAGS) $(LIBS) -c $<

# Rule for make c object file
.c.o:
	$(CC) $(CFLAGS) $(LIBS) -c $<

STATIC_LIB_SRC = libMM.c
STATIC_LIB_OBJ = libMM.o
OUT = ../lib/arm64/libMM.a
$(OUT): $(STATIC_LIB_OBJ)
	$(AR) rcs $(OUT) $(STATIC_LIB_OBJ)
```
Shouldn't it be `-no-mstrict-align`?
I had tried the options list below.
-mno-strict-align => compiler report "unrecognized command line option '-mno-strict-align'"
-mstrict-align => compile ok, but still get alignment fault in execution.
-no-mstrict-align => compiler report "unrecognized command line option '-no-mstrict-align'"
So it's `-mno-strict-align` then. But it may not help in this case, at least not from what the man page says:
```
       -mstrict-align
           Do not assume that unaligned memory references are handled by the
           system.
```

It's the hardware catching the unaligned access. See the A bit in SCTLR_EL1 in the ARMv8 reference manual.
I had download manual and search the content with "SCTLR_EL1".
I got 
The exception masking bits
 - D  Debug exception mask bit. When EL0 is enabled to modify the mask bits, this bit is
     visible and can be modified. However, this bit is architecturally ignored at EL0.
- A SError interrupt mask bit.
- I   IRQ interrupt mask bit.
- F  FIQ interrupt mask bit.

For each bit, the values are:
- 0  Exception not masked.
- 1  Exception masked.

Does the A bit means "A SError interrupt mask bit."?
Can I modify the A bit value in optee_os?
My develop environment is AOSP 7.1.1 with optee 2.3.0
Search further, the bits you found are from CPSR/SPSR...
@jenswi-linaro

I found the exactly one.
It's in B2.4 Aligmment Support.
The A bit of SCTLR_EL1 seems to be set in arm-trusted-firmware(u-boot).
I'll try to modify it.
Thanks!
I tried reset value in "arm-trusted-firmware/bl1/aarch64/bl1_entrypoint.S".
```assembly
        /* ---------------------------------------------
	 * Enable the instruction cache, stack pointer
	 * and data access alignment checks
	 * ---------------------------------------------
	 */
	mov	x1, #(SCTLR_I_BIT | SCTLR_A_BIT | SCTLR_SA_BIT)
	mrs	x0, sctlr_el3
	//orr	x0, x0, x1
        // reset values
        bic    x0, x0, x1
	msr	sctlr_el3, x0
	isb
```
Bit it still catch the align access, and report exception.
Did I look for the wrong direction?
Or the SCTLR_EL1.A bit shouldn't be modified?
Thanks.
Finally, I found "optee_os/core/arch/arm/kernel/generic_entry_a64.S"
```assembly
        mrs	x0, sctlr_el1
	mov	x1, #(SCTLR_I | SCTLR_A | SCTLR_SA)
	//orr	x0, x0, x1
        // reset values
	bic x0, x0, x1
	msr	sctlr_el1, x0
	isb
```
It works!
I tried my static library, and it works too.
Thanks for your help.
Very appreciate.
@jenswi-linaro @yuhsien-chen do you think we should make this configurable at build time?  Such as, in `core/arch/arm/arm.mk`:
```sh
# Allow SEL0/SEL1 unaligned access?
CFG_UNALIGNED_ACCESS=n
```
@jforissier sounds good, we need to be careful with how it's enabled, not all platforms may support this.
@jforissier it would be good for users.
But I agree @jenswi-linaro 's opinion. 
When I search relative data for "unaligned access", I found some platform does not support it.
Even if I currently disabled the config and makes my static library call works.
But It still occurs alignment fault exception sometimes.
So, I think the good way is to fix static library, to avoid unaligned access.
Thanks for your help. :)
OK, thanks for your feedback.
Hi,

I am using HiKey 8GB board with AOSP + OP-TEE.
I am trying to load a TA from an Android application in AOSP Permissive mode. It successfully loads the TA but getting failed at TEEC_OpenSession with error code 0xffff0005 (TEEC_ERROR_BAD_FORMAT).

Just before the TEEC_OpenSession TEE output log shows that  **Unknown relocation type 257**.

I've pulled the latest version of optee_os and optee_test repositories. 
AOSP 7.1.1
Kernel Version: 4.9.10

Here is the debug log:
```
ERROR:   TEE-CORE: Unknown relocation type 257
03-28 09:37:18.557  2720  2720 D log   : TEEC_OpenSession failed: 0xffff0005
03-28 09:37:18.560  2720  2720 F libc    : Fatal signal 11 (SIGSEGV), code 1, fault addr 0x0 in tid 2720 (test.app)
03-28 09:37:18.693  2936  2936 I crash_dump64: obtaining output fd from tombstoned
03-28 09:37:18.695  1877  1877 I /system/bin/tombstoned: received crash request for pid 2720
03-28 09:37:18.700  2936  2936 I crash_dump64: performing dump of process 2720 (target tid = 2720)
03-28 09:37:18.701  2936  2936 F DEBUG   : *** *** *** *** *** *** *** *** *** *** *** *** *** *** *** ***
03-28 09:37:18.701  2936  2936 F DEBUG   : Build fingerprint: 'Android/hikey/hikey:7.1.1/NYC/alican03280747:userdebug/test-keys'
03-28 09:37:18.701  2936  2936 F DEBUG   : Revision: '0'
03-28 09:37:18.701  2936  2936 F DEBUG   : ABI: 'arm64'
03-28 09:37:18.701  2936  2936 F DEBUG   : pid: 2720, tid: 2720, name: test.app  >>> org.test.app <<<
03-28 09:37:18.701  2936  2936 F DEBUG   : signal 11 (SIGSEGV), code 1 (SEGV_MAPERR), fault addr 0x0
03-28 09:37:18.701  2936  2936 F DEBUG   : Cause: null pointer dereference
03-28 09:37:18.701  2936  2936 F DEBUG   :     x0   0000000000000000  x1   000000008010a403  x2   0000007fffffa370  x3   0000007fffffa410
03-28 09:37:18.701  2936  2936 F DEBUG   :     x4   0000007fffffa440  x5   006869746166ffff  x6   0000000000000000  x7   0000000000000080
03-28 09:37:18.701  2936  2936 F DEBUG   :     x8   0000000000000000  x9   0000007f957a82c8  x10  0000000000000002  x11  0000000000000000
03-28 09:37:18.701  2936  2936 F DEBUG   :     x12  0101010101010101  x13  0000000000000038  x14  ffffffffffffffff  x15  000015c9ccaaa1be
03-28 09:37:18.701  2936  2936 F DEBUG   :     x16  0000007f957aafa0  x17  0000007fb781e6f0  x18  0000000000000020  x19  0000000000000000
03-28 09:37:18.701  2936  2936 F DEBUG   :     x20  0000007fffffa558  x21  0000000000000000  x22  0000007f95686008  x23  0000007fffffa458
03-28 09:37:18.701  2936  2936 F DEBUG   :     x24  0000000000000004  x25  0000007fb4496a98  x26  0000000000000000  x27  0000000000000000
03-28 09:37:18.702  2936  2936 F DEBUG   :     x28  0000000000000001  x29  0000007fffffa510  x30  0000007f957a86dc
03-28 09:37:18.702  2936  2936 F DEBUG   :     sp   0000007fffffa370  pc   0000007f957a870c  pstate 0000000060000000
03-28 09:37:18.707  2936  2936 F DEBUG   : 
03-28 09:37:18.707  2936  2936 F DEBUG   : backtrace:
03-28 09:37:18.707  2936  2936 F DEBUG   :     #00 pc 000000000000170c  /system/app/Test/test.apk (offset 0x2c5e000)
03-28 09:37:18.707  2936  2936 F DEBUG   :     #01 pc 0000000000002784  /system/app/Test/test.apk (offset 0x2c55000)
03-28 09:37:18.707  2936  2936 F DEBUG   :     #02 pc 0000000001a97bbc  /system/app/Test/oat/arm64/Test.odex (offset 0x933000)

```

Thank you so much for your time.
@acalb, I think this has been recently fixed: see https://github.com/OP-TEE/optee_os/pull/1419, related to issue https://github.com/OP-TEE/optee_os/issues/1399.
@etienne-lms, I see that fixes and pulled the latest commits of optee_os repository, however it still gives the same error.
@acalb, that's strange: try to rebuild optee-os from scratch. Maybe the fix fails on your relocation but at least you should not get the trace "ERROR:   TEE-CORE: Unknown relocation type 257".
Agreed, I fail to see how you can still have the "Unknown relocation" trace if you are running the correct version. @acalb, can you please make sure the correct TEE binary is loaded by checking the "Initializing" trace? It looks like this:
```
INFO:    TEE-CORE: Initializing (2.3.0-150-gb96f67d #3 Tue Mar 28 08:12:44 UTC 2017 aarch64)
```
The version/commit ID and the build date should be enough to know for sure if it's indeed the expected binary that's running.
@acalb Did you rebuild optee_os with arm-trusted-firmware?

In Hikey, optee_os is built into fip.bin and you should replace by fastboot.
```
$ sudo fastboot flash fastboot fip.bin
```
@acalb optee_os/fip.bin is built separately from the main aosp sources so you'll need to rebuild and flash it again as advised by all the good people above.
@jforissier, @yuhsien-chen, @vchong, @etienne-lms  thanks for your answers. I've rebuild fip.bin and flashed it again. Now it works as expected.