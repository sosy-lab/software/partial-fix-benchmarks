x86: don't leak proc infos on failure to detect
x86: disable and warn under Valgrind

CPUID is emulated under Valgrind, we always get the same results
independently from the binding we requested.
Warn and disable the x86 in that case, except if we're loading
a cpuid dump (which is one workaround for this issue).

Fix #145.
x86: disable and warn under Valgrind

CPUID is emulated under Valgrind, we always get the same results
independently from the binding we requested.
Warn and disable the x86 in that case.

Fix #145.

(cherry picked from commit 3727fa491ba25002ef862916568e2787cb2ddd18)
Allow free(NULL) everywhere (except in netloc yet)

Refs #145.
netloc: Allow free(NULL) everywhere, and remove useless assignments to NULL

Refs #145.
Allow free(NULL) everywhere (except in netloc yet)

We used to say "avoid free(NULL) because some systems didn't support it".
But we didn't apply that rule everywhere. Since the very first release,
we free(obj->name) for all objects during hwloc_topology_destroy().
But obj->name is NULL for pretty-much all CPU-side objects.
So it looks like free(NULL) works fine on all platforms that ever
ran hwloc once.

Fixes #145.
netloc: Allow free(NULL) everywhere, and remove useless assignments to NULL

netloc's part of commit 815ca6c46f64a618aaaded4edfdda5ea3226fd7a

Fixes #145.
