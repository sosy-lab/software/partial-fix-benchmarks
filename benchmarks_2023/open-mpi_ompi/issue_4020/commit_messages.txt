Merge pull request #4014 from bosilca/topic/treematch

Topic/treematch
mca/registry: fir problem group_component_register

Turns out that supplying NULL group_register in the
mca_base_var_group_component_register is not a good
idea if one wants for ompi_info to work as intended.

The ugni and vader btl's both cause this before
registering component variables.  This borks up
the ompi_info works since NULL is supplied as the project
name.  So, now supply the project name rather than
just NULL to group register.

Fixes #4020.

Signed-off-by: Howard Pritchard <howardp@lanl.gov>
mca/registry: fix problem group_component_register

Turns out that supplying NULL to group_register in the
mca_base_var_group_component_register is not a good
idea if one wants for ompi_info to work as intended.

The ugni and vader btl's both call this before
registering component variables.  This borks up
the ompi_info works since NULL is supplied as the project
name.  So, now supply the project name rather than
just NULL to group register.

Fixes #4020.

Signed-off-by: Howard Pritchard <howardp@lanl.gov>
mca/registry: fix problem group_component_register

Turns out that supplying NULL to group_register in the
mca_base_var_group_component_register is not a good
idea if one wants for ompi_info to work as intended.

The ugni and vader btl's both call this before
registering component variables.  This borks up
the ompi_info works since NULL is supplied as the project
name.  So, now supply the project name rather than
just NULL to group register.

Fixes #4020.

Signed-off-by: Howard Pritchard <howardp@lanl.gov>
mca/registry: fix problem group_component_register

Turns out that supplying NULL to group_register in the
mca_base_var_group_component_register is not a good
idea if one wants for ompi_info to work as intended.

The ugni and vader btl's both call this before
registering component variables.  This borks up
the ompi_info works since NULL is supplied as the project
name.  So, now supply the project name rather than
just NULL to group register.

Fixes #4020.

Signed-off-by: Howard Pritchard <howardp@lanl.gov>
(cherry picked from commit 55774d13907ce002185bb97525faa6300e0910b5)
mca/registry: fix problem group_component_register

Turns out that supplying NULL to group_register in the
mca_base_var_group_component_register is not a good
idea if one wants for ompi_info to work as intended.

The ugni and vader btl's both call this before
registering component variables.  This borks up
the ompi_info works since NULL is supplied as the project
name.  So, now supply the project name rather than
just NULL to group register.

Fixes #4020.

Signed-off-by: Howard Pritchard <howardp@lanl.gov>
(cherry picked from commit 55774d13907ce002185bb97525faa6300e0910b5)
