Merge pull request #8545 from shintaro-iwasaki/topic/sharedfp-sm-configure-fix

sharedfp/sm: fix configure
mtl/psm2: provide option to disable psm2 REFCNT check

There are cases where customers may have systems where they don't want to upgrade
lower level system software like PSM2 and now can't install OMPI 4.1 and newer
and still have PSM2 MTL support.

This PR provides a way for knowledgable admins to disable the PSM2 version
checking.

related to issue #8381

Signed-off-by: Howard Pritchard <hppritcha@gmail.com>
mtl/psm2: provide option to disable psm2 REFCNT check

There are cases where customers may have systems where they don't want to upgrade
lower level system software like PSM2 and now can't install OMPI 4.1 and newer
and still have PSM2 MTL support.

This PR provides a way for knowledgable admins to disable the PSM2 version
checking.

related to issue #8381

Signed-off-by: Howard Pritchard <hppritcha@gmail.com>
mtl/psm2: provide option to disable psm2 REFCNT check

There are cases where customers may have systems where they don't want to upgrade
lower level system software like PSM2 and now can't install OMPI 4.1 and newer
and still have PSM2 MTL support.

This PR provides a way for knowledgable admins to disable the PSM2 version
checking.

related to issue #8381

Signed-off-by: Howard Pritchard <hppritcha@gmail.com>
(cherry picked from commit 6e04f3decd20c16f90b41c6818cf566cb583ce07)
mtl/psm2: provide option to disable psm2 REFCNT check

There are cases where customers may have systems where they don't want to upgrade
lower level system software like PSM2 and now can't install OMPI 4.1 and newer
and still have PSM2 MTL support.

This PR provides a way for knowledgable admins to disable the PSM2 version
checking.

related to issue #8381

Signed-off-by: Howard Pritchard <hppritcha@gmail.com>
(cherry picked from commit 6e04f3decd20c16f90b41c6818cf566cb583ce07)
mtl/psm2: provide option to disable psm2 REFCNT check

There are cases where customers may have systems where they don't want to upgrade
lower level system software like PSM2 and now can't install OMPI 4.1 and newer
and still have PSM2 MTL support.

This PR provides a way for knowledgable admins to disable the PSM2 version
checking.

related to issue #8381

Signed-off-by: Howard Pritchard <hppritcha@gmail.com>
(cherry picked from commit 6e04f3decd20c16f90b41c6818cf566cb583ce07)
