doc: update document for v2.2.4
fix : Allow NSSAI in Registration accept (#910)

Always Present Allow NSSAI in Registration accept
[AMF] Add NGAP ErrorIndication [#910]

1. UE sends PDU session establishment request to the AMF.
2. AMF initiates Release Due to Duplicate Session ID.
3. SMF cannot find the session by SM-Context-Ref.

For the above condition, AMF sends NGAP ErrorIndication to the UE.
[AMF] 5G-GUTI generation changed (#910)

The AMF shall assign a new 5G-GUTI for a particular UE:
a) during  a successful initial registration procedure;
b) during a successful registration procedure
   for mobility registration update; and
c) after a successful service request procedure invoked as a response
   to a paging request from the network and before the release
   of the N1 NAS signalling connection as specified in subclause 5.4.4.1.

The AMF should assign a new 5G-GUTI for a particular UE
during a successful registration procedure
for periodic registration update. The AMF may assign a new 5G-GUTI
at any time for a particular UE by performing
the generic UE configuration update procedure.
[AMF] fix the bug for making allowed NSSAI (#910)
[AMF] fix the bug for making allowed NSSAI (#910)
