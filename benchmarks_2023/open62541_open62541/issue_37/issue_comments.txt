Decode-functions not safe!!
:+1 for more intrinsic safety!  I'd like to  propose to change the signature and logic of both encode and decode to use UA_ByteString\* instead of UA_Byte\* as src or dst. 

While acting on this, we could add a Binary-postfix to the method-names as well, e.g.

``` C
TYPE##_decodeBinary(UA_ByteStr const* src, UA_Int32* pos, TYPE* dst);
TYPE##_encodeBinary(TYPE const * src, UA_Int32* pos, UA_ByteStr* dst);
```

to be prepared for other encodings such as Xml or Rdf. Should this proposel get some positive votes I'd volunteer to implement...

+1 also for the postfix.

It happens to the best.
Because of a similar bug, one could read RAM (possibly private certificates) from remote in OpenSSL until this morning:  www.openssl.org/news/secadv_20140407.txt
**Edit:** http://heartbleed.com/

@jpfr woking since 10:00 on recompiling some distros here :(

Need to check analytically or via regression tests error handling. Current praxis of not evaluating retval within a sequence of encoders/decoders seems too lazy.

check_memory.c helped me to find some more issues, with commit dc7e8330367f26a9bc7de5eeaae9a32afa60dafd at least the decode functions seem to be at least a little bit more robust... 

Coming closer, however see commit f515aac

e24fd0adb26a40333ef0f1336ff0a4206b31c6f8
works for me.
was that all?

See here for a simple fuzzer for the decode functions: https://github.com/jpfr/open62541/commit/9724abfb4f4067f1cb9800b96b01487d1e918513
It's just a decode on a buffer of size 256, filled with random data.

I need to reboot my VM regularly as RAM and HDD (logging) are maxed out by check_memory.

@uleon 
You seem to grasp this area the best. Any idea where this comes from?

Great commit e24fd0adb26a40333ef0f1336ff0a4206b31c6f8! I don't believe that this was all, next steps on my agenda to close this issue would be: 
- fix the not intended memory leaks that we still find with

```
cd tests
make check_memory-mem 
```
- think about changing the signature of the encoder/decoders to have position as unsigned, i.e. UA_UInt32 or check pos > 0 where ever appropriate
- define regression-tests for the decoders, perhaps not with arbitrary random sequences but with fixed ones or cleverly chosen patterns such as 0xFF every byte, 0xFF-0x00 every other byte, huge array sizes close to MAX_INT, a.s.o.

The fuzzer is exactly what is missing, perfect approach.  At the time being this test should be run best with `configure --enable-debug=no`, some of the debug code assumes correct behaviour and is executed before the limits-checks.

The major pitfalls are currently the mediocre checking of array indices and type information and the too optimistic strategy to simply collect the retvals:

``` c
retval |= …
retval |= …
return retval;
```

We might want to replace this with a more defensive strategy for the generated decoders and encoders.

``` C
if (UA_SUCCESS != (retval |= …)) return retval;
```

Then of course a inline function such as

``` C
inline _Bool UA_VTable_isValidType(UA_UInt type) { return type >= UA_BOOLEAN && type <= UA_INVALID; }
```

is urgently needed. Furthermore I'd like to suggest to take it easy and start with a limited test-loop from UA_BOOLEAN to UA_VARIANT.  

no crashes observed since #53.

This thread has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.