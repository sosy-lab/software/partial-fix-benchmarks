Allow building only hostverify library and with minimal build dependencies
Completely agree with this. We should offer a simple interface for building only the hostverify library, and to make sure it doesn't link to any unnecessary libraries.

Thanks for filing this issue @letmaik !
+1.  For libraries designed to run on non-TEE platforms, we should also provide a way for developers who want to just consume them in binary form, rather than requiring compiling them at all.
What's the progress on this?
> What's the progress on this?

I will come back to work on this issue as long as other hostverify-library-related issues are done.
@letmaik 
Are you looking for an easy way to build host verifier package?
Here is what we have currently:
cpack -G DEB -D CPACK_DEB_COMPONENT_INSTALL=ON  -D CPACK_COMPONENTS_ALL=OEHOSTVERIFY

or you are asking for above package building process more efficient?
@soccerGB I'd like to build the host verifier library, not as package. And I need to build without any Intel SGX library dependencies available. The use case is to build redistributable Python binary wheels (which means manylinux wheels, building on CentOS) which make use of the host verifier library.
@letmaik are you making a Python library for quote verification? Is it going to be on pypi? Can we steal it/help with it? :)
@achamayou It's not standalone yet but I guess it could be. Ideally I'd like a pure-Python library but looks like this is not happening any time soon. 
What's the progress on this?
@letmaik Testing my work on Windows builds now (fine on Linux builds). After it is done I'll soon open a PR for everyone to review it. Then you will be able to use cmake and cpack to build desired minimal hostverify library without further modifications to the files installed by the library.
@ryanhsu19 Can you re-open this? PR #3193 is not the full solution.
I don't think I can re-open an issue not created by myself. Let me ask others in the team to do so.
Reopening per @letmaik's request
> @ryanhsu19 Can you re-open this? PR #3193 is not the full solution.

@letmaik  What's missing? Can you explain?
@yentsanglee I think it is due to the part where `oehost` could not be fully separated.
As long as https://github.com/openenclave/openenclave/pull/3123 is still open, this issue has to be open.
What's the status here? I saw that https://github.com/openenclave/openenclave/pull/3123 was closed. How much work is missing to enable this?