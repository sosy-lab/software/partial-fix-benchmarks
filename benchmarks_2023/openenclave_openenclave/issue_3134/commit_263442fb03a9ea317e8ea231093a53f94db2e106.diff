diff --git a/CHANGELOG.md b/CHANGELOG.md
index 440e70a0db..3780e864db 100644
--- a/CHANGELOG.md
+++ b/CHANGELOG.md
@@ -21,6 +21,7 @@ and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0
   application EDL. See [system EDL opt-in document]
   (docs/DesignDocs/system_ocall_opt_in.md#how-to-port-your-application) for more information.
 - Switch to oeedger8r written in C++.
+- Fix #3134. oesign tool will now reject .conf files that contain duplicate property definitions.
 
 ### Removed
 - Removed oehostapp and the appendent "-rdynamic" compiling option. Please use oehost instead and add the option back manually if necessary.
diff --git a/tests/tools/oesign/test-digest/CMakeLists.txt b/tests/tools/oesign/test-digest/CMakeLists.txt
index b3465287e9..98f548d88f 100644
--- a/tests/tools/oesign/test-digest/CMakeLists.txt
+++ b/tests/tools/oesign/test-digest/CMakeLists.txt
@@ -9,12 +9,37 @@ add_custom_command(
   COMMAND cmake -E copy ${CMAKE_CURRENT_SOURCE_DIR}/../sign-and-verify.py
           ${CMAKE_CURRENT_BINARY_DIR})
 
+add_custom_command(
+  OUTPUT oesign_test_enc.baseline.digest.sig
+  DEPENDS oesign_test_enc oesign_test_configs oesign_test_keys
+  COMMAND
+    oesign digest -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/valid.conf -d oesign_test_enc.baseline.digest
+  COMMAND
+    openssl pkeyutl -sign -pkeyopt digest:sha256 -in
+    oesign_test_enc.baseline.digest -inkey
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem -keyform PEM -out
+    oesign_test_enc.baseline.digest.sig)
+
+add_custom_command(
+  OUTPUT oesign_test_alt_enc.baseline.digest.sig
+  DEPENDS oesign_test_alt_enc
+  COMMAND oesign digest -e $<TARGET_FILE:oesign_test_alt_enc> -d
+          oesign_test_alt_enc.baseline.digest
+  COMMAND
+    openssl pkeyutl -sign -pkeyopt digest:sha256 -in
+    oesign_test_alt_enc.baseline.digest -inkey
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem -keyform PEM -out
+    oesign_test_alt_enc.baseline.digest.sig)
+
 add_custom_target(
   oesign_digest_test_dependencies ALL
   DEPENDS oesign
           oesign_test_host
           oesign_test_enc
+          oesign_test_enc.baseline.digest.sig
           oesign_test_alt_enc
+          oesign_test_alt_enc.baseline.digest.sig
           oesign_test_keys
           oesign_test_configs
           sign-and-verify.py)
@@ -111,7 +136,8 @@ add_test(
   NAME tests/oesign-digest-sign-missing-enclave-arg
   COMMAND
     oesign sign -c ${OESIGN_TEST_INPUTS_DIR}/valid.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-missing-enclave-arg
@@ -120,8 +146,9 @@ set_tests_properties(
 # Test sign command for digest options missing --x509 (-x) argument
 add_test(
   NAME tests/oesign-digest-sign-missing-x509-arg
-  COMMAND oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
-          ${OESIGN_TEST_INPUTS_DIR}/valid.conf -d oesign_test_enc.digest.sig)
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/valid.conf -d oesign_test_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-missing-x509-arg
@@ -147,8 +174,9 @@ add_test(
   COMMAND
     oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/valid.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig
-    -k ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_enc.baseline.digest.sig -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
 
 set_tests_properties(
   tests/oesign-digest-sign-conflicting-key-file-arg
@@ -161,7 +189,8 @@ add_test(
   COMMAND
     oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/valid.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key_2.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key_2.cert.pem -d
+    oesign_test_enc.baseline.digest.sig)
 
 set(OESIGN_DIGEST_SIGN_MISMATCHED_X509_REGEX
     "ERROR: Digest signature cannot be validated against the specified enclave configuration using the provided certificate"
@@ -178,12 +207,14 @@ set_tests_properties(
              ${OESIGN_DIGEST_SIGN_MISMATCHED_X509_REGEX})
 
 # Test sign command for digest options with mismatched Debug config
+# Also validates that the config Debug value overwrites the build time one
 add_test(
   NAME tests/oesign-digest-sign-new-debug-config
   COMMAND
-    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/non_debug.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_alt_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-new-debug-config
@@ -193,12 +224,14 @@ set_tests_properties(
 )
 
 # Test sign command for digest options with mismatched NumHeapPages config
+# Also validates that the config NumHeapPages value overwrites the build time one
 add_test(
   NAME tests/oesign-digest-sign-new-num-heap-pages
   COMMAND
-    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/more_num_heap_pages.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_alt_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-new-num-heap-pages
@@ -208,12 +241,14 @@ set_tests_properties(
 )
 
 # Test sign command for digest options with mismatched NumStackPages config
+# Also validates that the config NumStackPages value overwrites the build time one
 add_test(
   NAME tests/oesign-digest-sign-new-num-stack-pages
   COMMAND
-    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/more_num_stack_pages.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_alt_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-new-num-stack-pages
@@ -223,12 +258,14 @@ set_tests_properties(
 )
 
 # Test sign command for digest options with mismatched NumTCS config
+# Also validates that the config NumTCS value overwrites the build time one
 add_test(
   NAME tests/oesign-digest-sign-new-num-tcs
   COMMAND
-    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/more_num_tcs.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_alt_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-new-num-tcs
@@ -238,12 +275,14 @@ set_tests_properties(
 )
 
 # Test sign command for digest options with mismatched ProductID config
+# Also validates that the config ProductID value overwrites the build time one
 add_test(
   NAME tests/oesign-digest-sign-new-product-id
   COMMAND
-    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/new_product_id.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_alt_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-new-product-id
@@ -253,12 +292,14 @@ set_tests_properties(
 )
 
 # Test sign command for digest options with mismatched SecurityVersion config
+# Also validates that the config SecurityVersion value overwrites the build time one
 add_test(
   NAME tests/oesign-digest-sign-new-security-version
   COMMAND
-    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/new_security_version.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_alt_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-new-security-version
@@ -273,7 +314,8 @@ add_test(
   COMMAND
     oesign sign -e $<TARGET_FILE:oesign_test_alt_enc> -c
     ${OESIGN_TEST_INPUTS_DIR}/valid.conf -x
-    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d oesign_test_enc.digest.sig)
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.cert.pem -d
+    oesign_test_enc.baseline.digest.sig)
 
 set_tests_properties(
   tests/oesign-digest-sign-alt-enclave
diff --git a/tests/tools/oesign/test-inputs/CMakeLists.txt b/tests/tools/oesign/test-inputs/CMakeLists.txt
index d36953e475..f316d59dd0 100644
--- a/tests/tools/oesign/test-inputs/CMakeLists.txt
+++ b/tests/tools/oesign/test-inputs/CMakeLists.txt
@@ -1,12 +1,20 @@
 # Copyright (c) Open Enclave SDK contributors.
 # Licensed under the MIT License.
 
-# Generate test variations from a standard baseline configuration
-# TODO: Any test configurations that consist of more than just manipulations
+# Copy hand authored test configurations to output
+#
+# Any test configurations that consist of more than just manipulations
 # of the standard configuration values (e.g. syntax error tests) should be
 # added to the test-inputs source folder and copied to the binary output
 # folder on build.
+add_custom_command(
+  OUTPUT duplicate_debug.conf duplicate_num_heap_pages.conf
+         duplicate_num_stack_pages.conf duplicate_num_tcs.conf
+         duplicate_product_id.conf duplicate_security_version.conf
+  COMMAND cmake -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}
+          ${CMAKE_CURRENT_BINARY_DIR})
 
+# Generate test variations from a standard baseline configuration
 add_custom_command(
   OUTPUT valid.conf
   DEPENDS ${CMAKE_CURRENT_SOURCE_DIR}/make-oesign-config.py
@@ -63,7 +71,13 @@ add_custom_command(
 
 add_custom_target(
   oesign_test_configs ALL
-  DEPENDS empty.conf
+  DEPENDS duplicate_debug.conf
+          duplicate_num_heap_pages.conf
+          duplicate_num_stack_pages.conf
+          duplicate_num_tcs.conf
+          duplicate_product_id.conf
+          duplicate_security_version.conf
+          empty.conf
           more_num_heap_pages.conf
           more_num_stack_pages.conf
           more_num_tcs.conf
diff --git a/tests/tools/oesign/test-inputs/duplicate_debug.conf b/tests/tools/oesign/test-inputs/duplicate_debug.conf
new file mode 100644
index 0000000000..34ecc5e895
--- /dev/null
+++ b/tests/tools/oesign/test-inputs/duplicate_debug.conf
@@ -0,0 +1,11 @@
+# Copyright (c) Open Enclave SDK contributors.
+# Licensed under the MIT License.
+
+Debug=1
+# Duplicate Debug entry
+Debug=1
+NumHeapPages=1024
+NumStackPages=1024
+NumTCS=1
+ProductID=1
+SecurityVersion=1
diff --git a/tests/tools/oesign/test-inputs/duplicate_num_heap_pages.conf b/tests/tools/oesign/test-inputs/duplicate_num_heap_pages.conf
new file mode 100644
index 0000000000..699c4d2679
--- /dev/null
+++ b/tests/tools/oesign/test-inputs/duplicate_num_heap_pages.conf
@@ -0,0 +1,11 @@
+# Copyright (c) Open Enclave SDK contributors.
+# Licensed under the MIT License.
+
+Debug=1
+NumHeapPages=1024
+# Duplicate NumHeapPages entry
+NumHeapPages=1024
+NumStackPages=1024
+NumTCS=1
+ProductID=1
+SecurityVersion=1
diff --git a/tests/tools/oesign/test-inputs/duplicate_num_stack_pages.conf b/tests/tools/oesign/test-inputs/duplicate_num_stack_pages.conf
new file mode 100644
index 0000000000..9bee4d076e
--- /dev/null
+++ b/tests/tools/oesign/test-inputs/duplicate_num_stack_pages.conf
@@ -0,0 +1,11 @@
+# Copyright (c) Open Enclave SDK contributors.
+# Licensed under the MIT License.
+
+Debug=1
+NumHeapPages=1024
+NumStackPages=1024
+# Duplicate NumStackPages entry
+NumStackPages=1024
+NumTCS=1
+ProductID=1
+SecurityVersion=1
diff --git a/tests/tools/oesign/test-inputs/duplicate_num_tcs.conf b/tests/tools/oesign/test-inputs/duplicate_num_tcs.conf
new file mode 100644
index 0000000000..b4333af131
--- /dev/null
+++ b/tests/tools/oesign/test-inputs/duplicate_num_tcs.conf
@@ -0,0 +1,11 @@
+# Copyright (c) Open Enclave SDK contributors.
+# Licensed under the MIT License.
+
+Debug=1
+NumHeapPages=1024
+NumStackPages=1024
+NumTCS=1
+# Duplicate NumTCS entry
+NumTCS=1
+ProductID=1
+SecurityVersion=1
diff --git a/tests/tools/oesign/test-inputs/duplicate_product_id.conf b/tests/tools/oesign/test-inputs/duplicate_product_id.conf
new file mode 100644
index 0000000000..6f9650f743
--- /dev/null
+++ b/tests/tools/oesign/test-inputs/duplicate_product_id.conf
@@ -0,0 +1,11 @@
+# Copyright (c) Open Enclave SDK contributors.
+# Licensed under the MIT License.
+
+Debug=1
+NumHeapPages=1024
+NumStackPages=1024
+NumTCS=1
+ProductID=1
+# Duplicate ProductID entry
+ProductID=1
+SecurityVersion=1
diff --git a/tests/tools/oesign/test-inputs/duplicate_security_version.conf b/tests/tools/oesign/test-inputs/duplicate_security_version.conf
new file mode 100644
index 0000000000..865f31163e
--- /dev/null
+++ b/tests/tools/oesign/test-inputs/duplicate_security_version.conf
@@ -0,0 +1,11 @@
+# Copyright (c) Open Enclave SDK contributors.
+# Licensed under the MIT License.
+
+Debug=1
+NumHeapPages=1024
+NumStackPages=1024
+NumTCS=1
+ProductID=1
+SecurityVersion=1
+# Duplicate SecurityVersion entry
+SecurityVersion=1
diff --git a/tests/tools/oesign/test-sign/CMakeLists.txt b/tests/tools/oesign/test-sign/CMakeLists.txt
index 2ecec0dd83..eb821026f8 100644
--- a/tests/tools/oesign/test-sign/CMakeLists.txt
+++ b/tests/tools/oesign/test-sign/CMakeLists.txt
@@ -67,6 +67,79 @@ set_tests_properties(
   PROPERTIES PASS_REGULAR_EXPRESSION
              "ERROR: Failed to load file: does_not_exist.pem")
 
+# Test invalid .conf with duplicate Debug property
+add_test(
+  NAME tests/oesign-sign-duplicate-debug-config
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/duplicate_debug.conf -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+
+set_tests_properties(
+  tests/oesign-sign-duplicate-debug-config
+  PROPERTIES PASS_REGULAR_EXPRESSION "Duplicate 'Debug' value provided")
+
+# Test invalid .conf with duplicate NumHeapPages property
+add_test(
+  NAME tests/oesign-sign-duplicate-num-heap-pages-config
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/duplicate_num_heap_pages.conf -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+
+set_tests_properties(
+  tests/oesign-sign-duplicate-num-heap-pages-config
+  PROPERTIES PASS_REGULAR_EXPRESSION "Duplicate 'NumHeapPages' value provided")
+
+# Test invalid .conf with duplicate NumStackPages property
+add_test(
+  NAME tests/oesign-sign-duplicate-num-stack-pages-config
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/duplicate_num_stack_pages.conf -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+
+set_tests_properties(
+  tests/oesign-sign-duplicate-num-stack-pages-config
+  PROPERTIES PASS_REGULAR_EXPRESSION "Duplicate 'NumStackPages' value provided")
+
+# Test invalid .conf with duplicate NumTCS property
+add_test(
+  NAME tests/oesign-sign-duplicate-num-tcs-config
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/duplicate_num_tcs.conf -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+
+set_tests_properties(
+  tests/oesign-sign-duplicate-num-tcs-config
+  PROPERTIES PASS_REGULAR_EXPRESSION "Duplicate 'NumTCS' value provided")
+
+# Test invalid .conf with duplicate ProductID property
+add_test(
+  NAME tests/oesign-sign-duplicate-product-id-config
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/duplicate_product_id.conf -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+
+set_tests_properties(
+  tests/oesign-sign-duplicate-product-id-config
+  PROPERTIES PASS_REGULAR_EXPRESSION "Duplicate 'ProductID' value provided")
+
+# Test invalid .conf with duplicate SecurityVersion property
+add_test(
+  NAME tests/oesign-sign-duplicate-security-version-config
+  COMMAND
+    oesign sign -e $<TARGET_FILE:oesign_test_enc> -c
+    ${OESIGN_TEST_INPUTS_DIR}/duplicate_security_version.conf -k
+    ${OESIGN_TEST_INPUTS_DIR}/sign_key.private.pem)
+
+set_tests_properties(
+  tests/oesign-sign-duplicate-security-version-config
+  PROPERTIES PASS_REGULAR_EXPRESSION
+             "Duplicate 'SecurityVersion' value provided")
+
 # Test signing key with invalid exponent for SGX signing
 add_test(
   NAME tests/oesign-sign-invalid-key-exp
diff --git a/tools/oesign/oesign.c b/tools/oesign/oesign.c
index d84ace46c1..51b66abc0a 100644
--- a/tools/oesign/oesign.c
+++ b/tools/oesign/oesign.c
@@ -12,25 +12,36 @@
 #include "oe_err.h"
 #include "oeinfo.h"
 
+typedef struct _optional_bool
+{
+    bool has_value;
+    bool value;
+} optional_bool_t;
+
+typedef struct _optional_uint64
+{
+    bool has_value;
+    uint64_t value;
+} optional_uint64_t;
+
+typedef struct _optional_uint16
+{
+    bool has_value;
+    uint16_t value;
+} optional_uint16_t;
+
 // Options loaded from .conf file. Uninitialized fields contain the maximum
 // integer value for the corresponding type.
 typedef struct _config_file_options
 {
-    bool debug;
-    uint64_t num_heap_pages;
-    uint64_t num_stack_pages;
-    uint64_t num_tcs;
-    uint16_t product_id;
-    uint16_t security_version;
+    optional_bool_t debug;
+    optional_uint64_t num_heap_pages;
+    optional_uint64_t num_stack_pages;
+    optional_uint64_t num_tcs;
+    optional_uint16_t product_id;
+    optional_uint16_t security_version;
 } config_file_options_t;
 
-#define CONFIG_FILE_OPTIONS_INITIALIZER                                 \
-    {                                                                   \
-        .debug = false, .num_heap_pages = OE_UINT64_MAX,                \
-        .num_stack_pages = OE_UINT64_MAX, .num_tcs = OE_UINT64_MAX,     \
-        .product_id = OE_UINT16_MAX, .security_version = OE_UINT16_MAX, \
-    }
-
 static int _load_config_file(const char* path, config_file_options_t* options)
 {
     int rc = -1;
@@ -80,6 +91,12 @@ static int _load_config_file(const char* path, config_file_options_t* options)
         {
             uint64_t value;
 
+            if (options->debug.has_value)
+            {
+                oe_err("%s(%zu): Duplicate 'Debug' value provided", path, line);
+                goto done;
+            }
+
             // Debug must be 0 or 1
             if (str_u64(&rhs, &value) != 0 || (value > 1))
             {
@@ -87,12 +104,22 @@ static int _load_config_file(const char* path, config_file_options_t* options)
                 goto done;
             }
 
-            options->debug = (bool)value;
+            options->debug.value = (bool)value;
+            options->debug.has_value = true;
         }
         else if (strcmp(str_ptr(&lhs), "NumHeapPages") == 0)
         {
             uint64_t n;
 
+            if (options->num_heap_pages.has_value)
+            {
+                oe_err(
+                    "%s(%zu): Duplicate 'NumHeapPages' value provided",
+                    path,
+                    line);
+                goto done;
+            }
+
             if (str_ptr(&rhs)[0] == '-' || str_u64(&rhs, &n) != 0 ||
                 !oe_sgx_is_valid_num_heap_pages(n))
             {
@@ -104,12 +131,22 @@ static int _load_config_file(const char* path, config_file_options_t* options)
                 goto done;
             }
 
-            options->num_heap_pages = n;
+            options->num_heap_pages.value = n;
+            options->num_heap_pages.has_value = true;
         }
         else if (strcmp(str_ptr(&lhs), "NumStackPages") == 0)
         {
             uint64_t n;
 
+            if (options->num_stack_pages.has_value)
+            {
+                oe_err(
+                    "%s(%zu): Duplicate 'NumStackPages' value provided",
+                    path,
+                    line);
+                goto done;
+            }
+
             if (str_ptr(&rhs)[0] == '-' || str_u64(&rhs, &n) != 0 ||
                 !oe_sgx_is_valid_num_stack_pages(n))
             {
@@ -121,12 +158,20 @@ static int _load_config_file(const char* path, config_file_options_t* options)
                 goto done;
             }
 
-            options->num_stack_pages = n;
+            options->num_stack_pages.value = n;
+            options->num_stack_pages.has_value = true;
         }
         else if (strcmp(str_ptr(&lhs), "NumTCS") == 0)
         {
             uint64_t n;
 
+            if (options->num_tcs.has_value)
+            {
+                oe_err(
+                    "%s(%zu): Duplicate 'NumTCS' value provided", path, line);
+                goto done;
+            }
+
             if (str_ptr(&rhs)[0] == '-' || str_u64(&rhs, &n) != 0 ||
                 !oe_sgx_is_valid_num_tcs(n))
             {
@@ -138,12 +183,22 @@ static int _load_config_file(const char* path, config_file_options_t* options)
                 goto done;
             }
 
-            options->num_tcs = n;
+            options->num_tcs.value = n;
+            options->num_tcs.has_value = true;
         }
         else if (strcmp(str_ptr(&lhs), "ProductID") == 0)
         {
             uint16_t n;
 
+            if (options->product_id.has_value)
+            {
+                oe_err(
+                    "%s(%zu): Duplicate 'ProductID' value provided",
+                    path,
+                    line);
+                goto done;
+            }
+
             if (str_ptr(&rhs)[0] == '-' || str_u16(&rhs, &n) != 0 ||
                 !oe_sgx_is_valid_product_id(n))
             {
@@ -155,12 +210,22 @@ static int _load_config_file(const char* path, config_file_options_t* options)
                 goto done;
             }
 
-            options->product_id = n;
+            options->product_id.value = n;
+            options->product_id.has_value = true;
         }
         else if (strcmp(str_ptr(&lhs), "SecurityVersion") == 0)
         {
             uint16_t n;
 
+            if (options->security_version.has_value)
+            {
+                oe_err(
+                    "%s(%zu): Duplicate 'SecurityVersion' value provided",
+                    path,
+                    line);
+                goto done;
+            }
+
             if (str_ptr(&rhs)[0] == '-' || str_u16(&rhs, &n) != 0 ||
                 !oe_sgx_is_valid_security_version(n))
             {
@@ -172,7 +237,8 @@ static int _load_config_file(const char* path, config_file_options_t* options)
                 goto done;
             }
 
-            options->security_version = n;
+            options->security_version.value = n;
+            options->security_version.has_value = true;
         }
         else
         {
@@ -296,30 +362,35 @@ void _merge_config_file_options(
     }
 
     /* Debug option is present */
-    if (options->debug)
-        properties->config.attributes |= SGX_FLAGS_DEBUG;
+    if (options->debug.has_value)
+    {
+        if (options->debug.value)
+            properties->config.attributes |= SGX_FLAGS_DEBUG;
+        else
+            properties->config.attributes &= ~SGX_FLAGS_DEBUG;
+    }
 
     /* If ProductID option is present */
-    if (options->product_id != OE_UINT16_MAX)
-        properties->config.product_id = options->product_id;
+    if (options->product_id.has_value)
+        properties->config.product_id = options->product_id.value;
 
     /* If SecurityVersion option is present */
-    if (options->security_version != OE_UINT16_MAX)
-        properties->config.security_version = options->security_version;
+    if (options->security_version.has_value)
+        properties->config.security_version = options->security_version.value;
 
     /* If NumHeapPages option is present */
-    if (options->num_heap_pages != OE_UINT64_MAX)
+    if (options->num_heap_pages.has_value)
         properties->header.size_settings.num_heap_pages =
-            options->num_heap_pages;
+            options->num_heap_pages.value;
 
     /* If NumStackPages option is present */
-    if (options->num_stack_pages != OE_UINT64_MAX)
+    if (options->num_stack_pages.has_value)
         properties->header.size_settings.num_stack_pages =
-            options->num_stack_pages;
+            options->num_stack_pages.value;
 
     /* If NumTCS option is present */
-    if (options->num_tcs != OE_UINT64_MAX)
-        properties->header.size_settings.num_tcs = options->num_tcs;
+    if (options->num_tcs.has_value)
+        properties->header.size_settings.num_tcs = options->num_tcs.value;
 }
 
 oe_result_t _initialize_enclave_properties(
@@ -328,7 +399,7 @@ oe_result_t _initialize_enclave_properties(
     oe_sgx_enclave_properties_t* properties)
 {
     oe_result_t result = OE_INVALID_PARAMETER;
-    config_file_options_t options = CONFIG_FILE_OPTIONS_INITIALIZER;
+    config_file_options_t options = {{0}};
 
     /* Load the configuration file */
     if (conffile && _load_config_file(conffile, &options) != 0)
