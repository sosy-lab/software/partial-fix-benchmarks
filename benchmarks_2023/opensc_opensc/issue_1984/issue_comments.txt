Allow PKCS11SPY_OUTPUT to specify a filename template
Good idea. It could also be used in `opensc.conf` for debug_file=.

A subset of "%p" and "%t" might be all that is needed.  And it can all be done within OpenSC code, so should work on any system. 

OpenSC logs already have PID, Thread, time and a name of program if provided in each. But pkcs11 programs show up as "opensc-pkcs11" and "minidriver" shows up as minidriver. 

 A "%e" would be nice if it is easy to get the actual executable name. 

On windows pkcs11-spy  and ctx.c can do some expansions:
https://github.com/OpenSC/OpenSC/blob/master/src/pkcs11/pkcs11-spy.c#L166
https://github.com/OpenSC/OpenSC/blob/master/src/libopensc/ctx.c#L387

It could be expanded after expanding with a PR like you propose. 

I think that using the environment variables is powerful enough. Did you try modifying the startup scripts to "diversify" the output file?