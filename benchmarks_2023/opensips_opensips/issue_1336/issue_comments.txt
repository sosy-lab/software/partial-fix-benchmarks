doesn't handle ipv6 mask in address table
@pasanmdev , please check the above fix (for the moment only on master). If confirmed, I will proceed with the backport.
Thanks!
@bogdan-iancu Thanks for this. patch is good. It fixed the issue with dealing with ipv6 addresses. now it can load ipv6 address properly. You can proceed with backport and close this.
btw you may need to update docs re address table no ?

in here
http://www.opensips.org/html/docs/modules/2.3.x/permissions.html#idp4133952

"Name of address table column containing network mask of the address. Possible values are 0-32."
should be up to 128 now right ?

Good point @pasanmdev - I updated the docs, the html files should be regenerated
Cool :)