[BUG] OpenSIPS reuses TLS connections incorrectly
Hey @vasilevalex , could you give a try to this alpha patch 
[tls_extra_matching_3_0.patch.txt](https://github.com/OpenSIPS/opensips/files/3203589/tls_extra_matching_3_0.patch.txt) for 3.0 / master (it might apply on 2.4 also) ? Any testing and feedback will be highly appreciated. 

Hi @bogdan-iancu , I've tested little bit, and everything works fine. OpenSIPS can establish several TLS connections to one destination IP:5061 with different certificates. I'll attach patch for 2.4 branch. Thanks, it's cool!
[tls_extra_matching_2_4.patch.txt](https://github.com/OpenSIPS/opensips/files/3207311/tls_extra_matching_2_4.patch.txt)

@bogdan-iancu It seems, that not everything is ok. If I have only incoming TLS connection from phone, for example, I can't send anything to this connection. I'll make more tests tomorrow.
@vasilevalex , if I understand correctly, some UAC's are opening TLS connections to OpenSIPS, you are not able to send the replies back ? or ?
@bogdan-iancu , no, another case: UAC opens TLS connection to OpenSIPS (without certificate verification, as UAC can be some 3d party phone with unknown certificate), sends REGISTER through OpenSIPS to registrar, got replies back, that's ok. But than registrar sends e.g. OPTIONS to UAC, OpenSIPS can't pass it through, and answers 477 to registrar. Then OpenSIPS tries to send OPTIONS (with nathelper ping) and fails again. OpenSIPS can't establish new TLS session, because UAC behind NAT, firewalls etc. But it also can't reuse existing session - this is the case.
Basically the issue is: OpenSIPS is mainly configured with TLS server domains only (to accept incoming connections) - and no TLS client domains; In this case, when OpenSIPS has to send back to UAC some request (like OPTIONs ping, maybe a BYE from the other leg), it has no TLS client certificate to use when identifying the connection to write on ?
OK, please revert the prev patch and try this new one [tls_extra_matching_3_0.patch2.txt](https://github.com/OpenSIPS/opensips/files/3210914/tls_extra_matching_3_0.patch2.txt)

I built with the last patch, and added some more logging:
Phone connects:
```
May 23 08:50:53 test82 /usr/sbin/opensips[18441]: INFO:proto_tls:tls_accept: New TLS connection from 194.238.22.128:2133 accepted
May 23 08:50:53 test82 /usr/sbin/opensips[18441]: INFO:proto_tls:tls_accept: Client did not present a TLS certificate
May 23 08:50:53 test82 /usr/sbin/opensips[18441]: INFO:proto_tls:tls_dump_cert_info: tls_accept: local TLS server certificate subject: /CN=test.dk, issuer: /C=US/O=Let's Encrypt/CN=Let's Encrypt Authority X3
```
Sending OPTIONS to phone:
```
May 23 08:50:54 test82 /usr/sbin/opensips[18390]: INFO:tls_mgm:tls_find_client_domain: no TLS client domain AVP set, looking to match TLS client domain by socket
May 23 08:50:54 test82 /usr/sbin/opensips[18390]: INFO:tls_mgm:tls_find_client_domain_addr: virtual TLS client domain not found, Using default TLS client domain settings
May 23 08:50:54 test82 /usr/sbin/opensips[18390]: INFO:tls_mgm:tls_find_client_domain: found TLS client domain [0.0.0.0:0] based on socket
May 23 08:50:54 test82 /usr/sbin/opensips[18390]: ERROR:tm:msg_send: send() to 194.238.22.128:2133 for proto tls/3 failed
May 23 08:50:54 test82 /usr/sbin/opensips[18390]: ERROR:tm:t_forward_nonack: sending request failed
```
So OpenSIPS chooses default TLS client domain. If this can help?
I mean still the same behavior on incoming connections - can't use them to send messages.
@vasilevalex , you used 2.4 for the testing, right ?

[tls_extra_matching_2_4.patch.txt](https://github.com/OpenSIPS/opensips/files/3228156/tls_extra_matching_2_4.patch.txt)
Hi @bogdan-iancu , yes, I've used 2.4.5 for all the tests
In the existing (patched) code, could you replace this function:

```
static int proto_tls_extra_match(struct tcp_connection *c, void *id)
{
	if ( (c->flags&F_CONN_ACCEPTED) ||
	(SSL_get_SSL_CTX((SSL*)c->extra_data) == id) )
		return 1; /*true*/

	return 0; /*false*/
}
```
it is in `modules/proto_tls`.

And make another set of tests please.
@vasilevalex , I uploaded the last version on `master` and `3.0` branches. In order to be backward compatible, I added (in `proto_tls` and `proto_wss` modules) the `cert_check_on_conn_reusage` module parameter to enable / disable the cert checking when doing connection reusage. By default is off, so this change will not impact the previous behavior.
The idea is to have this support in `3.0` release, but to have it by default off, until we find the best formula for the check.
So discard the patch I sent you and update from git - and let's continue the discussion here about the checking formula.
@vasilevalex , when you have the time to retest your scenarios, please update here with the results. I'm curious if the matching condition is a good one this time :)
Hi, @bogdan-iancu I'm testing. It looks, like everything works. But I'll continue to be sure.
Thanks @vasilevalex - hopefully everything will turn out well :D...anyhow, keep us here posted.
@vasilevalex , I will close this and the ticket addressed the main issue. If we logic we added (for filtering the connections) needs further tuning, please open a new ticket.