net: handle CONN_ERROR2 on main
ratelimit: make rl_feedback_limit extern

Make sure `rl_feedback_limit` is properly alocated for the entire
module, not only for the current object.

Fixes the crash reported by Ben Newlin. Close #1882
ratelimit: make rl_feedback_limit extern

Make sure `rl_feedback_limit` is properly alocated for the entire
module, not only for the current object.

Fixes the crash reported by Ben Newlin. Close #1882

(cherry picked from commit 8f26f5aa501dfad69e50c2bedcd4325afe7b1508)
ratelimit: make rl_feedback_limit extern

Make sure `rl_feedback_limit` is properly alocated for the entire
module, not only for the current object.

Fixes the crash reported by Ben Newlin. Close #1882

(cherry picked from commit 8f26f5aa501dfad69e50c2bedcd4325afe7b1508)
ratelimit: allow traffic if NETWORKING stat not updated

Adresses issue #1882 reportd by Ben Newlin
ratelimit: allow traffic if NETWORKING stat not updated

Adresses issue #1882 reportd by Ben Newlin

(cherry picked from commit af6be0a44f1dc24326f3a7d251c95f31495dbb61)
ratelimit: allow traffic if NETWORKING stat not updated

Adresses issue #1882 reportd by Ben Newlin

(cherry picked from commit af6be0a44f1dc24326f3a7d251c95f31495dbb61)
