[CRASH] Crash using rl_check
@razvancrainea Thanks for the quick fix! I've tested with your commit and it does stop the crash, however now OpenSIPS simply stops processing at the rl_check call. There are no logs or any other error indication, the process simply stops and the message seems to be dropped.
Here are the logs when I send 2 INVITEs to the server that is calling rl_check as shown above: https://pastebin.com/72sMPu9R
@razvancrainea I can also open a separate ticket for the issue with OpenSIPS hanging if you prefer. 
No need of a new ticket for now, let me investigate this and will get back to you with further instructions.
Most likely one of the three `rl_check()` calls returns a value of `0`, which indicates that the script should halt. Can you comment all the calls, and uncomment them one by one, to  determine which algorithm determines the `0` value?

If none of these works, one of the algorithm makes OpenSIPS gets stuck in a loop - in that case, the output of `opensipsctl trap` would be very useful.
You are right, it is the `NETWORK` call that is hanging. Here is the output of the trap: https://pastebin.com/nsnEiuGF.
Further testing shows that my previous update is not entirely correct. While the `NETWORK` rl_check call is consistently hanging, the other `rl_check` calls do also frequently hang. It is just not quite so often. I am investigating to see if I can determine any specific scenarios that cause these other checks to fail. Anecdotally, it seems like it may be related to how long OpenSIPS has been running before `rl_check` is called, e.g. if a time_interval has elapsed yet.
I've just pushed a commit to address this issue. Can you run your tests again and let me know if you still experience this issue?
The issue appears to be resolved with this fix. Thanks!