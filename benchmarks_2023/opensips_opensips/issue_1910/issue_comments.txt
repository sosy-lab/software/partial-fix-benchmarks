[CRASH] Crash in drouting with high number of gateways on a carrier
100% it is related to this buffer (with the size or 64) used for sorting the GWs - https://github.com/OpenSIPS/opensips/blob/6bd19fcc96b9a05c46b4c9bf319b500ed93a735e/modules/drouting/drouting.c#L2383

It is bad that there is no overflow check. As a test, you can increase the define to 200, recompile and see if it still crashes in your tests.
Any updates here? No progress has been made in the last 15 days, marking as stale. Will close this issue if no further updates are made in the next 30 days.

@bcnewlin , could you please confirm the fix ? if everything ok I will do the backport to 2.4 and 3.0 also.
@bogdan-iancu We are running on 2.4 only. Could you possibly provide the backported patch and I can test?
Here is a 2.4 backport.
[opensips_2_4_drouting_1910.patch.txt](https://github.com/OpenSIPS/opensips/files/4029901/opensips_2_4_drouting_1910.patch.txt)
Let me know if it does the job.

I have confirmed the fix with the patch. Thanks!
backports done
Thanks! Unfortunately when trying to pick up that backport I've run into an issue with the fix from 59400f7. :(

I've opened a separate ticket for that. #1932 