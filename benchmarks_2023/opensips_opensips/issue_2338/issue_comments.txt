[BUG] topology hiding thinfo vulnerable to malicious input
Any updates here? No progress has been made in the last 15 days, marking as stale. Will close this issue if no further updates are made in the next 30 days.

Thank you @wdoekes for the detailed report and fix. I re-worked a bit your suggested patch (see 78909c3). Could you please give it a try and doublecheck it ? Many thanks!
PS: @wdoekes , upon your validation I will do the backporting and close here
Without your patch:
```
Jan 11 11:09:01 [250212] DBG:topology_hiding:topology_hiding_match: We found param in R-URI with value of fm9sHCoEalB9YXpcflt4GCJfaDo6CT9SNhAgW2JhY0FgQXZEfldhaWFfZl+ri2wSOiNuHD8CJhs+BBNiel1+XG1Ablorbg--
Jan 11 11:09:01 [250212] DBG:core:parse_headers: flags=ffffffffffffffff
Jan 11 11:09:01 [250212] DBG:core:parse_headers: flags=ffffffffffffffff
Jan 11 11:09:01 [250212] DBG:topology_hiding:topo_delete_vias: Delete via [Via: SIP/2.0/UDP 127.0.50.63:5060;branch=z9hG4bK-250201-1-8]
Jan 11 11:09:01 [250212] DBG:topology_hiding:topo_no_dlg_seq_handling: extracted routes [<sip:1.2.3.4;lr>;info=udp:127.0.50.62:5060] , ct [<sip:someone@1.2.3.4>;x=������������ͫ��ͫ8] and bind [udp:127.0.50.62:5060��<sip:someone@1.2.3.4>;x=������������ͫ��ͫ8]
Jan 11 11:09:01 [250212] DBG:core:parse_params: Parsing params for:[info=udp:127.0.50.62:5060]
Jan 11 11:09:01 [250212] DBG:topology_hiding:topo_no_dlg_seq_handling: Fixing message. Next hop is Loose router
Jan 11 11:09:01 [250212] DBG:topology_hiding:topo_no_dlg_seq_handling: Setting new URI to  <<sip:someone@1.2.3.4>;x=������������ͫ��ͫ8> 
Jan 11 11:09:01 [250212] ERROR:core:qm_malloc: not enough free pkg memory (1643840 bytes left, need 18446744073709551600), please increase the "-M" command line parameter!
Jan 11 11:09:01 [250212] ERROR:core:set_ruri: not enough pkg memory (-24)
Jan 11 11:09:01 [250212] ERROR:topology_hiding:topo_no_dlg_seq_handling: failed setting ruri
Jan 11 11:09:01 [250212] DBG:core:destroy_avp_list: destroying list (nil)
```
With your patch:
```
Jan 11 11:06:16 [246762] DBG:topology_hiding:topology_hiding_match: We found param in R-URI with value of fm9sHCoEalB9YXpcflt4GCJfaDo6CT9SNhAgW2JhY0FgQXZEfldhaWFfZl+ri2wSOiNuHD8CJhs+BBNiel1+XG1Ablorbg--
Jan 11 11:06:16 [246762] DBG:core:parse_headers: flags=ffffffffffffffff
Jan 11 11:06:16 [246762] DBG:core:parse_headers: flags=ffffffffffffffff
Jan 11 11:06:16 [246762] DBG:topology_hiding:topo_delete_vias: Delete via [Via: SIP/2.0/UDP 127.0.50.63:5060;branch=z9hG4bK-246741-1-8]
Jan 11 11:06:16 [246762] ERROR:topology_hiding:topo_no_dlg_seq_handling: bad length -24 in encoded contact
Jan 11 11:06:16 [246762] DBG:core:destroy_avp_list: destroying list (nil)
```
Code validated and test confirmed: LGTM!

Thanks :)

Thank you for the confirmation, backport done, closing here