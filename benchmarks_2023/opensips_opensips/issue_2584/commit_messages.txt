stir_shaken: allow the new Identity header to be appended to replies

Add a new parameter to the stir_shaken_auth() function that adds the
ability to append the new Identity header to replies or to return it
via an output variable.

Closes #2440
Fix self-IPC for cfg reloading on the MI process.

As starting 3.2 the MI procs also have IPC and cfg files, they are also eligible for receiving the cfg reload IPC command - so we need to run this IPC cmd inline for the MI proc triggering the reload.
Also adding some extra logging to help in understanding why a reload cmd may fail.

Fixes #2584.
Fix self-IPC for cfg reloading on the MI process.

As starting 3.2 the MI procs also have IPC and cfg files, they are also eligible for receiving the cfg reload IPC command - so we need to run this IPC cmd inline for the MI proc triggering the reload.
Also adding some extra logging to help in understanding why a reload cmd may fail.

Fixes #2584.

(cherry picked from commit 4c3150e461c31c29908a7ddd20e550bbfcbb69e3)
Fix race condition on reporting successful per-proc cfg reload

Set the initial per-proc reload status (to SENT) before actually sending the IPC job, to avoid any race condition (in updating the status) with the IPC callback (ran in the target process).

Fixes #2584
Many thanks to @etamme on providing the troubleshooting logs
Fix race condition on reporting successful per-proc cfg reload

Set the initial per-proc reload status (to SENT) before actually sending the IPC job, to avoid any race condition (in updating the status) with the IPC callback (ran in the target process).

Fixes #2584
Many thanks to @etamme on providing the troubleshooting logs

(cherry picked from commit e85072cd6e9f54d0a593449b87e492c488393301)
Fix race condition on reporting successful per-proc cfg reload

Set the initial per-proc reload status (to SENT) before actually sending the IPC job, to avoid any race condition (in updating the status) with the IPC callback (ran in the target process).

Fixes #2584
Many thanks to @etamme on providing the troubleshooting logs

(cherry picked from commit e85072cd6e9f54d0a593449b87e492c488393301)
