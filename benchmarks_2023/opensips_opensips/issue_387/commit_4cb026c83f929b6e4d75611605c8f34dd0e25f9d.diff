diff --git a/modules/dispatcher/dispatch.c b/modules/dispatcher/dispatch.c
index 45c1f56f894..26b37f4d8cb 100644
--- a/modules/dispatcher/dispatch.c
+++ b/modules/dispatcher/dispatch.c
@@ -1,10 +1,9 @@
 /*
- * $Id$
- *
  * dispatcher module
  *
- * Copyright (C) 2004-2006 FhG Fokus
+ * Copyright (C) 2010-2015 OpenSIPS Solutions
  * Copyright (C) 2005-2010 Voice-System.ro
+ * Copyright (C) 2004-2006 FhG Fokus
  *
  * This file is part of opensips, a free SIP server.
  *
@@ -85,8 +84,8 @@ static ds_data_t **ds_data = NULL;
 /* reader-writers lock for reloading the data */
 static rw_lock_t *ds_lock = NULL;
 
-
-
+#define dst_is_active(_dst) \
+	(!((_dst).flags&(DS_INACTIVE_DST|DS_PROBING_DST)))
 
 int init_ds_data(void)
 {
@@ -281,15 +280,45 @@ int add_dest2list(int id, str uri, struct socket_info *sock, int state,
 	return -1;
 }
 
+
+/* iterates the whole set and calculates (1) the number of
+   active destinations and (2) the running and total weight
+   sum for the active destinations */
+static inline void re_calculate_active_dsts(ds_set_p sp)
+{
+	int j,i;
+
+	/* pre-calculate the running weights for each destination */
+	for( j=0,i=-1,sp->active_nr=sp->nr ; j<sp->nr ; j++ ) {
+		/* running weight is the current weight plus the running weight of
+		 * the previous element */
+		sp->dlist[j].running_weight = sp->dlist[j].weight
+			+ ((j==0) ? 0 : sp->dlist[j-1].running_weight);
+		/* now the running weight for the active destinations */
+		if ( dst_is_active(sp->dlist[j]) ) {
+			sp->dlist[j].active_running_weight = sp->dlist[j].weight
+				+ ((i==-1) ? 0 : sp->dlist[i].active_running_weight);
+			i = j; /* last active destination */
+		} else {
+			sp->dlist[j].active_running_weight =
+				((i==-1) ? 0 : sp->dlist[i].active_running_weight);
+			sp->active_nr --;
+		}
+		LM_DBG("destination i=%d, j=%d , weight=%d, sum=%d, active_sum=%d\n",i,j,
+			sp->dlist[j].weight,
+			sp->dlist[j].running_weight,sp->dlist[j].active_running_weight);
+	}
+}
+
+
 /* compact destinations from sets for fast access */
 int reindex_dests( ds_data_t *d_data)
 {
 	int j;
-	int weight;
 	ds_set_p  sp = NULL;
 	ds_dest_p dp = NULL, dp0= NULL;
 
-	for( sp=d_data->sets ; sp!= NULL ; sp->dlist=dp0, sp=sp->next )
+	for( sp=d_data->sets ; sp!= NULL ; sp=sp->next )
 	{
 		if (sp->nr == 0) {
 			dp0 = NULL;
@@ -320,15 +349,9 @@ int reindex_dests( ds_data_t *d_data)
 			dp=NULL;
 		}
 
-		/* updated the weights (pre-calculate the weight limits)*/
-		for( j=0,weight=0 ; j<sp->nr ; j++ ) {
-			if (ds_use_default && dp0[j].next==NULL)
-				/* skip the last default record */
-				break;
-			dp0[j].weight += weight;
-			weight = dp0[j].weight;
-		}
-		sp->weight_sum = weight;
+		sp->dlist=dp0;
+
+		re_calculate_active_dsts(sp);
 
 	}
 
@@ -430,7 +453,7 @@ int ds_pvar_algo(struct sip_msg *msg, ds_set_p set, ds_dest_p **sorted_set)
 	}
 
 	for (i = 0, cnt = 0; i < set->nr - (ds_use_default?1:0); i++) {
-		if (set->dlist[i].flags & (DS_INACTIVE_DST|DS_PROBING_DST)) {
+		if ( !dst_is_active(set->dlist[i]) ) {
 			/* move to the end of the list */
 			sset[end_idx--] = &set->dlist[i];
 			continue;
@@ -1256,8 +1279,8 @@ static inline int push_ds_2_avps( ds_dest_t *ds )
  */
 int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 {
-	int i, cnt, i_unwrapped;
-	unsigned int ds_hash;
+	int i, j, cnt, i_unwrapped, set_size;
+	unsigned int ds_hash, ds_rand;
 	int_str avp_val;
 	int ds_id;
 	ds_set_p idx = NULL;
@@ -1286,7 +1309,6 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 		return -1;
 	}
 
-
 	/* access ds data under reader's lock */
 	lock_start_read( ds_lock );
 
@@ -1302,10 +1324,29 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 		goto error;
 	}
 
-	LM_DBG("set [%d]\n", ds_select_ctl->set);
+	if (idx->active_nr == 0) {
+		LM_DBG("no active destinations in set [%d] !\n", idx->id);
+		goto error;
+	}
+
+	/* calculate the real size of the set, depending on the USE_DEFAULT value
+	 * This size will be all the time higher than 0 (>=1) */
+	set_size =  (ds_flags&DS_USE_DEFAULT && idx->nr>1) ? idx->nr-1 : idx->nr ;
 
+	/* at this point we know for sure that we have
+	 * at least one  active destination */
+
+	LM_DBG("set [%d], using alg [%d], size [%d], used size [%d], "
+		"active size [%d]\n", ds_select_ctl->set, ds_select_ctl->alg, idx->nr,
+		set_size, idx->active_nr);
+
+	/* hash value used for picking the destination */
 	ds_hash = 0;
+	/* id of the destination candidate (still to check if active) */
 	ds_id = -1;
+	/* final selected destination */
+	selected = NULL;
+
 	switch(ds_select_ctl->alg)
 	{
 		case 0:
@@ -1337,8 +1378,8 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 			}
 		break;
 		case 4:
-			ds_id = idx->last;
-			idx->last = (idx->last+1) % idx->nr;
+			/* round robin */
+			ds_id = (idx->last+1) % set_size;
 		break;
 		case 5:
 			i = ds_hash_authusername(msg, &ds_hash);
@@ -1350,7 +1391,7 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 				case 1:
 					/* No Authorization found: Use round robin */
 					ds_id = idx->last;
-					idx->last = (idx->last+1) % idx->nr;
+					idx->last = (idx->last+1) % set_size;
 				break;
 				default:
 					LM_ERR("can't get authorization hash\n");
@@ -1382,7 +1423,7 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 				LM_ERR("can't get destination index\n");
 				goto error;
 			}
-			ds_id = 0;
+			selected = sorted_set[0];
 		break;
 		default:
 			LM_WARN("dispatching via [%d] with unknown algo [%d]"
@@ -1391,23 +1432,30 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 			ds_id = 0;
 	}
 
-	if (ds_id==-1) {
-		/* no destination yet actually selected -> do it based on hash */
-		if (idx->weight_sum==0) {
-			ds_id = ds_hash % idx->nr;
-		} else {
-			ds_hash = ds_hash%idx->weight_sum;
-			/* get the ds id based on weights */
-			for( ds_id=0 ; ds_id<idx->nr ; ds_id++ )
-				if (ds_hash<idx->dlist[ds_id].weight)
-					break;
+	/* any destination selected yet? */
+	if (selected==NULL) {
+
+		LM_DBG("hash [%u], candidate [%d], weight sum [%u]\n",
+			ds_hash, ds_id, idx->dlist[set_size-1].running_weight);
+
+		/* any candidate selected yet */
+		if (ds_id==-1) {
+			/* no candidate yet -> do it based on hash and weights */
+			if (idx->dlist[set_size-1].running_weight) {
+				ds_rand = ds_hash % idx->dlist[set_size-1].running_weight;
+				/* get the ds id based on weights */
+				for( ds_id=0 ; ds_id<set_size ; ds_id++ )
+					if (ds_rand<idx->dlist[ds_id].running_weight)
+						break;
+			} else {
+				/* get a candidate simply based on hash */
+				ds_id = ds_hash % set_size;
+			}
 		}
-	}
 
-	LM_DBG("alg hash [%u], id [%u]\n", ds_hash, ds_id);
-	cnt = 0;
+		LM_DBG("candidate is [%u]\n",ds_id);
 
-	if (ds_select_ctl->alg != 9) {
+		/* now we have a candidate, so we need to check if active or not */
 		i=ds_id;
 		while ( idx->dlist[i].flags&(DS_INACTIVE_DST|DS_PROBING_DST) )
 		{
@@ -1415,14 +1463,38 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 				if (idx->nr>1)
 					i = (i+1)%(idx->nr-1);
 			} else {
-				i = (i+1)%idx->nr;
+				/* use the hash and weights over active destinations only ;
+				 * if USE_DEFAULT is set, do a -1 if the default (last)
+				 * destination is active (we want to skip it) */
+				cnt = idx->active_nr - (ds_flags&DS_USE_DEFAULT &&
+					dst_is_active(idx->dlist[idx->nr-1]))?1:0 ;
+				if (cnt) {
+					/* weights or not ? */
+					if (idx->dlist[set_size-1].active_running_weight) {
+						ds_rand = ds_hash %
+							idx->dlist[set_size-1].active_running_weight;
+						/* get the ds id based on active weights */
+						for( i=0 ; i<set_size ; i++ )
+							if ( dst_is_active(idx->dlist[i]) &&
+							(ds_rand<idx->dlist[i].active_running_weight) )
+								break;
+					} else {
+						j = ds_hash % cnt;
+						/* translate this index to the full set of dsts */
+						for ( i=0 ; i<set_size ; i++ ) {
+							if ( dst_is_active(idx->dlist[i]) ) j--;
+							if (j<0) break;
+						}
+					}
+				}
+				/* i reflects the new candidate */
 			}
 			if(i==ds_id)
 			{
 				if(ds_use_default!=0)
 				{
 					i = idx->nr-1;
-					if (idx->dlist[i].flags&(DS_INACTIVE_DST|DS_PROBING_DST))
+					if (!dst_is_active(idx->dlist[i]))
 						goto error;
 					break;
 				} else {
@@ -1430,12 +1502,17 @@ int ds_select_dst(struct sip_msg *msg, ds_select_ctl_p ds_select_ctl)
 				}
 			}
 		}
+		LM_DBG("using destination [%u]\n",i);
 		ds_id = i;
 		selected = &idx->dlist[ds_id];
-	} else {
-		selected = sorted_set[0];
 	}
 
+	/* remember the last used destination */
+	idx->last = ds_id;
+
+	/* start pushing the destinations to SIP level */
+	cnt = 0;
+
 	if(ds_select_ctl->set_destination
 		&& ds_update_dst(msg, &selected->uri, selected->sock, ds_select_ctl->mode)!=0)
 	{
@@ -1611,6 +1688,7 @@ int ds_mark_dst(struct sip_msg *msg, int mode)
 		return -1; /* dst avp deleted -- strange */
 
 	if(mode==1) {
+		/* set as "active" */
 		ret = ds_set_state(group, &avp_value.s,
 				DS_INACTIVE_DST|DS_PROBING_DST, 0);
 	} else if(mode==2) {
@@ -1700,8 +1778,16 @@ int ds_set_state(int group, str *address, int state, int type)
 				idx->dlist[i].flags |= state;
 			else
 				idx->dlist[i].flags &= ~state;
-			if ( idx->dlist[i].flags != old_flags)
+			if ( idx->dlist[i].flags != old_flags) {
+				/* state actually changed -> do all updates */
 				idx->dlist[i].flags |= DS_STATE_DIRTY_DST;
+				/* update info on active destinations */
+				if ( ((old_flags&(DS_PROBING_DST|DS_INACTIVE_DST))?0:1) !=
+				((idx->dlist[i].flags&(DS_PROBING_DST|DS_INACTIVE_DST))?0:1) )
+					/* this destination switched state between disabled <> enabled
+					   -> update active info */
+					re_calculate_active_dsts( idx );
+			}
 
 			if (dispatch_evi_id == EVI_ERROR) {
 				LM_ERR("event not registered %d\n", dispatch_evi_id);
@@ -1805,8 +1891,7 @@ int ds_is_in_list(struct sip_msg *_m, pv_spec_t *pv_ip, pv_spec_t *pv_port,
 					|| port==list->dlist[j].ports[k]) &&
 					ip_addr_cmp( ip, &list->dlist[j].ips[k]) ) {
 						/* matching destination */
-						if (active_only &&
-						(list->dlist[j].flags&(DS_INACTIVE_DST|DS_PROBING_DST)) )
+						if (active_only && !dst_is_active(list->dlist[j]) )
 							continue;
 						if(set==-1 && ds_setid_pvname.s!=0) {
 							val.ri = list->id;
@@ -2048,7 +2133,7 @@ int ds_count(struct sip_msg *msg, int set_id, const char *cmp, pv_spec_p ret)
 
 	for (dst = set->dlist; dst; dst = dst->next)
 	{
-		if (!(dst->flags & (DS_INACTIVE_DST|DS_PROBING_DST)))
+		if ( dst_is_active(*dst) )
 		{
 			active++;
 
diff --git a/modules/dispatcher/dispatch.h b/modules/dispatcher/dispatch.h
index 5afdd560aac..77955fdb1ad 100644
--- a/modules/dispatcher/dispatch.h
+++ b/modules/dispatcher/dispatch.h
@@ -46,6 +46,8 @@
 
 #define DS_HASH_USER_ONLY	1  /* use only the uri user part for hashing */
 #define DS_FAILOVER_ON		2  /* store the other dest in avps */
+#define DS_USE_DEFAULT		4  /* use last address in destination set as last option */
+#define DS_FORCE_DST		8  /* force overwriting the destination address */
 
 #define DS_INACTIVE_DST		1  /* inactive destination */
 #define DS_PROBING_DST		2  /* checking destination */
@@ -67,7 +69,10 @@ typedef struct _ds_dest
 	str uri;
 	str attrs;
 	int flags;
-	int weight;
+	unsigned short weight;
+	unsigned short running_weight;
+	unsigned short active_running_weight;
+	unsigned short priority;
 	struct socket_info *sock;
 	struct ip_addr ips[DS_MAX_IPS]; /* IP-Address of the entry */
 	unsigned short int ports[DS_MAX_IPS]; /* Port of the request URI */
@@ -81,8 +86,8 @@ typedef struct _ds_set
 {
 	int id;				/* id of dst set */
 	int nr;				/* number of items in dst set */
+	int active_nr;		/* number of active items in dst set */
 	int last;			/* last used item in dst set */
-	int weight_sum;		/* sum of the weights from dst set */
 	ds_dest_p dlist;
 	struct _ds_set *next;
 } ds_set_t, *ds_set_p;
