rar format: asan error for ./jtrts.pl -random -seed=987135 -type rar
We could look for updates in clamav unrar before doing anything else but their code is not used as-is.

I synced from upstream but that did not fix the issue.

It's kinda annoying that GitHub auto-closes the issue from the phrase "This did not fix #1000."

I think I have a 'proper' fix for this now:  I have a line that prints 'out of data' when the problem is detected.  Here is what I now see (after rolling back to alloc == size + 2)

```
12345            (u25-rar)
12345            (u25-rar)
d�ffff�ff�fff    (u21-rar)
d�ffff�ff�fff    (u21-rar)
d�ffff�ff�fff    (u21-rar)
Bert$ErnIE       (u3-rar)
Bert$ErnIE       (u3-rar)
Bert$ErnIE       (u3-rar)
out of data
out of data
out of data
out of data
out of data
Bert$ErnIE       (u3-rar3hp)
Bert$ErnIE       (u3-rar3hp)
Bert$ErnIE       (u3-rar)
```

No ASAN, and no crash.  The only change is in rarvm_getbits() function.  I will get that checked in shortly.

This aa9589e should be proper fix for both #1000 and #1001 
