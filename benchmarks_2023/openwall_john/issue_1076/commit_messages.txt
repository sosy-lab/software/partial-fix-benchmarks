sha256crypt-cuda: Bump max length to 23. See #1077. This format
probably should be able to do 24, but then it loses some in the
Test Suite due to a bug somewhere.
Pending core change in cracker.c. Ensure get_key() is not called
(via status.c) after clear_keys() was called. Closes #1076.

http://www.openwall.com/lists/john-dev/2015/03/09/10
Pending core change in cracker.c. Ensure get_key() is not called
(via status.c) after clear_keys() was called. Take 2. Closes #1076.

http://www.openwall.com/lists/john-dev/2015/03/09/10
