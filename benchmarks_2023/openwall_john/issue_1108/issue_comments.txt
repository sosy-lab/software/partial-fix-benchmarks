Format valid problem by strtok()
This has been a bug IMHO in strtok since the 70's.   Write a correct one, which returns "" (not null) for multiple.

Here is code that we 'could' use to replace strtok.  This one does NOT merge adjacent. It is 100% same interface as strtok (with ALL of the bugs of that function, such as corruption of input string, having a static pointer inside the function, etc).  But it is a replacement that 'works'.

``` C
#include <stdio.h>
#include <string.h>

char *strtokm(char *s1, const char *delimit)
{
    static char *last = NULL;
    char *endp;

    if (!s1) {
        s1 = last;
        if (!s1)
            return NULL;
    }// else
    //    s1 += strspn(s1, delimit);
    endp = strpbrk(s1, delimit);
    if (endp) {
        *endp = '\0';
        last = endp + 1;
    } else
        last = NULL;
    return s1;
}

int main() {
    char Buf2[512], *cp;
    int i;

    strcpy(Buf2, "**token*2***token5*xxx**");
    printf ("tokenizing '%s' with * char using strtok\n", Buf2);
    cp = strtok(Buf2, "*");
    i = 0;
    if (cp)
    do {
        printf ("%d - '%s'\n", i++, cp);
        cp = strtok(NULL, "*");
    } while (cp);

    strcpy(Buf2, "**token*2***token5*xxx**");
    printf ("tokenizing '%s' with * char using strtokm\n", Buf2);
    cp = strtokm(Buf2, "*");
    i = 0;
    if (cp)
    do {
        printf ("%d - '%s'\n", i++, cp);
        cp = strtokm(NULL, "*");
    } while (cp);
}
```

It would be easy to put strtokm() into misc.[ch], and then modify all the code in jtr to use this function.

Here is the output from the above program:

```
tokenizing '**token*2***token5*xxx**' with * char using strtok
0 - 'token'
1 - '2'
2 - 'token5'
3 - 'xxx'
tokenizing '**token*2***token5*xxx**' with * char using strtokm
0 - ''
1 - ''
2 - 'token'
3 - '2'
4 - ''
5 - ''
6 - 'token5'
7 - 'xxx'
8 - ''
9 - ''
```

@jfoug Thanks. strtokm() can help to avoid this issue. Should we **change strtok() -> strtokm()** ? 

Let me check this in.  Should be a trivial change.

Note, code (and output changed).  I was stripping off leading empty tokens. That code is removed, the check strings chagned, and output redone.

There's a **lot** of formats using strtok but we could change them with some ninja one-liner (and obviously carefully review - as well as test - the changes before committing).

I will be putting this into misc.[ch]   In misc.h I will simply add this define (for now)

#define strtok(a,b) strtokm(a,b)

This allows us to do testing, to make sure things are 'happy', prior to changing a couple hundred lines of code.

I have not tested yet,   I wrote this code outside of JtR, so it needs testing.  The 1 liner will localize changes until we are 'happy', also, simply comment that line out and all changes are gone, and we are back to the CRTL strtok.

Very early testing (just -test=0 stuff for non GPU)

```
Testing: Blockchain, My Wallet (x10) [PBKDF2-SHA1 AES 8x SSE2]... (8xOMP) FAILED (promiscuous valid)
Testing: django-scrypt [Salsa20/8 128/128 AVX]... (8xOMP) FAILED (cmp_all(1))
Testing: gpg, OpenPGP / GnuPG Secret Key [32/64]... (8xOMP) FAILED (valid (before init))
Testing: kwallet, KDE KWallet [SHA1 32/64]... (8xOMP) FAILED (promiscuous valid)
Testing: Mozilla, Mozilla key3.db [SHA1 3DES 32/64]... (8xOMP) FAILED (valid (before init))
Testing: skey, S/Key [MD4/MD5/SHA1/RMD160 32/64]... FAILED (cmp_all(1))
Testing: SSH-ng [RSA/DSA 32/64]... (8xOMP) FAILED (promiscuous valid)
Testing: STRIP, Password Manager [PBKDF2-SHA1 8x SSE2]... (8xOMP) FAILED (valid (before init))
```

The promiscuous valids are almost certainly bugs that changing the strtok has flushed out. The others may also be bugs.  Note, opencl/cuda formats were not checked.

Actually these might be false positives from the "promiscuous valid" test. We might need to re-write it.

It is the one that adds $$$$$$$

Check out 343b1b3   The !len is the promiscuous valid. I also added a ishex(p) to validate the hex string.

django-scrypt is because in salt, we move pointer from scrypt$..... to $..... and the new code does not skip the leading delimiters.  We just need to skip that delimiter also.

I am 90% sure that ALL issues can easily be corrected. They will be items like this.

@magnumripper   Question:

I changed code to return empty for any leading delims, and any trailing delims.  But we are seeing problems, because there was code written to work with this 'buggy' behavior.

example is gpg  (in valid and get_salt).  Here is example)

``` C
char *cipher = "$gpg$*x*y*z";
cp = cipher+5;
cp  strtok(cp, "*");
.....
```

So cp starts out pointing at the '*' char. Since I changed strtokm to tokenize leading items, the way to 'fix' this is to either skip leading delims (causing strange behavior IMHO in the strtokm), OR to fix the code to skip the delim.

My vote is to change the format.  BUT this is a change in interface between strtokm and strtok.

I am open to suggestions.

the problem in skey is a little more involved. it "MAY" need original strtok. There is a 1 space gap, then a 2 space gap. Now, this is just in the test strings, BUT it may be that way for real!!

I have done tiny 'patches' to all formats listed above. For the SKEY format, I am going to use strtok, that combines the delimiters. I do not know the 'real' layout of the format, so we should use the original function here, as that buggy behavior 'may' be right in this case, since we are dealing with white space.

NOTE, all the tiny changes I have made (and will be checking in), work 100% fine with native strtok.  But they were needed for the small behavior changes done within strtokm.  The biggest 'nit' happens to be that skip leading delimiters.  We have a lot of code that depended upon that 'buggy' (IMHO) behavior.  

@magnumripper could there be any files that do not include 'misc.h'  that do a strtok??

Almost all are _fmt_plug.c  but there are other usage outside of those.   Is there any 'easy' way to check to see if misc.h gets included in all of these files?   I think I am ready to check in the strtokm()  AND since it is just a #define, and we have changed no source code to use it (outside of the define), then it should be fully safe.  If we see any issues, comment out one header line, and rebuild.

But finding a way to easily tell if all usages are using strtokm() it would be helpful.

Easily answered with a one-liner[tm]

```
$ git grep -l strtok | xargs grep -L misc.h
blockchain_fmt_plug.c
clipperz_srp_fmt_plug.c
dmg_fmt_plug.c
openbsdsoftraid_fmt_plug.c
opencl_blockchain_fmt_plug.c
opencl_dmg_fmt_plug.c
plugin.c
rar5_common.h
raw2dyna.c
ssh_ng_fmt_plug.c
```

> @magnumripper   Question:
> 
> I changed code to return empty for any leading delims, and any trailing delims.  But we are seeing problems, because there was code written to work with this 'buggy' behavior.
> 
> example is gpg  (in valid and get_salt).  Here is example)
> 
> ``` C
> char *cipher = "$gpg$*x*y*z";
> cp = cipher+5;
> cp  strtok(cp, "*");
> .....
> ```
> 
> So cp starts out pointing at the '*' char. Since I changed strtokm to tokenize leading items, the way to 'fix' this is to either skip leading delims (causing strange behavior IMHO in the strtokm), OR to fix the code to skip the delim.
> 
> My vote is to change the format.  BUT this is a change in interface between strtokm and strtok.

A good number of @kholia's early formats had this bug (an unneeded first \* after the $tag$) and Solar had to nag about it for quite some time before Dhiru stopped doing that. As we probably do not want to change the format syntax, me thinks we should just consider it as the **tag** being an actual `$gpg$*`. So the code snippet you refer to would just have +6 instead of +5. Would this not solve it without any ill effects?

> you refer to would just have +6 instead of +5

I did exactly that. Only a handful.  I really hate to suck up the leading delims.  I see that as a side affect of the poor design of merging the delims (same for trailing).    I have left my code as it is, and it will give you a token (possibly empty) for EVERY delim in the string.

I have checked in all files that 'required' to be changed. I have not checked in misc.[ch] which actually have the strtokm() function in it (yet).

> $ git grep -l strtok | xargs grep -L misc.h

But that does not account for other things that include misc.h  But it is a good start.  I can put an #error in there, and if I compile these files, if they do not error, then I know that they are using the native strtok() until we change code to actually 'force' use strtokm()  i.e. they would not see the macro.

> But that does not account for other things that include misc.h

True. The main thing that in turn includes misc.h, is formats.h.

More one-liner-fu:

```
$ git grep -l strtok *c | xargs grep -EL "misc.h `git grep -l misc.h *.h`"
config.c
encfs_common_plug.c
office_common_plug.c
options.c
plugin.c
raw2dyna.c
```

Still not recursive though :smile: but I think this is very close to the true list. For some reason it's totally different from the original list though?!

> For some reason it's totally different from the original list though?!

This one should be safe. No format needs a fix.

```
$ git grep -l strtok | xargs grep -L misc.h | xargs grep -L formats.h
plugin.c
raw2dyna.c
```

@jfoug  thank you very much for your reply and commits on the strtok() problem. You have made some work on use strtokm() instead of strtok(). I think it's great for there will be less bugs. I will pay attention to the changes of strtok(). 

@jfoug you never committed the strtokm(), did you?

2f7c5e4 adds it

NOTE, this is all done with a #define. We should probably go into code and switch the strtok calls to strtokm at some time.  There was ONE location where original strtok IS being used. That is in the SKEY format.  There is a pair of spaces at one point.  We 'could' write code to handle this, BUT I am not 100% sure that it is always 2 bytes. It may be 1, 2, or X. Not having native input strings makes it sort of hard to do. But this IS the case where original strtok shines. It does not care how many delims are there, it will merge any arbitrary amount > 0

However, I do feel that the strtokm function IS the function that strtok should have been from the start.  Or it might have been good to have a param that controlled how strtok handled leading, trailing and adjacent tokens. I know that strtok is one of my favorite functions, but it is UNUSABLE for many very common and useful file formats, such as CSV and that really is what many of the JTR input strings are (CSV but using $ or \* instead of a tab or comma).

@jfoug  I think there are two valid() functions should be changed to work with strtokm()

**opencl_strip_fmt_plug.c**

``` c
if (strncmp(ciphertext, "$strip$", 7))
        return 0;
    ctcopy = strdup(ciphertext);
    keeptr = ctcopy;
    ctcopy += 7;
```

**opencl_gpg_fmt_plug.c**

``` c
if (strncmp(ciphertext, "$gpg$", 5) != 0)
        return 0;
    ctcopy = strdup(ciphertext);
    keeptr = ctcopy;
    ctcopy += 5;    /* skip over "$gpg$" marker */
```

Those and blockchain-opencl. I have fixed them, committing soon.

So can we close this now?

It would be nice to convert all strtok() into strtokm() except for the few instances where we WANT strtok() behavior. But as far as this but, we now 'have' a correct strtok function that properly tokenizes 'optional' fields, ie adjacent delimiters.  Also, if there are a block of multiple delimiters, it will not pass valid with the new strtok, if there are not supposed to be multiples.

I forgot that! Fixed in 743f3d0. It was only SKEY, right? That was the only "undef" I found.

SKEY was the only format I found that I 'think' may need the strtok logic.  It also could be that the hashes we had were typed in by hand by someone, and the 2 space is a fat finger problem. I  really do NOT know.  If I KNEW that it was 2 char spaces, then it is easy. But if it is 1, 2 or X spaces, then it is a little more complex, but can work either way, it is just simpler with the 'lossy' method of strtok.  strtok was actually written TO parse arbitrary text into tokens, with variable spacing and stuff.  It is great for that (and does ltrim and rtrim also). But for truely tokenized data like CSV type stuff, it is no good.

But I saw no other formats where the dupe delimiters being smashed was correct logic, so yes, for all other strtok's, I 'think' strtokm is the correct function

NOTE, if we are using strtok for reading things like word lists, or other input files, that are NOT input hashes, then we might want to look at that code, instead of simply doing a global search/replace.  However, since I used the #define to change them all, it should have caught anything, OTHER than the fact that we really do not have coverage QA testing, other than running the TS, which misses a LOT of code.  So, I think the formats are solid (with SKEY still wanting old strtok), but we should make sure strtokm() IS the correct thing for the other parts of JtR.

I only replace "all" strtok's in formats. We need to carefully review the rest, or leave them alone.

```
$ git grep -l "strtok("
SKEY_fmt_plug.c
config.c
misc.h
mkv.c
options.c
plugin.c
raw2dyna.c
```

Hmm the PDF format segfaults now, on the build bots. I wonder why. They did not segfault with that #define in place!

Oh, it segfaults on my gear too now.

Problem was a `strtok (` that did not get replaced due to the space, and since it used NULL but the preceeding one was now `strtokm(`, well, no wonder it crashed and burned.

I checked for other instances of that, nothing more.
