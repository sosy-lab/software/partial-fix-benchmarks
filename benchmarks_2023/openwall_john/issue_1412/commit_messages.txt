django-scrypt:  if N too large, the kdf aborts, but we were not trapping it.  Fixes 1413.  This probably should be in valid also
pbkdf2-hmac-sha1: stack overflow possibile: Fix #1411 and #1412
opencl_pkbdf2_sha1: Same fix as done to core. Fix #1411   Fix #1412
