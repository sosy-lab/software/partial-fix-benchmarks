Drop a level macro that broke all real builds.
We can't use OpenCL 1.2 commands without wrapping them in #ifdef.
Addresses #1470.
OpenCL: Add a -D__MESA__ for MESA platforms. And use it to
enforce "safe goto" DES kernel version for decrypt.
See #1470.
Since Mesa incorrectly claims supporting OpenCL 1.2, add an
autoconf function test checking for actual presence in the
libs found. See #1470.
