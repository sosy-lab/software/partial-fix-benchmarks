Enable 4byte utf-8 characters.  Surprisingly, I saw a very slight performance gain (in -test mode)
 Handle 4 byte utf-8 characters #685
Loader and wordlist/PRINCE modes: Refuse to eat UTF-16 files. Wordlist
mode also had the UTF-8 BOM-skip extended to every line in the file (for
cases where luser concatenated several infected files). Also, the checks
were simplified/optimized, mitigating the performance hit. Closes #2802.
Relax the UTF-16 BOM check in wordlist and PRINCE modes to only warn, not
bail.  The rationale is some low quality wordlists contain a mix of
encodings but likely mostly UTF-8.  See #2812, #2802.
