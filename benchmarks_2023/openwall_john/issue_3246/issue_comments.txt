(cross) compiling with mingw and opencl fails
Thanks for sharing, good info. I'll review this when I get time and commit some of your suggestions. Those `uint` in host code are a pure bugs though, the real fix is changing them to `uint32_t`.
@BotPass can you please post the output of `john --list=build-info` and `john --list=opencl-devices`? Thanks!
I'm currently trying to find the cause of the segfault. I know it occurs in ```opencl_common.c:1111```, by 
```
MEM_FREE(full_path);
```
in ```include_source```. I could even hunt it down throw mingw source and saw that that the pointer returned from ```malloc``` to mingw is the same that is given to ```free``` by mingw.
The error occurs somewhere in the depth of ntdll.dll, after calling ```msvcrt!free```.
The only thing I found is that 
```
memcpy(full_path, john_home_path, PATH_BUFFER_SIZE);
```
in path.c (multiple times, e.g. in ```path_expand_safe```, used by ```include_source```) seems to copy uninitialized memory. If I'm not mistaken ```full_path``` is (in the end) allocated with malloc and thus uninitialized memory and later only the beginning is overwritten, but the code above copy it all. 
I know that MS Visual Studio does memory checks in debug build, but I thought the default build of john with mingw (not Visual Studio) should be in release, even though gdb does know function names and line numbers (I am not so familiar with mingw/gcc).
It's strange, very strange. Maybe 32 and 64 is mixed by linking or dll or ... what ever, I don't know up to now.

As a result I modified my build (e.g. adding ```-g -O0```). I tried to redo the work above in ```/build/jtr_``` in msys2 (under /dev/msys2) to answers your questions.

```
$ ./john.exe --list=build-info
Version: 1.8.0.13-jumbo-1-bleeding-705cfab580 2018-05-10 22:15:50 +0200
Build: mingw32 64-bit x86_64 AVX AC OMP
SIMD: AVX, interleaving: MD4:3 MD5:3 SHA1:1 SHA256:1 SHA512:1
CPU tests: AVX
$JOHN is /dev/msys2/build/jtr_/run/
Format interface version: 14
Max. number of reported tunable costs: 4
Rec file version: REC4
Charset file version: CHR3
CHARSET_MIN: 1 (0x01)
CHARSET_MAX: 255 (0xff)
CHARSET_LENGTH: 24
SALT_HASH_SIZE: 1048576
Max. Markov mode level: 400
Max. Markov mode password length: 30
gcc version: 7.3.0
OpenCL headers version: 2.2
Crypto library: OpenSSL
OpenSSL library version: 0100020ff
OpenSSL 1.0.2o  27 Mar 2018
GMP library version: 6.1.2
File locking: NOT supported by this build - do not run concurrent sessions!
fseek(): fseeko64
ftell(): ftello64
fopen(): fopen64
memmem(): JtR internal
```
```
$ ./john.exe --list=opencl-devices
Platform #0 name: NVIDIA CUDA, version: OpenCL 1.2 CUDA 8.0.0
    Device #0 (0) name:     GeForce GTX 1060 6GB
    Device vendor:          NVIDIA Corporation
    Device type:            GPU (LE)
    Device version:         OpenCL 1.2 CUDA
    Driver version:         382.33 [recommended]
    Native vector widths:   char 1, short 1, int 1, long 1
    Preferred vector width: char 1, short 1, int 1, long 1
    Global Memory:          6 GB
    Global Memory Cache:    160 KB
    Local Memory:           48 KB (Local)
    Constant Buffer size:   64 KB
    Max memory alloc. size: 1.5 GB
    Max clock (MHz):        1771
    Profiling timer res.:   1000 ns
    Max Work Group Size:    1024
    Parallel compute cores: 10
    CUDA cores:             1280  (10 x 128)
    Speed index:            2266880
    Warp size:              32
    Max. GPRs/work-group:   65536
    Compute capability:     6.1 (sm_61)
    Kernel exec. timeout:   yes
    PCI device topology:    01:00.0
```

And at least I modified my patch a little bit. At first I modified opencl_common.c to expand the cygwin code to mingw, too. And I unwind my uint type modification and replaced it by exchanging uint with uint32_t in a few files. As I see, you already did this.

[jtr_mingw_opencl_2_patch.txt](https://github.com/magnumripper/JohnTheRipper/files/2006300/jtr_mingw_opencl_2.txt)

@BotPass I committed your changes (actually your first patch except the uint stuff) but this broke MinGW builds on our build bots at Travis CI and CircleCI. So I reverted the `Makefile.in` changes. I can't do more about that issue - I never touch Windows, ever. Perhaps @kholia can figure it out when he gets some time (unless you can, of course!).

File name suffixes is so 1982. 
Must be one of these which went wrong:
```
 ~/git/JtR$ grep -n -C 1 -i "mv.*run/john" .circle/CircleCI-MinGW.sh 
68-mingw32 make -sj4
69:mv -v ../run/john ../run/john-avx.exe
70-make clean; make distclean
--
74-mingw32 make -sj4
75:mv -v ../run/john ../run/john-avx2.exe
76-make clean; make distclean
--
82-mingw64 make -sj4
83:mv -v ../run/john ../run/john-sse2.exe
84-
85-# AVX2 is default, but with CPU fallback
86:mv -v ../run/john-avx2.exe ../run/john.exe
87-
--
118-# restore the sse2 build for testing purposes
119:mv -v run/john-sse2.exe run/john.exe
120-
```
Interesting! That means the reverted Makefile change was actually correct and the CircleCI script is now wrong?
Yes, I think that's the case here. `s#mv -v ../run/john #mv -v ../run/john.exe #`
And there's also `../run/john -test-full=0` which must be changed, I guess.
No, the ` ../run/john -test-full=0` instances are for Linux builds, don't change these!

BTW: Why isn't it possible to edit my previous comment? 
The edit is just a little hidden now, behind the three dots, no?

Things like `../run/john` will work without extension. Except for some CP/M and similar systems, it would need to be `..\run\john`
