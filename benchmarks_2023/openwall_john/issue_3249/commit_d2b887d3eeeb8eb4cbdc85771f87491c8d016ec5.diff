diff --git a/doc/NEWS b/doc/NEWS
index 45d9697d7e..d2ccc08d35 100644
--- a/doc/NEWS
+++ b/doc/NEWS
@@ -50,6 +50,9 @@ Major changes from 1.9.0-jumbo-1 (May 2019) in this bleeding-edge version:
   or decrease). If that option isn't set but SingleMaxBufferSize is explicitly
   set to zero, no limit will be applied!  [magnum; 2019]
 
+- When running "-test -mask" benchmark, don't disable self-tests.  Instead
+  perform a non-mask self-test before the mask-mode benchmark.  [magnum; 2019]
+
 
 Major changes from 1.8.0-jumbo-1 (December 2014) to 1.9.0-jumbo-1 (May 2019):
 
diff --git a/src/bench.c b/src/bench.c
index 586d3c36c4..9fd3b39be8 100644
--- a/src/bench.c
+++ b/src/bench.c
@@ -348,7 +348,6 @@ char *benchmark_format(struct fmt_main *format, int salts,
 	static int binary_size = 0;
 	static char s_error[128];
 	static int wait_salts = 0;
-	char *where;
 	struct fmt_tests *current;
 	int pass;
 #if OS_TIMER
@@ -418,10 +417,12 @@ char *benchmark_format(struct fmt_main *format, int salts,
 
 	current = format->params.tests;
 #endif
+#ifdef BENCH_BUILD
 	if ((where = fmt_self_test(format, test_db))) {
 		snprintf(s_error, sizeof(s_error), "FAILED (%s)\n", where);
 		return s_error;
 	}
+#endif
 	if (!current->ciphertext)
 		return "FAILED (no ciphertext in test vector)";
 	if (!current->plaintext)
@@ -717,16 +718,6 @@ int benchmark_all(void)
 
 	benchmark_running = 1;
 
-#if defined(HAVE_OPENCL)
-	if (!benchmark_time) {
-		/* This will make the majority of OpenCL formats
-		   also do "quick" benchmarking. But if LWS or
-		   GWS was already set, we do not overwrite. */
-		setenv("LWS", "7", 0);
-		setenv("GWS", "49", 0);
-	}
-#endif
-
 #ifndef BENCH_BUILD
 #if defined(WITH_ASAN) || defined(WITH_UBSAN) || defined(DEBUG)
 	if (benchmark_time)
@@ -784,25 +775,17 @@ int benchmark_all(void)
 			fmt_init(format);
 
 		/* [GPU-side] mask mode benchmark */
-		if (options.flags & FLG_MASK_CHK) {
-			static struct db_main fakedb;
-
-			fakedb.format = format;
-			mask_init(&fakedb, options.mask);
+		if (options.mask) {
+			static char benchmark_comment[16];
+			int bl = format->params.benchmark_length & 0x7f;
+			int el = mask_calc_len(options.mask);
 
-			if (options.mask) {
-				static char benchmark_comment[16];
-				int bl = format->params.benchmark_length & 0x7f;
-				int el = mask_calc_len(options.mask);
-
-				if (options.flags & FLG_MASK_STACKED)
-					el = MAX(el, bl);
-
-				sprintf(benchmark_comment, " (length %d)", el);
-				format->params.benchmark_comment =
-					benchmark_comment;
-			}
+			if (options.flags & FLG_MASK_STACKED)
+				el = MAX(el, bl);
 
+			sprintf(benchmark_comment, " (length %d)", el);
+			format->params.benchmark_comment =
+				benchmark_comment;
 		}
 #endif
 
@@ -885,6 +868,36 @@ int benchmark_all(void)
 
 		test_db = ldr_init_test_db(format, NULL);
 
+#ifndef BENCH_BUILD
+		if ((result = fmt_self_test(format, test_db))) {
+			printf("FAILED (%s)\n", result);
+			failed++;
+			goto next;
+		}
+
+		if (john_main_process) {
+			printf("%s%s",
+			       (options.flags & FLG_NOTESTS) ? "SKIP" : "PASS",
+			       benchmark_time ? ", " : "\n");
+			fflush(stdout);
+		}
+
+		if (!benchmark_time)
+			goto next;
+
+		/*
+		 * Re-init for benchmark.  We need to trigger a proper auto-tune
+		 * for benchmark, with mask or not as appropriate
+		 */
+		fmt_done(format);
+
+		/* Re-init with mask mode if applicable */
+		if (options.flags & FLG_MASK_CHK)
+			mask_init(test_db, options.mask);
+
+		fmt_init(format);
+		format->methods.reset(test_db);
+#endif
 		if ((result = benchmark_format(format, salts,
 		                               &results_m, test_db))) {
 			puts(result);
@@ -954,8 +967,8 @@ int benchmark_all(void)
 #endif
 		}
 
-		if (john_main_process)
-			printf(benchmark_time ? "DONE\n" : "PASS\n");
+		if (john_main_process && benchmark_time)
+			puts("DONE");
 #ifdef _OPENMP
 		// reset this in case format capped it (we may be testing more formats)
 		omp_set_num_threads(ompt_start);
diff --git a/src/formats.c b/src/formats.c
index cb035e3dd7..e3e3bfd1a5 100644
--- a/src/formats.c
+++ b/src/formats.c
@@ -69,11 +69,9 @@ void fmt_init(struct fmt_main *format)
 
 	if (!format->private.initialized) {
 #ifndef BENCH_BUILD
-		if (options.flags & FLG_LOOPTEST) {
-			orig_min = format->params.min_keys_per_crypt;
-			orig_max = format->params.max_keys_per_crypt;
-			orig_len = format->params.plaintext_length;
-		}
+		orig_min = format->params.min_keys_per_crypt;
+		orig_max = format->params.max_keys_per_crypt;
+		orig_len = format->params.plaintext_length;
 #endif
 		if (!fmt_raw_len)
 			fmt_raw_len = format->params.plaintext_length;
@@ -81,7 +79,7 @@ void fmt_init(struct fmt_main *format)
 #ifndef BENCH_BUILD
 		/* NOTE, we have to grab these values (the first time), from after
 		   the format has been initialized for thin dynamic formats */
-		if (options.flags & FLG_LOOPTEST && orig_len == 0 && format->params.plaintext_length) {
+		if (orig_len == 0 && format->params.plaintext_length) {
 			orig_min = format->params.min_keys_per_crypt;
 			orig_max = format->params.max_keys_per_crypt;
 			orig_len = format->params.plaintext_length;
@@ -134,13 +132,10 @@ void fmt_done(struct fmt_main *format)
 		opencl_done();
 #endif
 #ifndef BENCH_BUILD
-		if (options.flags & FLG_LOOPTEST) {
-			format->params.min_keys_per_crypt = orig_min;
-			format->params.max_keys_per_crypt = orig_max;
-			format->params.plaintext_length = orig_len;
-		}
+		format->params.min_keys_per_crypt = orig_min;
+		format->params.max_keys_per_crypt = orig_max;
+		format->params.plaintext_length = orig_len;
 #endif
-
 	}
 	fmt_raw_len = 0;
 }
@@ -384,12 +379,6 @@ static char *fmt_self_test_body(struct fmt_main *format,
 	if (format->private.initialized == 2)
 		return NULL;
 #endif
-#if defined(HAVE_OPENCL)
-	if (strcasestr(format->params.label, "-opencl") &&
-	    !strstr(format->params.label, "-opencl")) {
-		return "-opencl suffix must be lower case";
-	}
-#endif
 #ifndef BENCH_BUILD
 	if (options.flags & FLG_NOTESTS) {
 		self_test_running = 0;
@@ -407,8 +396,15 @@ static char *fmt_self_test_body(struct fmt_main *format,
 		return NULL;
 	}
 #endif
+#if defined(HAVE_OPENCL)
+	if (strcasestr(format->params.label, "-opencl") &&
+	    !strstr(format->params.label, "-opencl")) {
+		return "-opencl suffix must be lower case";
+	}
+#endif
 
-	if (!(current = format->params.tests)) return NULL;
+	if (!(current = format->params.tests))
+		return NULL;
 	ntests = 0;
 	while ((current++)->ciphertext)
 		ntests++;
@@ -1596,6 +1592,16 @@ char *fmt_self_test(struct fmt_main *format, struct db_main *db)
 	char *retval;
 	void *binary_alloc, *salt_alloc;
 	void *binary_copy, *salt_copy;
+#if HAVE_OPENCL
+	static char *orig_lws, *orig_gws;
+
+	orig_lws = getenv("LWS");
+	orig_gws = getenv("GWS");
+
+	/* Force quick self-test without auto-tune */
+	setenv("LWS", "7", 1);
+	setenv("GWS", "49", 1);
+#endif
 
 	binary_copy = alloc_binary(&binary_alloc,
 	    format->params.binary_size?format->params.binary_size:1, format->params.binary_align);
@@ -1608,6 +1614,18 @@ char *fmt_self_test(struct fmt_main *format, struct db_main *db)
 
 	self_test_running = 0;
 
+#if HAVE_OPENCL
+	/* Reset original values */
+	if (orig_lws)
+		setenv("LWS", orig_lws, 1);
+	else
+		unsetenv("LWS");
+	if (orig_gws)
+		setenv("GWS", orig_gws, 1);
+	else
+		unsetenv("GWS");
+#endif
+
 	MEM_FREE(salt_alloc);
 	MEM_FREE(binary_alloc);
 
diff --git a/src/mask.c b/src/mask.c
index c98df4aae0..318391f7c1 100644
--- a/src/mask.c
+++ b/src/mask.c
@@ -49,6 +49,8 @@ struct db_main *mask_db;
 static int mask_bench_index;
 static int parent_fix_state_pending;
 static unsigned int int_mask_sum, format_cannot_reset;
+static int using_default_mask;
+
 
 int mask_add_len, mask_num_qw, mask_cur_len, mask_iter_warn;
 int mask_increments_len;
@@ -2112,7 +2114,6 @@ static void finalize_mask(int len);
  */
 void mask_init(struct db_main *db, char *unprocessed_mask)
 {
-	int using_default_mask = 0;
 	int i;
 
 	mask_db = db;
@@ -2475,11 +2476,16 @@ void mask_destroy()
 	fprintf(stderr, "%s()\n", __FUNCTION__);
 #endif
 
+	if (using_default_mask) {
+		options.mask = NULL;
+		using_default_mask = 0;
+	}
+
 	MEM_FREE(template_key);
 	MEM_FREE(template_key_offsets);
 	MEM_FREE(mask_skip_ranges);
 	MEM_FREE(mask_int_cand.int_cand);
-	mask_int_cand.num_int_cand = 0;
+	mask_int_cand.num_int_cand = 1;
 	mask_int_cand_target = 0;
 }
 
diff --git a/src/options.c b/src/options.c
index e1ad64079e..633ff85997 100644
--- a/src/options.c
+++ b/src/options.c
@@ -534,10 +534,6 @@ void opt_init(char *name, int argc, char **argv, int show_usage)
 		options.eff_mask = options.mask;
 		if (options.flags & FLG_TEST_CHK) {
 			options.flags &= ~FLG_PWD_SUP;
-			if (john_main_process && !(options.flags & FLG_NOTESTS))
-				fprintf(stderr, "Note: Self-tests currently not performed when using --mask with --test\n");
-			options.flags |= FLG_NOTESTS;
-
 			if (options.mask && strcasestr(options.mask, "?w"))
 				options.flags |= FLG_MASK_STACKED;
 
