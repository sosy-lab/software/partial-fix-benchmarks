diff --git a/doc/NEWS b/doc/NEWS
index d0872031d3..36f2c616dc 100644
--- a/doc/NEWS
+++ b/doc/NEWS
@@ -161,7 +161,9 @@ Major changes from 1.9.0-jumbo-1 (May 2019) in this bleeding-edge version:
   [blackfell; 2021]
 
 - Final fixes for zip2john selection of 1-byte or 2-byte early-reject checks,
-  after seeing several issues and digging into the specs.  [magnum; 2021]
+  as well as exactly what source to use for that check, after seeing several
+  issues and digging into the specs.  Note that a re-run of zip2john is needed
+  for correct cracking. [magnum; 2021]
 
 
 Major changes from 1.8.0-jumbo-1 (December 2014) to 1.9.0-jumbo-1 (May 2019):
diff --git a/src/pkzip.h b/src/pkzip.h
index 46cc09bb94..bffa705f75 100644
--- a/src/pkzip.h
+++ b/src/pkzip.h
@@ -1,3 +1,10 @@
+/*
+ * This software is Copyright (c) 2011-2018 Jim Fougeron,
+ * Copyright (c) 2013-2021 magnum,
+ * and it is hereby released to the general public under the following terms:
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted.
+ */
 #ifndef PKZIP_H
 #define PKZIP_H
 
@@ -38,6 +45,7 @@ typedef struct zip_magic_signatures_t {
 
 typedef struct zip_hash_type_t {
 	u8 *h;						// at getsalt time, we leave these null.  Later in setsalt, we 'fix' them
+	u8 type;					// JtR hash version. Version 2 ($pkzip2$) is now the deprecated one.
 	u16 c;
 	u16 c2;
 	u64 datlen;
diff --git a/src/pkzip_fmt_plug.c b/src/pkzip_fmt_plug.c
index 7e24734c6c..d75a385108 100644
--- a/src/pkzip_fmt_plug.c
+++ b/src/pkzip_fmt_plug.c
@@ -1,18 +1,11 @@
 /*
  * PKZIP patch for john to handle 'old' pkzip passwords (old 'native' format)
  *
- * Written by Jim Fougeron <jfoug at cox.net> in 2011.  No copyright
- * is claimed, and the software is hereby placed in the public domain.
- * In case this attempt to disclaim copyright and place the software in the
- * public domain is deemed null and void, then the software is
- * Copyright (c) 2011 Jim Fougeron and it is hereby released to the
- * general public under the following terms:
- *
+ * This software is Copyright (c) 2011-2018 Jim Fougeron,
+ * Copyright (c) 2013-2021 magnum,
+ * and it is hereby released to the general public under the following terms:
  * Redistribution and use in source and binary forms, with or without
  * modification, are permitted.
- *
- * There's ABSOLUTELY NO WARRANTY, express or implied.
- *
  */
 
 #include "arch.h"
@@ -86,36 +79,7 @@ john_register_one(&fmt_pkzip);
 #endif
 
 /*
- * filename:$pkzip$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*DA]*$/pkzip$   (deprecated)
- * filename:$pkzip2$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*TC*DA]*$/pkzip2$   (new format, with 2 checksums)
- *
- * All numeric and 'binary data' fields are stored in hex.
- *
- * C   is the count of hashes present (the array of items, inside the []  C can be 1 to 3.).
- * B   is number of valid bytes in the checksum (1 or 2).  Unix zip is 2 bytes, all others are 1
- * ARRAY of data starts here (there will be C array elements)
- *   DT  is a "Data Type enum".  This will be 1 2 or 3.  1 is 'partial'. 2 and 3 are full file data (2 is inline, 3 is load from file).
- *   MT  Magic Type enum.  0 is no 'type'.  255 is 'text'. Other types (like MS Doc, GIF, etc), see source.
- *     NOTE, CL, DL, CRC, OFF are only present if DT != 1
- *     CL  Compressed length of file blob data (includes 12 byte IV).
- *     UL  Uncompressed length of the file.
- *     CR  CRC32 of the 'final' file.
- *     OF  Offset to the PK\x3\x4 record for this file data. If DT==2, then this will be a 0, as it is not needed, all of the data is already included in the line.
- *     OX  Additional offset (past OF), to get to the zip data within the file.
- *     END OF 'optional' fields.
- *   CT  Compression type  (0 or 8)  0 is stored, 8 is imploded.
- *   DL  Length of the DA data.
- *   CS  Checksum from crc32.
- *   TC  Checksum from timestamp
- *   DA  This is the 'data'.  It will be hex data if DT==1 or 2. If DT==3, then it is a filename (name of the .zip file).
- * END of array items.
- * The format string will end with $/pkzip$
- *
- * NOTE, after some code testing, it has come to show, that the 'magic' may not be needed, or very useful. The problem with it, is IF the file
- * ends up NOT starting with any of the magic values, then we will have a false negative, and NEVER be able to crack the zip's password. For now
- * we have a #define (right before the #include "pkzip.h").  If that define is uncommented, then pkzip format will be built with magic logic.
- * However, right now it is not being built that way.
- *
+ * Format spec., see zip2john.c
  */
 static struct fmt_tests tests[] = {
 	/* compression of a perl file. We have the same password, same file used twice in a row (pkzip, 1 byte checksum).  NOTE, pkzip uses random IV, so both encrypted blobs are different */
@@ -132,8 +96,10 @@ static struct fmt_tests tests[] = {
 	{"$pkzip$1*1*2*0*163*2b6*46abc149*0*26*8*163*46ab*7ea9a6b07ddc9419439311702b4800e7e1f620b0ab8535c5aa3b14287063557b176cf87a800b8ee496643c0b54a77684929cc160869db4443edc44338294458f1b6c8f056abb0fa27a5e5099e19a07735ff73dc91c6b20b05c023b3ef019529f6f67584343ac6d86fa3d12113f3d374b047efe90e2a325c0901598f31f7fb2a31a615c51ea8435a97d07e0bd4d4afbd228231dbc5e60bf1116ce49d6ce2547b63a1b057f286401acb7c21afbb673f3e26bc1b2114ab0b581f039c2739c7dd0af92c986fc4831b6c294783f1abb0765cf754eada132df751cf94cad7f29bb2fec0c7c47a7177dea82644fc17b455ba2b4ded6d9a24e268fcc4545cae73b14ceca1b429d74d1ebb6947274d9b0dcfb2e1ac6f6b7cd2be8f6141c3295c0dbe25b65ff89feb62cb24bd5be33853b88b8ac839fdd295f71e17a7ae1f054e27ba5e60ca03c6601b85c3055601ce41a33127938440600aaa16cfdd31afaa909fd80afc8690aaf*$/pkzip$", "7J0rdan!!"},
 	/* CMIYC 2013 "pro" hard hash */
 	{"$pkzip$1*2*2*0*6b*73*8e687a5b*0*46*8*6b*0d9d*636fedc7a78a7f80cda8542441e71092d87d13da94c93848c230ea43fab5978759e506110b77bd4bc10c95bc909598a10adfd4febc0d42f3cd31e4fec848d6f49ab24bb915cf939fb1ce09326378bb8ecafde7d3fe06b6013628a779e017be0f0ad278a5b04e41807ae9fc*$/pkzip$", "c00rslit3!"},
-	/* http://corkami.googlecode.com/files/ChristmasGIFts.zip (fixed with 2 byte checksums from timestamp, using new $pkzip2$ type) */
+	/* https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/corkami/ChristmasGIFts.zip */
 	{"$pkzip2$3*2*1*2*8*c0*7224*72f6*6195f9f3401076b22f006105c4323f7ac8bb8ebf8d570dc9c7f13ddacd8f071783f6bef08e09ce4f749af00178e56bc948ada1953a0263c706fd39e96bb46731f827a764c9d55945a89b952f0503747703d40ed4748a8e5c31cb7024366d0ef2b0eb4232e250d343416c12c7cbc15d41e01e986857d320fb6a2d23f4c44201c808be107912dbfe4586e3bf2c966d926073078b92a2a91568081daae85cbcddec75692485d0e89994634c71090271ac7b4a874ede424dafe1de795075d2916eae*1*6*8*c0*26ee*461b*944bebb405b5eab4322a9ce6f7030ace3d8ec776b0a989752cf29569acbdd1fb3f5bd5fe7e4775d71f9ba728bf6c17aad1516f3aebf096c26f0c40e19a042809074caa5ae22f06c7dcd1d8e3334243bca723d20875bd80c54944712562c4ff5fdb25be5f4eed04f75f79584bfd28f8b786dd82fd0ffc760893dac4025f301c2802b79b3cb6bbdf565ceb3190849afdf1f17688b8a65df7bc53bc83b01a15c375e34970ae080307638b763fb10783b18b5dec78d8dfac58f49e3c3be62d6d54f9*2*0*2a*1e*4a204eab*ce8*2c*0*2a*4a20*7235*6b6e1a8de47449a77e6f0d126b217d6b2b72227c0885f7dc10a2fb3e7cb0e611c5c219a78f98a9069f30*$/pkzip2$", "123456"},
+	/* Same but with more knowledge, reverting to original type 1 format */
+	{"$pkzip$3*2*1*2*8*c0*72f6*6195f9f3401076b22f006105c4323f7ac8bb8ebf8d570dc9c7f13ddacd8f071783f6bef08e09ce4f749af00178e56bc948ada1953a0263c706fd39e96bb46731f827a764c9d55945a89b952f0503747703d40ed4748a8e5c31cb7024366d0ef2b0eb4232e250d343416c12c7cbc15d41e01e986857d320fb6a2d23f4c44201c808be107912dbfe4586e3bf2c966d926073078b92a2a91568081daae85cbcddec75692485d0e89994634c71090271ac7b4a874ede424dafe1de795075d2916eae*1*6*8*c0*461b*944bebb405b5eab4322a9ce6f7030ace3d8ec776b0a989752cf29569acbdd1fb3f5bd5fe7e4775d71f9ba728bf6c17aad1516f3aebf096c26f0c40e19a042809074caa5ae22f06c7dcd1d8e3334243bca723d20875bd80c54944712562c4ff5fdb25be5f4eed04f75f79584bfd28f8b786dd82fd0ffc760893dac4025f301c2802b79b3cb6bbdf565ceb3190849afdf1f17688b8a65df7bc53bc83b01a15c375e34970ae080307638b763fb10783b18b5dec78d8dfac58f49e3c3be62d6d54f9*2*0*2a*1e*4a204eab*ce8*2c*0*2a*7235*6b6e1a8de47449a77e6f0d126b217d6b2b72227c0885f7dc10a2fb3e7cb0e611c5c219a78f98a9069f30*$/pkzip$", "123456"},
 	{NULL}
 };
 
@@ -568,6 +534,8 @@ static void *get_salt(char *ciphertext)
 	sscanf(cp, "%x", &(salt->chk_bytes));
 	for (i = 0; i < salt->cnt; ++i) {
 		int data_enum;
+
+		salt->H[i].type = type2 ? 2 : 1;
 		cp = strtokm(NULL, "*");
 		data_enum = *cp - '0';
 		cp = strtokm(NULL, "*");
@@ -609,8 +577,7 @@ static void *get_salt(char *ciphertext)
 				salt->H[i].c2 <<= 4;
 				salt->H[i].c2 |= atoi16[ARCH_INDEX(cp[j])];
 			}
-		} else
-			salt->H[i].c2 = salt->H[i].c; // fake out 2nd hash, by copying first hash
+		}
 		cp = strtokm(NULL, "*");
 		if (data_enum > 1) {
 			/* if 2 or 3, we have the FULL zip blob for decrypting. */
@@ -1411,7 +1378,7 @@ static int crypt_all(int *pcount, struct db_salt *_salt)
 		u8 curInfBuf[128];
 #endif
 		int k, SigChecked;
-		u16 e, e2, v1, v2;
+		u16 e, v1, v2;
 		z_stream strm;
 		int ret;
 
@@ -1442,7 +1409,6 @@ static int crypt_all(int *pcount, struct db_salt *_salt)
 			b = salt->H[++cur_hash_idx].h;
 			k=11;
 			e = salt->H[cur_hash_idx].c;
-			e2 = salt->H[cur_hash_idx].c2;
 
 			do
 			{
@@ -1453,18 +1419,25 @@ static int crypt_all(int *pcount, struct db_salt *_salt)
 			}
 			while(--k);
 
-			/* if the hash is a 2 byte checksum type, then check that value first */
-			/* There is no reason to continue if this byte does not check out.  */
-			if (salt->chk_bytes == 2 && C != (e&0xFF) && C != (e2&0xFF))
-				goto Failed_Bailout;
+			if (salt->H[cur_hash_idx].type == 2) {
+				u16 e2 = salt->H[cur_hash_idx].c2;
 
-			C = PKZ_MULT(*b++,key2);
-#if 1
-			// https://github.com/openwall/john/issues/467
-			// Fixed, JimF.  Added checksum test for crc32 and timestamp.
-			if (C != (e>>8) && C != (e2>>8))
-				goto Failed_Bailout;
-#endif
+				if (salt->chk_bytes == 2 && C != (e & 0xff) && C != (e2 & 0xff))
+					goto Failed_Bailout;
+
+				C = PKZ_MULT(*b++, key2);
+
+				if (C != (e >> 8) && C != (e2 >> 8))
+					goto Failed_Bailout;
+			} else {
+				if (salt->chk_bytes == 2 && C != (e & 0xff))
+					goto Failed_Bailout;
+
+				C = PKZ_MULT(*b++, key2);
+
+				if (C != (e >> 8))
+					goto Failed_Bailout;
+			}
 
 			// Now, update the key data (with that last byte.
 			key0.u = jtr_crc32 (key0.u, C);
diff --git a/src/zip2john.c b/src/zip2john.c
index 1bcbec0f14..86942d0058 100644
--- a/src/zip2john.c
+++ b/src/zip2john.c
@@ -1,20 +1,13 @@
 /*
  * zip2john processes input ZIP files into a format suitable for use with JtR.
  *
- * This software is Copyright (c) 2011, Dhiru Kholia <dhiru.kholia at gmail.com>,
+ * This software is
+ * Copyright (c) 2011-2018 Dhiru Kholia <dhiru.kholia at gmail.com>,
+ * Copyright (c) 2011-2018 JimF, Copyright (c) 2020 Simon Rettberg,
+ * Copyright (c) 2013-2021 magnum,
  * and it is hereby released to the general public under the following terms:
- * Redistribution and use in source and binary forms, with or without modification,
- * are permitted.
- *
- * Updated in Aug 2011 by JimF.  Added PKZIP 'old' encryption.  The signature on the
- * pkzip will be $pkzip$ and does not look like the AES version written by Dhiru
- * Also fixed some porting issues, such as variables needing declared at top of blocks.
- *
- * Updated in 2020 by Simon Rettberg. Handle archives by scanning their central
- * directory first, which is more robust with archives that have been created using
- * streams. The old behavior of scanning for local headers from the start of the
- * file is still available through the -s option. Also fixed some minor issues and
- * refactored the code for slightly less redundant code beween AES and legacy.
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted.
  *
  * References:
  *
@@ -38,13 +31,27 @@
  * For type = 0, for ZIP files encrypted using AES
  * filename:$zip$*type*hex(CRC)*encryption_strength*hex(salt)*hex(password_verfication_value):hex(authentication_code)
  *
- * For original pkzip encryption:  (JimF, with longer explaination of fields)
- * filename:$pkzip$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*DA]*$/pkzip$   (deprecated)
- * filename:$pkzip2$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*TC*DA]*$/pkzip2$   (new format, with 2 checksums)
+ * Original $pkzip$ only had CS, fixed to be part of CRC, which was found out sometimes inappropriate.
+ * filename:$pkzip$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*DA]*$/pkzip$
+ *   CS  2 bytes of checksum data.
+ *
+ * The newer $pkzip2$ addressed the problem by adding TC (as in timestamp), but still neither zip2john or
+ * the format really knew when to use which (resulting in suboptimal early rejection).
+ * filename:$pkzip2$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*TC*DA]*$/pkzip2$ (with 2 checksums: CS & TC)
+ *   CS  2 bytes of checksum data.
+ *   TC  2 bytes of checksum data (from timestamp)
+ *
+ * Current version (Feb 2021) reverted to original $pkzip$ but now with the one correct value put in CS: Sometimes
+ * it's taken from timestamp, sometimes part of the CRC.
+ * filename:$pkzip$C*B*[DT*MT{CL*UL*CR*OF*OX}*CT*DL*CS*DA]*$/pkzip$
+ *   CS  Depending on archive, 2 bytes of either checksum data or timestamp.
+ *
  * All numeric and 'binary data' fields are stored in hex.
  *
  * C   is the count of hashes present (the array of items, inside the []  C can be 1 to 8.).
- * B   is number of valid bytes in the checksum (1 or 2).  Unix zip is 2 bytes, all others are 1 (NOTE, some can be 0)
+ * B   is number of valid bytes in the CS.  For ZIP version [needed to extract] < 2.0, this is 2, otherwise it's 1.
+ *     The B value should actually be defined within the ARRAY below, not per archive. For now we set it to 1 if any
+ *     of the files limits it.
  * ARRAY of data starts here
  *   DT  is a "Data Type enum".  This will be 1 2 or 3.  1 is 'partial'. 2 and 3 are full file data (2 is inline, 3 is load from file).
  *   MT  Magic Type enum.  0 is no 'type'.  255 is 'text'. Other types (like MS Doc, GIF, etc), see source.
@@ -57,8 +64,8 @@
  *     END OF 'optional' fields.
  *   CT  Compression type  (0 or 8)  0 is stored, 8 is imploded.
  *   DL  Length of the DA data.
- *   CS  2 bytes of checksum data.
- *   TC  2 bytes of checksum data (from timestamp)
+ *   CS  2 bytes of checksum data, from CRC, *or* from either CRC or timestamp (see above).
+ *   TC  2 bytes of checksum data, from timestamp (only $pkzip2$, see above).
  *   DA  This is the 'data'.  It will be hex data if DT == 1 or 2. If DT == 3, then it is a filename (name of the .zip file).
  * END of array item.  There will be C (count) array items.
  * The format string will end with $/pkzip$
@@ -211,8 +218,7 @@ typedef struct _zip_ptr
 	uint64_t      offset, offex;
 	uint64_t      cmp_len, decomp_len;
 	uint32_t      crc;
-	char          chksum[5];
-	char          chksum2[5];
+	char          cs[5]; // High-order word of either crc or timestamp
 	int           zip64; // Has extended header with 64bit data
 	uint16_t      lastmod_date, lastmod_time;
 	uint16_t      extrafield_length;
@@ -765,17 +771,27 @@ static int process_legacy(zip_file *zfp, zip_ptr *p)
 			extra_len_used += 4 + efh_datasize;
 		}
 
-		if (p->version < 20)
-			zfp->check_bytes = 2;
+		if (p->version >= 20)
+			zfp->check_bytes = 1;
+		else if (zfp->check_bytes == 1)
+			fprintf(stderr, "** 2b ** ");
 
 		scan_for_data_descriptor(zfp, p);
 
+		// Ok, now set checksum bytes.  This will depend upon if from crc, or from timestamp
+		if (p->flags & FLAG_LOCAL_SIZE_UNKNOWN)
+			sprintf(p->cs, "%02x%02x", p->lastmod_time >> 8, p->lastmod_time & 0xFF);
+		else
+			sprintf(p->cs, "%02x%02x", (p->crc >> 24) & 0xFF, (p->crc >> 16) & 0xFF);
+
+
 		fprintf(stderr,
-		        "%s/%s PKZIP%s Encr: %s cmplen=%"PRIu64", decmplen=%"PRIu64", crc=%X type=%"PRIu16"\n",
+		        "%s/%s PKZIP%s Encr: %s%scmplen=%"PRIu64", decmplen=%"PRIu64", crc=%08X ts=%04X cs=%s type=%"PRIu16"\n",
 		        jtr_basename(zfp->fname), p->file_name,
 		        zfp->zip64 ? "64" : "",
-		        zfp->check_bytes == 2 ? "2b chk," : "TS_chk,",
-		        p->cmp_len, p->decomp_len, p->crc, p->cmptype);
+		        zfp->check_bytes == 2 ? "2b chk, " : "",
+		        p->flags & FLAG_LOCAL_SIZE_UNKNOWN ? "TS_chk, " : "",
+		        p->cmp_len, p->decomp_len, p->crc, p->lastmod_time, p->cs, p->cmptype);
 
 		MEM_FREE(p->hash_data);
 		p->hash_data = mem_alloc(p->cmp_len + 1);
@@ -784,10 +800,6 @@ static int process_legacy(zip_file *zfp, zip_ptr *p)
 			return 0;
 		}
 
-		// Ok, now set checksum bytes.  This will depend upon if from crc, or from timestamp
-		sprintf(p->chksum, "%02x%02x", (p->crc>>24)&0xFF, (p->crc>>16)&0xFF);
-		sprintf(p->chksum2, "%02x%02x", p->lastmod_time>>8, p->lastmod_time&0xFF);
-
 		return 1;
 	}
 
@@ -860,7 +872,7 @@ static void init_zip_context(zip_context *ctx, const char *fname, FILE *fp)
 {
 	memset(ctx, 0, sizeof(*ctx));
 	ctx->archive.fname = fname;
-	ctx->archive.check_bytes = 1;
+	ctx->archive.check_bytes = 2;
 	ctx->archive.fp = fp;
 }
 
@@ -915,7 +927,7 @@ static void print_and_cleanup(zip_context *ctx)
 	filenames = strdup(ctx->best_files[0].file_name);
 	bname = jtr_basename(ctx->archive.fname);
 
-	printf("%s%s%s:$pkzip2$%x*%x*", bname,
+	printf("%s%s%s:$pkzip$%x*%x*", bname,
 			 ctx->num_candidates == 1 ? "/" : "",
 			 ctx->num_candidates == 1 ? ctx->best_files[0].file_name : "",
 			 ctx->num_candidates, ctx->archive.check_bytes);
@@ -935,21 +947,21 @@ static void print_and_cleanup(zip_context *ctx)
 			len = 12+180;
 		if (len > ctx->best_files[i].cmp_len)
 			len = ctx->best_files[i].cmp_len; // even though we 'could' output a '2', we do not.  We only need one full inflate CRC check file.
-		printf("1*%x*%x*%"PRIx64"*%s*%s*", ctx->best_files[i].magic_type, ctx->best_files[i].cmptype, (uint64_t)len, ctx->best_files[i].chksum, ctx->best_files[i].chksum2);
+		printf("1*%x*%x*%"PRIx64"*%s*", ctx->best_files[i].magic_type, ctx->best_files[i].cmptype, (uint64_t)len, ctx->best_files[i].cs);
 		print_hex((unsigned char*)ctx->best_files[i].hash_data, len);
 	}
 	// Ok, now output the 'little' one (the first).
 	if (!checksum_only) {
 		printf("%x*%x*%"PRIx64"*%"PRIx64"*%x*%"PRIx64"*%"PRIx64"*%x*", 2, ctx->best_files[0].magic_type, ctx->best_files[0].cmp_len, ctx->best_files[0].decomp_len, ctx->best_files[0].crc, ctx->best_files[0].offset, ctx->best_files[0].offex, ctx->best_files[0].cmptype);
-		printf("%"PRIx64"*%s*%s*", ctx->best_files[0].cmp_len, ctx->best_files[0].chksum, ctx->best_files[0].chksum2);
+		printf("%"PRIx64"*%s*", ctx->best_files[0].cmp_len, ctx->best_files[0].cs);
 		print_hex((unsigned char*)ctx->best_files[0].hash_data, ctx->best_files[0].cmp_len);
 	}
 	/* Don't allow our delimiter in there! */
 	replace(filenames, ':', ' ');
 	if (ctx->num_candidates > 1)
-		printf("$/pkzip2$::%s:%s:%s\n", bname, filenames, ctx->archive.fname);
+		printf("$/pkzip$::%s:%s:%s\n", bname, filenames, ctx->archive.fname);
 	else
-		printf("$/pkzip2$:%s:%s::%s\n", filenames, bname, ctx->archive.fname);
+		printf("$/pkzip$:%s:%s::%s\n", filenames, bname, ctx->archive.fname);
 
 	if (ctx->num_candidates > 1 && !once++)
 		fprintf(stderr,
