MPI: support any node ranges divisible by fork count
@solardiz said:
> ```
>   645                  options.node_min += mpi_id * npf;
>   646                  options.node_max = options.node_min + npf - 1;
>   647          }
> ```
> Previously, these lines were reached unconditionally, but now they're within `if (mpi_p > 1)`. Isn't this a bug for when that condition isn't met (running MPI with `mpi_p == 1`?)
> 
> Also, I assume `mpi_id` can be 0, for this to be equivalent to the main/parent process with `--fork`?

True, I probably need to correct this. First, I need to understand your code better. Latest here:
```
   537          unsigned int range = options.node_max - options.node_min + 1;
   538          unsigned int npf = range / options.fork;
   539  
   540          for (i = 1; i < options.fork; i++) {
   541                  switch ((pid = fork())) {
   542                  case -1:
   543                          pexit("fork");
   544  
   545                  case 0:
   546                          sig_preinit();
(...)
   564                          if (rec_restoring_now) {
   565                                  unsigned int save_min = options.node_min;
   566                                  unsigned int save_max = options.node_max;
   567                                  unsigned int save_count = options.node_count;
   568                                  unsigned int save_fork = options.fork;
   569                                  options.node_min += i * npf;
   570                                  options.node_max = options.node_min + npf - 1;
   571                                  rec_done(-2);
   572                                  rec_restore_args(1);
   573                                  john_set_tristates();
   574                                  if (options.node_min != save_min ||
   575                                      options.node_max != save_max ||
   576                                      options.node_count != save_count ||
   577                                      options.fork != save_fork)
   578                                          fprintf(stderr,
   579                                              "Inconsistent crash recovery file:"
   580                                              " %s\n", rec_name);
   581                          }
   582                          options.node_min += i * npf;
   583                          options.node_max = options.node_min + npf - 1;
   584                          sig_init_child();
   585                          return;
   586  
   587                  default:
   588                          pids[i - 1] = pid;
   589                  }
   590          }
   591  
   592          options.node_max = options.node_min + npf - 1;
```
Unlike in the corresponding MPI code, we have a return here (line 585), so I guess I need to use if...else to get it right. However, I'm puzzled over lines 569 and 582 above - aren't they both reached? Does that end up right? Same for 570 and 583 but they are not cumulative (except at 583, node_min has changed as it is now).
Oh I get it now... `rec_restore_args()` will restore `options.node_min` and `options.node_max`, that's what confused me.
> `rec_restore_args()` will restore `options.node_min` and `options.node_max`, that's what confused me.

Right. So I first update `options.node_min` to have the right `.rec` file read, and then do it again to undo the restoring of it from the file.

BTW, it's possibly wrong that we don't treat "Inconsistent crash recovery file" as a fatal error, proceeding anyway. Maybe we need to add curly braces and `error();` in there, for both fork and MPI. What do you think?
I take it the only thing I need is to move 645-646 below the closing bracket in 647.

> BTW, it's possibly wrong that we don't treat "Inconsistent crash recovery file" as a fatal error, proceeding anyway. Maybe we need to add curly braces and error(); in there, for both fork and MPI. What do you think?

Yes, I think we could just change to using `error_msg("Inconsistent crash recovery file: %s\n", rec_name);`
> I take it the only thing I need is to move 645-646 below the closing bracket in 647.

Yes, I think so. I don't really know what running with `mpi_p == 1` means, though.

> Yes, I think we could just change to using `error_msg("Inconsistent crash recovery file: %s\n", rec_name);`

OK, please feel free to include that change as well (separate commit).
> I don't really know what running with `mpi_p == 1` means, though.

We either aren't running under MPI at all, or we are but as a single process (eg. job launched on a remote host), and it doesn't matter which. Our `mpi_id` will be zero so the `options.node_min += mpi_id * npf` becomes a no-op.