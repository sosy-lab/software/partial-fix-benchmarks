krb5pa-sha1 should unify hashes in split()
krb5pa-sha1-opencl probalby needs the same fix.

Input format is 'user:$krb5pa$etype$user$realm$salt$timestamp+checksum' according to comments. This is a problem already: What if field[0] does not match the 'user' within the ciphertext? Should we fail valid() then? Well we can't because valid() can't see that.

Apparently, you have to run krbpa2john.py anyway, so I would just assume that $user$ is correct.
sap2john.pl also incorporates the user name into the hash, and the format ignores the user name in the separate field. I would keep it that way.

Since the "$mskrb5$" had been used in the past, can't we just replace "$mskrb5$" with "$krb5pa$" and treat "$mskrb5$" as invalid? 

A user may have "$mskrb5$" entries in her pot file, and we should keep recognizing them. A rewrite can't be done in prepare() - we need to accept both types in valid() and rewrite the hashes in split().

This also have the side effect we will continue to support "$mskrb5$" in input files.

You just dropped krb5-23 format, because these hashes are just NT hashes.
A user might have hashes prefixed with $krb23$ in the .pot file which are not recognized as valid NT hashes. If you want to continue supporting $mskrb5$, you should also change NT format to accept $krb23$.
(I admit there's a difference. While krb5-23 has been added in unstable, phps existed in 1.7.9-jumbo-7.)

The key is that $krb23$ has never been included in any released Jumbo. We could add support for it in NT, but since it was never released I think we shouldn't bother.

The mskrb5 discussion did not belong to this format at all.

The problem here is that Dhiru used the same test vector twice, with different formats. There is nothing wrong with the format. I will just delete one of the test vectors and we're cool.

f8ced75 fixes it better. We are now immune to the unused fields because they are cut out in valid. This is the way to go, in several other formats.

This lead to problems, reopening

reverted for now

Fix re-applied and hopefully working now. We now get 6 loaded out of 7 due to dupe suppression, but --show does not suppress dupe so shows 7 cracked - I am pretty sure that is normal.
