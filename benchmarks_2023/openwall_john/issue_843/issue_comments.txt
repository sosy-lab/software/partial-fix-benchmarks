memdbg issues
This likely comes from how you did memdbg in the new makefile.  YES, I am working to get the 64 bit stuff working better.

I am going to change how you did this. I really do not like it   I know it was a pain with memdbg_defines.h, and the biggest pain is GIT itself.  What I do not like is the extra checks being turned on by default  Extra checks is NOT normal usage. In that mode, it never frees memory, and slows down runtime a lot.

Can you test with f407415 and make sure the numbers look good.  NOTE you may have to rebuild to make it crash, using `make memdbg_ex` (after a new ./configure)

I got an error on OMP=16 linux (memdbg not memdbg_ex)

```
Testing: dahua, "MD5 based authentication" Dahua [MD5 32/64]... (16xOMP) PASS
Testing: Django (x10000) [PBKDF2-SHA256 128/128 SSE4.1 4x]... (16xOMP) PASS
Testing: django-scrypt [Salsa20/8 128/128 SSE2]... (16xOMP) 
Memory buffer underwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x7f807c000c50 - escrypt/scrypt_platform.c(53)

Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x7f807c000c50 - escrypt/scrypt_platform.c(53)

Exiting due to the error detected
```

some of these huge numbers may not be sign extension, but may be double deletes.

Like I said I still have the issue at 8xOMP

```
All 376 formats passed self-tests!

------------------------------
MEMDBG: allocation information:
   current normal alloc mem (leaks)18446744073709423576  max normal mem allocated: 18446744073709550205
   current 'tiny' alloc mem (leaks)0  max  tiny  mem allocated: 547056028
  Freed mem size: 1348996025 (freed cnt: 30462)
Index : alloc# :   Size : File(Line)  [first 20 bytes, or size of bytes]
At Program Exit
MemDbg_Validate level 0 checking Passed
MemDbg_Validate level 1 checking Passed
MemDbg_Validate level 2 checking Passed
MemDbg_Validate level 3 checking Passed
```

I agree your problems (which I can't reproduce now btw) doesn't look like sign extension. But this surely does.

Can you retest now that you have fixed many static buffer overwrites (the base64 stuff)

This might be a OMP bug in memdbg, where somehow I need to put in some flags listing that certain other vars are omp dangerous.  I will dig into this later today.

These formats are having problems for me, with OMP (and memdbg)

both scrypts  (scrypt/django-scrypt)  rar and LUKS.  All other formats are happy.

I think these are the formats that malloc INSIDE the crypt in the OMP block.   I am pretty sure that what we are seeing is that memdbg does not properly critical section itself in all cases.  I have been looking, and can not file it.  I have tons of debugging stuff, and what happens (scrypt) is that a free is called, (a set of them) and memdbg does not think it has that much memory allocated any more.  It is almost like free is being called against the same pointer, BUT I am not seeing crashes from multiple frees, etc.   I just do not get this one, and have put in a LOT of debugging.  It is almost like the named omp critical sections are NOT working properly.

I 'may' have found this one.   I had to add volatile to all of the data within memdbg.c (and some function params).  I will see if I need this. Also, if this IS the fix, I will not use volatile, but use a #define that can be turned off for non OMP builds.

So it appears that the OMP crit sections are there now, AND that with the addition of many volatiles, I now have it working properly.  I do have LOTS of changes.  I am going to back some of them out, testing to make sure things continue to work.   But hopefully, this issue will now go away.

Nope, this did not fix.  It is a little better, but not fixed.  :-1: 

From http://stackoverflow.com/questions/10522026/c-openmp-critical-one-way-locking:

"The `#pragma omp atomic` and `#pragma omp flush` directives may also be useful.

`atomic` causes a write to a memory location (lvalue in the expression preceded by the directive) to always be atomic.

`flush` ensures that any memory expected to be available to all threads is actually written to all threads, not stored in a processor cache and unavailable where it should be."

This looks like something I should read too: http://www.michaelsuess.net/publications/suess_leopold_common_mistakes_06.pdf

Maybe some of our `#pragma omp critical` for things like `any_cracked` could be atomic instead of critical.

On a side note that lead to d557419d

OMG!  I just found this line in memdbg.c    #undef _OPENMP 

5982c26 is where this undef was added.    I am not sure why, but it is causing ALL of the #ifdef _OPENMP code to simply not be compiled in, and thus, no critical sections.

LOL, that might be it!

I am pretty sure that was the problem.  I thought I had spent a LOT of time getting OMP safe allocation functions (realloc was the worse). I have done some changes that I do want to keep, so will be adding them also.  The problem I am getting is that some ancillary tools now link fail (markov stuff etc).  This can  be fixed in a couple of ways. 1 is to add OMP to markov IF building jtr in OMP mode.  The 2nd is to build memdbg.o a 2nd time without omp.

I am not sure which is the best alternative.  I would rather add OMP to the extras, if building in OMP mode.  @magnumripper what is your choice?  Either way, it will be  a change to makefile.in and to the legacy makefile set.

I too would rather add OMP to the extras. That's a no-op for them so can't hurt.

That was my feeling, and I think safer than trying to build a memdbg 2 ways (with 3 flavors of memdbg already, so that would be 6 flavors).  Adding @OMP_CFLAGS@ to the .in file is I think all that needs to be done (other than legacy)

We _could_ do this only when building memdbg, but I think it's not worth the extra work. Just add it to them.

Worked fine.

This 'audit' of memdbg did turn up one HUGE lacking problem.  i checked code, and the only place it was used is in sip2john, BUT that code has problems of its own (I will make a bug report about it).  That problem is that calloc is NOT wrapped by memdbg, BUT if used, then free would be used, which IS wrapped.  This must have some how gotten dropped when I ported this code, and I simply missed it.  I added it (super trivial).  Calloc simply calls MEMDBG_malloc (passing in proper file and line), and then memsets the returned pointer and returns that pointer.  I can not believe this was missed.

sip2john has a ton of utility functions (including malloc wrappers) in a header file.  There was a calloc in there (only place raw calloc is in john right now), BUT it was included prior to memdbg.h since it is a HEADER.

We REALLY need to look long and hard at putting code into headers.  That is REALLY bad practice.  If anything, if we have code we want to include (simply to keep the strange behavior of all static functions), then we should put it into a *.cx or *.ci or something other than *.h   Getting away from the 'C' convention of only having declarations in a .h file is not a good thing to do.

I think it's totally logical to `#include "salt_hash.c"` but apparently that is arguable, https://github.com/magnumripper/JohnTheRipper/issues/809#issuecomment-60796903

I have seen .c files included also.  It is not 'normal' way, but it is less likely to hide things. The problem with including .c files is when environments want to include it into a build package.  One other thing that jumps out as a issue here, is if there  are things that should be done to all source (like memdbg.h include).  Well, now you have a .c file that does not have that.

I know we have a couple instances where we have shared code for things like valid, BUT have them as static functions in some header.  A better way, would be to not have them as static, but make them unique and safe. A function like  jtr_PBKDF2_HMAC_SHA1_valid()     Put the delcaration in a header, the definition in some c file (or even code it into ONE of the format files), and then put  jtr_PBKDF2_HMAC_SHA1_valid into the function pointer part of the format structure in each *fmt.c file.  Yes, that one would not be static.  But the name is also not going to collide with anything else.  The ugliness could even be hidden with a strategically placed

#define valid jtr_PBKDF2_HMAC_SHA1_valid

so that you do not have to use the ugly form.

The reason we want to use shared functions in headers is function inlining. The way you suggest would ruin that (unless possibly if you run icc with some heavy extra options), and the performance loss can be very signifcant.

> if there are things that should be done to all source (like memdbg.h include). Well, now you have a .c file that does not have that.

I fail to see the problem. Since it's a .c file, you won't miss it when considering to add stuff to all C files. Also, we could change our "memdbg.h must be last" guideline to "memdbg.h must be last .h file but any included .c file must come after _that_"

No problem seen with just memdbg, but using memdbg_ex there is a new problem:

```
Will run 8 OpenMP threads
Testing: descrypt, traditional crypt(3) [DES 128/128 AVX-16]... (8xOMP) PASS

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x3609740 - bench.c(184) (352)
        data:  (..`.............  f0 96 60 03 00 00 00 00 00 00 00 00 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x36096f0 - bench.c(184) (352)
        data:  (@.`.....@.`.....  40 96 60 03 00 00 00 00 40 97 60 03 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x3609640 - formats.c(585) (352)
        data:  (..`.......`.....  90 96 60 03 00 00 00 00 f0 96 60 03 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x3609690 - formats.c(585) (352)
        data:  (..`.....@.`.....  f0 95 60 03 00 00 00 00 40 96 60 03 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x36095f0 - bench.c(184) (352)
        data:  (..`.......`.....  a0 95 60 03 00 00 00 00 90 96 60 03 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x36095a0 - bench.c(184) (352)
        data:  (..`.......`.....  f0 8f 60 03 00 00 00 00 f0 95 60 03 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x3608ff0 - formats.c(585) (352)
        data:  (@.`.......`.....  40 90 60 03 00 00 00 00 a0 95 60 03 00 00 00 00)

Freed Memory buffer overwrite found! Will try to list what file/line allocated the buffer
Memory fence_post error - 0x3609040 - formats.c(585) (352)
        data:  (..........`.....  00 00 00 00 00 00 00 00 f0 8f 60 03 00 00 00 00)

Exiting due to the error detected
```

Same problem with just -form:descrypt too.

Trivial and stupid bug (copy / paste error).  In the MEMDBG_free function, I was not setting the trailing fence post to the deleted signature. I was simply re-setting it back to what is normal alloc signature.

f84508e fixes this problem    Now I will see if travis sends me an email (I think I registered), or does it only send emails upon failures?

I think you'll get an email under any of these conditions:
1. You commit something that make it start failing.
2. Someone had already committed something that failed (you did not get that email) and you commit something and it still fails.
3. You commit something that fixes a fail

For this reason, not even I get an email when someone else commits something that make it fail. I only get it once I commit something and it still fails (no 2 above). This is configurable but I'm not quite sure how.
