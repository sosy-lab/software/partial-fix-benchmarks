Kernel traces with 3.12 with spl-git/zfs-git
These happen when the wrong type of memory allocation parameters are issued from the txg_sync thread. They should be harmless but also need to be fixed.

This doesn't appear to have anything to do with POSIX ACL support so I can't explain why it doesn't happen when you don't use `acltype=posixacl`.

In any case, this appears to have been caused by zfsonlinux/zfs@95fd54a.  I'm reviewing that patch right now because I see the patch author references a previous commit, zfsonlinux/zfs@dea9dfe which fixed a couple of these.  As mentioned, you can ignore these warnings for the moment.

The zfsonlinux/zfs@95fd54a commit restructures the hold/release processing quit a bit and pushes some processing into the sync task.  It introduced a bunch of new nvlists as well and it's their allocations along with the new work in the sync task are causing the problem.  I'm trying to figure out which ones need to be converted.

@jojun Could you please try dweeezil/zfs@4381976.  I think I found the minimal set of allocations that need to be converted.

Deployed it on the testing machine. Will report back.

The stacktrace persists

```
Nov 12 23:23:05 matroska kernel: [19725.125546] SPL: Showing stack for process 941                                                                                                                               
Nov 12 23:23:05 matroska kernel: [19725.125550] CPU: 2 PID: 941 Comm: txg_sync Tainted: P           O 3.12.0-core2-64 #1                                                                                         
Nov 12 23:23:05 matroska kernel: [19725.125552] Hardware name: MSI MS-7522/MSI X58 Pro-E (MS-7522), BIOS V8.8 01/07/2010                                                                                         
Nov 12 23:23:05 matroska kernel: [19725.125554]  ffff8800351aa540 ffff8802257bf9d8 ffffffff813c1e4d 0000000000000006                                                                                             Nov 12 23:23:05 matroska kernel: [19725.125559]  ffff8802257bfa44 ffff8802257bf9e8 ffffffffa0379411 ffff8802257bfa28                                                                                             
Nov 12 23:23:05 matroska kernel: [19725.125564]  ffffffffa037ca8c ffffffffa038c45b ffff8800351aaa40 00000000000003ad                                                                                             
Nov 12 23:23:05 matroska kernel: [19725.125568] Call Trace:                                                                                                                                                      
Nov 12 23:23:05 matroska kernel: [19725.125573]  [<ffffffff813c1e4d>] dump_stack+0x55/0x86                                                                                                                       
Nov 12 23:23:05 matroska kernel: [19725.125582]  [<ffffffffa0379411>] spl_debug_dumpstack+0x2b/0x2d [spl]                                                                                                        
Nov 12 23:23:05 matroska kernel: [19725.125592]  [<ffffffffa037ca8c>] sanitize_flags+0x73/0x82 [spl]                                                                                                             
Nov 12 23:23:05 matroska kernel: [19725.125603]  [<ffffffffa037cbc9>] kmalloc_nofail+0x21/0x3e [spl]                                                                                                             
Nov 12 23:23:05 matroska kernel: [19725.125613]  [<ffffffffa037ed4d>] kmem_alloc_debug+0x168/0x2d4 [spl]                                                                                                         
Nov 12 23:23:05 matroska kernel: [19725.125624]  [<ffffffffa037ed4d>] ? kmem_alloc_debug+0x168/0x2d4 [spl]                                                                                                       
Nov 12 23:23:05 matroska kernel: [19725.125655]  [<ffffffffa040fab6>] ? dmu_object_info_from_db+0x36/0x3d [zfs]                                                                                                  
Nov 12 23:23:05 matroska kernel: [19725.125693]  [<ffffffffa041e522>] ? dsl_dataset_hold_obj+0x6a/0x44c [zfs]                                                                                                    
Nov 12 23:23:05 matroska kernel: [19725.125703]  [<ffffffffa035b1f9>] nv_alloc_sleep_spl+0x23/0x25 [znvpair]                                                                                                     
Nov 12 23:23:05 matroska kernel: [19725.125711]  [<ffffffffa0357581>] nv_mem_zalloc.isra.12+0x10/0x29 [znvpair]                                                                                                  
Nov 12 23:23:05 matroska kernel: [19725.125721]  [<ffffffffa0359111>] nvlist_xalloc+0x55/0xaa [znvpair]                                                                                                          
Nov 12 23:23:05 matroska kernel: [19725.125730]  [<ffffffffa03591a1>] nvlist_alloc+0x3b/0x3f [znvpair]                                                                                                           
Nov 12 23:23:05 matroska kernel: [19725.125740]  [<ffffffffa035a826>] fnvlist_alloc+0x1b/0x65 [znvpair]                                                                                                          
Nov 12 23:23:05 matroska kernel: [19725.125790]  [<ffffffffa048c5e5>] dsl_dataset_user_release_check+0xeb/0x2af [zfs]                                                                                            
Nov 12 23:23:05 matroska kernel: [19725.125840]  [<ffffffffa048bfd8>] ? dsl_dataset_user_release_sync+0x220/0x220 [zfs]                                                                                          
Nov 12 23:23:05 matroska kernel: [19725.125845]  [<ffffffff81064fd1>] ? wake_up_atomic_t+0x2c/0x2c                                                                                                               
Nov 12 23:23:05 matroska kernel: [19725.125850]  [<ffffffff813c3b0f>] ? mutex_unlock+0x11/0x13                                                                                                                   
Nov 12 23:23:05 matroska kernel: [19725.125892]  [<ffffffffa042ea79>] dsl_sync_task_sync+0x99/0xc4 [zfs]                                                                                                         
Nov 12 23:23:05 matroska kernel: [19725.125933]  [<ffffffffa042828a>] dsl_pool_sync+0x353/0x49d [zfs]                                                                                                            
Nov 12 23:23:05 matroska kernel: [19725.125979]  [<ffffffffa043b7e2>] spa_sync+0x4f5/0x934 [zfs]                                                                                                                 
Nov 12 23:23:05 matroska kernel: [19725.126027]  [<ffffffffa044928b>] txg_sync_thread+0x2af/0x4e2 [zfs]                                                                                                          
Nov 12 23:23:05 matroska kernel: [19725.126075]  [<ffffffffa0448fdc>] ? txg_do_callbacks+0x4d/0x4d [zfs]                                                                                                         
Nov 12 23:23:05 matroska kernel: [19725.126087]  [<ffffffffa0380466>] ? __thread_create+0x2d6/0x2d6 [spl]                                                                                                        
Nov 12 23:23:05 matroska kernel: [19725.126098]  [<ffffffffa03804d2>] thread_generic_wrapper+0x6c/0x79 [spl]                                                                                                     
Nov 12 23:23:05 matroska kernel: [19725.126109]  [<ffffffffa0380466>] ? __thread_create+0x2d6/0x2d6 [spl]                                                                                                        
Nov 12 23:23:05 matroska kernel: [19725.126113]  [<ffffffff81064543>] kthread+0x88/0x90                                                                                                                          
Nov 12 23:23:05 matroska kernel: [19725.126118]  [<ffffffff810644bb>] ? __kthread_parkme+0x60/0x60                                                                                                               
Nov 12 23:23:05 matroska kernel: [19725.126122]  [<ffffffff813cb70c>] ret_from_fork+0x7c/0xb0                                                                                                                    
Nov 12 23:23:05 matroska kernel: [19725.126127]  [<ffffffff810644bb>] ? __kthread_parkme+0x60/0x60 
```

But as mentioned in #1855 it actually might be associated with zfs diff calls, as this problem does not happen anymore at random times, but at a time were are cronjob with zfs diff calls is running.

I found a new type of stack strace in the logs: (dbuf_rele_and_unlock was not there before)

```
Nov 12 23:39:43 matroska kernel: [20722.896476] SPL: Showing stack for process 941
Nov 12 23:39:43 matroska kernel: [20722.896478] CPU: 2 PID: 941 Comm: txg_sync Tainted: P           O 3.12.0-core2-64 #1
Nov 12 23:39:43 matroska kernel: [20722.896479] Hardware name: MSI MS-7522/MSI X58 Pro-E (MS-7522), BIOS V8.8 01/07/2010
Nov 12 23:39:43 matroska kernel: [20722.896483]  ffff8800351aa540 ffff8802257bf998 ffffffff813c1e4d 0000000048b648b6
Nov 12 23:39:43 matroska kernel: [20722.896486]  ffff8802257bfa04 ffff8802257bf9a8 ffffffffa0379411 ffff8802257bf9e8
Nov 12 23:39:43 matroska kernel: [20722.896488]  ffffffffa037ca8c ffffffffa038c45b ffff8800351aaa40 00000000000003ad
Nov 12 23:39:43 matroska kernel: [20722.896489] Call Trace:
Nov 12 23:39:43 matroska kernel: [20722.896492]  [<ffffffff813c1e4d>] dump_stack+0x55/0x86
Nov 12 23:39:43 matroska kernel: [20722.896500]  [<ffffffffa0379411>] spl_debug_dumpstack+0x2b/0x2d [spl]
Nov 12 23:39:43 matroska kernel: [20722.896508]  [<ffffffffa037ca8c>] sanitize_flags+0x73/0x82 [spl]
Nov 12 23:39:43 matroska kernel: [20722.896518]  [<ffffffffa037cbc9>] kmalloc_nofail+0x21/0x3e [spl]
Nov 12 23:39:43 matroska kernel: [20722.896528]  [<ffffffffa037ed4d>] kmem_alloc_debug+0x168/0x2d4 [spl]
Nov 12 23:39:43 matroska kernel: [20722.896533]  [<ffffffff813c3b0f>] ? mutex_unlock+0x11/0x13
Nov 12 23:39:43 matroska kernel: [20722.896558]  [<ffffffffa04033ec>] ? arc_buf_eviction_needed+0x63/0x6e [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896585]  [<ffffffffa040843d>] ? dbuf_rele_and_unlock+0x1b9/0x1be [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896595]  [<ffffffffa035b1f9>] nv_alloc_sleep_spl+0x23/0x25 [znvpair]
Nov 12 23:39:43 matroska kernel: [20722.896603]  [<ffffffffa0357581>] nv_mem_zalloc.isra.12+0x10/0x29 [znvpair]
Nov 12 23:39:43 matroska kernel: [20722.896613]  [<ffffffffa03580ef>] nvlist_add_common+0x114/0x307 [znvpair]
Nov 12 23:39:43 matroska kernel: [20722.896621]  [<ffffffffa035b1f9>] ? nv_alloc_sleep_spl+0x23/0x25 [znvpair]
Nov 12 23:39:43 matroska kernel: [20722.896631]  [<ffffffffa03585f0>] nvlist_add_boolean+0x13/0x15 [znvpair]
Nov 12 23:39:43 matroska kernel: [20722.896639]  [<ffffffffa035b0ee>] fnvlist_add_boolean+0x12/0x53 [znvpair]
Nov 12 23:39:43 matroska kernel: [20722.896697]  [<ffffffffa048c6c4>] dsl_dataset_user_release_check+0x1ca/0x2af [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896747]  [<ffffffffa048bfd8>] ? dsl_dataset_user_release_sync+0x220/0x220 [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896795]  [<ffffffffa042ea79>] dsl_sync_task_sync+0x99/0xc4 [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896835]  [<ffffffffa042828a>] dsl_pool_sync+0x353/0x49d [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896887]  [<ffffffffa043b7e2>] spa_sync+0x4f5/0x934 [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896934]  [<ffffffffa044928b>] txg_sync_thread+0x2af/0x4e2 [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896981]  [<ffffffffa0448fdc>] ? txg_do_callbacks+0x4d/0x4d [zfs]
Nov 12 23:39:43 matroska kernel: [20722.896992]  [<ffffffffa0380466>] ? __thread_create+0x2d6/0x2d6 [spl]
Nov 12 23:39:43 matroska kernel: [20722.897002]  [<ffffffffa03804d2>] thread_generic_wrapper+0x6c/0x79 [spl]
Nov 12 23:39:43 matroska kernel: [20722.897019]  [<ffffffffa0380466>] ? __thread_create+0x2d6/0x2d6 [spl]
Nov 12 23:39:43 matroska kernel: [20722.897023]  [<ffffffff81064543>] kthread+0x88/0x90
Nov 12 23:39:43 matroska kernel: [20722.897031]  [<ffffffff810644bb>] ? __kthread_parkme+0x60/0x60
Nov 12 23:39:43 matroska kernel: [20722.897034]  [<ffffffff813cb70c>] ret_from_fork+0x7c/0xb0
Nov 12 23:39:43 matroska kernel: [20722.897037]  [<ffffffff810644bb>] ? __kthread_parkme+0x60/0x60
```

@jojun Thanks for testing.  I was afraid there would be more allocations to fix in the hold/release processing.  Please try dweeezil/zfs@c888056.  Also, FYI, if you're running recent master code, you'll want to pick up dweeezil/zfs@393b281, too, which should cherry-pick or merge cleanly.

@dweeezil Any objection to merging dweeezil/zfs@c888056 as is?  It's clearly a step in the right direction even if it doesn't get every last allocation.

@behlendorf Sure, no problem.  I just create #1868.  I was holding off to try to squash as many of these as possible together.

I think I just discovered another place these flags are used.  I have a pool (separate from the one mentioned in #1912 ) that I was starting a "zfs send -Rpv" on and this backtrace was printed.

```
[109098.471403] SPLError: 5915:0:(kmem.h:91:sanitize_flags()) FATAL allocation for task txg_sync (5915) which used GFP flags 0x7afba7c with PF_NOFS set
[109098.471429] SPLError: 5915:0:(kmem.h:91:sanitize_flags()) SPL PANIC
[109098.471440] SPL: Showing stack for process 5915
[109098.471445] CPU: 1 PID: 5915 Comm: txg_sync Tainted: PF          O 3.11.9-200.fc19.x86_64 #1
[109098.471448] Hardware name: MICRO-STAR INT'L MS-7238/MS-7238, BIOS 080014  09/08/2006
[109098.471451]  0000000000000000 ffff880107afb9b0 ffffffff8164764b 0000000000000000
[109098.471459]  ffff880107afb9c0 ffffffffa04164d7 ffff880107afb9e8 ffffffffa0417671
[109098.471464]  ffffffffa04308f1 0000000000000000 0000000000000040 ffff880107afba18
[109098.471469] Call Trace:
[109098.471482]  [<ffffffff8164764b>] dump_stack+0x45/0x56
[109098.471499]  [<ffffffffa04164d7>] spl_debug_dumpstack+0x27/0x40 [spl]
[109098.471508]  [<ffffffffa0417671>] spl_debug_bug+0x81/0xe0 [spl]
[109098.471519]  [<ffffffffa042e64d>] sanitize_flags.part.5+0x66/0x68 [spl]
[109098.471530]  [<ffffffffa041eaa7>] kmem_alloc_debug+0x307/0x420 [spl]
[109098.471589]  [<ffffffffa053fae2>] ? refcount_remove_many+0x162/0x2e0 [zfs]
[109098.471628]  [<ffffffffa053fc76>] ? refcount_remove+0x16/0x20 [zfs]
[109098.471654]  [<ffffffffa04e62a9>] ? dbuf_rele_and_unlock+0x189/0x3b0 [zfs]
[109098.471693]  [<ffffffffa053fc76>] ? refcount_remove+0x16/0x20 [zfs]
[109098.471704]  [<ffffffffa0458468>] nv_alloc_sleep_spl+0x28/0x30 [znvpair]
[109098.471711]  [<ffffffffa0452705>] nv_mem_zalloc.isra.12+0x15/0x40 [znvpair]
[109098.471719]  [<ffffffffa04543ca>] nvlist_add_common.part.51+0x15a/0x430 [znvpair]
[109098.471728]  [<ffffffffa0454bbb>] nvlist_add_string+0x2b/0x30 [znvpair]
[109098.471736]  [<ffffffffa04579f0>] fnvlist_add_string+0x20/0x90 [znvpair]
[109098.471786]  [<ffffffffa05de2c9>] dsl_dataset_user_hold_check+0xf9/0x160 [zfs]
[109098.471847]  [<ffffffffa0535695>] dsl_sync_task_sync+0xb5/0x160 [zfs]
[109098.471891]  [<ffffffffa0564a35>] ? txg_list_remove+0x75/0xf0 [zfs]
[109098.471927]  [<ffffffffa052b5eb>] dsl_pool_sync+0x66b/0x970 [zfs]
[109098.471968]  [<ffffffffa054c069>] spa_sync+0x3d9/0xda0 [zfs]
[109098.472010]  [<ffffffffa0563fcc>] txg_sync_thread+0x34c/0x660 [zfs]
[109098.472052]  [<ffffffffa0563c80>] ? txg_fini+0x440/0x440 [zfs]
[109098.472062]  [<ffffffffa0420433>] thread_generic_wrapper+0x83/0xe0 [spl]
[109098.472071]  [<ffffffffa04203b0>] ? __thread_create+0x390/0x390 [spl]
[109098.472080]  [<ffffffffa04203b0>] ? __thread_create+0x390/0x390 [spl]
[109098.472087]  [<ffffffff81088660>] kthread+0xc0/0xd0
[109098.472091]  [<ffffffff810885a0>] ? insert_kthread_work+0x40/0x40
[109098.472097]  [<ffffffff816567ac>] ret_from_fork+0x7c/0xb0
[109098.472101]  [<ffffffff810885a0>] ? insert_kthread_work+0x40/0x40
```

@r0ssar00 Was f707635fa5a0a687f243a9b0976d7296955744d9 applied?  This was recently merged to master.

yup, I've been pulling and building almost daily (risky, but worth it to me).

Reopening, it seems we have one more to get.

I'm rather stumped as to how this could still be happening.  The stack trace clearly points at the dduha_chkholds member of the dsl_dataset_user_hold_arg struct (a struct only known within dsl_userhold.c).  At this point, every single nvlist allocation in dsl_userhold.c is using KM_PUSHPAGE.

@dweeezil I agree, I don't see how this is possible either.  @r0ssar00 can you please pull the latest source again and build clean to ensure you've picked up the fix.  I'm sure you know this but I'm going to say it anyway, make sure you unload and reload the module stack to pick up the fix.  Thanks!

I'm not really in a position to test this at the moment as I'm currently on vacation at my parents' house and the machine running ZFS is elsewhere.  As soon as I get home (the morning of the 28th) though I'll test it.

Closing as stale.  These issues were addresses by our reading of the source.  If someone is able to reproduce these warnings with the latest code we'll open a new issue.
