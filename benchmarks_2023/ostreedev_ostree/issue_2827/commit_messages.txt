Merge pull request #2832 from cgwalters/fix-itest-pull-space

itest-pull-space: Use mkfs.ext4, align to at least 512b
core: Ensure glib standard::size attribute is always set

* A recent change in glib [1] requires that the appropriate attribute
be available when calling getters. ostree core only sets this attribute
on regular files, and frequently triggers the critical warning. Solve
this by setting standard::size for regular files and symlinks (for which
stat reports correct sizes) and zero in all other cases.

Fixes https://github.com/ostreedev/ostree/issues/2827
core: Ensure glib standard::size attribute is always set

* A recent change in glib [1] requires that the appropriate attribute
be available when calling getters. ostree core only sets this attribute
on regular files, and frequently triggers the critical warning. Solve
this by setting standard::size for regular files and symlinks (for which
stat reports correct sizes) and zero in all other cases.

Fixes https://github.com/ostreedev/ostree/issues/2827

[1]: https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3261
core: Ensure glib standard::size attribute is always set

* A recent change in glib [1] requires that the appropriate attribute
be available when calling getters. ostree core only sets this attribute
on regular files, and frequently triggers the critical warning. Solve
this by setting standard::size to zero for non-regular files.

Fixes https://github.com/ostreedev/ostree/issues/2827

[1]: https://gitlab.gnome.org/GNOME/glib/-/merge_requests/3261
tests: Set size on fileinfo

Closes: https://github.com/ostreedev/ostree/issues/2827
