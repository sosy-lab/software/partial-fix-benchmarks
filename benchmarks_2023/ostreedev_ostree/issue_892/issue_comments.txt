remote: sysroot path handling
A symptom of this issue is that on Fedora Atomic Host, `/sysroot/etc/ostree/remotes.d` exists (and is empty).
We could likely introduce the concept of a "physical" sysroot vs a deployment one - perhaps just look for the existence of `/sysroot` as a dir?