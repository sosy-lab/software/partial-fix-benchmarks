SEGV with .immediate and missing SUBID fixup
The sub pmc gets the usage U_FIXUP | U_SUBID_LOOKUP, and is fixed up at
pbc.c: get_code_size() store_fixup

It crashes at `PackFile_fixup_subs(imcc->interp, PBC_IMMEDIATE, NULL);`
also without .const, just the .immediate pragma is enough.

```
.sub foo
.end
.sub baz :immediate :anon
  .const "Sub" foo = "foo"
  $P1 = foo."new"()
.end
```

crashes also even if the foo sub exists.

Dies now with the proper error message "Method 'new' not found for non-object"
Fixed in branch `rurban/null-call-gh1024`

`t/compilers/imcc/syn/const.t` test 41 is still unreliable
on the smokers. Needs to be improved when I can repro it.

Merged with 786869f9f9defc64f006c807e421bec45c43782e into 6.9.0

still crashing on linux:

```
$ valgrind ./parrot -D40 -w  t/compilers/imcc/syn/const_41.pir
==8231== Memcheck, a memory error detector
==8231== Copyright (C) 2002-2012, and GNU GPL'd, by Julian Seward et al.
==8231== Using Valgrind-3.8.1 and LibVEX; rerun with -h for copyright info
==8231== Command: ./parrot -D40 -w /usr/src/parrot/parrot-git/t/compilers/imcc/syn/const_41.pir
==8231==
==8231== Invalid read of size 8
==8231==    at 0x4D7C4C0: Parrot_set_p_pc(long*, parrot_interp_t*) (core_ops.c:20475)
==8231==    by 0x4E01339: runops_fast_core(parrot_interp_t*, runcore_t*, long*) (cores.c:495)
==8231==    by 0x4E007C6: runops_int(parrot_interp_t*, unsigned long) (main.c:221)
==8231==    by 0x4DD50AF: runops(parrot_interp_t*, unsigned long) (ops.c:123)
==8231==    by 0x4DCDFE3: Parrot_pcc_invoke_from_sig_object (pcc.c:340)
==8231==    by 0x4DCD774: Parrot_pcc_invoke_sub_from_c_args (pcc.c:140)
==8231==    by 0x4E15A99: do_1_sub_pragma(parrot_interp_t*, PMC*, pbc_action_enum_t) (api.c:744)
==8231==    by 0x4E16347: do_sub_pragmas (api.c:995)
==8231==    by 0x4E1952F: PackFile_fixup_subs (api.c:2477)
==8231==    by 0x4F559E2: e_pbc_end_sub(_imc_info_t*, void*, IMC_Unit*) (pbc.c:2264)
==8231==    by 0x4F464FD: emit_flush(_imc_info_t*, void*, IMC_Unit*) (instructions.c:700)
==8231==    by 0x4F407C9: imc_compile_unit(_imc_info_t*, IMC_Unit*) (imc.c:125)
==8231==  Address 0x5bd1f48 is 8 bytes before a block of size 24 alloc'd
==8231==    at 0x4A06C20: realloc (vg_replace_malloc.c:662)
==8231==    by 0x4DC16C1: gc_gms_reallocate_memory_chunk_zeroed(parrot_interp_t*, void*, unsigned long, unsigned long) (gc_gms.c:2031)

==8231== Invalid read of size 8
==8231==    at 0x4E9DDB9: Parrot_CallContext_push_pmc(parrot_interp_t*, PMC*, PMC*) (callcontext.pmc:1071)
==8231==    by 0x4DCE580: Parrot_pcc_build_sig_object_from_op (args.c:410)
==8231==    by 0x4D53F58: Parrot_set_args_pc(long*, parrot_interp_t*) (core_ops.c:13965)
==8231==    by 0x4E01339: runops_fast_core(parrot_interp_t*, runcore_t*, long*) (cores.c:495)
==8231==    by 0x4E007C6: runops_int(parrot_interp_t*, unsigned long) (main.c:221)
==8231==    by 0x4DD50AF: runops(parrot_interp_t*, unsigned long) (ops.c:123)
==8231==    by 0x4DCDFE3: Parrot_pcc_invoke_from_sig_object (pcc.c:340)
==8231==    by 0x4DCD774: Parrot_pcc_invoke_sub_from_c_args (pcc.c:140)
==8231==    by 0x4E15A99: do_1_sub_pragma(parrot_interp_t*, PMC*, pbc_action_enum_t) (api.c:744)
==8231==    by 0x4E16347: do_sub_pragmas (api.c:995)
==8231==    by 0x4E1952F: PackFile_fixup_subs (api.c:2477)
==8231==    by 0x4F559E2: e_pbc_end_sub(_imc_info_t*, void*, IMC_Unit*) (pbc.c:2264)
==8231==  Address 0x0 is not stack'd, malloc'd or (recently) free'd
```

WONTFIX invalid registers for :immediate subs, as all globals are not fixed up by 
definition, they are even cleared. That's a big :immediate limitation.
See http://trac.parrot.org/parrot/ticket/1324

But I added debugging assertions to detect rhs invalid pmc registers in set_p_pc?, 
and CallContext also has a NULL check now.
