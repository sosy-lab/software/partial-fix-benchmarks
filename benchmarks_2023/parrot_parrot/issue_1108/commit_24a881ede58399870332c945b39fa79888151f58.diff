diff --git a/include/parrot/interpreter.h b/include/parrot/interpreter.h
index 9dac1026b0..dd73aafe7b 100644
--- a/include/parrot/interpreter.h
+++ b/include/parrot/interpreter.h
@@ -38,6 +38,7 @@ typedef enum {
     PARROT_EVAL_DEBUG_FLAG          = 0x20,  /* create EVAL_n file */
     PARROT_REG_DEBUG_FLAG           = 0x40,  /* fill I,N with garbage */
     PARROT_CTX_DESTROY_DEBUG_FLAG   = 0x80,  /* ctx of a sub is gone */
+    PARROT_GC_DETAIL_DEBUG_FLAG     = 0x100,
     PARROT_ALL_DEBUG_FLAGS          = 0xffff
 } Parrot_debug_flags;
 /* &end_gen */
diff --git a/src/gc/alloc_memory.c b/src/gc/alloc_memory.c
index 24d6e585bc..a13512830c 100644
--- a/src/gc/alloc_memory.c
+++ b/src/gc/alloc_memory.c
@@ -1,5 +1,5 @@
 /*
-Copyright (C) 2001-2011, Parrot Foundation.
+Copyright (C) 2001-2014, Parrot Foundation.
 
 =head1 NAME
 
@@ -27,6 +27,13 @@ setup function to initialize the memory pools.
 #define PANIC_OUT_OF_MEM(size) panic_failed_allocation(__LINE__, (size))
 #define PANIC_ZERO_ALLOCATION(func) panic_zero_byte_allocation(__LINE__, (func))
 
+#ifndef DETAIL_MEMORY_DEBUG
+#  define MEMORY_DEBUG_DETAIL_2(s, a1, a2)
+#else
+#  define MEMORY_DEBUG_DETAIL_2(s, a1, a2) \
+        fprintf(stderr, (s), (a1), (a2))
+#endif
+
 /* HEADERIZER HFILE: include/parrot/memory.h */
 /* HEADERIZER BEGIN: static */
 /* Don't modify between HEADERIZER BEGIN / HEADERIZER END.  Your changes will be lost. */
@@ -68,9 +75,7 @@ mem_sys_allocate(size_t size)
     void * const ptr = malloc(size);
     if (size==0)
         PANIC_ZERO_ALLOCATION("mem_sys_allocate");
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %i at %p\n", size, ptr);
     if (!ptr)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -98,9 +103,7 @@ mem_sys_allocate_zeroed(size_t size)
 
     if (size==0)
         PANIC_ZERO_ALLOCATION("mem_sys_allocate_zeroed");
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %i at %p\n", size, ptr);
     if (!ptr)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -128,16 +131,12 @@ mem_sys_realloc(ARGFREE(void *from), size_t size)
     void *ptr;
     if (size==0)
         PANIC_ZERO_ALLOCATION("mem_sys_realloc");
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p (realloc -- %i bytes)\n", from, size);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p (realloc -- %i bytes)\n", from, size);
     if (from)
         ptr = realloc(from, size);
     else
         ptr = calloc(1, size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %i at %p\n", size, ptr);
     if (!ptr)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -166,13 +165,9 @@ mem_sys_realloc_zeroed(ARGFREE(void *from), size_t size, size_t old_size)
     void *ptr;
     if (size==0)
         PANIC_ZERO_ALLOCATION("mem_sys_realloc_zeroed");
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p (realloc -- %i bytes)\n", from, size);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p (realloc -- %i bytes)\n", from, size);
     ptr = from ? realloc(from, size) : malloc(size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %i at %p\n", size, ptr);
     if (!ptr)
         PANIC_OUT_OF_MEM(size);
 
@@ -197,9 +192,7 @@ void
 mem_sys_free(ARGFREE(void *from))
 {
     ASSERT_ARGS(mem_sys_free)
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p\n", from);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p%s\n", from, "");
     if (from)
         free(from);
 }
diff --git a/src/gc/gc_gms.c b/src/gc/gc_gms.c
index dfba812735..34a5604eb1 100644
--- a/src/gc/gc_gms.c
+++ b/src/gc/gc_gms.c
@@ -110,8 +110,6 @@ TBD
 #include "gc_private.h"
 #include "fixed_allocator.h"
 
-#define PANIC_OUT_OF_MEM(size) failed_allocation(__LINE__, (size))
-
 #ifdef THREAD_DEBUG
 #  define PARROT_GC_ASSERT_INTERP(pmc, interp) \
     PARROT_ASSERT((pmc) == NULL || (pmc)->orig_interp == (interp))
@@ -217,9 +215,6 @@ typedef void (*sweep_cb)(PARROT_INTERP, PObj *obj);
 /* HEADERIZER BEGIN: static */
 /* Don't modify between HEADERIZER BEGIN / HEADERIZER END.  Your changes will be lost. */
 
-PARROT_DOES_NOT_RETURN
-static void failed_allocation(unsigned int line, size_t size);
-
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static Parrot_Buffer* gc_gms_allocate_buffer_header(PARROT_INTERP,
@@ -238,12 +233,14 @@ static void* gc_gms_allocate_fixed_size_storage(PARROT_INTERP, size_t size)
 
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
-static void * gc_gms_allocate_memory_chunk(PARROT_INTERP, size_t size);
+static void * gc_gms_allocate_memory_chunk(PARROT_INTERP, size_t size)
+        __attribute__nonnull__(1);
 
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void * gc_gms_allocate_memory_chunk_zeroed(PARROT_INTERP,
-    size_t size);
+    size_t size)
+        __attribute__nonnull__(1);
 
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
@@ -315,7 +312,9 @@ static void gc_gms_free_fixed_size_storage(PARROT_INTERP,
         __attribute__nonnull__(3)
         FUNC_MODIFIES(*data);
 
-static void gc_gms_free_memory_chunk(PARROT_INTERP, ARGFREE(void *data));
+static void gc_gms_free_memory_chunk(PARROT_INTERP, ARGFREE(void *data))
+        __attribute__nonnull__(1);
+
 static void gc_gms_free_pmc_attributes(PARROT_INTERP, ARGMOD(PMC *pmc))
         __attribute__nonnull__(1)
         __attribute__nonnull__(2)
@@ -389,6 +388,7 @@ static void gc_gms_pmc_get_youngest_generation(PARROT_INTERP,
 static void gc_gms_pmc_needs_early_collection(PARROT_INTERP, PMC *pmc)
         __attribute__nonnull__(1);
 
+PARROT_INLINE
 static void gc_gms_print_stats(PARROT_INTERP, ARGIN(const char* header))
         __attribute__nonnull__(1)
         __attribute__nonnull__(2);
@@ -416,7 +416,8 @@ PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void * gc_gms_reallocate_memory_chunk(PARROT_INTERP,
     ARGFREE(void *from),
-    size_t size);
+    size_t size)
+        __attribute__nonnull__(1);
 
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
@@ -475,7 +476,6 @@ static void gc_gms_write_barrier(PARROT_INTERP, ARGMOD(PMC *pmc))
         FUNC_MODIFIES(*pmc);
 
 static int gen2flags(int gen);
-#define ASSERT_ARGS_failed_allocation __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
 #define ASSERT_ARGS_gc_gms_allocate_buffer_header __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(interp))
 #define ASSERT_ARGS_gc_gms_allocate_buffer_storage \
@@ -485,9 +485,11 @@ static int gen2flags(int gen);
 #define ASSERT_ARGS_gc_gms_allocate_fixed_size_storage \
      __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(interp))
-#define ASSERT_ARGS_gc_gms_allocate_memory_chunk __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
+#define ASSERT_ARGS_gc_gms_allocate_memory_chunk __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
+       PARROT_ASSERT_ARG(interp))
 #define ASSERT_ARGS_gc_gms_allocate_memory_chunk_zeroed \
-     __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
+     __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
+       PARROT_ASSERT_ARG(interp))
 #define ASSERT_ARGS_gc_gms_allocate_pmc_attributes \
      __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(interp) \
@@ -529,7 +531,8 @@ static int gen2flags(int gen);
      __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(interp) \
     , PARROT_ASSERT_ARG(data))
-#define ASSERT_ARGS_gc_gms_free_memory_chunk __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
+#define ASSERT_ARGS_gc_gms_free_memory_chunk __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
+       PARROT_ASSERT_ARG(interp))
 #define ASSERT_ARGS_gc_gms_free_pmc_attributes __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(interp) \
     , PARROT_ASSERT_ARG(pmc))
@@ -590,7 +593,8 @@ static int gen2flags(int gen);
        PARROT_ASSERT_ARG(interp) \
     , PARROT_ASSERT_ARG(str))
 #define ASSERT_ARGS_gc_gms_reallocate_memory_chunk \
-     __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
+     __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
+       PARROT_ASSERT_ARG(interp))
 #define ASSERT_ARGS_gc_gms_reallocate_memory_chunk_zeroed \
      __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
 #define ASSERT_ARGS_gc_gms_reallocate_string_storage \
@@ -1935,8 +1939,6 @@ size)>
 
 =item C<static void gc_gms_free_memory_chunk(PARROT_INTERP, void *data)>
 
-=item C<static void failed_allocation(unsigned int line, size_t size)>
-
 TODO Write docu.
 
 */
@@ -1944,13 +1946,11 @@ TODO Write docu.
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_gms_allocate_memory_chunk(SHIM_INTERP, size_t size)
+gc_gms_allocate_memory_chunk(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_gms_allocate_memory_chunk)
     void * const ptr = malloc(size);
-#if defined(DETAIL_MEMORY_DEBUG)
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1959,20 +1959,16 @@ gc_gms_allocate_memory_chunk(SHIM_INTERP, size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_gms_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
+gc_gms_reallocate_memory_chunk(PARROT_INTERP, ARGFREE(void *from), size_t size)
 {
     ASSERT_ARGS(gc_gms_reallocate_memory_chunk)
     void *ptr;
-#if defined(DETAIL_MEMORY_DEBUG)
-    fprintf(stderr, "Freed %p (realloc -- %i bytes)\n", from, size);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p (realloc -- %ld bytes)\n", from, size);
     if (from)
         ptr = realloc(from, size);
     else
         ptr = calloc(1, size);
-#if defined(DETAIL_MEMORY_DEBUG)
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1981,13 +1977,11 @@ gc_gms_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_gms_allocate_memory_chunk_zeroed(SHIM_INTERP, size_t size)
+gc_gms_allocate_memory_chunk_zeroed(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_gms_allocate_memory_chunk_zeroed)
     void * const ptr = calloc(1, size);
-#if defined(DETAIL_MEMORY_DEBUG)
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -2007,26 +2001,14 @@ gc_gms_reallocate_memory_chunk_zeroed(SHIM_INTERP, ARGFREE(void *data),
 }
 
 static void
-gc_gms_free_memory_chunk(SHIM_INTERP, ARGFREE(void *data))
+gc_gms_free_memory_chunk(PARROT_INTERP, ARGFREE(void *data))
 {
     ASSERT_ARGS(gc_gms_free_memory_chunk)
-#if defined(DETAIL_MEMORY_DEBUG)
-    fprintf(stderr, "Freed %p\n", data);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p%s\n", data, "");
     if (data)
         free(data);
 }
 
-PARROT_DOES_NOT_RETURN
-static void
-failed_allocation(unsigned int line, size_t size)
-{
-    ASSERT_ARGS(failed_allocation)
-    fprintf(stderr, "Failed allocation of %lu bytes\n", (unsigned long)size);
-    Parrot_x_panic_and_exit(NULL, "Out of mem", __FILE__, line);
-}
-
-
 /*
 
 =item C<static void gc_gms_pmc_needs_early_collection(PARROT_INTERP, PMC *pmc)>
@@ -2086,8 +2068,9 @@ gc_gms_write_barrier(PARROT_INTERP, ARGMOD(PMC *pmc))
         PARROT_GC_ASSERT_INTERP(pmc, interp);
 
 #ifdef MEMORY_DEBUG
-        fprintf(stderr, "GC WB pmc %-21s gen %ld at %p - %p\n",
-                pmc->vtable->whoami->strstart, gen, pmc, item->ptr);
+        if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG))
+            fprintf(stderr, "GC WB pmc %-21s gen %ld at %p - %p\n",
+                    pmc->vtable->whoami->strstart, gen, pmc, item->ptr);
 #endif
         Parrot_pa_remove(interp, self->objects[gen], item->ptr);
         item->ptr = Parrot_pa_insert(self->dirty_list, item);
@@ -2275,12 +2258,8 @@ gc_gms_check_sanity(PARROT_INTERP)
             PMC *pmc = &(((pmc_alloc_struct*)ptr)->pmc);
             const size_t gen  = POBJ2GEN(pmc);
             PARROT_GC_ASSERT_INTERP(pmc, interp);
-
-#  ifdef DETAIL_MEMORY_DEBUG
-            fprintf(stderr, "GC live pmc %-21s gen %ld at %p\n",
-                    pmc->vtable->whoami->strstart, gen, pmc);
-#  endif
-
+            MEMORY_DEBUG_DETAIL_3("GC live pmc %-21s gen %ld at %p\n",
+                                  pmc->vtable->whoami->strstart, gen, pmc);
             PARROT_ASSERT((gen == i)
                 || !"Object from wrong generation");
 
@@ -2297,20 +2276,16 @@ gc_gms_check_sanity(PARROT_INTERP)
     POINTER_ARRAY_ITER(self->dirty_list,
         PMC *pmc = &(((pmc_alloc_struct*)ptr)->pmc);
         PARROT_GC_ASSERT_INTERP(pmc, interp);
-#  ifdef DETAIL_MEMORY_DEBUG
-        fprintf(stderr, "GC dirty pmc %-21s at %p\n",
+        MEMORY_DEBUG_DETAIL_2("GC dirty pmc %-21s at %p\n",
                 pmc->vtable->whoami->strstart, pmc);
-#  endif
         PARROT_ASSERT(PObj_GC_on_dirty_list_TEST(pmc)
             || !"Object in dirty_list without dirty_flag"););
 
     POINTER_ARRAY_ITER(self->work_list,
         PMC *pmc = &(((pmc_alloc_struct*)ptr)->pmc);
         PARROT_GC_ASSERT_INTERP(pmc, interp);
-#  ifdef DETAIL_MEMORY_DEBUG
-        fprintf(stderr, "GC work pmc %-21s at %p\n",
+        MEMORY_DEBUG_DETAIL_2("GC work pmc %-21s at %p\n",
                 pmc->vtable->whoami->strstart, pmc);
-#  endif
         PARROT_ASSERT(!PObj_GC_on_dirty_list_TEST(pmc)
             || !"Dirty object in work_list"););
 #endif
@@ -2374,13 +2349,15 @@ gc_gms_print_stats_always(PARROT_INTERP, ARGIN(const char* header))
 
 }
 
+PARROT_INLINE
 static void
 gc_gms_print_stats(PARROT_INTERP, ARGIN(const char* header))
 {
     ASSERT_ARGS(gc_gms_print_stats)
 
 #ifdef MEMORY_DEBUG
-    gc_gms_print_stats_always(interp, header);
+    if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG))
+        gc_gms_print_stats_always(interp, header);
 #else
     UNUSED(interp);
     UNUSED(header);
diff --git a/src/gc/gc_inf.c b/src/gc/gc_inf.c
index f9ab5bb7df..45f0dc8301 100644
--- a/src/gc/gc_inf.c
+++ b/src/gc/gc_inf.c
@@ -31,16 +31,11 @@ to activate this core.
 #include "parrot/parrot.h"
 #include "gc_private.h"
 
-#define PANIC_OUT_OF_MEM(size) failed_allocation(__LINE__, (size))
-
 /* HEADERIZER HFILE: src/gc/gc_private.h */
 
 /* HEADERIZER BEGIN: static */
 /* Don't modify between HEADERIZER BEGIN / HEADERIZER END.  Your changes will be lost. */
 
-PARROT_DOES_NOT_RETURN
-static void failed_allocation(unsigned int line, unsigned long size);
-
 static void gc_inf_allocate_buffer_storage(PARROT_INTERP,
     ARGMOD(Parrot_Buffer *buffer),
     size_t size)
@@ -129,7 +124,6 @@ static void gc_inf_reallocate_string_storage(PARROT_INTERP,
         __attribute__nonnull__(2)
         FUNC_MODIFIES(*str);
 
-#define ASSERT_ARGS_failed_allocation __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
 #define ASSERT_ARGS_gc_inf_allocate_buffer_storage \
      __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(buffer))
@@ -181,7 +175,7 @@ static void gc_inf_reallocate_string_storage(PARROT_INTERP,
 =item C<static void gc_inf_mark_and_sweep(PARROT_INTERP, UINTVAL flags)>
 
 This function would perform a GC run, if we needed to. Luckily we have
-infinite memory!
+infinite memory.
 
 This function is called from the GC API function C<Parrot_gc_mark_and_sweep>.
 
@@ -334,12 +328,9 @@ static void
 gc_inf_allocate_string_storage(SHIM_INTERP, ARGMOD(STRING *str), size_t size)
 {
     ASSERT_ARGS(gc_inf_allocate_string_storage)
-
     Buffer_buflen(str)   = size;
-
     if (size > 0) {
         char * const mem = (char *)mem_internal_allocate(size);
-
         Buffer_bufstart(str) = str->strstart = mem;
     }
     else {
@@ -501,8 +492,6 @@ size)>
 
 =item C<static void gc_inf_free_memory_chunk(PARROT_INTERP, void *data)>
 
-=item C<static void failed_allocation(unsigned int line, unsigned long size)>
-
 TODO Write docu.
 
 */
@@ -510,13 +499,11 @@ TODO Write docu.
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_inf_allocate_memory_chunk(SHIM_INTERP, size_t size)
+gc_inf_allocate_memory_chunk(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_inf_allocate_memory_chunk)
     void * const ptr = malloc(size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -525,20 +512,16 @@ gc_inf_allocate_memory_chunk(SHIM_INTERP, size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_inf_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
+gc_inf_reallocate_memory_chunk(PARROT_INTERP, ARGFREE(void *from), size_t size)
 {
     ASSERT_ARGS(gc_inf_reallocate_memory_chunk)
     void *ptr;
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p (realloc -- %i bytes)\n", from, size);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p (realloc -- %ld bytes)\n", from, size);
     if (from)
         ptr = realloc(from, size);
     else
         ptr = calloc(1, size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -547,13 +530,11 @@ gc_inf_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_inf_allocate_memory_chunk_zeroed(SHIM_INTERP, size_t size)
+gc_inf_allocate_memory_chunk_zeroed(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_inf_allocate_memory_chunk_zeroed)
     void * const ptr = calloc(1, size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -573,27 +554,14 @@ gc_inf_reallocate_memory_chunk_zeroed(SHIM_INTERP, ARGFREE(void *data),
 }
 
 static void
-gc_inf_free_memory_chunk(SHIM_INTERP, ARGFREE(void *data))
+gc_inf_free_memory_chunk(PARROT_INTERP, ARGFREE(void *data))
 {
     ASSERT_ARGS(gc_inf_free_memory_chunk)
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p\n", data);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p%s\n", data, "");
     if (data)
         free(data);
 }
 
-PARROT_DOES_NOT_RETURN
-static void
-failed_allocation(unsigned int line, unsigned long size)
-{
-    ASSERT_ARGS(failed_allocation)
-    fprintf(stderr, "Failed allocation of %lu bytes\n", size);
-    Parrot_x_panic_and_exit(NULL, "Out of mem", __FILE__, line);
-}
-
-
-
 /*
 
 =back
diff --git a/src/gc/gc_ms.c b/src/gc/gc_ms.c
index 47e8dd991d..ba91146aaf 100644
--- a/src/gc/gc_ms.c
+++ b/src/gc/gc_ms.c
@@ -1,5 +1,5 @@
 /*
-Copyright (C) 2001-2011, Parrot Foundation.
+Copyright (C) 2001-2014, Parrot Foundation.
 
 =head1 NAME
 
@@ -20,8 +20,6 @@ without generations.
 
 #define DEBUG_FREE_LIST 0
 
-#define PANIC_OUT_OF_MEM(size) failed_allocation(__LINE__, (size))
-
 /* HEADERIZER HFILE: src/gc/gc_private.h */
 
 /* HEADERIZER BEGIN: static */
@@ -34,9 +32,6 @@ static INTVAL contained_in_attr_pool(
         __attribute__nonnull__(1)
         __attribute__nonnull__(2);
 
-PARROT_DOES_NOT_RETURN
-static void failed_allocation(unsigned int line, unsigned long size);
-
 static int gc_ms_active_sized_buffers(ARGIN(const Memory_Pools *mem_pools))
         __attribute__nonnull__(1);
 
@@ -258,7 +253,6 @@ static void Parrot_gc_initialize_fixed_size_pools(PARROT_INTERP,
 #define ASSERT_ARGS_contained_in_attr_pool __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(pool) \
     , PARROT_ASSERT_ARG(ptr))
-#define ASSERT_ARGS_failed_allocation __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
 #define ASSERT_ARGS_gc_ms_active_sized_buffers __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(mem_pools))
 #define ASSERT_ARGS_gc_ms_add_free_object __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
@@ -369,28 +363,6 @@ static void Parrot_gc_initialize_fixed_size_pools(PARROT_INTERP,
 
 /*
 
-=over 4
-
-=item C<static void failed_allocation(unsigned int line, unsigned long size)>
-
-Report error if allocation failed
-
-=back
-
-=cut
-
-*/
-
-PARROT_DOES_NOT_RETURN
-static void
-failed_allocation(unsigned int line, unsigned long size)
-{
-    fprintf(stderr, "Failed allocation of %lu bytes\n", size);
-    Parrot_x_panic_and_exit(NULL, "Out of mem", __FILE__, line);
-}
-
-/*
-
 =head2 Primary MS Functions
 
 =over 4
@@ -1378,13 +1350,11 @@ TODO Write docu.
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_ms_allocate_memory_chunk(SHIM_INTERP, size_t size)
+gc_ms_allocate_memory_chunk(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_ms_allocate_memory_chunk)
     void * const ptr = malloc(size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1393,20 +1363,16 @@ gc_ms_allocate_memory_chunk(SHIM_INTERP, size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_ms_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
+gc_ms_reallocate_memory_chunk(PARROT_INTERP, ARGFREE(void *from), size_t size)
 {
     ASSERT_ARGS(gc_ms_reallocate_memory_chunk)
     void *ptr;
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p (realloc -- %i bytes)\n", from, size);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p (realloc -- %ld bytes)\n", from, size);
     if (from)
         ptr = realloc(from, size);
     else
         ptr = calloc(1, size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1415,13 +1381,11 @@ gc_ms_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_ms_allocate_memory_chunk_zeroed(SHIM_INTERP, size_t size)
+gc_ms_allocate_memory_chunk_zeroed(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_ms_allocate_memory_chunk_zeroed)
     void * const ptr = calloc(1, size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1441,12 +1405,10 @@ gc_ms_reallocate_memory_chunk_zeroed(SHIM_INTERP, ARGFREE(void *data),
 }
 
 static void
-gc_ms_free_memory_chunk(SHIM_INTERP, ARGFREE(void *data))
+gc_ms_free_memory_chunk(PARROT_INTERP, ARGFREE(void *data))
 {
     ASSERT_ARGS(gc_ms_free_memory_chunk)
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p\n", data);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p%s\n", data, "");
     if (data)
         free(data);
 }
diff --git a/src/gc/gc_ms2.c b/src/gc/gc_ms2.c
index b489e20ee9..ccef445e75 100644
--- a/src/gc/gc_ms2.c
+++ b/src/gc/gc_ms2.c
@@ -33,8 +33,6 @@ typedef struct string_alloc_struct {
 #define PMC2PAC(p) ((pmc_alloc_struct *)((char*)(p) - sizeof (void *)))
 #define STR2PAC(p) ((string_alloc_struct *)((char*)(p) - sizeof (void *)))
 
-#define PANIC_OUT_OF_MEM(size) failed_allocation(__LINE__, (size))
-
 /* Private information */
 typedef struct MarkSweep_GC {
     /* Allocator for PMC headers */
@@ -76,9 +74,6 @@ typedef struct MarkSweep_GC {
 /* HEADERIZER BEGIN: static */
 /* Don't modify between HEADERIZER BEGIN / HEADERIZER END.  Your changes will be lost. */
 
-PARROT_DOES_NOT_RETURN
-static void failed_allocation(unsigned int line, unsigned long size);
-
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static Parrot_Buffer* gc_ms2_allocate_buffer_header(PARROT_INTERP,
@@ -267,7 +262,6 @@ static void gc_ms2_unblock_GC_mark(PARROT_INTERP)
 static void gc_ms2_unblock_GC_sweep(PARROT_INTERP)
         __attribute__nonnull__(1);
 
-#define ASSERT_ARGS_failed_allocation __attribute__unused__ int _ASSERT_ARGS_CHECK = (0)
 #define ASSERT_ARGS_gc_ms2_allocate_buffer_header __attribute__unused__ int _ASSERT_ARGS_CHECK = (\
        PARROT_ASSERT_ARG(interp))
 #define ASSERT_ARGS_gc_ms2_allocate_buffer_storage \
@@ -1375,8 +1369,6 @@ size)>
 
 =item C<static void gc_ms2_free_memory_chunk(PARROT_INTERP, void *data)>
 
-=item C<static void failed_allocation(unsigned int line, unsigned long size)>
-
 TODO Write docu.
 
 */
@@ -1384,13 +1376,11 @@ TODO Write docu.
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_ms2_allocate_memory_chunk(SHIM_INTERP, size_t size)
+gc_ms2_allocate_memory_chunk(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_ms2_allocate_memory_chunk)
     void * const ptr = malloc(size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1400,20 +1390,16 @@ gc_ms2_allocate_memory_chunk(SHIM_INTERP, size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_ms2_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
+gc_ms2_reallocate_memory_chunk(PARROT_INTERP, ARGFREE(void *from), size_t size)
 {
     ASSERT_ARGS(gc_ms2_reallocate_memory_chunk)
     void *ptr;
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p (realloc -- %i bytes)\n", from, size);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p (realloc -- %ld bytes)\n", from, size);
     if (from)
         ptr = realloc(from, size);
     else
         ptr = calloc(1, size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1423,13 +1409,11 @@ gc_ms2_reallocate_memory_chunk(SHIM_INTERP, ARGFREE(void *from), size_t size)
 PARROT_MALLOC
 PARROT_CAN_RETURN_NULL
 static void *
-gc_ms2_allocate_memory_chunk_zeroed(SHIM_INTERP, size_t size)
+gc_ms2_allocate_memory_chunk_zeroed(PARROT_INTERP, size_t size)
 {
     ASSERT_ARGS(gc_ms2_allocate_memory_chunk_zeroed)
     void * const ptr = calloc(1, (size_t)size);
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Allocated %i at %p\n", size, ptr);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Allocated %ld at %p\n", size, ptr);
     if (!ptr && size)
         PANIC_OUT_OF_MEM(size);
     return ptr;
@@ -1451,27 +1435,15 @@ gc_ms2_reallocate_memory_chunk_zeroed(SHIM_INTERP, ARGFREE(void *data),
 
 
 static void
-gc_ms2_free_memory_chunk(SHIM_INTERP, ARGFREE(void *data))
+gc_ms2_free_memory_chunk(PARROT_INTERP, ARGFREE(void *data))
 {
     ASSERT_ARGS(gc_ms2_free_memory_chunk)
-#ifdef DETAIL_MEMORY_DEBUG
-    fprintf(stderr, "Freed %p\n", data);
-#endif
+    MEMORY_DEBUG_DETAIL_2("Freed %p%s\n", data, "");
     if (data)
         free(data);
 }
 
 
-PARROT_DOES_NOT_RETURN
-static void
-failed_allocation(unsigned int line, unsigned long size)
-{
-    ASSERT_ARGS(failed_allocation)
-    fprintf(stderr, "Failed allocation of %lu bytes\n", size);
-    Parrot_x_panic_and_exit(NULL, "Out of mem", __FILE__, line);
-}
-
-
 /*
 
 =item C<static void gc_ms2_pmc_needs_early_collection(PARROT_INTERP, PMC *pmc)>
diff --git a/src/gc/gc_private.h b/src/gc/gc_private.h
index 0ebd693c36..05772a2ca1 100644
--- a/src/gc/gc_private.h
+++ b/src/gc/gc_private.h
@@ -10,6 +10,13 @@ src/gc/gc_private.h - private header file for the GC subsystem
 This is a private header file for the GC subsystem. It contains definitions
 that are only for use in the GC and don't need to be included in the rest of
 Parrot.
+
+=head2 Private Functions
+
+=over 4
+
+=cut
+
 */
 
 #ifndef PARROT_GC_PRIVATE_H_GUARD
@@ -23,6 +30,42 @@ Parrot.
 extern int CONSERVATIVE_POINTER_CHASING;
 #endif
 
+#ifndef MEMORY_DEBUG
+#  define MEMORY_DEBUG_DETAIL_2(s, a1, a2)
+#  define MEMORY_DEBUG_DETAIL_3(s, a1, a2, a3)
+#else
+#  define MEMORY_DEBUG_DETAIL_2(s, a1, a2) \
+    if (Interp_debug_TEST(interp, \
+                PARROT_MEM_STAT_DEBUG_FLAG | PARROT_GC_DETAIL_DEBUG_FLAG)) \
+        fprintf(stderr, (s), (a1), (a2))
+#  define MEMORY_DEBUG_DETAIL_3(s, a1, a2, a3)     \
+    if (Interp_debug_TEST(interp, \
+                PARROT_MEM_STAT_DEBUG_FLAG | PARROT_GC_DETAIL_DEBUG_FLAG)) \
+        fprintf(stderr, (s), (a1), (a2), (a3))
+#endif
+
+#define PANIC_OUT_OF_MEM(size) panic_failed_allocation(__LINE__, (size))
+
+/*
+
+=item C<static void panic_failed_allocation(unsigned int line, unsigned long
+size)>
+
+Print an error message and die.
+
+=cut
+
+*/
+
+PARROT_DOES_NOT_RETURN
+static void
+panic_failed_allocation(unsigned int line, size_t size)
+{
+    fprintf(stderr, "Failed allocation of %lu bytes\n", (unsigned long)size);
+    Parrot_x_panic_and_exit(NULL, "Out of mem", __FILE__, line);
+}
+
+
 #ifdef __ia64__
 
 #  include <ucontext.h>
@@ -785,6 +828,14 @@ void Parrot_gc_str_reallocate_string_storage(PARROT_INTERP,
 
 #endif /* PARROT_GC_PRIVATE_H_GUARD */
 
+/*
+
+=back
+
+=cut
+
+*/
+
 /*
  * Local variables:
  *   c-file-style: "parrot"
diff --git a/src/platform/darwin/sysmem.c b/src/platform/darwin/sysmem.c
index 3cd1021b11..591fe54f23 100644
--- a/src/platform/darwin/sysmem.c
+++ b/src/platform/darwin/sysmem.c
@@ -36,6 +36,8 @@ Get system memory information.
 
 Get information about available physical memory.
 
+Returns available physical memory in bytes, with respect to resource limits.
+
 =cut
 
 */
@@ -45,8 +47,10 @@ Parrot_sysmem_amount(PARROT_INTERP)
 {
     int           err = 0;
     size_t        memsize = 0;
-    char         *err_msg;
     unsigned long length = sizeof (memsize);
+#ifndef NDEBUG
+    size_t        ori_memsize = 0;
+#endif
 #if defined(PARROT_HAS_HEADER_SYSRESOURCE)
     struct rlimit rlim;
 #endif
@@ -60,7 +64,7 @@ Parrot_sysmem_amount(PARROT_INTERP)
 
     err = sysctl(selection, 2, &memsize, &length, NULL, 0) ;
     if (err) {
-        err_msg = strerror(err);
+        const char * const err_msg = strerror(err);
         /* sysmem_amount() is usually called too early, exceptions cannot be called yet */
         if (!Parrot_default_encoding_ptr) {
             fprintf(stderr, "sysctl failed: %s\n", err_msg);
@@ -71,6 +75,12 @@ Parrot_sysmem_amount(PARROT_INTERP)
             Parrot_ex_throw_from_c_args(interp, NULL, EXCEPTION_EXTERNAL_ERROR,
                 "sysctl failed: %s\n", err_msg);
     }
+#  ifndef NDEBUG
+    if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG)) {
+        ori_memsize = memsize;
+        fprintf(stderr, "Free Memory: %ld\n", memsize);
+    }
+#  endif
 
 #if defined(PARROT_HAS_HEADER_SYSRESOURCE)
     if (getrlimit(RLIMIT_DATA, &rlim) == 0) {
@@ -85,6 +95,11 @@ Parrot_sysmem_amount(PARROT_INTERP)
         else if ((rlim.rlim_cur != RLIM_INFINITY) && (rlim.rlim_cur <  memsize))
             memsize = rlim.rlim_cur;
     }
+#  ifndef NDEBUG
+    if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG)
+        && ori_memsize != memsize)
+        fprintf(stderr, "Memory via rlimit restricted to: %ld\n", memsize);
+#  endif
 #endif
 
     return memsize;
diff --git a/src/platform/generic/sysmem.c b/src/platform/generic/sysmem.c
index fed9e9b709..ad9da8b925 100644
--- a/src/platform/generic/sysmem.c
+++ b/src/platform/generic/sysmem.c
@@ -62,6 +62,9 @@ size_t
 Parrot_sysmem_amount(PARROT_INTERP)
 {
     size_t        memsize = 0;
+#ifndef NDEBUG
+    size_t        ori_memsize = 0;
+#endif
 #if defined(PARROT_HAS_HEADER_SYSRESOURCE)
     struct rlimit rlim;
 #endif
@@ -109,14 +112,25 @@ Parrot_sysmem_amount(PARROT_INTERP)
             Parrot_ex_throw_from_c_args(interp, NULL, EXCEPTION_EXTERNAL_ERROR,
                 "sysctl failed: %s\n", err_msg);
     }
-
+#  ifndef NDEBUG
+    if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG)) {
+        ori_memsize = memsize;
+        fprintf(stderr, "Free Memory: %ld\n", memsize);
+    }
+#  endif
 #else
     UNUSED(interp);
-    /* Method 3:  Random guess.  Simply guess 512 MB.  This way, parrot
+    /* Method 3:  Faith.  Simply guess 512 MB.  This way, parrot
      * will at least build.  Arguably, one could also just put an error
      * here and instruct the user to fix it manually.
      */
     memsize = 512 * 1024 * 1024;
+#  ifndef NDEBUG
+    if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG)) {
+        ori_memsize = memsize;
+        fprintf(stderr, "Default Memory: %ld\n", memsize);
+    }
+#  endif
 #endif
 
 #if defined(PARROT_HAS_HEADER_SYSRESOURCE)
@@ -132,6 +146,11 @@ Parrot_sysmem_amount(PARROT_INTERP)
         else if ((rlim.rlim_cur != RLIM_INFINITY) && (rlim.rlim_cur <  memsize))
             memsize = rlim.rlim_cur;
     }
+#  ifndef NDEBUG
+    if (Interp_debug_TEST(interp, PARROT_MEM_STAT_DEBUG_FLAG)
+        && ori_memsize != memsize)
+        fprintf(stderr, "Memory via rlimit restricted to: %ld\n", memsize);
+#  endif
 #endif
 
     return memsize;
