diff --git a/config/gen/makefiles/root.in b/config/gen/makefiles/root.in
index d8f3ecc4f9..a83250c75b 100644
--- a/config/gen/makefiles/root.in
+++ b/config/gen/makefiles/root.in
@@ -1065,7 +1065,7 @@ src/pointer_array$(O) : \
 
 src/oo$(O) : $(PARROT_H_HEADERS) $(INC_PMC_DIR)/pmc_class.h src/oo.c \
 	$(INC_PMC_DIR)/pmc_object.h src/oo.str $(INC_DIR)/oo_private.h \
-	$(INC_PMC_DIR)/pmc_namespace.h
+	$(INC_PMC_DIR)/pmc_namespace.h $(INC_PMC_DIR)/pmc_sub.h
 
 src/scheduler$(O) : \
 	$(PARROT_H_HEADERS) \
diff --git a/include/parrot/sub.h b/include/parrot/sub.h
index 1b5d62dc81..ab7eddf239 100644
--- a/include/parrot/sub.h
+++ b/include/parrot/sub.h
@@ -103,6 +103,11 @@ typedef enum {
 #define Sub_comp_INIT_SET(o) Sub_comp_flag_SET(PF_INIT, o)
 #define Sub_comp_INIT_CLEAR(o) Sub_comp_flag_CLEAR(PF_INIT, o)
 
+#define Sub_comp_METHOD_TEST(o) Sub_comp_flag_TEST(METHOD, o)
+#define Sub_comp_METHOD_SET(o) Sub_comp_flag_SET(METHOD, o)
+#define Sub_comp_METHOD_CLEAR(o) Sub_comp_flag_CLEAR(METHOD, o)
+
+
 /*
  * maximum sub recursion depth
  */
diff --git a/src/oo.c b/src/oo.c
index f7adfc62d0..7fac191c65 100644
--- a/src/oo.c
+++ b/src/oo.c
@@ -22,6 +22,7 @@ Handles class and object manipulation.
 #include "pmc/pmc_class.h"
 #include "pmc/pmc_object.h"
 #include "pmc/pmc_namespace.h"
+#include "pmc/pmc_sub.h"
 
 #include "oo.str"
 
@@ -926,6 +927,9 @@ Parrot_invalidate_method_cache(PARROT_INTERP, ARGIN_NULLOK(STRING *_class))
 Find a method PMC for a named method, given the class PMC, current
 interpreter, and name of the method. Don't use a possible method cache.
 
+Note that this function with DISPATCH_NAMESPACE is wrong. It needs to skip all
+non-methods, esp. for builtin PMCs. See GH #304.
+
 =cut
 
 */
@@ -938,6 +942,8 @@ Parrot_find_method_direct(PARROT_INTERP, ARGIN(PMC *_class), ARGIN(STRING *metho
 {
     ASSERT_ARGS(Parrot_find_method_direct)
 
+    /* old pre-8.2 behavior. see GH #304 */
+#ifdef DISPATCH_NAMESPACE
     STRING * const class_str   = CONST_STRING(interp, "class");
     STRING * const methods_str = CONST_STRING(interp, "methods");
     PMC    * const mro         = _class->vtable->mro;
@@ -971,9 +977,50 @@ Parrot_find_method_direct(PARROT_INTERP, ARGIN(PMC *_class), ARGIN(STRING *metho
             return method;
         }
     }
+    TRACE_FM(interp, _class, method_name, NULL);
+    return PMCNULL;
+#else
+    STRING * const class_str   = CONST_STRING(interp, "class");
+    STRING * const methods_str = CONST_STRING(interp, "methods");
+    PMC    * const mro         = _class->vtable->mro;
+    const INTVAL   n           = VTABLE_elements(interp, mro);
+    INTVAL         i;
+
+    for (i = 0; i < n; ++i) {
+        PMC * const _class_i  = VTABLE_get_pmc_keyed_int(interp, mro, i);
+        PMC * const ns        = VTABLE_get_namespace(interp, _class_i);
+        PMC * const class_obj = VTABLE_inspect_str(interp, ns, class_str);
+        STRING * const is_method = CONST_STRING(interp, "method_name");
+        PMC        *method    = PMCNULL;
+        PMC        *method_hash = PMCNULL;
+
+        if (PMC_IS_NULL(class_obj))
+            method_hash = VTABLE_inspect_str(interp, ns, methods_str);
+        else
+            method_hash = VTABLE_inspect_str(interp, class_obj, methods_str);
+        if (!PMC_IS_NULL(method_hash))
+            method = VTABLE_get_pmc_keyed_str(interp, method_hash, method_name);
+        if (PMC_IS_NULL(method))
+            method = VTABLE_get_pmc_keyed_str(interp, ns, method_name);
+        /* Ignore subs, only methods.
+           From all DOES->invokable NCI and NativePCCMethod are methods, so just Sub */
+        if (!PMC_IS_NULL(method)
+            && method->vtable->base_type == enum_class_Sub
+            && !Sub_comp_flag_TEST(METHOD, PMC_data_typed(method, Parrot_Sub_attributes *)))
+            method = PMCNULL;
+
+        TRACE_FM(interp, _class_i, method_name, method);
 
+        if (!PMC_IS_NULL(method)) {
+            if (interp->thread_data != NULL)
+                method = VTABLE_clone(interp, method);
+            PARROT_ASSERT_INTERP(method, interp);
+            return method;
+        }
+    }
     TRACE_FM(interp, _class, method_name, NULL);
     return PMCNULL;
+#endif
 }
 
 
@@ -987,8 +1034,8 @@ interp, and name of the method.
 
 This routine should use the current scope's method cache, if there is
 one. If not, it creates a new method cache. Or, rather, it will when
-we've got that bit working. For now it unconditionally goes and looks up
-the name in the global stash.
+we've got that bit working. For now it unconditionally goes and looks
+up the name in the global stash.
 
 =cut
 
@@ -1010,7 +1057,7 @@ Parrot_find_method_with_cache(PARROT_INTERP, ARGIN(PMC *_class), ARGIN(STRING *m
     Meth_cache_entry *e;
     UINTVAL type, bits;
 
-    if (! PObj_constant_TEST(method_name))
+    if (! PObj_constant_TEST(method_name) )
         return Parrot_find_method_direct(interp, _class, method_name);
 
     mc   = interp->caches;
diff --git a/src/pmc/object.pmc b/src/pmc/object.pmc
index f8a98b39fd..7965fa20ac 100644
--- a/src/pmc/object.pmc
+++ b/src/pmc/object.pmc
@@ -520,8 +520,7 @@ Queries this object's class to find the method with the given name.
         Parrot_Object_attributes * const obj    = PARROT_OBJECT(SELF);
         Parrot_Class_attributes  * const _class = PARROT_CLASS(obj->_class);
 
-        PMC                             *method =
-                 find_cached(INTERP, obj->_class, name);
+        PMC *method = find_cached(INTERP, obj->_class, name);
 
         if (!PMC_IS_NULL(method))
             return method;
