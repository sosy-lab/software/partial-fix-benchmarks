Merge branch 'PHP-8.2'

- PHP-8.1:
  Fix GH-10292 1st param of mt_srand() has UNKNOWN default on PHP <8.3
Fix GH-10437: Segfault on suspending a dead fiber
Fix GH-10437: Set active fiber to null on bailout
Fix GH-10437: Set active fiber to null on bailout
Fix GH-10437: Set active fiber to null on bailout (#10443)
Merge branch 'PHP-8.1' into PHP-8.2

* PHP-8.1:
  Fix GH-10437: Set active fiber to null on bailout (#10443)
Fix GH-10437: Set active fiber to null on bailout (#10443)
Merge branch 'PHP-8.2'

* PHP-8.2:
  [ci skip] NEWS
  Fix GH-10437: Set active fiber to null on bailout (#10443)
