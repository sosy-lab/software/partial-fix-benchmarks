ext/mysqli/pgsql: mysqli_fetch_object/pgsql_fetch_object raises ValueError on constructor args error.

Closes GH-10832.
Fix GH-10755: Memory leak in phar_rename_archive()

In phar_renmae_archive() context, added one reference but immediately
destroyed another, so do not need to increase refcount. With removal of
refcount++ line, PHP/Zend no longer reports memory leak.
Updated bug69958.phpt test file accordingly.

Testing
1) Running selected tests.
PASS Phar: bug #69958: Segfault in Phar::convertToData on invalid file [bug69958.phpt]

2) All tests under ext/phar/tests PASSED except skipped.
=====================================================================
Number of tests :   530               425
Tests skipped   :   105 ( 19.8%) --------
Tests warned    :     0 (  0.0%) (  0.0%)
Tests failed    :     0 (  0.0%) (  0.0%)
Tests passed    :   425 ( 80.2%) (100.0%)
---------------------------------------------------------------------
Time taken      :    19 seconds
=====================================================================

Signed-off-by: Su, Tao <tao.su@intel.com>
Fix GH-10755: Memory leak in phar_rename_archive()

In phar_renmae_archive() context, added one reference but immediately
destroyed another, so do not need to increase refcount. With removal of
refcount++ line, PHP/Zend no longer reports memory leak.
Updated bug69958.phpt test file accordingly.

Testing (PASS on both Debug and Release build)
Debug: ./configure --enable-debug
Release: ./configure

1) Running selected tests.
PASS Phar: bug #69958: Segfault in Phar::convertToData on invalid file [bug69958.phpt]

2) All tests under ext/phar/tests PASSED except skipped.
=====================================================================
Number of tests :   530               515
Tests skipped   :    15 (  2.8%) --------
Tests warned    :     0 (  0.0%) (  0.0%)
Tests failed    :     0 (  0.0%) (  0.0%)
Tests passed    :   515 ( 97.2%) (100.0%)
---------------------------------------------------------------------
Time taken      :    26 seconds
=====================================================================

Signed-off-by: Su, Tao <tao.su@intel.com>
Fix GH-10755: Memory leak in phar_rename_archive()

In phar_renmae_archive() context, added one reference but immediately
destroyed another, so do not need to increase refcount. With removal of
refcount++ line, PHP/Zend no longer reports memory leak.
Updated bug69958.phpt test file accordingly.

Testing (PASS on both Debug and Release build)
Debug: ./configure --enable-debug
Release: ./configure

1) Running selected tests.
PASS Phar: bug #69958: Segfault in Phar::convertToData on invalid file [bug69958.phpt]

2) All tests under ext/phar/tests PASSED except skipped.
=====================================================================
Number of tests :   530               515
Tests skipped   :    15 (  2.8%) --------
Tests warned    :     0 (  0.0%) (  0.0%)
Tests failed    :     0 (  0.0%) (  0.0%)
Tests passed    :   515 ( 97.2%) (100.0%)
---------------------------------------------------------------------
Time taken      :    26 seconds
=====================================================================

Signed-off-by: Su, Tao <tao.su@intel.com>
Fix GH-10755: Make bug69958.phpt support Linux and Windows

According to Niels Dossche, remove slash character (/) in bug69958.php
test file in order to support testing on both Linux and Windows.
Link: https://github.com/php/php-src/pull/10848

Signed-off-by: Tony Su <stkeke@gmail.com>
Fix GH-10755: Memory leak in phar_rename_archive()

In phar_renmae_archive() context, added one reference but immediately
destroyed another, so do not need to increase refcount. With removal of
refcount++ line, PHP/Zend no longer reports memory leak.
Updated bug69958.phpt test file accordingly.

Testing (PASS on both Debug and Release build)
Debug: ./configure --enable-debug
Release: ./configure

1) Running selected tests.
PASS Phar: bug #69958: Segfault in Phar::convertToData on invalid file [bug69958.phpt]

2) All tests under ext/phar/tests PASSED except skipped.
=====================================================================
Number of tests :   530               515
Tests skipped   :    15 (  2.8%) --------
Tests warned    :     0 (  0.0%) (  0.0%)
Tests failed    :     0 (  0.0%) (  0.0%)
Tests passed    :   515 ( 97.2%) (100.0%)
---------------------------------------------------------------------
Time taken      :    26 seconds
=====================================================================

Signed-off-by: Su, Tao <tao.su@intel.com>
Fix GH-10755: Make bug69958.phpt support Linux and Windows

According to Niels Dossche, remove slash character (/) in bug69958.php
test file in order to support testing on both Linux and Windows.
Link: https://github.com/php/php-src/pull/10848

Signed-off-by: Tony Su <tao.su@intel.com>
FIX GH-10755: Double-free of 'alias' caught by ASAN check

After having fixed memory leak, ASAN check reported double-free of
phar->alias (char*) string. We found the root cause and documented
it at https://github.com/php/php-src/pull/10856

Solution is provided by Ilija Tovilo (iluuu1994).

Signed-off-by: Tony Su <tao.su@intel.com>
Reviewed-by:   Ilija Tovilo
Tested-by:     Tony Su <tao.su@intel.com>
Fix GH-10755: Memory leak in phar_rename_archive()

In phar_renmae_archive() context, added one reference but immediately
destroyed another, so do not need to increase refcount. With removal of
refcount++ line, PHP/Zend no longer reports memory leak.
Updated bug69958.phpt test file accordingly.

Closes GH-10856
