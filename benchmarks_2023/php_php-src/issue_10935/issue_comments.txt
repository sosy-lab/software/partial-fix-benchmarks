Static properties in traits don't override static properties from parent class
Simplified example:

https://3v4l.org/Dll1m
```php
class A {
    static $test;
}

class B extends A {
    static $test;
}

A::$test = 'A';
B::$test = 'B';
var_dump(A::$test, B::$test);
// string(1) "A"
// string(1) "B"
```

https://3v4l.org/SClLn
```php
trait Foo {
    static $test;
}

class A {
    use Foo;
}

class B extends A {
    use Foo;
}

A::$test = 'A';
B::$test = 'B';
var_dump(A::$test, B::$test);
// string(1) "B"
// string(1) "B"
```

The RFC is super old, there's no mention on how this is supposed to behave. The docs aren't telling either. It's reasonable to expect the same behavior as the case without traits.