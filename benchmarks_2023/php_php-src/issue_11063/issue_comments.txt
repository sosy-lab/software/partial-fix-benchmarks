 error: unknown field ‘arData’ specified in initializer
Hi @Jan1205. Can you specify what compiler you're using? Have you tried a full rebuild? (`make distclean`, `./buildconf`, `./configure`, `make`)?
That was my frist try. The error still appears. Unfortunately it is a very old system and version: gcc 4.1.2
That is indeed a very old version. Is there a particular reason why you can't upgrade? Would it be possible to try with a new version to confirm this is actually the issue? What `./configure` flags are you using?
@iluuu1994 Thanks for your reply. The migration is already being planned. So in the next time we will use a newer version. It works with perfectly with version 8.3. or newer. It would be great if there is a solution for the older version. My configure: 

`./configure --with-config-file-path=/etc --with-pear --with-mysqli=mysqlnd --with-zlib --enable-ftp --with-gettext --with-openssl --enable-mbstring --enable-calendar --with-curl --with-pdo-mysql --without-sqlite3 --without-pdo-sqlite --enable-gd --with-jpeg --with-freetype --with-zip --enable-sockets --enable-soap --enable-exif --enable-pcntl --enable-opcache --enable-fpm`

The issue only effects on PHP 8.2.

Thank you very much for your support.
This line produces the error:

`/bin/sh /usr/local/src/php-8.2.4/libtool --silent --preserve-dup-deps --tag CC --mode=compile cc -std=gnu99 -IZend/ -I/usr/local/src/php-8.2.4/Zend/ -I/usr/local/src/php-8.2.4/include -I/usr/local/src/php-8.2.4/main -I/usr/local/src/php-8.2.4 -I/usr/local/src/php-8.2.4/ext/date/lib -I/usr/local/include/libxml2 -I/usr/local/include -I/usr/local/include/libpng12 -I/usr/local/include/freetype2 -I/usr/local/src/php-8.2.4/ext/mbstring/libmbfl -I/usr/local/src/php-8.2.4/ext/mbstring/libmbfl/mbfl -I/usr/local/lib/libzip/include -I/usr/local/src/php-8.2.4/TSRM -I/usr/local/src/php-8.2.4/Zend  -D_GNU_SOURCE  -fno-common -Wstrict-prototypes -Wlogical-op -Wno-clobbered -Wall -Wextra -Wno-strict-aliasing -Wno-unused-parameter -Wno-sign-compare -lrt -fvisibility=hidden -DZEND_SIGNALS   -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1 -c /usr/local/src/php-8.2.4/Zend/zend_hash.c -o Zend/zend_hash.lo  -MMD -MF Zend/zend_hash.dep -MT Zend/zend_hash.lo`

When i remove "-std=gnu99" it works. Is there any configure options?
I think `-std=gnu99` comes from https://github.com/php/php-src/commit/b51a99ae358b3295de272bb3c3edb6913eeb0fd4. This has been removed in `master`. https://github.com/php/php-src/commit/bf5fdbd3a8bedfbd7216dd5a491bdb57f98a9958

Can you remove it locally to confirm it works without it? I'm really not proficient enough with the build system to know if backporting is a good idea. @Girgias maybe?

*Edit*: It looks like the macro is still added for autoconf <2.70 so backporting this should be safe.
When I remove `AC_PROG_CC_C99` the flag is still added. autoconf is > 2.70. bf5fdbd works not for me. When I remove the flag manually the build is not possible. PHP 8.1 can I build with `-std=gnu99`. @iluuu1994 many thany for your support!
Hmm, then I don't know :disappointed: 
Looks like an old GCC shortcoming with anonymous unions in structs: https://godbolt.org/z/v5frzzo8v (last version of GCC on Godbolt that breaks is 4.5.3, first working version is 4.6.4).
I have encountered the same error when trying to compile with:
gcc version 4.4.7 20120313 (Red Hat 4.4.7-23.0.1) (GCC)

I was able to compile PHP successfully. Edit _Zend/zend_hash.c_:

`
.arData = (Bucket*)&uninitialized_bucket[2],
      `

replace with

`
{.arData = (Bucket*)&uninitialized_bucket[2]},
      `
Notice: the fix have been reverted, see 8f66b67ccffad70fdd21e189539989e65bf484c2

GCC 4.1.2 may be from EL-5...

For memory
* EL-5 have GCC 4.1.2 and is EOL for ~6 years (after 10 years of support, so this version is 16 years old)
* EL-6 have GCC 4.4.7 and is EOL for ~3 years
* EL-7 have GCC 4.8.5 and will be EOL in ~1 year (but also have GCC 10, 11 or 12)

IMHO, it makes sense to stop supporting such old things and encouraging people to use dead cows.
(building recent PHP versions on EL-7 is already a nightmare, so many libraries being outdated)
Only reverted for 8.1 afaict, because only 8.2+ needed the fix.
Yes, @nielsdos is right. I merged this for 8.1, not realizing the array structure was different then.

I agree we shouldn't make large changes to support old versions. This change was small and seemed harmless.