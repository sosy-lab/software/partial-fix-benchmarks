FPM: Fix memory leak for invalid primary script file handle

Closes GH-11088
Fix bug GH-11246 cli/get_set_process_title

Fail to clobber_error only when the argv is a non-contiguous area
Don't increment the end_of_error if a non-contiguous area is encountered in environ
Fix bug GH-11246 cli/get_set_process_title initialisation failure on MacOS

Patch for PHP >= 8.2 due to commit b468d6f
Fix bug GH-11246 cli/get_set_process_title

Fail to clobber_error only when the argv is a non-contiguous area
Don't increment the end_of_error if a non-contiguous area is encountered in environ

Closes GH-11247
Merge branch 'PHP-8.1' into PHP-8.2

* PHP-8.1:
  Fix bug GH-11246 cli/get_set_process_title
Merge branch 'PHP-8.2'

* PHP-8.2:
  Fix bug GH-11246 cli/get_set_process_title
