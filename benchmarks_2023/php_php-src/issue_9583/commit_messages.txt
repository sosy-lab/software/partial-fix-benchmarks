Reorder conditions to avoid valgrind "Conditional jump or move depends on uninitialised value" warning.
Fix GH-9583: session_create_id() fails with user defined save handler that doesn't have a validateId() method
Fix GH-9583: session_create_id() fails with user defined save handler that doesn't have a validateId() method
Merge branch 'PHP-8.0' into PHP-8.1

* PHP-8.0:
  Fix GH-9583: session_create_id() fails with user defined save handler that doesn't have a validateId() method
Fix GH-9583: session_create_id() fails with user defined save handler that doesn't have a validateId() method
Merge branch 'PHP-8.1' into PHP-8.2

* PHP-8.1:
  Fix GH-9583: session_create_id() fails with user defined save handler that doesn't have a validateId() method
Merge branch 'PHP-8.2'

* PHP-8.2:
  Fix GH-9583: session_create_id() fails with user defined save handler that doesn't have a validateId() method
Actually fix GH-9583

The issue is that PS(mod)->s_validate_sid is always defined for user modules, thus we need to check that the actual callable is set
Add another regression test to ensure current working behaviour is not broken (which was by the previous incorrect fix)
Actually fix GH-9583

The issue is that PS(mod)->s_validate_sid is always defined for user modules, thus we need to check that the actual callable is set
Add another regression test to ensure current working behaviour is not broken (which was by the previous incorrect fix)
Actually fix GH-9583

The issue is that PS(mod)->s_validate_sid is always defined for user modules, thus we need to check that the actual callable is set
Add another regression test to ensure current working behaviour is not broken (which was by the previous incorrect fix)

Closes GH-9638
Merge branch 'PHP-8.0' into PHP-8.1

* PHP-8.0:
  Actually fix GH-9583
Merge branch 'PHP-8.1' into PHP-8.2

* PHP-8.1:
  Actually fix GH-9583
Actually fix GH-9583

The issue is that PS(mod)->s_validate_sid is always defined for user modules, thus we need to check that the actual callable is set
Add another regression test to ensure current working behaviour is not broken (which was by the previous incorrect fix)

Closes GH-9638
Merge branch 'PHP-8.2'

* PHP-8.2:
  Actually fix GH-9583
