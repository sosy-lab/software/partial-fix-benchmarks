Merge branch 'regression-tzid-with-numbers' into PHP-8.1
Fix GH-9769: Misleading error message for unpacking of objects

Only arrays can be unpacked in constant expressions.

Closes GH-9776.
Merge branch 'PHP-8.1' into PHP-8.2

* PHP-8.1:
  Fix GH-9769: Misleading error message for unpacking of objects
Merge branch 'PHP-8.2'

* PHP-8.2:
  Fix GH-9769: Misleading error message for unpacking of objects
