Revert "mpi_h: Add MPIX_C_FLOAT16 data type."

This reverts commit df34c95a4b5a0cad91a86f8cc916ec942cd3be6f.
request: Fix duplicate symbol with profiling library

Moves profiling macros to the top of waitany.c to be consistent with
other files in src/mpi. The preprocessor guards ensure we only include
MPIR_Waitany_impl in libpmpi. Otherwise the linker may throw an error
when statically building an MPI program. See pmodels/mpich#3449.
mpi: Fix duplicate symbols with profiling library

Tweak preprocessor guards to ensure that internal functions
MPIR_Waitany_impl, MPIR_Op_create_impl, and MPIR_Op_free_impl only
appear once when building a separate profiling library. Otherwise the
linker may throw a duplicate symbol error when statically building an
MPI program. See pmodels/mpich#3449.
mpi: Fix duplicate symbols with profiling library

Tweak preprocessor guards to ensure that internal functions
MPIR_Waitany_impl, MPIR_Op_create_impl, and MPIR_Op_free_impl only
appear once when building a separate profiling library. Otherwise the
linker may throw a duplicate symbol error when statically building an
MPI program. See pmodels/mpich#3449.

Co-authored-by: Kavitha Tiptur Madhu <kmadhu@anl.gov>
mpi: Fix duplicate symbols with profiling library

Tweak preprocessor guards to ensure that internal functions
MPIR_Waitany_impl, MPIR_Op_create_impl, and MPIR_Op_free_impl only
appear once when building a separate profiling library. Otherwise the
linker may throw a duplicate symbol error when statically building an
MPI program. See pmodels/mpich#3449.

Co-authored-by: Kavitha Tiptur Madhu <kmadhu@anl.gov>
Tested-by: Satish Balay <balay@mcs.anl.gov>
Signed-off-by: Halim Amer <aamer@anl.gov>
mpi: Fix duplicate symbols with profiling library

Tweak preprocessor guards to ensure that internal functions
MPIR_Waitany_impl, MPIR_Op_create_impl, and MPIR_Op_free_impl only
appear once when building a separate profiling library. Otherwise the
linker may throw a duplicate symbol error when statically building an
MPI program. See pmodels/mpich#3449.

Co-authored-by: Kavitha Tiptur Madhu <kmadhu@anl.gov>
Tested-by: Satish Balay <balay@mcs.anl.gov>
Signed-off-by: Halim Amer <aamer@anl.gov>
mpi: Fix duplicate symbols with profiling library

Tweak preprocessor guards to ensure that internal functions
MPIR_Waitany_impl, MPIR_Op_create_impl, and MPIR_Op_free_impl only
appear once when building a separate profiling library. Otherwise the
linker may throw a duplicate symbol error when statically building an
MPI program. See pmodels/mpich#3449.

Co-authored-by: Kavitha Tiptur Madhu <kmadhu@anl.gov>
Tested-by: Satish Balay <balay@mcs.anl.gov>
Signed-off-by: Halim Amer <aamer@anl.gov>
hwloc: use exported LDFLAGS from HWLOC.

Without the LDFLAGS detected by hwloc, we might miss out on external
library flags that are necessary for the build.  This showed up on Mac
OS, when OpenCL was detected by hwloc, but the corresponding flags
were not added to mpich's wrapper scripts.

Fixes #3449.
hwloc: use exported LDFLAGS from HWLOC.

Without the LDFLAGS detected by hwloc, we might miss out on external
library flags that are necessary for the build.  This showed up on Mac
OS, when OpenCL was detected by hwloc, but the corresponding flags
were not added to mpich's wrapper scripts.

Fixes #3449.
hwloc: use exported LDFLAGS from HWLOC.

Without the LDFLAGS detected by hwloc, we might miss out on external
library flags that are necessary for the build.  This showed up on Mac
OS, when OpenCL was detected by hwloc, but the corresponding flags
were not added to mpich's wrapper scripts.

Fixes #3449.
