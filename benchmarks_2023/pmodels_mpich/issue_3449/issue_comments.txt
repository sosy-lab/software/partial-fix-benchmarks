build: mpich-3.3 static link error on OSX
This seems to be coming from `hwloc`.  @KavithaTipturMadhu can you look into it?
I am looking into this, will update it once I have more info.
I built the static library with the same config flags and static mpi lib on osx. cpi seems to run just fine. Can I get access to your build or details of compiler and linker versions? 
Its latest OSX with latest Xcode. We use gfortran from brew.

```
balay@ipro^~ $ sw_vers 
ProductName:	Mac OS X
ProductVersion:	10.14
BuildVersion:	18A391
balay@ipro^~ $ xcodebuild -version
Xcode 10.1
Build version 10B61
balay@ipro^~ $ clang --version
Apple LLVM version 10.0.0 (clang-1000.11.45.5)
Target: x86_64-apple-darwin18.0.0
Thread model: posix
InstalledDir: /Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin
balay@ipro^~ $ 

```
Note: the shared build is linked in with OpenCL - so presumably a static build presumably needs an explicit OpenCL link in the mpi wrapper.

```
balay@ipro^/Users/petsc/petsc.next-tmp-3/arch-osx-10.6-cxx-cmplx-pkgs-dbg/lib((f8627fd784...)) $ otool -L libpmpi.dylib 
libpmpi.dylib:
	/Users/petsc/petsc.next-tmp-3/arch-osx-10.6-cxx-cmplx-pkgs-dbg/lib/libpmpi.12.dylib (compatibility version 14.0.0, current version 14.6.0)
	/System/Library/Frameworks/OpenCL.framework/Versions/A/OpenCL (compatibility version 1.0.0, current version 1.0.0)
	/usr/local/opt/gcc/lib/gcc/8/libgfortran.5.dylib (compatibility version 6.0.0, current version 6.0.0)
	/usr/lib/libSystem.B.dylib (compatibility version 1.0.0, current version 1252.200.5)
	/usr/local/lib/gcc/8/libgcc_s.1.dylib (compatibility version 1.0.0, current version 1.0.0)
	/usr/local/opt/gcc/lib/gcc/8/libquadmath.0.dylib (compatibility version 1.0.0, current version 1.0.0)

```
```
balay@ipro^~/petsc/arch-darwin-c-debug/externalpackages/mpich-3.3/src/hwloc((f8627fd784...)) $ ./configure --help |grep opencl
  --disable-opencl        Disable the OpenCL device discovery
```
I guess I should use this option to workaround this issue.
Now I get duplicate symbol errors

```
balay@ipro^~/petsc/src/ksp/ksp/examples/tutorials((f8627fd784...)) $ /Users/balay/petsc/arch-darwin-c-debug/bin/mpicc -g3  -o ex2 ex2.o  -L/Users/balay/petsc/arch-darwin-c-debug/lib  -L/usr/local/Cellar/gcc/8.2.0/lib/gcc/8 -lpetsc -llapack -lblas -lgfortran
duplicate symbol _MPIR_Op_create_impl in:
    /Users/balay/petsc/arch-darwin-c-debug/lib/libmpi.a(lib_libmpi_la-op_create.o)
    /Users/balay/petsc/arch-darwin-c-debug/lib/libpmpi.a(lib_libpmpi_la-op_create.o)
duplicate symbol _MPIR_Op_free_impl in:
    /Users/balay/petsc/arch-darwin-c-debug/lib/libmpi.a(lib_libmpi_la-op_free.o)
    /Users/balay/petsc/arch-darwin-c-debug/lib/libpmpi.a(lib_libpmpi_la-op_free.o)
duplicate symbol _MPIR_Waitany_impl in:
    /Users/balay/petsc/arch-darwin-c-debug/lib/libmpi.a(lib_libmpi_la-waitany.o)
    /Users/balay/petsc/arch-darwin-c-debug/lib/libpmpi.a(lib_libpmpi_la-waitany.o)
ld: 3 duplicate symbols for architecture x86_64
clang: error: linker command failed with exit code 1 (use -v to see invocation)
balay@ipro^~/petsc/src/ksp/ksp/examples/tutorials((f8627fd784...)) $ nm -A /Users/balay/petsc/arch-darwin-c-debug/lib/lib*mpi.a |grep _MPIR_Op_create_impl
/Users/balay/petsc/arch-darwin-c-debug/lib/libmpi.a:lib_libmpi_la-op_create.o: 0000000000000000 T _MPIR_Op_create_impl
/Users/balay/petsc/arch-darwin-c-debug/lib/libpmpi.a:lib_libpmpi_la-op_create.o: 00000000000001e0 T _MPIR_Op_create_impl
balay@ipro^~/petsc/src/ksp/ksp/examples/tutorials((f8627fd784...)) $ 

```
At least waitany was broken in 7e047915e3ac8365082beeb6b9ded8333481bf48. @KavithaTipturMadhu I have a fix I will push. I'm sure we'll need something similar for the duplicate op symbols.
@raffenet @KavithaTipturMadhu , Thanks for the fixes. My builds go past this link error now.
Great. Thanks for confirming.
On second thought I reopened this so we can more fully investigate the OpenCL issue.
Yes the primary issue with OpenCL still exists. [I'm just working arround it].
@balay I don't think this is an OpenCL issue, per se.  I ran into this issue on my mac and it turned out that I had bad `binutils` installed through HomeBrew (specifically bad `ar`).  `binutils` installation shows a warning while installing about this.  Remove it and use the system-native `ar` and it seems to build fine for me.  I'm going to close this as "fixed", but please reopen if you still face this issue.
```
balay@ipro^~ $ which ar
/usr/bin/ar
```

I haven't tried a build in a while - but I didn't have a bad 'ar' in PATH