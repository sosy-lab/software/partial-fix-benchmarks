ifdef auto_ptr
Fix issue #1950 Address familiy Exception

Add workaround for clang, move inline functions from header to source file
#264, #1684, #1950
Feature gradle (#2031)

* Workaround bug in SolarisStudio 12.4 on RVO-ed objects.

* HttpClientSession set specific proxysettings for attached socket

If we have a global proxy settings and we attach an external socket to the HTTPClientSession there's no way to use different proxy settings for that connection. For example if you do not need for that connection httpproxy because is already attached to the correct and point

* Fix ".. has no member named ... compile error" (#1938)

* Fix ".. has no member named ... compile error" by renaming apache conn_rec
attributes

 - conn_rec attributes remote_ip and remote_addr were replaced by client_ip
 and client_addr once they have been renamed in Apache 2.4
 - a server_rec pointer must be passed to ap_log_error() since apache 2.4,
 therefore, a change was necessary at the ap_log_error log function.
 A null pointer has been passed for avoiding deeper changes at the function.
 - the smart pointer auto_ptr was replaced by unique_ptr once it was made
 deprecated in C++11 standard, it has been replaced by unique_ptr.

* Add the properly #ifdef directives for backward compatibility purposes

 - Adding proper #ifdef preprocessor directives to keeping backward
 compatibility with older apache versions.

* Update ApacheConnector.cpp

* Add Gradle build.scripts

Signed-off-by: zosrothko <zosrothko@orange.fr>

* New files

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add distrib directory

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add PostgreSQL. Remove POCO_NO_WSTRING

Signed-off-by: zosrothko <zosrothko@orange.fr>

* CYgwin: remove -DPOCO_NO_WSTRING & build Data/PostgreSQL (#1942)

* Upgrade to mysql-5.7.19-win32 (#1944)

* fix -Woverloaded-virtual

* remove return

* SyntaxException for DateTimeParser::parse not working #569

* remove leftover comment

* fix some warnings

* purge trailing whitespace (#1947)

* add Poco::makeUnique()

* NTP Packet impl not according to RFC958? #749

* Poco::Data ODBC impl doesn't bind to unsigned numeric types properly #1683

* Remove useless windows commands (#1948)

* Remove useless Windows commands

* Generate build_cmd only for VS140 & VS150

* Display target configuration

* Upgrade to mysql-5.7.19 (#1951)

* Travis & AppVeyor: Unbind PDF module (#1953)

* Unbind PDF

* Upgrade to mysql-5.7.19

* Put Cygwin ahead

* Add --omit=PDF

* Display target configuration (#1952)

* #1878 Add OrangePi on Build Support (#1949)

- Created the configuration `OrangePi`, based on ARM-Linux,
  with fine tune for OrangePi
- I tested using Poco samples on OrangePi Zero

Signed-off-by: Uilian Ries <uilianries@gmail.com>

* fix mysql odbc tests

* Renamed directory distrib to packaging

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update .gradle/

* Restore lost changes by git

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update openssl path

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Comment out displaying the compiler log

* Create issue_template.md

* Compile all C source as C++ source

* Add gradle submodule

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Remove /TP for compiling C code

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Reinsert SemiStaticLibrary build

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Fixed invalid merge

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Missing files

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Oracle ODBC fixes

* restore deleted documentation

* wrong field size calculation in ODBC code #1659; other max size excession checks and testcase

* Rebuild PocoDoc

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Remove deleted includes

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Align with develop branch

* Add task pocoTasks and dependency with nuget & candle

* some fixes (mostly unicode/bulk)

* #264, #1684, #1950

* Buffer<> swap miss ownMem #1964

* speling fix

* speling fix

* make uninstall does not remove .so symlinks #1017

* add uninstall to phony

* Base64Encoder/Decoder: added support for 'base64url' encoding

* removed $ tags

* remove remaining $Id$ tags from file headers

* Fix/tcp dispatcher (#1965)

* TCPServerDispatcher::run() issue #1884; make integral members atomic and minimize locking

* Update TCPServerDispatcher.cpp

* fix test

* fix test with gcc

* fixed GH #1934: File::setExecutable() on POSIX should set executable bit for group and other if corresponding readable bit is set

* Implement MailMessage::decode #1543 (wip)

* added PollSet class

* updated VS project files for PollSet

* PollSet: on Windows, use WSAPoll if available

* GH #1412: added Poco::DigestEngine::constantTimeEquals()

* - fix Zip Decompress Parent Path Injection #1968 bug
- add valid patch check test
- add vulnearbility triggering zip archive and test
- remove temporary test output files
- if possible, redirect temporary file generation to temp directory or
- delete temporary files after tests

* fix relative path check, eliminate unused warnings

* minor fixes

* minor fixes

* Implement MailMessage::decode #1543 (wip 2)

* fix warning

* only convert encoded-word if explicitly requested

* Add kit version 10 for message compiler (#1978)

* Restore _ostr(std::cout) to avoid timeouts on AppVeyor (#1980)

* additional fix for GH #1212: WebSocketImpl::available() now reports number of bytes in internal buffer.

* fixed GH #1828: DeflatingStreamBuf::sync() should also flush underlying stream.

* Implement MailMessage::decode #1543 (tentatively done); add encode 'B', decode 'Q' and 'B'

* TextEncodingRegistry documentation

* merged connection string URI support from 1.8

* fixed GH #1425: Workaround bug in SolarisStudio 12.4 on RVO-ed objects.

* Remove Cygwin build (#1985)

* fixed GH #1404: Add Poco::Data::Statement::bind() method

* GH #1988: Remove OpenVMS support

* replace strerror() with Poco::Error::getMessage()

* replace strerror() with Poco::Error::getMessage()

* upgraded bundled SQLite to 3.21.0

* Fix writing into closed socket from streambuf

In case of error occured in writeToDevice pptr may become one byte
farther than epptr. This can lead to crash in streambuf::xsputn from
libstdc++.

* CMake patches for FreeBSD (#1989)

* Switch FreeBSD to poll

* Link against dl and rt on FreeBSD

* pd_json strerror deprecation warning on Windows #1984

* revert #1828

* Backport from poco-1.8.0

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add coverage directory

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Backport from poco-1.8.0

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add coverage task & tools

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Removed

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Backport from poco-1.8.0

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Added

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add OpenCppCoverage & ReportGenerator tasks

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add CPPUNIT_IGNORE variable

Signed-off-by: zosrothko <zosrothko@orange.fr>

* style fix

* updated README.md

* Add mechanism to start a task from within a task (#1287)

* Add mechanism to start a task from within a task

Staying in the same thread.

* Provide seeds for a task queue creation

TaskManager::taskFinished removes the finished task from the task list
before dispatching the taskFinished notification

* fixup! Add mechanism to start a task from within a task

* fixup! Add mechanism to start a task from within a task

* Add Task::yield

on the same model as Task::sleep

* implement Poco::SharedPtr using std::shared_ptr (#1993)

* added additional move constructor and assignment operators

* Fix building XMLStreamParser with unbundled expat

* Add Directory for coverage task

* Remove Cygwin build that exceeds 2 hours and reaches the timeout

* WiX Poco wxs should not port the Poco version

* Harden RecursiveDirectoryIterator when walking the filesystem. (#2001)

* In the implementation for the *Traverse strategies the next method performs an unguarded list directory.  If the directory is not accessible an unrecoverable error is raised thus ruining the walk.  This changeset adopts and adapts the error handling protocol as defined in Python's os.walk function where errors from listdir are ignored or are reported to an optional on error callback function.

* Expand DirectoryIteratorsTest testsuite to confirm the hardened iterator behaviour over unreadable directories.

* Expand DirectoryIteratorsTest testsuite to confirm the hardened iterator behaviour over
  unreadable directories.  Correct bad formatting

* fix clang compile

* SharePtr fix for gcc 7.2 (#2004)

* Fix EVPTest on RHEL/Fedora by removing hard-coded EC curve name (#2002)

RHEL/Fedora seem to have a much more limited set of EC curves available by
default.  This change will instead use the first curve name as used in other
places.

* Parallel C++ compiler jobs limited to 2

* Add missing '\''

* Removed

* Cleanup

* Cleanup

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Remove removed XXXX_WIN32.h includes

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update for VisualStudio 2017

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add debug log

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Refactoring

Signed-off-by: zosrothko <zosrothko@orange.fr>

* dos2unix

Signed-off-by: zosrothko <zosrothko@orange.fr>

* SQLToMongoDB does not build

* Merge

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Exclude Crypto testsuite for now

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update for VS2017

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update for VS2017

Signed-off-by: zosrothko <zosrothko@orange.fr>
Poco by Gradle (#2065)

* Workaround bug in SolarisStudio 12.4 on RVO-ed objects.

* HttpClientSession set specific proxysettings for attached socket

If we have a global proxy settings and we attach an external socket to the HTTPClientSession there's no way to use different proxy settings for that connection. For example if you do not need for that connection httpproxy because is already attached to the correct and point

* Fix ".. has no member named ... compile error" (#1938)

* Fix ".. has no member named ... compile error" by renaming apache conn_rec
attributes

 - conn_rec attributes remote_ip and remote_addr were replaced by client_ip
 and client_addr once they have been renamed in Apache 2.4
 - a server_rec pointer must be passed to ap_log_error() since apache 2.4,
 therefore, a change was necessary at the ap_log_error log function.
 A null pointer has been passed for avoiding deeper changes at the function.
 - the smart pointer auto_ptr was replaced by unique_ptr once it was made
 deprecated in C++11 standard, it has been replaced by unique_ptr.

* Add the properly #ifdef directives for backward compatibility purposes

 - Adding proper #ifdef preprocessor directives to keeping backward
 compatibility with older apache versions.

* Update ApacheConnector.cpp

* Add Gradle build.scripts

Signed-off-by: zosrothko <zosrothko@orange.fr>

* New files

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add distrib directory

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add PostgreSQL. Remove POCO_NO_WSTRING

Signed-off-by: zosrothko <zosrothko@orange.fr>

* CYgwin: remove -DPOCO_NO_WSTRING & build Data/PostgreSQL (#1942)

* Upgrade to mysql-5.7.19-win32 (#1944)

* fix -Woverloaded-virtual

* remove return

* SyntaxException for DateTimeParser::parse not working #569

* remove leftover comment

* fix some warnings

* purge trailing whitespace (#1947)

* add Poco::makeUnique()

* NTP Packet impl not according to RFC958? #749

* Poco::Data ODBC impl doesn't bind to unsigned numeric types properly #1683

* Remove useless windows commands (#1948)

* Remove useless Windows commands

* Generate build_cmd only for VS140 & VS150

* Display target configuration

* Upgrade to mysql-5.7.19 (#1951)

* Travis & AppVeyor: Unbind PDF module (#1953)

* Unbind PDF

* Upgrade to mysql-5.7.19

* Put Cygwin ahead

* Add --omit=PDF

* Display target configuration (#1952)

* #1878 Add OrangePi on Build Support (#1949)

- Created the configuration `OrangePi`, based on ARM-Linux,
  with fine tune for OrangePi
- I tested using Poco samples on OrangePi Zero

Signed-off-by: Uilian Ries <uilianries@gmail.com>

* fix mysql odbc tests

* Renamed directory distrib to packaging

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update .gradle/

* Restore lost changes by git

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update openssl path

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Comment out displaying the compiler log

* Create issue_template.md

* Compile all C source as C++ source

* Add gradle submodule

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Remove /TP for compiling C code

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Reinsert SemiStaticLibrary build

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Fixed invalid merge

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Missing files

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Oracle ODBC fixes

* restore deleted documentation

* wrong field size calculation in ODBC code #1659; other max size excession checks and testcase

* Rebuild PocoDoc

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Remove deleted includes

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Align with develop branch

* Add task pocoTasks and dependency with nuget & candle

* some fixes (mostly unicode/bulk)

* #264, #1684, #1950

* Buffer<> swap miss ownMem #1964

* speling fix

* speling fix

* make uninstall does not remove .so symlinks #1017

* add uninstall to phony

* Base64Encoder/Decoder: added support for 'base64url' encoding

* removed $ tags

* remove remaining $Id$ tags from file headers

* Fix/tcp dispatcher (#1965)

* TCPServerDispatcher::run() issue #1884; make integral members atomic and minimize locking

* Update TCPServerDispatcher.cpp

* fix test

* fix test with gcc

* fixed GH #1934: File::setExecutable() on POSIX should set executable bit for group and other if corresponding readable bit is set

* Implement MailMessage::decode #1543 (wip)

* added PollSet class

* updated VS project files for PollSet

* PollSet: on Windows, use WSAPoll if available

* GH #1412: added Poco::DigestEngine::constantTimeEquals()

* - fix Zip Decompress Parent Path Injection #1968 bug
- add valid patch check test
- add vulnearbility triggering zip archive and test
- remove temporary test output files
- if possible, redirect temporary file generation to temp directory or
- delete temporary files after tests

* fix relative path check, eliminate unused warnings

* minor fixes

* minor fixes

* Implement MailMessage::decode #1543 (wip 2)

* fix warning

* only convert encoded-word if explicitly requested

* Add kit version 10 for message compiler (#1978)

* Restore _ostr(std::cout) to avoid timeouts on AppVeyor (#1980)

* additional fix for GH #1212: WebSocketImpl::available() now reports number of bytes in internal buffer.

* fixed GH #1828: DeflatingStreamBuf::sync() should also flush underlying stream.

* Implement MailMessage::decode #1543 (tentatively done); add encode 'B', decode 'Q' and 'B'

* TextEncodingRegistry documentation

* merged connection string URI support from 1.8

* fixed GH #1425: Workaround bug in SolarisStudio 12.4 on RVO-ed objects.

* Remove Cygwin build (#1985)

* fixed GH #1404: Add Poco::Data::Statement::bind() method

* GH #1988: Remove OpenVMS support

* replace strerror() with Poco::Error::getMessage()

* replace strerror() with Poco::Error::getMessage()

* upgraded bundled SQLite to 3.21.0

* Fix writing into closed socket from streambuf

In case of error occured in writeToDevice pptr may become one byte
farther than epptr. This can lead to crash in streambuf::xsputn from
libstdc++.

* CMake patches for FreeBSD (#1989)

* Switch FreeBSD to poll

* Link against dl and rt on FreeBSD

* pd_json strerror deprecation warning on Windows #1984

* revert #1828

* Backport from poco-1.8.0

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add coverage directory

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Backport from poco-1.8.0

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add coverage task & tools

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Removed

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Backport from poco-1.8.0

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Added

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add OpenCppCoverage & ReportGenerator tasks

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add CPPUNIT_IGNORE variable

Signed-off-by: zosrothko <zosrothko@orange.fr>

* style fix

* updated README.md

* Add mechanism to start a task from within a task (#1287)

* Add mechanism to start a task from within a task

Staying in the same thread.

* Provide seeds for a task queue creation

TaskManager::taskFinished removes the finished task from the task list
before dispatching the taskFinished notification

* fixup! Add mechanism to start a task from within a task

* fixup! Add mechanism to start a task from within a task

* Add Task::yield

on the same model as Task::sleep

* implement Poco::SharedPtr using std::shared_ptr (#1993)

* added additional move constructor and assignment operators

* Fix building XMLStreamParser with unbundled expat

* Add Directory for coverage task

* Remove Cygwin build that exceeds 2 hours and reaches the timeout

* WiX Poco wxs should not port the Poco version

* Harden RecursiveDirectoryIterator when walking the filesystem. (#2001)

* In the implementation for the *Traverse strategies the next method performs an unguarded list directory.  If the directory is not accessible an unrecoverable error is raised thus ruining the walk.  This changeset adopts and adapts the error handling protocol as defined in Python's os.walk function where errors from listdir are ignored or are reported to an optional on error callback function.

* Expand DirectoryIteratorsTest testsuite to confirm the hardened iterator behaviour over unreadable directories.

* Expand DirectoryIteratorsTest testsuite to confirm the hardened iterator behaviour over
  unreadable directories.  Correct bad formatting

* fix clang compile

* SharePtr fix for gcc 7.2 (#2004)

* Fix EVPTest on RHEL/Fedora by removing hard-coded EC curve name (#2002)

RHEL/Fedora seem to have a much more limited set of EC curves available by
default.  This change will instead use the first curve name as used in other
places.

* Parallel C++ compiler jobs limited to 2

* Updated to PCRE version 8.41

Testing Done: Built on Windows OS for all configurations.

* Add missing '\''

* Removed

* Cleanup

* Cleanup

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Remove removed XXXX_WIN32.h includes

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update for VisualStudio 2017

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Add debug log

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Refactoring

Signed-off-by: zosrothko <zosrothko@orange.fr>

* dos2unix

Signed-off-by: zosrothko <zosrothko@orange.fr>

* SQLToMongoDB does not build

* Merge

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Exclude Crypto testsuite for now

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update for VS2017

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Update for VS2017

Signed-off-by: zosrothko <zosrothko@orange.fr>

* Fixed performance issue: destructor of Poco::Timespan was not inlined [#CLICKHOUSE-3458].

* fixed GH #2038: Poco::Net::MultipartWriter::createBoundary() always returns the same string.

* GH #2039: support for nested multi-part content

* Small code style change (#2043)

Making operator precedence explicit.

* Add JSON in the includes path (#2027)

* merge File::linkTo() from 1.8.1

* remove volatile

* # 2042

* SQLite not handling parameter count mismatch correctly #2020

* Data/SQLite: Exception messages contain duplicate text #2012

* Travis CI (#2050)

* Factorize info into new verbose file. Refactor Makefile, global, cpp11*. Add Linux32-clang

* Display used config with POCO_VERBOSE

* Add cross compilation toward x86 with host amd64

* Refactor config names

* Add lib32gcc runtime

* Add g++-5-multilib

* Use OSARCH=i386 for OSX x86

* Avoid building Crypto since OpenSSL is only x64 on OSX

* Avoid building Crypto since OpenSSL is only x64

* Avoid Data/* on cross compilation to x86

* Add gcc-5-multilib to clang 4.0 x86

* Ignore TimerTest on OSX for now.

* Cleanup

* Add other set of TimerTest.

* New test that fails on OSX

* Add TimerTest.testScheduleInterval() (#2053)

* Poco::Net::NetworkInterface::list does not list inactive interfaces even when explicitly being asked for it #2044

* remove deprecated NetworkInterface typedef

* move upgraded PDF from 1.8

* Update for VS2017

* Add Util::TimerTest

* fix OSX NetworkInterface test

* fix NetworkInterface test
