PollSet not working as intended on Windows
not really fixed.
AFAICT so far, this is unfixable on Windows because different OS versions behave differently - eg. on Windows 10 `WSAPoll` reports socket trying to asynchronously connect to a non-existent server, but on Windows Server 2019 it does not.
tests disabled for windows.
A `WSAEventSelect`-based option also [tried](https://github.com/pocoproject/poco/tree/try/pollset-wsa-event-select); not viable because it implicitly sets non-blocking mode on sockets and expects they remain so.