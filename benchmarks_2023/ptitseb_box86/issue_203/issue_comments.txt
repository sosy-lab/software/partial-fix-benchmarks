handling /proc/self/exe in execv function
Thank you for changing the code for me.
Can you handle execve?
thank you!
3041|SIGSEGV @0x28bc968 (my_execve (./box86/box86)) (x86pc=0x45ec380/"???"), for accessing (nil) (code=1), db=(nil)((nil)/???)
Segmentation fault
Oh, now execve cannot run.
execlp is also.
And, execvp executes /proc/self/exe output
Error: / is not an executable file
do you have a sample that you can share?
#include<unistd.h>
int main(){
execve("/proc/self/exe",NULL,NULL);
}
#include<unistd.h>
int main()
{
execlp("/proc/self/exe",NULL);
}
#include<unistd.h>
int main(){
char *a[]={" ",0};
execvp("/proc/self/exe",a);
}
execv and execl are running normally.
That progam always start itself... => It never end. That's intended?
Also, are you sure `execve("/proc/self/exe",NULL,NULL);` is legal? I don't think `argv` or `envp` parameter can be NULL?
Oh, argv and envp really cannot be null.
But execve still has problems.
Cannot execute any file using execve.
#include<unistd.h>
int main(){
        char *argv[]={" ",NULL};
        char *envp[]={"0",0};
        execve("/proc/self/exe",argv,envp);
}
pi@raspberrypi:~ $ ./a.out
Dynarec for ARM, with extension: HALF FAST_MULT EDSP NEON VFPv4 IDIVA PageSize:4096
Using default BOX86_LD_LIBRARY_PATH: ./:lib/:lib32/:x86/
Using default BOX86_PATH: ./:bin/
Counted 24 Env var
Looking for ./a.out
Using native(wrapped) libc.so.6
Using native(wrapped) ld-linux.so.2
Using native(wrapped) libpthread.so.0
Using native(wrapped) librt.so.1
Dynarec for ARM, with extension: HALF FAST_MULT EDSP NEON VFPv4 IDIVA PageSize:4096
Using default BOX86_LD_LIBRARY_PATH: ./:lib/:lib32/:x86/
Using default BOX86_PATH: ./:bin/
Counted 3 Env var
1321|SIGSEGV @0xb6f861dc (???) (x86pc=0xffffffff/"???"), for accessing (nil) (code=1), db=(nil)((nil)/???)
Segmentation fault
Oh, I was wrong again, I let it execute /bin/ls, I didn't notice that this is an arm elf.
execve can execute /proc/self/exe normally.
Sorry.
Now, when execvp executes /proc/self/exe, it will be box86 itself,
Debug level is 2
Dynarec for ARM, with extension: HALF FAST_MULT EDSP NEON VFPv4 IDIVA PageSize:4096
Setting up canary (for Stack protector) at GS:0x14, value:00ECE328
Using default BOX86_LD_LIBRARY_PATH: ./:lib/:lib32/:x86/
Using default BOX86_PATH: ./:bin/                                                       Counted 25 Env var
Looking for ./a.out
Read 29 Section header
Read 9 Program header
Loading Sections Table String (idx = 28)
Loading SymTab Strings (idx = 27)
Loading SymTab (idx = 26)
Loading Dynamic (idx = 21)
Rel Table @0x368 (0x40/0x8)
PLT Table @0x3a8 (type=17 0x18/0x8)
The GOT Table is at address 0x1fd4
The .init is at address 0x3c0
The .init_array is at address 0x1ed4, and have 1 elements
The .fini is at address 0x684
The .fini_array is at address 0x1ed8, and have 1 elements
The .text is at address 0x440, and is 580 big
Loading DynSym Strings (idx = 6)
Loading DynSym (idx = 5)
Adding "./a.out" as #0 in elf collection
Elf Addr(v/p)=(nil)/(nil) Memsize=0x200c (align=0x1000)
Elf Stack Memsize=1048576 (align=16)
Elf TLS Memsize=0 (align=0)
Allocating 0x200c memory @(nil) for Elf "./a.out"
Got 0xb6f46000 (delta=0xb6f46000)
Loading block #2 @0xb6f46000 (0x810/0x810)
Loading block #3 @0xb6f47ed4 (0x134/0x138)
Calc stack size, based on 1 elf(s)
Stack is @0xb60a1020 size=0x800000 align=0x10
Allocate a new X86 Emu, with EIP=(nil) and Stack=0xb60a1020/0x800000
Setup X86 Emu
Trying to add "libc.so.6" to maplib
Trying to load "libc.so.6"
Simplified name is "libc.so.6"
Using native(wrapped) libc.so.6
Trying to add "ld-linux.so.2" to maplib
Trying to load "ld-linux.so.2"
Simplified name is "ld-linux.so.2"
Using native(wrapped) ld-linux.so.2
Created lib and added to maplib => success
Trying to add "libpthread.so.0" to maplib
Trying to load "libpthread.so.0"
Simplified name is "libpthread.so.0"
Using native(wrapped) libpthread.so.0
Created lib and added to maplib => success
Trying to add "librt.so.1" to maplib
Trying to load "librt.so.1"
Simplified name is "librt.so.1"
Using native(wrapped) librt.so.1
Created lib and added to maplib => success
Created lib and added to maplib => success
And now export symbols / relocation for ./a.out...
Applying 8 Relocation(s) for ./a.out
PLT Resolver injected in plt.got at 0xb6f47fdc
Applying 3 PLT Relocation(s) for ./a.out
Entry Point is 0xb6f46440
Start x86emu on Main
Run X86 (0x3805080), EIP=0xb6f46440, Stack=0xb60a1020
 ...  =>PltResolver: Addr=0x38031f0, Slot=16 Return=0xb6f46471: elf is ./a.out
PltReolver: Apply Global R_386_JMP_SLOT @0xb6f47fe8 with sym=__libc_start_main (0xb6f46426 -> 0x380859a / (noelf))
 return 0xB6F46600
3238|0xb6f46471: Calling __libc_start_main (B6F4657D, 00000001, B68A06E8...) =>Calling init(0xb6f46600) from __libc_start_main
3238|0xb6f463de: Calling __gmon_start__ (00000000, 00000000, B6F47FD4...) =>__gmon_start__ called (dummy call)
 return 0x03808534
Transfert to main(1, 0x38024e0, 0x3802730)=>0xb6f4657d from __libc_start_main            ...  =>PltResolver: Addr=0x38031f0, Slot=8 Return=0xb6f465d1: elf is ./a.out
PltReolver: Apply Global R_386_JMP_SLOT @0xb6f47fe4 with sym=execvp (0xb6f46416 -> 0x3808589 / (noelf))                                                                          return 0xB6F47FD4
3238|0xb6f465d1: Calling execvp (B6F466A2, B68A0680, 03802730...) =>Box86 with Dynarec v0.1.3 2c6e916 built on Sep 19 2020 17:19:22


This is Box86, the Linux x86 emulator with a twist
                                                                                        Usage is box86 [options] path/to/software [args]
to launch x86 software
 options can be :                                                                           '-v'|'--version' to print box86 version and quit
    '-h'|'--help'    to print box86 help and quit                                       You can also set some environment variables:
 BOX86_PATH is the box86 version of PATH (default is '.:bin')
 BOX86_LD_LIBRARY_PATH is the box86 version LD_LIBRARY_PATH (default is '.:lib')
 BOX86_LOG with 0/1/2/3 or NONE/INFO/DEBUG/DUMP to set the printed debug info
 BOX86_DYNAREC_LOG with 0/1/2/3 or NONE/INFO/DEBUG/DUMP to set the printed dynarec info
 BOX86_DYNAREC with 0/1 to disable or enable Dynarec (On by default)
 BOX86_DYNAREC_LINKER with 0/1 to disable or enable Dynarec Linker (On by default, use 0 only for easier debug)
 BOX86_TRACE_FILE with FileName to redirect logs in a file BOX86_DLSYM_ERROR with 1 to log dlsym errors
 BOX86_LOAD_ADDR=0xXXXXXX try to load at 0xXXXXXX main binary (if binary is a PIE)
 BOX86_NOSIGSEGV=1 to disable handling of SigSEGV
 BOX86_NOSIGILL=1  to disable handling of SigILL
 BOX86_X11THREADS=1 to call XInitThreads when loading X11 (for old Loki games with Loki_Compat lib) BOX86_LIBGL=libXXXX set the name (and optionnaly full path) for libGL.so.1
 BOX86_LD_PRELOAD=XXXX[:YYYYY] force loading XXXX (and YYYY...) libraries with the binary
 BOX86_ALLOWMISSINGLIBS with 1 to allow to continue even if a lib is missing (unadvised, will probably  crash later)
I haven't handled execvp like the other for now. It's on my todo, but I have to think a bit more for this one. It'll come later.
I cannot use the execve syscall now.
Error: Unsupported libc Syscall 0x0B (11)
You should let the system call execve be implemented by the my_execve function.
Now, I use the int $80 instruction to call execve to execute /proc/self/exe,  will execute box86.
Can I use box86 to run busybox and gcc?
I don't know. Try.
Error: Global Symbol lutimes not found, cannot apply R_386_GLOB_DAT @0xec111fb4 ((nil)) in /busybox
Error: Global Symbol xdr_int not found, cannot apply R_386_GLOB_DAT @0xec111fe8 ((nil)) in /busybox
Error: Global Symbol re_syntax_options not found, cannot apply R_386_GLOB_DAT @0xec111fec ((nil)) in /busybox
Error: PltReolver: Symbol mallopt not found, cannot apply R_386_JMP_SLOT @0xec111d44 (0xec081fc6) in /busybox
Some functions are missing.
Ok, create a new ticket please. I think this one on execve can be closed now.
@ptitSeb 
 In wrappedlibc.c my_execv my_execve ...., why do you use if(self && !argv), If argv!=NULL, this will have no effect.
already anwsered. Don't post the same thing in multiple tickets.
also, this is not Discord or some messenging program. Don't expect instantaneous response from me, even with tagging (and as I already mentionnend, it's useless here, I already receive a mail for each new entry)
Sorry.