iemguis - undo replaces '$0' with current ID
> the second undo will undo the color change then

This is not something new. Before you'd also need to hit undo twice to get the color change undone. I think that's a bit annoying, and a separate issue.
@porres sure, the double undo is not something I wanted to address with this issue here. But since the first undo is causing the `$0` to get replaced, I wanted to mention this at least since it seemed to be connected for me (it looks like the - opposite? -  replacement of the local id ->`$0` has a place in the undo history somehow).
@porres : is this also happening on macOS? I'd also change the title and description here then as well!
> is this also happening on macOS?

yup
Is this a regression or did it happen with older versions of Pd as well?
just tested with Pd-0.52-2. It behaves different there (although not in an expected way either):
* undo sets the empty fields (in the test case above these are the receive name and label) to `{empty}`. the send name becomes `{$0-foo}`

so I wouldn't call it a regression - it actually solves some of the prior issues. Only the `$0` doesn't survive the undo now.

... the second undo then resets the color as expected.
Two data points on Ubuntu 22.04.1:

Testing on **0.53.0** / 4528eb99f12136bc7145f7ba0b16b729199515f5 self-compiled I **cannot reproduce** the issue. Put `[hsl]`, edit properties (no matter if range or send or color), undo them again -- everything works as expected.

Testing on **0.52.1** / the current `apt install`able version  I **can reproduce** the issue. Again, put `[hsl]`, edit properties (**no matter which of them**), undo them again -- now the slider has a label reading "{empty}", immediately visible on the patch after the undo, and the send and receive symbols are set to "{empty}" as well.
> just tested with Pd-0.52-2. It behaves different there (although not in an expected way either):

that was a known bug that was kinda fixed, but now we have this other issue, so it is not a regression as I see it