Fix typo when casting object to t_block*

Closes: https://github.com/pure-data/pure-data/issues/1874
ALSA-midi: Check for valid MIDI-handle before using it...

Closes: https://github.com/pure-data/pure-data/issues/1878
initialize midi_handle to NULL

so the NULL-checks from the previous commit don't test against uninitialized values...

Closes: https://github.com/pure-data/pure-data/issues/1878
ALSA-midi: Check for valid MIDI-handle before using it...

Closes: https://github.com/pure-data/pure-data/issues/1878
