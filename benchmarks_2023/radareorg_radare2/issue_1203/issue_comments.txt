symbols broken?
Can you add more description to this bug report.  This command helps
diagnose the issue, but by itself may not be useful for producing a
potential fix, even if the person was in IRC channel at the time ;)

On Thu, Aug 14, 2014 at 4:31 PM, condret notifications@github.com wrote:

> compare 'nm /lib/libr_anal.so | grep r_anal_esil_parse' , 'rabin -s
> /lib/libr_anal.so | grep r_anal_esil_parse' and 'rabin2 -s
> /lib/libr_anal.so | grep r_anal_esil_parse'
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/radare/radare2/issues/1203.

symbol adresses are broken for elf 64

We need a test case for this issue

The fix broke other cases. This is related to the baddr/laddr/aslr thing. I plan to rework it to make it happy again, but this will require some time.. I will work on this issue mainly now

> On 15 Aug 2014, at 01:12, condret notifications@github.com wrote:
> 
> symbol adresses are broken for elf 64
> 
> —
> Reply to this email directly or view it on GitHub.

@radare : thx for this patch, this is necessary for my talk about r2.

I think it should be fine now. But still no aslr implemented, but fixed many other places that was not handled properly. Also, i need some feedback to see if the rabin2 -v change im going to do is fine for everyone.

I fixed several tests but didnt had time to run the whole testsuite. It will be nice to make a deeper review on this and add more tests for the recently bugs reported (elf64 shared library symbol addresses)

> On 15 Aug 2014, at 09:51, condret notifications@github.com wrote:
> 
> @radare : thx for this patch, this is necessary for my talk about r2.
> 
> —
> Reply to this email directly or view it on GitHub.

hm, i tried to continue on preparing my talk and it's still broken (or maybe again broken)

Can you write a test case? Or point to the one that fails?

> On 31 Aug 2014, at 01:41, condret notifications@github.com wrote:
> 
> hm, i tried to continue on preparing my talk and it's still broken (or maybe again broken)
> 
> —
> Reply to this email directly or view it on GitHub.

it's again 'r_anal_esil_parse' but this time rabin2 is ok, it's only wrong if you do:

```
r2 /lib/libr_anal.so
> fs symbols
> f | grep r_anal_esil_parse
```

Then maybe i missed to use the same rva function for the internal set of flags. Its all done in the same function. You can verify yhe output with rabin2 and using -r or -q

We should add tests to verify where flags are set from rabin2, r2...

> On 31 Aug 2014, at 10:17, condret notifications@github.com wrote:
> 
> it's again 'r_anal_esil_parse' but this time rabin2 is ok, it's only wrong if you do:
> 
> r2 /lib/libr_anal.so
> 
> > fs symbols
> > f | grep r_anal_esil_parse
> > —
> > Reply to this email directly or view it on GitHub.

As a workaround for now you can use 'r2 -B0 libr_anal.so'. Shared libraries don't have a specific address, and by default rabin2 uses an rva transformation from the sections information. But as long as it's not mapped at a specific address. We should probably explicity use a base address for objects and shared libraries. But maybe it's exposing a missfunction in the rva() function

-B0 is equal to e bin.baddr = 0 ?

not exactly.. it’s also using va=2 and sets address to UT64_MAX because addr=0 -> va = 1 use default baddr, laddr=-1 -> va = 2 , use baddr=0 ...

On 01 Sep 2014, at 01:23, condret notifications@github.com wrote:

> -B0 is equal to e bin.baddr = 0 ?
> 
> —
> Reply to this email directly or view it on GitHub.

Can you verify?

@condret can you test this and close the issue if it's correct now?

yaps
