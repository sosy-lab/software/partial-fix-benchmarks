shm+ debug free(): invalid next size (fast): 0x08efa5a8 ***
so it's a regression in here:

```
==7134== Invalid write of size 1
==7134==    at 0x4DF3A53: r_str_argv (str.c:1159)
==7134==    by 0x47365AE: fork_and_ptraceme (io_debug.c:270)
```

Can you confirm?

this one is not fixed, valgrind:

http://sprunge.us/LHYM

just saw that the invalid free triggered a mem corruption also:
**\* glibc detected **\* r2: malloc(): memory corruption: 0x099e8338 ***

full log: http://sprunge.us/PYEI

without -d I get into shm_open 

```
r2  shm://10000  ./bins/a.out.bak
shared memory init
Cannot connect to shared memory (10000)
r_io_open_nomap: Unable to open file: shm://10000
```

Bonus note: it only happens on this particular file, not /bin/ls
r2 -v now: 285b20ce1ad124d9a7c32220572b73f5e76aaefe

Shm is used to connect to an already opened shared memory region. This output is expected

> On 21 Mar 2015, at 14:29, zonkzonk notifications@github.com wrote:
> 
> without -d I get into shm_open
> 
> r2  shm://10000  ./bins/a.out.bak
> shared memory init
> Cannot connect to shared memory (10000)
> r_io_open_nomap: Unable to open file: shm://10000
> —
> Reply to this email directly or view it on GitHub.

No crash  without -d . Also, there was no shm segment open:

```
ipcs -a

------ Shared Memory Segments --------
key        shmid      owner      perms      bytes      nattch     status      

------ Semaphore Arrays --------
key        semid      owner      perms      nsems     

------ Message Queues --------
key        msqid      owner      perms      used-bytes   messages    
```

so connecting to an non-existant shm segment fails with segfault when using -d.
(I reproduced connecting to existing shm segment with .d does not crash)

Another subtle difference is ./path vs path

you have to create an shm segment with another program in order to connect to it with r2. so its all fine now
