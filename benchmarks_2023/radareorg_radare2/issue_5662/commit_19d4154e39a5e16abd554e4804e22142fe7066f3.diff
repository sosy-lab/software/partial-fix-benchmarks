diff --git a/libr/anal/Makefile b/libr/anal/Makefile
index 5108f53179bce..64c40766ad534 100644
--- a/libr/anal/Makefile
+++ b/libr/anal/Makefile
@@ -31,8 +31,11 @@ OBJLIBS+=hint.o anal.o data.o xrefs.o esil.o sign.o
 OBJLIBS+=anal_ex.o switch.o state.o cycles.o
 OBJLIBS+=esil_stats.o esil_trace.o flirt.o labels.o
 OBJLIBS+=esil2reil.o pin.o
+ASMOBJS+=$(LTOP)/asm/arch/xtensa/gnu/xtensa-modules.o
+ASMOBJS+=$(LTOP)/asm/arch/xtensa/gnu/xtensa-isa.o
+ASMOBJS+=$(LTOP)/asm/arch/xtensa/gnu/elf32-xtensa.o
 
-OBJS=${STATIC_OBJS} ${OBJLIBS}
+OBJS=${STATIC_OBJS} ${OBJLIBS} ${ASMOBJS}
 
 test tests: tests-esil
 
diff --git a/libr/anal/anal.c b/libr/anal/anal.c
index 26e62b67ce2d7..06c0016a34bd8 100644
--- a/libr/anal/anal.c
+++ b/libr/anal/anal.c
@@ -511,3 +511,10 @@ R_API void r_anal_build_range_on_hints(RAnal *a) {
 		}
 	}
 }
+
+R_API void r_anal_bind(RAnal *anal, RAnalBind *b) {
+	if (b) {
+		b->anal = anal;
+		b->get_fcn_in = r_anal_get_fcn_in;
+	}
+}
diff --git a/libr/anal/p/anal_xtensa.c b/libr/anal/p/anal_xtensa.c
index 827b3372ef314..d281d4943b3f7 100644
--- a/libr/anal/p/anal_xtensa.c
+++ b/libr/anal/p/anal_xtensa.c
@@ -6,6 +6,12 @@
 #include <r_asm.h>
 #include <r_anal.h>
 
+#include <xtensa-isa.h>
+
+#define CM ","
+#define XTENSA_MAX_LENGTH 8
+
+extern xtensa_isa xtensa_default_isa;
 
 static int xtensa_length(const ut8 *insn) {
 	static int length_table[16] = { 3, 3, 3, 3, 3, 3, 3, 3, 2, 2, 2, 2, 2, 2, 8, 8 };
@@ -613,6 +619,1292 @@ static XtensaOpFn xtensa_op0_fns[] = {
 	xtensa_null_op  /*xtensa_xt_format2_op*/ /*TODO*/
 };
 
+static void esil_push_signed_imm(RStrBuf * esil, st32 imm) {
+	if (imm >= 0) {
+		r_strbuf_appendf (esil, "0x%x" CM, imm);
+	} else {
+		r_strbuf_appendf (
+			esil,
+			"0x%x"	CM
+			"0x0"	CM
+			"-"	CM,
+			- imm
+		);
+	}
+}
+
+static void esil_sign_extend(RStrBuf *esil, ut8 bit) {
+	// check sign bit, and, if needed, apply or mask
+
+	ut32 bit_mask = 1 << bit;
+	ut32 extend_mask = 0xFFFFFFFF << bit;
+
+	r_strbuf_appendf (
+		esil,
+		"DUP"	CM
+		"0x%x"	CM
+		"&"	CM
+		"0"	CM
+		"==,!"	CM
+		"?{"	CM
+			"0x%x"	CM
+			"|"	CM
+		"}"	CM,
+		bit_mask,
+		extend_mask
+	);
+}
+
+static inline void sign_extend(st32 *value, ut8 bit) {
+	if (* value & (1 << bit)) {
+		* value |= 0xFFFFFFFF << bit;
+	}
+}
+
+static void esil_load_imm(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 offset;
+	ut32 reg_d;
+	ut32 reg_a;
+	ut8 sign_extend_bit;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &reg_d);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &reg_a);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &offset);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	// example: l32i a2, a1, 0x10
+	//          0x10,a1,+, // address on stack
+	//          [x], // read data
+	//          a2, // push data reg
+	//			= // assign to data reg
+
+	ut8 data_size = opcode == 82 ? 2 // l16ui
+			: opcode == 83 ? 2 // l16si
+			: opcode == 84 ? 4 // l32i
+			: opcode == 31 ? 4 // l32i.n
+			: opcode == 86 ? 1 : 1; // l8ui
+
+	sign_extend_bit = 0;
+
+	switch (opcode) {
+	case 84: // l32i
+	case 31: // l32i.n
+		offset <<= 2;
+		break;
+	case 83: // l16si
+		sign_extend_bit = 15;
+		/* no break */
+	case 82: // l16ui
+		offset <<= 1;
+		break;
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0x%x"	CM
+			"%s%d"	CM
+			"+"	CM
+			"[%d]"	CM,
+		// offset
+		offset,
+		// address
+		xtensa_regfile_shortname (isa, src_rf),
+		reg_a,
+		// size
+		data_size
+	);
+
+	if (sign_extend_bit != 0) {
+		esil_sign_extend (&op->esil, sign_extend_bit);
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"=",
+		// data
+		xtensa_regfile_shortname (isa, dst_rf),
+		reg_d
+	);
+}
+
+static void esil_load_relative(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 offset;
+	st32 dst;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, (ut32 *) &dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &offset);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	// example: l32r a2, 0x10
+	//          0x10,pc,-, // address on stack
+	//			0xFFFFFFFC,& // clear 3 lsb
+	//          [4], // read data
+	//			a2, // push data reg
+	//			= // assign to data reg
+
+	offset = - ((offset | 0xFFFF0000) << 2);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0x%x" 		CM
+			"pc"   		CM
+			"-"    		CM
+			"0xFFFFFFFC"	CM
+			"&" 		CM
+			"[4]"		CM
+			"%s%d" 		CM
+			"=",
+		// offset
+		offset,
+		// data
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst
+	);
+}
+
+static void esil_add_imm(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	st32 imm;
+	ut32 dst;
+	ut32 src;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &src);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, (ut32 *) &imm);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	// example: addi a3, a4, 0x01
+	//          a4,0x01,+,a3,=
+
+	// wide form of addi requires sign extension
+	if (opcode == 39) {
+		sign_extend (&imm, 7);
+	}
+
+	r_strbuf_appendf (&op->esil, "%s%d" CM, xtensa_regfile_shortname (isa, src_rf), src);
+	esil_push_signed_imm (&op->esil, imm);
+	r_strbuf_appendf (
+		&op->esil,
+			"+"    CM
+			"%s%d" CM
+			"=",
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst
+	);
+}
+
+static void esil_store_imm(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+
+	ut32 offset;
+	ut32 reg_d;
+	ut32 reg_a;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &reg_d);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &reg_a);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &offset);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	// example: s32i a2, a1, 0x10
+	//          a2, // push data
+	//          0x10,a1,+, // address on stack
+	//          =[x] // write data
+
+	ut8 data_size =
+		opcode == 453 ? 4 // s32cli
+		: opcode == 36 ? 4 // s32i.n
+		: opcode == 100 ? 4 // s32i
+		: opcode == 99 ? 2 // s16i
+		: opcode == 101 ? 1 : 1; // s8i
+
+	switch (opcode) {
+	case 100: // s32i
+	case 453: // s32cli
+	case 36: // s32i.n
+		offset <<= 2;
+		break;
+	case 99: // s16i
+		offset <<= 1;
+		break;
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d" CM
+			"0x%x" CM
+			"%s%d" CM
+			"+"    CM
+			"=[%d]",
+		// data
+		xtensa_regfile_shortname (isa, dst_rf),
+		reg_d,
+		// offset
+		offset,
+		// address
+		xtensa_regfile_shortname (isa, src_rf),
+		reg_a,
+		// size
+		data_size
+	);
+}
+
+static void esil_move_imm(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	st32 imm;
+	ut32 reg;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, (ut32 *) &imm);
+
+	xtensa_regfile rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	// sign extension
+	// 90: movi
+	if (opcode == 90) {
+		sign_extend (&imm, 11);
+	}
+
+	// 33: movi.n
+	if (opcode == 33) {
+		sign_extend (&imm, 6);
+	}
+
+	esil_push_signed_imm (&op->esil, imm);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d" CM
+			"=",
+		xtensa_regfile_shortname (isa, rf),
+		reg
+	);
+}
+
+static void esil_move(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 dst;
+	ut32 src;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &src);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d" CM
+			"%s%d" CM
+			"=",
+		xtensa_regfile_shortname (isa, src_rf),
+		src,
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst
+	);
+}
+
+static void esil_move_conditional(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 dst;
+	ut32 src;
+	ut32 cond;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &src);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &cond);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+	xtensa_regfile cond_rf = xtensa_operand_regfile (isa, opcode, 2);
+
+	const char *compare_op = "";
+
+	switch (opcode) {
+	case 91:	/* moveqz */
+		compare_op = "==";
+		break;
+	case 92:	/* movnez */
+		compare_op = "==,!";
+		break;
+	case 93:	/* movltz */
+		compare_op = "<";
+		break;
+	case 94:	/* movgez */
+		compare_op = ">=";
+		break;
+	}
+
+	// example: moveqz a3, a4, a5
+	//          0,
+	//          a5,
+	//          ==,
+	//          ?{,
+	//            a4,
+	//            a3,
+	//            =,
+	//          }
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0"	CM
+			"%s%d"	CM
+			"%s"	CM
+			"?{"	CM
+				"%s%d"	CM
+				"%s%d"	CM
+				"="	CM
+			"}",
+		xtensa_regfile_shortname (isa, cond_rf),
+		cond,
+		compare_op,
+		xtensa_regfile_shortname (isa, src_rf),
+		src,
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst
+	);
+}
+
+static void esil_add_sub(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 dst;
+	ut32 op1;
+	ut32 op2;
+	bool is_add;
+	ut8 shift;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &op1);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &op2);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile op1_rf = xtensa_operand_regfile (isa, opcode, 1);
+	xtensa_regfile op2_rf = xtensa_operand_regfile (isa, opcode, 2);
+
+	is_add =
+		(opcode == 26) ||
+		(opcode == 41) ||
+		(opcode == 43) ||
+		(opcode == 44) ||
+		(opcode == 45);
+
+	switch (opcode) {
+	case 43:
+	case 46:
+		shift = 1;
+		break;
+	case 44:
+	case 47:
+		shift = 2;
+		break;
+	case 45:
+	case 48:
+		shift = 3;
+		break;
+	default:
+		shift = 0;
+		break;
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"%d"    CM
+			"%s%d"	CM
+			"<<"    CM
+			"%s"	CM
+			"%s%d"	CM
+			"=",
+		xtensa_regfile_shortname (isa, op2_rf),
+		op2,
+		shift,
+		xtensa_regfile_shortname (isa, op1_rf),
+		op1,
+		(is_add ? "+" : "-"),
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst
+	);
+}
+
+static void esil_branch_compare_imm(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 cmp_reg;
+	// Unsigned immediate operands still fit in st32
+	st32 cmp_imm;
+	st32 branch_imm;
+
+	const char *compare_op = "";
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &cmp_reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, (ut32 *) &cmp_imm);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, (ut32 *) &branch_imm);
+
+	xtensa_regfile cmp_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	// TODO: unsigned comparisons
+	switch (opcode) {
+	case 52:	/* beqi */
+		compare_op = "==";
+		break;
+	case 53:	/* bnei */
+		compare_op = "==,!";
+		break;
+	case 58:	/* bgeui */
+	case 54:	/* bgei */
+		compare_op = ">=";
+		break;
+	case 59:	/* bltui */
+	case 55:	/* blti */
+		compare_op = "<";
+		break;
+	}
+
+	// example: beqi a4, 4, offset
+	//            a4, // push data reg
+	//            0x4, // push imm operand
+	//            ==,
+	//            ?{,
+	//              offset,
+	//              pc,
+	//              +=,
+	//            }
+
+	r_strbuf_appendf (
+		&op->esil,
+		"%s%d" CM,
+		// data reg
+		xtensa_regfile_shortname (isa, cmp_rf),
+		cmp_reg
+	);
+
+	esil_push_signed_imm (&op->esil, cmp_imm);
+
+	r_strbuf_appendf (&op->esil, "%s" CM, compare_op);
+	r_strbuf_appendf (&op->esil, "?{" CM);
+
+	// ISA defines branch target as offset + 4,
+	// but at the time of ESIL evaluation
+	// PC will be already incremented by 3
+	esil_push_signed_imm (&op->esil, branch_imm + 4 - 3);
+
+	r_strbuf_appendf (&op->esil, "pc" CM "+=" CM "}");
+}
+
+static void esil_branch_compare(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 op1_reg;
+	ut32 op2_reg;
+	st32 branch_imm;
+
+	const char *compare_op = "";
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &op1_reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &op2_reg);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, (ut32 *) &branch_imm);
+
+	xtensa_regfile op1_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile op2_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	switch (opcode) {
+	case 60:	/* beq */
+		compare_op = "==";
+		break;
+	case 61:	/* bne */
+		compare_op = "==,!";
+		break;
+	case 62:	/* bge */
+	case 64:	/* bgeu */
+		compare_op = ">=";
+		break;
+	case 63:	/* blt */
+	case 65:	/* bltu */
+		compare_op = "<";
+		break;
+	}
+
+	sign_extend (&branch_imm, 7);
+	branch_imm += 4 - 3;
+
+	// example: beq a4, a3, offset
+	//            a3, // push op1
+	//            a4, // push op2
+	//            ==,
+	//            ?{,
+	//              offset,
+	//              pc,
+	//              +=,
+	//            }
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"%s%d"	CM
+			"%s"	CM
+			"?{"	CM,
+		xtensa_regfile_shortname (isa, op2_rf),
+		op2_reg,
+		xtensa_regfile_shortname (isa, op1_rf),
+		op1_reg,
+		compare_op
+	);
+
+	esil_push_signed_imm (&op->esil, branch_imm);
+
+	r_strbuf_append (&op->esil, "pc" CM "+=" CM "}");
+}
+
+static void esil_branch_compare_single(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 op_reg;
+	st32 branch_imm;
+
+	const char *compare_op = "";
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &op_reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, (ut32 *) &branch_imm);
+
+	xtensa_regfile op_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	switch (opcode) {
+	case 72:	/* beqz */
+	case 28:	/* beqz.n */
+		compare_op = "==";
+		break;
+	case 73:	/* bnez */
+	case 29:	/* bnez.n */
+		compare_op = "==,!";
+		break;
+	case 74:	/* bgez */
+		compare_op = ">=";
+		break;
+	case 75:	/* bltz */
+		compare_op = "<";
+		break;
+	}
+
+	sign_extend (&branch_imm, 12);
+	branch_imm += 4 - 3;
+
+	// example: beqz a4, 0, offset
+	//            0,  // push 0
+	//            a4, // push op
+	//            ==,
+	//            ?{,
+	//              offset,
+	//              pc,
+	//              +=,
+	//            }
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0"	CM
+			"%s%d"	CM
+			"%s"	CM
+			"?{"	CM,
+		xtensa_regfile_shortname (isa, op_rf),
+		op_reg,
+		compare_op
+	);
+
+	esil_push_signed_imm (&op->esil, branch_imm);
+
+	r_strbuf_append (&op->esil, "pc" CM "+=" CM "}");
+}
+
+static void esil_branch_check_mask(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 op1_reg;
+	ut32 op2_reg;
+	st32 branch_imm;
+
+	const char *compare_op = "";
+	char compare_val[4] = "0";
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &op1_reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &op2_reg);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, (ut32 *) &branch_imm);
+
+	xtensa_regfile op1_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile op2_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	switch (opcode) {
+	case 69:	/* bnall */
+	case 66:	/* bany */
+		compare_op = "==,!";
+		break;
+	case 68:	/* ball */
+	case 67:	/* bnone */
+		compare_op = "==";
+		break;
+	}
+
+	switch (opcode) {
+	case 69:	/* bnall */
+	case 68:	/* ball */
+		snprintf(
+			compare_val,
+			sizeof(compare_val),
+			"%s%d",
+			xtensa_regfile_shortname (isa, op2_rf),
+			op2_reg
+		);
+		break;
+	}
+
+	sign_extend (&branch_imm, 7);
+	branch_imm += 4 - 3;
+
+	// example: bnall a4, a3, offset
+	//            a4, // push op1
+	//            a3, // push op2
+	//            &,
+	//            a3,
+	//            ==,!,
+	//            ?{,
+	//              offset,
+	//              pc,
+	//              +=,
+	//            }
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"%s%d"	CM
+			"&"	CM
+			"%s%d"	CM
+			"%s"	CM
+			"?{"	CM,
+		xtensa_regfile_shortname (isa, op1_rf),
+		op1_reg,
+		xtensa_regfile_shortname (isa, op2_rf),
+		op2_reg,
+		xtensa_regfile_shortname (isa, op2_rf),
+		op2_reg,
+		compare_op
+	);
+
+	esil_push_signed_imm (&op->esil, branch_imm);
+
+	r_strbuf_append (&op->esil, "pc" CM "+=" CM "}");
+}
+
+static void esil_bitwise_op(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 dst;
+	ut32 op1;
+	ut32 op2;
+	char bop;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &op1);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &op2);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile op1_rf = xtensa_operand_regfile (isa, opcode, 1);
+	xtensa_regfile op2_rf = xtensa_operand_regfile (isa, opcode, 2);
+
+	switch (opcode) {
+	case 49:	/* and */
+		bop = '&';
+		break;
+	case 50:	/* or */
+		bop = '|';
+		break;
+	case 51:	/* xor */
+		bop = '^';
+		break;
+	default:
+		bop = '=';
+		break;
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"%s%d"	CM
+			"%c"    CM
+			"%s%d"	CM
+			"=",
+		xtensa_regfile_shortname (isa, op1_rf),
+		op1,
+		xtensa_regfile_shortname (isa, op2_rf),
+		op2,
+		bop,
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst
+	);
+}
+
+static void esil_branch_check_bit_imm(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 src_reg;
+	ut32 imm_bit;
+	st32 imm_offset;
+	ut8 bit_clear;
+	ut32 mask;
+	const char *cmp_op;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &src_reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &imm_bit);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, (ut32 *) &imm_offset);
+
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	bit_clear = opcode == 56;
+	cmp_op = bit_clear ? "==" : "==,!";
+	mask = 1 << imm_bit;
+
+	sign_extend (&imm_offset, 7);
+	imm_offset += 4 - 3;
+
+	// example: bbsi a4, 2, offset
+	//          a4,
+	//          mask,
+	//          &,
+	//          0,
+	//          ==,
+	//          ?{,
+	//            offset,
+	//            pc,
+	//            +=,
+	//          }
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"0x%x"	CM
+			"&"	CM
+			"0"	CM
+			"%s"	CM
+			"?{"	CM,
+		xtensa_regfile_shortname (isa, src_rf),
+		src_reg,
+		mask,
+		cmp_op
+	);
+
+	esil_push_signed_imm (&op->esil, imm_offset);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"pc"	CM
+			"+="	CM
+			"}"
+	);
+}
+
+static void esil_branch_check_bit(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 src_reg;
+	ut32 bit_reg;
+	st32 imm_offset;
+
+	ut8 bit_clear;
+	const char *cmp_op;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &src_reg);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &bit_reg);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, (ut32 *) &imm_offset);
+
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile bit_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	// bbc
+	bit_clear = opcode == 70;
+	cmp_op = bit_clear ? "==" : "==,!";
+
+	sign_extend (&imm_offset, 7);
+	imm_offset += 4 - 3;
+
+	// example: bbc a4, a2, offset
+	//          a2,
+	//          1,
+	//          <<,
+	//          a4,
+	//          &
+	//          0
+	//          ==,
+	//          ?{,
+	//            offset,
+	//            pc,
+	//            +=,
+	//          }
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"1"	CM
+			"<<"	CM
+			"%s%d"	CM
+			"&"	CM
+			"0"	CM
+			"%s"	CM
+			"?{"	CM,
+		xtensa_regfile_shortname (isa, bit_rf),
+		bit_reg,
+		xtensa_regfile_shortname (isa, src_rf),
+		src_reg,
+		cmp_op
+	);
+
+	esil_push_signed_imm (&op->esil, imm_offset);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"pc"	CM
+			"+="	CM
+			"}"
+	);
+}
+
+static void esil_abs_neg(xtensa_isa isa, xtensa_opcode opcode, xtensa_format format,
+		size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 src_reg;
+	ut32 dst_reg;
+
+	ut8 neg;
+
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &dst_reg);
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &src_reg);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 1);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	neg = opcode == 95;
+
+	if (!neg) {
+		r_strbuf_appendf (
+			&op->esil,
+				"0"	CM
+				"%s%d"	CM
+				"<"	CM
+				"?{"	CM
+				"0"     CM
+				"%s%d"	CM
+				"-"     CM
+				"}"	CM
+				"0"	CM
+				"%s%d"	CM
+				">="	CM
+				"?{"	CM
+				"%s%d"	CM
+				"}"	CM,
+			xtensa_regfile_shortname (isa, src_rf),
+			src_reg,
+			xtensa_regfile_shortname (isa, src_rf),
+			src_reg,
+			xtensa_regfile_shortname (isa, src_rf),
+			src_reg,
+			xtensa_regfile_shortname (isa, src_rf),
+			src_reg
+		);
+	} else {
+		r_strbuf_appendf (
+			&op->esil,
+				"0"	CM
+				"%s%d"	CM
+				"-"	CM,
+			xtensa_regfile_shortname (isa, src_rf),
+			src_reg
+		);
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"="	CM,
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst_reg
+	);
+}
+
+static void esil_call(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	bool call = opcode == 76;
+	st32 imm_offset;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer,
+			(ut32 *) &imm_offset);
+
+	if (call) {
+		r_strbuf_append(
+			&op->esil,
+			"pc"	CM
+			"a0"	CM
+			"="	CM
+		);
+	}
+
+	sign_extend (&imm_offset, 17);
+
+	if (call) {
+		imm_offset <<= 2;
+	}
+
+	imm_offset += 4 - 3;
+
+	esil_push_signed_imm (&op->esil, imm_offset);
+
+	r_strbuf_append (&op->esil, "pc" CM "+=");
+}
+
+static void esil_callx(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	bool callx = opcode == 77;
+	ut32 dst_reg;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &dst_reg);
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	if (callx) {
+		r_strbuf_append (
+			&op->esil,
+			"pc"	CM
+			"a0"	CM
+			"="	CM
+		);
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"pc"	CM
+			"=",
+		xtensa_regfile_shortname (isa, dst_rf),
+		dst_reg
+	);
+}
+
+static void esil_set_shift_amount(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 src_reg;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &src_reg);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 0);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"%s%d"	CM
+			"sar"	CM
+			"=",
+		xtensa_regfile_shortname (isa, src_rf),
+		src_reg
+	);
+}
+
+static void esil_set_shift_amount_imm(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 sa_imm;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &sa_imm);
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0x%x"	CM
+			"sar"	CM
+			"=",
+		sa_imm
+	);
+}
+
+static void esil_shift_logic_imm(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 reg_dst;
+	ut32 reg_src;
+	ut32 imm_amount;
+
+	const char *shift_op = "";
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &reg_dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &reg_src);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &imm_amount);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	// srli
+	if (opcode == 113) {
+		shift_op = ">>";
+	} else {
+		shift_op = "<<";
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0x%x"	CM
+			"%s%d"	CM
+			"%s"	CM
+			"%s%d"	CM
+			"=",
+		imm_amount,
+		xtensa_regfile_shortname (isa, src_rf),
+		reg_src,
+		shift_op,
+		xtensa_regfile_shortname (isa, dst_rf),
+		reg_dst
+	);
+}
+
+static void esil_shift_logic_sar(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 reg_dst;
+	ut32 reg_src;
+
+	const char *shift_op = "";
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &reg_dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &reg_src);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	// srl
+	if (opcode == 109) {
+		shift_op = ">>";
+	} else {
+		shift_op = "<<";
+	}
+
+	r_strbuf_appendf (
+		&op->esil,
+			"sar"	CM
+			"%s%d"	CM
+			"%s"	CM
+			"%s%d"	CM
+			"=",
+		xtensa_regfile_shortname (isa, src_rf),
+		reg_src,
+		shift_op,
+		xtensa_regfile_shortname (isa, dst_rf),
+		reg_dst
+	);
+}
+
+static void esil_extract_unsigned(xtensa_isa isa, xtensa_opcode opcode,
+		xtensa_format format, size_t i, xtensa_insnbuf slot_buffer, RAnalOp *op) {
+	ut32 reg_dst;
+	ut32 reg_src;
+	ut32 imm_shift;
+	ut32 imm_mask;
+
+	xtensa_operand_get_field (isa, opcode, 0, format, i, slot_buffer, &reg_dst);
+	xtensa_operand_get_field (isa, opcode, 1, format, i, slot_buffer, &reg_src);
+	xtensa_operand_get_field (isa, opcode, 2, format, i, slot_buffer, &imm_shift);
+	xtensa_operand_get_field (isa, opcode, 3, format, i, slot_buffer, &imm_mask);
+
+	xtensa_regfile dst_rf = xtensa_operand_regfile (isa, opcode, 0);
+	xtensa_regfile src_rf = xtensa_operand_regfile (isa, opcode, 1);
+
+	ut32 and_mask = (1 << (imm_mask + 1)) - 1;
+
+	r_strbuf_appendf (
+		&op->esil,
+			"0x%x"	CM
+			"%s%d"	CM
+			">>"	CM
+			"0x%x"	CM
+			"&"	CM
+			"%s%d"	CM
+			"=",
+		imm_shift,
+		xtensa_regfile_shortname (isa, src_rf),
+		reg_src,
+		and_mask,
+		xtensa_regfile_shortname (isa, dst_rf),
+		reg_dst
+	);
+}
+
+static void analop_esil (RAnal *a, RAnalOp *op, ut64 addr, ut8 *buffer, size_t len) {
+	if (!xtensa_default_isa) {
+		xtensa_default_isa = xtensa_isa_init (0, 0);
+	}
+
+	xtensa_opcode opcode;
+	xtensa_isa isa = xtensa_default_isa;
+	xtensa_format format;
+	ut32 nslots;
+	size_t i;
+
+	static xtensa_insnbuf insn_buffer = NULL;
+	static xtensa_insnbuf slot_buffer = NULL;
+
+	r_strbuf_init (&op->esil);
+	r_strbuf_set (&op->esil, "");
+
+	if (!insn_buffer) {
+		insn_buffer = xtensa_insnbuf_alloc (isa);
+		slot_buffer = xtensa_insnbuf_alloc (isa);
+	}
+
+	memset (insn_buffer, 0,	xtensa_insnbuf_size (isa) * sizeof(xtensa_insnbuf_word));
+
+	xtensa_insnbuf_from_chars (isa, insn_buffer, buffer, len);
+	format = xtensa_format_decode (isa, insn_buffer);
+
+	if (format == XTENSA_UNDEFINED) {
+		return;
+	}
+
+	nslots = xtensa_format_num_slots (isa, format);
+
+	for (i = 0; i < nslots; i++) {
+		xtensa_format_get_slot (isa, format, i, insn_buffer, slot_buffer);
+		opcode = xtensa_opcode_decode (isa, format, i, slot_buffer);
+
+		switch (opcode) {
+		case 26: /* add.n */
+		case 41: /* add */
+		case 43: /* addx2 */
+		case 44: /* addx4 */
+		case 45: /* addx8 */
+		case 42: /* sub */
+		case 46: /* subx2 */
+		case 47: /* subx4 */
+		case 48: /* subx8 */
+			esil_add_sub (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 32: /* mov.n */
+			esil_move (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 90: /* movi */
+		case 33: /* movi.n */
+			esil_move_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 0:  /* excw */
+		case 34: /* nop.n */
+			r_strbuf_setf (&op->esil, "");
+			break;
+		// TODO: s32cli (s32c1i) is conditional (CAS)
+		// should it be handled here?
+		case 453: /* s32c1i */
+		case 36:  /* s32i.n */
+		case 100: /* s32i */
+		case 99:  /* s16i */
+		case 101: /* s8i */
+			esil_store_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 27: /* addi.n */
+		case 39: /* addi */
+			esil_add_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 98: /* ret */
+		case 35: /* ret.n */
+			r_strbuf_setf (&op->esil, "a0,pc,=");
+			break;
+		case 82: /* l16ui */
+		case 83: /* l16si */
+		case 84: /* l32i */
+		case 31: /* l32i.n */
+		case 86: /* l8ui */
+			esil_load_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		// TODO: s32r
+		// l32r is different because it is relative to LITBASE
+		// which also may or may not be present
+		case 85: /* l32r */
+			esil_load_relative (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 40: /* addmi */
+			break;
+		case 49: /* and */
+		case 50: /* or */
+		case 51: /* xor */
+			esil_bitwise_op (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 52: /* beqi */
+		case 53: /* bnei */
+		case 54: /* bgei */
+		case 55: /* blti */
+		case 58: /* bgeui */
+		case 59: /* bltui */
+			esil_branch_compare_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 56: /* bbci */
+		case 57: /* bbsi */
+			esil_branch_check_bit_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 60: /* beq */
+		case 61: /* bne */
+		case 62: /* bge */
+		case 63: /* blt */
+		case 64: /* bgeu */
+		case 65: /* bltu */
+			esil_branch_compare (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 66: /* bany */
+		case 67: /* bnone */
+		case 68: /* ball */
+		case 69: /* bnall */
+			esil_branch_check_mask (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 70: /* bbc */
+		case 71: /* bbs */
+			esil_branch_check_bit (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 72: /* beqz */
+		case 73: /* bnez */
+		case 28: /* beqz.n */
+		case 29: /* bnez.n */
+		case 74: /* bgez */
+		case 75: /* bltz */
+			esil_branch_compare_single (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 78: /* extui */
+			esil_extract_unsigned (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 79: /* ill */
+			r_strbuf_setf (&op->esil, "");
+			break;
+		// TODO: windowed calls?
+		case 7: /* call4 */
+			break;
+		case 76: /* call0 */
+		case 80: /* j */
+			esil_call (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 81: /* jx */
+		case 77: /* callx0 */
+			esil_callx (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 91: /* moveqz */
+		case 92: /* movnez */
+		case 93: /* movltz */
+		case 94: /* movgez */
+			esil_move_conditional (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 96: /* abs */
+		case 95: /* neg */
+			esil_abs_neg (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 102: /* ssr */
+		case 103: /* ssl */
+			esil_set_shift_amount (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 111: /* slli */
+		case 113: /* srli */
+			esil_shift_logic_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 106: /* ssai */
+			esil_set_shift_amount_imm (isa, opcode, format, i, slot_buffer, op);
+			break;
+		case 107: /* sll */
+		case 109: /* srl */
+			esil_shift_logic_sar (isa, opcode, format, i, slot_buffer, op);
+			break;
+		}
+	}
+}
+
 static int xtensa_op (RAnal *anal, RAnalOp *op, ut64 addr, const ut8 *buf, int len) {
 	if (op == NULL)
 		return 1;
@@ -624,6 +1916,13 @@ static int xtensa_op (RAnal *anal, RAnalOp *op, ut64 addr, const ut8 *buf, int l
 		return 1;
 
 	xtensa_op0_fns[(buf[0] & 0xf)] (anal, op, addr, buf);
+
+	if (anal->decode) {
+		ut8 copy[XTENSA_MAX_LENGTH] = { 0 };
+		memcpy (copy, buf, R_MIN(op->size, XTENSA_MAX_LENGTH));
+		analop_esil (anal, op, addr, copy, op->size);
+	}
+
 	return op->size;
 }
 
@@ -647,22 +1946,25 @@ static char *get_reg_profile(RAnal *anal) {
 		"gpr	a0	.32	0	0\n"
 		"gpr	a1	.32	4	0\n"
 		"gpr	a2	.32	8	0\n"
-		"gpr	a3	.32	8	0\n"
-		"gpr	a4	.32	8	0\n"
-		"gpr	a5	.32	8	0\n"
-		"gpr	a6	.32	8	0\n"
-		"gpr	a7	.32	8	0\n"
-		"gpr	a8	.32	8	0\n"
-		"gpr	a9	.32	8	0\n"
-		"gpr	a10	.32	8	0\n"
-		"gpr	a11	.32	8	0\n"
-		"gpr	a12	.32	8	0\n"
-		"gpr	a13	.32	8	0\n"
-		"gpr	a14	.32	8	0\n"
-		"gpr	a15	.32	8	0\n"
+		"gpr	a3	.32	16	0\n"
+		"gpr	a4	.32	20	0\n"
+		"gpr	a5	.32	24	0\n"
+		"gpr	a6	.32	28	0\n"
+		"gpr	a7	.32	32	0\n"
+		"gpr	a8	.32	36	0\n"
+		"gpr	a9	.32	40	0\n"
+		"gpr	a10	.32	44	0\n"
+		"gpr	a11	.32	48	0\n"
+		"gpr	a12	.32	52	0\n"
+		"gpr	a13	.32	56	0\n"
+		"gpr	a14	.32	60	0\n"
+		"gpr	a15	.32	64	0\n"
 
 		// pc
-		"gpr	pc	.32	8	0\n"
+		"gpr	pc	.32	68	0\n"
+
+		// sr
+		"gpr	sar	.32	72	0\n"
 	);
 }
 
diff --git a/libr/asm/Makefile b/libr/asm/Makefile
index 6c83fa060197a..eb1713ae83eda 100644
--- a/libr/asm/Makefile
+++ b/libr/asm/Makefile
@@ -2,7 +2,7 @@ include ../../global.mk
 
 NAME=r_asm
 DEPS=r_syscall r_lang r_util r_parse
-DEPS+=r_flags r_cons r_reg r_anal
+DEPS+=r_flags r_cons r_reg
 CFLAGS+=-DCORELIB -Iarch/include -Iarch -I../../shlr
 CURDIR=p/
 
diff --git a/libr/core/core.c b/libr/core/core.c
index 1c3667e439453..724adb9920e83 100644
--- a/libr/core/core.c
+++ b/libr/core/core.c
@@ -1398,6 +1398,7 @@ R_API int r_core_init(RCore *core) {
 	r_io_bind (core->io, &(core->fs->iob));
 	r_io_bind (core->io, &(core->bin->iob));
 	r_flag_bind (core->flags, &(core->anal->flb));
+	r_anal_bind (core->anal, &(core->parser->analb));
 
 	core->file = NULL;
 	core->files = r_list_new ();
diff --git a/libr/include/r_anal.h b/libr/include/r_anal.h
index b78bfd207c9e4..305d608ac7671 100644
--- a/libr/include/r_anal.h
+++ b/libr/include/r_anal.h
@@ -622,6 +622,13 @@ typedef struct r_anal_t {
 	RList /*RAnalRange*/ *bits_ranges;
 } RAnal;
 
+typedef RAnalFunction *(* RAnalGetFcnIn)(RAnal *anal, ut64 addr, int type);
+
+typedef struct r_anal_bind_t {
+	RAnal *anal;
+	RAnalGetFcnIn get_fcn_in;
+} RAnalBind;
+
 typedef struct r_anal_hint_t {
 	ut64 addr;
 	ut64 ptr;
@@ -1149,6 +1156,7 @@ R_API int r_anal_set_big_endian(RAnal *anal, int boolean);
 R_API char *r_anal_strmask (RAnal *anal, const char *data);
 R_API void r_anal_trace_bb(RAnal *anal, ut64 addr);
 R_API const char *r_anal_fcn_type_tostring(int type);
+R_API void r_anal_bind(RAnal *b, RAnalBind *bnd);
 
 /* fcnsign */
 R_API int r_anal_set_triplet(RAnal *anal, const char *os, const char *arch, int bits);
diff --git a/libr/include/r_parse.h b/libr/include/r_parse.h
index 3626e5584bd27..162d772a0ae24 100644
--- a/libr/include/r_parse.h
+++ b/libr/include/r_parse.h
@@ -29,6 +29,7 @@ typedef struct r_parse_t {
 	RAnalHint *hint; // weak anal ref
 	RList *parsers;
 	RAnalVarList varlist;
+	RAnalBind analb;
 } RParse;
 
 typedef struct r_parse_plugin_t {
diff --git a/libr/parse/Makefile b/libr/parse/Makefile
index 3474e5c597680..6adc786c3bf66 100644
--- a/libr/parse/Makefile
+++ b/libr/parse/Makefile
@@ -1,7 +1,7 @@
 include ../config.mk
 
 NAME=r_parse
-DEPS=r_flags r_util r_anal r_syscall r_reg
+DEPS=r_flags r_util r_syscall r_reg
 # indirect dependencies
 DEPS+=r_cons
 
diff --git a/libr/parse/parse.c b/libr/parse/parse.c
index 82b49b96314cb..7e43923a92c5c 100644
--- a/libr/parse/parse.c
+++ b/libr/parse/parse.c
@@ -168,7 +168,7 @@ static int filter(RParse *p, RFlag *f, char *data, char *str, int len, bool big_
 		}
 		off = r_num_math (NULL, ptr);
 		if (off > 0xff) {
-			fcn = r_anal_get_fcn_in (p->anal, off, 0);
+			fcn = p->analb.get_fcn_in (p->anal, off, 0);
 			if (fcn && fcn->addr == off) {
 				*ptr = 0;
 				// hack to realign pointer for colours
