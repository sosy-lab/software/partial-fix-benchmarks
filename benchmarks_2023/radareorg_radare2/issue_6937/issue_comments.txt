Segfault on graph display for MIPS
Typo: s/arm/MIPS/g
Hello,

Ensure you are using radare2 from git, if you're unsure paste output of `r2 -v` here.
To install radare2 from git, first uninstall your version of radare2 and clean your distro. Then use `git clone https://github.com/radare/radare2 && cd radare2 && ./sys/install.sh`, verify your version and check if there is no error using `r2 -v`.
It is from git, rebuilt just before doing that, and the version info is present in the bug report.
valgrind says this:

==65814== Conditional jump or move depends on uninitialised value(s)
==65814==    at 0x100468575: analop_esil (anal_mips_cs.c:153)
==65814==    by 0x100467918: analop (anal_mips_cs.c:839)
==65814==    by 0x1004B0AC0: r_anal_op (op.c:103)
==65814==    by 0x1000D7475: _anal_calls (cmd_anal.c:3549)
==65814==    by 0x1000D60B8: cmd_anal_calls (cmd_anal.c:3633)
==65814==    by 0x1000CDD9A: cmd_anal_all (cmd_anal.c:5084)
==65814==    by 0x100097AF1: cmd_anal (cmd_anal.c:5477)
==65814==    by 0x100145926: r_cmd_call (cmd_api.c:226)
==65814==    by 0x1000C3CDF: r_core_cmd_subst_i (cmd.c:2066)
==65814==    by 0x100095164: r_core_cmd_subst (cmd.c:1341)
==65814==    by 0x100092BD6: r_core_cmd (cmd.c:2622)
==65814==    by 0x10011355F: r_core_visual_prompt (visual.c:336)
==65814==
==65814==

ive fixed the issue, so pls try again to confirm it was this the problem

> On 07 Mar 2017, at 19:24, Cédric Picard <notifications@github.com> wrote:
> 
> Summary
> 
> Radare2 segfaults when asked to display a graph on some arm functions.
> 
> Test case
> 
> Prerequisite:
> 
> This file. <https://shareit.devys.org/download/2dc15fd8ee6a91b1a9c29a19f8f9dc8a>
> $ file diaglog-send
> fs_1/usr/bin/diaglog-send: setuid ELF 32-bit MSB executable, MIPS, MIPS32 version 1 (SYSV), dynamically linked, interpreter /lib/ld-uClibc.so.0, corrupted section header size
> Triggering the bug:
> 
> $ r2 -c "aaaa ; s fcn.00400920 ; VV" diaglog-send
> Warning: Cannot initialize section headers
> Warning: Cannot initialize strings table
> Warning: Cannot initialize dynamic strings
> [x] Analyze all flags starting with sym. and entry0 (aa)
> [x] Analyze len bytes of instructions for references (aar)
> [x] Analyze function calls (aac)
> [x] Emulate code to find computed references (aae)
> [x] Analyze consecutive function (aat)
> [aav: using from to 0x400000 0x401294
> Using vmin 0x400000 and vmax 0x411294
> aav: Cannot find section at 0x4198885
> [x] Analyze value pointers (aav)
> [Deinitialized mem.0x100000_0xf0000 functions (afta)unc.* functions (aan)
> [x] Type matching analysis for all functions (afta)
> [x] Type matching analysis for all functions (afta)
> zsh: segmentation fault (core dumped)  r2 -c "aaaa ; s fcn.00400920 ; VV" ./fs_1/usr/bin/diaglog-send
> 
> $ coredumpctl dump
>            PID: 22478 (r2)
>            UID: 1000 (cym13)
>            GID: 1000 (cym13)
>         Signal: 11 (SEGV)
>      Timestamp: Tue 2017-03-07 19:20:00 CET (24s ago)
>   Command Line: r2 -c aaaa ; s fcn.00400920 ; VV diaglog-send
>     Executable: /home/cym13/usr/git/radare2/binr/radare2/radare2
>  Control Group: /user.slice/user-1000.slice/session-c1.scope
>           Unit: session-c1.scope
>          Slice: user-1000.slice
>        Session: c1
>      Owner UID: 1000 (cym13)
>        Boot ID: c9bbcc8359904874979d74f0353a5930
>     Machine ID: 92ea047145e94fae99f1cb167c826fe9
>       Hostname: Esme
>        Storage: /var/lib/systemd/coredump/core.r2.1000.c9bbcc8359904874979d74f0353a5930.22478.1488910800000000000000.lz4
>        Message: Process 22478 (r2) of user 1000 dumped core.
> 
>                 Stack trace of thread 22478:
>                 #0  0x00007f80c1cfd4b4 strlen (libc.so.6)
>                 #1  0x00007f80c1cc5e23 vfprintf (libc.so.6)
>                 #2  0x00007f80c1ceced9 vsnprintf (libc.so.6)
>                 #3  0x00007f80c5acd621 r_cons_printf_list (libr_cons.so)
>                 #4  0x00007f80c6245830 _ds_comment (libr_core.so)
>                 #5  0x00007f80c62458e5 ds_comment (libr_core.so)
>                 #6  0x00007f80c624a4f9 ds_show_comments_right (libr_core.so)
>                 #7  0x00007f80c6251fbb r_core_print_disasm (libr_core.so)
>                 #8  0x00007f80c61d861c cmd_print (libr_core.so)
>                 #9  0x00007f80c622b3b9 r_cmd_call (libr_core.so)
>                 #10 0x00007f80c61f1eba r_core_cmd_subst_i (libr_core.so)
>                 #11 0x00007f80c61efa90 r_core_cmd_subst (libr_core.so)
>                 #12 0x00007f80c61f3e5f r_core_cmd (libr_core.so)
>                 #13 0x00007f80c61f4705 r_core_cmd_str (libr_core.so)
>                 #14 0x00007f80c61f46c0 r_core_cmd_strf (libr_core.so)
>                 #15 0x00007f80c6213c67 get_body (libr_core.so)
>                 #16 0x00007f80c6213e1b get_bb_body (libr_core.so)
>                 #17 0x00007f80c62143f3 get_bbnodes (libr_core.so)
>                 #18 0x00007f80c6214a7b reload_nodes (libr_core.so)
>                 #19 0x00007f80c6215e29 agraph_reload_nodes (libr_core.so)
>                 #20 0x00007f80c621624e check_changes (libr_core.so)
>                 #21 0x00007f80c6216520 agraph_print (libr_core.so)
>                 #22 0x00007f80c6216908 agraph_refresh (libr_core.so)
>                 #23 0x00007f80c6218249 r_core_visual_graph (libr_core.so)
>                 #24 0x00007f80c6206dab r_core_visual_cmd (libr_core.so)
>                 #25 0x00007f80c6209fad r_core_visual (libr_core.so)
>                 #26 0x00007f80c61ee945 cmd_visual (libr_core.so)
>                 #27 0x00007f80c622b3b9 r_cmd_call (libr_core.so)
>                 #28 0x00007f80c61f20d0 r_core_cmd_subst_i (libr_core.so)
>                 #29 0x00007f80c61efa90 r_core_cmd_subst (libr_core.so)
>                 #30 0x00007f80c61efb8a r_core_cmd_subst (libr_core.so)
>                 #31 0x00007f80c61efb8a r_core_cmd_subst (libr_core.so)
>                 #32 0x00007f80c61f3e5f r_core_cmd (libr_core.so)
>                 #33 0x00007f80c61f44ae r_core_cmd0 (libr_core.so)
>                 #34 0x00005575514e87b7 run_commands (radare2)
>                 #35 0x00005575514eab44 main (radare2)
>                 #36 0x00007f80c1c9d291 __libc_start_main (libc.so.6)
>                 #37 0x00005575514e7cba _start (radare2)
> Refusing to dump core to tty (use shell redirection or specify --output).
> The actual core dump can be provided on demand.
> 
> What was expected
> 
> A crashless but colorful graph display.
> 
> Version information
> 
> $ radare2 -version
> radare2 1.3.0-git 10739 @ linux-x86-64 git.0.10.1-255-g616a736
> commit: 616a73610ce7ff47d2da13b416d4334845111b92 build: 2016-03-26
> 
> $ uname -a
> Linux XXX 4.9.11-1-ARCH #1 SMP PREEMPT Sun Feb 19 13:45:52 UTC 2017 x86_64 GNU/Linux
> —
> You are receiving this because you are subscribed to this thread.
> Reply to this email directly, view it on GitHub <https://github.com/radare/radare2/issues/6937>, or mute the thread <https://github.com/notifications/unsubscribe-auth/AA3-lra7cfJxM_loinG8ncZ6mUUNickiks5rjaDFgaJpZM4MV2T8>.
> 


Ok, I do have a version problem even though rebuilding works fine... Fixing that and rechecking.
Yes see:
```
 build: 2016-03-26
```

Currently build is displaying time hours/min so you have an issue ;)
No, no change with:

    radare2 1.3.0-git 13985 @ linux-x86-64 git.1.2.0-319-g21e051faf
    commit: 21e051fafe1f94fd457404cc50d9fbc00f78c42b build: 2017-03-07__20:45:56
Can u show us a backtrace or something to reproduce this issue? Can you reproducd the problem in another x86 or ARM system?

> On 7 Mar 2017, at 20:48, Cédric Picard <notifications@github.com> wrote:
> 
> No, no change with:
> 
> radare2 1.3.0-git 13985 @ linux-x86-64 git.1.2.0-319-g21e051faf
> commit: 21e051fafe1f94fd457404cc50d9fbc00f78c42b build: 2017-03-07__20:45:56
> —
> You are receiving this because you commented.
> Reply to this email directly, view it on GitHub, or mute the thread.
> 

I believe I gave everything I could in the first post, nothing changed from then. As far as I can tell it doesn't happen on x86, can't really test on ARM right now. But as it doesn't happen on every functions even on MIPS I may just be unfortunately lucky.
Can u show me the register state at the moment of the crash? I have an idea.. do you know which function is the one that crashes? Also. If you can paste the disassembly it would be great too

Thanks. Ill try to blind fix with this info

> On 8 Mar 2017, at 18:52, Cédric Picard <notifications@github.com> wrote:
> 
> I believe I gave everything I could in the first post, nothing changed from then. As far as I can tell it doesn't happen on x86, can't really test on ARM right now. But as it doesn't happen on every functions even on MIPS I may just be unfortunately lucky.
> 
> —
> You are receiving this because you commented.
> Reply to this email directly, view it on GitHub, or mute the thread.
> 

Ok.

I rebuilt r2 today so here is the new version for reference:

    radare2 1.3.0-git 13991 @ linux-x86-64 git.1.2.0-325-gbb20c4592
    commit: bb20c4592a7a21c9e8c9366f448b9a0a63de7bf5 build: 2017-03-08__20:23:23

Here his the disassembly of the function that causes the crash:

    ┌ (fcn) fcn.00400920 176
    │   fcn.00400920 ();
    │     0x00400920      3c1c0042       lui gp, 0x42
    │     0x00400924      27bdfed8       addiu sp, sp, -0x128
    │     0x00400928      279c9210       addiu gp, gp, -0x6df0
    │     0x0040092c      afbf0124       sw ra, 0x124(sp)
    │     0x00400930      afb30120       sw s3, 0x120(sp)
    │     0x00400934      afb2011c       sw s2, 0x11c(sp)
    │     0x00400938      afb10118       sw s1, 0x118(sp)
    │     0x0040093c      afb00114       sw s0, 0x114(sp)
    │     0x00400940      afbc0020       sw gp, 0x20(sp)
    │     0x00400944      a0a00000       sb zero, (a1)
    │     0x00400948      24020020       addiu v0, zero, 0x20
    │     0x0040094c      8f99806c       lw t9, -0x7f94(gp)
    │     0x00400950      00a08021       move s0, a1
    │     0x00400954      27b30048       addiu s3, sp, 0x48
    │     0x00400958      8c850010       lw a1, 0x10(a0)
    │     0x0040095c      27b10028       addiu s1, sp, 0x28
    │     0x00400960      8c840014       lw a0, 0x14(a0)
    │     0x00400964      afa20014       sw v0, 0x14(sp)
    │     0x00400968      24020003       addiu v0, zero, 3
    │     0x0040096c      afa20018       sw v0, 0x18(sp)
    │     0x00400970      00c09021       move s2, a2
    │     0x00400974      afb10010       sw s1, 0x10(sp)
    │     0x00400978      02603021       move a2, s3
    │     0x0040097c      0320f809       jalr t9
    │     0x00400980      240700c4       addiu a3, zero, 0xc4
    │ ┌─< 0x00400984      1440000a       bnez v0, 0x4009b0
    │ │   0x00400988      8fbc0020       lw gp, 0x20(sp)
    │ │   0x0040098c      8f99807c       lw t9, -0x7f84(gp)
    │ │   0x00400990      3c060040       lui a2, 0x40
    │ │   0x00400994      afb10010       sw s1, 0x10(sp)
    │ │   0x00400998      02402821       move a1, s2
    │ │   0x0040099c      24c610e0       addiu a2, a2, 0x10e0
    │ │   0x004009a0      02603821       move a3, s3
    │ │   0x004009a4      0320f809       jalr t9
    │ │   0x004009a8      02002021       move a0, s0
    │ │   0x004009ac      8fbc0020       lw gp, 0x20(sp)
    │ └─> 0x004009b0      8fbf0124       lw ra, 0x124(sp)
    │     0x004009b4      02001021       move v0, s0
    │     0x004009b8      8fb30120       lw s3, 0x120(sp)
    │     0x004009bc      8fb2011c       lw s2, 0x11c(sp)
    │     0x004009c0      8fb10118       lw s1, 0x118(sp)
    │     0x004009c4      8fb00114       lw s0, 0x114(sp)
    │     0x004009c8      03e00008       jr ra
    └     0x004009cc      27bd0128       addiu sp, sp, 0x128

Here are the registers at the time of crash (thanks gdb):

    rax            0x7fffffc0       2147483584
    rbx            0x7fffffff8b10   140737488325392
    rcx            0xfee    4078
    rdx            0x0      0
    rsi            0x7fffffff8690   140737488324240
    rdi            0x7fffffee       2147483630
    rbp            0x7fffffff8b00   0x7fffffff8b00
    rsp            0x7fffffff8598   0x7fffffff8598
    r8             0x7fffffee       2147483630
    r9             0xc      12
    r10            0x73     115
    r11            0x7fffffff8ac8   140737488325320
    r12            0x555555a83a80   93824997669504
    r13            0x7fffffff8d18   140737488325912
    r14            0x0      0
    r15            0xffffffffffffffff       -1
    rip            0x7ffff3c324b4   0x7ffff3c324b4 <strlen+148>
    eflags         0x10206  [ PF IF RF ]
    cs             0x33     51
    ss             0x2b     43
    ds             0x0      0
    es             0x0      0
    fs             0x0      0
    gs             0x0      0

GDB stacktrace:

    #0  0x00007ffff3c324b4 in strlen () from /usr/lib/libc.so.6
    #1  0x00007ffff3bfae23 in vfprintf () from /usr/lib/libc.so.6
    #2  0x00007ffff3c21ed9 in vsnprintf () from /usr/lib/libc.so.6
    #3  0x00007ffff73ed621 in r_cons_printf_list (format=0x555555a83a80 "; esilref: '%s:%s'", ap=0x7fffffff8d18)
        at cons.c:793
    #4  0x00007ffff7b66151 in _ds_comment (ds=0x555555927c20, nl=false, align=false,
        format=0x555555a83a80 "; esilref: '%s:%s'", ap=0x7fffffff8d18) at disasm.c:346
    #5  0x00007ffff7b66206 in ds_comment (ds=0x555555927c20, align=false,
        format=0x555555a83a80 "; esilref: '%s:%s'") at disasm.c:352
    #6  0x00007ffff7b6ae1a in ds_show_comments_right (ds=0x555555927c20) at disasm.c:1413
    #7  0x00007ffff7b728dc in r_core_print_disasm (p=0x5555557c3240, core=0x55555575c4e0 <r>, addr=4196748,
        buf=0x555555aa3450 "\217\231\200|<\006", len=36, l=36, invbreak=0, cbytes=1) at disasm.c:3523
    #8  0x00007ffff7af8711 in cmd_print (data=0x55555575c4e0 <r>, input=0x555555a64db1 "D 36") at cmd_print.c:3558
    #9  0x00007ffff7b4b69e in r_cmd_call (cmd=0x555555840270, input=0x555555a64db0 "pD 36") at cmd_api.c:226
    #10 0x00007ffff7b11f9d in r_core_cmd_subst_i (core=0x55555575c4e0 <r>, cmd=0x555555a64db0 "pD 36", colon=0x0)
        at cmd.c:2023
    #11 0x00007ffff7b0fb73 in r_core_cmd_subst (core=0x55555575c4e0 <r>, cmd=0x555555a64db0 "pD 36") at cmd.c:1341
    #12 0x00007ffff7b13f42 in r_core_cmd (core=0x55555575c4e0 <r>, cstr=0x7fffffffa380 "pD 36 @ 0x0040098c", log=0)
        at cmd.c:2622
    #13 0x00007ffff7b147e8 in r_core_cmd_str (core=0x55555575c4e0 <r>, cmd=0x7fffffffa380 "pD 36 @ 0x0040098c")
        at cmd.c:2818
    #14 0x00007ffff7b147a3 in r_core_cmd_strf (core=0x55555575c4e0 <r>, fmt=0x7ffff7baddd7 "%s %d @ 0x%08llx")
        at cmd.c:2808
    #15 0x00007ffff7b33f4c in get_body (core=0x55555575c4e0 <r>, addr=4196748, size=36, opts=0) at graph.c:1765
    #16 0x00007ffff7b34100 in get_bb_body (core=0x55555575c4e0 <r>, b=0x5555557dfd90, opts=0, fcn=0x5555557d8f60,
        emu=false, saved_gp=0, saved_arena=0x0) at graph.c:1797
    #17 0x00007ffff7b346d8 in get_bbnodes (g=0x555555abd5c0, core=0x55555575c4e0 <r>, fcn=0x5555557d8f60)
        at graph.c:1908
    #18 0x00007ffff7b34d60 in reload_nodes (g=0x555555abd5c0, core=0x55555575c4e0 <r>, fcn=0x5555557d8f60)
        at graph.c:2046
    #19 0x00007ffff7b3610e in agraph_reload_nodes (g=0x555555abd5c0, core=0x55555575c4e0 <r>, fcn=0x5555557d8f60)
        at graph.c:2455
    #20 0x00007ffff7b36533 in check_changes (g=0x555555abd5c0, is_interactive=1, core=0x55555575c4e0 <r>,
        fcn=0x5555557d8f60) at graph.c:2548
    #21 0x00007ffff7b36805 in agraph_print (g=0x555555abd5c0, is_interactive=1, core=0x55555575c4e0 <r>,
        fcn=0x5555557d8f60) at graph.c:2600
    #22 0x00007ffff7b36bed in agraph_refresh (grd=0x555555a5e1f0) at graph.c:2684
    #23 0x00007ffff7b3852e in r_core_visual_graph (core=0x55555575c4e0 <r>, g=0x555555abd5c0, _fcn=0x0,
        is_interactive=1) at graph.c:3136
    #24 0x00007ffff7b27090 in r_core_visual_cmd (core=0x55555575c4e0 <r>, arg=0x555555ab6891 "V") at visual.c:1820
    #25 0x00007ffff7b2a292 in r_core_visual (core=0x55555575c4e0 <r>, input=0x555555ab6891 "V") at visual.c:2655
    #26 0x00007ffff7b0ea28 in cmd_visual (data=0x55555575c4e0 <r>, input=0x555555ab6891 "V") at cmd.c:972
    #27 0x00007ffff7b4b69e in r_cmd_call (cmd=0x555555840270, input=0x555555ab6890 "VV") at cmd_api.c:226
    #28 0x00007ffff7b121b3 in r_core_cmd_subst_i (core=0x55555575c4e0 <r>, cmd=0x555555ab6890 "VV", colon=0x0)
        at cmd.c:2066
    #29 0x00007ffff7b0fb73 in r_core_cmd_subst (core=0x55555575c4e0 <r>, cmd=0x555555ab6890 "VV") at cmd.c:1341
    #30 0x00007ffff7b0fc6d in r_core_cmd_subst (core=0x55555575c4e0 <r>, cmd=0x555555891b10 "s fcn.00400920")
        at cmd.c:1363
    #31 0x00007ffff7b0fc6d in r_core_cmd_subst (core=0x55555575c4e0 <r>, cmd=0x5555557be040 "aaaa") at cmd.c:1363
    #32 0x00007ffff7b13f42 in r_core_cmd (core=0x55555575c4e0 <r>,
        cstr=0x7fffffffe3ca "aaaa ; s fcn.00400920 ; VV", log=0) at cmd.c:2622
    #33 0x00007ffff7b14591 in r_core_cmd0 (user=0x55555575c4e0 <r>, cmd=0x7fffffffe3ca "aaaa ; s fcn.00400920 ; VV")
        at cmd.c:2768
    #34 0x0000555555557851 in run_commands (cmds=0x5555557be010, files=0x5555557be070, quiet=false) at radare2.c:349
    #35 0x0000555555559c18 in main (argc=4, argv=0x7fffffffe088, envp=0x7fffffffe0b0) at radare2.c:1129

Don't hesitate if you need anything

Oh, I forgot to say as it may not be obvious but the segfaulting address is 0x7fffffc0
What? Wasnt the crash happening on mips and was not reproducible on x86? Im busy irl now but ill fix that later. Thanks for being that responsive

> On 8 Mar 2017, at 20:34, Cédric Picard <notifications@github.com> wrote:
> 
> Oh, I forgot to say as it may not be obvious but the segfaulting address is 0x7fffffc0
> 
> —
> You are receiving this because you commented.
> Reply to this email directly, view it on GitHub, or mute the thread.
> 

There may be a confusion there:

I'm on x86_64 analyzing a MIPS executable (provided in the first post). I did not try to run radare on another architecture, neither could I reproduce the bug against an x86 executable.
Nope, still nothing changed with radare2 1.3.0-git 14012 @ linux-x86-64 git.1.2.0-345-g9c870ef16, commit: 9c870ef165adb13d7f9add7f112a5962ad15098a build: 2017-03-09__19:14:07.

Same segfault at the same address with the same backtrace.
Wtf i was reproducing the issue in two different machines and after the fix valgrind was no longer complaining. Busy irl again will rechk again later
I neither can reproduce. Did you have any configuration in .radarer2c or something that could affect?
One thing you can try is building using asan -> ./sys/asan.sh and try to reproduce your issue then paste the asan logs.
I wiped out my config to make sure, but it is still segfaulting for me.
I'll try the asan thing.
I confirm the fix, thank you very much.