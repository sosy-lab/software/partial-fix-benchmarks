Fix compatibility with OpenSSL 1.1.0. (#9233)
WATCH command can delete expired keys

After a discussion in #9068 and #9194, we reached an agreement
that we should handle all scenarios about expire, not only the real
expired deletion but also the logic expired time.

This PR aims to fix the issue below:
Time 1: we have a key "foo" is expired but not deleted.
Time 2: we WATCH the key "foo".
Time 3: execute EXEC and we find "foo" is expired and discard the
transaction, but it's not right, cause key "foo" is expired before
WATCH.

To fix it, the WATCH command now calls expireIfNeeded() to delete
the expired keys, and considering stale data would not be deleted if
clients are paused, WATCH command is now with may-replicate flag.

Notes: expireIfNeeded() cannot work in replicas, but WATCH expired keys
in replicas is rare case, we can fix it in future.
WATCH handle logical expired (stale) keys

After a discussion in #9068 and #9194, we reached an agreement
that we should handle all scenarios about expire, not only the real
expired deletion but also the logical expired time.

This PR aims to fix the issue below:
Time 1: we have a key "foo" is expired but not deleted.
Time 2: we WATCH the key "foo".
Time 3: execute EXEC and we find "foo" is expired and discard the
transaction, but it's not right, cause key "foo" is expired before
WATCH.

To adddress the issue, the WATCH command now calls expireIfNeeded()
try to delete the expired keys.

But there are two scenarios that expireIfNeeded() cannot work:
clients are paused and role is replica. To handle the stale key,
we add a flag to record if the watchedKey is stale, and don't
flag client as CLIENT_DIRTY_CAS if key is stale when touch the key.
