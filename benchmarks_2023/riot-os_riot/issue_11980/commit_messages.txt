Merge pull request #12481 from aabadie/pr/driver/rn2xx3_fix_sleep

drivers/rn2xx3: fix sleep function
gnrc_ipv6.c: fix SEGFAULT when multicasting with multiple interfaces

When writing to the IPv6 header the implementation currently doesn't
take the packet with the (potentially) duplicated header, but the
packet with the original one, which leads to the packet sent and then
released in `gnrc_netif_ethernet.c` first and then accessed again in
further iterations of the "writing to the IPv6 header" loop, which
causes access to an invalid pointer, causing a crash.

Fixes #11980
gnrc_ipv6: fix SEGFAULT when multicasting with multiple interfaces

When writing to the IPv6 header the implementation currently doesn't
take the packet with the (potentially) duplicated header, but the
packet with the original one, which leads to the packet sent and then
released in `gnrc_netif_ethernet.c` first and then accessed again in
further iterations of the "writing to the IPv6 header" loop, which
causes access to an invalid pointer, causing a crash.

Fixes #11980
gnrc_ipv6: fix SEGFAULT when multicasting with multiple interfaces

When writing to the IPv6 header the implementation currently doesn't
take the packet with the (potentially) duplicated header, but the
packet with the original one, which leads to the packet sent and then
released in `gnrc_netif_ethernet.c` first and then accessed again in
further iterations of the "writing to the IPv6 header" loop, which
causes access to an invalid pointer, causing a crash.

Fixes #11980

(cherry picked from commit ce14ee1f7702fec43b682c850da66285ca164ff0)
gnrc_ipv6: fix SEGFAULT when multicasting with multiple interfaces

When writing to the IPv6 header the implementation currently doesn't
take the packet with the (potentially) duplicated header, but the
packet with the original one, which leads to the packet sent and then
released in `gnrc_netif_ethernet.c` first and then accessed again in
further iterations of the "writing to the IPv6 header" loop, which
causes access to an invalid pointer, causing a crash.

Fixes #11980

(cherry picked from commit ce14ee1f7702fec43b682c850da66285ca164ff0)
gnrc_ipv6: fix SEGFAULT when multicasting with multiple interfaces

When writing to the IPv6 header the implementation currently doesn't
take the packet with the (potentially) duplicated header, but the
packet with the original one, which leads to the packet sent and then
released in `gnrc_netif_ethernet.c` first and then accessed again in
further iterations of the "writing to the IPv6 header" loop, which
causes access to an invalid pointer, causing a crash.

Fixes #11980
gnrc_ipv6: fix SEGFAULT when multicasting with multiple interfaces

When writing to the IPv6 header the implementation currently doesn't
take the packet with the (potentially) duplicated header, but the
packet with the original one, which leads to the packet sent and then
released in `gnrc_netif_ethernet.c` first and then accessed again in
further iterations of the "writing to the IPv6 header" loop, which
causes access to an invalid pointer, causing a crash.

Fixes #11980
