Merge #19071

19071: boards/rpi-pico-w: initial support for rpi pico W board r=maribu a=krzysztof-cabaj

### Contribution description

This PR adds initial RIOT OS support for the Raspberry Pico W - the Raspberry Pico with a Wifi/Bluetooth Infineon CYW 43439 module (*).

The code is based on PR #15822 and contains changes associated with small differences between RPi Pico and Pico W. The most important is that LED0 is connected to the CYW 43439 module and without PIO (PR #17425) cannot be used (RP2040 and CYW 43439 are connected together using SPI and Pico SDK utilize PIO to program SPI). Current LED0 implementation is similar to one for `native` board and prints appropriate texts on STDIO.      

(*) Currently network connectivity not implemented.

### Testing procedure

Flash sample program and look at doc:

```
make doc
xdg-open doc/doxygen/html/group__boards__rpi__pico__w.html
```

### Issues/PRs references

Based on PR #15822.
Needs PR #17425.

Additional doc:
[Raspberry Pi Pico and Pico W](https://www.raspberrypi.com/documentation/microcontrollers/raspberry-pi-pico.html)

Co-authored-by: krzysztof-cabaj <kcabaj@gmail.com>
cpu/esp32: don't initialize the UART pins if already initialized

To avoid garbage on reconfiguring the UART console pins, e.g. in initialization of the `arduino` module, pins that are already configured as UART pins must not be initialized.
Merge #19146

19146: cpu/esp32: fix and improve UART initialization r=benpicco a=gschorcht

### Contribution description

This PR fixes issue #19138 that was introduced with PR #19100. It contains the following changes to fix the problems and to improve the UART initialization:

- If `LOG_LEVEL` is greater or equal 4, such as in `tests/log_printfnoformat`, the ESP-IDF config function called for the GPIO pins of the UART will output the configuration with `printf` before the `_GLOBAL_REENT` structure is initialized. This causes a crash during system startup. Therefore the initialization by `syscalls_init` must be called by `earlier in the startup procedure.
- Since PR #19100 it is possible to define:
   - other pins for `UART_DEV(0)` than the default pins
   - different `UART_DEV(0)` pins for the bootloader and RIOT
   
   To allow correct reinitialization of the UART pins used by the bootloader as well as their usage for other purposes, the pin usage for the default UART0 pins and the UART pinsused by the bootloader are reset to `_GPIO`. This is done in `uart_system_init` which has to be called earlier in the startup procedure.
- To avoid garbage on reconfiguring the UART console pins, e.g. in initialization of the `arduino` module, pins that are already configured as UART pins must not be initialized.
- To avoid a several msec long LOW pulse resulting in some garbage during the UART initialization, the TX line is set to HIGH and temporarily configured as a pull-up open-drain output before configuring it as a push-pull output.

The PR requires a backport to 2023.1

### Testing procedure

The following tests should work with this PR:
- [ ] `tests/log_color`
- [ ] `tests/log_printfnoformat`
- [ ] `tests/sys_arduino`

### Issues/PRs references

Fixes #19138 

Co-authored-by: Gunar Schorcht <gunar@schorcht.net>
Merge #19146

19146: cpu/esp32: fix and improve UART initialization r=benpicco a=gschorcht

### Contribution description

This PR fixes issue #19138 that was introduced with PR #19100. It contains the following changes to fix the problems and to improve the UART initialization:

- If `LOG_LEVEL` is greater or equal 4, such as in `tests/log_printfnoformat`, the ESP-IDF config function called for the GPIO pins of the UART will output the configuration with `printf` before the `_GLOBAL_REENT` structure is initialized. This causes a crash during system startup. Therefore the initialization by `syscalls_init` must be called by `earlier in the startup procedure.
- Since PR #19100 it is possible to define:
   - other pins for `UART_DEV(0)` than the default pins
   - different `UART_DEV(0)` pins for the bootloader and RIOT
   
   To allow correct reinitialization of the UART pins used by the bootloader as well as their usage for other purposes, the pin usage for the default UART0 pins and the UART pinsused by the bootloader are reset to `_GPIO`. This is done in `uart_system_init` which has to be called earlier in the startup procedure.
- To avoid garbage on reconfiguring the UART console pins, e.g. in initialization of the `arduino` module, pins that are already configured as UART pins must not be initialized.
- To avoid a several msec long LOW pulse resulting in some garbage during the UART initialization, the TX line is set to HIGH and temporarily configured as a pull-up open-drain output before configuring it as a push-pull output.

The PR requires a backport to 2023.1

### Testing procedure

The following tests should work with this PR:
- [ ] `tests/log_color`
- [ ] `tests/log_printfnoformat`
- [ ] `tests/sys_arduino`

### Issues/PRs references

Fixes #19138 

Co-authored-by: Gunar Schorcht <gunar@schorcht.net>
Merge #19148

19148: cpu/esp32: fix and improve UART initialization [backport 2023.01] r=maribu a=gschorcht

# Backport of PR #19146

### Contribution description

This PR fixes issue #19138 that was introduced with PR #19100. It contains the following changes to fix the problems and to improve the UART initialization:

- If `LOG_LEVEL` is greater or equal 4, such as in `tests/log_printfnoformat`, the ESP-IDF config function called for the GPIO pins of the UART will output the configuration with `printf` before the `_GLOBAL_REENT` structure is initialized. This causes a crash during system startup. Therefore the initialization by `syscalls_init` must be called by `earlier in the startup procedure.
- Since PR #19100 it is possible to define:
   - other pins for `UART_DEV(0)` than the default pins
   - different `UART_DEV(0)` pins for the bootloader and RIOT
   
   To allow correct reinitialization of the UART pins used by the bootloader as well as their usage for other purposes, the pin usage for the default UART0 pins and the UART pinsused by the bootloader are reset to `_GPIO`. This is done in `uart_system_init` which has to be called earlier in the startup procedure.
- To avoid garbage on reconfiguring the UART console pins, e.g. in initialization of the `arduino` module, pins that are already configured as UART pins must not be initialized.
- To avoid a several msec long LOW pulse resulting in some garbage during the UART initialization, the TX line is set to HIGH and temporarily configured as a pull-up open-drain output before configuring it as a push-pull output.

The PR requires a backport to 2023.1

### Testing procedure

The following tests should work with this PR:
- [ ] `tests/log_color`
- [ ] `tests/log_printfnoformat`
- [ ] `tests/sys_arduino`

### Issues/PRs references

Fixes #19138 

Co-authored-by: Gunar Schorcht <gunar@schorcht.net>
