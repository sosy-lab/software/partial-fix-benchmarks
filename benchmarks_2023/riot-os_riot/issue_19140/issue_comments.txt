rmutex test KO
Sorry of the long latency. It is indeed intentional that two threads have the same priority here, as RIOT's scheduling is generally predictable and reproducible, except for when external factors (such as a thread being blocked waiting for an IRQ - so e.g. all networking) come into play. So it does indeed make sense to also test for this.

The wording in the README was a bit confusing:

> If two threads have the same priority the lower thread id should acquire the lock.

This is not generally true. Threads with higher priority (that is, lower numeric priority value) will always run before threads with lower priority. But if two threads with the same priority are ready to run, the scheduler will pick whatever thread is in front of the run queue. This just happens to be the thread with the lower id in this app.
Sorry, but the latest reported expected results, are showing exactly the same issue.

During the enumeration of the process, described in Read.me
Latest line before the main ...
...
T7 (prio 3, depth 0): trying to lock rmutex now
main: unlocking recursive mutex
...

- T7 is seen as prio 3, but in the source code, T7 is declared as prio 4 !

I agree on the explanations regarding scheduling behaviour, but there is a mismatch beetween code and test Results.
The registered tests results cannot come from the rmutex source code.

- Because 3 != 4
