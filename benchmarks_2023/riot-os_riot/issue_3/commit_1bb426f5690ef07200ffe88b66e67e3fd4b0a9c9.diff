diff --git a/boards/jiminy-mega256rfr2/include/board.h b/boards/jiminy-mega256rfr2/include/board.h
index 5379ffff0d0e..b14930f95f50 100644
--- a/boards/jiminy-mega256rfr2/include/board.h
+++ b/boards/jiminy-mega256rfr2/include/board.h
@@ -124,6 +124,19 @@ extern "C" {
 #define CPU_ATMEGA_CLK_SCALE_INIT    CPU_ATMEGA_CLK_SCALE_DIV1
 /** @} */
 
+/**
+ * @name TPS6274x Stepdown config
+ * @{
+ */
+#define TPS6274X_PARAMS { .vsel = { GPIO_PIN(PORT_D, 6), \
+                                    GPIO_PIN(PORT_D, 7), \
+                                    GPIO_PIN(PORT_G, 0), \
+                                    GPIO_PIN(PORT_G, 2), \
+                                   },                    \
+                         .ctrl_pin = GPIO_PIN(PORT_G, 5) \
+                        }
+/** @} */
+
 /**
  * @brief Initialize board specific hardware, including clock, LEDs and std-IO
  */
diff --git a/boards/jiminy-mega256rfr2/include/periph_conf.h b/boards/jiminy-mega256rfr2/include/periph_conf.h
index 6bf8722335b3..181aa2fd6168 100644
--- a/boards/jiminy-mega256rfr2/include/periph_conf.h
+++ b/boards/jiminy-mega256rfr2/include/periph_conf.h
@@ -128,18 +128,6 @@ extern "C" {
 #define ADC_NUMOF           (8U)
 /** @} */
 
-/**
- * @name TPS6274x Stepdown config
- * @{
- */
-#define TPS6274X_CONFIG { \
-    .vsel = { GPIO_PIN(PORT_D, 6), \
-              GPIO_PIN(PORT_D, 7), \
-              GPIO_PIN(PORT_G, 0), \
-              GPIO_PIN(PORT_G, 2) }, \
-    .ctrl_pin = GPIO_PIN(PORT_G, 5) \
-}
-/** @} */
 #ifdef __cplusplus
 }
 #endif
diff --git a/drivers/Makefile.dep b/drivers/Makefile.dep
index 176ad0f55588..982a2f2379b3 100644
--- a/drivers/Makefile.dep
+++ b/drivers/Makefile.dep
@@ -452,6 +452,10 @@ ifneq (,$(filter tcs37727,$(USEMODULE)))
   FEATURES_REQUIRED += periph_i2c
 endif
 
+ifneq (,$(filter tps6274,$(USEMODULE)))
+  FEATURES_REQUIRED += periph_gpio
+endif
+
 ifneq (,$(filter tja1042,$(USEMODULE)))
   USEMODULE += can_trx
   FEATURES_REQUIRED += periph_gpio
diff --git a/drivers/Makefile.include b/drivers/Makefile.include
index 5b0b949adcef..33f3006526ad 100644
--- a/drivers/Makefile.include
+++ b/drivers/Makefile.include
@@ -250,6 +250,10 @@ ifneq (,$(filter tmp006,$(USEMODULE)))
   USEMODULE_INCLUDES += $(RIOTBASE)/drivers/tmp006/include
 endif
 
+ifneq (,$(filter tps6274x,$(USEMODULE)))
+  USEMODULE_INCLUDES += $(RIOTBASE)/drivers/tps6274x/include
+endif
+
 ifneq (,$(filter tsl2561,$(USEMODULE)))
   USEMODULE_INCLUDES += $(RIOTBASE)/drivers/tsl2561/include
 endif
diff --git a/drivers/include/tps6274x.h b/drivers/include/tps6274x.h
index 8970225abdec..487e8fcfae56 100644
--- a/drivers/include/tps6274x.h
+++ b/drivers/include/tps6274x.h
@@ -34,32 +34,49 @@ extern "C"
  * @brief   TPS6274x Configuration struct
  */
 typedef struct {
-    gpio_t vsel[4];         /**< select line pin mapping */
-    gpio_t ctrl_pin;        /**< ctrl pin  mapping */
-} tps6274x_config_t;
+    gpio_t vsel[4];            /**< select line pin mapping */
+    gpio_t ctrl_pin;           /**< ctrl pin  mapping */
+} tps6274x_params_t;
 
 /**
- * @brief   init converter
+ * @brief   Device descriptor for the TPS6274x
+ */
+typedef struct {
+    tps6274x_params_t  params; /**< device initialization parameters */
+} tps6274x_t;
+
+/**
+ * @brief   Status and error return codes
+ */
+enum {
+    TPS6274X_OK = 0,           /**< everything was fine */
+    TPS6274X_ERR_INIT,         /**< error during init */
+};
+
+/**
+ * @brief   Init converter
+ *
+ * @param[in] voltage  Initialized device descriptor
+ * @param[in] params   Initialization parameters
  *
- * @param[in] voltage   Voltage to set in mV (needs to be between 1.8V-3.3V
  * @return              set voltage in mV
  */
-unsigned int tps6274x_init(unsigned int voltage);
+int tps6274x_init(tps6274x_t *dev, const tps6274x_params_t *params);
 
 /**
- * @brief   switch to different voltage level
+ * @brief   Switch to different voltage level
  *
  * @param[in] voltage   Voltage to set in mV (needs to be between 1.8V-3.3V
  * @return              the voltage that was set in mV
  */
-unsigned int tps6274x_switch_voltage(unsigned int voltage);
+uint16_t tps6274x_switch_voltage(tps6274x_t *dev, uint16_t voltage);
 
 /**
- * @brief   sets ctrl pin high to power a subsystem connected on the load pin
+ * @brief   Sets ctrl pin high to power a subsystem connected on the load pin
  *
  * @param[in] status    0 will disable the load, everything else will activate it
  */
-void tps6274x_load_ctrl(unsigned int status);
+void tps6274x_load_ctrl(tps6274x_t *dev, int status);
 
 #ifdef __cplusplus
 }
diff --git a/drivers/tps6274x/include/tps6274x_params.h b/drivers/tps6274x/include/tps6274x_params.h
new file mode 100644
index 000000000000..3ed96a41c2f2
--- /dev/null
+++ b/drivers/tps6274x/include/tps6274x_params.h
@@ -0,0 +1,57 @@
+/*
+ * Copyright (C) 2018 HAW Hamburg
+ *
+ * This file is subject to the terms and conditions of the GNU Lesser
+ * General Public License v2.1. See the file LICENSE in the top level
+ * directory for more details.
+ */
+
+/**
+ * @ingroup     drivers_tps6274x
+ *
+ * @{
+ * @file
+ * @brief       Default configuration for TPS6274x DC-DC Converter
+ *
+ * @author      Peter Kietzmann <peter.kietzmann@haw-hamburg.de>
+ */
+
+#ifndef TPS6274X_PARAMS_H
+#define TPS6274X_PARAMS_H
+
+#include "board.h"
+#include "tps6274x.h"
+
+#ifdef __cplusplus
+extern "C" {
+#endif
+
+/**
+ * @name    Set default configuration parameters for the TPS6274x
+ * @{
+ */
+#ifndef TPS6274X_PARAMS
+#define TPS6274X_PARAMS { .vsel = { GPIO_PIN(0, 0), \
+                                    GPIO_PIN(0, 1), \
+                                    GPIO_PIN(0, 2), \
+                                    GPIO_PIN(0, 3), \
+                                   },               \
+                         .ctrl_pin = GPIO_PIN(0, 4) \
+                        }
+#endif
+/**@}*/
+
+/**
+ * @brief   Configure TPS6274X
+ */
+static const tps6274x_params_t tps6274x_params[] =
+{
+    TPS6274X_PARAMS
+};
+
+#ifdef __cplusplus
+}
+#endif
+
+#endif /* TPS6274X_PARAMS_H */
+/** @} */
diff --git a/drivers/tps6274x/tps6274x.c b/drivers/tps6274x/tps6274x.c
index 6b6292d40ad6..e2608cf7f70b 100644
--- a/drivers/tps6274x/tps6274x.c
+++ b/drivers/tps6274x/tps6274x.c
@@ -24,29 +24,29 @@
 #define ENABLE_DEBUG (0)
 #include "debug.h"
 
-#ifdef TPS6274X_CONFIG
-const tps6274x_config_t converter_config = TPS6274X_CONFIG;
-#else
-/* Default device parameters are undefined */
-#error "No config struct for module tps6274x available"
-#endif
-
-
-
-unsigned int tps6274x_init(unsigned int voltage)
+int tps6274x_init(tps6274x_t *dev, const tps6274x_params_t *params)
 {
+    int ret;
+
+    dev->params = *params;
     for (uint8_t i = 0; i < 4; i++) {
-        if (converter_config.vsel[i] != GPIO_UNDEF) {
-            gpio_init(converter_config.vsel[i], GPIO_OUT);
+        if (dev->params.vsel[i] != GPIO_UNDEF) {
+            ret = gpio_init(dev->params.vsel[i], GPIO_OUT);
+            if(ret < 0) {
+                return TPS6274X_ERR_INIT;
+            }
         }
     }
-    if (converter_config.ctrl_pin != GPIO_UNDEF) {
-        gpio_init(converter_config.ctrl_pin, GPIO_OUT);
+    if (dev->params.ctrl_pin != GPIO_UNDEF) {
+        ret = gpio_init(dev->params.ctrl_pin, GPIO_OUT);
+        if(ret < 0) {
+            return TPS6274X_ERR_INIT;
+        }
     }
-    return tps6274x_switch_voltage(voltage);
+    return TPS6274X_OK;
 }
 
-unsigned int tps6274x_switch_voltage(unsigned int voltage)
+uint16_t tps6274x_switch_voltage(tps6274x_t *dev, uint16_t voltage)
 {
     if (voltage < 1800) {
         voltage = 1800;
@@ -57,24 +57,29 @@ unsigned int tps6274x_switch_voltage(unsigned int voltage)
     uint8_t vsel = (voltage - 1800) / 100;
     uint8_t vsel_set = 0;
     for (uint8_t i = 0; i < 4; i++) {
-        if (converter_config.vsel[i] != GPIO_UNDEF) {
-            gpio_write(converter_config.vsel[i], (vsel & (0x01 << i)));
+        if (dev->params.vsel[i] != GPIO_UNDEF) {
+            gpio_write(dev->params.vsel[i], (vsel & (0x01 << i)));
             /* mark pins that could and had to be set */
             vsel_set |= vsel & (1 << i);
         }
+#if ENABLE_DEBUG
         else {
-            DEBUG("[tps6274x] Pin vsel%u is not connected but is required for selected voltage level\n", i + 1);
+            printf("[tps6274x] Pin vsel%u is not connected but is required for \
+                selected voltage level\n", i + 1);
         }
+#endif
     }
-    return ((unsigned int)vsel_set) * 100 + 1800;
+    return ((uint16_t)vsel_set) * 100 + 1800;
 }
 
-void tps6274x_load_ctrl(unsigned int status)
+void tps6274x_load_ctrl(tps6274x_t *dev, int status)
 {
-    if (converter_config.ctrl_pin != GPIO_UNDEF) {
-        gpio_write(converter_config.ctrl_pin, status);
+    if (dev->params.ctrl_pin != GPIO_UNDEF) {
+        gpio_write(dev->params.ctrl_pin, status);
     }
+#if ENABLE_DEBUG
     else {
-        DEBUG("[TPS6274x] CTRL Pin not defined, no load activation possible\n");
+        printf("[TPS6274x] CTRL Pin not defined, no load activation possible\n");
     }
+#endif
 }
diff --git a/tests/driver_tps6274x/Makefile b/tests/driver_tps6274x/Makefile
index bb55552b752f..c9c8dab0fe91 100644
--- a/tests/driver_tps6274x/Makefile
+++ b/tests/driver_tps6274x/Makefile
@@ -1,10 +1,7 @@
 APPLICATION = driver_tps6274x
 include ../Makefile.tests_common
 
-# Known boards with support for the TPS6274x
-BOARD_WHITELIST = jiminy-mega256rfr2
-
-FEATURES_REQUIRED = periph_gpio
+BOARD ?= native
 
 USEMODULE += tps6274x
 USEMODULE += xtimer
diff --git a/tests/driver_tps6274x/README.md b/tests/driver_tps6274x/README.md
index 198e7d6ba416..d967b494e7b3 100644
--- a/tests/driver_tps6274x/README.md
+++ b/tests/driver_tps6274x/README.md
@@ -1,10 +1,10 @@
-# About
+## About
 Tests the tps6274x step-down converter
 
-# Usage
+## Usage
 Build the test by using the `make BOARD=??? flash` command in the `test/driver_tps6274x/` folder.
 
-# Results
+## Results
 The stepdown converter will step from 1.8V to 3.3V in 100mV steps.
 Each voltage will be set for 3 seconds and can be measured at the VOUT Pin.
 After 1 second also the LOAD output pin will be enabled for 2 seconds
diff --git a/tests/driver_tps6274x/main.c b/tests/driver_tps6274x/main.c
index 2eca175dbc86..c5999f00e275 100644
--- a/tests/driver_tps6274x/main.c
+++ b/tests/driver_tps6274x/main.c
@@ -18,26 +18,31 @@
  * @}
  */
 #include <stdio.h>
+
 #include "board.h"
-#include "tps6274x.h"
 #include "xtimer.h"
+#include "tps6274x.h"
+#include "tps6274x_params.h"
 
 int main(void)
 {
+    tps6274x_t dev;
+
     puts("This application will test the tps6274x step down converter by switching through all voltages.");
     puts("Every voltage will be active for 3s and can be verified with a multimeter");
-    tps6274x_init(1800);
+
+    tps6274x_init(&dev, &tps6274x_params[0]);
     for (unsigned int voltage = 1800; voltage <= 3300; voltage += 100) {
         printf("%u mV \n", voltage);
-        if (voltage != tps6274x_switch_voltage(voltage)) {
+        if (voltage != tps6274x_switch_voltage(&dev, voltage)) {
             printf("Not all Selector lines are connected in order to set a level of %umV.", voltage);
         }
         xtimer_sleep(1);
         puts("Load PIN will be enabled for 2s");
-        tps6274x_load_ctrl(1);
+        tps6274x_load_ctrl(&dev, 1);
         xtimer_sleep(2);
         puts("Load PIN will be shut off");
-        tps6274x_load_ctrl(0);
+        tps6274x_load_ctrl(&dev, 0);
     }
     printf("Test Done");
     return 0;
