`ansi` does not render correct output.
Yes, confirmed. I'm tracking down the bug. Is that possible the uft8_xxx functions do not work for a single `\n`?

That was my first idea too, but checking out a pre-utf8 version (b0166992) just now and doing a clean build still resulted in the same wrong rendering.

In fact I tested several commits down to the gl3 branch merge and it always shown the same wrong rendering for me.  Can you confirm this?  Maybe I didn't clean up the build properly somewhere in the process.

I guess it's a work for `git bisect` but I've never managed to use it...

If you add a simple space for each single newline, then rendering is ok. So this might be a bound problem somewhere (<= instead of <)

> I guess it's a work for git bisect but I've never managed to use it...

In theory yes but for this bug it isn't the case, unless you can find a 'working' commit.  I didn't find one yet.

From my investigation, the problem might in [line 351](https://github.com/rougier/freetype-gl/blob/master/text-buffer.c#L351) in text-buffer. We return without updating the cursor position.

According to `text_buffer_finish_line` doc the last parameter indicates if the pen should advance one line or not.  However the code doing this on [line 257](https://github.com/rougier/freetype-gl/blob/master/text-buffer.c#L257) refers to self->line_descender, which is 0 for "\n" strings.  Is line_descender even the correct variable?  A descender is something else then the line leading.  AFAIK we store the leading in the texture_font_t height attribute.

But just after that, there is `self->line_descender = 0`. Does not make much sense to me. Do you know who coded this function ? We should better ask him (I hope it's not me).

It seems the self->line_descender is taken from the markup. 

I coded text_buffer_finish_line but the lines in question I simply
extracted from the previous code that was inside line 351
https://github.com/rougier/freetype-gl/blob/master/text-buffer.c#L351 and
added the advancePen logic.   Can't say who coded that before.

On Tue, Feb 23, 2016 at 2:46 PM, Nicolas P. Rougier <
notifications@github.com> wrote:

> It seems the self->line_descender is taken from the markup.
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/rougier/freetype-gl/issues/102#issuecomment-187871304
> .

I think I've the explanation. `self->line_descender`/`self->line_ascender` are reset after each newline and are kept up to date with the actual markup ascender/descender. Problem is that if a line is made of a single `\n`, `self->line_descender`/`self->line_ascender` are not updated ([line 351](https://github.com/rougier/freetype-gl/blob/master/text-buffer.c#L351), update happens just after). One quick fix is to explicitely add `pen->y -= (markup->font->ascender - self->line_ascender);` after [line 352](https://github.com/rougier/freetype-gl/blob/master/text-buffer.c#L352).

> One quick fix is to explicitely add pen->y -= (markup->font->ascender - self->line_ascender); after line 352.

That didn't work, it introduced new lines between every actual existing new line.  But I think I've  fixed the missing newlines issue by putting the check for newlines after setting the line ascender and descender.

Yes, fixed for me.
