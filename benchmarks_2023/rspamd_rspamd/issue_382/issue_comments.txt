Exclude messages with `qid: <undef>` from statistics
Yes, that sounds reasonable, as neither webui nor rspamc set this header when scanning.

Would you consider adding a configuration directive for that?

What do you mean?

Optionally exclude from internal rspamd statistics scan results for messages with `qid: <undef>`.

I dislike this idea as is. What I think could be done is to create a special task flag to exclude it from stats and set it from lua, e.g.

``` lua
rspamd_config:register_post_filter(function(task)
  if task:get_queue_id() then task:set_flag('no_stat') end
end)
```

What do you think about it?

It would be great, looks very flexible.

Should be done.

I'v added code you mentioned before to `rspamd.local.lua`:

``` lua
rspamd_config:register_post_filter(function(task)
  if task:get_queue_id() then task:set_flag('no_stat') end
end)
```

Is that enough? It doesn't work.

Shouldn't it be `if not task:get_queue_id()` ? :)

Of course. It works now. Thank you all.

Would you add similar task flag to exclude messages from history as well?

@moisseev 
How can I set flags (no_stat, no_log,...)?
Can you write more examples?

to @mnik247    

rspamd.local.lua:

``` lua
rspamd_config:register_post_filter(function(task)
  if not task:get_queue_id() then task:set_flag('no_stat') end
end)
```

It works. I haven't tried anything else yet.

@moisseev 
It will work global for all messages?
How can skip specific messages?

Conditional operator sets flag for messages with undefined queue id. You can change condition as you need.

@mnik247, thanks for the hint!
Rspamd doesn't add messages to the history if `no_log` flag is set.
