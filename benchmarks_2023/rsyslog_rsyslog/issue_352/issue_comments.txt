check third-party patches for solaris
Oracle "updated" to 8.4.2 just one patch left there: https://hg.java.net/hg/solaris-userland~gate/file/bf30d46ab06e/components/rsyslog/patches

@alorbach can you please check to integrate this in your work. Please comment and/out close when done
Hey guys, the current pkgsrc patches that deal with SunOS and some BSD flavors are here (and not the seldom updated Joyent trunk branch):

https://github.com/NetBSD/pkgsrc/tree/trunk/sysutils/rsyslog/patches
@mamash Thanks for the link, very useful. We'll see that we evaluate them and get the into rsyslog.
all merged except:
* https://github.com/NetBSD/pkgsrc/blob/trunk/sysutils/rsyslog/patches/patch-plugins_imfile_imfile.c -- not a problem as code does not need it (any more)

Some merges were partial as the code had evolved.
The SOL_TCP problem (in plugins/imptcp/imptcp.c) is still not fixed by the commit. SOL_TCP doesn't exist on Darwin and SunoS either, and should just be replaced by the portable IPPROTO_TCP everywhere (including Linux).
> The SOL_TCP problem (in plugins/imptcp/imptcp.c) is still not fixed by the commit. SOL_TCP doesn't exist on Darwin and SunoS either

Mhhhh...  I did a define of SOL_TCP if not existing.  That should work. doesn't it? Also, your Solaris buildbot slaves passed the build. They also run sunstudio compiler, so I thought that should have found that -- shouldn't it?

Anyhow, I'll explore the IPPROTO_TCP path, which indeed sounds better.
I apologize, I tested the build and realized that imptcp doesn't get built on SunOS at all, so my point is moot. And your current commit does fix the problem on BSD.
NP -- your comment helped my to understand I can use IPPROTO_TCP - PR #2260 is underway :-)
If the "dirs" error still was an issue, it should be fixed with this PR  
https://github.com/rsyslog/rsyslog/pull/2227
yup, that's why I didn't touch it
@mamash SOL_TPC removed now -- thx!
This thread has been automatically locked since there has not been any recent activity after it was closed. Please open a new issue for related bugs.