Cannot parse messages from TP-Link T1500G-10PS
If "date autodetection" is difficult, it would be nice to support a `detect.DateFormat="x"` directive, that would correspond to the fmt parameter of the [strptime function](https://www.gnu.org/software/libc/manual/html_node/Low_002dLevel-Time-String-Parsing.html).

My C is very rusty, but here's some example code to show what I mean. The rsyslog end user would only have to set DateFormat:

```c
#define _XOPEN_SOURCE 700
#include <time.h>
#include <stdio.h>
#include <string.h>

#define DateFormat "%Y-%m-%d %H:%M:%S"

int main(void)
{
    char *input = "2023-05-30 12:49:51 sftsw1 61209 Message text";
    const char *cp;
    struct tm tm;
    char output[100];

    memset(&tm, '\0', sizeof(tm));
    cp = strptime(input, DateFormat, &tm);
    if (cp == NULL) {
        printf("Can't parse!");
        return 1;
    }
    strftime((char *)&output, sizeof(output), "%F %T", &tm);
    printf("DATE=%s\n", output);
    printf("REST OF STRING=%s\n", cp);

    return 0;
}
```

And the output of the program is:

```console
DATE=2023-05-30 12:49:51
REST OF STRING= sftsw1 61209 Message text
```
There is already a lot of code in place to guess at dates, this sender is not 
sending a RFC compliant date.

The biggest problem I see with a detect.DateFormat option is that you seldom get 
logs from only one source, so you need to have a stack of options not just one.

In your case, you also have the problem that they don't send a valid syslogtag 
as well, instead they have what is probably a sequence number.

see if they have an option to send a RFC-2545 message, they seem more likely to 
get that right.

other than that, you can define a parder using the liblognorm parsing rules and 
insert it into the stack ahead of the default catch-all parser that would be 
able to not only handle the date, but also the sequence number.

David Lang

On Mon, 29 May 2023, Alkis Georgopoulos wrote:

> Date: Mon, 29 May 2023 23:00:53 -0700
> From: Alkis Georgopoulos ***@***.***>
> Reply-To: rsyslog/rsyslog
>     ***@***.***>
> To: rsyslog/rsyslog ***@***.***>
> Cc: Subscribed ***@***.***>
> Subject: Re: [rsyslog/rsyslog] Cannot parse messages from TP-Link T1500G-10PS
>     (Issue #5148)
> 
> If "date autodetection" is difficult, it would be nice to support a `detect.DateFormat="x"` directive, that would correspond to the fmt parameter of the [strptime function](https://www.gnu.org/software/libc/manual/html_node/Low_002dLevel-Time-String-Parsing.html).
>
> My C is very rusty, but here's some example code to show what I mean. The rsyslog end user would only have to set DateFormat:
>
> ```c
> #define _XOPEN_SOURCE 700
> #include <time.h>
> #include <stdio.h>
> #include <string.h>
>
> #define DateFormat "%Y-%m-%d %H:%M:%S"
>
> int main(void)
> {
>    char *input = "2023-05-30 12:49:51 sftsw1 61209 Message text";
>    const char *cp;
>    struct tm tm;
>    char output[100];
>
>    memset(&tm, '\0', sizeof(tm));
>    cp = strptime(input, DateFormat, &tm);
>    if (cp == NULL) {
>        printf("Can't parse!");
>        return 1;
>    }
>    strftime((char *)&output, sizeof(output), "%F %T", &tm);
>    printf("DATE=%s\n", output);
>    printf("REST OF STRING=%s\n", cp);
>
>    return 0;
> }
> ```
>
> And the output of the program is:
>
> ```console
> DATE=2023-05-30 12:49:51
> REST OF STRING= sftsw1 61209 Message text
> ```
>
>

Hi David, thank you for your input,

> an option to send a RFC-2545 message

Unfortunately no, we have around 20 switches of various vendors that do not have that option (the rest of the switches are fine).

> you can define a parser using the liblognorm parsing rules
> and insert it into the stack ahead of the default catch-all parser

In that solution, would I need to patch and recompile the Ubuntu rsyslog package?
For a long-term solution, I'd like to avoid that. It would mean we'd have to do apt-pinning in our log servers and manually monitor for rsyslog security updates.
On Tue, 30 May 2023, Alkis Georgopoulos wrote:

>> you can define a parser using the liblognorm parsing rules
>> and insert it into the stack ahead of the default catch-all parser
>
> In that solution, would I need to patch and recompile the Ubuntu rsyslog package?
> For a long-term solution, I'd like to avoid that. It would mean we'd have to do apt-pinning in our log servers and manually monitor for rsyslog security updates.

No, this is all in the configuration language. A couple years ago a input parser 
was added that let you define the parsing with the same language that you use 
for mmnormalize

https://www.rsyslog.com/doc/master/configuration/modules/pmnormalize.html
https://www.rsyslog.com/doc/master/configuration/parser.html

David Lang

Thank you. It appears that Ubuntu doesn't have that library built into rsyslog, because rsyslog is in "main" while the library is in "universe":

```console
# zless /usr/share/doc/rsyslog/changelog.Debian.gz
rsyslog (8.2112.0-2ubuntu1) jammy; urgency=low
  * Merge from Debian unstable. Remaining changes:
    - Drop [mm|pm]normalize modules, depending on liblognorm from universe.
      + d/rules: drop --enable-mmnormalize & --enable-pmnormalize
      + d/control: drop build dependency on liblognorm-dev
```

That might be acceptable though. I think Debian Bookworm removed rsyslog from the default installation; if Ubuntu does the same, it might be demoted to universe; in that case, they won't need the Ubuntu-specific patch that removes these modules.

I'll also have a look at developing a small python script that listens on 514, does normalization, and then forwards to rsyslog in a socket or on port 515... Thanks again!
I filed a bug report in Ubuntu about this:
https://bugs.launchpad.net/ubuntu/+source/rsyslog/+bug/2023266

I'll re-compile the Ubuntu package, try pmnormalize and post the solution if I can find it.
I managed to parse the incompatible message but then I got stuck to this question:
**How can I configure rsyslog to use the normal parsers after my own parser fails?**
So that I'm able to parse the messages from the other, compatible switches, in the same IP:port.

Rsyslog config:

```log
module(load="pmnormalize")
parser(name="custom.pmnormalize" type="pmnormalize" undefinedPropertyError="on"
	rule="rule=:<%pri:number%>%date:word% %time:word% %hostname:word% %syslogtag:number% %msg:rest%")
template(name="RawFormat" type="string" string="RAW: %rawmsg%")

ruleset(name="udp-received" parser="custom.pmnormalize") {
    action(type="omfile" file="/var/log/site/normal.log" template="RSYSLOG_SyslogProtocol23Format")
    action(type="omfile" file="/var/log/site/debug.log" template="RSYSLOG_DebugFormat")
    action(type="omfile" file="/var/log/site/raw.log" template="RawFormat")
}

module(load="imudp")
input(type="imudp" port="514" ruleset="udp-received")
```

Example line received by the incompatible switch. It's parsed OK:

```
echo "<133>$(date "+%F %T") Hostname 51891 Message #$RANDOM body" | nc -q0 -u 192.168.67.38 514
```

Example line received by typical rsyslog forwarding. It fails to be parsed with my parser, and I can't get it to fall back to the normal parsers where it would succeed:

```
echo "<155>1 $(date -Iseconds) Hostname Syslogtag Procid Msgid [Structured Data] Message #$RANDOM body" | nc -q0 -u 192.168.67.38 514
```

Thank you!
you can define multiple parsers, it's described here 
https://www.rsyslog.com/doc/v8-stable/concepts/messageparser.html

but I'm not finding an example of the syntax in a quick search right now. 
Hopefully the folks in Germany who will be waking up soon can answer, otherwise 
I'll do more digging tomorrow.

It may just be as simple as defining multiple parser objects (keeping in mind 
that the legacy 3164 parser will match anything), but I'm not 100% sure.

David Lang

On Thu, 8 Jun 2023, Alkis Georgopoulos wrote:

> Date: Thu, 08 Jun 2023 23:10:50 -0700
> From: Alkis Georgopoulos ***@***.***>
> Reply-To: rsyslog/rsyslog
>     ***@***.***>
> To: rsyslog/rsyslog ***@***.***>
> Cc: David Lang ***@***.***>, Comment ***@***.***>
> Subject: Re: [rsyslog/rsyslog] Cannot parse messages from TP-Link T1500G-10PS
>     (Issue #5148)
> 
> I managed to parse the incompatible message but then I got stuck to this question:
> **How can I configure rsyslog to use the normal parsers after my own parser fails?**
> So that I'm able to parse the messages from the other, compatible switches, in the same IP:port.
>
> Rsyslog config:
>
> ```log
> module(load="pmnormalize")
> parser(name="custom.pmnormalize" type="pmnormalize" undefinedPropertyError="on"
> 	rule="rule=:<%pri:number%>%date:word% %time:word% %hostname:word% %syslogtag:number% %msg:rest%")
> template(name="RawFormat" type="string" string="RAW: %rawmsg%")
>
> ruleset(name="udp-received" parser="custom.pmnormalize") {
>    action(type="omfile" file="/var/log/site/normal.log" template="RSYSLOG_SyslogProtocol23Format")
>    action(type="omfile" file="/var/log/site/debug.log" template="RSYSLOG_DebugFormat")
>    action(type="omfile" file="/var/log/site/raw.log" template="RawFormat")
> }
>
> module(load="imudp")
> input(type="imudp" port="514" ruleset="udp-received")
> ```
>
> Example line received by the incompatible switch. It's parsed OK:
>
> ```
> echo "<133>$(date "+%F %T") Hostname 51891 Message #$RANDOM body" | nc -q0 -u 192.168.67.38 514
> ```
>
> Example line received by typical rsyslog forwarding. It fails to be parsed with my parser, and I can't get it to fall back to the normal parsers where it would succeed:
>
> ```
> echo "<155>1 $(date -Iseconds) Hostname Syslogtag Procid Msgid [Structured Data] Message #$RANDOM body" | nc -q0 -u 192.168.67.38 514
> ```
>
> Thank you!
>
>

Thank you David, you're great!

I grepped the sources, found an example, and indeed this works:

```
ruleset(name="udp-received" parser=["custom.pmnormalize","rsyslog.rfc5424","rsyslog.rfc3164"]) {
```

...I'll go ahead test it in production!
Unfortunately while that syntax allowed me to declare multiple parsers, only the first one is used.
I can't get it to fail and use the next ones, even if I send incompatible lines to it that it can't parse.
Am I missing some syntax that instructs the parser to fail if it can't match?
My latest configuration file is:

```
module(load="pmnormalize")
# http://rsyslog.github.io/liblognorm/doc/_build/html/configuration.html
parser(name="custom.pmnormalize" type="pmnormalize" undefinedPropertyError="on" rule=["rule=:<%pri:number%>%date:date-iso% %time:time-24hr% %hostname:word% %syslogtag:number% %msg:rest%"])
template(name="RawFormat" type="string" string="RAW: %rawmsg%")

ruleset(name="udp-received" parser=["custom.pmnormalize","rsyslog.rfc5424","rsyslog.rfc3164"]) {
    action(type="omfile" file="/var/log/site/normal.log" template="RSYSLOG_SyslogProtocol23Format")
    action(type="omfile" file="/var/log/site/debug.log" template="RSYSLOG_DebugFormat")
    action(type="omfile" file="/var/log/site/raw.log" template="RawFormat")
}

module(load="imudp")
input(type="imudp" port="514" ruleset="udp-received")
```

And as an example, if I send an rfc5424 line, it still gets parsed (wrongly) by my custom parser:

```
$ echo "<155>1 $(date -Iseconds) Hostname Syslogtag Procid Msgid [Structured Data] Message #$RANDOM body" | nc -q0 -u 192.168.67.38 514

$ tail debug.log
Debug line with all properties:
FROMHOST: '192.168.67.1', fromhost-ip: '192.168.67.1', HOSTNAME: '192.168.67.1', PRI: 155,
syslogtag '', programname: '', APP-NAME: '-', PROCID: '-', MSGID: '-',
TIMESTAMP: 'Jun  9 14:35:16', STRUCTURED-DATA: '-',
msg: '<155>1 2023-06-09T14:35:15+03:00 Hostname Syslogtag Procid Msgid [Structured Data] Message #22279 body'
escaped msg: '<155>1 2023-06-09T14:35:15+03:00 Hostname Syslogtag Procid Msgid [Structured Data] Message #22279 body'
inputname: imudp rawmsg: '<155>1 2023-06-09T14:35:15+03:00 Hostname Syslogtag Procid Msgid [Structured Data] Message #22279 body'
$!:
$.:
$/:
```


It unfortunately looks like there is a bug in pmnormalize, which makes it return the wrong code on parsing failure. :-(

I'll craft a patch. If you run on RHEL-like system, you can use the daily stable after merge, else you need to wait for the scheduled stable or re-build yourself. We will most probably have a new scheduled stable either next Tuesday or on the 20.
Thanks; I tried to apply the patch on Ubuntu 22.04's version, but I got the same build failures like the [CI on github](https://github.com/rsyslog/rsyslog/actions/runs/5221969501/jobs/9426891688?pr=5154):

```
pmnormalize.c:237:3: error: label 'finalize_it' used but not defined
   ABORT_FINALIZE(RS_RET_COULD_NOT_PARSE);
```
I added a `finalize_it:` line right above `ENDparse2` in pmnormalize.c.
And now everything works!
Except for the date/time parsing, could I please have a bit of more help there?

```
parser(name="custom.pmnormalize" type="pmnormalize" undefinedPropertyError="on" rule=["rule=:<%pri:number%>%date:date-iso% %time:time-24hr% %hostname:word% %syslogtag:number% %msg:rest%"])
```

It feels like I can't set the date and time with this: `%date:date-iso% %time:time-24hr%`
Am I using the wrong variables?

Thank you both!
I'll also point out that mmnormalize is extremely efficient when you have many 
rules. A few jobs ago, I had a 1400 line ruleset to parse my logs (in great 
detail, not just the header vs body) and the difference in peformance between 
matching the first rule vs the last rule was ~30% (which compared especially 
favorably to a 30ish line grok filter where first vs last was a 90% hit IIRC)

so don't be afraid to add additional parsing to your ruleset

@rainer, I haven't dug in to pmnormalize, can it create arbitrary variables? or 
only the standard properties?

David Lang


  On Fri, 9 Jun 2023, Alkis Georgopoulos wrote:

> I added a `finalize_it:` line right above `ENDparse2` in pmnormalize.c.
> And now everything works!
>
> Thank you both!
>
>
