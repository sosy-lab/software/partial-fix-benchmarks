Merge branch 'nitfix' of https://github.com/rgerhards/rsyslog into master-candidate
Fix #718 caused by writing to unallocated address

`evFileHelper.name` is not allocated and not used at all, in typical
case its address is the same as the local variable `etry` in the same
stack (compiler optimization), assigning to `evFileHelper.name[0]`
actually corrupts the etry data structure and crashes the program.

Here's an example output from gdb session.

```
(gdb) p &evFileHelper.name
$6 = (char (*)[]) 0x7ffff6dfc930
(gdb) p &etry
$7 = (wd_map_t **) 0x7ffff6dfc930
(gdb)
```
Remove bad use of an inotify_event structure

Fixes #718.
Remove bad use of an inotify_event structure

Fixes #718.
Remove bad use of an inotify_event structure

Fixes #718.
Fix #718 caused by writing to unallocated address

`evFileHelper.name` is not allocated and not used at all, in typical
case its address is the same as the local variable `etry` in the same
stack (compiler optimization), assigning to `evFileHelper.name[0]`
actually corrupts the etry data structure and crashes the program.

Here's an example output from gdb session.

```
(gdb) p &evFileHelper.name
$6 = (char (*)[]) 0x7ffff6dfc930
(gdb) p &etry
$7 = (wd_map_t **) 0x7ffff6dfc930
(gdb)
```
