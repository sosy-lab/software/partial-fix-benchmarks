diff --git a/core/autoload.rb b/core/autoload.rb
index a5b726bd72..7dea76b524 100644
--- a/core/autoload.rb
+++ b/core/autoload.rb
@@ -6,7 +6,9 @@ class Autoload
   attr_reader :scope
   attr_reader :path
   attr_reader :constant
-  attr_reader :thread
+
+  attr_accessor :loaded
+  attr_accessor :loading
 
   def self.allocate
     Rubinius.primitive :autoload_allocate
@@ -18,10 +20,16 @@ def initialize(name, scope, path)
     @scope = scope
     @path = path
     @constant = undefined
+    @loading = false
+    @loaded = false
   end
 
   private :initialize
 
+  def loading?
+    Rubinius.synchronize(self) { @loading }
+  end
+
   ##
   #
   # Change the file to autoload. Used by Module#autoload
@@ -33,29 +41,29 @@ def set_path(path)
   # When any code that finds a constant sees an instance of Autoload as its match,
   # it calls this method on us
   def call(under, honor_require=false)
-    # We leave the Autoload object in the constant table so that if another
-    # thread hits this while we're mid require they'll be come in here and
-    # be held by require until @path is available, at which time they'll
-    # attempt the lookup again.
-    #
-
-    if !undefined.equal?(constant) && Thread.current == thread
-      constant
-    else
-      worked = resolve
-
-      if !honor_require or worked
-        find_const under
-      end
+    return constant unless undefined.equal? constant
+
+    if resolve or not honor_require
+      find_const under
     end
   end
 
   def resolve
+    # The protocol that MRI defines for resolving an Autoload instance
+    # requires that the constant table entry be removed _during_ the load so
+    # that Module#const_defined? returns false and defined?() returns nil for
+    # the constant that triggered the load.
+
     Rubinius.synchronize(self) do
-      unless @loaded && @thread == Thread.current
-        @loaded = true
-        @thread = Thread.current
-        Rubinius::CodeLoader.new(@path).require
+      unless @loaded or @loading
+        @loading = true
+
+        result = Rubinius::CodeLoader.new(@path).require
+
+        @loaded = true if result
+        @loading = false
+
+        result
       end
     end
   end
diff --git a/core/module.rb b/core/module.rb
index 3fdcad89e8..6e483e93e3 100644
--- a/core/module.rb
+++ b/core/module.rb
@@ -90,10 +90,10 @@ def autoload(name, path)
       return
     end
 
-    autoload_contant = Autoload.new(name, self, path)
-    constant_table.store(name, autoload_contant, :public)
+    autoload_constant = Autoload.new(name, self, path)
+    constant_table.store(name, autoload_constant, :public)
     if self == Kernel
-      Object.singleton_class.constant_table.store(name, autoload_contant, :public)
+      Object.singleton_class.constant_table.store(name, autoload_constant, :public)
     end
 
     Rubinius.inc_global_serial
diff --git a/core/type.rb b/core/type.rb
index f1bfdf39f3..39d61740af 100644
--- a/core/type.rb
+++ b/core/type.rb
@@ -344,8 +344,15 @@ def self.const_get(mod, name, inherit=true, resolve=true)
       while current and object_kind_of? current, Module
         if bucket = current.constant_table.lookup(name)
           constant = bucket.constant
-          if resolve and object_kind_of? constant, Autoload
-            constant = constant.call(current)
+
+          if object_kind_of? constant, Autoload
+            if resolve
+              return constant.call(current)
+            elsif not constant.loading?
+              return constant
+            else
+              return undefined
+            end
           end
 
           return constant
@@ -361,8 +368,15 @@ def self.const_get(mod, name, inherit=true, resolve=true)
       if object_instance_of? mod, Module
         if bucket = Object.constant_table.lookup(name)
           constant = bucket.constant
-          if resolve and object_kind_of? constant, Autoload
-            constant = constant.call(current)
+
+          if object_kind_of? constant, Autoload
+            if resolve
+              return constant.call(current)
+            elsif not constant.loading?
+              return constant
+            else
+              return undefined
+            end
           end
 
           return constant
diff --git a/machine/class/autoload.hpp b/machine/class/autoload.hpp
index 4104f1f724..f582daaee1 100644
--- a/machine/class/autoload.hpp
+++ b/machine/class/autoload.hpp
@@ -21,6 +21,7 @@ namespace rubinius {
     attr_accessor(path, Object);
     attr_accessor(constant, Object);
     attr_accessor(thread, Thread);
+    attr_accessor(loading, Object);
     attr_accessor(loaded, Object);
 
     /**  Register class with the VM. */
@@ -31,6 +32,7 @@ namespace rubinius {
       obj->path(nil<Object>());
       obj->constant(nil<Object>());
       obj->thread(nil<Thread>());
+      obj->loading(nil<Object>());
       obj->loaded(nil<Object>());
     }
 
diff --git a/machine/class/system.cpp b/machine/class/system.cpp
index 0b0241c7bc..643aa54119 100644
--- a/machine/class/system.cpp
+++ b/machine/class/system.cpp
@@ -1,6 +1,7 @@
 #include "arguments.hpp"
 
 #include "class/array.hpp"
+#include "class/autoload.hpp"
 #include "class/bignum.hpp"
 #include "class/block_environment.hpp"
 #include "class/channel.hpp"
@@ -1453,8 +1454,16 @@ namespace rubinius {
 
     Object* res = Helpers::const_get(state, sym, &reason);
 
-    if(reason != vFound) {
-      return Primitives::failure();
+    if(reason == vFound) {
+      if(Autoload* autoload = try_as<Autoload>(res)) {
+        std::atomic_thread_fence(std::memory_order_seq_cst);
+
+        if(autoload->loading()->true_p()) {
+          res = Primitives::failure();
+        }
+      }
+    } else {
+      res = Primitives::failure();
     }
 
     return res;
@@ -1466,7 +1475,16 @@ namespace rubinius {
     ConstantMissingReason reason = vNonExistent;
 
     Object* res = Helpers::const_get_under(state, under, sym, &reason);
-    if(reason != vFound) {
+
+    if(reason == vFound) {
+      if(Autoload* autoload = try_as<Autoload>(res)) {
+        std::atomic_thread_fence(std::memory_order_seq_cst);
+
+        if(autoload->loading()->true_p()) {
+          res = Primitives::failure();
+        }
+      }
+    } else {
       if(send_const_missing->true_p()) {
         res = Helpers::const_missing_under(state, under, sym);
       } else {
diff --git a/spec/ruby/core/module/autoload_spec.rb b/spec/ruby/core/module/autoload_spec.rb
index d5f797deb8..b6d7aaaa80 100644
--- a/spec/ruby/core/module/autoload_spec.rb
+++ b/spec/ruby/core/module/autoload_spec.rb
@@ -369,12 +369,12 @@ class ModuleSpecs::Autoload::Z < ModuleSpecs::Autoload::ZZ
       t1.join
       t2.join
 
-      ScratchPad.recorded.should == [:con_pre, :con_post, :t1_post, :t2_post]
+      t2_exc.should be_nil
 
       t1_val.should == 1
       t2_val.should == t1_val
 
-      t2_exc.should be_nil
+      ScratchPad.recorded.should == [:con_pre, :con_post, :t1_post, :t2_post]
     end
   end
 
@@ -396,49 +396,26 @@ class ModuleSpecs::Autoload::Z < ModuleSpecs::Autoload::ZZ
 
   end
 
-  describe "(concurrently)" do
-    it "blocks others threads while doing an autoload" do
-      file_path     = fixture(__FILE__, "repeated_concurrent_autoload.rb")
-      autoload_path = file_path.sub(/\.rb\Z/, '')
-      mod_count     = 30
-      thread_count  = 16
-
-      mod_names = []
-      mod_count.times do |i|
-        mod_name = :"Mod#{i}"
-        autoload mod_name, autoload_path
-        mod_names << mod_name
-      end
+  it "causes 'defined?(A)' and 'defined?(A.m)' to return nil for A when loading the file for that constant" do
+    ScratchPad.record []
 
-      barrier = ModuleSpecs::CyclicBarrier.new thread_count
-      ScratchPad.record ModuleSpecs::ThreadSafeCounter.new
-
-      threads = (1..thread_count).map do
-        Thread.new do
-          mod_names.each do |mod_name|
-            break false unless barrier.enabled?
-
-            was_last_one_in = barrier.await # wait for all threads to finish the iteration
-            # clean up so we can autoload the same file again
-            $LOADED_FEATURES.delete(file_path) if was_last_one_in && $LOADED_FEATURES.include?(file_path)
-            barrier.await # get ready for race
-
-            begin
-              Object.const_get(mod_name).foo
-            rescue NoMethodError
-              barrier.disable!
-              break false
-            end
-          end
-        end
-      end
+    ModuleSpecs::Autoload.autoload(:ForDefined,
+                                   fixture(__FILE__, "autoload_defined.rb"))
+    ModuleSpecs::Autoload.autoload(:DefinedByForDefined,
+                                   fixture(__FILE__, "autoload_defined.rb"))
 
-      # check that no thread got a NoMethodError because of partially loaded module
-      threads.all? {|t| t.value}.should be_true
+    ModuleSpecs::Autoload::ForDefined.should == 1
+    ScratchPad.recorded.should == [nil, nil]
+  end
 
-      # check that the autoloaded file was evaled exactly once
-      ScratchPad.recorded.get.should == mod_count
-    end
+  it "causes 'SomeModule.const_defined?(A)' to return false when loading the file for that constant" do
+    ModuleSpecs::Autoload.autoload(:ForConstDefined,
+                                   fixture(__FILE__, "autoload_const_defined.rb"))
+    ModuleSpecs::Autoload.autoload(:DefinedByForConstDefined,
+                                   fixture(__FILE__, "autoload_const_defined.rb"))
+
+    ModuleSpecs::Autoload::ForConstDefined.should == 1
+    ScratchPad.recorded.should be_false
   end
 
   it "loads the registered constant even if the constant was already loaded by another thread" do
diff --git a/spec/ruby/core/module/fixtures/autoload_const_defined.rb b/spec/ruby/core/module/fixtures/autoload_const_defined.rb
new file mode 100644
index 0000000000..e8733b4e52
--- /dev/null
+++ b/spec/ruby/core/module/fixtures/autoload_const_defined.rb
@@ -0,0 +1,9 @@
+ScratchPad.record ModuleSpecs::Autoload.const_defined?(:ForConstDefined)
+
+module ModuleSpecs
+  module Autoload
+    ForConstDefined = 1
+
+    module DefinedByForConstDefined; end
+  end
+end
diff --git a/spec/ruby/core/module/fixtures/autoload_defined.rb b/spec/ruby/core/module/fixtures/autoload_defined.rb
new file mode 100644
index 0000000000..32bb749e61
--- /dev/null
+++ b/spec/ruby/core/module/fixtures/autoload_defined.rb
@@ -0,0 +1,10 @@
+ScratchPad << defined?(ModuleSpecs::Autoload::ForDefined)
+ScratchPad << defined?(ModuleSpecs::Autoload::ForDefined.size)
+
+module ModuleSpecs
+  module Autoload
+    ForDefined = 1
+
+    module DefinedByForDefined; end
+  end
+end
diff --git a/spec/ruby/core/module/fixtures/classes.rb b/spec/ruby/core/module/fixtures/classes.rb
index 69ceaed14f..5aca3951e8 100644
--- a/spec/ruby/core/module/fixtures/classes.rb
+++ b/spec/ruby/core/module/fixtures/classes.rb
@@ -432,59 +432,6 @@ def extend_object(obj)
     end
   end
 
-  class CyclicBarrier
-    def initialize(count = 1)
-      @count = count
-      @state = 0
-      @mutex = Mutex.new
-      @cond  = ConditionVariable.new
-    end
-
-    def await
-      @mutex.synchronize do
-        @state += 1
-        if @state >= @count
-          @state = 0
-          @cond.broadcast
-          true
-        else
-          @cond.wait @mutex
-          false
-        end
-      end
-    end
-
-    def enabled?
-      @mutex.synchronize { @count != -1 }
-    end
-
-    def disable!
-      @mutex.synchronize do
-        @count = -1
-        @cond.broadcast
-      end
-    end
-  end
-
-  class ThreadSafeCounter
-    def initialize(value = 0)
-      @value = 0
-      @mutex = Mutex.new
-    end
-
-    def get
-      @mutex.synchronize { @value }
-    end
-
-    def increment_and_get
-      @mutex.synchronize do
-        prev_value = @value
-        @value += 1
-        prev_value
-      end
-    end
-  end
-
   module ShadowingOuter
     module M
       SHADOW = 123
diff --git a/spec/ruby/core/module/fixtures/repeated_concurrent_autoload.rb b/spec/ruby/core/module/fixtures/repeated_concurrent_autoload.rb
deleted file mode 100644
index 32b770e6cf..0000000000
--- a/spec/ruby/core/module/fixtures/repeated_concurrent_autoload.rb
+++ /dev/null
@@ -1,8 +0,0 @@
-prev_value = ScratchPad.recorded.increment_and_get
-eval <<-RUBY_EVAL
-  module Mod#{prev_value}
-    sleep(0.05)
-    def self.foo
-    end
-  end
-RUBY_EVAL
diff --git a/spec/tags/ruby/core/module/autoload_tags.txt b/spec/tags/ruby/core/module/autoload_tags.txt
deleted file mode 100644
index acc980da51..0000000000
--- a/spec/tags/ruby/core/module/autoload_tags.txt
+++ /dev/null
@@ -1 +0,0 @@
-fails:Module#autoload loads the registered constant even if the constant was already loaded by another thread
