BUG: firmware error 6550
maybe fixed now in 20180722?
https://rusefi.com/forum/viewtopic.php?f=15&t=1031&e=1&view=unread#p29655 

CUSTOM_ERR_6591 was reported

also sounds like EMI noise was triggering input pins 
looks better now!!!
This bug really needs a unit test - reopening ticket
![image](https://user-images.githubusercontent.com/39065352/43359124-fa3df65e-92a5-11e8-822a-eb88c9096cfe.png)

![image](https://user-images.githubusercontent.com/39065352/43359367-d97f5a30-92a9-11e8-917a-28203f73db28.png)

So the issue is about ownIndex field containing ununitialized value while we are adding additional cylinder while engine is running.
CUSTOM_ERR_6687 reported :(
this is fixed, right?