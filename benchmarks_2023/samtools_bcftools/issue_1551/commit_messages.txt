Update documentation to prevent confusion, see #1538
Inframe indels can introduce a stop codon and should be marked as such

Resolves #1551
Check if stop codon occurs before frameshift is restored,

in such case it is not a real inframe variant.

See also #1551
