A single dot is a valid Number=G,Type=String field - missing value
Make consensus work with insertions immediately following deletions

and don't let the second overwrite the first.

Fixes #888
Make consensus skip duplicate insertions. Fixes #888
