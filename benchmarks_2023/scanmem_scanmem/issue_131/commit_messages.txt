handlers: watch: Respect the scan data type

Currently, the watch handler doesn't respect the scan data type and
this is why e.g. watching floats results in watching integers
instead. Also watching strings and bytearrays is allowed but doesn't
make any sense as also integers are displayed.

So prevent watching bytearray or string and set the flags of the
first value read from memory to the match info flags.

Fixes: GitHub issue #136
scanmem: Process async-signal-unsafe functions in a child

The sighandler() uses async-signal-unsafe functions which is a
security risk. These functions have to be moved out of the signal
handling context. One way to solve this is an event handler process
based on a pipe, read()/write() and select() but that is too complex
just for showing which signal killed scanmem.

Rather fork a child from the signal handler, call these functions
there, exit the child and wait in the parent for the exit of the
child. Only use async-signal-safe functions in the parent.

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers
scanmem: Call async-signal-unsafe functions from a child

The sighandler() uses async-signal-unsafe functions which is a
security risk. These functions have to be moved out of the signal
handling context.

So fork a child from the signal handler, call these functions there,
exit the child and wait in the parent for that. Use only
async-signal-safe functions in the parent. Use _exit() instead
of exit() in the parent as nothing calls atexit(). This also detaches
from the tracee. So drop calling detach(). Note: ptrace() is not
async-signal-safe.

Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
scanmem: Call async-signal-unsafe functions from a child

The sighandler() uses async-signal-unsafe functions which is a
security risk. These functions have to be moved out of the signal
handling context.

So use the self-pipe trick to solve this. This means: Create two
non-blocking pipes before forking an event handler process which
ignores the signals the sighandler() is registered to. Write the
signal number to the pipe from the sighandler() to the event handler,
let it print the message which signal killed scanmem and let it send
a confirmation through the pipe back to the signal handler and exit.
Exit scanmem with _exit() from the sighandler() then to achieve
async-signal-safety. This also detaches scanmem from the target
process. Use select() to poll for input with a timeout of 1 second.
Also implement an orphan detection for the event handler and exit
it if the parent exits.

Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
scanmem: Call async-signal-unsafe functions from a child

The sighandler() uses async-signal-unsafe functions which is a
security risk. These functions have to be moved out of the signal
handling context.

So use a child process to solve this. This means: Create two
non-blocking pipes before forking an event handler process which
ignores the signals the sighandler() is registered to. Write the
signal number to the pipe from the sighandler() to the event handler,
let it print the message which signal killed scanmem and let it send
a confirmation through the other pipe back to the signal handler and
exit. Exit scanmem with _exit() from the sighandler() then to achieve
async-signal-safety. This also detaches scanmem from the target
process. Use select() to poll for input with a timeout of 1 second.
Also implement an orphan detection for the event handler and exit
there if the parent exits.

Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
scanmem: Use only async-signal-safe functions in sighandler()

The sighandler() uses async-signal-unsafe functions which is a
security risk. Moving these functions out of the signal handling
context is too complex. So rather replace them with async-signal-
safe ones.

Replace the exit() call with _exit() and drop detaching from the
target process as that is done in _exit() as well. Also replace the
error message output with calls to the write() function and do
manual int to str conversion for the signal number.

Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
scanmem: Use only async-signal-safe functions in sighandler()

The sighandler() uses async-signal-unsafe functions which is a
security risk. Moving these functions out of the signal handling
context is too complex. So rather replace them with async-signal-
safe ones.

Replace the exit() call with _exit() and drop detaching from the
target process as that is done in _exit() as well. Also replace the
error message output with calls to the write() function and do
manual int to str conversion for the signal number.

Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
scanmem: Use only async-signal-safe functions in sighandler()

The sighandler() uses async-signal-unsafe functions which is a
security risk. Moving these functions out of the signal handling
context is too complex. So rather replace them with async-signal-
safe ones.

Replace the exit() call with _exit() and drop detaching from the
target process as that is done in _exit() as well. Also replace the
error message output with calls to the write() function and do
manual int to str conversion for the signal number.

Reference: https://www.securecoding.cert.org/confluence/display/c/
SIG30-C.+Call+only+asynchronous-safe+functions+within+signal+handlers

Fixes: GitHub issue #131
Reported-by: Markus Elfring (@elfring)
