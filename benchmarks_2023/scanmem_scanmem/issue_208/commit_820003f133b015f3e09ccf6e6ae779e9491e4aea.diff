diff --git a/handlers.c b/handlers.c
index 94950452..e9a92997 100644
--- a/handlers.c
+++ b/handlers.c
@@ -1051,7 +1051,7 @@ bool handler__update(globals_t *vars, char **argv, unsigned argc)
 
     USEPARAMS();
     if (vars->matches) {
-        if (sm_checkmatches(vars, MATCHANY, NULL) == false) {
+        if (sm_checkmatches(vars, MATCHUPDATE, NULL) == false) {
             show_error("failed to scan target address space.\n");
             return false;
         }
diff --git a/scanroutines.c b/scanroutines.c
index 12d603c0..572e15f2 100644
--- a/scanroutines.c
+++ b/scanroutines.c
@@ -25,6 +25,7 @@
 #include <assert.h>
 
 #include "scanroutines.h"
+#include "common.h"
 #include "endianness.h"
 #include "value.h"
 
@@ -36,6 +37,7 @@ unsigned int (*sm_scan_routine) SCAN_ROUTINE_ARGUMENTS;
 #define MEMORY_COMP(value,field,op) ((value)->flags.field && (get_##field(memory_ptr) op get_##field(value)))
 #define SET_FLAG(f, field)          ((f)->field = 1)
 
+
 /********************/
 /* Integer specific */
 /********************/
@@ -60,6 +62,22 @@ DEFINE_INTEGER_MATCHANY_ROUTINE(32)
 DEFINE_INTEGER_MATCHANY_ROUTINE(64)
 
 
+#define DEFINE_INTEGER_MATCHUPDATE_ROUTINE(DATAWIDTH) \
+    extern inline unsigned int scan_routine_INTEGER##DATAWIDTH##_UPDATE SCAN_ROUTINE_ARGUMENTS \
+    { \
+        if (memlength < (DATAWIDTH)/8) return 0; \
+        int ret = 0; \
+        if (old_value->flags.s##DATAWIDTH##b) { ret = (DATAWIDTH)/8; SET_FLAG(saveflags, s##DATAWIDTH##b); } \
+        if (old_value->flags.u##DATAWIDTH##b) { ret = (DATAWIDTH)/8; SET_FLAG(saveflags, u##DATAWIDTH##b); } \
+        return ret; \
+    }
+
+DEFINE_INTEGER_MATCHUPDATE_ROUTINE( 8)
+DEFINE_INTEGER_MATCHUPDATE_ROUTINE(16)
+DEFINE_INTEGER_MATCHUPDATE_ROUTINE(32)
+DEFINE_INTEGER_MATCHUPDATE_ROUTINE(64)
+
+
 #define DEFINE_INTEGER_ROUTINE(DATAWIDTH, MATCHTYPENAME, MATCHTYPE, VALUE_TO_COMPARE_WITH, REVENDIAN, REVEND_STR) \
     extern inline unsigned int scan_routine_INTEGER##DATAWIDTH##_##MATCHTYPENAME##REVEND_STR SCAN_ROUTINE_ARGUMENTS \
     { \
@@ -126,6 +144,20 @@ DEFINE_INTEGER_ROUTINE_FOR_ALL_INTEGER_TYPES(DECREASED, <, old_value)
 DEFINE_FLOAT_MATCHANY_ROUTINE(32)
 DEFINE_FLOAT_MATCHANY_ROUTINE(64)
 
+
+#define DEFINE_FLOAT_MATCHUPDATE_ROUTINE(DATAWIDTH) \
+    extern inline unsigned int scan_routine_FLOAT##DATAWIDTH##_UPDATE SCAN_ROUTINE_ARGUMENTS \
+    { \
+        if (memlength < (DATAWIDTH)/8) return 0; \
+        int ret = 0; \
+        if (old_value->flags.f##DATAWIDTH##b) { ret = (DATAWIDTH)/8; SET_FLAG(saveflags, f##DATAWIDTH##b); } \
+        return ret; \
+    }
+
+DEFINE_FLOAT_MATCHUPDATE_ROUTINE(32)
+DEFINE_FLOAT_MATCHUPDATE_ROUTINE(64)
+
+
 #define DEFINE_FLOAT_ROUTINE(DATAWIDTH, MATCHTYPENAME, MATCHTYPE, VALUE_TO_COMPARE_WITH, REVENDIAN, REVEND_STR) \
     extern inline unsigned int scan_routine_FLOAT##DATAWIDTH##_##MATCHTYPENAME##REVEND_STR SCAN_ROUTINE_ARGUMENTS \
     { \
@@ -295,6 +327,7 @@ DEFINE_FLOAT_RANGE_ROUTINE(64, 1, _REVENDIAN)
     } \
 
 DEFINE_ANYTYPE_ROUTINE(ANY, )
+DEFINE_ANYTYPE_ROUTINE(UPDATE, )
 
 DEFINE_ANYTYPE_ROUTINE(EQUALTO, )
 DEFINE_ANYTYPE_ROUTINE(NOTEQUALTO, )
@@ -317,9 +350,16 @@ DEFINE_ANYTYPE_ROUTINE(RANGE, _REVENDIAN)
 /*----------------------------------------*/
 /* for generic VLT (Variable Length Type) */
 /*----------------------------------------*/
+
 extern inline unsigned int scan_routine_VLT_ANY SCAN_ROUTINE_ARGUMENTS
 {
-   return saveflags->length = ((old_value)->flags.length);
+   return saveflags->length = MIN(memlength, (uint16_t)(-1));
+}
+
+extern inline unsigned int scan_routine_VLT_UPDATE SCAN_ROUTINE_ARGUMENTS
+{
+    /* memlength here is already MIN(memlength, old_value->flags.length) */
+   return saveflags->length = memlength;
 }
 
 /*---------------*/
@@ -591,6 +631,7 @@ DEFINE_STRING_SMALLOOP_EQUALTO_ROUTINE(56)
 scan_routine_t sm_get_scanroutine(scan_data_type_t dt, scan_match_type_t mt, const match_flags* uflags, bool reverse_endianness)
 {
     CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES(MATCHANY, ANY)
+    CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES(MATCHUPDATE, UPDATE)
     CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES_AND_ENDIANS(MATCHEQUALTO, EQUALTO)
     CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES_AND_ENDIANS(MATCHNOTEQUALTO, NOTEQUALTO)
     CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES_AND_ENDIANS(MATCHGREATERTHAN, GREATERTHAN)
@@ -603,10 +644,14 @@ scan_routine_t sm_get_scanroutine(scan_data_type_t dt, scan_match_type_t mt, con
     CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES(MATCHDECREASEDBY, DECREASEDBY)
     CHOOSE_ROUTINE_FOR_ALL_NUMBER_TYPES_AND_ENDIANS(MATCHRANGE, RANGE)
 
+    CHOOSE_ROUTINE(BYTEARRAY, VLT, MATCHANY, ANY, 0, )
+    CHOOSE_ROUTINE(BYTEARRAY, VLT, MATCHUPDATE, UPDATE, 0, )
     if (uflags) {
         CHOOSE_ROUTINE_VLT(BYTEARRAY, BYTEARRAY, MATCHEQUALTO, EQUALTO, uflags->length*8)
     }
 
+    CHOOSE_ROUTINE(STRING, VLT, MATCHANY, ANY, 0, )
+    CHOOSE_ROUTINE(STRING, VLT, MATCHUPDATE, UPDATE, 0, )
     if (uflags) {
         CHOOSE_ROUTINE_VLT(STRING, STRING, MATCHEQUALTO, EQUALTO, uflags->length*8)
     }
diff --git a/scanroutines.h b/scanroutines.h
index 90c9d866..75c5f482 100644
--- a/scanroutines.h
+++ b/scanroutines.h
@@ -42,14 +42,15 @@ typedef enum {
 } scan_data_type_t;
 
 typedef enum {
-    MATCHANY,                 /* to update */
+    MATCHANY,                /* for snapshot */
     /* following: compare with a given value */
     MATCHEQUALTO,
-    MATCHNOTEQUALTO,          
+    MATCHNOTEQUALTO,
     MATCHGREATERTHAN,
     MATCHLESSTHAN,
     MATCHRANGE,
     /* following: compare with the old value */
+    MATCHUPDATE,
     MATCHNOTCHANGED,
     MATCHCHANGED,
     MATCHINCREASED,
diff --git a/targetmem.h b/targetmem.h
index 492aae9a..db11f01e 100644
--- a/targetmem.h
+++ b/targetmem.h
@@ -248,20 +248,26 @@ add_element (matches_and_old_values_array **array,
    if more bytes are needed (e.g. bytearray),
    read them separately (for performance) */
 static inline value_t
-data_to_val_aux (matches_and_old_values_swath *swath,
+data_to_val_aux (const matches_and_old_values_swath *swath,
                  size_t index, size_t swath_length)
 {
     uint i;
     value_t val;
     size_t max_bytes = swath_length - index;
 
-    zero_value(&val);
+    /* Init all possible flags in a single go.
+     * Also init length to the maximum possible value */
+    val.flags.all_flags = 0xffffu;
 
-    if (max_bytes >  8) max_bytes = 8;
-    if (max_bytes >= 8) val.flags.u64b = val.flags.s64b = val.flags.f64b = 1;
-    if (max_bytes >= 4) val.flags.u32b = val.flags.s32b = val.flags.f32b = 1;
-    if (max_bytes >= 2) val.flags.u16b = val.flags.s16b                  = 1;
-    if (max_bytes >= 1) val.flags.u8b  = val.flags.s8b                   = 1;
+    /* NOTE: This does the right thing for VLT because the high flags are
+     * not in the least significant bits in the `length` union member,
+     * otherwise their zeroing would interfere with useful bits of `length`.
+     * This works on both endians, as the high flags are in the middle. */
+    if (max_bytes > 8) max_bytes = 8;
+    if (max_bytes < 8) val.flags.u64b = val.flags.s64b = val.flags.f64b = 0;
+    if (max_bytes < 4) val.flags.u32b = val.flags.s32b = val.flags.f32b = 0;
+    if (max_bytes < 2) val.flags.u16b = val.flags.s16b                  = 0;
+    if (max_bytes < 1) val.flags.u8b  = val.flags.s8b                   = 0;
 
     for (i = 0; i < max_bytes; ++i) {
         /* Both uint8_t, no explicit casting needed */
@@ -272,7 +278,7 @@ data_to_val_aux (matches_and_old_values_swath *swath,
 }
 
 static inline value_t
-data_to_val (matches_and_old_values_swath *swath, size_t index)
+data_to_val (const matches_and_old_values_swath *swath, size_t index)
 {
     return data_to_val_aux(swath, index, swath->number_of_bytes);
 }
