`update` and non-numeric types
This is just not supported. So queuing for `v0.17`.

I classed it as a 'bug' because you LOSE the previous matches.
If it just refused to work it'd be fine as 'enhancement'.

Fixed in 12345ieee@7afccdc


Memo for me, fix this one better.