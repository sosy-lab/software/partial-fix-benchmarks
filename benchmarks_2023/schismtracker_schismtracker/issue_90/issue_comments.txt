Specific MOD module sounds weird
Actually, the latest build of Schism segfaults for me when I try to open that module (IT and Milky have no problem).
When l try to play this song, l also get a segfault crash, valgrind tells that:
https://pastebin.com/raw/NaihQrkh
I'm not sure that perfect MOD playback is a goal of Schism Tracker though, just as XM and 669 and so on are supported for import but no guarantees beyond that. If IT itself plays it back "correctly" then it's definitely worth looking into, but unfortunately I can't run IT in DOSBox at full speed, so I can't really tell…
@Abc120, what version of Schism Tracker are you using? That info might help us figure out where the segfault bug was introduced.
@Abc120 https://github.com/schismtracker/schismtracker/issues/91 fixes the crash for me, if you're up for testing the patch or really want to play that mod in schism asap feel free to build and kick the tires!
Thanks for your quick replies! The version I'm using is 20170420, and it doesn't crash, it simply plays this specific module incorrectly. I didn't build from sources, just downloaded binaries from the main page. And yes, I know playing MODs is not a top priority task, but it's still a bug since the tracker supports such a format, and supports it perfectly for all other modules I tried. The problem is just this specific module. It would be really nice to have it fixed)
Oh, BTW my system is Windows 10 x64, in case it matters.
Oh, well, I'm not the ideal person for correcting playback issues.  Maybe one of the more experienced schism devs can take a look at that.  I was under the impression the main problem was segfaulting on load, as the issue title indicated, which is indeed an existing obvious bug with a trivial fix.
Here's how it sounds to me with schismtracker and openmpt:
![files zip](https://user-images.githubusercontent.com/3192173/27259595-b2887fa6-5416-11e7-8577-198ab4726ff0.png)
When saving the file as .it and playing it with openmpt123, it still sounds strange.
I've just converted the module to IT using OpenMPT 1.26.11.00 with compatibility export. It sounds as expected in Schism. Seems like the bug is in MOD-specific part.
![folk_n_storm_it zip](https://user-images.githubusercontent.com/29509021/27260049-7207f32c-5433-11e7-9cd8-44cc24c66495.png)

Sorry for the confusion—I should have opened a new issue instead of changing the title of this one to address the more serious segfault issue.

It sounds like the MOD import might be faulty more than the playback itself, which is certainly within the realm of "things that shouldn't happen". Going to take a look at the playback/import issue and how OpenMPT handles it…
OK, the samples are loaded all wrong, so strictly speaking the `csf_adjust_sample_loop` check wasn't necessary (if we fix the sample loading (although it's fine to have anyway)).
Can we hope it will be fixed sometime? I suppose it'll be much harder than simply editing an IF clause...
I also wonder why this specific module samples are being loaded wrong, while hundreds of other MODs sound fine...
(The issue is that the samples are ADPCM-compressed, which Schism Tracker does not check for. The MOD format is nebulous. Working on a fix.)
Loading / playback should be fixed on that branch; anyone want to verify?
I'd like to verify but unfortunately I don't have any IDE installed right now. If you can, please, upload Windows binaries, and I'll give it a try.
How about a render from the updated build?

[folknstorm_mp3.zip](https://github.com/schismtracker/schismtracker/files/1083156/folknstorm_mp3.zip)

Well, it sounds as expected) Awesome work! When will the next stable release be ready, BTW?
I really want to get some sort of build automation going (beyond the CI that's already happening) so that we can always have a "latest" build, with the assumption that code committed to the master branch is always as tested and "stable" as it's going to be. This would also include OS X, for which we currently don't have *any* builds…

I think that issue (or three, I suppose: #40, #42, and #76) is the next thing I want to tackle, so the next release will probably be as soon as I make that happen.
> The MOD format is nebulous.

Just for the record, ADPCM compression is a ModPlug addition and not very widely supported (it really shouldn't be). If you find a module with ADPCM-compressed samples, you should throw it in the trash as quickly as possible and look for the uncompressed original file instead.