Support kernel-timestamping of received packet, fixes #46
simplify restrictions, fixes #29, #32
discard late packets, auto-adjust timeout for -c/-C/-l, fixes #32
reword a bit help text for #32
fix wording, #32
