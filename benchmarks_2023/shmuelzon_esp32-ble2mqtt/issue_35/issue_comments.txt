TLS support
Hey,

> How feasible would it be to add TLS support for the connection to the MQTT server?

Enabling SSL is just a matter of configuration and could be achieved by applying the following changes:
```patch
diff --git a/main/mqtt.c b/main/mqtt.c
--- a/main/mqtt.c
+++ b/main/mqtt.c
@@ -260,6 +260,7 @@ int mqtt_connect(const char *host, uint16_t port, const char *client_id,
         .client_id = client_id,
         .username = username,
         .password = password,
+        .transport = MQTT_TRANSPORT_OVER_SSL,
     };
 
     ESP_LOGI(TAG, "Connecting MQTT client");
```

Doing it properly would include adding this option to the configuration file and also adding support for loading certificates. The later would take a bit more time since it would also require changing OTA updates to include the entire flash file system and not just the configuration file.
I'll try to get around to implementing this in the near future...

> Is it likely to grow the image size and RAM footprint beyond what an ESP32 can cope with?

I don't think there will be a big increase since all the TLS support is already a part of the image.

Good day!
I've pushed initial support of SSL, without supporting certificates yet.
I'll keep this issue open until that is ready as well.