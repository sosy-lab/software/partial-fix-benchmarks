sse2: fix _mm_bslli_si128 AltiVec implementation on XL C/C++

Even if the code can never be called, on XL C/C++ the third argument
to vec_sld must be in [0, 15].  The fix is a bit silly, but since
we're dealing with compile-time constants it shouldn't be a performance
issue.
sse2: use vector long long instead of float for __m128i on AltiVec

Fixes #396
sse2: use vector long long instead of float for __m128i on AltiVec

Fixes #396
sse2: use vector long long instead of float for __m128i on AltiVec

Fixes #396
mm_slli_epi32 + ppc64el + imm8 > 15

Fixes #396
mm_{srli,slli}_epi32 + altivec + imm8 > 15

Fixes #396
_mm_{srli,slli}_epi32 + altivec + imm8 > 15

Fixes #396
_mm_{srli,slli}_epi32 + altivec + imm8 > 15

Fixes #396
