missing DTLS fingerprint attribute in 200 OK
More testing revealed that rtpengine does include fingerprint in answer sdp if I turn off ice in callee (baresip).  Turning off ice removes these two lines from the sdp of 200 ok:

```
        Session Attribute (a): ice-ufrag:lx1L
        Session Attribute (a): ice-pwd:izSJlDbsmSVaJf2UUDJtXa
```

Otherwise the sdps are identical.   The whole sdp including the above two lines is below.

-- Juha

```
Message Body
    Session Description Protocol
        Session Description Protocol Version (v): 0
        Owner/Creator, Session Id (o): - 2871010381 1546336202 IN IP4 192.168.43.146
        Session Name (s): -
        Connection Information (c): IN IP4 192.168.43.146
        Time Description, active time (t): 0 0
        Session Attribute (a): tool:baresip 0.4.12
        Session Attribute (a): ice-ufrag:lx1L
        Session Attribute (a): ice-pwd:izSJlDbsmSVaJf2UUDJtXa
        Media Description, name and address (m): audio 10194 RTP/AVP 111 0 8 126
        Bandwidth Information (b): AS:128
        Media Attribute (a): rtpmap:111 opus/48000/2
        Media Attribute (a): fmtp:111 stereo=1;sprop-stereo=1
        Media Attribute (a): rtpmap:0 PCMU/8000
        Media Attribute (a): rtpmap:8 PCMA/8000
        Media Attribute (a): rtpmap:126 telephone-event/8000
        Media Attribute (a): fmtp:126 0-15
        Media Attribute (a): sendrecv
        Media Attribute (a): label:1
        Media Attribute (a): rtcp-rsize
        Media Attribute (a): ssrc:2637103868 cname:sip:test@test.tutpro.com
        Media Attribute (a): ptime:20
```

Can you post the full log please, in log level 7

Below is debug level 7 output.  -- Juha

---

May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Dump for 'offer' from 127.0.0.1:56346: { "sdp": "v=0#015#012o=- 2707663212741088063 2 IN IP4 127.0.0.1#015#012s=-#015#012t=0 0#015#012a=group:BUNDLE audio video#015#012a=msid-semantic: WMS uFFqMxWfRaQvSi5zo00U0ObxfUSsVwx5QPVm#015#012m=audio 36126 RTP/SAVPF 111 103 104 9 0 8 106 105 13 126#015#012c=IN IP4 192.168.43.146#015#012a=rtcp:39888 IN IP4 192.168.43.146#015#012a=candidate:1603072777 1 udp 2122260223 192.168.43.146 36126 typ host generation 0#015#012a=candidate:1603072777 2 udp 2122260222 192.168.43.146 39888 typ host generation 0#015#012a=candidate:2861 ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... 99801 1 tcp 1518280447 192.168.43.146 0 typ host tcptype active generation 0#015#012a=candidate:286199801 2 tcp 1518280446 192.168.43.146 0 typ host tcptype active generation 0#015#012a=ice-ufrag:ofcsykXOACI3xsng#015#012a=ice-pwd:qeRP0jwLOLTTsqn4QoD9Vrtn#015#012a=fingerprint:sha-256 D3:4E:A0:CA:6C:72:FD:7F:90:31:09:DE:14:F6:C2:F6:66:54:E4:77:9B:E3:6A:08:51:46:C2:55:54:89:3D:68#015#012a=setup:actpass#015#012a=mid:audio#015#012a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level#015#012a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/a ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... bs-send-time#015#012a=sendrecv#015#012a=rtcp-mux#015#012a=rtpmap:111 opus/48000/2#015#012a=fmtp:111 minptime=10; useinbandfec=1#015#012a=rtpmap:103 ISAC/16000#015#012a=rtpmap:104 ISAC/32000#015#012a=rtpmap:9 G722/8000#015#012a=rtpmap:0 PCMU/8000#015#012a=rtpmap:8 PCMA/8000#015#012a=rtpmap:106 CN/32000#015#012a=rtpmap:105 CN/16000#015#012a=rtpmap:13 CN/8000#015#012a=rtpmap:126 telephone-event/8000#015#012a=maxptime:60#015#012a=ssrc:1030961732 cname:cRFKRmNqLeTjRJ6z#015#012a=ssrc:1030961732 msid:uFFqMxWfRaQvSi5zo00U0ObxfUSsVwx5QPVm 08f52df4-1237-4d53-8357-b466351a2bc4#015#012a=ssrc:1030961732 msla ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... bel:uFFqMxWfRaQvSi5zo00U0ObxfUSsVwx5QPVm#015#012a=ssrc:1030961732 label:08f52df4-1237-4d53-8357-b466351a2bc4#015#012m=video 52284 RTP/SAVPF 100 116 117 96#015#012c=IN IP4 192.168.43.146#015#012a=rtcp:59797 IN IP4 192.168.43.146#015#012a=candidate:1603072777 1 udp 2122260223 192.168.43.146 52284 typ host generation 0#015#012a=candidate:1603072777 2 udp 2122260222 192.168.43.146 59797 typ host generation 0#015#012a=candidate:286199801 1 tcp 1518280447 192.168.43.146 0 typ host tcptype active generation 0#015#012a=candidate:286199801 2 tcp 15182 ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... 80446 192.168.43.146 0 typ host tcptype active generation 0#015#012a=ice-ufrag:ofcsykXOACI3xsng#015#012a=ice-pwd:qeRP0jwLOLTTsqn4QoD9Vrtn#015#012a=fingerprint:sha-256 D3:4E:A0:CA:6C:72:FD:7F:90:31:09:DE:14:F6:C2:F6:66:54:E4:77:9B:E3:6A:08:51:46:C2:55:54:89:3D:68#015#012a=setup:actpass#015#012a=mid:video#015#012a=extmap:2 urn:ietf:params:rtp-hdrext:toffset#015#012a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time#015#012a=extmap:4 urn:3gpp:video-orientation#015#012a=recvonly#015#012a=rtcp-mux#015#012a=rtpmap:100 VP8/90000#015#012a=rtcp-fb:100 ccm f ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... ir#015#012a=rtcp-fb:100 nack#015#012a=rtcp-fb:100 nack pli#015#012a=rtcp-fb:100 goog-remb#015#012a=rtpmap:116 red/90000#015#012a=rtpmap:117 ulpfec/90000#015#012a=rtpmap:96 rtx/90000#015#012a=fmtp:96 apt=100#015#012", "ICE": "force", "replace": [ "session-connection", "origin" ], "transport-protocol": "RTP/AVP", "rtcp-mux": [ "demux" ], "call-id": "4uk15dj6308kgn8su51k", "via-branch": "z9hG4bK708146", "received-from": [ "IP4", "<rtpe ip>" ], "from-tag": "e1ohs4jm69", "command": "offer" }
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Replying to 'offer' from 127.0.0.1:56346
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Response dump for 'offer' to 127.0.0.1:56346: { "sdp": "v=0#015#012o=- 2707663212741088063 2 IN IP4 <rtpe ip>#015#012s=-#015#012t=0 0#015#012a=group:BUNDLE audio video#015#012a=msid-semantic: WMS uFFqMxWfRaQvSi5zo00U0ObxfUSsVwx5QPVm#015#012m=audio 8034 RTP/AVP 111 103 104 9 0 8 106 105 13 126#015#012c=IN IP4 <rtpe ip>#015#012a=candidate:1603072777 1 udp 2122260223 192.168.43.146 36126 typ host generation 0#015#012a=candidate:1603072777 2 udp 2122260222 192.168.43.146 39888 typ host generation 0#015#012a=candidate:286199801 1 tcp 1518280447 192.16 ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... 8.43.146 0 typ host tcptype active generation 0#015#012a=candidate:286199801 2 tcp 1518280446 192.168.43.146 0 typ host tcptype active generation 0#015#012a=ice-ufrag:ofcsykXOACI3xsng#015#012a=ice-pwd:qeRP0jwLOLTTsqn4QoD9Vrtn#015#012a=fingerprint:sha-256 D3:4E:A0:CA:6C:72:FD:7F:90:31:09:DE:14:F6:C2:F6:66:54:E4:77:9B:E3:6A:08:51:46:C2:55:54:89:3D:68#015#012a=setup:actpass#015#012a=mid:audio#015#012a=extmap:1 urn:ietf:params:rtp-hdrext:ssrc-audio-level#015#012a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time#015#012a=rtpmap:111 op ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... us/48000/2#015#012a=fmtp:111 minptime=10; useinbandfec=1#015#012a=rtpmap:103 ISAC/16000#015#012a=rtpmap:104 ISAC/32000#015#012a=rtpmap:9 G722/8000#015#012a=rtpmap:0 PCMU/8000#015#012a=rtpmap:8 PCMA/8000#015#012a=rtpmap:106 CN/32000#015#012a=rtpmap:105 CN/16000#015#012a=rtpmap:13 CN/8000#015#012a=rtpmap:126 telephone-event/8000#015#012a=maxptime:60#015#012a=ssrc:1030961732 cname:cRFKRmNqLeTjRJ6z#015#012a=ssrc:1030961732 msid:uFFqMxWfRaQvSi5zo00U0ObxfUSsVwx5QPVm 08f52df4-1237-4d53-8357-b466351a2bc4#015#012a=ssrc:1030961732 mslabel:uFFqMxWfRaQvSi5zo00U0ObxfUSsVwx5QPVm#015#012a=ssrc:1030 ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... 961732 label:08f52df4-1237-4d53-8357-b466351a2bc4#015#012a=sendrecv#015#012a=rtcp:8035#015#012a=candidate:fTW1UxNqekUOuWxE 1 UDP 2122260479 <rtpe ip> 8034 typ host#015#012a=candidate:fTW1UxNqekUOuWxE 2 UDP 2122260478 <rtpe ip> 8035 typ host#015#012m=video 8012 RTP/AVP 100 116 117 96#015#012c=IN IP4 <rtpe ip>#015#012a=candidate:1603072777 1 udp 2122260223 192.168.43.146 52284 typ host generation 0#015#012a=candidate:1603072777 2 udp 2122260222 192.168.43.146 59797 typ host generation 0#015#012a=candidate:286199801 1 tcp 1518280447 192.16 ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... 8.43.146 0 typ host tcptype active generation 0#015#012a=candidate:286199801 2 tcp 1518280446 192.168.43.146 0 typ host tcptype active generation 0#015#012a=ice-ufrag:ofcsykXOACI3xsng#015#012a=ice-pwd:qeRP0jwLOLTTsqn4QoD9Vrtn#015#012a=fingerprint:sha-256 D3:4E:A0:CA:6C:72:FD:7F:90:31:09:DE:14:F6:C2:F6:66:54:E4:77:9B:E3:6A:08:51:46:C2:55:54:89:3D:68#015#012a=setup:actpass#015#012a=mid:video#015#012a=extmap:2 urn:ietf:params:rtp-hdrext:toffset#015#012a=extmap:3 http://www.webrtc.org/experiments/rtp-hdrext/abs-send-time#015#012a=extmap:4 urn:3gpp:vide ...
May 25 20:16:00 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... o-orientation#015#012a=rtpmap:100 VP8/90000#015#012a=rtcp-fb:100 ccm fir#015#012a=rtcp-fb:100 nack#015#012a=rtcp-fb:100 nack pli#015#012a=rtcp-fb:100 goog-remb#015#012a=rtpmap:116 red/90000#015#012a=rtpmap:117 ulpfec/90000#015#012a=rtpmap:96 rtx/90000#015#012a=fmtp:96 apt=100#015#012a=recvonly#015#012a=rtcp:8013#015#012a=candidate:fTW1UxNqekUOuWxE 1 UDP 2122260479 <rtpe ip> 8012 typ host#015#012a=candidate:fTW1UxNqekUOuWxE 2 UDP 2122260478 <rtpe ip> 8013 typ host#015#012", "result": "ok" }
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k port  8034] Error parsing RTP header: invalid header version
May 25 20:16:02 box /usr/bin/sip-proxy[18290]: INFO: ===== rtpengine_answer(ICE=force via-branch=2)
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Received command 'answer' from 127.0.0.1:56346
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Dump for 'answer' from 127.0.0.1:56346: { "sdp": "v=0#015#012o=- 2998781975 2025607347 IN IP4 192.168.43.146#015#012s=-#015#012c=IN IP4 192.168.43.146#015#012t=0 0#015#012a=tool:baresip 0.4.12#015#012a=ice-ufrag:Q65y#015#012a=ice-pwd:NflumY7IKJArNGWAm90jKv#015#012m=audio 10486 RTP/AVP 111 0 8 126#015#012c=IN IP4 93.106.37.37#015#012a=rtpmap:111 opus/48000/2#015#012a=fmtp:111 stereo=1;sprop-stereo=1#015#012a=rtpmap:0 PCMU/8000#015#012a=rtpmap:8 PCMA/8000#015#012a=rtpmap:126 telephone-event/8000#015#012a=fmtp:126 0-15#015#012a=rtcp:10487 IN IP4 93.106.37.37#015#012a=sendrecv#015#012a=label:1#015#012a=rtcp-rsi ...
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... ze#015#012a=ssrc:4127537037 cname:sip:test@tutpro.com#015#012a=ptime:20#015#012a=candidate:c062660a 1 UDP 2113932031 192.98.102.10 10486 typ host#015#012a=candidate:c062660a 2 UDP 2113932030 192.98.102.10 10487 typ host#015#012a=candidate:c0a82b92 1 UDP 2113932031 192.168.43.146 10486 typ host#015#012a=candidate:c0a82b92 2 UDP 2113932030 192.168.43.146 10487 typ host#015#012a=candidate:5d6a2524 1 UDP 1677721855 93.106.37.37 10486 typ srflx raddr 192.98.102.10 rport 10486#015#012a=candidate:5d6a2524 2 UDP 1677721854 93.106.37.37 10487 typ srfl ...
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... x raddr 192.98.102.10 rport 10487#015#012m=video 0 RTP/AVP 0#015#012", "ICE": "force", "call-id": "4uk15dj6308kgn8su51k", "via-branch": "z9hG4bK708146", "received-from": [ "IP4", "93.106.37.37" ], "from-tag": "e1ohs4jm69", "to-tag": "7f6371826ae04830", "command": "answer" }
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Replying to 'answer' from 127.0.0.1:56346
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] Response dump for 'answer' to 127.0.0.1:56346: { "sdp": "v=0#015#012o=- 2998781975 2025607347 IN IP4 192.168.43.146#015#012s=-#015#012c=IN IP4 <rtpe ip>#015#012t=0 0#015#012a=tool:baresip 0.4.12#015#012a=ice-ufrag:Q65y#015#012a=ice-pwd:NflumY7IKJArNGWAm90jKv#015#012m=audio 8046 RTP/SAVPF 111 0 8 126#015#012c=IN IP4 <rtpe ip>#015#012a=rtpmap:111 opus/48000/2#015#012a=fmtp:111 stereo=1;sprop-stereo=1#015#012a=rtpmap:0 PCMU/8000#015#012a=rtpmap:8 PCMA/8000#015#012a=rtpmap:126 telephone-event/8000#015#012a=fmtp:126 0-15#015#012a=label:1#015#012a=rtcp-rsize#015#012a=ssrc:4127537037 cname:sip:test@t ...
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... utpro.com#015#012a=ptime:20#015#012a=candidate:c062660a 1 UDP 2113932031 192.98.102.10 10486 typ host#015#012a=candidate:c062660a 2 UDP 2113932030 192.98.102.10 10487 typ host#015#012a=candidate:c0a82b92 1 UDP 2113932031 192.168.43.146 10486 typ host#015#012a=candidate:c0a82b92 2 UDP 2113932030 192.168.43.146 10487 typ host#015#012a=candidate:5d6a2524 1 UDP 1677721855 93.106.37.37 10486 typ srflx raddr 192.98.102.10 rport 10486#015#012a=candidate:5d6a2524 2 UDP 1677721854 93.106.37.37 10487 typ srflx raddr 192.98.102.10 rport 10487#015#012a=s ...
May 25 20:16:02 box rtpengine[25992]: [4uk15dj6308kgn8su51k] ... endrecv#015#012a=rtcp:8046#015#012a=rtcp-mux#015#012a=candidate:fTW1UxNqekUOuWxE 1 UDP 2113932287 <rtpe ip> 8046 typ host#015#012m=video 0 RTP/SAVPF 0#015#012", "result": "ok" }

I can't reproduce this, I get a completely different result. Can you please share your rtpengine command line and also double check that this is really the latest master.

Richard Fuchs writes:

> I can't reproduce this, I get a completely different result. Can you
> please share your rtpengine command line and also double check that
> this is really the latest master.

rtpengine -v produces

4.0.0.3+0~mr4.0.0.0

i have myself changed the last digit of the first part to 3 in order to
increase version number when master changes without change in
debian/changelog version.  i have generated the packages from master May
23 09:52.

-- juha

Was this a branched call by any chance? With a previous offer for the same call, but using ICE=force-relay?

Richard Fuchs writes:

> Was this a branched call by any chance? With a previous offer for the
> same call, but using ICE=force-relay?

yes, it was like this:

May 27 16:40:37 siika /usr/bin/sip-proxy[20446]: INFO: ===== rtpengine_offer(ICE=force-relay via-branch=1 rtcp-mux-demux)
May 27 16:40:37 siika /usr/bin/sip-proxy[20450]: INFO: ===== rtpengine_delete(auto)
May 27 16:40:37 siika /usr/bin/sip-proxy[20450]: INFO: ===== rtpengine_offer(ICE=force replace-session-connection replace-origin via-branch=1 rtcp-mux-demux RTP/AVP)
May 27 16:40:40 siika /usr/bin/sip-proxy[20450]: INFO: ===== rtpengine_answer(ICE=force via-branch=2)

the second answer is the one where DTLS fingerprint is missing.

-- juha

Well, that would have been good to know. So this is the same issue as #113 

Richard Fuchs writes:

> Well, that would have been good to know. So this is the same issue as
> #113

Sorry about that.  I didn't realize that this is the same issue and
didn't think that previous offer would matter anything, since I thought
that rtpengine_delete complete zeros the brain of rtpengine regarding
the call.

-- Juha

Yes and no. A "delete" doesn't actually delete the call until the delete delay has run its course, which by default is 30 seconds. You can use the --delete-delay= option to influence the delay, but keep in mind that even if set to zero, there will still be a small delay (fraction of a second).

Richard Fuchs writes:

> Yes and do. A "delete" doesn't actually delete the call until the
> delete delay has run its course, which by default is 30 seconds. You
> can use the --delete-delay= option to influence the delay, but keep in
> mind that even if set to zero, there will still be a small delay
> (fraction of a second).

OK, thanks for the explanation.  Since sip proxy makes new offer with
new set of arguments immediately when it receives 488 from the callee,
is there some other means to be sure that the old offer does not
interfere with the new one?

-- Juha

Not at the moment. Possible solutions that come to mind:
1. Add an option to _delete() to specify a custom, non-default delete timeout, including the possibility to delete the call immediately.
2. Use the new via-branch handling and make sure that passthrough mode doesn't immediately get enabled for both directions, but rather in one direction only at first and in the other direction only when the matching answer comes. I'm not sure how feasible this is though.

Richard Fuchs writes:

> Not at the moment. Possible solutions that come to mind:
> 1. Add an option to _delete() to specify a custom, non-default delete
>    timeout, including the possibility to delete the call immediately.

Looks like I misunderstood your previous reply:

   You can use the --delete-delay= option to influence the delay ...

I thought that delete delay is such an option to rtpengine_delete().  I
even tried it:

May 27 17:34:01 siika rtpengine[20273]: [efg6qcjr5do014j7hoa8] Received command 'delete' from 127.0.0.1:39776
May 27 17:34:01 siika rtpengine[20273]: [efg6qcjr5do014j7hoa8] Dump for 'delete' from 127.0.0.1:39776: { "delete-delay": "0", "call-id": "efg6qcjr5do014j7hoa8", "received-from": [ "IP4", "192.98.102.10" ], "from-tag": "bgde2ujdg9", "command": "delete" }
May 27 17:34:01 siika rtpengine[20273]: [efg6qcjr5do014j7hoa8] Scheduling deletion of call branch 'bgde2ujdg9' in 30 seconds
...

So looks like delete-delay had no effect.  Also auto option disappeared
somewhere.

-- Juha

It's a command line option, not a per-command option.

Richard Fuchs writes:

> It's a command line option, not a per-command option.

OK again.  Can there be some bad side effects if delete-delay is 0 in
all calls?  If yes, then per command option would be important.

-- Juha

Well, if you do parallel forking, depending on how you do it, you may end up deleting the call prematurely. I think a custom delete delay is an important feature to have anyway.

I tried with --delete-delay=0 command line argument and got

  Scheduling deletion of call branch ... in 0 seconds

Unfortunately it didn't help.  Answer to next offer didn't include
DTLS fingerprint.

Perhaps rtpengine could delay reply to delete before it is sure that the
branch is really deleted?

-- Juha

Richard Fuchs writes:

> Well, if you do parallel forking, depending on how you do it, you may
> end up deleting the call prematurely. I think a custom delete delay is
> an important feature to have anyway.

In case of parallel forking, I thought that rtpengine_delete only
deletes only the branch it was called on.

My test showed that delete-delay=0 alone didn't help.  If
delete-delay=0, rtpengine also needs to delay the reply until the delete
has been completed.

-- Juha

The problem arises when the deleted branch was the only branch at the time (or when no to-tag was present, which would delete the entire call tree), but other branches appear later on.

As I mentioned before, a delete-delay of zero still includes a small delay of a fraction of a second. Immediate deletion will be supported by the coming patch.

I tried latest master and checked that the source knows about
delete-delay:

$ egrep delete-delay *
call_interfaces.c:  delete_delay = bencode_dictionary_get_integer(input, "delete-delay", -1);
main.c:     { "delete-delay",  'd', 0, G_OPTION_ARG_INT, &delete_delay,
"Delay for deleting a session from memory.",    "INT" },

Still, when rtpengine receives delete command where delete-delay=0, it
tells that delete will happen in 30 seconds:

May 28 11:49:58 lohi /usr/bin/sip-proxy[2837]: INFO: ===== rtpengine_delete(via-branch=1 delete-delay=0)
May 28 11:49:58 lohi rtpengine[15605]: [02e3b5e971e19135] Received command 'delete' from 127.0.0.1:37386
May 28 11:49:58 lohi rtpengine[15605]: [02e3b5e971e19135] Dump for 'delete' from 127.0.0.1:37386: { "delete-delay": "0", "call-id": "02e3b5e971e19135", "via-branch": "z9hG4bK2c5d3c479b3431fa", "received-from": [ "IP4", "192.98.102.10" ], "from-tag": "cb610ca783e40459", "command": "delete" }
May 28 11:49:58 lohi rtpengine[15605]: [02e3b5e971e19135] Scheduling deletion of call branch 'cb610ca783e40459' in 30 seconds

-- Juha

Ah, it's being passed as a string by the module...
