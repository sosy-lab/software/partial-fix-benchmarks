diff --git a/configure.ac b/configure.ac
index 4d56a36b5a..0e93b8f06c 100644
--- a/configure.ac
+++ b/configure.ac
@@ -160,12 +160,18 @@ dnl Check if we should link with afflib
 TSK_OPT_DEP_CHECK([afflib], [], [], [afflib/afflib.h], [afflib], [af_open])
 dnl Check if we should link with zlib
 TSK_OPT_DEP_CHECK([zlib], [ZLIB], [zlib], [zlib.h], [z], [inflate])
+dnl Check if we should link with libbfio
+TSK_OPT_DEP_CHECK([libbfio], [], [libbfio], [libbfio.h], [bfio], [libbfio_get_version])
 dnl Check if we should link with libewf
 TSK_OPT_DEP_CHECK([libewf], [EWF], [libewf], [libewf.h], [ewf], [libewf_get_version])
 dnl Check if we should link with libvhdi
 TSK_OPT_DEP_CHECK([libvhdi], [VHDI], [libvhdi], [libvhdi.h], [vhdi], [libvhdi_get_version])
 dnl Check if we should link with libvmdk
 TSK_OPT_DEP_CHECK([libvmdk], [VMDK], [libvmdk], [libvmdk.h], [vmdk], [libvmdk_get_version])
+dnl Check if we should link with libvslvm
+AS_IF([test "x$ax_libbfio" = "xyes"],
+      [TSK_OPT_DEP_CHECK([libvslvm], [LVM], [libvslvm], [libvslvm.h], [vslvm], [libvslvm_get_version])],
+      [ax_libvslvm=no])
 
 dnl check for cppunit
 AC_ARG_ENABLE([cppunit],
@@ -359,8 +365,10 @@ Building:
    libewf support:                        $ax_libewf
    zlib support:                          $ax_zlib
 
+   libbfio support:                       $ax_libbfio
    libvhdi support:                       $ax_libvhdi
    libvmdk support:                       $ax_libvmdk
+   libvslvm support:                      $ax_libvslvm
 Features:
    Java/JNI support:                      $ax_java_support
    Multithreading:                        $ax_multithread
diff --git a/tsk/img/pool.hpp b/tsk/img/pool.hpp
index 17827c0c31..4d5ea6f9ab 100644
--- a/tsk/img/pool.hpp
+++ b/tsk/img/pool.hpp
@@ -15,7 +15,6 @@
 #define _POOL_H
 
 #include "../pool/tsk_pool.h"
-#include "../fs/tsk_apfs.hpp"
 
 #ifdef __cplusplus
 extern "C" {
@@ -27,6 +26,8 @@ extern "C" {
         const TSK_POOL_INFO *pool_info;
         TSK_DADDR_T pvol_block;
 
+        void *impl;
+
     } IMG_POOL_INFO;
 
 #ifdef __cplusplus
diff --git a/tsk/pool/Makefile.am b/tsk/pool/Makefile.am
index eb5e48ba05..2909959f90 100644
--- a/tsk/pool/Makefile.am
+++ b/tsk/pool/Makefile.am
@@ -4,7 +4,9 @@ EXTRA_DIST = .indent.pro
 
 noinst_LTLIBRARIES = libtskpool.la
 libtskpool_la_SOURCES = pool_open.cpp pool_read.cpp pool_types.cpp \
-	apfs_pool_compat.cpp apfs_pool.cpp
+	apfs_pool_compat.cpp apfs_pool.cpp \
+	img_bfio_handle.c \
+	lvm_pool_compat.cpp lvm_pool.cpp
 
 indent:
 	indent *.c *.cpp *.h *.hpp
diff --git a/tsk/pool/img_bfio_handle.c b/tsk/pool/img_bfio_handle.c
new file mode 100644
index 0000000000..58b67c0a80
--- /dev/null
+++ b/tsk/pool/img_bfio_handle.c
@@ -0,0 +1,255 @@
+/*
+ * The Sleuth Kit - Image BFIO handle
+ *
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
+ *
+ * This software is distributed under the Common Public License 1.0
+ */
+
+#include "tsk/base/tsk_base_i.h"
+
+#ifdef HAVE_LIBBFIO
+
+#include "img_bfio_handle.h"
+
+#include <libbfio.h>
+
+#include "tsk/img/tsk_img.h"
+
+/* Initializes the image BFIO handle
+ * Returns 1 if successful or -1 on error
+ */
+int img_bfio_handle_initialize(
+     libbfio_handle_t **handle,
+     TSK_IMG_INFO *image,
+     TSK_OFF_T offset,
+     libbfio_error_t **error )
+{
+	img_bfio_handle_t *img_bfio_handle = NULL;
+
+	img_bfio_handle = (img_bfio_handle_t *) tsk_malloc( sizeof( img_bfio_handle_t ) );
+
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	img_bfio_handle->image = image;
+	img_bfio_handle->base_offset = offset;
+	img_bfio_handle->logical_offset = 0;
+	img_bfio_handle->access_flags = LIBBFIO_ACCESS_FLAG_READ;
+
+	if( libbfio_handle_initialize(
+	     handle,
+	     (intptr_t *) img_bfio_handle,
+	     (int (*)(intptr_t **, libbfio_error_t **)) img_bfio_handle_free,
+	     NULL,
+	     (int (*)(intptr_t *, int, libbfio_error_t **)) img_bfio_handle_open,
+	     (int (*)(intptr_t *, libbfio_error_t **)) img_bfio_handle_close,
+	     (ssize_t (*)(intptr_t *, uint8_t *, size_t, libbfio_error_t **)) img_bfio_handle_read,
+	     NULL,
+	     (off64_t (*)(intptr_t *, off64_t, int, libbfio_error_t **)) img_bfio_handle_seek_offset,
+	     (int (*)(intptr_t *, libbfio_error_t **)) img_bfio_handle_exists,
+	     (int (*)(intptr_t *, libbfio_error_t **)) img_bfio_handle_is_open,
+	     (int (*)(intptr_t *, size64_t *, libbfio_error_t **)) img_bfio_handle_get_size,
+	     LIBBFIO_FLAG_IO_HANDLE_MANAGED | LIBBFIO_FLAG_IO_HANDLE_CLONE_BY_FUNCTION,
+	     error ) != 1 )
+	{
+		free(
+		 img_bfio_handle );
+
+		return( -1 );
+	}
+	return( 1 );
+}
+
+/* Frees an image BFIO handle
+ * Returns 1 if succesful or -1 on error
+ */
+int img_bfio_handle_free(
+     img_bfio_handle_t **img_bfio_handle,
+     libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	if( *img_bfio_handle != NULL )
+	{
+		free(
+		 *img_bfio_handle );
+
+		*img_bfio_handle = NULL;
+	}
+	return( 1 );
+}
+
+/* Opens the image BFIO handle
+ * Returns 1 if successful or -1 on error
+ */
+int img_bfio_handle_open(
+     img_bfio_handle_t *img_bfio_handle,
+     int access_flags,
+     libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	if( img_bfio_handle->image == NULL )
+	{
+		return( -1 );
+	}
+	if( ( ( access_flags & LIBBFIO_ACCESS_FLAG_READ ) != 0 )
+	 && ( ( access_flags & LIBBFIO_ACCESS_FLAG_WRITE ) != 0 ) )
+	{
+		return( -1 );
+	}
+	if( ( access_flags & LIBBFIO_ACCESS_FLAG_WRITE ) != 0 )
+	{
+		return( -1 );
+	}
+	/* No need to do anything here, because the file object is already open
+	 */
+	img_bfio_handle->access_flags = access_flags;
+
+	return( 1 );
+}
+
+/* Closes the image BFIO handle
+ * Returns 0 if successful or -1 on error
+ */
+int img_bfio_handle_close(
+     img_bfio_handle_t *img_bfio_handle,
+     libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	if( img_bfio_handle->image == NULL )
+	{
+		return( -1 );
+	}
+	/* Do not close the image, have Sleuthkit deal with it
+	 */
+	img_bfio_handle->access_flags = 0;
+
+	return( 0 );
+}
+
+/* Reads a buffer from the image BFIO handle
+ * Returns the number of bytes read if successful, or -1 on error
+ */
+ssize_t img_bfio_handle_read(
+         img_bfio_handle_t *img_bfio_handle,
+         uint8_t *buffer,
+         size_t size,
+         libbfio_error_t **error )
+{
+	ssize_t read_count = 0;
+
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	read_count = tsk_img_read(
+	              img_bfio_handle->image,
+	              img_bfio_handle->base_offset + img_bfio_handle->logical_offset,
+	              (char *) buffer,
+	              size );
+
+	if( read_count == -1 )
+	{
+		return( -1 );
+	}
+	return( read_count );
+}
+
+/* Seeks a certain offset within the image BFIO handle
+ * Returns the offset if the seek is successful or -1 on error
+ */
+off64_t img_bfio_handle_seek_offset(
+         img_bfio_handle_t *img_bfio_handle,
+         off64_t offset,
+         int whence,
+         libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+/* TODO add support for SEEK_CUR and SEEK_CUR */
+	if( whence != SEEK_SET )
+	{
+		return( -1 );
+	}
+	img_bfio_handle->logical_offset = offset;
+
+	return( offset );
+}
+
+/* Function to determine if a file exists
+ * Returns 1 if file exists, 0 if not or -1 on error
+ */
+int img_bfio_handle_exists(
+     img_bfio_handle_t *img_bfio_handle,
+     libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	if( img_bfio_handle->image == NULL )
+	{
+		return( 0 );
+	}
+	return( 1 );
+}
+
+/* Check if the file is open
+ * Returns 1 if open, 0 if not or -1 on error
+ */
+int img_bfio_handle_is_open(
+     img_bfio_handle_t *img_bfio_handle,
+     libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	if( img_bfio_handle->image == NULL )
+	{
+		return( -1 );
+	}
+	/* As far as BFIO is concerned the file object is always open
+	 */
+	return( 1 );
+}
+
+/* Retrieves the file size
+ * Returns 1 if successful or -1 on error
+ */
+int img_bfio_handle_get_size(
+     img_bfio_handle_t *img_bfio_handle,
+     size64_t *size,
+     libbfio_error_t **error )
+{
+	if( img_bfio_handle == NULL )
+	{
+		return( -1 );
+	}
+	if( img_bfio_handle->image == NULL )
+	{
+		return( -1 );
+	}
+	if( size == NULL )
+	{
+		return( -1 );
+	}
+	*size = img_bfio_handle->image->size;
+
+	return( 1 );
+}
+
+#endif /* HAVE_LIBBFIO */
+
diff --git a/tsk/pool/img_bfio_handle.h b/tsk/pool/img_bfio_handle.h
new file mode 100644
index 0000000000..842d155eea
--- /dev/null
+++ b/tsk/pool/img_bfio_handle.h
@@ -0,0 +1,90 @@
+/*
+ * The Sleuth Kit - Image BFIO handle
+ *
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
+ *
+ * This software is distributed under the Common Public License 1.0
+ */
+
+#if !defined( _IMG_BFIO_HANDLE_H )
+#define _IMG_BFIO_HANDLE_H
+
+#include "tsk/base/tsk_base_i.h"
+
+#ifdef HAVE_LIBBFIO
+
+#include <libbfio.h>
+
+#include "tsk/img/tsk_img.h"
+
+#if defined( __cplusplus )
+extern "C" {
+#endif
+
+typedef struct img_bfio_handle img_bfio_handle_t;
+
+struct img_bfio_handle
+{
+	TSK_IMG_INFO *image;
+	TSK_OFF_T base_offset;
+	TSK_OFF_T logical_offset;
+	int access_flags;
+};
+
+int img_bfio_handle_initialize(
+     libbfio_handle_t **handle,
+     TSK_IMG_INFO *image,
+     TSK_OFF_T offset,
+     libbfio_error_t **error );
+
+int img_bfio_handle_free(
+     img_bfio_handle_t **img_bfio_handle,
+     libbfio_error_t **error );
+
+int img_bfio_handle_clone(
+     img_bfio_handle_t **destination_img_bfio_handle,
+     img_bfio_handle_t *source_img_bfio_handle,
+     libbfio_error_t **error );
+
+int img_bfio_handle_open(
+     img_bfio_handle_t *img_bfio_handle,
+     int access_flags,
+     libbfio_error_t **error );
+
+int img_bfio_handle_close(
+     img_bfio_handle_t *img_bfio_handle,
+     libbfio_error_t **error );
+
+ssize_t img_bfio_handle_read(
+         img_bfio_handle_t *img_bfio_handle,
+         uint8_t *buffer,
+         size_t size,
+         libbfio_error_t **error );
+
+off64_t img_bfio_handle_seek_offset(
+         img_bfio_handle_t *img_bfio_handle,
+         off64_t offset,
+         int whence,
+         libbfio_error_t **error );
+
+int img_bfio_handle_exists(
+     img_bfio_handle_t *img_bfio_handle,
+     libbfio_error_t **error );
+
+int img_bfio_handle_is_open(
+     img_bfio_handle_t *img_bfio_handle,
+     libbfio_error_t **error );
+
+int img_bfio_handle_get_size(
+     img_bfio_handle_t *img_bfio_handle,
+     size64_t *size,
+     libbfio_error_t **error );
+
+#if defined( __cplusplus )
+}
+#endif
+
+#endif /* HAVE_LIBBFIO */
+
+#endif /* !defined( _IMG_BFIO_HANDLE_H ) */
+
diff --git a/tsk/pool/lvm_pool.cpp b/tsk/pool/lvm_pool.cpp
new file mode 100644
index 0000000000..308e72ac19
--- /dev/null
+++ b/tsk/pool/lvm_pool.cpp
@@ -0,0 +1,122 @@
+/*
+ * The Sleuth Kit - Add on for Linux LVM support
+ *
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
+ *
+ * This software is distributed under the Common Public License 1.0
+ */
+
+#include "tsk/base/tsk_base_i.h"
+
+#if HAVE_LIBVSLVM
+
+#include "img_bfio_handle.h"
+#include "tsk_lvm.hpp"
+
+#include "tsk/auto/guid.h"
+
+#include <stdexcept>
+#include <tuple>
+
+#include <libbfio.h>
+#include <libvslvm.h>
+
+#if !defined( LIBVSLVM_HAVE_BFIO )
+
+LIBVSLVM_EXTERN \
+int libvslvm_handle_open_file_io_handle(
+     libvslvm_handle_t *handle,
+     libbfio_handle_t *file_io_handle,
+     int access_flags,
+     libvslvm_error_t **error );
+
+LIBVSLVM_EXTERN \
+int libvslvm_handle_open_physical_volume_files_file_io_pool(
+     libvslvm_handle_t *handle,
+     libbfio_pool_t *file_io_pool,
+     libcerror_error_t **error );
+
+#endif /* !defined( LIBVSLVM_HAVE_BFIO ) */
+
+LVMPool::LVMPool(std::vector<img_t>&& imgs)
+    : TSKPool(std::forward<std::vector<img_t>>(imgs)) {
+  if (_members.size() != 1) {
+    throw std::runtime_error(
+        "Only single physical volume LVM pools are currently supported");
+  }
+  std::tie(_img, _offset) = _members[0];
+
+  libbfio_handle_t *file_io_handle = NULL;
+  int file_io_pool_entry =  0;
+
+  if (img_bfio_handle_initialize(&file_io_handle, _img, _offset, NULL) != 1) {
+    throw std::runtime_error("Unable to initialize image BFIO handle");
+  }
+  if (libbfio_pool_initialize(&( _file_io_pool ), 0, LIBBFIO_POOL_UNLIMITED_NUMBER_OF_OPEN_HANDLES, NULL) != 1) {
+    libbfio_handle_free(&file_io_handle, NULL);
+    throw std::runtime_error("Unable to initialize BFIO pool");
+  }
+  if (libbfio_pool_append_handle(_file_io_pool, &file_io_pool_entry, file_io_handle, LIBBFIO_OPEN_READ, NULL) != 1) {
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+    libbfio_handle_free(&file_io_handle, NULL);
+    throw std::runtime_error("Unable to add image BFIO handle to BFIO pool");
+  }
+  if (libvslvm_handle_initialize(&( _lvm_handle ), NULL) != 1) {
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+    throw std::runtime_error("Unable to initialize LVM handle");
+  }
+  if (libvslvm_handle_open_file_io_handle(_lvm_handle, file_io_handle, LIBVSLVM_OPEN_READ, NULL) != 1) {
+    libvslvm_handle_free(&( _lvm_handle ), NULL);
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+    throw std::runtime_error("Unable to open LVM handle");
+  }
+  if (libvslvm_handle_open_physical_volume_files_file_io_pool(_lvm_handle, _file_io_pool, NULL) != 1) {
+    libvslvm_handle_free(&( _lvm_handle ), NULL);
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+    throw std::runtime_error("Unable to open LVM handle");
+  }
+  if (libvslvm_handle_get_volume_group(_lvm_handle, &( _lvm_volume_group ), NULL) != 1) {
+    libvslvm_handle_free(&( _lvm_handle ), NULL);
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+    throw std::runtime_error("Unable to retrieve LVM volume group");
+  }
+  if (tsk_verbose) {
+    tsk_fprintf(stderr, "LVMPool: retrieved LVM volume group.\n" );
+  }
+  char identifier_string[ 64 ];
+
+  if (libvslvm_volume_group_get_identifier(_lvm_volume_group, identifier_string, 64, NULL) != 1) {
+    libvslvm_volume_group_free(&( _lvm_volume_group ), NULL);
+    libvslvm_handle_free(&( _lvm_handle ), NULL);
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+    throw std::runtime_error("Unable to retrieve LVM volume group identifier");
+  }
+  identifier = std::string(identifier_string);
+
+  _block_size = 0;
+  _dev_block_size = _img->sector_size;
+  _num_blocks = 0;
+
+  _num_vols = 0;
+}
+
+LVMPool::~LVMPool() {
+  if (_lvm_volume_group != nullptr) {
+    libvslvm_volume_group_free(&( _lvm_volume_group ), NULL);
+  }
+  if (_lvm_handle != nullptr) {
+    libvslvm_handle_free(&( _lvm_handle ), NULL);
+  }
+  if (_file_io_pool != nullptr) {
+    libbfio_pool_free(&( _file_io_pool ), NULL);
+  }
+}
+
+ssize_t LVMPool::read(uint64_t address, char* buf, size_t buf_size) const
+    noexcept {
+  // TODO implement, this functions appears to be only used by the JNI bindings
+  return tsk_img_read(_img, address + _offset, buf, buf_size);
+}
+
+#endif /* HAVE_LIBVSLVM */
+
diff --git a/tsk/pool/lvm_pool_compat.cpp b/tsk/pool/lvm_pool_compat.cpp
new file mode 100644
index 0000000000..85cedb7d72
--- /dev/null
+++ b/tsk/pool/lvm_pool_compat.cpp
@@ -0,0 +1,179 @@
+/*
+ * The Sleuth Kit - Add on for Linux LVM support
+ *
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
+ *
+ * This software is distributed under the Common Public License 1.0
+ */
+
+#include "tsk/base/tsk_base_i.h"
+
+#if HAVE_LIBVSLVM
+
+#include "lvm_pool_compat.hpp"
+
+#include "tsk/img/pool.hpp"
+#include "tsk/img/tsk_img_i.h"
+
+#include <stdexcept>
+
+/**
+ * Get error string from libvslvm and make buffer empty if that didn't work. 
+ * @returns 1 if error message was not set
+ */
+static uint8_t getError(libvslvm_error_t *vslvm_error, char error_string[512])
+{
+    error_string[0] = '\0';
+    int retval = libvslvm_error_backtrace_sprint(vslvm_error, error_string, 512);
+    return retval <= 0;
+}
+
+uint8_t LVMPoolCompat::poolstat(FILE *hFile) const noexcept try {
+
+    tsk_fprintf(hFile, "POOL CONTAINER INFORMATION\n");
+    tsk_fprintf(hFile, "--------------------------------------------\n\n");
+    tsk_fprintf(hFile, "Volume group %s\n", identifier.c_str());
+    tsk_fprintf(hFile, "==============================================\n");
+    tsk_fprintf(hFile, "Type: LVM\n");
+
+    int number_of_logical_volumes = 0;
+    if (libvslvm_volume_group_get_number_of_logical_volumes(_lvm_volume_group, &number_of_logical_volumes, NULL) != 1 ) {
+        return 1;
+    }
+    libvslvm_logical_volume_t *lvm_logical_volume = NULL;
+    char volume_name[ 64 ];
+    char volume_identifier[ 64 ];
+
+    for (int volume_index = 0; volume_index < number_of_logical_volumes; volume_index++ ) {
+        if (libvslvm_volume_group_get_logical_volume(_lvm_volume_group, volume_index, &lvm_logical_volume, NULL) != 1 ) {
+            return 1;
+        }
+        if (libvslvm_logical_volume_get_identifier(lvm_logical_volume, volume_identifier, 64, NULL) != 1 ) {
+            return 1;
+        }
+        if (libvslvm_logical_volume_get_name(lvm_logical_volume, volume_name, 64, NULL) != 1 ) {
+            return 1;
+        }
+        if (libvslvm_logical_volume_free(&lvm_logical_volume, NULL) != 1 ) {
+            return 1;
+        }
+        tsk_fprintf(hFile, "|\n");
+        tsk_fprintf(hFile, "+-> Volume %s\n", volume_identifier);
+        tsk_fprintf(hFile, "|   ===========================================\n");
+        tsk_fprintf(hFile, "|   Name: %s\n", volume_name);
+    }
+    return 0;
+} catch (const std::exception &e) {
+    tsk_error_reset();
+    tsk_error_set_errno(TSK_ERR_POOL_GENPOOL);
+    tsk_error_set_errstr("%s", e.what());
+    return 1;
+}
+
+static void
+lvm_logical_volume_img_close(TSK_IMG_INFO * img_info)
+{
+    if (img_info != NULL) {
+        IMG_POOL_INFO *pool_img_info = (IMG_POOL_INFO *)img_info;
+        libvslvm_logical_volume_free((libvslvm_logical_volume_t **) &( pool_img_info->impl ), NULL);
+
+        tsk_deinit_lock(&(img_info->cache_lock));
+        tsk_img_free(img_info);
+    }
+}
+
+static void
+lvm_logical_volume_img_imgstat(TSK_IMG_INFO * img_info, FILE *hFile)
+{
+    tsk_fprintf(hFile, "IMAGE FILE INFORMATION\n");
+    tsk_fprintf(hFile, "--------------------------------------------\n");
+    tsk_fprintf(hFile, "Image Type:\t\tLVM logical volume\n");
+    tsk_fprintf(hFile, "\nSize of data in bytes:\t%" PRIdOFF "\n",
+        img_info->size);
+}
+
+static ssize_t
+lvm_logical_volume_img_read(TSK_IMG_INFO * img_info, TSK_OFF_T offset, char *buf, size_t len)
+{
+    IMG_POOL_INFO *pool_img_info = (IMG_POOL_INFO *)img_info;
+    libvslvm_error_t *vslvm_error = NULL;
+
+    // correct the offset to be relative to the start of the logical volume
+    offset -= pool_img_info->pool_info->img_offset;
+
+    if (tsk_verbose) {
+        tsk_fprintf(stderr, "lvm_logical_volume_img_read: offset: %" PRIdOFF " read len: %" PRIuSIZE ".\n",
+          offset, len);
+    }
+    if ((offset < 0) || (offset > img_info->size)) {
+        return 0;
+    }
+    ssize_t read_count = libvslvm_logical_volume_read_buffer_at_offset((libvslvm_logical_volume_t *) pool_img_info->impl, buf, len, offset, &vslvm_error);
+
+    if (read_count == -1) {
+        char error_string[521];
+        getError(vslvm_error, error_string);
+        tsk_fprintf(stderr, "lvm_logical_volume_img_read: %s\n", error_string);
+    }
+    return read_count;
+}
+
+TSK_IMG_INFO * LVMPoolCompat::getImageInfo(const TSK_POOL_INFO *pool_info, TSK_DADDR_T pvol_block) noexcept try {
+
+    libvslvm_logical_volume_t *lvm_logical_volume = NULL;
+
+    // pvol_block contians the logical volume index + 1
+    if (libvslvm_volume_group_get_logical_volume(_lvm_volume_group, pvol_block - 1, &lvm_logical_volume, NULL) != 1 ) {
+        return NULL;
+    }
+    uint64_t logical_volume_size = 0;
+
+    if (libvslvm_logical_volume_get_size(lvm_logical_volume, &logical_volume_size, NULL) != 1 ) {
+        return NULL;
+    }
+    IMG_POOL_INFO *img_pool_info = (IMG_POOL_INFO *)tsk_img_malloc(sizeof(IMG_POOL_INFO));
+
+    if (img_pool_info == NULL) {
+        return NULL;
+    }
+    img_pool_info->pool_info = pool_info;
+    img_pool_info->pvol_block = pvol_block;
+
+    img_pool_info->img_info.read = lvm_logical_volume_img_read;
+    img_pool_info->img_info.close = lvm_logical_volume_img_close;
+    img_pool_info->img_info.imgstat = lvm_logical_volume_img_imgstat;
+
+    img_pool_info->impl = (void *) lvm_logical_volume;
+
+    TSK_IMG_INFO *img_info = (TSK_IMG_INFO *)img_pool_info;
+
+    img_info->tag = TSK_IMG_INFO_TAG;
+    img_info->itype = TSK_IMG_TYPE_POOL;
+
+    // Copy original info from the first TSK_IMG_INFO. There was a check in the
+    // LVMPool that _members has only one entry.
+    IMG_POOL_INFO *pool_img_info = (IMG_POOL_INFO *)img_info;
+    const auto pool = static_cast<LVMPoolCompat*>(pool_img_info->pool_info->impl);
+    TSK_IMG_INFO *origInfo = pool->_members[0].first;
+
+    img_info->size = logical_volume_size;
+    img_info->num_img = origInfo->num_img;
+    img_info->sector_size = origInfo->sector_size;
+    img_info->page_size = origInfo->page_size;
+    img_info->spare_size = origInfo->spare_size;
+    img_info->images = origInfo->images;
+
+    tsk_init_lock(&(img_info->cache_lock));
+
+    return img_info;
+
+}
+catch (const std::exception &e) {
+    tsk_error_reset();
+    tsk_error_set_errno(TSK_ERR_POOL_GENPOOL);
+    tsk_error_set_errstr("%s", e.what());
+    return NULL;
+}
+
+#endif /* HAVE_LIBVSLVM */
+
diff --git a/tsk/pool/lvm_pool_compat.hpp b/tsk/pool/lvm_pool_compat.hpp
new file mode 100644
index 0000000000..e9aad68974
--- /dev/null
+++ b/tsk/pool/lvm_pool_compat.hpp
@@ -0,0 +1,30 @@
+/*
+ * The Sleuth Kit - Add on for Linux LVM support
+ *
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
+ *
+ *
+ * This software is distributed under the Common Public License 1.0
+ */
+
+#pragma once
+
+#include "tsk/base/tsk_base_i.h"
+
+#if HAVE_LIBVSLVM
+
+#include "pool_compat.hpp"
+#include "tsk_lvm.hpp"
+
+class LVMPoolCompat : public TSKPoolCompat<LVMPool> {
+ public:
+  template <typename... Args>
+  LVMPoolCompat(Args&&... args)
+      : TSKPoolCompat<LVMPool>(TSK_POOL_TYPE_LVM, std::forward<Args>(args)...) { }
+
+  uint8_t poolstat(FILE* hFile) const noexcept;
+  TSK_IMG_INFO * getImageInfo(const TSK_POOL_INFO *pool_info, TSK_DADDR_T pvol_block) noexcept;
+};
+
+#endif /* HAVE_LIBVSLVM */
+
diff --git a/tsk/pool/pool_open.cpp b/tsk/pool/pool_open.cpp
index 66b07efd4d..9aae5e8d90 100755
--- a/tsk/pool/pool_open.cpp
+++ b/tsk/pool/pool_open.cpp
@@ -3,16 +3,19 @@
  *
  * Brian Carrier [carrier <at> sleuthkit [dot] org]
  * Copyright (c) 2018-2019 BlackBag Technologies.  All Rights reserved
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
  *
  * This software is distributed under the Common Public License 1.0
  */
-#include "../fs/tsk_apfs.hpp"
+#include "tsk/base/tsk_base_i.h"
 
 #include "apfs_pool_compat.hpp"
+#include "lvm_pool_compat.hpp"
 #include "pool_compat.hpp"
 
-#include "../img/tsk_img.h"
-#include "../vs/tsk_vs.h"
+#include "tsk/fs/tsk_apfs.hpp"
+#include "tsk/img/tsk_img.h"
+#include "tsk/vs/tsk_vs.h"
 
 const TSK_POOL_INFO *tsk_pool_open_sing(const TSK_VS_PART_INFO *part,
                                         TSK_POOL_TYPE_ENUM type) {
@@ -100,29 +103,50 @@ const TSK_POOL_INFO *tsk_pool_open_img_sing(TSK_IMG_INFO *img, TSK_OFF_T offset,
 const TSK_POOL_INFO *tsk_pool_open_img(int num_imgs, TSK_IMG_INFO *const imgs[],
                                        const TSK_OFF_T offsets[],
                                        TSK_POOL_TYPE_ENUM type) {
-  std::vector<APFSPool::img_t> v{};
-  v.reserve(num_imgs);
+  std::vector<APFSPool::img_t> apfs_v{};
+  apfs_v.reserve(num_imgs);
 
   for (auto i = 0; i < num_imgs; i++) {
-    v.emplace_back(imgs[i], offsets[i]);
+    apfs_v.emplace_back(imgs[i], offsets[i]);
   }
+#ifdef HAVE_LIBVSLVM
+  std::vector<LVMPool::img_t> lvm_v{};
+
+  lvm_v.reserve(num_imgs);
+
+  for (auto i = 0; i < num_imgs; i++) {
+    lvm_v.emplace_back(imgs[i], offsets[i]);
+  }
+#endif
+
+  const char *error_string = NULL;
 
   switch (type) {
     case TSK_POOL_TYPE_DETECT:
       try {
-        auto apfs = new APFSPoolCompat(std::move(v), APFS_POOL_NX_BLOCK_LATEST);
+        auto apfs = new APFSPoolCompat(std::move(apfs_v), APFS_POOL_NX_BLOCK_LATEST);
 
         return &apfs->pool_info();
       } catch (std::runtime_error &e) {
-        if (tsk_verbose) {
-          tsk_fprintf(stderr, "tsk_pool_open_img: APFS check failed: %s\n",
-                      e.what());
-        }
+        error_string = e.what();
+      }
+#ifdef HAVE_LIBVSLVM
+      try {
+        auto lvm = new LVMPoolCompat(std::move(lvm_v));
+
+        return &lvm->pool_info();
+      } catch (std::runtime_error &e) {
+        error_string = e.what();
+      }
+#endif
+      if (tsk_verbose) {
+        tsk_fprintf(stderr, "tsk_pool_open_img: pool type detection failed: %s\n",
+                    error_string);
       }
       break;
     case TSK_POOL_TYPE_APFS:
       try {
-        auto apfs = new APFSPoolCompat(std::move(v), APFS_POOL_NX_BLOCK_LATEST);
+        auto apfs = new APFSPoolCompat(std::move(apfs_v), APFS_POOL_NX_BLOCK_LATEST);
 
         return &apfs->pool_info();
       } catch (std::runtime_error &e) {
@@ -131,6 +155,21 @@ const TSK_POOL_INFO *tsk_pool_open_img(int num_imgs, TSK_IMG_INFO *const imgs[],
         tsk_error_set_errstr("%s", e.what());
       }
       return nullptr;
+
+    // Will fallthrough to TSK_POOL_TYPE_UNSUPP if libvslvm is not available.
+    case TSK_POOL_TYPE_LVM:
+#ifdef HAVE_LIBVSLVM
+      try {
+        auto lvm = new LVMPoolCompat(std::move(lvm_v));
+
+        return &lvm->pool_info();
+      } catch (std::runtime_error &e) {
+        tsk_error_reset();
+        tsk_error_set_errno(TSK_ERR_POOL_UNKTYPE);
+        tsk_error_set_errstr("%s", e.what());
+      }
+      return nullptr;
+#endif
     case TSK_POOL_TYPE_UNSUPP:
       // All other pool types are unsupported
       tsk_error_reset();
diff --git a/tsk/pool/pool_types.cpp b/tsk/pool/pool_types.cpp
index f4d3b93ad9..19bd8a5de0 100644
--- a/tsk/pool/pool_types.cpp
+++ b/tsk/pool/pool_types.cpp
@@ -22,6 +22,7 @@ struct POOL_TYPES {
 static const POOL_TYPES pool_type_table[]{
     {"auto", TSK_POOL_TYPE_DETECT, "auto-detect"},
     {"apfs", TSK_POOL_TYPE_APFS, "APFS container"},
+    {"lvm", TSK_POOL_TYPE_LVM, "Linux LVM volume group"},
 };
 
 /**
diff --git a/tsk/pool/tsk_lvm.hpp b/tsk/pool/tsk_lvm.hpp
new file mode 100644
index 0000000000..3dceb646ea
--- /dev/null
+++ b/tsk/pool/tsk_lvm.hpp
@@ -0,0 +1,51 @@
+/*
+ * The Sleuth Kit - Add on for Linux LVM support
+ *
+ * Copyright (c) 2022 Joachim Metz <joachim.metz@gmail.com>
+ *
+ * This software is distributed under the Common Public License 1.0
+ */
+
+#pragma once
+
+#include "tsk/base/tsk_base_i.h"
+
+#if HAVE_LIBVSLVM
+
+#include "tsk_pool.hpp"
+
+#include <libbfio.h>
+#include <libvslvm.h>
+
+class LVMPool;
+
+class LVMPool : public TSKPool {
+ protected:
+  TSK_IMG_INFO *_img;
+  // Start of the pool data within the image
+  TSK_OFF_T _offset;
+  libbfio_pool_t *_file_io_pool = NULL;
+  libvslvm_handle_t *_lvm_handle = NULL;
+  libvslvm_volume_group_t *_lvm_volume_group = NULL;
+
+ public:
+  LVMPool(std::vector<img_t> &&imgs);
+
+  // Moveable
+  LVMPool(LVMPool &&) = default;
+  LVMPool &operator=(LVMPool &&) = default;
+
+  // Not copyable because of TSK_IMG_INFO pointer
+  LVMPool(const LVMPool &) = delete;
+  LVMPool &operator=(const LVMPool &) = delete;
+
+  ~LVMPool();
+
+  std::string identifier;
+
+  ssize_t read(uint64_t address, char *buf, size_t buf_size) const
+      noexcept final;
+};
+
+#endif /* HAVE_LIBVSLVM */
+
diff --git a/tsk/pool/tsk_pool.h b/tsk/pool/tsk_pool.h
index 5c4bd0de2c..e80c191be9 100644
--- a/tsk/pool/tsk_pool.h
+++ b/tsk/pool/tsk_pool.h
@@ -25,6 +25,7 @@ typedef struct TSK_FS_ATTR_RUN TSK_FS_ATTR_RUN;
 typedef enum {
   TSK_POOL_TYPE_DETECT = 0x0000,  ///< Use autodetection methods
   TSK_POOL_TYPE_APFS = 0x0001,    ///< APFS Pooled Volumes
+  TSK_POOL_TYPE_LVM = 0x0002,    ///< Linux LVM volume group
   TSK_POOL_TYPE_UNSUPP = 0xffff,  ///< Unsupported pool container type
 } TSK_POOL_TYPE_ENUM;
 
diff --git a/tsk/tsk_config.h.in b/tsk/tsk_config.h.in
index 337d951437..332929169f 100644
--- a/tsk/tsk_config.h.in
+++ b/tsk/tsk_config.h.in
@@ -1,21 +1,15 @@
 /* tsk/tsk_config.h.in.  Generated from configure.ac by autoheader.  */
 
-/* Define to one of `_getb67', `GETB67', `getb67' for Cray-2 and Cray-YMP
-   systems. This function is required for `alloca.c' support on those systems.
-   */
-#undef CRAY_STACKSEG_END
-
-/* Define to 1 if using `alloca.c'. */
+/* Define to 1 if using 'alloca.c'. */
 #undef C_ALLOCA
 
 /* Define to 1 if you have the <afflib/afflib.h> header file. */
 #undef HAVE_AFFLIB_AFFLIB_H
 
-/* Define to 1 if you have `alloca', as a function or macro. */
+/* Define to 1 if you have 'alloca', as a function or macro. */
 #undef HAVE_ALLOCA
 
-/* Define to 1 if you have <alloca.h> and it should be used (not on Ultrix).
-   */
+/* Define to 1 if <alloca.h> works. */
 #undef HAVE_ALLOCA_H
 
 /* define if the compiler supports basic C++14 syntax */
@@ -54,6 +48,12 @@
 /* Define to 1 if you have the `afflib' library (-lafflib). */
 #undef HAVE_LIBAFFLIB
 
+/* Define to 1 if you have the `bfio' library (-lbfio). */
+#undef HAVE_LIBBFIO
+
+/* Define to 1 if you have the <libbfio.h> header file. */
+#undef HAVE_LIBBFIO_H
+
 /* Define to 1 if you have the `dl' library (-ldl). */
 #undef HAVE_LIBDL
 
@@ -63,9 +63,6 @@
 /* Define to 1 if you have the <libewf.h> header file. */
 #undef HAVE_LIBEWF_H
 
-/* Define if using opensll */
-#undef HAVE_LIBOPENSSL
-
 /* Define to 1 if you have the `sqlite3' library (-lsqlite3). */
 #undef HAVE_LIBSQLITE3
 
@@ -84,6 +81,12 @@
 /* Define to 1 if you have the <libvmdk.h> header file. */
 #undef HAVE_LIBVMDK_H
 
+/* Define to 1 if you have the `vslvm' library (-lvslvm). */
+#undef HAVE_LIBVSLVM
+
+/* Define to 1 if you have the <libvslvm.h> header file. */
+#undef HAVE_LIBVSLVM_H
+
 /* Define to 1 if you have the `z' library (-lz). */
 #undef HAVE_LIBZ
 
@@ -97,9 +100,6 @@
 /* Define to 1 if you have the <map> header file. */
 #undef HAVE_MAP
 
-/* Define to 1 if you have the <memory.h> header file. */
-#undef HAVE_MEMORY_H
-
 /* Define if you have POSIX threads libraries and header files. */
 #undef HAVE_PTHREAD
 
@@ -121,6 +121,9 @@
 /* Define to 1 if you have the <stdint.h> header file. */
 #undef HAVE_STDINT_H
 
+/* Define to 1 if you have the <stdio.h> header file. */
+#undef HAVE_STDIO_H
+
 /* Define to 1 if you have the <stdlib.h> header file. */
 #undef HAVE_STDLIB_H
 
@@ -239,17 +242,14 @@
 	STACK_DIRECTION = 0 => direction of growth unknown */
 #undef STACK_DIRECTION
 
-/* Define to 1 if you have the ANSI C header files. */
+/* Define to 1 if all of the C90 standard headers exist (not just the ones
+   required in a freestanding environment). This macro is provided for
+   backward compatibility; new code need not use it. */
 #undef STDC_HEADERS
 
 /* Version number of package */
 #undef VERSION
 
-/* Enable large inode numbers on Mac OS X 10.5.  */
-#ifndef _DARWIN_USE_64_BIT_INODE
-# define _DARWIN_USE_64_BIT_INODE 1
-#endif
-
 /* Number of bits in a file offset, on hosts where this is settable. */
 #undef _FILE_OFFSET_BITS
 
