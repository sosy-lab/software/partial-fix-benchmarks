markdownlint: enable rule MD032

MD032 - Lists should be surrounded by blank lines
Fixed all errors

Signed-off-by: Maciej Wawryk <maciejx.wawryk@intel.com>
Change-Id: Idd18d38386c0150f5d68068a59717a34d4e1d991
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/9142
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
spdk_top: add clear to the tab area after switch

Adds a window clear for the tab area after switching to another tab.
This is to avoid possible artifacts from previous tab after switching.
Added a column description redraw after the clear.

Fixes #2053

Change-Id: I204410096e5f2401938a36a8f29a09904eca83a8
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/9312
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Paul Luse <paul.e.luse@intel.com>
Reviewed-by: Chengqiang Meng <chengqiangx.meng@intel.com>
Reviewed-by: Dong Yi <dongx.yi@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Community-CI: Mellanox Build Bot
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
spdk_top: change border line display in tab window

Currently we use box() function to draw borders of tab windows.
This may be causing jumbled display in some terminals, when displaying
vertical lines inside tab window (as seen in #2053), so this
patch aims to change this.

Additionally changed length value of vertical line separating
tab menu from data rows, to avoid going "outside" the screen.

Change-Id: Ie842bb8c958a6c3aebef81e3a0e9f7b3d74dff53
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/9415
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Community-CI: Mellanox Build Bot
Reviewed-by: Chengqiang Meng <chengqiangx.meng@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Ben Walker <benjamin.walker@intel.com>
spdk_top: change border line display in tab window

Currently we use box() function to draw borders of tab windows.
This may be causing jumbled display in some terminals, when displaying
vertical lines inside tab window (as seen in #2053), so this
patch aims to change this.

Additionally changed length value of vertical line separating
tab menu from data rows, to avoid going "outside" the screen.

Change-Id: Ie842bb8c958a6c3aebef81e3a0e9f7b3d74dff53
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/9415
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Community-CI: Mellanox Build Bot
Reviewed-by: Chengqiang Meng <chengqiangx.meng@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Ben Walker <benjamin.walker@intel.com>
