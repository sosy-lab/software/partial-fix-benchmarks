nvmf/vfio-user: clarify doorbells area naming

Rename ->doorbells to ->bar0_doorbells. This will help avoid confusion
later with shadow doorbells.

Signed-off-by: John Levon <john.levon@nutanix.com>
Change-Id: Id432938cfeb3033e79dc6e1b491dad964227687a
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/11788
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Community-CI: Mellanox Build Bot
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Thanos Makatos <thanos.makatos@nutanix.com>
Reviewed-by: Ben Walker <benjamin.walker@intel.com>
Reviewed-by: Changpeng Liu <changpeng.liu@intel.com>
nvme_ctrlr.c: Add error logs

Add NVME_CTRLR_ERRLOGs to nvme_ctrlr_process_init().
The main goal is to help with debugging #2201 issue.

Change-Id: I1ae6a9b30d6124dfe25eb7912402c37d476b0d4c
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/10627
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Shuhei Matsumoto <smatsumoto@nvidia.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
nvme_ctrlr.c: Add error logs

Add NVME_CTRLR_ERRLOGs to nvme_ctrlr_process_init().
The main goal is to help with debugging #2201 issue.

Change-Id: I1ae6a9b30d6124dfe25eb7912402c37d476b0d4c
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/10627
Community-CI: Broadcom CI <spdk-ci.pdl@broadcom.com>
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Shuhei Matsumoto <smatsumoto@nvidia.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
nvme: check CSTS.CFS when initializing ctrlrs

If Controller Fatal Status (CFS) bit is set, there's no point in waiting
for CSTS.RDY and the only way to move forward with the initialization is
to perform a controller reset.

This fixes issues with test/nvme/sw_hotplug.sh when running under qemu.
It seems that during that test, qemu marks the emulated NVMe drives as
fatal, so if we didn't check CSTS.CFS, the initialization would time
out.

Fixes #2201.

Signed-off-by: Konrad Sztyber <konrad.sztyber@intel.com>
Change-Id: I97712debc80c3dd6199545d393c0f340f29d33b2
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/13820
Community-CI: Mellanox Build Bot
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Michal Berger <michal.berger@intel.com>
Reviewed-by: Changpeng Liu <changpeng.liu@intel.com>
nvme: check CSTS.CFS when initializing ctrlrs

If Controller Fatal Status (CFS) bit is set, there's no point in waiting
for CSTS.RDY and the only way to move forward with the initialization is
to perform a controller reset.

This fixes issues with test/nvme/sw_hotplug.sh when running under qemu.
It seems that during that test, qemu marks the emulated NVMe drives as
fatal, so if we didn't check CSTS.CFS, the initialization would time
out.

Fixes #2201.

Signed-off-by: Konrad Sztyber <konrad.sztyber@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/13820 (master)

(cherry picked from commit a818564374fa2c1300d2a66f56d94a79a7366152)
Change-Id: I97712debc80c3dd6199545d393c0f340f29d33b2
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/14451
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Konrad Sztyber <konrad.sztyber@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
nvme: check CSTS.CFS when initializing ctrlrs

If Controller Fatal Status (CFS) bit is set, there's no point in waiting
for CSTS.RDY and the only way to move forward with the initialization is
to perform a controller reset.

This fixes issues with test/nvme/sw_hotplug.sh when running under qemu.
It seems that during that test, qemu marks the emulated NVMe drives as
fatal, so if we didn't check CSTS.CFS, the initialization would time
out.

Fixes #2201.

Signed-off-by: Konrad Sztyber <konrad.sztyber@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/13820 (master)

(cherry picked from commit a818564374fa2c1300d2a66f56d94a79a7366152)
Change-Id: I97712debc80c3dd6199545d393c0f340f29d33b2
Signed-off-by: Konrad Sztyber <konrad.sztyber@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/14382
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
hotplug: let device insertions/removals take priority

When running io_loop() let insertions and removals counters
take precedence before timeouts. This will let us determine
the cause of premature io_loop() exit in case of timeout.

Consider two scenarios:
First:
 1. Hotplug app records enough insertions/removals
 2. During "if (now > tsc_end)" check it also notices that
    it should terminate due to time constraint
 3. Hotplug exits using the timeout condition, because
    enough time has passed and no error is printed at
    this point.

Second:
 1. Hotplug app did not record enough insertions/removals
    during app runtime
 2. During "if (now > tsc_end)" check it also notices that
    it should terminate due to time constraint
 3. Hotplug exits using the timeout condition, because
    enough time has passed and no error is printed at
    this point.

After prioritizing the check on counters we avoid the first
scenario and timeout will be checked after the counters
were confirmed to be less than expected value, allowing us
to determine the cause of failure earlier.

Additionally added an errorlog to the body of the if checking
for timeout to signal this failure.

First part of the series fixing #2201.

Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Change-Id: I3f6f9c5c95e82e0a003419fcf181d858ffbd94dd
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/15964
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
sw_hotplug: avoid hotplug timeouts

Avoid hotplug application timeouts on machines
with multiple NVMe drives by scaling app run time
to number of NVMe drives.
Furthermore, change the way we wait for hotplug
app initialization by using "perform_tests" RPC,
and termination by starting it via timeout command.

Second part of the series fixing #2201.

Fixes #2201

Change-Id: Id82c8e8f6b9e870a55c4f43a11c755982855deeb
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/15965
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Konrad Sztyber <konrad.sztyber@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
test/nvme/hotplug: adjust sleep per loop on number of SSDs

The amount of time required by the hotplug app to
re-attach SSDs after insertion is dependent on the number
of SSDs per loop.

This patch helps fix issue #2201.

Signed-off-by: Jim Harris <james.r.harris@intel.com>
Change-Id: I63a00c33ad194eb07fdac9a7ef05c08d1cb81d10
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/18078
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Slawomir Ptak <slawomir.ptak@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Konrad Sztyber <konrad.sztyber@intel.com>
test/nvme/hotplug: adjust sleep per loop on number of SSDs

The amount of time required by the hotplug app to
re-attach SSDs after insertion is dependent on the number
of SSDs per loop.

This patch helps fix issue #2201.

Signed-off-by: Jim Harris <james.r.harris@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/18078 (master)

(cherry picked from commit c2550778b26882f26b08614a996c15d5f04dcd4a)
Change-Id: I63a00c33ad194eb07fdac9a7ef05c08d1cb81d10
Signed-off-by: Krzysztof Karas <krzysztof.karas@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/18505
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Konrad Sztyber <konrad.sztyber@intel.com>
