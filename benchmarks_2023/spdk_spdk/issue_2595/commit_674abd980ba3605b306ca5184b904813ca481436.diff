diff --git a/include/spdk/nvmf.h b/include/spdk/nvmf.h
index e57f6a7a039..dd45a80ad0b 100644
--- a/include/spdk/nvmf.h
+++ b/include/spdk/nvmf.h
@@ -304,6 +304,11 @@ int spdk_nvmf_qpair_disconnect(struct spdk_nvmf_qpair *qpair, nvmf_qpair_disconn
 /**
  * Get the peer's transport ID for this queue pair.
  *
+ * This function will first zero the trid structure, and then fill
+ * in the relevant trid fields to identify the listener. The relevant
+ * fields will depend on the transport, but the subnqn will never
+ * be a relevant field for purposes of this function.
+ *
  * \param qpair The NVMe-oF qpair
  * \param trid Output parameter that will contain the transport id.
  *
@@ -316,6 +321,11 @@ int spdk_nvmf_qpair_get_peer_trid(struct spdk_nvmf_qpair *qpair,
 /**
  * Get the local transport ID for this queue pair.
  *
+ * This function will first zero the trid structure, and then fill
+ * in the relevant trid fields to identify the listener. The relevant
+ * fields will depend on the transport, but the subnqn will never
+ * be a relevant field for purposes of this function.
+ *
  * \param qpair The NVMe-oF qpair
  * \param trid Output parameter that will contain the transport id.
  *
@@ -328,6 +338,11 @@ int spdk_nvmf_qpair_get_local_trid(struct spdk_nvmf_qpair *qpair,
 /**
  * Get the associated listener transport ID for this queue pair.
  *
+ * This function will first zero the trid structure, and then fill
+ * in the relevant trid fields to identify the listener. The relevant
+ * fields will depend on the transport, but the subnqn will never
+ * be a relevant field for purposes of this function.
+ *
  * \param qpair The NVMe-oF qpair
  * \param trid Output parameter that will contain the transport id.
  *
@@ -1145,8 +1160,12 @@ spdk_nvmf_transport_stop_listen(struct spdk_nvmf_transport *transport,
  * qpairs that are connected to the specified listener. Because
  * this function disconnects the qpairs, it has to be asynchronous.
  *
+ * The subsystem is matched using the subsystem parameter, not the
+ * subnqn field in the trid.
+ *
  * \param transport The transport associated with the listen address.
- * \param trid The address to stop listening at.
+ * \param trid The address to stop listening at. subnqn must be an empty
+ *             string.
  * \param subsystem The subsystem to match for qpairs with the specified
  *                  trid. If NULL, it will disconnect all qpairs with the
  *                  specified trid.
diff --git a/lib/nvmf/nvmf.c b/lib/nvmf/nvmf.c
index 1de0d4702f2..98d5a06b6dd 100644
--- a/lib/nvmf/nvmf.c
+++ b/lib/nvmf/nvmf.c
@@ -1301,6 +1301,7 @@ int
 spdk_nvmf_qpair_get_peer_trid(struct spdk_nvmf_qpair *qpair,
 			      struct spdk_nvme_transport_id *trid)
 {
+	memset(trid, 0, sizeof(*trid));
 	return nvmf_transport_qpair_get_peer_trid(qpair, trid);
 }
 
@@ -1308,6 +1309,7 @@ int
 spdk_nvmf_qpair_get_local_trid(struct spdk_nvmf_qpair *qpair,
 			       struct spdk_nvme_transport_id *trid)
 {
+	memset(trid, 0, sizeof(*trid));
 	return nvmf_transport_qpair_get_local_trid(qpair, trid);
 }
 
@@ -1315,6 +1317,7 @@ int
 spdk_nvmf_qpair_get_listen_trid(struct spdk_nvmf_qpair *qpair,
 				struct spdk_nvme_transport_id *trid)
 {
+	memset(trid, 0, sizeof(*trid));
 	return nvmf_transport_qpair_get_listen_trid(qpair, trid);
 }
 
diff --git a/lib/nvmf/transport.c b/lib/nvmf/transport.c
index fc23f364ee9..bf2f19868ad 100644
--- a/lib/nvmf/transport.c
+++ b/lib/nvmf/transport.c
@@ -423,6 +423,11 @@ spdk_nvmf_transport_stop_listen_async(struct spdk_nvmf_transport *transport,
 {
 	struct nvmf_stop_listen_ctx *ctx;
 
+	if (trid->subnqn[0] != '\0') {
+		SPDK_ERRLOG("subnqn should be empty, use subsystem pointer instead\n");
+		return -EINVAL;
+	}
+
 	ctx = calloc(1, sizeof(struct nvmf_stop_listen_ctx));
 	if (ctx == NULL) {
 		return -ENOMEM;
