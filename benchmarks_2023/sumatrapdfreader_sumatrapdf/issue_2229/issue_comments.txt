Crash Report, While opening a text book
I confirm that.

[crashinfo.zip](https://github.com/sumatrapdfreader/sumatrapdf/files/7461829/crashinfo.zip)
@kjk reproduced it again with the symbols. Look at this please.

https://gist.github.com/skipik/3c170145b3c1bfef74e30f2503e1ba60

![111](https://user-images.githubusercontent.com/24621645/140332324-e0fc08b2-ed51-4970-b3fe-157668d6616b.png)
This will maybe fix this. If not, I need a file and instructions to reproduce.
Nope, that didnt help.
https://www.dropbox.com/s/n2h83721e99w5oy/pdf_test.zip?dl=1
These are my pdf files.
I have SumatraPDF associated with PDF and just make double click on both of those files -- almost everytime I get a crash.
I can reproduce this when opening 2 both files from cmd-line. 

Must be sth. to do with fz_try/fz_rethrow in multiple threads:
```
>~*kb
Callstack for Thread 1 (Thread Id: 42316 (0xa54c)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]
 2      SumatraPDF.exe!logToPipe(std::basic_string_view<char,std::char_traits<char>> sv)
 3      SumatraPDF.exe!log(std::basic_string_view<char,std::char_traits<char>> s)
 4      SumatraPDF.exe!log(const char * s)
 5      SumatraPDF.exe!fz_print_cb(void * user, const char * msg)
 6      SumatraPDF.exe!fz_vwarn(fz_context * ctx, const char * fmt, char * ap)
 7      SumatraPDF.exe!fz_warn(fz_context * ctx, const char * fmt, ...)
 8      SumatraPDF.exe!ResolveLink(fz_context * ctx, fz_document * doc, const char * uri, float * xp, float * yp)
 9      [Inline Frame] SumatraPDF.exe!FzGetPageNo(fz_context *)
 10     SumatraPDF.exe!NewPageDestinationMupdf(fz_context * ctx, fz_document * doc, fz_link * link, fz_outline * outline)
 11     SumatraPDF.exe!EngineMupdf::BuildTocTree(TocItem * parent, fz_outline * outline, int & idCounter, bool isAttachment)
 12     SumatraPDF.exe!EngineMupdf::BuildTocTree(TocItem * parent, fz_outline * outline, int & idCounter, bool isAttachment)
 13     SumatraPDF.exe!EngineMupdf::BuildTocTree(TocItem * parent, fz_outline * outline, int & idCounter, bool isAttachment)
 14     SumatraPDF.exe!EngineMupdf::GetToc()
 15     SumatraPDF.exe!DisplayModel::GetToc()
 16     SumatraPDF.exe!Controller::HacToc()
 17     SumatraPDF.exe!SetSidebarVisibility(WindowInfo * win, bool tocVisible, bool showFavorites)
 18     SumatraPDF.exe!LoadDocIntoCurrentTab(const LoadArgs & args, Controller * ctrl, FileState * fs)
 19     SumatraPDF.exe!LoadDocument(LoadArgs & args)
 20     SumatraPDF.exe!LoadOnStartup(const wchar_t * filePath, const Flags & flags, bool isFirstWin)
 21     SumatraPDF.exe!WinMain(HINSTANCE__ * hInstance, HINSTANCE__ * hPrevInstance, char * cmdLine, int nCmdShow)
 22     [External Code]

Callstack for Thread 2 (Thread Id: 29692 (0x73fc)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]

Callstack for Thread 3 (Thread Id: 32580 (0x7f44)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]

Callstack for Thread 4 (Thread Id: 23032 (0x59f8)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]

Callstack for Thread 5 (Thread Id: 43012 (0xa804)):
 Index  Function
--------------------------------------------------------------------------------
 1      ntdll.dll!RtlRaiseStatus()
 2      ntdll.dll!RtlUnwindEx()
 3      ntdll.dll!RtlUnwind()
 4      SumatraPDF.exe!__longjmp_internal()
*5      SumatraPDF.exe!throw(fz_context * ctx, int code)
 6      SumatraPDF.exe!fz_rethrow(fz_context * ctx)
 7      SumatraPDF.exe!pdf_show_path(fz_context * ctx, pdf_run_processor * pr, int doclose, int dofill, int dostroke, int even_odd)
 8      SumatraPDF.exe!pdf_run_n(fz_context * ctx, pdf_processor * proc)
 9      SumatraPDF.exe!pdf_process_keyword(fz_context * ctx, pdf_processor * proc, pdf_csi * csi, fz_stream * stm, char * word)
 10     SumatraPDF.exe!pdf_process_stream(fz_context * ctx, pdf_processor * proc, pdf_csi * csi, fz_stream * stm)
 11     SumatraPDF.exe!pdf_process_contents(fz_context * ctx, pdf_processor * proc, pdf_document * doc, pdf_obj * rdb, pdf_obj * stmobj, fz_cookie * cookie)
 12     SumatraPDF.exe!pdf_run_page_contents_with_usage_imp(fz_context * ctx, pdf_document * doc, pdf_page * page, fz_device * dev, fz_matrix ctm, const char * usage, fz_cookie * cookie)
 13     SumatraPDF.exe!pdf_run_page_with_usage(fz_context * ctx, pdf_page * page, fz_device * dev, fz_matrix ctm, const char * usage, fz_cookie * cookie)
 14     SumatraPDF.exe!EngineMupdf::RenderPage(RenderPageArgs & args)
 15     SumatraPDF.exe!RenderCache::RenderCacheThread(void * data)
 16     [External Code]
```
or:
```
Callstack for Thread 1 (Thread Id: 8428 (0x20ec)):
 Index  Function
--------------------------------------------------------------------------------
 1      SumatraPDF.exe!pdf_to_num(fz_context * ctx, pdf_obj * obj)
 2      SumatraPDF.exe!pdf_resolve_indirect(fz_context * ctx, pdf_obj * ref)
 3      SumatraPDF.exe!pdf_resolve_indirect_chain(fz_context * ctx, pdf_obj * ref)
 4      SumatraPDF.exe!pdf_dict_get(fz_context * ctx, pdf_obj * obj, pdf_obj * key)
 5      SumatraPDF.exe!pdf_dict_get_inheritable(fz_context * ctx, pdf_obj * node, pdf_obj * key)
 6      SumatraPDF.exe!pdf_page_obj_transform(fz_context * ctx, pdf_obj * pageobj, fz_rect * page_mediabox, fz_matrix * page_ctm)
 7      SumatraPDF.exe!pdf_resolve_link(fz_context * ctx, pdf_document * doc, const char * uri, float * xp, float * yp)
 8      SumatraPDF.exe!pdf_resolve_link_imp(fz_context * ctx, fz_document * doc_, const char * uri, float * xp, float * yp)
 9      SumatraPDF.exe!fz_resolve_link(fz_context * ctx, fz_document * doc, const char * uri, float * xp, float * yp)
 10     SumatraPDF.exe!ResolveLink(fz_context * ctx, fz_document * doc, const char * uri, float * xp, float * yp)
 11     [Inline Frame] SumatraPDF.exe!FzGetPageNo(fz_context *)
 12     SumatraPDF.exe!NewPageDestinationMupdf(fz_context * ctx, fz_document * doc, fz_link * link, fz_outline * outline)
 13     SumatraPDF.exe!EngineMupdf::BuildTocTree(TocItem * parent, fz_outline * outline, int & idCounter, bool isAttachment)
 14     SumatraPDF.exe!EngineMupdf::BuildTocTree(TocItem * parent, fz_outline * outline, int & idCounter, bool isAttachment)
 15     SumatraPDF.exe!EngineMupdf::GetToc()
 16     SumatraPDF.exe!DisplayModel::GetToc()
 17     SumatraPDF.exe!Controller::HacToc()
 18     SumatraPDF.exe!SetSidebarVisibility(WindowInfo * win, bool tocVisible, bool showFavorites)
 19     SumatraPDF.exe!LoadDocIntoCurrentTab(const LoadArgs & args, Controller * ctrl, FileState * fs)
 20     SumatraPDF.exe!LoadDocument(LoadArgs & args)
 21     SumatraPDF.exe!LoadOnStartup(const wchar_t * filePath, const Flags & flags, bool isFirstWin)
 22     SumatraPDF.exe!WinMain(HINSTANCE__ * hInstance, HINSTANCE__ * hPrevInstance, char * cmdLine, int nCmdShow)
 23     [External Code]

Callstack for Thread 2 (Thread Id: 35176 (0x8968)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]

Callstack for Thread 3 (Thread Id: 18820 (0x4984)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]

Callstack for Thread 4 (Thread Id: 32148 (0x7d94)):
 Index  Function
--------------------------------------------------------------------------------
 1      [External Code]

Callstack for Thread 5 (Thread Id: 36492 (0x8e8c)):
 Index  Function
--------------------------------------------------------------------------------
 1      ntdll.dll!RtlRaiseStatus()
 2      ntdll.dll!RtlUnwindEx()
 3      ntdll.dll!RtlUnwind()
 4      SumatraPDF.exe!__longjmp_internal()
 5      SumatraPDF.exe!throw(fz_context * ctx, int code)
 6      SumatraPDF.exe!fz_rethrow(fz_context * ctx)
 7      SumatraPDF.exe!pdf_dict_get_inheritable(fz_context * ctx, pdf_obj * node, pdf_obj * key)
 8      SumatraPDF.exe!pdf_page_obj_transform(fz_context * ctx, pdf_obj * pageobj, fz_rect * page_mediabox, fz_matrix * page_ctm)
 9      SumatraPDF.exe!pdf_page_transform(fz_context * ctx, pdf_page * page, fz_rect * page_mediabox, fz_matrix * page_ctm)
 10     SumatraPDF.exe!pdf_run_page_contents_with_usage_imp(fz_context * ctx, pdf_document * doc, pdf_page * page, fz_device * dev, fz_matrix ctm, const char * usage, fz_cookie * cookie)
 11     SumatraPDF.exe!pdf_run_page_with_usage(fz_context * ctx, pdf_page * page, fz_device * dev, fz_matrix ctm, const char * usage, fz_cookie * cookie)
*12     SumatraPDF.exe!EngineMupdf::RenderPage(RenderPageArgs & args)
 13     SumatraPDF.exe!RenderCache::RenderCacheThread(void * data)
```

It's the same `ctx`, Should take a lock in `ResolveLink` ?

Thanks for the repro documents, should be really fixed now