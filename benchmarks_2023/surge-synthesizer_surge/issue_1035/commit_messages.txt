Self-modulation to the Sin OSC with shapes (#1033)

The sin OSC gets self modulation (so self-FM) with feedback
between 0 and 1.

Closes #1030
Fix up the Sin osc FM channel

The Sin osc FM channel was totally different than
the FM2 one in a way which was wierdly inconsistent.
So choose to change the behavior of SIN in FM mode.
This may impact the sound of some patches.

Closes #1035
Fix up the Sin osc FM channel (#1036)

The Sin osc FM channel was totally different than
the FM2 one in a way which was wierdly inconsistent.
So choose to change the behavior of SIN in FM mode.
This may impact the sound of some patches.

Addresses #1035
Tuning Patches, Status Panel, and Patch Versions

This commit sets up a display of statuses in the synth so we
can bring to the front thigns like MPE and Tuning and bind the
menus more comprehensively. It also brings to bear the ability
(optionally) to store a tuning in a patch and the correct state
machine for what to do when loading a patch with tuning with
tuning active.

It almost completely addresses the remainder of #828
Addresses the streaming incompatability in #1035

Still outstanding is optionally a "lock" and to apply
the streaming version option to FX (which is #1037)

Handle streaming revision parameter set changes in OSCes. Deals with
Tuning Patches, Status Panel, and Patch Versions (#1038)

This commit sets up a display of statuses in the synth so we
can bring to the front thigns like MPE and Tuning and bind the
menus more comprehensively. It also brings to bear the ability
(optionally) to store a tuning in a patch and the correct state
machine for what to do when loading a patch with tuning with
tuning active.

It almost completely addresses the remainder of #828
Addresses the streaming incompatability in #1035

Still outstanding is optionally a "lock" and to apply
the streaming version option to FX (which is #1037)

Handle streaming revision parameter set changes in OSCes. Deals with
SIN backwards compatibility

The changes to Sin to make the FM feedback and the like work
properly were too big a change for old patches. So add a mode
where FM is legacy or consistent mode. In legacy mode we exactly
match 1.6.1.1 and earlier; old patches load in legacy mode automatically.
New instances load in consistent mode. Streamed patches at version
11 (this version) and higher save properly.

Addresses #1035
SIN backwards compatibility (#1056)

The changes to Sin to make the FM feedback and the like work
properly were too big a change for old patches. So add a mode
where FM is legacy or consistent mode. In legacy mode we exactly
match 1.6.1.1 and earlier; old patches load in legacy mode automatically.
New instances load in consistent mode. Streamed patches at version
11 (this version) and higher save properly.

Addresses #1035
Only apply Streaming fixes when Streaming

Fixes the mis-application of change in #1035 by making sure the
chance to handle old streaming versions only occurs when, you know,
you are actually streaming!

Closes #1035
Only apply Streaming fixes when Streaming (#1070)

Fixes the mis-application of change in #1035 by making sure the
chance to handle old streaming versions only occurs when, you know,
you are actually streaming!

Closes #1035
