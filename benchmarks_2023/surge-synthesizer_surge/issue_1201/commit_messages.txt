More Memory Cleanups for Typeins (#1879)

Linux really helped me find these bugs. In this case the stay-open
on-bad-value was mis-managing memory so just tear down and rebuild.

Addresses #1071
Change Modulator names for Step/Env

Closes #1201
Change Modulator names for Step/Env

Closes #1201

Additionally activate the edit menu from source not just target
Addresses #1071

f
Change Modulator names for Step/Env (#1881)

Closes #1201

Additionally activate the edit menu from source not just target
Addresses #1071

f
Fix a variety of Env Rename bugs found by the crew

Closes #1201 again
Fix a variety of Env Rename bugs found by the crew (#1882)

Closes #1201 again
Don't clobber control names

Control names are set by the synth based on user actions and
mappings. Don't clobber them with the new env remap loop.

Closes #1201. Again
Don't clobber control names (#1884)

Control names are set by the synth based on user actions and
mappings. Don't clobber them with the new env remap loop.

Closes #1201. Again
