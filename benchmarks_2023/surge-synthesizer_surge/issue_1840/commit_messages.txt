Force a more aggressive update on patch change (#1842)

On patch change the synth properly invalidates, but
FL20 with VST2 has some sort of wierd thread-order bug
that means it didn't catch the update. Only FL20 and only
VST2. But hammer a change request at the end of patch
udpate for all hosts just to be sure.

Closes #817 as well, I bet
Kill ZoomKeys; fix Tab focus; move Save dialog

Kill the zoomkeys which interfered with all sorts of stuff. Closes #1841
Make sure tab works with the typein up.
Move the store window down a tad. Addresses #1840
Kill ZoomKeys; fix Tab focus; move Save dialog (#1843)

Kill the zoomkeys which interfered with all sorts of stuff. Closes #1841
Make sure tab works with the typein up.
Move the store window down a tad. Addresses #1840
Initial version of category typeahead

The primary thing this accomplishes is give us a typeahead
widget, which still needs work for skinning, font, etc...
but which takes a data provider and does reductive typeahead
with mouse gestures to select.

I used it in the user category list in the Patch Selector for now

Addresses #1840
Initial version of category typeahead

The primary thing this accomplishes is give us a typeahead
widget, which still needs work for skinning, font, etc...
but which takes a data provider and does reductive typeahead
with mouse gestures to select.

I used it in the user category list in the Patch Selector for now

Addresses #1840
Initial version of category typeahead (#5185)

The primary thing this accomplishes is give us a typeahead
widget, which still needs work for skinning, font, etc...
but which takes a data provider and does reductive typeahead
with mouse gestures to select.

I used it in the user category list in the Patch Selector for now

Addresses #1840
Start of Skinning TypeAhead

Start putting some skin / color support into typeahead
Finish off the Category typeahead as a result

So closes #1840
Start of Skinning TypeAhead (#5204)

Start putting some skin / color support into typeahead
Finish off the Category typeahead as a result

So closes #1840
