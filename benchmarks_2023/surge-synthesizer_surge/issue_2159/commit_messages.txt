RingModulator Phase Overflow (#2171)

When pitch is modulated out of range the RM phase could overflow.
Simply reduce it by the integral part rather than 1.
Linux VST3 fix

Linux VST3 Juce hosts would sometimes stall a draw on open. This
is because the invalidation event was processed before the show
somewhere. Lots of ways I could debug and fix this I'm sure but
the way I chose was to just force a call to frame->invalid()
in the first run through the idle loop.

Also fixed some printf bounds for ubuntu 20.

Closes #2159
Linux VST3 fix (#2173)

Linux VST3 Juce hosts would sometimes stall a draw on open. This
is because the invalidation event was processed before the show
somewhere. Lots of ways I could debug and fix this I'm sure but
the way I chose was to just force a call to frame->invalid()
in the first run through the idle loop.

Also fixed some printf bounds for ubuntu 20.

Closes #2159
More host idle startup nonsense for Linux

Linux VST3 in Carla with JUCE misses the first runloop.
Instead of debugging, just invalidate twice at open, and
limit this behavior to linux VST3

Closes #2159
More host idle startup nonsense for Linux (#2185)

Linux VST3 in Carla with JUCE misses the first runloop.
Instead of debugging, just invalidate twice at open, and
limit this behavior to linux VST3

Closes #2159
