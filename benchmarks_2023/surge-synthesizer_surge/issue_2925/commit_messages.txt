A bunch more MSEG bits and bobs (#2924)

- Correct sign convention on line deform
- Remove spurious asset 307
- PolyLimit param default matches SurgePatch starting point. Closes #2917
- Update vertical label when type changes. Closes #2918
- Linear Drag near the edges is smoothed
- No longer explode with drag past 19 (but still have thining issues). Closes #2920
- S Curve is way more responsive in -ve range; responds to deform
Fix a couple of crash-on-close scenarios

1. The AU could get the editor cleaned up underneath it
2. The MSEG being on led to a double delete on close/reopen/close

Closes #2925
Fix a couple of crash-on-close scenarios (#2926)

1. The AU could get the editor cleaned up underneath it
2. The MSEG being on led to a double delete on close/reopen/close

Closes #2925
Don't draw empty path; and forget the path (#2939)

Fixes a Win81 crash. Closes #2925
