New cursor hiding behavior makes double-clicking macros to reset difficult
Since a couple of days, this behavior came back. It now does the same thing as described originally. 

"Maybe this was caused by a recent Linux fix?" -Paul
Sorry so I just tried it 50 times on my mac at current head and no problem. Double click resets every time.

What exactly is the current problem?

As described above, if you click on the slider tray instead of directly on the header, it doesn't reset. Instead, the first click puts the cursor on the slider, and the second is considered like a single-click. You need to click 3 times for it to effectively reset. This was fixed in #2942 but came back a couple of days after.

Here's a GIF demonstrating it:
![reset](https://user-images.githubusercontent.com/50145178/97447897-6cc0b980-1906-11eb-9398-8673dd8b22b8.gif)


I'm on Windows 10 x64.

Hope that helps!
OK I’m gonna need to send you a debugging branch since Evil and I don’t see that behavior. Something in your mouse and the event stream is confusing the slider I think - that behavior is consistent with your mouse issueing a drag event after the first click of a double click oddly. I’ll stare at the code and ponder for a bit.
OK just tested in the latest nightly. It seems now that it's inconsistent, as of sometimes it does it, sometime it doesn't. Hard to tell what the cause is..
On regular sliders, this doesn't happen to me, but on assigned macros, it does. The first double click just makes the cursor jump to the current slider position, only the second one resets it.

Version 1.8.nightly.86f3af6 (Intel 64-bit Windows VST3 in Ableton Live 10.1. Built 2020-10-27 at 20:52:13 on pipeline host 'WIN-FUUP911EET4')
Oh the controls is a separate codepath I didn’t inspect 
I can confirm that it now seems fixed for sliders, but it still happens for macros.
OK that's good! I need to apply the same fix to macros. Maybe I'll do that now.
Fix inbound
Sorry for reopening this. Unfortunately this still happens with Macros even after that last fix. Can you confirm @TheNerdyMusicGuy ?