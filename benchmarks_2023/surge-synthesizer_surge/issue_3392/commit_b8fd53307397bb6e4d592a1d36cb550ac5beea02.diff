diff --git a/src/common/dsp/FilterCoefficientMaker.cpp b/src/common/dsp/FilterCoefficientMaker.cpp
index 5033ec59b97..e4ab4258d83 100644
--- a/src/common/dsp/FilterCoefficientMaker.cpp
+++ b/src/common/dsp/FilterCoefficientMaker.cpp
@@ -131,7 +131,7 @@ void FilterCoefficientMaker::MakeCoeffs(
    case fut_nonlinearfb_n:
    case fut_nonlinearfb_bp:
    case fut_nonlinearfb_ap:
-      NonlinearFeedbackFilter::makeCoefficients(this, Freq, Reso, Type, storageI);
+      NonlinearFeedbackFilter::makeCoefficients(this, Freq, Reso, Type, SubType, storageI);
       break;
    case fut_nonlinearst_lp:
    case fut_nonlinearst_hp:
diff --git a/src/common/dsp/filters/NonlinearFeedback.cpp b/src/common/dsp/filters/NonlinearFeedback.cpp
index 3362206b958..4dc6889167c 100644
--- a/src/common/dsp/filters/NonlinearFeedback.cpp
+++ b/src/common/dsp/filters/NonlinearFeedback.cpp
@@ -81,6 +81,7 @@ static inline __m128 doNLFilter(
       const __m128 b0,
       const __m128 b1,
       const __m128 b2,
+      const __m128 makeup,
       const int sat,
       __m128 &z1,
       __m128 &z2)
@@ -110,7 +111,7 @@ static inline __m128 doNLFilter(
    z1 = A(z2, S(M(b1, input), M(a1, nf)));
    // z2 = b2 * input - a2 * nf
    z2 = S(M(b2, input), M(a2, nf));
-   return out;
+   return M(out, makeup);
 }
 
 namespace NonlinearFeedbackFilter
@@ -121,6 +122,7 @@ namespace NonlinearFeedbackFilter
       nlf_b0,
       nlf_b1,
       nlf_b2,
+      nlf_makeup,
       n_nlf_coeff
    };
 
@@ -135,13 +137,14 @@ namespace NonlinearFeedbackFilter
       nlf_z8, // 2nd z-1 state for fourth stage
    };
 
-   void makeCoefficients( FilterCoefficientMaker *cm, float freq, float reso, int type, SurgeStorage *storage )
+   void makeCoefficients( FilterCoefficientMaker *cm, float freq, float reso, int type, int subtype, SurgeStorage *storage )
    {
       float C[n_cm_coeffs];
 
       const float q = ((reso * reso * reso) * 18.0f + 0.1f);
 
-      const float wc = 2.0f * M_PI * clampedFrequency(freq, storage) / dsamplerate_os;
+      const float normalisedFreq = clampedFrequency(freq, storage) / dsamplerate_os;
+      const float wc = 2.0f * M_PI * normalisedFreq;
 
       const float wsin  = Surge::DSP::fastsin(wc);
       const float wcos  = Surge::DSP::fastcos(wc);
@@ -153,17 +156,57 @@ namespace NonlinearFeedbackFilter
 
       C[nlf_a1] = -2.0f * wcos    * a0r;
       C[nlf_a2] = (1.0f - alpha)  * a0r;
+      C[nlf_makeup] = 1.0f;
+
+      /*
+       * To see where this table comes from look in the HeadlessNonTestFunctions.
+       */
+      const bool useNormalization = true;
+      float normNumerator = 1.0f;
+      const float lpNormTable[12] = {
+          1.12215,
+          0.946168,
+          0.854176,
+          0.803073,
+          0.852984,
+          0.685058,
+          0.598567,
+          0.574069,
+          0.555141,
+          0.2764,
+          0.234219,
+          0.235227
+      };
+      const float hpNormTable[12] = {
+          4.18885,
+          3.11128,
+          2.69236,
+          2.06871,
+          3.16804,
+          2.36259,
+          2.1021,
+          1.64437,
+          1.44636,
+          1.3442,
+          1.19402,
+          1.07743
+      };
+
 
       switch(type){
          case fut_nonlinearfb_lp: // lowpass
+            if (useNormalization) normNumerator = lpNormTable[subtype];
             C[nlf_b1] =  (1.0f - wcos) * a0r;
             C[nlf_b0] = C[nlf_b1] *  0.5f;
             C[nlf_b2] = C[nlf_b0];
+            C[nlf_makeup] = normNumerator / std::pow(std::max(normalisedFreq, 0.001f), 0.333f);
             break;
          case fut_nonlinearfb_hp: // highpass
+            if (useNormalization) normNumerator = hpNormTable[subtype];
             C[nlf_b1] = -(1.0f + wcos) * a0r;
             C[nlf_b0] = C[nlf_b1] * -0.5f;
             C[nlf_b2] = C[nlf_b0];
+            C[nlf_makeup] = normNumerator / std::pow(std::max(1.0f - normalisedFreq, 0.001f), 0.333f);
             break;
          case fut_nonlinearfb_n: // notch
             C[nlf_b0] = a0r;
@@ -201,6 +244,7 @@ namespace NonlinearFeedbackFilter
                   f->C[nlf_b0],
                   f->C[nlf_b1],
                   f->C[nlf_b2],
+                  f->C[nlf_makeup],
                   sat,
                   f->R[nlf_z1 + stage*2],
                   f->R[nlf_z2 + stage*2]);
diff --git a/src/common/dsp/filters/NonlinearFeedback.h b/src/common/dsp/filters/NonlinearFeedback.h
index 197788eb24f..267dbb30b4e 100644
--- a/src/common/dsp/filters/NonlinearFeedback.h
+++ b/src/common/dsp/filters/NonlinearFeedback.h
@@ -6,6 +6,6 @@ class SurgeStorage;
 
 namespace NonlinearFeedbackFilter 
 {
-   void makeCoefficients( FilterCoefficientMaker *cm, float freq, float reso, int subtype, SurgeStorage *storage );
+   void makeCoefficients( FilterCoefficientMaker *cm, float freq, float reso, int type, int subtype, SurgeStorage *storage );
    __m128 process( QuadFilterUnitState * __restrict f, __m128 in );
 }
diff --git a/src/headless/HeadlessNonTestFunctions.cpp b/src/headless/HeadlessNonTestFunctions.cpp
index 028c8d41c0b..b7bac9196b1 100644
--- a/src/headless/HeadlessNonTestFunctions.cpp
+++ b/src/headless/HeadlessNonTestFunctions.cpp
@@ -210,6 +210,128 @@ void filterAnalyzer( int ft, int sft, float cut )
    }
 }
 
+void generateNLFeedbackNorms()
+{
+   /*
+    * This generates a normalization for the NL feedback. Here's how you use it
+    *
+    * In filters/NonlinearFeedback.cpp if there's a normalization table there
+    * disable it and make sure the law is just 1/f^1/3
+    *
+    * Build and run this with surge-headless --non-test --generate-nlf-norms
+    *
+    * Copy and paste the resulting table back into NLFB.cpp
+    *
+    * What it does is: Play a C major chord on init into the OBXD-24 LP for 3 seconds
+    * with resonance at 1, calculate the RMS
+    *
+    * Then do that for every subtype of NLF and calculate the RMS
+    *
+    * Then print out ratio basically.
+    */
+
+   bool genLowpass = false; // if false generate highpass
+
+   auto setup = [](int type, int subtype) {
+      auto surge = Surge::Headless::createSurge(48000);
+      for (auto i = 0; i < 10; ++i)
+      {
+         surge->process();
+      }
+      surge->storage.getPatch().scene[0].filterunit[0].type.val.i = type;
+      surge->storage.getPatch().scene[0].filterunit[0].subtype.val.i = subtype;
+      surge->storage.getPatch().scene[0].filterunit[0].cutoff.val.f = -9;
+      surge->storage.getPatch().scene[0].filterunit[0].resonance.val.f = 0.95;
+      for (auto i = 0; i < 10; ++i)
+      {
+         surge->process();
+      }
+      char td[256], sd[256], cd[256];
+
+      surge->storage.getPatch().scene[0].filterunit[0].type.get_display(td);
+      surge->storage.getPatch().scene[0].filterunit[0].subtype.get_display(sd);
+      surge->storage.getPatch().scene[0].filterunit[0].cutoff.get_display(cd);
+      std::cout << "// Created a filter: " << td << " / " << sd << " Cutoff=" << cd  << std::endl;
+      return surge;
+   };
+
+   auto singleRMS = [genLowpass](std::shared_ptr<SurgeSynthesizer> surge)
+   {
+      for (auto i=0;i<10; ++i ) {
+         surge->process();
+      }
+
+      std::vector<int> notes = {{ 48, 60, 65, 67 }};
+      int noff = -12;
+      if( ! genLowpass ) noff = +15;
+      for( auto n : notes )
+      {
+         surge->playNote( 0, n-noff, 127, 0 );
+         surge->process();
+      }
+
+      for( int i=0; i<100; ++i )
+         surge->process();
+
+      int blocks = (floor)( 2.0 * samplerate * BLOCK_SIZE_INV );
+      double rms = 0; // double to avoid so much overflow risk
+      for( int i=0; i<blocks; ++i )
+      {
+         surge->process();
+         for( int s=0; s<BLOCK_SIZE; ++s )
+         {
+            rms += surge->output[0][s] * surge->output[0][s] + surge->output[1][s] * surge->output[1][s];
+         }
+      }
+
+      for( auto n : notes )
+      {
+         surge->releaseNote(0,n-noff,0);
+         surge->process();
+      }
+      for( int i=0; i<100; ++i )
+         surge->process();
+
+      return sqrt(rms);
+   };
+
+   auto playRMS = [singleRMS](std::shared_ptr<SurgeSynthesizer> surge)
+   {
+      float ra = 0;
+      int nAvg = 20;
+
+      for( int i=0; i<nAvg; ++i )
+      {
+         float lra = singleRMS(surge);
+         std::cout << "LRA = " << lra << std::endl;
+         ra += lra;
+      }
+      return ra / nAvg;
+   };
+
+   // Remember if you don't set the normalization to FALSE in NLF.cpp this will be bad
+   auto basecaseRMS = playRMS(setup(genLowpass ? fut_obxd_4pole : fut_hp24,  genLowpass ? 3 : 0));
+   std::cout << "// no filter RMS is " << basecaseRMS << std::endl;
+
+   std::vector<float> results;
+   for( int nlfst = 0; nlfst < 12; nlfst++ )
+   {
+      auto r = playRMS( setup( genLowpass ? fut_nonlinearfb_lp : fut_nonlinearfb_hp, nlfst ));
+      //auto r = playRMS( setup( fut_nonlinearfb_hp, nlfst));
+      std::cout << "// RMS=" << r << " Ratio=" << r / basecaseRMS << std::endl;
+      results.push_back(r);
+   }
+   std::cout << "      bool useNormalization = true;\n"
+             << "      float normNumerator = 1.0f;\n"
+             << "      float " << ( genLowpass ? "lp" : "hp" ) << "NormTable[12] = {";
+   std::string pfx = "\n         ";
+   for( auto v : results ) {
+      std::cout << pfx << basecaseRMS / v;
+      pfx = ",\n         ";
+   }
+   std::cout << "\n       };\n"
+             << "       if (useNormalization) normNumerator = lpNormTable[subtype];\n";
+}
 
 }
 }
diff --git a/src/headless/HeadlessNonTestFunctions.h b/src/headless/HeadlessNonTestFunctions.h
index 244209897c1..33e62d75c90 100644
--- a/src/headless/HeadlessNonTestFunctions.h
+++ b/src/headless/HeadlessNonTestFunctions.h
@@ -10,6 +10,7 @@ void restreamTemplatesWithModifications();
 void statsFromPlayingEveryPatch();
 void playSomeBach();
 void filterAnalyzer( int ft, int fst, float cut = 12 /* in notes from note 69 */ );
+void generateNLFeedbackNorms();
 }
 }
 }
diff --git a/src/headless/main.cpp b/src/headless/main.cpp
index 50f15d83ea2..0959d7b70d6 100644
--- a/src/headless/main.cpp
+++ b/src/headless/main.cpp
@@ -32,6 +32,10 @@ int main(int argc, char** argv)
          {
             Surge::Headless::NonTest::restreamTemplatesWithModifications();
          }
+         if( strcmp( argv[2], "--generate-nlf-norms" ) == 0 )
+         {
+            Surge::Headless::NonTest::generateNLFeedbackNorms();
+         }
          if( strcmp( argv[2], "--filter-analyzer" ) == 0 )
          {
             if( argc < 4 )
