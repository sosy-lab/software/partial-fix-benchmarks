Overlays don't stay fronted on skin refresh
Reopening for one edge case:

1. Open MSEG editor
2. Click patch save button
3. F5 to refresh skin

Result: MSEG goes in front of patch save (which is supposedly modal)

Expected: patch save is modal so should be always on top.

Miniedit is also modal but they autoclose on skin refresh so we need not worry about them. But patch save should be always on top.
yeah that one may be hard to fix. the z-order on skin refresh is lost
this is all related to #5512 basically
the mseg/formula is 'special' in an annoying way and i want to make it non special and the skin refresh actually replaces components.
this edge case may bounce out of the xt milestone basically

(note as of last night you can change Z order by clicking on a title bar)
Managed to fix this extreme edge case in XT1. When you press F5 with a patch over the mseg, the mseg blinks through (fixing that required #5512) but then the modal overlay re-fronts. Will merge that and close this issue and the blink through will get fixed when #5512 gets done