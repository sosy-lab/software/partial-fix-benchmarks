MTS-ESP and Portamento
Oh thank you.
Can you let me know which voice mode you are in when you here this?
I have an idea.... but it may be different in mono_fp vs mono_st etc...

(my guess by the way is the 'portable starting point' is not key adjusted - and in theory the same thing may happen if you use native with tune before midi)
yup here's another way to repro

1. Load init sine
2. Load a long scale like ED2-22
3. Set the tuning mode to "tune after modulation"
4. Set porta to 2 seconds
5. play a high note like C6. You may hear an initial glide from wherever but repeated notes after that will be fine
6. Set the tuning mode to "tune at midi input"
7. Play a high note like C6. Each and every time you get a portable

So I think what's happening is the 'retune at midi input' (which really means 'remap the keyed and stay internally in 12-tet) which is also how MTS-ESP works is setting the portamento target on note on to the unmapped key not the mapped key.

Great great find.  
oh and in theory a 12 tone scale with some notes "very" off tune (so like the cents are 0 1 2 3 4 5 6 7 8 9 10 1200) should exhibit a similar problem
Thanks for the confirmation and explanation. That's really interesting that it also happens in SCL-KBM mode. Hadn't noticed that yet, so good mutual catches.

Voice mode was Poly during that test. Let me know if there's anything else I can check here.
Nope I have what I need. I’ll tag this issue when there’s a fix, thanks!
OK i have this trapped in the regtest engine. Good. 
And then it was a small fix. Inbound.
> And then it was a small fix. Inbound.

Fantastic news. Glad it was an easy fix and thanks for it. 
Oh my fix may only fix native not MTS so reopening until I confirm
Yes, just noticed this going through my testing routine this morning. Thanks for the confirmation here.
Should add from beta-testing notes:

Still happening in build d66cc10 with MTS-ESP Master loaded for all Play Modes, except Mono ST+FP.
What follows should perhaps go in a separate issue, but pasting it here for related interest:

**Related finding for both microtuning modes: With Glissando and Retrigger At Scale Degrees checked, the degrees appear to be related to 12-equal. Tested with ED2-31 and a glide across 31 MIDI Notes, plays 12 retriggered notes.**

[Let me know if I should create a new issue for this one, since it's actually not directly related to the one-note portamento behavior with MTS-ESP.]
that's a separate issue and is trickier. In RETUNE_AT_MIDI/MTS mode portamento is in key space so retrievers when you run over a key. If you work in RETUNE_AFTER_MODULATION it should work. But we should open a separate issue, since I have this particular one closed. I'll do that now.
#5483 
Anyway the MTS-equivalent fix that I made for native on portamento is in now. This will close again when it's merged. This time I actually tested it with MTS not just native!

(wish: MTS could run in the test harness. Sigh.)
Thanks for the quick fix. Once the new build is up, I'll double check it here again too. 