A Dirty Flag, but not much done with it (#5909)

This adds a dirty flag to the patch which lets you see when you
have modified it. We currently don't *do* antyhing with the flag
other than put an * by the patch name, but in the future we can do
quite a lot.

But since we just show it this only addresses part of #733
Handle PolyAT on non-0 channel

We were mis-masking the channels for poly at so poly at
on channel other than 0 was ignored.

Closes #5908
Handle PolyAT on non-0 channel (#5910)

We were mis-masking the channels for poly at so poly at
on channel other than 0 was ignored.

Closes #5908
"Use MIDI Channel for Octave Shift" grayed out if MPE active

Addresses #5908 (doesn't fix the underlying issue, this is just visual prep)
"Use MIDI Channel for Octave Shift" grayed out if MPE active (#6040)

Addresses #5908 (doesn't fix the underlying issue, this is just visual prep)
Remove the MPE/Channel per Octave conflict

If both are on, MPE wins

Closes #5908
Remove the MPE/Channel per Octave conflict (#6088)

If both are on, MPE wins

Closes #5908
