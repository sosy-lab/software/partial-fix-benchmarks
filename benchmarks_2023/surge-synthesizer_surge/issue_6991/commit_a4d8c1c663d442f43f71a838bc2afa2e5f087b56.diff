diff --git a/libs/sst/sst-basic-blocks b/libs/sst/sst-basic-blocks
index 829586779e6..8ab0c341c5b 160000
--- a/libs/sst/sst-basic-blocks
+++ b/libs/sst/sst-basic-blocks
@@ -1 +1 @@
-Subproject commit 829586779e690d30a0fb4222d2bacdca217c62e4
+Subproject commit 8ab0c341c5bdccb11115a1b63d7f6a2152a14352
diff --git a/libs/sst/sst-effects b/libs/sst/sst-effects
index c7f270cf859..0f29ca4c2fb 160000
--- a/libs/sst/sst-effects
+++ b/libs/sst/sst-effects
@@ -1 +1 @@
-Subproject commit c7f270cf85900b11f12d2899744c87e1ae6e0e18
+Subproject commit 0f29ca4c2fb82cf1704ea6dfbdc50b6838d2f849
diff --git a/src/common/Parameter.cpp b/src/common/Parameter.cpp
index b6c109ace6f..42c25009f77 100644
--- a/src/common/Parameter.cpp
+++ b/src/common/Parameter.cpp
@@ -32,6 +32,7 @@
 #include <cctype>
 #include <utility>
 #include <UserDefaults.h>
+#include <variant>
 #include "DebugHelpers.h"
 #include "StringOps.h"
 #include "Tunings.h"
@@ -557,6 +558,8 @@ void Parameter::set_type(int ctrltype)
     dynamicBipolar = nullptr;
     dynamicDeactivation = nullptr;
 
+    basicBlocksParamMetaData = {};
+
     /*
     ** Note we now have two ctrltype switches. This one sets ranges
     ** and, grouped below, we set display info
@@ -2312,6 +2315,42 @@ void Parameter::get_display_of_modulation_depth(char *txt, float modulationDepth
             Surge::Storage::getUserDefaultValue(storage, Surge::Storage::HighPrecisionReadouts, 0);
     }
 
+    if (basicBlocksParamMetaData.has_value() && basicBlocksParamMetaData->supportsStringConversion)
+    {
+        auto res = basicBlocksParamMetaData->modulationNaturalToString(val.f, modulationDepth,
+                                                                       isBipolar, detailedMode);
+        if (res.has_value())
+        {
+#if DEBUG_MOD_STRINGS
+            std::cout << "  value  : " << res->value << std::endl;
+            std::cout << "  summm  : " << res->summary << std::endl;
+            std::cout << "  change : " << res->changeUp << " | " << res->changeDown << std::endl;
+            std::cout << "  vUpDn  : " << res->valUp << " | " << res->valDown << std::endl;
+#endif
+            switch (displaymode)
+            {
+            case TypeIn:
+                strncpy(txt, res->value.c_str(), TXT_SIZE - 1);
+                return;
+            case Menu:
+                strncpy(txt, res->summary.c_str(), TXT_SIZE - 1);
+                return;
+            case InfoWindow:
+                iw->val = res->baseValue;
+                iw->valplus = res->valUp;
+                iw->valminus = res->valDown;
+                iw->dvalplus = res->changeUp;
+                iw->dvalminus = res->changeDown;
+                return;
+            }
+        }
+        else
+        {
+            std::cout << "Modulation formatting failed for [" << basicBlocksParamMetaData->name
+                      << "]" << std::endl;
+        }
+    }
+
     int dp = (detailedMode ? 6 : displayInfo.decimals);
 
     const char *lowersep = "<", *uppersep = ">";
@@ -2456,7 +2495,6 @@ void Parameter::get_display_of_modulation_depth(char *txt, float modulationDepth
             {
                 if (val.f <= val_min.f)
                     v = displayInfo.minLabelValue;
-                ;
                 if (val.f - modulationDepth <= val_min.f)
                     mn = displayInfo.minLabelValue;
                 if (val.f + modulationDepth <= val_min.f)
@@ -3071,6 +3109,18 @@ void Parameter::getSemitonesOrKeys(std::string &str) const
 
 void Parameter::get_display_alt(char *txt, bool external, float ef) const
 {
+    if (basicBlocksParamMetaData.has_value() && basicBlocksParamMetaData->supportsStringConversion)
+    {
+        auto bbf = val.f;
+        if (external)
+            bbf = basicBlocksParamMetaData->normalized01ToNatural(ef);
+        auto tryFormat = basicBlocksParamMetaData->valueToAlternateString(bbf);
+        if (tryFormat.has_value())
+        {
+            strncpy(txt, tryFormat->c_str(), TXT_SIZE - 1);
+            return;
+        }
+    }
 
     txt[0] = 0;
     switch (ctrltype)
@@ -3200,12 +3250,11 @@ void Parameter::get_display(char *txt, bool external, float ef) const
 
 std::string Parameter::get_display(bool external, float ef) const
 {
-    std::string txt = "";
+    std::string txt{""};
 
     if (ctrltype == ct_none)
     {
-        txt = "-";
-        return txt;
+        return "-";
     }
 
     int i;
@@ -3220,6 +3269,20 @@ std::string Parameter::get_display(bool external, float ef) const
             Surge::Storage::getUserDefaultValue(storage, Surge::Storage::HighPrecisionReadouts, 0);
     }
 
+    if (basicBlocksParamMetaData.has_value() && basicBlocksParamMetaData->supportsStringConversion)
+    {
+        auto bbf = val.f;
+        if (valtype == vt_int)
+            bbf = (float)val.i;
+        if (valtype == vt_bool)
+            bbf = val.b ? 1.f : 0.f;
+        if (external)
+            bbf = basicBlocksParamMetaData->normalized01ToNatural(ef);
+        auto tryFormat = basicBlocksParamMetaData->valueToString(bbf, detailedMode);
+        if (tryFormat.has_value())
+            return *tryFormat;
+    }
+
     switch (valtype)
     {
     case vt_float:
@@ -4274,6 +4337,34 @@ bool Parameter::set_value_from_string(const std::string &s, std::string &errMsg)
 bool Parameter::set_value_from_string_onto(const std::string &s, pdata &ontoThis,
                                            std::string &errMsg)
 {
+    if (basicBlocksParamMetaData.has_value() && basicBlocksParamMetaData->supportsStringConversion)
+    {
+        auto res = basicBlocksParamMetaData->valueFromString(s, errMsg);
+        if (res.has_value())
+        {
+            switch (valtype)
+            {
+            case vt_int:
+                val.i = (int)std::round(*res);
+                break;
+            case vt_float:
+                val.f = *res;
+                break;
+            case vt_bool:
+                val.b = (*res > 0.5);
+                break;
+            }
+            return true;
+        }
+        else if (!errMsg.empty())
+        {
+            return false;
+        }
+        else
+        {
+            std::cout << "Value from String failed" << std::endl;
+        }
+    }
     if (valtype == vt_int)
     {
         // default out of range value to test against later
@@ -4736,6 +4827,20 @@ bool Parameter::set_value_from_string_onto(const std::string &s, pdata &ontoThis
 float Parameter::calculate_modulation_value_from_string(const std::string &s, std::string &errMsg,
                                                         bool &valid)
 {
+    if (basicBlocksParamMetaData.has_value() && basicBlocksParamMetaData->supportsStringConversion)
+    {
+        auto res = basicBlocksParamMetaData->modulationNaturalFromString(s, val.f, errMsg);
+        if (res.has_value())
+        {
+            valid = true;
+            return (*res) / (basicBlocksParamMetaData->maxVal - basicBlocksParamMetaData->minVal);
+        }
+        else
+        {
+            valid = false;
+            return 0;
+        }
+    }
     errMsg = "Input is out of bounds!";
     valid = true;
 
diff --git a/src/common/Parameter.h b/src/common/Parameter.h
index d5c86bb4e6e..64d5a73c7ff 100644
--- a/src/common/Parameter.h
+++ b/src/common/Parameter.h
@@ -30,6 +30,8 @@
 #include <atomic>
 #include "SkinModel.h"
 
+#include "sst/basic-blocks/params/ParamMetadata.h"
+
 union pdata
 {
     int i;
@@ -439,6 +441,8 @@ class Parameter
         Special,
     };
 
+    std::optional<sst::basic_blocks::params::ParamMetaData> basicBlocksParamMetaData;
+
     void get_display_of_modulation_depth(char *txt, float modulationDepth, bool isBipolar,
                                          ModulationDisplayMode mode,
                                          ModulationDisplayInfoWindowStrings *iw = nullptr) const;
diff --git a/src/common/dsp/effects/FlangerEffect.cpp b/src/common/dsp/effects/FlangerEffect.cpp
index 54fa953238a..0da6684f32b 100644
--- a/src/common/dsp/effects/FlangerEffect.cpp
+++ b/src/common/dsp/effects/FlangerEffect.cpp
@@ -109,21 +109,3 @@ void FlangerEffect::init_ctrltypes()
 
     configureControlsFromFXMetadata();
 }
-
-#if 0
-void FlangerEffect::init_default_values()
-{
-    fxdata->p[fl_rate].val.f = -2.f;
-    fxdata->p[fl_depth].val.f = 1.f;
-
-    fxdata->p[fl_voices].val.f = 4.f;
-    fxdata->p[fl_voice_basepitch].val.f = 60.f;
-    fxdata->p[fl_voice_spacing].val.f = 0.f;
-
-    fxdata->p[fl_feedback].val.f = 0.f;
-    fxdata->p[fl_damping].val.f = 0.1f;
-
-    fxdata->p[fl_width].val.f = 0.f;
-    fxdata->p[fl_mix].val.f = 0.8f;
-}
-#endif
\ No newline at end of file
diff --git a/src/common/dsp/effects/Reverb1Effect.h b/src/common/dsp/effects/Reverb1Effect.h
index 9060611b9a8..0586daad519 100644
--- a/src/common/dsp/effects/Reverb1Effect.h
+++ b/src/common/dsp/effects/Reverb1Effect.h
@@ -34,6 +34,7 @@ class Reverb1Effect
     Reverb1Effect(SurgeStorage *storage, FxStorage *fxdata, pdata *pd);
     virtual ~Reverb1Effect() = default;
 
+    // TODO: Make it so we can kill this override - deactivated state mostly
     virtual void init_default_values() override;
     virtual void init_ctrltypes() override;
 
diff --git a/src/common/dsp/effects/SurgeSSTFXAdapter.h b/src/common/dsp/effects/SurgeSSTFXAdapter.h
index 73e54188e79..cf182392ea8 100644
--- a/src/common/dsp/effects/SurgeSSTFXAdapter.h
+++ b/src/common/dsp/effects/SurgeSSTFXAdapter.h
@@ -157,11 +157,11 @@ template <typename T> struct SurgeSSTFXBase : T
             auto pmd = T::paramAt(i);
             if (pmd.type == sst::basic_blocks::params::ParamMetaData::Type::FLOAT)
             {
-                this->fxdata->p[i].val_default.f = pmd.defaultVal;
+                this->fxdata->p[i].val.f = pmd.defaultVal;
             }
             if (pmd.type == sst::basic_blocks::params::ParamMetaData::Type::INT)
             {
-                this->fxdata->p[i].val_default.i = (int)std::round(pmd.defaultVal);
+                this->fxdata->p[i].val.i = (int)std::round(pmd.defaultVal);
             }
         }
     }
@@ -175,6 +175,7 @@ template <typename T> struct SurgeSSTFXBase : T
         {
             auto pmd = T::paramAt(i);
             this->fxdata->p[i].set_name(pmd.name.c_str());
+            this->fxdata->p[i].basicBlocksParamMetaData = pmd;
             auto check = [&](auto a, auto b, auto msg) {
                 if (a != b)
                     std::cout << "Unable to match " << pmd.name << " " << a << " " << b << " "
@@ -190,6 +191,15 @@ template <typename T> struct SurgeSSTFXBase : T
                 check((int)pmd.minVal, this->fxdata->p[i].val_min.i, "Minimum Values");
                 check((int)pmd.maxVal, this->fxdata->p[i].val_max.i, "Maximum Values");
             }
+            if (!pmd.supportsStringConversion)
+            {
+                std::cout << "No support for string conversion on " << pmd.name << std::endl;
+            }
+            check(pmd.canTemposync, this->fxdata->p[i].can_temposync(), "Can Temposync");
+            check(pmd.canDeform, this->fxdata->p[i].has_deformoptions(), "Can Deform");
+            check(pmd.canAbsolute, this->fxdata->p[i].can_be_absolute(), "Can Be Absolute");
+            check(pmd.canExtend, this->fxdata->p[i].can_extend_range(), "Can Extend");
+            check(pmd.supportsStringConversion, true, "Supports string value");
         }
     }
 };
