Seq mwheel overflow, bipolar centerline separate line, draw ruler seconds on top (#2004)

* Fixed stepseq mousewheel overflow, bipolar centerline actually drawn now

* Draw ruler seconds values after waveform (so that it's on top)
Right-mouse to set lines in step sequencer

RMB in a step and voila, you get a ramp.

CLoses #730
Right-mouse to set lines in step sequencer (#2005)

RMB in a step and voila, you get a ramp.

CLoses #730
Change LFO Ramp behavior

Feedback from @mkruselj about how other synths work so be
in line with that. Basically set line on mouse up and cancel
if you are in the same step

Closes #730
Change LFO Ramp behavior (#2007)

Feedback from @mkruselj about how other synths work so be
in line with that. Basically set line on mouse up and cancel
if you are in the same step

Closes #730
