bugfix (fixes #1570): use sd_journald_test_cursor() from systemd-journal API for additional cursor test in syslog-ng.presit file

short bug description: when syslog-ng checks cursor position from
syslog-ng.persist file it does not check if cursor is "in the future" with
respect to the system.  This causes logging glitch when syslog-ng is started
(syslog-ng waits for the "time to come" to continue logging) when there is no
systemd journal entries with the "future time". This is because
sd_journald_seek_cursor() seeks to the next closest entry (in terms of time)
if no entry matches.
bugfix (fixes #1570): use sd_journald_test_cursor() from systemd-journal API for additional cursor test in syslog-ng.presit file

short bug description: when syslog-ng checks cursor position from
syslog-ng.persist file it does not check if cursor is "in the future" with
respect to the system.  This causes logging glitch when syslog-ng is started
(syslog-ng waits for the "time to come" to continue logging) when there is no
systemd journal entries with the "future time". This is because
sd_journald_seek_cursor() seeks to the next closest entry (in terms of time)
if no entry matches.
Merge pull request #1571 from faithlesstomas/issue1570

bugfix (fixes #1570): use sd_journald_test_cursor() from systemd-jour…
bugfix (fixes #1570): use sd_journald_test_cursor() from systemd-journal API for additional cursor test in syslog-ng.presit file

short bug description: when syslog-ng checks cursor position from
syslog-ng.persist file it does not check if cursor is "in the future" with
respect to the system.  This causes logging glitch when syslog-ng is started
(syslog-ng waits for the "time to come" to continue logging) when there is no
systemd journal entries with the "future time". This is because
sd_journald_seek_cursor() seeks to the next closest entry (in terms of time)
if no entry matches.
bugfix (fixes #1570): use sd_journald_test_cursor() from systemd-journal API for additional cursor test in syslog-ng.presit file

short bug description: when syslog-ng checks cursor position from
syslog-ng.persist file it does not check if cursor is "in the future" with
respect to the system.  This causes logging glitch when syslog-ng is started
(syslog-ng waits for the "time to come" to continue logging) when there is no
systemd journal entries with the "future time". This is because
sd_journald_seek_cursor() seeks to the next closest entry (in terms of time)
if no entry matches.
Merge pull request #1612 from lbudai/reopen-pr/issue1570

bugfix (fixes #1570): use sd_journald_test_cursor() from systemd-jour…
