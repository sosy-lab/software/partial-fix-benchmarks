some tests fail
Yeah, there appears to be some kernel API breakage regarding the chattr() ioctls. I got other reports about this issue, but I can't reproduce it locally. I figure I need to downgrade my kernel somehow...
Please test with #28.
All tests OK for me with #28 !
#28 works fine for x84_64 but not for i586.

Full build log is here: https://build.opensuse.org/package/live_build_log/home:sebix/casync/openSUSE_Tumbleweed/i586
@sebix can you test if current git works? The main fix of #28 is now merged.
@poettering Then the checks are failing for x86_64 again:

```
[   17s] + /usr/bin/ninja test -v -j2 -C build
[   17s] ninja: Entering directory `build'
[   17s] [0/1] '/usr/bin/python3' '/usr/bin/mesontest' '--no-rebuild' '--print-errorlogs'
[   58s]  1/12 test-script.sh                          FAIL    19.78 s
[   58s]  2/12 test-nbd.sh                             OK      23.82 s
[   58s]  3/12 test-fuse.sh                            OK      16.96 s
[   58s]  4/12 test-cachunk                            OK       0.85 s
[   58s]  5/12 test-cachunker                          OK       0.90 s
[   58s]  6/12 test-cachunker-histogram                OK       2.06 s
[   58s]  7/12 test-caencoder                          OK       0.04 s
[   58s]  8/12 test-camakebst                          OK       4.37 s
[   58s]  9/12 test-caorigin                           OK       0.01 s
[   58s] 10/12 test-casync                             OK       8.18 s
[   58s] 11/12 test-cautil                             OK       0.01 s
[   58s] 12/12 test-util                               OK       0.02 s
[   58s] 
[   58s] OK:        11
[   58s] FAIL:       1
[   58s] SKIP:       0
[   58s] TIMEOUT:    0
[   58s] 
[   58s] 
[   58s] The output from the failed tests:
[   58s] 
[   58s]  1/12 test-script.sh                          FAIL    19.78 s
[   58s] 
[   58s] --- command ---
[   58s] /home/abuild/rpmbuild/BUILD/casync-master/build/test-script.sh
[   58s] --- Listing only the last 100 lines from a long log. ---
[   58s] Packing casync-master/src/meson.build
[   58s] Packed casync-master/src/meson.build
[   58s] Packing casync-master/src/notify.c
[   58s] Packed casync-master/src/notify.c
[   58s] Packing casync-master/src/notify.h
[   58s] Packed casync-master/src/notify.h
[   58s] Packing casync-master/src/parse-util.c
[   58s] Packed casync-master/src/parse-util.c
[   58s] Packing casync-master/src/parse-util.h
[   58s] Packed casync-master/src/parse-util.h
[   58s] Packing casync-master/src/realloc-buffer.c
[   58s] Packed casync-master/src/realloc-buffer.c
[   58s] Packing casync-master/src/realloc-buffer.h
[   58s] Packed casync-master/src/realloc-buffer.h
[   58s] Packing casync-master/src/reflink.c
[   58s] Packed casync-master/src/reflink.c
[   58s] Packing casync-master/src/reflink.h
[   58s] Packed casync-master/src/reflink.h
[   58s] Packing casync-master/src/rm-rf.c
[   58s] Packed casync-master/src/rm-rf.c
[   58s] Packing casync-master/src/rm-rf.h
[   58s] Packed casync-master/src/rm-rf.h
[   58s] Packing casync-master/src/signal-handler.c
[   58s] Packed casync-master/src/signal-handler.c
[   58s] Packing casync-master/src/signal-handler.h
[   58s] Packed casync-master/src/signal-handler.h
[   58s] Packing casync-master/src/siphash24.c
[   58s] Packed casync-master/src/siphash24.c
[   58s] Packing casync-master/src/siphash24.h
[   58s] Packed casync-master/src/siphash24.h
[   58s] Packing casync-master/src/util.c
[   58s] Packed casync-master/src/util.c
[   58s] Packing casync-master/src/util.h
[   58s] Packed casync-master/src/util.h
[   58s] Packed casync-master/src
[   58s] Packing casync-master/test
[   58s] Packing casync-master/test/Makefile
[   58s] Packed casync-master/test/Makefile
[   58s] Packing casync-master/test/http-server.py
[   58s] Packed casync-master/test/http-server.py
[   58s] Packing casync-master/test/meson.build
[   58s] Packed casync-master/test/meson.build
[   58s] Packing casync-master/test/notify-wait.c
[   58s] Packed casync-master/test/notify-wait.c
[   58s] Packing casync-master/test/pseudo-ssh
[   58s] Packed casync-master/test/pseudo-ssh
[   58s] Packing casync-master/test/test-cachunk.c
[   58s] Packed casync-master/test/test-cachunk.c
[   58s] Packing casync-master/test/test-cachunker-histogram.c
[   58s] Packed casync-master/test/test-cachunker-histogram.c
[   58s] Packing casync-master/test/test-cachunker.c
[   58s] Packed casync-master/test/test-cachunker.c
[   58s] Packing casync-master/test/test-caencoder.c
[   58s] Packed casync-master/test/test-caencoder.c
[   58s] Packing casync-master/test/test-caformat.c
[   58s] Packed casync-master/test/test-caformat.c
[   58s] Packing casync-master/test/test-caindex.c
[   58s] Packed casync-master/test/test-caindex.c
[   58s] Packing casync-master/test/test-camakebst.c
[   58s] Packed casync-master/test/test-camakebst.c
[   58s] Packing casync-master/test/test-caorigin.c
[   58s] Packed casync-master/test/test-caorigin.c
[   58s] Packing casync-master/test/test-casync.c
[   58s] Packed casync-master/test/test-casync.c
[   58s] Packing casync-master/test/test-cautil.c
[   58s] Packed casync-master/test/test-cautil.c
[   58s] Packing casync-master/test/test-fuse.sh.in
[   58s] Packed casync-master/test/test-fuse.sh.in
[   58s] Packing casync-master/test/test-nbd.sh.in
[   58s] Packed casync-master/test/test-nbd.sh.in
[   58s] Packing casync-master/test/test-script.sh.in
[   58s] Packed casync-master/test/test-script.sh.in
[   58s] Packing casync-master/test/test-util.c
[   58s] Packed casync-master/test/test-util.c
[   58s] Packed casync-master/test
[   58s] Packing casync-master/test-files
[   58s] Packing casync-master/test-files/.gitignore
[   58s] Packed casync-master/test-files/.gitignore
[   58s] Packing casync-master/test-files/a symlink with umläüts
[   58s] Packed casync-master/test-files/a symlink with umläüts
[   58s] Packing casync-master/test-files/bar
[   58s] Packed casync-master/test-files/bar
[   58s] Packing casync-master/test-files/large
[   58s] Packed casync-master/test-files/large
[   58s] Packing casync-master/test-files/test-files.sh
[   58s] Packed casync-master/test-files/test-files.sh
[   58s] Packing casync-master/test-files/äöüß
[   58s] Packed casync-master/test-files/äöüß
[   58s] Packed casync-master/test-files
[   58s] Packed casync-master
[   58s] Packed ./
[   58s] Selected feature flags not actually applicable to backing file systems: flag-hidden flag-archive flag-projinherit
[   58s] Archive size: 4.6M
[   58s] Number of chunks: 69
[   58s] Reused chunks: 69 (100%)
[   58s] Effective average chunk size: 68.5K
[   58s] + /home/abuild/rpmbuild/BUILD/casync-master/build/casync -v --without=privileged extract /var/tmp/test-casync.23581/seek.catar /var/tmp/test-casync.23581/extract-seek-catar casync/test-files
[   58s] Seek path not available in archive.
[   58s] -------
[   58s] 
[   58s] Full log written to /home/abuild/rpmbuild/BUILD/casync-master/build/meson-logs/testlog.txt
[   58s] FAILED: test 
[   58s] '/usr/bin/python3' '/usr/bin/mesontest' '--no-rebuild' '--print-errorlogs'
[   58s] ninja: build stopped: subcommand failed.
```
#34 should fix that particular issue.
With this patch it works again for x86_64, but not for i386

```
[   34s] + /usr/bin/ninja test -v -j3 -C build
[   34s] ninja: Entering directory `build'
[   34s] [0/1] '/usr/bin/python3' '/usr/bin/mesontest' '--no-rebuild' '--print-errorlogs'
[   47s]  1/12 test-script.sh                          FAIL     5.61 s
[   47s]  2/12 test-nbd.sh                             FAIL     9.47 s
[   47s]  3/12 test-fuse.sh                            OK       8.43 s
[   47s]  4/12 test-cachunk                            OK       0.36 s
[   47s]  5/12 test-cachunker                          OK       0.42 s
[   47s]  6/12 test-cachunker-histogram                OK       2.03 s
[   47s]  7/12 test-caencoder                          OK       0.02 s
[   47s]  8/12 test-camakebst                          OK       4.36 s
[   47s]  9/12 test-caorigin                           OK       0.00 s
[   47s] 10/12 test-casync                             FAIL     2.48 s
[   47s] 11/12 test-cautil                             OK       0.00 s
[   47s] 12/12 test-util                               OK       0.01 s
[   47s] 
[   47s] OK:         9
[   47s] FAIL:       3
[   47s] SKIP:       0
[   47s] TIMEOUT:    0
[   47s] 
[   47s] 
[   47s] The output from the failed tests:
[   47s] 
[   47s]  1/12 test-script.sh                          FAIL     5.61 s
[   47s] 
[   47s] --- command ---
[   47s] /home/abuild/rpmbuild/BUILD/casync-master/build/test-script.sh
[   47s] --- Listing only the last 100 lines from a long log. ---
[   47s] Packing casync/src/notify.h
[   47s] Packed casync/src/notify.h
[   47s] Packing casync/src/parse-util.c
[   47s] Packed casync/src/parse-util.c
[   47s] Packing casync/src/parse-util.h
[   47s] Packed casync/src/parse-util.h
[   47s] Packing casync/src/realloc-buffer.c
[   47s] Packed casync/src/realloc-buffer.c
[   47s] Packing casync/src/realloc-buffer.h
[   47s] Packed casync/src/realloc-buffer.h
[   47s] Packing casync/src/reflink.c
[   47s] Packed casync/src/reflink.c
[   47s] Packing casync/src/reflink.h
[   47s] Packed casync/src/reflink.h
[   47s] Packing casync/src/rm-rf.c
[   47s] Packed casync/src/rm-rf.c
[   47s] Packing casync/src/rm-rf.h
[   47s] Packed casync/src/rm-rf.h
[   47s] Packing casync/src/signal-handler.c
[   47s] Packed casync/src/signal-handler.c
[   47s] Packing casync/src/signal-handler.h
[   47s] Packed casync/src/signal-handler.h
[   47s] Packing casync/src/siphash24.c
[   47s] Packed casync/src/siphash24.c
[   47s] Packing casync/src/siphash24.h
[   47s] Packed casync/src/siphash24.h
[   47s] Packing casync/src/util.c
[   47s] Packed casync/src/util.c
[   47s] Packing casync/src/util.h
[   47s] Packed casync/src/util.h
[   47s] Packed casync/src
[   47s] Packing casync/test
[   47s] Packing casync/test/Makefile
[   47s] Packed casync/test/Makefile
[   47s] Packing casync/test/http-server.py
[   47s] Packed casync/test/http-server.py
[   47s] Packing casync/test/meson.build
[   47s] Packed casync/test/meson.build
[   47s] Packing casync/test/notify-wait.c
[   47s] Packed casync/test/notify-wait.c
[   47s] Packing casync/test/pseudo-ssh
[   47s] Packed casync/test/pseudo-ssh
[   47s] Packing casync/test/test-cachunk.c
[   47s] Packed casync/test/test-cachunk.c
[   47s] Packing casync/test/test-cachunker-histogram.c
[   47s] Packed casync/test/test-cachunker-histogram.c
[   47s] Packing casync/test/test-cachunker.c
[   47s] Packed casync/test/test-cachunker.c
[   47s] Packing casync/test/test-caencoder.c
[   47s] Packed casync/test/test-caencoder.c
[   47s] Packing casync/test/test-caformat.c
[   47s] Packed casync/test/test-caformat.c
[   47s] Packing casync/test/test-caindex.c
[   47s] Packed casync/test/test-caindex.c
[   47s] Packing casync/test/test-camakebst.c
[   47s] Packed casync/test/test-camakebst.c
[   47s] Packing casync/test/test-caorigin.c
[   47s] Packed casync/test/test-caorigin.c
[   47s] Packing casync/test/test-casync.c
[   47s] Packed casync/test/test-casync.c
[   47s] Packing casync/test/test-cautil.c
[   47s] Packed casync/test/test-cautil.c
[   47s] Packing casync/test/test-fuse.sh.in
[   47s] Packed casync/test/test-fuse.sh.in
[   47s] Packing casync/test/test-nbd.sh.in
[   47s] Packed casync/test/test-nbd.sh.in
[   47s] Packing casync/test/test-script.sh.in
[   47s] Packed casync/test/test-script.sh.in
[   47s] Packing casync/test/test-util.c
[   47s] Packed casync/test/test-util.c
[   47s] Packed casync/test
[   47s] Packing casync/test-files
[   47s] Packing casync/test-files/.gitignore
[   47s] Packed casync/test-files/.gitignore
[   47s] Packing casync/test-files/a symlink with umläüts
[   47s] Packed casync/test-files/a symlink with umläüts
[   47s] Packing casync/test-files/bar
[   47s] Packed casync/test-files/bar
[   47s] Packing casync/test-files/large
[   47s] /-\Packed casync/test-files/large
[   47s] Packing casync/test-files/test-files.sh
[   47s] Packed casync/test-files/test-files.sh
[   47s] Packing casync/test-files/äöüß
[   47s] Packed casync/test-files/äöüß
[   47s] Packed casync/test-files
[   47s] Packed casync
[   47s] Packed ./
[   47s] Selected feature flags not actually applicable to backing file systems: flag-hidden flag-archive flag-compr flag-nocow flag-nocomp flag-projinherit
[   47s] Archive size: 4.6M
[   47s] Number of chunks: 74
[   47s] Reused chunks: 0 (0%)
[   47s] Effective average chunk size: 64.1K
[   47s] + /home/abuild/rpmbuild/BUILD/casync-master/build/casync -v --without=privileged list /var/tmp/test-casync.23824/test.caidx
[   47s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   47s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   47s] Excluding files with chattr(1) -d flag: yes
[   47s] Excluding submounts: no
[   47s] Failed to run synchronizer: Bad message
[   47s] -------
[   47s] 
[   47s]  2/12 test-nbd.sh                             FAIL     9.47 s
[   47s] 
[   47s] --- command ---
[   47s] /home/abuild/rpmbuild/BUILD/casync-master/build/test-nbd.sh
[   47s] --- stdout ---
[   47s] fdb7cb6f7ecbba7abed7698ee09ece9ff5364c53dac67961252690fa66524ded
[   47s] --- stderr ---
[   47s] + PARAMS=-v
[   47s] + CASYNC_PROTOCOL_PATH=/home/abuild/rpmbuild/BUILD/casync-master/build
[   47s] + export CASYNC_PROTOCOL_PATH
[   47s] + SCRATCH_DIR=/var/tmp/test-casync.16955
[   47s] + mkdir -p /var/tmp/test-casync.16955
[   47s] + dd if=/dev/urandom of=/var/tmp/test-casync.16955/blob bs=102400 count=80
[   47s] 80+0 records in
[   47s] 80+0 records out
[   47s] 8192000 bytes (8.2 MB, 7.8 MiB) copied, 0.0792278 s, 103 MB/s
[   47s] + /home/abuild/rpmbuild/BUILD/casync-master/build/casync -v digest /var/tmp/test-casync.16955/blob
[   47s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   47s] Using feature flags: none
[   47s] Excluding files with chattr(1) -d flag: yes
[   47s] Excluding submounts: no
[   47s] -+ sha256sum /var/tmp/test-casync.16955/blob
[   47s] + cut -c -64
[   47s] + /home/abuild/rpmbuild/BUILD/casync-master/build/casync -v make /var/tmp/test-casync.16955/test.caibx /var/tmp/test-casync.16955/blob
[   47s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   47s] Using feature flags: none
[   47s] Excluding files with chattr(1) -d flag: yes
[   47s] Excluding submounts: no
[   47s] -\|/-\|/-\|/-\|/-\|/-\|/-\|/Selected feature flags not actually applicable to backing file systems: none
[   47s] Archive size: 7.8M
[   47s] Number of chunks: 124
[   47s] Reused chunks: 0 (0%)
[   47s] Effective average chunk size: 64.5K
[   47s] + /home/abuild/rpmbuild/BUILD/casync-master/build/casync -v digest /var/tmp/test-casync.16955/test.caibx
[   47s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   47s] -Failed to run synchronizer: Bad message
[   47s] -------
[   47s] 
[   47s] 10/12 test-casync                             FAIL     2.48 s
[   47s] 
[   47s] --- command ---
[   47s] /home/abuild/rpmbuild/BUILD/casync-master/build/test-casync
[   47s] --- stderr ---
[   47s] Assertion failed: r >= 0
[   47s] -------
[   47s] 
[   47s] Full log written to /home/abuild/rpmbuild/BUILD/casync-master/build/meson-logs/testlog.txt
```
See also https://build.opensuse.org/package/live_build_log/home:sebix/casync/openSUSE_Tumbleweed/i586
Yeah. i386/s390x/ppc64be/arm are broken. Also the tests don't pass under root.
@sebix hmm, could you uncomment the following two lines at the top of src/cadecoder.c:

```c
/* #undef EBADMSG */
/* #define EBADMSG __LINE__ */
```

and recompile and run the tests with that on i386? (Why? EBADMSG is the error you are seeing, and this way we redfine it to the line number throwing it, and when that's shown in the log output we know which lone precisely caused the failure)
And I figure we should reopen this, given that this still doesn't work everywhere
That's now the output for i586:
```
[   30s] + /usr/bin/ninja test -v -j3 -C build
[   30s] ninja: Entering directory `build'
[   30s] [0/1] '/usr/bin/python3' '/usr/bin/mesontest' '--no-rebuild' '--print-errorlogs'
[   39s]  1/12 test-script.sh                          FAIL     0.86 s
[   39s]  2/12 test-nbd.sh                             FAIL     7.40 s
[   39s]  3/12 test-fuse.sh                            OK       6.31 s
[   39s]  4/12 test-cachunk                            OK       0.30 s
[   39s]  5/12 test-cachunker                          OK       0.33 s
[   39s]  6/12 test-cachunker-histogram                OK       2.02 s
[   39s]  7/12 test-caencoder                          OK       0.01 s
[   39s]  8/12 test-camakebst                          OK       3.69 s
[   39s]  9/12 test-caorigin                           OK       0.00 s
[   39s] 10/12 test-casync                             FAIL     1.86 s
[   39s] 11/12 test-cautil                             OK       0.00 s
[   39s] 12/12 test-util                               OK       0.00 s
[   39s] 
[   39s] OK:         9
[   39s] FAIL:       3
[   39s] SKIP:       0
[   39s] TIMEOUT:    0
[   39s] 
[   39s] 
[   39s] The output from the failed tests:
[   39s] 
[   39s]  1/12 test-script.sh                          FAIL     0.86 s
[   39s] 
[   39s] --- command ---
[   39s] /home/abuild/rpmbuild/BUILD/casync-1/build/test-script.sh
[   39s] --- Listing only the last 100 lines from a long log. ---
[   39s] Processing test-files/bar
[   39s] Processed test-files/bar
[   39s] Processing test-files/large
[   39s] Processed test-files/large
[   39s] Processing test-files/test-files.sh
[   39s] Processed test-files/test-files.sh
[   39s] Processing test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Processed test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Processed test-files
[   39s] Processed ./
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged make /var/tmp/test-casync.17978/test.catar
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] Packing ./
[   39s] Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] -Packing test-files
[   39s] Packing test-files/.gitignore
[   39s] Packed test-files/.gitignore
[   39s] Packing test-files/a symlink with umlÃ¤Ã¼ts
[   39s] Packed test-files/a symlink with umlÃ¤Ã¼ts
[   39s] Packing test-files/bar
[   39s] Packed test-files/bar
[   39s] Packing test-files/large
[   39s] Packed test-files/large
[   39s] Packing test-files/test-files.sh
[   39s] Packed test-files/test-files.sh
[   39s] Packing test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Packed test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Packed test-files
[   39s] Packed ./
[   39s] Selected feature flags not actually applicable to backing file systems: flag-hidden flag-archive flag-compr flag-nocow flag-nocomp flag-projinherit
[   39s] Archive size: 1.0M
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged list /var/tmp/test-casync.17978/test.catar
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged mtree /var/tmp/test-casync.17978/test.catar
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged digest /var/tmp/test-casync.17978/test.catar
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] -Processing ./
[   39s] Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] Processing test-files
[   39s] Processing test-files/.gitignore
[   39s] Processed test-files/.gitignore
[   39s] Processing test-files/a symlink with umlÃ¤Ã¼ts
[   39s] Processed test-files/a symlink with umlÃ¤Ã¼ts
[   39s] Processing test-files/bar
[   39s] Processed test-files/bar
[   39s] Processing test-files/large
[   39s] Processed test-files/large
[   39s] Processing test-files/test-files.sh
[   39s] Processed test-files/test-files.sh
[   39s] Processing test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Processed test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Processed test-files
[   39s] Processed ./
[   39s] + sha256sum /var/tmp/test-casync.17978/test.catar
[   39s] + cut -c -64
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged make /var/tmp/test-casync.17978/test.caidx
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] Packing ./
[   39s] Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] -Packing test-files
[   39s] Packing test-files/.gitignore
[   39s] Packed test-files/.gitignore
[   39s] Packing test-files/a symlink with umlÃ¤Ã¼ts
[   39s] Packed test-files/a symlink with umlÃ¤Ã¼ts
[   39s] Packing test-files/bar
[   39s] Packed test-files/bar
[   39s] Packing test-files/large
[   39s] \|Packed test-files/large
[   39s] Packing test-files/test-files.sh
[   39s] Packed test-files/test-files.sh
[   39s] Packing test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Packed test-files/Ã¤Ã¶Ã¼ÃŸ
[   39s] Packed test-files
[   39s] Packed ./
[   39s] Selected feature flags not actually applicable to backing file systems: flag-hidden flag-archive flag-compr flag-nocow flag-nocomp flag-projinherit
[   39s] Archive size: 1.0M
[   39s] Number of chunks: 14
[   39s] Reused chunks: 0 (0%)
[   39s] Effective average chunk size: 73.2K
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged list /var/tmp/test-casync.17978/test.caidx
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] Failed to run synchronizer: Bad message
[   39s] -------
[   39s] 
[   39s]  2/12 test-nbd.sh                             FAIL     7.40 s
[   39s] 
[   39s] --- command ---
[   39s] /home/abuild/rpmbuild/BUILD/casync-1/build/test-nbd.sh
[   39s] --- stdout ---
[   39s] c7d3e392c394f86deb144910641f8f456979ea8ac7587e0d3a8994fcc0ae962b
[   39s] --- stderr ---
[   39s] + PARAMS=-v
[   39s] + CASYNC_PROTOCOL_PATH=/home/abuild/rpmbuild/BUILD/casync-1/build
[   39s] + export CASYNC_PROTOCOL_PATH
[   39s] + SCRATCH_DIR=/var/tmp/test-casync.14418
[   39s] + mkdir -p /var/tmp/test-casync.14418
[   39s] + dd if=/dev/urandom of=/var/tmp/test-casync.14418/blob bs=102400 count=80
[   39s] 80+0 records in
[   39s] 80+0 records out
[   39s] 8192000 bytes (8.2 MB, 7.8 MiB) copied, 0.0653938 s, 125 MB/s
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v digest /var/tmp/test-casync.14418/blob
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] Using feature flags: none
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] -+ cut -c -64
[   39s] + sha256sum /var/tmp/test-casync.14418/blob
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v make /var/tmp/test-casync.14418/test.caibx /var/tmp/test-casync.14418/blob
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] Using feature flags: none
[   39s] Excluding files with chattr(1) -d flag: yes
[   39s] Excluding submounts: no
[   39s] -\|/-\|/-\|/-\|/-\|/-\Selected feature flags not actually applicable to backing file systems: none
[   39s] Archive size: 7.8M
[   39s] Number of chunks: 113
[   39s] Reused chunks: 0 (0%)
[   39s] Effective average chunk size: 70.7K
[   39s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v digest /var/tmp/test-casync.14418/test.caibx
[   39s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   39s] -Failed to run synchronizer: Bad message
[   39s] -------
[   39s] 
[   39s] 10/12 test-casync                             FAIL     1.86 s
[   39s] 
[   39s] --- command ---
[   39s] /home/abuild/rpmbuild/BUILD/casync-1/build/test-casync
[   39s] --- stderr ---
[   39s] Assertion failed: r >= 0
[   39s] -------
```
Full log: https://build.opensuse.org/build/home:sebix/openSUSE_Tumbleweed/i586/casync/_log
I can not confirm that a959668782dafbfe469994b14d9c3adaef4e1c34 applied to v1 (together with #28) fixes the issue on i586:
```
[   49s] + /usr/bin/ninja test -v -j8 -C build
[   49s] ninja: Entering directory `build'
[   49s] [0/1] '/usr/bin/python3' '/usr/bin/mesontest' '--no-rebuild' '--print-errorlogs'
[   65s]  1/12 test-script.sh                          FAIL     3.41 s
[   65s]  2/12 test-nbd.sh                             FAIL    15.43 s
[   65s]  3/12 test-fuse.sh                            OK      12.06 s
[   65s]  4/12 test-cachunk                            OK       1.73 s
[   65s]  5/12 test-cachunker                          OK       2.37 s
[   65s]  6/12 test-cachunker-histogram                OK       2.04 s
[   65s]  7/12 test-caencoder                          OK       0.04 s
[   65s]  8/12 test-camakebst                          OK       5.10 s
[   65s]  9/12 test-caorigin                           OK       0.02 s
[   65s] 10/12 test-casync                             FAIL     5.86 s
[   65s] 11/12 test-cautil                             OK       0.03 s
[   65s] 12/12 test-util                               OK       0.03 s
[   65s] 
[   65s] OK:         9
[   65s] FAIL:       3
[   65s] SKIP:       0
[   65s] TIMEOUT:    0
[   65s] 
[   65s] 
[   65s] The output from the failed tests:
[   65s] 
[   65s]  1/12 test-script.sh                          FAIL     3.41 s
[   65s] 
[   65s] --- command ---
[   65s] /home/abuild/rpmbuild/BUILD/casync-1/build/test-script.sh
[   65s] --- Listing only the last 100 lines from a long log. ---
[   65s] Processing test-files/bar
[   65s] Processed test-files/bar
[   65s] Processing test-files/large
[   65s] Processed test-files/large
[   65s] Processing test-files/test-files.sh
[   65s] Processed test-files/test-files.sh
[   65s] Processing test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Processed test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Processed test-files
[   65s] Processed ./
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged make /var/tmp/test-casync.4124/test.catar
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] Packing ./
[   65s] Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] -Packing test-files
[   65s] Packing test-files/.gitignore
[   65s] Packed test-files/.gitignore
[   65s] Packing test-files/a symlink with umlÃ¤Ã¼ts
[   65s] Packed test-files/a symlink with umlÃ¤Ã¼ts
[   65s] Packing test-files/bar
[   65s] Packed test-files/bar
[   65s] Packing test-files/large
[   65s] Packed test-files/large
[   65s] Packing test-files/test-files.sh
[   65s] Packed test-files/test-files.sh
[   65s] Packing test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Packed test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Packed test-files
[   65s] Packed ./
[   65s] Selected feature flags not actually applicable to backing file systems: flag-hidden flag-archive flag-compr flag-nocow flag-nocomp flag-projinherit
[   65s] Archive size: 1.0M
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged list /var/tmp/test-casync.4124/test.catar
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged mtree /var/tmp/test-casync.4124/test.catar
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged digest /var/tmp/test-casync.4124/test.catar
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] -Processing ./
[   65s] Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] Processing test-files
[   65s] Processing test-files/.gitignore
[   65s] Processed test-files/.gitignore
[   65s] Processing test-files/a symlink with umlÃ¤Ã¼ts
[   65s] Processed test-files/a symlink with umlÃ¤Ã¼ts
[   65s] Processing test-files/bar
[   65s] Processed test-files/bar
[   65s] Processing test-files/large
[   65s] Processed test-files/large
[   65s] Processing test-files/test-files.sh
[   65s] Processed test-files/test-files.sh
[   65s] Processing test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Processed test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Processed test-files
[   65s] Processed ./
[   65s] + cut -c -64
[   65s] + sha256sum /var/tmp/test-casync.4124/test.catar
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged make /var/tmp/test-casync.4124/test.caidx
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] Packing ./
[   65s] Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] -Packing test-files
[   65s] Packing test-files/.gitignore
[   65s] Packed test-files/.gitignore
[   65s] Packing test-files/a symlink with umlÃ¤Ã¼ts
[   65s] Packed test-files/a symlink with umlÃ¤Ã¼ts
[   65s] Packing test-files/bar
[   65s] Packed test-files/bar
[   65s] Packing test-files/large
[   65s] \|/-\|Packed test-files/large
[   65s] Packing test-files/test-files.sh
[   65s] Packed test-files/test-files.sh
[   65s] Packing test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Packed test-files/Ã¤Ã¶Ã¼ÃŸ
[   65s] Packed test-files
[   65s] Packed ./
[   65s] Selected feature flags not actually applicable to backing file systems: flag-hidden flag-archive flag-compr flag-nocow flag-nocomp flag-projinherit
[   65s] Archive size: 1.0M
[   65s] Number of chunks: 14
[   65s] Reused chunks: 0 (0%)
[   65s] Effective average chunk size: 73.2K
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v --without=privileged list /var/tmp/test-casync.4124/test.caidx
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] -Using feature flags: nsec-time symlinks fifos sockets flag-hidden flag-archive flag-noatime flag-compr flag-nocow flag-dirsync flag-sync flag-nocomp flag-projinherit xattrs
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] Failed to run synchronizer: Bad message
[   65s] -------
[   65s] 
[   65s]  2/12 test-nbd.sh                             FAIL    15.43 s
[   65s] 
[   65s] --- command ---
[   65s] /home/abuild/rpmbuild/BUILD/casync-1/build/test-nbd.sh
[   65s] --- stdout ---
[   65s] bcf215bbda12da88a4a230e076173945a422bf73f2f1732ce722f41693ef92a4
[   65s] --- stderr ---
[   65s] + PARAMS=-v
[   65s] + CASYNC_PROTOCOL_PATH=/home/abuild/rpmbuild/BUILD/casync-1/build
[   65s] + export CASYNC_PROTOCOL_PATH
[   65s] + SCRATCH_DIR=/var/tmp/test-casync.241
[   65s] + mkdir -p /var/tmp/test-casync.241
[   65s] + dd if=/dev/urandom of=/var/tmp/test-casync.241/blob bs=102400 count=80
[   65s] 80+0 records in
[   65s] 80+0 records out
[   65s] 8192000 bytes (8.2 MB, 7.8 MiB) copied, 0.18386 s, 44.6 MB/s
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v digest /var/tmp/test-casync.241/blob
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] Using feature flags: none
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] -+ sha256sum /var/tmp/test-casync.241/blob
[   65s] + cut -c -64
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v make /var/tmp/test-casync.241/test.caibx /var/tmp/test-casync.241/blob
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] Using feature flags: none
[   65s] Excluding files with chattr(1) -d flag: yes
[   65s] Excluding submounts: no
[   65s] -\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\|/-\Selected feature flags not actually applicable to backing file systems: none
[   65s] Archive size: 7.8M
[   65s] Number of chunks: 127
[   65s] Reused chunks: 0 (0%)
[   65s] Effective average chunk size: 62.9K
[   65s] + /home/abuild/rpmbuild/BUILD/casync-1/build/casync -v digest /var/tmp/test-casync.241/test.caibx
[   65s] Selected chunk sizes: min=16384..avg=65536..max=262144
[   65s] -Failed to run synchronizer: Bad message
[   65s] -------
[   65s] 
[   65s] 10/12 test-casync                             FAIL     5.86 s
[   65s] 
[   65s] --- command ---
[   65s] /home/abuild/rpmbuild/BUILD/casync-1/build/test-casync
[   65s] --- stderr ---
[   65s] Assertion failed: r >= 0
[   65s] -------
[   65s] 
[   65s] Full log written to /home/abuild/rpmbuild/BUILD/casync-1/build/meson-logs/testlog.txt
[   65s] FAILED: test 
[   65s] '/usr/bin/python3' '/usr/bin/mesontest' '--no-rebuild' '--print-errorlogs'
[   65s] ninja: build stopped: subcommand failed.
```
See also https://build.opensuse.org/public/build/home:sebix/openSUSE_Tumbleweed/i586/casync/_log for the current log.

Are there other fixes needed too?