[validate table name when insert]
Fix the issue #185
更新代码 (#1)

* Fix compilation of taosdump

The mode argument must be supplied when O_CREAT or O_TMPFILE is
specified in flags for open(...).

* Don't SEGFAULT

* fix left/bs for release build

* Create readme.md

* Update pom.xml

Change the pom.xml for the JDBC demo project so that users can directly compile and package an executable jar with all the dependencies.

* [TBASE-641]

* The password is no longer stored in clear text, so you may need to reinstall the database

* Fix the bug when the stream calculation error occurs

* Fix the syntax error while EPOLLWAKEUP no defined in some systems

* Modify the default proportion of the last file

* Remove cmake's dependence on CXX

* Fix the Issue of #46, Build source code failure

* [modify libtaos.so link]

* [modify libtaos.so link]

* [modify libtaos.so link]

* [modify libtaos.so link]

* [modify libtaos.so link]

* [modify libtaos.so link]

* [modify libtaos.so link]

* update an expression

* Add make install error handle

* Update README.md

* Fix shell password issue #92

* Fix issue #91, remove assert for illegal filtering conditions

* Fix the issue #99, when the http request is broken, the query does not end normally

* Fix the issue #100, Change keyword stable to STable

* Fix the issue #108, limit does not have effects on the results

* Fix the issure #103, alter table change tag bug

* [issue #102]

* [issue #102]

* [issue #102]

* [modify path of libtaos.so]

* [modify path of libtaos.so]

* [modify path of libtaos.so]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* [Modify the use of sudo commands]

* fix issue #117

* Increase the comparison test between InfluxDB and TDengine

* Fix the issue #120, Set the default listening IP address to 0.0.0.0

* Fix the issue #121, Output dataDir in the log file

* Fix the issure #118, Change the alter tag value sql syntax

* fix issue #122

* Fix the issure #126, Taosd may crash while an invalid token input

* Fix the issue #127, Check dead http links and close them

* Fix the issue ##110, unsorted timestamp in one request may cause crash

* Fix the issue #130, reference count may have a problem

* Update tsched.c

fix buffer overflow and memory leak.

* issue #141, also need to modify the function declaration

* Fix issue #145

* Fix the issue #148, failed to find the next file when the file id of the vnode is not continuous

* Fix the issue #149, SParsedDataColInfo not initialized

* [add sql and csv file for demo]

* Fix the issue #156

* [Add single quotation marks to nchar fields when export to a file]

* [modify the path of exec file]

* [Support using capitalized by table/db name]

* Added nodejs connector and example usage of nodejs

* Update README.md

* fix #174

* Updated gitignore and added some documentation and better naming

- Using _ to indicate variable/function is private and shouldn't be accessed
- Added JSDocs syntax to generate documentation
-

* More documentation and added node async functionality

* [fix db/table name]

* Added local documentation files

* Updated Readme and fixed foldername

* [validate table name when insert]

* Fix the issue #185

* Updated nodejs connector to 1.1.1

Implemented higher level API to make it more modular
Using promises and callbacks
Argument binding is now available e.g query(select ? from ?).bind("*", "table")

* Minor improvement to 'setErrMsg'

1. code like `char msg[] = "xxxxxx"` unnecessarily copied the string to stack, changed to `const char* msg = "xxxxxx"`.
2. the 2rd argument of `setErrMsg` is changed from `char*` to `const char*`.
3. the 3rd argument of `setErrMsg` is removed as `strncpy` stops copying at the end of source string.

* Documentation Update

* remove duplicate include in trpc.c

* Windows client, #51 #38 #32 #12

* Windows client

* Grammar and typo fixes. Updated some docs

* Headings fix for connector docs

* fix issue #206

* Add initial driver connector for rust

* Fix the #issue 217

* update the example code of stream.c

* Fix the #issue 220, TDengine can't start when /var/lib/taos does not exist

* [modify timestamp]

* [modify timestamp]

* Docs Update; Completed async functionality for Node.js connector, updated to 1.2.0

Docs
- Docs updated
  - include third party connectors links
  - local docs updated to web versions and fixed for compatability
Node
- Async added
- Deleted uncessary code
- Docs updated

* Fix the issue #222, add win64 python connector

* Fix the issue #222, add win64 python connector

* Update cinterface.js

removed broken code

* Update readme.md

* fix the issue #233, failed to record slow query

* fix the issue #233, failed to record slow query

* fix the issue #233, failed to record slow query

* Node updated to 1.2.1

Bug fix

* Fix the issue #222

* fix python close connection error

* fix the issue #240

* fix the issue #240

* update README

* Update README for building error: No CMAKE_CXX_COMPILER could be found

* delete unused taosTmrStop

* [Fixed dsn parsing for network addr and port by caeret. #214]

* remove some compile warnings

* add -g option to CMakeList.txt

* fix possible commit log init failure

* data transfer via restful interface may be lost

* data transfer via restful interface may be lost

* [no remove config, data, log files]

* Uploaded markdown docs for testing syncing with web docs

* Fixed typos - Web docs sync test 1

* Update Connector.md

* Update connector-ch.md

* Websync test 4

* [rust connector] support subscribe

* remove useless files

* fix typo in JDBC demo code

* Update advanced features-ch.md

#250

* fix the issue #264 #253 #251

* fix the issue #264 #253 #251

* import data may cause database blocking

* Update CONTRIBUTING.md

* remove some warnings

* add command size check when source file

* Fix issue #270
