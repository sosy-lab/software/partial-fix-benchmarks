emulate slot-1 read timings (fixes SF bug 1258)
fix crash in slot1 retail(debug) when it's set while no game is loaded (fixes #63)
fix bug in fsnitro FAT reading; fixes #63
