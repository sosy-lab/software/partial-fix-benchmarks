TCP: Pico must handle window shrinking
Please double check the test "window shrinking" because the bench unit does not respond after the segment is split. First part of the 50B segment is never acknowledged even if it now fits the advertised window, and the DUT goes to probe mode.

Any chance this test gets fixed or I get more feedback?

windows shrinking test is fixed.
