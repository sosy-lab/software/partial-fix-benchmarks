diff --git a/include/pico_stack.h b/include/pico_stack.h
index 0bca40345..6cf3a1f76 100644
--- a/include/pico_stack.h
+++ b/include/pico_stack.h
@@ -23,12 +23,6 @@ int32_t pico_transport_receive(struct pico_frame *f, uint8_t proto);
 /* interface towards ethernet */
 int32_t pico_network_receive(struct pico_frame *f);
 
-/* The pico_ethernet_receive() function is used by
- * those devices supporting ETH in order to push packets up
- * into the stack.
- */
-/* DATALINK LEVEL */
-int32_t pico_ethernet_receive(struct pico_frame *f);
 
 /* LOWEST LEVEL: interface towards devices. */
 /* Device driver will call this function which returns immediately.
@@ -43,9 +37,23 @@ int32_t pico_stack_recv_zerocopy_ext_buffer(struct pico_device *dev, uint8_t *bu
 /* ===== SENDIING FUNCTIONS (from socket down to dev) ===== */
 
 int32_t pico_network_send(struct pico_frame *f);
-int32_t pico_ethernet_send(struct pico_frame *f);
 int32_t pico_sendto_dev(struct pico_frame *f);
 
+#ifdef PICO_SUPPORT_ETH
+int32_t pico_ethernet_send(struct pico_frame *f);
+
+/* The pico_ethernet_receive() function is used by
+ * those devices supporting ETH in order to push packets up
+ * into the stack.
+ */
+/* DATALINK LEVEL */
+int32_t pico_ethernet_receive(struct pico_frame *f);
+#else
+    /* When ETH is not supported by the stack... */
+#   define pico_ethernet_send(f)    (-1)
+#   define pico_ethernet_receive(f) (-1)
+#endif
+
 /* ----- Initialization ----- */
 int pico_stack_init(void);
 
diff --git a/modules/pico_ipv6_nd.c b/modules/pico_ipv6_nd.c
index 6f63125ae..eb3b7d1d4 100644
--- a/modules/pico_ipv6_nd.c
+++ b/modules/pico_ipv6_nd.c
@@ -96,7 +96,7 @@ static void pico_ipv6_nd_queued_trigger(void)
     {
         f = frames_queued_v6[i];
         if (f) {
-            pico_ethernet_send(f);
+            (void)pico_ethernet_send(f);
             frames_queued_v6[i] = NULL;
         }
     }
diff --git a/stack/pico_device.c b/stack/pico_device.c
index a503214f7..8b84d5682 100644
--- a/stack/pico_device.c
+++ b/stack/pico_device.c
@@ -213,7 +213,7 @@ static int devloop_in(struct pico_device *dev, int loop_score)
         if (f) {
             if (dev->eth) {
                 f->datalink_hdr = f->buffer;
-                pico_ethernet_receive(f);
+                (void)pico_ethernet_receive(f);
             } else {
                 f->net_hdr = f->buffer;
                 pico_network_receive(f);
diff --git a/stack/pico_stack.c b/stack/pico_stack.c
index 6151c8b36..16193bb16 100644
--- a/stack/pico_stack.c
+++ b/stack/pico_stack.c
@@ -223,34 +223,6 @@ int32_t pico_network_receive(struct pico_frame *f)
     }
     return (int32_t)f->buffer_len;
 }
-#ifdef PICO_SUPPORT_IPV4
-static int32_t pico_ipv4_network_receive(struct pico_frame *f)
-{
-    if (IS_IPV4(f)) {
-        pico_enqueue(pico_proto_ipv4.q_in, f);
-    } else {
-        (void)pico_icmp4_param_problem(f, 0);
-        pico_frame_discard(f);
-        return -1;
-    }
-    return (int32_t)f->buffer_len;
-}
-#endif
-
-#ifdef PICO_SUPPORT_IPV6
-static int32_t pico_ipv6_network_receive(struct pico_frame *f)
-{
-    if (IS_IPV6(f)) {
-        pico_enqueue(pico_proto_ipv6.q_in, f);
-    } else {
-        /* Wrong version for link layer type */
-        (void)pico_icmp6_parameter_problem(f, PICO_ICMP6_PARAMPROB_HDRFIELD, 0);
-        pico_frame_discard(f);
-        return -1;
-    }
-    return (int32_t)f->buffer_len;
-}
-#endif
 
 /* Network layer: interface towards socket for frame sending */
 int32_t pico_network_send(struct pico_frame *f)
@@ -287,7 +259,7 @@ int pico_source_is_local(struct pico_frame *f)
     return 0;
 }
 
-
+#ifdef PICO_SUPPORT_ETH
 /* DATALINK LEVEL: interface from network to the device
  * and vice versa.
  */
@@ -297,6 +269,76 @@ int pico_source_is_local(struct pico_frame *f)
  * into the stack.
  */
 
+static int destination_is_bcast(struct pico_frame *f)
+{
+    if (!f)
+        return 0;
+
+    if (IS_IPV6(f))
+        return 0;
+
+#ifdef PICO_SUPPORT_IPV4
+    else {
+        struct pico_ipv4_hdr *hdr = (struct pico_ipv4_hdr *) f->net_hdr;
+        return pico_ipv4_is_broadcast(hdr->dst.addr);
+    }
+#else
+    return 0;
+#endif
+}
+
+static int destination_is_mcast(struct pico_frame *f)
+{
+    int ret = 0;
+    if (!f)
+        return 0;
+
+#ifdef PICO_SUPPORT_IPV6
+    if (IS_IPV6(f)) {
+        struct pico_ipv6_hdr *hdr = (struct pico_ipv6_hdr *) f->net_hdr;
+        ret = pico_ipv6_is_multicast(hdr->dst.addr);
+    }
+
+#endif
+#ifdef PICO_SUPPORT_IPV4
+    else {
+        struct pico_ipv4_hdr *hdr = (struct pico_ipv4_hdr *) f->net_hdr;
+        ret = pico_ipv4_is_multicast(hdr->dst.addr);
+    }
+#endif
+
+    return ret;
+}
+
+#ifdef PICO_SUPPORT_IPV4
+static int32_t pico_ipv4_ethernet_receive(struct pico_frame *f)
+{
+    if (IS_IPV4(f)) {
+        pico_enqueue(pico_proto_ipv4.q_in, f);
+    } else {
+        (void)pico_icmp4_param_problem(f, 0);
+        pico_frame_discard(f);
+        return -1;
+    }
+    return (int32_t)f->buffer_len;
+}
+#endif
+
+#ifdef PICO_SUPPORT_IPV6
+static int32_t pico_ipv6_ethernet_receive(struct pico_frame *f)
+{
+    if (IS_IPV6(f)) {
+        pico_enqueue(pico_proto_ipv6.q_in, f);
+    } else {
+        /* Wrong version for link layer type */
+        (void)pico_icmp6_parameter_problem(f, PICO_ICMP6_PARAMPROB_HDRFIELD, 0);
+        pico_frame_discard(f);
+        return -1;
+    }
+    return (int32_t)f->buffer_len;
+}
+#endif
+
 static int32_t pico_ll_receive(struct pico_frame *f)
 {
     struct pico_eth_hdr *hdr = (struct pico_eth_hdr *) f->datalink_hdr;
@@ -309,12 +351,12 @@ static int32_t pico_ll_receive(struct pico_frame *f)
 
 #if defined (PICO_SUPPORT_IPV4)
     if (hdr->proto == PICO_IDETH_IPV4)
-        return pico_ipv4_network_receive(f);
+        return pico_ipv4_ethernet_receive(f);
 #endif
 
 #if defined (PICO_SUPPORT_IPV6)
     if (hdr->proto == PICO_IDETH_IPV6)
-        return pico_ipv6_network_receive(f);
+        return pico_ipv6_ethernet_receive(f);
 #endif
 
     pico_frame_discard(f);
@@ -351,51 +393,9 @@ int32_t pico_ethernet_receive(struct pico_frame *f)
     }
 
     pico_ll_check_bcast(f);
-
     return pico_ll_receive(f);
 }
 
-static int destination_is_bcast(struct pico_frame *f)
-{
-    if (!f)
-        return 0;
-
-    if (IS_IPV6(f))
-        return 0;
-
-#ifdef PICO_SUPPORT_IPV4
-    else {
-        struct pico_ipv4_hdr *hdr = (struct pico_ipv4_hdr *) f->net_hdr;
-        return pico_ipv4_is_broadcast(hdr->dst.addr);
-    }
-#else
-    return 0;
-#endif
-}
-
-static int destination_is_mcast(struct pico_frame *f)
-{
-    int ret = 0;
-    if (!f)
-        return 0;
-
-#ifdef PICO_SUPPORT_IPV6
-    if (IS_IPV6(f)) {
-        struct pico_ipv6_hdr *hdr = (struct pico_ipv6_hdr *) f->net_hdr;
-        ret = pico_ipv6_is_multicast(hdr->dst.addr);
-    }
-
-#endif
-#ifdef PICO_SUPPORT_IPV4
-    else {
-        struct pico_ipv4_hdr *hdr = (struct pico_ipv4_hdr *) f->net_hdr;
-        ret = pico_ipv4_is_multicast(hdr->dst.addr);
-    }
-#endif
-
-    return ret;
-}
-
 struct pico_eth *pico_ethernet_mcast_translate(struct pico_frame *f, uint8_t *pico_mcast_mac)
 {
     struct pico_ipv4_hdr *hdr = (struct pico_ipv4_hdr *) f->net_hdr;
@@ -487,6 +487,9 @@ static int32_t pico_ethsend_dispatch(struct pico_frame *f, int *ret)
     }
 }
 
+
+
+
 /* This function looks for the destination mac address
  * in order to send the frame being processed.
  */
@@ -529,7 +532,7 @@ int32_t MOCKABLE pico_ethernet_send(struct pico_frame *f)
         dstmac = pico_ethernet_mcast_translate(f, pico_mcast_mac);
     }
 
-#if (defined PICO_SUPPORT_IPV4) && (defined PICO_SUPPORT_ETH)
+#if (defined PICO_SUPPORT_IPV4)
     else {
         dstmac = pico_arp_get(f);
         /* At this point, ARP will discard the frame in any case.
@@ -566,6 +569,9 @@ int32_t MOCKABLE pico_ethernet_send(struct pico_frame *f)
     return -1;
 }
 
+#endif /* PICO_SUPPORT_ETH */
+
+
 void pico_store_network_origin(void *src, struct pico_frame *f)
 {
   #ifdef PICO_SUPPORT_IPV4
