Fixed tcp close request.
Issue #51 implementation
TCP: Decrease number of pending connections only once

socket->parent->number_of_pending_conn was decreased twice for the
same connection when an unaccepted socket was closed. This caused
an underflow and no new connections could be accepted.

Solves bug #51
