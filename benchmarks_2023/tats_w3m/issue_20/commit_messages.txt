Update ChangeLog
Prevent infinite recursion with nested table and textarea

Bug-Debian: https://github.com/tats/w3m/issues/20
Prevent infinite recursion with nested table and textarea

Bug-Debian: https://github.com/tats/w3m/issues/20#issuecomment-260590257
Prevent deref null pointer in renderCoTable()

Bug-Debian: https://github.com/tats/w3m/issues/20#issuecomment-260649537
Prevent infinite recursion with nested table and textarea

Bug-Debian: https://github.com/tats/w3m/issues/20 [CVE-2016-9439]
Origin: https://anonscm.debian.org/cgit/collab-maint/w3m.git/commit/?id=2a4a2fb9f116b50e7c80d573db06c0fdc6c69272
Prevent deref null pointer in renderCoTable()

Bug-Debian: https://github.com/tats/w3m/issues/20#issuecomment-260649537
Origin: https://anonscm.debian.org/cgit/collab-maint/w3m.git/commit/?id=ec99f186380d26ebf791569fdbc56dae60632365
