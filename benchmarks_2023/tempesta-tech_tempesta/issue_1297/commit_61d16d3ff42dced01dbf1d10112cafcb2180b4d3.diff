diff --git a/fw/http.c b/fw/http.c
index 33f92cdc0..3ee2468f9 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -5316,10 +5316,10 @@ tfw_http_req_process(TfwConn *conn, TfwStream *stream, struct sk_buff *skb)
 			}
 			ctx = tfw_h2_context(conn);
 			if (ctx->hdr.flags & HTTP2_F_END_HEADERS) {
-				if (tfw_http_parse_req_on_headers_done(req))
-					return TFW_BLOCK;
-
 				__set_bit(TFW_HTTP_B_HEADERS_PARSED, req->flags);
+
+				if (unlikely(tfw_http_parse_req_on_headers_done(req)))
+					return TFW_BLOCK;
 			}
 		}
 
diff --git a/fw/http_parser.c b/fw/http_parser.c
index 4b645b8e2..3bd700515 100644
--- a/fw/http_parser.c
+++ b/fw/http_parser.c
@@ -867,6 +867,10 @@ process_trailer_hdr(TfwHttpMsg *hm, TfwStr *hdr, unsigned int id)
 int
 tfw_http_parse_req_on_headers_done(TfwHttpReq *req)
 {
+#define IS_BODYLESS_METHOD(meth)						\
+	((meth) == TFW_HTTP_METH_GET || (meth) == TFW_HTTP_METH_HEAD		\
+	 || (meth) == TFW_HTTP_METH_DELETE || (meth) == TFW_HTTP_METH_TRACE)
+
 	TfwStr *tbl = req->h_tbl->tbl;
 
 	/* According to RFC 7231 4.3.* a payload within GET, HEAD,
@@ -878,31 +882,19 @@ tfw_http_parse_req_on_headers_done(TfwHttpReq *req)
 	{
 		/* Method override either honored or request message
 		 * with method override header dropped later in processing */
-		if (unlikely(req->method_override)) {
-			if (req->method_override == TFW_HTTP_METH_GET
-			    || req->method_override == TFW_HTTP_METH_HEAD
-			    || req->method_override == TFW_HTTP_METH_DELETE
-			    || req->method_override == TFW_HTTP_METH_TRACE)
-			{
-				T_WARN("%s: content-length or content-type"
+		if ((req->method_override
+			&& IS_BODYLESS_METHOD(req->method_override))
+		    || IS_BODYLESS_METHOD(req->method)) {
+				T_WARN("%s: Content-Length or Content-Type"
 				       " not allowed to be used with such"
-				       " overridden method\n", __func__);
+				       " (overridden) method\n", __func__);
 				return T_BAD;
-			}
-		}
-		else if (req->method == TFW_HTTP_METH_GET
-			 || req->method == TFW_HTTP_METH_HEAD
-			 || req->method == TFW_HTTP_METH_DELETE
-			 || req->method == TFW_HTTP_METH_TRACE)
-		{
-			T_WARN("%s: content-length or content-type"
-			       " not allowed to be used with such"
-			       " method\n", __func__);
-			return T_BAD;
 		}
 	}
 
 	return T_OK;
+
+#undef IS_BODYLESS_METHOD
 }
 
 /*
diff --git a/fw/t/unit/test_http_parser.c b/fw/t/unit/test_http_parser.c
index ac11e81a1..14c800fd9 100644
--- a/fw/t/unit/test_http_parser.c
+++ b/fw/t/unit/test_http_parser.c
@@ -1660,83 +1660,103 @@ TEST(http_parser, parses_connection_value)
 
 TEST(http_parser, content_type_in_bodyless_requests)
 {
-#define EXPECT_BLOCK_BODYLESS_REQ(METHOD)			\
-	EXPECT_BLOCK_REQ(#METHOD " / HTTP/1.1\r\n"		\
-		 	 "Content-Length: 0\r\n"		\
-		 	 "\r\n");				\
-	EXPECT_BLOCK_REQ(#METHOD " / HTTP/1.1\r\n"		\
-		 	 "Content-Type: text/html\r\n"		\
+#define EXPECT_BLOCK_BODYLESS_REQ(METHOD)				\
+	EXPECT_BLOCK_REQ(#METHOD " / HTTP/1.1\r\n"			\
+		 	 "Content-Length: 0\r\n"			\
+		 	 "\r\n");					\
+	EXPECT_BLOCK_REQ(#METHOD " / HTTP/1.1\r\n"			\
+		 	 "Content-Type: text/html\r\n"			\
 		 	 "\r\n");
 
+#define EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE(METHOD)			\
+	EXPECT_BLOCK_REQ("PUT / HTTP/1.1\r\n"				\
+		 	 "Content-Length: 0\r\n"			\
+			 "X-Method-Override: " #METHOD "\r\n"		\
+		 	 "\r\n");					\
+	EXPECT_BLOCK_REQ("PUT / HTTP/1.1\r\n"				\
+		 	 "Content-Length: 0\r\n"			\
+			 " X-HTTP-Method-Override: " #METHOD "\r\n"	\
+		 	 "\r\n");					\
+	EXPECT_BLOCK_REQ("PUT / HTTP/1.1\r\n"				\
+		 	 "Content-Type: text/html\r\n"			\
+			 "X-Method-Override: " #METHOD "\r\n"		\
+		 	 "\r\n");					\
+	EXPECT_BLOCK_REQ("PUT / HTTP/1.1\r\n"				\
+		 	 "Content-Type: text/html\r\n"			\
+			 "X-HTTP-Method-Override: " #METHOD "\r\n"	\
+		 	 "\r\n");
+
+#define EXPECT_BLOCK_BODYLESS_REQ_H2(METHOD)				\
+	EXPECT_BLOCK_REQ_H2(":method: "#METHOD"\n"			\
+			    ":scheme: https\n"				\
+			    ":path: /\n"				\
+			    "content-length: 0");			\
+	EXPECT_BLOCK_REQ_H2(":method: "#METHOD"\n"			\
+			    ":scheme: https\n"				\
+			    ":path: /\n"				\
+			    "content-type: text/plain");
+
+#define EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2(METHOD)			\
+	EXPECT_BLOCK_REQ_H2(":method: PUT\n"				\
+			    ":scheme: https\n"				\
+			    ":path: /\n"				\
+			    "content-length: 0\n"			\
+			    "x-method-override: "#METHOD);		\
+	EXPECT_BLOCK_REQ_H2(":method: PUT\n"				\
+			    ":scheme: https\n"				\
+			    ":path: /\n"				\
+			    "content-length: 0\n"			\
+			    "x-http-method-override: "#METHOD);		\
+	EXPECT_BLOCK_REQ_H2(":method: PUT\n"				\
+			    ":scheme: https\n"				\
+			    ":path: /\n"				\
+			    "content-type: text/plain\n"		\
+			    "x-method-override: "#METHOD);		\
+	EXPECT_BLOCK_REQ_H2(":method: PUT\n"				\
+			    ":scheme: https\n"				\
+			    ":path: /\n"				\
+			    "content-type: text/plain\n"		\
+			    "x-http-method-override: "#METHOD);
+
+
 	EXPECT_BLOCK_BODYLESS_REQ(GET);
 	EXPECT_BLOCK_BODYLESS_REQ(HEAD);
 	EXPECT_BLOCK_BODYLESS_REQ(DELETE);
 	EXPECT_BLOCK_BODYLESS_REQ(TRACE);
 
-
-#define EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE(METHOD)		\
-	EXPECT_BLOCK_REQ("PUT / HTTP/1.1\r\n"			\
-		 	 "Content-Length: 0\r\n"		\
-			 "X-Method-Override: " #METHOD "\r\n"	\
-		 	 "\r\n");				\
-	EXPECT_BLOCK_REQ("PUT / HTTP/1.1\r\n"			\
-		 	 "Content-Type: text/html\r\n"		\
-			 "X-Method-Override: " #METHOD "\r\n"	\
-		 	 "\r\n");
-
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE(GET);
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE(HEAD);
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE(DELETE);
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE(TRACE);
 
-
 	FOR_REQ("OPTIONS / HTTP/1.1\r\n"
 		"Content-Type: text/plain\r\n"
-		"\r\n");
+		"\r\n")
 	{
 		EXPECT_TFWSTR_EQ(&req->h_tbl->tbl[TFW_HTTP_HDR_CONTENT_TYPE],
 				 "Content-Type: text/plain");
 	}
 
-
-#define EXPECT_BLOCK_BODYLESS_REQ_H2(METHOD)			\
-	EXPECT_BLOCK_REQ_H2(":method: "#METHOD"\n"		\
-			    ":scheme: https\n"			\
-			    ":path: /\n"			\
-			    "content-length: 0");		\
-	EXPECT_BLOCK_REQ_H2(":method: "#METHOD"\n"		\
-			    ":scheme: https\n"			\
-			    ":path: /\n"			\
-			    "content-type: text/plain");
-
 	EXPECT_BLOCK_BODYLESS_REQ_H2(GET);
 	EXPECT_BLOCK_BODYLESS_REQ_H2(HEAD);
 	EXPECT_BLOCK_BODYLESS_REQ_H2(DELETE);
 	EXPECT_BLOCK_BODYLESS_REQ_H2(TRACE);
 
-
-#define EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2(METHOD)		\
-	EXPECT_BLOCK_REQ_H2(":method: PUT\n"			\
-			    ":scheme: https\n"			\
-			    ":path: /\n"			\
-			    "content-length: 0\n"		\
-			    "x-method-override: "#METHOD);	\
-	EXPECT_BLOCK_REQ_H2(":method: PUT\n"			\
-			    ":scheme: https\n"			\
-			    ":path: /\n"			\
-			    "content-type: text/plain\n"	\
-			    "x-method-override: "#METHOD);
-
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2(GET);
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2(HEAD);
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2(DELETE);
 	EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2(TRACE);
 
-
 	FOR_REQ_H2(":method: OPTIONS\n"
 		   ":scheme: https\n"
 		   ":path: /\n"
 		   "content-type: text/plain");
+
+
+#undef EXPECT_BLOCK_BODYLESS_REQ
+#undef EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE
+#undef EXPECT_BLOCK_BODYLESS_REQ_H2
+#undef EXPECT_BLOCK_BODYLESS_REQ_OVERRIDE_H2
 }
 
 TEST(http_parser, http2_check_important_fields)
