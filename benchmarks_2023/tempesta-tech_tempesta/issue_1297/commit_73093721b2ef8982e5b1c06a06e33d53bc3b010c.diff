diff --git a/fw/http.c b/fw/http.c
index 3ee2468f9..3f9e73606 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -5303,24 +5303,22 @@ tfw_http_req_process(TfwConn *conn, TfwStream *stream, struct sk_buff *skb)
 			return TFW_BLOCK;
 		}
 		if (TFW_MSG_H2(req)) {
-			TfwH2Ctx *ctx;
+			TfwH2Ctx *ctx = tfw_h2_context(conn);
+			if (ctx->hdr.flags & HTTP2_F_END_HEADERS) {
+				if (unlikely(tfw_http_parse_check_bodyless_meth(req)))
+					return TFW_BLOCK;
+
+				__set_bit(TFW_HTTP_B_HEADERS_PARSED, req->flags);
+			}
 
 			if (tfw_h2_stream_req_complete(req->stream)) {
-				if (likely(!tfw_http_parse_req_on_headers_done(req)
-					&& !tfw_h2_parse_req_finish(req)))
+				if (likely(!tfw_h2_parse_req_finish(req)))
 					break;
 				TFW_INC_STAT_BH(clnt.msgs_otherr);
 				tfw_http_req_parse_block(req, 500,
 					"Request parsing inconsistency");
 				return TFW_BLOCK;
 			}
-			ctx = tfw_h2_context(conn);
-			if (ctx->hdr.flags & HTTP2_F_END_HEADERS) {
-				__set_bit(TFW_HTTP_B_HEADERS_PARSED, req->flags);
-
-				if (unlikely(tfw_http_parse_req_on_headers_done(req)))
-					return TFW_BLOCK;
-			}
 		}
 
 		r = tfw_gfsm_move(&conn->state, TFW_HTTP_FSM_REQ_CHUNK, &data_up);
diff --git a/fw/http.h b/fw/http.h
index ac200e24f..3042b2bea 100644
--- a/fw/http.h
+++ b/fw/http.h
@@ -86,7 +86,7 @@ enum {
 };
 
 /* TODO: When CONNECT will be added, add it to tfw_handle_validation_req()
- * and to tfw_http_parse_req_on_headers_done() */
+ * and to tfw_http_parse_check_bodyless_meth() */
 /* New safe methods MUST be added to TFW_HTTP_IS_METH_SAFE macro */
 typedef enum {
 	_TFW_HTTP_METH_NONE,
diff --git a/fw/http_parser.c b/fw/http_parser.c
index 6c244ec43..8fd2e7026 100644
--- a/fw/http_parser.c
+++ b/fw/http_parser.c
@@ -1194,7 +1194,7 @@ __FSM_STATE(RGen_BodyInit, cold) {					\
 		TFW_PARSER_BLOCK(RGen_BodyInit);			\
 	}								\
 									\
-	if (tfw_http_parse_req_on_headers_done(req)) {			\
+	if (tfw_http_parse_check_bodyless_meth(req)) {			\
 		TFW_PARSER_BLOCK(RGen_BodyInit);			\
 	}								\
 									\
@@ -3730,8 +3730,19 @@ tfw_http_init_parser_req(TfwHttpReq *req)
 	hbh_hdrs->spec = 0x1 << TFW_HTTP_HDR_CONNECTION;
 }
 
+
+/**
+ * Check h1/h2 request after all headers was parsed.
+ *
+ * According to RFC 7231 4.3.* a payload within GET, HEAD,
+ * DELETE, TRACE and CONNECT requests has no defined semantics
+ * and implementations can reject it. We do this respecting overrides.
+ *
+ * Return T_BAD if request contains Content-Length or Content-Type field
+ * for bodyless method.
+ */
 int
-tfw_http_parse_req_on_headers_done(TfwHttpReq *req)
+tfw_http_parse_check_bodyless_meth(TfwHttpReq *req)
 {
 #define IS_BODYLESS_METHOD(meth)						\
 	((meth) == TFW_HTTP_METH_GET || (meth) == TFW_HTTP_METH_HEAD		\
@@ -3739,22 +3750,20 @@ tfw_http_parse_req_on_headers_done(TfwHttpReq *req)
 
 	TfwStr *tbl = req->h_tbl->tbl;
 
-	/* According to RFC 7231 4.3.* a payload within GET, HEAD,
-	 * DELETE, TRACE and CONNECT requests has no defined semantics
-	 * and implementations can reject it. We do this respecting overrides.
-	 */
-	if (TFW_STR_NOT_EMPTY(&tbl[TFW_HTTP_HDR_CONTENT_LENGTH])
-	    || TFW_STR_NOT_EMPTY(&tbl[TFW_HTTP_HDR_CONTENT_TYPE]))
+
+	if (!TFW_STR_EMPTY(&tbl[TFW_HTTP_HDR_CONTENT_LENGTH])
+	    || !TFW_STR_EMPTY(&tbl[TFW_HTTP_HDR_CONTENT_TYPE]))
 	{
 		/* Method override either honored or request message
 		 * with method override header dropped later in processing */
 		if ((req->method_override
 			&& IS_BODYLESS_METHOD(req->method_override))
-		    || IS_BODYLESS_METHOD(req->method)) {
-				T_WARN("%s: Content-Length or Content-Type"
-				       " not allowed to be used with such"
-				       " (overridden) method\n", __func__);
-				return T_DROP;
+		    || IS_BODYLESS_METHOD(req->method))
+		{
+			T_WARN("%s: Content-Length or Content-Type"
+			       " not allowed to be used with such"
+			       " (overridden) method\n", __func__);
+			return T_BAD;
 		}
 	}
 
@@ -8346,9 +8355,8 @@ tfw_h2_parse_req_hdr(unsigned char *data, unsigned long len, TfwHttpReq *req,
 
 		p += 1;
 		T_DBG3("%s: name next, to=Req_HdrX_Http_Method len=%lu,"
-		    " off=%lu\n",
-		    __func__, len, __data_off(p));
-		if (likely(__data_off(p) < len))
+		       " off=%lu\n", __func__, len, __data_off(p));
+		if (__data_off(p) < len)
 			__FSM_JMP(Req_HdrX_Http_Method);
 
 		__msg_hdr_chunk_fixup(data, len);
@@ -8771,6 +8779,9 @@ tfw_h2_parse_req_finish(TfwHttpReq *req)
 	if (WARN_ON_ONCE(!tfw_h2_stream_req_complete(req->stream)))
 		return T_DROP;
 
+	if (unlikely(!test_bit(TFW_HTTP_B_HEADERS_PARSED, req->flags)))
+		return T_DROP;
+
 	/*
 	 * TFW_HTTP_B_H2_HDRS_FULL flag is set on first TFW_HTTP_HDR_REGULAR
 	 * header, if no present, need to check mandatory pseudo headers.
@@ -8798,7 +8809,6 @@ tfw_h2_parse_req_finish(TfwHttpReq *req)
 	 */
 	req->content_length = req->body.len;
 	req->body.flags |= TFW_STR_COMPLETE;
-	__set_bit(TFW_HTTP_B_HEADERS_PARSED, req->flags);
 	__set_bit(TFW_HTTP_B_FULLY_PARSED, req->flags);
 
 	__h2_msg_hdr_val(&ht->tbl[TFW_HTTP_HDR_H2_AUTHORITY],
diff --git a/fw/http_parser.h b/fw/http_parser.h
index 45792a72e..11f8b7b0f 100644
--- a/fw/http_parser.h
+++ b/fw/http_parser.h
@@ -133,7 +133,7 @@ typedef struct {
 void tfw_http_init_parser_req(TfwHttpReq *req);
 void tfw_http_init_parser_resp(TfwHttpResp *resp);
 
-int tfw_http_parse_req_on_headers_done(TfwHttpReq *req);
+int tfw_http_parse_check_bodyless_meth(TfwHttpReq *req);
 
 int tfw_http_parse_req(void *req_data, unsigned char *data, size_t len,
 		       unsigned int *parsed);
diff --git a/fw/str.h b/fw/str.h
index 5adc4580f..edc6e3e05 100644
--- a/fw/str.h
+++ b/fw/str.h
@@ -271,8 +271,7 @@ typedef struct tfwstr_t {
 
 #define TFW_STR_INIT(s)		memset((s), 0, sizeof(TfwStr))
 
-#define TFW_STR_NOT_EMPTY(s)	((s)->nchunks | (s)->len)
-#define TFW_STR_EMPTY(s)	!TFW_STR_NOT_EMPTY(s)
+#define TFW_STR_EMPTY(s)	(!((s)->nchunks | (s)->len))
 #define TFW_STR_PLAIN(s)	(!((s)->nchunks))
 #define TFW_STR_DUP(s)		((s)->flags & TFW_STR_DUPLICATE)
 
