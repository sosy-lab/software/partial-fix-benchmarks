diff --git a/fw/hpack.c b/fw/hpack.c
index 25b19e925..637d241fe 100644
--- a/fw/hpack.c
+++ b/fw/hpack.c
@@ -3072,7 +3072,7 @@ tfw_hpack_rbuf_calc(TfwHPackETbl *__restrict tbl, unsigned short new_size,
 	do {
 		unsigned short f_len, fhdr_len;
 
-		if (i >= HPACK_MAX_ENC_EVICTION)
+		if (i >= HPACK_MAX_ENC_EVICTION && del_list)
 			return -E2BIG;
 
 		if (unlikely(!size))
@@ -3991,8 +3991,11 @@ tfw_hpack_encode(TfwHttpResp *__restrict resp, TfwStr *__restrict hdr,
 void
 tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 {
-	if (WARN_ON_ONCE(new_size > HPACK_ENC_TABLE_MAX_SIZE))
-		return;
+	if (new_size > HPACK_ENC_TABLE_MAX_SIZE) {
+		T_WARN("Client requests hpack table size (%hu), which is "
+			"greater than HPACK_ENC_TABLE_MAX_SIZE.", new_size);
+		new_size = HPACK_ENC_TABLE_MAX_SIZE;
+	}
 
 	spin_lock(&tbl->lock);
 
@@ -4000,14 +4003,91 @@ tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 	       " new_size=%hu\n", __func__, tbl->rb_len, tbl->size,
 	       tbl->window, new_size);
 
-	if (tbl->window > new_size) {
+	/*
+	 * RFC7541#section-4.2:
+	 * Multiple updates to the maximum table size can occur between the
+	 * transmission of two header blocks. In the case that this size is
+	 * changed more than once in this interval, the smallest maximum table
+	 * size that occurs in that interval MUST be signaled in a dynamic
+	 * table size update.
+	 */
+	if (tbl->window != new_size && (likely(!atomic_read(&tbl->wnd_changed))
+	    || unlikely(!tbl->window) || new_size < tbl->window))
+	{
 		if (tbl->size > new_size)
 			tfw_hpack_rbuf_calc(tbl, new_size, NULL,
 					    (TfwHPackETblIter *)tbl);
 		WARN_ON_ONCE(tbl->rb_len > tbl->size);
 
 		tbl->window = new_size;
+		atomic_set(&tbl->wnd_changed, 1);
 	}
 
 	spin_unlock(&tbl->lock);
 }
+
+int
+tfw_hpack_enc_tbl_write_sz(TfwHPackETbl *__restrict tbl, struct sk_buff *skb,
+			   unsigned int *bytes_wtitten)
+{
+	TfwMsgIter it = { .frag = -1, .skb = skb, .skb_head = skb};
+	TfwStr new_size = {};
+	TfwHPackInt tmp = {};
+	TfwFrameHdr hdr = {};
+	char *data = skb->data;
+	int r = 0;
+
+	/* Check header type */
+	if (skb->data[3] != HTTP2_HEADERS)
+		return 0;
+
+	/*
+	 * We should encode hpack dynamic table size, only in case when
+	 * it was changed and only once.
+	 */
+	if (unlikely(atomic_cmpxchg(&tbl->wnd_changed, 1, -1) == 1)) {
+		write_int(tbl->window, 0x1F, 0x20, &tmp);
+		new_size.data = tmp.buf;
+		new_size.len = tmp.sz;
+	
+		r = tfw_msg_iter_move(&it, (unsigned char **)&data,
+				      FRAME_HEADER_SIZE);
+		if (unlikely(r))
+			goto finish;
+
+		r = tfw_http_msg_insert(&it, &data, &new_size);
+		if (unlikely(r))
+			goto finish;
+
+		tfw_h2_unpack_frame_header(&hdr, skb->data);
+		hdr.length += tmp.sz;
+		tfw_h2_pack_frame_header(skb->data, &hdr);
+
+		*bytes_wtitten = tmp.sz;
+	}
+
+finish:
+	if (unlikely(r))
+		/*
+		 * In case of error we should restore value of `wnd_changed`
+		 * flag.
+		 */
+		atomic_set(&tbl->wnd_changed, 1);
+	return r;	
+}
+
+void
+tfw_hpack_enc_tbl_write_sz_release(TfwHPackETbl *__restrict tbl, int r)
+{
+	/*
+	 * Before calling this function, we should check that we encode
+	 * new dynamic table size into the frame, so `old` can have only
+	 * two values (-1 in most of all cases, since we set it previosly
+	 * or 1 if changing of dynamic table size was occured, before this
+	 * function is called).
+	 * We should change this flag only if it wasn't changed by
+	 * `tfw_hpack_set_rbuf_size` function.
+	 */
+	int old = atomic_cmpxchg(&tbl->wnd_changed, -1, r == 0 ? 0 : 1);
+	WARN_ON_ONCE(!old);
+}
diff --git a/fw/hpack.h b/fw/hpack.h
index 0990af777..375b42bf3 100644
--- a/fw/hpack.h
+++ b/fw/hpack.h
@@ -80,6 +80,13 @@ typedef struct {
  *
  * @window	- maximum pseudo-length of the dynamic table (in bytes); this
  *		  value used as threshold to flushing old entries;
+ * @wnd_changed	- flag indicates, that window was changed by settings update,
+ * 		- can be in three states:
+ * 		- 0 in case when window size isn't changed.
+ * 		- 1 in case when window size is changed and it should be written
+ * 		  into the first response, before the first header block.
+ * 		- -1 in case when window size is written into the first response,
+ * 		  but this response was not sent to a client yet.
  * @rbuf	- pointer to the ring buffer;
  * @root	- pointer to the root node of binary tree;
  * @pool	- memory pool for dynamic table;
@@ -92,6 +99,7 @@ typedef struct {
 typedef struct {
 	TFW_HPACK_ETBL_COMMON;
 	unsigned short		window;
+	atomic_t		wnd_changed;
 	char			*rbuf;
 	TfwHPackNode		*root;
 	TfwPool			*pool;
@@ -279,6 +287,9 @@ int tfw_hpack_cache_decode_expand(TfwHPack *__restrict hp,
 				  unsigned char *__restrict src, unsigned long n,
 				  TfwDecodeCacheIter *__restrict cd_iter);
 void tfw_hpack_enc_release(TfwHPack *__restrict hp, unsigned long *flags);
+int tfw_hpack_enc_tbl_write_sz(TfwHPackETbl *__restrict tbl, struct sk_buff * __restrict skb,
+			       unsigned int *bytes_wtitten);
+void tfw_hpack_enc_tbl_write_sz_release(TfwHPackETbl *__restrict tbl, int r);
 
 static inline unsigned int
 tfw_hpack_int_size(unsigned long index, unsigned short max)
diff --git a/fw/http.c b/fw/http.c
index 7e9af5563..16a83d666 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -4994,9 +4994,11 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		h_len -= max_sz;
 		frame_hdr.type = HTTP2_CONTINUATION;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     fr_flags);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 fr_flags);
+		if (unlikely(r))
+			return r;
 	}
 
 	if (local_response)
@@ -5025,9 +5027,11 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		b_len -= max_sz;
 		frame_hdr.type = HTTP2_DATA;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     HTTP2_F_END_STREAM);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 HTTP2_F_END_STREAM);
+		if (unlikely(r))
+			return r;
 	}
 
 	return 0;
diff --git a/fw/http_frame.c b/fw/http_frame.c
index afcc796c4..3cca192b9 100644
--- a/fw/http_frame.c
+++ b/fw/http_frame.c
@@ -251,18 +251,6 @@ tfw_h2_context_clear(TfwH2Ctx *ctx)
 	tfw_hpack_clean(&ctx->hpack);
 }
 
-static inline void
-tfw_h2_unpack_frame_header(TfwFrameHdr *hdr, const unsigned char *buf)
-{
-	hdr->length = ntohl(*(int *)buf) >> 8;
-	hdr->type = buf[3];
-	hdr->flags = buf[4];
-	hdr->stream_id = ntohl(*(unsigned int *)&buf[5]) & FRAME_STREAM_ID_MASK;
-
-	T_DBG3("%s: parsed, length=%d, stream_id=%u, type=%hhu, flags=0x%hhx\n",
-	       __func__, hdr->length, hdr->stream_id, hdr->type, hdr->flags);
-}
-
 static inline void
 tfw_h2_unpack_priority(TfwFramePri *pri, const unsigned char *buf)
 {
diff --git a/fw/http_frame.h b/fw/http_frame.h
index 22fbc0123..ea1bd9977 100644
--- a/fw/http_frame.h
+++ b/fw/http_frame.h
@@ -251,5 +251,16 @@ tfw_h2_pack_frame_header(unsigned char *p, const TfwFrameHdr *hdr)
 	*(unsigned int *)p = htonl(hdr->stream_id);
 }
 
+static inline void
+tfw_h2_unpack_frame_header(TfwFrameHdr *hdr, const unsigned char *buf)
+{
+        hdr->length = ntohl(*(int *)buf) >> 8;
+        hdr->type = buf[3];
+        hdr->flags = buf[4];
+        hdr->stream_id = ntohl(*(unsigned int *)&buf[5]) & FRAME_STREAM_ID_MASK;
+
+        T_DBG3("%s: parsed, length=%d, stream_id=%u, type=%hhu, flags=0x%hhx\n",
+               __func__, hdr->length, hdr->stream_id, hdr->type, hdr->flags);
+}
 
 #endif /* __HTTP_FRAME__ */
diff --git a/fw/sock_clnt.c b/fw/sock_clnt.c
index 5861b74c8..11b6729f7 100644
--- a/fw/sock_clnt.c
+++ b/fw/sock_clnt.c
@@ -157,6 +157,75 @@ tfw_cli_conn_send(TfwCliConn *cli_conn, TfwMsg *msg)
 	return r;
 }
 
+static int
+ftw_sc_write_xmit(struct sock *sk, struct sk_buff *skb, unsigned int limit)
+{
+	TfwConn *conn = sk->sk_user_data;
+	TfwH2Ctx *h2;
+	TfwHPackETbl *tbl;
+	unsigned int bytes_written = 0;
+	bool h2_mode;
+	int r = 0;
+
+	assert_spin_locked(&sk->sk_lock.slock);
+
+	/*
+	 * If client closes connection early, we may get here with conn
+	 * being NULL.
+	 */
+	if (unlikely(!conn)) {
+		WARN_ON_ONCE(!sock_flag(sk, SOCK_DEAD));
+		r = -EPIPE;
+		goto err_purge_tcp_write_queue;
+	}
+
+	h2_mode = TFW_CONN_PROTO(conn) == TFW_FSM_H2;
+
+	if (h2_mode) {
+		h2 = tfw_h2_context(conn);
+		tbl = &h2->hpack.enc_tbl;
+
+		r = tfw_hpack_enc_tbl_write_sz(tbl, skb, &bytes_written);
+		if (unlikely(r)) {
+			T_WARN("%s: failed to encode new hpack dynamic "
+			       "table size (%d)", __func__, r);
+			goto ret;
+		}
+
+		tcp_sk(sk)->write_seq += bytes_written;
+		TCP_SKB_CB(skb)->end_seq += bytes_written;
+		r = ss_add_overhead(sk, bytes_written);
+		if (unlikely(r)) {
+			T_WARN("%s: failed to add overhead to current TCP "
+			       "socket control data. (%d)", __func__, r);
+			goto ret;
+		}
+	}
+
+	r = tfw_tls_encrypt(sk, skb, limit);
+
+ret:
+	if (h2_mode && unlikely(bytes_written))
+		tfw_hpack_enc_tbl_write_sz_release(tbl, r);
+	if (unlikely(r) && r != -ENOMEM)
+		/*
+		 * We can not send unencrypted data and can not normally close the
+		 * socket with FIN since we're in progress on sending from the write
+		 * queue.
+		 */
+		ss_close(sk, SS_F_ABORT);
+
+	return r;
+
+err_purge_tcp_write_queue:
+	/*
+	 * Leave encrypted segments in the retransmission rb-tree,
+	 * but purge the send queue on unencrypted segments.
+	 */
+	tcp_write_queue_purge(sk);
+	return r;
+}
+
 /**
  * This hook is called when a new client connection is established.
  */
@@ -222,7 +291,7 @@ tfw_sock_clnt_new(struct sock *sk)
 		 * upcall beside GFSM and SS, but that's efficient and I didn't
 		 * find a simple and better solution.
 		 */
-		sk->sk_write_xmit = tfw_tls_encrypt;
+		sk->sk_write_xmit = ftw_sc_write_xmit;
 
 	/* Activate keepalive timer. */
 	mod_timer(&conn->timer,
diff --git a/fw/sync_socket.h b/fw/sync_socket.h
index 25d682813..a2541590e 100644
--- a/fw/sync_socket.h
+++ b/fw/sync_socket.h
@@ -111,6 +111,22 @@ ss_proto_init(SsProto *proto, const SsHooks *hooks, int type)
 	proto->type = type;
 }
 
+/**
+ * Add overhead to current TCP socket control data.
+ */
+static inline int
+ss_add_overhead(struct sock *sk, unsigned int overhead)
+{
+	if (!overhead)
+		return 0;
+	if (!sk_wmem_schedule(sk, overhead))
+		return -ENOMEM;
+	sk->sk_wmem_queued += overhead;
+	sk_mem_charge(sk, overhead);
+
+	return 0;
+}
+
 /* Dummy user ID to differentiate server from client sockets. */
 #define SS_SRV_USER			0x11223344
 
diff --git a/fw/tls.c b/fw/tls.c
index a1ed31569..188929ba1 100644
--- a/fw/tls.c
+++ b/fw/tls.c
@@ -194,20 +194,6 @@ tfw_tls_connection_recv(TfwConn *conn, struct sk_buff *skb)
 	return r;
 }
 
-/**
- * Add the TLS record overhead to current TCP socket control data.
- */
-static int
-tfw_tls_tcp_add_overhead(struct sock *sk, unsigned int overhead)
-{
-	if (!sk_wmem_schedule(sk, overhead))
-		return -ENOMEM;
-	sk->sk_wmem_queued += overhead;
-	sk_mem_charge(sk, overhead);
-
-	return 0;
-}
-
 /**
  * Propagate TCP correct sequence numbers from the current @skb with adjusted
  * sequence numbers for TLS overhead to the next one on TCP write queue.
@@ -272,18 +258,6 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	struct page **pages = NULL, **pages_end, **p;
 	struct page *auto_pages[AUTO_SEGS_N];
 
-	assert_spin_locked(&sk->sk_lock.slock);
-
-	/*
-	 * If client closes connection early, we may get here with sk_user_data
-	 * being NULL.
-	 */
-	if (unlikely(!sk->sk_user_data)) {
-		WARN_ON_ONCE(!sock_flag(sk, SOCK_DEAD));
-		r = -EPIPE;
-		goto err_purge_tcp_write_queue;
-	}
-
 	tls = tfw_tls_context(sk->sk_user_data);
 	io = &tls->io_out;
 	xfrm = &tls->xfrm;
@@ -306,7 +280,7 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	if (!type) {
 		T_WARN("%s: bad skb type %u\n", __func__, type);
 		r = -EINVAL;
-		goto err_kill_sock;
+		goto err_epilogue;
 	}
 
 	/* TLS header is always allocated from the skb headroom. */
@@ -418,7 +392,7 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	 * So to adjust the socket write memory we have to check the both skbs
 	 * and only for TTLS_TAG_LEN.
 	 */
-	if (tfw_tls_tcp_add_overhead(sk, t_sz))
+	if (ss_add_overhead(sk, t_sz))
 		return -ENOMEM;
 
 	if (likely(sgt.nents <= AUTO_SEGS_N)) {
@@ -518,23 +492,6 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 out:
 	if (unlikely(sgt.nents > AUTO_SEGS_N))
 		kfree(sgt.sgl);
-	if (!r || r == -ENOMEM)
-		return r;
-
-	/*
-	 * We can not send unencrypted data and can not normally close the
-	 * socket with FIN since we're in progress on sending from the write
-	 * queue.
-	 */
-err_kill_sock:
-	ss_close(sk, SS_F_ABORT);
-	goto err_epilogue;
-err_purge_tcp_write_queue:
-	/*
-	 * Leave encrypted segments in the retransmission rb-tree,
-	 * but purge the send queue on unencrypted segments.
-	 */
-	tcp_write_queue_purge(sk);
 err_epilogue:
 	T_WARN("%s: cannot encrypt data (%d), only partial data was sent\n",
 	       __func__, r);
