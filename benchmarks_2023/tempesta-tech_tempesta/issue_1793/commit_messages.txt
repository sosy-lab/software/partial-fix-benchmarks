Fix kernel panic in case of small SETTINGS_HEADER_TABLE_SIZE

If there is no enough space in the ring buffer for new header
we iterate over all entries in this buffer in the loop and
clear them until we have enough space or until buffer become
empty. But checking the condition that the buffer is empty
was incorrect (pointer to the first entry is equal to pointer
to the last entry can be in case when buffer is empty and in
case when there is one entry in buffer). So we need also check
that buffer size is equal to zero.

Closes #1792 #1806
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There was error in handling of hpack dynamic table resizing -
tempesta didn't change table size in case when new size is
greater then previous hpack dynamic table size. This patch
fixed this problem.

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
-

Closes #1793
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't hpack dynamic increase table size after
  it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't hpack dynamic increase table size after
  it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't hpack dynamic increase table size after
  it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't hpack dynamic increase table size after
  it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't hpack dynamic increase table size after
  it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
Fixed incorrect handling of hpack dynamic table resizing

There were several problems in handling of hpack dynamic
table resizing:
- tempesta didn't add new table size in first response
  after resizing of hpack dynamic table.
- tempesta didn't clear all entries in hpack dynamic
  table in case when new size of this table is equal
  to zero (tempesta cleared maximum five entries).
- tempesta ignored new size which is greater than
  4096 bytes.
- tempesta didn't increase hpack dynamic table size
  after it was reduced.

This patch fixed all this problems.

Closes #1792 #1793 #1806 #1807
