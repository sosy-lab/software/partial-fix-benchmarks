diff --git a/fw/gfsm.h b/fw/gfsm.h
index 20e693b1c..a519e00e9 100644
--- a/fw/gfsm.h
+++ b/fw/gfsm.h
@@ -139,6 +139,12 @@ enum {
 	 */
 	TFW_BLOCK	= SS_DROP,
 
+        /*
+         * Same as TFW_BLOCK, but we send FIN not RST in this case
+         * to the client.
+         */
+        TFW_BAD         = SS_BAD,
+
 	/*
 	 * We need more requests (or parts of a request) to make a decision.
 	 * Current message must be stashed and will be sent to the destination
diff --git a/fw/http.c b/fw/http.c
index 4a886ca0f..c4d03fc9b 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -5292,8 +5292,9 @@ tfw_http_cli_error_resp_and_log(TfwHttpReq *req, int status, const char *msg,
 
 /**
  * Unintentional error happen during request parsing. Stop the client connection
- * from receiving new requests. Caller must return TFW_BLOCK to the
- * ss_tcp_data_ready() function for propper connection close.
+ * from receiving new requests. Caller must return TFW_BAD to the
+ * ss_tcp_data_ready() function for propper connection close with FIN after
+ * sending all pending responses.
  */
 static inline void
 tfw_http_req_parse_drop(TfwHttpReq *req, int status, const char *msg)
@@ -5890,7 +5891,7 @@ tfw_http_req_process(TfwConn *conn, TfwStream *stream, struct sk_buff *skb)
 		T_DBG2("Block invalid HTTP request\n");
 		TFW_INC_STAT_BH(clnt.msgs_parserr);
 		tfw_http_req_parse_drop(req, 400, "failed to parse request");
-		return TFW_BLOCK;
+		return TFW_BAD;
 	case TFW_POSTPONE:
 		if (WARN_ON_ONCE(parsed != data_up.skb->len)) {
 			/*
@@ -5924,8 +5925,13 @@ tfw_http_req_process(TfwConn *conn, TfwStream *stream, struct sk_buff *skb)
 			 * - trailer HEADERS frame might contain END_HEADERS as well
 			 */
 			if (ctx->hdr.flags & HTTP2_F_END_HEADERS) {
-				if (unlikely(tfw_http_parse_check_bodyless_meth(req)))
+				if (unlikely(tfw_http_parse_check_bodyless_meth(req))) {
+					tfw_http_req_parse_block(req, 400,
+						"Request contains Content-Length"
+						" or Content-Type field"
+						" for bodyless method");
 					return TFW_BLOCK;
+				}
 
 				__set_bit(TFW_HTTP_B_HEADERS_PARSED, req->flags);
 			}
@@ -5989,8 +5995,11 @@ tfw_http_req_process(TfwConn *conn, TfwStream *stream, struct sk_buff *skb)
 		skb = NULL;
 	}
 
-	if ((r = tfw_http_req_client_link(conn, req)))
+	if ((r = tfw_http_req_client_link(conn, req))) {
+		tfw_http_req_parse_block(req, 400, "request dropped: "
+					 "incorrect X-Forwarded-For header");
 		return r;
+	}
 	/*
 	 * Assign a target virtual host for the current request before further
 	 * processing.
@@ -6659,8 +6668,10 @@ tfw_http_resp_process(TfwConn *conn, TfwStream *stream, struct sk_buff *skb)
 	 * event.
 	 */
 	r = tfw_http_resp_gfsm(hmresp, &data_up);
-	if (unlikely(r < TFW_PASS))
-		return TFW_BLOCK;
+	if (unlikely(r < TFW_PASS)) {
+		filtout = true;
+		goto bad_msg;
+	}
 
 	/*
 	 * We need to know if connection will be upgraded after response
@@ -6785,12 +6796,12 @@ tfw_http_msg_process_generic(TfwConn *conn, TfwStream *stream,
 	TfwHttpMsg *req;
 
 	if (WARN_ON_ONCE(!stream))
-		return -EINVAL;
+		return TFW_BAD;
 	if (unlikely(!stream->msg)) {
 		stream->msg = tfw_http_conn_msg_alloc(conn, stream);
 		if (!stream->msg) {
 			__kfree_skb(skb);
-			return TFW_BLOCK;
+			return TFW_BAD;
 		}
 		tfw_http_mark_wl_new_msg(conn, (TfwHttpMsg *)stream->msg, skb);
 		T_DBG2("Link new msg %p with connection %p\n",
diff --git a/fw/sock.c b/fw/sock.c
index 71a69b118..7b082f8bd 100644
--- a/fw/sock.c
+++ b/fw/sock.c
@@ -908,6 +908,8 @@ ss_tcp_process_data(struct sock *sk)
 static void
 ss_tcp_data_ready(struct sock *sk)
 {
+	int flags = 0;
+
 	T_DBG3("[%d]: %s: sk=%p state=%s\n",
 	       smp_processor_id(), __func__, sk, ss_statename[sk->sk_state]);
 	assert_spin_locked(&sk->sk_lock.slock);
@@ -938,28 +940,40 @@ ss_tcp_data_ready(struct sock *sk)
 	switch (ss_tcp_process_data(sk)) {
 	case SS_OK:
 		return;
-	case SS_BAD:
+	case SS_DROP:
 		/*
-		 * Close connection in case of internal errors,
-		 * banned packets, or FIN in the received packet,
-		 * and only if it's not on hold until explicitly
-		 * closed.
-		 *
-		 * ss_close() is responsible for calling
-		 * application layer connection closing callback.
-		 * The callback will free all SKBs linked with
-		 * the message that is currently being processed.
-		 *
-		 * Closing a socket should go through the queue and
-		 * should be done after all pending data has been sent.
+		 * Here we do not close the TCP connection immediately.
+		 * If the higher layer decides that there is an attack,
+		 * but we have to send an HTTP response (i.e. Conn_Stop
+		 * flag is set), connection will be closed later after
+		 * sending response.
 		 */
-		if (!(SS_CONN_TYPE(sk) & Conn_Stop))
-			ss_close(sk, SS_F_SYNC);
+		flags = SS_F_ABORT;
 		break;
-	case SS_DROP:
-		ss_close(sk, SS_F_ABORT);
+	case SS_BAD:
+		flags = SS_F_SYNC;
 		break;
+	default:
+		WARN_ON_ONCE(flags == 0);
+		flags = SS_F_SYNC;
 	}
+
+	/*
+	 * Close connection in case of internal errors,
+	 * banned packets, or FIN in the received packet,
+	 * and only if it's not on hold until explicitly
+	 * closed.
+	 *
+	 * ss_close() is responsible for calling
+	 * application layer connection closing callback.
+	 * The callback will free all SKBs linked with
+	 * the message that is currently being processed.
+	 *
+	 * Closing a socket should go through the queue and
+	 * should be done after all pending data has been sent.
+	 */
+	if (!(SS_CONN_TYPE(sk) & Conn_Stop))
+		ss_close(sk, flags);
 }
 
 /**
diff --git a/fw/tls.c b/fw/tls.c
index c2ed0131f..140aea427 100644
--- a/fw/tls.c
+++ b/fw/tls.c
@@ -170,7 +170,7 @@ tfw_tls_connection_recv(TfwConn *conn, struct sk_buff *skb)
 
 		/* Do upcall to http or websocket */
 		r = tfw_connection_recv(conn, data_up.skb);
-		if (r == TFW_BLOCK) {
+		if (r == TFW_BLOCK || r == TFW_BAD) {
 			kfree_skb(nskb);
 			return r;
 		}
