diff --git a/fw/http_frame.c b/fw/http_frame.c
index c3ea5f5cd..8ad10f903 100644
--- a/fw/http_frame.c
+++ b/fw/http_frame.c
@@ -750,10 +750,8 @@ tfw_h2_stream_process(TfwH2Ctx *ctx, TfwStream *stream,
 	TfwStreamQueue *queue;
 	TfwStreamFsmRes r = STREAM_FSM_RES_OK;
 
-	if (tfw_h2_stream_is_closed(stream)
-	    || stream->queue == &ctx->closed_streams)
-		return STREAM_FSM_RES_TERM_STREAM;
-
+	BUG_ON (tfw_h2_stream_is_closed(stream)
+		|| stream->queue == &ctx->closed_streams);
 	BUG_ON(stream->xmit.h_len && stream->xmit.b_len);
 	/*
 	 * If it is end of headers we move stream to
@@ -895,7 +893,6 @@ static void
 tfw_h2_closed_streams_shrink(TfwH2Ctx *ctx)
 {
 	TfwStream *cur;
-	unsigned int max_streams = ctx->lsettings.max_streams;
 	TfwStreamQueue *closed_streams = &ctx->closed_streams;
 
 	T_DBG3("%s: ctx [%p] max_streams %u\n", __func__, ctx, max_streams);
@@ -903,10 +900,7 @@ tfw_h2_closed_streams_shrink(TfwH2Ctx *ctx)
 	while (1) {
 		spin_lock(&ctx->lock);
 
-		if (closed_streams->num <= TFW_MAX_CLOSED_STREAMS
-		    || (max_streams == ctx->streams_num
-			&& closed_streams->num))
-		{
+		if (closed_streams->num <= TFW_MAX_CLOSED_STREAMS) {
 			spin_unlock(&ctx->lock);
 			break;
 		}
@@ -914,7 +908,6 @@ tfw_h2_closed_streams_shrink(TfwH2Ctx *ctx)
 		BUG_ON(list_empty(&closed_streams->list));
 		cur = list_first_entry(&closed_streams->list, TfwStream,
 				       hcl_node);
-		BUG_ON(cur->xmit.h_len || cur->xmit.b_len);
 		__tfw_h2_stream_unlink(ctx, cur);
 
 		spin_unlock(&ctx->lock);
@@ -1431,8 +1424,8 @@ tfw_h2_frame_type_process(TfwH2Ctx *ctx)
 			goto conn_term;
 		}
 
-		ctx->cur_stream = tfw_h2_find_stream(&ctx->sched,
-							hdr->stream_id);
+		ctx->cur_stream =
+			tfw_h2_find_stream_if_not_closed(ctx, hdr->stream_id);
 		/*
 		 * If stream is removed, it had been closed before, so this is
 		 * connection error (see RFC 7540 section 5.1).
@@ -1460,8 +1453,8 @@ tfw_h2_frame_type_process(TfwH2Ctx *ctx)
 			goto conn_term;
 		}
 
-		ctx->cur_stream = tfw_h2_find_stream(&ctx->sched,
-						     hdr->stream_id);
+		ctx->cur_stream =
+			tfw_h2_find_stream_if_not_closed(ctx, hdr->stream_id);
 		if (tfw_h2_stream_id_verify(ctx)) {
 			err_code = HTTP2_ECODE_PROTO;
 			goto conn_term;
@@ -1504,15 +1497,23 @@ tfw_h2_frame_type_process(TfwH2Ctx *ctx)
 			goto conn_term;
 		}
 
-		ctx->cur_stream = tfw_h2_find_stream(&ctx->sched,
-							hdr->stream_id);
+		ctx->cur_stream =
+			tfw_h2_find_stream_if_not_closed(ctx, hdr->stream_id);
 		if (hdr->length != FRAME_PRIORITY_SIZE)
 			goto conn_term;
 
-		if (ctx->cur_stream)
+		if (ctx->cur_stream) {
 			STREAM_RECV_PROCESS(ctx, hdr);
-
-		ctx->state = HTTP2_RECV_FRAME_PRIORITY;
+			ctx->state = HTTP2_RECV_FRAME_PRIORITY;
+		} else {
+			/*
+			 * According to RFC 9113 section 5.1:
+			 * PRIORITY frames are allowed in the `closed` state,
+			 * but if the stream was moved to closed queue or was
+			 * already removed from memory, just ignore this frame.
+			 */
+			ctx->state = HTTP2_IGNORE_FRAME_DATA;
+		}
 		SET_TO_READ(ctx);
 		return T_OK;
 
@@ -1529,17 +1530,31 @@ tfw_h2_frame_type_process(TfwH2Ctx *ctx)
 		}
 
 		if (hdr->stream_id) {
-			ctx->cur_stream = tfw_h2_find_stream(&ctx->sched,
-								hdr->stream_id);
-			if (!ctx->cur_stream) {
-				err_code = HTTP2_ECODE_CLOSED;
-				goto conn_term;
+			ctx->cur_stream =
+				tfw_h2_find_stream_if_not_closed(ctx,
+								 hdr->stream_id);
+			if (ctx->cur_stream) {
+				STREAM_RECV_PROCESS(ctx, hdr);
+				ctx->state = HTTP2_RECV_FRAME_WND_UPDATE;
+			} else {
+				/*
+				 * According to RFC 9113 section 5.1:
+				 * An endpoint that sends a frame with the
+				 * END_STREAM flag set or a RST_STREAM frame
+				 * might receive a WINDOW_UPDATE or RST_STREAM
+				 * frame from its peer in the time before the
+				 * peer receives and processes the frame that
+				 * closes the stream.
+				 * But if the stream was moved to closed queue
+				 * or was already removed from memory, just
+				 * ignore this frame.
+				 */
+				ctx->state = HTTP2_IGNORE_FRAME_DATA;
 			}
-
-			STREAM_RECV_PROCESS(ctx, hdr);
+		} else {
+			ctx->state = HTTP2_RECV_FRAME_WND_UPDATE;
 		}
 
-		ctx->state = HTTP2_RECV_FRAME_WND_UPDATE;
 		SET_TO_READ(ctx);
 		return T_OK;
 
@@ -1606,16 +1621,25 @@ tfw_h2_frame_type_process(TfwH2Ctx *ctx)
 			goto conn_term;
 		}
 
-		ctx->cur_stream = tfw_h2_find_stream(&ctx->sched,
-							hdr->stream_id);
-		if (!ctx->cur_stream) {
-			err_code = HTTP2_ECODE_CLOSED;
-			goto conn_term;
+		ctx->cur_stream =
+			tfw_h2_find_stream_if_not_closed(ctx, hdr->stream_id);
+		if (ctx->cur_stream) {
+			STREAM_RECV_PROCESS(ctx, hdr);
+			ctx->state = HTTP2_RECV_FRAME_RST_STREAM;
+		} else {
+			/*
+			 * According to RFC 9113 section 5.1:
+			 * An endpoint that sends a frame with the END_STREAM
+			 * flag set or a RST_STREAM frame might receive a
+			 * WINDOW_UPDATE or RST_STREAM frame from its peer in
+			 * the time before the peer receives and processes the
+			 * frame that closes the stream.
+			 * But if the stream was moved to closed queue or was
+			 * already removed from memory, just ignore this frame.
+			 */
+			ctx->state = HTTP2_IGNORE_FRAME_DATA;
 		}
 
-		STREAM_RECV_PROCESS(ctx, hdr);
-
-		ctx->state = HTTP2_RECV_FRAME_RST_STREAM;
 		SET_TO_READ(ctx);
 		return T_OK;
 
@@ -1646,8 +1670,8 @@ tfw_h2_frame_type_process(TfwH2Ctx *ctx)
 			goto conn_term;
 		}
 
-		ctx->cur_stream = tfw_h2_find_stream(&ctx->sched,
-							hdr->stream_id);
+		ctx->cur_stream =
+			tfw_h2_find_stream_if_not_closed(ctx, hdr->stream_id);
 		if (!ctx->cur_stream) {
 			err_code = HTTP2_ECODE_CLOSED;
 			goto conn_term;
@@ -2277,4 +2301,15 @@ tfw_h2_make_data_frames(struct sock *sk, struct sk_buff *skb,
 {
 	return tfw_h2_make_frames(sk, skb, ctx, stream, HTTP2_DATA,
 				  mss_now, limit, t_tz);
-}
\ No newline at end of file
+}
+
+TfwStream *tfw_h2_find_stream_if_not_closed(TfwH2Ctx *ctx, unsigned int id)
+{
+	TfwStream *stream;
+
+	stream = tfw_h2_find_stream(&ctx->sched, id);
+	if (!stream || stream->queue == &ctx->closed_streams)
+		return NULL;
+
+	return stream;
+}
diff --git a/fw/http_frame.h b/fw/http_frame.h
index e3664020b..254a19e6d 100644
--- a/fw/http_frame.h
+++ b/fw/http_frame.h
@@ -228,6 +228,7 @@ int tfw_h2_context_init(TfwH2Ctx *ctx);
 void tfw_h2_context_clear(TfwH2Ctx *ctx);
 int tfw_h2_frame_process(TfwConn *c, struct sk_buff *skb);
 void tfw_h2_conn_streams_cleanup(TfwH2Ctx *ctx);
+TfwStream *tfw_h2_find_stream_if_not_closed(TfwH2Ctx *ctx, unsigned int id);
 unsigned int tfw_h2_stream_id(TfwHttpReq *req);
 unsigned int tfw_h2_stream_id_send(TfwHttpReq *req, unsigned char type,
 				   unsigned char flags);
diff --git a/fw/sock_clnt.c b/fw/sock_clnt.c
index 4dd669e23..aceab8246 100644
--- a/fw/sock_clnt.c
+++ b/fw/sock_clnt.c
@@ -174,7 +174,7 @@ tfw_h2_sk_prepare_xmit(struct sock *sk, struct sk_buff *skb, unsigned int mss_no
 	TfwHPackETbl *tbl = &h2->hpack.enc_tbl;
 	unsigned short flags = skb_tfw_flags(skb);
 	unsigned int skb_priv = skb_tfw_cb(skb);
-	TfwStream *stream = tfw_h2_find_stream(&h2->sched, skb_priv);
+	TfwStream *stream = tfw_h2_find_stream_if_not_closed(h2, skb_priv);
 	unsigned int truesize = 0, tmp_truesize = 0;
 	bool headers_was_done = false;
 	int r = 0;
