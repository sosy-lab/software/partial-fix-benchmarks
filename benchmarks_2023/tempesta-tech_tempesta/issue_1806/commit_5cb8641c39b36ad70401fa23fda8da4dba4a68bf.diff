diff --git a/fw/hpack.c b/fw/hpack.c
index 25b19e925..af34408b7 100644
--- a/fw/hpack.c
+++ b/fw/hpack.c
@@ -3072,7 +3072,7 @@ tfw_hpack_rbuf_calc(TfwHPackETbl *__restrict tbl, unsigned short new_size,
 	do {
 		unsigned short f_len, fhdr_len;
 
-		if (i >= HPACK_MAX_ENC_EVICTION)
+		if (i >= HPACK_MAX_ENC_EVICTION && del_list)
 			return -E2BIG;
 
 		if (unlikely(!size))
@@ -3991,8 +3991,11 @@ tfw_hpack_encode(TfwHttpResp *__restrict resp, TfwStr *__restrict hdr,
 void
 tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 {
-	if (WARN_ON_ONCE(new_size > HPACK_ENC_TABLE_MAX_SIZE))
-		return;
+	if (new_size > HPACK_ENC_TABLE_MAX_SIZE) {
+		T_WARN("Client requests hpack table size (%hu), which is "
+			"greater than HPACK_ENC_TABLE_MAX_SIZE.", new_size);
+		new_size = HPACK_ENC_TABLE_MAX_SIZE;
+	}
 
 	spin_lock(&tbl->lock);
 
@@ -4000,14 +4003,95 @@ tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 	       " new_size=%hu\n", __func__, tbl->rb_len, tbl->size,
 	       tbl->window, new_size);
 
-	if (tbl->window > new_size) {
+	/*
+	 * RFC7541#section-4.2:
+	 * Multiple updates to the maximum table size can occur between the
+	 * transmission of two header blocks. In the case that this size is
+	 * changed more than once in this interval, the smallest maximum table
+	 * size that occurs in that interval MUST be signaled in a dynamic
+	 * table size update.
+	 */
+	if (tbl->window != new_size && (likely(!atomic_read(&tbl->wnd_changed))
+	    || unlikely(!tbl->window) || new_size < tbl->window))
+	{
 		if (tbl->size > new_size)
 			tfw_hpack_rbuf_calc(tbl, new_size, NULL,
 					    (TfwHPackETblIter *)tbl);
 		WARN_ON_ONCE(tbl->rb_len > tbl->size);
 
 		tbl->window = new_size;
+		atomic_set(&tbl->wnd_changed, 1);
 	}
 
 	spin_unlock(&tbl->lock);
 }
+
+int
+tfw_hpack_enc_tbl_write_sz(TfwHPackETbl *__restrict tbl, struct sk_buff *skb,
+			   unsigned int *bytes_wtitten)
+{
+	TfwMsgIter it = { .frag = -1, .skb = skb, .skb_head = skb};
+	TfwStr new_size = {};
+	TfwHPackInt tmp = {};
+	TfwFrameHdr hdr = {};
+	char *data = skb->data;
+	int r = 0;
+
+	/*
+	 * Check header type. Sometimes because of skb splitting,
+	 * headers data can present here, but HTTP2_HEADERS code
+	 * (0x01) is not valid for headers data.
+	 */
+	if (skb->data[3] != HTTP2_HEADERS)
+		return 0;
+
+	/*
+	 * We should encode hpack dynamic table size, only in case when
+	 * it was changed and only once.
+	 */
+	if (unlikely(atomic_cmpxchg(&tbl->wnd_changed, 1, -1) == 1)) {
+		write_int(tbl->window, 0x1F, 0x20, &tmp);
+		new_size.data = tmp.buf;
+		new_size.len = tmp.sz;
+	
+		r = tfw_msg_iter_move(&it, (unsigned char **)&data,
+				      FRAME_HEADER_SIZE);
+		if (unlikely(r))
+			goto finish;
+
+		r = tfw_http_msg_insert(&it, &data, &new_size);
+		if (unlikely(r))
+			goto finish;
+
+		tfw_h2_unpack_frame_header(&hdr, skb->data);
+		hdr.length += tmp.sz;
+		tfw_h2_pack_frame_header(skb->data, &hdr);
+
+		*bytes_wtitten = tmp.sz;
+	}
+
+finish:
+	if (unlikely(r))
+		/*
+		 * In case of error we should restore value of `wnd_changed`
+		 * flag.
+		 */
+		atomic_set(&tbl->wnd_changed, 1);
+	return r;	
+}
+
+void
+tfw_hpack_enc_tbl_write_sz_release(TfwHPackETbl *__restrict tbl, int r)
+{
+	/*
+	 * Before calling this function, we should check that we encode
+	 * new dynamic table size into the frame, so `old` can have only
+	 * two values (-1 in most of all cases, since we set it previosly
+	 * or 1 if changing of dynamic table size was occured, before this
+	 * function is called).
+	 * We should change this flag only if it wasn't changed by
+	 * `tfw_hpack_set_rbuf_size` function.
+	 */
+	int old = atomic_cmpxchg(&tbl->wnd_changed, -1, r == 0 ? 0 : 1);
+	WARN_ON_ONCE(!old);
+}
diff --git a/fw/hpack.h b/fw/hpack.h
index 0990af777..375b42bf3 100644
--- a/fw/hpack.h
+++ b/fw/hpack.h
@@ -80,6 +80,13 @@ typedef struct {
  *
  * @window	- maximum pseudo-length of the dynamic table (in bytes); this
  *		  value used as threshold to flushing old entries;
+ * @wnd_changed	- flag indicates, that window was changed by settings update,
+ * 		- can be in three states:
+ * 		- 0 in case when window size isn't changed.
+ * 		- 1 in case when window size is changed and it should be written
+ * 		  into the first response, before the first header block.
+ * 		- -1 in case when window size is written into the first response,
+ * 		  but this response was not sent to a client yet.
  * @rbuf	- pointer to the ring buffer;
  * @root	- pointer to the root node of binary tree;
  * @pool	- memory pool for dynamic table;
@@ -92,6 +99,7 @@ typedef struct {
 typedef struct {
 	TFW_HPACK_ETBL_COMMON;
 	unsigned short		window;
+	atomic_t		wnd_changed;
 	char			*rbuf;
 	TfwHPackNode		*root;
 	TfwPool			*pool;
@@ -279,6 +287,9 @@ int tfw_hpack_cache_decode_expand(TfwHPack *__restrict hp,
 				  unsigned char *__restrict src, unsigned long n,
 				  TfwDecodeCacheIter *__restrict cd_iter);
 void tfw_hpack_enc_release(TfwHPack *__restrict hp, unsigned long *flags);
+int tfw_hpack_enc_tbl_write_sz(TfwHPackETbl *__restrict tbl, struct sk_buff * __restrict skb,
+			       unsigned int *bytes_wtitten);
+void tfw_hpack_enc_tbl_write_sz_release(TfwHPackETbl *__restrict tbl, int r);
 
 static inline unsigned int
 tfw_hpack_int_size(unsigned long index, unsigned short max)
diff --git a/fw/http.c b/fw/http.c
index 7e9af5563..16a83d666 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -4994,9 +4994,11 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		h_len -= max_sz;
 		frame_hdr.type = HTTP2_CONTINUATION;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     fr_flags);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 fr_flags);
+		if (unlikely(r))
+			return r;
 	}
 
 	if (local_response)
@@ -5025,9 +5027,11 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		b_len -= max_sz;
 		frame_hdr.type = HTTP2_DATA;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     HTTP2_F_END_STREAM);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 HTTP2_F_END_STREAM);
+		if (unlikely(r))
+			return r;
 	}
 
 	return 0;
diff --git a/fw/http_frame.c b/fw/http_frame.c
index afcc796c4..3cca192b9 100644
--- a/fw/http_frame.c
+++ b/fw/http_frame.c
@@ -251,18 +251,6 @@ tfw_h2_context_clear(TfwH2Ctx *ctx)
 	tfw_hpack_clean(&ctx->hpack);
 }
 
-static inline void
-tfw_h2_unpack_frame_header(TfwFrameHdr *hdr, const unsigned char *buf)
-{
-	hdr->length = ntohl(*(int *)buf) >> 8;
-	hdr->type = buf[3];
-	hdr->flags = buf[4];
-	hdr->stream_id = ntohl(*(unsigned int *)&buf[5]) & FRAME_STREAM_ID_MASK;
-
-	T_DBG3("%s: parsed, length=%d, stream_id=%u, type=%hhu, flags=0x%hhx\n",
-	       __func__, hdr->length, hdr->stream_id, hdr->type, hdr->flags);
-}
-
 static inline void
 tfw_h2_unpack_priority(TfwFramePri *pri, const unsigned char *buf)
 {
diff --git a/fw/http_frame.h b/fw/http_frame.h
index 22fbc0123..ea1bd9977 100644
--- a/fw/http_frame.h
+++ b/fw/http_frame.h
@@ -251,5 +251,16 @@ tfw_h2_pack_frame_header(unsigned char *p, const TfwFrameHdr *hdr)
 	*(unsigned int *)p = htonl(hdr->stream_id);
 }
 
+static inline void
+tfw_h2_unpack_frame_header(TfwFrameHdr *hdr, const unsigned char *buf)
+{
+        hdr->length = ntohl(*(int *)buf) >> 8;
+        hdr->type = buf[3];
+        hdr->flags = buf[4];
+        hdr->stream_id = ntohl(*(unsigned int *)&buf[5]) & FRAME_STREAM_ID_MASK;
+
+        T_DBG3("%s: parsed, length=%d, stream_id=%u, type=%hhu, flags=0x%hhx\n",
+               __func__, hdr->length, hdr->stream_id, hdr->type, hdr->flags);
+}
 
 #endif /* __HTTP_FRAME__ */
diff --git a/fw/sock.c b/fw/sock.c
index d05564aa6..b80f693d5 100644
--- a/fw/sock.c
+++ b/fw/sock.c
@@ -372,7 +372,7 @@ static void
 ss_do_send(struct sock *sk, struct sk_buff **skb_head, int flags)
 {
 	struct tcp_sock *tp = tcp_sk(sk);
-	struct sk_buff *skb;
+	struct sk_buff *skb, *head = *skb_head;
 	int size, mss = tcp_send_mss(sk, &size, MSG_DONTWAIT);
 	unsigned int mark = (*skb_head)->mark;
 
@@ -403,8 +403,11 @@ ss_do_send(struct sock *sk, struct sk_buff **skb_head, int flags)
 		}
 
 		ss_skb_init_for_xmit(skb);
-		if (flags & SS_F_ENCRYPT)
-			tempesta_tls_skb_settype(skb, SS_SKB_F2TYPE(flags));
+		if (flags & SS_F_ENCRYPT) {
+			ss_skb_set_tls_type(skb, SS_SKB_F2TYPE(flags));
+			if (skb == head)
+				ss_skb_set_flags(skb, SS_F_HTTP2_FRAME_START);
+		}
 		/* Propagate mark of message head skb.*/
 		skb->mark = mark;
 
@@ -412,7 +415,7 @@ ss_do_send(struct sock *sk, struct sk_buff **skb_head, int flags)
 		       " truesize=%u mark=%u tls_type=%x\n",
 		       smp_processor_id(), __func__, sk,
 		       skb, skb->data_len, skb->len, skb->truesize, skb->mark,
-		       tempesta_tls_skb_type(skb));
+		       ss_skb_tls_type(skb));
 
 		ss_forced_mem_schedule(sk, skb->truesize);
 		skb_entail(sk, skb);
diff --git a/fw/sock_clnt.c b/fw/sock_clnt.c
index 27793e4fa..1414a1c9d 100644
--- a/fw/sock_clnt.c
+++ b/fw/sock_clnt.c
@@ -157,6 +157,89 @@ tfw_cli_conn_send(TfwCliConn *cli_conn, TfwMsg *msg)
 	return r;
 }
 
+static int
+tfw_sc_write_xmit(struct sock *sk, struct sk_buff *skb, unsigned int limit)
+{
+	TfwConn *conn = sk->sk_user_data;
+	TfwH2Ctx *h2;
+	TfwHPackETbl *tbl;
+	unsigned int bytes_written = 0;
+	unsigned char flags;
+	bool h2_mode;
+	int r = 0;
+
+	assert_spin_locked(&sk->sk_lock.slock);
+
+	/*
+	 * If client closes connection early, we may get here with conn
+	 * being NULL.
+	 */
+	if (unlikely(!conn)) {
+		WARN_ON_ONCE(!sock_flag(sk, SOCK_DEAD));
+		r = -EPIPE;
+		goto err_purge_tcp_write_queue;
+	}
+
+	h2_mode = TFW_CONN_PROTO(conn) == TFW_FSM_H2;
+	flags = ss_skb_flags(skb);
+
+	/*
+	 * We should write new hpack dynamic table size at the
+	 * beginning of the first header block, which is always
+	 * in linear part of skb. If skb_headlen is smaller
+	 * than FRAME_HEADER_SIZE, it means that skb doesn't
+	 * contain valid frame header in it's linear part.
+	 */
+	if (h2_mode && flags & SS_F_HTTP2_FRAME_START
+	    && skb_headlen(skb) >= FRAME_HEADER_SIZE)
+	{
+		h2 = tfw_h2_context(conn);
+		tbl = &h2->hpack.enc_tbl;
+
+		r = tfw_hpack_enc_tbl_write_sz(tbl, skb, &bytes_written);
+		if (unlikely(r)) {
+			T_WARN("%s: failed to encode new hpack dynamic "
+			       "table size (%d)", __func__, r);
+			goto ret;
+		}
+
+		tcp_sk(sk)->write_seq += bytes_written;
+		TCP_SKB_CB(skb)->end_seq += bytes_written;
+		r = ss_add_overhead(sk, bytes_written);
+		if (unlikely(r)) {
+			T_WARN("%s: failed to add overhead to current TCP "
+			       "socket control data. (%d)", __func__, r);
+			goto ret;
+		}
+	}
+
+	ss_skb_clear_flags(skb);
+	r = tfw_tls_encrypt(sk, skb, limit);
+
+ret:
+	if (h2_mode && unlikely(bytes_written))
+		tfw_hpack_enc_tbl_write_sz_release(tbl, r);
+	if (unlikely(r) && r != -ENOMEM)
+		/*
+		 * We can not send unencrypted data and can not normally close the
+		 * socket with FIN since we're in progress on sending from the write
+		 * queue.
+		 */
+		ss_close(sk, SS_F_ABORT);
+
+	if (unlikely(r))
+		ss_skb_set_flags(skb, flags);
+	return r;
+
+err_purge_tcp_write_queue:
+	/*
+	 * Leave encrypted segments in the retransmission rb-tree,
+	 * but purge the send queue on unencrypted segments.
+	 */
+	tcp_write_queue_purge(sk);
+	return r;
+}
+
 /**
  * This hook is called when a new client connection is established.
  */
@@ -222,7 +305,7 @@ tfw_sock_clnt_new(struct sock *sk)
 		 * upcall beside GFSM and SS, but that's efficient and I didn't
 		 * find a simple and better solution.
 		 */
-		sk->sk_write_xmit = tfw_tls_encrypt;
+		sk->sk_write_xmit = tfw_sc_write_xmit;
 
 	/* Activate keepalive timer. */
 	mod_timer(&conn->timer,
diff --git a/fw/ss_skb.h b/fw/ss_skb.h
index 6ffe2bf9a..1e3ba2aec 100644
--- a/fw/ss_skb.h
+++ b/fw/ss_skb.h
@@ -49,6 +49,46 @@ enum {
 typedef int ss_skb_actor_t(void *conn, unsigned char *data, unsigned int len,
 			   unsigned int *read);
 
+static inline unsigned char
+ss_skb_flags(struct sk_buff *skb)
+{
+	return tempesta_skb_get_cb_val(skb, TEMPESTA_SKB_FLAG_OFF,
+				       TEMPESTA_SKB_FLAG_MAX);
+}
+
+static inline void
+ss_skb_set_flags(struct sk_buff *skb, unsigned char flags)
+{
+	return tempesta_skb_set_cb_val(skb, flags, TEMPESTA_SKB_FLAG_OFF);
+}
+
+static inline void
+ss_skb_clear_flags(struct sk_buff *skb)
+{
+	return tempesta_skb_clear_cb_val(skb, TEMPESTA_SKB_FLAG_MAX,
+					 TEMPESTA_SKB_FLAG_OFF);
+}
+
+static inline unsigned char
+ss_skb_tls_type(struct sk_buff *skb)
+{
+	return tempesta_skb_get_cb_val(skb, TEMPESTA_TLS_SKB_TYPE_OFF,
+				       TEMPESTA_TLS_SKB_TYPE_MAX);
+}
+
+static inline void
+ss_skb_set_tls_type(struct sk_buff *skb, unsigned char type)
+{
+	return tempesta_skb_set_cb_val(skb, type, TEMPESTA_TLS_SKB_TYPE_OFF);
+}
+
+static inline void
+ss_skb_clear_type(struct sk_buff *skb)
+{
+	return tempesta_skb_clear_cb_val(skb, TEMPESTA_TLS_SKB_TYPE_MAX,
+					 TEMPESTA_TLS_SKB_TYPE_OFF);
+}
+
 /**
  * Add new _single_ @skb to the queue in FIFO order.
  */
diff --git a/fw/sync_socket.h b/fw/sync_socket.h
index 273b0979b..2e73ddb3c 100644
--- a/fw/sync_socket.h
+++ b/fw/sync_socket.h
@@ -111,6 +111,22 @@ ss_proto_init(SsProto *proto, const SsHooks *hooks, int type)
 	proto->type = type;
 }
 
+/**
+ * Add overhead to current TCP socket control data.
+ */
+static inline int
+ss_add_overhead(struct sock *sk, unsigned int overhead)
+{
+	if (!overhead)
+		return 0;
+	if (!sk_wmem_schedule(sk, overhead))
+		return -ENOMEM;
+	sk->sk_wmem_queued += overhead;
+	sk_mem_charge(sk, overhead);
+
+	return 0;
+}
+
 /* Dummy user ID to differentiate server from client sockets. */
 #define SS_SRV_USER			0x11223344
 
@@ -126,6 +142,11 @@ ss_proto_init(SsProto *proto, const SsHooks *hooks, int type)
 #define __SS_F_RST			0x10
 #define SS_F_ABORT			(__SS_F_RST | SS_F_SYNC)
 
+enum {
+	/* This skb contains start of http2 frame. */
+	SS_F_HTTP2_FRAME_START	=	0x01,
+};
+
 /* Conversion of skb type (flag) to/from TLS record type. */
 #define SS_SKB_TYPE2F(t)		(((int)(t)) << 8)
 #define SS_SKB_F2TYPE(f)		((f) >> 8)
diff --git a/fw/tls.c b/fw/tls.c
index a1ed31569..88193db7a 100644
--- a/fw/tls.c
+++ b/fw/tls.c
@@ -194,20 +194,6 @@ tfw_tls_connection_recv(TfwConn *conn, struct sk_buff *skb)
 	return r;
 }
 
-/**
- * Add the TLS record overhead to current TCP socket control data.
- */
-static int
-tfw_tls_tcp_add_overhead(struct sock *sk, unsigned int overhead)
-{
-	if (!sk_wmem_schedule(sk, overhead))
-		return -ENOMEM;
-	sk->sk_wmem_queued += overhead;
-	sk_mem_charge(sk, overhead);
-
-	return 0;
-}
-
 /**
  * Propagate TCP correct sequence numbers from the current @skb with adjusted
  * sequence numbers for TLS overhead to the next one on TCP write queue.
@@ -272,18 +258,6 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	struct page **pages = NULL, **pages_end, **p;
 	struct page *auto_pages[AUTO_SEGS_N];
 
-	assert_spin_locked(&sk->sk_lock.slock);
-
-	/*
-	 * If client closes connection early, we may get here with sk_user_data
-	 * being NULL.
-	 */
-	if (unlikely(!sk->sk_user_data)) {
-		WARN_ON_ONCE(!sock_flag(sk, SOCK_DEAD));
-		r = -EPIPE;
-		goto err_purge_tcp_write_queue;
-	}
-
 	tls = tfw_tls_context(sk->sk_user_data);
 	io = &tls->io_out;
 	xfrm = &tls->xfrm;
@@ -292,7 +266,7 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	       " skb=%pK(len=%u data_len=%u type=%u frags=%u headlen=%u"
 	       " seq=%u:%u)\n", __func__,
 	       sk, tcp_sk(sk)->snd_una, tcp_sk(sk)->snd_nxt, limit,
-	       skb, skb->len, skb->data_len, tempesta_tls_skb_type(skb),
+	       skb, skb->len, skb->data_len, ss_skb_tls_type(skb),
 	       skb_shinfo(skb)->nr_frags, skb_headlen(skb),
 	       tcb->seq, tcb->end_seq);
 	BUG_ON(!ttls_xfrm_ready(tls));
@@ -302,11 +276,11 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 
 	head_sz = ttls_payload_off(xfrm);
 	len = head_sz + skb->len + TTLS_TAG_LEN;
-	type = tempesta_tls_skb_type(skb);
+	type = ss_skb_tls_type(skb);
 	if (!type) {
 		T_WARN("%s: bad skb type %u\n", __func__, type);
 		r = -EINVAL;
-		goto err_kill_sock;
+		goto err_epilogue;
 	}
 
 	/* TLS header is always allocated from the skb headroom. */
@@ -319,13 +293,13 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 		T_DBG3("next skb (%pK) in write queue: len=%u frags=%u/%u"
 		       " type=%u seq=%u:%u\n",
 		       next, next->len, skb_shinfo(next)->nr_frags,
-		       !!skb_headlen(next), tempesta_tls_skb_type(next),
+		       !!skb_headlen(next), ss_skb_tls_type(next),
 		       TCP_SKB_CB(next)->seq, TCP_SKB_CB(next)->end_seq);
 
 		if (len + next->len > limit)
 			break;
 		/* Don't put different message types into the same record. */
-		if (type != tempesta_tls_skb_type(next))
+		if (type != ss_skb_tls_type(next))
 			break;
 
 		/*
@@ -418,7 +392,7 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	 * So to adjust the socket write memory we have to check the both skbs
 	 * and only for TTLS_TAG_LEN.
 	 */
-	if (tfw_tls_tcp_add_overhead(sk, t_sz))
+	if (ss_add_overhead(sk, t_sz))
 		return -ENOMEM;
 
 	if (likely(sgt.nents <= AUTO_SEGS_N)) {
@@ -469,7 +443,7 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 			goto out;
 		out_frags += r;
 
-		tempesta_tls_skb_clear(next);
+		tempesta_skb_clear_cb(next);
 		if (next == skb_tail)
 			break;
 		if (WARN_ON_ONCE(frags >= sgt.nents))
@@ -518,23 +492,8 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 out:
 	if (unlikely(sgt.nents > AUTO_SEGS_N))
 		kfree(sgt.sgl);
-	if (!r || r == -ENOMEM)
+	if (!r)
 		return r;
-
-	/*
-	 * We can not send unencrypted data and can not normally close the
-	 * socket with FIN since we're in progress on sending from the write
-	 * queue.
-	 */
-err_kill_sock:
-	ss_close(sk, SS_F_ABORT);
-	goto err_epilogue;
-err_purge_tcp_write_queue:
-	/*
-	 * Leave encrypted segments in the retransmission rb-tree,
-	 * but purge the send queue on unencrypted segments.
-	 */
-	tcp_write_queue_purge(sk);
 err_epilogue:
 	T_WARN("%s: cannot encrypt data (%d), only partial data was sent\n",
 	       __func__, r);
diff --git a/linux-5.10.35.patch b/linux-5.10.35.patch
index dacf77df4..c78b51fe9 100644
--- a/linux-5.10.35.patch
+++ b/linux-5.10.35.patch
@@ -545,7 +545,7 @@ index e37480b5f..617f4e76b 100644
  
  /*
 diff --git a/include/linux/skbuff.h b/include/linux/skbuff.h
-index a828cf99c..b877eb543 100644
+index a828cf99c..50f3419b4 100644
 --- a/include/linux/skbuff.h
 +++ b/include/linux/skbuff.h
 @@ -232,6 +232,12 @@
@@ -581,60 +581,91 @@ index a828cf99c..b877eb543 100644
  
  	__u8			ipvs_property:1;
  	__u8			inner_protocol_type:1;
-@@ -931,6 +943,52 @@ struct sk_buff {
+@@ -931,6 +943,83 @@ struct sk_buff {
  #define SKB_ALLOC_RX		0x02
  #define SKB_ALLOC_NAPI		0x04
  
 +#ifdef CONFIG_SECURITY_TEMPESTA
 +long __get_skb_count(void);
 +
++enum {
++	TEMPESTA_TLS_SKB_TYPE_OFF	= 0,
++	TEMPESTA_TLS_SKB_TYPE_MAX	= 0x7F,
++	TEMPESTA_SKB_FLAG_OFF		= 7,
++	TEMPESTA_SKB_FLAG_CLEAR_MASK	= 0x01,
++	TEMPESTA_SKB_FLAG_MAX		= 0xFF,
++};
++
 +/**
-+ * The skb type is used only for time between @skb was inserted into TCP send
++ * Tempesta uses skb->dev only for time between @skb was inserted into TCP send
 + * queue and it's processed (first time) in tcp_write_xmit(). This time the @skb
 + * isn't scheduled yet, so we can use skb->dev for our needs to avoid extending
 + * sk_buff. We use the least significant bit to be sure that this isn't a
-+ * pointer to not to break anything. TLS message type << 1 is always smaller
-+ * than 0xff.
++ * pointer to not to break anything.
 + */
 +static inline void
-+tempesta_tls_skb_settype(struct sk_buff *skb, unsigned char type)
++tempesta_skb_set_cb_val(struct sk_buff *skb, unsigned long val, unsigned char off)
 +{
-+	BUG_ON(type >= 0x80);
-+	WARN_ON_ONCE(skb->dev);
++	unsigned long d = (unsigned long)skb->dev;
 +
-+	skb->dev = (void *)((type << 1) | 1UL);
++	BUG_ON(off + 1 >= sizeof(skb->dev) * BITS_PER_BYTE);
++	BUG_ON(!val || __builtin_clzl(val) < off + 1);
++	BUG_ON(skb->dev && !((unsigned long)skb->dev & 1UL));
++
++	d |= ((val << (off + 1)) | 1UL);
++	skb->dev = (void *)d;
 +}
 +
-+static inline unsigned char
-+tempesta_tls_skb_type(struct sk_buff *skb)
++static inline unsigned long
++tempesta_skb_get_cb_val(struct sk_buff *skb, unsigned char off, unsigned long mask)
 +{
 +	unsigned long d = (unsigned long)skb->dev;
 +
++	BUG_ON(off + 1 >= sizeof(skb->dev) * BITS_PER_BYTE);
 +	if (!(d & 1UL))
 +		return 0; /* a pointer in skb->dev */
-+	return d >> 1;
++	return (d >> (off + 1)) & mask;
 +}
 +
 +static inline void
-+tempesta_tls_skb_typecp(struct sk_buff *dst, struct sk_buff *src)
++tempesta_skb_clear_cb_val(struct sk_buff *skb, unsigned long val, unsigned char off)
++{
++	unsigned long d = (unsigned long)skb->dev;
++	BUG_ON(off + 1 >= sizeof(skb->dev) * BITS_PER_BYTE);
++	BUG_ON(!val || __builtin_clzl(val) < off + 1);
++	BUG_ON(skb->dev && !((unsigned long)skb->dev & 1UL));
++
++	d &= ~(val << (off + 1));
++	skb->dev = (void *)d;
++}
++
++static inline unsigned long
++is_tempesta_skb_cb(struct sk_buff *skb)
++{
++	return ((unsigned long)skb->dev) & 1UL;
++}
++
++static inline void
++tempesta_skb_copy_cb(struct sk_buff *dst, struct sk_buff *src)
 +{
 +	dst->dev = src->dev;
 +}
 +
 +static inline void
-+tempesta_tls_skb_clear(struct sk_buff *skb)
++tempesta_skb_clear_cb(struct sk_buff *skb)
 +{
 +	unsigned long d = (unsigned long)skb->dev;
 +
-+	WARN_ON_ONCE(d & ~0xff);
++	WARN_ON_ONCE(!(d & 1UL));
 +	skb->dev = NULL;
 +}
++
 +#endif
 +
  /**
   * skb_pfmemalloc - Test if the skb was allocated from PFMEMALLOC reserves
   * @skb: buffer
-@@ -1074,6 +1132,7 @@ void kfree_skb_partial(struct sk_buff *skb, bool head_stolen);
+@@ -1074,6 +1163,7 @@ void kfree_skb_partial(struct sk_buff *skb, bool head_stolen);
  bool skb_try_coalesce(struct sk_buff *to, struct sk_buff *from,
  		      bool *fragstolen, int *delta_truesize);
  
@@ -642,7 +673,7 @@ index a828cf99c..b877eb543 100644
  struct sk_buff *__alloc_skb(unsigned int size, gfp_t priority, int flags,
  			    int node);
  struct sk_buff *__build_skb(void *data, unsigned int frag_size);
-@@ -2104,7 +2163,11 @@ struct sk_buff *skb_dequeue_tail(struct sk_buff_head *list);
+@@ -2104,7 +2194,11 @@ struct sk_buff *skb_dequeue_tail(struct sk_buff_head *list);
  
  static inline bool skb_is_nonlinear(const struct sk_buff *skb)
  {
@@ -654,7 +685,7 @@ index a828cf99c..b877eb543 100644
  }
  
  static inline unsigned int skb_headlen(const struct sk_buff *skb)
-@@ -2341,6 +2404,20 @@ static inline unsigned int skb_headroom(const struct sk_buff *skb)
+@@ -2341,6 +2435,20 @@ static inline unsigned int skb_headroom(const struct sk_buff *skb)
  	return skb->data - skb->head;
  }
  
@@ -2193,7 +2224,7 @@ index ab8ed0fc4..e260a0af6 100644
  		goto put_and_exit;
  	*own_req = inet_ehash_nolisten(newsk, req_to_sk(req_unhash),
 diff --git a/net/ipv4/tcp_output.c b/net/ipv4/tcp_output.c
-index f99494637..12ef8eb20 100644
+index f99494637..8a48826a9 100644
 --- a/net/ipv4/tcp_output.c
 +++ b/net/ipv4/tcp_output.c
 @@ -39,6 +39,9 @@
@@ -2240,12 +2271,15 @@ index f99494637..12ef8eb20 100644
  
  /* Initialize TSO segments for a packet. */
  static void tcp_set_skb_tso_segs(struct sk_buff *skb, unsigned int mss_now)
-@@ -1518,12 +1523,39 @@ static void tcp_insert_write_queue_after(struct sk_buff *skb,
+@@ -1518,12 +1523,42 @@ static void tcp_insert_write_queue_after(struct sk_buff *skb,
  					 struct sock *sk,
  					 enum tcp_queue tcp_queue)
  {
 +#ifdef CONFIG_SECURITY_TEMPESTA
-+	tempesta_tls_skb_typecp(buff, skb);
++	tempesta_skb_copy_cb(buff, skb);
++	if (is_tempesta_skb_cb(buff))
++		tempesta_skb_clear_cb_val(buff, TEMPESTA_SKB_FLAG_CLEAR_MASK,
++					  TEMPESTA_SKB_FLAG_OFF);
 +#endif
  	if (tcp_queue == TCP_FRAG_IN_WRITE_QUEUE)
  		__skb_queue_after(&sk->sk_write_queue, skb, buff);
@@ -2280,7 +2314,7 @@ index f99494637..12ef8eb20 100644
  /* Function to create two new TCP segments.  Shrinks the given segment
   * to the specified size and appends a new segment with the rest of the
   * packet to the list.  This won't be called frequently, I hope.
-@@ -1561,7 +1593,7 @@ int tcp_fragment(struct sock *sk, enum tcp_queue tcp_queue,
+@@ -1561,7 +1596,7 @@ int tcp_fragment(struct sock *sk, enum tcp_queue tcp_queue,
  		return -ENOMEM;
  	}
  
@@ -2289,7 +2323,7 @@ index f99494637..12ef8eb20 100644
  		return -ENOMEM;
  
  	/* Get a new skb... force flag on. */
-@@ -1670,7 +1702,7 @@ int tcp_trim_head(struct sock *sk, struct sk_buff *skb, u32 len)
+@@ -1670,7 +1705,7 @@ int tcp_trim_head(struct sock *sk, struct sk_buff *skb, u32 len)
  {
  	u32 delta_truesize;
  
@@ -2298,7 +2332,7 @@ index f99494637..12ef8eb20 100644
  		return -ENOMEM;
  
  	delta_truesize = __pskb_trim_head(skb, len);
-@@ -1848,6 +1880,7 @@ unsigned int tcp_current_mss(struct sock *sk)
+@@ -1848,6 +1883,7 @@ unsigned int tcp_current_mss(struct sock *sk)
  
  	return mss_now;
  }
@@ -2306,14 +2340,23 @@ index f99494637..12ef8eb20 100644
  
  /* RFC2861, slow part. Adjust cwnd, after it was not full during one rto.
   * As additional protections, we do not touch cwnd in retransmission phases,
-@@ -2666,7 +2699,20 @@ static bool tcp_write_xmit(struct sock *sk, unsigned int mss_now, int nonagle,
+@@ -2666,10 +2702,39 @@ static bool tcp_write_xmit(struct sock *sk, unsigned int mss_now, int nonagle,
  							  cwnd_quota,
  							  max_segs),
  						    nonagle);
 -
 +#ifdef CONFIG_SECURITY_TEMPESTA
-+		if (sk->sk_write_xmit && tempesta_tls_skb_type(skb)) {
-+			if (unlikely(limit <= TLS_MAX_OVERHEAD)) {
++/*
++ * Maximum number of bytes, that the hpack dynamic
++ * table size can occupy.
++ */
++#define HPACK_TBL_BYTES_OCCUPIED_MAX 3
++		if (sk->sk_write_xmit
++		    && (tempesta_skb_get_cb_val(skb, TEMPESTA_TLS_SKB_TYPE_OFF,
++						TEMPESTA_TLS_SKB_TYPE_MAX)
++			& TEMPESTA_TLS_SKB_TYPE_MAX)) {
++			if (unlikely(limit <= TLS_MAX_OVERHEAD +
++				     HPACK_TBL_BYTES_OCCUPIED_MAX)) {
 +				net_warn_ratelimited("%s: too small MSS %u"
 +						     " for TLS\n",
 +						     __func__, mss_now);
@@ -2324,11 +2367,21 @@ index f99494637..12ef8eb20 100644
 +			else
 +				limit -= TLS_MAX_OVERHEAD;
 +		}
-+#endif
++		if (skb->len > limit - HPACK_TBL_BYTES_OCCUPIED_MAX &&
++		    unlikely(tso_fragment(sk, skb,
++					  limit - HPACK_TBL_BYTES_OCCUPIED_MAX,
++					  mss_now, gfp)))
++			break;
++#undef HPACK_TBL_BYTES_OCCUPIED_MAX
++#else
  		if (skb->len > limit &&
  		    unlikely(tso_fragment(sk, skb, limit, mss_now, gfp)))
  			break;
-@@ -2681,7 +2727,30 @@ static bool tcp_write_xmit(struct sock *sk, unsigned int mss_now, int nonagle,
++#endif
+ 
+ 		if (tcp_small_queue_check(sk, skb, 0))
+ 			break;
+@@ -2681,7 +2746,33 @@ static bool tcp_write_xmit(struct sock *sk, unsigned int mss_now, int nonagle,
  		 */
  		if (TCP_SKB_CB(skb)->end_seq == TCP_SKB_CB(skb)->seq)
  			break;
@@ -2346,7 +2399,10 @@ index f99494637..12ef8eb20 100644
 +		 * different TCP segments, so coalesce skbs for transmission to
 +		 * get 16KB (maximum size of TLS message).
 +		 */
-+		if (sk->sk_write_xmit && tempesta_tls_skb_type(skb)) {
++		if (sk->sk_write_xmit
++		    && (tempesta_skb_get_cb_val(skb, TEMPESTA_TLS_SKB_TYPE_OFF,
++						TEMPESTA_TLS_SKB_TYPE_MAX)
++			& TEMPESTA_TLS_SKB_TYPE_MAX)) {
 +			result = sk->sk_write_xmit(sk, skb, limit);
 +			if (unlikely(result)) {
 +				if (result == -ENOMEM)
@@ -2360,7 +2416,7 @@ index f99494637..12ef8eb20 100644
  		if (unlikely(tcp_transmit_skb(sk, skb, 1, gfp)))
  			break;
  
-@@ -2866,6 +2935,7 @@ void __tcp_push_pending_frames(struct sock *sk, unsigned int cur_mss,
+@@ -2866,6 +2957,7 @@ void __tcp_push_pending_frames(struct sock *sk, unsigned int cur_mss,
  			   sk_gfp_mask(sk, GFP_ATOMIC)))
  		tcp_check_probe_timer(sk);
  }
@@ -2368,7 +2424,7 @@ index f99494637..12ef8eb20 100644
  
  /* Send _single_ skb sitting at the send head. This function requires
   * true push pending frames to setup probe timer etc.
-@@ -3183,7 +3253,7 @@ int __tcp_retransmit_skb(struct sock *sk, struct sk_buff *skb, int segs)
+@@ -3183,7 +3275,7 @@ int __tcp_retransmit_skb(struct sock *sk, struct sk_buff *skb, int segs)
  				 cur_mss, GFP_ATOMIC))
  			return -ENOMEM; /* We'll try again later. */
  	} else {
@@ -2377,7 +2433,7 @@ index f99494637..12ef8eb20 100644
  			return -ENOMEM;
  
  		diff = tcp_skb_pcount(skb);
-@@ -3454,6 +3524,7 @@ void tcp_send_active_reset(struct sock *sk, gfp_t priority)
+@@ -3454,6 +3546,7 @@ void tcp_send_active_reset(struct sock *sk, gfp_t priority)
  	 */
  	trace_tcp_send_reset(sk, NULL);
  }
