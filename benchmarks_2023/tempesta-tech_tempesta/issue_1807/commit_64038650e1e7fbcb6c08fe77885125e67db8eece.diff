diff --git a/fw/cache.c b/fw/cache.c
index 4c92c5c58..039300c35 100644
--- a/fw/cache.c
+++ b/fw/cache.c
@@ -846,6 +846,9 @@ tfw_cache_send_304(TfwHttpReq *req, TfwCacheEntry *ce)
 
 		resp->mit.start_off = FRAME_HEADER_SIZE;
 
+		r = tfw_hpack_enc_tbl_size_expand(resp, &h_len);
+		if (unlikely(r))
+			goto err_setup;
 		r = tfw_h2_resp_status_write(resp, 304, TFW_H2_TRANS_EXPAND,
 					     true);
 		if (unlikely(r))
@@ -886,6 +889,7 @@ tfw_cache_send_304(TfwHttpReq *req, TfwCacheEntry *ce)
 	return;
 err_setup:
 	T_WARN("Can't build 304 response, key=%lx\n", ce->key);
+	tfw_hpack_enc_tbl_size_release(resp, false);
 	tfw_http_msg_free((TfwHttpMsg *)resp);
 err_create:
 	tfw_http_resp_build_error(req);
diff --git a/fw/hpack.c b/fw/hpack.c
index a639e3d0d..15c110138 100644
--- a/fw/hpack.c
+++ b/fw/hpack.c
@@ -3072,7 +3072,7 @@ tfw_hpack_rbuf_calc(TfwHPackETbl *__restrict tbl, unsigned short new_size,
 	do {
 		unsigned short f_len, fhdr_len;
 
-		if (i >= HPACK_MAX_ENC_EVICTION)
+		if (i >= HPACK_MAX_ENC_EVICTION && del_list)
 			return -E2BIG;
 
 		if (!size) {
@@ -3994,8 +3994,11 @@ tfw_hpack_encode(TfwHttpResp *__restrict resp, TfwStr *__restrict hdr,
 void
 tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 {
-	if (WARN_ON_ONCE(new_size > HPACK_ENC_TABLE_MAX_SIZE))
-		return;
+	if (new_size > HPACK_ENC_TABLE_MAX_SIZE) {
+		T_WARN("Client requests hpack table size (%hu), which is "
+			"greater than HPACK_ENC_TABLE_MAX_SIZE.", new_size);
+		new_size = HPACK_ENC_TABLE_MAX_SIZE;
+	}
 
 	spin_lock(&tbl->lock);
 
@@ -4003,14 +4006,116 @@ tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 	       " new_size=%hu\n", __func__, tbl->rb_len, tbl->size,
 	       tbl->window, new_size);
 
-	if (tbl->window > new_size) {
+	if (tbl->window == new_size)
+		goto out;
+
+	/*
+	 * RFC7541#section-4.2:
+	 * Multiple updates to the maximum table size can occur between the
+	 * transmission of two header blocks.  In the case that this size is
+	 * changed more than once in this interval, the smallest maximum table
+	 * size that occurs in that interval MUST be signaled in a dynamic table
+	 * size update.
+	 */
+	if (likely(!tbl->wnd_changed) || unlikely(!tbl->window) ||
+	    new_size < tbl->window) {
 		if (tbl->size > new_size)
 			tfw_hpack_rbuf_calc(tbl, new_size, NULL,
 					    (TfwHPackETblIter *)tbl);
 		WARN_ON_ONCE(tbl->rb_len > tbl->size);
 
 		tbl->window = new_size;
+		tbl->wnd_changed = 1;
 	}
 
+out:
+	spin_unlock(&tbl->lock);
+}
+
+#define __TFW_HPACK_ENC_TBL_SIZE_CHANGED_DECLARE_VARS(resp)		\
+	struct sk_buff __maybe_unused **skb_head = &resp->msg.skb_head;	\
+	unsigned long *flags = resp->flags;				\
+	TfwH2Ctx *ctx = tfw_h2_context(resp->req->conn);		\
+	TfwHPackETbl *tbl = &ctx->hpack.enc_tbl;			\
+	TfwHttpTransIter *mit = &resp->mit;				\
+	TfwStr new_size = {};						\
+	int r = 0;
+
+#define __TWF_HPACK_ENC_TBL_SIZE_CHECK_WR_RESULT(r)			\
+	if (likely(!r)) {						\
+		__set_bit(TFW_HTTP_B_H2_ENC_TBL_SIZE_CONTAINED, flags);	\
+		resp->window = tbl->window;				\
+		*bytes_written = new_size.len;				\
+	}
+
+static inline void
+tfw_hpack_enc_tbl_size_write(TfwHPackETbl *__restrict tbl, TfwStr *new_size)
+{
+	TfwHPackInt tmp;
+
+	write_int(tbl->window, 0x1F, 0x20, &tmp);
+	new_size->data = tmp.buf;
+	new_size->len = tmp.sz;
+}
+
+int
+tfw_hpack_enc_tbl_size_expand(TfwHttpResp *__restrict resp,
+			      unsigned long *__restrict bytes_written)
+{
+	__TFW_HPACK_ENC_TBL_SIZE_CHANGED_DECLARE_VARS(resp);
+
+	spin_lock(&tbl->lock);
+
+	if (unlikely(tbl->wnd_changed == 1)) {
+		tfw_hpack_enc_tbl_size_write(tbl, &new_size);
+
+		r = tfw_http_msg_expand_data(&mit->iter, skb_head, &new_size,
+					     &mit->start_off);
+		__TWF_HPACK_ENC_TBL_SIZE_CHECK_WR_RESULT(r);
+	}
+
+	spin_unlock(&tbl->lock);
+
+	return r;
+}
+
+int
+tfw_hpack_enc_tbl_size_add(TfwHttpResp *__restrict resp, const char *stop,
+			   unsigned long *__restrict bytes_written)
+{
+	__TFW_HPACK_ENC_TBL_SIZE_CHANGED_DECLARE_VARS(resp);
+
+	spin_lock(&tbl->lock);
+
+	if (unlikely(tbl->wnd_changed == 1)) {
+		tfw_hpack_enc_tbl_size_write(tbl, &new_size);
+
+		r = tfw_h2_msg_rewrite_data(mit, &new_size, stop);
+		__TWF_HPACK_ENC_TBL_SIZE_CHECK_WR_RESULT(r);
+	}
+
+	spin_unlock(&tbl->lock);
+
+	return r;
+}
+
+#undef __TFW_HPACK_ENC_TBL_SIZE_CHANGED_DECLARE_VARS
+#undef __TWF_HPACK_ENC_TBL_SIZE_CHECK_WR_RESULT
+
+void
+tfw_hpack_enc_tbl_size_release(TfwHttpResp *__restrict resp, bool r)
+{
+	TfwH2Ctx *ctx = tfw_h2_context(resp->req->conn);
+	TfwHPackETbl *tbl = &ctx->hpack.enc_tbl;
+
+	spin_lock(&tbl->lock);
+
+	if (likely(!test_bit(TFW_HTTP_B_H2_ENC_TBL_SIZE_CONTAINED, resp->flags)))
+		goto out;
+
+	if (likely(r && resp->window == tbl->window))
+		 tbl->wnd_changed = 0;
+
+out:
 	spin_unlock(&tbl->lock);
 }
diff --git a/fw/hpack.h b/fw/hpack.h
index 5cb81ec69..f15e32974 100644
--- a/fw/hpack.h
+++ b/fw/hpack.h
@@ -80,6 +80,7 @@ typedef struct {
  *
  * @window	- maximum pseudo-length of the dynamic table (in bytes); this
  *		  value used as threshold to flushing old entries;
+ * @wnd_changed - flag indicates, that window was changed by settings update;
  * @rbuf	- pointer to the ring buffer;
  * @root	- pointer to the root node of binary tree;
  * @pool	- memory pool for dynamic table;
@@ -92,6 +93,7 @@ typedef struct {
 typedef struct {
 	TFW_HPACK_ETBL_COMMON;
 	unsigned short		window;
+	int			wnd_changed;
 	char			*rbuf;
 	TfwHPackNode		*root;
 	TfwPool			*pool;
@@ -279,6 +281,11 @@ int tfw_hpack_cache_decode_expand(TfwHPack *__restrict hp,
 				  unsigned char *__restrict src, unsigned long n,
 				  TfwDecodeCacheIter *__restrict cd_iter);
 void tfw_hpack_enc_release(TfwHPack *__restrict hp, unsigned long *flags);
+int tfw_hpack_enc_tbl_size_expand(TfwHttpResp *__restrict resp,
+				  unsigned long *__restrict bytes_written);
+int tfw_hpack_enc_tbl_size_add(TfwHttpResp *__restrict resp, const char *stop,
+			       unsigned long *__restrict bytes_written);
+void tfw_hpack_enc_tbl_size_release(TfwHttpResp *__restrict resp, bool r);
 
 static inline unsigned int
 tfw_hpack_int_size(unsigned long index, unsigned short max)
diff --git a/fw/http.c b/fw/http.c
index f22f2a38f..a4ca57a9d 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -582,6 +582,10 @@ tfw_h2_prep_resp(TfwHttpResp *resp, unsigned short status, TfwStr *msg,
 
 	/* Set HTTP/2 ':status' pseudo-header. */
 	mit->start_off = FRAME_HEADER_SIZE;
+	r = tfw_hpack_enc_tbl_size_expand(resp, &hdrs_len);
+	if (unlikely(r))
+		return r;
+
 	r = tfw_h2_resp_status_write(resp, status, TFW_H2_TRANS_EXPAND, false);
 	if (unlikely(r))
 		return r;
@@ -1072,11 +1076,12 @@ tfw_h2_resp_fwd(TfwHttpResp *resp)
 {
 	TfwHttpReq *req = resp->req;
 	TfwH2Ctx *ctx = tfw_h2_context(req->conn);
+	int r;
 
 	tfw_connection_get(req->conn);
 	do_access_log(resp);
 
-	if (tfw_cli_conn_send((TfwCliConn *)req->conn, (TfwMsg *)resp)) {
+	if ((r = tfw_cli_conn_send((TfwCliConn *)req->conn, (TfwMsg *)resp))) {
 		T_DBG("%s: cannot send data to client via HTTP/2\n", __func__);
 		TFW_INC_STAT_BH(serv.msgs_otherr);
 		tfw_connection_close(req->conn, true);
@@ -1088,6 +1093,7 @@ tfw_h2_resp_fwd(TfwHttpResp *resp)
 	tfw_connection_put(req->conn);
 
 	tfw_hpack_enc_release(&ctx->hpack, resp->flags);
+	tfw_hpack_enc_tbl_size_release(resp, !r);
 
 	tfw_http_resp_pair_free(req);
 }
@@ -1109,10 +1115,12 @@ tfw_h2_send_resp(TfwHttpReq *req, TfwStr *msg, int status,
 {
 	TfwH2Ctx *ctx = tfw_h2_context(req->conn);
 	TfwHttpResp *resp = tfw_http_msg_alloc_resp_light(req);
+	int r;
+
 	if (unlikely(!resp))
 		goto err;
 
-	if (tfw_h2_prep_resp(resp, status, msg, stream_id))
+	if ((r = tfw_h2_prep_resp(resp, status, msg, stream_id)))
 		goto err_setup;
 
 	/* Send resulting HTTP/2 response and release HPACK encoder index. */
@@ -1125,6 +1133,7 @@ tfw_h2_send_resp(TfwHttpReq *req, TfwStr *msg, int status,
 	      __func__, req->conn);
 
 	tfw_hpack_enc_release(&ctx->hpack, resp->flags);
+	tfw_hpack_enc_tbl_size_release(resp, !r);
 
 	tfw_http_msg_free((TfwHttpMsg *)resp);
 err:
@@ -5360,11 +5369,13 @@ tfw_h2_resp_adjust_fwd(TfwHttpResp *resp)
 {
 	int r;
 	unsigned int stream_id;
+	unsigned long bytes_written = 0;
 	bool hdrs_end = false;
 	TfwHttpReq *req = resp->req;
 	TfwH2Ctx *ctx = tfw_h2_context(req->conn);
 	TfwHttpTransIter *mit = &resp->mit;
 	TfwStr codings = {.data = *this_cpu_ptr(&g_te_buf), .len = 0};
+	TfwStr *last = TFW_STR_LAST(&resp->crlf);
 	const TfwHdrMods *h_mods = tfw_vhost_get_hdr_mods(req->location,
 							  req->vhost,
 							  TFW_VHOST_HDRMOD_RESP);
@@ -5409,6 +5420,11 @@ tfw_h2_resp_adjust_fwd(TfwHttpResp *resp)
 
 	tfw_h2_msg_transform_setup(mit, resp->msg.skb_head, true);
 
+	r = tfw_hpack_enc_tbl_size_add(resp, last->data + last->len,
+				       &bytes_written);
+	if (unlikely(r))
+		goto clean;
+
 	r = tfw_h2_resp_next_hdr(resp, h_mods);
 	if (unlikely(r))
 		goto clean;
@@ -5498,6 +5514,7 @@ tfw_h2_resp_adjust_fwd(TfwHttpResp *resp)
 				   TFW_NO_PORT, 500);
 	tfw_h2_send_err_resp(req, 500, stream_id);
 	tfw_hpack_enc_release(&ctx->hpack, resp->flags);
+	tfw_hpack_enc_tbl_size_release(resp, false);
 	TFW_INC_STAT_BH(serv.msgs_otherr);
 
 	return;
diff --git a/fw/http.h b/fw/http.h
index eb247aaff..3ae763625 100644
--- a/fw/http.h
+++ b/fw/http.h
@@ -279,6 +279,11 @@ enum {
 	TFW_HTTP_B_H2_HDRS_FULL,
 	/* Message in HTTP/2 transformation (applicable for HTTP/2 mode only). */
 	TFW_HTTP_B_H2_TRANS_ENTERED,
+	/*
+	 * Message contains new size of the header compression table
+	 * (applicable for HTTP/2 mode only).
+	 */
+	TFW_HTTP_B_H2_ENC_TBL_SIZE_CONTAINED,
 
 	/* Request flags. */
 	TFW_HTTP_FLAGS_REQ,
@@ -562,12 +567,15 @@ typedef struct {
  * HTTP Response.
  * TfwStr members must be the first for efficient scanning.
  *
- * @jrxtstamp	    - time the message has been received, in jiffies;
- * @mit		    - iterator for controlling HTTP/1.1 => HTTP/2 message
- *		      transformation process (applicable for HTTP/2 mode only).
- * @no_cache_tokens - tokens for cache-control directive e.g.
- *		      Cache-Control: no-cache="token1, token2"
- * @private_tokens  - similar to @no_cache_tokens but for private="tokens"
+ * @jrxtstamp		- time the message has been received, in jiffies;
+ * @window		- window of appropriate hpack encoder dynamic table
+ * 			  at the moment, when it was written to the response;
+ * @mit 		- iterator for controlling HTTP/1.1 => HTTP/2 message
+ * 			  transformation process (applicable for HTTP/2 mode
+ * 			  only).
+ * @no_cache_tokens	- tokens for cache-control directive e.g.
+ * 			  Cache-Control: no-cache="token1, token2"
+ * @private_tokens	- similar to @no_cache_tokens but for private="tokens"
  */
 struct tfw_http_resp_t {
 	TFW_HTTP_MSG_COMMON;
@@ -575,6 +583,7 @@ struct tfw_http_resp_t {
 	long			date;
 	long			last_modified;
 	unsigned long		jrxtstamp;
+	unsigned short		window;
 	TfwHttpTransIter	mit;
 	TfwStr			no_cache_tokens;
 	TfwStr			private_tokens;
