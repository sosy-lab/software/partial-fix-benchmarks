diff --git a/fw/cache.c b/fw/cache.c
index 6260eadbf..6f6009477 100644
--- a/fw/cache.c
+++ b/fw/cache.c
@@ -886,6 +886,7 @@ tfw_cache_send_304(TfwHttpReq *req, TfwCacheEntry *ce)
 	return;
 err_setup:
 	T_WARN("Can't build 304 response, key=%lx\n", ce->key);
+	tfw_hpack_enc_tbl_size_release(resp, false);
 	tfw_http_msg_free((TfwHttpMsg *)resp);
 err_create:
 	tfw_http_resp_build_error(req);
diff --git a/fw/hpack.c b/fw/hpack.c
index 25b19e925..db7769ca7 100644
--- a/fw/hpack.c
+++ b/fw/hpack.c
@@ -3072,7 +3072,7 @@ tfw_hpack_rbuf_calc(TfwHPackETbl *__restrict tbl, unsigned short new_size,
 	do {
 		unsigned short f_len, fhdr_len;
 
-		if (i >= HPACK_MAX_ENC_EVICTION)
+		if (i >= HPACK_MAX_ENC_EVICTION && del_list)
 			return -E2BIG;
 
 		if (unlikely(!size))
@@ -3991,8 +3991,11 @@ tfw_hpack_encode(TfwHttpResp *__restrict resp, TfwStr *__restrict hdr,
 void
 tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 {
-	if (WARN_ON_ONCE(new_size > HPACK_ENC_TABLE_MAX_SIZE))
-		return;
+	if (new_size > HPACK_ENC_TABLE_MAX_SIZE) {
+		T_WARN("Client requests hpack table size (%hu), which is "
+			"greater than HPACK_ENC_TABLE_MAX_SIZE.", new_size);
+		new_size = HPACK_ENC_TABLE_MAX_SIZE;
+	}
 
 	spin_lock(&tbl->lock);
 
@@ -4000,14 +4003,81 @@ tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 	       " new_size=%hu\n", __func__, tbl->rb_len, tbl->size,
 	       tbl->window, new_size);
 
-	if (tbl->window > new_size) {
+	if (tbl->window == new_size)
+		goto out;
+
+	/*
+	 * RFC7541#section-4.2:
+	 * Multiple updates to the maximum table size can occur between the
+	 * transmission of two header blocks.  In the case that this size is
+	 * changed more than once in this interval, the smallest maximum table
+	 * size that occurs in that interval MUST be signaled in a dynamic table
+	 * size update.
+	 */
+	if (likely(!atomic_read(&tbl->wnd_changed)) || unlikely(!tbl->window)
+		|| new_size < tbl->window) {
 		if (tbl->size > new_size)
 			tfw_hpack_rbuf_calc(tbl, new_size, NULL,
 					    (TfwHPackETblIter *)tbl);
 		WARN_ON_ONCE(tbl->rb_len > tbl->size);
 
 		tbl->window = new_size;
+		atomic_set(&tbl->wnd_changed, 1);
 	}
 
+out:
 	spin_unlock(&tbl->lock);
 }
+
+void
+tfw_hpack_enc_tbl_prepare_write_sz(TfwHttpResp *__restrict resp,
+				   TfwHPackInt *tmp)
+{
+	TfwH2Ctx *ctx = tfw_h2_context(resp->req->conn);
+	TfwHPackETbl *tbl = &ctx->hpack.enc_tbl;
+
+	if (unlikely(atomic_cmpxchg(&tbl->wnd_changed, 1, -1) == 1)) {
+		write_int(tbl->window, 0x1F, 0x20, tmp);
+		__set_bit(TFW_HTTP_B_H2_ENC_TBL_SIZE, resp->flags);
+		resp->window = tbl->window;
+	}
+}
+
+int
+tfw_hpack_enc_tbl_write_sz(TfwHttpResp *__restrict resp, TfwStr *new_size)
+{
+	TfwHttpTransIter *mit = &resp->mit;
+	TfwMsgIter *iter = &mit->iter;
+	char *data = iter->skb->data;
+	int r;
+
+	if (unlikely(test_bit(TFW_HTTP_B_H2_ENC_TBL_SIZE, resp->flags))) {
+		r = tfw_msg_iter_move(iter, (unsigned char **)&data,
+				      FRAME_HEADER_SIZE);
+
+		if (unlikely(r))
+			return r;
+
+		r = tfw_http_msg_insert(iter, &data, new_size);
+		if (unlikely(r))
+			return r;
+	}
+
+	return 0;
+}
+
+void
+tfw_hpack_enc_tbl_size_release(TfwHttpResp *__restrict resp, bool r)
+{
+	TfwH2Ctx *ctx = tfw_h2_context(resp->req->conn);
+	TfwHPackETbl *tbl = &ctx->hpack.enc_tbl;
+	int old;
+
+	if (likely(!test_bit(TFW_HTTP_B_H2_ENC_TBL_SIZE, resp->flags)))
+		return;
+
+	if (likely(resp->window == tbl->window)) {
+		old = atomic_cmpxchg(&tbl->wnd_changed, -1, r ? 0 : 1);
+		WARN_ON_ONCE(old != -1 && old != 1);
+	}
+}
diff --git a/fw/hpack.h b/fw/hpack.h
index 0990af777..c62fb0db8 100644
--- a/fw/hpack.h
+++ b/fw/hpack.h
@@ -80,6 +80,13 @@ typedef struct {
  *
  * @window	- maximum pseudo-length of the dynamic table (in bytes); this
  *		  value used as threshold to flushing old entries;
+ * @wnd_changed - flag indicates, that window was changed by settings update.
+ * 		  it can be in three states:
+ * 		  0 - window was not changed, usual case;
+ * 		  -1 - window was changed by settings update, and currently
+ * 		  the new window size is saving into response;
+ * 		  1 - window was changed by settings update, and we should
+ * 		  save new window size into first response;
  * @rbuf	- pointer to the ring buffer;
  * @root	- pointer to the root node of binary tree;
  * @pool	- memory pool for dynamic table;
@@ -92,6 +99,7 @@ typedef struct {
 typedef struct {
 	TFW_HPACK_ETBL_COMMON;
 	unsigned short		window;
+	atomic_t		wnd_changed;
 	char			*rbuf;
 	TfwHPackNode		*root;
 	TfwPool			*pool;
@@ -279,6 +287,11 @@ int tfw_hpack_cache_decode_expand(TfwHPack *__restrict hp,
 				  unsigned char *__restrict src, unsigned long n,
 				  TfwDecodeCacheIter *__restrict cd_iter);
 void tfw_hpack_enc_release(TfwHPack *__restrict hp, unsigned long *flags);
+void tfw_hpack_enc_tbl_size_release(TfwHttpResp *__restrict resp, bool r);
+void tfw_hpack_enc_tbl_prepare_write_sz(TfwHttpResp *__restrict resp,
+					TfwHPackInt *tmp);
+int tfw_hpack_enc_tbl_write_sz(TfwHttpResp *__restrict resp,
+			       TfwStr *new_size);
 
 static inline unsigned int
 tfw_hpack_int_size(unsigned long index, unsigned short max)
diff --git a/fw/http.c b/fw/http.c
index 7e9af5563..03a5a3b9d 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -582,6 +582,7 @@ tfw_h2_prep_resp(TfwHttpResp *resp, unsigned short status, TfwStr *msg,
 
 	/* Set HTTP/2 ':status' pseudo-header. */
 	mit->start_off = FRAME_HEADER_SIZE;
+
 	r = tfw_h2_resp_status_write(resp, status, TFW_H2_TRANS_EXPAND, false);
 	if (unlikely(r))
 		return r;
@@ -1072,11 +1073,12 @@ tfw_h2_resp_fwd(TfwHttpResp *resp)
 {
 	TfwHttpReq *req = resp->req;
 	TfwH2Ctx *ctx = tfw_h2_context(req->conn);
+	int r;
 
 	tfw_connection_get(req->conn);
 	do_access_log(resp);
 
-	if (tfw_cli_conn_send((TfwCliConn *)req->conn, (TfwMsg *)resp)) {
+	if ((r = tfw_cli_conn_send((TfwCliConn *)req->conn, (TfwMsg *)resp))) {
 		T_DBG("%s: cannot send data to client via HTTP/2\n", __func__);
 		TFW_INC_STAT_BH(serv.msgs_otherr);
 		tfw_connection_close(req->conn, true);
@@ -1088,6 +1090,7 @@ tfw_h2_resp_fwd(TfwHttpResp *resp)
 	tfw_connection_put(req->conn);
 
 	tfw_hpack_enc_release(&ctx->hpack, resp->flags);
+	tfw_hpack_enc_tbl_size_release(resp, !r);
 
 	tfw_http_resp_pair_free(req);
 }
@@ -1109,6 +1112,7 @@ tfw_h2_send_resp(TfwHttpReq *req, TfwStr *msg, int status,
 {
 	TfwH2Ctx *ctx = tfw_h2_context(req->conn);
 	TfwHttpResp *resp = tfw_http_msg_alloc_resp_light(req);
+
 	if (unlikely(!resp))
 		goto err;
 
@@ -1125,6 +1129,7 @@ tfw_h2_send_resp(TfwHttpReq *req, TfwStr *msg, int status,
 	      __func__, req->conn);
 
 	tfw_hpack_enc_release(&ctx->hpack, resp->flags);
+	tfw_hpack_enc_tbl_size_release(resp, false);
 
 	tfw_http_msg_free((TfwHttpMsg *)resp);
 err:
@@ -4924,9 +4929,16 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 	unsigned char fr_flags = (b_len || local_body)
 			? HTTP2_F_END_HEADERS
 			: HTTP2_F_END_HEADERS | HTTP2_F_END_STREAM;
+	TfwHPackInt tmp = {};
+	TfwStr new_size = {};
 
 	T_DBG2("%s: frame response with max frame size of %lu\n",
 	       __func__, max_sz);
+	tfw_hpack_enc_tbl_prepare_write_sz(resp, &tmp);
+	new_size.data = tmp.buf;
+	new_size.len = tmp.sz;
+	h_len += tmp.sz;
+
 	/*
 	 * First frame header before HEADERS block. A data enough to store
 	 * the header is reserved at the beginning of the skb data.
@@ -4994,13 +5006,15 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		h_len -= max_sz;
 		frame_hdr.type = HTTP2_CONTINUATION;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     fr_flags);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 fr_flags);
+		if (unlikely(r))
+			return r;
 	}
 
 	if (local_response)
-		return 0;
+		goto finish;
 
 	/* Add more frame headers for DATA block. */
 	if (b_len > max_sz) {
@@ -5025,12 +5039,15 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		b_len -= max_sz;
 		frame_hdr.type = HTTP2_DATA;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     HTTP2_F_END_STREAM);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 HTTP2_F_END_STREAM);
+		if (unlikely(r))
+			return r;
 	}
 
-	return 0;
+finish:	
+	return tfw_hpack_enc_tbl_write_sz(resp, &new_size);
 }
 
 /**
@@ -5499,6 +5516,7 @@ tfw_h2_resp_adjust_fwd(TfwHttpResp *resp)
 				   TFW_NO_PORT, 500);
 	tfw_h2_send_err_resp(req, 500, stream_id);
 	tfw_hpack_enc_release(&ctx->hpack, resp->flags);
+	tfw_hpack_enc_tbl_size_release(resp, false);
 	TFW_INC_STAT_BH(serv.msgs_otherr);
 
 	return;
diff --git a/fw/http.h b/fw/http.h
index eb247aaff..2d87dd2ec 100644
--- a/fw/http.h
+++ b/fw/http.h
@@ -279,6 +279,11 @@ enum {
 	TFW_HTTP_B_H2_HDRS_FULL,
 	/* Message in HTTP/2 transformation (applicable for HTTP/2 mode only). */
 	TFW_HTTP_B_H2_TRANS_ENTERED,
+	/*
+	 * Message contains new size of the hpack encoder dynamic table
+	 * (applicable for HTTP/2 mode only).
+	 */
+	TFW_HTTP_B_H2_ENC_TBL_SIZE,
 
 	/* Request flags. */
 	TFW_HTTP_FLAGS_REQ,
@@ -352,6 +357,8 @@ typedef struct {
  * @stream		- stream which the message is linked with;
  * @cache_ctl		- cache control data for a message;
  * @version		- HTTP version (1.0 and 1.1 are only supported);
+ * @window		- window of appropriate hpack encoder dynamic table
+ * 			  at the moment, when it was written to the response;
  * @flags		- message related flags. The flags are tested
  *			  concurrently, but concurrent updates aren't
  *			  allowed. Use atomic operations if concurrent
@@ -379,6 +386,7 @@ typedef struct {
 	TfwHttpError	httperr;					\
 	TfwCacheControl	cache_ctl;					\
 	unsigned char	version;					\
+	unsigned short	window;						\
 	unsigned int	keep_alive;					\
 	unsigned long	content_length;					\
 	DECLARE_BITMAP	(flags, _TFW_HTTP_FLAGS_NUM);			\
