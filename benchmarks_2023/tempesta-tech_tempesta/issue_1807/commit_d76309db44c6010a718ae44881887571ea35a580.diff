diff --git a/fw/hpack.c b/fw/hpack.c
index 25b19e925..8fcc332fb 100644
--- a/fw/hpack.c
+++ b/fw/hpack.c
@@ -3072,7 +3072,7 @@ tfw_hpack_rbuf_calc(TfwHPackETbl *__restrict tbl, unsigned short new_size,
 	do {
 		unsigned short f_len, fhdr_len;
 
-		if (i >= HPACK_MAX_ENC_EVICTION)
+		if (i >= HPACK_MAX_ENC_EVICTION && del_list)
 			return -E2BIG;
 
 		if (unlikely(!size))
@@ -3991,8 +3991,11 @@ tfw_hpack_encode(TfwHttpResp *__restrict resp, TfwStr *__restrict hdr,
 void
 tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 {
-	if (WARN_ON_ONCE(new_size > HPACK_ENC_TABLE_MAX_SIZE))
-		return;
+	if (new_size > HPACK_ENC_TABLE_MAX_SIZE) {
+		T_WARN("Client requests hpack table size (%hu), which is "
+			"greater than HPACK_ENC_TABLE_MAX_SIZE.", new_size);
+		new_size = HPACK_ENC_TABLE_MAX_SIZE;
+	}
 
 	spin_lock(&tbl->lock);
 
@@ -4000,14 +4003,73 @@ tfw_hpack_set_rbuf_size(TfwHPackETbl *__restrict tbl, unsigned short new_size)
 	       " new_size=%hu\n", __func__, tbl->rb_len, tbl->size,
 	       tbl->window, new_size);
 
-	if (tbl->window > new_size) {
+	/*
+	 * RFC7541#section-4.2:
+	 * Multiple updates to the maximum table size can occur between the
+	 * transmission of two header blocks. In the case that this size is
+	 * changed more than once in this interval, the smallest maximum table
+	 * size that occurs in that interval MUST be signaled in a dynamic
+	 * table size update.
+	 */
+	if (tbl->window != new_size && (likely(!atomic_read(&tbl->wnd_changed))
+		|| unlikely(!tbl->window) || new_size < tbl->window)) {
 		if (tbl->size > new_size)
 			tfw_hpack_rbuf_calc(tbl, new_size, NULL,
 					    (TfwHPackETblIter *)tbl);
 		WARN_ON_ONCE(tbl->rb_len > tbl->size);
 
 		tbl->window = new_size;
+		atomic_set(&tbl->wnd_changed, 1);
 	}
 
 	spin_unlock(&tbl->lock);
 }
+
+int
+tfw_hpack_enc_tbl_write_sz(TfwHPackETbl *__restrict tbl, struct sk_buff *skb,
+			   unsigned int *bytes_wtitten)
+{
+	TfwStr new_size = {};
+	TfwHPackInt tmp = {};
+	TfwFrameHdr hdr = {};
+	int old, r = 0;
+
+	tfw_h2_unpack_frame_header(&hdr, skb->data);
+	if (hdr.type != HTTP2_HEADERS)
+		return 0;
+
+	old = atomic_cmpxchg(&tbl->wnd_changed, 1, -1);
+	if (unlikely(old == 1)) {
+		TfwMsgIter it = { .frag = -1, .skb = skb, .skb_head = skb};
+		char *data = skb->data;
+
+		write_int(tbl->window, 0x1F, 0x20, &tmp);
+		new_size.data = tmp.buf;
+		new_size.len = tmp.sz;
+	
+		r = tfw_msg_iter_move(&it, (unsigned char **)&data,
+				      FRAME_HEADER_SIZE);
+		if (unlikely(r))
+			goto finish;
+
+		r = tfw_http_msg_insert(&it, &data, &new_size);
+		if (unlikely(r))
+			goto finish;
+
+		*bytes_wtitten = tmp.sz;
+		hdr.length += tmp.sz;
+		tfw_h2_pack_frame_header(skb->data, &hdr);
+	}
+
+finish:
+	if (unlikely(r))
+		atomic_cmpxchg(&tbl->wnd_changed, -1, old);
+	return r;	
+}
+
+void
+tfw_hpack_enc_tbl_write_sz_release(TfwHPackETbl *__restrict tbl, int r)
+{
+	int old = atomic_cmpxchg(&tbl->wnd_changed, -1, r == 0 ? 0 : 1);
+	WARN_ON_ONCE(!old);
+}
diff --git a/fw/hpack.h b/fw/hpack.h
index 0990af777..b3da1fdab 100644
--- a/fw/hpack.h
+++ b/fw/hpack.h
@@ -80,6 +80,9 @@ typedef struct {
  *
  * @window	- maximum pseudo-length of the dynamic table (in bytes); this
  *		  value used as threshold to flushing old entries;
+ * 		  save new window size into first response;
+ * @wnd_changed	- flag indicates, that window was changed by settings update,
+ * 		- can be in three states;
  * @rbuf	- pointer to the ring buffer;
  * @root	- pointer to the root node of binary tree;
  * @pool	- memory pool for dynamic table;
@@ -92,6 +95,7 @@ typedef struct {
 typedef struct {
 	TFW_HPACK_ETBL_COMMON;
 	unsigned short		window;
+	atomic_t		wnd_changed;
 	char			*rbuf;
 	TfwHPackNode		*root;
 	TfwPool			*pool;
@@ -279,6 +283,9 @@ int tfw_hpack_cache_decode_expand(TfwHPack *__restrict hp,
 				  unsigned char *__restrict src, unsigned long n,
 				  TfwDecodeCacheIter *__restrict cd_iter);
 void tfw_hpack_enc_release(TfwHPack *__restrict hp, unsigned long *flags);
+int tfw_hpack_enc_tbl_write_sz(TfwHPackETbl *__restrict tbl, struct sk_buff * __restrict skb,
+			       unsigned int *bytes_wtitten);
+void tfw_hpack_enc_tbl_write_sz_release(TfwHPackETbl *__restrict tbl, int r);
 
 static inline unsigned int
 tfw_hpack_int_size(unsigned long index, unsigned short max)
diff --git a/fw/http.c b/fw/http.c
index 7e9af5563..67a32d3e0 100644
--- a/fw/http.c
+++ b/fw/http.c
@@ -4927,6 +4927,7 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 	T_DBG2("%s: frame response with max frame size of %lu\n",
 	       __func__, max_sz);
+
 	/*
 	 * First frame header before HEADERS block. A data enough to store
 	 * the header is reserved at the beginning of the skb data.
@@ -4994,9 +4995,11 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		h_len -= max_sz;
 		frame_hdr.type = HTTP2_CONTINUATION;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     fr_flags);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 fr_flags);
+		if (unlikely(r))
+			return r;
 	}
 
 	if (local_response)
@@ -5025,9 +5028,11 @@ tfw_h2_make_frames(TfwHttpResp *resp, unsigned int stream_id,
 
 		b_len -= max_sz;
 		frame_hdr.type = HTTP2_DATA;
-		__tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
-				     max_sz, &frame_hdr_str, &frame_hdr,
-				     HTTP2_F_END_STREAM);
+		r = __tfw_h2_make_frames(b_len, data, buf, sizeof(buf), iter, skew,
+					 max_sz, &frame_hdr_str, &frame_hdr,
+					 HTTP2_F_END_STREAM);
+		if (unlikely(r))
+			return r;
 	}
 
 	return 0;
diff --git a/fw/http.h b/fw/http.h
index eb247aaff..fb762db58 100644
--- a/fw/http.h
+++ b/fw/http.h
@@ -279,6 +279,11 @@ enum {
 	TFW_HTTP_B_H2_HDRS_FULL,
 	/* Message in HTTP/2 transformation (applicable for HTTP/2 mode only). */
 	TFW_HTTP_B_H2_TRANS_ENTERED,
+	/*
+	 * Message contains new size of the hpack encoder dynamic table
+	 * (applicable for HTTP/2 mode only).
+	 */
+	TFW_HTTP_B_H2_ENC_TBL_SIZE,
 
 	/* Request flags. */
 	TFW_HTTP_FLAGS_REQ,
diff --git a/fw/http_frame.c b/fw/http_frame.c
index afcc796c4..3cca192b9 100644
--- a/fw/http_frame.c
+++ b/fw/http_frame.c
@@ -251,18 +251,6 @@ tfw_h2_context_clear(TfwH2Ctx *ctx)
 	tfw_hpack_clean(&ctx->hpack);
 }
 
-static inline void
-tfw_h2_unpack_frame_header(TfwFrameHdr *hdr, const unsigned char *buf)
-{
-	hdr->length = ntohl(*(int *)buf) >> 8;
-	hdr->type = buf[3];
-	hdr->flags = buf[4];
-	hdr->stream_id = ntohl(*(unsigned int *)&buf[5]) & FRAME_STREAM_ID_MASK;
-
-	T_DBG3("%s: parsed, length=%d, stream_id=%u, type=%hhu, flags=0x%hhx\n",
-	       __func__, hdr->length, hdr->stream_id, hdr->type, hdr->flags);
-}
-
 static inline void
 tfw_h2_unpack_priority(TfwFramePri *pri, const unsigned char *buf)
 {
diff --git a/fw/http_frame.h b/fw/http_frame.h
index 2e844ad9c..4f87b7e4c 100644
--- a/fw/http_frame.h
+++ b/fw/http_frame.h
@@ -251,5 +251,16 @@ tfw_h2_pack_frame_header(unsigned char *p, const TfwFrameHdr *hdr)
 	*(unsigned int *)p = htonl(hdr->stream_id);
 }
 
+static inline void
+tfw_h2_unpack_frame_header(TfwFrameHdr *hdr, const unsigned char *buf)
+{
+        hdr->length = ntohl(*(int *)buf) >> 8;
+        hdr->type = buf[3];
+        hdr->flags = buf[4];
+        hdr->stream_id = ntohl(*(unsigned int *)&buf[5]) & FRAME_STREAM_ID_MASK;
+
+        T_DBG3("%s: parsed, length=%d, stream_id=%u, type=%hhu, flags=0x%hhx\n",
+               __func__, hdr->length, hdr->stream_id, hdr->type, hdr->flags);
+}
 
 #endif /* __HTTP_FRAME__ */
diff --git a/fw/tls.c b/fw/tls.c
index a1ed31569..f2ae87d78 100644
--- a/fw/tls.c
+++ b/fw/tls.c
@@ -256,21 +256,19 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 #define AUTO_SEGS_N	8
 
 	int r = -ENOMEM;
-	unsigned int head_sz, len, frags, t_sz, out_frags;
+	unsigned int head_sz, len, frags, t_sz, out_frags, bytes_written = 0;
 	unsigned char type;
 	struct sk_buff *next = skb, *skb_tail = skb;
-	struct tcp_skb_cb *tcb = TCP_SKB_CB(skb);
+	struct tcp_skb_cb *tcb;
 	TlsCtx *tls;
+	TfwH2Ctx *h2;
+	TfwHPackETbl *tbl;
 	TlsIOCtx *io;
 	TlsXfrm *xfrm;
-	struct sg_table sgt = {
-		.nents = skb_shinfo(skb)->nr_frags + !!skb_headlen(skb),
-	}, out_sgt = {
-		.nents = skb_shinfo(skb)->nr_frags + !!skb_headlen(skb),
-	};
 	struct scatterlist sg[AUTO_SEGS_N], out_sg[AUTO_SEGS_N];
 	struct page **pages = NULL, **pages_end, **p;
 	struct page *auto_pages[AUTO_SEGS_N];
+	struct sg_table sgt = {}, out_sgt = {};
 
 	assert_spin_locked(&sk->sk_lock.slock);
 
@@ -285,8 +283,24 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	}
 
 	tls = tfw_tls_context(sk->sk_user_data);
+	h2 = tfw_h2_context(sk->sk_user_data);
 	io = &tls->io_out;
 	xfrm = &tls->xfrm;
+	tbl = &h2->hpack.enc_tbl;
+
+	r = tfw_hpack_enc_tbl_write_sz(tbl, skb, &bytes_written);
+	if (r) {
+		T_WARN("%s: failed to encode new hpack dynamic table size (%d)",
+		       __func__, r);
+		goto err_kill_sock;
+	}
+
+	tcb = TCP_SKB_CB(skb);
+	sgt.nents = skb_shinfo(skb)->nr_frags + !!skb_headlen(skb);
+	out_sgt.nents = skb_shinfo(skb)->nr_frags + !!skb_headlen(skb);
+
+	tcp_sk(sk)->write_seq += bytes_written;
+	TCP_SKB_CB(skb)->end_seq += bytes_written;
 
 	T_DBG3("%s: sk=%pK(snd_una=%u snd_nxt=%u limit=%u)"
 	       " skb=%pK(len=%u data_len=%u type=%u frags=%u headlen=%u"
@@ -418,8 +432,10 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	 * So to adjust the socket write memory we have to check the both skbs
 	 * and only for TTLS_TAG_LEN.
 	 */
-	if (tfw_tls_tcp_add_overhead(sk, t_sz))
-		return -ENOMEM;
+	if (tfw_tls_tcp_add_overhead(sk, t_sz + bytes_written)) {
+		r = -ENOMEM;
+		goto ret;
+	}
 
 	if (likely(sgt.nents <= AUTO_SEGS_N)) {
 		sgt.sgl = sg;
@@ -433,7 +449,8 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 		sgt.sgl = (struct scatterlist *)ptr;
 		if (!sgt.sgl) {
 			T_WARN("cannot alloc memory for TLS encryption.\n");
-			return -ENOMEM;
+			r = -ENOMEM;
+			goto ret;
 		}
 
 		ptr += sizeof(struct scatterlist) * sgt.nents;
@@ -518,8 +535,12 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 out:
 	if (unlikely(sgt.nents > AUTO_SEGS_N))
 		kfree(sgt.sgl);
-	if (!r || r == -ENOMEM)
+ret:
+	if (!r || r == -ENOMEM) {
+		if (unlikely(bytes_written))
+			tfw_hpack_enc_tbl_write_sz_release(tbl, r);
 		return r;
+	}
 
 	/*
 	 * We can not send unencrypted data and can not normally close the
@@ -536,6 +557,8 @@ tfw_tls_encrypt(struct sock *sk, struct sk_buff *skb, unsigned int limit)
 	 */
 	tcp_write_queue_purge(sk);
 err_epilogue:
+	if (unlikely(bytes_written))
+		tfw_hpack_enc_tbl_write_sz_release(tbl, r);
 	T_WARN("%s: cannot encrypt data (%d), only partial data was sent\n",
 	       __func__, r);
 	return r;
