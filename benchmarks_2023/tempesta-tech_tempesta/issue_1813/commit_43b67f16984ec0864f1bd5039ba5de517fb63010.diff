diff --git a/fw/cache.c b/fw/cache.c
index 846f569ec..e88dcb415 100644
--- a/fw/cache.c
+++ b/fw/cache.c
@@ -1261,14 +1261,41 @@ tfw_cache_h2_copy_int(unsigned int *acc_len, unsigned long src,
 	return 0;
 }
 
+static int
+tfw_cache_copy_str_with_extra_quotes(TfwCacheEntry *ce, char **p, TdbVRec **trec,
+				     TfwStr *src, size_t *tot_len,
+				     bool need_extra_quotes)
+{
+#define ADD_ETAG_QUOTE(flag)                                            \
+do {                                                                    \
+        TfwStr quote = { .data = "\"", .len = 1, .flags = flag };       \
+        if (tfw_cache_h2_copy_str(&ce->hdr_len, p, trec, &quote,        \
+                                  tot_len))                             \
+                return -ENOMEM;                                         \
+} while(0)
+
+	if (need_extra_quotes)
+		ADD_ETAG_QUOTE(0);
+
+	if (tfw_cache_h2_copy_str(&ce->hdr_len, p, trec, src, tot_len))
+		return -ENOMEM;
+
+	if (need_extra_quotes)
+		ADD_ETAG_QUOTE(TFW_STR_VALUE);
+
+	return 0;
+
+#undef ADD_ETAG_QUOTE
+}
+
 /**
  * Deep HTTP header copy to TdbRec.
  * @hdr is copied in depth first fashion to speed up upcoming scans.
  * @return number of copied bytes on success and negative value otherwise.
  */
 static long
-tfw_cache_h2_copy_hdr(TfwCacheEntry *ce, char **p, TdbVRec **trec, TfwStr *hdr,
-		      size_t *tot_len)
+tfw_cache_h2_copy_hdr(TfwCacheEntry *ce, TfwHttpResp *resp, int hid, char **p,
+		      TdbVRec **trec, TfwStr *hdr, size_t *tot_len)
 {
 	TfwCStr *cs;
 	long n = sizeof(TfwCStr);
@@ -1276,6 +1303,10 @@ tfw_cache_h2_copy_hdr(TfwCacheEntry *ce, char **p, TdbVRec **trec, TfwStr *hdr,
 	bool dupl = TFW_STR_DUP(hdr);
 	unsigned int init_len = ce->hdr_len;
 	TfwStr s_nm, s_val, *dup, *dup_end;
+	unsigned long s_val_len;
+	const bool need_extra_quotes =
+		test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags)
+		&& (hid == TFW_HTTP_HDR_ETAG);
 
 	T_DBG3("%s: ce=[%p] p=[%p], trec=[%p], tot_len='%zu'\n", __func__, ce,
 	       *p, *trec, *tot_len);
@@ -1288,7 +1319,8 @@ tfw_cache_h2_copy_hdr(TfwCacheEntry *ce, char **p, TdbVRec **trec, TfwStr *hdr,
 		tfw_http_hdr_split(hdr, &s_nm, &s_val, true);
 
 		st_index = hdr->hpack_idx;
-		h_len = tfw_h2_hdr_size(s_nm.len, s_val.len, st_index);
+		h_len = tfw_h2_hdr_size(s_nm.len, s_val.len, st_index) +
+			2 * need_extra_quotes;
 
 		/* Don't split short strings. */
 		if (sizeof(TfwCStr) + h_len <= L1_CACHE_BYTES)
@@ -1333,10 +1365,12 @@ tfw_cache_h2_copy_hdr(TfwCacheEntry *ce, char **p, TdbVRec **trec, TfwStr *hdr,
 				return -ENOMEM;
 		}
 
-		if (tfw_cache_h2_copy_int(&ce->hdr_len, s_val.len, 0x7f, p,
+		s_val_len =  s_val.len + 2 * need_extra_quotes;
+		if (tfw_cache_h2_copy_int(&ce->hdr_len, s_val_len, 0x7f, p,
 					  trec, tot_len)
-		    || tfw_cache_h2_copy_str(&ce->hdr_len, p, trec, &s_val,
-					     tot_len))
+		    || tfw_cache_copy_str_with_extra_quotes(ce, p, trec,
+							    &s_val, tot_len,
+							    need_extra_quotes))
 			return -ENOMEM;
 
 		CSTR_WRITE_HDR(0, ce->hdr_len - prev_len);
@@ -1501,6 +1535,13 @@ __set_etag(TfwCacheEntry *ce, TfwHttpResp *resp, long h_off, TdbVRec *h_trec,
 	tfw_http_hdr_split(h, &s_dummy, &s_val, true);
 	c_size = 2 + tfw_hpack_int_size(s_val.len, 0x7F);
 	CHECK_REC_SPACE();
+
+	if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags)) {
+		len += 1;
+		c_size = 1;
+		CHECK_REC_SPACE();
+	}
+
 	TFW_STR_FOR_EACH_CHUNK(c, &h_val, end) {
 		if (c->flags & TFW_STR_VALUE) {
 			flags = c->flags;
@@ -1705,6 +1746,7 @@ tfw_cache_copy_resp(TfwCacheEntry *ce, TfwHttpResp *resp, TfwStr *rph,
 	ce->hdr_num = resp->h_tbl->off;
 	FOR_EACH_HDR_FIELD_FROM(field, end1, resp, TFW_HTTP_HDR_REGULAR) {
 		int hid = field - resp->h_tbl->tbl;
+
 		/*
 		 * Skip hop-by-hop headers. Also skip 'Server' header (with
 		 * possible duplicates), since we will substitute it with our
@@ -1730,7 +1772,8 @@ tfw_cache_copy_resp(TfwCacheEntry *ce, TfwHttpResp *resp, TfwStr *rph,
 		}
 
 		__save_hdr_304_off(ce, resp, field, TDB_OFF(db->hdr, p));
-		n = tfw_cache_h2_copy_hdr(ce, &p, &trec, field, &tot_len);
+		n = tfw_cache_h2_copy_hdr(ce, resp, hid, &p, &trec, field, &tot_len);
+
 		if (unlikely(n < 0))
 			return n;
 	}
@@ -2072,6 +2115,9 @@ __cache_add_node(TDB *db, TfwHttpResp *resp, unsigned long key)
 	TfwStr rph, *s_line;
 	long data_len = __cache_entry_size(resp);
 
+	if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags))
+		data_len += 2;
+
 	if (unlikely(data_len < 0))
 		return;
 
diff --git a/fw/http.h b/fw/http.h
index eb247aaff..9442474e6 100644
--- a/fw/http.h
+++ b/fw/http.h
@@ -313,6 +313,11 @@ enum {
 	TFW_HTTP_B_HDR_LMODIFIED,
 	/* Response is fully processed and ready to be forwarded to the client. */
 	TFW_HTTP_B_RESP_READY,
+	/*
+	 * Response has header 'Etag: ' and this header is
+	 * not enclosed in double quotes.
+	 */
+	TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
 
 	_TFW_HTTP_FLAGS_NUM
 };
diff --git a/fw/http_msg.c b/fw/http_msg.c
index b0ebfdc4d..74032ac65 100644
--- a/fw/http_msg.c
+++ b/fw/http_msg.c
@@ -256,11 +256,14 @@ __http_msg_hdr_val(TfwStr *hdr, unsigned id, TfwStr *val, bool client)
 
 	nlen = hdr_lens[client][id];
 	/*
-	 * Only Host header is allowed to be empty.
+	 * Only Host header is allowed to be empty but because
+	 * we don't follow RFC and allow Etag header to be not
+	 * enclosed in double quotes it also can be empty.
 	 * If header string is plain, it is always empty header.
 	 * Not empty headers are compound strings.
 	 */
-	BUG_ON(id == TFW_HTTP_HDR_HOST ? nlen > hdr->len : nlen >= hdr->len);
+	BUG_ON(id == TFW_HTTP_HDR_HOST
+	       || id == TFW_HTTP_HDR_ETAG ? nlen > hdr->len : nlen >= hdr->len);
 
 	*val = *hdr;
 
diff --git a/fw/http_parser.c b/fw/http_parser.c
index f1131d376..7ad6d5ef6 100644
--- a/fw/http_parser.c
+++ b/fw/http_parser.c
@@ -2863,7 +2863,7 @@ __FSM_STATE(st) {							\
 static int
 __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 {
-	int r = CSTR_NEQ;
+	int no_quotes, r = CSTR_NEQ;
 	bool if_nmatch = TFW_CONN_TYPE(hm->conn) & Conn_Clnt;
 	__FSM_DECLARE_VARS(hm);
 
@@ -2917,6 +2917,20 @@ __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 
 		if (IS_WS(c))
 			__FSM_I_MOVE_fixup(I_Etag, 1, 0);
+
+		/*
+		 * According to RFC 9110 8.8.3:
+		 * An entity-tag consists of an opaque quoted string, possibly
+		 * prefixed by a weakness indicator.
+		 * But unfortunately many do not follow RFC and send Etag
+		 * without double quotes (wordpress for example), so we
+		 * should process such Etag here.
+		 */
+		if (!if_nmatch) {
+			__set_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, msg->flags);
+			parser->_i_st = &&I_Etag_Val;
+			goto I_Etag_Val;
+		}
 		return CSTR_NEQ;
 	}
 
@@ -2932,18 +2946,35 @@ __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 	 * in separate TfwStr chunk.
 	 */
 	__FSM_STATE(I_Etag_Val) {
+		no_quotes = test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
+				     msg->flags);
 		__FSM_I_MATCH_MOVE_fixup(etag, I_Etag_Val, TFW_STR_VALUE);
 		c = *(p + __fsm_sz);
-		if (likely(c == '"')) {
+		/*
+		 * Since we process Etags which are not enclosed in double
+		 * quotes, we check that there is quote at the end of Etag
+		 * only in case if it is in it's begin.
+		 */
+		if (likely(c == '"' && !no_quotes)) {
 			__FSM_I_MOVE_fixup(I_EoT, __fsm_sz + 1, TFW_STR_VALUE);
 		}
+		if (unlikely(IS_CRLFWS(c)) && no_quotes) {
+			__FSM_I_MOVE_fixup(I_EoT, __fsm_sz, __fsm_sz ?
+					   TFW_STR_VALUE : 0);
+		}
 		return CSTR_NEQ;
 	}
 
 	/* End of ETag */
 	__FSM_STATE(I_EoT) {
-		if (IS_WS(c))
-			__FSM_I_MOVE_fixup(I_EoT, 1, TFW_STR_OWS);
+		if (IS_WS(c)) {
+			no_quotes = test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
+					     msg->flags);
+			if (!no_quotes)
+				__FSM_I_MOVE_fixup(I_EoT, 1, TFW_STR_OWS);
+			else
+				__FSM_MOVE_nofixup(I_EoT);
+		}
 		if (IS_CRLF(c))
 			return __data_off(p);
 		if (if_nmatch && c == ',')
diff --git a/fw/t/unit/test_http1_parser.c b/fw/t/unit/test_http1_parser.c
index dbf4f5ce4..43922707d 100644
--- a/fw/t/unit/test_http1_parser.c
+++ b/fw/t/unit/test_http1_parser.c
@@ -2836,14 +2836,17 @@ TEST(http1_parser, etag)
 	FOR_ETAG("ETag: W/\"" ETAG_ALPHABET "\"",  ETAG_ALPHABET "\"");
 
 	/* Same code is used to parse ETag header and If-None-Match header. */
+	ETAG_BLOCK("Etag: \"dum my\"");
+	ETAG_BLOCK("Etag: \"dummy \"");
+	ETAG_BLOCK("Etag:  *\"");
 	ETAG_BLOCK("ETag: \"dummy1\", \"dummy2\"");
-	ETAG_BLOCK("ETag: *\r\n");
 	COMMON_ETAG_BLOCK("ETag: ", ETAG_BLOCK);
 	ETAG_BLOCK("ETag: \"dummy\"\r\n"
 		       ": \"dummy\"");
 
 #undef ETAG_BLOCK
 #undef FOR_ETAG
+#undef TFW_HTTP_MSG_SRVETAG_VAL
 #undef RESP_ETAG_END
 #undef RESP_ETAG_START
 }
@@ -2943,7 +2946,7 @@ TEST(http1_parser, if_none_match)
 	EXPECT_BLOCK_REQ_SIMPLE("If-None-Match: \"" ETAG_2 "\", * ");
 	EXPECT_BLOCK_REQ_SIMPLE("If-None-Match: *, \"" ETAG_2 "\" ");
 
-	COMMON_ETAG_BLOCK("If-None-Match: ", EXPECT_BLOCK_REQ_SIMPLE);
+	COMMON_IF_NON_MATCH_BLOCK("If-None-Match: ", EXPECT_BLOCK_REQ_SIMPLE);
 	EXPECT_BLOCK_REQ_SIMPLE("If-None-Match: \"dummy\"\r\n"
 					     ": \"dummy\"");
 
diff --git a/fw/t/unit/test_http_parser_common.h b/fw/t/unit/test_http_parser_common.h
index 5a2ba904e..951385eec 100644
--- a/fw/t/unit/test_http_parser_common.h
+++ b/fw/t/unit/test_http_parser_common.h
@@ -89,10 +89,9 @@ enum {
 	BLOCK_MACRO(head "4294967295" tail)
 
 /* For ETag and If-None-Match headers */
-#define COMMON_ETAG_BLOCK(head, BLOCK_MACRO)			\
+#define __COMMON_ETAG_IF_NONE_MATCH_BLOCK(head, BLOCK_MACRO)	\
 	BLOCK_MACRO(head "\"dummy");				\
 	BLOCK_MACRO(head "dummy\"");				\
-	BLOCK_MACRO(head "'dummy'");				\
 	BLOCK_MACRO(head "W/ \"dummy\"");			\
 	BLOCK_MACRO(head "w/\"dummy\"");			\
 	BLOCK_MACRO(head "\"\x00\"");				\
@@ -101,6 +100,13 @@ enum {
 	BLOCK_MACRO(head "\" \"");				\
 	BLOCK_MACRO(head "\"\"\"")
 
+#define COMMON_ETAG_BLOCK(head, BLOCK_MACRO)			\
+	__COMMON_ETAG_IF_NONE_MATCH_BLOCK(head, BLOCK_MACRO)
+
+#define COMMON_IF_NON_MATCH_BLOCK(head, BLOCK_MACRO)		\
+	BLOCK_MACRO(head "'dummy'");				\
+	__COMMON_ETAG_IF_NONE_MATCH_BLOCK(head, BLOCK_MACRO)
+
 /**
  * Reference to a frame of bytes containing the request data to be parsed.
  * Such a frame corresponds to one HTTP request.
