diff --git a/fw/cache.c b/fw/cache.c
index 4c92c5c58..3a98fd260 100644
--- a/fw/cache.c
+++ b/fw/cache.c
@@ -2063,17 +2063,69 @@ __cache_entry_size(TfwHttpResp *resp)
 #undef STATIC_INDEX_SZ
 }
 
+static inline int
+__add_etag_temporary_quote(TfwHttpResp *resp, TfwStr *etag)
+{
+	TfwStr src = { .data = "\"", .len = 1, .flags = TFW_STR_VALUE };
+	TfwStr *c;
+
+	c  = tfw_str_add_compound(resp->pool, etag);
+	if (unlikely(!c))
+		return -ENOMEM;
+	*c = src;
+	etag->len += src.len;
+	return 0;
+}
+
+static inline int
+__add_etag_temporary_quotes(TfwHttpResp *resp)
+{
+	TfwStr h_val, *h = &resp->h_tbl->tbl[TFW_HTTP_HDR_ETAG];
+	TfwStr src = { .data = "\"", .len = 1 };
+	int r;
+
+	h->flags &= ~TFW_STR_COMPLETE;
+	tfw_http_msg_srvhdr_val(h, TFW_HTTP_HDR_ETAG, &h_val);
+	r = tfw_str_insert(resp->pool, h, &src, h->nchunks - h_val.nchunks);
+	if (unlikely(r))
+		goto finish;
+	r = __add_etag_temporary_quote(resp, h);
+	if (unlikely(r))
+		tfw_str_del_chunk(h, h->nchunks - h_val.nchunks);
+
+finish:
+	h->flags |= TFW_STR_COMPLETE;
+	return r;
+}
+
+static inline void
+__del_etag_temporary_quotes(TfwHttpResp *resp)
+{
+	TfwStr h_val, *h = &resp->h_tbl->tbl[TFW_HTTP_HDR_ETAG];
+
+	h->flags &= ~TFW_STR_COMPLETE;
+	tfw_http_msg_srvhdr_val(h, TFW_HTTP_HDR_ETAG, &h_val);
+	tfw_str_del_chunk(h, h->nchunks - h_val.nchunks);
+	tfw_str_del_chunk(h, h->nchunks - 1);
+	h->flags |= TFW_STR_COMPLETE;
+}
+
 static void
 __cache_add_node(TDB *db, TfwHttpResp *resp, unsigned long key)
 {
 	size_t len;
 	TfwCacheEntry *ce;
 	TfwStr rph, *s_line;
-	long data_len = __cache_entry_size(resp);
+	long data_len;
 
-	if (unlikely(data_len < 0))
+	if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags) &&
+	    unlikely(__add_etag_temporary_quotes(resp)))
 		return;
 
+
+	data_len = __cache_entry_size(resp);
+	if (unlikely(data_len < 0))
+		goto finish;
 	/*
 	 * We need to save the reason-phrase for the case of HTTP/1.1-response
 	 * creation from cache. Note, that reason-phrase is always the last part
@@ -2082,7 +2134,7 @@ __cache_add_node(TDB *db, TfwHttpResp *resp, unsigned long key)
 	s_line = &resp->h_tbl->tbl[TFW_HTTP_STATUS_LINE];
 	rph = tfw_str_next_str_val(s_line);
 	if (WARN_ON_ONCE(TFW_STR_EMPTY(&rph)))
-		return;
+		goto finish;
 
 	data_len += rph.len;
 	len = data_len;
@@ -2100,13 +2152,17 @@ __cache_add_node(TDB *db, TfwHttpResp *resp, unsigned long key)
 	ce = (TfwCacheEntry *)tdb_entry_alloc(db, key, &len);
 	BUG_ON(len <= sizeof(TfwCacheEntry));
 	if (!ce)
-		return;
+		goto finish;
 
 	T_DBG3("%s: ce=[%p], alloc_len='%lu'\n", __func__, ce, len);
 
 	if (tfw_cache_copy_resp(ce, resp, &rph, data_len)) {
 		/* TODO delete the probably partially built TDB entry. */
 	}
+
+finish:
+	if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags))
+		__del_etag_temporary_quotes(resp);
 }
 
 static void
diff --git a/fw/http.h b/fw/http.h
index eb247aaff..9442474e6 100644
--- a/fw/http.h
+++ b/fw/http.h
@@ -313,6 +313,11 @@ enum {
 	TFW_HTTP_B_HDR_LMODIFIED,
 	/* Response is fully processed and ready to be forwarded to the client. */
 	TFW_HTTP_B_RESP_READY,
+	/*
+	 * Response has header 'Etag: ' and this header is
+	 * not enclosed in double quotes.
+	 */
+	TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
 
 	_TFW_HTTP_FLAGS_NUM
 };
diff --git a/fw/http_msg.c b/fw/http_msg.c
index b0ebfdc4d..8fab2483f 100644
--- a/fw/http_msg.c
+++ b/fw/http_msg.c
@@ -256,11 +256,13 @@ __http_msg_hdr_val(TfwStr *hdr, unsigned id, TfwStr *val, bool client)
 
 	nlen = hdr_lens[client][id];
 	/*
-	 * Only Host header is allowed to be empty.
+	 * Only Host header is allowed to be empty but because
+	 * we don't follow RFC and allow Etag header to be not
+	 * enlosed in double quotes it also can be empty.
 	 * If header string is plain, it is always empty header.
 	 * Not empty headers are compound strings.
 	 */
-	BUG_ON(id == TFW_HTTP_HDR_HOST ? nlen > hdr->len : nlen >= hdr->len);
+	BUG_ON(id == TFW_HTTP_HDR_HOST || id == TFW_HTTP_HDR_ETAG ? nlen > hdr->len : nlen >= hdr->len);
 
 	*val = *hdr;
 
diff --git a/fw/http_parser.c b/fw/http_parser.c
index f1131d376..eaabe08fa 100644
--- a/fw/http_parser.c
+++ b/fw/http_parser.c
@@ -2863,10 +2863,17 @@ __FSM_STATE(st) {							\
 static int
 __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 {
-	int r = CSTR_NEQ;
+	int no_quotes, r = CSTR_NEQ;
 	bool if_nmatch = TFW_CONN_TYPE(hm->conn) & Conn_Clnt;
 	__FSM_DECLARE_VARS(hm);
 
+#define ETAG_MOVE_fixup_flag(flag, to)					\
+do {									\
+	__set_bit(flag, msg->flags);					\
+	parser->_i_st = &&to;						\
+	goto to;							\
+} while (0)
+
 	/*
 	 * ETag value and closing DQUOTE is placed into separate chunks marked
 	 * with flags TFW_STR_VALUE.
@@ -2917,6 +2924,18 @@ __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 
 		if (IS_WS(c))
 			__FSM_I_MOVE_fixup(I_Etag, 1, 0);
+
+		/*
+		 * According to RFC 9110 8.8.3:
+		 * An entity-tag consists of an opaque quoted string, possibly
+		 * prefixed by a weakness indicator.
+		 * But unfortunately many do not follow RFC and send Etag
+		 * without double quotes (wordpress for example), so we
+		 * should process such Etag here.
+		 */
+		if (!if_nmatch)
+			ETAG_MOVE_fixup_flag(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
+					     I_Etag_Val);
 		return CSTR_NEQ;
 	}
 
@@ -2932,18 +2951,35 @@ __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 	 * in separate TfwStr chunk.
 	 */
 	__FSM_STATE(I_Etag_Val) {
+		no_quotes = test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
+				     msg->flags);
 		__FSM_I_MATCH_MOVE_fixup(etag, I_Etag_Val, TFW_STR_VALUE);
 		c = *(p + __fsm_sz);
-		if (likely(c == '"')) {
+		/*
+		 * Since we process Etags which are not enclosed in double
+		 * quotes, we check that there is quote at the end of Etag
+		 * only in case if it is in it's begin.
+		 */
+		if (likely(c == '"' && !no_quotes)) {
 			__FSM_I_MOVE_fixup(I_EoT, __fsm_sz + 1, TFW_STR_VALUE);
 		}
+		if (unlikely(IS_CRLFWS(c)) && no_quotes) {
+			__FSM_I_MOVE_fixup(I_EoT, __fsm_sz, __fsm_sz ?
+					   TFW_STR_VALUE : 0);
+		}
 		return CSTR_NEQ;
 	}
 
 	/* End of ETag */
 	__FSM_STATE(I_EoT) {
-		if (IS_WS(c))
-			__FSM_I_MOVE_fixup(I_EoT, 1, TFW_STR_OWS);
+		if (IS_WS(c)) {
+			no_quotes = test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,
+					     msg->flags);
+			if (!no_quotes)
+				__FSM_I_MOVE_fixup(I_EoT, 1, TFW_STR_OWS);
+			else
+				__FSM_MOVE_nofixup(I_EoT);
+		}
 		if (IS_CRLF(c))
 			return __data_off(p);
 		if (if_nmatch && c == ',')
@@ -2961,6 +2997,8 @@ __parse_etag_or_if_nmatch(TfwHttpMsg *hm, unsigned char *data, size_t len)
 
 done:
 	return r;
+
+#undef ETAG_MOVE_fixup_flag
 }
 STACK_FRAME_NON_STANDARD(__parse_etag_or_if_nmatch);
 
diff --git a/fw/t/unit/test_http1_parser.c b/fw/t/unit/test_http1_parser.c
index dbf4f5ce4..57de1fe0e 100644
--- a/fw/t/unit/test_http1_parser.c
+++ b/fw/t/unit/test_http1_parser.c
@@ -2792,6 +2792,51 @@ TEST(http1_parser, set_cookie)
 			  "0123456789");
 }
 
+static int
+__add_etag_temporary_quote(TfwHttpResp *resp, TfwStr *etag)
+{
+	TfwStr src = { .data = "\"", .len = 1, .flags = TFW_STR_VALUE };
+	TfwStr *c;
+
+	c  = tfw_str_add_compound(resp->pool, etag);
+	if (unlikely(!c))
+		return -ENOMEM;
+	*c = src;
+	etag->len += src.len;
+	return 0;
+}
+
+static int
+__add_etag_temporary_quotes(TfwHttpResp *resp)
+{
+	TfwStr h_val, *h = &resp->h_tbl->tbl[TFW_HTTP_HDR_ETAG];
+	TfwStr src = { .data = "\"", .len = 1 };
+	int r;
+
+	h->flags &= ~TFW_STR_COMPLETE;
+	tfw_http_msg_srvhdr_val(h, TFW_HTTP_HDR_ETAG, &h_val);
+	r = tfw_str_insert(resp->pool, h, &src, h->nchunks - h_val.nchunks);
+	if (unlikely(r))
+		goto finish;
+	r = __add_etag_temporary_quote(resp, h);
+
+finish:
+	h->flags |= TFW_STR_COMPLETE;
+	return r;
+}
+
+static void
+__del_etag_temporary_quotes(TfwHttpResp *resp)
+{
+	TfwStr h_val, *h = &resp->h_tbl->tbl[TFW_HTTP_HDR_ETAG];
+
+	h->flags &= ~TFW_STR_COMPLETE;
+	tfw_http_msg_srvhdr_val(h, TFW_HTTP_HDR_ETAG, &h_val);
+	tfw_str_del_chunk(h, h->nchunks - h_val.nchunks);
+	tfw_str_del_chunk(h, h->nchunks - 1);
+	h->flags |= TFW_STR_COMPLETE;
+}
+
 TEST(http1_parser, etag)
 {
 #define RESP_ETAG_START						\
@@ -2809,41 +2854,67 @@ TEST(http1_parser, etag)
 	"\r\n"                     \
 	"0123456789"
 
-#define FOR_ETAG(header, expected)				\
-	FOR_RESP(RESP_ETAG_START header "\r\n" RESP_ETAG_END)		\
-	{								\
-		TfwStr h_etag, s_etag;					\
-		DEFINE_TFW_STR(exp_etag, expected);			\
-									\
-		tfw_http_msg_srvhdr_val(				\
-			&resp->h_tbl->tbl[TFW_HTTP_HDR_ETAG],		\
-			TFW_HTTP_HDR_ETAG, &h_etag);			\
-		s_etag = tfw_str_next_str_val(&h_etag);			\
-		EXPECT_EQ(tfw_strcmpspn(&s_etag, &exp_etag, '"'), 0);	\
-									\
-		s_etag = tfw_str_next_str_val(&s_etag);			\
-		EXPECT_TRUE(TFW_STR_EMPTY(&s_etag));			\
+#define TFW_HTTP_MSG_SRVETAG_VAL(etag)						\
+	tfw_http_msg_srvhdr_val(&resp->h_tbl->tbl[TFW_HTTP_HDR_ETAG],		\
+				TFW_HTTP_HDR_ETAG, &etag)
+
+#define FOR_ETAG(header, expected, EXPECTED_QUOTES)				\
+	FOR_RESP(RESP_ETAG_START header "\r\n" RESP_ETAG_END)			\
+	{									\
+		TfwStr ah_etag, ph_etag, h_etag, s_etag;			\
+		DEFINE_TFW_STR(exp_etag, expected);				\
+										\
+		EXPECTED_QUOTES(!test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES,	\
+					  resp->flags));			\
+										\
+		if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags)) {	\
+			TFW_HTTP_MSG_SRVETAG_VAL(ph_etag);			\
+			EXPECT_EQ(__add_etag_temporary_quotes(resp), 0);	\
+		}								\
+										\
+		TFW_HTTP_MSG_SRVETAG_VAL(h_etag);				\
+		s_etag = tfw_str_next_str_val(&h_etag);				\
+		EXPECT_EQ(tfw_strcmpspn(&s_etag, &exp_etag, '"'), 0);		\
+										\
+		if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags)) {	\
+			__del_etag_temporary_quotes(resp);			\
+			TFW_HTTP_MSG_SRVETAG_VAL(ah_etag);			\
+		}								\
+										\
+		if (test_bit(TFW_HTTP_B_HDR_ETAG_HAS_NO_QOUTES, resp->flags))	\
+			EXPECT_EQ(tfw_strcmp(&ph_etag, &ah_etag), 0);		\
+										\
+		s_etag = tfw_str_next_str_val(&s_etag);				\
+		EXPECT_TRUE(TFW_STR_EMPTY(&s_etag));				\
 	}
 
 #define ETAG_BLOCK(header)						\
 	EXPECT_BLOCK_RESP(RESP_ETAG_START header "\r\n" RESP_ETAG_END)
 
-	FOR_ETAG("ETag:   \"dummy\"  ",  "dummy\"");
-	FOR_ETAG("ETag:   W/\"dummy\"  ", "dummy\"");
-	FOR_ETAG("ETag: \"\" ", "\"");
-	FOR_ETAG("ETag: W/\"\"", "\"");
-	FOR_ETAG("ETag: \"" ETAG_ALPHABET "\"",  ETAG_ALPHABET "\"");
-	FOR_ETAG("ETag: W/\"" ETAG_ALPHABET "\"",  ETAG_ALPHABET "\"");
+	FOR_ETAG("ETag:   \"dummy\"  ",  "dummy\"", EXPECT_TRUE);
+	FOR_ETAG("ETag:   dummy  ", "dummy\"", EXPECT_FALSE);
+	FOR_ETAG("Etag: 'dummy'", "'dummy'\"", EXPECT_FALSE);
+	FOR_ETAG("ETag:   W/\"dummy\"  ", "dummy\"", EXPECT_TRUE);
+	FOR_ETAG("ETag: \"\" ", "\"", EXPECT_TRUE);
+	FOR_ETAG("ETag:      ", "\"", EXPECT_FALSE);
+	FOR_ETAG("ETag: ", "\"", EXPECT_FALSE);
+	FOR_ETAG("ETag: W/\"\"", "\"", EXPECT_TRUE);
+	FOR_ETAG("ETag: " ETAG_ALPHABET,  ETAG_ALPHABET "\"", EXPECT_FALSE);
+	FOR_ETAG("ETag: \"" ETAG_ALPHABET "\"",  ETAG_ALPHABET "\"", EXPECT_TRUE);
+	FOR_ETAG("ETag: W/\"" ETAG_ALPHABET "\"",  ETAG_ALPHABET "\"", EXPECT_TRUE);
 
 	/* Same code is used to parse ETag header and If-None-Match header. */
+	ETAG_BLOCK("Etag: \"dum my\"");
+	ETAG_BLOCK("Etag: \"dummy \"");
+	ETAG_BLOCK("Etag:  *\"");
 	ETAG_BLOCK("ETag: \"dummy1\", \"dummy2\"");
-	ETAG_BLOCK("ETag: *\r\n");
 	COMMON_ETAG_BLOCK("ETag: ", ETAG_BLOCK);
 	ETAG_BLOCK("ETag: \"dummy\"\r\n"
 		       ": \"dummy\"");
 
 #undef ETAG_BLOCK
 #undef FOR_ETAG
+#undef TFW_HTTP_MSG_SRVETAG_VAL
 #undef RESP_ETAG_END
 #undef RESP_ETAG_START
 }
@@ -2943,7 +3014,7 @@ TEST(http1_parser, if_none_match)
 	EXPECT_BLOCK_REQ_SIMPLE("If-None-Match: \"" ETAG_2 "\", * ");
 	EXPECT_BLOCK_REQ_SIMPLE("If-None-Match: *, \"" ETAG_2 "\" ");
 
-	COMMON_ETAG_BLOCK("If-None-Match: ", EXPECT_BLOCK_REQ_SIMPLE);
+	COMMON_IF_NON_MATCH_BLOCK("If-None-Match: ", EXPECT_BLOCK_REQ_SIMPLE);
 	EXPECT_BLOCK_REQ_SIMPLE("If-None-Match: \"dummy\"\r\n"
 					     ": \"dummy\"");
 
diff --git a/fw/t/unit/test_http_parser_common.h b/fw/t/unit/test_http_parser_common.h
index 5a2ba904e..951385eec 100644
--- a/fw/t/unit/test_http_parser_common.h
+++ b/fw/t/unit/test_http_parser_common.h
@@ -89,10 +89,9 @@ enum {
 	BLOCK_MACRO(head "4294967295" tail)
 
 /* For ETag and If-None-Match headers */
-#define COMMON_ETAG_BLOCK(head, BLOCK_MACRO)			\
+#define __COMMON_ETAG_IF_NONE_MATCH_BLOCK(head, BLOCK_MACRO)	\
 	BLOCK_MACRO(head "\"dummy");				\
 	BLOCK_MACRO(head "dummy\"");				\
-	BLOCK_MACRO(head "'dummy'");				\
 	BLOCK_MACRO(head "W/ \"dummy\"");			\
 	BLOCK_MACRO(head "w/\"dummy\"");			\
 	BLOCK_MACRO(head "\"\x00\"");				\
@@ -101,6 +100,13 @@ enum {
 	BLOCK_MACRO(head "\" \"");				\
 	BLOCK_MACRO(head "\"\"\"")
 
+#define COMMON_ETAG_BLOCK(head, BLOCK_MACRO)			\
+	__COMMON_ETAG_IF_NONE_MATCH_BLOCK(head, BLOCK_MACRO)
+
+#define COMMON_IF_NON_MATCH_BLOCK(head, BLOCK_MACRO)		\
+	BLOCK_MACRO(head "'dummy'");				\
+	__COMMON_ETAG_IF_NONE_MATCH_BLOCK(head, BLOCK_MACRO)
+
 /**
  * Reference to a frame of bytes containing the request data to be parsed.
  * Such a frame corresponds to one HTTP request.
