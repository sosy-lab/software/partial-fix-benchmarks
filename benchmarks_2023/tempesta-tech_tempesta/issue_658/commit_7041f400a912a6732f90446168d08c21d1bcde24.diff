diff --git a/tempesta_fw/http.c b/tempesta_fw/http.c
index 7556bfb80..f8fc7415c 100644
--- a/tempesta_fw/http.c
+++ b/tempesta_fw/http.c
@@ -34,6 +34,8 @@
 
 #include "sync_socket.h"
 
+extern unsigned short tfw_block_action_flags;
+
 #define RESP_BUF_LEN			128
 static DEFINE_PER_CPU(char[RESP_BUF_LEN], g_buf);
 int ghprio; /* GFSM hook priority. */
@@ -713,6 +715,34 @@ tfw_http_req_error(TfwSrvConn *srv_conn, TfwHttpReq *req,
 	__tfw_http_req_error(req, equeue, status, reason);
 }
 
+static inline void
+tfw_http_error_resp_switch(TfwHttpReq *req, unsigned short status,
+			   const char *reason)
+{
+	switch(status) {
+	case 403:
+		tfw_http_send_403(req, reason);
+		break;
+	case 404:
+		tfw_http_send_404(req, reason);
+		break;
+	case 500:
+		tfw_http_send_500(req, reason);
+		break;
+	case 502:
+		tfw_http_send_502(req, reason);
+		break;
+	case 504:
+		tfw_http_send_504(req, reason);
+		break;
+	default:
+		TFW_WARN("Unexpected response error code: [%d]\n",
+			 status);
+		tfw_http_send_500(req, reason);
+		break;
+	}
+}
+
 /*
  * Forwarding of requests to a back end server is run under a lock
  * on the server connection's forwarding queue. It's performed as
@@ -735,25 +765,8 @@ tfw_http_req_zap_error(struct list_head *equeue)
 
 	list_for_each_entry_safe(req, tmp, equeue, fwd_list) {
 		list_del_init(&req->fwd_list);
-		switch(req->httperr.status) {
-		case 404:
-			tfw_http_send_404(req, req->httperr.reason);
-			break;
-		case 500:
-			tfw_http_send_500(req, req->httperr.reason);
-			break;
-		case 502:
-			tfw_http_send_502(req, req->httperr.reason);
-			break;
-		case 504:
-			tfw_http_send_504(req, req->httperr.reason);
-			break;
-		default:
-			TFW_WARN("Unexpected response error code: [%d]\n",
-				 req->httperr.status);
-			tfw_http_send_500(req, req->httperr.reason);
-			break;
-		}
+		tfw_http_error_resp_switch(req, req->httperr.status,
+					   req->httperr.reason);
 		TFW_INC_STAT_BH(clnt.msgs_otherr);
 	}
 }
@@ -1789,7 +1802,9 @@ __tfw_http_resp_fwd(TfwCliConn *cli_conn, struct list_head *ret_queue)
 	list_for_each_entry_safe(req, tmp, ret_queue, msg.seq_list) {
 		BUG_ON(!req->resp);
 		tfw_http_resp_init_ss_flags((TfwHttpResp *)req->resp, req);
-		if (tfw_cli_conn_send(cli_conn, (TfwMsg *)req->resp)) {
+		if (tfw_cli_conn_send(cli_conn, (TfwMsg *)req->resp) ||
+		    (TFW_CONN_TYPE(cli_conn) & Conn_Suspected &&
+		     req->flags & TFW_HTTP_SUSPECTED)) {
 			ss_close_sync(cli_conn->sk, true);
 			return;
 		}
@@ -2058,6 +2073,69 @@ tfw_http_req_set_context(TfwHttpReq *req)
 	return !req->vhost;
 }
 
+/*
+ * Function defines logging and response behaviour during
+ * detection of malformed or malicious messages. Can be
+ * called from both - client and server connection contexts.
+ *
+ * NOTE: @mark must be set only for client connection context
+ * because in this case we must mark client connection in
+ * special manner to delay its closing until transmission
+ * of error response will be finished.
+ */
+static inline void
+tfw_http_error_resp_and_log(bool reply, bool nolog, TfwHttpReq *req,
+			    unsigned short code, const char *msg, bool mark)
+{
+	TfwCliConn *cli_conn = (TfwCliConn *)req->conn;
+	if (reply) {
+		if (mark) {
+			BUG_ON(req->flags & TFW_HTTP_SUSPECTED ||
+			       TFW_CONN_TYPE(cli_conn) & Conn_Suspected);
+			TFW_CONN_TYPE(cli_conn) |= Conn_Suspected;
+			req->flags |= TFW_HTTP_SUSPECTED;
+			tfw_connection_unlink_msg(req->conn);
+			spin_lock(&cli_conn->seq_qlock);
+			list_add_tail(&req->msg.seq_list, &cli_conn->seq_queue);
+			spin_unlock(&cli_conn->seq_qlock);
+		}
+		tfw_http_error_resp_switch(req, code, msg);
+	}
+	else {
+		spin_lock(&cli_conn->seq_qlock);
+		if (unlikely(!list_empty(&req->msg.seq_list)))
+			list_del_init(&req->msg.seq_list);
+		spin_unlock(&cli_conn->seq_qlock);
+		tfw_http_conn_msg_free((TfwHttpMsg *)req);
+	}
+	if (!nolog)
+		TFW_WARN("Error response: %s, msg=%p conn=%p\n",
+			 msg, req, req->conn);
+}
+
+/**
+ * Defines behaviour for malformed messages depending on configuration
+ * settings: sending response error messages and logging.
+ */
+static void
+tfw_http_drop(TfwHttpReq *req, unsigned short code, const char *msg, bool mark)
+{
+	bool reply = tfw_block_action_flags & TFW_BLOCK_ACTION_ERROR_REPLY;
+ 	bool nolog = tfw_block_action_flags & TFW_BLOCK_ACTION_ERROR_NOLOG;
+	tfw_http_error_resp_and_log(reply, nolog, req, code, msg, mark);
+}
+
+/**
+ * Do the same as 'tfw_http_drop' function, but for malicious messages.
+ */
+static void
+tfw_http_block(TfwHttpReq *req, unsigned short code, const char *msg, bool mark)
+{
+	bool reply = tfw_block_action_flags & TFW_BLOCK_ACTION_ATTACK_REPLY;
+ 	bool nolog = tfw_block_action_flags & TFW_BLOCK_ACTION_ATTACK_NOLOG;
+	tfw_http_error_resp_and_log(reply, nolog, req, code, msg, mark);
+}
+
 /**
  * @return zero on success and negative value otherwise.
  * TODO enter the function depending on current GFSM state.
@@ -2112,16 +2190,19 @@ tfw_http_req_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 			BUG();
 		case TFW_BLOCK:
 			TFW_DBG2("Block invalid HTTP request\n");
-			tfw_http_conn_msg_free((TfwHttpMsg *)req);
 			TFW_INC_STAT_BH(clnt.msgs_parserr);
+			tfw_http_drop(req, 403, "failed to"
+				      " parse request", true);
 			return TFW_BLOCK;
 		case TFW_POSTPONE:
 			r = tfw_gfsm_move(&conn->state,
 					  TFW_HTTP_FSM_REQ_CHUNK, skb, off);
 			TFW_DBG3("TFW_HTTP_FSM_REQ_CHUNK return code %d\n", r);
 			if (r == TFW_BLOCK) {
-				tfw_http_conn_msg_free((TfwHttpMsg *)req);
 				TFW_INC_STAT_BH(clnt.msgs_filtout);
+				tfw_http_block(req, 403, "postponed"
+					       " request has been"
+					       " filtered out", true);
 				return TFW_BLOCK;
 			}
 			/*
@@ -2145,8 +2226,9 @@ tfw_http_req_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 		TFW_DBG3("TFW_HTTP_FSM_REQ_MSG return code %d\n", r);
 		/* Don't accept any following requests from the peer. */
 		if (r == TFW_BLOCK) {
-			tfw_http_conn_msg_free((TfwHttpMsg *)req);
 			TFW_INC_STAT_BH(clnt.msgs_filtout);
+			tfw_http_block(req, 403, "parsed request"
+				       " has been filtered out", true);
 			return TFW_BLOCK;
 		}
 
@@ -2159,7 +2241,9 @@ tfw_http_req_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 
 		/* Assign the right Vhost for this request. */
 		if (tfw_http_req_set_context(req)) {
-			tfw_http_conn_msg_free((TfwHttpMsg *)req);
+			TFW_INC_STAT_BH(clnt.msgs_otherr);
+			tfw_http_drop(req, 500, "cannot find"
+				      "Vhost for request", true);
 			return TFW_BLOCK;
 		}
 
@@ -2216,8 +2300,9 @@ tfw_http_req_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 				 */
 				TFW_WARN("Not enough memory to create"
 					 " a request sibling\n");
-				tfw_http_conn_msg_free((TfwHttpMsg *)req);
 				TFW_INC_STAT_BH(clnt.msgs_otherr);
+				tfw_http_drop(req, 500, "cannot create"
+					      " sibling request", true);
 				return TFW_BLOCK;
 			}
 		}
@@ -2414,7 +2499,7 @@ tfw_http_resp_gfsm(TfwHttpMsg *hmresp, struct sk_buff *skb, unsigned int off)
 		return TFW_BLOCK;
 	}
 
-	tfw_http_send_502(req, "response dropped: filtered out");
+	tfw_http_block(req, 502, "response blocked: filtered out", false);
 	tfw_http_conn_msg_free(hmresp);
 	TFW_INC_STAT_BH(serv.msgs_filtout);
 	return r;
@@ -2510,6 +2595,7 @@ tfw_http_resp_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 	unsigned int skb_len = skb->len;
 	TfwHttpReq *bad_req;
 	TfwHttpMsg *hmresp;
+	bool filtout = false;
 
 	BUG_ON(!conn->msg);
 	BUG_ON(data_off >= skb_len);
@@ -2570,6 +2656,7 @@ tfw_http_resp_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 			TFW_DBG3("TFW_HTTP_FSM_RESP_CHUNK return code %d\n", r);
 			if (r == TFW_BLOCK) {
 				TFW_INC_STAT_BH(serv.msgs_filtout);
+				filtout = true;
 				goto bad_msg;
 			}
 			/*
@@ -2658,8 +2745,16 @@ tfw_http_resp_process(TfwConn *conn, struct sk_buff *skb, unsigned int off)
 	return r;
 bad_msg:
 	bad_req = tfw_http_popreq(hmresp);
-	if (bad_req)
-		tfw_http_send_500(bad_req, "response dropped: processing error");
+	if (bad_req) {
+		if (filtout)
+			tfw_http_block(bad_req, 502,
+				       "response blocked:"
+				       " filtered out", false);
+		else
+			tfw_http_drop(bad_req, 500,
+				      "response dropped:"
+				      " processing error", false);
+	}
 	tfw_http_conn_msg_free(hmresp);
 	return r;
 }
diff --git a/tempesta_fw/http.h b/tempesta_fw/http.h
index 03aae4937..31e761dc9 100644
--- a/tempesta_fw/http.h
+++ b/tempesta_fw/http.h
@@ -290,6 +290,7 @@ typedef struct {
 /* URI has form http://authority/path, not just /path */
 #define TFW_HTTP_URI_FULL		0x000400
 #define TFW_HTTP_NON_IDEMP		0x000800
+#define TFW_HTTP_SUSPECTED		0x001000
 
 /* Response flags */
 #define TFW_HTTP_VOID_BODY		0x010000	/* Resp has no body */
@@ -474,6 +475,12 @@ typedef struct {
 #define FOR_EACH_HDR_FIELD_FROM(pos, end, msg, soff)			\
 	__FOR_EACH_HDR_FIELD(pos, end, msg, soff, (msg)->h_tbl->off)
 
+/* Bit flags for block action behaviour. */
+#define TFW_BLOCK_ACTION_ERROR_REPLY		0x0001
+#define TFW_BLOCK_ACTION_ATTACK_REPLY		0x0002
+#define TFW_BLOCK_ACTION_ERROR_NOLOG		0x0004
+#define TFW_BLOCK_ACTION_ATTACK_NOLOG		0x0008
+
 /* Get current timestamp in secs. */
 static inline time_t
 tfw_current_timestamp(void)
diff --git a/tempesta_fw/sock.c b/tempesta_fw/sock.c
index 3e550cd99..e556ec337 100644
--- a/tempesta_fw/sock.c
+++ b/tempesta_fw/sock.c
@@ -746,6 +746,11 @@ ss_tcp_process_skb(struct sock *sk, struct sk_buff *skb, int *processed)
 		 */
 		BUG_ON(conn == NULL);
 
+		if (SS_CONN_TYPE(sk) & Conn_Suspected) {
+			__kfree_skb(skb);
+			continue;
+		}
+
 		r = SS_CALL(connection_recv, conn, skb, offset);
 
 		if (r < 0) {
@@ -858,10 +863,13 @@ ss_tcp_data_ready(struct sock *sk)
 		TFW_ERR("error data in socket %p\n", sk);
 	}
 	else if (!skb_queue_empty(&sk->sk_receive_queue)) {
-		if (ss_tcp_process_data(sk)) {
+		if (ss_tcp_process_data(sk) &&
+		    !(SS_CONN_TYPE(sk) & Conn_Suspected)) {
 			/*
 			 * Drop connection in case of internal errors,
-			 * banned packets, or FIN in the received packet.
+			 * banned packets, or FIN in the received packet,
+			 * and only if we don't wait for sending the error
+			 * response to client (Conn_Suspected bit set).
 			 *
 			 * ss_linkerror() is responsible for calling
 			 * application layer connection closing callback.
diff --git a/tempesta_fw/sock_clnt.c b/tempesta_fw/sock_clnt.c
index 911c62c43..2c1f63696 100644
--- a/tempesta_fw/sock_clnt.c
+++ b/tempesta_fw/sock_clnt.c
@@ -37,6 +37,8 @@
  * ------------------------------------------------------------------------
  */
 
+unsigned short tfw_block_action_flags = TFW_BLOCK_ACTION_ERROR_REPLY;
+
 static struct kmem_cache *tfw_cli_conn_cache;
 static struct kmem_cache *tfw_tls_conn_cache;
 static int tfw_cli_cfg_ka_timeout = -1;
@@ -44,8 +46,8 @@ static int tfw_cli_cfg_ka_timeout = -1;
 static inline struct kmem_cache *
 tfw_cli_cache(int type)
 {
-	return type == Conn_HttpClnt ?
-		tfw_cli_conn_cache : tfw_tls_conn_cache;
+	return type & TFW_FSM_HTTPS ?
+		tfw_tls_conn_cache : tfw_cli_conn_cache;
 }
 
 static void
@@ -585,6 +587,74 @@ tfw_sock_clnt_cfg_handle_keepalive(TfwCfgSpec *cs, TfwCfgEntry *ce)
 	return 0;
 }
 
+static int
+tfw_cfg_define_block_action(const char *action, unsigned short mask,
+			    unsigned short *flags)
+{
+	if (!strcasecmp(action, "reply")) {
+		*flags |= mask;
+	} else if (!strcasecmp(action, "drop")) {
+		*flags &= ~mask;
+	} else {
+		TFW_ERR_NL("Unsupported argument: '%s'\n", action);
+		return -EINVAL;
+	}
+	return 0;
+}
+
+static int
+tfw_cfg_define_block_nolog(TfwCfgEntry *ce, unsigned short mask,
+			   unsigned short *flags)
+{
+	if (ce->val_n == 3) {
+		if (!strcasecmp(ce->vals[2], "nolog"))
+			*flags |= mask;
+		else {
+			TFW_ERR_NL("Unsupported argument: '%s'\n", ce->vals[2]);
+			return -EINVAL;
+		}
+	} else {
+		*flags &= ~mask;
+	}
+	return 0;
+}
+
+static int
+tfw_sock_clnt_cfg_handle_block_action(TfwCfgSpec *cs, TfwCfgEntry *ce)
+{
+	if (ce->val_n < 2 || ce->val_n > 3) {
+		TFW_ERR_NL("Invalid number of arguments: %zu\n", ce->val_n);
+		return -EINVAL;
+	}
+	if (ce->attr_n) {
+		TFW_ERR_NL("Unexpected attributes\n");
+		return -EINVAL;
+	}
+
+	if (!strcasecmp(ce->vals[0], "error")) {
+		if (tfw_cfg_define_block_action(ce->vals[1],
+						TFW_BLOCK_ACTION_ERROR_REPLY,
+						&tfw_block_action_flags) ||
+		    tfw_cfg_define_block_nolog(ce,
+					       TFW_BLOCK_ACTION_ERROR_NOLOG,
+					       &tfw_block_action_flags))
+			return -EINVAL;
+	} else if (!strcasecmp(ce->vals[0], "attack")) {
+		if (tfw_cfg_define_block_action(ce->vals[1],
+						TFW_BLOCK_ACTION_ATTACK_REPLY,
+						&tfw_block_action_flags) ||
+		    tfw_cfg_define_block_nolog(ce,
+					       TFW_BLOCK_ACTION_ATTACK_NOLOG,
+					       &tfw_block_action_flags))
+			return -EINVAL;
+	} else {
+		TFW_ERR_NL("Unsupported argument: '%s'\n", ce->vals[0]);
+		return -EINVAL;
+	}
+
+	return 0;
+}
+
 static void
 tfw_sock_clnt_cfg_cleanup_listen(TfwCfgSpec *cs)
 {
@@ -610,6 +680,13 @@ TfwCfgMod tfw_sock_clnt_cfg_mod  = {
 			.allow_repeat = false,
 			.cleanup = tfw_sock_clnt_cfg_cleanup_listen
 		},
+		{
+			"block_action",
+			NULL,
+			tfw_sock_clnt_cfg_handle_block_action,
+			.allow_repeat = true,
+			.allow_none = true
+		},
 		{}
 	}
 };
diff --git a/tempesta_fw/sync_socket.h b/tempesta_fw/sync_socket.h
index c57bfe3f6..cf1ba7708 100644
--- a/tempesta_fw/sync_socket.h
+++ b/tempesta_fw/sync_socket.h
@@ -35,6 +35,12 @@ typedef struct ss_proto_t {
 	int			type;
 } SsProto;
 
+/* Flag bits offset for SsProto field. */
+#define	__Flag_Bits		0x10
+
+/* No requests longer accepted (flag only for client connections) */
+#define	Conn_Suspected		(0x1 << __Flag_Bits)
+
 /* Table of Synchronous Sockets connection callbacks. */
 typedef struct ss_hooks {
 	/* New connection accepted. */
@@ -133,4 +139,7 @@ void ss_get_stat(SsStat *stat);
 	? ((SsProto *)(sk)->sk_user_data)->hooks->f(__VA_ARGS__)	\
 	: 0)
 
+#define SS_CONN_TYPE(sk)							\
+	(((SsProto *)(sk)->sk_user_data)->type)
+
 #endif /* __SS_SOCK_H__ */
