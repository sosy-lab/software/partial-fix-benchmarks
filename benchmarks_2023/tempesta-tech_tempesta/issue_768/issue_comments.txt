Chunked Transfer Coding - chunk-size decoding issue
@milabs thank you for the report!
The `acc` limit checking has following problems which must be fixed:

- `UINT_MAX` limit don't work against negative values;
- 32-bit integer could be not enough since there is no RFC limitation for maximum chunk size, so a poor server can send quite large file in single chunk. So `long` type must be used instead. Please check all other code for `int`/`long` compatibility.

Issue #498 (HTTP message buffering and streaming) requires streaming HTTP processing. Meantime, having too large chunks (as well as too large message bodies) we can fail by OOM. To mitigate the problem one can use `http_body_len` Frang limit.