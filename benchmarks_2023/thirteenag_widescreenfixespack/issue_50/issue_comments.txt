Sniper Elite 2005 HUD fix + Project Snowblind Request
I don't know, can't make any promises.

There is a fix available here: https://bitbucket.org/fk/snowblind-widescreen-fix/downloads

WOW! Thanks!

2015-10-09 23:59 GMT+02:00 Keith94 notifications@github.com:

> There is a fix available here:
> https://bitbucket.org/fk/snowblind-widescreen-fix/downloads
> 
> —
> Reply to this email directly or view it on GitHub
> https://github.com/ThirteenAG/Widescreen_Fixes_Pack/issues/50#issuecomment-146996005
> .

Up for Sniper Elite.

Some elements are still in need of some treatment (when you'll have some time!)
*crosshair
*sniper rifle gunsight (scope)
*minimap

Minimap fi xis not complete, objective locators (arrows) keep stretching horizontally.

I was able to fix radar arrows, turns out it also fixes text scaling. I'll reupload the result soon.
