RFE: please enable NVMe boot in OVMF
What does NVMe enable that virtio-blk / virtio-scsi disks don't?

(Not arguing against the feature request, I'd just like to know.)

Please also provide a reasonably short QEMU command line that exposes a drive as an NVMe device to the guest. Thanks.

The title for this entry should state "OVMF"; can you please edit it?

@stamerlan haha, sorry, I was not clear enough. In more details:

This tracker is not just for OVMF, it is for the entirety of edk2. In sensible bug tracking systems, like Bugzilla, you have products, components, sub-components and such -- metadata for each bug report. When you report a bug in those sensible systems, you are expected to make an effort to categorize your bug report _up-front_, in order to save time for the people who _read_ the bug reports.

In _this_ bug tracker though, we have no such metadata, the best we can do is (apparently) "tags". So @jljusten tends to tag OVMF-related bug reports with the "OVMF" label. But, since you know up-front that you are reporting the issue for OVMF, not another platform built from edk2, you should immediately signal in the title of your report that it is for OVMF.

Therefore the title should contain _both_ OVMF _and_ NVMe boot.

Really, there's a huge disconnect here between users of OVMF, and edk2, the open source project. One advantage that a well-maintained Bugzilla instance has is that it _forces_ bug reporters to at least think about metadata, plus it provides a bug report template that must be completed with important information. Github's issue tracker encourages reckless user behavior -- just dump the three words that come to your mind into a new item. (But hey at least you can format those three words with markdown!)

This open letter is completely justified: https://github.com/dear-github/dear-github

To top it all off, I personally lack the credentials to change bug titles, bug statuses, tags; anything at all beyond commenting. (Except, of course, I have the privilege of fixing bugs.)

I knew using a web-era issue tracker for edk2 would be a mistake, and I stand vindicated.

TL;DR: please change the title to: "RFE: please enable NVMe boot in OVMF".

Also, please try to answer my other questions above [1]. In any RFE (request for enhancement), you gotta come up with some justification _why_ the developers should spend their time on the feature you'd like to have. As I said, I don't doubt this is a reasonable request, but I do care about your reasons for wanting to use NVMe.

[1] In a sane bug tracker, I would reference earlier comments by writing "comment 3" or "comment 4", and the tracker would automatically turn those words into links. In this tracker however, comments have no numbers (they don't even have human readable exact timestamps! WTF is "lersek commented an hour ago" supposed to mean?!), I can only copy and paste garbage-looking links like this: https://github.com/tianocore/edk2/issues/48#issuecomment-175144378

GitHub issue tracker, the social media of bug reporting, for the facebook generation.

I meant qemu virtual nvme disk:
qemu-system-x86_64 \
-drive file=ssd.img,if=none,id=disk0,format=raw \
-device nvme,drive=drive0,serial=1234

I will assume that http://thread.gmane.org/gmane.comp.bios.coreboot.seabios/10375 is related.

Yup, @lersek it's my e-mail. Shell I add explanation in the 1st post or leave one more comment?

@stamerlan if you could expand the first comment with details, that would be great. Thanks!

@stamerlan thanks for explaining your use case in the first comment. It is perfectly reasonable; OVMF is regularly used for prototyping and testing otherwise hardware-oriented features.

@lersek as far as I understand virtio parameters used to cooperate with hypervisor to use real HW?

@stamerlan I don't understand the question. But anyway I just wanted to be sure that NVMe was specifically needed for your use case (i.e., it couldn't be served by virtio disks).

Posted prerequisite patch for QEMU:
http://news.gmane.org/find-root.php?message_id=1453850483-27511-1-git-send-email-lersek@redhat.com

Posted edk2 series:
http://thread.gmane.org/gmane.comp.bios.edk2.devel/6950

@stamerlan: can you please test my patches (QEMU and OVMF) that I linked in https://github.com/tianocore/edk2/issues/48#issuecomment-175290253 and https://github.com/tianocore/edk2/issues/48#issuecomment-175354318 ? I CC'd you on both sets directly, so you should have them in your email. A `Tested-by` from users is highly appreciated. Thanks.

@Hi, @lersek 
I saw e-mails, but I got sick, so I'll come to office to test it tomorrow.

@stamerlan take your time, get better first. thx

Hi, @lersek 

Yes, your patch is working. I successfully installed windows 10 x86_64 on qemu!

Thank you a lot!

P.S. what happened with nvme_boot_issue48, today I can't checkout it?

@stamerlan Thank you very much for the testing and the feedback. I highly appreciate it!

Please don't close this item just yet though -- can you please reopen it? The patches are still under review, and have not been committed to either QEMU or OVMF. This item should only be closed when the edk2 patches are "in".

However, please _do_ report your successful testing in _both_ the QEMU and edk2 threads. Please locate the following messages in your mailbox:

```
[PATCH 0/3] OvmfPkg: enable NVMe devices
[PATCH] nvme: generate OpenFirmware device path in the "bootorder" fw_cfg file
```

hit _Reply All_, and respond with

```
Tested-by: Vladislav Vovchenko <vladislav.vovchenko@sk.com>
```

This way reviewers will see that the patch has undergone independent testing, and your testing feedback will be captured in the commit log. Thank you!

@stamerlan 

> P.S. what happened with nvme_boot_issue48, today I can't checkout it?

I have no clue, it must be a github issue or a network glitch between you & github; I didn't touch this branch.

(I never touch branches that I push for review. If a new version of the series becomes necessary, I push XXXX_v2, XXXX_v3 branches. Such a branch should stay reflecting the mailing list posting.)

Sorry for my delay. I have no access to my work-mailbox from home.
I replied today in qemu and edk2 threads.

Thank you a lot!

Patches are upstream, both QEMU and edk2. Closing.

This (closed) item has been manually migrated to
https://tianocore.acgmultimedia.com/show_bug.cgi?id=79
