diff --git a/src/chunk.c b/src/chunk.c
index 03245e5fdb1..73bd836c16c 100644
--- a/src/chunk.c
+++ b/src/chunk.c
@@ -2659,7 +2659,21 @@ chunk_tuple_delete(TupleInfo *ti, DropBehavior behavior, bool preserve_chunk_cat
 					ts_dimension_slice_scan_by_id_and_lock(cc->fd.dimension_slice_id,
 														   &tuplock,
 														   CurrentMemoryContext);
-				if (ts_chunk_constraint_scan_by_dimension_slice_id(slice->fd.id,
+				/* If the slice is not found in the scan above, the table is
+				 * broken so we do not delete the slice. We proceed with
+				 * anyway since users need to be able to drop broken tables or
+				 * remove broken chunks. */
+				if (!slice)
+					ereport(WARNING,
+							(errmsg("dimension slice %d was missing, proceeding anyway",
+									cc->fd.dimension_slice_id),
+							 errdetail("Chunk \"%s.%s\" is dependent on dimension slice %d, but it "
+									   "was missing. Proceeding to delete chunk anyway.",
+									   quote_identifier(NameStr(form.schema_name)),
+									   quote_identifier(NameStr(form.table_name)),
+									   cc->fd.dimension_slice_id)));
+				if (slice &&
+					ts_chunk_constraint_scan_by_dimension_slice_id(slice->fd.id,
 																   NULL,
 																   CurrentMemoryContext) == 0)
 					ts_dimension_slice_delete_by_id(cc->fd.dimension_slice_id, false);
diff --git a/test/expected/broken_tables.out b/test/expected/broken_tables.out
new file mode 100644
index 00000000000..711bd0f0ab0
--- /dev/null
+++ b/test/expected/broken_tables.out
@@ -0,0 +1,124 @@
+-- This file and its contents are licensed under the Apache License 2.0.
+-- Please see the included NOTICE for copyright information and
+-- LICENSE-APACHE for a copy of the license.
+-- Hypertables can break as a result of race conditions, but we should
+-- still not crash when trying to truncate or delete the broken table.
+\c :TEST_DBNAME :ROLE_SUPERUSER
+CREATE VIEW missing_slices AS
+SELECT DISTINCT
+    dimension_slice_id,
+    constraint_name,
+    attname AS column_name,
+    pg_get_expr(conbin, conrelid) AS constraint_expr
+FROM
+    _timescaledb_catalog.chunk_constraint cc
+    JOIN _timescaledb_catalog.chunk ch ON cc.chunk_id = ch.id
+    JOIN pg_constraint ON conname = constraint_name
+    JOIN pg_namespace ns ON connamespace = ns.oid
+        AND ns.nspname = ch.schema_name
+    JOIN pg_attribute ON attnum = conkey[1]
+        AND attrelid = conrelid
+WHERE
+    dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice);
+-- To drop rows from dimension_slice table, we need to remove some
+-- constraints.
+ALTER TABLE _timescaledb_catalog.chunk_constraint
+      DROP CONSTRAINT chunk_constraint_dimension_slice_id_fkey;
+CREATE TABLE chunk_test_int(time integer, temp float8, tag integer, color integer);
+SELECT create_hypertable('chunk_test_int', 'time', 'tag', 2, chunk_time_interval => 3);
+NOTICE:  adding not-null constraint to column "time"
+      create_hypertable      
+-----------------------------
+ (1,public,chunk_test_int,t)
+(1 row)
+
+INSERT INTO chunk_test_int VALUES
+       (4, 24.3, 1, 1),
+       (4, 24.3, 2, 1),
+       (10, 24.3, 2, 1);
+SELECT * FROM _timescaledb_catalog.dimension_slice ORDER BY id;
+ id | dimension_id |     range_start      |      range_end      
+----+--------------+----------------------+---------------------
+  1 |            1 |                    3 |                   6
+  2 |            2 | -9223372036854775808 |          1073741823
+  3 |            2 |           1073741823 | 9223372036854775807
+  4 |            1 |                    9 |                  12
+(4 rows)
+
+SELECT DISTINCT
+       chunk_id,
+       dimension_slice_id,
+       constraint_name,
+       pg_get_expr(conbin, conrelid) AS constraint_expr
+FROM _timescaledb_catalog.chunk_constraint,
+     LATERAL (
+     	     SELECT *
+	     FROM pg_constraint JOIN pg_namespace ns ON connamespace = ns.oid
+	     WHERE conname = constraint_name
+     ) AS con
+ORDER BY chunk_id, dimension_slice_id;
+ chunk_id | dimension_slice_id | constraint_name |                        constraint_expr                        
+----------+--------------------+-----------------+---------------------------------------------------------------
+        1 |                  1 | constraint_1    | (("time" >= 3) AND ("time" < 6))
+        1 |                  2 | constraint_2    | (_timescaledb_internal.get_partition_hash(tag) < 1073741823)
+        2 |                  1 | constraint_1    | (("time" >= 3) AND ("time" < 6))
+        2 |                  3 | constraint_3    | (_timescaledb_internal.get_partition_hash(tag) >= 1073741823)
+        3 |                  3 | constraint_3    | (_timescaledb_internal.get_partition_hash(tag) >= 1073741823)
+        3 |                  4 | constraint_4    | (("time" >= 9) AND ("time" < 12))
+(6 rows)
+
+DELETE FROM _timescaledb_catalog.dimension_slice WHERE id = 1;
+SELECT * FROM missing_slices;
+ dimension_slice_id | constraint_name | column_name |         constraint_expr          
+--------------------+-----------------+-------------+----------------------------------
+                  1 | constraint_1    | time        | (("time" >= 3) AND ("time" < 6))
+(1 row)
+
+TRUNCATE TABLE chunk_test_int;
+WARNING:  dimension slice 1 was missing, proceeding anyway
+WARNING:  dimension slice 1 was missing, proceeding anyway
+DROP TABLE chunk_test_int;
+CREATE TABLE chunk_test_int(time integer, temp float8, tag integer, color integer);
+SELECT create_hypertable('chunk_test_int', 'time', 'tag', 2, chunk_time_interval => 3);
+NOTICE:  adding not-null constraint to column "time"
+      create_hypertable      
+-----------------------------
+ (2,public,chunk_test_int,t)
+(1 row)
+
+INSERT INTO chunk_test_int VALUES
+       (4, 24.3, 1, 1),
+       (4, 24.3, 2, 1),
+       (10, 24.3, 2, 1);
+SELECT DISTINCT
+       chunk_id,
+       dimension_slice_id,
+       constraint_name,
+       pg_get_expr(conbin, conrelid) AS constraint_expr
+FROM _timescaledb_catalog.chunk_constraint,
+     LATERAL (
+     	     SELECT *
+	     FROM pg_constraint JOIN pg_namespace ns ON connamespace = ns.oid
+	     WHERE conname = constraint_name
+     ) AS con
+ORDER BY chunk_id, dimension_slice_id;
+ chunk_id | dimension_slice_id | constraint_name |                        constraint_expr                        
+----------+--------------------+-----------------+---------------------------------------------------------------
+        4 |                  5 | constraint_5    | (("time" >= 3) AND ("time" < 6))
+        4 |                  6 | constraint_6    | (_timescaledb_internal.get_partition_hash(tag) < 1073741823)
+        5 |                  5 | constraint_5    | (("time" >= 3) AND ("time" < 6))
+        5 |                  7 | constraint_7    | (_timescaledb_internal.get_partition_hash(tag) >= 1073741823)
+        6 |                  7 | constraint_7    | (_timescaledb_internal.get_partition_hash(tag) >= 1073741823)
+        6 |                  8 | constraint_8    | (("time" >= 9) AND ("time" < 12))
+(6 rows)
+
+DELETE FROM _timescaledb_catalog.dimension_slice WHERE id = 5;
+SELECT * FROM missing_slices;
+ dimension_slice_id | constraint_name | column_name |         constraint_expr          
+--------------------+-----------------+-------------+----------------------------------
+                  5 | constraint_5    | time        | (("time" >= 3) AND ("time" < 6))
+(1 row)
+
+DROP TABLE chunk_test_int;
+WARNING:  dimension slice 5 was missing, proceeding anyway
+WARNING:  dimension slice 5 was missing, proceeding anyway
diff --git a/test/sql/CMakeLists.txt b/test/sql/CMakeLists.txt
index f82bf7ab4ee..074858f9adb 100644
--- a/test/sql/CMakeLists.txt
+++ b/test/sql/CMakeLists.txt
@@ -1,6 +1,7 @@
 set(TEST_FILES
   alter.sql
   alternate_users.sql
+  broken_tables.sql
   chunks.sql
   chunk_utils.sql
   cluster.sql
diff --git a/test/sql/broken_tables.sql b/test/sql/broken_tables.sql
new file mode 100644
index 00000000000..09d53c6ee62
--- /dev/null
+++ b/test/sql/broken_tables.sql
@@ -0,0 +1,85 @@
+-- This file and its contents are licensed under the Apache License 2.0.
+-- Please see the included NOTICE for copyright information and
+-- LICENSE-APACHE for a copy of the license.
+
+-- Hypertables can break as a result of race conditions, but we should
+-- still not crash when trying to truncate or delete the broken table.
+
+\c :TEST_DBNAME :ROLE_SUPERUSER
+
+CREATE VIEW missing_slices AS
+SELECT DISTINCT
+    dimension_slice_id,
+    constraint_name,
+    attname AS column_name,
+    pg_get_expr(conbin, conrelid) AS constraint_expr
+FROM
+    _timescaledb_catalog.chunk_constraint cc
+    JOIN _timescaledb_catalog.chunk ch ON cc.chunk_id = ch.id
+    JOIN pg_constraint ON conname = constraint_name
+    JOIN pg_namespace ns ON connamespace = ns.oid
+        AND ns.nspname = ch.schema_name
+    JOIN pg_attribute ON attnum = conkey[1]
+        AND attrelid = conrelid
+WHERE
+    dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice);
+
+-- To drop rows from dimension_slice table, we need to remove some
+-- constraints.
+ALTER TABLE _timescaledb_catalog.chunk_constraint
+      DROP CONSTRAINT chunk_constraint_dimension_slice_id_fkey;
+
+CREATE TABLE chunk_test_int(time integer, temp float8, tag integer, color integer);
+SELECT create_hypertable('chunk_test_int', 'time', 'tag', 2, chunk_time_interval => 3);
+
+INSERT INTO chunk_test_int VALUES
+       (4, 24.3, 1, 1),
+       (4, 24.3, 2, 1),
+       (10, 24.3, 2, 1);
+
+SELECT * FROM _timescaledb_catalog.dimension_slice ORDER BY id;
+
+SELECT DISTINCT
+       chunk_id,
+       dimension_slice_id,
+       constraint_name,
+       pg_get_expr(conbin, conrelid) AS constraint_expr
+FROM _timescaledb_catalog.chunk_constraint,
+     LATERAL (
+     	     SELECT *
+	     FROM pg_constraint JOIN pg_namespace ns ON connamespace = ns.oid
+	     WHERE conname = constraint_name
+     ) AS con
+ORDER BY chunk_id, dimension_slice_id;
+
+DELETE FROM _timescaledb_catalog.dimension_slice WHERE id = 1;
+SELECT * FROM missing_slices;
+
+TRUNCATE TABLE chunk_test_int;
+DROP TABLE chunk_test_int;
+
+CREATE TABLE chunk_test_int(time integer, temp float8, tag integer, color integer);
+SELECT create_hypertable('chunk_test_int', 'time', 'tag', 2, chunk_time_interval => 3);
+
+INSERT INTO chunk_test_int VALUES
+       (4, 24.3, 1, 1),
+       (4, 24.3, 2, 1),
+       (10, 24.3, 2, 1);
+
+SELECT DISTINCT
+       chunk_id,
+       dimension_slice_id,
+       constraint_name,
+       pg_get_expr(conbin, conrelid) AS constraint_expr
+FROM _timescaledb_catalog.chunk_constraint,
+     LATERAL (
+     	     SELECT *
+	     FROM pg_constraint JOIN pg_namespace ns ON connamespace = ns.oid
+	     WHERE conname = constraint_name
+     ) AS con
+ORDER BY chunk_id, dimension_slice_id;
+
+DELETE FROM _timescaledb_catalog.dimension_slice WHERE id = 5;
+SELECT * FROM missing_slices;
+
+DROP TABLE chunk_test_int;
