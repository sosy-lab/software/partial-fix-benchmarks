diff --git a/src/chunk.c b/src/chunk.c
index 73bd836c16c..a0646628170 100644
--- a/src/chunk.c
+++ b/src/chunk.c
@@ -35,6 +35,7 @@
 #include <access/tupdesc.h>
 
 #include "export.h"
+#include "debug_wait.h"
 #include "chunk.h"
 #include "chunk_index.h"
 #include "chunk_data_node.h"
@@ -2672,10 +2673,9 @@ chunk_tuple_delete(TupleInfo *ti, DropBehavior behavior, bool preserve_chunk_cat
 									   quote_identifier(NameStr(form.schema_name)),
 									   quote_identifier(NameStr(form.table_name)),
 									   cc->fd.dimension_slice_id)));
-				if (slice &&
-					ts_chunk_constraint_scan_by_dimension_slice_id(slice->fd.id,
-																   NULL,
-																   CurrentMemoryContext) == 0)
+				else if (ts_chunk_constraint_scan_by_dimension_slice_id(slice->fd.id,
+																		NULL,
+																		CurrentMemoryContext) == 0)
 					ts_dimension_slice_delete_by_id(cc->fd.dimension_slice_id, false);
 			}
 		}
@@ -3443,6 +3443,8 @@ ts_chunk_do_drop_chunks(Hypertable *ht, Datum older_than_datum, Datum newer_than
 											   &num_chunks,
 											   &tuplock);
 
+	DEBUG_WAITPOINT("drop_chunks_chunks_found");
+
 	if (has_continuous_aggs)
 		ts_chunk_drop_process_invalidations(ht->main_table_relid,
 											older_than_datum,
diff --git a/src/compat.h b/src/compat.h
index 48a4e9ea593..76b8072b5bc 100644
--- a/src/compat.h
+++ b/src/compat.h
@@ -254,6 +254,12 @@
 #define TUPLE_DESC_HAS_OIDS(desc) false
 #endif
 
+#if defined(__GNUC__)
+#define TS_ATTRIBUTE_NONNULL(X) __attribute__((nonnull X))
+#else
+#define TS_ATTRIBUTE_NONNULL(X)
+#endif
+
 /* Compatibility functions for table access method API introduced in PG12 */
 #if PG11
 #include "compat/tupconvert.h"
diff --git a/src/dimension_slice.c b/src/dimension_slice.c
index a9c591dc970..2142f811fb6 100644
--- a/src/dimension_slice.c
+++ b/src/dimension_slice.c
@@ -26,6 +26,18 @@
 
 #include "compat.h"
 
+static const char *tuple_status_string[] = {
+	[TM_Ok] = "Ok",
+	[TM_Invisible] = "Invisible",
+	[TM_SelfModified] = "SelfModified",
+	[TM_Updated] = "Updated",
+#if PG12_GE
+	[TM_Deleted] = "Deleted",
+#endif
+	[TM_BeingModified] = "BeingModified",
+	[TM_WouldBlock] = "WouldBlock",
+};
+
 /* Put DIMENSION_SLICE_MAXVALUE point in same slice as DIMENSION_SLICE_MAXVALUE-1, always */
 /* This avoids the problem with coord < range_end where coord and range_end is an int64 */
 #define REMAP_LAST_COORDINATE(coord)                                                               \
@@ -110,6 +122,9 @@ lock_result_ok_or_abort(TupleInfo *ti, DimensionSlice *slice)
 		case TM_Ok:
 			break;
 
+#if PG12_GE
+		case TM_Deleted:
+#endif
 		case TM_Updated:
 			ereport(ERROR,
 					(errcode(ERRCODE_LOCK_NOT_AVAILABLE),
@@ -124,6 +139,7 @@ lock_result_ok_or_abort(TupleInfo *ti, DimensionSlice *slice)
 					 errmsg("dimension slice %d updated by other transaction", slice->fd.id),
 					 errhint("Retry the operation again.")));
 			pg_unreachable();
+			break;
 
 		case TM_Invisible:
 			elog(ERROR, "attempt to lock invisible tuple");
@@ -132,7 +148,12 @@ lock_result_ok_or_abort(TupleInfo *ti, DimensionSlice *slice)
 
 		case TM_WouldBlock:
 		default:
-			elog(ERROR, "unexpected tuple lock status");
+			if (tuple_status_string[ti->lockresult])
+				elog(ERROR,
+					 "unexpected tuple lock status: %s",
+					 tuple_status_string[ti->lockresult]);
+			else
+				elog(ERROR, "unexpected tuple lock status: %d", ti->lockresult);
 			pg_unreachable();
 			break;
 	}
@@ -587,6 +608,11 @@ dimension_slice_tuple_found(TupleInfo *ti, void *data)
 	return SCAN_DONE;
 }
 
+/* Scan for a slice by dimension slice id.
+ *
+ * If you're scanning for a tuple, you have to provide a lock, since, otherwise,
+ * concurrent threads can do bad things with the tuple and you probably want
+ * it to not change nor disappear. */
 DimensionSlice *
 ts_dimension_slice_scan_by_id_and_lock(int32 dimension_slice_id, ScanTupLock *tuplock,
 									   MemoryContext mctx)
diff --git a/src/dimension_slice.h b/src/dimension_slice.h
index 2b129b8c0d3..b837c3b9936 100644
--- a/src/dimension_slice.h
+++ b/src/dimension_slice.h
@@ -53,7 +53,8 @@ extern DimensionVec *ts_dimension_slice_collision_scan_limit(int32 dimension_id,
 extern bool ts_dimension_slice_scan_for_existing(DimensionSlice *slice);
 extern DimensionSlice *ts_dimension_slice_scan_by_id_and_lock(int32 dimension_slice_id,
 															  ScanTupLock *tuplock,
-															  MemoryContext mctx);
+															  MemoryContext mctx)
+	TS_ATTRIBUTE_NONNULL((2));
 extern DimensionVec *ts_dimension_slice_scan_by_dimension(int32 dimension_id, int limit);
 extern DimensionVec *ts_dimension_slice_scan_by_dimension_before_point(int32 dimension_id,
 																	   int64 point, int limit,
diff --git a/src/hypercube.c b/src/hypercube.c
index 7462860d3ed..af460969505 100644
--- a/src/hypercube.c
+++ b/src/hypercube.c
@@ -176,9 +176,18 @@ ts_hypercube_from_constraints(ChunkConstraints *constraints, MemoryContext mctx)
 		if (is_dimension_constraint(cc))
 		{
 			DimensionSlice *slice;
+			ScanTupLock tuplock = {
+				.lockmode = LockTupleKeyShare,
+				.waitpolicy = LockWaitBlock,
+			};
 
 			Assert(hc->num_slices < constraints->num_dimension_constraints);
-			slice = ts_dimension_slice_scan_by_id_and_lock(cc->fd.dimension_slice_id, NULL, mctx);
+			/* When building the hypercube, we reference the dimension slices
+			 * to construct the hypercube. This means that the we need to add
+			 * a tuple lock on the dimension slices to prevent them from being
+			 * removed by a concurrently executing operation. */
+			slice =
+				ts_dimension_slice_scan_by_id_and_lock(cc->fd.dimension_slice_id, &tuplock, mctx);
 			Assert(slice != NULL);
 			hc->slices[hc->num_slices++] = slice;
 		}
diff --git a/src/hypertable.c b/src/hypertable.c
index db3d3967398..83a9dbd2989 100644
--- a/src/hypertable.c
+++ b/src/hypertable.c
@@ -862,6 +862,10 @@ ts_hypertable_lock_tuple_simple(Oid table_relid)
 		case TM_Ok:
 			/* successfully locked */
 			return true;
+
+#if PG12_GE
+		case TM_Deleted:
+#endif
 		case TM_Updated:
 			ereport(ERROR,
 					(errcode(ERRCODE_LOCK_NOT_AVAILABLE),
@@ -870,6 +874,7 @@ ts_hypertable_lock_tuple_simple(Oid table_relid)
 					 errhint("Retry the operation again.")));
 			pg_unreachable();
 			return false;
+
 		case TM_BeingModified:
 			ereport(ERROR,
 					(errcode(ERRCODE_LOCK_NOT_AVAILABLE),
diff --git a/test/isolation/expected/dropchunks_race.out b/test/isolation/expected/dropchunks_race.out
new file mode 100644
index 00000000000..971c003eca7
--- /dev/null
+++ b/test/isolation/expected/dropchunks_race.out
@@ -0,0 +1,22 @@
+Parsed test spec with 3 sessions
+
+starting permutation: s1a s2a s3a s3b
+debug_waitpoint_enable
+
+               
+step s1a: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); <waiting ...>
+step s2a: SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); <waiting ...>
+step s3a: SELECT debug_waitpoint_release('drop_chunks_chunks_found');
+debug_waitpoint_release
+
+               
+step s1a: <... completed>
+count          
+
+1              
+step s2a: <... completed>
+error in steps s3a s1a s2a: ERROR:  dimension slice 222 locked by other transaction
+step s3b: SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice);
+count          
+
+0              
diff --git a/test/isolation/specs/CMakeLists.txt b/test/isolation/specs/CMakeLists.txt
index 1c68db8ad96..1e5005ff658 100644
--- a/test/isolation/specs/CMakeLists.txt
+++ b/test/isolation/specs/CMakeLists.txt
@@ -15,7 +15,8 @@ set(TEST_TEMPLATES
 )
 
 set(TEST_TEMPLATES_DEBUG
- bgw_job_delete.spec.in
+  bgw_job_delete.spec.in
+  dropchunks_race.spec.in
 )
 
 if (CMAKE_BUILD_TYPE MATCHES Debug)
diff --git a/test/isolation/specs/dropchunks_race.spec.in b/test/isolation/specs/dropchunks_race.spec.in
new file mode 100644
index 00000000000..4bd000043bc
--- /dev/null
+++ b/test/isolation/specs/dropchunks_race.spec.in
@@ -0,0 +1,33 @@
+# This file and its contents are licensed under the Apache License 2.0.
+# Please see the included NOTICE for copyright information and
+# LICENSE-APACHE for a copy of the license.
+
+setup {
+  DROP TABLE IF EXISTS dropchunks_race_t1;
+  CREATE TABLE dropchunks_race_t1 (time timestamptz, device int, temp float);
+  SELECT create_hypertable('dropchunks_race_t1', 'time', 'device', 2);
+  INSERT INTO dropchunks_race_t1 VALUES ('2020-01-03 10:30', 1, 32.2);
+
+  CREATE FUNCTION debug_waitpoint_enable(TEXT) RETURNS VOID LANGUAGE C VOLATILE
+  AS '@TS_MODULE_PATHNAME@', 'ts_debug_waitpoint_enable';
+
+  CREATE FUNCTION debug_waitpoint_release(TEXT) RETURNS VOID LANGUAGE C VOLATILE
+  AS '@TS_MODULE_PATHNAME@', 'ts_debug_waitpoint_release';
+}
+
+teardown {
+  DROP TABLE dropchunks_race_t1;
+}
+
+session "s1"
+step "s1a"	{ SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); }
+
+session "s2"
+step "s2a"	{ SELECT COUNT(*) FROM drop_chunks('dropchunks_race_t1', TIMESTAMPTZ '2020-03-01'); }
+
+session "s3"
+setup           { SELECT debug_waitpoint_enable('drop_chunks_chunks_found'); }
+step "s3a"      { SELECT debug_waitpoint_release('drop_chunks_chunks_found'); }
+step "s3b" 	{ SELECT COUNT(*) FROM _timescaledb_catalog.chunk_constraint WHERE dimension_slice_id NOT IN (SELECT id FROM _timescaledb_catalog.dimension_slice); }
+
+permutation "s1a" "s2a" "s3a" "s3b"
