[Bug]: Segmentation Fault when VACUUM FULL pg_class 
Potential fix: https://github.com/timescale/timescaledb/pull/3875#issuecomment-993351397
@mfundul's potential fix seems to remove the crashes/errors.
The reproduction steps mentioned above are always able to reproduce the issue. However, we need to spend time to understand the actual sequence of events which is causing the bug to surface up. 

It also appears that it's somehow being triggered via parallel worker processes in the `cache_invalidate_callback` callback which seems to be doing a lot of cached catalog accesses WHILE the cache is being invalidated. This is, in general, non-standard behavior in cache clearing callbacks.

One option is to NOT to use the cache via `get_relname_relid` and `get_namespace_oid` calls but to directly use `systable_beginscan` and `systable_getnext` calls to get the required information. 


https://github.com/timescale/timescaledb/pull/3875 has a version of fix. But that is not what finally needs to be done. Linking here just in case.
I just lost a 26 TB database to this.
FTR: it would appear that `reindex system` on the affected database has recovered most, if not all, the corrupted data and therefore avoided my immediate dismissal for gross incompetence. For the time being at least.
We're actively looking into this issue.
Apologize, I accidentally close the issue.