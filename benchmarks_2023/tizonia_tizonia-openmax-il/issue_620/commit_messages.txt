player: added support for '--spotify-preferred-bitrate'
plugins/pcm_renderer_pa: added support for customized default volume in config file (#620)
plugins/pcm_renderer_pa/src/pulsearprc.c: the component's default volume needs
to be set on the port on construction (#620)
player: a more consistent approach to obtaining the default volume from the
component (fixes #620) (take II)
player: a more consistent approach to obtaining the default volume from the
component (fixes #620) (take II)
