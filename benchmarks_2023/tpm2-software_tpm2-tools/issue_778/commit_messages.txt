Update CHANGELOG.md
Limit selected PCRs to 8

When a PCR selection has more than 8 selections, the pcrSelections
array was being indexed past its end.

Fixes: #778

Signed-off-by: William Roberts <william.c.roberts@intel.com>
Limit selected PCRs to 8

When a PCR selection has more than 8 selections, the pcrSelections
array was being indexed past its end.

Fixes: #778

Signed-off-by: William Roberts <william.c.roberts@intel.com>
Limit selected PCRs to 8

When a PCR selection has more than 8 selections, the pcrSelections
array was being indexed past its end.

Fixes: #778

Signed-off-by: William Roberts <william.c.roberts@intel.com>
Limit selected PCRs to 8

When a PCR selection has more than 8 selections, the pcrSelections
array was being indexed past its end.

Fixes: #778

Signed-off-by: William Roberts <william.c.roberts@intel.com>
Limit selected PCRs to 8

When a PCR selection has more than 8 selections, the pcrSelections
array was being indexed past its end.

Fixes: #778

Signed-off-by: William Roberts <william.c.roberts@intel.com>
