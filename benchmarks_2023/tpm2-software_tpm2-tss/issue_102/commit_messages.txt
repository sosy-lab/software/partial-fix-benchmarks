Merge pull request #99 from flihp/mvheaders

build: Move all distributed headers into a common directory.
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tcti.so -> libtcti.so
Minor changes:
- Attempt to get some consistency in our use of either "" or <> form of
  the include directive. For the headers that have moved we're using the
  <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers currently so more work will be required
  to clean this up completely and that will be tracked as part of issue
  #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either "" or <> form of
  the include directive. For the headers that have moved we're using the
  <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers currently so more work will be required
  to clean this up completely and that will be tracked as part of issue
  #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either "" or <> form of
  the include directive. For the headers that have moved we're using the
  <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers currently so more work will be required
  to clean this up completely and that will be tracked as part of issue
  #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either "" or <> form of
  the include directive. For the headers that have moved we're using the
  <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers currently so more work will be required
  to clean this up completely and that will be tracked as part of issue
  #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either "" or <> form of
  the include directive. For the headers that have moved we're using the
  <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers currently so more work will be required
  to clean this up completely and that will be tracked as part of issue
  #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either the "" or <> form
  of the include directive. For the headers that have moved we're using
  the <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers, so more work will be required to clean
  this up completely and that will be tracked as part of issue #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either the "" or <> form
  of the include directive. For the headers that have moved we're using
  the <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers, so more work will be required to clean
  this up completely and that will be tracked as part of issue #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
tcti: Use consistent naming structure for tcti headers.

This naming structure is copied from the tss2 headers:
tss2/tss2_*.h -> tcti/tcti_*.h

This resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
tcti: Use consistent naming structure for tcti headers.

This naming structure is copied from the tss2 headers:
tss2/tss2_*.h -> tcti/tcti_*.h

This resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
Big change to better align with spec for exposed headers and structures.

Top level changes:
- Rename the tpm2sapi and tpm2tcti include directory to the preferred tss2
  and tcti respectively.
- Move headers that aren't part of the spec out of the installed system
  directory and remove the tss2_ prefix from them. They're back in the
  sapi include directory. There are still a few files that aren't part of
  the spec (tpm20.h) but we're including these for convenience for the
  time being. This is still very much subject to change but future changes
  will have a much lower impact.
- Rename the libraries produced:
  libtpm2sapi.so -> libtss2.so
  libtpm2tctidev.so -> libtctidev.so
  libtpm2tctisock.so -> libtctisock.so
Minor changes:
- Attempt to get some consistency in our use of either the "" or <> form
  of the include directive. For the headers that have moved we're using
  the <> form for system headers including those installed by this package.
  For headers that are internal to the project we're using the "" form.
- Cope with the removal of some 'extern' variables declared in
  previously public headers. I've only touched the cases that were
  required to move the headers, so more work will be required to clean
  this up completely and that will be tracked as part of issue #107.
- Fixup both autotools and visual studio build files.

Resolves #100
Resolves #102

Signed-off-by: Philip Tricca <philip.b.tricca@intel.com>
