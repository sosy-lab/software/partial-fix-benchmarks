dbus daemon not starting - v3.1-rc1
OK, I'll have a look at it as soon as possible. Thank you for reporting it!
This regression seems to be related to a follow-up fix to #81, in d7401b2, where I [removed auto-mounting of `/dev/shm`](https://github.com/troglobit/finit/commit/d7401b2a961d9fd9aeca72011394d5a00e0b2b50#diff-e1ecc5913887bfee7f4588d367bba913L381).

I'm trying out a variant of a revert of that part for you right now. Also, considering adding an `#ifdef HAVE_DBUS_PLUGIN` around it, what do you think?
The resulting code would look something like this, [@finit.c:L398](https://github.com/troglobit/finit/blob/master/src/finit.c#L398):

```C
	/*
	 * Some systems rely on us to both create /dev/shm and, to mount
	 * a tmpfs there.  Any system with dbus needs shared memory, so
	 * mount it, unless its already mounted, but not if listed in
	 * the /etc/fstab file already.
	 */
	makedir("/dev/shm", 0755);
#ifdef HAVE_DBUS_PLUGIN
	if (!fismnt("/dev/shm") && !ismnt("/etc/fstab", "/dev/shm"))
		mount("shm", "/dev/shm", "tmpfs", 0, NULL);
#endif
```
It would be nice to have this code, but I don't mind mounting /dev/shm manually. Thanks again for your effort!
I think I'll leave it as it was in 3.0, without the `#ifdef`, with only the new `!ismnt()` added. Should prevent double-mounting at least.
Mounted /dev/shm doesn't make any difference, the issue is still present. I'll do some test builds to see what has changed between 3.0 and 3.1-rc1.
Huh, OK. Sorry about that, hope you can zoom in on the problem ... (reopening issue)

Update: I'm guessing my last pushed fix (3258edc) doesn't help either?
so far finit works fine until the commit b0663d0  and I tested commit 2f9b97b but I got kernel panic, which has been fixed I guess.
commit f7c0366 did work without kernel panic but still dbus-daemon doesn't start, so the issue is between b0663d0 commit  and f7c0366. Maybe the new version of libite(2.0.1) is causing the issue.
Thanks!  Lots of major changes since d6b2ac9 though, and I've already noticed some commits don't even build cleanly, so this could be a bit of a challenge to figure out.  

I'll give 3.1-rc1 (and latest master) a try on one of my Debian machines later in the weekend. But since it's Christmas I may not be able to spend as much time on this as I'd like. Sorry!

Maybe you could try with `--debug` on the kernel cmdline, to enable Finit debug mode from boot.  Hopefully something around dbus will pop up in the log ...
Ah, I think I've found it ... the service(s) is removed when .conf files are reloaded (no difference between services loaded from /etc/finit.conf and /etc/finit.d/*.conf anymore) as of commit f3669c7 ... working on a fix.
Yup, that's it.  The fix works on my Debian Stretch system, should hopefully also work for you :-)

Yes it works indeed :) Thanks a lot and marry christmas!
That's great news, thank you for taking the time to test and report this!   
Hope you have a great Xmas :smiley: 