Safe to publish callback?
now in mqttConnectCb function, you can publish a message

This appears to be broken again with the latest version :(

All I've done is insert

MQTT_Publish(&mqttClient, "/test/topic", "Hello World 99", 14, 2, 0);

at the end of the mqttConnectedCb, and the message never gets sent.

ip:192.168.1.47,mask:255.255.255.0,gw:192.168.1.1
TCP: Connect to ip  192.168.1.147:1883
MQTT: Connected to broker 192.168.1.147:1883
MQTT: Send mqtt connection info, to broker 192.168.1.147:1883
TCP: Sent
TCP: data received
MQTT: Connected to 192.168.1.147:1883
MQTT: Connected
MQTT: subscribe, topic"/test/topic" at broker 192.168.1.147:1883
MQTT: subscribe, topic"/test2/topic" at broker 192.168.1.147:1883
TCP: Sent
TCP: data received
MQTT: Subscribe successful
TCP: Sent
TCP: data received
MQTT: Subscribe successful

MQTT: Send keepalive packet to 192.168.1.147:1883!
TCP: Sent
MQTT: Published
TCP: data received

Any news on this, my project is on hold until this is fixed.

Thanks.
