Fix undefined shift behaviour in opj_dwt_is_whole_tile_decoding(). Fixes https://bugs.chromium.org/p/oss-fuzz/issues/detail?id=3255. Credit to OSS Fuzz
opj_compress help: indicate 0 value, instead of 1, for -r parameter to get lossless encoding (#1009)
opj_compress help: revert 32572617765cb9d77302384653a48d793b8f657f and indicate 1 again as being the value to get lossless for -r. In opj_j2k_setup_encoder(), make sure that ll rates[] <= 1.0 are set to 0. Document 0 as being lossless for -q / tcp_distoratio (#1009)
opj_j2k_setup_encoder(): emit warnings if tcp_rates are not decreasing or tcp_distoratio are not increasing (#1009)
opj_tcd_rateallocate(): make sure to use all passes for a lossless layer (#1009)

And save a useless loop, which should be a tiny faster.
