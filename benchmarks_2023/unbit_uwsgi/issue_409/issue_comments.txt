%x (section) might not be set correctly
I realized this also holds for a few other magic variables that are set conditionally, as shown by this (somehat more complex) example. Note that the extension is actually determined from the full path instead of just the filename, which causes a problem when the path contains a . but the filename does not.

```
           $ cat tmp.ini
                   [uwsgi]
                   ini = tmp2.ini
                   ini = test.foo/bar
                   ini = tmp2.ini
                   ini = /root.ini
           $ cat tmp2.ini
                   [uwsgi]
                   zero = %0
                   one = %1
                   two = %2
           $ cat test.foo/bar
                   [uwsgi]
                   name = %n
                   extension = %e
           $ cat /root.ini
                   [uwsgi]
                   dir = %d
                   basename_dir = %c
           $ uwsgi --ini tmp.ini --show-config
                   [uWSGI] getting INI configuration from tmp.ini
                   [uWSGI] getting INI configuration from tmp2.ini
                   [uWSGI] getting INI configuration from test.foo/bar
                   [uWSGI] getting INI configuration from tmp2.ini
                   [uWSGI] getting INI configuration from /root.ini

                   ;uWSGI instance configuration
                   [uwsgi]
                   ini = tmp.ini
                   ini = tmp2.ini
                   zero = etc
                   one = uwsgi-emperor
                   two =
      These are ok ^^^^
                   ini = test.foo/bar
                   name = tmp2
This should be bar ^^^^
                   extension = foo/bar
   Should be empty ^^^^
                   ini = tmp2.ini
                   zero = etc
                   one = uwsgi-emperor
                   two = test.foo
   Should be empty ^^^^
                   ini = /root.ini
                   dir = /
                   basename_dir = uwsgi-emperor
   Should be empty ^^^^
                   show-config = true
                   ;end of configuration

                   ... more output ...
```

Here's a proposal for a fix (untested again):

```
--- a/core/uwsgi.c
+++ b/core/uwsgi.c
@@ -1060,6 +1060,8 @@ void config_magic_table_fill(char *filename, char **magic_table) {
                if (magic_table['c'][strlen(magic_table['c']) - 1] == '/') {
                        magic_table['c'][strlen(magic_table['c']) - 1] = 0;
                }
+       } else {
+               magic_table['c'] = "";
        }

        int base = '0';
@@ -1074,19 +1076,28 @@ void config_magic_table_fill(char *filename, char **magic_table) {
                        break;
                }
        }
+       while (base <= '9') {
+               magic_table[base] = "";
+       }

        if (tmp)
                *tmp = '/';

-       if (uwsgi_get_last_char(filename, '.'))
-               magic_table['e'] = uwsgi_get_last_char(filename, '.') + 1;
+       if (uwsgi_get_last_char(magic_table['s'], '.'))
+               magic_table['e'] = uwsgi_get_last_char(magic_table['s'], '.') + 1;
+       else
+               magic_table['e'] = "";
        if (uwsgi_get_last_char(magic_table['s'], '.'))
                magic_table['n'] = uwsgi_concat2n(magic_table['s'], uwsgi_get_last_char(magic_table['s'], '.') - magic_table['s'], "", 0);
+       else
+               magic_table['n'] = magic_table['s'];

 reuse:
        if (section) {
                magic_table['x'] = section+1;
                *section = ':';
+       } else {
+               magic_table['x'] = "wsgi";
        }
 }
```

The fix looks good, but still doesn't fix the "test.foo/bar" issue, and leaves `%n` empty when a file without an extension is used (and I'd say that it should just contain the filename, then).

Perhaps also include this part?

```
-       if (uwsgi_get_last_char(filename, '.'))
-               magic_table['e'] = uwsgi_get_last_char(filename, '.') + 1;
+       if (uwsgi_get_last_char(magic_table['s'], '.'))
+               magic_table['e'] = uwsgi_get_last_char(magic_table['s'], '.') + 1;

        if (uwsgi_get_last_char(magic_table['s'], '.'))
                magic_table['n'] = uwsgi_concat2n(magic_table['s'], uwsgi_get_last_char(magic_table['s'], '.') - magic_table['s'], "", 0);
+       else
+               magic_table['n'] = magic_table['s'];
```

Just tested 1.9.18.1 and things work as expected now. With the testcase from the first comment:

```
$ uwsgi --ini tmp.ini --show-config
    [uWSGI] getting INI configuration from tmp.ini
    [uWSGI] getting INI configuration from tmp2.ini
    [uWSGI] getting INI configuration from test.foo/bar
    [uWSGI] getting INI configuration from tmp2.ini
    [uWSGI] getting INI configuration from /root.ini

    ;uWSGI instance configuration
    [uwsgi]
    ini = tmp.ini
    ini = tmp2.ini
    zero = etc
    one = uwsgi-emperor
    two = 
    ini = test.foo/bar
    name = bar
    extension = 
    ini = tmp2.ini
    zero = etc
    one = uwsgi-emperor
    two = 
    ini = /root.ini
    dir = /
    basename_dir = 
    show-config = true
    ;end of configuration
```

Which is the correct output.

Thanks!
