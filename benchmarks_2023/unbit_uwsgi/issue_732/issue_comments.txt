Segfault with python traceback
We actually see this issue when printing traceback after harakiri.

Can you report your full uWSGI config ? Thanks a lot

```
[uwsgi]
socket=0.0.0.0:9000
chdir=/path/to/index
module=index
processes=16
enable-threads=true
thunder-lock=true
threads=5
master=true
harakiri=60
max-requests=20000
buffer-size=32768
log-syslog=uwsgi
py-tracebacker=/tmp/trace_
```

it looks like the tracebacker has not been ported to python3, i will push a fix in the next few minutes

this patch https://github.com/unbit/uwsgi/commit/8e3c127e0944d8d133eaba46fe9c7c9a23731a59 should address the issue and should fix a bug where the first thread is always marked as "unnamed"

If you confirm it works i will backport it to 2.0

Thanks a lot

Patched version of 2.0.7 is ok. Thanks so much for patch.

Pushed to 2.0.8
