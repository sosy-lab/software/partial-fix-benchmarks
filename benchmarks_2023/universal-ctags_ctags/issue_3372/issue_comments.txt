cxx: Memory leak for anonymous function parameters
I'm also getting this leak:
```
==27073== 144 (88 direct, 56 indirect) bytes in 1 blocks are definitely lost in loss record 14,839 of 17,610
==27073==    at 0x48490C0: malloc (vg_replace_malloc.c:381)
==27073==    by 0x4B13233: eMalloc (routines.c:220)
==27073==    by 0x4AB7433: createToken (cxx_token.c:30)
==27073==    by 0x4AEE40F: objPoolGet (objpool.c:66)
==27073==    by 0x4AB37EF: cxxParserParseNextToken (cxx_parser_tokenizer.c:1241)
==27073==    by 0x4AAE23B: cxxParserParseAndCondenseSubchainsUpToOneOf (cxx_parser.c:196)
==27073==    by 0x4AAE41F: cxxParserParseAndCondenseCurrentSubchain (cxx_parser.c:117)
==27073==    by 0x4AAFEE3: cxxParserParseBlockInternal (cxx_parser_block.c:738)
==27073==    by 0x4AAFEE3: cxxParserParseBlock (cxx_parser_block.c:798)
==27073==    by 0x4AB040F: cxxParserParseBlockHandleOpeningBracket (cxx_parser_block.c:167)
==27073==    by 0x4AAFEC7: cxxParserParseBlockInternal (cxx_parser_block.c:717)
==27073==    by 0x4AAFEC7: cxxParserParseBlock (cxx_parser_block.c:798)
==27073==    by 0x4AAE1C7: cxxParserMain (cxx_parser.c:1886)
==27073==    by 0x4B04D77: createTagsForFile (parse.c:3737)
==27073==    by 0x4B04D77: createTagsWithFallback1 (parse.c:3857)
```
but in this case I don't really know what's going on.
What kind of input did you give to ctags?
The first one is not reproduced with the input `int foo(int *);`.

```
[jet@living]~/var/ctags-github% cat foo.c                                         
void foo(int *);
[jet@living]~/var/ctags-github%  valgrind ./ctags --kinds-C='*' -o - foo.c        
==102964== Memcheck, a memory error detector
==102964== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==102964== Using Valgrind-3.18.1 and LibVEX; rerun with -h for copyright info
==102964== Command: ./ctags --kinds-C=* -o - foo.c
==102964== 
__anon0f73801a010d	foo.c	/^void foo(int *);$/;"	z	prototype:foo	typeref:typename:int *	file:
foo	foo.c	/^void foo(int *);$/;"	p	typeref:typename:void	file:
==102964== 
==102964== HEAP SUMMARY:
==102964==     in use at exit: 18,398 bytes in 665 blocks
==102964==   total heap usage: 9,569 allocs, 8,904 frees, 518,762 bytes allocated
==102964== 
==102964== LEAK SUMMARY:
==102964==    definitely lost: 0 bytes in 0 blocks
==102964==    indirectly lost: 0 bytes in 0 blocks
==102964==      possibly lost: 0 bytes in 0 blocks
==102964==    still reachable: 18,398 bytes in 665 blocks
==102964==         suppressed: 0 bytes in 0 blocks
==102964== Rerun with --leak-check=full to see details of leaked memory
==102964== 
==102964== For lists of detected and suppressed errors, rerun with: -s
==102964== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```
> What kind of input did you give to ctags?

That's a good question - basically all Geany sources, GTK, glib and all of our unit tests. But I haven't explored in detail what exactly is causing it yet.
The `cxxTokenCreateAnonymousIdentifier ` leak seems to be triggered by this piece of code
```
G_DEFINE_TYPE_WITH_CODE (GDummyProxyResolver, g_dummy_proxy_resolver, G_TYPE_OBJECT,
			 G_IMPLEMENT_INTERFACE (G_TYPE_PROXY_RESOLVER,
						g_dummy_proxy_resolver_iface_init)
			 _g_io_modules_ensure_extension_points_registered ();
			 g_io_extension_point_implement (G_PROXY_RESOLVER_EXTENSION_POINT_NAME,
							 g_define_type_id,
							 "dummy",
							 -100))

static void
g_dummy_proxy_resolver_finalize (GObject *object)
{
  /* must chain up */
  G_OBJECT_CLASS (g_dummy_proxy_resolver_parent_class)->finalize (object);
}
```
from
https://gitlab.gnome.org/GNOME/glib/-/blob/main/gio/gdummyproxyresolver.c
@techee, thank you.
I shrank the input you showed:
```
f(a,){
```

I'm also getting this
```
==12381== 112 (88 direct, 24 indirect) bytes in 1 blocks are definitely lost in loss record 523 of 540
==12381==    at 0x48490C0: malloc (vg_replace_malloc.c:381)
==12381==    by 0x211FC3: eMalloc (routines.c:220)
==12381==    by 0x16555B: createToken (cxx_token.c:30)
==12381==    by 0x225DF7: objPoolGet (objpool.c:66)
==12381==    by 0x161917: cxxParserParseNextToken (cxx_parser_tokenizer.c:1241)
==12381==    by 0x15C3A7: cxxParserParseAndCondenseSubchainsUpToOneOf (cxx_parser.c:305)
==12381==    by 0x15C3A7: cxxParserParseAndCondenseSubchainsUpToOneOf (cxx_parser.c:187)
==12381==    by 0x15C4F7: cxxParserParseAndCondenseCurrentSubchain (cxx_parser.c:117)
==12381==    by 0x15C37F: cxxParserParseAndCondenseSubchainsUpToOneOf (cxx_parser.c:256)
==12381==    by 0x15C37F: cxxParserParseAndCondenseSubchainsUpToOneOf (cxx_parser.c:187)
==12381==    by 0x15C4F7: cxxParserParseAndCondenseCurrentSubchain (cxx_parser.c:117)
==12381==    by 0x15E00B: cxxParserParseBlockInternal (cxx_parser_block.c:738)
==12381==    by 0x15E00B: cxxParserParseBlock (cxx_parser_block.c:798)
==12381==    by 0x15C29F: cxxParserMain (cxx_parser.c:1914)
==12381==    by 0x13F61F: createTagsForFile (parse.c:3759)
==12381==    by 0x13F61F: createTagsWithFallback1 (parse.c:3907)
```
for
```
G_DEFINE_TYPE_WITH_CODE (GBufferedInputStream,
			 g_buffered_input_stream,
			 G_TYPE_FILTER_INPUT_STREAM,
                         G_ADD_PRIVATE (GBufferedInputStream)
			 G_IMPLEMENT_INTERFACE (G_TYPE_SEEKABLE,
						g_buffered_input_stream_seekable_iface_init))

static void
g_buffered_input_stream_class_init (GBufferedInputStreamClass *klass)
{
}
```
but the code looks very similar and is probably the same leak.
Reproduced even with ctags + https://github.com/universal-ctags/ctags/pull/3373.

I tried the input shorter: `G (f () g ()) h (void) {`.
I made a new pull request for fixing this issue.
About the example input shown in the initial comment, #3375 fixes the memory leak.
So I will merge #3375.
However, #3375 doesn't fix the memory leaks about the 2nd example input (https://github.com/universal-ctags/ctags/issues/3372#issuecomment-1114689980) and its shortened version `g (a () b c)) {`. The memory leak is reproduced even if `--extras=-{anonymous}` is given.
So I will keep this open.
When I tried reproducing the leak today on Fedora36, I couldn't.

```
$ cat /tmp/foo.c              
G_DEFINE_TYPE_WITH_CODE (GBufferedInputStream,
			 g_buffered_input_stream,
			 G_TYPE_FILTER_INPUT_STREAM,
                         G_ADD_PRIVATE (GBufferedInputStream)
			 G_IMPLEMENT_INTERFACE (G_TYPE_SEEKABLE,
						g_buffered_input_stream_seekable_iface_init))

static void
g_buffered_input_stream_class_init (GBufferedInputStreamClass *klass)
{
}
$ ./ctags --version
Universal Ctags 5.9.0(cbdc5c4b6), Copyright (C) 2015-2022 Universal Ctags Team
Universal Ctags is derived from Exuberant Ctags.
Exuberant Ctags 5.8, Copyright (C) 1996-2009 Darren Hiebert
  Compiled: Jun 20 2022, 00:48:48
  URL: https://ctags.io/
  Optional compiled features: +wildcards, +regex, +iconv, +option-directory, +xpath, +json, +interactive, +sandbox, +yaml, +packcc, +optscript, +pcre2
$ valgrind --leak-check=full ./ctags /tmp/foo.c
==2184768== Memcheck, a memory error detector
==2184768== Copyright (C) 2002-2022, and GNU GPL'd, by Julian Seward et al.
==2184768== Using Valgrind-3.19.0 and LibVEX; rerun with -h for copyright info
==2184768== Command: ./ctags /tmp/foo.c
==2184768== 
==2184768== 
==2184768== HEAP SUMMARY:
==2184768==     in use at exit: 18,398 bytes in 665 blocks
==2184768==   total heap usage: 9,634 allocs, 8,969 frees, 526,786 bytes allocated
==2184768== 
==2184768== LEAK SUMMARY:
==2184768==    definitely lost: 0 bytes in 0 blocks
==2184768==    indirectly lost: 0 bytes in 0 blocks
==2184768==      possibly lost: 0 bytes in 0 blocks
==2184768==    still reachable: 18,398 bytes in 665 blocks
==2184768==         suppressed: 0 bytes in 0 blocks
==2184768== Reachable blocks (those to which a pointer was found) are not shown.
==2184768== To see them, rerun with: --leak-check=full --show-leak-kinds=all
==2184768== 
==2184768== For lists of detected and suppressed errors, rerun with: -s
==2184768== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
```
Can't reproduce it on Debian 11 either (previously I used Debian 10 but with the latest valgrind). Instead, I'm getting
```
==8354== Conditional jump or move depends on uninitialised value(s)
==8354==    at 0x15BC98: directiveHash (cpreprocessor.c:1153)
==8354==    by 0x15BC98: handleDirective (cpreprocessor.c:1208)
==8354==    by 0x15BC98: cppGetc (cpreprocessor.c:1871)
==8354==    by 0x162843: cxxParserSkipToNonWhiteSpace (cxx_parser_tokenizer.c:32)
==8354==    by 0x162843: cxxParserParseNextToken (cxx_parser_tokenizer.c:1248)
==8354==    by 0x15D85F: cxxParserParseAndCondenseSubchainsUpToOneOf (cxx_parser.c:196)
==8354==    by 0x15D85F: cxxParserParseUpToOneOf (cxx_parser.c:333)
==8354==    by 0x15D85F: cxxParserParseEnum (cxx_parser.c:898)
==8354==    by 0x15F367: cxxParserParseBlockInternal (cxx_parser_block.c:395)
==8354==    by 0x15F63F: cxxParserParseBlock (cxx_parser_block.c:798)
==8354==    by 0x15CDDB: cxxParserMain (cxx_parser.c:1922)
==8354==    by 0x13E71F: createTagsForFile (parse.c:3759)
==8354==    by 0x13E71F: createTagsWithFallback1 (parse.c:3907)
==8354==    by 0x13EA0B: createTagsWithFallback (parse.c:3998)
==8354==    by 0x13EA0B: parseMio (parse.c:4160)
==8354==    by 0x13EB53: parseFileWithMio (parse.c:4207)
==8354==    by 0x12FCC3: createTagsForEntry (main.c:221)
==8354==    by 0x12FB1F: recurseUsingOpendir (main.c:115)
==8354==    by 0x12FB1F: recurseIntoDirectory (main.c:184)
==8354==    by 0x12FC9B: createTagsForEntry (main.c:215)
```

but that seems to be like a false positive because I don't see anything wrong with the code. So maybe this issue can be closed.
Thank you for trying.

What kind of input did you give to ctags for making valgrind report the "Conditional jump or move depends on uninitialised value(s)" ?
It was
```
G_SLICE=always-malloc G_DEBUG=gc-friendly:resident-modules valgrind --tool=memcheck --leak-check=full --leak-resolution=high --log-file=vgdump --suppressions=/usr/share/glib-2.0/valgrind/glib.supp ctags --kinds-C='*' -R ~/Downloads/glib-2.70.2
```
with valgrind 3.16.1.

On Debian 10 for which I reported this issue originally it was valgrind 3.19.0 compiled form sources (the shipped valgrind version didn't work correctly on my ARM-based macbook with parallels desktop linux VM). Yeah, I'm making by best not to make bugs reported by me reproducible by anyone else ;-).
Thank you.
I'll close this one in this time.