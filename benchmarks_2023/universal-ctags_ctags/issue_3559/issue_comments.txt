readtags: improper handling of escape sequences in input field
Thank you for reporting.
I must re-read #3085.

I have not read tags(5) again.
However, as far as the area you reported here, I guess readtags works as explained in tags(5).

The comparisons between the way handling regular tags and pseudo tags are not fair.

``` console
$ cat tags
!_PTAG  value   /comment/;"
regular_tag     /path/to/file   /^pattern$/;"   kind:\\
$ readtags -Ee -l
regular_tag     /path/to/file   /^pattern$/;"   kind:\\
```
This focuses on a value slot of `{tagfield}`.

``` console
$ cat tags
!_PTAG  value\\ /comment/;"
regular_tag     /path/to/file   /^pattern$/;"   kind:\\
$ readtags -Ee -D
!_PTAG  value\\\\       /comment/
```

On the other hand, this focuses on `{tagfile}`.

About `{tagfield}` and `{tagfile}`, readtags prints regular tags and pseudo tags in the same way. The way of printing `{tagfield}` looks correct. The way of printing `{tagfile}` is questionable whether it is in a regular tag or a pseudo tag<1>.

What I found during inspecting this issue, I found a bug; about `{tagname}` name,
readtags prints regular tags and pseudo tags in different ways.
The way of printing `{tagname}` in a pseudo tag looks wrong<2>. I made a pull request #3560 for fixing this.

I extended and normalized your test cases.
See #3560 and https://github.com/universal-ctags/ctags/pull/3560/commits/f09376ce96428f7e64e71f9c9763dda05da6b961 .

About <1>, I will inspect it more. I have to read tags(5) more carefully.



I read tags(5) carefully.

`{tagname}` and `{tagfield}`, tags(5) explains the way for escaping.
About `{tagfile}`, it doesn't explain the way for escaping.
@AmaiKinono I would like you to confirm what I wrote above.

What I was surprised at is the implementation.

Just one common routine, printValue() is used for printing  `{tagname}`, and value slots of `{tagfield}`. This implies the difference in the way of escaping between `{tagname}` and `{tagfile}` comes from libreadtags.

> `{tagname}` and `{tagfield}`, tags(5) explains the way for escaping.
> About `{tagfile}`, it doesn't explain the way for escaping.
> @AmaiKinono I would like you to confirm what I wrote above.

Yes, you are right.

I guess `{tagfile}` is wrote literally. However, I think we do need some escaping rules, as in Unix, tabs and newlines are valid file name characters, and they can't appear in a tagline field.

Besides, the pattern

```
{tagname}<tab>{tagfile}<tab>{tagaddress}
```

are for regular tags. Formally we can say pseudo tags has the same pattern, but semantically pseudo tags are more like

```
!_{tagname}<tab>{value}<tab>{comment}
```

It feels weird for me to use escaping rules in regular tags directly to pseudo tags. But it's okay as long as

- The escaping rules are well defined
- It doesn't prevent us from encoding arbitrary information in pseudo tags, for future needs
The current implementation comes from https://ctags.sourceforge.net/FORMAT .
This is the master peace, our bible.

About 10 years ago, I'm looking for a tool to help me read and understand source code.
I wanted a low-level tool but higher than grep. I knew I didn't have enough ability and time to develop a new tool from scratch. When I read the FORMAT file, I was attracted not by ctags but by the file format. It was very different from TAGS that I was familiar with.

Writing a tool with a given spec is much easier than writing a tool with designing a new spec.
So we (ctags developers including me) have not wanted to extend more modify the FORMAT.
To support documentation languages like TeX and HTML, we extended the FORMAT to accept names including various characters. I can image document files using such characters.
This is the only exception.
Understandable. But I will not work on extending tags(5) for the reason of "feeling weird".

Basically, I'm negative about extending FORMAT. I agree with you about what you wrote in the comment. But I don't want to take my time to update the bible. Instead, I would like to take my time improving parsers, extending the main part like supporting reference tags, and hacking the real frontend, Citre. I would like to assume the FORMAT is a gift from haven. 


So tell me if ctags and readtags behavior violates what is written in tags(5). I will fix it.
If tags(5) has an ambitious sentence, let's add our annotation for people relying on tags(5).
u-ctags becomes popular. So our annotation may be useful to tags(5) community.

If you are interested in extending the FORMAT, you must drive the parts of development: adding and modifying sentences in tags(5), updating output-ctags.c, and updating libreadtags/readtags.c.

ctags + readtags + citre work well incredibly. But I have more ideas to improve the combination. I would like to go forward.

I'm thinking about releasing version 6.0.0. If this issue reported ctags and/or libreadtags violate tags(5), please, explain it again. I would like to fix it before releasing 6.0.0.
If ctags and/or libreadtags have a critical bug that makes citre not work. I will fix it. However, I cannot say the schedule but I always assign my time to citre-related issues rather high priority.


.. I wrote too much. What I would like to say is, in simple words, "if this is not a bug, can I remove "next-release" label from this issue?
I didn't expect a long reply like this. I love the story ;)

I think it's totally fine to make pseudo tags using the same pattern and same escaping rules as regular tags. It would be benefit to state this explicitly in tags(5).

The other problem of tags(5) is it only defines how the "value" part in a `{tagfield}` is escaped. `{tagname}`, `{tagfile}` and `{tagaddress}` are left. Though, in "Proposal" section, `{tagname}` is mentioned:

> EXCEPTION:  Universal  Ctags violates this item of the proposal; name may contain spaces.
> However, tabs are not allowed.  Conversion, for  some  characters
> including  <Tab>  in the "value", explained in the last of this section is applied.

For `{tagaddress}`, we know it well in ctags-client-tools(7), in "Parse Readtags Output" section.

Simple experiment shows that escaping are used in `{tagfile}`:

``` console
$ ls
'my-strange\'$'\t\n''-dir'
(The directory name is "my-strange\	⏎-dir", ⏎ is a newline char)
$ ctags -R
$ cat tags
... pseudo tags ...
hello	my-strange\\\t\n-dir/test.el	/^(defun hello ())$/;"	f
$ readtags -Ene -l
hello	my-strange\\\\\\t\\n-dir/test.el	/^(defun hello ())$/;"	kind:f
```

It seems backslashes, tabs and newlines in `{tagfile}` are escaped in a tags file. I expect readtags prints them literally like it does with `{tagfield}`. This should be a bug of readtags.
> It seems backslashes, tabs and newlines in {tagfile} are escaped in a tags file. I expect readtags prints them literally like it does with {tagfield}. This should be a bug of readtags.

The real issue becomes clear to me.
Can I ask you to update the title of this issue?
> Can I ask you to update the title of this issue?

Done.
Thank you. I will inspect this again.
The following change fixes this issue.

```
diff --git a/extra-cmds/printtags.c b/extra-cmds/printtags.c
index fa49b079e..a844cb7e4 100644
--- a/extra-cmds/printtags.c
+++ b/extra-cmds/printtags.c
@@ -142,8 +142,7 @@ static void tagsPrintTag (const tagEntry *entry,
                                        print_str, print_char, outfp);
 
        print_char ('\t', outfp);
-       printValue  (entry->file, printingWithEscaping,
-                                print_str, print_char, outfp);
+       print_str (entry->file, outfp);
        print_char ('\t', outfp);
        print_str (entry->address.pattern, outfp);

```

After considering this fix and #3560, It seems that I was confused with name fields and input fields.


As you considered, `!_TAG_PROC_CWD` doesn't work for input including \n and \t.
It seems that this is an issue of ctags side.
tags(5) says nothing about `{tagfile}`.
readtags just reads the field and prints it literally whether `-E` is given or not.

Values stored as `{tagfile}` and printed as `{tagsfile}` are up to an implementation of ctags.
Psuedo tags values are passed from ctags to client tools literally. ctags and the client tools must agree on the way of evaluating the pseudo tags.
It seems that I went a wrong way.
Let me reconsider about `{tagfile}`.
Making an input file having \t in its name:
```
$ echo 'int main(void) {return 0;}' > "$(printf '\t'.c)"
```

Tagging with Exuberant ctags"
```
$ e-ctags -o tab-e-ctags.tags  "$(printf '\t'.c)"
```

The tags file becomes broken; the \t in the file name is printed as is:
```
$ cat tab-e-ctags.tags 
!_TAG_FILE_FORMAT	2	/extended format; --format=1 will not append ;" to lines/
!_TAG_PROGRAM_NAME	Exuberant Ctags	//
!_TAG_PROGRAM_VERSION	Development	//
main		.c	/^int main(void) {return 0;}$/;"	f
```

Tagging with Universal ctags"
```
$ u-ctags -o tab-u-ctags.tags  "$(printf '\t'.c)"
```

The tags file is not broken; the \t in the file name is quoted:
```
$ cat tab-u-ctags.tags
...
!_TAG_FILE_FORMAT	2	/extended format; --format=1 will not append ;" to lines/
!_TAG_FILE_SORTED	1	/0=unsorted, 1=sorted, 2=foldcase/
...
!_TAG_OUTPUT_EXCMD	mixed	/number, pattern, mixed, or combineV2/
!_TAG_OUTPUT_FILESEP	slash	/slash or backslash/
!_TAG_OUTPUT_MODE	u-ctags	/u-ctags or e-ctags/
!_TAG_OUTPUT_VERSION	0.0	/current.age/
!_TAG_PARSER_VERSION!C	0.0	/current.age/
!_TAG_PATTERN_LENGTH_LIMIT	96	/0 for no limit/
!_TAG_PROC_CWD	/home/jet/var/ctags-github/	//
!_TAG_PROGRAM_AUTHOR	Universal Ctags Team	//
!_TAG_PROGRAM_NAME	Universal Ctags	/Derived from Exuberant Ctags/
!_TAG_PROGRAM_URL	https://ctags.io/	/official site/
!_TAG_PROGRAM_VERSION	5.9.0	/fbfb18a92/
...
main	\t.c	/^int main(void) {return 0;}$/;"	f	typeref:typename:int
```

Tagging with Universal ctags with Exuberant Ctags output mode:
```
$ ./ctags --output-format=e-ctags  -o tab-u+e-ctags.tags  "$(printf '\t'.c)"
```

Universal ctags doesn't print the tag entry for "main" because the tags file becomes broken if printing in Exuberant ctags mode:
```
$ cat tab-u+e-ctags.tags
...
!_TAG_FILE_FORMAT	2	/extended format; --format=1 will not append ;" to lines/
...
$ 
```

Making an escape sequence for input file names is the u-ctags' extension.
This must be written in tags(5). No change is needed in u-ctags program itself.

libreadtags, a library used in readtags command for reading tags files, doesn't consider
the escape sequences. This behavior of the library must be fixed.
If `!_TAG_OUTPUT_MODE` is "u-ctags", the library should consider the way to handle the escape sequences when reading the input fields.

The printing routine of readtags command was implemented correctly.
It considered the escape sequences when -E is given.
However, I introduced a bug in #3578. It must be reverted.

Summary:
- [x] we must add an "exception" to tags(5) document.
- [x] we must add a feature to support the escape sequences in libreadtags.
- [x] we must revert the change of readtags command about printing.
I got a question: how the field should be on windows?
`C:\newfile.c`. Users on Windows don't expect the file name is recorded as `C:\\newfile.c`.
On u-ctags replaces `\` in a file with `/`.  So ctags is not involved with the question.

libreadtags is involved. If `\n` is found in `{tagfile}`, what kind of string should be built on its buffer?

I cannot find a consistent solution quickly.
I would like to work on this topic again after releasing 6.0.
The following rule may be consistent.

No change is needed in ctags side.
If !_TAG_OUTPUT_MODE is "u-ctags", libreadtags should do the unescaping.
If !_TAG_OUTPUT_MODE is "e-ctags", libreadtags should not do the unescaping.
If !_TAG_OUTPUT_MODE is "u-ctags" and      -E is     passed, readtags should print entries with escaping.
If !_TAG_OUTPUT_MODE is "u-ctags" and      -E is not passed, readtags should print entries as are.
If !_TAG_OUTPUT_MODE is "e-ctags" and even -E is     passed, readtags should print entries as are.
If !_TAG_OUTPUT_MODE is "e-ctags" and      -E is not passed, readtags should print entries as are.

tags(5) must explain the format of `!_TAG_OUTPUT_MODE == u-ctags` as COMMENT.
The comments in readtags.h of libreadtags must explain the behavior when `!_TAG_OUTPUT_MODE == u-ctags` is set.
readtags(1) must explain the command behavior when `!_TAG_OUTPUT_MODE == u-ctags`.
ctags-client-tools(7) must explain `!_TAG_OUTPUT_MODE`.


---

!_TAG_OUTPUT_FILESEP must be considered.
