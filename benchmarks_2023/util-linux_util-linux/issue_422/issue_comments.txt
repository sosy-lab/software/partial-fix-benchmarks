`col` adds not-existing newline
Is it expected behavior that col(1) requires \n at the end of the line? If yes, we need to document it.
```
$ echo -ne 'AAA\n' | col
AAA
```
but
```
$ echo -ne 'AAA' | col
<nothing>
```
It seems that for example, https://github.com/dspinellis/unix-history-repo/blob/FreeBSD-release/11.1.0/usr.bin/col/col.c (and other FreeBSDs) do not use max_line in this way. 

It would be nice to check it. Maybe we can port something latest from some BSD.

 @kerolasa ?

It seems we can close it now. Thanks for your report!