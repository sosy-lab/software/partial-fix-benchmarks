Panic: Assert error in VBE_Delete()
I can't find the commit you mentioned, so more details about this custom branch could help. Especially the nature of your changes, possible VMODs in use, and what you are doing with backends.

I'm sorry, it seems that the commit I mentioned is now rehashed due to merge/rebase; here is the correct one: 0577f3fba200e45c05099427eec01610ee061436

VMOD in use: named, for dynamic backends using Amazon servers.  Also, low TTL (3 seconds), and a larger workspace (96 KB) to avoid panics we had with #1834 (not yet allowed to lower it back to 64 KB). Our branch has 25 small-ish commits for byte-account billing, traffic shaping, API keys.

Let me know if you need more info.

Assigning to myself, this looks like a dynamic backend issue as I suspected :)

Sorry, incorrect issue in 11692e7. It concerns #1999.

@rnsanchez I'm currently suspecting a bug in [vmod-named](https://github.com/Dridi/libvmod-named) but you mention byte-accounting among your 25 commits and this is a panic related to backend counters.

So now I'm also suspecting a bug in your fork... Especially since the triggering assertion reported on line 249 in your panic message is on line 216 in the branched commit:

https://github.com/varnishcache/varnish-cache/blob/0577f3f/bin/varnishd/cache/cache_backend_cfg.c#L192-L219

@Dridi thanks for the pointer, I will investigate on our side and report back.

We simply duplicate some attributes from ReqAcct, for convenience.  What we did notice is that the problem is triggered when:
1. `vcl.load t1 /New.vcl`
2. `vcl.use t1`
3. `vcl.state t0 cold`

The only VMOD we use is libvmod-named.  We just deployed it without the last step, just in case this is race-related (setting it as cold too early). To simplify things further, we rebased our repo, so recent fixes are included as well.

It takes some time to trigger it (10~15k dynamic backends and then changing the VCL), maybe a couple days.  We'll report back soon with a backtrace (with symbols), as soon as it is reproduced.

Can you please confirm you upgraded to 4.1.3?

From the previous panic, it doesn't look like changing the temperature triggers the panic. It happens during the VCL and backends cleanup performed before every CLI commands. So any command sent via `varnishadm` could trigger it for instance and in your case the VCL isn't cold/cooling yet.

There must be a race somewhere between `vmod-named` deleting a backend when at the same time `VBE_Poll` is called by the CLI thread.

I've been looking hard a `vmod-named` and especially how backends may be deleted or not at discard time (with different expectations from Varnish) and couldn't find anything. So I had a look at the `VRT_backend_delete` function provided by Varnish and found a missing lock acquire when the backend is moved to the cool backends list.

Below is a simple patch that fixes the locking issues and checks other locking requirements, but it doesn't solve your problem.

``` diff
diff --git i/bin/varnishd/cache/cache_backend_cfg.c w/bin/varnishd/cache/cache_backend_cfg.c
index ad96330..06f5fe8 100644
--- i/bin/varnishd/cache/cache_backend_cfg.c
+++ w/bin/varnishd/cache/cache_backend_cfg.c
@@ -157,9 +157,11 @@ VRT_delete_backend(VRT_CTX, struct director **dp)
        be->admin_health = vbe_ah_deleted;
        be->health_changed = VTIM_real();
        be->cooled = VTIM_real() + 60.;
+       Lck_Unlock(&be->mtx);
+       Lck_Lock(&backends_mtx);
        VTAILQ_REMOVE(&backends, be, list);
        VTAILQ_INSERT_TAIL(&cool_backends, be, list);
-       Lck_Unlock(&be->mtx);
+       Lck_Unlock(&backends_mtx);

        // NB. The backend is still usable for the ongoing transactions,
        // this is why we don't bust the director's magic number.
```

The `VBE_Poll` function _guarantees_ a `COLD` event on a warm backend before calling `VBE_Delete`. And when a VCL cools down, all its backends invariably get a `COLD` event. All of that happens on the CLI thread, sequentially, so no locking is needed.

A newly created backend on the other hand can be created outside of the CLI thread and reads the unguarded VCL temperature to then emit a `WARM` event. I think we have our race, now the question is how to close it. At first glance it could be covered by a lock in `cache_backend_cfg.c`, only used in this file.

Thanks @Dridi, I'm passing this info along.  We're trying to reliably trigger the panic, in the hopes of confirming an eventual correction.

Second glance, the VCL's lock could fit too.

After one more stare at the code I came to the conclusion that we need a rwlock in `struct vcl`. The `temp` field is only ever written in the CLI thread, but it is read during a backend creation as explained above. And an event may be fired depending on unguarded reads.

Moreover, the `temp` field is used in assertions outside of the CLI thread, even more dangerous unguarded reads. I need to verify the correctness of four [assertions](https://github.com/varnishcache/varnish-cache/blob/8322836/bin/varnishd/cache/cache_vcl.c#L163) on [temperature](https://github.com/varnishcache/varnish-cache/blob/8322836/bin/varnishd/cache/cache_vcl.c#L180) that look [incomplete](https://github.com/varnishcache/varnish-cache/blob/8322836/bin/varnishd/cache/cache_vcl.c#L193) or [temporary](https://github.com/varnishcache/varnish-cache/blob/8322836/bin/varnishd/cache/cache_vcl.c#L424).

I'm concerned about the potential size of critical sections for write locks given the [current shape](https://github.com/varnishcache/varnish-cache/blob/8322836/bin/varnishd/cache/cache_vcl.c#L548-L587) of temperature code. Although it might be just fine because it's only one (leaf) branch at a time, and further staring-at-the-code may show nothing scary.

I'll be working on this when I come back from holidays, I'm dumping some notes here to help my future self.

Off-topic note to future self: there is probably room for a [condvar](https://github.com/varnishcache/varnish-cache/blob/8322836/bin/varnishd/cache/cache_vcl.c#L159-L160) on `vcl_mtx`.

@Dridi we're in the process of updating our branches to include d398402.  It takes a while to trigger the panic (usually more than 10k backends).

Thanks for the confirmation, I will follow the other lead documented above anyway. Except that the master branch now includes support for VCL labels and VCL switching (not sure we have a fancy name for the latter) so fixing this for 5.0 and 4.1 will take twice as long once I get to it.

@Dridi: we have more info: rebasing our local branch to match 4.1 (02df97a710d0b3c1814d2300d23f3d64d8699d2a) and cherry-picking your fix (d398402537e0d9e80720a79ce078e8ba6962d13d), the panic still happens.

When the third element in vcl.list reaches zero, that's where the panic usually happens.  I was told that varnishadm felt frozen for quite a while when it happened.  Also, that the uptime did not increase until the panic completely flushed out.  It was not related to load, as the machine had barely any traffic.

Here is a minimal VCL used for testing:

```
probe probe_D__B_147223793380_s3 {
    .request = 
        "HEAD / HTTP/1.1"
        "Host: testbucket.s3.amazonaws.com"
        "Connection: close";
    .initial  = 0;
    .interval = 3s;
    .timeout  = 2m;
    .window   = 2;
    .expected_response = 200;
    .threshold = 1;
}

sub vcl_init {
    # Director: named director
    new D___B_147223793380_s3 = named.director(
        port = "80",
        usage_timeout = 300s,
        first_lookup_timeout = 2s,
        probe = probe_D__B_147223793380_s3,
        ttl = 60s
    );
}

sub vcl_recv {
    set req.backend_hint = D___B_147223793380_s3.backend("testbucket.s3.amazonaws.com");
}
```

Commands used for the test:

```
varnish> vcl.list
200        
active     auto/warm         20 p2

varnish> vcl.load p3 /tmp/singular/0826-1472237933/0826-1472237933.vcl 
200        
VCL compiled.

varnish> vcl.use p3
200        
VCL 'p3' now active
varnish> vcl.discard p2
200        

varnish> vcl.list
200        
discarded  auto/busy         16 p2
active     auto/warm          8 p3
```

Panic:

```
panic.show 
200        
Last panic at: Fri, 26 Aug 2016 15:54:53 GMT
"Assert error in VBP_Remove(), cache/cache_backend_probe.c line 587:
  Condition(vt->heap_idx == BINHEAP_NOIDX) not true.
thread = (cache-main)
version = varnish-4.1.3 revision 92d62e5
ident = Linux,4.6.3,x86_64,-junix,-smalloc,-smalloc,-hcritbit,epoll
Backtrace:
  0x43e5ab: pan_backtrace+0x1d
  0x43e9b6: pan_ic+0x2a7
  0x416272: VBP_Remove+0x17c
  0x4139cd: VBE_Delete+0x82
  0x4145e4: VBE_Poll+0x86
  0x41cf0e: cli_cb_before+0x71
  0x7f6f654a9072: libvarnish.so(+0x8072) [0x7f6f654a9072]
  0x7f6f654a964e: libvarnish.so(+0x864e) [0x7f6f654a964e]
  0x7f6f654b05e8: libvarnish.so(+0xf5e8) [0x7f6f654b05e8]
  0x7f6f654b07af: libvarnish.so(VLU_Fd+0xe3) [0x7f6f654b07af]

"
```

Hello @rnsanchez, the last panic you reported is a new one. Please open a new issue if you get this panic again after fixing the first one.

For the first panic, please give a spin to #2068.

Reopening until I land a fix in the 4.1 branch too. I'd like to thank @rnsanchez for reporting and my past self for making his future easier.

@rnsanchez please test the current 4.1 branch and let me know whether it fixes your crash.

@Dridi: I've got confirmation that current 4.1 fixes the problem.  We are not able to reproduce it anymore.

Thank you!

Great, thanks for confirming.

Backport review: Fixed in 4.1 in 9ae3861.
