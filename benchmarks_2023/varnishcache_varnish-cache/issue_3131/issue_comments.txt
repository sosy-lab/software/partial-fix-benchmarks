Panic with partial WS reservation in vmod_proxy
as I cannot reopen the PR, I will reopen this bug until I am convinced that the fix is correct. At this point, I am not.
@ehocdet https://github.com/varnishcache/varnish-cache/commit/1d23a7335eda7741574e9b680401f5acc2a4d67d makes sense because reserving space which we are going to use in all cases makes no sense, `WS_Alloc()` is the better choice.
But the commit does not change anything about the `ws->r` assertion, so my prediction would be that you will see the issue also with that change.
I do not see the bug, but at this point I am certain that the root cause is elsewhere - some other (vmod?) function leaving the workspace reservation in place for an overflow situation.
You have omitted the vmod list form your panic string, can you please check the calls preceding the failing one?

What we are looking for is a most likely a failing `WS_ReserverAll()` for which the function returns without a `WS_Release()`.
I think this bug could be related to #3132., but...
What i can tell is, when I tried to find the bug, i spotted #3132.
quick IRC discussion: We agree that #3132 did not fix anything and I have asked @ehocdet to hunt down the actual bug elsewhere
@nigoroll It make sense, thank's. I will investigate more
With same binary, assert after WS_ReserveSize

```
Panic at: Thu, 14 Nov 2019 10:50:58 GMT
Assert error in ses_reserve_attr(), cache/cache_session.c line 116:
  Condition(o >= sz) not true.
version = varnish-6.3.1 revision NOGIT, vrt api = 10.0
ident = Linux,4.9.0-0.bpo.3-amd64,x86_64,-junix,-sfile,-sdefault,-hcritbit,epoll
now = 28060882.682663 (mono), 1573728658.530339 (real)
Backtrace:
  0x43d8fa: /usr/sbin/varnishd() [0x43d8fa]
  0x4a31c2: /usr/sbin/varnishd(VAS_Fail+0x42) [0x4a31c2]
  0x445336: /usr/sbin/varnishd() [0x445336]
  0x473b64: /usr/sbin/varnishd() [0x473b64]
  0x45dc9c: /usr/sbin/varnishd() [0x45dc9c]
  0x45e140: /usr/sbin/varnishd() [0x45e140]
  0x7f0ef113a064: /lib/x86_64-linux-gnu/libpthread.so.0(+0x8064) [0x7f0ef113a064]
  0x7f0ef0e6f62d: /lib/x86_64-linux-gnu/libc.so.6(clone+0x6d) [0x7f0ef0e6f62d]
thread = (cache-worker)
pthread.attr = {
  guard = 4096,
  stack_bottom = 0x7f0ef0cdf000,
  stack_top = 0x7f0ef0ced000,
  stack_size = 57344,
}
thr.req = (nil) {
},
thr.busyobj = (nil) {
},
vmods = {
  std = {0x7f0ef0500160, Varnish 6.3.1 NOGIT, 0.0},
  proxy = {0x7f0ef05001d0, Varnish 6.3.1 NOGIT, 0.0},
  geoip2 = {0x7f0ef05004e0, Varnish 6.3.1 NOGIT, 0.0},
  unidirectors = {0x7f0ef05007f0, Varnish 6.3.1 NOGIT, 0.0},
},

```
@ehocdet how do you mean "after WS_ReserveSize"?
it's about:
https://github.com/varnishcache/varnish-cache/blob/6e96ff048692235e64565211a38c41432a26c055/bin/varnishd/cache/cache_session.c#L115-L116
Yes, we do not handle allocation failures on the session workspace properly
bugwash: I will work on handling errors on the session workspace
@nigoroll @Dridi 
I think my first intuition was the right one, and #3132 really fix the bug:
- call WS_ReserveSize  with free workspace < 8 bytes will reserve 0 byte en return the reserved space (0).
https://github.com/varnishcache/varnish-cache/blob/6e96ff048692235e64565211a38c41432a26c055/bin/varnishd/cache/cache_ws.c#L261
- tlv_string return without WS_Release:
https://github.com/varnishcache/varnish-cache/blob/6e96ff048692235e64565211a38c41432a26c055/lib/libvmod_proxy/vmod_proxy.c#L108-L109
- Next call to WS_{Reserve,Alloc} will assert. Another call to tlv_string in my case.
https://github.com/varnishcache/varnish-cache/pull/3132#issuecomment-553840194
@ehocdet `WS_ReserveSize()` return without a reservation (before setting `ws->r`) when overflown:
https://github.com/varnishcache/varnish-cache/blob/6e96ff048692235e64565211a38c41432a26c055/bin/varnishd/cache/cache_ws.c#L267
I really cannot follow your argument.

@nigoroll 
b2 is set to 0: no overflow, and:
https://github.com/varnishcache/varnish-cache/blob/6e96ff048692235e64565211a38c41432a26c055/bin/varnishd/cache/cache_ws.c#L269

I thought I had already replied, but apparently I hadn't: Thank you @ehocdet, I see it now.
FTR: https://github.com/varnishcache/varnish-cache/issues/3131#issuecomment-553877629 actually is a separate issue which I will also address