[build] Fix that a dist failed for "make distcheck"
Fix: numbers not in the fixed indices shouldn't match to any indices

This closes #149 and closes #153.
The same fix also for symmetric functions

The patch of 6ce79f3fa7 should have been applied also to symmetr.c.

Closes #149.
