Added object file IDs to entries in [source files] and [addr-to-line mapping] in symbol files created with -S (and -A). Should fix GitHub issue #416.
[addr-to-line mapping] contains now RAM address as well as full ROM address. Should fix GitHub issue #417.
Tweaked [addr-to-line mapping], separated memory address from ROM offset and bank. Affects GitHub issue #417.
Added version number to WLA symbol files as the format was recently changed. We now write v2 WLA symbol files. Affects GitHub issue #417.
Added more information to doc/symbols.rst about WLA symbol file format changes. Affects GitHub issue #417.
[source files] -> [source files v2] and [addr-to-line mapping] -> [addr-to-line mapping v2]. Affects GitHub issue #417.
Updated documentation about [information] in WLA symbol files (v2+). Affects GitHub issue #417.
Update symbols.rst to include version history

Fix for discussion https://github.com/vhelin/wla-dx/issues/417#issuecomment-847331286_
