Added built-in function random(min, max). Should fix GitHub issue #532.
Fixed INSTANCEOF in .ENUM when .STRUCT used SIZE (WLA doesn't generate bytes in that case any more). This hopefully fixes GitHub issue #536.
Fixed INSTANCEOF in .STRUCT when the INSTANCEOF .STRUCT used SIZE (no more defining bytes in .STRUCT). Might fix GitHub issue #536.
