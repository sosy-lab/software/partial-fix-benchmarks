.ENUM generates ROM data when referenced .STRUCT has a given SIZE
Oof, sounds like I might have misunderstood something (or forgotten something) when implementing that SIZE... I'll try to fix this tomorrow evening.
I hope the latest sources solve this issue, and nothing was broken. The .ENUM code is a bit complex IMHO...
Oh, wow! I'll try right away, and report back!
It works flawlessly now.
You are a programming wizard, Ville! :D
Thanks a lot!
> You are a programming wizard, Ville! :D

Thanks, but I just spend too much time coding -> practice, practice, practice... :)

And I make a lot of bugs while coding! Fortunately my manual tests help me quite a lot...
I spoke too soon (sorry), there's still an issue remaining with nested `.STRUCT`s.

If you add

```
.STRUCT testnest
	nest1 INSTANCEOF teststruct
.ENDST
```
around line 39 in the new https://github.com/vhelin/wla-dx/blob/master/tests/65816/base_test_3/main.s file and make, you'll see in the verbose info that it generates 6 more bytes of ROM data.
Oof, that's the same issue, but in .STRUCT! Good testing! I'll fix that asap!
How about now? :) I said I make bugs... :D
Yay, now we're good! Thank you so much, Ville! :D
No, thank you for reporting this bug. If you hadn't reported it now, it wouldn't have gotten fixed today...
Haha, true. And no false alarm this time, for a change! XD

BTW, you might want to clean up the /tests/65816/base_test_3/main.s file again (I merely chose this one for reproducing the issue because it was small, and used 64K ROM banks, like my game project).
Done, I moved the .STRUCT tests to struct_tests...