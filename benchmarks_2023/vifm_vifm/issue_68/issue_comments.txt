Unable to quit dialog window when current remote folder has been deleted
I couldn't reproduce it. Actually the code path for FUSE mounts and regular directories is almost the same except for additional checks in case of FUSE mounts. I guess sshfs is the one to blame, here is why. I saw different checks in two places: where dialog was shown and directory accessibility was checked. Both are quite close to each other, it must be that sshfs reported something wrong (one system call returns "path exists", the other one says the opposite. Now checks should agree with each other.

Thanks!

Workaround:
It is possible to quit the "dialog window loop": after pressing Return, quickly try to navigate out of the deleted folder. Once outside the deleted folder, the dialog window disappears.

So the issue can stay closed if you want.

Does not seem to be fixed: sometimes it seems fixed (dialog window never appears, sometimes a "Reading directory..." message appears on the status bar). However, from what I have seen, once the dialog window appears, it is still in an infinite loop.

The first time you have to wait a few seconds for the dialog window to come up. Sshfs mount options: <user>@<host>:/ -p <port> -o follow_symlinks

How do you remove the directory? I tried unmounting it and removing directory, so it's too commands. Trying to move it without unmounting first gives me "access denied, still in use" message.

I mount the sshfs "connection" by visiting an .ssh file in vifm with the ssh options mentioned above. I never unmount the sshfs "connection".

From the command line (in an ssh connection to the remote host):

Create an empty folder (creating more folders and files is not necessary, I think):

```
$ mkdir ~/1
```

or

```
$ mkdir -p ~/1/2; touch ~/1/2/3
```

Then in vifm navigate to the deepest folder (1 or 2).

Remove everything from the command line:

```
$ rm -rf ~/1
```

Then go back to vifm and wait a few seconds.

So I misunderstood the issue, I thought that you remove `/tmp/vifm_FUSE/001_something.ssh` directory after connection is broken in some way.

I've also compared wrong checks in code, that's why first fix didn't work. Please give it another try as I still can't reproduce the issue (maybe remote file system matters or something else).

By the way, you might be interested in using `'slowfs'` option if you don't know of it. Initially I forgot that I have `fuse.sshfs` listed there, which disables directory existence checks among other optimizations (there isn't many, but it does make difference when response time is high).

Now it is fixed and there is now also no need to enable 'slowfs' for this. Thanks.

Great. Thanks.
