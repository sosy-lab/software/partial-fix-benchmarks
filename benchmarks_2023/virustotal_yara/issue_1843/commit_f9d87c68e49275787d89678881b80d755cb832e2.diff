diff --git a/docs/modules/string.rst b/docs/modules/string.rst
index 2a2424b28c..9f7151bba8 100644
--- a/docs/modules/string.rst
+++ b/docs/modules/string.rst
@@ -28,7 +28,7 @@ strings section of your rule.
     .. versionadded:: 4.3.0
 
     Convert the given string, interpreted with the given base, to a signed
-    integer. Base must be 0 or between 2 and 32 inclusive. If it is zero then
+    integer. Base must be 0 or between 2 and 36 inclusive. If it is zero then
     the string will be intrepreted as base 16 if it starts with "0x" or as base
     8 if it starts with "0". Leading '+' or '-' is also supported.
 
diff --git a/libyara/modules/string/string.c b/libyara/modules/string/string.c
index c6f6b17eb2..2fc909b7cf 100644
--- a/libyara/modules/string/string.c
+++ b/libyara/modules/string/string.c
@@ -36,19 +36,55 @@ SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 #define MODULE_NAME string
 
+bool string_to_int(char* s, int base, int64_t* result)
+{
+  char* endp = s;
+
+  errno = 0;
+  *result = strtoll(s, &endp, base);
+
+  if (errno != 0) {
+    // Error while parsing the string.
+    return false;
+  }
+  if (endp == s) {
+    // No digits were found.
+    return false;
+  }
+  if (*endp != '\0') {
+    // Parsing did not reach the end of the string.
+    return false;
+  }
+
+  return true;
+}
+
 define_function(to_int)
 {
   char* s = string_argument(1);
-  int64_t result = strtoll(s, NULL, 0);
-  return_integer(result == 0 && errno ? YR_UNDEFINED : result);
+  int64_t result = 0;
+
+  if (string_to_int(s, 0, &result)) {
+      return_integer(result);
+  } else {
+      return_integer(YR_UNDEFINED);
+  }
 }
 
 define_function(to_int_base)
 {
   char* s = string_argument(1);
   int64_t base = integer_argument(2);
-  int64_t result = strtoll(s, NULL, base);
-  return_integer(result == 0 && errno ? YR_UNDEFINED : result);
+  int64_t result = 0;
+
+  if (!(base == 0 || (base >= 2 && base <= 36))) {
+      return_integer(YR_UNDEFINED);
+  }
+  if (string_to_int(s, base, &result)) {
+      return_integer(result);
+  } else {
+      return_integer(YR_UNDEFINED);
+  }
 }
 
 define_function(string_length)
diff --git a/tests/test-string.c b/tests/test-string.c
index c729c8faeb..61b65d3087 100644
--- a/tests/test-string.c
+++ b/tests/test-string.c
@@ -55,15 +55,6 @@ int main(int argc, char** argv)
       }",
       NULL);
 
-  // Strings that are only partially converted are still fine.
-  assert_true_rule(
-      "import \"string\" \
-      rule test { \
-        condition: \
-          string.to_int(\"10A20\") == 10 \
-      }",
-      NULL);
-
   assert_true_rule(
       "import \"string\" \
       rule test { \
@@ -85,6 +76,56 @@ int main(int argc, char** argv)
       }",
       NULL);
 
+  // Test undefined cases
+
+  // on invalid base value
+  assert_true_rule(
+      "import \"string\" \
+      rule test { \
+        condition: \
+          not defined string.to_int(\"1\", -1) and \
+          not defined string.to_int(\"1\", 1) and \
+          not defined string.to_int(\"1\", 37) \
+      }",
+      NULL);
+
+  // on underflow or underflow
+  assert_true_rule(
+      "import \"string\" \
+      rule test { \
+        condition: \
+          not defined string.to_int(\"9223372036854775808\") \
+      }",
+      NULL);
+  assert_true_rule(
+      "import \"string\" \
+      rule test { \
+        condition: \
+          not defined string.to_int(\"-9223372036854775809\") \
+      }",
+      NULL);
+
+  // if parsing does not use all the string
+  assert_true_rule(
+      "import \"string\" \
+      rule test { \
+        condition: \
+          not defined string.to_int(\"FOO\") and \
+          not defined string.to_int(\"10A20\") \
+      }",
+      NULL);
+
+  // if parsing does not consume any digits
+  assert_true_rule(
+      "import \"string\" \
+      rule test { \
+        condition: \
+          not defined string.to_int(\"\") and \
+          not defined string.to_int(\"   -\") and \
+          not defined string.to_int(\" +0x\") \
+      }",
+      NULL);
+
   assert_true_rule(
       "import \"string\" \
       rule test { \
