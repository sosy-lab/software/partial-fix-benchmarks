diff --git a/src/btree.c b/src/btree.c
index e82f3bfd..c576fdd4 100644
--- a/src/btree.c
+++ b/src/btree.c
@@ -2585,7 +2585,8 @@ btree_iterator_print(iterator *itor)
    btree_print_node(Platform_default_log_handle,
                     btree_itor->cc,
                     btree_itor->cfg,
-                    &btree_itor->curr);
+                    &btree_itor->curr,
+                    btree_itor->page_type);
 }
 
 const static iterator_ops btree_iterator_ops = {
@@ -3174,9 +3175,11 @@ static void
 btree_print_index_node(platform_log_handle *log_handle,
                        btree_config        *cfg,
                        uint64               addr,
-                       btree_hdr           *hdr)
+                       btree_hdr           *hdr,
+                       page_type            type)
 {
-   platform_log(log_handle, "**  INDEX NODE \n");
+   platform_log(
+      log_handle, "**  Page type: %s, INDEX NODE \n", page_type_str[type]);
    platform_log(log_handle, "**  Header ptr: %p\n", hdr);
    platform_log(log_handle, "**  addr: %lu \n", addr);
    platform_log(log_handle, "**  next_addr: %lu \n", hdr->next_addr);
@@ -3217,9 +3220,11 @@ static void
 btree_print_leaf_node(platform_log_handle *log_handle,
                       btree_config        *cfg,
                       uint64               addr,
-                      btree_hdr           *hdr)
+                      btree_hdr           *hdr,
+                      page_type            type)
 {
-   platform_log(log_handle, "**  LEAF NODE \n");
+   platform_log(
+      log_handle, "**  Page type: %s, LEAF NODE \n", page_type_str[type]);
    platform_log(log_handle, "**  hdrptr: %p\n", hdr);
    platform_log(log_handle, "**  addr: %lu \n", addr);
    platform_log(log_handle, "**  next_addr: %lu \n", hdr->next_addr);
@@ -3255,14 +3260,15 @@ void
 btree_print_locked_node(platform_log_handle *log_handle,
                         btree_config        *cfg,
                         uint64               addr,
-                        btree_hdr           *hdr)
+                        btree_hdr           *hdr,
+                        page_type            type)
 {
    platform_log(log_handle, "*******************\n");
    platform_log(log_handle, "BTree node at addr=%lu\n{\n", addr);
    if (btree_height(hdr) > 0) {
-      btree_print_index_node(log_handle, cfg, addr, hdr);
+      btree_print_index_node(log_handle, cfg, addr, hdr, type);
    } else {
-      btree_print_leaf_node(log_handle, cfg, addr, hdr);
+      btree_print_leaf_node(log_handle, cfg, addr, hdr, type);
    }
    platform_log(log_handle, "} -- End BTree node at addr=%lu\n", addr);
 }
@@ -3272,7 +3278,8 @@ void
 btree_print_node(platform_log_handle *log_handle,
                  cache               *cc,
                  btree_config        *cfg,
-                 btree_node          *node)
+                 btree_node          *node,
+                 page_type            type)
 {
    if (!allocator_page_valid(cache_get_allocator(cc), node->addr)) {
       platform_log(log_handle, "*******************\n");
@@ -3281,8 +3288,8 @@ btree_print_node(platform_log_handle *log_handle,
       platform_log(log_handle, "-------------------\n");
       return;
    }
-   btree_node_get(cc, cfg, node, PAGE_TYPE_BRANCH);
-   btree_print_locked_node(log_handle, cfg, node->addr, node->hdr);
+   btree_node_get(cc, cfg, node, type);
+   btree_print_locked_node(log_handle, cfg, node->addr, node->hdr, type);
    btree_node_unget(cc, cfg, node);
 }
 
@@ -3290,24 +3297,31 @@ void
 btree_print_subtree(platform_log_handle *log_handle,
                     cache               *cc,
                     btree_config        *cfg,
-                    uint64               addr)
+                    uint64               addr,
+                    page_type            type)
 {
    btree_node node;
    node.addr = addr;
-   btree_print_node(log_handle, cc, cfg, &node);
    if (!allocator_page_valid(cache_get_allocator(cc), node.addr)) {
-      platform_log(log_handle, "Unallocated BTree node addr=%lu\n", addr);
+      platform_log(log_handle,
+                   "Unallocated %s BTree node addr=%lu\n",
+                   page_type_str[type],
+                   addr);
       return;
    }
-   btree_node_get(cc, cfg, &node, PAGE_TYPE_BRANCH);
+   // Print node's contents only if it's a validly allocated node.
+   btree_print_node(log_handle, cc, cfg, &node, type);
+
+   btree_node_get(cc, cfg, &node, type);
    table_index idx;
 
    if (node.hdr->height > 0) {
       int nentries = node.hdr->num_entries;
       platform_log(log_handle,
-                   "\n---- BTree sub-trees under addr=%lu"
+                   "\n---- Page type: %s, BTree sub-trees under addr=%lu"
                    " num_entries=%d"
                    ", height=%d {\n",
+                   page_type_str[type],
                    addr,
                    nentries,
                    node.hdr->height);
@@ -3315,8 +3329,11 @@ btree_print_subtree(platform_log_handle *log_handle,
       for (idx = 0; idx < nentries; idx++) {
          platform_log(
             log_handle, "\n-- Sub-tree index=%d of %d\n", idx, nentries);
-         btree_print_subtree(
-            log_handle, cc, cfg, btree_get_child_addr(cfg, node.hdr, idx));
+         btree_print_subtree(log_handle,
+                             cc,
+                             cfg,
+                             btree_get_child_addr(cfg, node.hdr, idx),
+                             type);
       }
       platform_log(log_handle,
                    "\n} -- End BTree sub-trees under"
@@ -3326,18 +3343,31 @@ btree_print_subtree(platform_log_handle *log_handle,
    btree_node_unget(cc, cfg, &node);
 }
 
+/*
+ * Driver routine to print a Memtable BTree starting from root_addr.
+ */
+void
+btree_print_memtable_tree(platform_log_handle *log_handle,
+                          cache               *cc,
+                          btree_config        *cfg,
+                          uint64               root_addr)
+{
+   btree_print_subtree(log_handle, cc, cfg, root_addr, PAGE_TYPE_MEMTABLE);
+}
+
 /*
  * btree_print_tree()
  *
- * Driver routine to print a BTree starting from root_addr.
+ * Driver routine to print a BTree of page-type 'type', starting from root_addr.
  */
 void
 btree_print_tree(platform_log_handle *log_handle,
                  cache               *cc,
                  btree_config        *cfg,
-                 uint64               root_addr)
+                 uint64               root_addr,
+                 page_type            type)
 {
-   btree_print_subtree(log_handle, cc, cfg, root_addr);
+   btree_print_subtree(log_handle, cc, cfg, root_addr, type);
 }
 
 void
@@ -3423,7 +3453,7 @@ btree_verify_node(cache        *cc,
             {
                btree_node_unget(cc, cfg, &child);
                btree_node_unget(cc, cfg, &node);
-               btree_print_tree(Platform_error_log_handle, cc, cfg, addr);
+               btree_print_tree(Platform_error_log_handle, cc, cfg, addr, type);
                platform_error_log("out of order pivots\n");
                platform_error_log("addr: %lu idx %u\n", node.addr, idx);
                goto out;
@@ -3460,9 +3490,9 @@ btree_verify_node(cache        *cc,
                platform_error_log("addr: %lu idx %u\n", node.addr, idx);
                platform_error_log("child addr: %lu idx %u\n", child.addr, idx);
                btree_print_locked_node(
-                  Platform_error_log_handle, cfg, node.addr, node.hdr);
+                  Platform_error_log_handle, cfg, node.addr, node.hdr, type);
                btree_print_locked_node(
-                  Platform_error_log_handle, cfg, child.addr, child.hdr);
+                  Platform_error_log_handle, cfg, child.addr, child.hdr, type);
                platform_assert(0);
                btree_node_unget(cc, cfg, &child);
                btree_node_unget(cc, cfg, &node);
@@ -3482,9 +3512,9 @@ btree_verify_node(cache        *cc,
                platform_error_log("addr: %lu idx %u\n", node.addr, idx);
                platform_error_log("child addr: %lu idx %u\n", child.addr, idx);
                btree_print_locked_node(
-                  Platform_error_log_handle, cfg, node.addr, node.hdr);
+                  Platform_error_log_handle, cfg, node.addr, node.hdr, type);
                btree_print_locked_node(
-                  Platform_error_log_handle, cfg, child.addr, child.hdr);
+                  Platform_error_log_handle, cfg, child.addr, child.hdr, type);
                platform_assert(0);
                btree_node_unget(cc, cfg, &child);
                btree_node_unget(cc, cfg, &node);
@@ -3525,7 +3555,7 @@ btree_print_lookup(cache        *cc,        // IN
    int64      child_idx;
 
    node.addr = root_addr;
-   btree_print_node(Platform_default_log_handle, cc, cfg, &node);
+   btree_print_node(Platform_default_log_handle, cc, cfg, &node, type);
    btree_node_get(cc, cfg, &node, type);
 
    for (h = node.hdr->height; h > 0; h--) {
@@ -3535,7 +3565,7 @@ btree_print_lookup(cache        *cc,        // IN
          child_idx = 0;
       }
       child_node.addr = btree_get_child_addr(cfg, node.hdr, child_idx);
-      btree_print_node(Platform_default_log_handle, cc, cfg, &child_node);
+      btree_print_node(Platform_default_log_handle, cc, cfg, &child_node, type);
       btree_node_get(cc, cfg, &child_node, type);
       btree_node_unget(cc, cfg, &node);
       node = child_node;
diff --git a/src/btree.h b/src/btree.h
index ec99d3f8..50e28276 100644
--- a/src/btree.h
+++ b/src/btree.h
@@ -380,23 +380,32 @@ btree_rough_count(cache        *cc,
                   key           min_key,
                   key           max_key);
 
+void
+btree_print_memtable_tree(platform_log_handle *log_handle,
+                          cache               *cc,
+                          btree_config        *cfg,
+                          uint64               addr);
+
 void
 btree_print_tree(platform_log_handle *log_handle,
                  cache               *cc,
                  btree_config        *cfg,
-                 uint64               addr);
+                 uint64               addr,
+                 page_type            type);
 
 void
 btree_print_locked_node(platform_log_handle *log_handle,
                         btree_config        *cfg,
                         uint64               addr,
-                        btree_hdr           *hdr);
+                        btree_hdr           *hdr,
+                        page_type            type);
 
 void
 btree_print_node(platform_log_handle *log_handle,
                  cache               *cc,
                  btree_config        *cfg,
-                 btree_node          *node);
+                 btree_node          *node,
+                 page_type            type);
 
 void
 btree_print_tree_stats(platform_log_handle *log_handle,
diff --git a/src/data_internal.h b/src/data_internal.h
index e0425dce..630e1e01 100644
--- a/src/data_internal.h
+++ b/src/data_internal.h
@@ -604,7 +604,7 @@ static inline void
 data_key_to_string(const data_config *cfg, key k, char *str, size_t size)
 {
    if (key_is_negative_infinity(k)) {
-      snprintf(str, size, "(negaitive_infinity)");
+      snprintf(str, size, "(negative_infinity)");
    } else if (key_is_negative_infinity(k)) {
       snprintf(str, size, "(positive_infinity)");
    } else {
diff --git a/src/memtable.h b/src/memtable.h
index bb5e1aaa..aa6fdb89 100644
--- a/src/memtable.h
+++ b/src/memtable.h
@@ -286,11 +286,11 @@ memtable_verify(cache *cc, memtable *mt)
 static inline void
 memtable_print(platform_log_handle *log_handle, cache *cc, memtable *mt)
 {
-   btree_print_tree(log_handle, cc, mt->cfg, mt->root_addr);
+   btree_print_memtable_tree(log_handle, cc, mt->cfg, mt->root_addr);
 }
 
 static inline void
 memtable_print_stats(platform_log_handle *log_handle, cache *cc, memtable *mt)
 {
    btree_print_tree_stats(log_handle, cc, mt->cfg, mt->root_addr);
-};
+}
diff --git a/src/trunk.c b/src/trunk.c
index 233e4804..5d383c6a 100644
--- a/src/trunk.c
+++ b/src/trunk.c
@@ -7920,6 +7920,12 @@ trunk_print_locked_node(platform_log_handle *log_handle,
                         trunk_node          *node)
 {
    uint16 height = trunk_height(node);
+
+   platform_log(log_handle,
+                "\nPage type: %s, Node addr=%lu\n{\n",
+                page_type_str[PAGE_TYPE_TRUNK],
+                node->addr);
+
    // clang-format off
    platform_log(log_handle, "----------------------------------------------------------------------------------------------------\n");
    platform_log(log_handle, "|          |     addr     |   next addr  | height |   gen   | pvt gen |                            |\n");
@@ -7939,6 +7945,9 @@ trunk_print_locked_node(platform_log_handle *log_handle,
    platform_log(log_handle, "}\n");
 }
 
+// We print leading n-bytes of pivot's key, given by this define.
+#define PIVOT_KEY_PREFIX_LEN 24
+
 /*
  * trunk_print_pivots() -- Print pivot array information.
  */
@@ -7962,8 +7971,9 @@ trunk_print_pivots(platform_log_handle *log_handle,
       trunk_pivot_data *pdata = trunk_get_pivot_data(spl, node, pivot_no);
       if (pivot_no == trunk_num_pivot_keys(spl, node) - 1) {
          platform_log(log_handle,
-                      "| %.*s | %12s | %12s | %11s | %9s | %5s | %5s |\n",
-                      24,
+                      "| %*.*s | %12s | %12s | %11s | %9s | %5s | %5s |\n",
+                      PIVOT_KEY_PREFIX_LEN,
+                      PIVOT_KEY_PREFIX_LEN,
                       key_string(spl->cfg.data_cfg, pivot),
                       "",
                       "",
@@ -7972,16 +7982,18 @@ trunk_print_pivots(platform_log_handle *log_handle,
                       "",
                       "");
       } else {
-         platform_log(log_handle,
-                      "| %.*s | %12lu | %12lu | %11lu | %9lu | %5ld | %5lu |\n",
-                      24,
-                      key_string(spl->cfg.data_cfg, pivot),
-                      pdata->addr,
-                      pdata->filter.addr,
-                      pdata->num_tuples_whole + pdata->num_tuples_bundle,
-                      pdata->num_kv_bytes_whole + pdata->num_kv_bytes_bundle,
-                      pdata->srq_idx,
-                      pdata->generation);
+         platform_log(
+            log_handle,
+            "| %*.*s | %12lu | %12lu | %11lu | %9lu | %5ld | %5lu |\n",
+            PIVOT_KEY_PREFIX_LEN,
+            PIVOT_KEY_PREFIX_LEN,
+            key_string(spl->cfg.data_cfg, pivot),
+            pdata->addr,
+            pdata->filter.addr,
+            pdata->num_tuples_whole + pdata->num_tuples_bundle,
+            pdata->num_kv_bytes_whole + pdata->num_kv_bytes_bundle,
+            pdata->srq_idx,
+            pdata->generation);
       }
       if (key_is_user_key(pivot)) {
          platform_log(log_handle, "| Full key: ");
@@ -8152,7 +8164,7 @@ trunk_print_memtable(platform_log_handle *log_handle, trunk_handle *spl)
    platform_log(log_handle, "&&&&&&&&&&&&&&&&&&&\n");
    platform_log(log_handle, "&&  MEMTABLES \n");
    platform_log(log_handle, "&&  curr: %lu\n", curr_memtable);
-   platform_log(log_handle, "-------------------\n");
+   platform_log(log_handle, "-------------------\n{\n");
 
    uint64 mt_gen_start = memtable_generation(spl->mt_ctxt);
    uint64 mt_gen_end   = memtable_generation_retired(spl->mt_ctxt);
@@ -8160,14 +8172,14 @@ trunk_print_memtable(platform_log_handle *log_handle, trunk_handle *spl)
       memtable *mt = trunk_get_memtable(spl, mt_gen);
       platform_log(log_handle,
                    "Memtable root_addr=%lu: gen %lu ref_count %u state %d\n",
-                   mt_gen,
                    mt->root_addr,
+                   mt_gen,
                    allocator_get_refcount(spl->al, mt->root_addr),
                    mt->state);
 
       memtable_print(log_handle, spl->cc, mt);
    }
-   platform_log(log_handle, "\n");
+   platform_log(log_handle, "\n}\n");
 }
 
 /*
@@ -8821,11 +8833,16 @@ trunk_node_print_branches(trunk_handle *spl, uint64 addr, void *arg)
    platform_log(
       log_handle,
       "------------------------------------------------------------------\n");
-   platform_log(
-      log_handle, "| node %12lu height %u\n", addr, trunk_height(&node));
+   platform_log(log_handle,
+                "| Page type: %s, Node addr=%lu height=%u next_addr=%lu\n",
+                page_type_str[PAGE_TYPE_TRUNK],
+                addr,
+                trunk_height(&node),
+                trunk_next_addr(&node));
    platform_log(
       log_handle,
       "------------------------------------------------------------------\n");
+
    uint16 num_pivot_keys = trunk_num_pivot_keys(spl, &node);
    platform_log(log_handle, "| pivots:\n");
    for (uint16 pivot_no = 0; pivot_no < num_pivot_keys; pivot_no++) {
diff --git a/tests/functional/btree_test.c b/tests/functional/btree_test.c
index 6c5abd7c..22b51cf0 100644
--- a/tests/functional/btree_test.c
+++ b/tests/functional/btree_test.c
@@ -277,7 +277,7 @@ test_btree_perf(cache             *cc,
    memtable_print_stats(Platform_default_log_handle, cc, &ctxt->mt_ctxt->mt[0]);
    // for (i = 0; i < num_trees; i++) {
    //   if (!btree_verify_tree(cc, cfg, root_addr[i]))
-   //      btree_print_tree(cc, cfg, root_addr[i]);
+   //      btree_print_tree(cc, cfg, root_addr[i], PAGE_TYPE_BRANCH);
    //}
 
    // for (i = 0; i < num_threads; i++) {
@@ -436,8 +436,11 @@ btree_test_run_pending(cache                   *cc,
             break;
          case async_success:
             if (local_found ^ expected_found) {
-               btree_print_tree(
-                  Platform_default_log_handle, cc, cfg, root_addr);
+               btree_print_tree(Platform_default_log_handle,
+                                cc,
+                                cfg,
+                                root_addr,
+                                PAGE_TYPE_BRANCH);
                char key_string[128];
                data_key_to_string(cfg->data_cfg,
                                   key_buffer_key(&ctxt->keybuf),
@@ -699,8 +702,11 @@ test_btree_basic(cache             *cc,
                               key_buffer_key(&keybuf),
                               merge_accumulator_to_message(&expected_data));
          if (!correct) {
-            btree_print_tree(
-               Platform_default_log_handle, cc, btree_cfg, packed_root_addr);
+            btree_print_tree(Platform_default_log_handle,
+                             cc,
+                             btree_cfg,
+                             packed_root_addr,
+                             PAGE_TYPE_BRANCH);
             char key_string[128];
             key  target = key_buffer_key(&keybuf);
             btree_key_to_string(btree_cfg, target, key_string);
@@ -722,8 +728,11 @@ test_btree_basic(cache             *cc,
                                                           &correct);
          if (res == async_success) {
             if (!correct) {
-               btree_print_tree(
-                  Platform_default_log_handle, cc, btree_cfg, packed_root_addr);
+               btree_print_tree(Platform_default_log_handle,
+                                cc,
+                                btree_cfg,
+                                packed_root_addr,
+                                PAGE_TYPE_BRANCH);
                char key_string[128];
                key  target = key_buffer_key(&async_ctxt->keybuf);
                btree_key_to_string(btree_cfg, target, key_string);
@@ -754,8 +763,11 @@ test_btree_basic(cache             *cc,
                                        key_buffer_key(&keybuf),
                                        NULL_MESSAGE);
       if (!correct) {
-         btree_print_tree(
-            Platform_default_log_handle, cc, btree_cfg, packed_root_addr);
+         btree_print_tree(Platform_default_log_handle,
+                          cc,
+                          btree_cfg,
+                          packed_root_addr,
+                          PAGE_TYPE_BRANCH);
          char key_string[128];
          key  target = key_buffer_key(&keybuf);
          btree_key_to_string(btree_cfg, target, key_string);
@@ -860,7 +872,11 @@ test_count_tuples_in_range(cache        *cc,
    *count = 0;
    for (i = 0; i < num_trees; i++) {
       if (!btree_verify_tree(cc, cfg, root_addr[i], type)) {
-         btree_print_tree(Platform_default_log_handle, cc, cfg, root_addr[i]);
+         btree_print_tree(Platform_default_log_handle,
+                          cc,
+                          cfg,
+                          root_addr[i],
+                          PAGE_TYPE_BRANCH);
          platform_assert(0);
       }
       btree_iterator_init(
@@ -878,8 +894,11 @@ test_count_tuples_in_range(cache        *cc,
             char last_key_str[128], key_str[128];
             data_key_to_string(cfg->data_cfg, last_key, last_key_str, 128);
             data_key_to_string(cfg->data_cfg, curr_key, key_str, 128);
-            btree_print_tree(
-               Platform_default_log_handle, cc, cfg, root_addr[i]);
+            btree_print_tree(Platform_default_log_handle,
+                             cc,
+                             cfg,
+                             root_addr[i],
+                             PAGE_TYPE_BRANCH);
             platform_default_log(
                "test_count_tuples_in_range: key out of order\n");
             platform_default_log("last %s\nkey %s\n", last_key_str, key_str);
@@ -890,8 +909,11 @@ test_count_tuples_in_range(cache        *cc,
             data_key_to_string(cfg->data_cfg, low_key, low_key_str, 128);
             data_key_to_string(cfg->data_cfg, curr_key, key_str, 128);
             data_key_to_string(cfg->data_cfg, high_key, high_key_str, 128);
-            btree_print_tree(
-               Platform_default_log_handle, cc, cfg, root_addr[i]);
+            btree_print_tree(Platform_default_log_handle,
+                             cc,
+                             cfg,
+                             root_addr[i],
+                             PAGE_TYPE_BRANCH);
             platform_default_log(
                "test_count_tuples_in_range: key out of range\n");
             platform_default_log(
@@ -905,8 +927,11 @@ test_count_tuples_in_range(cache        *cc,
             data_key_to_string(cfg->data_cfg, low_key, low_key_str, 128);
             data_key_to_string(cfg->data_cfg, curr_key, key_str, 128);
             data_key_to_string(cfg->data_cfg, high_key, high_key_str, 128);
-            btree_print_tree(
-               Platform_default_log_handle, cc, cfg, root_addr[i]);
+            btree_print_tree(Platform_default_log_handle,
+                             cc,
+                             cfg,
+                             root_addr[i],
+                             PAGE_TYPE_BRANCH);
             platform_default_log(
                "test_count_tuples_in_range: key out of range\n");
             platform_default_log(
diff --git a/tests/unit/btree_stress_test.c b/tests/unit/btree_stress_test.c
index ed0f4238..8ff32248 100644
--- a/tests/unit/btree_stress_test.c
+++ b/tests/unit/btree_stress_test.c
@@ -262,7 +262,8 @@ CTEST2(btree_stress, test_random_inserts_concurrent)
    btree_print_tree(Platform_default_log_handle,
                     (cache *)&data->cc,
                     &data->dbtree_cfg,
-                    packed_root_addr);
+                    packed_root_addr,
+                    PAGE_TYPE_BRANCH);
 
    set_log_streams_for_tests(MSG_LEVEL_INFO);
 
diff --git a/tests/unit/btree_test.c b/tests/unit/btree_test.c
index 88dd814b..63f2156c 100644
--- a/tests/unit/btree_test.c
+++ b/tests/unit/btree_test.c
@@ -430,14 +430,16 @@ leaf_split_tests(btree_config    *cfg,
       bool success = btree_leaf_incorporate_tuple(
          cfg, hid, hdr, tuple_key, bigger_msg, &spec, &generation);
       if (success) {
-         btree_print_locked_node(Platform_error_log_handle, cfg, 0, hdr);
+         btree_print_locked_node(
+            Platform_error_log_handle, cfg, 0, hdr, PAGE_TYPE_MEMTABLE);
          ASSERT_FALSE(success,
                       "Weird.  An incorporate that was supposed to fail "
                       "actually succeeded (nkvs=%d, realnkvs=%d, i=%d).\n",
                       nkvs,
                       realnkvs,
                       i);
-         btree_print_locked_node(Platform_error_log_handle, cfg, 0, hdr);
+         btree_print_locked_node(
+            Platform_error_log_handle, cfg, 0, hdr, PAGE_TYPE_MEMTABLE);
          ASSERT_FALSE(success);
       }
       leaf_splitting_plan plan =
