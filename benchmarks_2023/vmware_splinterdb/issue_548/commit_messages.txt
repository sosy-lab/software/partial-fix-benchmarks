Identify Memtable v/s Branch page types via BTree-print routines.

This commit extends BTree-print routines to also report the page
type, whether it's a branch or a memtable BTree. As the structures and
print methods are shared between two objects, this extra information
will help in diagnostics. Trunk nodes are likewise identified.
Extend btree_print_tree() to receive page_type arg.
Minor fix in trunk_print_pivots() to align outputs for pivot key's string.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.
(#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.
(#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.

--
(#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.

-- (#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Use _Bool for boolean fields in external config struct.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across dot-oh's.) This commit redefines these fields with _Bool
type, that is part of standard C. This should reduce the risk of such
incompatibilities.

Cleaned-up few instances around use of bool type:

 - Minor adjustment to routing_filter_is_value_found() returning bool.
 - Stray references to use of 0/1 for boolean values with FALSE/TRUE.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.

-- (#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.

-- (#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Change typedef of bool from int32 to 1-byte 'unsigned char'.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across objects. This commit slightly adjusts our definition to
default to 1-byte 'unsigned char', which should reduce the risk of such
incompatibilities.

Found only one instance where 'bool' return value from
routing_filter_is_value_found() had to be adjusted to fit
1-byte return value. Otherwise, function would incorrectly return
FALSE, causing filter_test to fail.

-- (#548) Fix few more stray references to 0/1 properly. Code tightening.

This commit cleans up some stray references to use of 0/1 for boolean
variables w/FALSE/TRUE. And other minor edits. None of these changes
functionally change the code-flow / semantics, but are done mainly to
clean-up the code.
(#548) Use _Bool for boolean fields in external config struct.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across dot-oh's.) This commit slightly adjusts the typedefs of
boolean fields in external structs to now use _Bool. This should reduce
the risk of such incompatibilities.

Change return type of methods in public_util.h to _Bool .

Relocate typedef int32 bool to private platform_linux/platform.h so it's
used only on Splinter-side.

Cleaned-up few instances around use of bool type for code hygiene:

 - Minor adjustment to routing_filter_is_value_found() returning bool.
 - Stray references to use of 0/1 for boolean values with FALSE/TRUE.
(#548) Use _Bool for boolean fields in external config struct.

In SplinterDB's public splinterdb_config{} config, we have few fields
defined as 'bool' which is typedef'ed to int32 on our side. This creates
compatibility problems when linking this library with other s/w
which may have defined 'bool' as 1-byte field. (Offsets of fields in
the splinterdb_config{} struct following 1st field defined as 'bool'
changes across dot-oh's.) This commit slightly adjusts the typedefs of
boolean fields in external structs to now use _Bool. This should reduce
the risk of such incompatibilities.

Change return type of methods in public_util.h to _Bool .

Relocate typedef int32 bool to private platform_linux/platform.h so it's
used only on Splinter-side.

Cleaned-up few instances around use of bool type for code hygiene:

 - Minor adjustment to routing_filter_is_value_found() returning bool.
 - Stray references to use of 0/1 for boolean values with FALSE/TRUE.
