Definition of  typedef bool  as int32 on Linux causes integration issues w/ other s/w that define bool as BYTE.
For historical reference, this issue was aired out internally in these threads:

1. 21.Dec.2022: [Original slack post](https://vmware.slack.com/archives/G01MEDQ05MX/p1671671258434059)
2. 20.Mar.2023: Follow-up [post](https://vmware.slack.com/archives/G01MEDQ05MX/p1679340319477579), leading to closure and creation of this issue