Ability to intercept GC
@skejeton @marekmaskarinec I implemented `umkaAllocData()`, a reference-counted replacement for `malloc()`. Usage example:

```
// Umka
type P = struct {x, y: int}

fn makedata(x, y: int): ^void
fn procdata(p: ^void)

fn main() {
    for i := 0; i < 100; i++ {
        p := makedata(5 + i, 7 + i)
        procdata(p)
    }
}


// C
typedef struct {int64_t x, y;} P;


void makedata(UmkaStackSlot *params, UmkaStackSlot *result)
{
    int64_t x = params[1].intVal;
    int64_t y = params[0].intVal;
    void *umka = result->ptrVal;    // Upon entry, the result slot stores the Umka instance

    P *p = umkaAllocData(umka, sizeof(P));
    p->x = x;
    p->y = y;

    result->ptrVal = p;
}


void procdata(UmkaStackSlot *params, UmkaStackSlot *result)
{
    P *p = params[0].ptrVal;
    printf("procdata: p = {%lld %lld}\n", p->x, p->y);
}
```
Please try it.

Also we now have `umkaIncRef()` and `umkaDecRef()` for a forced increment/decrement of the reference count (perhaps rarely needed).

as for `umkaIncRef()` and `umkaDecRef()`, I'm not a big fan since most GC's don't actually do refcounting, but if it's temporary i'm cool with that
@skejeton No, it's not temporary. Umka's GC is based on reference counting, so having a means to manually control the counts is important. However, as  I said before, you don't need to use this manual control quite often. One possible case is assigning a reference-counted pointer to another reference-counted pointer in C.
@skejeton @marekmaskarinec I added the `onFree()` optional callback. 

Usage example (see the Umka portion above):
```
// C
typedef struct {int64_t x, y;} P;


void onfreedata(UmkaStackSlot *params, UmkaStackSlot *result)
{
    P *p = params[0].ptrVal;
    printf("onfreedata: p = {%lld %lld}\n", p->x, p->y);
}


void makedata(UmkaStackSlot *params, UmkaStackSlot *result)
{
    int64_t x = params[1].intVal;
    int64_t y = params[0].intVal;
    void *umka = result->ptrVal;    // Upon entry, the result slot stores the Umka instance

    P *p = umkaAllocData(umka, sizeof(P), onfreedata);
    p->x = x;
    p->y = y;

    result->ptrVal = p;
}


void procdata(UmkaStackSlot *params, UmkaStackSlot *result)
{
    P *p = params[0].ptrVal;
    printf("procdata: p = {%lld %lld}\n", p->x, p->y);
}
```
 