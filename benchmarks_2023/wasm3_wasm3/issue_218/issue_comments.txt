Invalid execution of optimized smallpt binary
Looks related to this PR: https://github.com/wasm3/wasm3/pull/175
After reverting #175 things are better, but `-Os` build still fails:
```log
$ ./wasm3 smallpt-ex-Os.wasm 4 32 > out-Os.ppm
...
$ sha1sum *.ppm
ea05d85998b2f453b588ef76a1256215bf9b851c  out-O2.ppm
ea05d85998b2f453b588ef76a1256215bf9b851c  out-O3.ppm
b25e6e933fef1ac856e32775706c2ec1dab22ec5  out-Os.ppm
ea05d85998b2f453b588ef76a1256215bf9b851c  out-Os-wasmer.ppm
ea05d85998b2f453b588ef76a1256215bf9b851c  out-Oz.ppm
```
Also in this example wasm3 fails with `[trap] invalid conversion to integer`, but it doesn't happen on browser engines.
[music-bug.zip](https://github.com/wasm3/wasm3/files/6267877/music-bug.zip)

The zip file contains execution traces of `wasm3` and `nodejs` for comparison.

Steps:

```log
➜ ./wasm3 --repl
wasm3> :load music.wasm
wasm3> :set-global SAMPLERATE 1024
wasm3> :get-global SAMPLERATE
1024.000000:f32
wasm3> playEventsAndFillSampleBuffer
==== wasm backtrace:
  0: 0x0039ca - .unnamed!<unnamed>
  1: 0x003a7e - .unnamed!<unnamed>
  2: 0x0028f4 - .unnamed!<unnamed>
  3: 0x002e01 - .unnamed!fillSampleBuffer
  4: 0x003693 - .unnamed!playEventsAndFillSampleBuffer
Error: [trap] invalid conversion to integer ()
```
Made some progress figuring this out. With some tweaking, i can achieve a successful hash on all but I still haven't tracked down the cause of failure.
@soundandform I can confirm it also fixes the music bug.
@petersalomonsen We can now play your midisynth example 🥳 
@soundandform but it also breaks some other examples from `pywasm3` 😢 
> @soundandform but it also breaks some other examples from `pywasm3` 😢

@vshymanskyy taking a quick look at pywasm code, this is probably due to the change of call frame, preparing for multi-return. empty slots (= to return count) need to be padded before args.

oops. will re-check!

On Fri, 9 Apr 2021, 20:54 Steven Massey, ***@***.***> wrote:

> @soundandform <https://github.com/soundandform> but it also breaks some
> other examples from pywasm3 😢
>
> @vshymanskyy <https://github.com/vshymanskyy> taking a quick look at
> pywasm code, this is probably due to the change of call frame, preparing
> for multi-return. empty slots (= to return count) need to be padded before
> args.
>
> —
> You are receiving this because you were mentioned.
> Reply to this email directly, view it on GitHub
> <https://github.com/wasm3/wasm3/issues/218#issuecomment-816853687>, or
> unsubscribe
> <https://github.com/notifications/unsubscribe-auth/AALP3FAYTO6Y22Y7RL4GBQLTH45NXANCNFSM4ZYDQFYQ>
> .
>

Awesome that you now can play the midisynth 😀
@soundandform please check `pygame-maze.py` example. This one now fails with `[trap] invalid conversion to integer`. it's not related to arguments layout, as it only runs a single `void(void)` function.
Also, pywasm3 uses only the public Wasm3 API (those `put_arg_on_stack` and `get_arg_from_stack` functions are aptly named).
@soundandform nevermind, figured it out. The issue was on `pywasm3` side, in the generic `RawFunction` handler.
I think we can close this, all of the found issues were covered by tests