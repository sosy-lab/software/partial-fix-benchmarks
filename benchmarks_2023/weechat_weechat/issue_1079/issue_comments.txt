Item buffer_nicklist doesn't display nicks inside a group if there are both nick_groups and nicks in it
Thanks for reporting, it should be fixed.
The commit fixed N1.A but now there's another weird thing.

Using the same test script (with the last two lines still commented) and then at the end add:

```
ptr_g111 = w.nicklist_add_group(ptr_buffer, ptr_g11, "G1.1.1", "default", 1)
```

The result is:

```
G1
    G1.1
        G1.1.1
        N1.1.A
        N1.1.B
```

G1.2, N1.2.A and N1.A disappear.

It should be fixed now.
Please let me know if you find other issues.