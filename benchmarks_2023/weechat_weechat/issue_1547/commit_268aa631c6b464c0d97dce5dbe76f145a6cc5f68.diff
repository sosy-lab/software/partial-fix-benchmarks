diff --git a/ChangeLog.adoc b/ChangeLog.adoc
index 24a763c720..22d317ce35 100644
--- a/ChangeLog.adoc
+++ b/ChangeLog.adoc
@@ -20,6 +20,7 @@ https://weechat.org/files/releasenotes/ReleaseNotes-devel.html[release notes]
 
 New features::
 
+  * api: add function string_color_code_size (issue #1547)
   * fset: add option fset.look.auto_refresh (issue #1553)
   * irc: add pointer to irc_nick in focus of bar item "buffer_nicklist" (issue #1535, issue #1538)
   * irc: allow to send text on buffers with commands /allchan, /allpv and /allserv
diff --git a/doc/de/weechat_scripting.de.adoc b/doc/de/weechat_scripting.de.adoc
index 82ae34c19f..3f1921a213 100644
--- a/doc/de/weechat_scripting.de.adoc
+++ b/doc/de/weechat_scripting.de.adoc
@@ -562,6 +562,7 @@ Liste der Skript API Funktionen:
   string_has_highlight_regex +
   string_mask_to_regex +
   string_format_size +
+  string_color_code_size +
   string_remove_color +
   string_is_command_char +
   string_input_for_buffer +
diff --git a/doc/en/weechat_plugin_api.en.adoc b/doc/en/weechat_plugin_api.en.adoc
index c7fb69c34f..172e9a9365 100644
--- a/doc/en/weechat_plugin_api.en.adoc
+++ b/doc/en/weechat_plugin_api.en.adoc
@@ -1888,6 +1888,55 @@ str = weechat.string_format_size(size)
 str = weechat.string_format_size(15200)  # == "15.2 KB"
 ----
 
+==== string_color_code_size
+
+_WeeChat ≥ 3.0._
+
+Return the size (in bytes) of the WeeChat color code at the beginning of
+the string.
+
+Prototype:
+
+[source,C]
+----
+int weechat_string_color_code_size (const char *string);
+----
+
+Arguments:
+
+* _string_: string
+
+Return value:
+
+* size (in bytes) of the WeeChat color code at the beginning of the string;
+  if the string is NULL, empty or does not start with a color code, 0 is returned;
+  if the string begins with multiple color codes, only the size of the first one
+  is returned
+
+C examples:
+
+[source,C]
+----
+int size;
+
+size = weechat_string_color_code_size ("test");  /* size == 0 */
+size = weechat_string_color_code_size (weechat_color ("bold"));  /* size == 2 */
+size = weechat_string_color_code_size (weechat_color ("yellow,red"));  /* size == 7 */
+----
+
+Script (Python):
+
+[source,python]
+----
+# prototype
+size = weechat.string_color_code_size(string)
+
+# examples
+size = weechat.string_color_code_size("test")  # size == 0
+size = weechat.string_color_code_size(weechat.color("bold"))  # size == 2
+size = weechat.string_color_code_size(weechat.color("yellow,red"))  # size == 7
+----
+
 ==== string_remove_color
 
 Remove WeeChat colors from a string.
diff --git a/doc/en/weechat_scripting.en.adoc b/doc/en/weechat_scripting.en.adoc
index 1c57349f29..a14fc1342c 100644
--- a/doc/en/weechat_scripting.en.adoc
+++ b/doc/en/weechat_scripting.en.adoc
@@ -546,6 +546,7 @@ List of functions in script API:
   string_has_highlight_regex +
   string_mask_to_regex +
   string_format_size +
+  string_color_code_size +
   string_remove_color +
   string_is_command_char +
   string_input_for_buffer +
diff --git a/doc/fr/weechat_plugin_api.fr.adoc b/doc/fr/weechat_plugin_api.fr.adoc
index c89527c4db..df275701ea 100644
--- a/doc/fr/weechat_plugin_api.fr.adoc
+++ b/doc/fr/weechat_plugin_api.fr.adoc
@@ -1922,6 +1922,53 @@ str = weechat.string_format_size(size)
 str = weechat.string_format_size(15200)  # == "15.2 Ko"
 ----
 
+==== string_color_code_size
+
+_WeeChat ≥ 3.0._
+
+Retourner la taille (en octets) du code couleur WeeChat au début de la chaîne.
+
+Prototype :
+
+[source,C]
+----
+int weechat_string_color_code_size (const char *string);
+----
+
+Paramètres :
+
+* _string_ : chaîne
+
+Valeur de retour :
+
+* taille (en octets) du code couleur WeeChat au début de la chaîne ;
+  si la chaîne est NULL, vide ou ne commence pas avec un code couleur, 0 est retourné ;
+  si la chaîne commence par plusieurs codes couleur, seule la taille du premier est retournée
+
+Exemples en C :
+
+[source,C]
+----
+int size;
+
+size = weechat_string_color_code_size ("test");  /* size == 0 */
+size = weechat_string_color_code_size (weechat_color ("bold"));  /* size == 2 */
+size = weechat_string_color_code_size (weechat_color ("yellow,red"));  /* size == 7 */
+----
+
+Script (Python) :
+
+[source,python]
+----
+# prototype
+size = weechat.string_color_code_size(string)
+
+# exemples
+size = weechat.string_color_code_size("test")  # size == 0
+size = weechat.string_color_code_size(weechat.color("bold"))  # size == 2
+size = weechat.string_color_code_size(weechat.color("yellow,red"))  # size == 7
+----
+
 ==== string_remove_color
 
 Supprimer les couleurs WeeChat dans une chaîne.
diff --git a/doc/fr/weechat_scripting.fr.adoc b/doc/fr/weechat_scripting.fr.adoc
index b42e29d14a..bb60b9acb0 100644
--- a/doc/fr/weechat_scripting.fr.adoc
+++ b/doc/fr/weechat_scripting.fr.adoc
@@ -565,6 +565,7 @@ Liste des fonctions de l'API script :
   string_has_highlight_regex +
   string_mask_to_regex +
   string_format_size +
+  string_color_code_size +
   string_remove_color +
   string_is_command_char +
   string_input_for_buffer +
diff --git a/doc/it/weechat_plugin_api.it.adoc b/doc/it/weechat_plugin_api.it.adoc
index 2896e2ce4a..62b85ec7ce 100644
--- a/doc/it/weechat_plugin_api.it.adoc
+++ b/doc/it/weechat_plugin_api.it.adoc
@@ -1967,6 +1967,57 @@ str = weechat.string_format_size(size)
 str = weechat.string_format_size(15200)  # == "15.2 KB"
 ----
 
+==== string_color_code_size
+
+_WeeChat ≥ 3.0._
+
+// TRANSLATION MISSING
+Return the size (in bytes) of the WeeChat color code at the beginning of
+the string.
+
+Prototipo:
+
+[source,C]
+----
+int weechat_string_color_code_size (const char *string);
+----
+
+Argomenti:
+
+* _string_: stringa
+
+Valore restituito:
+
+// TRANSLATION MISSING
+* size (in bytes) of the WeeChat color code at the beginning of the string;
+  if the string is NULL, empty or does not start with a color code, 0 is returned;
+  if the string begins with multiple color codes, only the size of the first one
+  is returned
+
+Esempi:
+
+[source,C]
+----
+int size;
+
+size = weechat_string_color_code_size ("test");  /* size == 0 */
+size = weechat_string_color_code_size (weechat_color ("bold"));  /* size == 2 */
+size = weechat_string_color_code_size (weechat_color ("yellow,red"));  /* size == 7 */
+----
+
+Script (Python):
+
+[source,python]
+----
+# prototipo
+size = weechat.string_color_code_size(string)
+
+# esempio
+size = weechat.string_color_code_size("test")  # size == 0
+size = weechat.string_color_code_size(weechat.color("bold"))  # size == 2
+size = weechat.string_color_code_size(weechat.color("yellow,red"))  # size == 7
+----
+
 ==== string_remove_color
 
 Rimuove i colori di WeeChat da una stringa.
diff --git a/doc/it/weechat_scripting.it.adoc b/doc/it/weechat_scripting.it.adoc
index 21d8f57955..c008468118 100644
--- a/doc/it/weechat_scripting.it.adoc
+++ b/doc/it/weechat_scripting.it.adoc
@@ -575,6 +575,7 @@ Elenco di funzioni nelle API per gli script:
   string_has_highlight_regex +
   string_mask_to_regex +
   string_format_size +
+  string_color_code_size +
   string_remove_color +
   string_is_command_char +
   string_input_for_buffer +
diff --git a/doc/ja/weechat_plugin_api.ja.adoc b/doc/ja/weechat_plugin_api.ja.adoc
index c9b1ee1f0f..fdd40dea59 100644
--- a/doc/ja/weechat_plugin_api.ja.adoc
+++ b/doc/ja/weechat_plugin_api.ja.adoc
@@ -1900,6 +1900,57 @@ str = weechat.string_format_size(size)
 str = weechat.string_format_size(15200)  # == "15.2 KB"
 ----
 
+==== string_color_code_size
+
+_WeeChat ≥ 3.0._
+
+// TRANSLATION MISSING
+Return the size (in bytes) of the WeeChat color code at the beginning of
+the string.
+
+プロトタイプ:
+
+[source,C]
+----
+int weechat_string_color_code_size (const char *string);
+----
+
+引数:
+
+* _string_: 文字列
+
+戻り値:
+
+// TRANSLATION MISSING
+* size (in bytes) of the WeeChat color code at the beginning of the string;
+  if the string is NULL, empty or does not start with a color code, 0 is returned;
+  if the string begins with multiple color codes, only the size of the first one
+  is returned
+
+C 言語での使用例:
+
+[source,C]
+----
+int size;
+
+size = weechat_string_color_code_size ("test");  /* size == 0 */
+size = weechat_string_color_code_size (weechat_color ("bold"));  /* size == 2 */
+size = weechat_string_color_code_size (weechat_color ("yellow,red"));  /* size == 7 */
+----
+
+スクリプト (Python) での使用例:
+
+[source,python]
+----
+# プロトタイプ
+size = weechat.string_color_code_size(string)
+
+# 例
+size = weechat.string_color_code_size("test")  # size == 0
+size = weechat.string_color_code_size(weechat.color("bold"))  # size == 2
+size = weechat.string_color_code_size(weechat.color("yellow,red"))  # size == 7
+----
+
 ==== string_remove_color
 
 文字列から WeeChat 色コードを削除。
diff --git a/doc/ja/weechat_scripting.ja.adoc b/doc/ja/weechat_scripting.ja.adoc
index 2527d20685..feca5729ff 100644
--- a/doc/ja/weechat_scripting.ja.adoc
+++ b/doc/ja/weechat_scripting.ja.adoc
@@ -567,6 +567,7 @@ link:weechat_plugin_api.ja.html[WeeChat プラグイン API リファレンス]
   string_has_highlight_regex +
   string_mask_to_regex +
   string_format_size +
+  string_color_code_size +
   string_remove_color +
   string_is_command_char +
   string_input_for_buffer +
diff --git a/doc/pl/weechat_scripting.pl.adoc b/doc/pl/weechat_scripting.pl.adoc
index abc2f2eb4b..29279354c6 100644
--- a/doc/pl/weechat_scripting.pl.adoc
+++ b/doc/pl/weechat_scripting.pl.adoc
@@ -553,6 +553,7 @@ Lista funkcji w API skryptów:
   string_has_highlight_regex +
   string_mask_to_regex +
   string_format_size +
+  string_color_code_size +
   string_remove_color +
   string_is_command_char +
   string_input_for_buffer +
diff --git a/src/gui/gui-color.c b/src/gui/gui-color.c
index f6e99e5a87..c487eb68b3 100644
--- a/src/gui/gui-color.c
+++ b/src/gui/gui-color.c
@@ -562,6 +562,179 @@ gui_color_convert_rgb_to_term (int rgb, int limit)
     return best_color;
 }
 
+/*
+ * Returns the size (in bytes) of the WeeChat color code at the beginning
+ * of "string".
+ *
+ * If "string" is NULL, empty or does not start with a color code,
+ * it returns 0.
+ *
+ * If "string" begins with multiple color codes, only the size of the first
+ * one is returned.
+ */
+
+int
+gui_color_code_size (const char *string)
+{
+    const char *ptr_string;
+
+    if (!string)
+        return 0;
+
+    ptr_string = string;
+
+    switch (ptr_string[0])
+    {
+        case GUI_COLOR_COLOR_CHAR:
+            ptr_string++;
+            switch (ptr_string[0])
+            {
+                case GUI_COLOR_FG_CHAR:
+                    ptr_string++;
+                    if (ptr_string[0] == GUI_COLOR_EXTENDED_CHAR)
+                    {
+                        ptr_string++;
+                        while (gui_color_attr_get_flag (ptr_string[0]) > 0)
+                        {
+                            ptr_string++;
+                        }
+                        if (ptr_string[0] && ptr_string[1] && ptr_string[2]
+                            && ptr_string[3] && ptr_string[4])
+                        {
+                            ptr_string += 5;
+                        }
+                    }
+                    else
+                    {
+                        while (gui_color_attr_get_flag (ptr_string[0]) > 0)
+                        {
+                            ptr_string++;
+                        }
+                        if (ptr_string[0] && ptr_string[1])
+                            ptr_string += 2;
+                    }
+                    break;
+                case GUI_COLOR_BG_CHAR:
+                    ptr_string++;
+                    if (ptr_string[0] == GUI_COLOR_EXTENDED_CHAR)
+                    {
+                        ptr_string++;
+                        if (ptr_string[0] && ptr_string[1] && ptr_string[2]
+                            && ptr_string[3] && ptr_string[4])
+                        {
+                            ptr_string += 5;
+                        }
+                    }
+                    else
+                    {
+                        if (ptr_string[0] && ptr_string[1])
+                            ptr_string += 2;
+                    }
+                    break;
+                case GUI_COLOR_FG_BG_CHAR:
+                    ptr_string++;
+                    if (ptr_string[0] == GUI_COLOR_EXTENDED_CHAR)
+                    {
+                        ptr_string++;
+                        while (gui_color_attr_get_flag (ptr_string[0]) > 0)
+                        {
+                            ptr_string++;
+                        }
+                        if (ptr_string[0] && ptr_string[1] && ptr_string[2]
+                            && ptr_string[3] && ptr_string[4])
+                        {
+                            ptr_string += 5;
+                        }
+                    }
+                    else
+                    {
+                        while (gui_color_attr_get_flag (ptr_string[0]) > 0)
+                        {
+                            ptr_string++;
+                        }
+                        if (ptr_string[0] && ptr_string[1])
+                            ptr_string += 2;
+                    }
+                    /*
+                     * note: the comma is an old separator not used any
+                     * more (since WeeChat 2.6), but we still use it here
+                     * so in case of/upgrade this will not break colors in
+                     * old messages
+                     */
+                    if ((ptr_string[0] == ',') || (ptr_string[0] == '~'))
+                    {
+                        if (ptr_string[1] == GUI_COLOR_EXTENDED_CHAR)
+                        {
+                            ptr_string += 2;
+                            if (ptr_string[0] && ptr_string[1]
+                                && ptr_string[2] && ptr_string[3]
+                                && ptr_string[4])
+                            {
+                                ptr_string += 5;
+                            }
+                        }
+                        else
+                        {
+                            ptr_string++;
+                            if (ptr_string[0] && ptr_string[1])
+                                ptr_string += 2;
+                        }
+                    }
+                    break;
+                case GUI_COLOR_EXTENDED_CHAR:
+                    ptr_string++;
+                    if ((isdigit (ptr_string[0])) && (isdigit (ptr_string[1]))
+                        && (isdigit (ptr_string[2])) && (isdigit (ptr_string[3]))
+                        && (isdigit (ptr_string[4])))
+                    {
+                        ptr_string += 5;
+                    }
+                    break;
+                case GUI_COLOR_EMPHASIS_CHAR:
+                    ptr_string++;
+                    break;
+                case GUI_COLOR_BAR_CHAR:
+                    ptr_string++;
+                    switch (ptr_string[0])
+                    {
+                        case GUI_COLOR_BAR_FG_CHAR:
+                        case GUI_COLOR_BAR_BG_CHAR:
+                        case GUI_COLOR_BAR_DELIM_CHAR:
+                        case GUI_COLOR_BAR_START_INPUT_CHAR:
+                        case GUI_COLOR_BAR_START_INPUT_HIDDEN_CHAR:
+                        case GUI_COLOR_BAR_MOVE_CURSOR_CHAR:
+                        case GUI_COLOR_BAR_START_ITEM:
+                        case GUI_COLOR_BAR_START_LINE_ITEM:
+                            ptr_string++;
+                            break;
+                    }
+                    break;
+                case GUI_COLOR_RESET_CHAR:
+                    ptr_string++;
+                    break;
+                default:
+                    if (isdigit (ptr_string[0]) && isdigit (ptr_string[1]))
+                        ptr_string += 2;
+                    break;
+            }
+            return ptr_string - string;
+            break;
+        case GUI_COLOR_SET_ATTR_CHAR:
+        case GUI_COLOR_REMOVE_ATTR_CHAR:
+            ptr_string++;
+            if (ptr_string[0])
+                ptr_string++;
+            return ptr_string - string;
+            break;
+        case GUI_COLOR_RESET_CHAR:
+            ptr_string++;
+            return ptr_string - string;
+            break;
+    }
+
+    return 0;
+}
+
 /*
  * Removes WeeChat color codes from a message.
  *
diff --git a/src/gui/gui-color.h b/src/gui/gui-color.h
index 7dfc35e483..b2b68a726f 100644
--- a/src/gui/gui-color.h
+++ b/src/gui/gui-color.h
@@ -186,6 +186,7 @@ extern void gui_color_attr_build_string (int color, char *str_attr);
 extern const char *gui_color_get_custom (const char *color_name);
 extern int gui_color_convert_term_to_rgb (int color);
 extern int gui_color_convert_rgb_to_term (int rgb, int limit);
+extern int gui_color_code_size (const char *string);
 extern char *gui_color_decode (const char *string, const char *replacement);
 extern char *gui_color_decode_ansi (const char *string, int keep_colors);
 extern char *gui_color_encode_ansi (const char *string);
diff --git a/src/plugins/guile/weechat-guile-api.c b/src/plugins/guile/weechat-guile-api.c
index 8355c441cb..09b514b837 100644
--- a/src/plugins/guile/weechat-guile-api.c
+++ b/src/plugins/guile/weechat-guile-api.c
@@ -428,6 +428,20 @@ weechat_guile_api_string_format_size (SCM size)
     API_RETURN_STRING_FREE(result);
 }
 
+SCM
+weechat_guile_api_string_color_code_size (SCM string)
+{
+    int size;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    if (!scm_is_string (string))
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    size = weechat_string_color_code_size (API_SCM_TO_STRING(string));
+
+    API_RETURN_INT(size);
+}
+
 SCM
 weechat_guile_api_string_remove_color (SCM string, SCM replacement)
 {
@@ -5012,6 +5026,7 @@ weechat_guile_api_module_init (void *data)
     API_DEF_FUNC(string_has_highlight_regex, 2);
     API_DEF_FUNC(string_mask_to_regex, 1);
     API_DEF_FUNC(string_format_size, 1);
+    API_DEF_FUNC(string_color_code_size, 1);
     API_DEF_FUNC(string_remove_color, 2);
     API_DEF_FUNC(string_is_command_char, 1);
     API_DEF_FUNC(string_input_for_buffer, 1);
diff --git a/src/plugins/javascript/weechat-js-api.cpp b/src/plugins/javascript/weechat-js-api.cpp
index 67b3ef68fb..8938ab0da4 100644
--- a/src/plugins/javascript/weechat-js-api.cpp
+++ b/src/plugins/javascript/weechat-js-api.cpp
@@ -376,6 +376,19 @@ API_FUNC(string_format_size)
     API_RETURN_STRING_FREE(result);
 }
 
+API_FUNC(string_color_code_size)
+{
+    int size;
+
+    API_INIT_FUNC(1, "string_color_code_size", "s", API_RETURN_INT(0));
+
+    v8::String::Utf8Value string(args[0]);
+
+    size = weechat_string_color_code_size (*string);
+
+    API_RETURN_INT(size);
+}
+
 API_FUNC(string_remove_color)
 {
     char *result;
@@ -4945,6 +4958,7 @@ WeechatJsV8::loadLibs()
     API_DEF_FUNC(string_has_highlight_regex);
     API_DEF_FUNC(string_mask_to_regex);
     API_DEF_FUNC(string_format_size);
+    API_DEF_FUNC(string_color_code_size);
     API_DEF_FUNC(string_remove_color);
     API_DEF_FUNC(string_is_command_char);
     API_DEF_FUNC(string_input_for_buffer);
diff --git a/src/plugins/lua/weechat-lua-api.c b/src/plugins/lua/weechat-lua-api.c
index 057c0c4915..72cbd6267c 100644
--- a/src/plugins/lua/weechat-lua-api.c
+++ b/src/plugins/lua/weechat-lua-api.c
@@ -423,6 +423,22 @@ API_FUNC(string_format_size)
     API_RETURN_STRING_FREE(result);
 }
 
+API_FUNC(string_color_code_size)
+{
+    const char *string;
+    int size;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    if (lua_gettop (L) < 1)
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    string = lua_tostring (L, -1);
+
+    size = weechat_string_color_code_size (string);
+
+    API_RETURN_INT(size);
+}
+
 API_FUNC(string_remove_color)
 {
     const char *string, *replacement;
@@ -5309,6 +5325,7 @@ const struct luaL_Reg weechat_lua_api_funcs[] = {
     API_DEF_FUNC(string_has_highlight_regex),
     API_DEF_FUNC(string_mask_to_regex),
     API_DEF_FUNC(string_format_size),
+    API_DEF_FUNC(string_color_code_size),
     API_DEF_FUNC(string_remove_color),
     API_DEF_FUNC(string_is_command_char),
     API_DEF_FUNC(string_input_for_buffer),
diff --git a/src/plugins/perl/weechat-perl-api.c b/src/plugins/perl/weechat-perl-api.c
index d9fc273afa..62e1823a4b 100644
--- a/src/plugins/perl/weechat-perl-api.c
+++ b/src/plugins/perl/weechat-perl-api.c
@@ -383,6 +383,20 @@ API_FUNC(string_format_size)
     API_RETURN_STRING_FREE(result);
 }
 
+API_FUNC(string_color_code_size)
+{
+    int size;
+    dXSARGS;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    if (items < 1)
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    size = weechat_string_color_code_size (SvPV_nolen (ST (0))); /* string */
+
+    API_RETURN_INT(size);
+}
+
 API_FUNC(string_remove_color)
 {
     char *result, *string, *replacement;
@@ -5266,6 +5280,7 @@ weechat_perl_api_init (pTHX)
     API_DEF_FUNC(string_has_highlight_regex);
     API_DEF_FUNC(string_mask_to_regex);
     API_DEF_FUNC(string_format_size);
+    API_DEF_FUNC(string_color_code_size);
     API_DEF_FUNC(string_remove_color);
     API_DEF_FUNC(string_is_command_char);
     API_DEF_FUNC(string_input_for_buffer);
diff --git a/src/plugins/php/weechat-php-api.c b/src/plugins/php/weechat-php-api.c
index 6b05825c46..8d5e4bc34e 100644
--- a/src/plugins/php/weechat-php-api.c
+++ b/src/plugins/php/weechat-php-api.c
@@ -497,6 +497,23 @@ API_FUNC(string_format_size)
     API_RETURN_STRING_FREE(result);
 }
 
+API_FUNC(string_color_code_size)
+{
+    zend_string *z_string;
+    char *string;
+    int result;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    if (zend_parse_parameters (ZEND_NUM_ARGS(), "S", &z_string) == FAILURE)
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    string = ZSTR_VAL(z_string);
+
+    result = weechat_string_color_code_size ((const char *)string);
+
+    API_RETURN_INT(result);
+}
+
 API_FUNC(string_remove_color)
 {
     zend_string *z_string, *z_replacement;
diff --git a/src/plugins/php/weechat-php-api.h b/src/plugins/php/weechat-php-api.h
index 71ca67b268..65ce9d3568 100644
--- a/src/plugins/php/weechat-php-api.h
+++ b/src/plugins/php/weechat-php-api.h
@@ -59,6 +59,7 @@ PHP_FUNCTION(weechat_string_has_highlight);
 PHP_FUNCTION(weechat_string_has_highlight_regex);
 PHP_FUNCTION(weechat_string_mask_to_regex);
 PHP_FUNCTION(weechat_string_format_size);
+PHP_FUNCTION(weechat_string_color_code_size);
 PHP_FUNCTION(weechat_string_remove_color);
 PHP_FUNCTION(weechat_string_is_command_char);
 PHP_FUNCTION(weechat_string_input_for_buffer);
diff --git a/src/plugins/php/weechat-php.c b/src/plugins/php/weechat-php.c
index 93a1f20698..ace023828a 100644
--- a/src/plugins/php/weechat-php.c
+++ b/src/plugins/php/weechat-php.c
@@ -112,6 +112,7 @@ const zend_function_entry weechat_functions[] = {
     PHP_FE(weechat_string_has_highlight_regex, NULL)
     PHP_FE(weechat_string_mask_to_regex, NULL)
     PHP_FE(weechat_string_format_size, NULL)
+    PHP_FE(weechat_string_color_code_size, NULL)
     PHP_FE(weechat_string_remove_color, NULL)
     PHP_FE(weechat_string_is_command_char, NULL)
     PHP_FE(weechat_string_input_for_buffer, NULL)
diff --git a/src/plugins/plugin.c b/src/plugins/plugin.c
index 5f0a75f5c7..f1f8404633 100644
--- a/src/plugins/plugin.c
+++ b/src/plugins/plugin.c
@@ -632,6 +632,7 @@ plugin_load (const char *filename, int init_plugin, int argc, char **argv)
         new_plugin->string_split_command = &string_split_command;
         new_plugin->string_free_split_command = &string_free_split_command;
         new_plugin->string_format_size = &string_format_size;
+        new_plugin->string_color_code_size = &gui_color_code_size;
         new_plugin->string_remove_color = &gui_color_decode;
         new_plugin->string_base_encode = &string_base_encode;
         new_plugin->string_base_decode = &string_base_decode;
diff --git a/src/plugins/python/weechat-python-api.c b/src/plugins/python/weechat-python-api.c
index 690c33d1fd..b55efe7f78 100644
--- a/src/plugins/python/weechat-python-api.c
+++ b/src/plugins/python/weechat-python-api.c
@@ -369,6 +369,21 @@ API_FUNC(string_format_size)
     API_RETURN_STRING_FREE(result);
 }
 
+API_FUNC(string_color_code_size)
+{
+    char *string;
+    int size;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    string = NULL;
+    if (!PyArg_ParseTuple (args, "s", &string))
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    size = weechat_string_color_code_size (string);
+
+    API_RETURN_INT(size);
+}
+
 API_FUNC(string_remove_color)
 {
     char *string, *replacement, *result;
@@ -5217,6 +5232,7 @@ PyMethodDef weechat_python_funcs[] =
     API_DEF_FUNC(string_has_highlight_regex),
     API_DEF_FUNC(string_mask_to_regex),
     API_DEF_FUNC(string_format_size),
+    API_DEF_FUNC(string_color_code_size),
     API_DEF_FUNC(string_remove_color),
     API_DEF_FUNC(string_is_command_char),
     API_DEF_FUNC(string_input_for_buffer),
diff --git a/src/plugins/ruby/weechat-ruby-api.c b/src/plugins/ruby/weechat-ruby-api.c
index 20c003f1b0..e993af323e 100644
--- a/src/plugins/ruby/weechat-ruby-api.c
+++ b/src/plugins/ruby/weechat-ruby-api.c
@@ -448,6 +448,25 @@ weechat_ruby_api_string_format_size (VALUE class, VALUE size)
     API_RETURN_STRING_FREE(result);
 }
 
+static VALUE
+weechat_ruby_api_string_color_code_size (VALUE class, VALUE string)
+{
+    char *c_string;
+    int size;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    if (NIL_P (string))
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    Check_Type (string, T_STRING);
+
+    c_string = StringValuePtr (string);
+
+    size = weechat_string_color_code_size (c_string);
+
+    API_RETURN_INT(size);
+}
+
 static VALUE
 weechat_ruby_api_string_remove_color (VALUE class, VALUE string,
                                       VALUE replacement)
@@ -6424,6 +6443,7 @@ weechat_ruby_api_init (VALUE ruby_mWeechat)
     API_DEF_FUNC(string_has_highlight_regex, 2);
     API_DEF_FUNC(string_mask_to_regex, 1);
     API_DEF_FUNC(string_format_size, 1);
+    API_DEF_FUNC(string_color_code_size, 1);
     API_DEF_FUNC(string_remove_color, 2);
     API_DEF_FUNC(string_is_command_char, 1);
     API_DEF_FUNC(string_input_for_buffer, 1);
diff --git a/src/plugins/tcl/weechat-tcl-api.c b/src/plugins/tcl/weechat-tcl-api.c
index 528e501dc1..475f746e4f 100644
--- a/src/plugins/tcl/weechat-tcl-api.c
+++ b/src/plugins/tcl/weechat-tcl-api.c
@@ -530,6 +530,20 @@ API_FUNC(string_format_size)
     API_RETURN_STRING_FREE(result);
 }
 
+API_FUNC(string_color_code_size)
+{
+    Tcl_Obj *objp;
+    int result, i;
+
+    API_INIT_FUNC(1, "string_color_code_size", API_RETURN_INT(0));
+    if (objc < 2)
+        API_WRONG_ARGS(API_RETURN_INT(0));
+
+    result = weechat_string_color_code_size (Tcl_GetStringFromObj (objv[1], &i)); /* string */
+
+    API_RETURN_INT(result);
+}
+
 API_FUNC(string_remove_color)
 {
     Tcl_Obj *objp;
@@ -5739,6 +5753,7 @@ void weechat_tcl_api_init (Tcl_Interp *interp)
     API_DEF_FUNC(string_has_highlight_regex);
     API_DEF_FUNC(string_mask_to_regex);
     API_DEF_FUNC(string_format_size);
+    API_DEF_FUNC(string_color_code_size);
     API_DEF_FUNC(string_remove_color);
     API_DEF_FUNC(string_is_command_char);
     API_DEF_FUNC(string_input_for_buffer);
diff --git a/src/plugins/weechat-plugin.h b/src/plugins/weechat-plugin.h
index 44c74ea2de..b8cf10456e 100644
--- a/src/plugins/weechat-plugin.h
+++ b/src/plugins/weechat-plugin.h
@@ -67,7 +67,7 @@ struct timeval;
  * please change the date with current one; for a second change at same
  * date, increment the 01, otherwise please keep 01.
  */
-#define WEECHAT_PLUGIN_API_VERSION "20200621-01"
+#define WEECHAT_PLUGIN_API_VERSION "20200822-01"
 
 /* macros for defining plugin infos */
 #define WEECHAT_PLUGIN_NAME(__name)                                     \
@@ -334,6 +334,7 @@ struct t_weechat_plugin
     char **(*string_split_command) (const char *command, char separator);
     void (*string_free_split_command) (char **split_command);
     char *(*string_format_size) (unsigned long long size);
+    int (*string_color_code_size) (const char *string);
     char *(*string_remove_color) (const char *string, const char *replacement);
     int (*string_base_encode) (int base, const char *from, int length,
                                char *to);
@@ -1265,6 +1266,8 @@ extern int weechat_plugin_end (struct t_weechat_plugin *plugin);
     (weechat_plugin->string_free_split_command)(__split_command)
 #define weechat_string_format_size(__size)                              \
     (weechat_plugin->string_format_size)(__size)
+#define weechat_string_color_code_size(__string)                        \
+    (weechat_plugin->string_color_code_size)(__string)
 #define weechat_string_remove_color(__string, __replacement)            \
     (weechat_plugin->string_remove_color)(__string, __replacement)
 #define weechat_string_base_encode(__base, __from, __length, __to)      \
diff --git a/tests/scripts/python/testapi.py b/tests/scripts/python/testapi.py
index 2fbf8370d1..aaa03157a5 100644
--- a/tests/scripts/python/testapi.py
+++ b/tests/scripts/python/testapi.py
@@ -71,6 +71,10 @@ def test_strings():
     check(weechat.string_format_size(1) == '1 byte')
     check(weechat.string_format_size(2097152) == '2.10 MB')
     check(weechat.string_format_size(420000000) == '420.00 MB')
+    check(weechat.string_color_code_size('') == 0)
+    check(weechat.string_color_code_size('test') == 0)
+    str_color = weechat.color('yellow,red')
+    check(weechat.string_color_code_size(str_color) == 7)
     check(weechat.string_remove_color('test', '?') == 'test')
     check(weechat.string_is_command_char('/test') == 1)
     check(weechat.string_is_command_char('test') == 0)
diff --git a/tests/unit/gui/test-gui-color.cpp b/tests/unit/gui/test-gui-color.cpp
index 19c861dd7f..32fe7cd537 100644
--- a/tests/unit/gui/test-gui-color.cpp
+++ b/tests/unit/gui/test-gui-color.cpp
@@ -233,6 +233,95 @@ TEST(GuiColor, GetCustom)
     STRCMP_EQUAL(string, gui_color_get_custom ("_/227:blue"));
 }
 
+/*
+ * Tests functions:
+ *   gui_color_code_size
+ */
+
+TEST(GuiColor, CodeSize)
+{
+    char string[256];
+
+    /* NULL/empty string */
+    LONGS_EQUAL(0, gui_color_code_size (NULL));
+    LONGS_EQUAL(0, gui_color_code_size (""));
+
+    /* no color code */
+    LONGS_EQUAL(0, gui_color_code_size ("test"));
+
+    /* reset */
+    LONGS_EQUAL(1, gui_color_code_size (gui_color_get_custom ("reset")));
+
+    /* reset (×2) */
+    snprintf (string, sizeof (string),
+              "%s%s",
+              gui_color_get_custom ("reset"),
+              gui_color_get_custom ("reset"));
+    LONGS_EQUAL(1, gui_color_code_size (string));
+
+    /* resetcolor */
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("resetcolor")));
+
+    /* emphasis */
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("emphasis")));
+
+    /* bold */
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("bold")));
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("-bold")));
+
+    /* reverse */
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("reverse")));
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("-reverse")));
+
+    /* italic */
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("italic")));
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("-italic")));
+
+    /* underline */
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("underline")));
+    LONGS_EQUAL(2, gui_color_code_size (gui_color_get_custom ("-underline")));
+
+    /* bar_fg */
+    LONGS_EQUAL(3, gui_color_code_size (gui_color_get_custom ("bar_fg")));
+
+    /* bar_delim */
+    LONGS_EQUAL(3, gui_color_code_size (gui_color_get_custom ("bar_delim")));
+
+    /* bar_bg */
+    LONGS_EQUAL(3, gui_color_code_size (gui_color_get_custom ("bar_bg")));
+
+    /* fg color */
+    LONGS_EQUAL(4, gui_color_code_size (gui_color_get_custom ("blue")));
+
+    /* bg color */
+    LONGS_EQUAL(4, gui_color_code_size (gui_color_get_custom (",blue")));
+
+    /* fg+bg color */
+    LONGS_EQUAL(7, gui_color_code_size (gui_color_get_custom ("yellow,blue")));
+
+    /* fg+bg color (×2) */
+    snprintf (string, sizeof (string),
+              "%s%s",
+              gui_color_get_custom ("yellow,blue"),
+              gui_color_get_custom ("yellow,blue"));
+    LONGS_EQUAL(7, gui_color_code_size (string));
+
+    /* fg terminal color */
+    LONGS_EQUAL(8, gui_color_code_size (gui_color_get_custom ("214")));
+
+    /* bg terminal color */
+    LONGS_EQUAL(8, gui_color_code_size (gui_color_get_custom (",214")));
+
+    /* fg+bg terminal color */
+    LONGS_EQUAL(15, gui_color_code_size (gui_color_get_custom ("227,240")));
+
+    /* fg terminal color + bg color */
+    LONGS_EQUAL(11, gui_color_code_size (gui_color_get_custom ("227,blue")));
+
+    /* WeeChat color */
+    LONGS_EQUAL(3, gui_color_code_size (GUI_COLOR(GUI_COLOR_CHAT_HOST)));
+}
+
 /*
  * Tests functions:
  *   gui_color_decode
