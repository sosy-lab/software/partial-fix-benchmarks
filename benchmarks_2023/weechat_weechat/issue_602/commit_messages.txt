doc: update German auto-generated files
irc: add a missing colon before the password in PASS message (closes #602)
irc: add a condition for colon before the password in PASS message (issue #602)

Some weird IRC servers may refuse a PASS command with a colon, so we
send a colon only if the password contains spaces or begins with a
colon.
