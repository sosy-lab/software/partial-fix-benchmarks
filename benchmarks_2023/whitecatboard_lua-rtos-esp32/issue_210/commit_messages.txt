Merge pull request #208 from gjongenelen/add-esp32-poe

Added ESP32-POE
bugfix/MQTT client/not authorized result code if user and password are empty in test.mosquitto.org (see https://github.com/whitecatboard/Lua-RTOS-ESP32/issues/210)

Prior to this commit, when calling to client:connect("","") from Lua with an empty user name / user password, client fails to connect to test.mosquitto.org. When user name / password are empty it is required to set usename and password into MQTTAsync_connectOptions structure to NULL.
new feature/MQTT client/new argument in client:connect function to set if session must ble cleaned on connection

In the previous version the connection was stablished to not clean the session. This seems to be incompatible by the "IBM Watson IoT Quickstart" platform, and probably others (see https://github.com/whitecatboard/Lua-RTOS-ESP32/issues/210#issuecomment-442434278). To solve this the client:connect signature is now client:connect(user, password, [clean]), where clean is an optional boolean (set to false by default) that cleans the session if it's true.

Code example:

client = mqtt.client("d:quickstart:mosquitto:whitecat", "quickstart.messaging.internetofthings.ibmcloud.com", 1883, false)
client:connect("","", true)
client:publish("iot-2/evt/helloworld/fmt/json", '{"temp": 14}', mqtt.QOS0)
