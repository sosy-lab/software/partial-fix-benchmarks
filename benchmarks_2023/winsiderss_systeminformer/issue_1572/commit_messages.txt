Add support for loading KSI without SCM via NT or FLT
Disable legacy mitigation policies (Fixes #1572)
Completely remove legacy mitigation policy (#1572)
