#8 #13 camera switch by trigger, secrets
#9 add simple sound mixing for a PCM, MS ADPCM (from TR3 cdaudio.wad), MP3 (via minimp3 lib) & OGG (via stb_vorbis lib)
#9 tab to space
#9 support Web Audio API, defines for ADPCM, MP3 & OGG decoders, play bubble sound when Lara is under water
#9 force resample web audio api to 44.1kHz
#9 apply sample volume
#9 fading & panning of sound sources
#8 fix loosing camera room; #10 temporary turn off underwater sound; #9 add rewind mode for samples; #14 add basic AI logic for bears and bats; #3 health, death and basic inventory logic; #15 add fast & slow-motion while holding R & T keys
closed #13 add doors implementation & puzzle hole mesh swap; #9 loop ambient sound;
#9 fix static sounds (no free channels message)
#9 sound reverberation
#9 fix listener params for first person view
#9 underwater ambient sound
