The Menu Bar Flashes During Fullscreen Switching
This is due to the way changes of the window size are handled in sxiv and I will not fix this.

You can change TO_REDRAW_RESIZE in types.h to 0 to force sxiv to fully redraw its window contents after every ConfigureNotify event it receives. Please be aware that this could have a huge impact on sxiv cpu usage, depending on the selected scaling option and image size.

Setting TO_REDRAW_RESIZE to 0 in types.h doesn't yield any change.

Sorry, I've forgot to mention, that you need to run `make clean' after making changes to types.h and before building sxiv.

Sorry, I've misread the description of the problem. I read `sxiv -f foo` instead of `sxiv -b foo`. Now that I understand the real problem, I will take a second look at it.

> Sorry, I've misread the description of the problem. I read `sxiv -f foo' instead of`sxiv -b foo'. Now that I understand the real problem, I will take a second look at it.

Thanks for your diligence.

I have to admit I didn't understand why it was normal for the bar to
appear, briefly or not.

A rather stupid mistake. Fixed.
