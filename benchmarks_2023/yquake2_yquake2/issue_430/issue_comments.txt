[Suggestion] Add 'entity' and 'teleport/warp' (setviewpos) commands
Sounds like a good idea and shouldn't be too hard to implement. We may need to wrap it behind `cheats 1`, though.

I'm pretty sure that's possible to play through baseq2 with getting 100% kills. That wasn't possible with Vanilla Quake II, there were some unreachable monsters in the mines and the city if I remember correctly.  We fixed that by manipulating the counter, not by despawning the monsters. So they're still there and can be found noclipping through the levels...

xatrix should work, too. rogue has a high possibility mess up the kill counter. The game has a lot enhances Medics, spawning other monsters. That's prone to introduce offsets, e.g. count too much monsters.
I'm not aware of rogue count offsets, personally. Minions of boss enemies as well as Medic patients are all flagged as do not count before monster_start is called, and unless they lose their do not count flag while alive (which I haven't observed before) they will be ignored when Killed is called upon their death.

If anything, a command like this would make it easier to notice and fix such count offsets if they do come up.
I've added two new commands to baseq1, xatrix and rogue. They're in the game DLL, so they won't be available in mods and the like:

* `teleport x y z` teleports the player to the given coordinate. This is implemented as a full fashioned teleport and not just a simple move. All movements are cleared, angle predictions are reset, the player is pushed up by 10 units (to prevent that he's getting stuck in the floor) and monsters or other players at the target location are telefragged.
* `listentities <all|ammo|items|keys|monsters|weapons>` prints all known entities of the given class and their location. Multiple classes can be combined into one command, e.g. `listentities ammo weapons` will print all ammunition and weapons. The special class `all` prints everything.

Both command are hidden behind `cheats 1`.
I forgot to mention that the `teleport` command **does not** check if the target location is sane. The player can teleport into the world model and into the void.
The new commands are great, thanks for adding!
But I have a bug to report:
Every class except 'all' crashes the game after reloading a save or revisiting a level in a hub.
Also, the two commands aren't shown when hitting tab yet.

Edit: The crash isn't related to reloading or reentering. It worked fine again in the second hub even after multiple quicksaves and -loads and then suddenly didn't again. I'd attach a log, but stderr.txt is empty and stdout.txt only contains console history, :/
> Every class except 'all' crashes the game after reloading a save or revisiting a level in a hub.

Stupid oversight... I didn't take into account that the entity array may contain empty slots. I've pushed a fix.

> Also, the two commands aren't shown when hitting tab yet.

The problem here is that the client can only complete commands he knows about. For standard command that are present in all (or at least most game DLL) the hack is to add them as NULL commands. That's done in `CL_InitLocal()`. If I would add `teleport` and `listentities` they would be valid commands, no matter if the game DLL implements them or not. It should be possible to add them based on the game name, but that would make them available to all cases were `game` is empty or set to `rogue` or `xatrix`. Even if the player uses another DLL that doesn't implement them.

@DanielGibson: What do you think? Should we add them or not? 
Yeah, let's add them.
What happens if the game.dll doesn't implement them - nothing? That's ok..
(Would be cooler if we could have a fallback that just displays an error message if the game.dll doesn't implement it, but not sure that's even possible)
Okay, I've added them unconditionally. If the game dll doesn't implement them they're still available for autocompletion but will will error out as unknown.