install docbook-xsl instead of docbook-xml for travis

seems to help with a2x hangs
only process results of OTP check after user is found

relates #97
drop check for OTP length, should trigger error later anyways.

relates #97
Return early if the user has no authorized tokens

Currently, if a user has no associated tokens, we still prompt for an
OTP challenge and attempt to verify it.

This adds a check earlier to avoid the useless prompt in that case.

The `nullok` option is also added. It changes the return value from
PAM_USER_UNKNOWN to PAM_IGNORE. (fixes #97)

Finally, some constants have been turned to symbolic form for clarity
and debugging output is improved.
Return early if the user has no authorized tokens

Currently, if a user has no associated tokens, we still prompt for an
OTP challenge and attempt to verify it.

This adds a check earlier to avoid the useless prompt in that case.

The `nullok` option is also added. It changes the return value from
PAM_USER_UNKNOWN to PAM_IGNORE. (fixes #97)

Finally, some constants have been turned to symbolic form for clarity
and debugging output is improved.
Return early if the user has no authorized tokens

Currently, if a user has no associated tokens, we still prompt for an
OTP challenge and attempt to verify it.

This adds a check earlier to avoid the useless prompt in that case.

The `nullok` option is also added. It changes the return value from
PAM_USER_UNKNOWN to PAM_IGNORE. (fixes #97)

Finally, some constants have been turned to symbolic form for clarity
and debugging output is improved.
Return early if the user has no authorized tokens

Currently, if a user has no associated tokens, we still prompt for an
OTP challenge and attempt to verify it.

This adds a check earlier to avoid the useless prompt in that case.

The `nullok` option is also added. It changes the return value from
PAM_USER_UNKNOWN to PAM_IGNORE. (fixes #97)

Finally, some constants have been turned to symbolic form for clarity
and debugging output is improved.
