diff --git a/src/yb/client/ql-dml-test-base.h b/src/yb/client/ql-dml-test-base.h
index 203541eb9c84..bbf83f28f898 100644
--- a/src/yb/client/ql-dml-test-base.h
+++ b/src/yb/client/ql-dml-test-base.h
@@ -49,7 +49,7 @@ class QLDmlTestBase : public MiniClusterTestWithClient<MiniClusterType> {
   virtual ~QLDmlTestBase() {}
 
  protected:
-  void SetFlags();
+  virtual void SetFlags();
   void StartCluster();
 
   using MiniClusterTestWithClient<MiniClusterType>::client_;
diff --git a/src/yb/integration-tests/tablet-split-itest.cc b/src/yb/integration-tests/tablet-split-itest.cc
index 9a2a3582721a..19c8d31471ba 100644
--- a/src/yb/integration-tests/tablet-split-itest.cc
+++ b/src/yb/integration-tests/tablet-split-itest.cc
@@ -43,6 +43,7 @@
 #include "yb/gutil/strings/join.h"
 
 #include "yb/integration-tests/cdc_test_util.h"
+#include "yb/integration-tests/cluster_itest_util.h"
 #include "yb/integration-tests/mini_cluster.h"
 #include "yb/integration-tests/redis_table_test_base.h"
 #include "yb/integration-tests/test_workload.h"
@@ -112,7 +113,6 @@ DECLARE_int64(tablet_split_high_phase_size_threshold_bytes);
 DECLARE_int64(tablet_force_split_threshold_bytes);
 DECLARE_bool(TEST_disable_split_tablet_candidate_processing);
 DECLARE_int32(process_split_tablet_candidates_interval_msec);
-DECLARE_bool(TEST_disable_cleanup_split_tablet);
 DECLARE_int32(tserver_heartbeat_metrics_interval_ms);
 DECLARE_bool(TEST_validate_all_tablet_candidates);
 DECLARE_bool(TEST_select_all_tablets_for_split);
@@ -213,6 +213,10 @@ namespace {
 constexpr auto kRpcTimeout = 60s * kTimeMultiplier;
 constexpr auto kDefaultNumRows = 500;
 
+// We set small data block size, so we don't have to write much data to have multiple blocks.
+// We need multiple blocks to be able to detect split key (see BlockBasedTable::GetMiddleKey).
+constexpr auto kDbBlockSizeBytes = 2_KB;
+
 } // namespace
 
 template <class MiniClusterType>
@@ -241,7 +245,21 @@ class TabletSplitITestBase : public client::TransactionTestBase<MiniClusterType>
   // Writes `num_rows` rows into test table using `CreateInsertRequest`.
   // Returns a pair with min and max hash code written.
   Result<std::pair<docdb::DocKeyHash, docdb::DocKeyHash>> WriteRows(
-      size_t num_rows = 2000, size_t start_key = 1);
+      size_t num_rows = kDefaultNumRows, size_t start_key = 1);
+
+  CHECKED_STATUS FlushTestTable() {
+    return this->client_->FlushTables(
+        {this->table_->id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+        /* is_compaction = */ false);
+  }
+
+  Result<std::pair<docdb::DocKeyHash, docdb::DocKeyHash>> WriteRowsAndFlush(
+      const size_t num_rows = kDefaultNumRows, const size_t start_key = 1) {
+    auto result = VERIFY_RESULT(WriteRows(num_rows));
+    RETURN_NOT_OK(WriteRows(num_rows));
+    RETURN_NOT_OK(FlushTestTable());
+    return result;
+  }
 
   Result<docdb::DocKeyHash> WriteRowsAndGetMiddleHashCode(size_t num_rows) {
     auto min_max_hash_code = VERIFY_RESULT(WriteRows(num_rows, 1));
@@ -318,9 +336,7 @@ class TabletSplitITest : public TabletSplitITestBase<MiniCluster> {
     FLAGS_enable_automatic_tablet_splitting = false;
     FLAGS_TEST_validate_all_tablet_candidates = true;
     FLAGS_TEST_select_all_tablets_for_split = true;
-    // We set small data block size, so we don't have to write much data to have multiple blocks.
-    // We need multiple blocks to be able to detect split key (see BlockBasedTable::GetMiddleKey).
-    FLAGS_db_block_size_bytes = 2_KB;
+    FLAGS_db_block_size_bytes = kDbBlockSizeBytes;
     // We set other block sizes to be small for following test reasons:
     // 1) To have more granular change of SST file size depending on number of rows written.
     // This helps to do splits earlier and have faster tests.
@@ -2144,35 +2160,27 @@ TEST_F(TabletSplitSingleServerITest, TabletServerSplitAlreadySplitTablet) {
 }
 
 class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<ExternalMiniCluster> {
- public:
-  void SetUp() override {
-    for (const auto& flag : GetTserverFlags()) {
-      this->mini_cluster_opt_.extra_tserver_flags.push_back(flag);
+ protected:
+  void SetFlags() override {
+    TabletSplitITestBase<ExternalMiniCluster>::SetFlags();
+    for (const auto& master_flag : {
+             "--TEST_disable_split_tablet_candidate_processing=true",
+             "--tablet_split_low_phase_shard_count_per_node=-1",
+             "--tablet_split_high_phase_shard_count_per_node=-1",
+             "--tablet_split_low_phase_size_threshold_bytes=-1",
+             "--tablet_split_high_phase_size_threshold_bytes=-1",
+             "--tablet_force_split_threshold_bytes=-1",
+         }) {
+      mini_cluster_opt_.extra_master_flags.push_back(master_flag);
     }
 
-    for (const auto& flag : GetMasterFlags()) {
-      this->mini_cluster_opt_.extra_master_flags.push_back(flag);
+    for (const auto& tserver_flag : std::initializer_list<std::string>{
+             Format("--db_block_size_bytes=$0", kDbBlockSizeBytes),
+             "--cleanup_split_tablets_interval_sec=1",
+             "--tserver_heartbeat_metrics_interval_ms=100",
+         }) {
+      mini_cluster_opt_.extra_tserver_flags.push_back(tserver_flag);
     }
-
-    TabletSplitITestBase<ExternalMiniCluster>::SetUp();
-  }
-
-  virtual std::vector<std::string> GetTserverFlags() const {
-    return {
-      "--cleanup_split_tablets_interval_sec=1",
-      "--tserver_heartbeat_metrics_interval_ms=100"
-    };
-  }
-
-  virtual std::vector<std::string> GetMasterFlags() const {
-    return {
-      "--TEST_disable_split_tablet_candidate_processing=true",
-      "--tablet_split_low_phase_shard_count_per_node=-1",
-      "--tablet_split_high_phase_shard_count_per_node=-1",
-      "--tablet_split_low_phase_size_threshold_bytes=-1",
-      "--tablet_split_high_phase_size_threshold_bytes=-1",
-      "--tablet_force_split_threshold_bytes=-1",
-    };
   }
 
   CHECKED_STATUS SplitTablet(const std::string& tablet_id) {
@@ -2210,6 +2218,9 @@ class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<External
   Result<std::set<TabletId>> GetTestTableTabletIds() {
     std::set<TabletId> tablet_ids;
     for (int i = 0; i < cluster_->num_tablet_servers(); ++i) {
+      if (cluster_->tablet_server(i)->IsShutdown()) {
+        continue;
+      }
       auto res = VERIFY_RESULT(GetTestTableTabletIds(i));
       for (const auto& id : res) {
         tablet_ids.insert(id);
@@ -2251,16 +2262,27 @@ class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<External
 
   CHECKED_STATUS WaitForTabletsExcept(
       int num_tablets, int tserver_idx, const TabletId& exclude_tablet) {
-    return WaitFor([&]() -> Result<bool> {
-      auto res = VERIFY_RESULT(GetTestTableTabletIds(tserver_idx));
-      int count = 0;
-      for (auto& tablet_id : res) {
-        if (tablet_id != exclude_tablet) {
-          count++;
-        }
-      }
-      return count == num_tablets;
-    }, 20s * kTimeMultiplier, Format("Waiting for tablet count: $0", num_tablets));
+    std::set<TabletId> tablets;
+    auto status = WaitFor(
+        [&]() -> Result<bool> {
+          tablets = VERIFY_RESULT(GetTestTableTabletIds(tserver_idx));
+          int count = 0;
+          for (auto& tablet_id : tablets) {
+            if (tablet_id != exclude_tablet) {
+              count++;
+            }
+          }
+          return count == num_tablets;
+        },
+        20s * kTimeMultiplier,
+        Format(
+            "Waiting for tablet count: $0 at tserver: $1",
+            num_tablets,
+            cluster_->tablet_server(tserver_idx)->uuid()));
+    if (!status.ok()) {
+      status = status.CloneAndAppend(Format("Got tablets: $0", tablets));
+    }
+    return status;
   }
 
   CHECKED_STATUS WaitForTablets(int num_tablets, int tserver_idx) {
@@ -2268,19 +2290,23 @@ class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<External
   }
 
   CHECKED_STATUS WaitForTablets(int num_tablets) {
-    return WaitFor([&]() -> Result<bool> {
-      auto res = VERIFY_RESULT(GetTestTableTabletIds());
-      return res.size() == num_tablets;
+    std::set<TabletId> tablets;
+    auto status = WaitFor([&]() -> Result<bool> {
+      tablets = VERIFY_RESULT(GetTestTableTabletIds());
+      return tablets.size() == num_tablets;
     }, 20s * kTimeMultiplier, Format("Waiting for tablet count: $0", num_tablets));
+    if (!status.ok()) {
+      status = status.CloneAndAppend(Format("Got tablets: $0", tablets));
+    }
+    return status;
   }
 
   CHECKED_STATUS SplitTabletCrashMaster(bool change_split_boundary, string* split_partition_key) {
     CreateSingleTablet();
     int key = 1, num_rows = 2000;
-    RETURN_NOT_OK(WriteRows(num_rows, key));
+    RETURN_NOT_OK(WriteRowsAndFlush(num_rows, key));
     key += num_rows;
-    RETURN_NOT_OK(client_->FlushTables({table_->id()}, false, 30, false));
-    auto tablet_id = CHECK_RESULT(GetOnlyTabletId());
+    auto tablet_id = CHECK_RESULT(GetOnlyTestTabletId());
 
     RETURN_NOT_OK(cluster_->SetFlagOnMasters(
       "TEST_crash_after_creating_single_split_tablet", "1.0"));
@@ -2329,7 +2355,7 @@ class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<External
     return Status::OK();
   }
 
-  Result<TabletId> GetOnlyTabletId(int tserver_idx) {
+  Result<TabletId> GetOnlyTestTabletId(int tserver_idx) {
     auto tablet_ids = VERIFY_RESULT(GetTestTableTabletIds(tserver_idx));
     if (tablet_ids.size() != 1) {
       return STATUS(InternalError, "Expected one tablet");
@@ -2337,7 +2363,7 @@ class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<External
     return *tablet_ids.begin();
   }
 
-  Result<TabletId> GetOnlyTabletId() {
+  Result<TabletId> GetOnlyTestTabletId() {
     auto tablet_ids = VERIFY_RESULT(GetTestTableTabletIds());
     if (tablet_ids.size() != 1) {
       return STATUS(InternalError, Format("Expected one tablet, got $0", tablet_ids.size()));
@@ -2348,9 +2374,8 @@ class TabletSplitExternalMiniClusterITest : public TabletSplitITestBase<External
 
 TEST_F(TabletSplitExternalMiniClusterITest, Simple) {
   CreateSingleTablet();
-  CHECK_OK(WriteRows());
-  ASSERT_OK(client_->FlushTables({table_->id()}, false, 30, false));
-  auto tablet_id = CHECK_RESULT(GetOnlyTabletId());
+  CHECK_OK(WriteRowsAndFlush());
+  auto tablet_id = CHECK_RESULT(GetOnlyTestTabletId());
   CHECK_OK(SplitTablet(tablet_id));
   ASSERT_OK(WaitForTablets(3));
 }
@@ -2389,9 +2414,8 @@ TEST_F(TabletSplitExternalMiniClusterITest, CrashMasterCheckConsistentPartitionK
 TEST_F(TabletSplitExternalMiniClusterITest, FaultedSplitNodeRejectsRemoteBootstrap) {
   constexpr int kTabletSplitInjectDelayMs = 20000 * kTimeMultiplier;
   CreateSingleTablet();
-  ASSERT_OK(WriteRows());
-  ASSERT_OK(client_->FlushTables({table_->id()}, false, 30, false));
-  const auto tablet_id = CHECK_RESULT(GetOnlyTabletId());
+  ASSERT_OK(WriteRowsAndFlush());
+  const auto tablet_id = CHECK_RESULT(GetOnlyTestTabletId());
 
   const auto leader_idx = CHECK_RESULT(cluster_->GetTabletLeaderIndex(tablet_id));
   const auto healthy_follower_idx = (leader_idx + 1) % 3;
@@ -2442,9 +2466,8 @@ TEST_F(TabletSplitExternalMiniClusterITest, CrashesAfterChildLogCopy) {
   ASSERT_OK(cluster_->SetFlagOnMasters("unresponsive_ts_rpc_retry_limit", "0"));
 
   CreateSingleTablet();
-  CHECK_OK(WriteRows());
-  ASSERT_OK(client_->FlushTables({table_->id()}, false, 30, false));
-  const auto tablet_id = CHECK_RESULT(GetOnlyTabletId());
+  CHECK_OK(WriteRowsAndFlush());
+  const auto tablet_id = CHECK_RESULT(GetOnlyTestTabletId());
 
   // We will fault one of the non-leader servers after it performs a WAL Log copy from parent to
   // the first child, but before it can mark the child as TABLET_DATA_READY.
@@ -2480,10 +2503,11 @@ TEST_F(TabletSplitExternalMiniClusterITest, CrashesAfterChildLogCopy) {
 }
 
 class TabletSplitRemoteBootstrapEnabledTest : public TabletSplitExternalMiniClusterITest {
-  std::vector<std::string> GetTserverFlags() const override {
-    return {
-      "--TEST_disable_post_split_tablet_rbs_check=true",
-    };
+ protected:
+  void SetFlags() override {
+    TabletSplitExternalMiniClusterITest::SetFlags();
+    mini_cluster_opt_.extra_tserver_flags.push_back(
+        "--TEST_disable_post_split_tablet_rbs_check=true");
   }
 };
 
@@ -2517,9 +2541,8 @@ TEST_F(TabletSplitRemoteBootstrapEnabledTest, TestSplitAfterFailedRbsCreatesDire
   };
 
   CreateSingleTablet();
-  ASSERT_OK(WriteRows());
-  ASSERT_OK(client_->FlushTables({table_->id()}, false, 30, false));
-  const auto tablet_id = CHECK_RESULT(GetOnlyTabletId());
+  ASSERT_OK(WriteRowsAndFlush());
+  const auto tablet_id = CHECK_RESULT(GetOnlyTestTabletId());
 
   const auto leader_idx = CHECK_RESULT(cluster_->GetTabletLeaderIndex(tablet_id));
   const auto leader = cluster_->tablet_server(leader_idx);
@@ -2588,7 +2611,7 @@ TEST_F(TabletSplitExternalMiniClusterITest, RemoteBootstrapsFromNodeWithUncommit
 
   CreateSingleTablet();
   const auto other_server_idx = *other_servers.begin();
-  const auto tablet_id = CHECK_RESULT(GetOnlyTabletId(other_server_idx));
+  const auto tablet_id = CHECK_RESULT(GetOnlyTestTabletId(other_server_idx));
 
   CHECK_OK(WriteRows());
   for (int i = 0; i < cluster_->num_tablet_servers(); i++) {
@@ -2637,6 +2660,116 @@ TEST_F(TabletSplitExternalMiniClusterITest, RemoteBootstrapsFromNodeWithUncommit
   CHECK_OK(server_to_kill->Restart());
 }
 
+class TabletSplitReplaceNodeITest : public TabletSplitExternalMiniClusterITest {
+ protected:
+  void SetFlags() override {
+    TabletSplitExternalMiniClusterITest::SetFlags();
+
+    for (const auto& tserver_flag : {
+        // We want to test behavior of the source tablet, so setting up to skip deleting it.
+        "--TEST_skip_deleting_split_tablets=true",
+        // Reduce follower_unavailable_considered_failed_sec, so offline tserver is evicted
+        // from Raft group faster.
+        "--follower_unavailable_considered_failed_sec=5",
+      }) {
+      mini_cluster_opt_.extra_tserver_flags.push_back(tserver_flag);
+    }
+
+    for (const auto& master_flag : {
+        // Should be less than follower_unavailable_considered_failed_sec, so load balancer
+        // doesn't go into infinite loop trying to add failed follower back.
+        "--tserver_unresponsive_timeout_ms=3000",
+        // To speed up load balancing:
+        // - Allow more concurrent adds/removes, so we deal with transaction status tablets
+        // faster.
+        "--load_balancer_max_concurrent_adds=10", "--load_balancer_max_concurrent_removals=10",
+        // - Allow more over replicated tablets, so temporary child tablets over replication
+        // doesn't block parent tablet move.
+        "--load_balancer_max_over_replicated_tablets=5",
+        // TODO: should be default behaviour after
+        // https://github.com/yugabyte/yugabyte-db/issues/10301 is fixed.
+        "--TEST_load_balancer_skip_inactive_tablets=false",
+      }) {
+      mini_cluster_opt_.extra_master_flags.push_back(master_flag);
+    }
+  }
+};
+
+TEST_F_EX(
+    TabletSplitExternalMiniClusterITest, ReplaceNodeForParentTablet, TabletSplitReplaceNodeITest) {
+  constexpr auto kNumRows = kDefaultNumRows;
+
+  CreateSingleTablet();
+  ASSERT_OK(WriteRowsAndFlush(kNumRows));
+  const auto source_tablet_id = ASSERT_RESULT(GetOnlyTestTabletId());
+  LOG(INFO) << "Source tablet ID: " << source_tablet_id;
+
+  auto* offline_ts = cluster_->tablet_server(0);
+  offline_ts->Shutdown();
+  LOG(INFO) << "Shutdown completed for tserver: " << offline_ts->uuid();
+  const auto offline_ts_id = offline_ts->uuid();
+
+  ASSERT_OK(SplitTablet(source_tablet_id));
+  ASSERT_OK(WaitForTablets(3));
+
+  ASSERT_OK(cluster_->AddTabletServer());
+  const auto new_ts_id = cluster_->tablet_server(3)->uuid();
+  LOG(INFO) << "Started new tserver: " << new_ts_id;
+
+  ASSERT_OK(cluster_->WaitForTabletServerCount(4, 20s));
+  LOG(INFO) << "New tserver has been added: " << new_ts_id;
+
+  const auto deadline = CoarseMonoClock::Now() + 30s * kTimeMultiplier;
+  std::set<TabletServerId> source_tablet_replicas;
+  auto s = LoggedWait(
+      [this, &deadline, &source_tablet_id, &offline_ts_id, &new_ts_id, &source_tablet_replicas] {
+        const MonoDelta remaining_timeout = deadline - CoarseMonoClock::Now();
+        if (remaining_timeout.IsNegative()) {
+          return false;
+        }
+        master::TabletLocationsPB resp;
+        const auto s = itest::GetTabletLocations(
+            cluster_->GetLeaderMasterProxy(), source_tablet_id, remaining_timeout, &resp);
+        if (!s.ok()) {
+          return false;
+        }
+        source_tablet_replicas.clear();
+        for (auto& replica : resp.replicas()) {
+          source_tablet_replicas.insert(replica.ts_info().permanent_uuid());
+        }
+        if (source_tablet_replicas.size() != 3) {
+          return false;
+        }
+        if (source_tablet_replicas.count(offline_ts_id) > 0) {
+          // We don't expect source tablet to have replica on offline tserver.
+          return false;
+        }
+        return source_tablet_replicas.count(new_ts_id) > 0;
+      },
+      deadline,
+      Format("Waiting for source tablet $0 to be moved to ts-4 ($1)", source_tablet_id, new_ts_id));
+
+  ASSERT_TRUE(s.ok()) << s << ". Source tablet replicas: " << AsString(source_tablet_replicas);
+
+  // Wait for the split to be completed on all online tservers.
+  for (auto ts_idx = 0; ts_idx < cluster_->num_tablet_servers(); ++ts_idx) {
+    if (ts_idx == 3) {
+      // Skip new TS, because of https://github.com/yugabyte/yugabyte-db/issues/10301.
+      // TODO(tsplit): remove after it is fixed.
+      continue;
+    }
+    if (cluster_->tablet_server(ts_idx)->IsProcessAlive()) {
+      ASSERT_OK(WaitForTablets(3, ts_idx));
+    }
+  }
+
+  // Restarting offline_ts, because ClusterVerifier requires all tservers to be online.
+  ASSERT_OK(offline_ts->Start());
+
+  // TODO(tsplit): remove after https://github.com/yugabyte/yugabyte-db/issues/10301 is fixed.
+  DontVerifyClusterBeforeNextTearDown();
+}
+
 namespace {
 
 PB_ENUM_FORMATTERS(IsolationLevel);
diff --git a/src/yb/master/cluster_balance.cc b/src/yb/master/cluster_balance.cc
index 6585fe825612..4a18f25e3129 100644
--- a/src/yb/master/cluster_balance.cc
+++ b/src/yb/master/cluster_balance.cc
@@ -128,6 +128,11 @@ DEFINE_bool(load_balancer_drive_aware, true,
             "When LB decides to move a tablet from server A to B, on the target LB "
             "should select the tablet to move from most loaded drive.");
 
+// TODO(tsplit): make false by default or even remove flag after
+// https://github.com/yugabyte/yugabyte-db/issues/10301 is fixed.
+DEFINE_test_flag(
+    bool, load_balancer_skip_inactive_tablets, true, "Don't move inactive (hidden) tablets");
+
 namespace yb {
 namespace master {
 
@@ -1386,7 +1391,7 @@ Result<TabletInfos> ClusterLoadBalancer::GetTabletsForTable(const TableId& table
         table_uuid);
   }
 
-  return table_info->GetTablets();
+  return table_info->GetTablets(IncludeInactive(!FLAGS_TEST_load_balancer_skip_inactive_tablets));
 }
 
 const TableInfoMap& ClusterLoadBalancer::GetTableMap() const {
diff --git a/src/yb/tablet/operations/split_operation.cc b/src/yb/tablet/operations/split_operation.cc
index 29af73942dac..d2e24ed301ab 100644
--- a/src/yb/tablet/operations/split_operation.cc
+++ b/src/yb/tablet/operations/split_operation.cc
@@ -71,12 +71,14 @@ bool SplitOperation::ShouldAllowOpAfterSplitTablet(const consensus::OperationTyp
       // We allow NO_OP, so old tablet can have leader changes in case of re-elections.
     case consensus::NO_OP: FALLTHROUGH_INTENDED;
       // We allow SNAPSHOT_OP, so old tablet can be restored.
-    case consensus::SNAPSHOT_OP:
+    case consensus::SNAPSHOT_OP: FALLTHROUGH_INTENDED;
+      // Allow CHANGE_CONFIG_OP, so the old tablet replicas can be moved between tservers while we
+      // keep the tablet available.
+    case consensus::CHANGE_CONFIG_OP:
       return true;
     case consensus::UNKNOWN_OP: FALLTHROUGH_INTENDED;
     case consensus::WRITE_OP: FALLTHROUGH_INTENDED;
     case consensus::CHANGE_METADATA_OP: FALLTHROUGH_INTENDED;
-    case consensus::CHANGE_CONFIG_OP: FALLTHROUGH_INTENDED;
     case consensus::HISTORY_CUTOFF_OP: FALLTHROUGH_INTENDED;
     case consensus::UPDATE_TRANSACTION_OP: FALLTHROUGH_INTENDED;
     case consensus::TRUNCATE_OP: FALLTHROUGH_INTENDED;
diff --git a/src/yb/tablet/tablet_bootstrap.cc b/src/yb/tablet/tablet_bootstrap.cc
index f99aad8b5d10..69960aefaf07 100644
--- a/src/yb/tablet/tablet_bootstrap.cc
+++ b/src/yb/tablet/tablet_bootstrap.cc
@@ -360,6 +360,10 @@ struct ReplayDecision {
   // This is true for transaction update operations that have already been applied to the regular
   // RocksDB but not to the intents RocksDB.
   AlreadyAppliedToRegularDB already_applied_to_regular_db = AlreadyAppliedToRegularDB::kFalse;
+
+  std::string ToString() const {
+    return YB_STRUCT_TO_STRING(should_replay, already_applied_to_regular_db);
+  }
 };
 
 ReplayDecision ShouldReplayOperation(
@@ -374,6 +378,9 @@ ReplayDecision ShouldReplayOperation(
   if (index <= std::min(regular_flushed_index, intents_flushed_index)) {
     // Never replay anyting that is flushed to both regular and intents RocksDBs in a transactional
     // table.
+    VLOG_WITH_FUNC(3) << "index: " << index << " "
+                      << "regular_flushed_index: " << regular_flushed_index
+                      << " intents_flushed_index: " << intents_flushed_index;
     return {false};
   }
 
@@ -381,18 +388,27 @@ ReplayDecision ShouldReplayOperation(
     if (txn_status == TransactionStatus::APPLYING &&
         intents_flushed_index < index && index <= regular_flushed_index) {
       // Intents were applied/flushed to regular RocksDB, but not flushed into the intents RocksDB.
+      VLOG_WITH_FUNC(3) << "index: " << index << " "
+                        << "regular_flushed_index: " << regular_flushed_index
+                        << " intents_flushed_index: " << intents_flushed_index;
       return {true, AlreadyAppliedToRegularDB::kTrue};
     }
     // For other types of transaction updates, we ignore them if they have been flushed to the
     // regular RocksDB.
+    VLOG_WITH_FUNC(3) << "index: " << index << " > "
+                      << "regular_flushed_index: " << regular_flushed_index;
     return {index > regular_flushed_index};
   }
 
   if (op_type == consensus::WRITE_OP && write_op_has_transaction) {
     // Write intents that have not been flushed into the intents DB.
+    VLOG_WITH_FUNC(3) << "index: " << index << " > "
+                      << "intents_flushed_index: " << intents_flushed_index;
     return {index > intents_flushed_index};
   }
 
+  VLOG_WITH_FUNC(3) << "index: " << index << " > "
+                    << "regular_flushed_index: " << regular_flushed_index;
   return {index > regular_flushed_index};
 }
 
@@ -920,6 +936,7 @@ class TabletBootstrap {
 
     if (tablet_->metadata()->tablet_data_state() == TabletDataState::TABLET_DATA_SPLIT_COMPLETED) {
       // Ignore SPLIT_OP if tablet has been already split.
+      VLOG_WITH_PREFIX_AND_FUNC(1) << "Tablet has been already split.";
       return Status::OK();
     }
 
@@ -971,7 +988,7 @@ class TabletBootstrap {
         WriteOpHasTransaction(*replicate));
 
     HandleRetryableRequest(*replicate, entry_time);
-
+    VLOG_WITH_PREFIX_AND_FUNC(3) << "decision: " << AsString(decision);
     if (decision.should_replay) {
       const auto status = PlayAnyRequest(replicate, decision.already_applied_to_regular_db);
       if (!status.ok()) {
diff --git a/src/yb/util/test_util.cc b/src/yb/util/test_util.cc
index 970b815af8d1..341808ff882b 100644
--- a/src/yb/util/test_util.cc
+++ b/src/yb/util/test_util.cc
@@ -298,6 +298,20 @@ Status Wait(const std::function<Result<bool>()>& condition,
               delay_multiplier, max_delay);
 }
 
+Status LoggedWait(
+    const std::function<Result<bool>()>& condition,
+    CoarseTimePoint deadline,
+    const string& description,
+    MonoDelta initial_delay,
+    double delay_multiplier,
+    MonoDelta max_delay) {
+  LOG(INFO) << description << " - started";
+  auto status =
+      Wait(condition, deadline, description, initial_delay, delay_multiplier, max_delay);
+  LOG(INFO) << description << " - completed: " << status;
+  return status;
+}
+
 // Waits for the given condition to be true or until the provided timeout has expired.
 Status WaitFor(const std::function<Result<bool>()>& condition,
                MonoDelta timeout,
@@ -317,8 +331,9 @@ Status LoggedWaitFor(
     double delay_multiplier,
     MonoDelta max_delay) {
   LOG(INFO) << description << " - started";
-  auto status = WaitFor(condition, timeout, description, initial_delay);
-  LOG(INFO) << description << " - completed: " << yb::ToString(status);
+  auto status =
+      WaitFor(condition, timeout, description, initial_delay, delay_multiplier, max_delay);
+  LOG(INFO) << description << " - completed: " << status;
   return status;
 }
 
diff --git a/src/yb/util/test_util.h b/src/yb/util/test_util.h
index bfae0cefc5c0..303ed26df49b 100644
--- a/src/yb/util/test_util.h
+++ b/src/yb/util/test_util.h
@@ -207,6 +207,14 @@ CHECKED_STATUS Wait(
     double delay_multiplier = test_util::kDefaultWaitDelayMultiplier,
     MonoDelta max_delay = MonoDelta::FromMilliseconds(test_util::kDefaultMaxWaitDelayMs));
 
+CHECKED_STATUS LoggedWait(
+    const std::function<Result<bool>()>& condition,
+    CoarseTimePoint deadline,
+    const string& description,
+    MonoDelta initial_delay = MonoDelta::FromMilliseconds(test_util::kDefaultInitialWaitMs),
+    double delay_multiplier = test_util::kDefaultWaitDelayMultiplier,
+    MonoDelta max_delay = MonoDelta::FromMilliseconds(test_util::kDefaultMaxWaitDelayMs));
+
 // Waits for the given condition to be true or until the provided timeout has expired.
 CHECKED_STATUS WaitFor(
     const std::function<Result<bool>()>& condition,
