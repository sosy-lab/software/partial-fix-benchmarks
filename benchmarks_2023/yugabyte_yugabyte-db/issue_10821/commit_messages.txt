[YSQL] [#12767] Send list of aborted sub txns to the status tablet during a savepoint rollback + 2 bug fixes

Summary:
As part of this change, YSQL will complete a savepoint rollback only after it
sends the list of aborted sub txns to the transaction status tablet using the
UpdateTransactionRequestPB heartbeat.

This is to ensure that after rolling back some part of transaction T1, any
statement in another transaction T2 should be able to see that the rolled back
provisional writes are invalid/ aborted.

Other bug fixes -

(1) a minor bug in the 4fb1676 -- the list of aborted sub txns, which is sent
periodically by YSQL, isn't always updated in the status tablet. The aborted
list was accepted by the transaction coordinator only if
`data.state.aborted().set_size() > aborted_.set_size()`, but the set_size() here
isn't the same as the number of aborted sub txns (see AbortedSubTransactionSet).

(2) Intents written for explicit row locks didn't persist sub txn id with them. This is
because for read queries with row level locks, the `write_batch` didn't have
sub txn id set. This is now done in `ReadQuery::DoPerform()`.

Test Plan:
Jenkins: urgent

./yb_build.sh --java-test org.yb.pgsql.TestPgConflictWithAbortedSubTxns#ensureNoConflictsWithAbortedSubTxns
./yb_build.sh --java-test org.yb.pgsql.TestPgIsolationRegress#isolationRegress
    - Enabled aborted-keyrevoke and delete-abort-savept-2 in the isolation test schedule

Reviewers: esheng, rsami

Reviewed By: esheng

Subscribers: zyu, bogdan, yql

Differential Revision: https://phabricator.dev.yugabyte.com/D17358
[#10821] YSQL: Cache sys catalog read request on local t-server side [WIP]

Summary: TBD

Test Plan: Jenkins

Reviewers: sergei, mihnea, jason

Subscribers: yql, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17824
[#10821] YSQL: Cache sys catalog read request on local t-server side

Summary:
On cache refresh YSQL sends RPC to a master which reads multiple system tables.
Nowadays they are:
- pg_database
- pg_class
- pg_attribute
- pg_opclass
- pg_am
- pg_amproc
- pg_index
- pg_rewrite
- pg_attrdef
- pg_constraint
- pg_partitioned_table
- pg_type
- pg_namespace
- pg_authid

All active postgres connections send the same RPC to master on cache refresh. As long as YSQL sends RPC via local t-server proxy it is reasonable to cache the response on this RPC on local t-server and reuse data from cache instead of resending same RPC to a master over and over again (i.e. 1 request per each active connection).

`PgResponseCache` is the `LRUCache` which stored in the `PgClientService`.
Key in this cache is a text representation of RPC request. But special field in this request must be nullified (cleared out) first. Because the value of this field is unique for each request and it doesn't affect the response. This field is `PgsqlReadRequestPB::stmt_id`.

Also cacheable request must have additional field which will contains info about system catalog version. Because in case of catalog changes it is necessary to send new request to a master instead of using data from the local t-server cache as this data is outdated.

**Note:**
Only request which preloads the sys catalog is cacheable now. Requests which loads sys catalog records on-demand are not cacheable. The necessity of caching these requests will be estimated in future.

Test Plan:
Jenkins

```
./yb_build.sh --gtest_filter PgCatalogPerfTest.ResponseCacheEfficiency
```

Reviewers: mihnea, jason, myang, sergei

Reviewed By: myang, sergei

Subscribers: plee, smishra, yql, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17824
[BACKPORT 2.16][#10821] YSQL: Cache sys catalog read request on local t-server side

Summary:
On cache refresh YSQL sends RPC to a master which reads multiple system tables.
Nowadays they are:
- pg_database
- pg_class
- pg_attribute
- pg_opclass
- pg_am
- pg_amproc
- pg_index
- pg_rewrite
- pg_attrdef
- pg_constraint
- pg_partitioned_table
- pg_type
- pg_namespace
- pg_authid

All active postgres connections send the same RPC to master on cache refresh. As long as YSQL sends RPC via local t-server proxy it is reasonable to cache the response on this RPC on local t-server and reuse data from cache instead of resending same RPC to a master over and over again (i.e. 1 request per each active connection).

`PgResponseCache` is the `LRUCache` which stored in the `PgClientService`.
Key in this cache is a text representation of RPC request. But special field in this request must be nullified (cleared out) first. Because the value of this field is unique for each request and it doesn't affect the response. This field is `PgsqlReadRequestPB::stmt_id`.

Also cacheable request must have additional field which will contains info about system catalog version. Because in case of catalog changes it is necessary to send new request to a master instead of using data from the local t-server cache as this data is outdated.

**Note:**
Only request which preloads the sys catalog is cacheable now. Requests which loads sys catalog records on-demand are not cacheable. The necessity of caching these requests will be estimated in future.

Original commit: 7672a48821139e5d3b9e92da22824244c700d7ea / D17824

Test Plan:
Jenkins: rebase: 2.16

```
./yb_build.sh --gtest_filter PgCatalogPerfTest.ResponseCacheEfficiency
```

Reviewers: jason, sergei, mihnea, myang

Reviewed By: myang

Subscribers: smishra, plee, mihnea, yql, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D21452
[#10821] YSQL: Cache sys catalog read request on local t-server side

Summary:
On cache refresh YSQL sends RPC to a master which reads multiple system tables.
Nowadays they are:
- pg_database
- pg_class
- pg_attribute
- pg_opclass
- pg_am
- pg_amproc
- pg_index
- pg_rewrite
- pg_attrdef
- pg_constraint
- pg_partitioned_table
- pg_type
- pg_namespace
- pg_authid

All active postgres connections send the same RPC to master on cache refresh. As long as YSQL sends RPC via local t-server proxy it is reasonable to cache the response on this RPC on local t-server and reuse data from cache instead of resending same RPC to a master over and over again (i.e. 1 request per each active connection).

`PgResponseCache` is the `LRUCache` which stored in the `PgClientService`.
Key in this cache is a text representation of RPC request. But special field in this request must be nullified (cleared out) first. Because the value of this field is unique for each request and it doesn't affect the response. This field is `PgsqlReadRequestPB::stmt_id`.

Also cacheable request must have additional field which will contains info about system catalog version. Because in case of catalog changes it is necessary to send new request to a master instead of using data from the local t-server cache as this data is outdated.

**Note:**
Only request which preloads the sys catalog is cacheable now. Requests which loads sys catalog records on-demand are not cacheable. The necessity of caching these requests will be estimated in future.

Test Plan:
Jenkins

```
./yb_build.sh --gtest_filter PgCatalogPerfTest.ResponseCacheEfficiency
```

Reviewers: mihnea, jason, myang, sergei

Reviewed By: myang, sergei

Subscribers: plee, smishra, yql, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17824
