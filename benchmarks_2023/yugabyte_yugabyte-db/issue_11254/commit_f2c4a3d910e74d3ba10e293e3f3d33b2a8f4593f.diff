diff --git a/src/postgres/src/backend/access/yb_access/yb_scan.c b/src/postgres/src/backend/access/yb_access/yb_scan.c
index 5d5d13d5738c..47824d877e29 100644
--- a/src/postgres/src/backend/access/yb_access/yb_scan.c
+++ b/src/postgres/src/backend/access/yb_access/yb_scan.c
@@ -736,6 +736,97 @@ ybcSetupScanKeys(YbScanDesc ybScan, YbScanPlan scan_plan)
 	}
 }
 
+/* Return true if typid is one of the Object Identifier Types */
+static bool YbIsOidType(Oid typid) {
+	switch (typid)
+	{
+		case OIDOID:
+		case REGPROCOID:
+		case REGPROCEDUREOID:
+		case REGOPEROID:
+		case REGOPERATOROID:
+		case REGCLASSOID:
+		case REGTYPEOID:
+		case REGROLEOID:
+		case REGNAMESPACEOID:
+		case REGCONFIGOID:
+		case REGDICTIONARYOID:
+			return true;
+		default:
+			return false;
+	}
+}
+
+/*
+ * Return true if a scan key column type is compatible with value type. 'equal_strategy' is true
+ * for BTEqualStrategyNumber.
+ */
+static bool YbIsScanCompatible(Oid column_typid, Oid value_typid, bool equal_strategy) {
+	if (column_typid == value_typid)
+		return true;
+
+	switch (column_typid)
+	{
+		case INT2OID:
+			/*
+			 * If column c0 has INT2OID type and value type is INT4OID, the value may overflow
+			 * INT2OID. For example, where clause condition "c0 = 65539" would become "c0 = 3"
+			 * and will unnecessarily fetch a row with key of 3. This will not affect correctness
+			 * because at upper Postgres layer "c0 = 65539" will be applied again to filter out
+			 * this row. We prefer to bind scan key c0 to account for the common case where INT4OID
+			 * value does not overflow INT2OID, which happens in some system relation scan queries.
+			 */
+			return equal_strategy ? (value_typid == INT4OID || value_typid == INT8OID) : false;
+		case INT4OID:
+			return equal_strategy ? (value_typid == INT2OID || value_typid == INT8OID) :
+									value_typid == INT2OID;
+		case INT8OID:
+			return value_typid == INT2OID || value_typid == INT4OID;
+
+		case TEXTOID:
+		case BPCHAROID:
+		case VARCHAROID:
+			return value_typid == TEXTOID || value_typid == BPCHAROID || value_typid == VARCHAROID;
+
+		default:
+			if (YbIsOidType(column_typid) && YbIsOidType(value_typid))
+				return true;
+			/* Conservatively return false. */
+			return false;
+	}
+}
+
+/*
+ * We require compatible column type and value type to avoid misinterpreting the value Datum
+ * using a column type that can cause wrong scan results. Returns true if the column type
+ * and value type are compatible.
+ */
+static bool YbCheckScanTypes(YbScanDesc ybScan, YbScanPlan scan_plan, int i, bool is_hashed) {
+	/* We treat the hash code column as a special column with INT4 type */
+	Oid	atttypid = is_hashed ? INT4OID :
+		ybc_get_atttypid(scan_plan->bind_desc, scan_plan->bind_key_attnums[i]);
+	Assert(OidIsValid(atttypid));
+	Oid valtypid = ybScan->key[i].sk_subtype;
+	/*
+	 * Example: CREATE TABLE t1(c0 REAL, c1 TEXT, PRIMARY KEY(c0 asc));
+	 *          INSERT INTO t1(c0, c1) VALUES(0.4, 'SHOULD BE IN RESULT');
+	 *          SELECT ALL t1.c1 FROM t1 WHERE ((0.6)>(t1.c0));
+	 * Internally, c0 has float4 type, 0.6 has float8 type. If we bind 0.6 directly with
+	 * column c0, float8 0.6 will be misinterpreted as float4. However, casting to float4
+	 * may lose precision. Here we simply do not bind a key when there is a type mismatch
+	 * by leaving start_valid[idx] and end_valid[idx] as false. For the following cases
+	 * we assume that Postgres ensures there is no concern for type mismatch.
+	 * (1) value type is not a valid type id
+	 * (2) InvalidStrategy (for IS NULL)
+	 * (3) value type is a polymorphic pseudotype
+	 */
+	return !OidIsValid(valtypid) ||
+			ybScan->key[i].sk_strategy == InvalidStrategy ||
+			YbIsScanCompatible(atttypid, valtypid,
+				ybScan->key[i].sk_strategy == BTEqualStrategyNumber) ||
+			IsPolymorphicType(valtypid);
+}
+
 /* Use the scan-descriptor and scan-plan to setup binds for the queryplan */
 static void
 ybcBindScanKeys(YbScanDesc ybScan, YbScanPlan scan_plan) {
@@ -757,6 +848,8 @@ ybcBindScanKeys(YbScanDesc ybScan, YbScanPlan scan_plan) {
 			{
 				bool is_null = (ybScan->key[i].sk_flags & SK_ISNULL) == SK_ISNULL;
 
+				Assert(YbCheckScanTypes(ybScan, scan_plan, i, is_hashed));
+
 				ybcBindColumn(ybScan, scan_plan->bind_desc, scan_plan->bind_key_attnums[i],
 							  ybScan->key[i].sk_argument, is_null, is_hashed);
 			}
@@ -898,6 +991,9 @@ ybcBindScanKeys(YbScanDesc ybScan, YbScanPlan scan_plan) {
 		if (is_column_bound[idx])
 			continue;
 
+		if (!YbCheckScanTypes(ybScan, scan_plan, i, is_hashed))
+			continue;
+
 		switch (ybScan->key[i].sk_strategy)
 		{
 			case InvalidStrategy: /* fallthrough, c IS NULL -> c = NULL (checked above) */
@@ -934,6 +1030,7 @@ ybcBindScanKeys(YbScanDesc ybScan, YbScanPlan scan_plan) {
 						* workspace context.
 						*/
 					arrayval = DatumGetArrayTypeP(cur->sk_argument);
+					Assert(ybScan->key[i].sk_subtype == ARR_ELEMTYPE(arrayval));
 					/* We could cache this data, but not clear it's worth it */
 					get_typlenbyvalalign(ARR_ELEMTYPE(arrayval), &elmlen,
 											&elmbyval, &elmalign);
diff --git a/src/postgres/src/test/regress/expected/yb_index_scan.out b/src/postgres/src/test/regress/expected/yb_index_scan.out
index 5bc37df9c487..6c5af569ad14 100644
--- a/src/postgres/src/test/regress/expected/yb_index_scan.out
+++ b/src/postgres/src/test/regress/expected/yb_index_scan.out
@@ -128,14 +128,14 @@ SELECT * FROM pk_multi WHERE h = 1;
 (4 rows)
 
 EXPLAIN (COSTS OFF) SELECT * FROM pk_multi WHERE yb_hash_code(h) = yb_hash_code(1);
-                 QUERY PLAN                 
+                 QUERY PLAN
 --------------------------------------------
  Index Scan using pk_multi_pkey on pk_multi
    Index Cond: (yb_hash_code(h) = 4624)
 (2 rows)
 
 SELECT * FROM pk_multi WHERE yb_hash_code(h) = yb_hash_code(1);
- h | r |  v  
+ h | r |  v
 ---+---+-----
  1 | 3 | 1-3
  1 | 2 | 1-2
@@ -437,7 +437,7 @@ SELECT * FROM sc_desc_nl WHERE yb_hash_code(h) = yb_hash_code(1) AND r IS null;
 (1 row)
 
 EXPLAIN (COSTS OFF) SELECT * FROM sc_desc_nl WHERE yb_hash_code(h) = yb_hash_code(1) AND r IS null;
-                        QUERY PLAN                        
+                        QUERY PLAN
 ----------------------------------------------------------
  Index Scan using sc_desc_nl_h_r_idx on sc_desc_nl
    Index Cond: ((yb_hash_code(h) = 4624) AND (r IS NULL))
@@ -810,31 +810,31 @@ SELECT col4 FROM test WHERE col4 = 232 and col5 % 3 = 0;
 -- test index scans where the filter trivially rejects everything and
 -- no request should be sent to DocDB
 EXPLAIN (COSTS OFF, TIMING OFF, SUMMARY OFF, ANALYZE) SELECT * FROM test WHERE col3 = ANY('{}');
-                        QUERY PLAN                         
+                        QUERY PLAN
 -----------------------------------------------------------
  Index Scan using idx_col3 on test (actual rows=0 loops=1)
    Index Cond: (col3 = ANY ('{}'::integer[]))
 (2 rows)
 
 SELECT * FROM test WHERE col3 = ANY('{}');
- pk | col2 | col3 | col4 | col5 | col6 | col7 | col8 | col9 
+ pk | col2 | col3 | col4 | col5 | col6 | col7 | col8 | col9
 ----+------+------+------+------+------+------+------+------
 (0 rows)
 
 EXPLAIN (COSTS OFF, TIMING OFF, SUMMARY OFF, ANALYZE) SELECT * FROM test WHERE col3 = ANY('{NULL}');
-                        QUERY PLAN                         
+                        QUERY PLAN
 -----------------------------------------------------------
  Index Scan using idx_col3 on test (actual rows=0 loops=1)
    Index Cond: (col3 = ANY ('{NULL}'::integer[]))
 (2 rows)
 
 SELECT * FROM test WHERE col3 = ANY('{NULL}');
- pk | col2 | col3 | col4 | col5 | col6 | col7 | col8 | col9 
+ pk | col2 | col3 | col4 | col5 | col6 | col7 | col8 | col9
 ----+------+------+------+------+------+------+------+------
 (0 rows)
 
 EXPLAIN (COSTS OFF, TIMING OFF, SUMMARY OFF, ANALYZE) SELECT col3 FROM test WHERE col3 = ANY('{NULL}');
-                           QUERY PLAN                           
+                           QUERY PLAN
 ----------------------------------------------------------------
  Index Only Scan using idx_col3 on test (actual rows=0 loops=1)
    Index Cond: (col3 = ANY ('{NULL}'::integer[]))
@@ -842,19 +842,19 @@ EXPLAIN (COSTS OFF, TIMING OFF, SUMMARY OFF, ANALYZE) SELECT col3 FROM test WHER
 (3 rows)
 
 SELECT col3 FROM test WHERE col3 = ANY('{NULL}');
- col3 
+ col3
 ------
 (0 rows)
 
 EXPLAIN (COSTS OFF, TIMING OFF, SUMMARY OFF, ANALYZE) SELECT * FROM test WHERE col3 = ANY('{NULL, NULL}');
-                        QUERY PLAN                         
+                        QUERY PLAN
 -----------------------------------------------------------
  Index Scan using idx_col3 on test (actual rows=0 loops=1)
    Index Cond: (col3 = ANY ('{NULL,NULL}'::integer[]))
 (2 rows)
 
 SELECT * FROM test WHERE col3 = ANY('{NULL, NULL}');
- pk | col2 | col3 | col4 | col5 | col6 | col7 | col8 | col9 
+ pk | col2 | col3 | col4 | col5 | col6 | col7 | col8 | col9
 ----+------+------+------+------+------+------+------+------
 (0 rows)
 
@@ -897,3 +897,89 @@ select * from test where pk=25;
 ----+------+------+------+------+------+------+------+------
  25 |    2 |    2 |  777 |  223 |    2 | Bb   |    2 |  199
 (1 row)
+
+-- test index scan where the column type does not match value type
+CREATE TABLE pk_real(c0 REAL, PRIMARY KEY(c0 asc));
+INSERT INTO pk_real(c0) VALUES(0.4);
+EXPLAIN SELECT ALL pk_real.c0 FROM pk_real WHERE ((0.6)>(pk_real.c0));
+                                 QUERY PLAN
+----------------------------------------------------------------------------
+ Index Scan using pk_real_pkey on pk_real  (cost=0.00..4.11 rows=1 width=4)
+   Index Cond: ('0.6'::double precision > c0)
+(2 rows)
+
+SELECT ALL pk_real.c0 FROM pk_real WHERE ((0.6)>(pk_real.c0));
+ c0
+-----
+ 0.4
+(1 row)
+
+EXPLAIN SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = ANY(ARRAY[0.6, 0.4]);
+                                 QUERY PLAN
+----------------------------------------------------------------------------
+ Index Scan using pk_real_pkey on pk_real  (cost=0.00..4.11 rows=1 width=4)
+   Index Cond: (c0 = ANY ('{0.6,0.4}'::double precision[]))
+(2 rows)
+
+-- 0.4::FLOAT4 is not equal to 0.4::DOUBLE PRECISION
+SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = ANY(ARRAY[0.6, 0.4]);
+ c0
+----
+(0 rows)
+
+INSERT INTO pk_real(c0) VALUES(0.5);
+EXPLAIN SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = 0.5;
+                                 QUERY PLAN
+----------------------------------------------------------------------------
+ Index Scan using pk_real_pkey on pk_real  (cost=0.00..4.11 rows=1 width=4)
+   Index Cond: (c0 = '0.5'::double precision)
+(2 rows)
+
+-- 0.5::FLOAT4 is equal to 0.5::DOUBLE PRECISION
+SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = 0.5;
+ c0
+-----
+ 0.5
+(1 row)
+
+CREATE TABLE pk_smallint(c0 SMALLINT, PRIMARY KEY(c0 asc));
+INSERT INTO pk_smallint VALUES(123), (-123);
+EXPLAIN SELECT c0 FROM pk_smallint WHERE (65568 > c0);
+                                     QUERY PLAN
+------------------------------------------------------------------------------------
+ Index Scan using pk_smallint_pkey on pk_smallint  (cost=0.00..4.11 rows=1 width=2)
+   Index Cond: (65568 > c0)
+(2 rows)
+
+SELECT c0 FROM pk_smallint WHERE (65568 > c0);
+  c0
+------
+ -123
+  123
+(2 rows)
+
+EXPLAIN SELECT c0 FROM pk_smallint WHERE (c0 > -65539);
+                                     QUERY PLAN
+------------------------------------------------------------------------------------
+ Index Scan using pk_smallint_pkey on pk_smallint  (cost=0.00..4.11 rows=1 width=2)
+   Index Cond: (c0 > '-65539'::integer)
+(2 rows)
+
+SELECT c0 FROM pk_smallint WHERE (c0 > -65539);
+  c0
+------
+ -123
+  123
+(2 rows)
+
+EXPLAIN SELECT c0 FROM pk_smallint WHERE (c0 = ANY(ARRAY[-65539, 65568]));
+                                     QUERY PLAN
+------------------------------------------------------------------------------------
+ Index Scan using pk_smallint_pkey on pk_smallint  (cost=0.00..4.11 rows=1 width=2)
+   Index Cond: (c0 = ANY ('{-65539,65568}'::integer[]))
+(2 rows)
+
+SELECT c0 FROM pk_smallint WHERE (c0 = ANY(ARRAY[-65539, 65568]));
+ c0
+----
+(0 rows)
diff --git a/src/postgres/src/test/regress/sql/yb_index_scan.sql b/src/postgres/src/test/regress/sql/yb_index_scan.sql
index ec41c04f5ea0..ffc0db5669ad 100644
--- a/src/postgres/src/test/regress/sql/yb_index_scan.sql
+++ b/src/postgres/src/test/regress/sql/yb_index_scan.sql
@@ -260,3 +260,24 @@ select * from test where pk=17;
 explain select * from test where pk=25;
 select * from test where pk=25;
 
+-- test index scan where the column type does not match value type
+CREATE TABLE pk_real(c0 REAL, PRIMARY KEY(c0 asc));
+INSERT INTO pk_real(c0) VALUES(0.4);
+EXPLAIN SELECT ALL pk_real.c0 FROM pk_real WHERE ((0.6)>(pk_real.c0));
+SELECT ALL pk_real.c0 FROM pk_real WHERE ((0.6)>(pk_real.c0));
+EXPLAIN SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = ANY(ARRAY[0.6, 0.4]);
+-- 0.4::FLOAT4 is not equal to 0.4::DOUBLE PRECISION
+SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = ANY(ARRAY[0.6, 0.4]);
+INSERT INTO pk_real(c0) VALUES(0.5);
+EXPLAIN SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = 0.5;
+-- 0.5::FLOAT4 is equal to 0.5::DOUBLE PRECISION
+SELECT ALL pk_real.c0 FROM pk_real WHERE pk_real.c0 = 0.5;
+
+CREATE TABLE pk_smallint(c0 SMALLINT, PRIMARY KEY(c0 asc));
+INSERT INTO pk_smallint VALUES(123), (-123);
+EXPLAIN SELECT c0 FROM pk_smallint WHERE (65568 > c0);
+SELECT c0 FROM pk_smallint WHERE (65568 > c0);
+EXPLAIN SELECT c0 FROM pk_smallint WHERE (c0 > -65539);
+SELECT c0 FROM pk_smallint WHERE (c0 > -65539);
+EXPLAIN SELECT c0 FROM pk_smallint WHERE (c0 = ANY(ARRAY[-65539, 65568]));
+SELECT c0 FROM pk_smallint WHERE (c0 = ANY(ARRAY[-65539, 65568]));
