[YSQL] ysql_dump fails when yb_enable_read_committed_isolation is set to true
The error comes from `check_XactIsoLevel` (variable.c). It does make sense to block the current isolation level in presence of an outstanding subtransaction because user's intention is unclear, especially for the internal uses.

The subtransaction that is triggering the error is created by the code that takes care of auto read restart in the read committed isolation level.
```
(lldb) Process 97191 stopped
* thread #1, queue = 'com.apple.main-thread', stop reason = breakpoint 2.1
    frame #0: 0x0000000102ab7612 postgres`StartTransactionCommandInternal + 242 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/access/transam/xact.c:2961
   2958					 * BeginInternalSubTransaction() again, but it is simpler and less error-prone to just copy
   2959					 * the minimal required logic.
   2960					 */
-> 2961					BeginInternalSubTransactionForReadCommittedStatement();
    					^
   2962				}
   2963	
   2964				break;
Target 0: (postgres) stopped.
(lldb)  bt
bt
* thread #1, queue = 'com.apple.main-thread', stop reason = breakpoint 2.1
  * frame #0: 0x0000000102ab7612 postgres`StartTransactionCommandInternal + 242 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/access/transam/xact.c:2961
    frame #1: 0x0000000102ab751b postgres`StartTransactionCommand + 11 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/access/transam/xact.c:3012
    frame #2: 0x0000000102f59836 postgres`start_xact_command + 22 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:2530
    frame #3: 0x0000000102f5c7e5 postgres`exec_simple_query + 133 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:973
    frame #4: 0x0000000102f5b445 postgres`yb_exec_simple_query_impl + 21 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:4576
    frame #5: 0x0000000102f5b371 postgres`yb_exec_query_wrapper + 321 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:4557
    frame #6: 0x0000000102f57005 postgres`yb_exec_simple_query + 69 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:4591
    frame #7: 0x0000000102f55e2b postgres`PostgresMain + 2907 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:5199
    frame #8: 0x0000000102e86964 postgres`BackendRun + 836 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/postmaster/postmaster.c:4470
    frame #9: 0x0000000102e85b52 postgres`BackendStartup + 626 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/postmaster/postmaster.c:4136
    frame #10: 0x0000000102e8469c postgres`ServerLoop + 956 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/postmaster/postmaster.c:1754
    frame #11: 0x0000000102e8184a postgres`PostmasterMain + 6906 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/postmaster/postmaster.c:1417
    frame #12: 0x0000000102d88154 postgres`PostgresServerProcessMain + 772 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/main/main.c:234
    frame #13: 0x0000000102d88582 postgres`main + 34
    frame #14: 0x00007fff20527f3d libdyld.dylib`start + 1
(lldb) f 3
f 3
frame #3: 0x0000000102f5c7e5 postgres`exec_simple_query + 133 at /Users/mtakahara/code/yugabyte-db3/src/postgres/src/backend/tcop/postgres.c:973
   970 		 * one of those, else bad things will happen in xact.c. (Note that this
   971 		 * will normally change current memory context.)
   972 		 */
-> 973 		start_xact_command();
    		^
   974 	
   975 		/*
   976 		 * Zap any pre-existing unnamed statement.  (While not strictly necessary,
(lldb) p query_string
p query_string
(const char *) $0 = 0x00000001139e6120 "SET TRANSACTION ISOLATION LEVEL SERIALIZABLE, READ ONLY, DEFERRABLE"
```

@pkj415 I suspect read restart is only relevant to DML statements (SELECT/INSERT/UPDATE/DELETE). Can we limit the scope of the "read committed handling" to those statements?  Or at least not to do it for the SET TRANSACTION command?
When yb_enable_read_committed_isolation is set to true, the user can't use the SET TRANSACTION command.

A repro case with ysqlsh:
```
yugabyte(54068)# BEGIN;
BEGIN
yugabyte(54068)# SET TRANSACTION ISOLATION LEVEL SERIALIZABLE, READ ONLY, DEFERRABLE;
ERROR:  SET TRANSACTION ISOLATION LEVEL must not be called in a subtransaction
yugabyte(54068)# 
```
Facts:
* The current transaction mode cannot be changed (i.e.: SET TRANSACTION) within a subtransaction.
* The subtransaction that is causing the error in both ysql_dump and the SET TRANSACTION command above was introduced by the change below: 
```
commit 9f2cc7fe3730025d1fd0a56517cebde362e3ab65
Author: Piyush Jain <piyushjain1996@gmail.com>
Date:   2022-02-02 09:34:56 -0800

    [#9468] YSQL: Ensure clients don't see serialization errors in READ COMMITTED isolation (Part-3)
```
* According to the postgres doc: [The transaction isolation level cannot be changed after the first query or data-modification statement (SELECT, INSERT, DELETE, UPDATE, FETCH, or COPY) of a transaction has been executed.](https://www.postgresql.org/docs/11/sql-set-transaction.html) So, it is okay to start the subtransaction just before executing one of those statements. Because the current transaction mode can no longer be changed after that. 

We must delay starting the subtransaction for read restart until just before execution of the first, and either avoid starting another one in subsequent executions or end it after each.

cc: @pkj415 @m-iancu 
A quick fix is in to unblock this, but keeping this issue open to track the right fix.
Closing this since the issue of ysql_dump is fixed via a hack right now. The real underlying issue of supporting "SET TRANSACTION ISOLATION LEVEL..." after "BEGIN;" if yb_enable_read_committed_isolation=true, will be tracked by #12494.