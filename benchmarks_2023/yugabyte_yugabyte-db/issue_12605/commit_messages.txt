[PLAT-3625] Filter target universes dropdown to contain only ready/good universes

Summary:
**Context**
While creating/configuring xcluster replication,
the target universes dropdown list shows all customer universes,
It would be nice if we only suggest universes in "good" state.
This would help  users avoid creating replication on a universe
currently processing another task
JIRA: https://yugabyte.atlassian.net/browse/PLAT-3625

**Change**
Add universe status to the universe list in the ConfigureReplicationModal.
Filter for only universes with "good" status

Test Plan:
- Create 3 universes
{F26167}
- Navigate to the replication tab on universe details for
  any of the 3 universes
{F26168}
- Configure a replication and observe that 2 other universes are
  suggested in the dropdown
{F26169}
- Pause one of the other universes and attempt to configure a replication again
- Observe that only the universe in "ready" state shows up in the dropdown
{F26170}

Reviewers: sshevchenko, lsangappa, asathyan, andrew, kkannan

Reviewed By: kkannan

Subscribers: jenkins-bot

Differential Revision: https://phabricator.dev.yugabyte.com/D17215
[#12605] YSQL: Prevent non-superusers from reassigning objects owned by superusers.

Summary: Other than superusers, no other user should ever find a need to reassign objects owned by superusers.

Test Plan: ybd --java-test org.yb.pgsql.TestPgRegressMisc

Reviewers: smishra, fizaa

Reviewed By: fizaa

Subscribers: zyu, myang, yql

Differential Revision: https://phabricator.dev.yugabyte.com/D17113
[BACKPORT 2.12][#12605] YSQL: Prevent non-superusers from reassigning objects owned by superusers.

Summary:
Other than superusers, no other user should ever find a need to reassign objects owned by superusers.

Original commit: D17113 / 13b2ff412f7acac3428fd9338c6a6878b4b69fa6

Test Plan: ybd --java-test org.yb.pgsql.TestPgRegressMisc

Reviewers: fizaa, smishra

Reviewed By: smishra

Subscribers: yql, myang, zyu

Differential Revision: https://phabricator.dev.yugabyte.com/D17338
[BACKPORT 2.14][#12605] YSQL: Prevent non-superusers from reassigning objects owned by superusers.

Summary:
Other than superusers, no other user should ever find a need to reassign objects owned by superusers.

Original commit: D17113 / 13b2ff412f7acac3428fd9338c6a6878b4b69fa6

Test Plan: ybd --java-test org.yb.pgsql.TestPgRegressMisc

Reviewers: fizaa, smishra

Reviewed By: smishra

Subscribers: yql, myang, zyu

Differential Revision: https://phabricator.dev.yugabyte.com/D17520
