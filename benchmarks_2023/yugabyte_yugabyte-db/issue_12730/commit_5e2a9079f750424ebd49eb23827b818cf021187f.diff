diff --git a/ent/src/yb/integration-tests/cdcsdk_ysql-test.cc b/ent/src/yb/integration-tests/cdcsdk_ysql-test.cc
index 25e56edce453..71cc27b1d8ad 100644
--- a/ent/src/yb/integration-tests/cdcsdk_ysql-test.cc
+++ b/ent/src/yb/integration-tests/cdcsdk_ysql-test.cc
@@ -63,6 +63,7 @@
 #include "yb/tserver/ts_tablet_manager.h"
 #include "yb/tserver/tserver_admin.proxy.h"
 
+#include "yb/util/enums.h"
 #include "yb/util/monotime.h"
 #include "yb/util/random_util.h"
 #include "yb/util/result.h"
@@ -80,6 +81,8 @@ DECLARE_bool(enable_update_local_peer_min_index);
 DECLARE_int32(update_min_cdc_indices_interval_secs);
 DECLARE_bool(stream_truncate_record);
 DECLARE_int32(cdc_state_checkpoint_update_interval_ms);
+DECLARE_int32(update_metrics_interval_ms);
+DECLARE_uint64(log_segment_size_bytes);
 
 namespace yb {
 
@@ -111,6 +114,8 @@ using rpc::RpcController;
 namespace cdc {
 namespace enterprise {
 
+YB_DEFINE_ENUM(IntentCountCompareOption, (GreaterThanOrEqualTo)(GreaterThan)(EqualTo));
+
 class CDCSDKYsqlTest : public CDCSDKTestBase {
  public:
   struct ExpectedRecord {
@@ -748,6 +753,32 @@ class CDCSDKYsqlTest : public CDCSDKTestBase {
 
     return resp;
   }
+
+  void PollForIntentCount(const int64& min_expected_num_intents, const uint32_t& tserver_index,
+                          const IntentCountCompareOption intentCountCompareOption,
+                          int64* num_intents) {
+    ASSERT_OK(WaitFor(
+      [this, &num_intents, &min_expected_num_intents, &tserver_index,
+       &intentCountCompareOption]() -> Result<bool> {
+        auto status = GetIntentCounts(tserver_index, num_intents);
+        if (!status.ok()) {
+          return false;
+        }
+
+        switch (intentCountCompareOption) {
+          case IntentCountCompareOption::GreaterThan:
+            return (*num_intents > min_expected_num_intents);
+          case IntentCountCompareOption::GreaterThanOrEqualTo:
+            return (*num_intents >= min_expected_num_intents);
+          case IntentCountCompareOption::EqualTo:
+            return (*num_intents == min_expected_num_intents);
+        }
+
+        return false;
+      },
+      MonoDelta::FromSeconds(120),
+      "Getting Number of intents"));
+  }
 };
 
 TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestBaseFunctions)) {
@@ -1473,7 +1504,7 @@ TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestCheckPointPersistencyNodeRest
         // transaction participant tablet peer.
         ASSERT_EQ(
             peer->cdc_sdk_min_checkpoint_op_id(),
-            peer->tablet()->transaction_participant()->TEST_GetRetainOpId());
+            peer->tablet()->transaction_participant()->GetRetainOpId());
       }
     }
   }
@@ -1497,7 +1528,7 @@ TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestCleanupSingleStreamSingleTser
   ASSERT_FALSE(resp.has_error());
   EnableCDCServiceInAllTserver(1);
 
-  // insert some records in transaction.
+  // Insert some records in transaction.
   ASSERT_OK(WriteRowsHelper(0 /* start */, 100 /* end */, &test_cluster_, true));
   ASSERT_OK(test_client()->FlushTables(
       {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
@@ -1591,7 +1622,7 @@ TEST_F(
   ASSERT_FALSE(resp_2.has_error());
   EnableCDCServiceInAllTserver(3);
 
-  // insert some records in transaction.
+  // Insert some records in transaction.
   ASSERT_OK(WriteRowsHelper(0 /* start */, 100 /* end */, &test_cluster_, true));
   ASSERT_OK(test_client()->FlushTables(
       {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
@@ -1799,14 +1830,13 @@ TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestMultpleActiveStreamOnSameTabl
         while (i < test_cluster()->num_tablet_servers()) {
           for (const auto& peer : test_cluster()->GetTabletPeers(i)) {
             if (peer->tablet_id() == tablets[0].tablet_id()) {
-              if (peer->tablet()->transaction_participant()->TEST_GetRetainOpId() !=
-                  min_checkpoint) {
+              if (peer->tablet()->transaction_participant()->GetRetainOpId() != min_checkpoint) {
                 SleepFor(MonoDelta::FromMilliseconds(2));
               } else {
                 i += 1;
                 LOG(INFO) << "In tserver: " << i
                           << " tablet peer have transaction_participant op_id set as: "
-                          << peer->tablet()->transaction_participant()->TEST_GetRetainOpId();
+                          << peer->tablet()->transaction_participant()->GetRetainOpId();
               }
               break;
             }
@@ -1903,16 +1933,16 @@ TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestActiveAndInActiveStreamOnSame
         while (i < test_cluster()->num_tablet_servers()) {
           for (const auto& peer : test_cluster()->GetTabletPeers(i)) {
             if (peer->tablet_id() == tablets[0].tablet_id()) {
-              if (peer->tablet()->transaction_participant()->TEST_GetRetainOpId() !=
+              if (peer->tablet()->transaction_participant()->GetRetainOpId() !=
                       overall_min_checkpoint &&
-                  peer->tablet()->transaction_participant()->TEST_GetRetainOpId() !=
+                  peer->tablet()->transaction_participant()->GetRetainOpId() !=
                       active_stream_checkpoint) {
                 SleepFor(MonoDelta::FromMilliseconds(2));
               } else {
                 i += 1;
                 LOG(INFO) << "In tserver: " << i
                           << " tablet peer have transaction_participant op_id set as: "
-                          << peer->tablet()->transaction_participant()->TEST_GetRetainOpId();
+                          << peer->tablet()->transaction_participant()->GetRetainOpId();
               }
               break;
             }
@@ -1923,6 +1953,304 @@ TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestActiveAndInActiveStreamOnSame
       MonoDelta::FromSeconds(60), "Waiting for all the tservers intent counts"));
 }
 
+TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestCheckPointPersistencyAllNodesRestart)) {
+  FLAGS_update_min_cdc_indices_interval_secs = 1;
+  FLAGS_cdc_state_checkpoint_update_interval_ms = 1;
+  ASSERT_OK(SetUpWithParams(3, 1, false));
+
+  const uint32_t num_tablets = 1;
+  auto table = ASSERT_RESULT(CreateTable(&test_cluster_, kNamespaceName, kTableName, num_tablets));
+  google::protobuf::RepeatedPtrField<master::TabletLocationsPB> tablets;
+  ASSERT_OK(test_client()->GetTablets(table, 0, &tablets, /* partition_list_version =*/nullptr));
+  ASSERT_EQ(tablets.size(), num_tablets);
+
+  TableId table_id = ASSERT_RESULT(GetTableId(&test_cluster_, kNamespaceName, kTableName));
+  CDCStreamId stream_id = ASSERT_RESULT(CreateDBStream(IMPLICIT));
+
+  auto resp = ASSERT_RESULT(SetCDCCheckpoint(stream_id, tablets));
+  ASSERT_FALSE(resp.has_error());
+
+  // Insert some records in transaction.
+  ASSERT_OK(WriteRowsHelper(0 /* start */, 100 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+
+  // Call get changes.
+  GetChangesResponsePB change_resp_1 = ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets));
+  uint32_t record_size = change_resp_1.cdc_sdk_proto_records_size();
+  LOG(INFO) << "Total records read by GetChanges call: " << record_size;
+  // Greater than 100 check because  we got records for BEGIN, COMMIT also.
+  ASSERT_GT(record_size, 100);
+
+  ASSERT_OK(WriteRowsHelper(100 /* start */, 200 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+
+  SleepFor(MonoDelta::FromSeconds(10));
+  // Call get changes.
+  GetChangesResponsePB change_resp_2 =
+      ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets, &change_resp_1.cdc_sdk_checkpoint()));
+  record_size = change_resp_2.cdc_sdk_proto_records_size();
+  ASSERT_GT(record_size, 100);
+  LOG(INFO) << "Total records read by second GetChanges call: " << record_size;
+
+  SleepFor(MonoDelta::FromSeconds(60));
+  std::map<const std::string, OpId> tablet_peer_to_cdc_min_checkpoint_op_id_map;
+  for (size_t i = 0; i < test_cluster()->num_tablet_servers(); ++i) {
+    for (const auto& peer : test_cluster()->GetTabletPeers(i)) {
+      if (peer->tablet_id() == tablets[0].tablet_id()) {
+        tablet_peer_to_cdc_min_checkpoint_op_id_map[peer->permanent_uuid()] =
+            peer->cdc_sdk_min_checkpoint_op_id();
+      }
+    }
+  }
+  LOG(INFO) << "Stored min checkpoint OpId for each tablet peer";
+
+  // Restart all the nodes.
+  SleepFor(MonoDelta::FromSeconds(1));
+  for (size_t i = 0; i < test_cluster()->num_tablet_servers(); ++i) {
+    test_cluster()->mini_tablet_server(i)->Shutdown();
+    ASSERT_OK(test_cluster()->mini_tablet_server(i)->Start());
+    ASSERT_OK(test_cluster()->mini_tablet_server(i)->WaitStarted());
+  }
+  LOG(INFO) << "All nodes restarted";
+
+  // Check the checkpoint info for all tservers - it should be valid.
+  for (size_t i = 0; i < test_cluster()->num_tablet_servers(); ++i) {
+    for (const auto& peer : test_cluster()->GetTabletPeers(i)) {
+      if (peer->tablet_id() == tablets[0].tablet_id()) {
+        // Checkpoint persisted in the RAFT logs should be same as in memory transaction
+        // participant tablet peer.
+        ASSERT_EQ(
+            peer->cdc_sdk_min_checkpoint_op_id(),
+            peer->tablet()->transaction_participant()->GetRetainOpId());
+        // The cdc_sdk_min_checkpoint_op_id should be the same as before restart.
+        ASSERT_EQ(
+            tablet_peer_to_cdc_min_checkpoint_op_id_map[peer->permanent_uuid()],
+            peer->cdc_sdk_min_checkpoint_op_id());
+      }
+    }
+  }
+}
+
+TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestIntentCountPersistencyAllNodesRestart)) {
+  FLAGS_update_min_cdc_indices_interval_secs = 1;
+  FLAGS_cdc_state_checkpoint_update_interval_ms = 1;
+  ASSERT_OK(SetUpWithParams(1, 1, false));
+
+  const uint32_t num_tablets = 1;
+  auto table = ASSERT_RESULT(CreateTable(&test_cluster_, kNamespaceName, kTableName, num_tablets));
+  google::protobuf::RepeatedPtrField<master::TabletLocationsPB> tablets;
+  ASSERT_OK(test_client()->GetTablets(table, 0, &tablets, /* partition_list_version =*/nullptr));
+  ASSERT_EQ(tablets.size(), num_tablets);
+
+  TableId table_id = ASSERT_RESULT(GetTableId(&test_cluster_, kNamespaceName, kTableName));
+  CDCStreamId stream_id = ASSERT_RESULT(CreateDBStream(IMPLICIT));
+
+  auto resp = ASSERT_RESULT(SetCDCCheckpoint(stream_id, tablets));
+  ASSERT_FALSE(resp.has_error());
+
+  // Insert some records in transaction.
+  ASSERT_OK(WriteRowsHelper(0 /* start */, 100 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+  GetChangesResponsePB change_resp_1 = ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets));
+  LOG(INFO) << "Number of records after first transaction: " << change_resp_1.records().size();
+  change_resp_1 =
+      ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets, &change_resp_1.cdc_sdk_checkpoint()));
+
+  ASSERT_OK(WriteRowsHelper(100 /* start */, 200 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+
+  ASSERT_OK(WriteRowsHelper(200 /* start */, 300 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+  SleepFor(MonoDelta::FromSeconds(10));
+
+  int64 initial_num_intents;
+  PollForIntentCount(1, 0, IntentCountCompareOption::GreaterThan, &initial_num_intents);
+
+  LOG(INFO) << "All nodes will be restarted";
+  for (size_t i = 0; i < test_cluster()->num_tablet_servers(); ++i) {
+    test_cluster()->mini_tablet_server(i)->Shutdown();
+    ASSERT_OK(test_cluster()->mini_tablet_server(i)->Start());
+    ASSERT_OK(test_cluster()->mini_tablet_server(i)->WaitStarted());
+  }
+  LOG(INFO) << "All nodes restarted";
+  SleepFor(MonoDelta::FromSeconds(60));
+
+  int64 num_intents_after_restart;
+  PollForIntentCount(
+      initial_num_intents, 0, IntentCountCompareOption::EqualTo, &num_intents_after_restart);
+  LOG(INFO) << "Number of intents after restart: " << num_intents_after_restart;
+  ASSERT_EQ(num_intents_after_restart, initial_num_intents);
+
+  GetChangesResponsePB change_resp_2 =
+      ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets, &change_resp_1.cdc_sdk_checkpoint()));
+  uint32_t record_size = change_resp_2.cdc_sdk_proto_records_size();
+  // We have run 2 transactions after the last call to "GetChangesFromCDC", thus we expect
+  // atleast 200 records if we call "GetChangesFromCDC" now.
+  LOG(INFO) << "Number of records after restart: " << record_size;
+  ASSERT_GE(record_size, 200);
+
+  // Now that there are no more transaction, and we have called "GetChangesFromCDC" already, there
+  // must be no more records or intents remaining.
+  GetChangesResponsePB change_resp_3 =
+      ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets, &change_resp_2.cdc_sdk_checkpoint()));
+  uint32_t final_record_size = change_resp_3.cdc_sdk_proto_records_size();
+  LOG(INFO) << "Number of recrods after no new transactions: " << final_record_size;
+  ASSERT_EQ(final_record_size, 0);
+
+  int64 final_num_intents;
+  PollForIntentCount(0, 0, IntentCountCompareOption::EqualTo, &final_num_intents);
+  ASSERT_EQ(0, final_num_intents);
+}
+
+TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestHighIntentCountPersistencyAllNodesRestart)) {
+  FLAGS_update_min_cdc_indices_interval_secs = 1;
+  FLAGS_cdc_state_checkpoint_update_interval_ms = 1;
+  FLAGS_log_segment_size_bytes = 100;
+  ASSERT_OK(SetUpWithParams(1, 1, false));
+
+  const uint32_t num_tablets = 1;
+  auto table = ASSERT_RESULT(CreateTable(&test_cluster_, kNamespaceName, kTableName, num_tablets));
+  google::protobuf::RepeatedPtrField<master::TabletLocationsPB> tablets;
+  ASSERT_OK(test_client()->GetTablets(table, 0, &tablets, /* partition_list_version =*/nullptr));
+  ASSERT_EQ(tablets.size(), num_tablets);
+
+  TableId table_id = ASSERT_RESULT(GetTableId(&test_cluster_, kNamespaceName, kTableName));
+  CDCStreamId stream_id = ASSERT_RESULT(CreateDBStream(IMPLICIT));
+
+  auto resp = ASSERT_RESULT(SetCDCCheckpoint(stream_id, tablets));
+  ASSERT_FALSE(resp.has_error());
+
+  // Insert some records in transaction.
+  ASSERT_OK(WriteRowsHelper(0 /* start */, 1 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+
+  ASSERT_OK(WriteRowsHelper(1, 75, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+
+  int64 initial_num_intents;
+  PollForIntentCount(1, 0, IntentCountCompareOption::GreaterThan, &initial_num_intents);
+  LOG(INFO) << "Number of intents before restart: " << initial_num_intents;
+
+  LOG(INFO) << "All nodes will be restarted";
+  for (size_t i = 0; i < test_cluster()->num_tablet_servers(); ++i) {
+    test_cluster()->mini_tablet_server(i)->Shutdown();
+    ASSERT_OK(test_cluster()->mini_tablet_server(i)->Start());
+    ASSERT_OK(test_cluster()->mini_tablet_server(i)->WaitStarted());
+  }
+  LOG(INFO) << "All nodes restarted";
+  SleepFor(MonoDelta::FromSeconds(60));
+
+  int64 num_intents_after_restart;
+  PollForIntentCount(initial_num_intents, 0, IntentCountCompareOption::EqualTo,
+                     &num_intents_after_restart);
+  LOG(INFO) << "Number of intents after restart: " << num_intents_after_restart;
+  ASSERT_EQ(num_intents_after_restart, initial_num_intents);
+}
+
+TEST_F(CDCSDKYsqlTest, YB_DISABLE_TEST_IN_TSAN(TestIntentCountPersistencyRemoteBootstrap)) {
+  FLAGS_update_min_cdc_indices_interval_secs = 1;
+  FLAGS_update_metrics_interval_ms = 1;
+  FLAGS_cdc_state_checkpoint_update_interval_ms = 1;
+  ASSERT_OK(SetUpWithParams(3, 1, false));
+
+  const uint32_t num_tablets = 1;
+  auto table = ASSERT_RESULT(CreateTable(&test_cluster_, kNamespaceName, kTableName, num_tablets));
+  google::protobuf::RepeatedPtrField<master::TabletLocationsPB> tablets;
+  ASSERT_OK(test_client()->GetTablets(table, 0, &tablets, /* partition_list_version =*/nullptr));
+  ASSERT_EQ(tablets.size(), num_tablets);
+
+  TableId table_id = ASSERT_RESULT(GetTableId(&test_cluster_, kNamespaceName, kTableName));
+  CDCStreamId stream_id = ASSERT_RESULT(CreateDBStream(IMPLICIT));
+
+  auto resp = ASSERT_RESULT(SetCDCCheckpoint(stream_id, tablets));
+  ASSERT_FALSE(resp.has_error());
+
+  for (int i = 0; i < 2; ++i) {
+    ASSERT_OK(test_cluster()->AddTabletServer());
+    ASSERT_OK(test_cluster()->WaitForAllTabletServers());
+    LOG(INFO) << "Added new TServer to test cluster";
+  }
+
+  size_t leader_index_pre_shutdown = 0;
+  for (auto replica : tablets[0].replicas()) {
+    if (replica.role() == PeerRole::LEADER) {
+      for (size_t i = 0; i < test_cluster()->num_tablet_servers(); i++) {
+        if (test_cluster()->mini_tablet_server(i)->server()->permanent_uuid() ==
+            replica.ts_info().permanent_uuid()) {
+          leader_index_pre_shutdown = i;
+          LOG(INFO) << "Found leader index: " << i;
+          break;
+        }
+      }
+      break;
+    }
+  }
+
+  // Insert some records in transaction.
+  ASSERT_OK(WriteRowsHelper(0 /* start */, 100 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+  GetChangesResponsePB change_resp_1 = ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets));
+  change_resp_1 =
+    ASSERT_RESULT(GetChangesFromCDC(stream_id, tablets, &change_resp_1.cdc_sdk_checkpoint()));
+
+  ASSERT_OK(WriteRowsHelper(100 /* start */, 200 /* end */, &test_cluster_, true));
+  ASSERT_OK(test_client()->FlushTables(
+      {table.table_id()}, /* add_indexes = */ false, /* timeout_secs = */ 30,
+      /* is_compaction = */ false));
+  SleepFor(MonoDelta::FromSeconds(10));
+
+  // Shutdown tserver hosting tablet leader.
+  test_cluster()->mini_tablet_server(leader_index_pre_shutdown)->Shutdown();
+  LOG(INFO) << "TServer hosting tablet leader shutdown";
+  SleepFor(MonoDelta::FromSeconds(90));
+
+  OpId last_seen_checkpoint_op_id = OpId::Invalid();
+  int64 last_seen_num_intents = -1;
+  for (uint32_t i = 0; i < test_cluster()->num_tablet_servers(); ++i) {
+    if (i == leader_index_pre_shutdown) continue;
+
+    std::shared_ptr<tablet::TabletPeer> tablet_peer;
+    auto status =
+      test_cluster()->GetTabletManager(i)->GetTabletPeer(tablets[0].tablet_id(), &tablet_peer);
+    if (!status.IsOk()) {
+      continue;
+    }
+
+    OpId checkpoint = (*tablet_peer).cdc_sdk_min_checkpoint_op_id();
+    LOG(INFO) << "Checkpoint OpId : " << checkpoint << " ,  on tserver index: " << i;
+    if (last_seen_checkpoint_op_id == OpId::Invalid()) {
+      last_seen_checkpoint_op_id = checkpoint;
+    } else {
+      ASSERT_EQ(last_seen_checkpoint_op_id, checkpoint);
+    }
+
+    int64 num_intents;
+    PollForIntentCount(0, i, IntentCountCompareOption::GreaterThan, &num_intents);
+    LOG(INFO) << "Num of intents: " << num_intents << ", on tserver index" << i;
+    if (last_seen_num_intents == -1) {
+      last_seen_num_intents = num_intents;
+    } else {
+      ASSERT_EQ(last_seen_num_intents, num_intents);
+    }
+  }
+}
+
 }  // namespace enterprise
 }  // namespace cdc
 }  // namespace yb
diff --git a/src/yb/consensus/log-test-base.h b/src/yb/consensus/log-test-base.h
index 0885c14d6873..e4abc85715d5 100644
--- a/src/yb/consensus/log-test-base.h
+++ b/src/yb/consensus/log-test-base.h
@@ -250,6 +250,7 @@ class LogTestBase : public YBTest {
     if (op_type == consensus::OperationType::UPDATE_TRANSACTION_OP) {
       ASSERT_TRUE(!txn_id.IsNil());
       replicate->mutable_transaction_state()->set_status(txn_status);
+      replicate->mutable_transaction_state()->set_transaction_id(txn_id.data(), txn_id.size());
     } else if (op_type == consensus::OperationType::WRITE_OP) {
       if (writes.empty()) {
         const int opid_index_as_int = static_cast<int>(opid.index());
diff --git a/src/yb/tablet/running_transaction.cc b/src/yb/tablet/running_transaction.cc
index 50172b6a68e3..af8a7f2b800f 100644
--- a/src/yb/tablet/running_transaction.cc
+++ b/src/yb/tablet/running_transaction.cc
@@ -517,9 +517,8 @@ void RunningTransaction::SetApplyData(const docdb::ApplyTransactionState& apply_
   }
 }
 
-void RunningTransaction::SetOpId(const OpId& id) {
-  opId.index = id.index;
-  opId.term = id.term;
+void RunningTransaction::SetApplyOpId(const OpId& op_id) {
+  apply_record_op_id_ = op_id;
 }
 
 bool RunningTransaction::ProcessingApply() const {
diff --git a/src/yb/tablet/running_transaction.h b/src/yb/tablet/running_transaction.h
index 5537c9063260..a23b64906ed5 100644
--- a/src/yb/tablet/running_transaction.h
+++ b/src/yb/tablet/running_transaction.h
@@ -109,10 +109,10 @@ class RunningTransaction : public std::enable_shared_from_this<RunningTransactio
                     const TransactionApplyData* data = nullptr,
                     ScopedRWOperation* operation = nullptr);
 
-  void SetOpId(const OpId& id);
+  void SetApplyOpId(const OpId& id);
 
-  OpId GetOpId() {
-    return opId;
+  const OpId& GetApplyOpId() {
+    return apply_record_op_id_;
   }
 
   // Whether this transactions is currently applying intents.
@@ -175,7 +175,7 @@ class RunningTransaction : public std::enable_shared_from_this<RunningTransactio
   std::vector<TransactionStatusCallback> abort_waiters_;
 
   TransactionApplyData apply_data_;
-  OpId opId;
+  OpId apply_record_op_id_;
   docdb::ApplyTransactionState apply_state_;
   // Atomic that reflects active state, required to provide concurrent access to ProcessingApply.
   std::atomic<bool> processing_apply_{false};
diff --git a/src/yb/tablet/tablet.cc b/src/yb/tablet/tablet.cc
index a3bf1f6ac5aa..787ea8104264 100644
--- a/src/yb/tablet/tablet.cc
+++ b/src/yb/tablet/tablet.cc
@@ -259,6 +259,7 @@ DECLARE_int32(rocksdb_level0_stop_writes_trigger);
 DECLARE_uint64(rocksdb_max_file_size_for_compaction);
 DECLARE_int64(apply_intents_task_injected_delay_ms);
 DECLARE_string(regular_tablets_data_block_key_value_encoding);
+DECLARE_int64(cdc_intent_retention_ms);
 
 DEFINE_test_flag(uint64, inject_sleep_before_applying_intents_ms, 0,
                  "Sleep before applying intents to docdb after transaction commit");
@@ -776,6 +777,11 @@ Status Tablet::OpenKeyValueTablet() {
 
   ql_storage_.reset(new docdb::QLRocksDBStorage(doc_db()));
   if (transaction_participant_) {
+    // We need to set the "cdc_sdk_min_checkpoint_op_id" so that intents don't get
+    // garbage collected after transactions are loaded.
+    transaction_participant_->SetIntentRetainOpIdAndTime(
+        metadata_->cdc_sdk_min_checkpoint_op_id(),
+        MonoDelta::FromMilliseconds(GetAtomicFlag(&FLAGS_cdc_intent_retention_ms)));
     transaction_participant_->SetDB(doc_db(), &key_bounds_, &pending_non_abortable_op_counter_);
   }
 
diff --git a/src/yb/tablet/tablet_bootstrap-test.cc b/src/yb/tablet/tablet_bootstrap-test.cc
index 524b14bd4917..af3aad7647f1 100644
--- a/src/yb/tablet/tablet_bootstrap-test.cc
+++ b/src/yb/tablet/tablet_bootstrap-test.cc
@@ -827,6 +827,17 @@ void GenerateRandomInput(size_t num_entries, std::mt19937_64* rng, BootstrapInpu
             }
           }
         }
+        if (index <= intents_flushed_index && op_id >= first_op_id_of_segment_to_replay) {
+          // We replay Update transactions having an APPLY record even if their intents were only
+          // flushed to intents db.
+          if (op_type == consensus::OperationType::UPDATE_TRANSACTION_OP) {
+            bool replay = batch_data.txn_status == TransactionStatus::APPLYING;
+            if (replay) {
+              replayed.push_back(op_id);
+              replayed_to_intents_only.push_back(op_id);
+            }
+          }
+        }
       } else {
         // This operation was never committed. Mark it as "overwritable", meaning it _could_ be
         // overwritten as part of tablet bootstrap, but is not guaranteed to be.
diff --git a/src/yb/tablet/tablet_bootstrap.cc b/src/yb/tablet/tablet_bootstrap.cc
index fa25c0055b8d..f3b17ca2482b 100644
--- a/src/yb/tablet/tablet_bootstrap.cc
+++ b/src/yb/tablet/tablet_bootstrap.cc
@@ -414,21 +414,11 @@ ReplayDecision ShouldReplayOperation(
     const int64_t intents_flushed_index,
     TransactionStatus txn_status,
     bool write_op_has_transaction) {
-  // In most cases we assume that intents_flushed_index <= regular_flushed_index but here we are
-  // trying to be resilient to violations of that assumption.
-  if (index <= std::min(regular_flushed_index, intents_flushed_index)) {
-    // Never replay anyting that is flushed to both regular and intents RocksDBs in a transactional
-    // table.
-    VLOG_WITH_FUNC(3) << "index: " << index << " "
-                      << "regular_flushed_index: " << regular_flushed_index
-                      << " intents_flushed_index: " << intents_flushed_index;
-    return {false};
-  }
-
   if (op_type == consensus::UPDATE_TRANSACTION_OP) {
-    if (txn_status == TransactionStatus::APPLYING &&
-        intents_flushed_index < index && index <= regular_flushed_index) {
-      // Intents were applied/flushed to regular RocksDB, but not flushed into the intents RocksDB.
+    if (txn_status == TransactionStatus::APPLYING && index <= regular_flushed_index) {
+      // TODO: Replaying even transactions that are flushed to both regular and intents RocksDB is a
+      // temporary change. The long term change is to track write and apply operations separately
+      // instead of a tracking a single "intents_flushed_index".
       VLOG_WITH_FUNC(3) << "index: " << index << " "
                         << "regular_flushed_index: " << regular_flushed_index
                         << " intents_flushed_index: " << intents_flushed_index;
@@ -441,6 +431,17 @@ ReplayDecision ShouldReplayOperation(
     return {index > regular_flushed_index};
   }
 
+  // In most cases we assume that intents_flushed_index <= regular_flushed_index but here we are
+  // trying to be resilient to violations of that assumption.
+  if (index <= std::min(regular_flushed_index, intents_flushed_index)) {
+    // Never replay anyting that is flushed to both regular and intents RocksDBs in a transactional
+    // table.
+    VLOG_WITH_FUNC(3) << "index: " << index << " "
+                      << "regular_flushed_index: " << regular_flushed_index
+                      << " intents_flushed_index: " << intents_flushed_index;
+    return {false};
+  }
+
   if (op_type == consensus::WRITE_OP && write_op_has_transaction) {
     // Write intents that have not been flushed into the intents DB.
     VLOG_WITH_FUNC(3) << "index: " << index << " > "
@@ -1276,8 +1277,18 @@ class TabletBootstrap {
     log::SegmentSequence segments;
     RETURN_NOT_OK(log_->GetSegmentsSnapshot(&segments));
 
+    // If any cdc stream is active for this tablet, we do not want to skip flushed entries.
+    bool should_skip_flushed_entries = FLAGS_skip_flushed_entries;
+    if (should_skip_flushed_entries && tablet_->transaction_participant()) {
+      if (tablet_->transaction_participant()->GetRetainOpId() != OpId::Invalid()) {
+        should_skip_flushed_entries = false;
+        LOG_WITH_PREFIX(WARNING) << "Ignoring skip_flushed_entries even though it is set, because "
+                                 << "we need to scan all segments when any cdc stream is active "
+                                 << "for this tablet.";
+      }
+    }
     // Find the earliest log segment we need to read, so the rest can be ignored.
-    auto iter = FLAGS_skip_flushed_entries ? SkipFlushedEntries(&segments) : segments.begin();
+    auto iter = should_skip_flushed_entries ? SkipFlushedEntries(&segments) : segments.begin();
 
     yb::OpId last_committed_op_id;
     yb::OpId last_read_entry_op_id;
diff --git a/src/yb/tablet/tablet_peer.cc b/src/yb/tablet/tablet_peer.cc
index e3cf6c9dd979..d411372727e4 100644
--- a/src/yb/tablet/tablet_peer.cc
+++ b/src/yb/tablet/tablet_peer.cc
@@ -314,11 +314,6 @@ Status TabletPeer::InitTabletPeer(
   }
 
   RETURN_NOT_OK(set_cdc_min_replicated_index(meta_->cdc_min_replicated_index()));
-  if (tablet_->transaction_participant()) {
-    tablet_->transaction_participant()->SetIntentRetainOpIdAndTime(
-        meta_->cdc_sdk_min_checkpoint_op_id(),
-        MonoDelta::FromMilliseconds(GetAtomicFlag(&FLAGS_cdc_intent_retention_ms)));
-  }
 
   TRACE("TabletPeer::Init() finished");
   VLOG_WITH_PREFIX(2) << "Peer Initted";
diff --git a/src/yb/tablet/transaction_participant.cc b/src/yb/tablet/transaction_participant.cc
index 5021888133dd..0c33faade623 100644
--- a/src/yb/tablet/transaction_participant.cc
+++ b/src/yb/tablet/transaction_participant.cc
@@ -397,7 +397,7 @@ class TransactionParticipant::Impl
       auto it = transactions_.find(id);
 
       if (it != transactions_.end() && !(**it).ProcessingApply()) {
-        OpId op_id = (**it).GetOpId();
+        OpId op_id = (**it).GetApplyOpId();
 
         // If transaction op_id is greater than the CDCSDK checkpoint op_id.
         // don't clean the intent as well as intent after this.
@@ -580,7 +580,7 @@ class TransactionParticipant::Impl
     auto lock_and_iterator = LockAndFind(
         data.transaction_id, "apply"s, TransactionLoadFlags{TransactionLoadFlag::kMustExist});
     if (lock_and_iterator.found()) {
-      lock_and_iterator.transaction().SetOpId(data.op_id);
+      lock_and_iterator.transaction().SetApplyOpId(data.op_id);
       if (!apply_state.active()) {
         RemoveUnlocked(lock_and_iterator.iterator, RemoveReason::kApplied, &min_running_notifier);
       } else {
@@ -638,10 +638,14 @@ class TransactionParticipant::Impl
         cleanup_cache_.Insert(data.transaction_id);
         return Status::OK();
       }
-    } else if ((**it).ProcessingApply()) {
-      VLOG_WITH_PREFIX(2) << "Don't cleanup transaction because it is applying intents: "
-                          << data.transaction_id;
-      return Status::OK();
+    } else {
+      transactions_.modify(it, [&data](auto& txn) { txn->SetApplyOpId(data.op_id); });
+
+      if ((**it).ProcessingApply()) {
+        VLOG_WITH_PREFIX(2) << "Don't cleanup transaction because it is applying intents: "
+                            << data.transaction_id;
+        return Status::OK();
+      }
     }
 
     if (cleanup_type == CleanupType::kGraceful) {
@@ -1170,8 +1174,7 @@ class TransactionParticipant::Impl
       MinRunningNotifier* min_running_notifier) REQUIRES(mutex_) {
     TransactionId txn_id = (**it).id();
     OpId checkpoint_op_id = GetLatestCheckPoint();
-    auto itr = transactions_.find(txn_id);
-    OpId op_id = (**itr).GetOpId();
+    OpId op_id = (**it).GetApplyOpId();
 
     if (running_requests_.empty() && op_id < checkpoint_op_id) {
       (**it).ScheduleRemoveIntents(*it);
@@ -1791,7 +1794,7 @@ void TransactionParticipant::SetIntentRetainOpIdAndTime(
   impl_->SetIntentRetainOpIdAndTime(op_id, cdc_sdk_op_id_expiration);
 }
 
-OpId TransactionParticipant::TEST_GetRetainOpId() const {
+OpId TransactionParticipant::GetRetainOpId() const {
   return impl_->GetRetainOpId();
 }
 
diff --git a/src/yb/tablet/transaction_participant.h b/src/yb/tablet/transaction_participant.h
index e33692a73151..4350d6a66b1c 100644
--- a/src/yb/tablet/transaction_participant.h
+++ b/src/yb/tablet/transaction_participant.h
@@ -212,7 +212,7 @@ class TransactionParticipant : public TransactionStatusManager {
 
   void SetIntentRetainOpIdAndTime(const yb::OpId& op_id, const MonoDelta& cdc_sdk_op_id_expiration);
 
-  OpId TEST_GetRetainOpId() const;
+  OpId GetRetainOpId() const;
 
   const TabletId& tablet_id() const override;
 
diff --git a/src/yb/tserver/ts_tablet_manager.cc b/src/yb/tserver/ts_tablet_manager.cc
index 68a57404f43e..6f5ebaa21543 100644
--- a/src/yb/tserver/ts_tablet_manager.cc
+++ b/src/yb/tserver/ts_tablet_manager.cc
@@ -1370,7 +1370,6 @@ void TSTabletManager::OpenTablet(const RaftGroupMetadataPtr& meta,
 
   consensus::ConsensusBootstrapInfo bootstrap_info;
   consensus::RetryableRequests retryable_requests(kLogPrefix);
-  yb::OpId split_op_id;
 
   LOG_TIMING_PREFIX(INFO, kLogPrefix, "bootstrapping tablet") {
     // Read flag before CAS to avoid TSAN race conflict with GetAllFlags.
