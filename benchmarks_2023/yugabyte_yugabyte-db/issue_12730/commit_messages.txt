[BACKPORT 2.12.4][CDCSDK] Data inconsistency in CDC after restart of tserver

Summary:
2.12.4
"Original commit:
- 733f21c36f6318f0dfb596ba65e8d99e2424a574/D17065
- 3f42d6cd99979afe23401dbd03ed767176e4ae45/D16182
- 429e28634c13711eaa81f91d3f0d457ac6a401bc/D17159
- 58af25500276d21f7fb1d4f5dac6307dcdb09a9e/D17416
- 04c968399ecf440ffe8b01894cd31ff2edfa5a03/D17062"
CDCSDK maintain the checkpoint op_id in transaction layer to clean the intent before the checkpoint op_id, but this checkpoint info we never
persists, so when any failure happen and server restart transaction participation for the tablets initialised to -1.-1 and clean all those old intents those are not read by client.

To solve this problem we periodically update with "cdc_state_checkpoint_update_interval_ms" flag (default value is 60secs), to persist checkpoint information per tablet peers in RAFT logs. when server recover we read from the RAFT logs, so that intents whose op_id are larger than read checkpoint will not be cleaned up.

[CDCSDK] [#11879] Add YSQL unit tests

Unit tests for the following have been added:
  # Insert operation for single row and multiple rows
  # Insert and update operation for single row and multiple rows
  # Insert and delete a row
  # Begin a transaction, insert rows and abort the transaction
  # Insert operation before and after a snapshot

Unit test for MultiRowInsertion has been removed and replaced with SingleShardInsert4Rows. Both tests serve the same purpose of inserting multiple rows

[CDCSDK][12612/12614]  Intent are not cleaned up even after cdc retention time expired

In CDCSDK intents are cleaned up based on 2 identifiers cdc_sdk_min_checkpoint_op_id_ and cdc_sdk_min_checkpoint_op_id_expiration_.
for the failure testcases:-
a. TestGarbageCollectionWithLargerInterval, we set cdc_sdk_min_checkpoint_op_id_expiration_ to 3000ms.
b. TestGarbageCollectionWithSmallInterval, we set cdc_sdk_min_checkpoint_op_id_expiration_ to 200ms.

so after time expired, all the tserver intent counts should be zero, but these testcases are failing intermittently during intent count check
from all the tservers. During failure log analysis we found that, some time tablet LEADER ship change causing  the "cdc_enabled_" FLAG
to FALSE, for that reason "UpdatePeersAndMetrics" thread not able to do cleanup transaction, that's the reason intent counts are not zero.

To handle this scenario, in the testcase we should keep calling "GetChange", as part intent count check for all the tsevers, so that
even if leader change  "cdc_enabled_" will be TRUE.

[CDCSDK] [#12716] Reset cdc_sdk_min_checkpoint_op_id of transaction participant on deletion of the last stream

This diff has a dependancy on https://phabricator.dev.yugabyte.com/D17002
Refer to Problem 3 of this doc: https://docs.google.com/document/d/1Vxj_H04H59BiX97zQ95mIu4o5p1b7FpvN67ib5IUaC8/edit?usp=sharing
When multiple streams are present, and each of the streams is deleted, we want the cdc_sdk_min_checkpoint_op_id of the transaction participant to be reset to some "invalid value" when the last stream is deleted. We choose OpId::Max() to represent the checkpoint of a "deleted stream".

As an example consider the case of a single tablet with 2 streams. The cdc_sdk_min_checkpoint_op_id will be 1.2 :
tablet1 stream S1 checkpoint=1.2
tablet1 stream S2 checkpoint=1.4

Now, stream S1 is deleted. The cdc_sdk_min_checkpoint_op_id will be 1.4:
tablet1 stream S1 checkpoint=OpId::Max()
tablet1 stream S2 checkpoint=1.4

Finally, stream S2 is deleted. The cdc_sdk_min_checkpoint_op_id will be set to OpId::Max()
tablet1 stream S1 checkpoint=OpId::Max()
tablet1 stream S2 checkpoint=OpId::Max()
Thus, when an intermittent stream is deleted, the cdc_sdk_min_checkpoint_op_id is set as the minimum of the remaining streams' checkpoint. But when the last stream is deleted, the cdc_sdk_min_checkpoint_op_id for the transaction participant will be reset to OpId::Max()

The solution approach:
  - On deleting a CDCSDK stream, CleanUpDeletedCDCStreams() will set the checkpoint to OpId::Max() for the particular tabletid, stream entry in cdc_state table
  - However, CleanUpDeletedCDCStreams() will not deleted the entry from the cdc_state table
  - The actual deletion of entries from the cdc_state table will be done by the DeleteCDCStateTableMetadata() in cdc_service.cc
  - While scanning the cdc_state table in UpdatePeersAndMetrics() -> PopulateTabletCheckPointInfo() , we keep track of any {tablet_id, stream_id} that has its checkpoint set to OpId::Max(). All such pairs are stored in a "SET"
  - After updating the peers in UpdateTabletPeersWithMinReplicatedIndex(), the DeleteCDCStateTableMetadata() function is called. This deletes the entries contained in the "SET" from the cdc_state table.

[CDCSDK] [#12646][#12702] Intents are Garbage collected even client keep calling GetChange

Issue:-1
We  have observed that  if an active stream is  pooled after FLAG cdc_intent_retention_ms  has expired(default value is 4hrs),  then records inserted after that are not received by user, if user call "GetChange". During code analysis we found that to cleanup the intent CDCSDK maintain 2 identifiers a. cdc_sdk_min_checkpint_op_id_ b. cdc_sdk_min_checkpint_op_id_expiration_, but in current implementation both are tightly coupled. which is an issue when there is no changes in table but client is keep pooling the server, but because there no new checkpoint, cdc_sdk_min_checkpint_op_id_expiration_ can expired and future records can be garbage collected.

Approach:-
To handle this server will reset the "cdc_sdk_min_checkpint_op_id_expiration_" value when ever client call "GetChange"/"setCDCCheckpoint".  in the "UpdatePeersAndMetrics" thread, tablet peers read the LEADER expiration time and update their "cdc_sdk_min_checkpint_op_id_expiration_".

Issue:-2
It's observe that if multiple streams are defined on the same tablet, and one of stream is not polling by the client, till the cdc_intent_retention_ms is expired, but the inactive stream never expired.

Approach:-
To handle the scenario, we will keep track each stream's active ness per tablet, in the "cache". if the stream is not active till cdc_intent_retention_ms is expired, we will throw error.

Test Plan: Jenkins: skip

Reviewers: vkushwaha, slingam, sergei, jhe, iamoncar, aagrawal, abharadwaj, skumar

Reviewed By: abharadwaj, skumar

Subscribers: abharadwaj, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17599
[BACKPORT 2.12.4][#12730][CDCSDK] Set OpId of APPLY Record in the RunningTransaction during tablet bootstrap

Summary:
Original commit: https://phabricator.dev.yugabyte.com/D17297
The intents of transactions were deleted on restart because their APPLY Record OpId was lost and reset to 0.0 .
Now, during tablet bootstrap stage, the OpIds will be updated through the newly added "SetOpIdForTransaction" function.

The problem and solution is described in detail in the below doc:
https://docs.google.com/document/d/11XfL6Zv2KVXaE5wGxBYfhi4k5Wq-nMZlR1t_60iiz14/edit#

Test Plan:
Jenkins: urgent
Added ctests TestIntentCountPersistencyAllNodesRestart, TestIntentCountPersistencyRemoteBootstrap, TestHighIntentCountPersistencyAllNodesRestart

Reviewers: skumar

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17619
[#12730][CDCSDK] Set OpId of APPLY Record in the RunningTransaction during tablet bootstrap

Summary:
For CDC, we cannot delete the entries of a transaction from IntentDB right after the transaction gets committed, but rather we have to wait for the CDC client to get the changes. Each tablet maintains an OpId checkpoint called ““cdc_sdk_min_checkpoint_op_id_”. Every committed transaction having an APPLY record whose OpId is higher than this checkpointed OpId will be retained in the IntentDB.

But upon restart, the entries of these committed transactions which were not yet streamed were being deleted from IntentDB.

As part of the solution we are doing two things:
1) Now, every transaction of type UPDATE having an APPLY record will be replayed in tablet bootstrap
2) We set the OpId of the APPLY operation to the transaction in ProcessCleanup

Test Plan:
Jenkins: compile only

Added ctests TestIntentCountPersistencyAllNodesRestart, TestIntentCountPersistencyRemoteBootstrap, TestHighIntentCountPersistencyAllNodesRestart

Reviewers: skumar, vkushwaha, sdash, sergei

Reviewed By: sergei

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17730
[BACKPORT 2.12][#12730][CDCSDK] Set OpId of APPLY Record in the RunningTransaction during tablet bootstrap

Summary:
Original commit: ec5118b362e10ca3177a63b08469a0424c3785ab / https://phabricator.dev.yugabyte.com/D17730
For CDC, we cannot delete the entries of a transaction from IntentDB right after the transaction gets committed, but rather we have to wait for the CDC client to get the changes. Each tablet maintains an OpId checkpoint called ““cdc_sdk_min_checkpoint_op_id_”. Every committed transaction having an APPLY record whose OpId is higher than this checkpointed OpId will be retained in the IntentDB.

But upon restart, the entries of these committed transactions which were not yet streamed were being deleted from IntentDB.

As part of the solution we are doing two things:
1) Now, every transaction of type UPDATE having an APPLY record will be replayed in tablet bootstrap
2) We set the OpId of the APPLY operation to the transaction in ProcessCleanup

Test Plan: Added ctests TestIntentCountPersistencyAllNodesRestart, TestIntentCountPersistencyRemoteBootstrap, TestHighIntentCountPersistencyAllNodesRestart

Reviewers: vkushwaha, sdash, skumar

Reviewed By: skumar

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17746
[BACKPORT 2.15.0][#12730][CDCSDK] Set OpId of APPLY Record in the RunningTransaction during tablet bootstrap

Summary:
Original commit: ec5118b362e10ca3177a63b08469a0424c3785ab / https://phabricator.dev.yugabyte.com/D17730
For CDC, we cannot delete the entries of a transaction from IntentDB right after the transaction gets committed, but rather we have to wait for the CDC client to get the changes. Each tablet maintains an OpId checkpoint called ““cdc_sdk_min_checkpoint_op_id_”. Every committed transaction having an APPLY record whose OpId is higher than this checkpointed OpId will be retained in the IntentDB.

But upon restart, the entries of these committed transactions which were not yet streamed were being deleted from IntentDB.

As part of the solution we are doing two things:
1) Now, every transaction of type UPDATE having an APPLY record will be replayed in tablet bootstrap
2) We set the OpId of the APPLY operation to the transaction in ProcessCleanup

Test Plan: Added ctests TestIntentCountPersistencyAllNodesRestart, TestIntentCountPersistencyRemoteBootstrap, TestHighIntentCountPersistencyAllNodesRestart

Reviewers: vkushwaha, sdash, skumar

Reviewed By: skumar

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17749
[BACKPORT 2.14][#12730][CDCSDK] Set OpId of APPLY Record in the RunningTransaction during tablet bootstrap

Summary:
Original commit: ec5118b362e10ca3177a63b08469a0424c3785ab / https://phabricator.dev.yugabyte.com/D17730
For CDC, we cannot delete the entries of a transaction from IntentDB right after the transaction gets committed, but rather we have to wait for the CDC client to get the changes. Each tablet maintains an OpId checkpoint called ““cdc_sdk_min_checkpoint_op_id_”. Every committed transaction having an APPLY record whose OpId is higher than this checkpointed OpId will be retained in the IntentDB.

But upon restart, the entries of these committed transactions which were not yet streamed were being deleted from IntentDB.

As part of the solution we are doing two things:
1) Now, every transaction of type UPDATE having an APPLY record will be replayed in tablet bootstrap
2) We set the OpId of the APPLY operation to the transaction in ProcessCleanup

Test Plan: Added ctests TestIntentCountPersistencyAllNodesRestart, TestIntentCountPersistencyRemoteBootstrap, TestHighIntentCountPersistencyAllNodesRestart

Reviewers: vkushwaha, sdash, skumar

Reviewed By: skumar

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17747
[BACKPORT 2.12.4][#12730][CDCSDK] Set OpId of APPLY Record in the RunningTransaction during tablet bootstrap

Summary:
Original commit: ec5118b362e10ca3177a63b08469a0424c3785ab / https://phabricator.dev.yugabyte.com/D17730
For CDC, we cannot delete the entries of a transaction from IntentDB right after the transaction gets committed, but rather we have to wait for the CDC client to get the changes. Each tablet maintains an OpId checkpoint called ““cdc_sdk_min_checkpoint_op_id_”. Every committed transaction having an APPLY record whose OpId is higher than this checkpointed OpId will be retained in the IntentDB.

But upon restart, the entries of these committed transactions which were not yet streamed were being deleted from IntentDB.

As part of the solution we are doing two things:
1) Now, every transaction of type UPDATE having an APPLY record will be replayed in tablet bootstrap
2) We set the OpId of the APPLY operation to the transaction in ProcessCleanup

Test Plan: Added ctests TestIntentCountPersistencyAllNodesRestart, TestIntentCountPersistencyRemoteBootstrap, TestHighIntentCountPersistencyAllNodesRestart

Reviewers: vkushwaha, sdash, skumar

Reviewed By: skumar

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D17745
[#12730][CDCSDK] Adding logs for more information on GC of Intents

Summary: To debug cases like data loss, we have to add additional information in the logs such as number of intents, transaction id, OpId of the apply record of the transaction , cdcsdk checkpoint op id, etc.

Test Plan: Just adding logs, no need of additional tests

Reviewers: skumar, sdash

Reviewed By: sdash

Subscribers: ycdcxcluster

Differential Revision: https://phabricator.dev.yugabyte.com/D18024
