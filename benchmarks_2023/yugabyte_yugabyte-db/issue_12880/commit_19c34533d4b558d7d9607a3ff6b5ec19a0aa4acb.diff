diff --git a/src/yb/tablet/write_query.cc b/src/yb/tablet/write_query.cc
index da99c012897f..b360c05f9698 100644
--- a/src/yb/tablet/write_query.cc
+++ b/src/yb/tablet/write_query.cc
@@ -622,24 +622,25 @@ void WriteQuery::RedisExecuteDone(const Status& status) {
 }
 
 bool WriteQuery::CqlCheckSchemaVersion() {
+  constexpr auto error_code = QLResponsePB::YQL_STATUS_SCHEMA_VERSION_MISMATCH;
   auto& metadata = *tablet().metadata();
-  const auto& ql_write_batch = client_request_->ql_write_batch();
+  const auto& req_batch = client_request_->ql_write_batch();
+  auto& resp_batch = *response_->mutable_ql_response_batch();
 
   auto table_info = metadata.primary_table_info();
   int index = 0;
   int num_mismatches = 0;
-  for (const auto& req : ql_write_batch) {
+  for (const auto& req : req_batch) {
     if (!CheckSchemaVersion(
             table_info.get(), req.schema_version(), req.is_compatible_with_previous_version(),
-            QLResponsePB::YQL_STATUS_SCHEMA_VERSION_MISMATCH, index,
-            response_->mutable_ql_response_batch())) {
+            error_code, index, &resp_batch)) {
       ++num_mismatches;
     }
     ++index;
   }
 
   if (num_mismatches != 0) {
-    SchemaVersionMismatch(num_mismatches, ql_write_batch.size());
+    SchemaVersionMismatch(error_code, req_batch.size(), &resp_batch);
     return false;
   }
 
@@ -663,9 +664,17 @@ void WriteQuery::CqlExecuteDone(const Status& status) {
   }
 }
 
-void WriteQuery::SchemaVersionMismatch(int num_mismatches, int batch_size) {
-  LOG_IF(DFATAL, num_mismatches != batch_size)
-      << "Wrong number or mismatches: " << num_mismatches << " vs " << batch_size;
+template <class Code, class Resp>
+void WriteQuery::SchemaVersionMismatch(Code code, int size, Resp* resp) {
+  for (int i = 0; i != size; ++i) {
+    auto* entry = resp->size() > i ? resp->Mutable(i) : resp->Add();
+    if (entry->status() == code) {
+      continue;
+    }
+    entry->Clear();
+    entry->set_status(code);
+    entry->set_error_message("Other request entry schema version mismatch");
+  }
   auto self = std::move(self_);
   submit_token_.Reset();
   Cancel(Status::OK());
@@ -827,12 +836,14 @@ void WriteQuery::UpdateQLIndexesFlushed(
 }
 
 bool WriteQuery::PgsqlCheckSchemaVersion() {
+  constexpr auto error_code = PgsqlResponsePB::PGSQL_STATUS_SCHEMA_VERSION_MISMATCH;
   auto& metadata = *tablet().metadata();
-  const auto& pgsql_write_batch = client_request_->pgsql_write_batch();
+  const auto& req_batch = client_request_->pgsql_write_batch();
+  auto& resp_batch = *response_->mutable_pgsql_response_batch();
 
   int index = 0;
   int num_mismatches = 0;
-  for (const auto& req : pgsql_write_batch) {
+  for (const auto& req : req_batch) {
     auto table_info = metadata.GetTableInfo(req.table_id());
     if (!table_info.ok()) {
       StartSynchronization(std::move(self_), table_info.status());
@@ -840,15 +851,14 @@ bool WriteQuery::PgsqlCheckSchemaVersion() {
     }
     if (!CheckSchemaVersion(
             table_info->get(), req.schema_version(), /* compatible_with_previous_version= */ false,
-            PgsqlResponsePB::PGSQL_STATUS_SCHEMA_VERSION_MISMATCH, index,
-            response_->mutable_pgsql_response_batch())) {
+            error_code, index, &resp_batch)) {
       ++num_mismatches;
     }
     ++index;
   }
 
   if (num_mismatches != 0) {
-    SchemaVersionMismatch(num_mismatches, pgsql_write_batch.size());
+    SchemaVersionMismatch(error_code, req_batch.size(), &resp_batch);
     return false;
   }
 
diff --git a/src/yb/tablet/write_query.h b/src/yb/tablet/write_query.h
index 7705e4ea264f..72dd47b2344a 100644
--- a/src/yb/tablet/write_query.h
+++ b/src/yb/tablet/write_query.h
@@ -168,7 +168,8 @@ class WriteQuery {
 
   void CompleteQLWriteBatch(const Status& status);
 
-  void SchemaVersionMismatch(int num_mismatches, int batch_size);
+  template <class Code, class Resp>
+  void SchemaVersionMismatch(Code code, int size, Resp* resp);
 
   bool CqlCheckSchemaVersion();
   bool PgsqlCheckSchemaVersion();
diff --git a/src/yb/yql/pgwrapper/pg_packed_row-test.cc b/src/yb/yql/pgwrapper/pg_packed_row-test.cc
index 9b7af2035498..29eb100dcb65 100644
--- a/src/yb/yql/pgwrapper/pg_packed_row-test.cc
+++ b/src/yb/yql/pgwrapper/pg_packed_row-test.cc
@@ -396,5 +396,24 @@ TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(BigValue)) {
   ASSERT_OK(update_value(0, kHalfBigValue, 2));
 }
 
+TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(AddColumn)) {
+  {
+    auto conn = ASSERT_RESULT(Connect());
+    ASSERT_OK(conn.Execute("CREATE DATABASE test WITH colocated = true"));
+  }
+
+  auto conn = ASSERT_RESULT(ConnectToDB("test"));
+
+  ASSERT_OK(conn.Execute("CREATE TABLE t (key INT PRIMARY KEY, ival INT) WITH (colocated = true)"));
+  ASSERT_OK(conn.Execute("CREATE INDEX t_idx ON t(ival)"));
+
+  auto conn2 = ASSERT_RESULT(ConnectToDB("test"));
+  ASSERT_OK(conn2.Execute("INSERT INTO t (key, ival) VALUES (1, 1)"));
+
+  ASSERT_OK(conn.Execute("ALTER TABLE t ADD COLUMN v1 INT"));
+
+  ASSERT_OK(conn2.Execute("INSERT INTO t (key, ival) VALUES (2, 2)"));
+}
+
 } // namespace pgwrapper
 } // namespace yb
