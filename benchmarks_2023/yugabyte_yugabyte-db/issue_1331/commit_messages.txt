[docdb] load balance the tablets at the global level also (#3079)

Summary:
Introduce global level load balancing. The main changes here are:
1. Also keep track of global load counts across all tservers, this is stored in `global_state_`.
2. Use this global load as a tiebreaker when sorting load. Thus, when we have a choice of where to move load from and to, we will favour moving from higher global load to lower.
3. Once all tables are balanced, we then check if we can perform additional global balancing. This is checked via `global_state_->CanBalanceGlobalLoad()`, and is persisted across runs using `can_balance_global_load_`. Essentially, this value only turns true once the load balancer becomes idle (ie all tables are balanced), and then only turns false once we perform a move that isn't a strictly global load balancing move (a table level move).
4. If strictly global load balancing moves are enabled, then they are done if the load variance between two tservers is greater than 0 (exactly equal to 1 with the default flags), and if the global load variance is >= 2.

Adding gflag `enable_global_load_balancing` to toggle this feature.

Test Plan:
Added new tests:
```
ybd --cxx-test load_balancer_multi_table-test --gtest_filter LoadBalancerMultiTableTest.GlobalLoadBalancing
ybd --cxx-test load_balancer_multi_table-test --gtest_filter LoadBalancerMultiTableTest.GlobalLoadBalancingWithBlacklist
ybd --cxx-test load_balancer_colocated_tables-test
```

Reviewers: bogdan, rahuldesirazu

Reviewed By: rahuldesirazu

Subscribers: jason, zyu, ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D9288
#1331: [YCQL] Operations audit logging

Summary:
Audit logging feature for YCQL is similar to Apache Cassandra 4.0-beta2, with some inspiration taken from pgAudit.
* Audit is controlled and configured through runtime gflags, which can changed using `yb-ts-cli` without restarting a `yb-tserver` process.
* Audit log in written into a general log, with `AUDIT: ` prefix.
* Output format follows that of Cassandra binary log as seen through `auditlogviewer`.
* We also follow Cassandra's audit approach for event types, categories and scopes.
* We rely on `glog` for logging, which results in logging being done pseudo-asynchronously (i.e. buffered and sometimes flushed).
* Operations that fail at parse or analyze time will be logged once.
* Operations that fail at execution time will be logged twice - before attempting to execute it, and before reporting an error. This differs from both Cassandra and pgAudit.
* For `CREATE/ALTER ROLE`, password will be replaced with `<REDACTED>` (only if the statement was written without grammatical errors!).

# Usage

Audit is disabled by default and is configured using `yb-tserver` gflags. The most important one is `--ycql_enable_audit_log`, enabling which would by default audit everything except system table queries. Without it, other audit config flags would have no effect.

`--ycql_audit_log_level` controls a log level at which audit records will be logged. It defaults to `ERROR`, but might be set to `INFO` or `WARNING` instead.
Note that only `ERROR`-level logs are immediately flushed to disk, lower levels might be buffered.

Here are few use cases:
| **Audit use case**             | **`yb-tserver` gflags (in addition to `--ycql_enable_audit_log`)**             |
| Auth events only               | `--ycql_audit_included_categories=AUTH`                                        |
| DDLs on  `ks1`                 | `--ycql_audit_included_categories=DDL --ycql_audit_included_keyspaces=ks1`     |
| SELECTs and DMLs by `user1`    | `--ycql_audit_included_categories=QUERY,DML --ycql_audit_included_users=user1` |
| DCLs by everyone but `dbadmin` | `--ycql_audit_included_categories=DCL --ycql_audit_excluded_users=dbadmin`     |

# Sample output

```
E0919 17:45:33.313267 344780800 audit_logger.cc:564] AUDIT: user:null|host:127.0.0.1:9042|source:127.0.0.1|port:52784|timestamp:1600519533313|type:LOGIN_ERROR|category:AUTH|operation:LOGIN FAILURE; Provided username cassandra and/or password are incorrect
E0919 17:45:59.617997 212246528 audit_logger.cc:564] AUDIT: user:cassandra|host:127.0.0.1:9042|source:127.0.0.1|port:52789|timestamp:1600519559617|type:LOGIN_SUCCESS|category:AUTH|operation:LOGIN SUCCESSFUL
E0919 17:46:20.512369 345853952 audit_logger.cc:564] AUDIT: user:cassandra|host:127.0.0.1:9042|source:127.0.0.1|port:52790|timestamp:1600519580512|type:SELECT|category:QUERY|ks:k|scope:t|operation:SELECT * FROM k.t;
E0919 17:46:35.132189 344780800 audit_logger.cc:564] AUDIT: user:cassandra|host:127.0.0.1:9042|source:127.0.0.1|port:52790|timestamp:1600519595132|type:REQUEST_FAILURE|category:ERROR|operation:SELECT * FROM k.tx;; Object Not Found
```

# Known issues:

* Operations restarted internally are logged upon restart.
* Client-side only operations (`DESCRIBE`, `LIST`) are not logged.
* Login events are logged twice (could have something to do with control connection).
* More comprehensive Java tests are needed.

Test Plan: ybd --java-test org.yb.cql.TestAudit

Reviewers: neil, oleg, mihnea, dmitry, sergei

Reviewed By: dmitry, sergei

Subscribers: sergei, yql

Differential Revision: https://phabricator.dev.yugabyte.com/D9376
[Backport to 2.2] #1331: [YCQL] Operations audit logging

Summary:
Audit logging feature for YCQL is similar to Apache Cassandra 4.0-beta2, with some inspiration taken from pgAudit.
* Audit is controlled and configured through runtime gflags, which can changed using `yb-ts-cli` without restarting a `yb-tserver` process.
* Audit log in written into a general log, with `AUDIT: ` prefix.
* Output format follows that of Cassandra binary log as seen through `auditlogviewer`.
* We also follow Cassandra's audit approach for event types, categories and scopes.
* We rely on `glog` for logging, which results in logging being done pseudo-asynchronously (i.e. buffered and sometimes flushed).
* Operations that fail at parse or analyze time will be logged once.
* Operations that fail at execution time will be logged twice - before attempting to execute it, and before reporting an error. This differs from both Cassandra and pgAudit.
* For `CREATE/ALTER ROLE`, password will be replaced with `<REDACTED>` (only if the statement was written without grammatical errors!).

# Usage

Audit is disabled by default and is configured using `yb-tserver` gflags. The most important one is `--ycql_enable_audit_log`, enabling which would by default audit everything except system table queries. Without it, other audit config flags would have no effect.

`--ycql_audit_log_level` controls a log level at which audit records will be logged. It defaults to `ERROR`, but might be set to `INFO` or `WARNING` instead.
Note that only `ERROR`-level logs are immediately flushed to disk, lower levels might be buffered.

Here are few use cases:
| **Audit use case**             | **`yb-tserver` gflags (in addition to `--ycql_enable_audit_log`)**             |
| Auth events only               | `--ycql_audit_included_categories=AUTH`                                        |
| DDLs on  `ks1`                 | `--ycql_audit_included_categories=DDL --ycql_audit_included_keyspaces=ks1`     |
| SELECTs and DMLs by `user1`    | `--ycql_audit_included_categories=QUERY,DML --ycql_audit_included_users=user1` |
| DCLs by everyone but `dbadmin` | `--ycql_audit_included_categories=DCL --ycql_audit_excluded_users=dbadmin`     |

# Sample output

```
E0919 17:45:33.313267 344780800 audit_logger.cc:564] AUDIT: user:null|host:127.0.0.1:9042|source:127.0.0.1|port:52784|timestamp:1600519533313|type:LOGIN_ERROR|category:AUTH|operation:LOGIN FAILURE; Provided username cassandra and/or password are incorrect
E0919 17:45:59.617997 212246528 audit_logger.cc:564] AUDIT: user:cassandra|host:127.0.0.1:9042|source:127.0.0.1|port:52789|timestamp:1600519559617|type:LOGIN_SUCCESS|category:AUTH|operation:LOGIN SUCCESSFUL
E0919 17:46:20.512369 345853952 audit_logger.cc:564] AUDIT: user:cassandra|host:127.0.0.1:9042|source:127.0.0.1|port:52790|timestamp:1600519580512|type:SELECT|category:QUERY|ks:k|scope:t|operation:SELECT * FROM k.t;
E0919 17:46:35.132189 344780800 audit_logger.cc:564] AUDIT: user:cassandra|host:127.0.0.1:9042|source:127.0.0.1|port:52790|timestamp:1600519595132|type:REQUEST_FAILURE|category:ERROR|operation:SELECT * FROM k.tx;; Object Not Found
```

# Known issues:

* Operations restarted internally are logged upon restart.
* Client-side only operations (`DESCRIBE`, `LIST`) are not logged.
* Login events are logged twice (could have something to do with control connection).
* More comprehensive Java tests are needed.

Test Plan:
ybd --java-test org.yb.cql.TestAudit
Jenkins: urgent, rebase: 2.2

Reviewers: sergei, dmitry, mihnea

Reviewed By: mihnea

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D9468
[#1331][YSQL] Audit logging support for YSQL

Summary:
The diff enables Audit Logging support for YSQL. It utilizes the postgres-compatible feature [[ https://github.com/pgaudit/pgaudit | pgAudit ]] for enabling Audit logging.

Then, to enable auditing on a database run:
```
CREATE EXTENSION IF NOT EXISTS pgaudit;
```

To configure auditing you can use the `ysql_pg_conf` flag.
For instance, to enable auditing of DDLs and raising the log level to notice, do:
```
ysql_pg_conf="pgaudit.log='DDL',pgaudit.log_level=notice"
```
Similarly, there exists other options like //log_client, log_catalog, log_parameter, log_relation, log_statement_once and role//. All options can be set as a cluster-level setting by using `ysql_pg_conf` (as above).
They can also be set per-session (connection) using the `SET` command. For example, log_level can be set using the following `SET` command. However, these settings are alive only for the session during which we set these variables.

```
SET pgaudit.log_level="notice";
```

Test Plan:
The associated test for Audit logging can be execute by running the following command.

```
ybd --java-test org.yb.pgsql.TestPgRegressExtension
```

Reviewers: mihnea

Reviewed By: mihnea

Subscribers: kannan, yql

Differential Revision: https://phabricator.dev.yugabyte.com/D9667
[Backport to 2.3][#1331][YSQL] Audit logging support for YSQL

Summary:
The diff enables Audit Logging support for YSQL. It utilizes the postgres-compatible feature [[ https://github.com/pgaudit/pgaudit | pgAudit ]] for enabling Audit logging.

Then, to enable auditing on a database run:
```
CREATE EXTENSION IF NOT EXISTS pgaudit;
```

To configure auditing you can use the `ysql_pg_conf` flag.
For instance, to enable auditing of DDLs and raising the log level to notice, do:
```
ysql_pg_conf="pgaudit.log='DDL',pgaudit.log_level=notice"
```
Similarly, there exists other options like //log_client, log_catalog, log_parameter, log_relation, log_statement_once and role//. All options can be set as a cluster-level setting by using `ysql_pg_conf` (as above).
They can also be set per-session (connection) using the `SET` command. For example, log_level can be set using the following `SET` command. However, these settings are alive only for the session during which we set these variables.

```
SET pgaudit.log_level="notice";
```

Test Plan:
Jenkins: rebase: 2.3
The associated test for Audit logging can be execute by running the following command.

```
ybd --java-test org.yb.pgsql.TestPgRegressExtension
```

Reviewers: mihnea

Reviewed By: mihnea

Subscribers: yql, kannan

Differential Revision: https://phabricator.dev.yugabyte.com/D9762
