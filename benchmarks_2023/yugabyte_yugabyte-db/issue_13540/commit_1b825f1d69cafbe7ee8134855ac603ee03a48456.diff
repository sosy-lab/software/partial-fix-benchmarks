diff --git a/src/yb/docdb/CMakeLists.txt b/src/yb/docdb/CMakeLists.txt
index 813dc1cbde01..4693b032dca2 100644
--- a/src/yb/docdb/CMakeLists.txt
+++ b/src/yb/docdb/CMakeLists.txt
@@ -28,6 +28,7 @@ ADD_YB_LIBRARY(docdb_proto
         NONLINK_DEPS ${DOCDB_PROTO_TGTS})
 
 set(DOCDB_ENCODING_SRCS
+    docdb-internal.cc
     doc_key.cc
     doc_kv_util.cc
     doc_path.cc
diff --git a/src/yb/server/CMakeLists.txt b/src/yb/server/CMakeLists.txt
index 5d38183758a8..d5f035d451e9 100644
--- a/src/yb/server/CMakeLists.txt
+++ b/src/yb/server/CMakeLists.txt
@@ -89,6 +89,7 @@ set(SERVER_PROCESS_SRCS
   default-path-handlers.cc
   generic_service.cc
   glog_metrics.cc
+  intentsdb-path-handler.cc
   monitored_task.cc
   pgsql_webserver_wrapper.cc
   pprof-path-handlers.cc
@@ -109,6 +110,8 @@ set(SERVER_PROCESS_SRCS
 add_library(server_process ${SERVER_PROCESS_SRCS})
 target_link_libraries(server_process
   server_base_proto
+  tserver_proto
+  tserver_service_proto
   server_common
   yb_common
   yb_fs
diff --git a/src/yb/server/intentsdb-path-handler.cc b/src/yb/server/intentsdb-path-handler.cc
new file mode 100644
index 000000000000..a3364a704c32
--- /dev/null
+++ b/src/yb/server/intentsdb-path-handler.cc
@@ -0,0 +1,108 @@
+// Licensed to the Apache Software Foundation (ASF) under one
+// or more contributor license agreements.  See the NOTICE file
+// distributed with this work for additional information
+// regarding copyright ownership.  The ASF licenses this file
+// to you under the Apache License, Version 2.0 (the
+// "License"); you may not use this file except in compliance
+// with the License.  You may obtain a copy of the License at
+//
+//   http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing,
+// software distributed under the License is distributed on an
+// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+// KIND, either express or implied.  See the License for the
+// specific language governing permissions and limitations
+// under the License.
+//
+// The following only applies to changes made to this file as part of YugaByte development.
+//
+// Portions Copyright (c) YugaByte, Inc.
+//
+// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
+// in compliance with the License.  You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software distributed under the License
+// is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
+// or implied.  See the License for the specific language governing permissions and limitations
+// under the License.
+//
+#include "yb/server/intentsdb-path-handler.h"
+
+#include <functional>
+#include <memory>
+#include <string>
+
+#include "yb/rpc/messenger.h"
+#include "yb/server/webserver.h"
+#include "yb/tserver/tserver.pb.h"
+#include "yb/tserver/tablet_server.h"
+#include "yb/tserver/tserver_admin.proxy.h"
+#include "yb/util/net/net_util.h"
+#include "yb/util/status_log.h"
+
+namespace yb {
+
+using yb::tserver::ReadIntentsRequestPB;
+using yb::tserver::ReadIntentsResponsePB;
+using yb::rpc::Messenger;
+using std::shared_ptr;
+using std::stringstream;
+
+using namespace std::placeholders;
+
+DEFINE_bool(enable_intentsdb_page, false, "Enable displaying the contents of intentsdb page.");
+
+namespace {
+
+void IntentsDBPathHandler(
+    rpc::Messenger* messenger,
+    Webserver* webserver,
+    const Webserver::WebRequest& req,
+    Webserver::WebResponse* resp) {
+  std::stringstream *output = &resp->output;
+  if (!FLAGS_enable_intentsdb_page) {
+    (*output) << "Intentsdb is disabled, please set the gflag enable_intentsdb_page to true"
+                 "to show the content of intentsdb.";
+    return;
+  }
+
+  HostPort host_port("localhost", tserver::TabletServer::kDefaultPort);
+
+  auto proxy_cache = std::make_unique<rpc::ProxyCache>(messenger);
+  auto ts_admin_proxy_ = std::make_unique<tserver::TabletServerAdminServiceProxy>(
+    proxy_cache.get(), host_port);
+
+  ReadIntentsRequestPB intents_req;
+  ReadIntentsResponsePB intents_resp;
+
+  auto it = req.parsed_args.find("tablet_id");
+  if (it != req.parsed_args.end()) {
+    intents_req.set_tablet_id(it->second);
+  }
+
+  rpc::RpcController rpc;
+  rpc.set_timeout(MonoDelta::FromMilliseconds(1000 * 60));
+  Status s = ts_admin_proxy_->ReadIntents(intents_req, &intents_resp, &rpc);
+  if (!s.ok()) {
+    (*output) << "Error when reading intents information";
+    return;
+  }
+
+  (*output) << Format("Intents size: $0\n", intents_resp.intents().size()) << "\n";
+  for (const auto& item : intents_resp.intents()) {
+    (*output) << Format("$0\n", item);
+  }
+}
+
+} // anonymous namespace
+
+void AddIntentsDBPathHandler(rpc::Messenger* messenger, Webserver* webserver) {
+  webserver->RegisterPathHandler(
+      "/intentsdb", "IntentsDB",
+      std::bind(IntentsDBPathHandler, messenger, webserver, _1, _2), false, false);
+}
+
+} // namespace yb
diff --git a/src/yb/server/intentsdb-path-handler.h b/src/yb/server/intentsdb-path-handler.h
new file mode 100644
index 000000000000..2da2cec548c8
--- /dev/null
+++ b/src/yb/server/intentsdb-path-handler.h
@@ -0,0 +1,48 @@
+// Licensed to the Apache Software Foundation (ASF) under one
+// or more contributor license agreements.  See the NOTICE file
+// distributed with this work for additional information
+// regarding copyright ownership.  The ASF licenses this file
+// to you under the Apache License, Version 2.0 (the
+// "License"); you may not use this file except in compliance
+// with the License.  You may obtain a copy of the License at
+//
+//   http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing,
+// software distributed under the License is distributed on an
+// "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+// KIND, either express or implied.  See the License for the
+// specific language governing permissions and limitations
+// under the License.
+//
+// The following only applies to changes made to this file as part of YugaByte development.
+//
+// Portions Copyright (c) YugaByte, Inc.
+//
+// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
+// in compliance with the License.  You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software distributed under the License
+// is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
+// or implied.  See the License for the specific language governing permissions and limitations
+// under the License.
+//
+#ifndef YB_SERVER_INTENTSDB_PATH_HANDLER_H
+#define YB_SERVER_INTENTSDB_PATH_HANDLER_H
+
+#include <memory>
+
+#include "yb/rpc/rpc_fwd.h"
+
+DECLARE_bool(enable_intentsdb_page);
+
+namespace yb {
+
+class Webserver;
+
+void AddIntentsDBPathHandler(rpc::Messenger* messenger, Webserver* webserver);
+
+} // namespace yb
+#endif /* YB_SERVER_INTENTSDB_PATH_HANDLER_H */
diff --git a/src/yb/server/server_base.cc b/src/yb/server/server_base.cc
index 6b87a75e5022..5bbb9f05fddc 100644
--- a/src/yb/server/server_base.cc
+++ b/src/yb/server/server_base.cc
@@ -57,6 +57,7 @@
 #include "yb/server/generic_service.h"
 #include "yb/server/glog_metrics.h"
 #include "yb/server/hybrid_clock.h"
+#include "yb/server/intentsdb-path-handler.h"
 #include "yb/server/logical_clock.h"
 #include "yb/server/rpc_server.h"
 #include "yb/server/rpcz-path-handler.h"
@@ -618,6 +619,8 @@ void RpcAndWebServerBase::DisplayGeneralInfoIcons(std::stringstream* output) {
   DisplayIconTile(output, "fa-hdd-o", "Drives", "/drives");
   // TLS.
   DisplayIconTile(output, "fa-lock", "TLS", "/tls");
+  // IntentsDB.
+  DisplayIconTile(output, "fa-lock", "IntentsDB", "/intentsdb");
 }
 
 Status RpcAndWebServerBase::DisplayRpcIcons(std::stringstream* output) {
@@ -647,6 +650,7 @@ Status RpcAndWebServerBase::Start() {
 
   AddDefaultPathHandlers(web_server_.get());
   AddRpczPathHandlers(messenger_.get(), web_server_.get());
+  AddIntentsDBPathHandler(messenger_.get(), web_server_.get());
   RegisterMetricsJsonHandler(web_server_.get(), metric_registry_.get());
   RegisterPathUsageHandler(web_server_.get(), fs_manager_.get());
   RegisterTlsHandler(web_server_.get(), this);
diff --git a/src/yb/tablet/tablet.cc b/src/yb/tablet/tablet.cc
index b777388a0645..1c6c99568c50 100644
--- a/src/yb/tablet/tablet.cc
+++ b/src/yb/tablet/tablet.cc
@@ -3564,6 +3564,29 @@ Result<int64_t> Tablet::CountIntents() {
   return num_intents;
 }
 
+Status Tablet::ReadIntents(tserver::ReadIntentsResponsePB* resp) {
+  auto pending_op = CreateNonAbortableScopedRWOperation();
+  RETURN_NOT_OK(pending_op);
+
+  if (!intents_db_) {
+    return Status::OK();
+  }
+
+  rocksdb::ReadOptions read_options;
+  auto intent_iter = std::unique_ptr<rocksdb::Iterator>(
+      intents_db_->NewIterator(read_options));
+  intent_iter->SeekToFirst();
+  docdb::SchemaPackingStorage schema_packing_storage;
+
+  for (; intent_iter->Valid(); intent_iter->Next()) {
+    auto item = EntryToString(intent_iter->key(), intent_iter->value(),
+      schema_packing_storage, docdb::StorageDbType::kIntents);
+    *resp->add_intents() = item;
+  }
+
+  return Status::OK();
+}
+
 void Tablet::ListenNumSSTFilesChanged(std::function<void()> listener) {
   std::lock_guard<std::mutex> lock(num_sst_files_changed_listener_mutex_);
   bool has_new_listener = listener != nullptr;
diff --git a/src/yb/tablet/tablet.h b/src/yb/tablet/tablet.h
index 61d85ca5fdce..8cc38c9d2083 100644
--- a/src/yb/tablet/tablet.h
+++ b/src/yb/tablet/tablet.h
@@ -62,6 +62,8 @@
 #include "yb/tablet/tablet_options.h"
 #include "yb/tablet/transaction_intent_applier.h"
 
+#include "yb/tserver/tserver.fwd.h"
+
 #include "yb/util/status_fwd.h"
 #include "yb/util/enums.h"
 #include "yb/util/locks.h"
@@ -654,6 +656,10 @@ class Tablet : public AbstractTablet, public TransactionIntentApplier {
   // Scans the intent db. Potentially takes a long time. Used for testing/debugging.
   Result<int64_t> CountIntents();
 
+  // Get all the entries of intent db.
+  // Potentially takes a long time. Used for testing/debugging.
+  Status ReadIntents(tserver::ReadIntentsResponsePB* resp);
+
   // Flushed intents db if necessary.
   void FlushIntentsDbIfNecessary(const yb::OpId& lastest_log_entry_op_id);
 
diff --git a/src/yb/tserver/tablet_service.cc b/src/yb/tserver/tablet_service.cc
index 3967b3daabf6..464f05598204 100644
--- a/src/yb/tserver/tablet_service.cc
+++ b/src/yb/tserver/tablet_service.cc
@@ -1585,6 +1585,40 @@ void TabletServiceAdminImpl::CountIntents(
   context.RespondSuccess();
 }
 
+void TabletServiceAdminImpl::ReadIntents(
+    const ReadIntentsRequestPB* req,
+    ReadIntentsResponsePB* resp,
+    rpc::RpcContext context) {
+  TSTabletManager::TabletPtrs tablet_ptrs;
+  TabletPeers tablet_peers;
+
+  if (req->has_tablet_id()) {
+    auto tablet_peer = VERIFY_RESULT_OR_RETURN(LookupTabletPeerOrRespond(
+        server_->tablet_peer_lookup(), req->tablet_id(), resp, &context));
+    auto tablet = tablet_peer.tablet;
+    if (tablet != nullptr) {
+      tablet_ptrs.push_back(std::move(tablet));
+    } else {
+      SetupErrorAndRespond(resp->mutable_error(),
+          STATUS_FORMAT(InternalError, "Unable to lookup for tablet with ID $0", req->tablet_id()),
+          &context);
+      return;
+    }
+  } else {
+    tablet_peers = server_->tablet_manager()->GetTabletPeers(&tablet_ptrs);
+  }
+
+  for (const auto& tablet : tablet_ptrs) {
+    auto res = tablet->ReadIntents(resp);
+    if (!res.ok()) {
+      SetupErrorAndRespond(resp->mutable_error(), res, &context);
+      return;
+    }
+  }
+
+  context.RespondSuccess();
+}
+
 void TabletServiceAdminImpl::AddTableToTablet(
     const AddTableToTabletRequestPB* req, AddTableToTabletResponsePB* resp,
     rpc::RpcContext context) {
diff --git a/src/yb/tserver/tablet_service.h b/src/yb/tserver/tablet_service.h
index 902ac4779b21..85f6a3a0432e 100644
--- a/src/yb/tserver/tablet_service.h
+++ b/src/yb/tserver/tablet_service.h
@@ -227,6 +227,10 @@ class TabletServiceAdminImpl : public TabletServerAdminServiceIf {
                     CountIntentsResponsePB* resp,
                     rpc::RpcContext context) override;
 
+  void ReadIntents(const ReadIntentsRequestPB* req,
+                   ReadIntentsResponsePB* resp,
+                   rpc::RpcContext context) override;
+
   void AddTableToTablet(const AddTableToTabletRequestPB* req,
                         AddTableToTabletResponsePB* resp,
                         rpc::RpcContext context) override;
diff --git a/src/yb/tserver/tserver.proto b/src/yb/tserver/tserver.proto
index a8c65ca9ca19..b62c278f271e 100644
--- a/src/yb/tserver/tserver.proto
+++ b/src/yb/tserver/tserver.proto
@@ -300,3 +300,15 @@ message IsTabletServerReadyResponsePB {
   optional int32 num_tablets_not_running = 2;
   optional int32 total_tablets = 3 [default = 0];
 }
+
+message ReadIntentsRequestPB {
+  // If specified, only read the intents from this tablet ID.
+  optional bytes tablet_id = 1;
+}
+
+message ReadIntentsResponsePB {
+  optional TabletServerErrorPB error = 1;
+
+  // List of intents
+  repeated bytes intents = 2;
+}
diff --git a/src/yb/tserver/tserver_admin.proto b/src/yb/tserver/tserver_admin.proto
index 9a8e6568868e..fc8c2eb02b87 100644
--- a/src/yb/tserver/tserver_admin.proto
+++ b/src/yb/tserver/tserver_admin.proto
@@ -41,6 +41,7 @@ import "yb/consensus/metadata.proto";
 import "yb/tablet/metadata.proto";
 import "yb/tablet/operations.proto";
 import "yb/tablet/tablet_types.proto";
+import "yb/tserver/tserver.proto";
 import "yb/tserver/tserver_types.proto";
 
 // This is used to export tablet metadata changes to a protobuf file to be reloaded on a new cluster
@@ -349,6 +350,8 @@ service TabletServerAdminService {
 
   rpc CountIntents(CountIntentsRequestPB) returns (CountIntentsResponsePB);
 
+  rpc ReadIntents(ReadIntentsRequestPB) returns (ReadIntentsResponsePB);
+
   rpc AddTableToTablet(AddTableToTabletRequestPB) returns (AddTableToTabletResponsePB);
 
   rpc RemoveTableFromTablet(RemoveTableFromTabletRequestPB)
