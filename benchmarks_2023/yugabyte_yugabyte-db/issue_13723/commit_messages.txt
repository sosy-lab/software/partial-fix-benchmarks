Allow using a custom-built Clang version on macOS

Summary:
We can build newer versions of Clang and make them available using the `llvm-installer` Python module on macOS in addition to Linux.

When using our custom-built Clang, we have to specify macOS SDK directory using compiler and linker flags.

Other changes:
- Refactoring CMake scripts (yb_disable_tests_if_necessary, add_latest_symlink_target functions).
- Creating the `toolchain` link in the build directory pointing to the LLVM toolchain, and the `latest-for-clangd` link in the parent directory of build directories, so that we can always refer to the appropriate clangd version as `build/latest-fo-clangd/toolchain/bin/clangd` from IDE (e.g. Visual Studio Code) configuration.

Test Plan:
Jenkins: compile only

./yb_build.sh --clang14

Reviewers: sergei

Reviewed By: sergei

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D19751
[#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Test Plan:
ybd --cxx-test consensus_raft_consensus-test

Jenkins: hot

Reviewers: asrivastava, amitanand

Reviewed By: amitanand

Subscribers: qhu, kpopali, rthallam, kannan, ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D19130
[BACKPORT 2.14][#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Original commit: aaea395461229ff8eb78e2f13a65236c0487262b / D19130
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Test Plan:
ybd --cxx-test consensus_raft_consensus-test

Jenkins: hot

Reviewers: asrivastava, amitanand

Reviewed By: amitanand

Subscribers: bogdan, ybase, kannan, rthallam, kpopali, qhu

Differential Revision: https://phabricator.dev.yugabyte.com/D19780
[BACKPORT 2.12][#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Original commit: aaea395461229ff8eb78e2f13a65236c0487262b / D19130
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Test Plan:
ybd --cxx-test consensus_raft_consensus-test

Jenkins: hot

Reviewers: asrivastava, amitanand

Reviewed By: amitanand

Subscribers: qhu, kpopali, rthallam, kannan, ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D19781
[BACKPORT 2.8][#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Original commit: aaea395461229ff8eb78e2f13a65236c0487262b / D19130
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Test Plan:
ybd --cxx-test consensus_raft_consensus-test

Jenkins: hot

Reviewers: asrivastava, amitanand

Reviewed By: amitanand

Subscribers: bogdan, ybase, kannan, rthallam, kpopali, qhu

Differential Revision: https://phabricator.dev.yugabyte.com/D19782
Revert "[BACKPORT 2.8][#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well"

Summary: This reverts commit d473c32ca95ef2d70694196494c50184f14b7fe4.

Test Plan: Jenkins

Reviewers: amitanand, bogdan, rthallam

Reviewed By: rthallam

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D19883
Revert "[BACKPORT 2.12][#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well"

Summary: This reverts commit cf05fc6135612fc93c9a54107403799eff6ca6f2.

Test Plan: Jenkins

Reviewers: amitanand, bogdan, rthallam

Reviewed By: rthallam

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D19884
Revert "[BACKPORT 2.14][#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well"

Summary: This reverts commit 7ef3521cdc11e1c9ce776de50b469f406dee9182.

Test Plan: Jenkins

Reviewers: amitanand, bogdan, rthallam

Reviewed By: rthallam

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D19885
Revert "[#13723] Update last_received_current_leader sent from replica to leader by including de-duped messages as well"

Summary: This reverts commit aaea395461229ff8eb78e2f13a65236c0487262b.

Test Plan: Jenkins

Reviewers: rthallam

Reviewed By: rthallam

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D19886
[#15629] Docdb: Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Previously this was committed as: https://phabricator.dev.yugabyte.com/D19130
(See #14213, #13723)

However, there was a change where the next_index_ was also getting updated. This diff
ensures that next_index_ is not updated.

Also, add a log line to log information about the tablet if/when this happens. This
should be rare, so logging as VLOG(0)

Test Plan:
ybd --cxx-test consensus_raft_consensus-test

Reviewers: rthallam, rsami

Reviewed By: rsami

Subscribers: sergei, qhu, ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D22183
[BACKPORT 2.16][#15629] Docdb: Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Original commit: b349fa73d1d40f59c948c0264e5350edd74ddae7 / D22183
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Previously this was committed as: https://phabricator.dev.yugabyte.com/D19130
(See #14213, #13723)

However, there was a change where the next_index_ was also getting updated. This diff
ensures that next_index_ is not updated.

Also, add a log line to log information about the tablet if/when this happens. This
should be rare, so logging as VLOG(0)

Test Plan: ybd --cxx-test consensus_raft_consensus-test

Reviewers: rsami, rthallam

Reviewed By: rthallam

Subscribers: bogdan, ybase, qhu, sergei

Differential Revision: https://phabricator.dev.yugabyte.com/D23588
[BACKPORT 2.14][#15629] Docdb: Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Original commit: b349fa73d1d40f59c948c0264e5350edd74ddae7 / D22183
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Previously this was committed as: https://phabricator.dev.yugabyte.com/D19130
(See #14213, #13723)

However, there was a change where the next_index_ was also getting updated. This diff
ensures that next_index_ is not updated.

Also, add a log line to log information about the tablet if/when this happens. This
should be rare, so logging as VLOG(0)

Test Plan: ybd --cxx-test consensus_raft_consensus-test

Reviewers: rsami, rthallam

Reviewed By: rthallam

Subscribers: bogdan, ybase, qhu, sergei

Differential Revision: https://phabricator.dev.yugabyte.com/D23580
[BACKPORT 2.12][#15629] Docdb: Update last_received_current_leader sent from replica to leader by including de-duped messages as well

Summary:
Original commit: b349fa73d1d40f59c948c0264e5350edd74ddae7 / D22183
Before this revision, it was possible that a replica could end up in a state where:
1) It had received operations
2) that the current leader considers committed
3) and that the current leader does not realize this replica already has

In this case, we would end up finding all proposed operations in an UpdateConsensus call in the
replica's local log, and they would all be "deduplicated". This code path prevented us from updating
what value we send back to the leader for last_received_current_leader, and we would instead send
(0, 0). The leader would get stuck in a state where it was trying to send the same messages over and
over again, and the follower would never acknowledge receipt of anything.

In this revision, we update last_received_current_leader based on the last OpId we saw while
deduping the requests received from the leader. We only do this in the case that
last_received_op_id_current_leader_ is MinimumOpId().

Previously this was committed as: https://phabricator.dev.yugabyte.com/D19130
(See #14213, #13723)

However, there was a change where the next_index_ was also getting updated. This diff
ensures that next_index_ is not updated.

Also, add a log line to log information about the tablet if/when this happens. This
should be rare, so logging as VLOG(0)

Test Plan: ybd --cxx-test consensus_raft_consensus-test

Reviewers: rsami, rthallam

Reviewed By: rthallam

Subscribers: sergei, qhu, ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D23581
