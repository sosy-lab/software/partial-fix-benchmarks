diff --git a/src/postgres/contrib/yb_pg_metrics/yb_pg_metrics.c b/src/postgres/contrib/yb_pg_metrics/yb_pg_metrics.c
index 2a276db03e99..781cee4edf0c 100644
--- a/src/postgres/contrib/yb_pg_metrics/yb_pg_metrics.c
+++ b/src/postgres/contrib/yb_pg_metrics/yb_pg_metrics.c
@@ -96,7 +96,7 @@ int port = 0;
 static int num_backends = 0;
 static rpczEntry *rpcz = NULL;
 static MemoryContext ybrpczMemoryContext = NULL;
-PgBackendStatus **backendStatusArrayPointer = NULL;
+PgBackendStatus *backendStatusArray = NULL;
 extern int MaxConnections;
 
 static long last_cache_misses_val = 0;
@@ -206,9 +206,6 @@ getElapsedMs(TimestampTz start_time, TimestampTz stop_time)
 void
 pullRpczEntries(void)
 {
-  if (!(*backendStatusArrayPointer))
-    elog(LOG, "Backend Status Array hasn't been initialized yet.");
-
   ybrpczMemoryContext = AllocSetContextCreate(TopMemoryContext,
                                              "YB RPCz memory context",
                                              ALLOCSET_SMALL_SIZES);
@@ -217,9 +214,7 @@ pullRpczEntries(void)
   rpcz = (rpczEntry *) palloc(sizeof(rpczEntry) * NumBackendStatSlots);
 
   num_backends = NumBackendStatSlots;
-  volatile PgBackendStatus *beentry;
-
-  beentry = *backendStatusArrayPointer;
+  volatile PgBackendStatus *beentry = backendStatusArray;
 
   for (int i = 0; i < NumBackendStatSlots; i++)
   {
@@ -237,7 +232,6 @@ pullRpczEntries(void)
       before_changecount = beentry->st_changecount;
 
       rpcz[i].proc_id = beentry->st_procpid;
-      rpcz[i].new_conn = beentry->yb_new_conn;
 
       /* avoid filling any more fields if invalid */
       if (beentry->st_procpid <= 0) {
@@ -340,20 +334,23 @@ freeRpczEntries(void)
 void
 webserver_worker_main(Datum unused)
 {
-  /*
-   * We need to use a pointer to a pointer here because the shared memory for BackendStatusArray
-   * is not allocated when we enter this function. The memory is allocated after the background
-   * works are registered.
-   */
-
   YBCInitThreading();
 
-  backendStatusArrayPointer = getBackendStatusArrayPointer();
+  backendStatusArray = getBackendStatusArray();
 
   BackgroundWorkerUnblockSignals();
 
   HandleYBStatus(YBCInitGFlags(NULL /* argv[0] */));
 
+  /*
+   * Assert that shared memory is allocated to backendStatusArray before this webserver
+   * is started.
+  */
+  if (!backendStatusArray)
+    ereport(FATAL,
+            (errcode(ERRCODE_INTERNAL_ERROR),
+             errmsg("Shared memory not allocated to BackendStatusArray before starting YSQL webserver")));
+
   webserver = CreateWebserver(ListenAddresses, port);
 
   RegisterMetrics(ybpgm_table, num_entries, metric_node_name);
@@ -365,7 +362,12 @@ webserver_worker_main(Datum unused)
   callbacks.getTimestampTzDiffMs = getElapsedMs;
   callbacks.getTimestampTzToStr  = timestamptz_to_str;
 
-  RegisterRpczEntries(&callbacks, &num_backends, &rpcz, yb_too_many_conn, &MaxConnections);
+  YbConnectionMetrics conn_metrics;
+  conn_metrics.max_conn = &MaxConnections;
+  conn_metrics.too_many_conn = yb_too_many_conn;
+  conn_metrics.new_conn = yb_new_conn;
+
+  RegisterRpczEntries(&callbacks, &num_backends, &rpcz, &conn_metrics);
 
   HandleYBStatus(StartWebserver(webserver));
 
diff --git a/src/postgres/src/backend/postmaster/pgstat.c b/src/postgres/src/backend/postmaster/pgstat.c
index 31b0f74a06e8..5b13269748e2 100644
--- a/src/postgres/src/backend/postmaster/pgstat.c
+++ b/src/postgres/src/backend/postmaster/pgstat.c
@@ -2653,6 +2653,8 @@ static Size BackendActivityBufferSize = 0;
 static PgBackendSSLStatus *BackendSslStatusBuffer = NULL;
 #endif
 
+uint64_t *yb_new_conn = NULL;
+
 
 /*
  * Report shared-memory space needed by CreateSharedBackendStatus.
@@ -2679,6 +2681,10 @@ BackendStatusShmemSize(void)
 					mul_size(sizeof(PgBackendSSLStatus), NumBackendStatSlots));
 #endif
 	size = add_size(size, mul_size(NAMEDATALEN, NumBackendStatSlots));
+
+	/* yb_new_conn metric */
+	size = add_size(size, sizeof(uint64_t));
+
 	return size;
 }
 
@@ -2805,6 +2811,9 @@ CreateSharedBackendStatus(void)
 		}
 	}
 #endif
+
+	yb_new_conn = (uint64_t *) ShmemAlloc(sizeof(uint64_t));
+	(*yb_new_conn) = 0;
 }
 
 
@@ -2994,6 +3003,11 @@ pgstat_bestart(void)
 	lbeentry.st_xact_start_timestamp = 0;
 	lbeentry.st_databaseid = MyDatabaseId;
 
+	/* Increment the total connections counter */
+	if (lbeentry.st_procpid > 0 && lbeentry.st_backendType == B_BACKEND)
+		(*yb_new_conn)++;
+
+
 	if (YBIsEnabledInPostgresEnvVar() && lbeentry.st_databaseid > 0) {
 		HeapTuple tuple;
 		tuple = SearchSysCache1(DATABASEOID, ObjectIdGetDatum(lbeentry.st_databaseid));
@@ -3196,7 +3210,6 @@ pgstat_report_activity(BackendState state, const char *cmd_str)
 			PGSTAT_BEGIN_WRITE_ACTIVITY(beentry);
 			beentry->st_state = STATE_DISABLED;
 			beentry->st_state_start_timestamp = 0;
-			beentry->yb_new_conn = 0;
 			beentry->st_activity_raw[0] = '\0';
 			beentry->st_activity_start_timestamp = 0;
 			/* st_xact_start_timestamp and wait_event_info are also disabled */
@@ -3228,10 +3241,6 @@ pgstat_report_activity(BackendState state, const char *cmd_str)
 	 */
 	PGSTAT_BEGIN_WRITE_ACTIVITY(beentry);
 
-	if ((state == STATE_RUNNING || state == STATE_FASTPATH) &&
-		beentry->st_state != state) {
-		beentry->yb_new_conn++;
-	}
 	beentry->st_state = state;
 	beentry->st_state_start_timestamp = current_timestamp;
 
@@ -6908,8 +6917,8 @@ pgstat_clip_activity(const char *raw_activity)
 	return activity;
 }
 
-PgBackendStatus **
-getBackendStatusArrayPointer(void)
+PgBackendStatus *
+getBackendStatusArray(void)
 {
-	return &BackendStatusArray;
+  return BackendStatusArray;
 }
diff --git a/src/postgres/src/include/pgstat.h b/src/postgres/src/include/pgstat.h
index 6e3fab096941..60546c327bc9 100644
--- a/src/postgres/src/include/pgstat.h
+++ b/src/postgres/src/include/pgstat.h
@@ -1077,8 +1077,6 @@ typedef struct PgBackendStatus
 
 	/* current state */
 	BackendState st_state;
-	/* new connection count */
-	int yb_new_conn;
 
 	/* application name; MUST be null-terminated */
 	char	   *st_appname;
@@ -1469,6 +1467,12 @@ extern PgStat_StatFuncEntry *pgstat_fetch_stat_funcentry(Oid funcid);
 extern int	pgstat_fetch_stat_numbackends(void);
 extern PgStat_ArchiverStats *pgstat_fetch_stat_archiver(void);
 extern PgStat_GlobalStats *pgstat_fetch_global(void);
-extern PgBackendStatus **getBackendStatusArrayPointer(void);
+extern PgBackendStatus		*getBackendStatusArray(void);
+
+/*
+ * Metric to track number of sql connections established since
+ * postmaster started.
+ */
+extern uint64_t *yb_new_conn;
 
 #endif							/* PGSTAT_H */
diff --git a/src/yb/server/pgsql_webserver_wrapper.cc b/src/yb/server/pgsql_webserver_wrapper.cc
index c0a5acd957a7..3196548738ef 100644
--- a/src/yb/server/pgsql_webserver_wrapper.cc
+++ b/src/yb/server/pgsql_webserver_wrapper.cc
@@ -39,8 +39,7 @@ MetricEntity::AttributeMap prometheus_attr;
 static void (*pullYsqlStatementStats)(void *);
 static void (*resetYsqlStatementStats)();
 static rpczEntry **rpczResultPointer;
-static int* too_many_conn_p = NULL;
-static int* max_conn_p = NULL;
+static YbConnectionMetrics *conn_metrics = NULL;
 
 static postgresCallbacks pgCallbacks;
 
@@ -65,7 +64,6 @@ void emitConnectionMetrics(PrometheusWriter *pwriter) {
   rpczEntry *entry = *rpczResultPointer;
 
   uint64_t tot_connections = 0;
-  uint64_t new_connections = 0;
   uint64_t tot_active_connections = 0;
   for (int i = 0; i < *num_backends; ++i, ++entry) {
     if (entry->proc_id > 0) {
@@ -74,7 +72,6 @@ void emitConnectionMetrics(PrometheusWriter *pwriter) {
       }
       tot_connections++;
     }
-    new_connections += entry->new_conn;
   }
 
   std::ostringstream errMsg;
@@ -89,22 +86,28 @@ void emitConnectionMetrics(PrometheusWriter *pwriter) {
       pwriter->WriteSingleEntryNonTable(
           prometheus_attr, PSQL_SERVER_CONNECTION_TOTAL, tot_connections),
       errMsg.str());
-  if (max_conn_p) {
-    WARN_NOT_OK(
-      pwriter->WriteSingleEntryNonTable(
-          prometheus_attr, PSQL_SERVER_MAX_CONNECTION_TOTAL, *max_conn_p),
-      errMsg.str());
-  }
-  if (too_many_conn_p) {
-    WARN_NOT_OK(
-      pwriter->WriteSingleEntryNonTable(
-          prometheus_attr, PSQL_SERVER_CONNECTION_OVER_LIMIT, *too_many_conn_p),
-      errMsg.str());
+
+  if (conn_metrics) {
+    if (conn_metrics->max_conn) {
+      WARN_NOT_OK(
+          pwriter->WriteSingleEntryNonTable(
+              prometheus_attr, PSQL_SERVER_MAX_CONNECTION_TOTAL, *conn_metrics->max_conn),
+          errMsg.str());
+    }
+    if (conn_metrics->too_many_conn) {
+      WARN_NOT_OK(
+          pwriter->WriteSingleEntryNonTable(
+              prometheus_attr, PSQL_SERVER_CONNECTION_OVER_LIMIT, *conn_metrics->too_many_conn),
+          errMsg.str());
+    }
+    if (conn_metrics->new_conn) {
+      WARN_NOT_OK(
+          pwriter->WriteSingleEntryNonTable(
+              prometheus_attr, PSQL_SERVER_NEW_CONNECTION_TOTAL, *conn_metrics->new_conn),
+          errMsg.str());
+    }
   }
-  WARN_NOT_OK(
-    pwriter->WriteSingleEntryNonTable(
-          prometheus_attr, PSQL_SERVER_NEW_CONNECTION_TOTAL, new_connections),
-    errMsg.str());
+
   pgCallbacks.freeRpczEntries();
 }
 
@@ -369,12 +372,11 @@ void RegisterResetYsqlStatStatements(void (*fn)()) {
 
 void RegisterRpczEntries(
     postgresCallbacks *callbacks, int *num_backends_ptr, rpczEntry **rpczEntriesPointer,
-    int* too_many_conn_ptr, int* max_conn_ptr) {
+    YbConnectionMetrics *conn_metrics_ptr) {
   pgCallbacks = *callbacks;
   num_backends = num_backends_ptr;
   rpczResultPointer = rpczEntriesPointer;
-  too_many_conn_p = too_many_conn_ptr;
-  max_conn_p = max_conn_ptr;
+  conn_metrics = conn_metrics_ptr;
 }
 
 YBCStatus StartWebserver(WebserverWrapper *webserver_wrapper) {
diff --git a/src/yb/server/pgsql_webserver_wrapper.h b/src/yb/server/pgsql_webserver_wrapper.h
index 3c8893d7b087..eb40436a7138 100644
--- a/src/yb/server/pgsql_webserver_wrapper.h
+++ b/src/yb/server/pgsql_webserver_wrapper.h
@@ -52,7 +52,6 @@ typedef struct rpczEntry {
   int64 query_start_timestamp;
   char *backend_type;
   uint8 backend_active;
-  int new_conn;
   char *backend_status;
   char *host;
   char *port;
@@ -81,11 +80,22 @@ typedef struct {
   const char *(*getTimestampTzToStr)(int64);
 } postgresCallbacks;
 
+typedef struct {
+  /* # of connections rejected due to the connection limit. */
+  int *too_many_conn;
+
+  /* maximum # of concurrent sql connections allowed. */
+  int *max_conn;
+
+  /* # of connections established since start of postmaster. */
+  uint64_t *new_conn;
+} YbConnectionMetrics;
+
 struct WebserverWrapper *CreateWebserver(char *listen_addresses, int port);
 void RegisterMetrics(ybpgmEntry *tab, int num_entries, char *metric_node_name);
 void RegisterRpczEntries(
     postgresCallbacks *callbacks, int *num_backends_ptr, rpczEntry **rpczEntriesPointer,
-    int* too_many_conn_ptr, int* max_conn_ptr);
+    YbConnectionMetrics *conn_metrics_ptr);
 YBCStatus StartWebserver(struct WebserverWrapper *webserver);
 void RegisterGetYsqlStatStatements(void (*getYsqlStatementStats)(void *));
 void RegisterResetYsqlStatStatements(void (*fn)());
