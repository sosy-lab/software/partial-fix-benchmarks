diff --git a/src/yb/util/env-test.cc b/src/yb/util/env-test.cc
index 72871300c2a8..970b423ae583 100644
--- a/src/yb/util/env-test.cc
+++ b/src/yb/util/env-test.cc
@@ -55,6 +55,7 @@
 #include "yb/util/status.h"
 #include "yb/util/stopwatch.h"
 #include "yb/util/test_macros.h"
+#include "yb/util/test_thread_holder.h"
 #include "yb/util/test_util.h"
 
 DECLARE_int32(o_direct_block_size_bytes);
@@ -781,6 +782,34 @@ TEST_F(TestEnv, TestRWFile) {
   ASSERT_EQ(result, kNewTestData);
 }
 
+TEST_F(TestEnv, NonblockWritableFile) {
+  string path = GetTestPath("test_nonblock");
+  mkfifo(path.c_str(), 0644);
+
+  WritableFileOptions opts;
+  opts.mode = Env::CREATE_NONBLOCK_IF_NON_EXISTING;
+  std::shared_ptr<WritableFile> writer;
+  ASSERT_OK(env_util::OpenFileForWrite(opts, env_.get(), path, &writer));
+
+  TestThreadHolder threads;
+
+  auto read_data = [this, &path] {
+    std::shared_ptr<SequentialFile> reader;
+    ASSERT_OK(env_util::OpenFileForSequential(env_.get(), path, &reader));
+    Slice s;
+    std::vector<uint8_t> scratch(kOneMb);
+    ASSERT_OK(reader->Read(kOneMb, &s, scratch.data()));
+    ASSERT_EQ(s.size(), kOneMb);
+  };
+
+  threads.AddThreadFunctor(read_data);
+
+  // Write 1 MB
+  uint8_t scratch[kOneMb] = {};
+  Slice slice(scratch, kOneMb);
+  ASSERT_OK(writer->Append(slice));
+}
+
 TEST_F(TestEnv, TestCanonicalize) {
   vector<string> synonyms = { GetTestPath("."), GetTestPath("./."), GetTestPath(".//./") };
   for (const string& synonym : synonyms) {
diff --git a/src/yb/util/env.h b/src/yb/util/env.h
index b13ab1bc4e51..0d2f564e47d2 100644
--- a/src/yb/util/env.h
+++ b/src/yb/util/env.h
@@ -172,13 +172,15 @@ class Env {
  public:
   // Governs if/how the file is created.
   //
-  // enum value                      | file exists       | file does not exist
-  // --------------------------------+-------------------+--------------------
-  // CREATE_IF_NON_EXISTING_TRUNCATE | opens + truncates | creates
-  // CREATE_NON_EXISTING             | fails             | creates
-  // OPEN_EXISTING                   | opens             | fails
+  // enum value                      | file exists        | file does not exist
+  // --------------------------------+--------------------+--------------------
+  // CREATE_IF_NON_EXISTING_TRUNCATE | opens + truncates  | creates
+  // CREATE_NONBLOCK_IF_NON_EXISTING | opens (O_NONBLOCK) | creates (O_NONBLOCK)
+  // CREATE_NON_EXISTING             | fails              | creates
+  // OPEN_EXISTING                   | opens              | fails
   enum CreateMode {
     CREATE_IF_NON_EXISTING_TRUNCATE,
+    CREATE_NONBLOCK_IF_NON_EXISTING,
     CREATE_NON_EXISTING,
     OPEN_EXISTING
   };
diff --git a/src/yb/util/env_posix.cc b/src/yb/util/env_posix.cc
index 9dc61b41f702..c6925164d61d 100644
--- a/src/yb/util/env_posix.cc
+++ b/src/yb/util/env_posix.cc
@@ -218,6 +218,9 @@ static Status DoOpen(const string& filename, Env::CreateMode mode, int* fd, int
     case Env::CREATE_IF_NON_EXISTING_TRUNCATE:
       flags |= O_CREAT | O_TRUNC;
       break;
+    case Env::CREATE_NONBLOCK_IF_NON_EXISTING:
+      flags |= O_CREAT | O_NONBLOCK;
+      break;
     case Env::CREATE_NON_EXISTING:
       flags |= O_CREAT | O_EXCL;
       break;
@@ -428,6 +431,22 @@ class PosixWritableFile : public WritableFile {
   const string& filename() const override { return filename_; }
 
  protected:
+    void UnwrittenRemaining(struct iovec** remaining_iov, ssize_t written, int* remaining_count) {
+      size_t bytes_to_consume = written;
+      do {
+        if (bytes_to_consume >= (*remaining_iov)->iov_len) {
+          bytes_to_consume -= (*remaining_iov)->iov_len;
+          (*remaining_iov)++;
+          (*remaining_count)--;
+        } else {
+          (*remaining_iov)->iov_len -= bytes_to_consume;
+          (*remaining_iov)->iov_base =
+              static_cast<uint8_t*>((*remaining_iov)->iov_base) + bytes_to_consume;
+          bytes_to_consume = 0;
+        }
+      } while (bytes_to_consume > 0);
+    }
+
     const std::string filename_;
     int fd_;
     bool sync_on_close_;
@@ -450,19 +469,29 @@ class PosixWritableFile : public WritableFile {
       nbytes += data.size();
     }
 
-    ssize_t written = writev(fd_, iov, n);
+    struct iovec* remaining_iov = iov;
+    int remaining_count = static_cast<int>(n);
+    ssize_t total_written = 0;
 
-    if (PREDICT_FALSE(written == -1)) {
-      int err = errno;
-      return STATUS_IO_ERROR(filename_, err);
+    while (remaining_count > 0) {
+      ssize_t written = writev(fd_, remaining_iov, remaining_count);
+      if (PREDICT_FALSE(written == -1)) {
+        if (errno == EINTR || errno == EAGAIN) {
+          continue;
+        }
+        return STATUS_IO_ERROR(filename_, errno);
+      }
+
+      UnwrittenRemaining(&remaining_iov, written, &remaining_count);
+      total_written += written;
     }
 
-    filesize_ += written;
+    filesize_ += total_written;
 
-    if (PREDICT_FALSE(written != nbytes)) {
+    if (PREDICT_FALSE(total_written != nbytes)) {
       return STATUS_FORMAT(
           IOError, "writev error: expected to write $0 bytes, wrote $1 bytes instead",
-          nbytes, written);
+          nbytes, total_written);
     }
 
     return Status::OK();
@@ -632,25 +661,36 @@ class PosixDirectIOWritableFile final : public PosixWritableFile {
       iov[j].iov_base = block_ptr_vec_[j].get();
       iov[j].iov_len = block_size_;
     }
-    auto bytes_to_write = blocks_to_write * block_size_;
-    ssize_t written = pwritev(fd_, iov, blocks_to_write, next_write_offset_);
+    ssize_t bytes_to_write = blocks_to_write * block_size_;
 
-    if (PREDICT_FALSE(written == -1)) {
-      int err = errno;
-      return STATUS_IO_ERROR(filename_, err);
+    struct iovec* remaining_iov = iov;
+    int remaining_blocks = static_cast<int>(blocks_to_write);
+    ssize_t total_written = 0;
+
+    while (remaining_blocks > 0) {
+      ssize_t written = pwritev(
+          fd_, remaining_iov, remaining_blocks, next_write_offset_);
+      if (PREDICT_FALSE(written == -1)) {
+        if (errno == EINTR || errno == EAGAIN) {
+          continue;
+        }
+        return STATUS_IO_ERROR(filename_, errno);
+      }
+      next_write_offset_ += written;
+
+      UnwrittenRemaining(&remaining_iov, written, &remaining_blocks);
+      total_written += written;
     }
 
-    if (PREDICT_FALSE(written != bytes_to_write)) {
+    if (PREDICT_FALSE(total_written != bytes_to_write)) {
       return STATUS(IOError,
                     Substitute("pwritev error: expected to write $0 bytes, wrote $1 bytes instead",
-                               bytes_to_write, written));
+                               bytes_to_write, total_written));
     }
 
-    filesize_ = next_write_offset_ + written;
+    filesize_ = next_write_offset_;
     CHECK_EQ(filesize_, align_up(filesize_, block_size_));
 
-    next_write_offset_ = filesize_;
-
     if (last_block_used_bytes_ != block_size_) {
       // Next write will happen at filesize_ - block_size_ offset in the file if the last block is
       // not full.
diff --git a/src/yb/util/memenv/memenv.cc b/src/yb/util/memenv/memenv.cc
index cdb3d29af581..11b0c6f079c4 100644
--- a/src/yb/util/memenv/memenv.cc
+++ b/src/yb/util/memenv/memenv.cc
@@ -436,6 +436,8 @@ class InMemoryEnv : public EnvWrapper {
     if (ContainsKey(file_map_, fname)) {
       switch (mode) {
         case CREATE_IF_NON_EXISTING_TRUNCATE:
+          FALLTHROUGH_INTENDED;
+        case CREATE_NONBLOCK_IF_NON_EXISTING:
           DeleteFileInternal(fname);
           break; // creates a new file below
         case CREATE_NON_EXISTING:
