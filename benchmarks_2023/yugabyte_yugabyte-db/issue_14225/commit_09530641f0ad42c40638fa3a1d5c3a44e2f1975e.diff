diff --git a/src/yb/consensus/raft_consensus.cc b/src/yb/consensus/raft_consensus.cc
index 985f287247bf..58668f7e791d 100644
--- a/src/yb/consensus/raft_consensus.cc
+++ b/src/yb/consensus/raft_consensus.cc
@@ -132,6 +132,9 @@ DEFINE_test_flag(int32, follower_reject_update_consensus_requests_seconds, 0,
                  "the first TEST_follower_reject_update_consensus_requests_seconds seconds after "
                  "the Consensus objet is created.");
 
+DEFINE_test_flag(bool, leader_skip_no_op, false,
+                 "Whether a leader replicate NoOp to follower.");
+
 DEFINE_test_flag(bool, follower_fail_all_prepare, false,
                  "Whether a follower will fail preparing all operations.");
 
@@ -1081,18 +1084,20 @@ Status RaftConsensus::BecomeLeaderUnlocked() {
   // because this method is not executed on the TabletPeer's prepare thread.
   replicate->set_hybrid_time(clock_->Now().ToUint64());
 
-  auto round = make_scoped_refptr<ConsensusRound>(this, replicate);
-  round->SetCallback(MakeNonTrackedRoundCallback(round.get(),
-      [this, term = state_->GetCurrentTermUnlocked()](const Status& status) {
-    // Set 'Leader is ready to serve' flag only for committed NoOp operation
-    // and only if the term is up-to-date.
-    // It is guaranteed that successful notification is called only while holding replicate state
-    // mutex.
-    if (status.ok() && term == state_->GetCurrentTermUnlocked()) {
-      state_->SetLeaderNoOpCommittedUnlocked(true);
-    }
-  }));
-  RETURN_NOT_OK(AppendNewRoundToQueueUnlocked(round));
+  if (!PREDICT_FALSE(FLAGS_TEST_leader_skip_no_op)) {
+    auto round = make_scoped_refptr<ConsensusRound>(this, replicate);
+    round->SetCallback(MakeNonTrackedRoundCallback(round.get(),
+        [this, term = state_->GetCurrentTermUnlocked()](const Status& status) {
+      // Set 'Leader is ready to serve' flag only for committed NoOp operation
+      // and only if the term is up-to-date.
+      // It is guaranteed that successful notification is called only while holding replicate state
+      // mutex.
+      if (status.ok() && term == state_->GetCurrentTermUnlocked()) {
+        state_->SetLeaderNoOpCommittedUnlocked(true);
+      }
+    }));
+    RETURN_NOT_OK(AppendNewRoundToQueueUnlocked(round));
+  }
 
   peer_manager_->SignalRequest(RequestTriggerMode::kNonEmptyOnly);
 
@@ -1362,8 +1367,12 @@ void RaftConsensus::UpdateMajorityReplicated(
     }
   }
 
-  state_->SetMajorityReplicatedLeaseExpirationUnlocked(majority_replicated_data, flags);
-  leader_lease_wait_cond_.notify_all();
+  s = state_->SetMajorityReplicatedLeaseExpirationUnlocked(majority_replicated_data, flags);
+  if (s.ok()) {
+    leader_lease_wait_cond_.notify_all();
+  } else {
+    LOG(WARNING) << "Leader lease expiration was not set: " << s;
+  }
 
   VLOG_WITH_PREFIX(1) << "Marking majority replicated up to "
       << majority_replicated_data.ToString();
@@ -1497,6 +1506,7 @@ Status RaftConsensus::Update(ConsensusRequestPB* request,
     return STATUS(IllegalState, "Rejected: --TEST_follower_reject_update_consensus_requests "
                                 "is set to true.");
   }
+
   TEST_PAUSE_IF_FLAG(TEST_follower_pause_update_consensus_requests);
 
   auto reject_mode = reject_mode_.load(std::memory_order_acquire);
diff --git a/src/yb/consensus/replica_state.cc b/src/yb/consensus/replica_state.cc
index 282a61c98803..fd5a4831d89c 100644
--- a/src/yb/consensus/replica_state.cc
+++ b/src/yb/consensus/replica_state.cc
@@ -1329,9 +1329,21 @@ Result<MicrosTime> ReplicaState::MajorityReplicatedHtLeaseExpiration(
   return result;
 }
 
-void ReplicaState::SetMajorityReplicatedLeaseExpirationUnlocked(
+Status ReplicaState::SetMajorityReplicatedLeaseExpirationUnlocked(
     const MajorityReplicatedData& majority_replicated_data,
     EnumBitSet<SetMajorityReplicatedLeaseExpirationFlag> flags) {
+  if (!leader_no_op_committed_) {
+    // Don't setting leader lease until NoOp at current term is committed.
+    // If the older leader containing old, unreplicated operations is elected as leader again,
+    // when replicating old operations to current node, might have a too low hybrid time.
+    // See https://github.com/yugabyte/yugabyte-db/issues/14225 for more details.
+    return STATUS_FORMAT(IllegalState,
+                         "NoOp of current term is not committed "
+                         "(majority_replicated_lease_expiration_ = $0, "
+                         "majority_replicated_ht_lease_expiration_ = $1)",
+                         majority_replicated_lease_expiration_,
+                         majority_replicated_ht_lease_expiration_.load(std::memory_order_acquire));
+  }
   majority_replicated_lease_expiration_ = majority_replicated_data.leader_lease_expiration;
   majority_replicated_ht_lease_expiration_.store(majority_replicated_data.ht_lease_expiration,
                                                  std::memory_order_release);
@@ -1353,6 +1365,7 @@ void ReplicaState::SetMajorityReplicatedLeaseExpirationUnlocked(
   CoarseTimePoint now;
   RefreshLeaderStateCacheUnlocked(&now);
   cond_.notify_all();
+  return Status::OK();
 }
 
 uint64_t ReplicaState::OnDiskSize() const {
diff --git a/src/yb/consensus/replica_state.h b/src/yb/consensus/replica_state.h
index 8086dd8252d5..702e5fbf55de 100644
--- a/src/yb/consensus/replica_state.h
+++ b/src/yb/consensus/replica_state.h
@@ -372,7 +372,7 @@ class ReplicaState {
   void UpdateOldLeaderLeaseExpirationOnNonLeaderUnlocked(
       const CoarseTimeLease& lease, const PhysicalComponentLease& ht_lease);
 
-  void SetMajorityReplicatedLeaseExpirationUnlocked(
+  Status SetMajorityReplicatedLeaseExpirationUnlocked(
       const MajorityReplicatedData& majority_replicated_data,
       EnumBitSet<SetMajorityReplicatedLeaseExpirationFlag> flags);
 
diff --git a/src/yb/integration-tests/raft_consensus-itest.cc b/src/yb/integration-tests/raft_consensus-itest.cc
index 692218ad75f4..89eabb8e1c69 100644
--- a/src/yb/integration-tests/raft_consensus-itest.cc
+++ b/src/yb/integration-tests/raft_consensus-itest.cc
@@ -241,6 +241,7 @@ class RaftConsensusITest : public TabletServerIntegrationTestBase {
       results->clear();
       ASSERT_NO_FATALS(ScanReplica(replica_proxy, results));
       if (results->size() == expected_count) {
+        LOG(INFO) << "Get rows " << *results;
         return;
       }
       SleepFor(MonoDelta::FromMilliseconds(10));
@@ -418,7 +419,7 @@ class RaftConsensusITest : public TabletServerIntegrationTestBase {
 
   // Writes 'num_writes' operations to the current leader. Each of the operations
   // has a payload of around `size_bytes`. Causes a gtest failure on error.
-  void WriteOpsToLeader(int num_writes, size_t size_bytes);
+  void WriteOpsToLeader(int num_writes, size_t size_bytes, bool accept_failure = false);
 
   // Check for and restart any TS that have crashed.
   // Returns the number of servers restarted.
@@ -726,7 +727,7 @@ TEST_F(RaftConsensusITest, TestRunLeaderElection) {
   ASSERT_ALL_REPLICAS_AGREE(FLAGS_client_inserts_per_thread * num_iters * 2);
 }
 
-void RaftConsensusITest::WriteOpsToLeader(int num_writes, size_t size_bytes) {
+void RaftConsensusITest::WriteOpsToLeader(int num_writes, size_t size_bytes, bool accept_failure) {
   TServerDetails* leader = nullptr;
   ASSERT_OK(GetLeaderReplicaWithRetries(tablet_id_, &leader));
 
@@ -744,9 +745,12 @@ void RaftConsensusITest::WriteOpsToLeader(int num_writes, size_t size_bytes) {
     req.set_tablet_id(tablet_id_);
     AddTestRowInsert(key, key, test_payload, &req);
     key++;
-    ASSERT_OK(leader->tserver_proxy->Write(req, &resp, &rpc));
 
-    ASSERT_FALSE(resp.has_error()) << resp.DebugString();
+    auto s = leader->tserver_proxy->Write(req, &resp, &rpc);
+    if (!accept_failure) {
+      ASSERT_OK(s);
+      ASSERT_FALSE(resp.has_error()) << resp.DebugString();
+    }
   }
 }
 
@@ -3577,5 +3581,74 @@ TEST_F(RaftConsensusITest, CatchupAfterLeaderRestarted) {
   ASSERT_OK(WaitForServersToAgree(60s, tablet_servers_, tablet_id_, kNumOps));
 }
 
+// Test old leader ts-1 has operations not majority replicated and crashed.
+// ts-2 becomes the new leader, but before NO_OP replicated on ts-3, it serves a read,
+// and advanced the safe time lower bound.
+// ts-1 becomes the leader again and replicated its pending operations with
+// lower hybrid time to ts-1. ts-2 should be able to accept those operations.
+TEST_F(RaftConsensusITest, GetSafeTimeBeforeNoOpReplicated) {
+  const auto kTimeout = 30s * kTimeMultiplier;
+  ASSERT_NO_FATALS(BuildAndStart());
+  ASSERT_OK(WaitForServersToAgree(MonoDelta::FromSeconds(10), tablet_servers_,
+                                  tablet_id_, 1));
+
+  const auto leader_idx = CHECK_RESULT(cluster_->GetTabletLeaderIndex(tablet_id_));
+  const auto follower_idx = (leader_idx + 1) % 3;
+  const auto other_follower_idx = (leader_idx + 2) % 3;
+
+  const auto leader = cluster_->tablet_server(leader_idx);
+
+  // Pause one follower and write one more row.
+  const auto follower = cluster_->tablet_server(follower_idx);
+  follower->Shutdown();
+  ASSERT_OK(cluster_->WaitForTSToCrash(follower));
+  // Write two rows: (0, 0, "0") (1, 1, "0").
+  ASSERT_NO_FATALS(WriteOpsToLeader(/* num_writes = */ 2, /* size_bytes = */ 1));
+  std::vector<string> results;
+  ASSERT_NO_FATALS(WaitForRowCount(
+      tablet_servers_[leader->uuid()]->tserver_proxy.get(), 2, &results));
+
+  // Reject update consensus requests on other_follower.
+  const auto other_follower = cluster_->tablet_server(other_follower_idx);
+  ASSERT_OK(
+      cluster_->SetFlag(other_follower, "TEST_follower_reject_update_consensus_requests", "true"));
+
+  // Update (0, 0, "0") => (0, 0, "00") on leader, but not commited.
+  ASSERT_NO_FATALS(
+      WriteOpsToLeader(/* num_writes = */ 1, /* size_bytes = */ 2, /* accept_failure = */ true));
+
+  SleepFor(2s * kTimeMultiplier);
+
+  other_follower->Shutdown();
+  ASSERT_OK(cluster_->WaitForTSToCrash(other_follower));
+
+  // Shutdown leader and then start the other two followers.
+  leader->Shutdown();
+  ASSERT_OK(cluster_->WaitForTSToCrash(leader));
+  ASSERT_OK(follower->Restart(ExternalMiniClusterOptions::kDefaultStartCqlProxy,
+                              {std::make_pair("enable_leader_failure_detection", "false")}));
+  ASSERT_OK(cluster_->WaitForTabletsRunning(follower, kTimeout));
+
+  // Discard the last op to avoid NoOp replicated to follower.
+  ASSERT_OK(other_follower->Restart(
+      ExternalMiniClusterOptions::kDefaultStartCqlProxy,
+      {std::make_pair("TEST_leader_skip_no_op", "true")}));
+
+  // Wait rows: (0, 0, "0") (1, 1, "0") replicated on follower.
+  ASSERT_OK(WaitUntilCommittedOpIdIndexIs(
+      3, tablet_servers_[follower->uuid()].get(), tablet_id_, kTimeout));
+
+  ASSERT_OK(other_follower->Pause());
+  ASSERT_OK(leader->Restart());
+  ASSERT_OK(cluster_->WaitForTabletsRunning(leader, kTimeout));
+
+  ASSERT_OK(WaitUntilLeader(tablet_servers_[leader->uuid()].get(), tablet_id_, kTimeout));
+
+  ASSERT_OK(other_follower->Resume());
+
+  ASSERT_OK(WaitForServersToAgree(MonoDelta::FromSeconds(10), tablet_servers_,
+                                  tablet_id_, 5));
+}
+
 }  // namespace tserver
 }  // namespace yb
