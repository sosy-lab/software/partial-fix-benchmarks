[YSQL] New operation's hybrid time too low FATAL in tablet splitting case with large keys
@qvad can you confirm this was on a version that had the lease revocation flag off?
@bmatican It occurred on 2.15.4.0-b29 version, how can we check default flag version on this build?
[#13611] DocDB: Disable enable_lease_revocation by default

Was in 2.14.4.0-b1 now, so it's indeed off.
For reference http://stress.dev.yugabyte.com/stress_test/9f0388ea-2fe1-4074-8fd3-ed47991a71ad
The tablet peer having issue is the leader at term 12

```
I0927 23:31:35.186293 21116 raft_consensus.cc:3384] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d [term 13 FOLLOWER]: NO_OP [12.200361] replication failed: Aborted (yb/consensus/replica_state.cc:634): Operation aborted by new leader
I0927 23:31:35.272329 21116 log.cc:1839] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d: Reset last synced entry op id from 12.200361 to 11.200360
I0927 23:31:35.272343 21116 consensus_meta.cc:317] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d: Updating active role from FOLLOWER to FOLLOWER. Consensus state: current_term: 13 leader_uuid: "75dd98266e764de7bc492e865dcb25bf" config { opid_index: -1 peers { permanent_uuid: "685986ddf2304b5a98e14437e293803f" member_type: VOTER last_known_private_addr { host: "172.151.18.83" port: 9100 } cloud_info { placement_cloud: "aws" placement_region: "us-west-2" placement_zone: "us-west-2a" } } peers { permanent_uuid: "75dd98266e764de7bc492e865dcb25bf" member_type: VOTER last_known_private_addr { host: "172.151.31.42" port: 9100 } cloud_info { placement_cloud: "aws" placement_region: "us-west-2" placement_zone: "us-west-2a" } } peers { permanent_uuid: "f4da37c3c25f44c7a52504461c87be5d" member_type: VOTER last_known_private_addr { host: "172.151.28.243" port: 9100 } cloud_info { placement_cloud: "aws" placement_region: "us-west-2" placement_zone: "us-west-2a" } } }, has_pending_config = 0
I0927 23:31:35.272374 21116 raft_consensus.cc:3359] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d [term 13 FOLLOWER]: Calling mark dirty synchronously for reason code NEW_LEADER_ELECTED
I0927 23:31:35.272387 21116 replica_state.cc:1156] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d [term 13 FOLLOWER]: Reset our lease: 33.465s
I0927 23:31:35.272397 21116 replica_state.cc:1163] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d [term 13 FOLLOWER]: Reset our ht lease: { physical: 1664321461809671 }
...
New operation's hybrid time too low: { physical: 1664321222064997 }, op id: 11.200361
  max_safe_time_returned_with_lease_={ safe_time: { physical: 1664321461809671 } source: kHybridTimeLease }
```

Current term is 13

the previous ht lease is reset to NoneValue
max_safe_time_returned_with_lease_ is get from the previous ht lease, shall we also reset it?
```
term 11
TS-1      last committed op_id  11.200360
(Leader)TS-2 last op_id 11.200361
TS-3 last committed op_id 11.127389
11.127389. Preceding OpId from leader: 12.200361

TS-1 detect leader failure, and itself become leader after getting TS-3’s vote

term 12
(New leader)TS-1 last committed op_id 12.200361(NO_OP)
TS-2 last op_id 11.200361
TS-3 last committed op_id 11.127389 catching up TS-1

TS-2 start election again, and gets vote from TS-3

term 13
TS-1 last committed op_id 12.200361(NO_OP) <- 11.200361 ….. so 12.200361 is aborted, append 11.200361 to TS-1’s log
(Leader)TS-2 last op_id 11.200361
TS-3 last committed op_id 11.127389
```

`11.200361` is a update transaction op having hybrid time `{ physical: 1664321222064997 }`
[1] op_id: 11.200361 hybrid_time: 6817059725578227712 op_type: UPDATE_TRANSACTION_OP committed_op_id: 11.200360
Still a pending replicate on leader TS-2

the sanity safe time lower bound is ` max_safe_time_returned_with_lease_={ safe_time: { physical: 1664321461809671 } source: kHybridTimeLease }`, should be calculated from ht lease.

And the ht lease has been reset to NoneValue due to abort of NO_OP 12.200361,
`[term 13 FOLLOWER]: Reset our ht lease: { physical: 1664321461809671 }`

Does it make sense to still use the ht lease as the safe time lower bound?

ht_lease can be set at update majority replicated

1664321461809671 is at Tuesday, September 27, 2022 11:31:01.809 PM

Looking for logs nearby:
```
W0927 23:31:01.387616 20254 replica_state.cc:793] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d [term 12 LEADER]: Can't advance the committed index across term boundaries until operations from the current term are replicated. Last committed operation was: 11.200360, New majority replicated is: 11.196175, Current term is: 12
```

Seems we are getting such a big ht lease because of calling UpdateMajorityReplicdated with a latest ht_lease_expiration sent to follower
@Huqicheng could this be due to us having disabled the lease ~expiration~ revocation mechanism? is this happening during leader changes/stepdowns? or just steady state?
@bmatican 

1) do you mean lease revocation?
2) It's happening during leader changes. 
```
F0927 23:31:35.272414 21116 mvcc.cc:393] T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d: T 7b8909519ae64772a5a5b880a7175157 P f4da37c3c25f44c7a52504461c87be5d: Recent 32 MVCC operations:
1. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
2. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321483683504 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
3. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
4. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321483890465 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
5. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
6. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
7. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
8. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
9. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
10. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
11. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321486909626 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
12. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321487077585 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
13. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
14. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
15. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
16. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
17. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
18. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
19. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321489965743 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
20. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321490111706 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
21. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
22. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
23. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
24. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
25. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
26. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
27. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321492984860 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
28. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
29. SafeTime { min_allowed: <min> deadline: 9223372036.855s ht_lease: { time: { physical: 1664321493632846 } lease: { physical: 1664321461809671 } } safe_time: { physical: 1664321461809671 } }
30. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
31. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
32. SafeTimeForFollower { min_allowed: <min> deadline: <uninitialized> safe_time_with_source: { safe_time: { physical: 1664321221771262 } source: kPropagated } }
```
I have reproduced the scenario with a small test

To summarize it:

1. old leader ts-1 has operations not majority replicated and crashed.
   ts-1 has 1.1(NoOp)  1.2 1.3 1.4           last committed 1.3 and last received 1.4
   ts-2 has 1.1(NoOp)  1.2 1.3                last committed 1.3 and last received 1.3
   ts-3 has  1.1(NoOp)                            last committed 1.1 and last received 1.1

2. ts-2 becomes the new leader and replicate 1.2 1.3 to ts-3, but before NO_OP replicated on ts-3, it serves a read,
    and advanced the safe time lower bound.

    ts-1 crashed
    ts-2 has 1.1(NoOp)  1.2 1.3  2.4(NoOp)   last committed 1.3 and last received 2.4
    ts-3 has 1.1(NoOp)  1.2 1.3                        last committed 1.3 and last received 1.3

3. ts-1 becomes the leader again and replicated its pending operation (1.4) with lower hybrid time to ts-2, ts-2 raise a fatal error

New operation's hybrid time too low: { physical: 1666367884528238 }, op id: 1.4    



Closing, fixed by Jonathan's change above.
@Huqicheng , Reactivating for backports.