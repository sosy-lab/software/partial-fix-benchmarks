[#14378] ysql: Output current ysql_max_connections as a metric

Summary: This revision exposes the value for max_connections as a prometheus metric.

Test Plan:
```
curl 127.0.0.1:13000/prometheus-metrics
```
Observe the value for yb_ysqlserver_max_connection_total :
```
yb_ysqlserver_max_connection_total{metric_id="yb.ysqlserver",metric_type="server",exported_instance="dev-server-zyu:9000"} 300 1665371892015
```
verify that it is consistent with the guc shown in ysqlsh:
```
yugabyte=# show max_connections;
 max_connections
-----------------
 300
(1 row)
```

Reviewers: ssong

Reviewed By: ssong

Subscribers: yql, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D20064
[#14324] DocDB: Add master address to tservers when a new master is added in ExternalMiniCluster tests

Summary:
AutoFlags tests adds master to an existing cluster and then restarts all mater and tserver processes.
We should add the new master address to tservers before restarting them.

Fixes #14324

Test Plan: AutoFlagsExternalMiniClusterTest.UpgradeCluster

Reviewers: bogdan

Reviewed By: bogdan

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D20158
Revert "[#14324] DocDB: Add master address to tservers when a new master is added in ExternalMiniCluster tests"

This reverts commit b18563a3689479dbdd084d0e7e73ea4b3088b8f0.
[#14324] DocDB: Add master address to tservers when a new master is added in ExternalMiniCluster tests

Summary:
AutoFlags tests adds master to an existing cluster and then restarts all mater and tserver processes.
We should add the new master address to tservers before restarting them.

Fixes #14324

Test Plan: AutoFlagsExternalMiniClusterTest.UpgradeCluster

Reviewers: bogdan

Reviewed By: bogdan

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D20182
