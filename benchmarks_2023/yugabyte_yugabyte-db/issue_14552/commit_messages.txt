Enable Clang 15 in x86_64 build types, except ASAN

Summary:
Enable Clang 15 in x86_64 build types.

Upgrading ASAN to Clang 14 or later still requires fixing or suppressing ODR violation errors.

Test Plan: Jenkins

Reviewers: bogdan, jharveysmith, steve.varnau

Reviewed By: jharveysmith, steve.varnau

Subscribers: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D20141
[#14552] DST: PITR - Do not crash if table not found from colocated list

Summary:
On deleting a colocated table once it goes out of retention window, we don't remove the table id
from the corresponding table_ids list of SysTabletsEntryPB of the colocated tablet on the master.
Thus during restore when sending schema of all the colocated tables of this tablet we also
attempt to find the schema for this DELETED table (it traverses over this table_ids list to find
out candidates) and generates a fatal.

Removed the fatal and instead logged a warning until the underlying issue is fixed.

Test Plan: Jenkins

Reviewers: zdrudi, msun, slingam

Reviewed By: slingam

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D20374
[BACKPORT 2.17.0][#14552] DST: PITR - Do not crash if table not found from colocated list

Summary:
Original commit: b15c253450d3675acda3993d06fea1bf7ad64e4a / D20374
On deleting a colocated table once it goes out of retention window, we don't remove the table id
from the corresponding table_ids list of SysTabletsEntryPB of the colocated tablet on the master.
Thus during restore when sending schema of all the colocated tables of this tablet we also
attempt to find the schema for this DELETED table (it traverses over this table_ids list to find
out candidates) and generates a fatal.

Removed the fatal and instead logged a warning until the underlying issue is fixed.

Test Plan: Jenkins

Reviewers: msun, slingam, zdrudi

Reviewed By: zdrudi

Subscribers: bogdan, ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D20677
[#14552] DST: PITR - Continue after checking table not found in colocated list

Summary:
Follow-up of D20374. Missed adding a `continue` after checking that table not found in colocated
list.

Test Plan: Jenkins

Reviewers: zdrudi

Reviewed By: zdrudi

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D20867
[BACKPORT 2.17.0][#14552] DST: PITR - Continue after checking table not found in colocated list

Summary:
Original commit: ce800cbf2f366cba18ebe3ca3084c3a12ce63286 / D20867
Follow-up of D20374. Missed adding a `continue` after checking that table not found in colocated
list.

Test Plan: Jenkins

Reviewers: zdrudi

Reviewed By: zdrudi

Subscribers: bogdan, ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D20902
[BACKPORT 2.16][#14552] DST: PITR - Continue after checking table not found in colocated list

Summary:
Original commit: ce800cbf2f366cba18ebe3ca3084c3a12ce63286 / D20867
Follow-up of D20374. Missed adding a `continue` after checking that table not found in colocated
list.

Test Plan: Jenkins

Reviewers: zdrudi

Reviewed By: zdrudi

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D20903
[#14552] DST: PITR - Continue after checking table not found in colocated list

Summary:
Follow-up of D20374. Missed adding a `continue` after checking that table not found in colocated
list.

Test Plan: Jenkins

Reviewers: zdrudi

Reviewed By: zdrudi

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D20867
