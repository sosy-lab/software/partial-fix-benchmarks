diff --git a/src/yb/common/schema.h b/src/yb/common/schema.h
index a903a892e9f4..844a8826311e 100644
--- a/src/yb/common/schema.h
+++ b/src/yb/common/schema.h
@@ -792,6 +792,10 @@ class Schema {
     colocation_id_ = colocation_id;
   }
 
+  bool is_colocated() const {
+    return has_colocation_id() || has_cotable_id();
+  }
+
   // Extract a given column from a row where the type is
   // known at compile-time. The type is checked with a debug
   // assertion -- but if the wrong type is used and these assertions
diff --git a/src/yb/docdb/docdb_compaction_context.cc b/src/yb/docdb/docdb_compaction_context.cc
index ef266e600067..56cc6cb1a8d8 100644
--- a/src/yb/docdb/docdb_compaction_context.cc
+++ b/src/yb/docdb/docdb_compaction_context.cc
@@ -22,6 +22,7 @@
 #include "yb/docdb/doc_ttl_util.h"
 #include "yb/docdb/key_bounds.h"
 #include "yb/docdb/packed_row.h"
+#include "yb/docdb/pgsql_operation.h"
 #include "yb/docdb/schema_packing.h"
 #include "yb/docdb/value.h"
 #include "yb/docdb/value_type.h"
@@ -42,7 +43,6 @@ using std::unordered_set;
 using rocksdb::VectorToString;
 
 DECLARE_bool(ycql_enable_packed_row);
-DECLARE_bool(ysql_enable_packed_row);
 
 DECLARE_uint64(ycql_packed_row_size_limit);
 DECLARE_uint64(ysql_packed_row_size_limit);
@@ -354,7 +354,7 @@ class PackedRowData {
     active_coprefix_ = coprefix;
     active_coprefix_dropped_ = false;
     new_packing_ = *packing;
-    can_start_packing_ = packing->enabled();
+    can_start_packing_ = packing->enabled;
     used_schema_versions_it_ = used_schema_versions_.find(new_packing_.cotable_id);
     return Status::OK();
   }
@@ -1082,12 +1082,12 @@ HybridTime MinHybridTime(const std::vector<rocksdb::FileMetaData*>& inputs) {
 
 } // namespace
 
-bool CompactionSchemaInfo::enabled() const {
+bool PackedRowEnabled(TableType table_type, bool is_colocated) {
   switch (table_type) {
     case TableType::YQL_TABLE_TYPE:
       return FLAGS_ycql_enable_packed_row;
     case TableType::PGSQL_TABLE_TYPE:
-      return FLAGS_ysql_enable_packed_row;
+      return ShouldYsqlPackRow(is_colocated);
     case TableType::REDIS_TABLE_TYPE: [[fallthrough]];
     case TableType::TRANSACTION_STATUS_TABLE_TYPE:
       return false;
diff --git a/src/yb/docdb/docdb_compaction_context.h b/src/yb/docdb/docdb_compaction_context.h
index 4eb2699403bc..98f60d6e81cf 100644
--- a/src/yb/docdb/docdb_compaction_context.h
+++ b/src/yb/docdb/docdb_compaction_context.h
@@ -46,6 +46,8 @@ YB_STRONGLY_TYPED_BOOL(ShouldRetainDeleteMarkersInMajorCompaction);
 struct Expiration;
 using ColumnIds = std::unordered_set<ColumnId, boost::hash<ColumnId>>;
 
+bool PackedRowEnabled(TableType table_type, bool is_colocated);
+
 // A "directive" of how a particular compaction should retain old (overwritten or deleted) values.
 struct HistoryRetentionDirective {
   // We will not keep history below this hybrid_time. The view of the database at this hybrid_time
@@ -66,8 +68,8 @@ struct CompactionSchemaInfo {
   std::shared_ptr<const SchemaPacking> schema_packing;
   Uuid cotable_id;
   ColumnIds deleted_cols;
+  bool enabled;
 
-  bool enabled() const;
   size_t pack_limit() const; // As usual, when not specified size is in bytes.
 
   // Whether we should keep original write time when combining columns updates into packed row.
diff --git a/src/yb/docdb/pgsql_operation.cc b/src/yb/docdb/pgsql_operation.cc
index 3432bfd0c37a..36e47d204175 100644
--- a/src/yb/docdb/pgsql_operation.cc
+++ b/src/yb/docdb/pgsql_operation.cc
@@ -92,6 +92,11 @@ DEFINE_test_flag(bool, ysql_suppress_ybctid_corruption_details, false,
 namespace yb {
 namespace docdb {
 
+bool ShouldYsqlPackRow(bool is_colocated) {
+  return FLAGS_ysql_enable_packed_row &&
+         (!is_colocated || FLAGS_ysql_enable_packed_row_for_colocated_table);
+}
+
 namespace {
 
 // Compatibility: accept column references from a legacy nodes as a list of column ids only
@@ -185,11 +190,6 @@ Result<DocKey> FetchDocKey(const Schema& schema, const PgsqlWriteRequestPB& requ
       });
 }
 
-bool DisablePackedRowIfColocatedTable(const Schema& schema) {
-  return !FLAGS_ysql_enable_packed_row_for_colocated_table &&
-         (schema.has_colocation_id() || schema.has_cotable_id());
-}
-
 Result<YQLRowwiseIteratorIf::UniPtr> CreateIterator(
     const YQLStorageIf& ql_storage,
     const PgsqlReadRequestPB& request,
@@ -578,8 +578,7 @@ Status PgsqlWriteOperation::ApplyInsert(const DocOperationApplyData& data, IsUps
     }
   }
 
-  if (FLAGS_ysql_enable_packed_row &&
-      !DisablePackedRowIfColocatedTable(doc_read_context_->schema)) {
+  if (ShouldYsqlPackRow(doc_read_context_->schema.is_colocated())) {
     RowPackContext pack_context(
         request_, data, VERIFY_RESULT(RowPackerData::Create(request_, *doc_read_context_)));
 
@@ -684,8 +683,7 @@ Status PgsqlWriteOperation::ApplyUpdate(const DocOperationApplyData& data) {
 
     skipped = request_.column_new_values().empty();
     const size_t num_non_key_columns = schema.num_columns() - schema.num_key_columns();
-    if (FLAGS_ysql_enable_packed_row &&
-        !DisablePackedRowIfColocatedTable(schema) &&
+    if (ShouldYsqlPackRow(schema.is_colocated()) &&
         make_unsigned(request_.column_new_values().size()) == num_non_key_columns) {
       RowPackContext pack_context(
           request_, data, VERIFY_RESULT(RowPackerData::Create(request_, *doc_read_context_)));
diff --git a/src/yb/docdb/pgsql_operation.h b/src/yb/docdb/pgsql_operation.h
index dc7db40b13ce..66488fb26740 100644
--- a/src/yb/docdb/pgsql_operation.h
+++ b/src/yb/docdb/pgsql_operation.h
@@ -29,6 +29,8 @@ namespace docdb {
 
 YB_STRONGLY_TYPED_BOOL(IsUpsert);
 
+bool ShouldYsqlPackRow(bool has_cotable_id);
+
 class PgsqlWriteOperation :
     public DocOperationBase<DocOperationType::PGSQL_WRITE_OPERATION, PgsqlWriteRequestPB>,
     public DocExprExecutor {
diff --git a/src/yb/integration-tests/packed_row_test_base.cc b/src/yb/integration-tests/packed_row_test_base.cc
index 0abf93410a89..268fd5e37f5f 100644
--- a/src/yb/integration-tests/packed_row_test_base.cc
+++ b/src/yb/integration-tests/packed_row_test_base.cc
@@ -22,6 +22,7 @@
 
 DECLARE_bool(ycql_enable_packed_row);
 DECLARE_bool(ysql_enable_packed_row);
+DECLARE_bool(ysql_enable_packed_row_for_colocated_table);
 
 DECLARE_int32(history_cutoff_propagation_interval_ms);
 DECLARE_int32(timestamp_history_retention_interval_sec);
@@ -33,6 +34,7 @@ namespace yb {
 
 void SetUpPackedRowTestFlags() {
   FLAGS_ysql_enable_packed_row = true;
+  FLAGS_ysql_enable_packed_row_for_colocated_table = true;
   FLAGS_ysql_packed_row_size_limit = 1_KB;
   FLAGS_ycql_enable_packed_row = true;
   FLAGS_ycql_packed_row_size_limit = 1_KB;
diff --git a/src/yb/tablet/tablet_metadata.cc b/src/yb/tablet/tablet_metadata.cc
index 9664e117cffc..379036b7619a 100644
--- a/src/yb/tablet/tablet_metadata.cc
+++ b/src/yb/tablet/tablet_metadata.cc
@@ -50,6 +50,7 @@
 
 #include "yb/docdb/doc_read_context.h"
 #include "yb/docdb/docdb_rocksdb_util.h"
+#include "yb/docdb/pgsql_operation.h"
 
 #include "yb/gutil/atomicops.h"
 #include "yb/gutil/dynamic_annotations.h"
@@ -274,6 +275,8 @@ Result<docdb::CompactionSchemaInfo> TableInfo::Packing(
     .schema_packing = rpc::SharedField(self, packing.get_ptr()),
     .cotable_id = self->cotable_id,
     .deleted_cols = std::move(deleted_before_history_cutoff),
+    .enabled =
+        docdb::PackedRowEnabled(self->table_type, self->doc_read_context->schema.is_colocated())
   };
 }
 
diff --git a/src/yb/yql/pgwrapper/pg_packed_row-test.cc b/src/yb/yql/pgwrapper/pg_packed_row-test.cc
index 72a54eea0ca9..1e40b47d014d 100644
--- a/src/yb/yql/pgwrapper/pg_packed_row-test.cc
+++ b/src/yb/yql/pgwrapper/pg_packed_row-test.cc
@@ -35,13 +35,15 @@ DECLARE_bool(ysql_enable_packed_row);
 DECLARE_int32(history_cutoff_propagation_interval_ms);
 DECLARE_int32(timestamp_history_retention_interval_sec);
 DECLARE_uint64(ysql_packed_row_size_limit);
+DECLARE_bool(ysql_enable_packed_row_for_colocated_table);
 
 namespace yb {
 namespace pgwrapper {
 
 class PgPackedRowTest : public PackedRowTestBase<PgMiniTestBase> {
  protected:
-  void TestCompaction(const std::string& expr_suffix);
+  void TestCompaction(int num_keys, const std::string& expr_suffix);
+  void TestColocated(int num_keys, int num_expected_records);
 };
 
 TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(Simple)) {
@@ -368,8 +370,7 @@ TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(SchemaGC)) {
   }
 }
 
-void PgPackedRowTest::TestCompaction(const std::string& expr_suffix) {
-  constexpr int kKeys = 10;
+void PgPackedRowTest::TestCompaction(int num_keys, const std::string& expr_suffix) {
   constexpr size_t kValueLen = 32;
 
   auto conn = ASSERT_RESULT(ConnectToDB("test"));
@@ -382,7 +383,7 @@ void PgPackedRowTest::TestCompaction(const std::string& expr_suffix) {
 
   std::string t1;
   std::string t2;
-  for (auto key : Range(kKeys)) {
+  for (auto key : Range(num_keys)) {
     if (key) {
       t1 += ";";
       t2 += ";";
@@ -414,19 +415,50 @@ void PgPackedRowTest::TestCompaction(const std::string& expr_suffix) {
   }
 }
 
+void PgPackedRowTest::TestColocated(int num_keys, int num_expected_records) {
+  auto conn = ASSERT_RESULT(Connect());
+  ASSERT_OK(conn.Execute("CREATE DATABASE test WITH colocated = true"));
+  TestCompaction(num_keys, "WITH (colocated = true)");
+  CheckNumRecords(cluster_.get(), num_expected_records);
+}
+
 TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(TableGroup)) {
   auto conn = ASSERT_RESULT(Connect());
   ASSERT_OK(conn.Execute("CREATE DATABASE test"));
   conn = ASSERT_RESULT(ConnectToDB("test"));
   ASSERT_OK(conn.Execute("CREATE TABLEGROUP tg"));
 
-  TestCompaction("TABLEGROUP tg");
+  TestCompaction(/* num_keys = */ 10, "TABLEGROUP tg");
 }
 
 TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(Colocated)) {
+  TestColocated(/* num_keys = */ 10, /* num_expected_records = */ 20);
+}
+
+TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(ColocatedCompactionPackRowDisabled)) {
+  ANNOTATE_UNPROTECTED_WRITE(FLAGS_ysql_enable_packed_row_for_colocated_table) = false;
+  TestColocated(/* num_keys = */ 10, /* num_expected_records = */ 40);
+}
+
+TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(ColocatedPackRowDisabled)) {
+  ANNOTATE_UNPROTECTED_WRITE(FLAGS_ysql_enable_packed_row_for_colocated_table) = false;
   auto conn = ASSERT_RESULT(Connect());
   ASSERT_OK(conn.Execute("CREATE DATABASE test WITH colocated = true"));
-  TestCompaction("WITH (colocated = true)");
+  conn = ASSERT_RESULT(ConnectToDB("test"));
+  ASSERT_OK(conn.Execute(
+      "CREATE TABLE t1 (key INT PRIMARY KEY, value TEXT, payload TEXT) WITH (colocated = true)"));
+  ASSERT_OK(conn.Execute("INSERT INTO t1 (key, value, payload) VALUES (1, '', '')"));
+  // The only row should not be packed.
+  CheckNumRecords(cluster_.get(), 3);
+  // Trigger full row update.
+  ASSERT_OK(conn.Execute("UPDATE t1 SET value = '1', payload = '1' WHERE key = 1"));
+  // The updated row should not be packed.
+  CheckNumRecords(cluster_.get(), 5);
+
+  // Enable pack row for colocated table and trigger compaction.
+  ANNOTATE_UNPROTECTED_WRITE(FLAGS_ysql_enable_packed_row_for_colocated_table) = true;
+  ASSERT_OK(cluster_->CompactTablets());
+  CheckNumRecords(cluster_.get(), 1);
 }
 
 TEST_F(PgPackedRowTest, YB_DISABLE_TEST_IN_TSAN(CompactAfterTransaction)) {
