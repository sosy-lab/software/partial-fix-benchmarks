diff --git a/src/yb/docdb/intent_aware_iterator.cc b/src/yb/docdb/intent_aware_iterator.cc
index 1bf4df14566c..ba94ac493ddc 100644
--- a/src/yb/docdb/intent_aware_iterator.cc
+++ b/src/yb/docdb/intent_aware_iterator.cc
@@ -778,6 +778,8 @@ void IntentAwareIterator::SeekToSuitableIntent() {
           intent_upperbound_keybytes_.Clear();
           intent_upperbound_keybytes_.AppendKeyEntryType(KeyEntryType::kTransactionId);
           intent_upperbound_ = intent_upperbound_keybytes_.AsSlice();
+          // We are not calling RevalidateAfterUpperBoundChange here because it is only needed
+          // during forward iteration, and is not needed immediately before a seek.
           intent_iter_.SeekToLast();
           break;
       }
@@ -948,6 +950,13 @@ Result<HybridTime> IntentAwareIterator::FindOldestRecord(
   return result;
 }
 
+void IntentAwareIterator::SetUpperbound(const Slice& upperbound) {
+  if (!upperbound_.empty() && intent_upperbound_.data() == upperbound_.data()) {
+    DoSetIntentUpperBound(upperbound);
+  }
+  upperbound_ = upperbound;
+}
+
 Status IntentAwareIterator::FindLatestRecord(
     const Slice& key_without_ht,
     DocHybridTime* latest_record_ht,
@@ -1182,8 +1191,7 @@ Status IntentAwareIterator::SetIntentUpperbound() {
             << SubDocKey::DebugSliceToString(intent_upperbound_keybytes_.AsSlice()) << "/"
             << intent_upperbound_keybytes_.AsSlice().ToDebugHexString();
     intent_upperbound_keybytes_.AppendKeyEntryType(KeyEntryType::kMaxByte);
-    intent_upperbound_ = intent_upperbound_keybytes_.AsSlice();
-    intent_iter_.RevalidateAfterUpperBoundChange();
+    DoSetIntentUpperBound(intent_upperbound_keybytes_.AsSlice());
     HandleStatus(intent_iter_.status());
     RETURN_NOT_OK(status_);
   } else {
@@ -1197,13 +1205,26 @@ Status IntentAwareIterator::SetIntentUpperbound() {
 }
 
 void IntentAwareIterator::ResetIntentUpperbound() {
-  intent_upperbound_keybytes_.Clear();
-  intent_upperbound_keybytes_.AppendKeyEntryType(KeyEntryType::kHighest);
-  intent_upperbound_ = intent_upperbound_keybytes_.AsSlice();
-  intent_iter_.RevalidateAfterUpperBoundChange();
+  Slice new_intent_upper_bound;
+  if (upperbound_.empty()) {
+    // If the regular iterator does not have an upper bound, we do not restrict the intent iterator
+    // at all.
+    intent_upperbound_keybytes_.Clear();
+    intent_upperbound_keybytes_.AppendKeyEntryType(KeyEntryType::kHighest);
+    new_intent_upper_bound = intent_upperbound_keybytes_.AsSlice();
+  } else {
+    // Reuse the upper bound we already have for the regular RocksDB.
+    new_intent_upper_bound = upperbound_;
+  }
+  DoSetIntentUpperBound(new_intent_upper_bound);
   VLOG(4) << "ResetIntentUpperbound = " << intent_upperbound_.ToDebugString();
 }
 
+void IntentAwareIterator::DoSetIntentUpperBound(const Slice& intent_upper_bound) {
+  intent_upperbound_ = intent_upper_bound;
+  intent_iter_.RevalidateAfterUpperBoundChange();
+}
+
 std::string IntentAwareIterator::DebugPosToString() {
   if (IsOutOfRecords()) {
     return "<OUT_OF_RECORDS>";
diff --git a/src/yb/docdb/intent_aware_iterator.h b/src/yb/docdb/intent_aware_iterator.h
index 79c8ede82d1e..b46267fce858 100644
--- a/src/yb/docdb/intent_aware_iterator.h
+++ b/src/yb/docdb/intent_aware_iterator.h
@@ -148,9 +148,8 @@ class IntentAwareIterator {
   Result<HybridTime> FindOldestRecord(const Slice& key_without_ht,
                                       HybridTime min_hybrid_time);
 
-  void SetUpperbound(const Slice& upperbound) {
-    upperbound_ = upperbound;
-  }
+  // Set the upper bound for the iterator.
+  void SetUpperbound(const Slice& upperbound);
 
   void DebugDump();
 
@@ -246,10 +245,15 @@ class IntentAwareIterator {
   // beyond the current regular key unnecessarily.
   Status SetIntentUpperbound();
 
-  // Resets the exclusive upperbound of the intent iterator to the beginning of the transaction
-  // metadata and reverse index region.
+  // Resets the exclusive upperbound of the intent iterator to its default value.
+  // - If we are using an upper bound for the regular RocksDB iterator, we will reuse the same upper
+  //   bound for the intent iterator.
+  // - If there is no upper bound for regular RocksDB (e.g. because we are scanning the entire
+  //   table), we will fall back to scanning the entire intents RocksDB.
   void ResetIntentUpperbound();
 
+  void DoSetIntentUpperBound(const Slice& intent_upper_bound);
+
   void SeekIntentIterIfNeeded();
 
   // Does initial steps for prev doc key/sub doc key seek.
@@ -302,8 +306,9 @@ class IntentAwareIterator {
   // Upperbound for seek. If we see regular or intent record past this bound, it will be ignored.
   Slice upperbound_;
 
-  // Exclusive upperbound of the intent key.
+  // Buffer for holding the exclusive upper bound of the intent key.
   KeyBytes intent_upperbound_keybytes_;
+
   Slice intent_upperbound_;
 
   // Following fields contain information related to resolved suitable intent.
diff --git a/src/yb/rocksdb/db/db_iter.cc b/src/yb/rocksdb/db/db_iter.cc
index 1dfbee196e32..3646ccf72587 100644
--- a/src/yb/rocksdb/db/db_iter.cc
+++ b/src/yb/rocksdb/db/db_iter.cc
@@ -816,7 +816,7 @@ void DBIter::FindParseableKey(ParsedInternalKey* ikey, Direction direction) {
 
 void DBIter::Seek(const Slice& target) {
   saved_key_.Clear();
-  // now savved_key is used to store internal key.
+  // now saved_key_ is used to store internal key.
   saved_key_.SetInternalKey(target, sequence_);
 
   {
