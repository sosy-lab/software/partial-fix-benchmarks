[#15952] YSQL: Reduce verbosity of a Postgres log line in read committed isolation level codepath

Test Plan: Jenkins

Reviewers: yguan

Reviewed By: yguan

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D22755
[#15849] DocDB: Don't hold mutex when starting TransactionStatusResolver

Summary:
CheckForAbortedTransactions starts TransactionStatusResolver which synchronously creates a YBClient. This runs while we hold onto the mutex. YBClient creation requires a master leader to be ready and process rpcs. This leads to the following deadlocks in master:
- CatalogManager loader writes to the sys_catalog which requires the mutex. Until the loader completes we do not open up the master to process rpcs.
- ReplicationFinished on the sys_catalog table can call UpdateTxnOperation::DoReplicated which requires the mutex. A new master cannot be elected until the ReplicationFinished call completes.

Fixing this by releasing the mutex before starting TransactionStatusResolver. This is safe to perform as the rpc is anyway async and its response is handed in TransactionsStatus (on another thread)

Fixes #15849

Test Plan: PgDdlAtomicitySanityTest.FailureRecoveryTestWithAbortedTran

Reviewers: sergei, rsami, asrivastava, esheng

Reviewed By: asrivastava, esheng

Subscribers: bogdan, slingam, ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D22658
[BACKPORT 2.14][#15849] DocDB: Don't hold mutex when starting TransactionStatusResolver

Summary:
CheckForAbortedTransactions starts TransactionStatusResolver which synchronously creates a YBClient. This runs while we hold onto the mutex. YBClient creation requires a master leader to be ready and process rpcs. This leads to the following deadlocks in master:

CatalogManager loader writes to the sys_catalog which requires the mutex. Until the loader completes we do not open up the master to process rpcs.
ReplicationFinished on the sys_catalog table can call UpdateTxnOperation::DoReplicated which requires the mutex. A new master cannot be elected until the ReplicationFinished call completes.
Fixing this by releasing the mutex before starting TransactionStatusResolver. This is safe to perform as the rpc is anyway async and its response is handed in TransactionsStatus (on another thread)

Fixes #15849

Original commit: 14744f4dac08a75d665bdf2bbf737803a44c9e15 / D22658

Some additional test only util functions have also been hand picked over.

Test Plan: PgDdlAtomicitySanityTest.FailureRecoveryTestWithAbortedTran

Reviewers: esheng, bogdan, asrivastava

Reviewed By: bogdan, asrivastava

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D22781
[BACKPORT 2.16][#15849] DocDB: Don't hold mutex when starting TransactionStatusResolver

Summary:
CheckForAbortedTransactions starts TransactionStatusResolver which synchronously creates a YBClient. This runs while we hold onto the mutex. YBClient creation requires a master leader to be ready and process rpcs. This leads to the following deadlocks in master:

CatalogManager loader writes to the sys_catalog which requires the mutex. Until the loader completes we do not open up the master to process rpcs.
ReplicationFinished on the sys_catalog table can call UpdateTxnOperation::DoReplicated which requires the mutex. A new master cannot be elected until the ReplicationFinished call completes.
Fixing this by releasing the mutex before starting TransactionStatusResolver. This is safe to perform as the rpc is anyway async and its response is handed in TransactionsStatus (on another thread)

Fixes #15849

Original commit: 14744f4dac08a75d665bdf2bbf737803a44c9e15 / D22658

Some additional test only util functions have also been hand picked over.

Test Plan: PgDdlAtomicitySanityTest.FailureRecoveryTestWithAbortedTran

Reviewers: esheng, asrivastava, bogdan

Reviewed By: asrivastava, bogdan

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D22783
[BACKPORT 2.12][#15849] DocDB: Don't hold mutex when starting TransactionStatusResolver

Summary:
CheckForAbortedTransactions starts TransactionStatusResolver which synchronously creates a YBClient. This runs while we hold onto the mutex. YBClient creation requires a master leader to be ready and process rpcs. This leads to the following deadlocks in master:

CatalogManager loader writes to the sys_catalog which requires the mutex. Until the loader completes we do not open up the master to process rpcs.
ReplicationFinished on the sys_catalog table can call UpdateTxnOperation::DoReplicated which requires the mutex. A new master cannot be elected until the ReplicationFinished call completes.
Fixing this by releasing the mutex before starting TransactionStatusResolver. This is safe to perform as the rpc is anyway async and its response is handed in TransactionsStatus (on another thread)

Fixes #15849

Original commit: 14744f4dac08a75d665bdf2bbf737803a44c9e15 / D22658

Some additional test only util functions have also been hand picked over.

Test Plan: PgDdlAtomicitySanityTest.FailureRecoveryTestWithAbortedTran

Reviewers: esheng, asrivastava, bogdan

Reviewed By: asrivastava, bogdan

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D22782
[#15909] docdb: Move sys catalog writes out of catalog loader.

Summary:
This diff moves migration writes (to the sys catalog) out of the catalog loading phase. This should make the master leader startup faster by avoiding network and disk IO, and avoid deadlocks like in #15849. Specifically, this diff moves the following two writes to the catalog manager background thread pool:
 1. The migration that inserts table_id into table_ids for old empty colocated tables.
 2. The migration that deletes an orphaned tablet (part of a deleting table that crashed).

Other things in this diff:
 - Move some tests that were not part of sys catalog out of sys_catalog-test
 - Add a ForceUpsert to sys catalog. This is required because Upsert only does a write if the PB is dirty. Since the in-memory PB update is done separately, the PB is not dirty when the async sys catalog write happens. We want to force a write here to persist the migration changes. (I tried doing this with a default flag on Upsert / Mutate, but variadic arguments + defaults having to be last makes this hard, without modifying everywhere we currently call upsert).

Test Plan:
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestOrphanedTabletsDeleted"`
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestTableIdsHasAtLeastOneTable"`

Reviewers: zdrudi

Reviewed By: zdrudi

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D23291
[#15909] docdb: Move sys catalog writes out of catalog loader.

Summary:
This diff moves migration writes (to the sys catalog) out of the catalog loading phase. This should make the master leader startup faster by avoiding network and disk IO, and avoid deadlocks like in #15849. Specifically, this diff moves the following two writes to the catalog manager background thread pool:
 1. The migration that inserts table_id into table_ids for old empty colocated tables.
 2. The migration that deletes an orphaned tablet (part of a deleting table that crashed).

Other things in this diff:
 - Move some tests that were not part of sys catalog out of sys_catalog-test
 - Add a ForceUpsert to sys catalog. This is required because Upsert only does a write if the PB is dirty. Since the in-memory PB update is done separately, the PB is not dirty when the async sys catalog write happens. We want to force a write here to persist the migration changes. (I tried doing this with a default flag on Upsert / Mutate, but variadic arguments + defaults having to be last makes this hard, without modifying everywhere we currently call upsert).

Test Plan:
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestOrphanedTabletsDeleted"`
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestTableIdsHasAtLeastOneTable"`

Reviewers: zdrudi

Reviewed By: zdrudi

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D23291
[BACKPORT 2.14][#15909] docdb: Move sys catalog writes out of catalog loader.

Summary:
Original commit: 31a9e4874520512335801a5c47a4f60e01d9299d / D23291
This diff moves migration writes (to the sys catalog) out of the catalog loading phase. This should make the master leader startup faster by avoiding network and disk IO, and avoid deadlocks like in #15849. Specifically, this diff moves the following two writes to the catalog manager background thread pool:
 1. The migration that inserts table_id into table_ids for old empty colocated tables.
 2. The migration that deletes an orphaned tablet (part of a deleting table that crashed).

Other things in this diff:
 - Move some tests that were not part of sys catalog out of sys_catalog-test
 - Add a ForceUpsert to sys catalog. This is required because Upsert only does a write if the PB is dirty. Since the in-memory PB update is done separately, the PB is not dirty when the async sys catalog write happens. We want to force a write here to persist the migration changes. (I tried doing this with a default flag on Upsert / Mutate, but variadic arguments + defaults having to be last makes this hard, without modifying everywhere we currently call upsert).
Jira: DB-5315

Test Plan:
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestOrphanedTabletsDeleted"`
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestTableIdsHasAtLeastOneTable"`

Reviewers: zdrudi, jhe

Reviewed By: jhe

Subscribers: ybase, bogdan

Tags: #jenkins-ready

Differential Revision: https://phorge.dev.yugabyte.com/D29208
[BACKPORT 2.16][#15909] docdb: Move sys catalog writes out of catalog loader.

Summary:
Original commit: 31a9e4874520512335801a5c47a4f60e01d9299d / D23291
This diff moves migration writes (to the sys catalog) out of the catalog loading phase. This should make the master leader startup faster by avoiding network and disk IO, and avoid deadlocks like in #15849. Specifically, this diff moves the following two writes to the catalog manager background thread pool:
 1. The migration that inserts table_id into table_ids for old empty colocated tables.
 2. The migration that deletes an orphaned tablet (part of a deleting table that crashed).

Other things in this diff:
 - Move some tests that were not part of sys catalog out of sys_catalog-test
 - Add a ForceUpsert to sys catalog. This is required because Upsert only does a write if the PB is dirty. Since the in-memory PB update is done separately, the PB is not dirty when the async sys catalog write happens. We want to force a write here to persist the migration changes. (I tried doing this with a default flag on Upsert / Mutate, but variadic arguments + defaults having to be last makes this hard, without modifying everywhere we currently call upsert).
Jira: DB-5315

Test Plan:
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestOrphanedTabletsDeleted"`
`ybd --cxx-test sys_catalog-test --gtest_filter="SysCatalogTest.TestTableIdsHasAtLeastOneTable"`

Reviewers: zdrudi, jhe

Reviewed By: jhe

Subscribers: bogdan, ybase

Tags: #jenkins-ready

Differential Revision: https://phorge.dev.yugabyte.com/D29207
