diff --git a/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgExplainAnalyze.java b/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgExplainAnalyze.java
index 3c1d2a4a16d8..74726c95d41c 100644
--- a/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgExplainAnalyze.java
+++ b/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgExplainAnalyze.java
@@ -467,7 +467,7 @@ public void testPgExplainAnalyze() throws Exception {
                                kTableReadRpcWaitTime, kGreaterThanZero))),
                    ImmutableMap.of(
                        kTotalReadRpcCount, kGreaterThanZero,
-                       kTotalWriteRpcCount, 308.0,
+                       kTotalWriteRpcCount, 1.0,
                        kTotalRpcWaitTime, kGreaterThanZero));
 
       // DELETE using index
@@ -541,7 +541,7 @@ public void testPgExplainAnalyze() throws Exception {
                                kTableReadRpcWaitTime, kShouldNotExist))),
                    ImmutableMap.of(
                        kTotalReadRpcCount, 2.0,
-                       kTotalWriteRpcCount, 206.0,
+                       kTotalWriteRpcCount, 1.0,
                        kTotalRpcWaitTime, kGreaterThanZero));
       stmt.execute("ROLLBACK");
 
@@ -575,7 +575,7 @@ public void testPgExplainAnalyze() throws Exception {
                                kTableReadRpcWaitTime, kShouldNotExist))),
                    ImmutableMap.of(
                        kTotalReadRpcCount, kGreaterThanZero,
-                       kTotalWriteRpcCount, 2.0,
+                       kTotalWriteRpcCount, 1.0,
                        kTotalRpcWaitTime, kGreaterThanZero));
 
       testExplainOneQuery(stmt, String.format(
diff --git a/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgIndexSelectiveUpdate.java b/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgIndexSelectiveUpdate.java
index b9e4f6ae2dad..98c558de578e 100644
--- a/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgIndexSelectiveUpdate.java
+++ b/java/yb-pgsql/src/test/java/org/yb/pgsql/TestPgIndexSelectiveUpdate.java
@@ -150,6 +150,60 @@ public void testUpdateTableIndexWrites() throws Exception {
     }
   }
 
+  /**
+   * Test index updates with pushdown disabled and non-constant SET clause expressions.
+   * Not pushable expressions should not prevent index analysis.
+   *
+   * @throws Exception
+   */
+  @Test
+  public void testUpdateTableIndexWritesNoPushdown() throws Exception {
+    try (Statement stmt = connection.createStatement()) {
+      prepareTest(stmt);
+
+      // Disable expression pushdown
+      stmt.execute("SET yb_enable_expression_pushdown to false");
+
+      // Add the value of metrics before updating the table test
+      updateCounter();
+
+      // column 4 is changed. this changes idx_col3, idx_col4_idx_col5_idx_col6.
+      stmt.execute(String.format("update %s set col4=col4+1 where pk=1", TABLE_NAME));
+      updateCounter();
+      checkWrites(3, 0, 0, 3, 0);
+
+      // column 6 is changed. this changes idx_col3, idx_col5, idx_col6, idx_col4_idx_col5_idx_col6.
+      stmt.execute(String.format("update %s set col6=col6+1 where pk=1", TABLE_NAME));
+      updateCounter();
+      checkWrites(3, 3, 3, 3, 0);
+
+      // column 5 is changed. this changes idx_col3, idx_col5, idx_col4_idx_col5_idx_col6.
+      stmt.execute(String.format("update %s set col5=col5+1 where pk=1", TABLE_NAME));
+      updateCounter();
+      checkWrites(3, 3, 0, 3, 0);
+
+      // column 9 is changed. this changes idx_col6.
+      stmt.execute(String.format("update %s set col9=col9+1 where pk=1", TABLE_NAME));
+      updateCounter();
+      checkWrites(0, 0, 3, 0, 0);
+
+      // column 2 is changed. this does not affect any index.
+      stmt.execute(String.format("update %s set col2=col2+1 where pk=1", TABLE_NAME));
+      updateCounter();
+      checkWrites(0, 0, 0, 0, 0);
+
+      // column 9 is changed for multiple rows.
+      stmt.execute(String.format("update %s set col9=col9+1 where pk>1", TABLE_NAME));
+      updateCounter();
+      checkWrites(0, 0, 7, 0, 0);
+
+      // column 8 is changed. No include columns hence just the table and index are updated.
+      stmt.execute(String.format("update %s set col8=col8+1 where pk=1", TABLE_NAME));
+      updateCounter();
+      checkWrites(0, 0, 0, 0, 2);
+    }
+  }
+
   @Test
   public void testUpdateTableAvoidTransactionSingleRowModify() throws Exception {
     try (Statement stmt = connection.createStatement()) {
diff --git a/src/postgres/src/backend/optimizer/plan/createplan.c b/src/postgres/src/backend/optimizer/plan/createplan.c
index ba3b018d17f1..7dd95597c013 100644
--- a/src/postgres/src/backend/optimizer/plan/createplan.c
+++ b/src/postgres/src/backend/optimizer/plan/createplan.c
@@ -2608,6 +2608,8 @@ yb_single_row_update_or_delete_path(PlannerInfo *root,
 	AttrNumber attr_offset;
 	Bitmapset *update_attrs = NULL;
 	Bitmapset *pushdown_update_attrs = NULL;
+	/* Delay bailout because of not pushable expressions to analyze indexes. */
+	bool has_unpushable_exprs = false;
 
 	/* Verify YB is enabled. */
 	if (!IsYugaByteEnabled())
@@ -2801,16 +2803,18 @@ yb_single_row_update_or_delete_path(PlannerInfo *root,
 			 * elements not supported by DocDB, or expression pushdown is
 			 * disabled.
 			 *
-			 * However, if not pushable expression does not refer any target
-			 * table column, the Result node can evaluate it, and the statement
-			 * would still be one row.
+			 * If expression is not pushable, we can not do single line update,
+			 * but do not bail out until after we analyse indexes and make a
+			 * list of secondary indexes unaffected by the update. We can skip
+			 * update of those indexes regardless. Still allow to bail out
+			 * if there are triggers. There is no easy way to tell what columns
+			 * are affected by a trigger, so we should update all indexes.
 			 */
 			if (TupleDescAttr(tupDesc, resno - 1)->attnotnull ||
 				YBIsCollationValidNonC(ybc_get_attcollation(tupDesc, resno)) ||
 				!YbCanPushdownExpr(tle->expr, &colrefs))
 			{
-				RelationClose(relation);
-				return false;
+				has_unpushable_exprs = true;
 			}
 
 			pushdown_update_attrs = bms_add_member(pushdown_update_attrs,
@@ -2840,6 +2844,17 @@ yb_single_row_update_or_delete_path(PlannerInfo *root,
 		return false;
 	}
 
+	/*
+	 * Now it is OK to bail out because of unpushable expressions.
+	 * We have made a list, and can skip unaffected indexes even though
+	 * the update is not single row.
+	 */
+	if (has_unpushable_exprs)
+	{
+		RelationClose(relation);
+		return false;
+	}
+
 	/*
 	 * Cannot allow check constraints for single-row update as we will need
 	 * to ensure we read all columns they reference to check them correctly.
