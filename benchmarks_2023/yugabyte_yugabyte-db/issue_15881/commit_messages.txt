[#16018] xCluster: Add null check for Consensus in cdc_producer

Summary:
During tablet transitions like load balancing and tablet splits, the consensus pointer can be null. This is now handled as tablet not found error.

Fixes #16018

Test Plan: TwoDCTestParams__TwoDCTest_TestAlterUniverseRemoveTableAndDrop__0

Reviewers: bogdan, jhe, slingam

Reviewed By: jhe, slingam

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D22807
[#15881] YSQL: unpushable SET clause expressions caused unnecessary index updates

Summary:
There is a loop over the SET clauses that is used to both make a list of columns
affected by the update, and check if the expressions are either constant or pushable.
That loop used to bail out in the case if not pushable expression was encountered,
before indexes were analysed and list of indexes unaffected by the update was made.

This change postpones the bail out on the grounds of unpushable expression until
after the index analysis.

Test Plan: ybd --java-test org.yb.pgsql.TestPgIndexSelectiveUpdate#testUpdateTableIndexWritesNoPushdown

Reviewers: rskannan, smishra, mihnea, tnayak

Reviewed By: tnayak

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D22657
[BACKPORT 2.16][#15881] YSQL: unpushable SET clause expressions caused unnecessary index updates

Summary:
There is a loop over the SET clauses that is used to both make a list of columns
affected by the update, and check if the expressions are either constant or pushable.
That loop used to bail out in the case if not pushable expression was encountered,
before indexes were analysed and list of indexes unaffected by the update was made.

This change postpones the bail out on the grounds of unpushable expression until
after the index analysis.

Original commit: 03055c430fa5e5424f8c241c8ba2e9a9c029881d / D22657

Test Plan: ybd --java-test org.yb.pgsql.TestPgIndexSelectiveUpdate#testUpdateTableIndexWritesNoPushdown

Reviewers: smishra, tnayak, mihnea, rskannan

Reviewed By: mihnea, rskannan

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D22933
[BACKPORT 2.14][#15881] YSQL: unpushable SET clause expressions caused unnecessary index updates

Summary:
There is a loop over the SET clauses that is used to both make a list of columns
affected by the update, and check if the expressions are either constant or pushable.
That loop used to bail out in the case if not pushable expression was encountered,
before indexes were analysed and list of indexes unaffected by the update was made.

This change postpones the bail out on the grounds of unpushable expression until
after the index analysis.

Original commit: 03055c430fa5e5424f8c241c8ba2e9a9c029881d / D22657

Test Plan: ybd --java-test org.yb.pgsql.TestPgIndexSelectiveUpdate#testUpdateTableIndexWritesNoPushdown

Reviewers: smishra, tnayak, rskannan, mihnea

Reviewed By: mihnea

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D22965
[WIP][#16034] YSQL: Avoid picking hybrid timestamps on YSQL

As part of commit c5f51258, a major refactor was done in 2.14 to move the
YSQL-to-tserver clients from YSQL backend processes into the same node's
tserver process. This was done because before the commit, each YSQL backend
would have a connection to each tserver (for issuing reads/ writes) resulting in
a lot of connections in the cluster. After the commit, all YSQL backends would
only talk to a proxy running on the local node's tserver process which would
forward the request to the required tserver. This eliminated the need for a
connection from each Postgres backend to each tserver on the cluster. A new
PgClientService was introducted on the tserver process to serve as the proxy.

This refactor resulted in a subtle regression where in the hybrid time wasn't
propagated from the transaction participants (i.e., tservers where reads/ writes
were performed) to the YSQL backends. The hybrid time was propagated from these
tservers to the PgClientService proxies, but not to the YSQL backends.

This results in certain scenarios where clock skew can cause data visibility
issues. One such visibility bug occurs as follows:

(1) Assume a main table `tbl` (k primary key, v1 int, v2 int) and a secondary
index `idx` on v2.
(2) Due to #15881, if an update was performed to a main table `tbl` such that
it doesn't change v2, it would still update the index by performing a DELETE
on the index key and then re-INSERTing the index key.
(3) Assume the update to the main table is being handled by query layer on
node N1.
(4) Assume the physical clock on N1 is lagging behind the physical clock on
the tserver where the index DELETE is performed (say N2).
(5) Since the `propagated_hybrid_time` is no longer sent from N2 to the YSQL
query layer on N1, the insert rpc picks an `in_txn_limit` using the local
clock which gives a value lower than the DELETE rpc's write hybrid time.

See ReadHybridTimePB and src/yb/yql/pggate/README.txt for information on
in_txn_limit.

The above visibilty bug gets masked once #15881 is fixed. However, not
propagating the hybrid time from tservers serving reads/ writes to the YSQL
query layer might result in other unknown issues. Propagating hybrid times
helps ensure that we maintain causalilty information i.e., if
event A on some node (occuring at hybrid time ht_a) leads to event B on
another node, event B's code path, for any purpose, will always pick a
hybrid time >= ht_a. In the above example, event A is the write and event B
is the INSERT code path on the query layer which picks a hybrid time for
`in_txn_limit` which is < ht_a.

This issue can be solved in 2 ways -
(1) Propagate hybrid times between YSQL and the local node's tserver proxy.
(2) Move all logic that picks hybrid times to the local node's tserver proxy.
This ensures that any hybrid time picked for fields like in_txn_limit have the
causal knowledge of past events backed in.

We are moving forward with approach (2) because of 2 reasons:

1) Propagating hybrid times from multiple YSQL backends to the local node can
result in contention on the hybrid clock's cache line due to repeated
CAS operation (see HybridClock::Update() which helps update the hybrid clock
with the propagated time).

2) The bottleneck in (1) can be solved by only propagating the hybrid time
from local TServer to YSQL. However, that is valid only if we can guarantee
that there are no causal events whose information needs to be propagated to
the TServer. It seems to be valid in the current state of code, but in future
there could be some operation that YSQL does on some YB-Master whose hybrid
time needs to be propagated to the local tserver.
[WIP][#16034] YSQL: Avoid picking hybrid timestamps on YSQL

Summary:
As part of commit c5f51258, a major refactor was done in 2.14 to move the
YSQL-to-tserver clients from YSQL backend processes into the same node's
tserver process. This was done because before the commit, each YSQL backend
would have a connection to each tserver (for issuing reads/ writes) resulting in
a lot of connections in the cluster. After the commit, all YSQL backends would
only talk to a proxy running on the local node's tserver process which would
forward the request to the required tserver. This eliminated the need for a
connection from each Postgres backend to each tserver on the cluster. A new
PgClientService was introducted on the tserver process to serve as the proxy.

This refactor resulted in a subtle regression where in the hybrid time wasn't
propagated from the transaction participants (i.e., tservers where reads/ writes
were performed) to the YSQL backends. The hybrid time was propagated from these
tservers to the PgClientService proxies, but not to the YSQL backends.

This results in certain scenarios where clock skew can cause data visibility
issues. One such visibility bug occurs as follows:

(1) Assume a main table `tbl` (k primary key, v1 int, v2 int) and a secondary
index `idx` on v2.
(2) Due to #15881, if an update was performed to a main table `tbl` such that
it doesn't change v2, it would still update the index by performing a DELETE
on the index key and then re-INSERTing the index key.
(3) Assume the update to the main table is being handled by query layer on
node N1.
(4) Assume the physical clock on N1 is lagging behind the physical clock on
the tserver where the index DELETE is performed (say N2).
(5) Since the `propagated_hybrid_time` is no longer sent from N2 to the YSQL
query layer on N1, the insert rpc picks an `in_txn_limit` using the local
clock which gives a value lower than the DELETE rpc's write hybrid time.

See ReadHybridTimePB and src/yb/yql/pggate/README.txt for information on
in_txn_limit.

The above visibilty bug gets masked once #15881 is fixed. However, not
propagating the hybrid time from tservers serving reads/ writes to the YSQL
query layer might result in other unknown issues. Propagating hybrid times
helps ensure that we maintain causalilty information i.e., if
event A on some node (occuring at hybrid time ht_a) leads to event B on
another node, event B's code path, for any purpose, will always pick a
hybrid time >= ht_a. In the above example, event A is the write and event B
is the INSERT code path on the query layer which picks a hybrid time for
`in_txn_limit` which is < ht_a.

This issue can be solved in 2 ways -
(1) Propagate hybrid times between YSQL and the local node's tserver proxy.
(2) Move all logic that picks hybrid times to the local node's tserver proxy.
This ensures that any hybrid time picked for fields like in_txn_limit have the
causal knowledge of past events backed in.

We are moving forward with approach (2) because of 2 reasons:

1) Propagating hybrid times from multiple YSQL backends to the local node can
result in contention on the hybrid clock's cache line due to repeated
CAS operation (see HybridClock::Update() which helps update the hybrid clock
with the propagated time).

2) The bottleneck in (1) can be solved by only propagating the hybrid time
from local TServer to YSQL. However, that is valid only if we can guarantee
that there are no causal events whose information needs to be propagated to
the TServer. It seems to be valid in the current state of code, but in future
there could be some operation that YSQL does on some YB-Master whose hybrid
time needs to be propagated to the local tserver.

Test Plan: ./yb_build.sh --cxx-test pgwrapper_pg_clock_skew-test --gtest_filter PgClockSkewTest.InTxnLimitBug

Reviewers: dmitry, sergei

Subscribers: yql, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D23075
