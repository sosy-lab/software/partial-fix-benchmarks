[YSQL] Update to non index columns also updates the Index columns
Index analysis (if we should update secondary indexes) is a part of the "single row update" check.
If expression pushdown is not enabled, the "single row update" check bails out during SET clause analysis, where it collects the list of columns affected.
The bailout may be delayed until after index analysis.
That explains differences between master and stable branches, the expression pushdown is on in master by default.
@sushantrmishra , @andrei-mart -- is title of github issue still correct, or needs to be made more specific to presence of NOT NULL constraints (or other factors)?
Reopening to evaluate the backport needs to 2.14 and 2.16 branches. cc @sushantrmishra , @andrei-mart , @m-iancu 
Backports have been completed