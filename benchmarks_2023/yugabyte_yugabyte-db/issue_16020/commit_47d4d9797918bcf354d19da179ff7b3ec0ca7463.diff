diff --git a/src/postgres/src/backend/utils/misc/pg_yb_utils.c b/src/postgres/src/backend/utils/misc/pg_yb_utils.c
index a1289815faef..824c38aeb8da 100644
--- a/src/postgres/src/backend/utils/misc/pg_yb_utils.c
+++ b/src/postgres/src/backend/utils/misc/pg_yb_utils.c
@@ -2206,36 +2206,43 @@ getSplitPointsInfo(Oid relid, YBCPgTableDesc yb_tabledesc,
 {
 	Assert(yb_table_properties->num_tablets > 1);
 
-	/* Get key columns' YBCPgTypeEntity and YBCPgTypeAttrs */
 	size_t num_range_key_columns = yb_table_properties->num_range_key_columns;
 	const YBCPgTypeEntity *type_entities[num_range_key_columns];
 	YBCPgTypeAttrs type_attrs_arr[num_range_key_columns];
+	/*
+	 * Get key columns' YBCPgTypeEntity and YBCPgTypeAttrs.
+	 * For range-partitioned tables, use primary key to get key columns' type
+	 * info. For range-partitioned indexes, get key columns' type info from
+	 * indexes themselves.
+	 */
 	Relation rel = relation_open(relid, AccessShareLock);
+	bool is_table = rel->rd_rel->relkind == RELKIND_RELATION;
+	Relation index_rel = is_table
+							? relation_open(RelationGetPrimaryKeyIndex(rel),
+											AccessShareLock)
+							: rel;
+	Form_pg_index rd_index = index_rel->rd_index;
 	TupleDesc tupledesc = rel->rd_att;
-	Bitmapset *pkey = YBGetTablePrimaryKeyBms(rel);
-	AttrNumber attr_offset = YBGetFirstLowInvalidAttributeNumber(rel);
 
-	int key_idx = 0;
-	for (int i = 0; i < tupledesc->natts; ++i)
+	for (int i = 0; i < rd_index->indnkeyatts; ++i)
 	{
-		Form_pg_attribute attr = TupleDescAttr(tupledesc, i);
-		/* Key column */
-		if (bms_is_member(attr->attnum - attr_offset, pkey))
-		{
-			type_entities[key_idx] = YbDataTypeFromOidMod(InvalidAttrNumber,
-														  attr->atttypid);
-			YBCPgTypeAttrs type_attrs;
-			type_attrs.typmod = attr->atttypmod;
-			type_attrs_arr[key_idx] = type_attrs;
-			pkeys_atttypid[key_idx] = attr->atttypid;
-			++key_idx;
-		}
-	}
+		Form_pg_attribute attr =
+			TupleDescAttr(tupledesc, is_table ? rd_index->indkey.values[i] - 1
+											  : i);
+		type_entities[i] = YbDataTypeFromOidMod(InvalidAttrNumber,
+												attr->atttypid);
+		YBCPgTypeAttrs type_attrs;
+		type_attrs.typmod = attr->atttypmod;
+		type_attrs_arr[i] = type_attrs;
+		pkeys_atttypid[i] = attr->atttypid;
+	}
+	if (is_table)
+		relation_close(index_rel, AccessShareLock);
+	relation_close(rel, AccessShareLock);
 
 	/* Get Split point values as Postgres datums */
 	HandleYBStatus(YBCGetSplitPoints(yb_tabledesc, type_entities,
 									 type_attrs_arr, split_datums, has_null));
-	relation_close(rel, AccessShareLock);
 }
 
 /*
diff --git a/src/postgres/src/test/regress/expected/yb_get_range_split_clause.out b/src/postgres/src/test/regress/expected/yb_get_range_split_clause.out
index 6c719cb5d501..8ffd32e975f8 100644
--- a/src/postgres/src/test/regress/expected/yb_get_range_split_clause.out
+++ b/src/postgres/src/test/regress/expected/yb_get_range_split_clause.out
@@ -632,3 +632,53 @@ SELECT yb_get_range_split_clause('tbl_group_table'::regclass);
 
 DROP TABLE tbl_group_table;
 DROP TABLEGROUP tbl_group;
+-- Test the scenario where primary key columns' ordering in PRIMARY KEY
+-- constraint is different from the key column's ordering in CREATE TABLE
+-- statement.
+CREATE TABLE column_ordering_mismatch (
+  k2 TEXT,
+  v DOUBLE PRECISION,
+  k1 INT,
+  PRIMARY KEY (k1 ASC, k2 ASC)
+) SPLIT AT VALUES((1, '1'), (100, '100'));
+SELECT yb_get_range_split_clause('column_ordering_mismatch'::regclass);
+        yb_get_range_split_clause
+------------------------------------------
+ SPLIT AT VALUES ((1, '1'), (100, '100'))
+(1 row)
+
+DROP TABLE column_ordering_mismatch;
+-- Test table PRIMARY KEY with INCLUDE clause
+CREATE TABLE tbl_with_include_clause (
+  k2 TEXT,
+  v DOUBLE PRECISION,
+  k1 INT,
+  PRIMARY KEY (k1 ASC, k2 ASC) INCLUDE (v)
+) SPLIT AT VALUES((1, '1'), (100, '100'));
+SELECT yb_get_range_split_clause('tbl_with_include_clause'::regclass);
+        yb_get_range_split_clause
+------------------------------------------
+ SPLIT AT VALUES ((1, '1'), (100, '100'))
+(1 row)
+
+DROP TABLE tbl_with_include_clause;
+-- Test secondary index with duplicate columns and backwards order columns
+CREATE TABLE test_tbl (
+  k1 INT,
+  k2 TEXT,
+  k3 DOUBLE PRECISION
+);
+CREATE INDEX test_idx on test_tbl (
+  k3 ASC,
+  k2 ASC,
+  k1 ASC,
+  k3 DESC
+) SPLIT AT VALUES ((1.1, '11', 1, 1.1), (3.3, '33', 3, 3.3));
+SELECT yb_get_range_split_clause('test_idx'::regclass);
+                 yb_get_range_split_clause
+------------------------------------------------------------
+ SPLIT AT VALUES ((1.1, '11', 1, 1.1), (3.3, '33', 3, 3.3))
+(1 row)
+
+DROP INDEX test_idx;
+DROP TABLE test_tbl;
diff --git a/src/postgres/src/test/regress/sql/yb_get_range_split_clause.sql b/src/postgres/src/test/regress/sql/yb_get_range_split_clause.sql
index b67d25bd91f4..ec81f7db0f88 100644
--- a/src/postgres/src/test/regress/sql/yb_get_range_split_clause.sql
+++ b/src/postgres/src/test/regress/sql/yb_get_range_split_clause.sql
@@ -409,3 +409,41 @@ SELECT yb_get_range_split_clause('tbl_group_table'::regclass);
 DROP TABLE tbl_group_table;
 
 DROP TABLEGROUP tbl_group;
+
+-- Test the scenario where primary key columns' ordering in PRIMARY KEY
+-- constraint is different from the key column's ordering in CREATE TABLE
+-- statement.
+CREATE TABLE column_ordering_mismatch (
+  k2 TEXT,
+  v DOUBLE PRECISION,
+  k1 INT,
+  PRIMARY KEY (k1 ASC, k2 ASC)
+) SPLIT AT VALUES((1, '1'), (100, '100'));
+SELECT yb_get_range_split_clause('column_ordering_mismatch'::regclass);
+DROP TABLE column_ordering_mismatch;
+
+-- Test table PRIMARY KEY with INCLUDE clause
+CREATE TABLE tbl_with_include_clause (
+  k2 TEXT,
+  v DOUBLE PRECISION,
+  k1 INT,
+  PRIMARY KEY (k1 ASC, k2 ASC) INCLUDE (v)
+) SPLIT AT VALUES((1, '1'), (100, '100'));
+SELECT yb_get_range_split_clause('tbl_with_include_clause'::regclass);
+DROP TABLE tbl_with_include_clause;
+
+-- Test secondary index with duplicate columns and backwards order columns
+CREATE TABLE test_tbl (
+  k1 INT,
+  k2 TEXT,
+  k3 DOUBLE PRECISION
+);
+CREATE INDEX test_idx on test_tbl (
+  k3 ASC,
+  k2 ASC,
+  k1 ASC,
+  k3 DESC
+) SPLIT AT VALUES ((1.1, '11', 1, 1.1), (3.3, '33', 3, 3.3));
+SELECT yb_get_range_split_clause('test_idx'::regclass);
+DROP INDEX test_idx;
+DROP TABLE test_tbl;
