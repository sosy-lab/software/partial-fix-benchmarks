[DocDB] Transaction not found when local transaction requires promotion on first operation
Fixed with 571d485.
Reactivating till the 2.16 backport is done.