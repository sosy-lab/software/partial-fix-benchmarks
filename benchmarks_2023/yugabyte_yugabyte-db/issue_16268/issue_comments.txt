[DocDB][Tablet Splitting][PITR] Queries and PITR are timing out. 
I observed the issue with 2.17.3.0-b123(in which the fix is landed) as well
@Arjun-yb can you share the master/tserver logs for this run?
https://github.com/yugabyte/yugabyte-db/issues/16669 tracks the deadlock that causes the recent failure. 
@Arjun-yb , I am closing this issue as the next follow is tracked as part of https://github.com/yugabyte/yugabyte-db/issues/16669. This keeps the backport tracking simple.