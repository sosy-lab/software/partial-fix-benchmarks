diff --git a/ent/src/yb/integration-tests/twodc_ysql-test.cc b/ent/src/yb/integration-tests/twodc_ysql-test.cc
index a87f5fabb2f8..15e6aa69aba7 100644
--- a/ent/src/yb/integration-tests/twodc_ysql-test.cc
+++ b/ent/src/yb/integration-tests/twodc_ysql-test.cc
@@ -44,6 +44,8 @@
 #include "yb/client/transaction.h"
 #include "yb/client/yb_op.h"
 
+#include "yb/docdb/docdb.h"
+
 #include "yb/gutil/stl_util.h"
 #include "yb/gutil/strings/join.h"
 #include "yb/gutil/strings/substitute.h"
@@ -399,6 +401,16 @@ class TwoDCYsqlTest : public TwoDCTestBase
     }, MonoDelta::FromSeconds(kRpcTimeout), "Verify number of records");
   }
 
+  void VerifyExternalTxnIntentsStateEmpty() {
+    tserver::TSTabletManager::TabletPtrs tablet_ptrs;
+    for (auto& mini_tserver : consumer_cluster()->mini_tablet_servers()) {
+      mini_tserver->server()->tablet_manager()->GetTabletPeers(&tablet_ptrs);
+      for (auto& tablet : tablet_ptrs) {
+        ASSERT_EQ(0, tablet->GetExternalTxnIntentsState()->EntryCount());
+      }
+    }
+  }
+
   Status TruncateTable(Cluster* cluster,
                        std::vector<string> table_ids) {
     RETURN_NOT_OK(cluster->client_->TruncateTables(table_ids));
@@ -442,6 +454,8 @@ TEST_P(TwoDCYsqlTestToggleBatching, GenerateSeries) {
   ASSERT_NO_FATALS(WriteGenerateSeries(0, 50, &producer_cluster_, producer_table->name()));
 
   ASSERT_OK(VerifyWrittenRecords(producer_table->name(), consumer_table->name()));
+
+  VerifyExternalTxnIntentsStateEmpty();
 }
 
 TEST_P(TwoDCYsqlTestToggleBatching, GenerateSeriesMultipleTransactions) {
@@ -464,6 +478,8 @@ TEST_P(TwoDCYsqlTestToggleBatching, GenerateSeriesMultipleTransactions) {
   ASSERT_EQ(resp.entry().tables_size(), 1);
   ASSERT_EQ(resp.entry().tables(0), producer_table->id());
   ASSERT_OK(VerifyWrittenRecords(producer_table->name(), consumer_table->name()));
+
+  VerifyExternalTxnIntentsStateEmpty();
 }
 
 TEST_P(TwoDCYsqlTestToggleBatching, ChangeRole) {
diff --git a/src/yb/docdb/docdb.cc b/src/yb/docdb/docdb.cc
index c68968fa5dfa..870bc7e76d25 100644
--- a/src/yb/docdb/docdb.cc
+++ b/src/yb/docdb/docdb.cc
@@ -450,9 +450,24 @@ IntraTxnWriteId ExternalTxnIntentsState::GetWriteIdAndIncrement(const Transactio
   return map_[txn_id]++;
 }
 
-void ExternalTxnIntentsState::EraseEntry(const TransactionId& txn_id) {
+void ExternalTxnIntentsState::EraseEntries(
+    const ExternalTxnApplyState& apply_external_transactions) {
   std::lock_guard<decltype(mutex_)> lock(mutex_);
-  map_.erase(txn_id);
+  for (const auto& apply : apply_external_transactions) {
+    map_.erase(apply.first);
+  }
+}
+
+void ExternalTxnIntentsState::EraseEntries(const TransactionIdSet& transactions) {
+  std::lock_guard<decltype(mutex_)> lock(mutex_);
+  for (const auto& transaction : transactions) {
+    map_.erase(transaction);
+  }
+}
+
+size_t ExternalTxnIntentsState::EntryCount() {
+  std::lock_guard<decltype(mutex_)> lock(mutex_);
+  return map_.size();
 }
 
 bool AddExternalPairToWriteBatch(
@@ -491,9 +506,6 @@ bool AddExternalPairToWriteBatch(
     // The same write operation could contain external intents and instruct us to apply them.
     CHECK_OK(PrepareApplyExternalIntentsBatch(
         it->second.commit_ht, key_value, regular_write_batch, &it->second.write_id));
-    if (external_txns_intents_state) {
-      external_txns_intents_state->EraseEntry(txn_id);
-    }
     return false;
   }
 
@@ -552,6 +564,11 @@ bool PrepareExternalWriteBatch(
         write_pair, hybrid_time, &apply_external_transactions, regular_write_batch,
         intents_write_batch, external_txns_intents_state) || has_non_external_kvs;
   }
+
+  // Cleanup the txn, write_id map for transactions which are being applied as part of this batch.
+  if (external_txns_intents_state) {
+    external_txns_intents_state->EraseEntries(apply_external_transactions);
+  }
   return has_non_external_kvs;
 }
 
diff --git a/src/yb/docdb/docdb.h b/src/yb/docdb/docdb.h
index acfdba2504bc..2e87bc67b157 100644
--- a/src/yb/docdb/docdb.h
+++ b/src/yb/docdb/docdb.h
@@ -152,7 +152,12 @@ using ExternalTxnApplyState = std::map<TransactionId, ExternalTxnApplyStateData>
 class ExternalTxnIntentsState {
  public:
   IntraTxnWriteId GetWriteIdAndIncrement(const TransactionId& txn_id);
-  void EraseEntry(const TransactionId& txn_id);
+  // Used by PrepareExternalWriteBatch when applying external transactions.
+  void EraseEntries(const ExternalTxnApplyState& apply_external_transaction);
+  // Used by DocDBCompactionFilterIntents when cleaning up aborted external txns.
+  void EraseEntries(const TransactionIdSet& transactions);
+  size_t EntryCount();
+
  private:
   std::mutex mutex_;
   std::unordered_map<TransactionId, IntraTxnWriteId, TransactionIdHash> map_;
diff --git a/src/yb/docdb/docdb_compaction_filter_intents.cc b/src/yb/docdb/docdb_compaction_filter_intents.cc
index 042ba1546266..a5d5648c0336 100644
--- a/src/yb/docdb/docdb_compaction_filter_intents.cc
+++ b/src/yb/docdb/docdb_compaction_filter_intents.cc
@@ -19,6 +19,7 @@
 
 #include "yb/common/common.pb.h"
 
+#include "yb/docdb/docdb.h"
 #include "yb/docdb/doc_kv_util.h"
 #include "yb/docdb/docdb-internal.h"
 #include "yb/docdb/intent.h"
@@ -87,12 +88,13 @@ class DocDBIntentsCompactionFilter : public rocksdb::CompactionFilter {
   Result<boost::optional<TransactionId>> FilterTransactionMetadata(
       const Slice& key, const Slice& existing_value);
 
-  Result<rocksdb::FilterDecision> FilterExternalIntent(const Slice& key);
+  Result<boost::optional<TransactionId>> FilterExternalIntent(const Slice& key);
 
   tablet::Tablet* const tablet_;
   const MicrosTime compaction_start_time_;
 
   TransactionIdSet transactions_to_cleanup_;
+  TransactionIdSet external_transactions_to_cleanup_;
   int rejected_transactions_ = 0;
   uint64_t num_errors_ = 0;
 
@@ -113,15 +115,23 @@ class DocDBIntentsCompactionFilter : public rocksdb::CompactionFilter {
 
 Status DocDBIntentsCompactionFilter::CleanupTransactions() {
   VLOG_WITH_PREFIX(3) << "DocDB intents compaction filter is being deleted";
-  if (transactions_to_cleanup_.empty()) {
-    return Status::OK();
+  if (!transactions_to_cleanup_.empty()) {
+    TransactionStatusManager* manager = tablet_->transaction_participant();
+    if (rejected_transactions_ > 0) {
+      LOG_WITH_PREFIX(WARNING) << "Number of aborted transactions not cleaned up "
+                               << "on account of reaching size limits: " << rejected_transactions_;
+    }
+    RETURN_NOT_OK(manager->Cleanup(std::move(transactions_to_cleanup_)));
   }
-  TransactionStatusManager* manager = tablet_->transaction_participant();
-  if (rejected_transactions_ > 0) {
-    LOG_WITH_PREFIX(WARNING) << "Number of aborted transactions not cleaned up " <<
-                                "on account of reaching size limits:" << rejected_transactions_;
+
+  if (!external_transactions_to_cleanup_.empty()) {
+    auto external_txn_intents_state = tablet_->GetExternalTxnIntentsState();
+    if (external_txn_intents_state) {
+      external_txn_intents_state->EraseEntries(external_transactions_to_cleanup_);
+    }
   }
-  return manager->Cleanup(std::move(transactions_to_cleanup_));
+
+  return Status::OK();
 }
 
 DocDBIntentsCompactionFilter::~DocDBIntentsCompactionFilter() {
@@ -135,19 +145,22 @@ rocksdb::FilterDecision DocDBIntentsCompactionFilter::Filter(
     filter_usage_logged_ = true;
   }
 
-  if (GetKeyType(key, StorageDbType::kIntents) == KeyType::kExternalIntents) {
-    auto filter_decision_result = FilterExternalIntent(key);
-    MAYBE_LOG_ERROR_AND_RETURN_KEEP(filter_decision_result);
-    return *filter_decision_result;
-  }
-
-  // Find transaction metadata row.
-  if (GetKeyType(key, StorageDbType::kIntents) == KeyType::kTransactionMetadata) {
-    auto transaction_id_result = FilterTransactionMetadata(key, existing_value);
+  bool is_external_intent = (GetKeyType(key, StorageDbType::kIntents) == KeyType::kExternalIntents);
+  if (is_external_intent ||
+      GetKeyType(key, StorageDbType::kIntents) == KeyType::kTransactionMetadata) {
+    auto transaction_id_result = is_external_intent
+                                     ? FilterExternalIntent(key)
+                                     : FilterTransactionMetadata(key, existing_value);
     MAYBE_LOG_ERROR_AND_RETURN_KEEP(transaction_id_result);
     auto transaction_id_optional = *transaction_id_result;
-    if (transaction_id_optional.has_value()) {
-      AddToSet(transaction_id_optional.value(), &transactions_to_cleanup_);
+    if (!transaction_id_optional.has_value()) {
+      return rocksdb::FilterDecision::kKeep;
+    }
+    AddToSet(
+        *transaction_id_optional,
+        is_external_intent ? &external_transactions_to_cleanup_ : &transactions_to_cleanup_);
+    if (is_external_intent) {
+      return rocksdb::FilterDecision::kDiscard;
     }
   }
 
@@ -176,13 +189,12 @@ Result<boost::optional<TransactionId>> DocDBIntentsCompactionFilter::FilterTrans
       DecodeTransactionIdFromIntentValue(&key_slice), "Could not decode Transaction metadata");
 }
 
-Result<rocksdb::FilterDecision>
-DocDBIntentsCompactionFilter::FilterExternalIntent(const Slice& key) {
+Result<boost::optional<TransactionId>> DocDBIntentsCompactionFilter::FilterExternalIntent(
+    const Slice& key) {
   Slice key_slice = key;
   RETURN_NOT_OK_PREPEND(key_slice.consume_byte(KeyEntryTypeAsChar::kExternalTransactionId),
                         "Could not decode external transaction byte");
-  // Ignoring transaction id result since function just returns kKeep or kDiscard.
-  RETURN_NOT_OK_PREPEND(
+  auto txn_id = VERIFY_RESULT_PREPEND(
       DecodeTransactionId(&key_slice), "Could not decode external transaction id");
   auto doc_hybrid_time = VERIFY_RESULT_PREPEND(
       DecodeInvertedDocHt(key_slice), "Could not decode hybrid time");
@@ -190,9 +202,9 @@ DocDBIntentsCompactionFilter::FilterExternalIntent(const Slice& key) {
   int64_t delta_micros = compaction_start_time_ - write_time_micros;
   if (delta_micros >
       GetAtomicFlag(&FLAGS_external_intent_cleanup_secs) * MonoTime::kMicrosecondsPerSecond) {
-    return rocksdb::FilterDecision::kDiscard;
+    return txn_id;
   }
-  return rocksdb::FilterDecision::kKeep;
+  return boost::none;
 }
 
 void DocDBIntentsCompactionFilter::CompactionFinished() {
diff --git a/src/yb/tablet/tablet.h b/src/yb/tablet/tablet.h
index 5166f2bbf83d..cb614621b860 100644
--- a/src/yb/tablet/tablet.h
+++ b/src/yb/tablet/tablet.h
@@ -778,6 +778,9 @@ class Tablet : public AbstractTablet, public TransactionIntentApplier {
 
   std::string LogPrefix() const;
 
+  docdb::ExternalTxnIntentsState* GetExternalTxnIntentsState() const {
+    return external_txn_intents_state_.get();
+  }
  private:
   friend class Iterator;
   friend class TabletPeerTest;
