diff --git a/src/postgres/src/backend/optimizer/util/clauses.c b/src/postgres/src/backend/optimizer/util/clauses.c
index 13e09c757398..523d0fa08631 100644
--- a/src/postgres/src/backend/optimizer/util/clauses.c
+++ b/src/postgres/src/backend/optimizer/util/clauses.c
@@ -5292,6 +5292,13 @@ inline_set_returning_function(PlannerInfo *root, RangeTblEntry *rte)
 	 */
 	record_plan_function_dependency(root, func_oid);
 
+	/*
+	 * We must also notice if the inserted query adds a dependency on the
+	 * calling role due to RLS quals.
+	 */
+	if (querytree->hasRowSecurity)
+		root->glob->dependsOnRole = true;
+
 	return querytree;
 
 	/* Here if func is not inlinable: release temp memory and return NULL */
diff --git a/src/postgres/src/test/regress/expected/rowsecurity.out b/src/postgres/src/test/regress/expected/rowsecurity.out
index 791581241d41..0cc84997da7d 100644
--- a/src/postgres/src/test/regress/expected/rowsecurity.out
+++ b/src/postgres/src/test/regress/expected/rowsecurity.out
@@ -3975,6 +3975,33 @@ DROP OPERATOR <<< (int, int);
 DROP FUNCTION op_leak(int, int);
 RESET SESSION AUTHORIZATION;
 DROP TABLE rls_tbl;
+-- CVE-2023-2455: inlining an SRF may introduce an RLS dependency
+create table rls_t (c text);
+insert into rls_t values ('invisible to bob');
+alter table rls_t enable row level security;
+grant select on rls_t to regress_rls_alice, regress_rls_bob;
+create policy p1 on rls_t for select to regress_rls_alice using (true);
+create policy p2 on rls_t for select to regress_rls_bob using (false);
+create function rls_f () returns setof rls_t
+  stable language sql
+  as $$ select * from rls_t $$;
+prepare q as select current_user, * from rls_f();
+set role regress_rls_alice;
+execute q;
+   current_user    |        c         
+-------------------+------------------
+ regress_rls_alice | invisible to bob
+(1 row)
+
+set role regress_rls_bob;
+execute q;
+ current_user | c 
+--------------+---
+(0 rows)
+
+RESET ROLE;
+DROP FUNCTION rls_f();
+DROP TABLE rls_t;
 --
 -- Clean up objects
 --
diff --git a/src/postgres/src/test/regress/expected/yb_pg_rowsecurity.out b/src/postgres/src/test/regress/expected/yb_pg_rowsecurity.out
index 0f736ab2d331..ac8ffaf9ec34 100644
--- a/src/postgres/src/test/regress/expected/yb_pg_rowsecurity.out
+++ b/src/postgres/src/test/regress/expected/yb_pg_rowsecurity.out
@@ -3693,6 +3693,33 @@ DROP OPERATOR <<< (int, int);
 DROP FUNCTION op_leak(int, int);
 RESET SESSION AUTHORIZATION;
 DROP TABLE rls_tbl;
+-- CVE-2023-2455: inlining an SRF may introduce an RLS dependency
+create table rls_t (c text);
+insert into rls_t values ('invisible to bob');
+alter table rls_t enable row level security;
+grant select on rls_t to regress_rls_alice, regress_rls_bob;
+create policy p1 on rls_t for select to regress_rls_alice using (true);
+create policy p2 on rls_t for select to regress_rls_bob using (false);
+create function rls_f () returns setof rls_t
+  stable language sql
+  as $$ select * from rls_t $$;
+prepare q as select current_user, * from rls_f();
+set role regress_rls_alice;
+execute q;
+   current_user    |        c
+-------------------+------------------
+ regress_rls_alice | invisible to bob
+(1 row)
+
+set role regress_rls_bob;
+execute q;
+ current_user | c
+--------------+---
+(0 rows)
+
+RESET ROLE;
+DROP FUNCTION rls_f();
+DROP TABLE rls_t;
 --
 -- Clean up objects
 --
diff --git a/src/postgres/src/test/regress/sql/rowsecurity.sql b/src/postgres/src/test/regress/sql/rowsecurity.sql
index 795411a7f342..f8c44ceb6bd5 100644
--- a/src/postgres/src/test/regress/sql/rowsecurity.sql
+++ b/src/postgres/src/test/regress/sql/rowsecurity.sql
@@ -1816,6 +1816,26 @@ DROP FUNCTION op_leak(int, int);
 RESET SESSION AUTHORIZATION;
 DROP TABLE rls_tbl;
 
+-- CVE-2023-2455: inlining an SRF may introduce an RLS dependency
+create table rls_t (c text);
+insert into rls_t values ('invisible to bob');
+alter table rls_t enable row level security;
+grant select on rls_t to regress_rls_alice, regress_rls_bob;
+create policy p1 on rls_t for select to regress_rls_alice using (true);
+create policy p2 on rls_t for select to regress_rls_bob using (false);
+create function rls_f () returns setof rls_t
+  stable language sql
+  as $$ select * from rls_t $$;
+prepare q as select current_user, * from rls_f();
+set role regress_rls_alice;
+execute q;
+set role regress_rls_bob;
+execute q;
+
+RESET ROLE;
+DROP FUNCTION rls_f();
+DROP TABLE rls_t;
+
 --
 -- Clean up objects
 --
diff --git a/src/postgres/src/test/regress/sql/yb_pg_rowsecurity.sql b/src/postgres/src/test/regress/sql/yb_pg_rowsecurity.sql
index b6cb9dfbc8ad..c489b6d7a261 100644
--- a/src/postgres/src/test/regress/sql/yb_pg_rowsecurity.sql
+++ b/src/postgres/src/test/regress/sql/yb_pg_rowsecurity.sql
@@ -1881,6 +1881,26 @@ DROP FUNCTION op_leak(int, int);
 RESET SESSION AUTHORIZATION;
 DROP TABLE rls_tbl;
 
+-- CVE-2023-2455: inlining an SRF may introduce an RLS dependency
+create table rls_t (c text);
+insert into rls_t values ('invisible to bob');
+alter table rls_t enable row level security;
+grant select on rls_t to regress_rls_alice, regress_rls_bob;
+create policy p1 on rls_t for select to regress_rls_alice using (true);
+create policy p2 on rls_t for select to regress_rls_bob using (false);
+create function rls_f () returns setof rls_t
+  stable language sql
+  as $$ select * from rls_t $$;
+prepare q as select current_user, * from rls_f();
+set role regress_rls_alice;
+execute q;
+set role regress_rls_bob;
+execute q;
+
+RESET ROLE;
+DROP FUNCTION rls_f();
+DROP TABLE rls_t;
+
 --
 -- Clean up objects
 --
