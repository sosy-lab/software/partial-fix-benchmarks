[#13999] docdb: Implement code and tests for /api/v1/table?id=X JSON API

Summary: [#13999] docdb: Implement code and tests for /api/v1/table?id=X JSON API

Test Plan: Integration tests covering the new functionality

Reviewers: jhe, skedia

Reviewed By: jhe

Subscribers: ybase, bogdan

Differential Revision: https://phorge.dev.yugabyte.com/D25227
[#17360] docdb: Dynamically set the db_block_cache_num_shard_bits based on the number of cores

Summary:
Automatically sets the gflag db_block_cache_num_shard_bits based on the number of cores available on the machine.
The flag is initialised to -1, and then set to 4 if number of cores is less than or equal to 16, 5 for 17-32 cores, 6 for 33-64 cores and so on.
Jira: DB-6553

Test Plan: TestDbBlockCacheNumShardBits

Reviewers: qhu, rthallam, arybochkin

Reviewed By: rthallam, arybochkin

Subscribers: kannan, arybochkin, cchiplunkar, rthallam, qhu, ybase, bogdan

Differential Revision: https://phorge.dev.yugabyte.com/D25478
[BACKPORT 2.18][#17360] docdb: Dynamically set the db_block_cache_num_shard_bits based on the number of cores

Summary:
Original commit: c99b7c90f5a281d73673b6fada18993e9912de20 / D25478
Automatically sets the gflag db_block_cache_num_shard_bits based on the number of cores available on the machine.
The flag is initialised to -1, and then set to 4 if number of cores is less than or equal to 16, 5 for 17-32 cores, 6 for 33-64 cores and so on.
Jira: DB-6553

Test Plan: TestDbBlockCacheNumShardBits

Reviewers: qhu, rthallam, arybochkin

Reviewed By: rthallam

Subscribers: bogdan, ybase, qhu, rthallam, cchiplunkar, arybochkin, kannan

Differential Revision: https://phorge.dev.yugabyte.com/D28671
