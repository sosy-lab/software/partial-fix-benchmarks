[#16511] Add support for defining preview flags

Summary:
Some features like tablet splitting, catalog connection caching, etc. are initially done under a gflag, and they are off by default - because they still need soak time (from internal testing). To be safe, we also don't document the gflag when it is still in this dev/preview phase, because we don't want a user (OSS or otherwise) to accidentally turn it on.

This diff adds macros `DEFINE_RUNTIME_PREVIEW_<type>` and  `DEFINE_NON_RUNTIME_PREVIEW_<type>` that can be used to declare preview flags. If a user wants to change the value of any preview flag the flag has to be explicitly added to another flag called `allowed_preview_flags_csv` which maintains all the preview flags allowed by the user.

If there is a pereview flag changed by the user and is not included in the allow list, the yb process will crash. In case these validations fail on runtime during `SetFlag` rpc, the update request fails with `BAD_VALUE`.

Once the feature has moved out of preview (Stable) the flag name can be removed from allowed_preview_flags_csv lazily. allowed_preview_flags_csv allows preview, stable, deprecated and even non existent flags. This allows the users who already have preview flags set to upgrade seamlessly to a new version in which the flag has been moved out of preview.

Example Usage
```
yb-master --some_preview_flag=true --allowed_preview_flags_csv="some_preview_flag"
```

Test Plan:
**Unit tests**
```
ybd --cxx-test flags-test --gtest_filter FlagsTest.TestPreviewFlagsAllowList
```

**Manual Tests**

Declare a preview flag and override it without including it in allow list.
```
❯ ./yb-master --preview_flag=true
WARNING: Logging before InitGoogleLogging() is written to STDERR
F0427 01:30:08.394629 4149156672 flags.cc:494] Preview flag 'preview_flag' not found in the allow list
*** Check failure stack trace: ***
[1]    82329 abort      ./yb-master --preview_flag=true
```

Reviewers: hsunder, skumar

Reviewed By: hsunder

Differential Revision: https://phorge.dev.yugabyte.com/D24743
[#17395] DocDB: Fix for LoadBalancerPlacementPolicyTest + FullCompactionManager task refactor

Summary:
Fixes a condition between tablet peer metrics and the FullCompactionManager which can cause a nullptr access by the latter on tablet peer move/deletion. Specifically, the DocDB key statistics collection windows could previously access metrics for tablets that no longer exist.

Also refactors the full_compaction_manager_bg_task thread to be owned by FullCompactionManager rather than TsTabletManager.

This diff does not address a potential memory leak in the in-memory maps held by FullCompactionManager. That issue is to be addressed in https://github.com/yugabyte/yugabyte-db/issues/17494
Jira: DB-6580

Test Plan: ./yb_build.sh tsan --cxx-test integration-tests_load_balancer_placement_policy-test --gtest-filter LoadBalancerPlacementPolicyTest.PrefixPlacementTest

Reviewers: timur, arybochkin, esheng, rthallam

Reviewed By: arybochkin

Subscribers: ybase, bogdan

Differential Revision: https://phorge.dev.yugabyte.com/D25506
[#17494] DocDB: Add memory cleanup to FullCompactionManager

Summary:
In the case where a tablet peer moves nodes or is deleted, the FullCompactionManager has the potential to hold onto entries that no longer exist. Over time, this is will leak
memory. Specifically, the next_compact_time_per_tablet_ and tablet_stats_window_ in-memory maps hold entries that may no longer be relevant.

Adds a CleanupIfNecessary() function to FullCompactionManager which removes obsolete entries from those maps if certain conditions are met (either when too many obsolete entries are
detected in those maps or once it has been long enough since the last cleanup).

Adds the auto_compact_memory_cleanup_interval_sec flag which determines how frequently to run the cleanup process (1 hour by default, -1 turns cleanup off).
Jira: DB-6652

Test Plan: ybd --cxx-test tserver_ts_tablet_manager-test --gtest-filter TsTabletManagerTest.FullCompactionManagerCleanup

Reviewers: arybochkin, timur, esheng

Reviewed By: arybochkin

Subscribers: bogdan, ybase

Differential Revision: https://phorge.dev.yugabyte.com/D25628
[BACKPORT 2.18][#13786, #17395, #17494] DocDB: Auto full compactions based on deletes in SST files

Summary:
In use cases involving range scans, if there are several deleted/overwritten records in the range, we can benefit from automatically triggering major compactions (on the tablet/shard). This issue is not specific to tables, but can happen with indexes also. While yb-admin is a manual way of forcing this compaction, we should automatically handle detection of this condition and just automatically handle this internally, rather than rely on diagnosis/human intervention.

Using the `docdb_keys_found` and `docdb_obsolete_keys_found` metrics, we can detect the ratio of obsolete keys read vs total keys read over a window of time. If that ratio is over a certain threshold, we can schedule a full compaction accordingly. This happens when a large portion of the work in an active workload is spent searching through deleted or expired keys (i.e. TTL).

Tserver gflags added:

- `auto_compact_check_interval_sec` - Frequency with which we collect statistics and check whether any tablets are now eligible for compaction. Applies to scheduled full compactions and replaces the `scheduled_full_compaction_check_interval_min` flag. Default: 60 sec
- `auto_compact_stat_window_seconds` - Window of statistics to be collected and analyzed at any given point of time. Default 300 sec
- `auto_compact_percent_obsolete` - Percentage of obsolete keys read over the window of time to indicate that we should compact the tablet. Default 99.
- `auto_compact_min_obsolete_keys_found` - Minimum number of keys that need to have been read (since the last compaction) in order to trigger a new compaction. Default: 10000
- `auto_compact_min_wait_between_seconds` - Minimum wait time between automatic full compactions. Applies to scheduled full compactions as well. Default: 0

Original commit: e9c9f74dc46f631ce6fd291d7699739405e36046 / D24928

[#17395] DocDB: Fix for LoadBalancerPlacementPolicyTest + FullCompactionManager task refactor

Fixes a condition between tablet peer metrics and the FullCompactionManager which can cause a nullptr access by the latter on tablet peer move/deletion. Specifically, the DocDB key statistics collection windows could previously access metrics for tablets that no longer exist.

Also refactors the full_compaction_manager_bg_task thread to be owned by FullCompactionManager rather than TsTabletManager.

This diff does not address a potential memory leak in the in-memory maps held by FullCompactionManager. That issue is to be addressed in https://github.com/yugabyte/yugabyte-db/issues/17494

Original commit: bc6682c0229447cfffc7bf8aa6c9d5deb7802ff2 / D25506

[#17494] DocDB: Add memory cleanup to FullCompactionManager

In the case where a tablet peer moves nodes or is deleted, the FullCompactionManager has the potential to hold onto entries that no longer exist. Over time, this is will leak
memory. Specifically, the next_compact_time_per_tablet_ and tablet_stats_window_ in-memory maps hold entries that may no longer be relevant.

Adds a CleanupIfNecessary() function to FullCompactionManager which removes obsolete entries from those maps if certain conditions are met (either when too many obsolete entries are
detected in those maps or once it has been long enough since the last cleanup).

Adds the auto_compact_memory_cleanup_interval_sec flag which determines how frequently to run the cleanup process (1 hour by default, -1 turns cleanup off).

Original commit: 72bb9d399dfcdcadc48d7ae3613528a3587d1d0e / D25628
Jira: DB-3305, DB-6580, DB-6652

Test Plan:
Jenkins: rebase: 2.18
Tested ad-hoc by running a set of "INSERT", "DELETE", and "SELECT TOP LIMIT 1" commands (detailed further in [[ https://github.com/yugabyte/yugabyte-db/issues/15098 | #15098 ]])
./yb_build.sh tsan --cxx-test integration-tests_load_balancer_placement_policy-test --gtest-filter LoadBalancerPlacementPolicyTest.PrefixPlacementTest
ybd --cxx-test tserver_ts_tablet_manager-test --gtest-filter TsTabletManagerTest.FullCompactionManagerCleanup

Additional testing to be provided as part of [[ https://github.com/yugabyte/yugabyte-db/issues/17226 | 17226 ]]

Reviewers: esheng, timur, arybochkin, rthallam

Reviewed By: arybochkin

Subscribers: ybase, bogdan

Differential Revision: https://phorge.dev.yugabyte.com/D25362
[BACKPORT 2.19.0][#17395, #17494] DocDB: Fix for LoadBalancerPlacementPolicyTest + FullCompactionManager memory cleanup

Summary:
Fixes a condition between tablet peer metrics and the FullCompactionManager which can cause a nullptr access by the latter on tablet peer move/deletion. Specifically, the DocDB key statistics collection windows could previously access metrics for tablets that no longer exist.

Also refactors the full_compaction_manager_bg_task thread to be owned by FullCompactionManager rather than TsTabletManager.

This diff does not address a potential memory leak in the in-memory maps held by FullCompactionManager. That issue is to be addressed in https://github.com/yugabyte/yugabyte-db/issues/17494

Original commit: bc6682c0229447cfffc7bf8aa6c9d5deb7802ff2 / D25506
Jira: DB-6580

[#17494] DocDB: Add memory cleanup to FullCompactionManager

In the case where a tablet peer moves nodes or is deleted, the FullCompactionManager has the potential to hold onto entries that no longer exist. Over time, this is will leak
memory. Specifically, the next_compact_time_per_tablet_ and tablet_stats_window_ in-memory maps hold entries that may no longer be relevant.

Adds a CleanupIfNecessary() function to FullCompactionManager which removes obsolete entries from those maps if certain conditions are met (either when too many obsolete entries are
detected in those maps or once it has been long enough since the last cleanup).

Adds the auto_compact_memory_cleanup_interval_sec flag which determines how frequently to run the cleanup process (1 hour by default, -1 turns cleanup off).
Original commit: 72bb9d399dfcdcadc48d7ae3613528a3587d1d0e / D25628
Jira: DB-6652

Test Plan:
Jenkins: rebase: 2.19.0
./yb_build.sh tsan --cxx-test integration-tests_load_balancer_placement_policy-test --gtest-filter LoadBalancerPlacementPolicyTest.PrefixPlacementTest
ybd --cxx-test tserver_ts_tablet_manager-test --gtest-filter TsTabletManagerTest.FullCompactionManagerCleanup

Reviewers: timur, arybochkin, esheng, rthallam

Reviewed By: arybochkin

Subscribers: bogdan, ybase

Differential Revision: https://phorge.dev.yugabyte.com/D25827
