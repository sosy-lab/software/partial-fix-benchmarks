[#18449] YSQL: Support handling of system columns in pg_doc_op row width estimation

Summary: The row width estimation code to estimate fetch size limits when `yb_fetch_size_limit` is used crashes when a negative column attribute number is encountered. These correspond to system columns such as ybctid. We handle these possibly variable length fields by adding a constant to the estimated row width in the presence of such fields.

Test Plan: ./yb_build.sh --java-test 'org.yb.pgsql.TestPgRegressYbFetchLimits'

Reviewers: amartsinchyk, telgersma

Reviewed By: amartsinchyk

Subscribers: yql

Differential Revision: https://phorge.dev.yugabyte.com/D27361
[#18321] YSQL: Enable preloading additional tables by specifying catalog list in flags

Summary:
For the additional catalog tables to preload, different customers may have different schemas and the need to preload varies case by case. We had to make a customized build to adjust which catalogs to preload, which took repetitive work. This approach is also not forward-compatible.

Instead, unlike controlling preloading the core tables by `ysql_enable_read_request_caching`, we should make the flag take a list of catalog tables as input and introduce a new string gflag `ysql_catalog_preload_additional_table_list`. This allows us and our customers to choose what to preload without extra customized builds.

Caveat:
Though this change doesn't implement all PG catalog name mappings, it is extensible as needed and it won't cause conflicts for different preload needs, in contrast to using the old boolean flag `ysql_catalog_preload_additional_tables`, which is dependent on hardcoded catalogs.
Jira: DB-7310

Test Plan:
**Manual test
**Restart cluster with
```
./bin/yb-ctl restart --data_dir ~/yugabyte-data-debug --tserver_flags='ysql_enable_read_request_caching=true,"ysql_catalog_preload_additional_table_list=pg_range,pg_statistic,pg_sth,pg_sth2"'
```
Checked logs (removed before sending for review) that additional catalogs are registered.

Got the following in the logs
```
W0725 07:01:44.150847 1699151 relcache.c:2743] Found unrecognized catalog "pg_sth" in the flag. Ignored.
W0725 07:01:44.150902 1699151 relcache.c:2743] Found unrecognized catalog "pg_sth2" in the flag. Ignored.
I0725 07:01:44.150913 1699151 relcache.c:2763] YSQL is prefetching additional catalogs.
```

Validated invalid chars in this flag and it failed the validation:
```
E0728 17:53:07.141602 937851 ybc_pggate.cc:113] Found invalid character * in flag ysql_catalog_preload_additional_table_list
ERROR: failed validation of new value 'pg_range,pg_statistic,,pg_invalid,*,@' for flag 'ysql_catalog_preload_additional_table_list'
```

Also, validate it in the `debug` build.

**Automatic test**
```
./yb_build.sh debug --ccmds --clang16 --sjb --cxx-test pg_catalog_perf-test --gtest_filter PgCatalogPerfTest.RPCCountOnStartupAdditionalPreload --no-remote
```

Reviewers: dmitry, myang

Reviewed By: myang

Subscribers: jason, yql

Differential Revision: https://phorge.dev.yugabyte.com/D27186
[#18321] YSQL: restore --ysql_catalog_preload_additional_tables

Summary:
It was found that diff D27186 has deprecated the gflag
`--ysql_catalog_preload_additional_tables` (via `DEPRECATE_FLAG`) which several
customers already rely on. Currently the `DEPRECATE_FLAG` framework does not
allow continued usage of `ysql_catalog_preload_additional_tables` so if an
existing cluster using this gflag is upgraded, it will silently lose the
functionality provided by `--ysql_catalog_preload_additional_tables=true`.
Those customers must restore the functionality using the new gflag
`--ysql_catalog_preload_additional_table_list` which is not convenient.

This diff restores the old `--ysql_catalog_preload_additional_tables` and we now
check both the old and new gflags. If both are set, we will preload a superset
by combining the default set of additional catalog tables from the old gflag
with the set of additional catalog tables from the new gflag.
Jira: DB-7310

Test Plan: ./yb_build.sh debug --cxx-test pg_catalog_perf-test

Reviewers: tverona, smishra

Reviewed By: tverona

Subscribers: yql

Differential Revision: https://phorge.dev.yugabyte.com/D27723
[Backport 2.18][#18321] YSQL: Enable preloading additional tables by specifying catalog list in flags

Summary:
For the additional catalog tables to preload, different customers may have different schemas and the need to preload varies case by case. We had to make a customized build to adjust which catalogs to preload, which took repetitive work. This approach is also not forward-compatible.

Instead, unlike controlling preloading the core tables by `ysql_enable_read_request_caching`, we should make the flag take a list of catalog tables as input and introduce a new string gflag `ysql_catalog_preload_additional_table_list`. This allows us and our customers to choose what to preload without extra customized builds.

Caveat:
Though this change doesn't implement all PG catalog name mappings, it is extensible as needed and it won't cause conflicts for different preload needs, in contrast to using the old boolean flag `ysql_catalog_preload_additional_tables`, which is dependent on hardcoded catalogs.
Jira: DB-7310

Original commit: D27186 / f2f6a1ecbb845539fa934ab20282103b8fe39040

Test Plan:
Jenkins: rebase: 2.18

**Manual test
**Restart cluster with
```
./bin/yb-ctl restart --data_dir ~/yugabyte-data-debug --tserver_flags='ysql_enable_read_request_caching=true,"ysql_catalog_preload_additional_table_list=pg_range,pg_statistic,pg_sth,pg_sth2"'
```
Checked logs (removed before sending for review) that additional catalogs are registered.

Got the following in the logs
```
W0725 07:01:44.150847 1699151 relcache.c:2743] Found unrecognized catalog "pg_sth" in the flag. Ignored.
W0725 07:01:44.150902 1699151 relcache.c:2743] Found unrecognized catalog "pg_sth2" in the flag. Ignored.
I0725 07:01:44.150913 1699151 relcache.c:2763] YSQL is prefetching additional catalogs.
```

Validated invalid chars in this flag and it failed the validation:
```
E0728 17:53:07.141602 937851 ybc_pggate.cc:113] Found invalid character * in flag ysql_catalog_preload_additional_table_list
ERROR: failed validation of new value 'pg_range,pg_statistic,,pg_invalid,*,@' for flag 'ysql_catalog_preload_additional_table_list'
```

Also, validate it in the `debug` build.

**Automatic test**
```
./yb_build.sh debug --ccmds --clang16 --sjb --cxx-test pg_catalog_perf-test --gtest_filter PgCatalogPerfTest.RPCCountOnStartupAdditionalPreload --no-remote
```

Reviewers: dmitry, smishra, tverona

Reviewed By: tverona

Subscribers: yql, jason

Differential Revision: https://phorge.dev.yugabyte.com/D27637
[BACKPORT 2.18][#18321] YSQL: restore --ysql_catalog_preload_additional_tables

Summary:
It was found that diff D27186 has deprecated the gflag
`--ysql_catalog_preload_additional_tables` (via `DEPRECATE_FLAG`) which several
customers already rely on. Currently the `DEPRECATE_FLAG` framework does not
allow continued usage of `ysql_catalog_preload_additional_tables` so if an
existing cluster using this gflag is upgraded, it will silently lose the
functionality provided by `--ysql_catalog_preload_additional_tables=true`.
Those customers must restore the functionality using the new gflag
`--ysql_catalog_preload_additional_table_list` which is not convenient.

This diff restores the old `--ysql_catalog_preload_additional_tables` and we now
check both the old and new gflags. If both are set, we will preload a superset
by combining the default set of additional catalog tables from the old gflag
with the set of additional catalog tables from the new gflag.
Jira: DB-7310

Original commit: 389e637f4625a2b577a672f1968b6b6cfe7cea94 / D27723

Test Plan:
Jenkins: rebase: 2.18

./yb_build.sh debug --cxx-test pg_catalog_perf-test

Reviewers: tverona, smishra

Reviewed By: tverona

Subscribers: yql

Differential Revision: https://phorge.dev.yugabyte.com/D27763
