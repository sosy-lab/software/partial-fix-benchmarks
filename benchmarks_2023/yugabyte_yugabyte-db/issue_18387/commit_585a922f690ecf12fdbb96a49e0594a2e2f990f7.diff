diff --git a/src/yb/client/async_rpc.cc b/src/yb/client/async_rpc.cc
index 63ba9064b5e2..a4d057bfa768 100644
--- a/src/yb/client/async_rpc.cc
+++ b/src/yb/client/async_rpc.cc
@@ -28,7 +28,6 @@
 #include "yb/common/wire_protocol.h"
 
 #include "yb/gutil/casts.h"
-#include "yb/gutil/strings/substitute.h"
 
 #include "yb/rpc/rpc_controller.h"
 
@@ -87,11 +86,17 @@ DEFINE_UNKNOWN_bool(forward_redis_requests, true,
     "attempt to read from leaders, so redis_allow_reads_from_followers will be ignored.");
 
 DEFINE_UNKNOWN_bool(detect_duplicates_for_retryable_requests, true,
-            "Enable tracking of write requests that prevents the same write from being applied "
-                "twice.");
+    "Enable tracking of write requests that prevents the same write from being applied twice.");
 
 DEFINE_UNKNOWN_bool(ysql_forward_rpcs_to_local_tserver, false,
-            "DEPRECATED. Feature has been removed");
+    "DEPRECATED. Feature has been removed");
+
+DEFINE_test_flag(bool, asyncrpc_finished_set_timedout, false,
+    "Whether to reset asyncrpc response status to Timedout.");
+
+DEFINE_test_flag(bool, asyncrpc_common_response_check_fail_once, false,
+    "For testing only. When set to true triggers AsyncRpc::Failure() with RuntimeError status "
+    "inside AsyncRpcBase::CommonResponseCheck() and returns false from this method.");
 
 // DEPRECATED. It is assumed that all t-servers and masters in the cluster has this capability.
 // Remove it completely when it won't be necessary to support upgrade from releases which checks
@@ -100,20 +105,9 @@ DEFINE_CAPABILITY(PickReadTimeAtTabletServer, 0x8284d67b);
 
 DECLARE_bool(collect_end_to_end_traces);
 
-DEFINE_test_flag(bool, asyncrpc_finished_set_timedout, false,
-                 "Whether to reset asyncrpc response status to Timedout.");
-
 using namespace std::placeholders;
 
-namespace yb {
-
-using std::shared_ptr;
-using tserver::WriteRequestPB;
-using strings::Substitute;
-
-namespace client {
-
-namespace internal {
+namespace yb::client::internal {
 
 bool IsTracingEnabled() {
   auto *trace = Trace::CurrentTrace();
@@ -238,6 +232,7 @@ std::shared_ptr<const YBTable> AsyncRpc::table() const {
 }
 
 void AsyncRpc::Finished(const Status& status) {
+  VLOG_WITH_FUNC(4) << "status: " << status << ", error: " << AsString(response_error());
   Status new_status = status;
   if (status.ok()) {
     if (PREDICT_FALSE(ANNOTATE_UNPROTECTED_READ(FLAGS_TEST_asyncrpc_finished_set_timedout))) {
@@ -263,6 +258,7 @@ void AsyncRpc::Finished(const Status& status) {
 }
 
 void AsyncRpc::Failed(const Status& status) {
+  VLOG_WITH_FUNC(4) << "status: " << status.ToString();
   std::string error_message = status.message().ToBuffer();
   auto redis_error_code = status.IsInvalidCommand() || status.IsInvalidArgument() ?
       RedisResponsePB_RedisStatusCode_PARSING_ERROR : RedisResponsePB_RedisStatusCode_SERVER_ERROR;
@@ -283,8 +279,8 @@ void AsyncRpc::Failed(const Status& status) {
         // cluster map instead.
         if (status.IsIllegalState()) {
           resp->set_code(RedisResponsePB_RedisStatusCode_SERVER_ERROR);
-          resp->set_error_message(Substitute("MOVED $0 0.0.0.0:0",
-                                             down_cast<YBRedisOp*>(yb_op)->hash_code()));
+          resp->set_error_message(Format("MOVED $0 0.0.0.0:0",
+                                         down_cast<YBRedisOp*>(yb_op)->hash_code()));
         } else {
           resp->set_code(redis_error_code);
           resp->set_error_message(error_message);
@@ -305,6 +301,8 @@ void AsyncRpc::Failed(const Status& status) {
         PgsqlResponsePB* resp = down_cast<YBPgsqlOp*>(yb_op)->mutable_response();
         resp->set_status(status.IsTryAgain() ? PgsqlResponsePB::PGSQL_STATUS_RESTART_REQUIRED_ERROR
                                              : PgsqlResponsePB::PGSQL_STATUS_RUNTIME_ERROR);
+        // TODO(14814, 18387): At the moment only one error status is supported.
+        resp->mutable_error_status()->Clear();
         StatusToPB(status, resp->add_error_status());
         // For backward compatibility set also deprecated fields
         resp->set_error_message(error_message);
@@ -408,6 +406,14 @@ AsyncRpcBase<Req, Resp>::~AsyncRpcBase() {
 
 template <class Req, class Resp>
 bool AsyncRpcBase<Req, Resp>::CommonResponseCheck(const Status& status) {
+  if (PREDICT_FALSE(ANNOTATE_UNPROTECTED_READ(
+      FLAGS_TEST_asyncrpc_common_response_check_fail_once))) {
+    ANNOTATE_UNPROTECTED_WRITE(FLAGS_TEST_asyncrpc_common_response_check_fail_once) = false;
+    const auto status = STATUS(RuntimeError, "CommonResponseCheck test runtime error");
+    LOG_WITH_FUNC(INFO) << "Generating failure: " << status;
+    Failed(status);
+    return false;
+  }
   if (!status.ok()) {
     return false;
   }
@@ -453,6 +459,7 @@ void AsyncRpcBase<Req, Resp>::ProcessResponseFromTserver(const Status& status) {
   }
   NotifyBatcher(status);
   if (!CommonResponseCheck(status)) {
+    VLOG_WITH_FUNC(4) << "CommonResponseCheck failed, status: " << status;
     return;
   }
   auto swap_status = SwapResponses();
@@ -502,7 +509,7 @@ void FillOps(
     auto* concrete_op = down_cast<OpType*>(op.yb_op.get());
     out->AddAllocated(concrete_op->mutable_request());
     HandleExtraFields(concrete_op, req);
-    VLOG(4) << ++idx << ") encoded row: " << op.yb_op->ToString();
+    VLOG(5) << ++idx << ") encoded row: " << op.yb_op->ToString();
   }
 }
 
@@ -833,6 +840,4 @@ void ReadRpc::NotifyBatcher(const Status& status) {
   batcher_->ProcessReadResponse(*this, status);
 }
 
-}  // namespace internal
-}  // namespace client
-}  // namespace yb
+}  // namespace yb::client::internal
diff --git a/src/yb/client/session.cc b/src/yb/client/session.cc
index 57a618cf14f2..f33a110ccc3b 100644
--- a/src/yb/client/session.cc
+++ b/src/yb/client/session.cc
@@ -28,10 +28,11 @@
 #include "yb/tserver/tserver_error.h"
 
 #include "yb/util/debug-util.h"
+#include "yb/util/flags.h"
 #include "yb/util/logging.h"
 #include "yb/util/metrics.h"
 #include "yb/util/status_log.h"
-#include "yb/util/flags.h"
+#include "yb/util/sync_point.h"
 
 using namespace std::literals;
 using namespace std::placeholders;
@@ -39,20 +40,15 @@ using namespace std::placeholders;
 DEFINE_UNKNOWN_int32(client_read_write_timeout_ms, 60000,
     "Timeout for client read and write operations.");
 
-namespace yb {
-namespace client {
-
-using internal::AsyncRpcMetrics;
-using internal::Batcher;
-
-using std::shared_ptr;
+namespace yb::client {
 
 YBSession::YBSession(YBClient* client, const scoped_refptr<ClockBase>& clock) {
   batcher_config_.client = client;
   batcher_config_.non_transactional_read_point =
       clock ? std::make_unique<ConsistentReadPoint>(clock) : nullptr;
   const auto metric_entity = client->metric_entity();
-  async_rpc_metrics_ = metric_entity ? std::make_shared<AsyncRpcMetrics>(metric_entity) : nullptr;
+  async_rpc_metrics_ =
+      metric_entity ? std::make_shared<internal::AsyncRpcMetrics>(metric_entity) : nullptr;
 }
 
 YBSession::YBSession(YBClient* client, MonoDelta delta, const scoped_refptr<ClockBase>& clock)
@@ -207,6 +203,9 @@ void BatcherFlushDone(
     }
     retry_batcher->Add(op);
   }
+
+  TEST_SYNC_POINT("BatcherFlushDone:Retry:1");
+
   FlushBatcherAsync(retry_batcher, std::move(callback), batcher_config,
       internal::IsWithinTransactionRetry::kTrue);
 }
@@ -439,5 +438,4 @@ bool ShouldSessionRetryError(const Status& status) {
          consensus::ConsensusError(status) == consensus::ConsensusErrorPB::TABLET_SPLIT;
 }
 
-} // namespace client
-} // namespace yb
+} // namespace yb::client
diff --git a/src/yb/tablet/abstract_tablet.cc b/src/yb/tablet/abstract_tablet.cc
index 75decd935e59..c8fa5c2feeaa 100644
--- a/src/yb/tablet/abstract_tablet.cc
+++ b/src/yb/tablet/abstract_tablet.cc
@@ -27,10 +27,7 @@
 
 #include "yb/util/trace.h"
 
-using std::vector;
-
-namespace yb {
-namespace tablet {
+namespace yb::tablet {
 
 Result<HybridTime> AbstractTablet::SafeTime(RequireLease require_lease,
                                             HybridTime min_allowed,
@@ -104,6 +101,9 @@ Status AbstractTablet::ProcessPgsqlReadRequest(
   if (!fetched_rows.ok()) {
     result->response.set_status(PgsqlResponsePB::PGSQL_STATUS_RUNTIME_ERROR);
     const auto& s = fetched_rows.status();
+
+    // TODO(14814, 18387): At the moment only one error status is supported.
+    result->response.mutable_error_status()->Clear();
     StatusToPB(s, result->response.add_error_status());
     // For backward compatibility set also deprecated error message
     result->response.set_error_message(s.message().cdata(), s.message().size());
@@ -128,5 +128,4 @@ Status AbstractTablet::ProcessPgsqlReadRequest(
   return Status::OK();
 }
 
-}  // namespace tablet
-}  // namespace yb
+}  // namespace yb::tablet
diff --git a/src/yb/tablet/tablet_peer.cc b/src/yb/tablet/tablet_peer.cc
index 38a0cd35b92c..494c8d094f9c 100644
--- a/src/yb/tablet/tablet_peer.cc
+++ b/src/yb/tablet/tablet_peer.cc
@@ -1590,8 +1590,8 @@ bool TabletPeer::CanBeDeleted() {
 
   LOG_WITH_PREFIX(INFO) << Format(
       "Marked tablet $0 as requiring cleanup due to all replicas have been split (all applied op "
-      "id: $1, split op id: $2)",
-      tablet_id(), all_applied_op_id, op_id);
+      "id: $1, split op id: $2, data state: $3)",
+      tablet_id(), all_applied_op_id, op_id, TabletDataState_Name(data_state()));
 
   return true;
 }
diff --git a/src/yb/tserver/pg_client_session.cc b/src/yb/tserver/pg_client_session.cc
index 2e4b0d766fe8..2a1a8e08d6d4 100644
--- a/src/yb/tserver/pg_client_session.cc
+++ b/src/yb/tserver/pg_client_session.cc
@@ -268,12 +268,12 @@ Status HandleResponse(uint64_t session_id,
   }
 
   if (response.error_status().size() > 0) {
-    // We do not currently expect more than one status, when we do, we need to decide how to handle
-    // them. Possible options: aggregate multiple statuses into one, discard all but one, etc.
-    DCHECK_EQ(response.error_status().size(), 1) << "Too many error statuses in the response";
-    for (const auto& pb : response.error_status()) {
-      return StatusFromPB(pb);
-    }
+    // TODO(14814, 18387):  We do not currently expect more than one status, when we do, we need
+    // to decide how to handle them. Possible options: aggregate multiple statuses into one, discard
+    // all but one, etc. Historically, for the one set of status fields (like error_message), new
+    // error message was overriting the previous one, that's why let's return the last entry from
+    // error_status to mimic that past behavior, refer AsyncRpc::Finished for details.
+    return StatusFromPB(*response.error_status().rbegin());
   }
 
   // Older nodes may still use deprecated fields for status, so keep legacy handling
@@ -410,7 +410,8 @@ struct PerformData {
         if (PgsqlRequestStatus(status) == PgsqlResponsePB::PGSQL_STATUS_SCHEMA_VERSION_MISMATCH) {
           table_cache.Invalidate(op->table()->id());
         }
-        VLOG(2) << SessionLogPrefix(session_id) << "Failed op " << idx << ": " << status;
+        VLOG_WITH_FUNC(2) << SessionLogPrefix(session_id) << "status: " << status
+                          << ", failed op[" << idx << "]: " << AsString(op);
         return status.CloneAndAddErrorCode(OpIndex(idx));
       }
       // In case of write operation, increase mutation counter
diff --git a/src/yb/yql/pgwrapper/pg_tablet_split-test.cc b/src/yb/yql/pgwrapper/pg_tablet_split-test.cc
index cc58c098e85f..95ff3cd3e070 100644
--- a/src/yb/yql/pgwrapper/pg_tablet_split-test.cc
+++ b/src/yb/yql/pgwrapper/pg_tablet_split-test.cc
@@ -63,6 +63,7 @@ DECLARE_bool(ysql_enable_packed_row);
 
 DECLARE_int32(TEST_fetch_next_delay_ms);
 DECLARE_int32(TEST_partitioning_version);
+DECLARE_bool(TEST_asyncrpc_common_response_check_fail_once);
 DECLARE_bool(TEST_skip_partitioning_version_validation);
 DECLARE_uint64(TEST_wait_row_mark_exclusive_count);
 
@@ -256,6 +257,51 @@ TEST_F(PgTabletSplitTest, SplitDuringLongScan) {
   ASSERT_OK(WaitForSplitCompletion(table_id));
 }
 
+#ifndef NDEBUG
+// Repro for https://github.com/yugabyte/yugabyte-db/issues/18387.
+// The test checks that we are getting the expected error if an operation is failing several
+// times in a row: in this case it is expected to get the latest error status.
+// The test reproduces two failure. The first failure is happening after a tablet has been split
+// and the request should be forwarded to the one of its children. The second failure is generated
+// synthetically in AsyncRpcBase::CommonResponseCheck(). Before the fix, the test fails with
+// DCHECK_EQ(response.error_status().size(), 1) in pg_client_session.cc:HandleResponse() due to
+// PgsqlResponsePB::error_status contains two entries because this collection was not cleaned
+// before the retry in the original change (where error_status has been introduced).
+TEST_F(PgTabletSplitTest, CommonResponseCheckFailureAfterOperationRetry) {
+  constexpr auto kNumRows = 100;
+
+  auto conn = ASSERT_RESULT(Connect());
+
+  ASSERT_OK(conn.Execute("CREATE TABLE t(k INT PRIMARY KEY, v INT) SPLIT INTO 1 TABLETS;"));
+  ASSERT_OK(conn.ExecuteFormat(
+      "INSERT INTO t SELECT i, 1 FROM (SELECT generate_series(1, $0) i) t2;", kNumRows));
+
+  const auto table_id = ASSERT_RESULT(GetTableIDFromTableName("t"));
+  const auto peers = ListTableActiveTabletLeadersPeers(cluster_.get(), table_id);
+  ASSERT_EQ(1, peers.size());
+
+  // Flush tablets and make sure SST files have appeared to be able to split.
+  ASSERT_OK(cluster_->FlushTablets());
+  ASSERT_OK(WaitForAnySstFiles(peers.front()));
+
+  ASSERT_OK(SplitSingleTabletAndWaitForActiveChildTablets(table_id));
+
+  yb::SyncPoint::GetInstance()->SetCallBack("BatcherFlushDone:Retry:1", [&](void* arg) {
+    LOG(INFO) << "Batcher retry detected: setting flag to fail retry.";
+    ANNOTATE_UNPROTECTED_WRITE(FLAGS_TEST_asyncrpc_common_response_check_fail_once) = true;
+  });
+  SyncPoint::GetInstance()->EnableProcessing();
+
+  // Select and stop on retry
+  auto result = conn.Fetch("SELECT * FROM t");
+  ASSERT_NOK(result);
+  ASSERT_STR_CONTAINS(result.status().ToString(), "CommonResponseCheck test runtime error");
+
+  yb::SyncPoint::GetInstance()->DisableProcessing();
+  yb::SyncPoint::GetInstance()->ClearAllCallBacks();
+}
+#endif // NDEBUG
+
 TEST_F(PgTabletSplitTest, SplitSequencesDataTable) {
   // Test that tablet splitting is blocked on system_postgres.sequences_data table
   auto conn = ASSERT_RESULT(Connect());
