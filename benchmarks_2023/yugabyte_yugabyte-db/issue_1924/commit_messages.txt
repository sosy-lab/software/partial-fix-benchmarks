[#2014] Replace BOOST_SCOPE_EXIT with small utility

Summary:
While editing files with `BOOST_SCOPE_EXIT` CLion constantly freezes for couple of seconds.
Making it very inconvenient to work with such files.

Hopefully since C++11 the same behaviour could be provided using small utility class and lambda.

Implemented and migrated from `BOOST_SCOPE_EXIT` to it.

Test Plan: Jenkins

Reviewers: timur, dmitry

Reviewed By: dmitry

Subscribers: ybase, bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D7044
[YSQL] [FORMAT CHANGE] #1924 Change internal representation for TEXT from BINARY to STRING

Summary:
Current when writing to DocDB, we store Postgres::TEXT as BINARY.
As result, we'd be sorting TEXT in BINARY ORDER, which is not correct even though the order might be correct in most cases.

Test Plan: TestPgRegressTypesString

Reviewers: mihnea

Reviewed By: mihnea

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D7038
Revert "[YSQL] [FORMAT CHANGE] #1924 Change internal representation for TEXT from BINARY to STRING"

Test Plan: yb_pg_text

Differential Revision: https://phabricator.dev.yugabyte.com/D7055
[YSQL] [FORMAT CHANGE] #1924 Change internal representation for TEXT from BINARY to STRING

Summary:
Current when writing to DocDB, we store Postgres::TEXT as BINARY.
As result, we'd be sorting TEXT in BINARY ORDER, which might not correct for some collation settings even though the order might be correct in most cases.

Test Plan:
TestPgRegressTypesString
Test for index on char-based types.

Reviewers: jason, mihnea

Reviewed By: mihnea

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D7062
