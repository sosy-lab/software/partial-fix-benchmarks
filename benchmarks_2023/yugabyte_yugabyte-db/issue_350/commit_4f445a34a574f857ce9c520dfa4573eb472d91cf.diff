diff --git a/src/yb/consensus/consensus_peers-test.cc b/src/yb/consensus/consensus_peers-test.cc
index 7db9cc082329..c9bb46fcfc67 100644
--- a/src/yb/consensus/consensus_peers-test.cc
+++ b/src/yb/consensus/consensus_peers-test.cc
@@ -62,6 +62,7 @@ namespace consensus {
 using log::Log;
 using log::LogOptions;
 using log::LogAnchorRegistry;
+using std::shared_ptr;
 using std::unique_ptr;
 
 const char* kTableId = "test-peers-table";
@@ -118,7 +119,7 @@ class ConsensusPeersTest : public YBTest {
 
   DelayablePeerProxy<NoOpTestPeerProxy>* NewRemotePeer(
       const string& peer_name,
-      std::unique_ptr<Peer>* peer) {
+      std::shared_ptr<Peer>* peer) {
     RaftPeerPB peer_pb;
     peer_pb.set_permanent_uuid(peer_name);
     auto proxy_ptr = new DelayablePeerProxy<NoOpTestPeerProxy>(
@@ -164,7 +165,6 @@ class ConsensusPeersTest : public YBTest {
   scoped_refptr<server::Clock> clock_;
 };
 
-
 // Tests that a remote peer is correctly built and tracked
 // by the message queue.
 // After the operations are considered done the proxy (which
@@ -174,10 +174,10 @@ TEST_F(ConsensusPeersTest, TestRemotePeer) {
   // We use a majority size of 2 since we make one fake remote peer
   // in addition to our real local log.
 
-  std::unique_ptr<Peer> remote_peer;
+  std::shared_ptr<Peer> remote_peer;
   DelayablePeerProxy<NoOpTestPeerProxy>* proxy = NewRemotePeer(kFollowerUuid, &remote_peer);
 
-  // Append a bunch of messages to the queue
+  // Append a bunch of messages to the queue.
   AppendReplicateMessagesToQueue(message_queue_.get(), clock_, 1, 20);
 
   // The above append ends up appending messages in term 2, so we update the peer's term to match.
@@ -194,11 +194,11 @@ TEST_F(ConsensusPeersTest, TestRemotePeer) {
 }
 
 TEST_F(ConsensusPeersTest, TestLocalAppendAndRemotePeerDelay) {
-  // Create a set of remote peers
-  std::unique_ptr<Peer> remote_peer1;
+  // Create a set of remote peers.
+  std::shared_ptr<Peer> remote_peer1;
   NewRemotePeer("peer-1", &remote_peer1);
 
-  std::unique_ptr<Peer> remote_peer2;
+  std::shared_ptr<Peer> remote_peer2;
   DelayablePeerProxy<NoOpTestPeerProxy>* remote_peer2_proxy =
       NewRemotePeer("peer-2", &remote_peer2);
 
@@ -219,7 +219,7 @@ TEST_F(ConsensusPeersTest, TestLocalAppendAndRemotePeerDelay) {
   ASSERT_OK(remote_peer1->SignalRequest(RequestTriggerMode::kNonEmptyOnly));
   ASSERT_OK(remote_peer2->SignalRequest(RequestTriggerMode::kNonEmptyOnly));
 
-  // Replication should time out, because the time
+  // Replication should time out, because the time.
   WaitForMajorityReplicatedIndex(first.index());
   const auto elapsed_time = MonoTime::Now() - start_time;
   LOG(INFO) << "Replication elapsed time: " << elapsed_time;
@@ -229,12 +229,12 @@ TEST_F(ConsensusPeersTest, TestLocalAppendAndRemotePeerDelay) {
 }
 
 TEST_F(ConsensusPeersTest, TestRemotePeers) {
-  // Create a set of remote peers
-  std::unique_ptr<Peer> remote_peer1;
+  // Create a set of remote peers.
+  std::shared_ptr<Peer> remote_peer1;
   DelayablePeerProxy<NoOpTestPeerProxy>* remote_peer1_proxy =
       NewRemotePeer("peer-1", &remote_peer1);
 
-  std::unique_ptr<Peer> remote_peer2;
+  std::shared_ptr<Peer> remote_peer2;
   DelayablePeerProxy<NoOpTestPeerProxy>* remote_peer2_proxy =
       NewRemotePeer("peer-2", &remote_peer2);
 
@@ -271,7 +271,7 @@ TEST_F(ConsensusPeersTest, TestRemotePeers) {
     std::this_thread::sleep_for(1ms);
   }
 
-  // Now append another message to the queue
+  // Now append another message to the queue.
   AppendReplicateMessagesToQueue(message_queue_.get(), clock_, 2, 1);
 
   // We should not see it replicated, even after 10ms,
diff --git a/src/yb/consensus/consensus_peers.cc b/src/yb/consensus/consensus_peers.cc
index 7d5c6aa70f39..b9d16ca43a50 100644
--- a/src/yb/consensus/consensus_peers.cc
+++ b/src/yb/consensus/consensus_peers.cc
@@ -114,12 +114,10 @@ Peer::Peer(
       peer_pb_(peer_pb),
       proxy_(std::move(proxy)),
       queue_(queue),
-      sem_(1),
       heartbeater_(
           peer_pb.permanent_uuid(), MonoDelta::FromMilliseconds(FLAGS_raft_heartbeat_interval_ms),
           std::bind(&Peer::SignalRequest, this, RequestTriggerMode::kAlwaysSend)),
       raft_pool_token_(raft_pool_token),
-      state_(kPeerCreated),
       consensus_(consensus) {}
 
 void Peer::SetTermForTest(int term) {
@@ -130,58 +128,47 @@ Status Peer::Init() {
   std::lock_guard<simple_spinlock> lock(peer_lock_);
   queue_->TrackPeer(peer_pb_.permanent_uuid());
   RETURN_NOT_OK(heartbeater_.Start());
-  state_ = kPeerStarted;
   return Status::OK();
 }
 
 Status Peer::SignalRequest(RequestTriggerMode trigger_mode) {
-  // If the peer is currently sending, return Status::OK().
-  // If there are new requests in the queue we'll get them on ProcessResponse().
-  std::unique_lock<Semaphore> lock(sem_, std::try_to_lock);
-  if (!lock.owns_lock()) {
-    return Status::OK();
+  std::lock_guard<simple_spinlock> l(peer_lock_);
+
+  if (PREDICT_FALSE(closed_)) {
+    return STATUS(IllegalState, "Peer was closed.");
   }
 
-  {
-    std::lock_guard<simple_spinlock> l(peer_lock_);
+  RETURN_NOT_OK(raft_pool_token_->SubmitFunc([=, s_this = shared_from_this()]() {
+    s_this->SendNextRequest(trigger_mode);
+  }));
 
-    if (PREDICT_FALSE(state_ == kPeerClosed)) {
-      return STATUS(IllegalState, "Peer was closed.");
-    }
+  return Status::OK();
+}
 
-    // For the first request sent by the peer, we send it even if the queue is empty, which it will
-    // always appear to be for the first request, since this is the negotiation round.
-    if (PREDICT_FALSE(state_ == kPeerStarted)) {
-      trigger_mode = RequestTriggerMode::kAlwaysSend;
-      state_ = kPeerRunning;
-    }
-    DCHECK_EQ(state_, kPeerRunning);
-
-    // If our last request generated an error, and this is not a normal heartbeat request (i.e.
-    // we're not forcing a request even if the queue is empty, unlike we do during heartbeats),
-    // then don't send the "per-RPC" request. Instead, we'll wait for the heartbeat.
-    //
-    // TODO: we could consider looking at the number of consecutive failed attempts, and instead of
-    // ignoring the signal, ask the heartbeater to "expedite" the next heartbeat in order to achieve
-    // something like exponential backoff after an error. As it is implemented today, any transient
-    // error will result in a latency blip as long as the heartbeat period.
-    if (failed_attempts_ > 0 && trigger_mode == RequestTriggerMode::kNonEmptyOnly) {
-      return Status::OK();
-    }
+void Peer::SendNextRequest(RequestTriggerMode trigger_mode) {
+  std::unique_lock<simple_spinlock> l(peer_lock_);
+  if (PREDICT_FALSE(closed_)) {
+    return;
   }
 
-  auto status = raft_pool_token_->SubmitClosure(
-      Bind(&Peer::SendNextRequest, Unretained(this), trigger_mode));
-  if (status.ok()) {
-    lock.release();
+  // Only allow one request at a time.
+  if (request_pending_) {
+    return;
   }
-  return status;
-}
 
-void Peer::SendNextRequest(RequestTriggerMode trigger_mode) {
-  DCHECK_EQ(sem_.GetValue(), 0) << "Cannot send request";
-
-  std::unique_lock<Semaphore> lock(sem_, std::adopt_lock);
+  // If our last request generated an error, and this is not a normal
+  // heartbeat request, then don't send the "per-op" request. Instead,
+  // we'll wait for the heartbeat.
+  //
+  // TODO(todd): we could consider looking at the number of consecutive failed
+  // attempts, and instead of ignoring the signal, ask the heartbeater
+  // to "expedite" the next heartbeat in order to achieve something like
+  // exponential backoff after an error. As it is implemented today, any
+  // transient error will result in a latency blip as long as the heartbeat
+  // period.
+  if (failed_attempts_ > 0 && trigger_mode != RequestTriggerMode::kAlwaysSend) {
+    return;
+  }
 
   // The peer has no pending request nor is sending: send the request.
   bool needs_remote_bootstrap = false;
@@ -201,13 +188,22 @@ void Peer::SendNextRequest(RequestTriggerMode trigger_mode) {
   }
 
   if (PREDICT_FALSE(needs_remote_bootstrap)) {
-    Status s = SendRemoteBootstrapRequest();
+    Status s = PrepareRemoteBootstrapRequest();
     if (!s.ok()) {
       LOG_WITH_PREFIX_UNLOCKED(WARNING) << "Unable to generate remote bootstrap request for peer: "
                                         << s.ToString();
-    } else {
-      lock.release();
     }
+
+    controller_.Reset();
+    request_pending_ = true;
+    l.unlock();
+    // Capture a shared_ptr reference into the RPC callback so that we're guaranteed
+    // that this object outlives the RPC.
+    proxy_->StartRemoteBootstrap(
+        &rb_request_, &rb_response_, &controller_,
+        [s_this = shared_from_this()]() {
+          s_this->ProcessRemoteBootstrapResponse();
+        });
     return;
   }
 
@@ -217,7 +213,9 @@ void Peer::SendNextRequest(RequestTriggerMode trigger_mode) {
       (member_type == RaftPeerPB::PRE_VOTER || member_type == RaftPeerPB::PRE_OBSERVER)) {
     if (PREDICT_TRUE(consensus_)) {
       auto uuid = peer_pb_.permanent_uuid();
-      lock.unlock();
+      request_pending_ = false;
+      l.unlock();
+
       consensus::ChangeConfigRequestPB req;
       consensus::ChangeConfigResponsePB resp;
 
@@ -233,7 +231,8 @@ void Peer::SendNextRequest(RequestTriggerMode trigger_mode) {
       auto status = consensus_->ChangeConfig(req, &DoNothingStatusCB, &error_code);
       if (PREDICT_FALSE(!status.ok())) {
         LOG(WARNING) << "Unable to change role for peer " << uuid << ": " << status.ToString(false);
-        // Since we released the semaphore, we need to call SignalRequest again to send a message
+        // Since we decided to send a ChangeConfig instead of sending the next request, call
+        // SignalRequest so the next op (if there is one) can be processed.
         status = SignalRequest(RequestTriggerMode::kAlwaysSend);
         if (PREDICT_FALSE(!status.ok())) {
           LOG(WARNING) << "Unexpected error when trying to send request: "
@@ -264,17 +263,25 @@ void Peer::SendNextRequest(RequestTriggerMode trigger_mode) {
   MAYBE_FAULT(FLAGS_fault_crash_on_leader_request_fraction);
   controller_.Reset();
 
-  lock.release();
+  request_pending_ = true;
+  l.unlock();
+  // Capture a shared_ptr reference into the RPC callback so that we're guaranteed
+  // that this object outlives the RPC.
   proxy_->UpdateAsync(&request_, trigger_mode, &response_, &controller_,
-                      std::bind(&Peer::ProcessResponse, this));
+                      [s_this = shared_from_this()]() {
+                        s_this->ProcessResponse();
+                      });
 }
 
 void Peer::ProcessResponse() {
   // Note: This method runs on the reactor thread.
 
-  DCHECK_EQ(sem_.GetValue(), 0) << "Got a response when nothing was pending";
+  std::unique_lock<simple_spinlock> lock(peer_lock_);
+  if (closed_) {
+    return;
+  }
 
-  std::unique_lock<Semaphore> lock(sem_, std::adopt_lock);
+  DCHECK(request_pending_) << "Got a response when nothing was pending";
 
   if (!controller_.status().ok()) {
     if (controller_.status().IsRemoteError()) {
@@ -317,30 +324,36 @@ void Peer::ProcessResponse() {
   // The queue's handling of the peer response may generate IO (reads against the WAL) and
   // SendNextRequest() may do the same thing. So we run the rest of the response handling logic on
   // our thread pool and not on the reactor thread.
-  Status s = raft_pool_token_->SubmitClosure(Bind(&Peer::DoProcessResponse, Unretained(this)));
+  Status s = raft_pool_token_->SubmitFunc([s_this = shared_from_this()]() {
+    s_this->DoProcessResponse();
+  });
   if (PREDICT_FALSE(!s.ok())) {
     LOG_WITH_PREFIX_UNLOCKED(WARNING) << "Unable to process peer response: " << s.ToString()
         << ": " << response_.ShortDebugString();
-  } else {
-    lock.release();
+    request_pending_ = false;
   }
 }
 
 void Peer::DoProcessResponse() {
-  DCHECK_EQ(0, sem_.GetValue());
-  std::unique_lock<Semaphore> lock(sem_, std::adopt_lock);
+  VLOG_WITH_PREFIX_UNLOCKED(2) << "Response from peer " << peer_pb().permanent_uuid() << ": "
+                               << response_.ShortDebugString();
 
-  failed_attempts_ = 0;
   bool more_pending = false;
   queue_->ResponseFromPeer(peer_pb_.permanent_uuid(), response_, &more_pending);
 
-  if (more_pending && state_.load(std::memory_order_acquire) != kPeerClosed) {
-    lock.release();
+  {
+    std::unique_lock<simple_spinlock> lock(peer_lock_);
+    DCHECK(request_pending_) << "Got a response when nothing was pending";
+    failed_attempts_ = 0;
+    request_pending_ = false;
+  }
+
+  if (more_pending) {
     SendNextRequest(RequestTriggerMode::kAlwaysSend);
   }
 }
 
-Status Peer::SendRemoteBootstrapRequest() {
+Status Peer::PrepareRemoteBootstrapRequest() {
   if (!FLAGS_enable_remote_bootstrap) {
     failed_attempts_++;
     return STATUS(NotSupported, "remote bootstrap is disabled");
@@ -348,30 +361,37 @@ Status Peer::SendRemoteBootstrapRequest() {
 
   LOG_WITH_PREFIX_UNLOCKED(INFO) << "Sending request to remotely bootstrap";
   RETURN_NOT_OK(queue_->GetRemoteBootstrapRequestForPeer(peer_pb_.permanent_uuid(), &rb_request_));
-  controller_.Reset();
-  proxy_->StartRemoteBootstrap(
-      &rb_request_, &rb_response_, &controller_,
-      std::bind(&Peer::ProcessRemoteBootstrapResponse, this));
   return Status::OK();
 }
 
 void Peer::ProcessRemoteBootstrapResponse() {
-  std::unique_lock<Semaphore> lock(sem_, std::adopt_lock);
+  // If the peer is already closed return.
+  {
+    std::unique_lock<simple_spinlock> lock(peer_lock_);
+    if (closed_) {
+      return;
+    }
+    DCHECK(request_pending_) << "Got a remote bootstrap response when nothing was pending";
+    request_pending_ = false;
+  }
 
   // We treat remote bootstrap as fire-and-forget.
-  if (rb_response_.has_error()) {
+  if (controller_.status().ok() && rb_response_.has_error()) {
     LOG_WITH_PREFIX_UNLOCKED(WARNING) << "Unable to begin remote bootstrap on peer: "
                                       << rb_response_.ShortDebugString();
   }
 }
 
 void Peer::ProcessResponseError(const Status& status) {
-  DCHECK_EQ(0, sem_.GetValue());
+  DCHECK(request_pending_) << "Got an error response when nothing was pending";
+
   failed_attempts_++;
   LOG_WITH_PREFIX_UNLOCKED(WARNING) << "Couldn't send request to peer " << peer_pb_.permanent_uuid()
       << " for tablet " << tablet_id_
       << " Status: " << status.ToString() << ". Retrying in the next heartbeat period."
-      << " Already tried " << failed_attempts_ << " times. State: " << state_;
+      << " Already tried " << failed_attempts_ << " times";
+
+  request_pending_ = false;
 }
 
 string Peer::LogPrefixUnlocked() const {
@@ -386,24 +406,20 @@ void Peer::Close() {
   // If the peer is already closed return.
   {
     std::lock_guard<simple_spinlock> lock(peer_lock_);
-    if (state_ == kPeerClosed) return;
-    DCHECK(state_ == kPeerRunning || state_ == kPeerStarted) << "Unexpected state: " << state_;
-    state_ = kPeerClosed;
+    if (closed_) return;
+    closed_ = true;
   }
+
   LOG_WITH_PREFIX_UNLOCKED(INFO) << "Closing peer: " << peer_pb_.permanent_uuid();
 
-  // Acquire the semaphore to wait for any concurrent request to finish.  They will see the state_
-  // == kPeerClosed and not start any new requests, but we can't currently cancel the already-sent
-  // ones. (see KUDU-699)
-  std::lock_guard<Semaphore> l(sem_);
   queue_->UntrackPeer(peer_pb_.permanent_uuid());
-  // We don't own the ops (the queue does).
-  request_.mutable_ops()->ExtractSubrange(0, request_.ops_size(), /* elements */ nullptr);
   replicate_msg_refs_.clear();
 }
 
 Peer::~Peer() {
   Close();
+  // We don't own the ops (the queue does).
+  request_.mutable_ops()->ExtractSubrange(0, request_.ops_size(), /* elements */ nullptr);
 }
 
 RpcPeerProxy::RpcPeerProxy(HostPort hostport,
diff --git a/src/yb/consensus/consensus_peers.h b/src/yb/consensus/consensus_peers.h
index 50c4a56eb342..e27581b56bb0 100644
--- a/src/yb/consensus/consensus_peers.h
+++ b/src/yb/consensus/consensus_peers.h
@@ -114,11 +114,11 @@ namespace consensus {
 //  SignalRequest()                    return
 //
 class Peer;
-typedef std::unique_ptr<Peer> PeerPtr;
+typedef std::shared_ptr<Peer> PeerPtr;
 
-class Peer {
+class Peer : public std::enable_shared_from_this<Peer> {
  public:
-  // Initializes a peer and get its status.
+  // Initializes a peer and start sending periodic heartbeats.
   CHECKED_STATUS Init();
 
   // Signals that this peer has a new request to replicate/store.
@@ -180,12 +180,11 @@ class Peer {
   // Run on 'raft_pool_token'. Does response handling that requires IO or may block.
   void DoProcessResponse();
 
-  // Fetch the desired remote bootstrap request from the queue and send it to the peer. The callback
-  // goes to ProcessRemoteBootstrapResponse().
+  // Fetch the desired remote bootstrap request from the queue and set up rb_request_ appropriately.
   //
   // Returns a bad Status if remote bootstrap is disabled, or if the request cannot be generated for
   // some reason.
-  CHECKED_STATUS SendRemoteBootstrapRequest();
+  CHECKED_STATUS PrepareRemoteBootstrapRequest();
 
   // Handle RPC callback from initiating remote bootstrap.
   void ProcessRemoteBootstrapResponse();
@@ -223,10 +222,6 @@ class Peer {
 
   rpc::RpcController controller_;
 
-  // Held if there is an outstanding request.  This is used in order to ensure that we only have a
-  // single request outstanding at a time, and to wait for the outstanding requests at Close().
-  Semaphore sem_;
-
   // Heartbeater for remote peer implementations.  This will send status only requests to the remote
   // peers whenever we go more than 'FLAGS_raft_heartbeat_interval_ms' without sending actual data.
   ResettableHeartbeater heartbeater_;
@@ -244,8 +239,11 @@ class Peer {
   // Lock that protects Peer state changes, initialization, etc.  Must not try to acquire sem_ while
   // holding peer_lock_.
   mutable simple_spinlock peer_lock_;
-  std::atomic<State> state_;
   Consensus* consensus_ = nullptr;
+
+  bool request_pending_ = false;
+  bool closed_ = false;
+  bool has_sent_first_request_ = false;
 };
 
 // A proxy to another peer. Usually a thin wrapper around an rpc proxy but can be replaced for
@@ -307,7 +305,7 @@ class PeerProxyFactory {
   }
 };
 
-// PeerProxy implementation that does RPC calls
+// PeerProxy implementation that does RPC calls.
 class RpcPeerProxy : public PeerProxy {
  public:
   RpcPeerProxy(HostPort hostport, ConsensusServiceProxyPtr consensus_proxy);
@@ -345,7 +343,7 @@ class RpcPeerProxy : public PeerProxy {
   ConsensusServiceProxyPtr consensus_proxy_;
 };
 
-// PeerProxyFactory implementation that generates RPCPeerProxies
+// PeerProxyFactory implementation that generates RPCPeerProxies.
 class RpcPeerProxyFactory : public PeerProxyFactory {
  public:
   RpcPeerProxyFactory(std::shared_ptr<rpc::Messenger> messenger, rpc::ProxyCache* proxy_cache);
diff --git a/src/yb/consensus/peer_manager.cc b/src/yb/consensus/peer_manager.cc
index 46ddbb5ea143..ffd5a26d611b 100644
--- a/src/yb/consensus/peer_manager.cc
+++ b/src/yb/consensus/peer_manager.cc
@@ -69,7 +69,7 @@ void PeerManager::UpdateRaftConfig(const RaftConfigPB& config) {
   VLOG(1) << "Updating peers from new config: " << config.ShortDebugString();
 
   std::lock_guard<simple_spinlock> lock(lock_);
-  // Create new peers
+  // Create new peers.
   for (const RaftPeerPB& peer_pb : config.peers()) {
     if (peers_.find(peer_pb.permanent_uuid()) != peers_.end()) {
       continue;
@@ -100,7 +100,7 @@ void PeerManager::SignalRequest(RequestTriggerMode trigger_mode) {
       LOG(WARNING) << GetLogPrefix()
                    << "Peer was closed, removing from peers. Peer: "
                    << (*iter).second->peer_pb().ShortDebugString();
-      iter = peers_.erase(iter);
+      iter = peers_.erase(iter++);
     } else {
       iter++;
     }
@@ -109,7 +109,7 @@ void PeerManager::SignalRequest(RequestTriggerMode trigger_mode) {
 
 void PeerManager::Close() {
   std::lock_guard<simple_spinlock> lock(lock_);
-  for (const PeersMap::value_type& entry : peers_) {
+  for (const auto& entry : peers_) {
     entry.second->Close();
   }
   peers_.clear();
diff --git a/src/yb/consensus/peer_manager.h b/src/yb/consensus/peer_manager.h
index 3c5744b8e761..2e268b413d2b 100644
--- a/src/yb/consensus/peer_manager.h
+++ b/src/yb/consensus/peer_manager.h
@@ -90,7 +90,7 @@ class PeerManager {
  private:
   std::string GetLogPrefix() const;
 
-  typedef std::unordered_map<std::string, std::unique_ptr<Peer>> PeersMap;
+  typedef std::unordered_map<std::string, std::shared_ptr<Peer>> PeersMap;
   const std::string tablet_id_;
   const std::string local_uuid_;
   PeerProxyFactory* peer_proxy_factory_;
diff --git a/src/yb/integration-tests/cluster_itest_util.cc b/src/yb/integration-tests/cluster_itest_util.cc
index 0d208aba800b..933748cfc560 100644
--- a/src/yb/integration-tests/cluster_itest_util.cc
+++ b/src/yb/integration-tests/cluster_itest_util.cc
@@ -348,6 +348,53 @@ Status WaitUntilAllReplicasHaveOp(const int64_t log_index,
                                               log_index, passed.ToString()));
 }
 
+Status WaitUntilNumberOfAliveTServersEqual(int n_tservers,
+                                           MasterServiceProxy* master_proxy,
+                                           const MonoDelta& timeout) {
+
+  master::ListTabletServersRequestPB req;
+  master::ListTabletServersResponsePB resp;
+  rpc::RpcController controller;
+  controller.set_timeout(timeout);
+
+  // The field primary_only means only tservers that are alive (tservers that have sent at least on
+  // heartbeat in the last FLAG_tserver_unresponsive_timeout_ms milliseconds.)
+  req.set_primary_only(true);
+
+  MonoTime start = MonoTime::Now();
+  MonoDelta passed = MonoDelta::FromMilliseconds(0);
+  while (true) {
+    Status s = master_proxy->ListTabletServers(req, &resp, &controller);
+
+    if (s.ok() &&
+        controller.status().ok() &&
+        !resp.has_error()) {
+      if (resp.servers_size() == n_tservers) {
+        passed = MonoTime::Now().GetDeltaSince(start);
+        return Status::OK();
+      }
+    } else {
+      string error;
+      if (!s.ok()) {
+        error = s.ToString();
+      } else if (!controller.status().ok()) {
+        error = controller.status().ToString();
+      } else {
+        error = resp.error().ShortDebugString();
+      }
+      LOG(WARNING) << "Got error getting list of tablet servers: " << error;
+    }
+    passed = MonoTime::Now().GetDeltaSince(start);
+    if (passed.MoreThan(timeout)) {
+      break;
+    }
+    SleepFor(MonoDelta::FromMilliseconds(50));
+    controller.Reset();
+  }
+  return STATUS(TimedOut, Substitute("Number of alive tservers not equal to $0 after $1 ms. ",
+                                     n_tservers, timeout.ToMilliseconds()));
+}
+
 Status CreateTabletServerMap(MasterServiceProxy* master_proxy,
                              rpc::ProxyCache* proxy_cache,
                              TabletServerMap* ts_map) {
diff --git a/src/yb/integration-tests/cluster_itest_util.h b/src/yb/integration-tests/cluster_itest_util.h
index 992f8662673c..ccc3b1d9d282 100644
--- a/src/yb/integration-tests/cluster_itest_util.h
+++ b/src/yb/integration-tests/cluster_itest_util.h
@@ -182,6 +182,13 @@ Status WaitUntilAllReplicasHaveOp(const int64_t log_index,
                                   const std::vector<TServerDetails*>& replicas,
                                   const MonoDelta& timeout);
 
+// Wait until the number of alive tservers is equal to n_tservers. An alive tserver is a tserver
+// that has heartbeated the master at least once in the last FLAGS_raft_heartbeat_interval_ms
+// milliseconds.
+Status WaitUntilNumberOfAliveTServersEqual(int n_tservers,
+                                           master::MasterServiceProxy* master_proxy,
+                                           const MonoDelta& timeout);
+
 // Get the consensus state from the given replica.
 Status GetConsensusState(const TServerDetails* replica,
                          const TabletId& tablet_id,
diff --git a/src/yb/integration-tests/raft_consensus-itest.cc b/src/yb/integration-tests/raft_consensus-itest.cc
index 56ff21b3333f..39a6dad6e6b5 100644
--- a/src/yb/integration-tests/raft_consensus-itest.cc
+++ b/src/yb/integration-tests/raft_consensus-itest.cc
@@ -115,6 +115,7 @@ using itest::LeaderStepDown;
 using itest::TabletServerMapUnowned;
 using itest::RemoveServer;
 using itest::StartElection;
+using itest::WaitUntilNumberOfAliveTServersEqual;
 using itest::WaitUntilLeader;
 using itest::WriteSimpleTestRow;
 using master::GetTabletLocationsRequestPB;
@@ -170,7 +171,7 @@ class RaftConsensusITest : public TabletServerIntegrationTestBase {
       ++id;
     }
 
-    // Send the call
+    // Send the call.
     {
       SCOPED_TRACE(req.DebugString());
       ASSERT_OK(replica_proxy->Read(req, &resp, &rpc));
@@ -694,7 +695,7 @@ void RaftConsensusITest::Write128KOpsToLeader(int num_writes) {
   rpc.set_timeout(MonoDelta::FromMilliseconds(10000));
   int key = 0;
 
-  // generate a 128Kb dummy payload
+  // generate a 128Kb dummy payload.
   string test_payload(128 * 1024, '0');
   for (int i = 0; i < num_writes; i++) {
     rpc.Reset();
@@ -720,7 +721,7 @@ TEST_F(RaftConsensusITest, TestCatchupAfterOpsEvicted) {
   ASSERT_TRUE(replica != nullptr);
   ExternalTabletServer* replica_ets = cluster_->tablet_server_by_uuid(replica->uuid());
 
-  // Pause a replica
+  // Pause a replica.
   ASSERT_OK(replica_ets->Pause());
   LOG(INFO)<< "Paused one of the replicas, starting to write.";
 
@@ -1377,7 +1378,7 @@ TEST_F(RaftConsensusITest, TestAutomaticLeaderElection) {
     }
   }
 
-  // Restart every node that was killed, and wait for the nodes to converge
+  // Restart every node that was killed, and wait for the nodes to converge.
   for (TServerDetails* killed_node : killed_leaders) {
     CHECK_OK(cluster_->tablet_server_by_uuid(killed_node->uuid())->Restart());
   }
@@ -1694,6 +1695,43 @@ TEST_F(RaftConsensusITest, TestLeaderStepDown) {
                                   << s.ToString();
 }
 
+// Test for #350: sets the consensus RPC timeout to be long, and freezes both followers before
+// asking the leader to step down. Prior to fixing #350, the step-down process would block
+// until the pending requests timed out.
+TEST_F(RaftConsensusITest, TestStepDownWithSlowFollower) {
+  vector<string> ts_flags = {
+      "--enable_leader_failure_detection=false",
+      // Bump up the RPC timeout, so that we can verify that the stepdown responds
+      // quickly even when an outbound request is hung.
+      "--consensus_rpc_timeout_ms=15000",
+      // Make heartbeats more often so we can detect dead tservers faster.
+      "--raft_heartbeat_interval_ms=10",
+      // Set it high enough so that the election rpcs don't time out.
+      "--leader_failure_max_missed_heartbeat_periods=100"
+  };
+  vector<string> master_flags = {
+      "--catalog_manager_wait_for_new_tablets_to_elect_leader=false",
+      "--tserver_unresponsive_timeout_ms=5000"
+  };
+  BuildAndStart(ts_flags, master_flags);
+
+  vector<TServerDetails *> tservers = TServerDetailsVector(tablet_servers_);
+  ASSERT_OK(StartElection(tservers[0], tablet_id_, MonoDelta::FromSeconds(10)));
+  ASSERT_OK(WaitUntilLeader(tservers[0], tablet_id_, MonoDelta::FromSeconds(10)));
+
+  // Stop both followers.
+  for (int i = 1; i < 3; i++) {
+    ASSERT_OK(cluster_->tablet_server_by_uuid(tservers[i]->uuid())->Pause());
+  }
+
+  // Wait until the paused tservers have stopped heartbeating.
+  ASSERT_OK(WaitUntilNumberOfAliveTServersEqual(1, cluster_->master_proxy().get(),
+                                                MonoDelta::FromSeconds(20)));
+
+  // Step down should respond quickly despite the hung requests.
+  ASSERT_OK(LeaderStepDown(tservers[0], tablet_id_, nullptr, MonoDelta::FromSeconds(3)));
+}
+
 void RaftConsensusITest::AssertMajorityRequiredForElectionsAndWrites(
     const TabletServerMapUnowned& tablet_servers, const string& leader_uuid) {
 
