diff --git a/src/yb/common/CMakeLists.txt b/src/yb/common/CMakeLists.txt
index 2611978cb3df..691c796f8043 100644
--- a/src/yb/common/CMakeLists.txt
+++ b/src/yb/common/CMakeLists.txt
@@ -178,6 +178,7 @@ target_link_libraries(yb_common_test_util
 set(YB_TEST_LINK_LIBS yb_common yb_partition ${YB_MIN_TEST_LIBS})
 ADD_YB_TEST(id_mapping-test)
 ADD_YB_TEST(jsonb-test)
+ADD_YB_TEST(ql_table_row-test)
 ADD_YB_TEST(partial_row-test)
 ADD_YB_TEST(partition-test)
 ADD_YB_TEST(row_key-util-test)
diff --git a/src/yb/common/ql_expr.cc b/src/yb/common/ql_expr.cc
index 476b3824d377..c83fe7b22ffc 100644
--- a/src/yb/common/ql_expr.cc
+++ b/src/yb/common/ql_expr.cc
@@ -10,6 +10,12 @@
 
 namespace yb {
 
+namespace {
+
+constexpr size_t kInvalidIndex = std::numeric_limits<size_t>::max();
+
+}
+
 bfql::TSOpcode QLExprExecutor::GetTSWriteInstruction(const QLExpressionPB& ql_expr) const {
   // "kSubDocInsert" instructs the tablet server to insert a new value or replace an existing value.
   if (ql_expr.has_tscall()) {
@@ -580,13 +586,52 @@ CHECKED_STATUS QLExprExecutor::EvalCondition(const PgsqlConditionPB& condition,
 
 //--------------------------------------------------------------------------------------------------
 
-const QLValuePB* QLTableRow::GetColumn(ColumnIdRep col_id) const {
-  const auto& col_iter = col_map_.find(col_id);
-  if (col_iter == col_map_.end()) {
+const QLTableRow& QLTableRow::empty_row() {
+  static QLTableRow empty_row;
+  return empty_row;
+}
+
+size_t QLTableRow::ColumnCount() const {
+  size_t result = 0;
+  for (auto i : assigned_) {
+    result += i;
+  }
+  return result;
+}
+
+void QLTableRow::Clear() {
+  if (num_assigned_ == 0) {
+    return;
+  }
+
+  memset(assigned_.data(), 0, assigned_.size());
+  num_assigned_ = 0;
+}
+
+size_t QLTableRow::ColumnIndex(ColumnIdRep col_id) const {
+  if (col_id < kFirstNonPreallocatedColumnId && col_id >= kFirstColumnIdRep) {
+    return col_id - kFirstColumnIdRep;
+  }
+  const auto& col_iter = column_id_to_index_.find(col_id);
+  if (col_iter == column_id_to_index_.end()) {
+    return kInvalidIndex;
+  }
+
+  return col_iter->second;
+}
+
+const QLTableColumn* QLTableRow::FindColumn(ColumnIdRep col_id) const {
+  size_t index = ColumnIndex(col_id);
+  if (index == kInvalidIndex || index >= assigned_.size() || !assigned_[index]) {
     return nullptr;
   }
 
-  return &col_iter->second.value;
+  return &values_[index];
+}
+
+const QLValuePB* QLTableRow::GetColumn(ColumnIdRep col_id) const {
+  const auto* column = FindColumn(col_id);
+  return column ? &column->value : nullptr;
 }
 
 CHECKED_STATUS QLTableRow::ReadColumn(ColumnIdRep col_id, QLExprResultWriter result_writer) const {
@@ -603,23 +648,25 @@ CHECKED_STATUS QLTableRow::ReadColumn(ColumnIdRep col_id, QLExprResultWriter res
 CHECKED_STATUS QLTableRow::ReadSubscriptedColumn(const QLSubscriptedColPB& subcol,
                                                  const QLValuePB& index_arg,
                                                  QLExprResultWriter result_writer) const {
-  const auto& col_iter = col_map_.find(subcol.column_id());
-  if (col_iter == col_map_.end()) {
-    // Not exists.
+  const auto* value = GetColumn(subcol.column_id());
+  if (!value) {
+    // Does not exist.
     result_writer.SetNull();
     return Status::OK();
-  } else if (col_iter->second.value.has_map_value()) {
+  }
+
+  if (value->has_map_value()) {
     // map['key']
-    auto& map = col_iter->second.value.map_value();
+    auto& map = value->map_value();
     for (int i = 0; i < map.keys_size(); i++) {
       if (map.keys(i) == index_arg) {
         result_writer.SetExisting(&map.values(i));
         return Status::OK();
       }
     }
-  } else if (col_iter->second.value.has_list_value()) {
+  } else if (value->has_list_value()) {
     // list[index]
-    auto& list = col_iter->second.value.list_value();
+    auto& list = value->list_value();
     if (index_arg.has_int32_value()) {
       int list_index = index_arg.int32_value();
       if (list_index >= 0 && list_index < list.elems_size()) {
@@ -633,86 +680,162 @@ CHECKED_STATUS QLTableRow::ReadSubscriptedColumn(const QLSubscriptedColPB& subco
   return Status::OK();
 }
 
-CHECKED_STATUS QLTableRow::GetTTL(ColumnIdRep col_id, int64_t *ttl_seconds) const {
-  const auto& col_iter = col_map_.find(col_id);
-  if (col_iter == col_map_.end()) {
-    // Not exists.
+Result<const QLTableColumn&> QLTableRow::Column(ColumnIdRep col_id) const {
+  const auto* column = FindColumn(col_id);
+  if (column == nullptr) {
+    // Does not exist.
     return STATUS(InternalError, "Column unexpectedly not found in cache");
   }
-  *ttl_seconds = col_iter->second.ttl_seconds;
+
+  return *column;
+}
+
+CHECKED_STATUS QLTableRow::GetTTL(ColumnIdRep col_id, int64_t *ttl_seconds) const {
+  *ttl_seconds = VERIFY_RESULT(Column(col_id)).get().ttl_seconds;
   return Status::OK();
 }
 
 CHECKED_STATUS QLTableRow::GetWriteTime(ColumnIdRep col_id, int64_t *write_time) const {
-  const auto& col_iter = col_map_.find(col_id);
-  if (col_iter == col_map_.end()) {
-    // Not exists.
-    return STATUS(InternalError, "Column unexpectedly not found in cache");
-  }
-  DCHECK_NE(QLTableColumn::kUninitializedWriteTime, col_iter->second.write_time);
-  *write_time = col_iter->second.write_time;
+  const QLTableColumn& column = VERIFY_RESULT(Column(col_id));
+  DCHECK_NE(QLTableColumn::kUninitializedWriteTime, column.write_time) << "Column id: " << col_id;
+  *write_time = column.write_time;
   return Status::OK();
 }
 
 CHECKED_STATUS QLTableRow::GetValue(ColumnIdRep col_id, QLValue *column) const {
-  const auto& col_iter = col_map_.find(col_id);
-  if (col_iter == col_map_.end()) {
-    // Not exists.
-    return STATUS(InternalError, "Column unexpectedly not found in cache");
-  }
-  *column = std::move(col_iter->second.value);
+  *column = VERIFY_RESULT(Column(col_id)).get().value;
   return Status::OK();
 }
 
 boost::optional<const QLValuePB&> QLTableRow::GetValue(ColumnIdRep col_id) const {
-  const auto& col_iter = col_map_.find(col_id);
-  if (col_iter == col_map_.end()) {
-    return boost::none;
+  const auto* column = FindColumn(col_id);
+  if (column) {
+    return column->value;
   }
-  return col_iter->second.value;
+  return boost::none;
 }
 
 bool QLTableRow::IsColumnSpecified(ColumnIdRep col_id) const {
-  return col_map_.find(col_id) != col_map_.end();
+  size_t index = ColumnIndex(col_id);
+  if (index == kInvalidIndex) {
+    LOG(DFATAL) << "Checking whether unknown column is specified: " << col_id;
+    return false;
+  }
+  return assigned_[index];
 }
 
-void QLTableRow::ClearValue(ColumnIdRep col_id) {
-  col_map_[col_id].value.Clear();
+void QLTableRow::MarkTombstoned(ColumnIdRep col_id) {
+  AllocColumn(col_id).value.Clear();
 }
 
 bool QLTableRow::MatchColumn(ColumnIdRep col_id, const QLTableRow& source) const {
-  auto this_iter = col_map_.find(col_id);
-  auto source_iter = source.col_map_.find(col_id);
-  if (this_iter != col_map_.end() && source_iter != source.col_map_.end()) {
-    return this_iter->second.value == source_iter->second.value;
-  }
-  if (this_iter != col_map_.end() || source_iter != source.col_map_.end()) {
-    return false;
+  const auto* this_column = FindColumn(col_id);
+  const auto* source_column = source.FindColumn(col_id);
+  if (this_column && source_column) {
+    return this_column->value == source_column->value;
   }
-  return true;
+  return !this_column && !source_column;
+}
+
+QLTableColumn& QLTableRow::AppendColumn() {
+  values_.emplace_back();
+  assigned_.push_back(true);
+  ++num_assigned_;
+  return values_.back();
 }
 
 QLTableColumn& QLTableRow::AllocColumn(ColumnIdRep col_id) {
-  return col_map_[col_id];
+  size_t index = col_id;
+  if (index < kFirstNonPreallocatedColumnId && index >= kFirstColumnIdRep) {
+    index -= kFirstColumnIdRep;
+    // We are in directly mapped part. Ensure that vector is big enough.
+    if (values_.size() <= index) {
+      // We don't need reserve here, because no allocation would take place.
+      if (values_.size() < index) {
+        values_.resize(index);
+        assigned_.resize(index);
+      }
+      // This column was not yet allocated, so allocate it. Also vector has `col_id` size, so
+      // new column will be added at `col_id` position.
+      return AppendColumn();
+    }
+  } else {
+    // We are in part that is mapped using `column_id_to_index_`, so need to allocate at least
+    // part that is mapped directly, to avoid index overlapping.
+    if (values_.size() < kPreallocatedSize) {
+      values_.resize(kPreallocatedSize);
+      assigned_.resize(kPreallocatedSize);
+    }
+    auto iterator_and_flag = column_id_to_index_.emplace(col_id, values_.size());
+    if (iterator_and_flag.second) {
+      return AppendColumn();
+    }
+    index = iterator_and_flag.first->second;
+  }
+
+  if (!assigned_[index]) {
+    assigned_[index] = true;
+    ++num_assigned_;
+  }
+  return values_[index];
 }
 
 QLTableColumn& QLTableRow::AllocColumn(ColumnIdRep col_id, const QLValue& ql_value) {
-  col_map_[col_id].value = ql_value.value();
-  return col_map_[col_id];
+  return AllocColumn(col_id, ql_value.value());
 }
 
 QLTableColumn& QLTableRow::AllocColumn(ColumnIdRep col_id, const QLValuePB& ql_value) {
-  col_map_[col_id].value = ql_value;
-  return col_map_[col_id];
+  QLTableColumn& result = AllocColumn(col_id);
+  result.value = ql_value;
+  return result;
 }
 
-CHECKED_STATUS QLTableRow::CopyColumn(ColumnIdRep col_id,
-                                      const QLTableRow& source) {
-  auto col_iter = source.col_map_.find(col_id);
-  if (col_iter != source.col_map_.end()) {
-    col_map_[col_id] = col_iter->second;
+QLTableColumn& QLTableRow::AllocColumn(ColumnIdRep col_id, QLValuePB&& ql_value) {
+  QLTableColumn& result = AllocColumn(col_id);
+  result.value = std::move(ql_value);
+  return result;
+}
+
+void QLTableRow::CopyColumn(ColumnIdRep col_id, const QLTableRow& source) {
+  const auto* value = source.FindColumn(col_id);
+  if (value) {
+    AllocColumn(col_id) = *value;
+    return;
   }
-  return Status::OK();
+
+  auto index = ColumnIndex(col_id);
+  if (index == kInvalidIndex) {
+    return;
+  }
+  if (assigned_[index]) {
+    assigned_[index] = false;
+    --num_assigned_;
+  }
+}
+
+std::string QLTableRow::ToString() const {
+  std::string ret("{ ");
+
+  for (size_t i = 0; i != kPreallocatedSize; ++i) {
+    if (i >= values_.size()) {
+      break;
+    }
+    if (!assigned_[i]) {
+      continue;
+    }
+    ret.append(Format("$0 => $1 ", i + kFirstColumnIdRep, values_[i]));
+  }
+
+  for (auto p : column_id_to_index_) {
+    if (!assigned_[p.second]) {
+      continue;
+    }
+
+    ret.append(Format("$0 => $1 ", p.first, values_[p.second]));
+  }
+
+  ret.append("}");
+  return ret;
 }
 
 std::string QLTableRow::ToString(const Schema& schema) const {
@@ -720,9 +843,9 @@ std::string QLTableRow::ToString(const Schema& schema) const {
   ret.append("{ ");
 
   for (size_t col_idx = 0; col_idx < schema.num_columns(); col_idx++) {
-    auto it = col_map_.find(schema.column_id(col_idx));
-    if (it != col_map_.end() && it->second.value.value_case() != QLValuePB::VALUE_NOT_SET) {
-      ret += it->second.value.ShortDebugString();
+    const auto* value = GetColumn(schema.column_id(col_idx));
+    if (value && value->value_case() != QLValuePB::VALUE_NOT_SET) {
+      ret += value->ShortDebugString();
     } else {
       ret += "null";
     }
diff --git a/src/yb/common/ql_expr.h b/src/yb/common/ql_expr.h
index 076fd79a7105..e9479aa0a751 100644
--- a/src/yb/common/ql_expr.h
+++ b/src/yb/common/ql_expr.h
@@ -83,23 +83,16 @@ class QLTableRow {
   typedef std::shared_ptr<QLTableRow> SharedPtr;
   typedef std::shared_ptr<const QLTableRow> SharedPtrConst;
 
-  static const QLTableRow& empty_row() {
-    static QLTableRow empty_row;
-    return empty_row;
-  }
+  static const QLTableRow& empty_row();
 
   // Check if row is empty (no column).
-  bool IsEmpty() const {
-    return col_map_.empty();
-  }
+  bool IsEmpty() const { return num_assigned_ == 0; }
 
   // Get column count.
-  size_t ColumnCount() const {
-    return col_map_.size();
-  }
+  size_t ColumnCount() const;
 
   // Clear the row.
-  void Clear() { col_map_.clear(); }
+  void Clear();
 
   // Compare column value between two rows.
   bool MatchColumn(ColumnIdRep col_id, const QLTableRow& source) const;
@@ -120,9 +113,14 @@ class QLTableRow {
     return AllocColumn(col.rep(), ql_value);
   }
 
+  QLTableColumn& AllocColumn(ColumnIdRep col_id, QLValuePB&& ql_value);
+  QLTableColumn& AllocColumn(const ColumnId& col, QLValuePB&& ql_value) {
+    return AllocColumn(col.rep(), std::move(ql_value));
+  }
+
   // Copy column-value from 'source' to the 'col_id' entry in the cached column-map.
-  CHECKED_STATUS CopyColumn(ColumnIdRep col_id, const QLTableRow& source);
-  CHECKED_STATUS CopyColumn(const ColumnId& col, const QLTableRow& source) {
+  void CopyColumn(ColumnIdRep col_id, const QLTableRow& source);
+  void CopyColumn(const ColumnId& col, const QLTableRow& source) {
     return CopyColumn(col.rep(), source);
   }
 
@@ -147,9 +145,9 @@ class QLTableRow {
   bool IsColumnSpecified(ColumnIdRep col_id) const;
 
   // Clear the column value.
-  void ClearValue(ColumnIdRep col_id);
-  void ClearValue(const ColumnId& col) {
-    return ClearValue(col.rep());
+  void MarkTombstoned(ColumnIdRep col_id);
+  void MarkTombstoned(const ColumnId& col) {
+    return MarkTombstoned(col.rep());
   }
 
   // Get the column value in PB format.
@@ -161,20 +159,43 @@ class QLTableRow {
 
   // For testing only (no status check).
   const QLTableColumn& TestValue(ColumnIdRep col_id) const {
-    return col_map_.at(col_id);
+    return *FindColumn(col_id);
   }
   const QLTableColumn& TestValue(const ColumnId& col) const {
-    return col_map_.at(col.rep());
-  }
-
-  std::string ToString() const {
-    return yb::ToString(col_map_);
+    return *FindColumn(col.rep());
   }
 
+  std::string ToString() const;
   std::string ToString(const Schema& schema) const;
 
  private:
-  std::unordered_map<ColumnIdRep, QLTableColumn> col_map_;
+  // Return kInvalidIndex when column index is unknown.
+  size_t ColumnIndex(ColumnIdRep col_id) const;
+  const QLTableColumn* FindColumn(ColumnIdRep col_id) const;
+  Result<const QLTableColumn&> Column(ColumnIdRep col_id) const;
+  // Appends new entry to values_ and assigned_ fields.
+  QLTableColumn& AppendColumn();
+
+  // Map from column id to index in values_ and assigned_ vectors.
+  // For columns from [kFirstColumnId; kFirstColumnId + kPreallocatedSize) we don't use
+  // this field and map them directly.
+  // I.e. column with id kFirstColumnId will have index 0 etc.
+  // We are using unsigned int as map value and std::numeric_limits<size_t>::max() as invalid
+  // column.
+  // This way, the compiler would understand that this invalid value could never be stored in the
+  // map and optimize away the comparison with it when inlining the ColumnIndex function call.
+  std::unordered_map<ColumnIdRep, unsigned int> column_id_to_index_;
+
+  static constexpr size_t kPreallocatedSize = 8;
+  static constexpr size_t kFirstNonPreallocatedColumnId = kFirstColumnIdRep + kPreallocatedSize;
+
+  // The two following vectors will be of the same size.
+  // We use separate fields to achieve the following features:
+  // 1) Fast was to cleanup row, just by setting assigned to false with one call.
+  // 2) Avoid destroying values_, so they would be able to reuse allocated storage during row reuse.
+  boost::container::small_vector<QLTableColumn, kPreallocatedSize> values_;
+  boost::container::small_vector<bool, kPreallocatedSize> assigned_;
+  size_t num_assigned_ = 0;
 };
 
 class QLExprExecutor {
diff --git a/src/yb/common/ql_table_row-test.cc b/src/yb/common/ql_table_row-test.cc
new file mode 100644
index 000000000000..df9c7e353761
--- /dev/null
+++ b/src/yb/common/ql_table_row-test.cc
@@ -0,0 +1,77 @@
+// Copyright (c) YugaByte, Inc.
+//
+// Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
+// in compliance with the License.  You may obtain a copy of the License at
+//
+// http://www.apache.org/licenses/LICENSE-2.0
+//
+// Unless required by applicable law or agreed to in writing, software distributed under the License
+// is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
+// or implied.  See the License for the specific language governing permissions and limitations
+// under the License.
+//
+
+#include <gtest/gtest.h>
+
+#include "yb/common/ql_expr.h"
+
+#include "yb/util/random_util.h"
+
+namespace yb {
+
+TEST(QLTableRowTest, Simple) {
+  const ColumnIdRep column1 = kFirstColumnId.rep();
+  const ColumnIdRep column2 = kFirstColumnId.rep() + 100;
+
+  QLTableRow row;
+  QLValuePB value1;
+  value1.set_int32_value(42);
+  QLValuePB value2;
+  value2.set_string_value("test");
+
+  row.AllocColumn(column1, value1);
+  ASSERT_TRUE(row.IsColumnSpecified(column1));
+  ASSERT_EQ(*row.GetColumn(column1), value1);
+  ASSERT_EQ(
+      Format("{ $0 => { value: int32_value: 42 ttl_seconds: 0 write_time: "
+                 "kUninitializedWriteTime } }",
+             kFirstColumnIdRep),
+      row.ToString());
+
+  row.AllocColumn(column2, value2);
+
+  ASSERT_TRUE(row.IsColumnSpecified(column2));
+  ASSERT_EQ(*row.GetColumn(column2), value2);
+}
+
+TEST(QLTableRowTest, Random) {
+  constexpr int kRows = 100;
+  constexpr int kRowIterations = 100;
+  constexpr int kMutations = 10;
+  constexpr int kColumns = 16;
+  for (int i = kRows; i-- > 0;) {
+    QLTableRow row;
+    for (int j = kRowIterations; j-- > 0;) {
+      row.Clear();
+      std::unordered_map<ColumnId, QLValuePB> map;
+      for (int m = kMutations; m-- > 0;) {
+        ColumnId column_id(kFirstColumnIdRep + RandomUniformInt(0, kColumns));
+        if (map.count(column_id)) {
+          continue;
+        }
+        QLValuePB value;
+        value.set_int32_value(RandomUniformInt(0, 100));
+        map.emplace(column_id, value);
+        row.AllocColumn(column_id, std::move(value));
+
+        ASSERT_EQ(row.ColumnCount(), map.size());
+
+        for (const auto& p : map) {
+          ASSERT_EQ(*row.GetColumn(p.first), p.second);
+        }
+      }
+    }
+  }
+}
+
+} // namespace yb
diff --git a/src/yb/common/schema.h b/src/yb/common/schema.h
index 74f4c6d6b804..9f062d7b9021 100644
--- a/src/yb/common/schema.h
+++ b/src/yb/common/schema.h
@@ -149,10 +149,11 @@ static const ColumnId kInvalidColumnId = ColumnId(std::numeric_limits<ColumnIdRe
 // 10, ensuring that if we accidentally mix up IDs and indexes, we're likely to fire an
 // assertion or bad memory access.
 #ifdef NDEBUG
-static const ColumnId kFirstColumnId(0);
+constexpr ColumnIdRep kFirstColumnIdRep = 0;
 #else
-static const ColumnId kFirstColumnId(10);
+constexpr ColumnIdRep kFirstColumnIdRep = 10;
 #endif
+const ColumnId kFirstColumnId(kFirstColumnIdRep);
 
 template<char... digits>
 ColumnId operator"" _ColId() {
diff --git a/src/yb/docdb/cql_operation.cc b/src/yb/docdb/cql_operation.cc
index 32c60826ab87..05806178a1a8 100644
--- a/src/yb/docdb/cql_operation.cc
+++ b/src/yb/docdb/cql_operation.cc
@@ -136,7 +136,7 @@ bool JoinStaticRow(
 
   // Join the static columns in the static row into the non-static row.
   for (size_t i = 0; i < static_projection.num_columns(); i++) {
-    CHECK_OK(non_static_row->CopyColumn(static_projection.column_id(i), static_row));
+    non_static_row->CopyColumn(static_projection.column_id(i), static_row);
   }
 
   return true;
@@ -163,7 +163,7 @@ bool JoinNonStaticRow(
     }
 
     for (size_t i = 0; i < schema.num_hash_key_columns(); i++) {
-      CHECK_OK(static_row->CopyColumn(schema.column_id(i), non_static_row));
+      static_row->CopyColumn(schema.column_id(i), non_static_row);
     }
   }
   return join_successful;
@@ -905,7 +905,7 @@ Status QLWriteOperation::Apply(const DocOperationApplyData& data) {
           RETURN_NOT_OK(data.doc_write_batch->DeleteSubDoc(sub_path,
               data.read_time, data.deadline, request_.query_id(), user_timestamp));
           if (update_indexes_) {
-            new_row.ClearValue(column_id);
+            new_row.MarkTombstoned(column_id);
           }
         }
         if (update_indexes_) {
diff --git a/src/yb/docdb/primitive_value.cc b/src/yb/docdb/primitive_value.cc
index a47c43b4d9f5..9712989b095b 100644
--- a/src/yb/docdb/primitive_value.cc
+++ b/src/yb/docdb/primitive_value.cc
@@ -1689,6 +1689,7 @@ void PrimitiveValue::ToQLValuePB(const PrimitiveValue& primitive_value,
     case FROZEN: {
       const auto& type = ql_type->param_type(0);
       QLSeqValuePB *frozen_value = ql_value->mutable_frozen_value();
+      frozen_value->clear_elems();
       switch (type->main()) {
         case MAP: {
           const std::shared_ptr<QLType>& keys_type = type->param_type(0);
diff --git a/src/yb/docdb/primitive_value.h b/src/yb/docdb/primitive_value.h
index 7a7d760e182a..d73e0d2872dc 100644
--- a/src/yb/docdb/primitive_value.h
+++ b/src/yb/docdb/primitive_value.h
@@ -40,7 +40,7 @@ namespace docdb {
 // Used for extending a list.
 // PREPEND prepends the arguments one by one (PREPEND a b c) will prepend [c b a] to the list,
 // while PREPEND_BLOCK prepends the arguments together, so it will prepend [a b c] to the list.
-  YB_DEFINE_ENUM(ListExtendOrder, (APPEND)(PREPEND_BLOCK)(PREPEND))
+YB_DEFINE_ENUM(ListExtendOrder, (APPEND)(PREPEND_BLOCK)(PREPEND))
 
 // A necessary use of a forward declaration to avoid circular inclusion.
 class SubDocument;
diff --git a/src/yb/docdb/subdocument.cc b/src/yb/docdb/subdocument.cc
index b29003afc18c..cb05bb70908c 100644
--- a/src/yb/docdb/subdocument.cc
+++ b/src/yb/docdb/subdocument.cc
@@ -423,6 +423,8 @@ void SubDocument::ToQLValuePB(const SubDocument& doc,
       const shared_ptr<QLType>& keys_type = ql_type->params()[0];
       const shared_ptr<QLType>& values_type = ql_type->params()[1];
       QLMapValuePB *value_pb = ql_value->mutable_map_value();
+      value_pb->clear_keys();
+      value_pb->clear_values();
       for (auto &pair : doc.object_container()) {
         QLValuePB *key = value_pb->add_keys();
         PrimitiveValue::ToQLValuePB(pair.first, keys_type, key);
@@ -434,6 +436,7 @@ void SubDocument::ToQLValuePB(const SubDocument& doc,
     case SET: {
       const shared_ptr<QLType>& elems_type = ql_type->params()[0];
       QLSeqValuePB *value_pb = ql_value->mutable_set_value();
+      value_pb->clear_elems();
       for (auto &pair : doc.object_container()) {
         QLValuePB *elem = value_pb->add_elems();
         PrimitiveValue::ToQLValuePB(pair.first, elems_type, elem);
@@ -444,6 +447,7 @@ void SubDocument::ToQLValuePB(const SubDocument& doc,
     case LIST: {
       const shared_ptr<QLType>& elems_type = ql_type->params()[0];
       QLSeqValuePB *value_pb = ql_value->mutable_list_value();
+      value_pb->clear_elems();
       for (auto &pair : doc.object_container()) {
         // list elems are represented as subdocument values with keys only used for ordering
         QLValuePB *elem = value_pb->add_elems();
@@ -454,6 +458,8 @@ void SubDocument::ToQLValuePB(const SubDocument& doc,
     case USER_DEFINED_TYPE: {
       const shared_ptr<QLType>& keys_type = QLType::Create(INT16);
       QLMapValuePB *value_pb = ql_value->mutable_map_value();
+      value_pb->clear_keys();
+      value_pb->clear_values();
       for (auto &pair : doc.object_container()) {
         QLValuePB *key = value_pb->add_keys();
         PrimitiveValue::ToQLValuePB(pair.first, keys_type, key);
diff --git a/src/yb/tablet/tablet.cc b/src/yb/tablet/tablet.cc
index faa1ae79a846..79e9235f3c9a 100644
--- a/src/yb/tablet/tablet.cc
+++ b/src/yb/tablet/tablet.cc
@@ -2066,7 +2066,7 @@ Result<std::string> Tablet::BackfillIndexes(const std::vector<IndexInfo> &indexe
 Status Tablet::UpdateIndexInBatches(
     const QLTableRow& row, const std::vector<IndexInfo>& indexes,
     std::vector<std::pair<const IndexInfo*, QLWriteRequestPB>>* index_requests) {
-  const QLTableRow kEmptyRow;
+  const QLTableRow& kEmptyRow = QLTableRow::empty_row();
   QLExprExecutor expr_executor;
 
   for (const IndexInfo& index : indexes) {
