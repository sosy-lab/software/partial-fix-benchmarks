[#5408] [YSQL] Make SQL/JSON error code names match SQL standard

Summary:
Postgres Commit was eb892102e01a2073df9250d65e33ec1ed21798df

Commit message was :

    see also a00c53b0cb

Test Plan: Build yugabyte db and run tests via Jenkins

Reviewers: mihnea, jason

Reviewed By: jason

Subscribers: yql

Differential Revision: https://phabricator.dev.yugabyte.com/D9591
[#5492][yb-admin] fixed create_snapshot fatal for YCQL system tables

Summary:
yb-admin create_snapshot doesn't seem to work for YCQL system tables. In addition to that, it crashes yb-master. This diff fixes that. In addition to that, it throws error when users want to snapshot only system tables.

YCQL system tables:

```
./bin/yb-ctl create
./build/latest/bin/yb-admin --master_addresses 127.0.0.1 create_snapshot system peers
```
master logs show

F0825 15:01:34.036633   967 flush_manager.cc:68] Check failed: ts_tablet_map.size() > 0 (0 vs. 0)

Test Plan:
Initially when we run

```
./bin/yb-ctl create
./build/latest/bin/yb-admin --master_addresses 127.0.0.1 create_snapshot system peers

```
yb-master crashes.

after this patch, running create_snapshot on system peers throws this error

```
Error: Invalid argument (yb/tools/yb-admin_cli.cc:828): Cannot create snapshot of system tables
```
Additionally, master does not crash.

This patch can be tested by running the following test command:
```
ybd --cxx-test yb-admin-test --gtest_filter AdminCliTest.TestSnapshotCreation
```

Reviewers: bogdan, rsami, oleg

Reviewed By: oleg

Subscribers: zyu, ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D9535
Revert "[#5492][yb-admin] fixed create_snapshot fatal for YCQL system tables"

Summary:
This reverts commit d0cfbad8766ecd3bfeae8cba2db32214cf7431be.

This commit disabled snapshots for redis tables which are user system tables. Hence, reverting this diff till i figure out a way to disable snapshots for system tables without affecting redis tables.

Test Plan: This reverts an old diff and hence no test plan required.

Reviewers: oleg, arnav, bogdan

Reviewed By: bogdan

Differential Revision: https://phabricator.dev.yugabyte.com/D9604
[#5492] yb-admin: Added error message when attempting to create snapshot of YCQL system tables

Summary:
YCQL system tables are all virtual, with no data stored in RocksDB, and as a result,
attempting to create a snapshot of these tables will take down either the master or tablet servers.

This diff adds a check to prevent this from happening.

Test Plan:
Added to the create snapshot test case and manually tested the following:
- create_snapshot on YCQL system tables results in the new error message (e.g. system.peers,
  system.transactions)
- create_snapshot on non-YCQL system tables (especially YEDIS's system_redis.redis table) does
  not result in the new error message

`ybd --cxx-test tools_yb-admin-test --gtest_filter AdminCliTest.TestSnapshotCreation`

Reviewers: bogdan, rskannan, oleg

Reviewed By: oleg

Subscribers: ybase

Differential Revision: https://phabricator.dev.yugabyte.com/D12861
