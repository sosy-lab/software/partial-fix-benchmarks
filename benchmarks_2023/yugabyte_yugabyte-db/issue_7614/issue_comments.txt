[docdb] Add rocksdb option to run a full compaction at a fixed interval
Would it be possible to make it run on a specific day of the week? For example, if we want to run it on the weekend, can we have it start on Saturday?
From slack after a conversation with @kmuthukk 

To begin with, just adding a HBase like control (gflag) which forces a major compaction every so often:
force_major_compact_interval_secs (default to say 30 days). (or do we want to default this to inf for backward compat)?
force_major_compact_jitter (default to 0.15)
So, by default any tablet's next major compaction time will be set to interval * (1 + rand(-jitter, jitter)) . This can keep getting re-evaluated after every major compaction for every tablet. The jitter notion is only to avoid a compaction storm hitting all tablets at the same time.
We might also need to think through if we want to persist any of this information in some metadata, so as to not get reset on node restart and miss out on the interval.
Cassandra handles this by doing single file compactions. Each SSTable keeps track of how much of its data is expired at any point in time and it that exceeds a threshold it runs a single file compaction. This is much better than requiring a major compaction and creating a very large file which again needs to go thru compaction.
https://cassandra.apache.org/doc/latest/cassandra/operating/compaction/index.html#single-sstable-tombstone-compaction
Will backport it to 2.14 release if support requests it.