diff --git a/src/yb/yql/pggate/pg_session.cc b/src/yb/yql/pggate/pg_session.cc
index 3ab45fb42d4f..49e8a358f666 100644
--- a/src/yb/yql/pggate/pg_session.cc
+++ b/src/yb/yql/pggate/pg_session.cc
@@ -78,16 +78,6 @@ using yb::master::MasterServiceProxy;
 
 using yb::tserver::TServerSharedObject;
 
-#if defined(__APPLE__) && !defined(NDEBUG)
-// We are experiencing more slowness in tests on macOS in debug mode.
-const int kDefaultPgYbSessionTimeoutMs = 120 * 1000;
-#else
-const int kDefaultPgYbSessionTimeoutMs = 60 * 1000;
-#endif
-
-DEFINE_int32(pg_yb_session_timeout_ms, kDefaultPgYbSessionTimeoutMs,
-             "Timeout for operations between PostgreSQL server and YugaByte DocDB services");
-
 namespace {
 //--------------------------------------------------------------------------------------------------
 // Constants used for the sequences data table.
@@ -314,13 +304,6 @@ bool Erase(Container* container, PgOid table_id, const Slice& ybctid) {
   return false;
 }
 
-void SetupSession(client::YBSession* session) {
-  // Sets the timeout for each rpc as well as the whole operation to
-  // 'FLAGS_pg_yb_session_timeout_ms'.
-  session->SetTimeout(MonoDelta::FromMilliseconds(FLAGS_pg_yb_session_timeout_ms));
-  session->SetForceConsistentRead(client::ForceConsistentRead::kTrue);
-}
-
 } // namespace
 
 //--------------------------------------------------------------------------------------------------
@@ -549,15 +532,12 @@ PgSession::PgSession(
     const tserver::TServerSharedObject* tserver_shared_object,
     const YBCPgCallbacks& pg_callbacks)
     : client_(client),
-      session_(client_->NewSession()),
+      session_(BuildSession(client_)),
       pg_txn_manager_(std::move(pg_txn_manager)),
       clock_(std::move(clock)),
-      catalog_session_(std::make_shared<YBSession>(client_, clock_.get())),
+      catalog_session_(BuildSession(client_, clock_)),
       tserver_shared_object_(tserver_shared_object),
       pg_callbacks_(pg_callbacks) {
-
-  SetupSession(session_.get());
-  SetupSession(catalog_session_.get());
 }
 
 PgSession::~PgSession() {
diff --git a/src/yb/yql/pggate/pg_txn_manager.cc b/src/yb/yql/pggate/pg_txn_manager.cc
index 698af6370eb5..5cba6fc8a877 100644
--- a/src/yb/yql/pggate/pg_txn_manager.cc
+++ b/src/yb/yql/pggate/pg_txn_manager.cc
@@ -91,6 +91,25 @@ using client::YBSession;
 using client::YBSessionPtr;
 using client::LocalTabletFilter;
 
+#if defined(__APPLE__) && !defined(NDEBUG)
+// We are experiencing more slowness in tests on macOS in debug mode.
+const int kDefaultPgYbSessionTimeoutMs = 120 * 1000;
+#else
+const int kDefaultPgYbSessionTimeoutMs = 60 * 1000;
+#endif
+
+DEFINE_int32(pg_yb_session_timeout_ms, kDefaultPgYbSessionTimeoutMs,
+             "Timeout for operations between PostgreSQL server and YugaByte DocDB services");
+
+std::shared_ptr<yb::client::YBSession> BuildSession(
+    yb::client::YBClient* client,
+    const scoped_refptr<ClockBase>& clock) {
+  auto session = std::make_shared<YBSession>(client, clock);
+  session->SetForceConsistentRead(client::ForceConsistentRead::kTrue);
+  session->SetTimeout(MonoDelta::FromMilliseconds(FLAGS_pg_yb_session_timeout_ms));
+  return session;
+}
+
 PgTxnManager::PgTxnManager(
     AsyncClientInitialiser* async_client_init,
     scoped_refptr<ClockBase> clock,
@@ -154,9 +173,8 @@ Status PgTxnManager::SetDeferrable(bool deferrable) {
 }
 
 void PgTxnManager::StartNewSession() {
-  session_ = std::make_shared<YBSession>(async_client_init_->client(), clock_);
+  session_ = BuildSession(async_client_init_->client(), clock_);
   session_->SetReadPoint(client::Restart::kFalse);
-  session_->SetForceConsistentRead(client::ForceConsistentRead::kTrue);
 }
 
 uint64_t PgTxnManager::GetPriority(const NeedsPessimisticLocking needs_pessimistic_locking) {
@@ -368,8 +386,7 @@ Status PgTxnManager::EnterSeparateDdlTxnMode() {
   RSTATUS_DCHECK(!ddl_txn_,
           IllegalState, "EnterSeparateDdlTxnMode called when already in a DDL transaction");
   VLOG_TXN_STATE(2);
-  ddl_session_ = std::make_shared<YBSession>(async_client_init_->client(), clock_);
-  ddl_session_->SetForceConsistentRead(client::ForceConsistentRead::kTrue);
+  ddl_session_ = BuildSession(async_client_init_->client(), clock_);
   ddl_txn_ = std::make_shared<YBTransaction>(GetOrCreateTransactionManager());
   ddl_session_->SetTransaction(ddl_txn_);
   RETURN_NOT_OK(ddl_txn_->Init(
diff --git a/src/yb/yql/pggate/pg_txn_manager.h b/src/yb/yql/pggate/pg_txn_manager.h
index 41ea6e0676cc..cbebec9953ce 100644
--- a/src/yb/yql/pggate/pg_txn_manager.h
+++ b/src/yb/yql/pggate/pg_txn_manager.h
@@ -47,6 +47,10 @@ YB_DEFINE_ENUM(
   ((SERIALIZABLE, 3))
 );
 
+std::shared_ptr<yb::client::YBSession> BuildSession(
+    yb::client::YBClient* client,
+    const scoped_refptr<ClockBase>& clock = nullptr);
+
 class PgTxnManager : public RefCountedThreadSafe<PgTxnManager> {
  public:
   PgTxnManager(client::AsyncClientInitialiser* async_client_init,
