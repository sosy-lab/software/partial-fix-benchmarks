samples/display: Introduce the cfb_shell sample app

This adds the shell sample app for the Character Framebuffer testing.

cfb - Character Framebuffer shell commands
Options:
  -h, --help  :Show command help.
Subcommands:
  init        :[none]
  get_device  :[none]
  get_param   :<all, height, width, ppt, rows, cols>
  get_fonts   :[none]
  set_font    :<idx>
  invert      :[none]
  print       :<col: pos> <row: pos> <text>
  scroll      :<dir: (vertical|horizontal)> <col: pos> <row: pos> <text>
  clear       :[none]

Signed-off-by: Diego Sueiro <diego.sueiro@gmail.com>
esp32: fix build error due to BIT conflict

Fixes #11077

Signed-off-by: Anas Nashif <anas.nashif@intel.com>
esp32: fix build error regarding multiplt BIT() definitions

This partially reverts commit 5a47c60dbf4f24fa9ad2206502d5eab152587eac.
The soc.h is now only included when _soc_irq_*() is being referred.

Fixes #11077.

Signed-off-by: Daniel Leung <daniel.leung@intel.com>
esp32: fix build error regarding multiplt BIT() definitions

This partially reverts commit 5a47c60dbf4f24fa9ad2206502d5eab152587eac.
The soc.h is now only included when _soc_irq_*() is being referred.

Fixes #11077.

Signed-off-by: Daniel Leung <daniel.leung@intel.com>
