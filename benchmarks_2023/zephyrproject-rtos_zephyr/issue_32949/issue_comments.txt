[Coverity CID :219491] Resource leak in tests/net/socket/af_packet/src/main.c
Aren’t `sock1` and `sock2` the cause of the issue? They are never closed. 🤔 What else could be leaking?
Yeah, both sock1 and sock2 are not closed which triggers this Coverity issue.
I have found this test case is enough to reproduce the failing test for `qemu_x86`. It looks like we cannot close the sockets for whatever reason. I'm not sure how to debug this since the logs don't give any location in the code.
```c
static void test_packet_sockets(void)
{
	int sock1, sock2;

	__test_packet_sockets(&sock1, &sock2);

        close(sock1);
        close(sock2);
}
```

The test does not fail if we don't call `close()` (as it is now on `master`) but then we have a resource leak. 
I have narrowed the problem down to this line:

https://github.com/zephyrproject-rtos/zephyr/blob/7874052df220caf53e9bb6b949f7e6a4c1903311/subsys/net/lib/sockets/sockets.c#L261

I added some additional print statements to narrow it down, but here is the entire log at the moment:

```
gudni@ubuntu:~/zephyrproject$ west build -b qemu_x86 zephyr/tests/net/socket/af_packet
[7/27] Linking C executable zephyr/app_smem_unaligned_prebuilt.elf

[9/27] Linking C executable zephyr/zephyr_prebuilt.elf

[27/27] Linking C executable zephyr/zephyr.elf
Memory region         Used Size  Region Size  %age Used
             RAM:      329313 B         3 MB     10.47%
        IDT_LIST:          0 GB         2 KB      0.00%

gudni@ubuntu:~/zephyrproject$ west build -t run
-- west build: running target run
[2/21] Linking C executable zephyr/zephyr_prebuilt.elf

[20/21] Linking C executable zephyr/zephyr.elf
Memory region         Used Size  Region Size  %age Used
             RAM:      329313 B         3 MB     10.47%
        IDT_LIST:          0 GB         2 KB      0.00%

[20/21] To exit from QEMU enter: 'CTRL+a, x'[QEMU] CPU: qemu32,+nx,+pae
SeaBIOS (version zephyr-v1.0.0-0-g31d4e0e-dirty-20200714_234759-fv-az50-zephyr)
Booting from ROM..*** Booting Zephyr OS build zephyr-v2.5.0-1119-g15b9565b37ea  ***
I: Initializing network
I: IPv4 address: 127.0.0.1
Running test suite socket_packet
===================================================================
START - test_packet_sockets
W: Setup socket 1
W: OK access on sock 1 by thread 0x121c40
W: Closing socket...
W: OK access on sock 1 by thread 0x121c40
W: close: ctx=0x121360, fd=1
W: freeing sock...
W: ret = vtable->fd_vtable.close(ctx); 0x121360
E: Page fault at address (nil) (error code 0x10)
E: Linear address not present in page tables
E: Access violation: supervisor thread not allowed to execute
E: PTE: not present
E: EAX: 0x0011b1e0, EBX: 0x00121360, ECX: 0x00144f18, EDX: 0x00144f78
E: ESI: 0x00000001, EDI: 0x001019d7, EBP: 0x00144f90, ESP: 0x00144f78
E: EFLAGS: 0x00000212 CS: 0x0008 CR3: 0x00150000
E: call trace:
E: EIP: 0x00000000
E:      0x001005b5 (0x1)
E:      0x00100969 (0x14a1e8)
E:      0x00100b84 (0x1)
E:      0x0010eabf (0x0)
E:      0x001019e7 (0x120000)
E: >>> ZEPHYR FATAL ERROR 0: CPU exception on CPU 0
E: Current thread: 0x121c40 (unknown)
E: Halting system
FAILED: zephyr/CMakeFiles/run 
cd /home/gudni/zephyrproject/build && /home/gudni/zephyr-sdk-0.12.2/sysroots/x86_64-pokysdk-linux/usr/bin/qemu-system-i386 -m 4 -cpu qemu32,+nx,+pae -device isa-debug-exit,iobase=0xf4,iosize=0x04 -no-reboot -nographic -no-acpi -net none -pidfile qemu.pid -chardev stdio,id=con,mux=on -serial chardev:con -mon chardev=con,mode=readline -kernel /home/gudni/zephyrproject/build/zephyr/zephyr.elf
ninja: build stopped: subcommand failed.
FATAL ERROR: command exited with status 1: /usr/bin/cmake --build /home/gudni/zephyrproject/build --target run
```


> It looks like we cannot close the sockets for whatever reason.

Thanks for the analysis. We definitely should be able to close the sockets.
After looking the packet socket code a bit more, I noticed that we are missing close callback for packet sockets. Then we get NULL pointer access when trying to access it. I can send a patch for this in few minutes.

@jukkar https://github.com/zephyrproject-rtos/zephyr/pull/33336 here is my attempt.

Using `sock_fd_op_vtable.fd_vtable.close(ctx)` instead is sort of a guess on my part 😃 

Edit: that same callback is provided in `sockets.c`
> Using `sock_fd_op_vtable.fd_vtable.close(ctx)` instead is sort of a guess on my part

Thanks for the proposal, but I think we need to fix this in packet socket side as I propose in #33338 