devicetree: supported devices API
Thanks for the clear analysis and description.

> I don't believe that the requirement to walk supporting devices is general enough to justify an array in the core device struct like the dependency array, but maybe it is.

I think @gmarull is better situated to comment on the implementation details, but I actually think it is justified, personally.

If you look in the test cases related to ordinals, I anticipated this use case being handled in exactly that way:

https://github.com/zephyrproject-rtos/zephyr/blob/976c5fee289fa07111e04a8bc3368a1984560dae/tests/lib/devicetree/api/src/main.c#L1818-L1839

Nobody's gotten around to doing this sort of thing in device.h yet is all, or at least that's what I understand.

I could see turning off the extra ROM if you don't need it, depending on some PM Kconfigs perhaps.

> but I actually think it is justified, personally.

It is certainly convenient, and simply extending the existing handles array makes the implementation a whole lot cleaner. The documentation around the handles array already specified it was for all nodes with relationships, not just dependent devices.
https://github.com/zephyrproject-rtos/zephyr/pull/39581
Implemented in #40418