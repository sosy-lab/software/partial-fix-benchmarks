BLE stack can get stuck in connected state despite connection failure
The `send_conn_le_param_update()` will only be called if the connection is not in the disconnected state, so most likely there is a missing state transition somewhere. Can you share some logs that shows what happens before the failed connection parameter update?
Sorry, not really - first of all this is pretty hard to reproduce but basically there was no other indications that something went wrong. Everything looked fine until that opcode warning. Maybe I'd have more info if I had higher log levels set, but as I said it's not easy to reproduce.
@tautologyclub could you please add a couple of details:
1. When you say that Zephyr doesn't handle this correctly, what do you mean? Is the BLE stack stuck after?
2. Can you please provide a sniffer trace, just like [here](https://github.com/zephyrproject-rtos/zephyr/issues/48813#issuecomment-1209267048)
3. This seems like the opposite situation to #48813, with the L2CAP procedure here being initiated by the local device instead of the remote peer, but in that case it was only an annoyance to see the message printed
@carlescufi

1. Yeah it's stuck in a perpetually connected state (i.e. the disconnected callbacks never trigger etc but the other side has disconnected)
2. I wish I could and I will if we manage to reproduce it consistently. 
3. Yeah this is more than an annoyance - luckily we got a hardware watchdog that gets us out of this but it's def not good

But yeah it seems like a pretty easy fix to me - just don't ignore the retval of `send_conn_le_param_update()` in the work handler? Yall feel like enlightening me as to why this is not a good solution?
I think those are two separate issues. The return value of `send_conn_le_param_update` shouldn't hang the connection in any case.
Could you try the linked branch and report back if it fixes the issue?

I'll still try to reproduce it on my end to see if we're not just masking a deeper issue, but that could take some time as I have other bug I have to address first.
@jori-nordic What I mean is that knowledge of this hanged connection state is propagated through that retval, so we could use it to escape the state, if we checked the retval. Are you saying that "this is a solution but not the correct solution" or are you saying that this is not a solution at all?
I'm saying this is (maybe) a solution, but likely not the correct (or long-term) solution. 

Problem is that we're in a work handler so we can't really propagate the error. The only thing we can do as you say is check it and execute some procedure (like explicitly disconnect the connection). 

But that ideally shouldn't be necessary and the device should know it's disconnected when the supervision timeout fires in the controller. It shouldn't just 'hang' like this. What I mean is I'd like to find out what's keeping this connection alive in the stack.
Maybe related to #44998
This issue has been marked as stale because it has been open (more than) 60 days with no activity. Remove the stale label or add a comment saying that you would like to have the label removed otherwise this issue will automatically be closed in 14 days. Note, that you can always re-open a closed issue at any time.
I haven't seen this for a while, closing. @tautologyclub feel free to re-open the ticket if it happens again.
Hello, I have the exact same scenario as @tautologyclub. I am using the nRF52840 as a peripheral and the ESP32-C3 as a central, and they are connected over BLE. The ESP32_C3 board is also using WiFi. I am noticing that the `bt_conn_cb .disconnected` callback does not always trigger on the nRF52840 when the ESP32-C3 is turned off (to save power). I do not get any warnings or errors from the BLE stack. Does anyone with a little bit more knowledge on this topic have any clue on why `bt_conn_cb .disconnected` is called sometimes, but not always? 
@Tjoms99 You really don't get any errors or warnings from the stack? Could you check that `CONFIG_LOG=y`.

Could you add `CONFIG_BT_CONN_LOG_LEVEL_DBG=y` to generate some logs?
Also having `CONFIG_BT_MAX_CONN=1` will help, as if the connection object is not freed, the next connection attempt from the ESP should fail with some errors that can help us debug more.

Something else that could help us is getting sniffer log. You can use [The nordic sniffer](https://www.nordicsemi.com/Products/Development-tools/nrf-sniffer-for-bluetooth-le) for gathering a trace, provided you have no encryption. If you're using encryption there is another config to dump the keys for decrypting the trace: `CONFIG_BT_LOG_SNIFFER_INFO=y`.

Some other questions:
- are you running a standard sample, or is this a custom app? If custom app, can you reproduce it using a peripheral sample?
- are you using the Zephyr LL or the Softdevice Controller?
@jori-nordic Sorry for the confusion, the nRF52840 is the central (client) and the ESP32-C3 is the peripheral (server), got it mixed up. Regardless, the application did not produce any logs even though I had ```CONFIG_BT_CONN_LOG_LEVEL_DBG=y``` and I could not reproduce the same behavior using a peripheral sample, but I managed to pinpoint the problem. <br>

This is a custom application with the purpose of relaying information sent over BLE to a RS485 bus and it is mostly interrupt driven. Long story short: the nRF52 has subscribed to ESP32's characteristics and is notified of any changes (interrupt-driven), and then calls ```rs485_write(message)``` inside a notify function. The ```rs485_write(message)``` function has a polling mechanism (bad) to check if the RS485 bus can be written to:     
```
    // Wait for the RS485 bus to become available.
    while (uart_get_is_busy())
    {
        LOG_DBG("UART busy");
        k_msleep(30);
    }
    rs485_tx_enable();
    uart_write(message);
    rs485_tx_disable();
```
Once in a blue moon, when a BLE notification arrives, the RS485 bus is busy, and the ESP32 is turned off during nRF52's  ```k_msleep(30)```, the ```bt_conn_cb.disconnected``` is never called on the nRF52. Removing the polling feature solved the problem.<br>

PS: I am using the Zephyr LL.
Both the GATT notify callback and the disconnected callback are called from the Bluetooth RX thread. So if you do blocking operations in the notify callback that will block the disconnected callback from firing as well. For how long are you blocking in the notify callback? Do you get the disconnected callback once you return from the notify callback?
@hermabe That makes sense. The notify callback   can block for 100's of milliseconds in the worst case scenario, and it can not block at all (which is usually the case). <Br>
The disconnected callback is not called if the notify callback is polling, even after it returns. I am not sure if this is the case all the time; Polling for 30ms vs 300ms might be different. My solution to this might be to offload the work in the notify callback and schedule it in a work queue instead.<br>
I am not able to give specifics until late next week (due to vacation). 
This issue has been marked as stale because it has been open (more than) 60 days with no activity. Remove the stale label or add a comment saying that you would like to have the label removed otherwise this issue will automatically be closed in 14 days. Note, that you can always re-open a closed issue at any time.
Removing the stale label until we get an update from @Tjoms99 
@Tjoms99 tried and failed to reproduce your issue in https://github.com/jori-nordic/zephyr/blob/disconnect-cb/tests/bsim/bluetooth/host/misc/disconnect/dut/src/main.c#L235

Do you have anything else going on in that app? other cooperative threads, other users of the system workqueue, etc..
@jori-nordic There was something else going on, have fixed the issue. In short, the MCU the BLE code was running on also communicates with another MCU over UART. When the BLE notify callback was triggered, the BLE MCU would ask the other MCU if it is present before sending data to it. If the MCU never sent an answer, the notify callback would never return (stuck in a while loop), meaning that the BLE disconnect callback would never have a chance to run.
Good to know, I'll close this again then :+1: 