net: core: Always have a timeout when allocating a net_buf

Instead of waiting forever for a network buffer, have a timeout
when allocating net_buf. This way we cannot left hanging for a
long time waiting for a buffer and possibly deadlock the system.
This commit only adds checks to core IP stack in subsys/net/ip

Fixes #7571

Signed-off-by: Jukka Rissanen <jukka.rissanen@linux.intel.com>
net: l2/lib: Always have a timeout when allocating a net_buf

Instead of waiting forever for a network buffer, have a timeout
when allocating net_buf. This way we cannot left hanging for a
long time waiting for a buffer and possibly deadlock the system.
This commit adds checks to L2 and network support libraries.

Fixes #7571

Signed-off-by: Jukka Rissanen <jukka.rissanen@linux.intel.com>
net: core: Always have a timeout when allocating a net_buf

Instead of waiting forever for a network buffer, have a timeout
when allocating net_buf. This way we cannot left hanging for a
long time waiting for a buffer and possibly deadlock the system.
This commit only adds checks to core IP stack in subsys/net/ip

Fixes #7571

Signed-off-by: Jukka Rissanen <jukka.rissanen@linux.intel.com>
net: l2/lib: Always have a timeout when allocating a net_buf

Instead of waiting forever for a network buffer, have a timeout
when allocating net_buf. This way we cannot left hanging for a
long time waiting for a buffer and possibly deadlock the system.
This commit adds checks to L2 and network support libraries.

Fixes #7571

Signed-off-by: Jukka Rissanen <jukka.rissanen@linux.intel.com>
