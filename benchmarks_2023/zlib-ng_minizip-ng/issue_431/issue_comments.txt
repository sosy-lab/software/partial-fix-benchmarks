macOS and files over 4GB in Finder-created zips
Do you have an example ZIP file?
Sure, here's a zip file with a 5 GB random file (and two small ones) inside, created with Finder:

https://www.dropbox.com/s/lz2t58ai6gaadlt/MacZipDemo-dev-urandom.zip?dl=0

Interestingly enough, if the 5 GB file is all zeroes (and gets compressed nicely, to just 5 MB), the extraction works well; sample file:

https://www.dropbox.com/s/m1sa5avgihm7ssh/MacZipDemo-dev-zero.zip?dl=0

I have made some changes that should fix it in `dev` branch. It will attempt to recover the zip entries in a 4gb zip file that contains no zip64 information.
Thanks for the patch. I've tried recovering the first sample file linked above, with minizip built from `dev`, but with the same error -103 as the result.
I tested against this [file](https://uce9d58206b68256e605b4113dc2.dl.dropboxusercontent.com/cd/0/get/Aqcbm4ciSUvY5bSgiY4-xdmBzjtzXNAlYY0xKnnWJjxyjvltRacYmwhRS2O_ccnx0ZEoMv3sEGj1X9ZfyEQtIBVgoYI5DylwIC133p6W5J3bQA/file?_download_id=2417371403843647009487183659893191861833529124673258736818204395&_notify_domain=www.dropbox.com&dl=1).
It is working fine for me even with your test file. Not getting -103. I suggest you try and debug the source code and see what the issue is on your end.
Must have been an error on my side - now extraction works on both Mac and Windows. Thanks again.
Found another problem. I'm testing on a zip archive with lots of files inside, among them a single large (> 4 GB) file, created with Finder.

Situation:
- commit a44fae3b0cffed80f735e4333e9e38c9e3b305b2 (before the first 431 fix) - extracts up to the big file, then fails with -103 (as expected);
- commits be483d8b6ed86ae3b5e5b67ff2ea18c77bc0a4e4, 28d87dd8b8f75c44069d4e524c44c0aced1be844 - fails with -105 on a regular small file (many files before the big file where it failed originally);
- commits 545d729594ce788d3bbe90fa3505228b144e12bd, 75741f4b2cd014a2b368d9103cb9d7347092b29e - fails the same, with -105, but on a different file, also way before the big file (so behavior has changed after the second 431 fix).

The failure happens in `mz_zip_entry_read_close()`, on the `zip->entry_crc32 != zip->file_info.crc` check.

I can't share the test zip file in question, but if I manage to create a sample one, I'll post it. This is on a Mac, `cmake -G Xcode .` and using the resulting `minizip -x -d <...> <...zip>`.

OK, here's a sample zip file - https://www.dropbox.com/s/pjb2v6s4ltblzu3/protobuf.zip?dl=0

Basically a clone of https://github.com/protocolbuffers/protobuf with the 5 GB `...-dev-urandom.zip` file placed inside `protobuf/cmake` and compressed with Finder.

Fails at 
`...`
`Extracting protobuf/benchmarks/util/__init__.py`
`Error -105 saving entries to disk /.../.../protobuf.zip`

Tested with the current `HEAD` of `dev` (75741f4b2cd014a2b368d9103cb9d7347092b29e).
@supersv do you think that you could come up with a fix?
@Coeur Let's hear Nathan's verdict first.
macOS archive utility doesn't write zip files that are 4gb properly with ZIP64 extension. This is a long time known mac bug.
@nmoinvaz in that case, I guess I don't need to wait for a fix for that. Do you know how close we are from a minizip 2.9.1 release? I plan to update minizip once you'll be making a release.
I was hoping to get to look at these two issues that are open before I do. Maybe I can get to it by next week. I'm currently working on incorporating code coverage into the build system.
@supersv the zip file is no longer available..
@nmoinvaz Try this link: https://www.dropbox.com/sh/me9lzpe1lbnpb8u/AAClsrIrjw4QWJxNf8zx1UsFa?dl=0
I think I have made a change that could possibly fix it so please test. But the problem is that your zip file contains zip files which make it hard to determine where some entries start and end.
@nmoinvaz Seems to work like a charm. Thanks and congrats!