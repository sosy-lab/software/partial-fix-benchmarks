diff --git a/pkg/base1/test-file.js b/pkg/base1/test-file.js
index eab74cc5068..5383353414a 100644
--- a/pkg/base1/test-file.js
+++ b/pkg/base1/test-file.js
@@ -65,6 +65,17 @@ QUnit.test("simple replace", async assert => {
     await cockpit.file(dir + "/bar").replace("4321\n");
     const res = await cockpit.spawn(["cat", dir + "/bar"]);
     assert.equal(res, "4321\n", "correct content");
+    // keeps existing permissions; as we don't want to assume an umask, test with a narrow and a wide scenario
+    // we cannot test owner changes here, that's done in integration tests
+    for (const mode of ["600", "777"]) {
+        await cockpit.spawn(["chmod", mode, dir + "/bar"]);
+        const content = `"new-${mode}\n`;
+        await cockpit.file(dir + "/bar").replace(content);
+        const res = await cockpit.spawn(["cat", dir + "/bar"]);
+        assert.equal(res, content, "correct content after overwriting");
+        const stat = await cockpit.spawn(["stat", "-c", "%a", dir + "/bar"]);
+        assert.equal(stat.trim(), mode, "correct permissions");
+    }
 });
 
 QUnit.test("stringify replace", async assert => {
diff --git a/src/bridge/cockpitfsreplace.c b/src/bridge/cockpitfsreplace.c
index bde3ee12008..5042ddd8fbc 100644
--- a/src/bridge/cockpitfsreplace.c
+++ b/src/bridge/cockpitfsreplace.c
@@ -177,11 +177,30 @@ cockpit_fsreplace_control (CockpitChannel *channel,
             {
               new_tag = cockpit_get_file_tag (self->tmp_path);
               json_object_set_string_member (options, "tag", new_tag);
+
+              // if the file exists already, record its permissions
+              struct stat st;
+              gboolean has_stat = stat (self->path, &st) == 0;
+
               if (rename (self->tmp_path, self->path) < 0)
                 {
                   close_with_errno (self, "couldn't rename", errno);
                   goto out;
                 }
+
+              // restore original permissions
+              if (has_stat) {
+                if (chmod (self->path, st.st_mode) < 0)
+                  {
+                    close_with_errno (self, "couldn't chmod", errno);
+                    goto out;
+                  }
+                if (chown (self->path, st.st_uid, st.st_gid) < 0)
+                  {
+                    close_with_errno (self, "couldn't chown", errno);
+                    goto out;
+                  }
+              }
             }
         }
     }
diff --git a/src/cockpit/channels/filesystem.py b/src/cockpit/channels/filesystem.py
index c593a373272..2b9b87544cb 100644
--- a/src/cockpit/channels/filesystem.py
+++ b/src/cockpit/channels/filesystem.py
@@ -188,12 +188,23 @@ def do_done(self):
             if self._tag and self._tag != tag_from_path(self._path):
                 raise ChannelError('change-conflict')
 
+            # if the file exists already, record its permissions
+            try:
+                stat = os.stat(self._path)
+            except (FileNotFoundError, PermissionError):
+                stat = None
+
             try:
                 os.rename(self._temppath, self._path)
             except OSError:
                 # ensure to not leave the temp file behind
                 self.unlink_temppath()
                 raise
+            # restore original permissions
+            if stat is not None:
+                os.chmod(self._path, stat.st_mode)
+                os.chown(self._path, stat.st_uid, stat.st_gid)
+
             self._tempfile.close()
             self._tempfile = None
 
diff --git a/test/verify/check-shell-keys b/test/verify/check-shell-keys
index cf9104f0804..57eb013bea3 100755
--- a/test/verify/check-shell-keys
+++ b/test/verify/check-shell-keys
@@ -97,17 +97,20 @@ class TestKeys(MachineCase):
 
         b.wait_in_text("#account-authorized-keys", "no authorized public keys")
 
+        def check_perms():
+            perms = m.execute("getfacl -a /home/user/.ssh")
+            self.assertIn("owner: user", perms)
+
+            perms = m.execute("getfacl -a /home/user/.ssh/authorized_keys")
+            self.assertIn("owner: user", perms)
+            self.assertIn("user::rw-", perms)
+            self.assertIn("group::---", perms)
+            self.assertIn("other::---", perms)
+
         # Adding keys sets permissions properly
         b.wait_text("#account-user-name", "user")
         add_key(KEY, FP_SHA256, "test-name")
-        perms = m.execute("getfacl -a /home/user/.ssh")
-        self.assertIn("owner: user", perms)
-
-        perms = m.execute("getfacl -a /home/user/.ssh/authorized_keys")
-        self.assertIn("owner: user", perms)
-        self.assertIn("user::rw-", perms)
-        self.assertIn("group::---", perms)
-        self.assertIn("other::---", perms)
+        check_perms()
 
         b.wait_js_func("ph_count_check", "#account-authorized-keys-list tr", 1)
 
@@ -122,6 +125,8 @@ class TestKeys(MachineCase):
         b.wait_js_func("ph_count_check", "#account-authorized-keys-list tr", 1)
         data = m.execute("cat /home/user/.ssh/authorized_keys")
         self.assertEqual(data, KEY + '\n')
+        # Permissions are still ok
+        check_perms()
         b.logout()
 
         # User can still see their keys
