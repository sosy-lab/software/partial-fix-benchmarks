libssh: use sftp_get_error output in printed error message

Fixes #2141

Signed-off-by: Nikos Mavrogiannopoulos <nmav@gnutls.org>
libssh: corrected use of sftp_statvfs() in SSH_SFTP_QUOTE_STATVFS

The previous code was incorrectly following the libssh2 error detection
for libssh2_sftp_statvfs, which is not correct for libssh's sftp_statvfs.

Fixes #2142

Signed-off-by: Nikos Mavrogiannopoulos <nmav@gnutls.org>
libssh: corrected use of sftp_statvfs() in SSH_SFTP_QUOTE_STATVFS

The previous code was incorrectly following the libssh2 error detection
for libssh2_sftp_statvfs, which is not correct for libssh's sftp_statvfs.

Fixes #2142

Signed-off-by: Nikos Mavrogiannopoulos <nmav@gnutls.org>
libssh: corrected use of sftp_statvfs() in SSH_SFTP_QUOTE_STATVFS

The previous code was incorrectly following the libssh2 error detection
for libssh2_sftp_statvfs, which is not correct for libssh's sftp_statvfs.

Fixes #2142

Signed-off-by: Nikos Mavrogiannopoulos <nmav@gnutls.org>
libssh: fixed dereference in statvfs access

The behavior is now equivalent to ssh.c when SSH_SFTP_QUOTE_STATVFS
handling fails.

Fixes #2142
