Undo BSTstackLib warning (#1758)

Undo #1751
Only adjust SPI format when it changes

Avoid potential interaction with Pico SDK 1.5.1 update that causes hiccups in
SPI transmissions.

Fixes #1760
Avoid calling spi_set_format during transactions (#1762)

Avoid potential interaction with Pico SDK 1.5.1 update that causes hiccups in
SPI transmissions.

Fixes #1760
Avoid calling spi_set_format during SPI transfers (#1762)

Avoid potential interaction with Pico SDK 1.5.1 update that causes hiccups in
SPI transmissions.  SPI.transfer16 to use 8-bit transfers.

Fixes #1760
