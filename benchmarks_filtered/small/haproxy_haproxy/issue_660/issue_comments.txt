src/server.c: null pointer dereference and leak suspected by coverity
I definitely see the leak when facing an error in the global state file, since we jump there without closing f. However I'm not sure about the NULL deref, I think it's on filepath, but it's not clear to me how to arrive there with it. CCing @bedis since he's definitely the one who knows this best.
here's longer report

```
3143        globalfilepathlen = 0;
3144        /* create the globalfilepath variable */
    1. Condition global.server_state_file, taking true branch.
3145        if (global.server_state_file) {
3146                /* absolute path or no base directory provided */
    2. Condition global.server_state_file[0] == '/', taking true branch.
3147                if ((global.server_state_file[0] == '/') || (!global.server_state_base)) {
3148                        len = strlen(global.server_state_file);
    3. Condition len > 4096, taking true branch.
3149                        if (len > MAXPATHLEN) {
3150                                globalfilepathlen = 0;
    4. Jumping to label globalfileerror.
3151                                goto globalfileerror;
3152                        }
3153                        memcpy(globalfilepath, global.server_state_file, len);
3154                        globalfilepath[len] = '\0';
3155                        globalfilepathlen = len;
3156                }
3157                else if (global.server_state_base) {
3158                        len = strlen(global.server_state_base);
3159                        if (len > MAXPATHLEN) {
3160                                globalfilepathlen = 0;
3161                                goto globalfileerror;
3162                        }
3163                        memcpy(globalfilepath, global.server_state_base, len);
3164                        globalfilepath[len] = 0;
3165                        globalfilepathlen = len;
3166
3167                        /* append a slash if needed */
3168                        if (!globalfilepathlen || globalfilepath[globalfilepathlen - 1] != '/') {
3169                                if (globalfilepathlen + 1 > MAXPATHLEN) {
3170                                        globalfilepathlen = 0;
3171                                        goto globalfileerror;
3172                                }
3173                                globalfilepath[globalfilepathlen++] = '/';
3174                        }
3175
3176                        len = strlen(global.server_state_file);
3177                        if (globalfilepathlen + len > MAXPATHLEN) {
3178                                globalfilepathlen = 0;
3179                                goto globalfileerror;
3180                        }
3181                        memcpy(globalfilepath + globalfilepathlen, global.server_state_file, len);
3182                        globalfilepathlen += len;
3183                        globalfilepath[globalfilepathlen++] = 0;
3184                }
3185        }
3186 globalfileerror:
    5. Condition globalfilepathlen == 0, taking true branch.
3187        if (globalfilepathlen == 0)
3188                globalfilepath[0] = '\0';
3189
3190        /* Load global server state in a tree */
    6. Condition globalfilepathlen > 0, taking false branch.
3191        if (globalfilepathlen > 0) {
3192                errno = 0;
3193                f = fopen(globalfilepath, "r");
3194                if (errno)
3195                        ha_warning("Can't open global server state file '%s': %s\n", globalfilepath, strerror(errno));
3196                if (!f)
3197                        goto out_load_server_state_in_tree;
3198
3199                global_file_version = srv_state_get_version(f);
3200                if (global_file_version == 0)
3201                        goto out_load_server_state_in_tree;
3202
3203                while (fgets(mybuf, SRV_STATE_LINE_MAXLEN, f)) {
3204                        line = NULL;
3205
3206                        line = strdup(mybuf);
3207                        if (line == NULL)
3208                                continue;
3209
3210                        srv_state_parse_line(mybuf, global_file_version, params, srv_params);
3211                        if (params[0] == NULL)
3212                                goto nextline;
3213
3214                        /* bkname */
3215                        bkname = params[1];
3216                        /* srvname */
3217                        srvname = params[3];
3218
3219                        /* key */
3220                        chunk_printf(&trash, "%s %s", bkname, srvname);
3221
3222                        /* store line in tree */
3223                        st = calloc(1, sizeof(*st) + trash.data + 1);
3224                        if (st == NULL) {
3225                                goto nextline;
3226                        }
3227                        memcpy(st->name_name.key, trash.area, trash.data + 1);
3228                        if (ebst_insert(&state_file, &st->name_name) != &st->name_name) {
3229                                /* this is a duplicate key, probably a hand-crafted file,
3230                                 * drop it!
3231                                 */
3232                                goto nextline;
3233                        }
3234
3235                        /* save line */
3236                        st->line = line;
3237
3238                        continue;
3239
3240 nextline:
3241                        /* free up memory in case of error during the processing of the line */
3242                        free(line);
3243                }
3244        }
3245 out_load_server_state_in_tree:
3246
3247        /* parse all proxies and load states form tree (global file) or from local file */
    7. Condition curproxy != NULL, taking true branch.
    10. Condition curproxy != NULL, taking true branch.
    13. Condition curproxy != NULL, taking true branch.
    18. Condition curproxy != NULL, taking true branch.
3248        for (curproxy = proxies_list; curproxy != NULL; curproxy = curproxy->next) {
3249                /* servers are only in backends */
    8. Condition !(curproxy->cap & 2), taking true branch.
    11. Condition !(curproxy->cap & 2), taking true branch.
    14. Condition !(curproxy->cap & 2), taking false branch.
    19. Condition !(curproxy->cap & 2), taking false branch.
3250                if (!(curproxy->cap & PR_CAP_BE))
    9. Continuing loop.
    12. Continuing loop.
3251                        continue;
3252                fileopenerr = 0;
    20. assign_zero: Assigning: filepath = NULL.
3253                filepath = NULL;
3254
3255                /* search server state file path and name */
    15. Switch case value PR_SRV_STATE_FILE_GLOBAL.
    21. Switch case value PR_SRV_STATE_FILE_LOCAL.
3256                switch (curproxy->load_server_state_from_file) {
3257                        /* read servers state from global file */
3258                        case PR_SRV_STATE_FILE_GLOBAL:
3259                                /* there was an error while generating global server state file path */
    16. Condition globalfilepathlen == 0, taking true branch.
3260                                if (globalfilepathlen == 0)
    17. Continuing loop.
3261                                        continue;
3262                                filepath = globalfilepath;
3263                                fileopenerr = 1;
3264                                break;
3265                        /* this backend has its own file */
3266                        case PR_SRV_STATE_FILE_LOCAL:
3267                                localfilepathlen = 0;
3268                                localfilepath[0] = '\0';
3269                                len = 0;
3270                                /* create the localfilepath variable */
3271                                /* absolute path or no base directory provided */
    22. Condition curproxy->server_state_file_name[0] == '/', taking true branch.
3272                                if ((curproxy->server_state_file_name[0] == '/') || (!global.server_state_base)) {
3273                                        len = strlen(curproxy->server_state_file_name);
    23. Condition len > 4096, taking true branch.
3274                                        if (len > MAXPATHLEN) {
3275                                                localfilepathlen = 0;
    24. Jumping to label localfileerror.
3276                                                goto localfileerror;
3277                                        }
3278                                        memcpy(localfilepath, curproxy->server_state_file_name, len);
3279                                        localfilepath[len] = '\0';
3280                                        localfilepathlen = len;
3281                                }
3282                                else if (global.server_state_base) {
3283                                        len = strlen(global.server_state_base);
3284                                        localfilepathlen += len;
3285
3286                                        if (localfilepathlen > MAXPATHLEN) {
3287                                                localfilepathlen = 0;
3288                                                goto localfileerror;
3289                                        }
3290                                        memcpy(localfilepath, global.server_state_base, len);
3291                                        localfilepath[localfilepathlen] = 0;
3292
3293                                        /* append a slash if needed */
3294                                        if (!localfilepathlen || localfilepath[localfilepathlen - 1] != '/') {
3295                                                if (localfilepathlen + 1 > MAXPATHLEN) {
3296                                                        localfilepathlen = 0;
3297                                                        goto localfileerror;
3298                                                }
3299                                                localfilepath[localfilepathlen++] = '/';
3300                                        }
3301
3302                                        len = strlen(curproxy->server_state_file_name);
3303                                        if (localfilepathlen + len > MAXPATHLEN) {
3304                                                localfilepathlen = 0;
3305                                                goto localfileerror;
3306                                        }
3307                                        memcpy(localfilepath + localfilepathlen, curproxy->server_state_file_name, len);
3308                                        localfilepathlen += len;
3309                                        localfilepath[localfilepathlen++] = 0;
3310                                }
3311                                filepath = localfilepath;
3312 localfileerror:
    25. Condition localfilepathlen == 0, taking true branch.
3313                                if (localfilepathlen == 0)
3314                                        localfilepath[0] = '\0';
3315
    26. Breaking from switch.
3316                                break;
3317                        case PR_SRV_STATE_FILE_NONE:
3318                        default:
3319                                continue;
3320                }
3321
3322                /* when global file is used, we get data from the tree
3323                 * Note that in such case we don't check backend name neither uuid.
3324                 * Backend name can't be wrong since it's used as a key to retrieve the server state
3325                 * line from the tree.
3326                 */
    27. Condition curproxy->load_server_state_from_file == PR_SRV_STATE_FILE_GLOBAL, taking false branch.
3327                if (curproxy->load_server_state_from_file == PR_SRV_STATE_FILE_GLOBAL) {
3328                        struct server *srv = curproxy->srv;
3329                        while (srv) {
3330                                struct ebmb_node *node;
3331                                struct state_line *st;
3332
3333                                chunk_printf(&trash, "%s %s", curproxy->id, srv->id);
3334                                node = ebst_lookup(&state_file, trash.area);
3335                                if (!node)
3336                                        goto next;
3337
3338                                st = container_of(node, struct state_line, name_name);
    CID 1402202: Copy into fixed size buffer (STRING_OVERFLOW) [select issue]
3339                                memcpy(mybuf, st->line, strlen(st->line));
3340                                mybuf[strlen(st->line)] = 0;
3341
3342                                srv_state_parse_line(mybuf, global_file_version, params, srv_params);
3343                                if (params[0] == NULL)
3344                                        goto next;
3345
3346                                srv_update_state(srv, global_file_version, srv_params);
3347
3348 next:
3349                                srv = srv->next;
3350                        }
3351
3352                        continue; /* next proxy in list */
3353                }
3354                else {
3355                        /* load 'local' state file */
3356                        errno = 0;
    CID 1327881 (2): Resource leak (RESOURCE_LEAK) [select issue]
    CID 1327883 (#1 of 1): Explicit null dereferenced (FORWARD_NULL)28. var_deref_model: Passing null pointer filepath to fopen, which dereferences it.
3357                        f = fopen(filepath, "r");
```
I guess it is when the local file length exceeds MAXPATHLEN. When it happens, filepath variable is never set.
No, it's at step 20, there's an explicit null. But there's also a possible buffer overflow at line 3338.

patch sent to ML: https://www.mail-archive.com/haproxy@formilux.org/msg37754.html
Ilya, I remember I glanced over it a few days ago and switched to something else because there's no info in your commit message. It doesn't explain what the problem is, your analysis nor the solution you've chosen and why you think it's the best. I know I sound like a parrot, but commit messages are far more important than the code. Everyone could write the code from a good commit message, the opposite is not true. And sadly, more than 50% of the development time is spent trying to reverse engineer old code whose impacts were not clearly stated.

In addition, your work really is the analysis more than moving a few lines around, in 6 months you'll have forgotten everything if someone asks you about it. The time you spent on this deserves a better fate.

Please just run "git log --grep BUG" to get some inspiration if needed, but we really need the details. If in doubt, always keep in mind that you should see it as trying to sell me your patch, you need to advertise all its merits.

Thanks!
I used [the principle of least action](https://en.wikipedia.org/wiki/Principle_of_least_action)  :)

I will add more text in "v2" patch
Thank you!

I adjusted commit message

https://www.mail-archive.com/haproxy@formilux.org/msg37908.html