win32: Download deps during installation
Fixes #836.  In gtk_xtext_scroll() always force line integral line boundary.
Fixes #836.  Move to line boundary in _scroll{down,up}_timeout() not _scroll().
