Segfault in  janus_ice_nacked_packet_cleanup
@lminiero @atoppi can you reopen? We've observed a variant of this again in production (worse because it's an invalid but non-NULL pointer):
```
gdb) bt
#0  0x000055fcf52c46cf in janus_ice_nacked_packet_cleanup (user_data=0x7f86dc01c320) at ice.c:334
#1  0x00007f86ffc27d03 in ?? () from /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0
#2  0x00007f86ffc27285 in g_main_context_dispatch () from /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0
#3  0x00007f86ffc27650 in ?? () from /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0
#4  0x00007f86ffc27962 in g_main_loop_run () from /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0
#5  0x000055fcf52c2aab in janus_ice_static_event_loop_thread (data=0x55fcf645ba60) at ice.c:134
#6  0x00007f86ffc4f195 in ?? () from /usr/lib/x86_64-linux-gnu/libglib-2.0.so.0
#7  0x00007f86fe4076db in start_thread (arg=0x7f86f5813700) at pthread_create.c:463
#8  0x00007f86fe13088f in clone () at ../sysdeps/unix/sysv/linux/x86_64/clone.S:95

(gdb) print pkt->handle->stream
$1 = (janus_ice_stream *) 0xa25cd7ff242f8746
(gdb) print *(pkt->handle->stream)
Cannot access memory at address 0xa25cd7ff242f8746
```
IIUC, it seems like when the `janus_ice_nacked_packet_cleanup` may need to either be cancelled or its data invalidated (as it may be holding a dangling pointer). I'm not really clear on the chain of events leading to this though.

It may help to point out that this surfaced when using 8 ICE event loops.
@tmatth can you check the PR above?
@lminiero confirmed that this fixes the crash.