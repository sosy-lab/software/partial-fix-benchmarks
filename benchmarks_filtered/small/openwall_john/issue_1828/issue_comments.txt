Mask mode: default mask vs. format's max length.
@Sayantan2048 please review 76a41c41. It might be some cleaner way to implement this. The problem was that default mask was ?1?2?2?2?2?2?2?3?3?3?3?d?d?d?d and the last four ?d?d?d?d were picked as "GPU-side". Only then, truncation removed them. My hack fix truncates it earlier in the process but I'm not sure it's very well written as-is.

That hack fix is not enough when using a length range with `-min-len=x -max-len=y`

I thought this was a regression but the problem is only with _GPU-side_ truncation.

These work (truncating or stretching, not GPU-side):

```
$ ../run/john -stdout -mask -min-len=1 -max-len=5

$ ../run/john -stdout -mask=?d -min-len=1 -max-len=5
```

This doesn't (truncating, GPU-side):

```
$ ../run/john scraped.sam -form:nt-opencl -mask -min-len=4 -max-len=6
```

That hack fix did more bad than good. Reverted now.

@magnumripper , can you run a `../run/john -form:nt-opencl --mask --test` on super or well?

```
[magnum@super src]$ ../run/john -form:nt-opencl --mask --test
Device 6: GeForce GTX TITAN X
Benchmarking: nt-opencl, NT [MD4 OpenCL]... Format internal ranges cannot be truncated!
Use a bigger key length or non-gpu format.

[magnum@super src]$ ../run/john -form:nt-opencl --mask:?a?a?a --test
Device 6: GeForce GTX TITAN X
Benchmarking: nt-opencl, NT [MD4 OpenCL]... DONE, GPU util:97%
Raw:    11311M c/s real, 11199M c/s virtual
```

The problem when you don't specify a mask is that the default mask is `?1?2?2?2?2?2?2?3?3?3?3?d?d?d?d`. GPU-side mask mode picks ?d?d?d?d in the end, but then the mask is truncated to length 8. The fix would obviously be to pick GPU-side mask _after_ truncation.

This bug died of age (some other change fixed it).

Are you sure?

```
$ ../run/john | head -n 3
John the Ripper 1.8.0-jumbo-1-5103-g393456a+ OMP [linux-gnu 64-bit AVX-ac]
Copyright (c) 1996-2016 by Solar Designer and others
Homepage: http://www.openwall.com/john/

super src]$ ../run/john -form:nt-opencl --mask --test 
Device 6: GeForce GTX TITAN X
Benchmarking: nt-opencl, NT [MD4 OpenCL]... Format internal ranges cannot be truncated!
Use a bigger key length or non-gpu format.
```

I tested exactly that!? Perhaps I got different GPU-side size because I used a weaker device.

(Confimation neeed) I am almost sure it used to work on my deceased computer with 6770. 
- The final mask/cut is calculated based on the computer and device in use (?).

Apparently we can't coun't on Sayantan doing any more work. We need to dig in to all these issues and try to fix them...
