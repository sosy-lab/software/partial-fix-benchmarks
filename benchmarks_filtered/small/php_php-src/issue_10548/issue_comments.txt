copy() fails on cifs mounts because of incorrect length (cfr_max) specified in streams.c:1584 copy_file_range()
~~Could be fixed by #10440.~~
EDIT: nevermind I misunderstood the issue, sorry (when I looked at the length of the strace I immediately thought about the corruption case but this one is different, I should've looked more carefully).
Your methodology to fix this issue sounds correct. You can probably use `php_stream_state` instead of `fstat` to support all kinds of streams of PHP (and if that call fails by returning -1, then you can skip the file_copy_range I guess). If you have a patch feel free to PR it :)
I would love to write a patch, but I am not sure if I am able to do it properly. The main problem here is that PHP uses a length argument with copy_file_range() that is larger than the source file size.

...so as long as PHP can ensure to reduce the length to the source size it would be good, and I guess combining it with looping through copy_file_range() until every byte is copied as proposed in #10440 would be the correct solution here.

Can anyone help out to make this happen?
I can put it on my todo list for this week, unless someone beats me to it
> I can put it on my todo list for this week, unless someone beats me to it

Fantastic!
Thanks again!

Does this mean that PHP will always try to copy_file_range() on cifs with length cfr_max, fail with EIO and then fall back to "classic copying"? I like that it will work, which is the most important improvement for me (so Wordpress can update itself in an Azure Web App).

But will this also mean that copies will never be made server side, resulting in PHP reading the source file over the network and writing it back over the network? If so, that sounds unfortunate for those who want to copy larger files over cifs, or those who have limited network bandwidth for such operations. I am no expert, but is it that bad to use a stat call, even it is cached, since it will fall back to classic copying if it fails anyway? I mean there is also already code for "there may be more data; continue copying using the fallback code below" as well.
> Does this mean that PHP will always try to copy_file_range() on cifs with length cfr_max, fail with EIO and then fall back to "classic copying"? I like that it will work, which is the most important improvement for me (so Wordpress can update itself in an Azure Web App).

Yes. Actually, I did find that it depends on the server. On my testing I found that I could specify a file length larger than the max file length and it worked properly without EIO.

> But will this also mean that copies will never be made server side, resulting in PHP reading the source file over the network and writing it back over the network?

Depends on the configuration.
> If so, that sounds unfortunate for those who want to copy larger files over cifs, or those who have limited network bandwidth for such operations. I am no expert, but is it that bad to use a stat call, even it is cached, since it will fall back to classic copying if it fails anyway? I mean there is also already code for "there may be more data; continue copying using the fallback code below" as well.

Hmm that could work even in the case of getting a too small size or having races because we now *do* fallback in case of EIO.
@arnaud-lb It might make sense to extend the current approach with a stat call. In case of having a size less than the actual file size, the code will fall back. In case the size is too large and CIFS complains then EIO will be hit and the fallback will be taken. In case of races the size is wrong so we again end up in the fallback case. So I think it would work. What do you think?
Yes, this seems sensible
Thank you so much for your efforts! 