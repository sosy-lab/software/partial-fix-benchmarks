use ::pdwindow::font_size for the PdWindow font-size

hopefully this is going to be more easy to find than some hardcoded value deep down.
Have the active outlet-area spread over the entire object-width

similar to how inlets are handled.
so if you start dragging from "somewhere" (x-wise) at the bottom of the object,
this will start a connection from the the nearest outlet,
even if you were not directly above the outlet.

this also fixes the start-position of the the connection-line to match the
start position of the finalized connection.

Related: https://github.com/pure-data/pure-data/issues/1926
Make the active region for an outlet larger (so it's easier to hit)

originally the active height for an outlet was visOutHeight+1.
now it is up to visOutHeight*2, but no larger than 1/4 of the entire object height.
the old active height is a lower boundary.

Closes: https://github.com/pure-data/pure-data/issues/1926
Have the active outlet-area spread over the entire object-width

similar to how inlets are handled.
so if you start dragging from "somewhere" (x-wise) at the bottom of the object,
this will start a connection from the the nearest outlet,
even if you were not directly above the outlet.

this also fixes the start-position of the the connection-line to match the
start position of the finalized connection.

Related: https://github.com/pure-data/pure-data/issues/1926
Make the active region for an outlet larger (so it's easier to hit)

originally the active height for an outlet was visOutHeight+1.
now it is up to visOutHeight*2, but no larger than 1/4 of the entire object height.
the old active height is a lower boundary.

Closes: https://github.com/pure-data/pure-data/issues/1926
Make the active region for an outlet larger (so it's easier to hit)

originally the active height for an outlet was visOutHeight+1.
now it is up to visOutHeight*2, but no larger than 1/4 of the entire object height.
the old active height is a lower boundary.

Closes: https://github.com/pure-data/pure-data/issues/1926
