Fix AppVeyor badge
Fix #4190 - Scrolling beyond zero
Fix #4190 - Scroll below 0 in cursor mode not jumping to -1
