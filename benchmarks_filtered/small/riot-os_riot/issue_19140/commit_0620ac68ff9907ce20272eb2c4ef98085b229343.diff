diff --git a/tests/rmutex/Makefile.ci b/tests/rmutex/Makefile.ci
index 67fd4b10386e..1d21bb590056 100644
--- a/tests/rmutex/Makefile.ci
+++ b/tests/rmutex/Makefile.ci
@@ -5,19 +5,10 @@ BOARD_INSUFFICIENT_MEMORY := \
     arduino-uno \
     atmega328p \
     atmega328p-xplained-mini \
-    bluepill-stm32f030c8 \
-    i-nucleo-lrwan1 \
-    nucleo-f030r8 \
     nucleo-f031k6 \
     nucleo-f042k6 \
     nucleo-l011k4 \
-    nucleo-l031k6 \
-    nucleo-l053r8 \
     samd10-xmini \
-    slstk3400a \
     stk3200 \
     stm32f030f4-demo \
-    stm32f0discovery \
-    stm32g0316-disco \
-    stm32l0538-disco \
     #
diff --git a/tests/rmutex/README.md b/tests/rmutex/README.md
index 55854b418f78..9f23de463602 100644
--- a/tests/rmutex/README.md
+++ b/tests/rmutex/README.md
@@ -5,7 +5,8 @@ When successful, you should see 5 different threads printing their
 PID, priority and recursion depth. The thread with the lowest priority
 should be able to lock (and unlock) the mutex first, followed by the
 other threads in the order of their priority (highest next). If two
-threads have the same priority the lower thread id should acquire the
+threads have the same priority the thread who comes first in the run queue
+(in this test this is the one with the lower thread id) should acquire the
 lock. The output should look like the following:
 
 ```
diff --git a/tests/rmutex/main.c b/tests/rmutex/main.c
index deca4cccd8cc..88b5830a7bf3 100644
--- a/tests/rmutex/main.c
+++ b/tests/rmutex/main.c
@@ -18,6 +18,8 @@
  * @}
  */
 
+#include <stdint.h>
+#include <inttypes.h>
 #include <stdio.h>
 
 #include "rmutex.h"
@@ -25,38 +27,39 @@
 
 #define THREAD_NUMOF            (5U)
 
-static char stacks[THREAD_NUMOF][THREAD_STACKSIZE_MAIN];
+static char stacks[THREAD_NUMOF][THREAD_STACKSIZE_SMALL + THREAD_EXTRA_STACKSIZE_PRINTF];
 
-static const char prios[THREAD_NUMOF] = {THREAD_PRIORITY_MAIN - 1, 4, 5, 2, 4};
-static const char depth[THREAD_NUMOF] = {5, 3, 3, 4, 5};
+static const uint8_t prios[THREAD_NUMOF] = {THREAD_PRIORITY_MAIN - 1, 4, 5, 2, 4};
+static const uint8_t depth[THREAD_NUMOF] = {5, 3, 3, 4, 5};
 
 static rmutex_t testlock;
 
-static void lock_recursive(char n, char depth)
+static void lock_recursive(uint8_t current_depth, uint8_t max_depth)
 {
     thread_t *t = thread_get_active();
 
     printf("T%i (prio %i, depth %i): trying to lock rmutex now\n",
-        (int)t->pid, (int)t->priority, (int)n);
+        (int)t->pid, (int)t->priority, (int)current_depth);
     rmutex_lock(&testlock);
 
     printf("T%i (prio %i, depth %i): locked rmutex now\n",
-           (int)t->pid, (int)t->priority, (int)n);
+           (int)t->pid, (int)t->priority, (int)current_depth);
 
-    if (n + 1 < depth)
-        lock_recursive(n + 1, depth);
+    if (current_depth + 1 < max_depth) {
+        lock_recursive(current_depth + 1, max_depth);
+    }
 
     thread_yield();
 
     rmutex_unlock(&testlock);
 
     printf("T%i (prio %i, depth %i): unlocked rmutex\n",
-           (int)t->pid, (int)t->priority, (int)n);
+           (int)t->pid, (int)t->priority, (int)current_depth);
 }
 
 static void *lockme(void *arg)
 {
-    intptr_t depth = (intptr_t)arg;
+    uintptr_t depth = (uintptr_t)arg;
 
     lock_recursive(0, depth);
 
@@ -75,7 +78,7 @@ int main(void)
     /* create threads */
     for (unsigned i = 0; i < THREAD_NUMOF; i++) {
         thread_create(stacks[i], sizeof(stacks[i]), prios[i], 0,
-                    lockme, (void*)(intptr_t)depth[i], "t");
+                      lockme, (void*)(intptr_t)depth[i], "t");
     }
     /* allow threads to lock the mutex */
     printf("main: unlocking recursive mutex\n");
