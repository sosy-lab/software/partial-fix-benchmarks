maintain ChangeLog
mmnormalize bugfix: if msg cannot be parsed, parser chain is stopped

When an parser is not able to parse a message, it should indicate this
to rsyslog core, which then activates the next parser(s) inside the
configured parser chain.

Unfortunatley, mmnormalize always tells core "success", and so no
other parsers are activated.

closes https://github.com/rsyslog/rsyslog/issues/5148
mmnormalize bugfix: if msg cannot be parsed, parser chain is stopped

When an parser is not able to parse a message, it should indicate this
to rsyslog core, which then activates the next parser(s) inside the
configured parser chain.

Unfortunatley, mmnormalize always tells core "success", and so no
other parsers are activated.

closes https://github.com/rsyslog/rsyslog/issues/5148
