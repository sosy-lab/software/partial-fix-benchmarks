nvmf/rdma: Fix search for req to abort when SRQ enabled

When SRQ is enabled, all qpairs share common pool of
rdma requests and we should check that rdma_req which
cid matches belongs to the correct qpair.

Signed-off-by: Alexey Marchuk <alexeymar@mellanox.com>
Change-Id: I2505ca39c5110e8e67a48abbe34b6df9ff00229f
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/6110
Community-CI: Broadcom CI
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Ziye Yang <ziye.yang@intel.com>
Reviewed-by: Michael Haeuptle <michaelhaeuptle@gmail.com>
Reviewed-by: Shuhei Matsumoto <shuhei.matsumoto.xt@hitachi.com>
Reviewed-by: Ben Walker <benjamin.walker@intel.com>
nvmf/rdma: Handle CQ resize for iWARP transport

Since iWARP doesn't support CQ resize, we can't
accept new qpairs when CQ size is insufficient.
In other case if we accept new qpairs we can
et CQ overrun and receive IBV_EVENT_CQ_ERR ibv
async event. In that case we will have to close
all qpairs that use this CQ.

Part of the fix for issue #1747

Signed-off-by: Alexey Marchuk <alexeymar@mellanox.com>
Change-Id: I30b9163a7c8abb8651f8813c6bbdb80000741c9c
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/6106
Community-CI: Broadcom CI
Community-CI: Mellanox Build Bot
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-by: Ziye Yang <ziye.yang@intel.com>
nvmf/rdma: Handle CQ resize for iWARP transport

Since iWARP doesn't support CQ resize, we can't
accept new qpairs when CQ size is insufficient.
In other case if we accept new qpairs we can
et CQ overrun and receive IBV_EVENT_CQ_ERR ibv
async event. In that case we will have to close
all qpairs that use this CQ.

Part of the fix for issue #1747

Signed-off-by: Alexey Marchuk <alexeymar@mellanox.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/6106 (master)

(cherry picked from commit 4a2c27f717b545711b8316b585240a0f73d84af9)
Change-Id: I30b9163a7c8abb8651f8813c6bbdb80000741c9c
Signed-off-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/6179
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Aleksey Marchuk <alexeymar@mellanox.com>
Reviewed-by: Ben Walker <benjamin.walker@intel.com>
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
test/nvmf: Make sure RoCEv2 protocol is used by irdma platform

This is done in order to avoid hitting issues similar to #1747
for which https://review.spdk.io/gerrit/c/spdk/spdk/+/6106 was
submitted.

Signed-off-by: Michal Berger <michalx.berger@intel.com>
Change-Id: I95817f63287795f438f1d392cf1fb2894226ce0e
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/6240
Community-CI: Broadcom CI
Community-CI: Mellanox Build Bot
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Karol Latecki <karol.latecki@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Tomasz Zawadzki <tomasz.zawadzki@intel.com>
nvmf/rdma: add RPC parameter to specify CQ size with RDMA transport

nvmf_create_transport rpc parameter to configure the CQ size helps
if the user is aware of CQ size needed as iWARP doesn't support CQ resize.

Fixes issue #1747

Signed-off-by: Monica Kenguva <monica.kenguva@intel.com>
Change-Id: Ia9ba2b5f612993be27ebfa3455fb4fefd80ae738
Reviewed-on: https://review.spdk.io/gerrit/c/spdk/spdk/+/6495
Community-CI: Broadcom CI
Community-CI: Mellanox Build Bot
Tested-by: SPDK CI Jenkins <sys_sgci@intel.com>
Reviewed-by: Jim Harris <james.r.harris@intel.com>
Reviewed-by: Aleksey Marchuk <alexeymar@mellanox.com>
