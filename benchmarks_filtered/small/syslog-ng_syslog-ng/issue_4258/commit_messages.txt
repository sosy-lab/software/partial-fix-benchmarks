Merge pull request #4231 from OverOrion/gha_depr_warnings

GitHub Actions: fix (some) deprecation warnings
mainloop-call: extract main_loop_wait_for_pending_call_to_finish()

As a preparation for fixing #4258, this patch extracts a few lines as
a new function from main_loop_call().

The goal of the new function is to wait for any pending executions of
mainloop-calls, which can happen if a worker thread submits a new one while
the old is still executing. At the moment, there can be only ONE outstanding
mainloop-calls at the same time.

Signed-off-by: Balazs Scheidler <bazsi77@gmail.com>
mainloop-call: fix crash by waiting for pending mainloop-calls at thread deinit time

As explained in #4258, a crash can happen in syslog-ng if a worker thread
exits while a mainloop call is still being processed.

The solution is to prevent the worker from exiting until that pending call
is finished.

NOTE: in this state the worker is not going to execute tasks, but for all
ivykis knows this thread does not really exist anymore. So if the workpool
requires new threads, they are going to be launched, while this exists
in limbo.

Signed-off-by: Balazs Scheidler <bazsi77@gmail.com>
mainloop-call: fix crash by waiting for pending mainloop-calls at thread deinit time

As explained in #4258, a crash can happen in syslog-ng if a worker thread
exits while a mainloop call is still being processed.

The solution is to prevent the worker from exiting until that pending call
is finished.

NOTE: in this state the worker is not going to execute tasks, but for all
ivykis knows this thread does not really exist anymore. So if the workpool
requires new threads, they are going to be launched, while this exists
in limbo.

Reported-by: @chucksilvers
Signed-off-by: Balazs Scheidler <bazsi77@gmail.com>
mainloop-call: extract main_loop_wait_for_pending_call_to_finish()

As a preparation for fixing #4258, this patch extracts a few lines as
a new function from main_loop_call().

The goal of the new function is to wait for any pending executions of
mainloop-calls, which can happen if a worker thread submits a new one while
the old is still executing. At the moment, there can be only ONE outstanding
mainloop-calls at the same time.

Signed-off-by: Balazs Scheidler <bazsi77@gmail.com>
mainloop-call: fix crash by waiting for pending mainloop-calls at thread deinit time

As explained in #4258, a crash can happen in syslog-ng if a worker thread
exits while a mainloop call is still being processed.

The solution is to prevent the worker from exiting until that pending call
is finished.

NOTE: in this state the worker is not going to execute tasks, but for all
ivykis knows this thread does not really exist anymore. So if the workpool
requires new threads, they are going to be launched, while this exists
in limbo.

Reported-by: @chucksilvers
Signed-off-by: Balazs Scheidler <bazsi77@gmail.com>
