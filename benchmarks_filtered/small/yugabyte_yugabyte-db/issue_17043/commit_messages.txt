[PLAT-9904] Node agent token is exposed in the log on error

Summary: Remove the line logging the command args. These are already logged by Java code after redaction.

Test Plan:
Trivial

Also verified manually by aborting the task before provision step.

Reviewers: muthu, cvankayalapati

Reviewed By: cvankayalapati

Subscribers: yugaware

Differential Revision: https://phorge.dev.yugabyte.com/D27462
[#17043] YSQL: Fix check to see when to attempt binding hash key options in scanspec

Summary:
It appeared that the check in doc_pgsql_scanspec.cc to see if there are hash IN options to bind was incomplete. It checks to see if there are options to bind in a hash partitioned table without checking to see if any of these options were relevant to the hash keys. In the case where a bound on the hash code is specified, range options would be pushed down and break the preexisting logic. This change fixes that check.

This fix does not apply to YCQL as that code does not appear to be checking for hash IN options in the same way.

Fixes regression introduced by: fc57665374b7249724790c1d62f05f395ff18b99 in 2.17.3
Backport required: Y
Backport-through: 2.18
Closes: #17043

Test Plan: ./yb_build.sh --java-test org.yb.pgsql.TestPgRegressProc

Reviewers: kpopali

Reviewed By: kpopali

Subscribers: ybase, yql

Differential Revision: https://phorge.dev.yugabyte.com/D27424
[BACKPORT 2.18][#17043] YSQL: Fix check to see when to attempt binding hash key options in scanspec

Summary:
Original commit: dea1831c3340389a7a8a6e03c737f5dc7e578238 / D27424
It appeared that the check in doc_pgsql_scanspec.cc to see if there are hash IN options to bind was incomplete. It checks to see if there are options to bind in a hash partitioned table without checking to see if any of these options were relevant to the hash keys. In the case where a bound on the hash code is specified, range options would be pushed down and break the preexisting logic. This change fixes that check.

This fix does not apply to YCQL as that code does not appear to be checking for hash IN options in the same way.

Fixes regression introduced by: fc57665374b7249724790c1d62f05f395ff18b99 in 2.17.3
Backport required: Y
Backport-through: 2.18
Closes: #17043
Jira: DB-6354

Test Plan: ./yb_build.sh --java-test org.yb.pgsql.TestPgRegressProc

Reviewers: kpopali

Reviewed By: kpopali

Subscribers: yql, ybase

Differential Revision: https://phorge.dev.yugabyte.com/D27467
