Merge pull request #1239 from paddor/master

Problem: Ruby binding is outdated wrt zproject
Problem: ipv6 changes conflict with boost::asio

Solution: rename conflicting define to `in4addr`

Fixes #1238
Problem: ipv6 changes conflict with boost::asio

Solution: rename conflicting defines to `ipv4addr` and `ipv6addr`

Fixes #1238
