diff --git a/server/src/main/java/org/elasticsearch/action/search/MultiSearchAction.java b/server/src/main/java/org/elasticsearch/action/search/MultiSearchAction.java
index 298c7593f6c97..9017a7b94ecb4 100644
--- a/server/src/main/java/org/elasticsearch/action/search/MultiSearchAction.java
+++ b/server/src/main/java/org/elasticsearch/action/search/MultiSearchAction.java
@@ -20,6 +20,7 @@
 package org.elasticsearch.action.search;
 
 import org.elasticsearch.action.Action;
+import org.elasticsearch.common.io.stream.Writeable;
 
 public class MultiSearchAction extends Action<MultiSearchResponse> {
 
@@ -32,6 +33,11 @@ private MultiSearchAction() {
 
     @Override
     public MultiSearchResponse newResponse() {
-        return new MultiSearchResponse();
+        throw new UnsupportedOperationException("usage of Streamable is to be replaced by Writeable");
+    }
+
+    @Override
+    public Writeable.Reader<MultiSearchResponse> getResponseReader() {
+        return MultiSearchResponse::new;
     }
 }
diff --git a/server/src/main/java/org/elasticsearch/action/search/MultiSearchResponse.java b/server/src/main/java/org/elasticsearch/action/search/MultiSearchResponse.java
index 3ea90073f2b96..639c9570a76b5 100644
--- a/server/src/main/java/org/elasticsearch/action/search/MultiSearchResponse.java
+++ b/server/src/main/java/org/elasticsearch/action/search/MultiSearchResponse.java
@@ -27,7 +27,7 @@
 import org.elasticsearch.common.Strings;
 import org.elasticsearch.common.io.stream.StreamInput;
 import org.elasticsearch.common.io.stream.StreamOutput;
-import org.elasticsearch.common.io.stream.Streamable;
+import org.elasticsearch.common.io.stream.Writeable;
 import org.elasticsearch.common.unit.TimeValue;
 import org.elasticsearch.common.xcontent.ConstructingObjectParser;
 import org.elasticsearch.common.xcontent.ToXContentObject;
@@ -59,19 +59,37 @@
     /**
      * A search response item, holding the actual search response, or an error message if it failed.
      */
-    public static class Item implements Streamable {
-        private SearchResponse response;
-        private Exception exception;
-
-        Item() {
-
-        }
+    public static class Item implements Writeable {
+        private final SearchResponse response;
+        private final Exception exception;
 
         public Item(SearchResponse response, Exception exception) {
             this.response = response;
             this.exception = exception;
         }
 
+        Item(StreamInput in) throws IOException{
+            if (in.readBoolean()) {
+                this.response = new SearchResponse();
+                this.response.readFrom(in);
+                this.exception = null;
+            } else {
+                this.exception = in.readException();
+                this.response = null;
+            }
+        }
+
+        @Override
+        public void writeTo(StreamOutput out) throws IOException {
+            if (response != null) {
+                out.writeBoolean(true);
+                response.writeTo(out);
+            } else {
+                out.writeBoolean(false);
+                out.writeException(exception);
+            }
+        }
+
         /**
          * Is it a failed search?
          */
@@ -95,47 +113,20 @@ public SearchResponse getResponse() {
             return this.response;
         }
 
-        public static Item readItem(StreamInput in) throws IOException {
-            Item item = new Item();
-            item.readFrom(in);
-            return item;
-        }
-
-        @Override
-        public void readFrom(StreamInput in) throws IOException {
-            if (in.readBoolean()) {
-                this.response = new SearchResponse();
-                response.readFrom(in);
-            } else {
-                exception = in.readException();
-            }
-        }
-
-        @Override
-        public void writeTo(StreamOutput out) throws IOException {
-            if (response != null) {
-                out.writeBoolean(true);
-                response.writeTo(out);
-            } else {
-                out.writeBoolean(false);
-                out.writeException(exception);
-            }
-        }
-
         public Exception getFailure() {
             return exception;
         }
     }
 
-    private Item[] items;
-
-    private long tookInMillis;
-
-    MultiSearchResponse() {
-    }
+    private final Item[] items;
+    private final long tookInMillis;
 
     MultiSearchResponse(StreamInput in) throws IOException {
-        readFrom(in);
+        items = new Item[in.readVInt()];
+        for (int i = 0; i < items.length; i++) {
+            items[i] = new Item(in);
+        }
+        tookInMillis = in.readVLong();
     }
 
     public MultiSearchResponse(Item[] items, long tookInMillis) {
@@ -164,12 +155,7 @@ public TimeValue getTook() {
 
     @Override
     public void readFrom(StreamInput in) throws IOException {
-        super.readFrom(in);
-        items = new Item[in.readVInt()];
-        for (int i = 0; i < items.length; i++) {
-            items[i] = Item.readItem(in);
-        }
-        tookInMillis = in.readVLong();
+        throw new UnsupportedOperationException("usage of Streamable is to be replaced by Writeable");
     }
 
     @Override
