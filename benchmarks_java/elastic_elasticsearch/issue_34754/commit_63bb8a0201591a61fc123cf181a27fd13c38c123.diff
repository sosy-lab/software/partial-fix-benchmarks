diff --git a/x-pack/plugin/sql/src/test/java/org/elasticsearch/xpack/sql/tree/NodeSubclassTests.java b/x-pack/plugin/sql/src/test/java/org/elasticsearch/xpack/sql/tree/NodeSubclassTests.java
index 4c763fa95cd26..caacee0f4bada 100644
--- a/x-pack/plugin/sql/src/test/java/org/elasticsearch/xpack/sql/tree/NodeSubclassTests.java
+++ b/x-pack/plugin/sql/src/test/java/org/elasticsearch/xpack/sql/tree/NodeSubclassTests.java
@@ -16,12 +16,17 @@
 import org.elasticsearch.xpack.sql.expression.function.aggregate.AggregateFunction;
 import org.elasticsearch.xpack.sql.expression.function.aggregate.Avg;
 import org.elasticsearch.xpack.sql.expression.function.aggregate.InnerAggregate;
+import org.elasticsearch.xpack.sql.expression.function.aggregate.Percentile;
+import org.elasticsearch.xpack.sql.expression.function.aggregate.PercentileRanks;
+import org.elasticsearch.xpack.sql.expression.function.aggregate.Percentiles;
 import org.elasticsearch.xpack.sql.expression.gen.pipeline.AggExtractorInput;
 import org.elasticsearch.xpack.sql.expression.gen.pipeline.BinaryPipesTests;
 import org.elasticsearch.xpack.sql.expression.gen.pipeline.Pipe;
 import org.elasticsearch.xpack.sql.expression.gen.processor.ConstantProcessor;
 import org.elasticsearch.xpack.sql.expression.gen.processor.Processor;
+import org.elasticsearch.xpack.sql.expression.predicate.In;
 import org.elasticsearch.xpack.sql.expression.predicate.fulltext.FullTextPredicate;
+import org.elasticsearch.xpack.sql.expression.predicate.operator.comparison.InPipe;
 import org.elasticsearch.xpack.sql.expression.predicate.regex.LikePattern;
 import org.elasticsearch.xpack.sql.tree.NodeTests.ChildrenAreAProperty;
 import org.elasticsearch.xpack.sql.tree.NodeTests.Dummy;
@@ -41,6 +46,7 @@
 import java.nio.file.SimpleFileVisitor;
 import java.nio.file.attribute.BasicFileAttributes;
 import java.util.ArrayList;
+import java.util.Arrays;
 import java.util.Collection;
 import java.util.EnumSet;
 import java.util.HashMap;
@@ -78,6 +84,10 @@
  * </ul>
  */
 public class NodeSubclassTests<T extends B, B extends Node<B>> extends ESTestCase {
+
+    private static final List<Class<? extends Node<?>>> CLASSES_WITH_MIN_TWO_CHILDREN = Arrays.asList(
+        In.class, InPipe.class, Percentile.class, Percentiles.class, PercentileRanks.class);
+
     private final Class<T> subclass;
 
     public NodeSubclassTests(Class<T> subclass) {
@@ -147,7 +157,6 @@ public void testTransform() throws Exception {
     /**
      * Test {@link Node#replaceChildren} implementation on {@link #subclass}.
      */
-    @AwaitsFix(bugUrl = "https://github.com/elastic/elasticsearch/issues/34775")
     public void testReplaceChildren() throws Exception {
         Constructor<T> ctor = longestCtor(subclass);
         Object[] nodeCtorArgs = ctorArgs(ctor);
@@ -343,20 +352,14 @@ private Object makeArg(Type argType) {
      */
     @SuppressWarnings("unchecked")
     private static Object makeArg(Class<? extends Node<?>> toBuildClass, Type argType) throws Exception {
+
         if (argType instanceof ParameterizedType) {
             ParameterizedType pt = (ParameterizedType) argType;
             if (pt.getRawType() == Map.class) {
-                Map<Object, Object> map = new HashMap<>();
-                int size = between(0, 10);
-                while (map.size() < size) {
-                    Object key = makeArg(toBuildClass, pt.getActualTypeArguments()[0]);
-                    Object value = makeArg(toBuildClass, pt.getActualTypeArguments()[1]);
-                    map.put(key, value);
-                }
-                return map;
+                return makeMap(toBuildClass, pt);
             }
             if (pt.getRawType() == List.class) {
-                return makeList(toBuildClass, pt, between(1, 10));
+                return makeList(toBuildClass, pt);
             }
             if (pt.getRawType() == EnumSet.class) {
                 @SuppressWarnings("rawtypes")
@@ -512,6 +515,10 @@ public boolean equals(Object obj) {
         }
     }
 
+    private static List<?> makeList(Class<? extends Node<?>> toBuildClass, ParameterizedType listType) throws Exception {
+        return makeList(toBuildClass, listType, randomSizeForCollection(toBuildClass));
+    }
+
     private static List<?> makeList(Class<? extends Node<?>> toBuildClass, ParameterizedType listType, int size) throws Exception {
         List<Object> list = new ArrayList<>();
         for (int i = 0; i < size; i++) {
@@ -520,6 +527,27 @@ public boolean equals(Object obj) {
         return list;
     }
 
+    private static Object makeMap(Class<? extends Node<?>> toBuildClass, ParameterizedType pt) throws Exception {
+        Map<Object, Object> map = new HashMap<>();
+        int size = randomSizeForCollection(toBuildClass);
+        while (map.size() < size) {
+            Object key = makeArg(toBuildClass, pt.getActualTypeArguments()[0]);
+            Object value = makeArg(toBuildClass, pt.getActualTypeArguments()[1]);
+            map.put(key, value);
+        }
+        return map;
+    }
+
+    private static int randomSizeForCollection(Class<? extends Node<?>> toBuildClass) {
+        int minCollectionLength = 0;
+        int maxCollectionLength = 10;
+
+        if (CLASSES_WITH_MIN_TWO_CHILDREN.stream().anyMatch(c -> c == toBuildClass)) {
+            minCollectionLength = 2;
+        }
+        return between(minCollectionLength, maxCollectionLength);
+    }
+
     private List<?> makeListOfSameSizeOtherThan(Type listType, List<?> original) throws Exception {
         if (original.isEmpty()) {
             throw new IllegalArgumentException("Can't make a different empty list");
