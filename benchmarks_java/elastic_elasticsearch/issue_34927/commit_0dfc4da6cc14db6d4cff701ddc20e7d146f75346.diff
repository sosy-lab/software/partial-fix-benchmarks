diff --git a/client/rest-high-level/src/main/java/org/elasticsearch/client/indexlifecycle/Phase.java b/client/rest-high-level/src/main/java/org/elasticsearch/client/indexlifecycle/Phase.java
index 6dcd77bce3b59..0c19d39c85964 100644
--- a/client/rest-high-level/src/main/java/org/elasticsearch/client/indexlifecycle/Phase.java
+++ b/client/rest-high-level/src/main/java/org/elasticsearch/client/indexlifecycle/Phase.java
@@ -40,7 +40,7 @@
  */
 public class Phase implements ToXContentObject {
 
-    static final ParseField MINIMUM_AGE = new ParseField("minimum_age");
+    static final ParseField MIN_AGE = new ParseField("min_age");
     static final ParseField ACTIONS_FIELD = new ParseField("actions");
 
     @SuppressWarnings("unchecked")
@@ -49,7 +49,7 @@
             .collect(Collectors.toMap(LifecycleAction::getName, Function.identity()))));
     static {
         PARSER.declareField(ConstructingObjectParser.optionalConstructorArg(),
-            (p, c) -> TimeValue.parseTimeValue(p.text(), MINIMUM_AGE.getPreferredName()), MINIMUM_AGE, ValueType.VALUE);
+            (p, c) -> TimeValue.parseTimeValue(p.text(), MIN_AGE.getPreferredName()), MIN_AGE, ValueType.VALUE);
         PARSER.declareNamedObjects(ConstructingObjectParser.constructorArg(),
             (p, c, n) -> p.namedObject(LifecycleAction.class, n, null), v -> {
                 throw new IllegalArgumentException("ordered " + ACTIONS_FIELD.getPreferredName() + " are not supported");
@@ -111,7 +111,7 @@ public String getName() {
     @Override
     public XContentBuilder toXContent(XContentBuilder builder, Params params) throws IOException {
         builder.startObject();
-        builder.field(MINIMUM_AGE.getPreferredName(), minimumAge.getStringRep());
+        builder.field(MIN_AGE.getPreferredName(), minimumAge.getStringRep());
         builder.field(ACTIONS_FIELD.getPreferredName(), actions);
         builder.endObject();
         return builder;
diff --git a/docs/reference/ilm/apis/delete-lifecycle.asciidoc b/docs/reference/ilm/apis/delete-lifecycle.asciidoc
index 6462f0faa8354..9adf2c0c3dcd5 100644
--- a/docs/reference/ilm/apis/delete-lifecycle.asciidoc
+++ b/docs/reference/ilm/apis/delete-lifecycle.asciidoc
@@ -48,7 +48,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -56,7 +56,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/apis/explain.asciidoc b/docs/reference/ilm/apis/explain.asciidoc
index 2611ea235bd97..c15803b1ddad6 100644
--- a/docs/reference/ilm/apis/explain.asciidoc
+++ b/docs/reference/ilm/apis/explain.asciidoc
@@ -52,7 +52,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -60,7 +60,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
@@ -120,7 +120,7 @@ When the index is first taken over by ILM you will see a response like the follo
 <1> Shows if the index is being managed by ILM. If the index is not managed by
 ILM the other fields will not be shown
 <2> The name of the policy which ILM is using for this index
-<3> The timestamp used for the `minimum_age`
+<3> The timestamp used for the `min_age`
 <4> The current phase
 <5> The timestamp for when the index entered the current phase
 <6> The current action
@@ -151,7 +151,7 @@ phase definition has been completely executed.
       "phase_execution": { <1>
         "policy": "my_lifecycle3", <2>
         "phase_definition": { <3>
-          "minimum_age": "0ms",
+          "min_age": "0ms",
           "actions": {
             "rollover": {
               "max_age": "30s"
@@ -203,7 +203,7 @@ If the policy is waiting for a step to complete for the index, the response will
       "phase_execution": {
         "policy": "my_lifecycle3",
         "phase_definition": {
-          "minimum_age": "0ms",
+          "min_age": "0ms",
           "actions": {
             "allocate": {
               "number_of_replicas": 2,
@@ -262,7 +262,7 @@ that occurred in `step_info`.
       "phase_execution": {
         "policy": "my_lifecycle3",
         "phase_definition": {
-          "minimum_age": "0ms",
+          "min_age": "0ms",
           "actions": {
             "rollover": {
               "max_age": "30s"
diff --git a/docs/reference/ilm/apis/get-lifecycle.asciidoc b/docs/reference/ilm/apis/get-lifecycle.asciidoc
index 306688cbb13f7..5d20af24ab93c 100644
--- a/docs/reference/ilm/apis/get-lifecycle.asciidoc
+++ b/docs/reference/ilm/apis/get-lifecycle.asciidoc
@@ -51,7 +51,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -59,7 +59,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
@@ -91,7 +91,7 @@ If the request does not encounter errors, you receive the following result:
     "policy": {
       "phases": {
         "warm": {
-          "minimum_age": "10d",
+          "min_age": "10d",
           "actions": {
             "forcemerge": {
               "max_num_segments": 1
@@ -99,7 +99,7 @@ If the request does not encounter errors, you receive the following result:
           }
         },
         "delete": {
-          "minimum_age": "30d",
+          "min_age": "30d",
           "actions": {
             "delete": {}
           }
diff --git a/docs/reference/ilm/apis/move-to-step.asciidoc b/docs/reference/ilm/apis/move-to-step.asciidoc
index cd6f26737771f..f927c3ac4c890 100644
--- a/docs/reference/ilm/apis/move-to-step.asciidoc
+++ b/docs/reference/ilm/apis/move-to-step.asciidoc
@@ -55,7 +55,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -63,7 +63,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/apis/put-lifecycle.asciidoc b/docs/reference/ilm/apis/put-lifecycle.asciidoc
index 93d5319b603b2..9ac7d956c3406 100644
--- a/docs/reference/ilm/apis/put-lifecycle.asciidoc
+++ b/docs/reference/ilm/apis/put-lifecycle.asciidoc
@@ -51,7 +51,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -59,7 +59,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/apis/remove-policy.asciidoc b/docs/reference/ilm/apis/remove-policy.asciidoc
index 3e13f852af866..1fab265ede9d1 100644
--- a/docs/reference/ilm/apis/remove-policy.asciidoc
+++ b/docs/reference/ilm/apis/remove-policy.asciidoc
@@ -49,7 +49,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -57,7 +57,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/apis/start.asciidoc b/docs/reference/ilm/apis/start.asciidoc
index 34a7d4cbe649a..a1c112da77ce3 100644
--- a/docs/reference/ilm/apis/start.asciidoc
+++ b/docs/reference/ilm/apis/start.asciidoc
@@ -45,7 +45,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -53,7 +53,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/apis/stop.asciidoc b/docs/reference/ilm/apis/stop.asciidoc
index 66024b7737318..ced79bcd43495 100644
--- a/docs/reference/ilm/apis/stop.asciidoc
+++ b/docs/reference/ilm/apis/stop.asciidoc
@@ -47,7 +47,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -55,7 +55,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/set-up-lifecycle-policy.asciidoc b/docs/reference/ilm/set-up-lifecycle-policy.asciidoc
index 9a2815213a57e..cdc24ae8d569d 100644
--- a/docs/reference/ilm/set-up-lifecycle-policy.asciidoc
+++ b/docs/reference/ilm/set-up-lifecycle-policy.asciidoc
@@ -22,7 +22,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {} <2>
         }
diff --git a/docs/reference/ilm/start-stop-ilm.asciidoc b/docs/reference/ilm/start-stop-ilm.asciidoc
index d6cf18d3d539e..816c5581956df 100644
--- a/docs/reference/ilm/start-stop-ilm.asciidoc
+++ b/docs/reference/ilm/start-stop-ilm.asciidoc
@@ -22,7 +22,7 @@ PUT _ilm/my_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -30,7 +30,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/update-lifecycle-policy.asciidoc b/docs/reference/ilm/update-lifecycle-policy.asciidoc
index 76c86fddeb4b6..1fc02cac1f5ff 100644
--- a/docs/reference/ilm/update-lifecycle-policy.asciidoc
+++ b/docs/reference/ilm/update-lifecycle-policy.asciidoc
@@ -33,7 +33,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
@@ -65,7 +65,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "10d", <1>
+        "min_age": "10d", <1>
         "actions": {
           "delete": {}
         }
@@ -76,7 +76,7 @@ PUT _ilm/my_policy
 ------------------------
 // CONSOLE
 // TEST[continued]
-<1> update `minimum_age` to 10 days
+<1> update `min_age` to 10 days
 
 //////////
 [source,js]
@@ -99,7 +99,7 @@ with its version bumped to 2.
     "policy": {
       "phases": {
         "hot": {
-          "minimum_age": "0ms",
+          "min_age": "0ms",
           "actions": {
             "rollover": {
               "max_size": "25gb"
@@ -107,7 +107,7 @@ with its version bumped to 2.
           }
         },
         "delete": {
-          "minimum_age": "10d",
+          "min_age": "10d",
           "actions": {
             "delete": {}
           }
@@ -148,7 +148,7 @@ PUT _ilm/my_executing_policy
         }
       },
       "delete": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "delete": {}
         }
@@ -203,7 +203,7 @@ GET my_index/_ilm/explain
         "modified_date_in_millis": 1538475653317,
         "version": 1,
         "phase_definition": {
-          "minimum_age": "0ms",
+          "min_age": "0ms",
           "actions": {
             "rollover": {
               "max_docs": 1
@@ -231,7 +231,7 @@ PUT _ilm/my_executing_policy
   "policy": {
     "phases": {
       "warm": {
-        "minimum_age": "1d",
+        "min_age": "1d",
         "actions": {
           "forcemerge": {
             "max_num_segments": 1
@@ -239,7 +239,7 @@ PUT _ilm/my_executing_policy
         }
       },
       "delete": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "delete": {}
         }
@@ -283,7 +283,7 @@ GET my_index/_ilm/explain
         "modified_date_in_millis": 1538475653317,
         "version": 1,
         "phase_definition": {
-          "minimum_age": "0ms",
+          "min_age": "0ms",
           "actions": {
             "rollover": {
               "max_docs": 1
@@ -339,7 +339,7 @@ GET my_index/_ilm/explain
         "modified_date_in_millis": 1538475653317,
         "version": 2, <1>
         "phase_definition": {
-          "minimum_age": "1d",
+          "min_age": "1d",
           "actions": {
             "forcemerge": {
               "max_num_segments": 1
@@ -383,7 +383,7 @@ PUT _ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "10d",
+        "min_age": "10d",
         "actions": {
           "delete": {}
         }
@@ -397,7 +397,7 @@ PUT _ilm/my_other_policy
   "policy": {
     "phases": {
       "delete": {
-        "minimum_age": "1d",
+        "min_age": "1d",
         "actions": {
           "delete": {}
         }
diff --git a/docs/reference/ilm/using-policies-rollover.asciidoc b/docs/reference/ilm/using-policies-rollover.asciidoc
index fcb50a3c49593..327dbe07f94a0 100644
--- a/docs/reference/ilm/using-policies-rollover.asciidoc
+++ b/docs/reference/ilm/using-policies-rollover.asciidoc
@@ -41,7 +41,7 @@ when the index size reaches 25GB. The old index is subsequently deleted after
 
 NOTE: Once an index rolls over, {ilm} uses the timestamp of the rollover
 operation rather than the index creation time to evaluate when to move the
-index to the next phase. For indices that have rolled over, the `minimum_age`
+index to the next phase. For indices that have rolled over, the `min_age`
 criteria specified for a phase is relative to the rollover time for indices. In
 this example, that means the index will be deleted 30 days after rollover, not
 30 days from when the index was created.
@@ -60,7 +60,7 @@ PUT /_ilm/my_policy
         }
       },
       "delete": {
-        "minimum_age": "30d",
+        "min_age": "30d",
         "actions": {
           "delete": {}
         }
diff --git a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/IndexLifecycleFeatureSetUsage.java b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/IndexLifecycleFeatureSetUsage.java
index a738ad3e34cfa..f0dd25eabac1f 100644
--- a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/IndexLifecycleFeatureSetUsage.java
+++ b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/IndexLifecycleFeatureSetUsage.java
@@ -59,7 +59,7 @@ protected void innerXContent(XContentBuilder builder, Params params) throws IOEx
             builder.field("policy_stats", policyStats);
         }
     }
-    
+
     public List<PolicyStats> getPolicyStats() {
         return policyStats;
     }
@@ -170,25 +170,25 @@ public void writeTo(StreamOutput out) throws IOException {
         @Override
         public XContentBuilder toXContent(XContentBuilder builder, Params params) throws IOException {
             builder.startObject();
-            builder.field(Phase.MINIMUM_AGE.getPreferredName(), minimumAge.getMillis());
+            builder.field(Phase.MIN_AGE.getPreferredName(), minimumAge.getMillis());
             builder.field(Phase.ACTIONS_FIELD.getPreferredName(), actionNames);
             builder.endObject();
             return builder;
         }
-        
+
         public String[] getActionNames() {
             return actionNames;
         }
-        
+
         public TimeValue getAfter() {
             return minimumAge;
         }
-        
+
         @Override
         public int hashCode() {
             return Objects.hash(Arrays.hashCode(actionNames), minimumAge);
         }
-        
+
         @Override
         public boolean equals(Object obj) {
             if (obj == null) {
diff --git a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/Phase.java b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/Phase.java
index e0ff413e0f08e..08b995ade1460 100644
--- a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/Phase.java
+++ b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/Phase.java
@@ -31,7 +31,7 @@
  */
 public class Phase implements ToXContentObject, Writeable {
 
-    public static final ParseField MINIMUM_AGE = new ParseField("minimum_age");
+    public static final ParseField MIN_AGE = new ParseField("min_age");
     public static final ParseField ACTIONS_FIELD = new ParseField("actions");
 
     @SuppressWarnings("unchecked")
@@ -40,7 +40,7 @@
                     .collect(Collectors.toMap(LifecycleAction::getWriteableName, Function.identity()))));
     static {
         PARSER.declareField(ConstructingObjectParser.optionalConstructorArg(),
-                (p, c) -> TimeValue.parseTimeValue(p.text(), MINIMUM_AGE.getPreferredName()), MINIMUM_AGE, ValueType.VALUE);
+                (p, c) -> TimeValue.parseTimeValue(p.text(), MIN_AGE.getPreferredName()), MIN_AGE, ValueType.VALUE);
         PARSER.declareNamedObjects(ConstructingObjectParser.constructorArg(),
                 (p, c, n) -> p.namedObject(LifecycleAction.class, n, null), v -> {
                     throw new IllegalArgumentException("ordered " + ACTIONS_FIELD.getPreferredName() + " are not supported");
@@ -128,7 +128,7 @@ public String getName() {
     @Override
     public XContentBuilder toXContent(XContentBuilder builder, Params params) throws IOException {
         builder.startObject();
-        builder.field(MINIMUM_AGE.getPreferredName(), minimumAge.getStringRep());
+        builder.field(MIN_AGE.getPreferredName(), minimumAge.getStringRep());
         builder.field(ACTIONS_FIELD.getPreferredName(), actions);
         builder.endObject();
         return builder;
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/10_basic.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/10_basic.yml
index dd0ab626b5360..385430c1bf704 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/10_basic.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/10_basic.yml
@@ -24,7 +24,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "10s",
+                   "min_age": "10s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -32,7 +32,7 @@ setup:
                    }
                  },
                  "delete": {
-                   "minimum_age": "30s",
+                   "min_age": "30s",
                    "actions": {
                      "delete": {}
                    }
@@ -46,8 +46,8 @@ setup:
         policy: "my_timeseries_lifecycle"
   - match: { my_timeseries_lifecycle.version: 1 }
   - is_true: my_timeseries_lifecycle.modified_date
-  - match: { my_timeseries_lifecycle.policy.phases.warm.minimum_age: "10s" }
-  - match: { my_timeseries_lifecycle.policy.phases.delete.minimum_age: "30s" }
+  - match: { my_timeseries_lifecycle.policy.phases.warm.min_age: "10s" }
+  - match: { my_timeseries_lifecycle.policy.phases.delete.min_age: "30s" }
 
   - do:
       ilm.delete_lifecycle:
@@ -68,7 +68,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "10s",
+                   "min_age": "10s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -76,7 +76,7 @@ setup:
                    }
                  },
                  "delete": {
-                   "minimum_age": "30s",
+                   "min_age": "30s",
                    "actions": {
                      "delete": {}
                    }
@@ -90,8 +90,8 @@ setup:
         policy: "my_timeseries_lifecycle"
   - match: { my_timeseries_lifecycle.version: 1 }
   - is_true: my_timeseries_lifecycle.modified_date
-  - match: { my_timeseries_lifecycle.policy.phases.warm.minimum_age: "10s" }
-  - match: { my_timeseries_lifecycle.policy.phases.delete.minimum_age: "30s" }
+  - match: { my_timeseries_lifecycle.policy.phases.warm.min_age: "10s" }
+  - match: { my_timeseries_lifecycle.policy.phases.delete.min_age: "30s" }
 
 
   - do:
@@ -116,7 +116,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "300s",
+                   "min_age": "300s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -124,7 +124,7 @@ setup:
                    }
                  },
                  "delete": {
-                   "minimum_age": "600s",
+                   "min_age": "600s",
                    "actions": {
                      "delete": {}
                    }
@@ -138,8 +138,8 @@ setup:
         policy: "my_timeseries_lifecycle"
   - match: { my_timeseries_lifecycle.version: 2 }
   - is_true: my_timeseries_lifecycle.modified_date
-  - match: { my_timeseries_lifecycle.policy.phases.warm.minimum_age: "300s" }
-  - match: { my_timeseries_lifecycle.policy.phases.delete.minimum_age: "600s" }
+  - match: { my_timeseries_lifecycle.policy.phases.warm.min_age: "300s" }
+  - match: { my_timeseries_lifecycle.policy.phases.delete.min_age: "600s" }
 
   - do:
       indices.delete:
@@ -167,7 +167,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "10s",
+                   "min_age": "10s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -175,7 +175,7 @@ setup:
                    }
                  },
                  "delete": {
-                   "minimum_age": "30s",
+                   "min_age": "30s",
                    "actions": {
                      "delete": {}
                    }
@@ -187,8 +187,8 @@ setup:
   - do:
       ilm.get_lifecycle:
         policy: "my_timeseries_lifecycle"
-  - match: { my_timeseries_lifecycle.policy.phases.warm.minimum_age: "10s" }
-  - match: { my_timeseries_lifecycle.policy.phases.delete.minimum_age: "30s" }
+  - match: { my_timeseries_lifecycle.policy.phases.warm.min_age: "10s" }
+  - match: { my_timeseries_lifecycle.policy.phases.delete.min_age: "30s" }
 
   - do:
       indices.create:
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/20_move_to_step.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/20_move_to_step.yml
index 3c80b98d89fbe..57223188d655b 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/20_move_to_step.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/20_move_to_step.yml
@@ -11,7 +11,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -19,7 +19,7 @@ setup:
                    }
                  },
                  "hot": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": { }
                  }
                }
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/30_retry.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/30_retry.yml
index 52aad1fc393e2..c6bdfb2a05e14 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/30_retry.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/30_retry.yml
@@ -12,7 +12,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -20,7 +20,7 @@ setup:
                    }
                  },
                  "hot": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": { }
                  }
                }
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/40_explain_lifecycle.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/40_explain_lifecycle.yml
index 6ac0a3fcf76c1..ab6f88821d003 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/40_explain_lifecycle.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/40_explain_lifecycle.yml
@@ -11,7 +11,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -19,7 +19,7 @@ setup:
                    }
                  },
                  "hot": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": { }
                  }
                }
@@ -225,11 +225,11 @@ teardown:
              "policy": {
                "phases": {
                  "hot": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": {}
                  },
                  "warm": {
-                   "minimum_age": "2000s",
+                   "min_age": "2000s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_operation_mode.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_operation_mode.yml
index a3263a270ae40..e8abc5b2c8d4e 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_operation_mode.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_operation_mode.yml
@@ -18,7 +18,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "10s",
+                   "min_age": "10s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -26,7 +26,7 @@ setup:
                    }
                  },
                  "delete": {
-                   "minimum_age": "30s",
+                   "min_age": "30s",
                    "actions": {
                      "delete": {}
                    }
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_remove_policy_for_index.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_remove_policy_for_index.yml
index 14c4a3cd75407..c9537d9779733 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_remove_policy_for_index.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/ilm/60_remove_policy_for_index.yml
@@ -11,7 +11,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -19,7 +19,7 @@ setup:
                    }
                  },
                  "hot": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": { }
                  }
                }
@@ -38,7 +38,7 @@ setup:
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -46,7 +46,7 @@ setup:
                    }
                  },
                  "hot": {
-                   "minimum_age": "1000s",
+                   "min_age": "1000s",
                    "actions": { }
                  }
                }
diff --git a/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/mixed_cluster/70_ilm.yml b/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/mixed_cluster/70_ilm.yml
index d444d5fefeee0..97523d235d94a 100644
--- a/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/mixed_cluster/70_ilm.yml
+++ b/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/mixed_cluster/70_ilm.yml
@@ -3,7 +3,7 @@
   - do:
       ilm.get_lifecycle:
         policy: "my_lifecycle"
-  - match: { my_lifecycle.policy.phases.warm.minimum_age: "1000d" }
+  - match: { my_lifecycle.policy.phases.warm.min_age: "1000d" }
 
   - do:
       ilm.explain_lifecycle:
diff --git a/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/old_cluster/70_ilm.yml b/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/old_cluster/70_ilm.yml
index d2c92de684a06..48b8375c23314 100644
--- a/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/old_cluster/70_ilm.yml
+++ b/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/old_cluster/70_ilm.yml
@@ -18,7 +18,7 @@
              "policy": {
                "phases": {
                  "warm": {
-                   "minimum_age": "1000d",
+                   "min_age": "1000d",
                    "actions": {
                      "forcemerge": {
                        "max_num_segments": 10000
@@ -32,7 +32,7 @@
   - do:
       ilm.get_lifecycle:
         policy: "my_lifecycle"
-  - match: { my_lifecycle.policy.phases.warm.minimum_age: "1000d" }
+  - match: { my_lifecycle.policy.phases.warm.min_age: "1000d" }
 
   - do:
       indices.create:
diff --git a/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/upgraded_cluster/70_ilm.yml b/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/upgraded_cluster/70_ilm.yml
index f548d6a804786..dc323955e6035 100644
--- a/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/upgraded_cluster/70_ilm.yml
+++ b/x-pack/qa/rolling-upgrade/src/test/resources/rest-api-spec/test/upgraded_cluster/70_ilm.yml
@@ -3,7 +3,7 @@
   - do:
       ilm.get_lifecycle:
         policy: "my_lifecycle"
-  - match: { my_lifecycle.policy.phases.warm.minimum_age: "1000d" }
+  - match: { my_lifecycle.policy.phases.warm.min_age: "1000d" }
 
   - do:
       ilm.explain_lifecycle:
