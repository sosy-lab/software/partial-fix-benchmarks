diff --git a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionDefinition.java b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionDefinition.java
index d513ca07df4ae..a284ba83a972b 100644
--- a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionDefinition.java
+++ b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionDefinition.java
@@ -19,6 +19,7 @@
     public interface Builder {
         Function build(UnresolvedFunction uf, boolean distinct, TimeZone tz);
     }
+
     private final String name;
     private final List<String> aliases;
     private final Class<? extends Function> clazz;
diff --git a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionRegistry.java b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionRegistry.java
index 4da4cf4d02301..b36789bfbc6be 100644
--- a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionRegistry.java
+++ b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionRegistry.java
@@ -20,6 +20,7 @@
 import org.elasticsearch.xpack.sql.expression.function.aggregate.Sum;
 import org.elasticsearch.xpack.sql.expression.function.aggregate.SumOfSquares;
 import org.elasticsearch.xpack.sql.expression.function.aggregate.VarPop;
+import org.elasticsearch.xpack.sql.expression.function.scalar.Cast;
 import org.elasticsearch.xpack.sql.expression.function.scalar.datetime.DayName;
 import org.elasticsearch.xpack.sql.expression.function.scalar.datetime.DayOfMonth;
 import org.elasticsearch.xpack.sql.expression.function.scalar.datetime.DayOfWeek;
@@ -84,6 +85,7 @@
 import org.elasticsearch.xpack.sql.expression.predicate.operator.arithmetic.Mod;
 import org.elasticsearch.xpack.sql.parser.ParsingException;
 import org.elasticsearch.xpack.sql.tree.Location;
+import org.elasticsearch.xpack.sql.type.DataType;
 import org.elasticsearch.xpack.sql.util.StringUtils;
 
 import java.util.Arrays;
@@ -116,14 +118,14 @@
     public FunctionRegistry() {
         defineDefaultFunctions();
     }
-    
+
     /**
      * Constructor specifying alternate functions for testing.
      */
     FunctionRegistry(FunctionDefinition... functions) {
         addToMap(functions);
     }
-    
+
     private void defineDefaultFunctions() {
         // Aggregate functions
         addToMap(def(Avg.class, Avg::new),
@@ -206,11 +208,13 @@ private void defineDefaultFunctions() {
                 def(Space.class, Space::new),
                 def(Substring.class, Substring::new),
                 def(UCase.class, UCase::new));
+        // DataType conversion
+        addToMap(def(Cast.class, Cast::new, "CONVERT"));
         // Special
         addToMap(def(Score.class, Score::new));
     }
-    
-    protected void addToMap(FunctionDefinition...functions) {
+
+    void addToMap(FunctionDefinition...functions) {
         // temporary map to hold [function_name/alias_name : function instance]
         Map<String, FunctionDefinition> batchMap = new HashMap<>();
         for (FunctionDefinition f : functions) {
@@ -227,7 +231,7 @@ protected void addToMap(FunctionDefinition...functions) {
         // sort the temporary map by key name and add it to the global map of functions
         defs.putAll(batchMap.entrySet().stream()
                 .sorted(Map.Entry.comparingByKey())
-                .collect(Collectors.<Entry<String, FunctionDefinition>, String, 
+                .collect(Collectors.<Entry<String, FunctionDefinition>, String,
                         FunctionDefinition, LinkedHashMap<String, FunctionDefinition>> toMap(Map.Entry::getKey, Map.Entry::getValue,
                 (oldValue, newValue) -> oldValue, LinkedHashMap::new)));
     }
@@ -390,7 +394,7 @@ private static FunctionDefinition def(Class<? extends Function> function, Functi
     private interface FunctionBuilder {
         Function build(Location location, List<Expression> children, boolean distinct, TimeZone tz);
     }
-    
+
     @SuppressWarnings("overloads")  // These are ambiguous if you aren't using ctor references but we always do
     static <T extends Function> FunctionDefinition def(Class<T> function,
             ThreeParametersFunctionBuilder<T> ctorRef, String... aliases) {
@@ -408,11 +412,11 @@ private static FunctionDefinition def(Class<? extends Function> function, Functi
         };
         return def(function, builder, false, aliases);
     }
-    
+
     interface ThreeParametersFunctionBuilder<T> {
         T build(Location location, Expression source, Expression exp1, Expression exp2);
     }
-    
+
     @SuppressWarnings("overloads")  // These are ambiguous if you aren't using ctor references but we always do
     static <T extends Function> FunctionDefinition def(Class<T> function,
             FourParametersFunctionBuilder<T> ctorRef, String... aliases) {
@@ -427,11 +431,29 @@ private static FunctionDefinition def(Class<? extends Function> function, Functi
         };
         return def(function, builder, false, aliases);
     }
-    
+
     interface FourParametersFunctionBuilder<T> {
         T build(Location location, Expression source, Expression exp1, Expression exp2, Expression exp3);
     }
 
+    /**
+     * Special method to create function definition for {@link Cast} as its
+     * signature is not compatible with {@link UnresolvedFunction}
+     *
+     * @return Cast function definition
+     */
+    @SuppressWarnings("overloads")  // These are ambiguous if you aren't using ctor references but we always do
+    private static <T extends Function> FunctionDefinition def(Class<T> function,
+                                                               CastFunctionBuilder<T> ctorRef,
+                                                               String... aliases) {
+        FunctionBuilder builder = (location, children, distinct, tz) ->
+            ctorRef.build(location, children.get(0), children.get(0).dataType());
+        return def(function, builder, false, aliases);
+    }
+    private interface CastFunctionBuilder<T> {
+        T build(Location location, Expression expression, DataType dataType);
+    }
+
     private static String normalize(String name) {
         // translate CamelCase to camel_case
         return StringUtils.camelCaseToUnderscore(name);
diff --git a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionType.java b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionType.java
index dc75f0f5be37a..5ed81e354fce2 100644
--- a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionType.java
+++ b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/FunctionType.java
@@ -8,10 +8,10 @@
 import org.elasticsearch.xpack.sql.SqlIllegalArgumentException;
 import org.elasticsearch.xpack.sql.expression.function.aggregate.AggregateFunction;
 import org.elasticsearch.xpack.sql.expression.function.scalar.ScalarFunction;
-import org.elasticsearch.xpack.sql.expression.function.Score;
 
 
 public enum FunctionType {
+
     AGGREGATE(AggregateFunction.class),
     SCALAR(ScalarFunction.class),
     SCORE(Score.class);
diff --git a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/scalar/Cast.java b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/scalar/Cast.java
index 13b9d9822c4fa..5c874cc7667f7 100644
--- a/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/scalar/Cast.java
+++ b/x-pack/plugin/sql/src/main/java/org/elasticsearch/xpack/sql/expression/function/scalar/Cast.java
@@ -16,6 +16,7 @@
 import java.util.Objects;
 
 public class Cast extends UnaryScalarFunction {
+
     private final DataType dataType;
 
     public Cast(Location location, Expression field, DataType dataType) {
diff --git a/x-pack/qa/sql/src/main/java/org/elasticsearch/xpack/qa/sql/cli/ShowTestCase.java b/x-pack/qa/sql/src/main/java/org/elasticsearch/xpack/qa/sql/cli/ShowTestCase.java
index b4e87d3e20711..723ca8efb94d1 100644
--- a/x-pack/qa/sql/src/main/java/org/elasticsearch/xpack/qa/sql/cli/ShowTestCase.java
+++ b/x-pack/qa/sql/src/main/java/org/elasticsearch/xpack/qa/sql/cli/ShowTestCase.java
@@ -39,6 +39,7 @@ public void testShowFunctions() throws IOException {
         while (scalarFunction.matcher(line).matches()) {
             line = readLine();
         }
+
         assertThat(line, RegexMatcher.matches("\\s*SCORE\\s*\\|\\s*SCORE\\s*"));
         assertEquals("", readLine());
     }
diff --git a/x-pack/qa/sql/src/main/resources/command.csv-spec b/x-pack/qa/sql/src/main/resources/command.csv-spec
index cc71dd947129a..0514b2a4982bb 100644
--- a/x-pack/qa/sql/src/main/resources/command.csv-spec
+++ b/x-pack/qa/sql/src/main/resources/command.csv-spec
@@ -99,7 +99,9 @@ RTRIM           |SCALAR
 SPACE           |SCALAR         
 SUBSTRING       |SCALAR         
 UCASE           |SCALAR         
-SCORE           |SCORE          
+CAST            |SCALAR
+CONVERT         |SCALAR
+SCORE           |SCORE
 ;
 
 showFunctionsWithExactMatch
diff --git a/x-pack/qa/sql/src/main/resources/docs.csv-spec b/x-pack/qa/sql/src/main/resources/docs.csv-spec
index 66b1d4117dfc5..bba77664ceda8 100644
--- a/x-pack/qa/sql/src/main/resources/docs.csv-spec
+++ b/x-pack/qa/sql/src/main/resources/docs.csv-spec
@@ -276,7 +276,9 @@ RTRIM           |SCALAR
 SPACE           |SCALAR         
 SUBSTRING       |SCALAR         
 UCASE           |SCALAR         
-SCORE           |SCORE           
+CAST            |SCALAR
+CONVERT         |SCALAR
+SCORE           |SCORE
 // end::showFunctions
 ;
 
