diff --git a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStep.java b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStep.java
index 55b5e6e5053b4..d064a3d74d517 100644
--- a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStep.java
+++ b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStep.java
@@ -41,7 +41,7 @@ public SetSingleNodeAllocateStep(StepKey key, StepKey nextStepKey, Client client
     public void performAction(IndexMetaData indexMetaData, ClusterState clusterState, Listener listener) {
         RoutingAllocation allocation = new RoutingAllocation(ALLOCATION_DECIDERS, clusterState.getRoutingNodes(), clusterState, null,
                 System.nanoTime());
-        List<String> validNodeNames = new ArrayList<>();
+        List<String> validNodeIds = new ArrayList<>();
         Optional<ShardRouting> anyShard = clusterState.getRoutingTable().allShards(indexMetaData.getIndex().getName()).stream().findAny();
         if (anyShard.isPresent()) {
             // Iterate through the nodes finding ones that are acceptable for the current allocation rules of the shard
@@ -50,15 +50,15 @@ public void performAction(IndexMetaData indexMetaData, ClusterState clusterState
                         .type() == Decision.Type.YES;
                 if (canRemainOnCurrentNode) {
                     DiscoveryNode discoveryNode = node.node();
-                    validNodeNames.add(discoveryNode.getName());
+                    validNodeIds.add(discoveryNode.getId());
                 }
             }
             // Shuffle the list of nodes so the one we pick is random
-            Randomness.shuffle(validNodeNames);
-            Optional<String> nodeName = validNodeNames.stream().findAny();
-            if (nodeName.isPresent()) {
+            Randomness.shuffle(validNodeIds);
+            Optional<String> nodeId = validNodeIds.stream().findAny();
+            if (nodeId.isPresent()) {
                 Settings settings = Settings.builder()
-                        .put(IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_name", nodeName.get()).build();
+                        .put(IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_id", nodeId.get()).build();
                 UpdateSettingsRequest updateSettingsRequest = new UpdateSettingsRequest(indexMetaData.getIndex().getName())
                         .settings(settings);
                 getClient().admin().indices().updateSettings(updateSettingsRequest,
diff --git a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStep.java b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStep.java
index 2821645e1d34a..8eeaed82e4c12 100644
--- a/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStep.java
+++ b/x-pack/plugin/core/src/main/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStep.java
@@ -49,7 +49,7 @@ public void performAction(IndexMetaData indexMetaData, ClusterState currentState
             .put(IndexMetaData.SETTING_NUMBER_OF_SHARDS, numberOfShards)
             .put(IndexMetaData.SETTING_NUMBER_OF_REPLICAS, indexMetaData.getNumberOfReplicas())
             .put(LifecycleSettings.LIFECYCLE_NAME, lifecycle)
-            .put(IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_name", (String) null) // need to remove the single shard
+            .put(IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_id", (String) null) // need to remove the single shard
                                                                                              // allocation so replicas can be allocated
             .build();
 
@@ -72,7 +72,7 @@ public void performAction(IndexMetaData indexMetaData, ClusterState currentState
     public int hashCode() {
         return Objects.hash(super.hashCode(), numberOfShards, shrunkIndexPrefix);
     }
-    
+
     @Override
     public boolean equals(Object obj) {
         if (obj == null) {
@@ -82,8 +82,8 @@ public boolean equals(Object obj) {
             return false;
         }
         ShrinkStep other = (ShrinkStep) obj;
-        return super.equals(obj) && 
-                Objects.equals(numberOfShards, other.numberOfShards) && 
+        return super.equals(obj) &&
+                Objects.equals(numberOfShards, other.numberOfShards) &&
                 Objects.equals(shrunkIndexPrefix, other.shrunkIndexPrefix);
     }
 
diff --git a/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStepTests.java b/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStepTests.java
index 7b8eed38b4cdd..b42ada6956f87 100644
--- a/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStepTests.java
+++ b/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/SetSingleNodeAllocateStepTests.java
@@ -99,7 +99,7 @@ public void testPerformActionNoAttrs() throws IOException {
         IndexMetaData indexMetaData = IndexMetaData.builder(randomAlphaOfLength(10)).settings(settings(Version.CURRENT))
                 .numberOfShards(randomIntBetween(1, 5)).numberOfReplicas(randomIntBetween(0, 5)).build();
         Index index = indexMetaData.getIndex();
-        Set<String> validNodeNames = new HashSet<>();
+        Set<String> validNodeIds = new HashSet<>();
         Settings validNodeSettings = Settings.EMPTY;
         DiscoveryNodes.Builder nodes = DiscoveryNodes.builder();
         int numNodes = randomIntBetween(1, 20);
@@ -107,13 +107,13 @@ public void testPerformActionNoAttrs() throws IOException {
             String nodeId = "node_id_" + i;
             String nodeName = "node_" + i;
             int nodePort = 9300 + i;
-            Settings nodeSettings = Settings.builder().put(validNodeSettings).put("node.name", nodeName).build();
+            Settings nodeSettings = Settings.builder().put(validNodeSettings).put(Node.NODE_NAME_SETTING.getKey(), nodeName).build();
             nodes.add(
                     DiscoveryNode.createLocal(nodeSettings, new TransportAddress(TransportAddress.META_ADDRESS, nodePort), nodeId));
-            validNodeNames.add(nodeName);
+            validNodeIds.add(nodeId);
         }
 
-        assertNodeSelected(indexMetaData, index, validNodeNames, nodes);
+        assertNodeSelected(indexMetaData, index, validNodeIds, nodes);
     }
 
     public void testPerformActionAttrsAllNodesValid() throws IOException {
@@ -129,7 +129,7 @@ public void testPerformActionAttrsAllNodesValid() throws IOException {
         IndexMetaData indexMetaData = IndexMetaData.builder(randomAlphaOfLength(10)).settings(indexSettings)
                 .numberOfShards(randomIntBetween(1, 5)).numberOfReplicas(randomIntBetween(0, 5)).build();
         Index index = indexMetaData.getIndex();
-        Set<String> validNodeNames = new HashSet<>();
+        Set<String> validNodeIds = new HashSet<>();
         Settings validNodeSettings = Settings.EMPTY;
         DiscoveryNodes.Builder nodes = DiscoveryNodes.builder();
         int numNodes = randomIntBetween(1, 20);
@@ -141,10 +141,10 @@ public void testPerformActionAttrsAllNodesValid() throws IOException {
             Settings nodeSettings = Settings.builder().put(validNodeSettings).put(Node.NODE_NAME_SETTING.getKey(), nodeName)
                     .put(Node.NODE_ATTRIBUTES.getKey() + nodeAttr[0], nodeAttr[1]).build();
             nodes.add(DiscoveryNode.createLocal(nodeSettings, new TransportAddress(TransportAddress.META_ADDRESS, nodePort), nodeId));
-            validNodeNames.add(nodeName);
+            validNodeIds.add(nodeId);
         }
 
-        assertNodeSelected(indexMetaData, index, validNodeNames, nodes);
+        assertNodeSelected(indexMetaData, index, validNodeIds, nodes);
     }
 
     public void testPerformActionAttrsSomeNodesValid() throws IOException {
@@ -155,7 +155,7 @@ public void testPerformActionAttrsSomeNodesValid() throws IOException {
         IndexMetaData indexMetaData = IndexMetaData.builder(randomAlphaOfLength(10)).settings(indexSettings)
                 .numberOfShards(randomIntBetween(1, 5)).numberOfReplicas(randomIntBetween(0, 5)).build();
         Index index = indexMetaData.getIndex();
-        Set<String> validNodeNames = new HashSet<>();
+        Set<String> validNodeIds = new HashSet<>();
         Settings validNodeSettings = Settings.builder().put(Node.NODE_ATTRIBUTES.getKey() + validAttr[0], validAttr[1]).build();
         Settings invalidNodeSettings = Settings.builder().put(Node.NODE_ATTRIBUTES.getKey() + invalidAttr[0], invalidAttr[1]).build();
         DiscoveryNodes.Builder nodes = DiscoveryNodes.builder();
@@ -166,9 +166,9 @@ public void testPerformActionAttrsSomeNodesValid() throws IOException {
             int nodePort = 9300 + i;
             Builder nodeSettingsBuilder = Settings.builder();
             // randomise whether the node had valid attributes or not but make sure at least one node is valid
-            if (randomBoolean() || (i == numNodes - 1 && validNodeNames.isEmpty())) {
+            if (randomBoolean() || (i == numNodes - 1 && validNodeIds.isEmpty())) {
                 nodeSettingsBuilder.put(validNodeSettings).put(Node.NODE_NAME_SETTING.getKey(), nodeName);
-                validNodeNames.add(nodeName);
+                validNodeIds.add(nodeId);
             } else {
                 nodeSettingsBuilder.put(invalidNodeSettings).put(Node.NODE_NAME_SETTING.getKey(), nodeName);
             }
@@ -176,7 +176,7 @@ public void testPerformActionAttrsSomeNodesValid() throws IOException {
                     nodeId));
         }
 
-        assertNodeSelected(indexMetaData, index, validNodeNames, nodes);
+        assertNodeSelected(indexMetaData, index, validNodeIds, nodes);
     }
 
     public void testPerformActionAttrsNoNodesValid() {
@@ -215,7 +215,7 @@ public void testPerformActionAttrsRequestFails() {
         IndexMetaData indexMetaData = IndexMetaData.builder(randomAlphaOfLength(10)).settings(indexSettings)
                 .numberOfShards(randomIntBetween(1, 5)).numberOfReplicas(randomIntBetween(0, 5)).build();
         Index index = indexMetaData.getIndex();
-        Set<String> validNodeNames = new HashSet<>();
+        Set<String> validNodeIds = new HashSet<>();
         Settings validNodeSettings = Settings.EMPTY;
         DiscoveryNodes.Builder nodes = DiscoveryNodes.builder();
         int numNodes = randomIntBetween(1, 20);
@@ -227,7 +227,7 @@ public void testPerformActionAttrsRequestFails() {
             Settings nodeSettings = Settings.builder().put(validNodeSettings).put(Node.NODE_NAME_SETTING.getKey(), nodeName)
                     .put(Node.NODE_ATTRIBUTES.getKey() + nodeAttr[0], nodeAttr[1]).build();
             nodes.add(DiscoveryNode.createLocal(nodeSettings, new TransportAddress(TransportAddress.META_ADDRESS, nodePort), nodeId));
-            validNodeNames.add(nodeName);
+            validNodeIds.add(nodeId);
         }
 
         ImmutableOpenMap.Builder<String, IndexMetaData> indices = ImmutableOpenMap.<String, IndexMetaData> builder().fPut(index.getName(),
@@ -253,7 +253,7 @@ public Void answer(InvocationOnMock invocation) throws Throwable {
                 @SuppressWarnings("unchecked")
                 ActionListener<AcknowledgedResponse> listener = (ActionListener<AcknowledgedResponse>) invocation.getArguments()[1];
                 assertSettingsRequestContainsValueFrom(request,
-                        IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_name", validNodeNames, true,
+                        IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_id", validNodeIds, true,
                         indexMetaData.getIndex().getName());
                 listener.onFailure(exception);
                 return null;
@@ -296,7 +296,7 @@ public void testPerformActionAttrsNoShard() {
         IndexMetaData indexMetaData = IndexMetaData.builder(randomAlphaOfLength(10)).settings(indexSettings)
                 .numberOfShards(randomIntBetween(1, 5)).numberOfReplicas(randomIntBetween(0, 5)).build();
         Index index = indexMetaData.getIndex();
-        Set<String> validNodeNames = new HashSet<>();
+        Set<String> validNodeIds = new HashSet<>();
         Settings validNodeSettings = Settings.EMPTY;
         DiscoveryNodes.Builder nodes = DiscoveryNodes.builder();
         int numNodes = randomIntBetween(1, 20);
@@ -308,7 +308,7 @@ public void testPerformActionAttrsNoShard() {
             Settings nodeSettings = Settings.builder().put(validNodeSettings).put(Node.NODE_NAME_SETTING.getKey(), nodeName)
                     .put(Node.NODE_ATTRIBUTES.getKey() + nodeAttr[0], nodeAttr[1]).build();
             nodes.add(DiscoveryNode.createLocal(nodeSettings, new TransportAddress(TransportAddress.META_ADDRESS, nodePort), nodeId));
-            validNodeNames.add(nodeName);
+            validNodeIds.add(nodeId);
         }
 
         ImmutableOpenMap.Builder<String, IndexMetaData> indices = ImmutableOpenMap.<String, IndexMetaData> builder().fPut(index.getName(),
@@ -341,7 +341,7 @@ public void onFailure(Exception e) {
     }
 
     private void assertNodeSelected(IndexMetaData indexMetaData, Index index,
-                                    Set<String> validNodeNames, DiscoveryNodes.Builder nodes) throws IOException {
+                                    Set<String> validNodeIds, DiscoveryNodes.Builder nodes) throws IOException {
         ImmutableOpenMap.Builder<String, IndexMetaData> indices = ImmutableOpenMap.<String, IndexMetaData> builder().fPut(index.getName(),
                 indexMetaData);
         IndexRoutingTable.Builder indexRoutingTable = IndexRoutingTable.builder(index)
@@ -365,7 +365,7 @@ public Void answer(InvocationOnMock invocation) throws Throwable {
                 @SuppressWarnings("unchecked")
                 ActionListener<AcknowledgedResponse> listener = (ActionListener<AcknowledgedResponse>) invocation.getArguments()[1];
                 assertSettingsRequestContainsValueFrom(request,
-                        IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_name", validNodeNames, true,
+                        IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_id", validNodeIds, true,
                         indexMetaData.getIndex().getName());
                 listener.onResponse(new AcknowledgedResponse(true));
                 return null;
diff --git a/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStepTests.java b/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStepTests.java
index 820d1d15ded9d..472c22025e195 100644
--- a/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStepTests.java
+++ b/x-pack/plugin/core/src/test/java/org/elasticsearch/xpack/core/indexlifecycle/ShrinkStepTests.java
@@ -117,7 +117,7 @@ public Void answer(InvocationOnMock invocation) throws Throwable {
                     .put(IndexMetaData.SETTING_NUMBER_OF_SHARDS, step.getNumberOfShards())
                     .put(IndexMetaData.SETTING_NUMBER_OF_REPLICAS, sourceIndexMetaData.getNumberOfReplicas())
                     .put(LifecycleSettings.LIFECYCLE_NAME, lifecycleName)
-                    .put(IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_name", (String) null)
+                    .put(IndexMetaData.INDEX_ROUTING_REQUIRE_GROUP_SETTING.getKey() + "_id", (String) null)
                     .build()));
                 assertThat(request.getTargetIndexRequest().settings()
                         .getAsInt(IndexMetaData.SETTING_NUMBER_OF_SHARDS, -1), equalTo(step.getNumberOfShards()));
