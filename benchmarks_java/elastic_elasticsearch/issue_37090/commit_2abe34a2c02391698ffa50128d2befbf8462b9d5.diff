diff --git a/client/rest-high-level/src/main/java/org/elasticsearch/client/RestHighLevelClient.java b/client/rest-high-level/src/main/java/org/elasticsearch/client/RestHighLevelClient.java
index a9c6901d9820a..69d0dd89994c3 100644
--- a/client/rest-high-level/src/main/java/org/elasticsearch/client/RestHighLevelClient.java
+++ b/client/rest-high-level/src/main/java/org/elasticsearch/client/RestHighLevelClient.java
@@ -1671,15 +1671,19 @@ protected final ElasticsearchStatusException parseResponseException(ResponseExce
         Response response = responseException.getResponse();
         HttpEntity entity = response.getEntity();
         ElasticsearchStatusException elasticsearchException;
-        if (entity == null) {
+        RestStatus restStatus = RestStatus.fromCode(response.getStatusLine().getStatusCode());
+
+        if (responseException instanceof WarningFailureException) {
+            elasticsearchException = new ElasticsearchStatusException("Warnings/Deprecations caused response to fail", restStatus,
+                responseException);
+        } else if (entity == null) {
             elasticsearchException = new ElasticsearchStatusException(
-                    responseException.getMessage(), RestStatus.fromCode(response.getStatusLine().getStatusCode()), responseException);
+                    responseException.getMessage(), restStatus, responseException);
         } else {
             try {
                 elasticsearchException = parseEntity(entity, BytesRestResponse::errorFromXContent);
                 elasticsearchException.addSuppressed(responseException);
             } catch (Exception e) {
-                RestStatus restStatus = RestStatus.fromCode(response.getStatusLine().getStatusCode());
                 elasticsearchException = new ElasticsearchStatusException("Unable to parse response body", restStatus, responseException);
                 elasticsearchException.addSuppressed(e);
             }
diff --git a/client/rest-high-level/src/test/java/org/elasticsearch/client/MockRestHighLevelTests.java b/client/rest-high-level/src/test/java/org/elasticsearch/client/MockRestHighLevelTests.java
new file mode 100644
index 0000000000000..9ecdd648336d2
--- /dev/null
+++ b/client/rest-high-level/src/test/java/org/elasticsearch/client/MockRestHighLevelTests.java
@@ -0,0 +1,75 @@
+/*
+ * Licensed to Elasticsearch under one or more contributor
+ * license agreements. See the NOTICE file distributed with
+ * this work for additional information regarding copyright
+ * ownership. Elasticsearch licenses this file to you under
+ * the Apache License, Version 2.0 (the "License"); you may
+ * not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *    http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing,
+ * software distributed under the License is distributed on an
+ * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+ * KIND, either express or implied.  See the License for the
+ * specific language governing permissions and limitations
+ * under the License.
+ */
+
+package org.elasticsearch.client;
+
+import org.apache.http.HttpHost;
+import org.apache.http.ProtocolVersion;
+import org.apache.http.RequestLine;
+import org.apache.http.client.methods.HttpGet;
+import org.apache.http.message.BasicRequestLine;
+import org.apache.http.message.BasicStatusLine;
+import org.elasticsearch.ElasticsearchStatusException;
+import org.elasticsearch.test.ESTestCase;
+import org.junit.Before;
+
+import java.io.IOException;
+import java.util.Collections;
+import java.util.List;
+
+import static org.hamcrest.Matchers.equalTo;
+import static org.mockito.Matchers.any;
+import static org.mockito.Mockito.doThrow;
+import static org.mockito.Mockito.mock;
+import static org.mockito.Mockito.when;
+
+public class MockRestHighLevelTests extends ESTestCase {
+    private RestHighLevelClient client;
+    private WarningFailureException expectedException;
+    private static final List<String> WARNINGS = Collections.singletonList("Some Warning");
+
+    @Before
+    private void setupClient() throws IOException {
+        final RestClient mockClient = mock(RestClient.class);
+        final Response mockResponse = mock(Response.class);
+
+        when(mockResponse.getHost()).thenReturn(new HttpHost("localhost", 9200));
+        when(mockResponse.getWarnings()).thenReturn(WARNINGS);
+
+        ProtocolVersion protocol = new ProtocolVersion("HTTP", 1, 1);
+        when(mockResponse.getStatusLine()).thenReturn(new BasicStatusLine(protocol, 200, "OK"));
+
+        RequestLine requestLine = new BasicRequestLine(HttpGet.METHOD_NAME, "/_blah", protocol);
+        when(mockResponse.getRequestLine()).thenReturn(requestLine);
+
+        expectedException = new WarningFailureException(mockResponse);
+        doThrow(expectedException).when(mockClient).performRequest(any());
+
+        client = new RestHighLevelClient(mockClient, RestClient::close, Collections.emptyList());
+    }
+
+    public void testWarningFailure() {
+        ElasticsearchStatusException exception = expectThrows(ElasticsearchStatusException.class,
+            () -> client.info(RequestOptions.DEFAULT));
+        assertThat(exception.getMessage(), equalTo("Warnings/Deprecations caused response to fail"));
+        assertThat(exception.getCause(), equalTo(expectedException));
+        WarningFailureException warningFailureException = (WarningFailureException) exception.getCause();
+        assertThat(warningFailureException.getResponse().getWarnings(), equalTo(WARNINGS));
+    }
+}
diff --git a/client/rest/src/main/java/org/elasticsearch/client/ResponseException.java b/client/rest/src/main/java/org/elasticsearch/client/ResponseException.java
index 0957e25fb7033..71b545e6ed9eb 100644
--- a/client/rest/src/main/java/org/elasticsearch/client/ResponseException.java
+++ b/client/rest/src/main/java/org/elasticsearch/client/ResponseException.java
@@ -30,7 +30,7 @@
  * Exception thrown when an elasticsearch node responds to a request with a status code that indicates an error.
  * Holds the response that was returned.
  */
-public final class ResponseException extends IOException {
+public class ResponseException extends IOException {
 
     private Response response;
 
diff --git a/client/rest/src/main/java/org/elasticsearch/client/RestClient.java b/client/rest/src/main/java/org/elasticsearch/client/RestClient.java
index 3b1946ef9ed58..6f3541b1ac288 100644
--- a/client/rest/src/main/java/org/elasticsearch/client/RestClient.java
+++ b/client/rest/src/main/java/org/elasticsearch/client/RestClient.java
@@ -301,7 +301,7 @@ public void completed(HttpResponse httpResponse) {
                     if (isSuccessfulResponse(statusCode) || ignoreErrorCodes.contains(response.getStatusLine().getStatusCode())) {
                         onResponse(node);
                         if (thisWarningsHandler.warningsShouldFailRequest(response.getWarnings())) {
-                            listener.onDefinitiveFailure(new ResponseException(response));
+                            listener.onDefinitiveFailure(new WarningFailureException(response));
                         } else {
                             listener.onSuccess(response);
                         }
@@ -686,6 +686,9 @@ Response get() throws IOException {
                  * like the asynchronous API. We wrap the exception so that the caller's
                  * signature shows up in any exception we throw.
                  */
+                if (exception instanceof WarningFailureException) {
+                    throw new WarningFailureException((WarningFailureException) exception);
+                }
                 if (exception instanceof ResponseException) {
                     throw new ResponseException((ResponseException) exception);
                 }
diff --git a/client/rest/src/main/java/org/elasticsearch/client/WarningFailureException.java b/client/rest/src/main/java/org/elasticsearch/client/WarningFailureException.java
new file mode 100644
index 0000000000000..b9648a7ac83c4
--- /dev/null
+++ b/client/rest/src/main/java/org/elasticsearch/client/WarningFailureException.java
@@ -0,0 +1,32 @@
+/*
+ * Licensed to Elasticsearch under one or more contributor
+ * license agreements. See the NOTICE file distributed with
+ * this work for additional information regarding copyright
+ * ownership. Elasticsearch licenses this file to you under
+ * the Apache License, Version 2.0 (the "License"); you may
+ * not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *    http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing,
+ * software distributed under the License is distributed on an
+ * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+ * KIND, either express or implied.  See the License for the
+ * specific language governing permissions and limitations
+ * under the License.
+ */
+
+package org.elasticsearch.client;
+
+import java.io.IOException;
+
+public class WarningFailureException extends ResponseException {
+    public WarningFailureException(Response response) throws IOException {
+        super(response);
+    }
+
+    public WarningFailureException(ResponseException e) throws IOException {
+        super(e);
+    }
+}
diff --git a/client/rest/src/test/java/org/elasticsearch/client/RestClientSingleHostTests.java b/client/rest/src/test/java/org/elasticsearch/client/RestClientSingleHostTests.java
index a37cfe87ca1c8..aaef5404f2802 100644
--- a/client/rest/src/test/java/org/elasticsearch/client/RestClientSingleHostTests.java
+++ b/client/rest/src/test/java/org/elasticsearch/client/RestClientSingleHostTests.java
@@ -421,9 +421,9 @@ private void assertDeprecationWarnings(List<String> warningHeaderTexts, List<Str
         if (expectFailure) {
             try {
                 restClient.performRequest(request);
-                fail("expected ResponseException from warnings");
+                fail("expected WarningFailureException from warnings");
                 return;
-            } catch (ResponseException e) {
+            } catch (WarningFailureException e) {
                 if (false == warningBodyTexts.isEmpty()) {
                     assertThat(e.getMessage(), containsString("\nWarnings: " + warningBodyTexts));
                 }
