diff --git a/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/ApiKeyIntegTests.java b/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/ApiKeyIntegTests.java
index 499578e9cd3ae..9d6b08e47383b 100644
--- a/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/ApiKeyIntegTests.java
+++ b/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/ApiKeyIntegTests.java
@@ -6,6 +6,7 @@
 
 package org.elasticsearch.xpack.security.authc;
 
+import org.elasticsearch.ElasticsearchException;
 import org.elasticsearch.ElasticsearchSecurityException;
 import org.elasticsearch.action.admin.cluster.health.ClusterHealthResponse;
 import org.elasticsearch.action.search.SearchResponse;
@@ -54,7 +55,9 @@
 import static org.hamcrest.Matchers.containsString;
 import static org.hamcrest.Matchers.equalTo;
 import static org.hamcrest.Matchers.is;
+import static org.hamcrest.Matchers.isIn;
 import static org.hamcrest.Matchers.not;
+import static org.hamcrest.Matchers.notNullValue;
 
 public class ApiKeyIntegTests extends SecurityIntegTestCase {
 
@@ -232,7 +235,6 @@ public void testInvalidateApiKeysForApiKeyName() throws InterruptedException, Ex
         verifyInvalidateResponse(1, responses, invalidateResponse);
     }
 
-    @AwaitsFix(bugUrl = "https://github.com/elastic/elasticsearch/issues/38408")
     public void testGetAndInvalidateApiKeysWithExpiredAndInvalidatedApiKey() throws Exception {
         List<CreateApiKeyResponse> responses = createApiKeys(1, null);
         Instant created = Instant.now();
@@ -249,6 +251,9 @@ public void testGetAndInvalidateApiKeysWithExpiredAndInvalidatedApiKey() throws
             assertThat(searchResponse.getHits().getTotalHits().value, equalTo(1L));
             docId.set(searchResponse.getHits().getAt(0).getId());
         });
+        logger.info("searched and found API key with doc id = " + docId.get());
+        assertThat(docId.get(), is(notNullValue()));
+        assertThat(docId.get(), is(responses.get(0).getId()));
 
         // hack doc to modify the expiration time to the week before
         Instant weekBefore = created.minus(8L, ChronoUnit.DAYS);
@@ -259,6 +264,11 @@ public void testGetAndInvalidateApiKeysWithExpiredAndInvalidatedApiKey() throws
         PlainActionFuture<InvalidateApiKeyResponse> listener = new PlainActionFuture<>();
         securityClient.invalidateApiKey(InvalidateApiKeyRequest.usingApiKeyId(responses.get(0).getId()), listener);
         InvalidateApiKeyResponse invalidateResponse = listener.get();
+        if (invalidateResponse.getErrors().isEmpty() == false) {
+            logger.error("error occurred while invalidating API key by id" + invalidateResponse.getErrors().stream()
+                    .map(ElasticsearchException::getMessage)
+                    .collect(Collectors.joining(", ")));
+        }
         verifyInvalidateResponse(1, responses, invalidateResponse);
 
         // try again
@@ -303,6 +313,9 @@ public void testInvalidatedApiKeysDeletedByRemover() throws Exception {
             assertThat(searchResponse.getHits().getTotalHits().value, equalTo(2L));
             docId.set(searchResponse.getHits().getAt(0).getId());
         });
+        logger.info("searched and found API key with doc id = " + docId.get());
+        assertThat(docId.get(), is(notNullValue()));
+        assertThat(docId.get(), isIn(responses.stream().map(CreateApiKeyResponse::getId).collect(Collectors.toList())));
 
         AtomicBoolean deleteTriggered = new AtomicBoolean(false);
         assertBusy(() -> {
@@ -333,6 +346,9 @@ public void testExpiredApiKeysDeletedAfter1Week() throws Exception {
             assertThat(searchResponse.getHits().getTotalHits().value, equalTo(2L));
             docId.set(searchResponse.getHits().getAt(0).getId());
         });
+        logger.info("searched and found API key with doc id = " + docId.get());
+        assertThat(docId.get(), is(notNullValue()));
+        assertThat(docId.get(), isIn(responses.stream().map(CreateApiKeyResponse::getId).collect(Collectors.toList())));
 
         // hack doc to modify the expiration time to the week before
         Instant weekBefore = created.minus(8L, ChronoUnit.DAYS);
@@ -490,6 +506,7 @@ private void verifyGetResponse(int noOfApiKeys, List<CreateApiKeyResponse> respo
             assertNotNull(response.getKey());
             responses.add(response);
         }
+        assertThat(responses.size(), is(noOfApiKeys));
         return responses;
     }
 }
