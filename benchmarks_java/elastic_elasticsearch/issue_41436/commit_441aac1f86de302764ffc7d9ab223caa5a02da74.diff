diff --git a/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/ArchiveTestCase.java b/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/ArchiveTestCase.java
index d427017d0b041..e5cbbcc60ccfc 100644
--- a/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/ArchiveTestCase.java
+++ b/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/ArchiveTestCase.java
@@ -31,7 +31,6 @@
 import org.elasticsearch.packaging.util.Shell;
 import org.elasticsearch.packaging.util.Shell.Result;
 
-import java.nio.charset.StandardCharsets;
 import java.nio.file.Files;
 import java.nio.file.Path;
 import java.nio.file.Paths;
@@ -173,8 +172,8 @@ public void assertRunsWithJavaHome() throws Exception {
         Archives.stopElasticsearch(installation);
 
         String systemJavaHome = sh.getEnv().get("JAVA_HOME");
-        Path log = installation.logs.resolve("elasticsearch.log");
-        assertThat(new String(Files.readAllBytes(log), StandardCharsets.UTF_8), containsString(systemJavaHome));
+        assertThat(FileUtils.slurpAllLogs(installation.logs, "elasticsearch.log", "*.log.gz"),
+            containsString(systemJavaHome));
     }
 
     public void test51JavaHomeOverride() throws Exception {
diff --git a/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/PackageTestCase.java b/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/PackageTestCase.java
index 193edd7c32f60..20781c63f69c9 100644
--- a/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/PackageTestCase.java
+++ b/qa/vagrant/src/main/java/org/elasticsearch/packaging/test/PackageTestCase.java
@@ -124,8 +124,8 @@ public void assertRunsWithJavaHome() throws Exception {
             Files.write(installation.envFile, originalEnvFile);
         }
 
-        Path log = installation.logs.resolve("elasticsearch.log");
-        assertThat(new String(Files.readAllBytes(log), StandardCharsets.UTF_8), containsString(systemJavaHome));
+        assertThat(FileUtils.slurpAllLogs(installation.logs, "elasticsearch.log", "*.log.gz"),
+            containsString(systemJavaHome));
     }
 
     public void test32JavaHomeOverride() throws Exception {
diff --git a/qa/vagrant/src/main/java/org/elasticsearch/packaging/util/FileUtils.java b/qa/vagrant/src/main/java/org/elasticsearch/packaging/util/FileUtils.java
index efbf0bd74a354..ca6c3e48d411e 100644
--- a/qa/vagrant/src/main/java/org/elasticsearch/packaging/util/FileUtils.java
+++ b/qa/vagrant/src/main/java/org/elasticsearch/packaging/util/FileUtils.java
@@ -24,7 +24,10 @@
 import org.hamcrest.Matcher;
 
 import java.io.BufferedWriter;
+import java.io.ByteArrayOutputStream;
 import java.io.IOException;
+import java.nio.channels.Channels;
+import java.nio.channels.FileChannel;
 import java.nio.charset.StandardCharsets;
 import java.nio.file.DirectoryStream;
 import java.nio.file.Files;
@@ -38,6 +41,9 @@
 import java.util.Arrays;
 import java.util.Collections;
 import java.util.List;
+import java.util.StringJoiner;
+import java.util.zip.GZIPInputStream;
+import java.util.zip.ZipException;
 
 import static org.hamcrest.MatcherAssert.assertThat;
 import static org.hamcrest.Matchers.emptyIterable;
@@ -124,6 +130,56 @@ public static String slurp(Path file) {
         }
     }
 
+    /**
+     * Returns the content a {@link java.nio.file.Path} file. The file can be in plain text or GZIP format.
+     * @param file The {@link java.nio.file.Path} to the file.
+     * @return The content of {@code file}.
+     */
+    public static String slurpTxtorGz(Path file) {
+        ByteArrayOutputStream fileBuffer = new ByteArrayOutputStream();
+        try (GZIPInputStream in = new GZIPInputStream(Channels.newInputStream(FileChannel.open(file)))) {
+            byte[] buffer = new byte[1024];
+            int len;
+
+            while ((len = in.read(buffer)) != -1) {
+                fileBuffer.write(buffer, 0, len);
+            }
+
+            return (new String(fileBuffer.toByteArray(), StandardCharsets.UTF_8));
+        } catch (ZipException e) {
+            if (e.toString().contains("Not in GZIP format")) {
+                return slurp(file);
+            }
+            throw new RuntimeException(e);
+        } catch (IOException e) {
+            throw new RuntimeException(e);
+        }
+    }
+
+    /**
+     * Returns combined content of a text log file and rotated log files matching a pattern. Order of rotated log files is
+     * not guaranteed.
+     * @param logPath Base directory where log files reside.
+     * @param activeLogFile The currently active log file. This file needs to be plain text under {@code logPath}.
+     * @param rotatedLogFilesGlob A glob pattern to match rotated log files under {@code logPath}.
+     *                            See {@link java.nio.file.FileSystem#getPathMatcher(String)} for glob examples.
+     * @return Merges contents of {@code activeLogFile} and contents of filenames matching {@code rotatedLogFilesGlob}.
+     * File contents are separated by a newline. The order of rotated log files matched by {@code rotatedLogFilesGlob} is not guaranteed.
+     */
+    public static String slurpAllLogs(Path logPath, String activeLogFile, String rotatedLogFilesGlob) {
+        StringJoiner logFileJoiner = new StringJoiner("\n");
+        try {
+            logFileJoiner.add(new String(Files.readAllBytes(logPath.resolve(activeLogFile)), StandardCharsets.UTF_8));
+
+            for (Path rotatedLogFile : FileUtils.lsGlob(logPath, rotatedLogFilesGlob)) {
+                logFileJoiner.add(FileUtils.slurpTxtorGz(rotatedLogFile));
+            }
+            return(logFileJoiner.toString());
+        } catch (IOException e) {
+            throw new RuntimeException(e);
+        }
+    }
+
     /**
      * Gets the owner of a file in a way that should be supported by all filesystems that have a concept of file owner
      */
