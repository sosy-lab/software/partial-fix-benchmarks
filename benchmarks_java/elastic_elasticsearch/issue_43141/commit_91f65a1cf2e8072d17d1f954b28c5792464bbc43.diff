diff --git a/client/rest/src/test/java/org/elasticsearch/client/RestClientBuilderIntegTests.java b/client/rest/src/test/java/org/elasticsearch/client/RestClientBuilderIntegTests.java
index 8bc8d73fd2b26..4aa69d3b3e9c1 100644
--- a/client/rest/src/test/java/org/elasticsearch/client/RestClientBuilderIntegTests.java
+++ b/client/rest/src/test/java/org/elasticsearch/client/RestClientBuilderIntegTests.java
@@ -134,7 +134,7 @@ private static SSLContext getSslContext() throws Exception {
      * 12.0.1 so we pin to TLSv1.2 when running on an earlier JDK
      */
     private static String getProtocol() {
-        String version = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getProperty("java.specification.version"));
+        String version = AccessController.doPrivileged((PrivilegedAction<String>) () -> System.getProperty("java.version"));
         String[] components = version.split("\\.");
         if (components.length > 0) {
             final int major = Integer.valueOf(components[0]);
diff --git a/libs/core/src/main/java/org/elasticsearch/bootstrap/JavaVersion.java b/libs/core/src/main/java/org/elasticsearch/bootstrap/JavaVersion.java
index f22087c6e7d8d..89dd91b28e8d9 100644
--- a/libs/core/src/main/java/org/elasticsearch/bootstrap/JavaVersion.java
+++ b/libs/core/src/main/java/org/elasticsearch/bootstrap/JavaVersion.java
@@ -28,12 +28,14 @@
 public class JavaVersion implements Comparable<JavaVersion> {
 
     private final List<Integer> version;
+    private final boolean earlyAccess;
 
     public List<Integer> getVersion() {
         return version;
     }
 
-    private JavaVersion(List<Integer> version) {
+    private JavaVersion(List<Integer> version, boolean earlyAccess) {
+        this.earlyAccess = earlyAccess;
         if (version.size() >= 2 && version.get(0) == 1 && version.get(1) == 8) {
             // for Java 8 there is ambiguity since both 1.8 and 8 are supported,
             // so we rewrite the former to the latter
@@ -43,22 +45,26 @@ private JavaVersion(List<Integer> version) {
     }
 
     public static JavaVersion parse(String value) {
+        boolean earlyAccess = false;
         Objects.requireNonNull(value);
         if (!isValid(value)) {
             throw new IllegalArgumentException("value");
         }
-
+        if (value.endsWith("-ea")) {
+            value = value.substring(0, value.length() - 3);
+            earlyAccess = true;
+        }
         List<Integer> version = new ArrayList<>();
-        String[] components = value.split("\\.");
+        String[] components = value.replace("-ea", "").split("\\.");
         for (String component : components) {
             version.add(Integer.valueOf(component));
         }
 
-        return new JavaVersion(version);
+        return new JavaVersion(version, earlyAccess);
     }
 
     public static boolean isValid(String value) {
-        return value.matches("^0*[0-9]+(\\.[0-9]+)*$");
+        return value.matches("^0*[0-9]+(\\.[0-9]+)*(-ea)?$");
     }
 
     private static final JavaVersion CURRENT = parse(System.getProperty("java.specification.version"));
@@ -67,6 +73,10 @@ public static JavaVersion current() {
         return CURRENT;
     }
 
+    public boolean isEarlyAccess() {
+        return earlyAccess;
+    }
+
     @Override
     public int compareTo(JavaVersion o) {
         int len = Math.max(version.size(), o.version.size());
@@ -78,7 +88,12 @@ public int compareTo(JavaVersion o) {
             if (s > d)
                 return -1;
         }
-        return 0;
+        if (isEarlyAccess()) {
+            return -1;
+        } else if (o.isEarlyAccess()) {
+            return 1;
+        } else
+            return 0;
     }
 
     @Override
@@ -96,6 +111,7 @@ public int hashCode() {
 
     @Override
     public String toString() {
-        return version.stream().map(v -> Integer.toString(v)).collect(Collectors.joining("."));
+        final String versionString = version.stream().map(v -> Integer.toString(v)).collect(Collectors.joining("."));
+        return earlyAccess ? versionString + "-ea" : versionString;
     }
 }
diff --git a/plugins/discovery-azure-classic/src/test/java/org/elasticsearch/discovery/azure/classic/AzureDiscoveryClusterFormationTests.java b/plugins/discovery-azure-classic/src/test/java/org/elasticsearch/discovery/azure/classic/AzureDiscoveryClusterFormationTests.java
index c5b310c14817a..e723520f5c8b5 100644
--- a/plugins/discovery-azure-classic/src/test/java/org/elasticsearch/discovery/azure/classic/AzureDiscoveryClusterFormationTests.java
+++ b/plugins/discovery-azure-classic/src/test/java/org/elasticsearch/discovery/azure/classic/AzureDiscoveryClusterFormationTests.java
@@ -280,7 +280,7 @@ private static String getProtocol() {
         } else {
             JavaVersion full =
                 AccessController.doPrivileged(
-                        (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.specification.version")));
+                    (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.version")));
             if (full.compareTo(JavaVersion.parse("12.0.1")) < 0) {
                 return "TLSv1.2";
             }
diff --git a/server/src/test/java/org/elasticsearch/bootstrap/JavaVersionTests.java b/server/src/test/java/org/elasticsearch/bootstrap/JavaVersionTests.java
index a6e74a4770635..60c807c67348a 100644
--- a/server/src/test/java/org/elasticsearch/bootstrap/JavaVersionTests.java
+++ b/server/src/test/java/org/elasticsearch/bootstrap/JavaVersionTests.java
@@ -29,10 +29,18 @@
     public void testParse() {
         JavaVersion javaVersion = JavaVersion.parse("1.7.0");
         List<Integer> version = javaVersion.getVersion();
-        assertThat(3, is(version.size()));
-        assertThat(1, is(version.get(0)));
-        assertThat(7, is(version.get(1)));
-        assertThat(0, is(version.get(2)));
+        assertThat(version.size(), is(3));
+        assertThat(version.get(0), is(1));
+        assertThat(version.get(1), is(7));
+        assertThat(version.get(2), is(0));
+
+        JavaVersion javaVersionEarlyAccess = JavaVersion.parse("14.0.1-ea");
+        List<Integer> version14 = javaVersionEarlyAccess.getVersion();
+        assertThat(version14.size(), is(3));
+        assertThat(version14.get(0), is(14));
+        assertThat(version14.get(1), is(0));
+        assertThat(version14.get(2), is(1));
+        assertTrue(javaVersionEarlyAccess.isEarlyAccess());
     }
 
     public void testToString() {
@@ -40,6 +48,8 @@ public void testToString() {
         assertThat(javaVersion170.toString(), is("1.7.0"));
         JavaVersion javaVersion9 = JavaVersion.parse("9");
         assertThat(javaVersion9.toString(), is("9"));
+        JavaVersion javaVersion13ea = JavaVersion.parse("13.1-ea");
+        assertThat(javaVersion13ea.toString(), is("13.1-ea"));
     }
 
     public void testCompare() {
@@ -50,6 +60,9 @@ public void testCompare() {
         JavaVersion onePointSevenPointTwo = JavaVersion.parse("1.7.2");
         JavaVersion onePointSevenPointOnePointOne = JavaVersion.parse("1.7.1.1");
         JavaVersion onePointSevenPointTwoPointOne = JavaVersion.parse("1.7.2.1");
+        JavaVersion fourteen = JavaVersion.parse("14");
+        JavaVersion fourteenPointTwoPointOne = JavaVersion.parse("14.2.1");
+        JavaVersion fourteenPointTwoPointOneEarlyAccess = JavaVersion.parse("14.2.1-ea");
 
         assertTrue(onePointSix.compareTo(onePointSeven) < 0);
         assertTrue(onePointSeven.compareTo(onePointSix) > 0);
@@ -57,10 +70,13 @@ public void testCompare() {
         assertTrue(onePointSeven.compareTo(onePointSevenPointZero) == 0);
         assertTrue(onePointSevenPointOnePointOne.compareTo(onePointSevenPointOne) > 0);
         assertTrue(onePointSevenPointTwo.compareTo(onePointSevenPointTwoPointOne) < 0);
+        assertTrue(fourteenPointTwoPointOneEarlyAccess.compareTo(fourteenPointTwoPointOne) < 0);
+        assertTrue(fourteenPointTwoPointOneEarlyAccess.compareTo(fourteen) > 0);
+
     }
 
     public void testValidVersions() {
-        String[] versions = new String[]{"1.7", "1.7.0", "0.1.7", "1.7.0.80"};
+        String[] versions = new String[]{"1.7", "1.7.0", "0.1.7", "1.7.0.80", "12-ea", "13.0.2.3-ea"};
         for (String version : versions) {
             assertTrue(JavaVersion.isValid(version));
         }
@@ -76,4 +92,4 @@ public void testInvalidVersions() {
     public void testJava8Compat() {
         assertEquals(JavaVersion.parse("1.8"), JavaVersion.parse("8"));
     }
-}
\ No newline at end of file
+}
diff --git a/x-pack/plugin/monitoring/src/test/java/org/elasticsearch/xpack/monitoring/exporter/http/HttpExporterSslIT.java b/x-pack/plugin/monitoring/src/test/java/org/elasticsearch/xpack/monitoring/exporter/http/HttpExporterSslIT.java
index 19a981939c480..3aa07a58a7666 100644
--- a/x-pack/plugin/monitoring/src/test/java/org/elasticsearch/xpack/monitoring/exporter/http/HttpExporterSslIT.java
+++ b/x-pack/plugin/monitoring/src/test/java/org/elasticsearch/xpack/monitoring/exporter/http/HttpExporterSslIT.java
@@ -204,7 +204,7 @@ private void clearTransientSettings(String... names) {
         } else {
             JavaVersion full =
                 AccessController.doPrivileged(
-                        (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.specification.version")));
+                    (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.version")));
             if (full.compareTo(JavaVersion.parse("12.0.1")) < 0) {
                 return List.of("TLSv1.2");
             }
diff --git a/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/saml/SamlRealmTests.java b/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/saml/SamlRealmTests.java
index 52961b52e3815..824655d59c7e5 100644
--- a/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/saml/SamlRealmTests.java
+++ b/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/security/authc/saml/SamlRealmTests.java
@@ -732,7 +732,7 @@ private void assertIdp1MetadataParsedCorrectly(EntityDescriptor descriptor) {
         } else {
             JavaVersion full =
                 AccessController.doPrivileged(
-                        (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.specification.version")));
+                    (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.version")));
             if (full.compareTo(JavaVersion.parse("12.0.1")) < 0) {
                 return List.of("TLSv1.2");
             }
diff --git a/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/ssl/SSLClientAuthTests.java b/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/ssl/SSLClientAuthTests.java
index 7d2bcf1232519..a50d1b5a9b818 100644
--- a/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/ssl/SSLClientAuthTests.java
+++ b/x-pack/plugin/security/src/test/java/org/elasticsearch/xpack/ssl/SSLClientAuthTests.java
@@ -164,7 +164,7 @@ private SSLContext getSSLContext() {
     private static List<String> getProtocols() {
         JavaVersion full =
             AccessController.doPrivileged(
-                    (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.specification.version")));
+                (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.version")));
         if (full.compareTo(JavaVersion.parse("11.0.3")) < 0) {
             return List.of("TLSv1.2");
         }
diff --git a/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/actions/webhook/WebhookHttpsIntegrationTests.java b/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/actions/webhook/WebhookHttpsIntegrationTests.java
index e5cf3a54a4c66..c03d924cd6faa 100644
--- a/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/actions/webhook/WebhookHttpsIntegrationTests.java
+++ b/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/actions/webhook/WebhookHttpsIntegrationTests.java
@@ -150,7 +150,7 @@ public void testHttpsAndBasicAuth() throws Exception {
         } else {
             JavaVersion full =
                 AccessController.doPrivileged(
-                        (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.specification.version")));
+                    (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.version")));
             if (full.compareTo(JavaVersion.parse("12.0.1")) < 0) {
                 return List.of("TLSv1.2");
             }
diff --git a/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/common/http/HttpClientTests.java b/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/common/http/HttpClientTests.java
index d24bba0b1ba57..cc1fdec18bafe 100644
--- a/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/common/http/HttpClientTests.java
+++ b/x-pack/plugin/watcher/src/test/java/org/elasticsearch/xpack/watcher/common/http/HttpClientTests.java
@@ -759,7 +759,7 @@ private String getWebserverUri() {
         } else {
             JavaVersion full =
                 AccessController.doPrivileged(
-                        (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.specification.version")));
+                    (PrivilegedAction<JavaVersion>) () -> JavaVersion.parse(System.getProperty("java.version")));
             if (full.compareTo(JavaVersion.parse("12.0.1")) < 0) {
                 return List.of("TLSv1.2");
             }
