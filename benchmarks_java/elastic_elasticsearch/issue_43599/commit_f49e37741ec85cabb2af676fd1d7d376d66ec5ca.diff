diff --git a/server/src/main/java/org/elasticsearch/common/settings/ClusterSettings.java b/server/src/main/java/org/elasticsearch/common/settings/ClusterSettings.java
index 81429e011f49c..b5db483a76e0b 100644
--- a/server/src/main/java/org/elasticsearch/common/settings/ClusterSettings.java
+++ b/server/src/main/java/org/elasticsearch/common/settings/ClusterSettings.java
@@ -194,6 +194,7 @@ public void apply(Settings value, Settings current, Settings previous) {
             IndicesQueryCache.INDICES_CACHE_QUERY_SIZE_SETTING,
             IndicesQueryCache.INDICES_CACHE_QUERY_COUNT_SETTING,
             IndicesQueryCache.INDICES_QUERIES_CACHE_ALL_SEGMENTS_SETTING,
+            IndicesService.INDICES_ID_FIELD_DATA_ENABLED_SETTING,
             MappingUpdatedAction.INDICES_MAPPING_DYNAMIC_TIMEOUT_SETTING,
             MetaData.SETTING_READ_ONLY_SETTING,
             MetaData.SETTING_READ_ONLY_ALLOW_DELETE_SETTING,
diff --git a/server/src/main/java/org/elasticsearch/index/IndexModule.java b/server/src/main/java/org/elasticsearch/index/IndexModule.java
index b42bb2c2cb384..752c583b04c6a 100644
--- a/server/src/main/java/org/elasticsearch/index/IndexModule.java
+++ b/server/src/main/java/org/elasticsearch/index/IndexModule.java
@@ -74,6 +74,7 @@
 import java.util.Set;
 import java.util.concurrent.atomic.AtomicBoolean;
 import java.util.function.BiFunction;
+import java.util.function.BooleanSupplier;
 import java.util.function.Consumer;
 import java.util.function.Function;
 
@@ -394,7 +395,8 @@ public IndexService newIndexService(
             IndicesQueryCache indicesQueryCache,
             MapperRegistry mapperRegistry,
             IndicesFieldDataCache indicesFieldDataCache,
-            NamedWriteableRegistry namedWriteableRegistry)
+            NamedWriteableRegistry namedWriteableRegistry,
+            BooleanSupplier idFieldDataEnabled)
         throws IOException {
         final IndexEventListener eventListener = freeze();
         Function<IndexService, CheckedFunction<DirectoryReader, DirectoryReader, IOException>> readerWrapperFactory =
@@ -422,7 +424,7 @@ public IndexService newIndexService(
                 new SimilarityService(indexSettings, scriptService, similarities), shardStoreDeleter, indexAnalyzers,
                 engineFactory, circuitBreakerService, bigArrays, threadPool, scriptService, clusterService, client, queryCache,
                 directoryFactory, eventListener, readerWrapperFactory, mapperRegistry, indicesFieldDataCache, searchOperationListeners,
-                indexOperationListeners, namedWriteableRegistry);
+                indexOperationListeners, namedWriteableRegistry, idFieldDataEnabled);
             success = true;
             return indexService;
         } finally {
diff --git a/server/src/main/java/org/elasticsearch/index/IndexService.java b/server/src/main/java/org/elasticsearch/index/IndexService.java
index 7c29e342cb1de..47c3bfb35fee7 100644
--- a/server/src/main/java/org/elasticsearch/index/IndexService.java
+++ b/server/src/main/java/org/elasticsearch/index/IndexService.java
@@ -92,6 +92,7 @@
 import java.util.Set;
 import java.util.concurrent.TimeUnit;
 import java.util.concurrent.atomic.AtomicBoolean;
+import java.util.function.BooleanSupplier;
 import java.util.function.Consumer;
 import java.util.function.Function;
 import java.util.function.LongSupplier;
@@ -161,7 +162,8 @@ public IndexService(
             IndicesFieldDataCache indicesFieldDataCache,
             List<SearchOperationListener> searchOperationListeners,
             List<IndexingOperationListener> indexingOperationListeners,
-            NamedWriteableRegistry namedWriteableRegistry) {
+            NamedWriteableRegistry namedWriteableRegistry,
+            BooleanSupplier idFieldDataEnabled) {
         super(indexSettings);
         this.indexSettings = indexSettings;
         this.xContentRegistry = xContentRegistry;
@@ -172,7 +174,7 @@ public IndexService(
             assert indexAnalyzers != null;
             this.mapperService = new MapperService(indexSettings, indexAnalyzers, xContentRegistry, similarityService, mapperRegistry,
                 // we parse all percolator queries as they would be parsed on shard 0
-                () -> newQueryShardContext(0, null, System::currentTimeMillis, null));
+                () -> newQueryShardContext(0, null, System::currentTimeMillis, null), idFieldDataEnabled);
             this.indexFieldData = new IndexFieldDataService(indexSettings, indicesFieldDataCache, circuitBreakerService, mapperService);
             if (indexSettings.getIndexSortConfig().hasIndexSort()) {
                 // we delay the actual creation of the sort order for this index because the mapping has not been merged yet.
diff --git a/server/src/main/java/org/elasticsearch/index/mapper/IdFieldMapper.java b/server/src/main/java/org/elasticsearch/index/mapper/IdFieldMapper.java
index e0a2cd7ee428b..325f6d7b0a41b 100644
--- a/server/src/main/java/org/elasticsearch/index/mapper/IdFieldMapper.java
+++ b/server/src/main/java/org/elasticsearch/index/mapper/IdFieldMapper.java
@@ -19,6 +19,7 @@
 
 package org.elasticsearch.index.mapper;
 
+import org.apache.logging.log4j.LogManager;
 import org.apache.lucene.document.Field;
 import org.apache.lucene.index.IndexOptions;
 import org.apache.lucene.index.IndexableField;
@@ -28,6 +29,7 @@
 import org.apache.lucene.search.SortField;
 import org.apache.lucene.search.TermInSetQuery;
 import org.apache.lucene.util.BytesRef;
+import org.elasticsearch.common.logging.DeprecationLogger;
 import org.elasticsearch.common.lucene.Lucene;
 import org.elasticsearch.common.xcontent.XContentBuilder;
 import org.elasticsearch.index.Index;
@@ -41,6 +43,7 @@
 import org.elasticsearch.index.fielddata.fieldcomparator.BytesRefFieldComparatorSource;
 import org.elasticsearch.index.fielddata.plain.PagedBytesIndexFieldData;
 import org.elasticsearch.index.query.QueryShardContext;
+import org.elasticsearch.indices.IndicesService;
 import org.elasticsearch.indices.breaker.CircuitBreakerService;
 import org.elasticsearch.search.MultiValueMode;
 
@@ -55,6 +58,10 @@
  * queries.
  */
 public class IdFieldMapper extends MetadataFieldMapper {
+    private static final DeprecationLogger deprecationLogger = new DeprecationLogger(LogManager.getLogger(IdFieldMapper.class));
+    static final String ID_FIELD_DATA_DEPRECATION_MESSAGE =
+        "Loading the fielddata on the _id field is deprecated and will be removed in future versions. "
+            + "If you require sorting or aggregating on this field you should use concrete copy field with docvalues enabled.";
 
     public static final String NAME = "_id";
 
@@ -158,6 +165,12 @@ public Query termsQuery(List<?> values, QueryShardContext context) {
                 @Override
                 public IndexFieldData<?> build(IndexSettings indexSettings, MappedFieldType fieldType, IndexFieldDataCache cache,
                         CircuitBreakerService breakerService, MapperService mapperService) {
+                    if (mapperService.isIdFieldDataEnabled() == false) {
+                        throw new IllegalArgumentException("Fielddata access on the _id field is disallowed, "
+                            + "you can re-enable it by updating the dynamic cluster setting: "
+                            + IndicesService.INDICES_ID_FIELD_DATA_ENABLED_SETTING.getKey());
+                    }
+                    deprecationLogger.deprecatedAndMaybeLog("id_field_data", ID_FIELD_DATA_DEPRECATION_MESSAGE);
                     final IndexFieldData<?> fieldData = fieldDataBuilder.build(indexSettings, fieldType, cache,
                         breakerService, mapperService);
                     return new IndexFieldData<AtomicFieldData>() {
diff --git a/server/src/main/java/org/elasticsearch/index/mapper/MapperService.java b/server/src/main/java/org/elasticsearch/index/mapper/MapperService.java
index 2a57cacf7ff45..d60a6dfb4152e 100644
--- a/server/src/main/java/org/elasticsearch/index/mapper/MapperService.java
+++ b/server/src/main/java/org/elasticsearch/index/mapper/MapperService.java
@@ -67,6 +67,7 @@
 import java.util.List;
 import java.util.Map;
 import java.util.Set;
+import java.util.function.BooleanSupplier;
 import java.util.function.Function;
 import java.util.function.Supplier;
 import java.util.stream.Stream;
@@ -142,9 +143,18 @@
 
     final MapperRegistry mapperRegistry;
 
+    private final BooleanSupplier idFieldDataEnabled;
+
     public MapperService(IndexSettings indexSettings, IndexAnalyzers indexAnalyzers, NamedXContentRegistry xContentRegistry,
                          SimilarityService similarityService, MapperRegistry mapperRegistry,
                          Supplier<QueryShardContext> queryShardContextSupplier) {
+       this(indexSettings, indexAnalyzers, xContentRegistry, similarityService, mapperRegistry, queryShardContextSupplier, () -> false);
+    }
+
+
+    public MapperService(IndexSettings indexSettings, IndexAnalyzers indexAnalyzers, NamedXContentRegistry xContentRegistry,
+                         SimilarityService similarityService, MapperRegistry mapperRegistry,
+                         Supplier<QueryShardContext> queryShardContextSupplier, BooleanSupplier idFieldDataEnabled) {
         super(indexSettings);
         this.indexAnalyzers = indexAnalyzers;
         this.fieldTypes = new FieldTypeLookup();
@@ -154,12 +164,12 @@ public MapperService(IndexSettings indexSettings, IndexAnalyzers indexAnalyzers,
         this.searchAnalyzer = new MapperAnalyzerWrapper(indexAnalyzers.getDefaultSearchAnalyzer(), p -> p.searchAnalyzer());
         this.searchQuoteAnalyzer = new MapperAnalyzerWrapper(indexAnalyzers.getDefaultSearchQuoteAnalyzer(), p -> p.searchQuoteAnalyzer());
         this.mapperRegistry = mapperRegistry;
+        this.idFieldDataEnabled = idFieldDataEnabled;
 
         if (INDEX_MAPPER_DYNAMIC_SETTING.exists(indexSettings.getSettings()) &&
                 indexSettings.getIndexVersionCreated().onOrAfter(Version.V_7_0_0)) {
             throw new IllegalArgumentException("Setting " + INDEX_MAPPER_DYNAMIC_SETTING.getKey() + " was removed after version 6.0.0");
         }
-
     }
 
     public boolean hasNested() {
@@ -663,6 +673,13 @@ public Analyzer searchQuoteAnalyzer() {
         return this.searchQuoteAnalyzer;
     }
 
+    /**
+     * Returns <code>true</code> if fielddata is enabled for the {@link IdFieldMapper} field, <code>false</code> otherwise.
+     */
+    public boolean isIdFieldDataEnabled() {
+        return idFieldDataEnabled.getAsBoolean();
+    }
+
     @Override
     public void close() throws IOException {
         indexAnalyzers.close();
diff --git a/server/src/main/java/org/elasticsearch/indices/IndicesService.java b/server/src/main/java/org/elasticsearch/indices/IndicesService.java
index 3e2c4d45055c1..ead99e2511116 100644
--- a/server/src/main/java/org/elasticsearch/indices/IndicesService.java
+++ b/server/src/main/java/org/elasticsearch/indices/IndicesService.java
@@ -92,6 +92,7 @@
 import org.elasticsearch.index.fielddata.IndexFieldDataCache;
 import org.elasticsearch.index.flush.FlushStats;
 import org.elasticsearch.index.get.GetStats;
+import org.elasticsearch.index.mapper.IdFieldMapper;
 import org.elasticsearch.index.mapper.MapperService;
 import org.elasticsearch.index.merge.MergeStats;
 import org.elasticsearch.index.query.QueryBuilder;
@@ -168,6 +169,9 @@
     public static final String INDICES_SHARDS_CLOSED_TIMEOUT = "indices.shards_closed_timeout";
     public static final Setting<TimeValue> INDICES_CACHE_CLEAN_INTERVAL_SETTING =
         Setting.positiveTimeSetting("indices.cache.cleanup_interval", TimeValue.timeValueMinutes(1), Property.NodeScope);
+    public static final Setting<Boolean> INDICES_ID_FIELD_DATA_ENABLED_SETTING =
+        Setting.boolSetting("indices.id_field_data.enabled", false, Property.Dynamic, Property.NodeScope);
+
 
     /**
      * The node's settings.
@@ -203,6 +207,7 @@
     private final Map<String, IndexStorePlugin.DirectoryFactory> directoryFactories;
     final AbstractRefCounted indicesRefCount; // pkg-private for testing
     private final CountDownLatch closeLatch = new CountDownLatch(1);
+    private volatile boolean idFieldDataEnabled;
 
     @Override
     protected void doStart() {
@@ -238,6 +243,8 @@ public IndicesService(Settings settings, PluginsService pluginsService, NodeEnvi
         this.scriptService = scriptService;
         this.clusterService = clusterService;
         this.client = client;
+        this.idFieldDataEnabled = INDICES_ID_FIELD_DATA_ENABLED_SETTING.get(clusterService.getSettings());
+        clusterService.getClusterSettings().addSettingsUpdateConsumer(INDICES_ID_FIELD_DATA_ENABLED_SETTING, this::setIdFieldDataEnabled);
         this.indicesFieldDataCache = new IndicesFieldDataCache(settings, new IndexFieldDataCache.Listener() {
             @Override
             public void onRemoval(ShardId shardId, String fieldName, boolean wasEvicted, long sizeInBytes) {
@@ -558,7 +565,8 @@ private synchronized IndexService createIndexService(IndexService.IndexCreationC
                 indicesQueryCache,
                 mapperRegistry,
                 indicesFieldDataCache,
-                namedWriteableRegistry
+                namedWriteableRegistry,
+                this::isIdFieldDataEnabled
         );
     }
 
@@ -1445,6 +1453,17 @@ public boolean isMetaDataField(Version indexCreatedVersion, String field) {
         return mapperRegistry.isMetaDataField(indexCreatedVersion, field);
     }
 
+    /**
+     * Returns <code>true</code> if fielddata is enabled for the {@link IdFieldMapper} field, <code>false</code> otherwise.
+     */
+    public boolean isIdFieldDataEnabled() {
+        return idFieldDataEnabled;
+    }
+
+    private void setIdFieldDataEnabled(boolean value) {
+        this.idFieldDataEnabled = value;
+    }
+
     /**
      * Checks to see if an operation can be performed without taking the cluster over the cluster-wide shard limit. Adds a deprecation
      * warning or returns an error message as appropriate
diff --git a/server/src/test/java/org/elasticsearch/index/IndexModuleTests.java b/server/src/test/java/org/elasticsearch/index/IndexModuleTests.java
index 6310bf0457be4..adeb49faa8941 100644
--- a/server/src/test/java/org/elasticsearch/index/IndexModuleTests.java
+++ b/server/src/test/java/org/elasticsearch/index/IndexModuleTests.java
@@ -160,7 +160,7 @@ public void tearDown() throws Exception {
     private IndexService newIndexService(IndexModule module) throws IOException {
         return module.newIndexService(CREATE_INDEX, nodeEnvironment, xContentRegistry(), deleter, circuitBreakerService, bigArrays,
                 threadPool, scriptService, clusterService, null, indicesQueryCache, mapperRegistry,
-                new IndicesFieldDataCache(settings, listener), writableRegistry());
+                new IndicesFieldDataCache(settings, listener), writableRegistry(), () -> false);
     }
 
     public void testWrapperIsBound() throws IOException {
diff --git a/server/src/test/java/org/elasticsearch/index/mapper/IdFieldMapperTests.java b/server/src/test/java/org/elasticsearch/index/mapper/IdFieldMapperTests.java
index 4f2198c577a3f..b9ca422cb3c7f 100644
--- a/server/src/test/java/org/elasticsearch/index/mapper/IdFieldMapperTests.java
+++ b/server/src/test/java/org/elasticsearch/index/mapper/IdFieldMapperTests.java
@@ -28,7 +28,9 @@
 import org.elasticsearch.common.settings.Settings;
 import org.elasticsearch.common.xcontent.XContentFactory;
 import org.elasticsearch.common.xcontent.XContentType;
+import org.elasticsearch.index.IndexService;
 import org.elasticsearch.index.mapper.MapperService.MergeReason;
+import org.elasticsearch.indices.IndicesService;
 import org.elasticsearch.plugins.Plugin;
 import org.elasticsearch.test.ESSingleNodeTestCase;
 import org.elasticsearch.test.InternalSettingsPlugin;
@@ -37,6 +39,9 @@
 import java.util.Collection;
 import java.util.Collections;
 
+import static org.elasticsearch.index.mapper.IdFieldMapper.ID_FIELD_DATA_DEPRECATION_MESSAGE;
+import static org.hamcrest.Matchers.containsString;
+
 public class IdFieldMapperTests extends ESSingleNodeTestCase {
 
     @Override
@@ -71,4 +76,32 @@ public void testDefaults() throws IOException {
         assertEquals(Uid.encodeId("id"), fields[0].binaryValue());
     }
 
+    public void testEnableFieldData() throws IOException {
+        IndexService service = createIndex("test", Settings.EMPTY);
+        MapperService mapperService = service.mapperService();
+        mapperService.merge("type", new CompressedXContent("{\"type\":{}}"), MergeReason.MAPPING_UPDATE);
+        IdFieldMapper.IdFieldType ft = (IdFieldMapper.IdFieldType) service.mapperService().fullName("_id");
+
+
+        IllegalArgumentException exc = expectThrows(IllegalArgumentException.class,
+            () -> ft.fielddataBuilder("test").build(mapperService.getIndexSettings(),
+                ft, null, null, mapperService));
+        assertThat(exc.getMessage(), containsString(IndicesService.INDICES_ID_FIELD_DATA_ENABLED_SETTING.getKey()));
+
+        client().admin().cluster().prepareUpdateSettings()
+            .setTransientSettings(Settings.builder().put(IndicesService.INDICES_ID_FIELD_DATA_ENABLED_SETTING.getKey(), true))
+            .get();
+
+        try {
+            ft.fielddataBuilder("test").build(mapperService.getIndexSettings(),
+                ft, null, null, mapperService);
+            assertWarnings(ID_FIELD_DATA_DEPRECATION_MESSAGE);
+        } finally {
+            // unset cluster setting
+            client().admin().cluster().prepareUpdateSettings()
+                .setTransientSettings(Settings.builder().putNull(IndicesService.INDICES_ID_FIELD_DATA_ENABLED_SETTING.getKey()))
+                .get();
+        }
+    }
+
 }
