diff --git a/x-pack/plugin/ml/src/main/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinder.java b/x-pack/plugin/ml/src/main/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinder.java
index 9dec0ddbf5ba0..08686ed26a31c 100644
--- a/x-pack/plugin/ml/src/main/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinder.java
+++ b/x-pack/plugin/ml/src/main/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinder.java
@@ -279,9 +279,10 @@ public TimestampFormatFinder(List<String> explanation, @Nullable String override
                         }
                         throw new IllegalArgumentException(msg);
                     }
-                    // No need to append to the Grok pattern as %{SECOND} already allows for an optional
-                    // fraction, but we need to remove the separator that's included in %{SECOND}
-                    grokPatternBuilder.deleteCharAt(grokPatternBuilder.length() - 1);
+                    // No need to append to the Grok pattern as %{SECOND} already allows for an optional fraction,
+                    // but we need to remove the separator that's included in %{SECOND} (and that might be escaped)
+                    int numCharsToDelete = (PUNCTUATION_THAT_NEEDS_ESCAPING_IN_REGEX.indexOf(prevChar) >= 0) ? 2 : 1;
+                    grokPatternBuilder.delete(grokPatternBuilder.length() - numCharsToDelete, grokPatternBuilder.length());
                     regexBuilder.append("\\d{").append(endPos - startPos).append('}');
                 } else {
                     grokPatternBuilder.append(grokPatternAndRegexForGroup.v1());
diff --git a/x-pack/plugin/ml/src/test/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinderTests.java b/x-pack/plugin/ml/src/test/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinderTests.java
index 0b872e630b6ab..12fb14d642a55 100644
--- a/x-pack/plugin/ml/src/test/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinderTests.java
+++ b/x-pack/plugin/ml/src/test/java/org/elasticsearch/xpack/ml/filestructurefinder/TimestampFormatFinderTests.java
@@ -737,16 +737,27 @@ public void testCustomOverrideMatchingBuiltInFormat() {
         assertEquals(1, lenientTimestampFormatFinder.getNumMatchedFormats());
     }
 
-    public void testCustomOverrideNotMatchingBuiltInFormat() {
+    public void testCustomOverridesNotMatchingBuiltInFormat() {
 
-        String overrideFormat = "MM/dd HH.mm.ss,SSSSSS 'in' yyyy";
-        String text = "05/15 17.14.56,374946 in 2018";
-        String expectedSimpleRegex = "\\b\\d{2}/\\d{2} \\d{2}\\.\\d{2}\\.\\d{2},\\d{6} in \\d{4}\\b";
-        String expectedGrokPatternName = "CUSTOM_TIMESTAMP";
-        Map<String, String> expectedCustomGrokPatternDefinitions =
+        validateCustomOverrideNotMatchingBuiltInFormat("MM/dd HH.mm.ss,SSSSSS 'in' yyyy", "05/15 17.14.56,374946 in 2018",
+            "\\b\\d{2}/\\d{2} \\d{2}\\.\\d{2}\\.\\d{2},\\d{6} in \\d{4}\\b", "CUSTOM_TIMESTAMP",
             Collections.singletonMap(TimestampFormatFinder.CUSTOM_TIMESTAMP_GROK_NAME,
-                "%{MONTHNUM2}/%{MONTHDAY} %{HOUR}\\.%{MINUTE}\\.%{SECOND} in %{YEAR}");
+                "%{MONTHNUM2}/%{MONTHDAY} %{HOUR}\\.%{MINUTE}\\.%{SECOND} in %{YEAR}"));
 
+        validateCustomOverrideNotMatchingBuiltInFormat("'some_prefix 'dd.MM.yyyy HH:mm:ss.SSSSSS", "some_prefix 06.01.2018 16:56:14.295748",
+            "some_prefix \\d{2}\\.\\d{2}\\.\\d{4} \\d{2}:\\d{2}:\\d{2}\\.\\d{6}\\b", "CUSTOM_TIMESTAMP",
+            Collections.singletonMap(TimestampFormatFinder.CUSTOM_TIMESTAMP_GROK_NAME,
+                "some_prefix %{MONTHDAY}\\.%{MONTHNUM2}\\.%{YEAR} %{HOUR}:%{MINUTE}:%{SECOND}"));
+
+        validateCustomOverrideNotMatchingBuiltInFormat("dd.MM. yyyy HH:mm:ss.SSSSSS", "06.01. 2018 16:56:14.295748",
+            "\\b\\d{2}\\.\\d{2}\\. \\d{4} \\d{2}:\\d{2}:\\d{2}\\.\\d{6}\\b", "CUSTOM_TIMESTAMP",
+            Collections.singletonMap(TimestampFormatFinder.CUSTOM_TIMESTAMP_GROK_NAME,
+                "%{MONTHDAY}\\.%{MONTHNUM2}\\. %{YEAR} %{HOUR}:%{MINUTE}:%{SECOND}"));
+    }
+
+    private void validateCustomOverrideNotMatchingBuiltInFormat(String overrideFormat, String text, String expectedSimpleRegex,
+                                                                String expectedGrokPatternName,
+                                                                Map<String, String> expectedCustomGrokPatternDefinitions) {
         TimestampFormatFinder strictTimestampFormatFinder = new TimestampFormatFinder(explanation, overrideFormat, true, true, true,
             NOOP_TIMEOUT_CHECKER);
         strictTimestampFormatFinder.addSample(text);
