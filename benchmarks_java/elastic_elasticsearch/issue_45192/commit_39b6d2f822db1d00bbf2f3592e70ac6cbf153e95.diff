diff --git a/x-pack/plugin/ccr/src/test/java/org/elasticsearch/xpack/ccr/CcrRetentionLeaseIT.java b/x-pack/plugin/ccr/src/test/java/org/elasticsearch/xpack/ccr/CcrRetentionLeaseIT.java
index 0dca0ffea2259..fe6400b513b4f 100644
--- a/x-pack/plugin/ccr/src/test/java/org/elasticsearch/xpack/ccr/CcrRetentionLeaseIT.java
+++ b/x-pack/plugin/ccr/src/test/java/org/elasticsearch/xpack/ccr/CcrRetentionLeaseIT.java
@@ -43,6 +43,7 @@
 import org.elasticsearch.plugins.Plugin;
 import org.elasticsearch.snapshots.RestoreInfo;
 import org.elasticsearch.snapshots.RestoreService;
+import org.elasticsearch.test.junit.annotations.TestLogging;
 import org.elasticsearch.test.transport.MockTransportService;
 import org.elasticsearch.transport.ConnectTransportException;
 import org.elasticsearch.transport.RemoteTransportException;
@@ -86,6 +87,9 @@
 import static org.hamcrest.Matchers.greaterThan;
 import static org.hamcrest.Matchers.hasSize;
 
+@TestLogging(
+    // issue: https://github.com/elastic/elasticsearch/issues/45192
+    value = "org.elasticsearch.xpack.ccr:trace,org.elasticsearch.indices.recovery:trace,org.elasticsearch.index.seqno:debug")
 public class CcrRetentionLeaseIT extends CcrIntegTestCase {
 
     public static final class RetentionLeaseRenewIntervalSettingPlugin extends Plugin {
@@ -780,6 +784,9 @@ public void testRetentionLeaseIsAddedIfItDisappearsWhileFollowing() throws Excep
                             || TransportActionProxy.getProxyAction(RetentionLeaseActions.Renew.ACTION_NAME).equals(action)) {
                             senderTransportService.clearAllRules();
                             final RetentionLeaseActions.RenewRequest renewRequest = (RetentionLeaseActions.RenewRequest) request;
+                            final String retentionLeaseId = getRetentionLeaseId(followerIndex, leaderIndex);
+                            assertThat(retentionLeaseId, equalTo(renewRequest.getId()));
+                            logger.info("--> intercepting renewal request for retention lease [{}]", retentionLeaseId);
                             final String primaryShardNodeId =
                                 getLeaderCluster()
                                     .clusterService()
@@ -797,17 +804,17 @@ public void testRetentionLeaseIsAddedIfItDisappearsWhileFollowing() throws Excep
                                     .getShardOrNull(renewRequest.getShardId());
                             final CountDownLatch innerLatch = new CountDownLatch(1);
                             // this forces the background renewal from following to face a retention lease not found exception
-                            primary.removeRetentionLease(
-                                getRetentionLeaseId(followerIndex, leaderIndex),
+                            logger.info("--> removing retention lease [{}] on the leader", retentionLeaseId);
+                            primary.removeRetentionLease(retentionLeaseId,
                                 ActionListener.wrap(r -> innerLatch.countDown(), e -> fail(e.toString())));
-
+                            logger.info("--> waiting for the removed retention lease [{}] to be synced on the leader", retentionLeaseId);
                             try {
                                 innerLatch.await();
                             } catch (final InterruptedException e) {
                                 Thread.currentThread().interrupt();
                                 fail(e.toString());
                             }
-
+                            logger.info("--> removed retention lease [{}] on the leader", retentionLeaseId);
                             latch.countDown();
                         }
                         connection.sendRequest(requestId, action, request, options);
@@ -878,6 +885,7 @@ public void testPeriodicRenewalDoesNotAddRetentionLeaseAfterUnfollow() throws Ex
                             if (RetentionLeaseActions.Renew.ACTION_NAME.equals(action)
                                     || TransportActionProxy.getProxyAction(RetentionLeaseActions.Renew.ACTION_NAME).equals(action)) {
                                 final String retentionLeaseId = getRetentionLeaseId(followerIndex, leaderIndex);
+                                logger.info("--> blocking renewal request for retention lease [{}] until unfollowed", retentionLeaseId);
                                 try {
                                     removeLeaseLatch.countDown();
                                     unfollowLatch.await();
