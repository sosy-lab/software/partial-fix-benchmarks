diff --git a/docs/reference/mapping/types/dense-vector.asciidoc b/docs/reference/mapping/types/dense-vector.asciidoc
index 98834d0123465..68c73b2cd7e3e 100644
--- a/docs/reference/mapping/types/dense-vector.asciidoc
+++ b/docs/reference/mapping/types/dense-vector.asciidoc
@@ -53,4 +53,5 @@ PUT my_index/_doc/2
 
 Internally, each document's dense vector is encoded as a binary
 doc value. Its size in bytes is equal to
-`4 * dims + 4`, where `dims`—the number of the vector's dimensions.
\ No newline at end of file
+`2 * dims + 4`, where `dims` — the number of the vector's dimensions.
+Each dimension value is represented as bfloat16 value.
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/10_dense_vector_basic.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/10_dense_vector_basic.yml
index 903b9dc3de3b0..e5ee2cbd8ee8a 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/10_dense_vector_basic.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/10_dense_vector_basic.yml
@@ -58,17 +58,20 @@ setup:
 
   - match: {hits.total: 3}
 
+  # result should be 65425.624, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.0._id: "1"}
-  - gte: {hits.hits.0._score: 65425.62}
-  - lte: {hits.hits.0._score: 65425.63}
+  - gte: {hits.hits.0._score: 62154.34}
+  - lte: {hits.hits.0._score: 68696.91}
 
+  # result should be 37111.98, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.1._id: "3"}
-  - gte: {hits.hits.1._score: 37111.98}
-  - lte: {hits.hits.1._score: 37111.99}
+  - gte: {hits.hits.1._score: 35256.38}
+  - lte: {hits.hits.1._score: 38967.58}
 
+  # result should be 35853.78, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.2._id: "2"}
-  - gte: {hits.hits.2._score: 35853.78}
-  - lte: {hits.hits.2._score: 35853.79}
+  - gte: {hits.hits.2._score: 34061.09}
+  - lte: {hits.hits.2._score: 37646.47}
 
 ---
 "Cosine Similarity":
diff --git a/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/15_dense_vector_l1l2.yml b/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/15_dense_vector_l1l2.yml
index dbb274d077645..316d9384648e4 100644
--- a/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/15_dense_vector_l1l2.yml
+++ b/x-pack/plugin/src/test/resources/rest-api-spec/test/vectors/15_dense_vector_l1l2.yml
@@ -59,17 +59,20 @@ setup:
 
   - match: {hits.total: 3}
 
+  # result should be 485.18, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.0._id: "1"}
-  - gte: {hits.hits.0._score: 485.18}
-  - lte: {hits.hits.0._score: 485.19}
+  - gte: {hits.hits.0._score: 460.92}
+  - lte: {hits.hits.0._score: 509.44}
 
+  # result should be 12.3, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.1._id: "2"}
-  - gte: {hits.hits.1._score: 12.29}
-  - lte: {hits.hits.1._score: 12.31}
+  - gte: {hits.hits.1._score: 11.68}
+  - lte: {hits.hits.1._score: 12.91}
 
+  # result here should be 0, but we allow it be up to 0.5 because vectors are indexed as bfloat16
   - match: {hits.hits.2._id: "3"}
   - gte: {hits.hits.2._score: 0.00}
-  - lte: {hits.hits.2._score: 0.01}
+  - lte: {hits.hits.2._score: 0.5}
 
 ---
 "L2 norm":
@@ -89,14 +92,17 @@ setup:
 
   - match: {hits.total: 3}
 
+  # result should be 301.36, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.0._id: "1"}
-  - gte: {hits.hits.0._score: 301.36}
-  - lte: {hits.hits.0._score: 301.37}
+  - gte: {hits.hits.0._score: 286.29}
+  - lte: {hits.hits.0._score: 316.43}
 
+  # result should be 11.34, but we allow 1% error because vectors are indexed as bfloat16
   - match: {hits.hits.1._id: "2"}
-  - gte: {hits.hits.1._score: 11.34}
-  - lte: {hits.hits.1._score: 11.35}
+  - gte: {hits.hits.1._score: 10.77}
+  - lte: {hits.hits.1._score: 11.90}
 
+  # result here should be 0, but we allow it be up to 0.5 because vectors are indexed as bfloat16
   - match: {hits.hits.2._id: "3"}
   - gte: {hits.hits.2._score: 0.00}
-  - lte: {hits.hits.2._score: 0.01}
+  - lte: {hits.hits.2._score: 0.5}
diff --git a/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapper.java b/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapper.java
index b1518d3ecd586..37f3f327aa2bd 100644
--- a/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapper.java
+++ b/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapper.java
@@ -30,7 +30,6 @@
 import org.elasticsearch.xpack.vectors.query.VectorDVIndexFieldData;
 
 import java.io.IOException;
-import java.nio.ByteBuffer;
 import java.time.ZoneId;
 import java.util.List;
 import java.util.Map;
@@ -45,6 +44,7 @@
     public static final String CONTENT_TYPE = "dense_vector";
     public static short MAX_DIMS_COUNT = 1024; //maximum allowed number of dimensions
     private static final byte INT_BYTES = 4;
+    private static final byte SHORT_BYTES = 2;
 
     public static class Defaults {
         public static final MappedFieldType FIELD_TYPE = new DenseVectorFieldType();
@@ -182,11 +182,10 @@ public void parse(ParseContext context) throws IOException {
 
         // encode array of floats as array of integers and store into buf
         // this code is here and not int the VectorEncoderDecoder so not to create extra arrays
-        byte[] bytes = indexCreatedVersion.onOrAfter(Version.V_7_5_0) ? new byte[dims * INT_BYTES + INT_BYTES] : new byte[dims * INT_BYTES];
+        byte[] bytes = indexCreatedVersion.onOrAfter(Version.V_7_5_0) ? new byte[dims * SHORT_BYTES + INT_BYTES] : new byte[dims * INT_BYTES];
 
-        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
         double dotProduct = 0f;
-
+        int offset = 0;
         int dim = 0;
         for (Token token = context.parser().nextToken(); token != Token.END_ARRAY; token = context.parser().nextToken()) {
             if (dim++ >= dims) {
@@ -195,9 +194,14 @@ public void parse(ParseContext context) throws IOException {
             }
             ensureExpectedToken(Token.VALUE_NUMBER, token, context.parser()::getTokenLocation);
             float value = context.parser().floatValue(true);
-
-            byteBuffer.putFloat(value);
             dotProduct += value * value;
+            int intValue = Float.floatToIntBits(value);
+            bytes[offset++] = (byte) (intValue >> 24);
+            bytes[offset++] = (byte) (intValue >> 16);
+            if (indexCreatedVersion.onOrAfter(Version.V_7_5_0) == false) {
+                bytes[offset++] = (byte) (intValue >> 8);
+                bytes[offset++] = (byte) intValue;
+            }
         }
         if (dim != dims) {
             throw new IllegalArgumentException("Field [" + name() + "] of type [" + typeName() + "] of doc [" +
@@ -208,7 +212,11 @@ public void parse(ParseContext context) throws IOException {
         if (indexCreatedVersion.onOrAfter(Version.V_7_5_0)) {
             // encode vector magnitude at the end
             float vectorMagnitude = (float) Math.sqrt(dotProduct);
-            byteBuffer.putFloat(vectorMagnitude);
+            int intValue = Float.floatToIntBits(vectorMagnitude);
+            bytes[offset++] = (byte) (intValue >> 24);
+            bytes[offset++] = (byte) (intValue >> 16);
+            bytes[offset++] = (byte) (intValue >> 8);
+            bytes[offset++] = (byte) intValue;
         }
         BinaryDocValuesField field = new BinaryDocValuesField(fieldType().name(), new BytesRef(bytes));
         if (context.doc().getByKey(fieldType().name()) != null) {
diff --git a/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoder.java b/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoder.java
index 2d591aaccd48f..8e7f6998947cd 100644
--- a/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoder.java
+++ b/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoder.java
@@ -158,7 +158,7 @@ public void swap(int i, int j) {
 
     public static int denseVectorLength(Version indexVersion, BytesRef vectorBR) {
         return indexVersion.onOrAfter(Version.V_7_5_0)
-            ? (vectorBR.length - INT_BYTES) / INT_BYTES
+            ? (vectorBR.length - INT_BYTES) / SHORT_BYTES
             : vectorBR.length / INT_BYTES;
     }
 
diff --git a/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtils.java b/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtils.java
index f286ab7328556..3bf96a59374ff 100644
--- a/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtils.java
+++ b/x-pack/plugin/vectors/src/main/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtils.java
@@ -84,11 +84,19 @@ public L1Norm(ScoreScript scoreScript, List<Number> queryVector) {
         public double l1norm(VectorScriptDocValues.DenseVectorScriptDocValues dvs) {
             BytesRef vector = dvs.getEncodedValue();
             validateDocVector(vector);
-            ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
-            double l1norm = 0;
 
-            for (float queryValue : queryVector) {
-                l1norm += Math.abs(queryValue - byteBuffer.getFloat());
+            double l1norm = 0;
+            if (scoreScript._getIndexVersion().onOrAfter(Version.V_7_5_0)) {
+                int offset = vector.offset;
+                for (float queryValue : queryVector) {
+                    int intValue = ((vector.bytes[offset++] & 0xFF) << 24) | ((vector.bytes[offset++] & 0xFF) << 16);
+                    l1norm += Math.abs(queryValue - Float.intBitsToFloat(intValue));
+                }
+            } else {
+                ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
+                for (float queryValue : queryVector) {
+                    l1norm += Math.abs(queryValue - byteBuffer.getFloat());
+                }
             }
             return l1norm;
         }
@@ -104,12 +112,21 @@ public L2Norm(ScoreScript scoreScript, List<Number> queryVector) {
         public double l2norm(VectorScriptDocValues.DenseVectorScriptDocValues dvs) {
             BytesRef vector = dvs.getEncodedValue();
             validateDocVector(vector);
-            ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
 
             double l2norm = 0;
-            for (float queryValue : queryVector) {
-                double diff = queryValue - byteBuffer.getFloat();
-                l2norm += diff * diff;
+            if (scoreScript._getIndexVersion().onOrAfter(Version.V_7_5_0)) {
+                int offset = vector.offset;
+                for (float queryValue : queryVector) {
+                    int intValue = ((vector.bytes[offset++] & 0xFF) << 24) | ((vector.bytes[offset++] & 0xFF) << 16);
+                    double diff = queryValue - Float.intBitsToFloat(intValue);
+                    l2norm += diff * diff;
+                }
+            } else {
+                ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
+                for (float queryValue : queryVector) {
+                    double diff = queryValue - byteBuffer.getFloat();
+                    l2norm += diff * diff;
+                }
             }
             return Math.sqrt(l2norm);
         }
@@ -125,11 +142,19 @@ public DotProduct(ScoreScript scoreScript, List<Number> queryVector) {
         public double dotProduct(VectorScriptDocValues.DenseVectorScriptDocValues dvs){
             BytesRef vector = dvs.getEncodedValue();
             validateDocVector(vector);
-            ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
 
             double dotProduct = 0;
-            for (float queryValue : queryVector) {
-                dotProduct += queryValue * byteBuffer.getFloat();
+            if (scoreScript._getIndexVersion().onOrAfter(Version.V_7_5_0)) {
+                int offset = vector.offset;
+                for (float queryValue : queryVector) {
+                    int intValue = ((vector.bytes[offset++] & 0xFF) << 24) | ((vector.bytes[offset++] & 0xFF) << 16);
+                    dotProduct += queryValue * Float.intBitsToFloat(intValue);
+                }
+            } else {
+                ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
+                for (float queryValue : queryVector) {
+                    dotProduct += queryValue * byteBuffer.getFloat();
+                }
             }
             return dotProduct;
         }
@@ -146,16 +171,17 @@ public double cosineSimilarity(VectorScriptDocValues.DenseVectorScriptDocValues
             BytesRef vector = dvs.getEncodedValue();
             validateDocVector(vector);
 
-            ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
-
             double dotProduct = 0.0;
             double vectorMagnitude = 0.0f;
             if (scoreScript._getIndexVersion().onOrAfter(Version.V_7_5_0)) {
+                int offset = vector.offset;
                 for (float queryValue : queryVector) {
-                    dotProduct += queryValue * byteBuffer.getFloat();
+                    int intValue = ((vector.bytes[offset++] & 0xFF) << 24) | ((vector.bytes[offset++] & 0xFF) << 16);
+                    dotProduct += queryValue * Float.intBitsToFloat(intValue);
                 }
                 vectorMagnitude = VectorEncoderDecoder.decodeVectorMagnitude(scoreScript._getIndexVersion(), vector);
             } else {
+                ByteBuffer byteBuffer = ByteBuffer.wrap(vector.bytes, vector.offset, vector.length);
                 for (float queryValue : queryVector) {
                     float docValue = byteBuffer.getFloat();
                     dotProduct += queryValue * docValue;
diff --git a/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapperTests.java b/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapperTests.java
index 52ef487935b68..53ab20bb7ddc8 100644
--- a/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapperTests.java
+++ b/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/DenseVectorFieldMapperTests.java
@@ -98,13 +98,11 @@ public void testDefaults() throws Exception {
         BytesRef vectorBR = fields[0].binaryValue();
         float[] decodedValues = decodeDenseVector(indexVersion, vectorBR);
         float decodedMagnitude = VectorEncoderDecoder.decodeVectorMagnitude(indexVersion, vectorBR);
-        assertEquals(expectedMagnitude, decodedMagnitude, 0.001f);
-        assertArrayEquals(
-            "Decoded dense vector values is not equal to the indexed one.",
-            validVector,
-            decodedValues,
-            0.001f
-        );
+        float errorThreshold = expectedMagnitude / 100; //allow relative 1% error
+        assertEquals(expectedMagnitude, decodedMagnitude, errorThreshold);
+        for (int dim = 0; dim < validVector.length; dim++) {
+            assertEquals(validVector[dim], decodedValues[dim], Math.abs(validVector[dim]/100)); //allow relative 1% error
+        }
     }
 
     public void testAddDocumentsToIndexBefore_V_7_5_0() throws Exception {
@@ -147,9 +145,18 @@ public void testAddDocumentsToIndexBefore_V_7_5_0() throws Exception {
         int dimCount = VectorEncoderDecoder.denseVectorLength(indexVersion, encodedVector);
         float[] vector = new float[dimCount];
 
-        ByteBuffer byteBuffer = ByteBuffer.wrap(encodedVector.bytes, encodedVector.offset, encodedVector.length);
-        for (int dim = 0; dim < dimCount; dim++) {
-            vector[dim] = byteBuffer.getFloat();
+        if (indexVersion.onOrAfter(Version.V_7_5_0)) {
+            int offset = encodedVector.offset;;
+            for (int dim = 0; dim < dimCount; dim++) {
+                int intValue = ((encodedVector.bytes[offset++] & 0xFF) << 24) | ((encodedVector.bytes[offset++] & 0xFF) << 16);
+                vector[dim] = Float.intBitsToFloat(intValue);
+            }
+            return vector;
+        } else {
+            ByteBuffer byteBuffer = ByteBuffer.wrap(encodedVector.bytes, encodedVector.offset, encodedVector.length);
+            for (int dim = 0; dim < dimCount; dim++) {
+                vector[dim] = byteBuffer.getFloat();
+            }
         }
         return vector;
     }
diff --git a/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoderTests.java b/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoderTests.java
index c81bdfe147ebd..eea11fc5b7465 100644
--- a/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoderTests.java
+++ b/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/mapper/VectorEncoderDecoderTests.java
@@ -96,21 +96,31 @@ public void testSparseVectorEncodingDecodingBefore_V_7_5_0() {
 
     // imitates the code in DenseVectorFieldMapper::parse
     public static BytesRef mockEncodeDenseVector(float[] values, Version indexVersion) {
-        byte[] bytes = indexVersion.onOrAfter(Version.V_7_5_0)
-            ? new byte[VectorEncoderDecoder.INT_BYTES * values.length + VectorEncoderDecoder.INT_BYTES]
-            : new byte[VectorEncoderDecoder.INT_BYTES * values.length];
-        double dotProduct = 0f;
-
-        ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
-        for (float value : values) {
-            byteBuffer.putFloat(value);
-            dotProduct += value * value;
-        }
+        byte[] bytes;
 
         if (indexVersion.onOrAfter(Version.V_7_5_0)) {
+            bytes = new byte[VectorEncoderDecoder.SHORT_BYTES * values.length + VectorEncoderDecoder.INT_BYTES];
+            double dotProduct = 0f;
+            int offset = 0;
+            for (float value : values) {
+                dotProduct += value * value;
+                int intValue = Float.floatToIntBits(value);
+                bytes[offset++] = (byte) (intValue >> 24);
+                bytes[offset++] = (byte) (intValue >> 16);
+            }
             // encode vector magnitude at the end
             float vectorMagnitude = (float) Math.sqrt(dotProduct);
-            byteBuffer.putFloat(vectorMagnitude);
+            int intValue = Float.floatToIntBits(vectorMagnitude);
+            bytes[offset++] = (byte) (intValue >> 24);
+            bytes[offset++] = (byte) (intValue >> 16);
+            bytes[offset++] = (byte) (intValue >> 8);
+            bytes[offset++] = (byte) intValue;
+        } else {
+            bytes = new byte[VectorEncoderDecoder.INT_BYTES * values.length];
+            ByteBuffer byteBuffer = ByteBuffer.wrap(bytes);
+            for (float value : values) {
+                byteBuffer.putFloat(value);
+            }
         }
         return new BytesRef(bytes);
     }
diff --git a/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtilsTests.java b/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtilsTests.java
index 343db845e6e3b..1c8a99ebf6223 100644
--- a/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtilsTests.java
+++ b/x-pack/plugin/vectors/src/test/java/org/elasticsearch/xpack/vectors/query/ScoreScriptUtilsTests.java
@@ -50,23 +50,31 @@ private void testDenseVectorFunctions(Version indexVersion) {
 
         // test dotProduct
         DotProduct dotProduct = new DotProduct(scoreScript, queryVector);
+        double expected = 65425.624;
+        float delta = indexVersion.onOrAfter(Version.V_7_5_0) ? (float) expected/100 : 0.001f; // allow 1% error rate
         double result = dotProduct.dotProduct(dvs);
-        assertEquals("dotProduct result is not equal to the expected value!", 65425.624, result, 0.001);
+        assertEquals("dotProduct result is not equal to the expected value!", expected, result, delta);
 
         // test cosineSimilarity
         CosineSimilarity cosineSimilarity = new CosineSimilarity(scoreScript, queryVector);
+        double expected2 = 0.790;
+        float delta2 = indexVersion.onOrAfter(Version.V_7_5_0) ? (float) expected2/100 : 0.001f;
         double result2 = cosineSimilarity.cosineSimilarity(dvs);
-        assertEquals("cosineSimilarity result is not equal to the expected value!", 0.790, result2, 0.001);
+        assertEquals("cosineSimilarity result is not equal to the expected value!", expected2, result2, delta2);
 
         // test l1Norm
         L1Norm l1norm = new L1Norm(scoreScript, queryVector);
+        double expected3 = 485.184;
+        float delta3 = indexVersion.onOrAfter(Version.V_7_5_0) ? (float) expected3/100 : 0.001f;
         double result3 = l1norm.l1norm(dvs);
-        assertEquals("l1norm result is not equal to the expected value!", 485.184, result3, 0.001);
+        assertEquals("l1norm result is not equal to the expected value!", expected3, result3, delta3);
 
         // test l2norm
         L2Norm l2norm = new L2Norm(scoreScript, queryVector);
+        double expected4 = 301.36;
+        float delta4 = indexVersion.onOrAfter(Version.V_7_5_0) ? (float) expected4/100 : 0.001f;
         double result4 = l2norm.l2norm(dvs);
-        assertEquals("l2norm result is not equal to the expected value!", 301.361, result4, 0.001);
+        assertEquals("l2norm result is not equal to the expected value!", 301.361, result4, delta4);
 
         // test dotProduct fails when queryVector has wrong number of dims
         List<Number> invalidQueryVector = Arrays.asList(0.5, 111.3);
