diff --git a/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/FingerprintProcessor.java b/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/FingerprintProcessor.java
new file mode 100644
index 0000000000000..3e4159ba192f8
--- /dev/null
+++ b/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/FingerprintProcessor.java
@@ -0,0 +1,220 @@
+/*
+ * Licensed to Elasticsearch under one or more contributor
+ * license agreements. See the NOTICE file distributed with
+ * this work for additional information regarding copyright
+ * ownership. Elasticsearch licenses this file to you under
+ * the Apache License, Version 2.0 (the "License"); you may
+ * not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *    http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing,
+ * software distributed under the License is distributed on an
+ * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+ * KIND, either express or implied.  See the License for the
+ * specific language governing permissions and limitations
+ * under the License.
+ */
+
+package org.elasticsearch.ingest.common;
+
+import static org.elasticsearch.ingest.ConfigurationUtils.newConfigurationException;
+import static org.elasticsearch.ingest.ConfigurationUtils.readBooleanProperty;
+import static org.elasticsearch.ingest.ConfigurationUtils.readOptionalList;
+import static org.elasticsearch.ingest.ConfigurationUtils.readStringProperty;
+
+import java.nio.charset.StandardCharsets;
+import java.util.Arrays;
+import java.util.Base64;
+import java.util.HashMap;
+import java.util.HashSet;
+import java.util.List;
+import java.util.Locale;
+import java.util.Map;
+import java.util.Set;
+
+import org.elasticsearch.common.Numbers;
+import org.elasticsearch.common.UUIDs;
+import org.elasticsearch.common.hash.MessageDigests;
+import org.elasticsearch.common.hash.MurmurHash3;
+import org.elasticsearch.ingest.AbstractProcessor;
+import org.elasticsearch.ingest.IngestDocument;
+import org.elasticsearch.ingest.Processor;
+
+/**
+ * Processor that generates fingerprint from field names and values and write it
+ * to target field. If the target field is already present, its value will be
+ * replaced with the fingerprint.
+ */
+public final class FingerprintProcessor extends AbstractProcessor {
+
+    public static final String TYPE = "fingerprint";
+
+    private final Set<String> fields;
+    private final String targetField;
+    private final Method method;
+    private final boolean base64Encode;
+    private final boolean concatenateAllFields;
+    private final boolean ignoreMissing;
+
+    FingerprintProcessor(String tag, Set<String> fields, String targetField, Method method, boolean base64Encode,
+            boolean concatenateAllFields, boolean ignoreMissing) {
+        super(tag);
+        this.fields = fields;
+        this.targetField = targetField;
+        this.method = method;
+        this.base64Encode = base64Encode;
+        this.concatenateAllFields = concatenateAllFields;
+        this.ignoreMissing = ignoreMissing;
+    }
+
+    boolean isBase64Encode() {
+        return base64Encode;
+    }
+
+    boolean isConcatenateAllFields() {
+        return concatenateAllFields;
+    }
+
+    boolean isIgnoreMissing() {
+        return ignoreMissing;
+    }
+
+    @Override
+    public IngestDocument execute(IngestDocument ingestDocument) {
+        if (method.equals(Method.UUID)) {
+            // Generating UUID doesn't rely on document
+            ingestDocument.setFieldValue(targetField, UUIDs.base64UUID());
+            return ingestDocument;
+        }
+        // Get content from document that is used to generate fingerprint.
+        byte[] content;
+        if (concatenateAllFields) {
+            content = ingestDocument.getSourceAndMetadata().toString().getBytes(StandardCharsets.UTF_8);
+        } else {
+            Map<String, Object> m = new HashMap<>();
+            for (String field : fields) {
+                Object value = ingestDocument.getFieldValue(field, Object.class, ignoreMissing);
+                if (value == null) {
+                    if (!ignoreMissing) {
+                        throw new IllegalArgumentException(
+                                "field [" + field + "] is null, cannot generate fingerprint from it.");
+                    }
+                } else {
+                    m.put(field, value);
+                }
+            }
+            // All fields are missing, just ignore this document.
+            if (m.isEmpty()) {
+                return ingestDocument;
+            }
+            content = m.toString().getBytes(StandardCharsets.UTF_8);
+        }
+
+        // Generate fingerprint from content using specified hash function.
+        byte[] digest;
+        switch (method) {
+        case MD5:
+            digest = MessageDigests.md5().digest(content);
+            break;
+        case SHA256:
+            digest = MessageDigests.sha256().digest(content);
+            break;
+        case SHA1:
+            digest = MessageDigests.sha1().digest(content);
+            break;
+        case MURMUR3:
+            MurmurHash3.Hash128 h = MurmurHash3.hash128(content, 0, content.length, 17, new MurmurHash3.Hash128());
+            digest = new byte[16];
+            System.arraycopy(Numbers.longToBytes(h.h1), 0, digest, 0, 8);
+            System.arraycopy(Numbers.longToBytes(h.h2), 0, digest, 8, 8);
+            break;
+        default:
+            throw new IllegalArgumentException("Invalid method: " + method.toString());
+        }
+        String targetValue;
+        if (base64Encode) {
+            targetValue = Base64.getEncoder().encodeToString(digest);
+        } else {
+            targetValue = MessageDigests.toHexString(digest);
+        }
+        // Set generated fingerprint to target field
+        ingestDocument.setFieldValue(targetField, targetValue);
+
+        return ingestDocument;
+    }
+
+    @Override
+    public String getType() {
+        return TYPE;
+    }
+
+    Set<String> getFields() {
+        return fields;
+    }
+
+    String getTargetField() {
+        return targetField;
+    }
+
+    Method getMethod() {
+        return method;
+    }
+
+    public static final class Factory implements Processor.Factory {
+        @Override
+        public FingerprintProcessor create(Map<String, Processor.Factory> registry, String processorTag,
+                Map<String, Object> config) {
+            List<String> fields = readOptionalList(TYPE, processorTag, config, "fields");
+            String targetField = readStringProperty(TYPE, processorTag, config, "target_field", "fingerprint");
+            String method = readStringProperty(TYPE, processorTag, config, "method", "MD5");
+            boolean base64encode = readBooleanProperty(TYPE, processorTag, config, "base64_encode", false);
+            boolean concatenateAllFields = readBooleanProperty(TYPE, processorTag, config, "concatenate_all_fields",
+                    false);
+            boolean ignoreMissing = readBooleanProperty(TYPE, processorTag, config, "ignore_missing", false);
+
+            Method m;
+            try {
+                m = Method.parse(method);
+            } catch (Exception e) {
+                throw newConfigurationException(TYPE, processorTag, "method",
+                        "illegal value [" + method + "]. Valid values are " + Arrays.toString(Method.values()));
+            }
+
+            if (m.equals(Method.UUID)) {
+                if (fields != null || concatenateAllFields) {
+                    throw newConfigurationException(TYPE, processorTag, "method",
+                            "is [" + method + "], [fields] or [concatenate_all_fields] must not be set");
+                }
+            } else {
+                if (fields == null && !concatenateAllFields) {
+                    throw newConfigurationException(TYPE, processorTag, "method",
+                            "is [" + method + "], one of [fields] and [concatenate_all_fields] must be set");
+                }
+                if (fields != null && concatenateAllFields) {
+                    throw newConfigurationException(TYPE, processorTag, "fields",
+                            "can not be set when [concatenate_all_fields] is true");
+                }
+                if (fields != null && fields.isEmpty()) {
+                    throw newConfigurationException(TYPE, processorTag, "fields", "can not be empty");
+                }
+            }
+
+            Set<String> fieldsSet = new HashSet<>();
+            if (fields != null) {
+                fieldsSet.addAll(fields);
+            }
+            return new FingerprintProcessor(processorTag, fieldsSet, targetField, m, base64encode, concatenateAllFields,
+                    ignoreMissing);
+        }
+    }
+
+    enum Method {
+        SHA1, SHA256, MD5, MURMUR3, UUID;
+
+        public static Method parse(String value) {
+            return valueOf(value.toUpperCase(Locale.ROOT));
+        }
+    }
+}
diff --git a/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/IngestCommonPlugin.java b/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/IngestCommonPlugin.java
index d4293c50e14a0..625790fbfcde2 100644
--- a/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/IngestCommonPlugin.java
+++ b/modules/ingest-common/src/main/java/org/elasticsearch/ingest/common/IngestCommonPlugin.java
@@ -89,7 +89,9 @@ public IngestCommonPlugin() {
                 entry(DissectProcessor.TYPE, new DissectProcessor.Factory()),
                 entry(DropProcessor.TYPE, new DropProcessor.Factory()),
                 entry(HtmlStripProcessor.TYPE, new HtmlStripProcessor.Factory()),
-                entry(CsvProcessor.TYPE, new CsvProcessor.Factory()));
+                entry(CsvProcessor.TYPE, new CsvProcessor.Factory()),
+                entry(FingerprintProcessor.TYPE, new FingerprintProcessor.Factory()));
+
     }
 
     @Override
diff --git a/modules/ingest-common/src/test/java/org/elasticsearch/ingest/common/FingerprintProcessorFactoryTests.java b/modules/ingest-common/src/test/java/org/elasticsearch/ingest/common/FingerprintProcessorFactoryTests.java
new file mode 100644
index 0000000000000..c92230e33cf21
--- /dev/null
+++ b/modules/ingest-common/src/test/java/org/elasticsearch/ingest/common/FingerprintProcessorFactoryTests.java
@@ -0,0 +1,258 @@
+/*
+ * Licensed to Elasticsearch under one or more contributor
+ * license agreements. See the NOTICE file distributed with
+ * this work for additional information regarding copyright
+ * ownership. Elasticsearch licenses this file to you under
+ * the Apache License, Version 2.0 (the "License"); you may
+ * not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *    http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing,
+ * software distributed under the License is distributed on an
+ * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+ * KIND, either express or implied.  See the License for the
+ * specific language governing permissions and limitations
+ * under the License.
+ */
+
+package org.elasticsearch.ingest.common;
+
+import static org.hamcrest.Matchers.equalTo;
+
+import java.util.ArrayList;
+import java.util.Collections;
+import java.util.HashMap;
+import java.util.HashSet;
+import java.util.List;
+import java.util.Map;
+import java.util.Set;
+
+import org.elasticsearch.ElasticsearchParseException;
+import org.elasticsearch.test.ESTestCase;
+
+public class FingerprintProcessorFactoryTests extends ESTestCase {
+
+    private FingerprintProcessor.Factory factory = new FingerprintProcessor.Factory();
+
+    public void testBuildDefaults() {
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        Set<String> expectFeilds = new HashSet<>();
+        expectFeilds.add("_field");
+        assertEquals(expectFeilds, processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.MD5, processor.getMethod());
+        assertFalse(processor.isBase64Encode());
+        assertFalse(processor.isConcatenateAllFields());
+        assertFalse(processor.isIgnoreMissing());
+    }
+
+    public void testValidMethod() {
+
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+        config.put("method", "SHA256");
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        Set<String> expectFeilds = new HashSet<>();
+        expectFeilds.add("_field");
+        assertEquals(expectFeilds, processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.SHA256, processor.getMethod());
+        assertFalse(processor.isBase64Encode());
+        assertFalse(processor.isConcatenateAllFields());
+        assertFalse(processor.isIgnoreMissing());
+
+    }
+
+    public void testInvalidMethod() {
+
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+        config.put("method", "Invalid method");
+
+        String processorTag = randomAlphaOfLength(10);
+        Exception exception = expectThrows(ElasticsearchParseException.class,
+                () -> factory.create(null, processorTag, config));
+        assertThat(exception.getMessage(), equalTo(
+                "[method] illegal value [Invalid method]. Valid values are [SHA1, SHA256, MD5, MURMUR3, UUID]"));
+
+    }
+
+    public void testUUID() {
+        Map<String, Object> config = new HashMap<>();
+        config.put("method", "UUID");
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        assertEquals(Collections.emptySet(), processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.UUID, processor.getMethod());
+        assertFalse(processor.isBase64Encode());
+        assertFalse(processor.isConcatenateAllFields());
+        assertFalse(processor.isIgnoreMissing());
+    }
+
+    public void testUUIDWithFields() {
+        Map<String, Object> config = new HashMap<>();
+        config.put("method", "UUID");
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+
+        String processorTag = randomAlphaOfLength(10);
+        Exception exception = expectThrows(ElasticsearchParseException.class,
+                () -> factory.create(null, processorTag, config));
+        assertThat(exception.getMessage(),
+                equalTo("[method] is [UUID], [fields] or [concatenate_all_fields] must not be set"));
+    }
+
+    public void testUUIDWithConcatenateAllFields() {
+        Map<String, Object> config = new HashMap<>();
+        config.put("method", "UUID");
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+
+        String processorTag = randomAlphaOfLength(10);
+        Exception exception = expectThrows(ElasticsearchParseException.class,
+                () -> factory.create(null, processorTag, config));
+        assertThat(exception.getMessage(),
+                equalTo("[method] is [UUID], [fields] or [concatenate_all_fields] must not be set"));
+    }
+
+    public void testMD5WithMultiFields() {
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("field1");
+        fieldList.add("field2");
+        fieldList.add("field3");
+        fieldList.add("field4");
+        config.put("fields", fieldList);
+        config.put("method", "MD5");
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        Set<String> expectFeilds = new HashSet<>();
+        expectFeilds.add("field1");
+        expectFeilds.add("field2");
+        expectFeilds.add("field3");
+        expectFeilds.add("field4");
+        assertEquals(expectFeilds, processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.MD5, processor.getMethod());
+        assertFalse(processor.isBase64Encode());
+        assertFalse(processor.isConcatenateAllFields());
+        assertFalse(processor.isIgnoreMissing());
+    }
+
+    public void testMD5WithConcatenate() {
+        Map<String, Object> config = new HashMap<>();
+        config.put("method", "MD5");
+        config.put("concatenate_all_fields", true);
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        assertEquals(Collections.emptySet(), processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.MD5, processor.getMethod());
+        assertFalse(processor.isBase64Encode());
+        assertTrue(processor.isConcatenateAllFields());
+        assertFalse(processor.isIgnoreMissing());
+    }
+
+    public void testMD5WithoutFieldsAndConcatenate() {
+        Map<String, Object> config = new HashMap<>();
+        String processorTag = randomAlphaOfLength(10);
+        Exception exception = expectThrows(ElasticsearchParseException.class,
+                () -> factory.create(null, processorTag, config));
+        assertThat(exception.getMessage(),
+                equalTo("[method] is [MD5], one of [fields] and [concatenate_all_fields] must be set"));
+    }
+
+    public void testMD5WithFieldsAndConcatenate() {
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+        config.put("method", "MD5");
+        config.put("concatenate_all_fields", true);
+        String processorTag = randomAlphaOfLength(10);
+        Exception exception = expectThrows(ElasticsearchParseException.class,
+                () -> factory.create(null, processorTag, config));
+        assertThat(exception.getMessage(), equalTo("[fields] can not be set when [concatenate_all_fields] is true"));
+    }
+
+    public void testMD5WithEmptyFields() {
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        config.put("fields", fieldList);
+        config.put("method", "MD5");
+        String processorTag = randomAlphaOfLength(10);
+        Exception exception = expectThrows(ElasticsearchParseException.class,
+                () -> factory.create(null, processorTag, config));
+        assertThat(exception.getMessage(), equalTo("[fields] can not be empty"));
+    }
+
+    public void testMD5Base64Encode() {
+
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+        config.put("method", "MD5");
+        config.put("base64_encode", true);
+
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        Set<String> expectFeilds = new HashSet<>();
+        expectFeilds.add("_field");
+        assertEquals(expectFeilds, processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.MD5, processor.getMethod());
+        assertTrue(processor.isBase64Encode());
+        assertFalse(processor.isConcatenateAllFields());
+        assertFalse(processor.isIgnoreMissing());
+    }
+
+    public void testIgnoreMissing() {
+        Map<String, Object> config = new HashMap<>();
+        List<String> fieldList = new ArrayList<>();
+        fieldList.add("_field");
+        config.put("fields", fieldList);
+        config.put("ignore_missing", true);
+
+        String processorTag = randomAlphaOfLength(10);
+        FingerprintProcessor processor = factory.create(null, processorTag, config);
+        assertEquals(processorTag, processor.getTag());
+        assertEquals(FingerprintProcessor.TYPE, processor.getType());
+        Set<String> expectFeilds = new HashSet<>();
+        expectFeilds.add("_field");
+        assertEquals(expectFeilds, processor.getFields());
+        assertEquals("fingerprint", processor.getTargetField());
+        assertEquals(FingerprintProcessor.Method.MD5, processor.getMethod());
+        assertFalse(processor.isBase64Encode());
+        assertFalse(processor.isConcatenateAllFields());
+        assertTrue(processor.isIgnoreMissing());
+
+    }
+}
diff --git a/modules/ingest-common/src/test/java/org/elasticsearch/ingest/common/FingerprintProcessorTests.java b/modules/ingest-common/src/test/java/org/elasticsearch/ingest/common/FingerprintProcessorTests.java
new file mode 100644
index 0000000000000..fb6a1c3831b03
--- /dev/null
+++ b/modules/ingest-common/src/test/java/org/elasticsearch/ingest/common/FingerprintProcessorTests.java
@@ -0,0 +1,202 @@
+/*
+ * Licensed to Elasticsearch under one or more contributor
+ * license agreements. See the NOTICE file distributed with
+ * this work for additional information regarding copyright
+ * ownership. Elasticsearch licenses this file to you under
+ * the Apache License, Version 2.0 (the "License"); you may
+ * not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *    http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing,
+ * software distributed under the License is distributed on an
+ * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
+ * KIND, either express or implied.  See the License for the
+ * specific language governing permissions and limitations
+ * under the License.
+ */
+
+package org.elasticsearch.ingest.common;
+
+import static org.hamcrest.Matchers.equalTo;
+
+import java.util.HashMap;
+import java.util.HashSet;
+import java.util.Map;
+import java.util.Set;
+
+import org.elasticsearch.ingest.IngestDocument;
+import org.elasticsearch.ingest.RandomDocumentPicks;
+import org.elasticsearch.test.ESTestCase;
+
+public class FingerprintProcessorTests extends ESTestCase {
+
+    private FingerprintProcessor processor;
+
+    public void testDefault() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MD5, false, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("a0a003767ebde642b4d908ef7417d8e5", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testMultiFields() {
+        Set<String> fields = new HashSet<>();
+        int numFields = 100;
+        for (int i = 0; i < numFields; i++) {
+            fields.add("_field" + i);
+        }
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MD5, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        for (int i = 0; i < numFields; i++) {
+            document.put("_field" + i, "test content" + i);
+        }
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("WuSUaVpCeHF2PG2cx+nxGg==", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testUUID() {
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), null, "fingerprint",
+                FingerprintProcessor.Method.UUID, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        IngestDocument doc1 = RandomDocumentPicks.randomIngestDocument(random(), document);
+        doc1 = processor.execute(doc1);
+        assertNotNull(doc1.getFieldValue("fingerprint", String.class));
+        IngestDocument doc2 = RandomDocumentPicks.randomIngestDocument(random(), document);
+        doc2 = processor.execute(doc2);
+        assertNotNull(doc2.getFieldValue("fingerprint", String.class));
+        assertNotSame(doc1.getFieldValue("fingerprint", String.class), doc2.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testMD5Base64() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MD5, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("oKADdn695kK02QjvdBfY5Q==", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testSHA1Base64() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.SHA1, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("LNYh2CsbjGHNxZS2OfiQ9xrPZNo=", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testSHA256Base64() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.SHA256, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("34grIrfHH6eL3FDjLTnG/b30L2VYV2oxl1fER7Epbsg=", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testMURMUR3() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MURMUR3, false, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("155d94baf6c2bf3ca5bc0c22c67f3158", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testMURMUR3Base64() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MURMUR3, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("FV2UuvbCvzylvAwixn8xWA==", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testConcatenateAllFields() {
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), null, "fingerprint",
+                FingerprintProcessor.Method.MD5, true, true, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+
+        IngestDocument oldDoc = new IngestDocument("my_index", null, null, null, null, document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("XAT5+Ih2t2i6exqpH0fo9w==", newDoc.getFieldValue("fingerprint", String.class));
+
+        // changing the meta-field 'id' will also change the fingerprint
+        oldDoc = new IngestDocument("my_index", "1", null, null, null, document);
+        newDoc = processor.execute(oldDoc);
+        assertEquals("BgH4x6ZtRUP6QZ8f3ecB+g==", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testMissingFieldThrowException() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field1");
+        fields.add("_field2");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MD5, true, false, false);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field1", "test content");
+        IngestDocument doc1 = RandomDocumentPicks.randomIngestDocument(random(), document);
+        Exception exception = expectThrows(java.lang.IllegalArgumentException.class, () -> processor.execute(doc1));
+        assertThat(exception.getMessage(), equalTo("field [_field2] not present as part of path [_field2]"));
+
+        document.put("_field2", null);
+        IngestDocument doc2 = RandomDocumentPicks.randomIngestDocument(random(), document);
+        exception = expectThrows(java.lang.IllegalArgumentException.class, () -> processor.execute(doc2));
+        assertThat(exception.getMessage(), equalTo("field [_field2] is null, cannot generate fingerprint from it."));
+    }
+
+    public void testMissingSomeFields() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        fields.add("_field2");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MD5, true, false, true);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = processor.execute(oldDoc);
+        assertEquals("oKADdn695kK02QjvdBfY5Q==", newDoc.getFieldValue("fingerprint", String.class));
+    }
+
+    public void testIgnoreMissingAllFields() {
+        Set<String> fields = new HashSet<>();
+        fields.add("_field");
+        fields.add("_field2");
+        processor = new FingerprintProcessor(randomAlphaOfLength(10), fields, "fingerprint",
+                FingerprintProcessor.Method.MD5, true, false, true);
+        Map<String, Object> document = new HashMap<>();
+        document.put("_field3", "test content");
+        IngestDocument oldDoc = RandomDocumentPicks.randomIngestDocument(random(), document);
+        IngestDocument newDoc = new IngestDocument(processor.execute(oldDoc));
+        assertEquals(oldDoc, newDoc);
+        Exception exception = expectThrows(java.lang.IllegalArgumentException.class,
+                () -> newDoc.getFieldValue("fingerprint", String.class));
+        assertThat(exception.getMessage(), equalTo("field [fingerprint] not present as part of path [fingerprint]"));
+    }
+
+}
diff --git a/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/10_basic.yml b/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/10_basic.yml
index 8a803eae1fc3d..162fb1613ac9a 100644
--- a/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/10_basic.yml
+++ b/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/10_basic.yml
@@ -36,3 +36,4 @@
     - contains:  { nodes.$master.ingest.processors: { type: split } }
     - contains:  { nodes.$master.ingest.processors: { type: trim } }
     - contains:  { nodes.$master.ingest.processors: { type: uppercase } }
+    - contains:  { nodes.$master.ingest.processors: { type: fingerprint } }
diff --git a/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/260_fingerprint.yml b/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/260_fingerprint.yml
new file mode 100644
index 0000000000000..229809e925472
--- /dev/null
+++ b/modules/ingest-common/src/test/resources/rest-api-spec/test/ingest/260_fingerprint.yml
@@ -0,0 +1,171 @@
+---
+"Test ingest fingerprint processor with defaults":
+  - do:
+      ingest.put_pipeline:
+        id: "my_pipeline"
+        body:  >
+          {
+            "description": "_description",
+            "processors": [
+              {
+                "fingerprint" : {
+                  "fields" : ["_field"]
+                }
+              }
+            ]
+          }
+  - match: { acknowledged: true }
+
+  - do:
+      index:
+        index: test
+        id: 1
+        pipeline: "my_pipeline"
+        body: { _field: "test content" }
+
+  - do:
+      get:
+        index: test
+        id: 1
+  - match: { _source._field: "test content" }
+  - match: { _source.fingerprint: "a0a003767ebde642b4d908ef7417d8e5" }
+
+---
+"Test ingest fingerprint processor with method 'SHA1' and base64 encoding":
+  - do:
+      ingest.put_pipeline:
+        id: "my_pipeline"
+        body:  >
+          {
+            "description": "_description",
+            "processors": [
+              {
+                "fingerprint" : {
+                  "fields" : ["field1", "field2"],
+                  "method": "SHA1",
+                  "base64_encode": true,
+                  "target_field": "hash"
+                }
+              }
+            ]
+          }
+  - match: { acknowledged: true }
+
+  - do:
+      index:
+        index: test
+        id: 1
+        pipeline: "my_pipeline"
+        body: { field1: "test content1",  field2: "test content2"}
+
+  - do:
+      get:
+        index: test
+        id: 1
+  - match: { _source.field1: "test content1" }
+  - match: { _source.field2: "test content2" }
+  - match: { _source.hash: "WoS6Wglc7lPWj6Lo4Rg7DTc3coM=" }
+
+---
+"Test ingest fingerprint processor with method UUID":
+  - do:
+      ingest.put_pipeline:
+        id: "my_pipeline"
+        body:  >
+          {
+            "description": "_description",
+            "processors": [
+              {
+                "fingerprint" : {
+                  "method": "UUID",
+                  "target_field": "hash"
+                }
+              }
+            ]
+          }
+  - match: { acknowledged: true }
+
+  - do:
+      index:
+        index: test
+        id: 1
+        pipeline: "my_pipeline"
+        body: { field1: "test content1",  field2: "test content2"}
+
+  - do:
+      get:
+        index: test
+        id: 1
+  - match: { _source.field1: "test content1" }
+  - match: { _source.field2: "test content2" }
+  - is_true: _source.hash
+
+---
+"Test ingest fingerprint processor with concatenate_all_fields":
+  - do:
+      ingest.put_pipeline:
+        id: "my_pipeline"
+        body:  >
+          {
+            "description": "_description",
+            "processors": [
+              {
+                "fingerprint" : {
+                  "concatenate_all_fields": true,
+                  "method": "SHA256",
+                  "base64_encode": true
+                }
+              }
+            ]
+          }
+  - match: { acknowledged: true }
+
+  - do:
+      index:
+        index: test
+        id: 1
+        pipeline: "my_pipeline"
+        body: { field1: "test content1",  field2: "test content2"}
+
+  - do:
+      get:
+        index: test
+        id: 1
+  - match: { _source.field1: "test content1" }
+  - match: { _source.field2: "test content2" }
+  - match: { _source.fingerprint: "pCxZhjiaRyJ0yxP1Xp/KCYXxGkNPCJOjOdCR4Kx+cFU=" }
+
+
+---
+"Test ingest fingerprint processor with ignore_missing":
+  - do:
+      ingest.put_pipeline:
+        id: "my_pipeline"
+        body:  >
+          {
+            "description": "_description",
+            "processors": [
+              {
+                "fingerprint" : {
+                  "fields" : ["_field", "_field2"],
+                  "base64_encode": true,
+                  "ignore_missing": true
+                }
+              }
+            ]
+          }
+  - match: { acknowledged: true }
+
+  - do:
+      index:
+        index: test
+        id: 1
+        pipeline: "my_pipeline"
+        body: { _field: "test content"}
+
+  - do:
+      get:
+        index: test
+        id: 1
+  - match: { _source._field: "test content" }
+  - match: { _source.fingerprint: "oKADdn695kK02QjvdBfY5Q==" }
