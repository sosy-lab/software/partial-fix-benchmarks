diff --git a/server/src/main/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateService.java b/server/src/main/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateService.java
index c016d5d587647..68bf5e0d9a7dd 100644
--- a/server/src/main/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateService.java
+++ b/server/src/main/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateService.java
@@ -178,7 +178,8 @@ public void clusterStateProcessed(String source, ClusterState oldState, ClusterS
     // Package visible for testing
     ClusterState addComponentTemplate(final ClusterState currentState, final boolean create,
                                       final String name, final ComponentTemplate template) throws Exception {
-        if (create && currentState.metadata().componentTemplates().containsKey(name)) {
+        final ComponentTemplate existing = currentState.metadata().componentTemplates().get(name);
+        if (create && existing != null) {
             throw new IllegalArgumentException("component template [" + name + "] already exists");
         }
 
@@ -193,8 +194,6 @@ ClusterState addComponentTemplate(final ClusterState currentState, final boolean
                 .build();
         }
 
-        validateTemplate(finalSettings, stringMappings, indicesService, xContentRegistry);
-
         // Collect all the composable (index) templates that use this component template, we'll use
         // this for validating that they're still going to be valid after this component template
         // has been updated
@@ -238,8 +237,15 @@ ClusterState addComponentTemplate(final ClusterState currentState, final boolean
         final Template finalTemplate = new Template(finalSettings,
             stringMappings == null ? null : new CompressedXContent(stringMappings), template.template().aliases());
         final ComponentTemplate finalComponentTemplate = new ComponentTemplate(finalTemplate, template.version(), template.metadata());
+
+        if (finalComponentTemplate.equals(existing)) {
+            return currentState;
+        }
+
+        validateTemplate(finalSettings, stringMappings, indicesService, xContentRegistry);
         validate(name, finalComponentTemplate);
 
+        // Validate all composable index templates that use this component template
         if (templatesUsingComponent.size() > 0) {
             ClusterState tempStateWithComponentTemplateAdded = ClusterState.builder(currentState)
                 .metadata(Metadata.builder(currentState.metadata()).put(name, finalComponentTemplate))
@@ -265,7 +271,7 @@ ClusterState addComponentTemplate(final ClusterState currentState, final boolean
             }
         }
 
-        logger.info("adding component template [{}]", name);
+        logger.info("{} component template [{}]", existing == null ? "adding" : "updating", name);
         return ClusterState.builder(currentState)
             .metadata(Metadata.builder(currentState.metadata()).put(name, finalComponentTemplate))
             .build();
@@ -404,7 +410,8 @@ public static void validateV2TemplateRequest(Metadata metadata, String name, Com
 
     public ClusterState addIndexTemplateV2(final ClusterState currentState, final boolean create,
                                            final String name, final ComposableIndexTemplate template) throws Exception {
-        if (create && currentState.metadata().templatesV2().containsKey(name)) {
+        final ComposableIndexTemplate existing = currentState.metadata().templatesV2().get(name);
+        if (create && existing != null) {
             throw new IllegalArgumentException("index template [" + name + "] already exists");
         }
 
@@ -471,6 +478,10 @@ public ClusterState addIndexTemplateV2(final ClusterState currentState, final bo
                 template.priority(), template.version(), template.metadata(), template.getDataStreamTemplate());
         }
 
+        if (finalIndexTemplate.equals(existing)) {
+            return currentState;
+        }
+
         validate(name, finalIndexTemplate);
 
         // Finally, right before adding the template, we need to ensure that the composite settings,
@@ -483,7 +494,7 @@ public ClusterState addIndexTemplateV2(final ClusterState currentState, final bo
                 (finalIndexTemplate.composedOf().size() > 0 ? "with component templates " + finalIndexTemplate.composedOf() + " " : "") +
                 "is invalid", e);
         }
-        logger.info("adding index template [{}]", name);
+        logger.info("{} index template [{}]", existing == null ? "adding" : "updating", name);
         return ClusterState.builder(currentState)
             .metadata(Metadata.builder(currentState.metadata()).put(name, finalIndexTemplate))
             .build();
diff --git a/server/src/test/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateServiceTests.java b/server/src/test/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateServiceTests.java
index 8955852ca156d..67cba9848e73f 100644
--- a/server/src/test/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateServiceTests.java
+++ b/server/src/test/java/org/elasticsearch/cluster/metadata/MetadataIndexTemplateServiceTests.java
@@ -1040,6 +1040,30 @@ public void testUpdateComponentTemplateFailsIfResolvedIndexTemplatesWouldBeInval
             containsString("mapping fields [field2] cannot be replaced during template composition"));
     }
 
+    public void testPutExistingComponentTemplateIsNoop() throws Exception {
+        MetadataIndexTemplateService metadataIndexTemplateService = getMetadataIndexTemplateService();
+        ClusterState state = ClusterState.EMPTY_STATE;
+        ComponentTemplate componentTemplate = ComponentTemplateTests.randomInstance();
+        state = metadataIndexTemplateService.addComponentTemplate(state, false, "foo", componentTemplate);
+
+        assertNotNull(state.metadata().componentTemplates().get("foo"));
+        assertThat(state.metadata().componentTemplates().get("foo"), equalTo(componentTemplate));
+
+        assertThat(metadataIndexTemplateService.addComponentTemplate(state, false, "foo", componentTemplate), equalTo(state));
+    }
+
+    public void testPutExistingComposableTemplateIsNoop() throws Exception {
+        ClusterState state = ClusterState.EMPTY_STATE;
+        final MetadataIndexTemplateService metadataIndexTemplateService = getMetadataIndexTemplateService();
+        ComposableIndexTemplate template = ComposableIndexTemplateTests.randomInstance();
+        state = metadataIndexTemplateService.addIndexTemplateV2(state, false, "foo", template);
+
+        assertNotNull(state.metadata().templatesV2().get("foo"));
+        assertTemplatesEqual(state.metadata().templatesV2().get("foo"), template);
+
+        assertThat(metadataIndexTemplateService.addIndexTemplateV2(state, false, "foo", template), equalTo(state));
+    }
+
     private static List<Throwable> putTemplate(NamedXContentRegistry xContentRegistry, PutRequest request) {
         MetadataCreateIndexService createIndexService = new MetadataCreateIndexService(
                 Settings.EMPTY,
