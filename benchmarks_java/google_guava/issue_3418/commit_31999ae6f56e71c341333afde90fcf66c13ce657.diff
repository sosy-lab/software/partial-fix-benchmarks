diff --git a/android/guava/src/com/google/common/util/concurrent/ServiceManager.java b/android/guava/src/com/google/common/util/concurrent/ServiceManager.java
index a7ab33d7b1..fa7293783b 100644
--- a/android/guava/src/com/google/common/util/concurrent/ServiceManager.java
+++ b/android/guava/src/com/google/common/util/concurrent/ServiceManager.java
@@ -39,7 +39,6 @@
 import com.google.common.collect.ImmutableCollection;
 import com.google.common.collect.ImmutableList;
 import com.google.common.collect.ImmutableMap;
-import com.google.common.collect.ImmutableMultimap;
 import com.google.common.collect.ImmutableSet;
 import com.google.common.collect.ImmutableSetMultimap;
 import com.google.common.collect.Lists;
@@ -121,7 +120,7 @@
  */
 @Beta
 @GwtIncompatible
-public final class ServiceManager {
+public final class ServiceManager implements ServiceManagerBridge {
   private static final Logger logger = Logger.getLogger(ServiceManager.class.getName());
   private static final ListenerCallQueue.Event<Listener> HEALTHY_EVENT =
       new ListenerCallQueue.Event<Listener>() {
@@ -396,8 +395,11 @@ public boolean isHealthy() {
    *
    * <p>N.B. This snapshot is guaranteed to be consistent, i.e. the set of states returned will
    * correspond to a point in time view of the services.
+   *
+   * @since NEXT (present with return type {@code ImmutableMultimap} since 14.0)
    */
-  public ImmutableMultimap<State, Service> servicesByState() {
+  @Override
+  public ImmutableSetMultimap<State, Service> servicesByState() {
     return state.servicesByState();
   }
 
@@ -601,7 +603,7 @@ void awaitStopped(long timeout, TimeUnit unit) throws TimeoutException {
       }
     }
 
-    ImmutableMultimap<State, Service> servicesByState() {
+    ImmutableSetMultimap<State, Service> servicesByState() {
       ImmutableSetMultimap.Builder<State, Service> builder = ImmutableSetMultimap.builder();
       monitor.enter();
       try {
diff --git a/android/guava/src/com/google/common/util/concurrent/ServiceManagerBridge.java b/android/guava/src/com/google/common/util/concurrent/ServiceManagerBridge.java
new file mode 100644
index 0000000000..a9300a6166
--- /dev/null
+++ b/android/guava/src/com/google/common/util/concurrent/ServiceManagerBridge.java
@@ -0,0 +1,31 @@
+/*
+ * Copyright (C) 2020 The Guava Authors
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package com.google.common.util.concurrent;
+
+import com.google.common.annotations.GwtIncompatible;
+import com.google.common.collect.ImmutableMultimap;
+import com.google.common.util.concurrent.Service.State;
+
+/**
+ * Superinterface of {@link ServiceManager} to introduce a bridge method for {@code
+ * servicesByState()}, to ensure binary compatibility with older Guava versions that specified
+ * {@code servicesByState()} to return {@code ImmutableMultimap}.
+ */
+@GwtIncompatible
+interface ServiceManagerBridge {
+  ImmutableMultimap<State, Service> servicesByState();
+}
diff --git a/guava/src/com/google/common/util/concurrent/ServiceManager.java b/guava/src/com/google/common/util/concurrent/ServiceManager.java
index 29b5209ebd..1748ca6972 100644
--- a/guava/src/com/google/common/util/concurrent/ServiceManager.java
+++ b/guava/src/com/google/common/util/concurrent/ServiceManager.java
@@ -40,7 +40,6 @@
 import com.google.common.collect.ImmutableCollection;
 import com.google.common.collect.ImmutableList;
 import com.google.common.collect.ImmutableMap;
-import com.google.common.collect.ImmutableMultimap;
 import com.google.common.collect.ImmutableSet;
 import com.google.common.collect.ImmutableSetMultimap;
 import com.google.common.collect.Lists;
@@ -123,7 +122,7 @@
  */
 @Beta
 @GwtIncompatible
-public final class ServiceManager {
+public final class ServiceManager implements ServiceManagerBridge {
   private static final Logger logger = Logger.getLogger(ServiceManager.class.getName());
   private static final ListenerCallQueue.Event<Listener> HEALTHY_EVENT =
       new ListenerCallQueue.Event<Listener>() {
@@ -426,8 +425,11 @@ public boolean isHealthy() {
    *
    * <p>N.B. This snapshot is guaranteed to be consistent, i.e. the set of states returned will
    * correspond to a point in time view of the services.
+   *
+   * @since NEXT (present with return type {@code ImmutableMultimap} since 14.0)
    */
-  public ImmutableMultimap<State, Service> servicesByState() {
+  @Override
+  public ImmutableSetMultimap<State, Service> servicesByState() {
     return state.servicesByState();
   }
 
@@ -631,7 +633,7 @@ void awaitStopped(long timeout, TimeUnit unit) throws TimeoutException {
       }
     }
 
-    ImmutableMultimap<State, Service> servicesByState() {
+    ImmutableSetMultimap<State, Service> servicesByState() {
       ImmutableSetMultimap.Builder<State, Service> builder = ImmutableSetMultimap.builder();
       monitor.enter();
       try {
diff --git a/guava/src/com/google/common/util/concurrent/ServiceManagerBridge.java b/guava/src/com/google/common/util/concurrent/ServiceManagerBridge.java
new file mode 100644
index 0000000000..a9300a6166
--- /dev/null
+++ b/guava/src/com/google/common/util/concurrent/ServiceManagerBridge.java
@@ -0,0 +1,31 @@
+/*
+ * Copyright (C) 2020 The Guava Authors
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ * http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package com.google.common.util.concurrent;
+
+import com.google.common.annotations.GwtIncompatible;
+import com.google.common.collect.ImmutableMultimap;
+import com.google.common.util.concurrent.Service.State;
+
+/**
+ * Superinterface of {@link ServiceManager} to introduce a bridge method for {@code
+ * servicesByState()}, to ensure binary compatibility with older Guava versions that specified
+ * {@code servicesByState()} to return {@code ImmutableMultimap}.
+ */
+@GwtIncompatible
+interface ServiceManagerBridge {
+  ImmutableMultimap<State, Service> servicesByState();
+}
