diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisConnectionFactoryFactory.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisConnectionFactoryFactory.java
index e8b9979189f..5243600959a 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisConnectionFactoryFactory.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisConnectionFactoryFactory.java
@@ -40,6 +40,7 @@
  * @author Eddú Meléndez
  * @author Phillip Webb
  * @author Stephane Nicoll
+ * @author Justin Bertram
  */
 class ArtemisConnectionFactoryFactory {
 
@@ -127,13 +128,20 @@ private boolean isEmbeddedJmsClassPresent() {
 
 	private <T extends ActiveMQConnectionFactory> T createNativeConnectionFactory(Class<T> factoryClass)
 			throws Exception {
+		T connectionFactory;
 		Map<String, Object> params = new HashMap<>();
-		params.put(TransportConstants.HOST_PROP_NAME, this.properties.getHost());
-		params.put(TransportConstants.PORT_PROP_NAME, this.properties.getPort());
-		TransportConfiguration transportConfiguration = new TransportConfiguration(
-				NettyConnectorFactory.class.getName(), params);
-		Constructor<T> constructor = factoryClass.getConstructor(boolean.class, TransportConfiguration[].class);
-		T connectionFactory = constructor.newInstance(false, new TransportConfiguration[] { transportConfiguration });
+		String url = this.properties.getBrokerUrl();
+		if (StringUtils.hasText(url)) {
+			Constructor<T> constructor = factoryClass.getConstructor(String.class);
+			connectionFactory = constructor.newInstance(url);
+		} else {
+			params.put(TransportConstants.HOST_PROP_NAME, this.properties.getHost());
+			params.put(TransportConstants.PORT_PROP_NAME, this.properties.getPort());
+			TransportConfiguration transportConfiguration = new TransportConfiguration(
+					NettyConnectorFactory.class.getName(), params);
+			Constructor<T> constructor = factoryClass.getConstructor(boolean.class, TransportConfiguration[].class);
+			connectionFactory = constructor.newInstance(false, new TransportConfiguration[] { transportConfiguration });
+		}
 		String user = this.properties.getUser();
 		if (StringUtils.hasText(user)) {
 			connectionFactory.setUser(user);
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisProperties.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisProperties.java
index 1b227f60a7b..50103b696ae 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisProperties.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisProperties.java
@@ -32,6 +32,7 @@
  *
  * @author Eddú Meléndez
  * @author Stephane Nicoll
+ * @author Justin Bertram
  * @since 1.3.0
  */
 @ConfigurationProperties(prefix = "spring.artemis")
@@ -44,14 +45,25 @@
 
 	/**
 	 * Artemis broker host.
+	 *
+	 * This property is deprecated. Use <code>brokerUrl</code> instead.
 	 */
+	@Deprecated
 	private String host = "localhost";
 
 	/**
 	 * Artemis broker port.
+	 *
+	 * This property is deprecated. Use <code>brokerUrl</code> instead.
 	 */
+	@Deprecated
 	private int port = 61616;
 
+	/**
+	 * Artemis broker port.
+	 */
+	private String brokerUrl = "tcp://localhost:61616";
+
 	/**
 	 * Login user of the broker.
 	 */
@@ -91,6 +103,14 @@ public void setPort(int port) {
 		this.port = port;
 	}
 
+	public String getBrokerUrl() {
+		return this.brokerUrl;
+	}
+
+	public void setBrokerUrl(String brokerUrl) {
+		this.brokerUrl = brokerUrl;
+	}
+
 	public String getUser() {
 		return this.user;
 	}
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisAutoConfigurationTests.java
index b93e4a45b5c..353c5fd2564 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jms/artemis/ArtemisAutoConfigurationTests.java
@@ -126,10 +126,18 @@ void nativeConnectionFactory() {
 	@Test
 	void nativeConnectionFactoryCustomHost() {
 		this.contextRunner.withUserConfiguration(EmptyConfiguration.class)
-				.withPropertyValues("spring.artemis.mode:native", "spring.artemis.host:192.168.1.144",
-						"spring.artemis.port:9876")
-				.run((context) -> assertNettyConnectionFactory(
-						getActiveMQConnectionFactory(getConnectionFactory(context)), "192.168.1.144", 9876));
+			    .withPropertyValues("spring.artemis.mode:native", "spring.artemis.host:192.168.1.144",
+			 		    "spring.artemis.port:9876", "spring.artemis.broker-url: ") // unset the url so the default doesn't override
+			    .run((context) -> assertNettyConnectionFactory(
+					    getActiveMQConnectionFactory(getConnectionFactory(context)), "192.168.1.144", 9876));
+	}
+
+	@Test
+	void nativeConnectionFactoryCustomUrl() {
+		this.contextRunner.withUserConfiguration(EmptyConfiguration.class)
+			    .withPropertyValues("spring.artemis.mode:native", "spring.artemis.broker-url:tcp://192.168.1.144:9876")
+			    .run((context) -> assertNettyConnectionFactory(
+					    getActiveMQConnectionFactory(getConnectionFactory(context)), "192.168.1.144", 9876));
 	}
 
 	@Test
@@ -377,7 +385,11 @@ private TransportConfiguration assertNettyConnectionFactory(ActiveMQConnectionFa
 		TransportConfiguration transportConfig = getSingleTransportConfiguration(connectionFactory);
 		assertThat(transportConfig.getFactoryClassName()).isEqualTo(NettyConnectorFactory.class.getName());
 		assertThat(transportConfig.getParams().get("host")).isEqualTo(host);
-		assertThat(transportConfig.getParams().get("port")).isEqualTo(port);
+		Object transportConfigPort = transportConfig.getParams().get("port"); // may be an int or a String
+		if (transportConfigPort instanceof String) {
+			transportConfigPort = Integer.parseInt((String) transportConfigPort);
+		}
+		assertThat(transportConfigPort).isEqualTo(port);
 		return transportConfig;
 	}
 
diff --git a/spring-boot-project/spring-boot-docs/src/docs/asciidoc/spring-boot-features.adoc b/spring-boot-project/spring-boot-docs/src/docs/asciidoc/spring-boot-features.adoc
index 6b80d2d750a..84acd166b05 100644
--- a/spring-boot-project/spring-boot-docs/src/docs/asciidoc/spring-boot-features.adoc
+++ b/spring-boot-project/spring-boot-docs/src/docs/asciidoc/spring-boot-features.adoc
@@ -5709,16 +5709,16 @@ By default, ActiveMQ creates a destination if it does not yet exist so that dest
 
 
 [[boot-features-artemis]]
-==== Artemis Support
-Spring Boot can auto-configure a `ConnectionFactory` when it detects that https://activemq.apache.org/components/artemis/[Artemis] is available on the classpath.
+==== ActiveMQ Artemis Support
+Spring Boot can auto-configure a `ConnectionFactory` when it detects that https://activemq.apache.org/components/artemis/[ActiveMQ Artemis] is available on the classpath.
 If the broker is present, an embedded broker is automatically started and configured (unless the mode property has been explicitly set).
 The supported modes are `embedded` (to make explicit that an embedded broker is required and that an error should occur if the broker is not available on the classpath) and `native` (to connect to a broker using the `netty` transport protocol).
 When the latter is configured, Spring Boot configures a `ConnectionFactory` that connects to a broker running on the local machine with the default settings.
 
-NOTE: If you use `spring-boot-starter-artemis`, the necessary dependencies to connect to an existing Artemis instance are provided, as well as the Spring infrastructure to integrate with JMS.
+NOTE: If you use `spring-boot-starter-artemis`, the necessary dependencies to connect to an existing ActiveMQ Artemis instance are provided, as well as the Spring infrastructure to integrate with JMS.
 Adding `org.apache.activemq:artemis-jms-server` to your application lets you use embedded mode.
 
-Artemis configuration is controlled by external configuration properties in `+spring.artemis.*+`.
+ActiveMQ Artemis configuration is controlled by external configuration properties in `+spring.artemis.*+`.
 For example, you might declare the following section in `application.properties`:
 
 [source,yaml,indent=0,configprops,configblocks]
@@ -5726,8 +5726,7 @@ For example, you might declare the following section in `application.properties`
 	spring:
 	  artemis:
 	    mode: native
-	    host: "192.168.1.210"
-	    port: 9876
+	    broker-url: "tcp://192.168.1.210:9876"
 	    user: "admin"
 	    password: "secret"
 ----
