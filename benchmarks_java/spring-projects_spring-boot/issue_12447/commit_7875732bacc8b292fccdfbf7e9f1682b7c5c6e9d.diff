diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/pom.xml b/spring-boot-project/spring-boot-actuator-autoconfigure/pom.xml
index ef0ecf425de..6ca25c42a9d 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/pom.xml
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/pom.xml
@@ -92,6 +92,11 @@
 			<artifactId>micrometer-core</artifactId>
 			<optional>true</optional>
 		</dependency>
+		<dependency>
+			<groupId>io.micrometer</groupId>
+			<artifactId>micrometer-jersey2</artifactId>
+			<optional>true</optional>
+		</dependency>
 		<dependency>
 			<groupId>io.micrometer</groupId>
 			<artifactId>micrometer-registry-atlas</artifactId>
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsAutoConfiguration.java
new file mode 100644
index 00000000000..73afe7301b6
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsAutoConfiguration.java
@@ -0,0 +1,79 @@
+/*
+ * Copyright 2012-2018 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.metrics.jersey2.server;
+
+import java.lang.annotation.Annotation;
+import java.lang.reflect.AnnotatedElement;
+
+import io.micrometer.core.instrument.MeterRegistry;
+import io.micrometer.jersey2.server.AnnotationFinder;
+import io.micrometer.jersey2.server.DefaultJerseyTagsProvider;
+import io.micrometer.jersey2.server.JerseyTagsProvider;
+import io.micrometer.jersey2.server.MetricsApplicationEventListener;
+import org.glassfish.jersey.server.ResourceConfig;
+
+import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
+import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
+import org.springframework.boot.autoconfigure.AutoConfigureAfter;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
+import org.springframework.boot.autoconfigure.jersey.ResourceConfigCustomizer;
+import org.springframework.boot.context.properties.EnableConfigurationProperties;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.core.annotation.AnnotationUtils;
+
+/**
+ * Auto-configuration for Jersey server instrumentation.
+ *
+ * @author Michael Weirauch
+ * @since 2.0.1
+ */
+@Configuration
+@AutoConfigureAfter({ MetricsAutoConfiguration.class,
+		SimpleMetricsExportAutoConfiguration.class })
+@ConditionalOnWebApplication(type = ConditionalOnWebApplication.Type.SERVLET)
+@ConditionalOnClass({ ResourceConfig.class, MetricsApplicationEventListener.class })
+@ConditionalOnBean(MeterRegistry.class)
+@EnableConfigurationProperties(JerseyServerMetricsProperties.class)
+public class JerseyServerMetricsAutoConfiguration {
+
+	@Bean
+	@ConditionalOnMissingBean(JerseyTagsProvider.class)
+	public DefaultJerseyTagsProvider jerseyTagsProvider() {
+		return new DefaultJerseyTagsProvider();
+	}
+
+	@Bean
+	public ResourceConfigCustomizer jerseyResourceConfigCustomizer(
+			MeterRegistry meterRegistry, JerseyServerMetricsProperties properties,
+			JerseyTagsProvider tagsProvider) {
+		return (config) -> config.register(new MetricsApplicationEventListener(
+				meterRegistry, tagsProvider, properties.getRequestsMetricName(),
+				properties.isAutoTimeRequests(), new AnnotationFinder() {
+					@Override
+					public <A extends Annotation> A findAnnotation(
+							AnnotatedElement annotatedElement, Class<A> annotationType) {
+						return AnnotationUtils.findAnnotation(annotatedElement,
+								annotationType);
+					}
+				}));
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsProperties.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsProperties.java
new file mode 100644
index 00000000000..8ff5f1a92e0
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsProperties.java
@@ -0,0 +1,50 @@
+/*
+ * Copyright 2012-2018 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.metrics.jersey2.server;
+
+import org.springframework.boot.context.properties.ConfigurationProperties;
+
+/**
+ * Configuration for Jersey server instrumentation.
+ *
+ * @author Michael Weirauch
+ * @since 2.0.1
+ */
+@ConfigurationProperties(prefix = "management.metrics.jersey2.server")
+public class JerseyServerMetricsProperties {
+
+	private String requestsMetricName = "http.server.requests";
+
+	private boolean autoTimeRequests = true;
+
+	public String getRequestsMetricName() {
+		return this.requestsMetricName;
+	}
+
+	public void setRequestsMetricName(String requestsMetricName) {
+		this.requestsMetricName = requestsMetricName;
+	}
+
+	public boolean isAutoTimeRequests() {
+		return this.autoTimeRequests;
+	}
+
+	public void setAutoTimeRequests(boolean autoTimeRequests) {
+		this.autoTimeRequests = autoTimeRequests;
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/package-info.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/package-info.java
new file mode 100644
index 00000000000..093d2288788
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/package-info.java
@@ -0,0 +1,20 @@
+/*
+ * Copyright 2012-2018 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+/**
+ * Auto-configuration for Jersey server actuator metrics.
+ */
+package org.springframework.boot.actuate.autoconfigure.metrics.jersey2.server;
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/resources/META-INF/spring.factories b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/resources/META-INF/spring.factories
index 60dabb05a23..e8b0dd4605a 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/resources/META-INF/spring.factories
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/resources/META-INF/spring.factories
@@ -49,6 +49,7 @@ org.springframework.boot.actuate.autoconfigure.metrics.export.signalfx.SignalFxM
 org.springframework.boot.actuate.autoconfigure.metrics.export.statsd.StatsdMetricsExportAutoConfiguration,\
 org.springframework.boot.actuate.autoconfigure.metrics.export.wavefront.WavefrontMetricsExportAutoConfiguration,\
 org.springframework.boot.actuate.autoconfigure.metrics.jdbc.DataSourcePoolMetricsAutoConfiguration,\
+org.springframework.boot.actuate.autoconfigure.metrics.jersey2.server.JerseyServerMetricsAutoConfiguration,\
 org.springframework.boot.actuate.autoconfigure.metrics.web.client.RestTemplateMetricsAutoConfiguration,\
 org.springframework.boot.actuate.autoconfigure.metrics.web.reactive.WebFluxMetricsAutoConfiguration,\
 org.springframework.boot.actuate.autoconfigure.metrics.web.servlet.WebMvcMetricsAutoConfiguration,\
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsAutoConfigurationTests.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsAutoConfigurationTests.java
new file mode 100644
index 00000000000..5f204022986
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/jersey2/server/JerseyServerMetricsAutoConfigurationTests.java
@@ -0,0 +1,128 @@
+/*
+ * Copyright 2012-2018 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.metrics.jersey2.server;
+
+import java.net.URI;
+
+import javax.ws.rs.ApplicationPath;
+import javax.ws.rs.GET;
+import javax.ws.rs.Path;
+import javax.ws.rs.PathParam;
+
+import io.micrometer.core.instrument.MeterRegistry;
+import io.micrometer.core.instrument.Timer;
+import io.micrometer.jersey2.server.MetricsApplicationEventListener;
+import org.glassfish.jersey.server.ResourceConfig;
+import org.junit.Test;
+
+import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
+import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
+import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.autoconfigure.jersey.JerseyAutoConfiguration;
+import org.springframework.boot.autoconfigure.jersey.ResourceConfigCustomizer;
+import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
+import org.springframework.boot.test.context.FilteredClassLoader;
+import org.springframework.boot.test.context.assertj.AssertableWebApplicationContext;
+import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
+import org.springframework.boot.web.servlet.context.AnnotationConfigServletWebServerApplicationContext;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.web.client.RestTemplate;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link JerseyServerMetricsAutoConfiguration}.
+ *
+ * @author Michael Weirauch
+ */
+public class JerseyServerMetricsAutoConfigurationTests {
+
+	private WebApplicationContextRunner contextRunner = new WebApplicationContextRunner(
+			AnnotationConfigServletWebServerApplicationContext::new)
+					.withConfiguration(
+							AutoConfigurations.of(JerseyAutoConfiguration.class,
+									JerseyServerMetricsAutoConfiguration.class,
+									ServletWebServerFactoryAutoConfiguration.class,
+									SimpleMetricsExportAutoConfiguration.class,
+									MetricsAutoConfiguration.class))
+					.withUserConfiguration(ResourceConfiguration.class)
+					.withPropertyValues("server.port:0");
+
+	@Test
+	public void httpRequestsAreTimed() {
+		this.contextRunner.run((context) -> {
+			doRequest(context);
+
+			MeterRegistry registry = context.getBean(MeterRegistry.class);
+			Timer timer = registry.get("http.server.requests").tag("uri", "/users/{id}")
+					.timer();
+			assertThat(timer.count()).isEqualTo(1);
+		});
+	}
+
+	@Test
+	public void noHttpRequestsTimedWhenJerseyInstrumentationMissingFromClasspath() {
+		this.contextRunner
+				.withClassLoader(
+						new FilteredClassLoader(MetricsApplicationEventListener.class))
+				.run((context) -> {
+					doRequest(context);
+
+					MeterRegistry registry = context.getBean(MeterRegistry.class);
+					assertThat(registry.find("http.server.requests").timer()).isNull();
+				});
+	}
+
+	private static void doRequest(AssertableWebApplicationContext context) {
+		int port = context
+				.getSourceApplicationContext(
+						AnnotationConfigServletWebServerApplicationContext.class)
+				.getWebServer().getPort();
+		RestTemplate restTemplate = new RestTemplate();
+		restTemplate.getForEntity(URI.create("http://localhost:" + port + "/users/3"),
+				String.class);
+	}
+
+	@Configuration
+	@ApplicationPath("/")
+	static class ResourceConfiguration {
+
+		@Bean
+		ResourceConfig resourceConfig() {
+			return new ResourceConfig();
+		}
+
+		@Bean
+		ResourceConfigCustomizer resourceConfigCustomizer() {
+			return (config) -> config.register(new TestResource());
+		}
+
+		@Path("/users")
+		public class TestResource {
+
+			@GET
+			@Path("/{id}")
+			public String getUser(@PathParam("id") String id) {
+				return id;
+			}
+
+		}
+
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java
index 761c2a7766a..0b3016a17ab 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java
@@ -37,6 +37,7 @@
 import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.export.statsd.StatsdMetricsExportAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.jdbc.DataSourcePoolMetricsAutoConfiguration;
+import org.springframework.boot.actuate.autoconfigure.metrics.jersey2.server.JerseyServerMetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.web.client.RestTemplateMetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.web.reactive.WebFluxMetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.web.servlet.WebMvcMetricsAutoConfiguration;
@@ -76,7 +77,8 @@
 			RabbitMetricsAutoConfiguration.class, CacheMetricsAutoConfiguration.class,
 			DataSourcePoolMetricsAutoConfiguration.class,
 			RestTemplateMetricsAutoConfiguration.class,
-			WebFluxMetricsAutoConfiguration.class, WebMvcMetricsAutoConfiguration.class);
+			WebFluxMetricsAutoConfiguration.class, WebMvcMetricsAutoConfiguration.class,
+			JerseyServerMetricsAutoConfiguration.class);
 
 	private MetricsRun() {
 	}
diff --git a/spring-boot-project/spring-boot-dependencies/pom.xml b/spring-boot-project/spring-boot-dependencies/pom.xml
index 42542701bc9..cfa9f168ae3 100644
--- a/spring-boot-project/spring-boot-dependencies/pom.xml
+++ b/spring-boot-project/spring-boot-dependencies/pom.xml
@@ -860,6 +860,11 @@
 				<artifactId>micrometer-core</artifactId>
 				<version>${micrometer.version}</version>
 			</dependency>
+			<dependency>
+				<groupId>io.micrometer</groupId>
+				<artifactId>micrometer-jersey2</artifactId>
+				<version>${micrometer.version}</version>
+			</dependency>
 			<dependency>
 				<groupId>io.micrometer</groupId>
 				<artifactId>micrometer-registry-atlas</artifactId>
