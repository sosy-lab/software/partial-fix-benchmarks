diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/MetricsProperties.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/MetricsProperties.java
index 860a94d948f..2eeabbdf4d3 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/MetricsProperties.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/MetricsProperties.java
@@ -137,6 +137,13 @@ public void setMaxUriTags(int maxUriTags) {
 			 */
 			private String requestsMetricName = "http.server.requests";
 
+			/**
+			 * Maximum number of unique URI tag values allowed. After the max number of
+			 * tag values is reached, metrics with additional tag values are denied by
+			 * filter.
+			 */
+			private int maxUriTags = 100;
+
 			public boolean isAutoTimeRequests() {
 				return this.autoTimeRequests;
 			}
@@ -153,6 +160,14 @@ public void setRequestsMetricName(String requestsMetricName) {
 				this.requestsMetricName = requestsMetricName;
 			}
 
+			public int getMaxUriTags() {
+				return this.maxUriTags;
+			}
+
+			public void setMaxUriTags(int maxUriTags) {
+				this.maxUriTags = maxUriTags;
+			}
+
 		}
 
 	}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/OnlyOnceLoggingDenyMeterFilter.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/OnlyOnceLoggingDenyMeterFilter.java
new file mode 100644
index 00000000000..87c007d77c6
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/OnlyOnceLoggingDenyMeterFilter.java
@@ -0,0 +1,58 @@
+/*
+ * Copyright 2012-2018 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.metrics;
+
+import java.util.concurrent.atomic.AtomicBoolean;
+import java.util.function.Supplier;
+
+import io.micrometer.core.instrument.Meter;
+import io.micrometer.core.instrument.config.MeterFilter;
+import io.micrometer.core.instrument.config.MeterFilterReply;
+import org.slf4j.Logger;
+import org.slf4j.LoggerFactory;
+
+import org.springframework.util.Assert;
+
+/**
+ * {@link MeterFilter} to log only once a warning message and deny {@link Meter.Id}.
+ *
+ * @author Dmytro Nosan
+ */
+public final class OnlyOnceLoggingDenyMeterFilter implements MeterFilter {
+
+	private final Logger logger = LoggerFactory
+			.getLogger(OnlyOnceLoggingDenyMeterFilter.class);
+
+	private final AtomicBoolean alreadyWarned = new AtomicBoolean(false);
+
+	private final Supplier<String> message;
+
+	public OnlyOnceLoggingDenyMeterFilter(Supplier<String> message) {
+		Assert.notNull(message, "Message must not be null");
+		this.message = message;
+	}
+
+	@Override
+	public MeterFilterReply accept(Meter.Id id) {
+		if (this.logger.isWarnEnabled()
+				&& this.alreadyWarned.compareAndSet(false, true)) {
+			this.logger.warn(this.message.get());
+		}
+		return MeterFilterReply.DENY;
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfiguration.java
index 66ac2f7eb1c..f5c00134b56 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfiguration.java
@@ -16,17 +16,12 @@
 
 package org.springframework.boot.actuate.autoconfigure.metrics.web.client;
 
-import java.util.concurrent.atomic.AtomicBoolean;
-
-import io.micrometer.core.instrument.Meter.Id;
 import io.micrometer.core.instrument.MeterRegistry;
 import io.micrometer.core.instrument.config.MeterFilter;
-import io.micrometer.core.instrument.config.MeterFilterReply;
-import org.slf4j.Logger;
-import org.slf4j.LoggerFactory;
 
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsProperties;
+import org.springframework.boot.actuate.autoconfigure.metrics.OnlyOnceLoggingDenyMeterFilter;
 import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
 import org.springframework.boot.actuate.metrics.web.client.DefaultRestTemplateExchangeTagsProvider;
 import org.springframework.boot.actuate.metrics.web.client.MetricsRestTemplateCustomizer;
@@ -76,43 +71,12 @@ public MetricsRestTemplateCustomizer metricsRestTemplateCustomizer(
 	@Order(0)
 	public MeterFilter metricsHttpClientUriTagFilter(MetricsProperties properties) {
 		String metricName = properties.getWeb().getClient().getRequestsMetricName();
-		MeterFilter denyFilter = new MaximumUriTagsReachedMeterFilter(metricName);
+		MeterFilter denyFilter = new OnlyOnceLoggingDenyMeterFilter(() -> String.format(
+				"Reached the maximum number of URI tags for '%s'. "
+						+ "Are you using uriVariables on RestTemplate calls?",
+				metricName));
 		return MeterFilter.maximumAllowableTags(metricName, "uri",
 				properties.getWeb().getClient().getMaxUriTags(), denyFilter);
 	}
 
-	/**
-	 * {@link MeterFilter} to deny further URI tags and log a warning.
-	 */
-	private static class MaximumUriTagsReachedMeterFilter implements MeterFilter {
-
-		private final Logger logger = LoggerFactory
-				.getLogger(MaximumUriTagsReachedMeterFilter.class);
-
-		private final String metricName;
-
-		private final AtomicBoolean alreadyWarned = new AtomicBoolean(false);
-
-		MaximumUriTagsReachedMeterFilter(String metricName) {
-			this.metricName = metricName;
-		}
-
-		@Override
-		public MeterFilterReply accept(Id id) {
-			if (this.alreadyWarned.compareAndSet(false, true)) {
-				logWarning();
-			}
-			return MeterFilterReply.DENY;
-		}
-
-		private void logWarning() {
-			if (this.logger.isWarnEnabled()) {
-				this.logger.warn(
-						"Reached the maximum number of URI tags for '" + this.metricName
-								+ "'. Are you using uriVariables on HTTP client calls?");
-			}
-		}
-
-	}
-
 }
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfiguration.java
index 9828805672b..f692c56dec2 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfiguration.java
@@ -17,9 +17,11 @@
 package org.springframework.boot.actuate.autoconfigure.metrics.web.reactive;
 
 import io.micrometer.core.instrument.MeterRegistry;
+import io.micrometer.core.instrument.config.MeterFilter;
 
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsProperties;
+import org.springframework.boot.actuate.autoconfigure.metrics.OnlyOnceLoggingDenyMeterFilter;
 import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
 import org.springframework.boot.actuate.metrics.web.reactive.server.DefaultWebFluxTagsProvider;
 import org.springframework.boot.actuate.metrics.web.reactive.server.MetricsWebFilter;
@@ -31,12 +33,14 @@
 import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
+import org.springframework.core.annotation.Order;
 
 /**
  * {@link EnableAutoConfiguration Auto-configuration} for instrumentation of Spring
  * WebFlux applications.
  *
  * @author Jon Schneider
+ * @author Dmytro Nosan
  * @since 2.0.0
  */
 @Configuration
@@ -59,4 +63,14 @@ public MetricsWebFilter webfluxMetrics(MeterRegistry registry,
 				properties.getWeb().getServer().getRequestsMetricName());
 	}
 
+	@Bean
+	@Order(0)
+	public MeterFilter metricsHttpServerUriTagFilter(MetricsProperties properties) {
+		String metricName = properties.getWeb().getServer().getRequestsMetricName();
+		MeterFilter filter = new OnlyOnceLoggingDenyMeterFilter(() -> String
+				.format("Reached the maximum number of URI tags for '%s'.", metricName));
+		return MeterFilter.maximumAllowableTags(metricName, "uri",
+				properties.getWeb().getServer().getMaxUriTags(), filter);
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/servlet/WebMvcMetricsAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/servlet/WebMvcMetricsAutoConfiguration.java
index 566307f2632..1a52fedcc0c 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/servlet/WebMvcMetricsAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/metrics/web/servlet/WebMvcMetricsAutoConfiguration.java
@@ -19,10 +19,12 @@
 import javax.servlet.DispatcherType;
 
 import io.micrometer.core.instrument.MeterRegistry;
+import io.micrometer.core.instrument.config.MeterFilter;
 
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsProperties;
 import org.springframework.boot.actuate.autoconfigure.metrics.MetricsProperties.Web.Server;
+import org.springframework.boot.actuate.autoconfigure.metrics.OnlyOnceLoggingDenyMeterFilter;
 import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
 import org.springframework.boot.actuate.metrics.web.servlet.DefaultWebMvcTagsProvider;
 import org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter;
@@ -38,6 +40,7 @@
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.core.Ordered;
+import org.springframework.core.annotation.Order;
 import org.springframework.web.context.WebApplicationContext;
 import org.springframework.web.servlet.DispatcherServlet;
 
@@ -46,6 +49,7 @@
  * MVC servlet-based request mappings.
  *
  * @author Jon Schneider
+ * @author Dmytro Nosan
  * @since 2.0.0
  */
 @Configuration
@@ -78,4 +82,14 @@ public DefaultWebMvcTagsProvider webMvcTagsProvider() {
 		return registration;
 	}
 
+	@Bean
+	@Order(0)
+	public MeterFilter metricsHttpServerUriTagFilter(MetricsProperties properties) {
+		String metricName = properties.getWeb().getServer().getRequestsMetricName();
+		MeterFilter filter = new OnlyOnceLoggingDenyMeterFilter(() -> String
+				.format("Reached the maximum number of URI tags for '%s'.", metricName));
+		return MeterFilter.maximumAllowableTags(metricName, "uri",
+				properties.getWeb().getServer().getMaxUriTags(), filter);
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java
index dfd8e7bf8a2..9a08cb93c87 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/test/MetricsRun.java
@@ -43,6 +43,7 @@
 import org.springframework.boot.actuate.autoconfigure.metrics.web.reactive.WebFluxMetricsAutoConfiguration;
 import org.springframework.boot.actuate.autoconfigure.metrics.web.servlet.WebMvcMetricsAutoConfiguration;
 import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.test.context.runner.AbstractApplicationContextRunner;
 import org.springframework.boot.test.context.runner.ApplicationContextRunner;
 import org.springframework.util.Assert;
 
@@ -90,7 +91,7 @@ private MetricsRun() {
 	 * implementation.
 	 * @return the function to apply
 	 */
-	public static Function<ApplicationContextRunner, ApplicationContextRunner> simple() {
+	public static <T extends AbstractApplicationContextRunner> Function<T, T> simple() {
 		return limitedTo(SimpleMetricsExportAutoConfiguration.class);
 	}
 
@@ -100,18 +101,19 @@ private MetricsRun() {
 	 * @param exportAutoConfigurations the export auto-configurations to include
 	 * @return the function to apply
 	 */
-	public static Function<ApplicationContextRunner, ApplicationContextRunner> limitedTo(
+	public static <T extends AbstractApplicationContextRunner> Function<T, T> limitedTo(
 			Class<?>... exportAutoConfigurations) {
 		return (contextRunner) -> apply(contextRunner, exportAutoConfigurations);
 	}
 
-	private static ApplicationContextRunner apply(ApplicationContextRunner contextRunner,
+	@SuppressWarnings("unchecked")
+	private static <T extends AbstractApplicationContextRunner> T apply(T contextRunner,
 			Class<?>[] exportAutoConfigurations) {
 		for (Class<?> configuration : exportAutoConfigurations) {
 			Assert.state(EXPORT_AUTO_CONFIGURATIONS.contains(configuration),
 					() -> "Unknown export auto-configuration " + configuration.getName());
 		}
-		return contextRunner
+		return (T) contextRunner
 				.withPropertyValues("management.metrics.use-global-registry=false")
 				.withConfiguration(AUTO_CONFIGURATIONS)
 				.withConfiguration(AutoConfigurations.of(exportAutoConfigurations));
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfigurationTests.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfigurationTests.java
index 62abdf3b6ed..c2501d2caf5 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/client/RestTemplateMetricsAutoConfigurationTests.java
@@ -97,7 +97,7 @@ public void afterMaxUrisReachedFurtherUrisAreDenied() {
 							.hasSize(maxUriTags);
 					assertThat(this.out.toString())
 							.contains("Reached the maximum number of URI tags "
-									+ "for 'http.client.requests'");
+									+ "for 'http.client.requests'. Are you using uriVariables on RestTemplate calls?");
 				});
 	}
 
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfigurationTests.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfigurationTests.java
index b1760fa64d8..5069686a5e7 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/metrics/web/reactive/WebFluxMetricsAutoConfigurationTests.java
@@ -16,17 +16,24 @@
 
 package org.springframework.boot.actuate.autoconfigure.metrics.web.reactive;
 
+import io.micrometer.core.instrument.MeterRegistry;
+import org.junit.Rule;
 import org.junit.Test;
 
-import org.springframework.boot.actuate.autoconfigure.metrics.MetricsAutoConfiguration;
-import org.springframework.boot.actuate.autoconfigure.metrics.export.simple.SimpleMetricsExportAutoConfiguration;
+import org.springframework.boot.actuate.autoconfigure.metrics.test.MetricsRun;
 import org.springframework.boot.actuate.metrics.web.reactive.server.DefaultWebFluxTagsProvider;
 import org.springframework.boot.actuate.metrics.web.reactive.server.MetricsWebFilter;
 import org.springframework.boot.actuate.metrics.web.reactive.server.WebFluxTagsProvider;
 import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.autoconfigure.web.reactive.WebFluxAutoConfiguration;
 import org.springframework.boot.test.context.runner.ReactiveWebApplicationContextRunner;
+import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
+import org.springframework.boot.test.rule.OutputCapture;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
+import org.springframework.test.web.reactive.server.WebTestClient;
+import org.springframework.web.bind.annotation.GetMapping;
+import org.springframework.web.bind.annotation.RestController;
 
 import static org.assertj.core.api.Assertions.assertThat;
 import static org.mockito.Mockito.mock;
@@ -35,13 +42,24 @@
  * Tests for {@link WebFluxMetricsAutoConfiguration}
  *
  * @author Brian Clozel
+ * @author Dmytro Nosan
  */
 public class WebFluxMetricsAutoConfigurationTests {
 
 	private ReactiveWebApplicationContextRunner contextRunner = new ReactiveWebApplicationContextRunner()
-			.withConfiguration(AutoConfigurations.of(MetricsAutoConfiguration.class,
-					SimpleMetricsExportAutoConfiguration.class,
-					WebFluxMetricsAutoConfiguration.class));
+			.with(MetricsRun.simple());
+
+	@Rule
+	public OutputCapture output = new OutputCapture();
+
+	@Test
+	public void backsOffWhenMeterRegistryIsMissing() {
+		new WebApplicationContextRunner()
+				.withConfiguration(
+						AutoConfigurations.of(WebFluxMetricsAutoConfiguration.class))
+				.run((context) -> assertThat(context)
+						.doesNotHaveBean(WebFluxMetricsAutoConfiguration.class));
+	}
 
 	@Test
 	public void shouldProvideWebFluxMetricsBeans() {
@@ -58,6 +76,28 @@ public void shouldNotOverrideCustomTagsProvider() {
 						.hasSize(1).containsKey("customWebFluxTagsProvider"));
 	}
 
+	@Test
+	public void afterMaxUrisReachedFurtherUrisAreDenied() {
+		this.contextRunner
+				.withConfiguration(AutoConfigurations.of(WebFluxAutoConfiguration.class))
+				.withUserConfiguration(TestController.class)
+				.withPropertyValues("management.metrics.web.server.max-uri-tags=2")
+				.run((context) -> {
+					WebTestClient webTestClient = WebTestClient
+							.bindToApplicationContext(context).build();
+
+					for (int i = 0; i < 3; i++) {
+						webTestClient.get().uri("/test" + i).exchange().expectStatus()
+								.isOk();
+					}
+					MeterRegistry registry = context.getBean(MeterRegistry.class);
+					assertThat(registry.get("http.server.requests").meters()).hasSize(2);
+					assertThat(this.output.toString())
+							.contains("Reached the maximum number of URI tags "
+									+ "for 'http.server.requests'");
+				});
+	}
+
 	@Configuration
 	protected static class CustomWebFluxTagsProviderConfig {
 
@@ -68,4 +108,24 @@ public WebFluxTagsProvider customWebFluxTagsProvider() {
 
 	}
 
+	@RestController
+	static class TestController {
+
+		@GetMapping("test0")
+		public String test0() {
+			return "test0";
+		}
+
+		@GetMapping("test1")
+		public String test1() {
+			return "test1";
+		}
+
+		@GetMapping("test2")
+		public String test2() {
+			return "test2";
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/web/servlet/WebMvcMetricsAutoConfigurationTests.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/web/servlet/WebMvcMetricsAutoConfigurationTests.java
index 671ddbf51bb..0d5855d7158 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/web/servlet/WebMvcMetricsAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/web/servlet/WebMvcMetricsAutoConfigurationTests.java
@@ -20,59 +20,74 @@
 import java.util.EnumSet;
 
 import javax.servlet.DispatcherType;
+import javax.servlet.Filter;
 import javax.servlet.http.HttpServletRequest;
 import javax.servlet.http.HttpServletResponse;
 
 import io.micrometer.core.instrument.MeterRegistry;
 import io.micrometer.core.instrument.Tag;
-import io.micrometer.core.instrument.simple.SimpleMeterRegistry;
+import org.junit.Rule;
 import org.junit.Test;
 
+import org.springframework.boot.actuate.autoconfigure.metrics.test.MetricsRun;
 import org.springframework.boot.actuate.autoconfigure.metrics.web.servlet.WebMvcMetricsAutoConfiguration;
 import org.springframework.boot.actuate.metrics.web.servlet.DefaultWebMvcTagsProvider;
 import org.springframework.boot.actuate.metrics.web.servlet.WebMvcMetricsFilter;
 import org.springframework.boot.actuate.metrics.web.servlet.WebMvcTagsProvider;
 import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
 import org.springframework.boot.test.context.runner.WebApplicationContextRunner;
+import org.springframework.boot.test.rule.OutputCapture;
 import org.springframework.boot.web.servlet.FilterRegistrationBean;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.core.Ordered;
+import org.springframework.test.web.servlet.MockMvc;
+import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
+import org.springframework.test.web.servlet.setup.MockMvcBuilders;
+import org.springframework.web.bind.annotation.GetMapping;
+import org.springframework.web.bind.annotation.RestController;
 
 import static org.assertj.core.api.Assertions.assertThat;
+import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
 
 /**
  * Tests for {@link WebMvcMetricsAutoConfiguration}.
  *
  * @author Andy Wilkinson
+ * @author Dmytro Nosan
  */
 public class WebMvcMetricsAutoConfigurationTests {
 
 	private WebApplicationContextRunner contextRunner = new WebApplicationContextRunner()
-			.withConfiguration(
-					AutoConfigurations.of(WebMvcMetricsAutoConfiguration.class));
+			.with(MetricsRun.simple());
+
+	@Rule
+	public OutputCapture output = new OutputCapture();
 
 	@Test
 	public void backsOffWhenMeterRegistryIsMissing() {
-		this.contextRunner.run((context) -> assertThat(context)
-				.doesNotHaveBean(WebMvcMetricsAutoConfiguration.class));
+		new WebApplicationContextRunner()
+				.withConfiguration(
+						AutoConfigurations.of(WebMvcMetricsAutoConfiguration.class))
+				.run((context) -> assertThat(context)
+						.doesNotHaveBean(WebMvcMetricsAutoConfiguration.class));
 	}
 
 	@Test
 	public void definesTagsProviderAndFilterWhenMeterRegistryIsPresent() {
-		this.contextRunner.withUserConfiguration(MeterRegistryConfiguration.class)
-				.run((context) -> {
-					assertThat(context).hasSingleBean(DefaultWebMvcTagsProvider.class);
-					assertThat(context).hasSingleBean(FilterRegistrationBean.class);
-					assertThat(context.getBean(FilterRegistrationBean.class).getFilter())
-							.isInstanceOf(WebMvcMetricsFilter.class);
-				});
+		this.contextRunner.run((context) -> {
+			assertThat(context).hasSingleBean(DefaultWebMvcTagsProvider.class);
+			assertThat(context).hasSingleBean(FilterRegistrationBean.class);
+			assertThat(context.getBean(FilterRegistrationBean.class).getFilter())
+					.isInstanceOf(WebMvcMetricsFilter.class);
+		});
 	}
 
 	@Test
 	public void tagsProviderBacksOff() {
-		this.contextRunner.withUserConfiguration(MeterRegistryConfiguration.class,
-				TagsProviderConfiguration.class).run((context) -> {
+		this.contextRunner.withUserConfiguration(TagsProviderConfiguration.class)
+				.run((context) -> {
 					assertThat(context).doesNotHaveBean(DefaultWebMvcTagsProvider.class);
 					assertThat(context).hasSingleBean(TestWebMvcTagsProvider.class);
 				});
@@ -80,26 +95,42 @@ public void tagsProviderBacksOff() {
 
 	@Test
 	public void filterRegistrationHasExpectedDispatcherTypesAndOrder() {
-		this.contextRunner.withUserConfiguration(MeterRegistryConfiguration.class)
-				.run((context) -> {
-					FilterRegistrationBean<?> registration = context
-							.getBean(FilterRegistrationBean.class);
-					assertThat(registration).hasFieldOrPropertyWithValue(
-							"dispatcherTypes",
-							EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC));
-					assertThat(registration.getOrder())
-							.isEqualTo(Ordered.HIGHEST_PRECEDENCE + 1);
-				});
+		this.contextRunner.run((context) -> {
+			FilterRegistrationBean<?> registration = context
+					.getBean(FilterRegistrationBean.class);
+			assertThat(registration).hasFieldOrPropertyWithValue("dispatcherTypes",
+					EnumSet.of(DispatcherType.REQUEST, DispatcherType.ASYNC));
+			assertThat(registration.getOrder()).isEqualTo(Ordered.HIGHEST_PRECEDENCE + 1);
+		});
 	}
 
-	@Configuration
-	static class MeterRegistryConfiguration {
-
-		@Bean
-		public MeterRegistry meterRegistry() {
-			return new SimpleMeterRegistry();
-		}
+	@Test
+	public void afterMaxUrisReachedFurtherUrisAreDenied() {
+		this.contextRunner
+				.withConfiguration(AutoConfigurations.of(WebMvcAutoConfiguration.class))
+				.withUserConfiguration(TestController.class)
+				.withPropertyValues("management.metrics.web.server.max-uri-tags=2")
+				.run((context) -> {
 
+					assertThat(context).hasSingleBean(FilterRegistrationBean.class);
+					Filter filter = context.getBean(FilterRegistrationBean.class)
+							.getFilter();
+					assertThat(filter).isInstanceOf(WebMvcMetricsFilter.class);
+
+					MockMvc mockMvc = MockMvcBuilders.webAppContextSetup(context)
+							.addFilters(filter).build();
+
+					for (int i = 0; i < 3; i++) {
+						mockMvc.perform(MockMvcRequestBuilders.get("/test" + i))
+								.andExpect(status().isOk());
+					}
+
+					MeterRegistry registry = context.getBean(MeterRegistry.class);
+					assertThat(registry.get("http.server.requests").meters()).hasSize(2);
+					assertThat(this.output.toString())
+							.contains("Reached the maximum number of URI tags "
+									+ "for 'http.server.requests'");
+				});
 	}
 
 	@Configuration
@@ -128,4 +159,24 @@ public TestWebMvcTagsProvider tagsProvider() {
 
 	}
 
+	@RestController
+	static class TestController {
+
+		@GetMapping("test0")
+		public String test0() {
+			return "test0";
+		}
+
+		@GetMapping("test1")
+		public String test1() {
+			return "test1";
+		}
+
+		@GetMapping("test2")
+		public String test2() {
+			return "test2";
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc b/spring-boot-project/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
index 2ff43506242..8931f066720 100644
--- a/spring-boot-project/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
+++ b/spring-boot-project/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
@@ -1489,6 +1489,8 @@ content into your application. Rather, pick only the properties that you need.
 	management.metrics.web.client.requests-metric-name=http.client.requests # Name of the metric for sent requests.
 	management.metrics.web.server.auto-time-requests=true # Whether requests handled by Spring MVC or WebFlux should be automatically timed.
 	management.metrics.web.server.requests-metric-name=http.server.requests # Name of the metric for received requests.
+	management.metrics.web.server.max-uri-tags=100 # Maximum number of unique URI tag values allowed. After the max number of tag values is reached, metrics with additional tag values are denied by filter.
+
 
 
 	# ----------------------------------------
