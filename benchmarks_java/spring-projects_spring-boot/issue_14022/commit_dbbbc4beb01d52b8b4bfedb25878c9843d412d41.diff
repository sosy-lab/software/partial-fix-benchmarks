diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthEndpointWebExtensionConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthEndpointWebExtensionConfiguration.java
index 77c2602c8c3..2fbb1cebee2 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthEndpointWebExtensionConfiguration.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthEndpointWebExtensionConfiguration.java
@@ -16,6 +16,8 @@
 
 package org.springframework.boot.actuate.autoconfigure.health;
 
+import java.util.Map;
+
 import org.springframework.beans.factory.ObjectProvider;
 import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnEnabledEndpoint;
 import org.springframework.boot.actuate.autoconfigure.endpoint.condition.ConditionalOnExposedEndpoint;
@@ -51,10 +53,11 @@
 	@Bean
 	@ConditionalOnMissingBean
 	public HealthStatusHttpMapper createHealthStatusHttpMapper(
-			HealthIndicatorProperties healthIndicatorProperties) {
+			HealthIndicatorProperties properties) {
 		HealthStatusHttpMapper statusHttpMapper = new HealthStatusHttpMapper();
-		if (healthIndicatorProperties.getHttpMapping() != null) {
-			statusHttpMapper.addStatusMapping(healthIndicatorProperties.getHttpMapping());
+		Map<String, Integer> httpMapping = properties.getStatus().getHttpMapping();
+		if (httpMapping != null) {
+			statusHttpMapper.addStatusMapping(httpMapping);
 		}
 		return statusHttpMapper;
 	}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorAutoConfiguration.java
index 0ab5521bfda..500b0e10625 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorAutoConfiguration.java
@@ -16,15 +16,21 @@
 
 package org.springframework.boot.actuate.autoconfigure.health;
 
+import java.util.LinkedHashMap;
+import java.util.LinkedHashSet;
 import java.util.Map;
+import java.util.Set;
+import java.util.function.Function;
 
 import reactor.core.publisher.Flux;
 
 import org.springframework.boot.actuate.health.ApplicationHealthIndicator;
+import org.springframework.boot.actuate.health.GroupHealthIndicator;
 import org.springframework.boot.actuate.health.HealthAggregator;
 import org.springframework.boot.actuate.health.HealthIndicator;
 import org.springframework.boot.actuate.health.HealthIndicatorRegistry;
 import org.springframework.boot.actuate.health.OrderedHealthAggregator;
+import org.springframework.boot.actuate.health.ReactiveGroupHealthIndicator;
 import org.springframework.boot.actuate.health.ReactiveHealthIndicator;
 import org.springframework.boot.actuate.health.ReactiveHealthIndicatorRegistry;
 import org.springframework.boot.actuate.health.ReactiveHealthIndicatorRegistryFactory;
@@ -60,8 +66,8 @@ public ApplicationHealthIndicator applicationHealthIndicator() {
 	public OrderedHealthAggregator healthAggregator(
 			HealthIndicatorProperties properties) {
 		OrderedHealthAggregator healthAggregator = new OrderedHealthAggregator();
-		if (properties.getOrder() != null) {
-			healthAggregator.setStatusOrder(properties.getOrder());
+		if (properties.getStatus().getOrder() != null) {
+			healthAggregator.setStatusOrder(properties.getStatus().getOrder());
 		}
 		return healthAggregator;
 	}
@@ -69,8 +75,15 @@ public OrderedHealthAggregator healthAggregator(
 	@Bean
 	@ConditionalOnMissingBean(HealthIndicatorRegistry.class)
 	public HealthIndicatorRegistry healthIndicatorRegistry(
+			HealthIndicatorProperties properties, HealthAggregator healthAggregator,
 			ApplicationContext applicationContext) {
-		return HealthIndicatorRegistryBeans.get(applicationContext);
+		HealthIndicatorRegistry registry = HealthIndicatorRegistryBeans
+				.get(applicationContext);
+		extractGroups(properties, registry::get)
+				.forEach((groupName, groupHealthIndicators) -> registry
+						.register(groupName, new GroupHealthIndicator(healthAggregator,
+								groupHealthIndicators)));
+		return registry;
 	}
 
 	@Configuration(proxyBeanMethods = false)
@@ -80,13 +93,41 @@ public HealthIndicatorRegistry healthIndicatorRegistry(
 		@Bean
 		@ConditionalOnMissingBean
 		public ReactiveHealthIndicatorRegistry reactiveHealthIndicatorRegistry(
+				HealthIndicatorProperties properties, HealthAggregator healthAggregator,
 				Map<String, ReactiveHealthIndicator> reactiveHealthIndicators,
 				Map<String, HealthIndicator> healthIndicators) {
-			return new ReactiveHealthIndicatorRegistryFactory()
+			ReactiveHealthIndicatorRegistry registry = new ReactiveHealthIndicatorRegistryFactory()
 					.createReactiveHealthIndicatorRegistry(reactiveHealthIndicators,
 							healthIndicators);
+			extractGroups(properties, registry::get).forEach(
+					(groupName, groupHealthIndicators) -> registry.register(groupName,
+							new ReactiveGroupHealthIndicator(healthAggregator,
+									groupHealthIndicators)));
+			return registry;
 		}
 
 	}
 
+	private static <T> Map<String, Set<String>> extractGroups(
+			HealthIndicatorProperties properties,
+			Function<String, T> healthIndicatorByName) {
+		Map<String, Set<String>> groupDefinitions = new LinkedHashMap<>();
+		properties.getGroups().forEach((groupName, indicatorNames) -> {
+			if (healthIndicatorByName.apply(groupName) != null) {
+				throw new IllegalArgumentException(
+						"Could not register health indicator group named '" + groupName
+								+ "', an health indicator with that name is already registered");
+			}
+			Set<String> groupHealthIndicators = new LinkedHashSet<>();
+			indicatorNames.forEach((name) -> {
+				T healthIndicator = healthIndicatorByName.apply(name);
+				if (healthIndicator != null) {
+					groupHealthIndicators.add(name);
+				}
+			});
+			groupDefinitions.put(groupName, groupHealthIndicators);
+		});
+		return groupDefinitions;
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorProperties.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorProperties.java
index 0f4d4854592..3c1bc53d52d 100644
--- a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorProperties.java
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/health/HealthIndicatorProperties.java
@@ -28,32 +28,52 @@
  * @author Christian Dupuis
  * @since 2.0.0
  */
-@ConfigurationProperties(prefix = "management.health.status")
+@ConfigurationProperties(prefix = "management.health")
 public class HealthIndicatorProperties {
 
 	/**
-	 * Comma-separated list of health statuses in order of severity.
+	 * Health indicator groups. Each entry maps the name of a group with a list of health
+	 * indicators to associate with the group.
 	 */
-	private List<String> order = null;
+	private final Map<String, List<String>> groups = new HashMap<>();
 
-	/**
-	 * Mapping of health statuses to HTTP status codes. By default, registered health
-	 * statuses map to sensible defaults (for example, UP maps to 200).
-	 */
-	private final Map<String, Integer> httpMapping = new HashMap<>();
+	private final Status status = new Status();
 
-	public List<String> getOrder() {
-		return this.order;
+	public Map<String, List<String>> getGroups() {
+		return this.groups;
 	}
 
-	public void setOrder(List<String> statusOrder) {
-		if (statusOrder != null && !statusOrder.isEmpty()) {
-			this.order = statusOrder;
-		}
+	public Status getStatus() {
+		return this.status;
 	}
 
-	public Map<String, Integer> getHttpMapping() {
-		return this.httpMapping;
+	public static class Status {
+
+		/**
+		 * Comma-separated list of health statuses in order of severity.
+		 */
+		private List<String> order = null;
+
+		/**
+		 * Mapping of health statuses to HTTP status codes. By default, registered health
+		 * statuses map to sensible defaults (for example, UP maps to 200).
+		 */
+		private final Map<String, Integer> httpMapping = new HashMap<>();
+
+		public List<String> getOrder() {
+			return this.order;
+		}
+
+		public void setOrder(List<String> statusOrder) {
+			if (statusOrder != null && !statusOrder.isEmpty()) {
+				this.order = statusOrder;
+			}
+		}
+
+		public Map<String, Integer> getHttpMapping() {
+			return this.httpMapping;
+		}
+
 	}
 
 }
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/AggregatedHealth.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/AggregatedHealth.java
new file mode 100644
index 00000000000..83e95642bbf
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/AggregatedHealth.java
@@ -0,0 +1,54 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import java.util.HashMap;
+import java.util.Map;
+
+/**
+ * @author Stephane Nicoll
+ */
+public class AggregatedHealth {
+
+	private static final Health UNKNOWN_HEALTH = Health.unknown().build();
+
+	private final HealthIndicatorRegistry registry;
+
+	private final Map<String, Health> healths;
+
+	public AggregatedHealth(HealthIndicatorRegistry registry) {
+		this.registry = registry;
+		this.healths = new HashMap<>();
+	}
+
+	public Health health(String name) {
+		Health health = this.healths.computeIfAbsent(name, this::determineHealth);
+		return (health != UNKNOWN_HEALTH) ? health : null;
+	}
+
+	private Health determineHealth(String name) {
+		HealthIndicator healthIndicator = this.registry.get(name);
+		if (healthIndicator == null) {
+			return UNKNOWN_HEALTH;
+		}
+		if (healthIndicator instanceof AggregatedHealthIndicator) {
+			return ((AggregatedHealthIndicator) healthIndicator).health(this);
+		}
+		return healthIndicator.health();
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/AggregatedHealthIndicator.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/AggregatedHealthIndicator.java
new file mode 100644
index 00000000000..d8f69a44a0b
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/AggregatedHealthIndicator.java
@@ -0,0 +1,32 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+/**
+ * @author Stephane Nicoll
+ */
+@FunctionalInterface
+public interface AggregatedHealthIndicator extends HealthIndicator {
+
+	@Override
+	default Health health() {
+		return health(new AggregatedHealth(new DefaultHealthIndicatorRegistry()));
+	}
+
+	Health health(AggregatedHealth aggregatedHealth);
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/GroupHealthIndicator.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/GroupHealthIndicator.java
new file mode 100644
index 00000000000..f4d4025061b
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/GroupHealthIndicator.java
@@ -0,0 +1,49 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import java.util.LinkedHashMap;
+import java.util.Map;
+import java.util.Set;
+
+/**
+ * @author Stephane Nicoll
+ */
+public class GroupHealthIndicator implements AggregatedHealthIndicator {
+
+	private final HealthAggregator aggregator;
+
+	private final Set<String> indicatorNames;
+
+	public GroupHealthIndicator(HealthAggregator aggregator, Set<String> indicatorNames) {
+		this.aggregator = aggregator;
+		this.indicatorNames = indicatorNames;
+	}
+
+	@Override
+	public Health health(AggregatedHealth aggregatedHealth) {
+		Map<String, Health> healths = new LinkedHashMap<>();
+		for (String indicatorName : this.indicatorNames) {
+			Health health = aggregatedHealth.health(indicatorName);
+			if (health != null) {
+				healths.put(indicatorName, health);
+			}
+		}
+		return this.aggregator.aggregate(healths);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveAggregatedHealth.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveAggregatedHealth.java
new file mode 100644
index 00000000000..b8171c5ddbe
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveAggregatedHealth.java
@@ -0,0 +1,56 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import java.util.HashMap;
+import java.util.Map;
+
+import reactor.core.publisher.Mono;
+
+/**
+ * @author Stephane Nicoll
+ */
+public class ReactiveAggregatedHealth {
+
+	private static final Mono<Health> UNKNOWN_HEALTH = Mono.empty();
+
+	private final ReactiveHealthIndicatorRegistry registry;
+
+	private final Map<String, Mono<Health>> healths;
+
+	public ReactiveAggregatedHealth(ReactiveHealthIndicatorRegistry registry) {
+		this.registry = registry;
+		this.healths = new HashMap<>();
+	}
+
+	public Mono<Health> health(String name) {
+		return this.healths.computeIfAbsent(name,
+				(indicator) -> determineHealth(name).cache());
+	}
+
+	private Mono<Health> determineHealth(String name) {
+		ReactiveHealthIndicator healthIndicator = this.registry.get(name);
+		if (healthIndicator == null) {
+			return UNKNOWN_HEALTH;
+		}
+		if (healthIndicator instanceof ReactiveAggregatedHealthIndicator) {
+			return ((ReactiveAggregatedHealthIndicator) healthIndicator).health(this);
+		}
+		return healthIndicator.health();
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveAggregatedHealthIndicator.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveAggregatedHealthIndicator.java
new file mode 100644
index 00000000000..7107aa4be74
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveAggregatedHealthIndicator.java
@@ -0,0 +1,35 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import reactor.core.publisher.Mono;
+
+/**
+ * @author Stephane Nicoll
+ */
+@FunctionalInterface
+public interface ReactiveAggregatedHealthIndicator extends ReactiveHealthIndicator {
+
+	@Override
+	default Mono<Health> health() {
+		return health(new ReactiveAggregatedHealth(
+				new DefaultReactiveHealthIndicatorRegistry()));
+	}
+
+	Mono<Health> health(ReactiveAggregatedHealth aggregatedHealth);
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveGroupHealthIndicator.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveGroupHealthIndicator.java
new file mode 100644
index 00000000000..7be7a4288ef
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/health/ReactiveGroupHealthIndicator.java
@@ -0,0 +1,48 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import java.util.Set;
+
+import reactor.core.publisher.Flux;
+import reactor.core.publisher.Mono;
+import reactor.util.function.Tuple2;
+
+/**
+ * @author Stephane Nicoll
+ */
+public class ReactiveGroupHealthIndicator implements ReactiveAggregatedHealthIndicator {
+
+	private final HealthAggregator healthAggregator;
+
+	private final Set<String> indicatorNames;
+
+	public ReactiveGroupHealthIndicator(HealthAggregator healthAggregator,
+			Set<String> indicatorNames) {
+		this.healthAggregator = healthAggregator;
+		this.indicatorNames = indicatorNames;
+	}
+
+	@Override
+	public Mono<Health> health(ReactiveAggregatedHealth aggregatedHealth) {
+		return Flux.fromIterable(this.indicatorNames).flatMap(
+				(name) -> Mono.zip(Mono.just(name), aggregatedHealth.health(name)))
+				.collectMap(Tuple2::getT1, Tuple2::getT2)
+				.map(this.healthAggregator::aggregate);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/health/AggregatedHealthTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/health/AggregatedHealthTests.java
new file mode 100644
index 00000000000..538d935e86a
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/health/AggregatedHealthTests.java
@@ -0,0 +1,95 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import java.util.Collections;
+
+import org.junit.Before;
+import org.junit.Test;
+import org.mockito.Mock;
+import org.mockito.MockitoAnnotations;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.mockito.BDDMockito.given;
+import static org.mockito.Mockito.mock;
+import static org.mockito.Mockito.times;
+import static org.mockito.Mockito.verify;
+
+/**
+ * Tests for {@link AggregatedHealth}.
+ *
+ * @author Stephane Nicoll
+ */
+public class AggregatedHealthTests {
+
+	@Mock
+	private HealthIndicator one;
+
+	private HealthIndicatorRegistry registry;
+
+	@Before
+	public void setup() {
+		MockitoAnnotations.initMocks(this);
+		given(this.one.health())
+				.willReturn(new Health.Builder().up().withDetail("1", "1").build());
+		this.registry = new DefaultHealthIndicatorRegistry(
+				Collections.singletonMap("one", this.one));
+	}
+
+	@Test
+	public void healthForKnownIndicator() {
+		AggregatedHealth aggregatedHealth = new AggregatedHealth(this.registry);
+		assertThat(aggregatedHealth.health("one"))
+				.isEqualTo(new Health.Builder().up().withDetail("1", "1").build());
+	}
+
+	@Test
+	public void healthForKnownIndicatorInvokesTargetHealthIndicatorOnce() {
+		AggregatedHealth aggregatedHealth = new AggregatedHealth(this.registry);
+		Health first = aggregatedHealth.health("one");
+		Health second = aggregatedHealth.health("one");
+		assertThat(first).isSameAs(second);
+		verify(this.one, times(1)).health();
+	}
+
+	@Test
+	public void healthForKnownAggregatedHealthIndicatorUsesAggregatedHealth() {
+		AggregatedHealth aggregatedHealth = new AggregatedHealth(this.registry);
+		AggregatedHealthIndicator two = mock(AggregatedHealthIndicator.class);
+		given(two.health(aggregatedHealth))
+				.willReturn(new Health.Builder().down().withDetail("2", "2").build());
+		this.registry.register("two", two);
+		assertThat(aggregatedHealth.health("two"))
+				.isEqualTo(new Health.Builder().down().withDetail("2", "2").build());
+		verify(two, times(1)).health(aggregatedHealth);
+	}
+
+	@Test
+	public void healthForUnknownIndicator() {
+		AggregatedHealth aggregatedHealth = new AggregatedHealth(this.registry);
+		assertThat(aggregatedHealth.health("unknown")).isNull();
+	}
+
+	@Test
+	public void healthForUnknownIndicatorIsCached() {
+		AggregatedHealth aggregatedHealth = new AggregatedHealth(this.registry);
+		assertThat(aggregatedHealth.health("unknown")).isNull();
+		this.registry.register("unknown", () -> Health.unknown().build());
+		assertThat(aggregatedHealth.health("unknown")).isNull();
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/health/GroupHealthIndicatorTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/health/GroupHealthIndicatorTests.java
new file mode 100644
index 00000000000..7e2fc98e081
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/health/GroupHealthIndicatorTests.java
@@ -0,0 +1,28 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.health;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ *
+ * @author Stephane Nicoll
+ */
+public class GroupHealthIndicatorTests {
+
+
+}
