diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/SharedMetadataReaderFactoryContextInitializer.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/SharedMetadataReaderFactoryContextInitializer.java
index d42431e9b16..2da74b5fabd 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/SharedMetadataReaderFactoryContextInitializer.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/SharedMetadataReaderFactoryContextInitializer.java
@@ -23,9 +23,9 @@
 import org.springframework.beans.factory.config.BeanDefinition;
 import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
 import org.springframework.beans.factory.config.RuntimeBeanReference;
+import org.springframework.beans.factory.support.BeanDefinitionBuilder;
 import org.springframework.beans.factory.support.BeanDefinitionRegistry;
 import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
-import org.springframework.beans.factory.support.RootBeanDefinition;
 import org.springframework.boot.type.classreading.ConcurrentReferenceCachingMetadataReaderFactory;
 import org.springframework.context.ApplicationContextInitializer;
 import org.springframework.context.ApplicationListener;
@@ -85,8 +85,10 @@ public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry)
 		}
 
 		private void register(BeanDefinitionRegistry registry) {
-			RootBeanDefinition definition = new RootBeanDefinition(
-					SharedMetadataReaderFactoryBean.class);
+			BeanDefinition definition = BeanDefinitionBuilder
+					.genericBeanDefinition(SharedMetadataReaderFactoryBean.class,
+							SharedMetadataReaderFactoryBean::new)
+					.getBeanDefinition();
 			registry.registerBeanDefinition(BEAN_NAME, definition);
 		}
 
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/condition/BeanTypeRegistry.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/condition/BeanTypeRegistry.java
index 694e926d25a..f032be8b59d 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/condition/BeanTypeRegistry.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/condition/BeanTypeRegistry.java
@@ -38,6 +38,7 @@
 import org.springframework.beans.factory.config.BeanDefinition;
 import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
 import org.springframework.beans.factory.support.AbstractBeanDefinition;
+import org.springframework.beans.factory.support.BeanDefinitionBuilder;
 import org.springframework.beans.factory.support.DefaultListableBeanFactory;
 import org.springframework.beans.factory.support.RootBeanDefinition;
 import org.springframework.core.ResolvableType;
@@ -93,8 +94,11 @@ static BeanTypeRegistry get(ListableBeanFactory beanFactory) {
 		Assert.isTrue(listableBeanFactory.isAllowEagerClassLoading(),
 				"Bean factory must allow eager class loading");
 		if (!listableBeanFactory.containsLocalBean(BEAN_NAME)) {
-			BeanDefinition bd = new RootBeanDefinition(BeanTypeRegistry.class);
-			bd.getConstructorArgumentValues().addIndexedArgumentValue(0, beanFactory);
+			BeanDefinition bd = BeanDefinitionBuilder
+					.genericBeanDefinition(BeanTypeRegistry.class,
+							() -> new BeanTypeRegistry(
+									(DefaultListableBeanFactory) beanFactory))
+					.getBeanDefinition();
 			listableBeanFactory.registerBeanDefinition(BEAN_NAME, bd);
 
 		}
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/webservices/WebServicesAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/webservices/WebServicesAutoConfiguration.java
index 6bbae47a7cd..2541b4b3f0b 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/webservices/WebServicesAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/webservices/WebServicesAutoConfiguration.java
@@ -19,13 +19,14 @@
 import java.io.IOException;
 import java.util.Collections;
 import java.util.List;
+import java.util.function.Function;
 
 import org.springframework.beans.BeansException;
+import org.springframework.beans.factory.config.BeanDefinition;
 import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
-import org.springframework.beans.factory.config.ConstructorArgumentValues;
+import org.springframework.beans.factory.support.BeanDefinitionBuilder;
 import org.springframework.beans.factory.support.BeanDefinitionRegistry;
 import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
-import org.springframework.beans.factory.support.RootBeanDefinition;
 import org.springframework.boot.autoconfigure.AutoConfigureAfter;
 import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
@@ -118,8 +119,9 @@ public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry)
 					.orElse(Collections.emptyList());
 			for (String wsdlLocation : wsdlLocations) {
 				registerBeans(wsdlLocation, "*.wsdl", SimpleWsdl11Definition.class,
-						registry);
-				registerBeans(wsdlLocation, "*.xsd", SimpleXsdSchema.class, registry);
+						SimpleWsdl11Definition::new, registry);
+				registerBeans(wsdlLocation, "*.xsd", SimpleXsdSchema.class,
+						SimpleXsdSchema::new, registry);
 			}
 		}
 
@@ -128,13 +130,12 @@ public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory)
 				throws BeansException {
 		}
 
-		private void registerBeans(String location, String pattern, Class<?> type,
-				BeanDefinitionRegistry registry) {
+		private <T> void registerBeans(String location, String pattern, Class<T> type,
+				Function<Resource, T> beanSupplier, BeanDefinitionRegistry registry) {
 			for (Resource resource : getResources(location, pattern)) {
-				RootBeanDefinition beanDefinition = new RootBeanDefinition(type);
-				ConstructorArgumentValues constructorArguments = new ConstructorArgumentValues();
-				constructorArguments.addIndexedArgumentValue(0, resource);
-				beanDefinition.setConstructorArgumentValues(constructorArguments);
+				BeanDefinition beanDefinition = BeanDefinitionBuilder
+						.genericBeanDefinition(type, () -> beanSupplier.apply(resource))
+						.getBeanDefinition();
 				registry.registerBeanDefinition(
 						StringUtils.stripFilenameExtension(resource.getFilename()),
 						beanDefinition);
