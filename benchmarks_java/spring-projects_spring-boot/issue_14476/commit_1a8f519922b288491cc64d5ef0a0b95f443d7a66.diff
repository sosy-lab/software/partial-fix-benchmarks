diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfiguration.java
index 5be48fdc6c6..be84094a0ec 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfiguration.java
@@ -35,6 +35,7 @@
 import de.flapdoodle.embed.mongo.config.Storage;
 import de.flapdoodle.embed.mongo.distribution.Feature;
 import de.flapdoodle.embed.mongo.distribution.IFeatureAwareVersion;
+import de.flapdoodle.embed.mongo.distribution.Version;
 import de.flapdoodle.embed.mongo.distribution.Versions;
 import de.flapdoodle.embed.process.config.IRuntimeConfig;
 import de.flapdoodle.embed.process.config.io.ProcessOutput;
@@ -125,11 +126,8 @@ private MongodStarter getMongodStarter(IRuntimeConfig runtimeConfig) {
 	@Bean
 	@ConditionalOnMissingBean
 	public IMongodConfig embeddedMongoConfiguration() throws IOException {
-		IFeatureAwareVersion featureAwareVersion = Versions.withFeatures(
-				new GenericVersion(this.embeddedProperties.getVersion()),
-				this.embeddedProperties.getFeatures().toArray(new Feature[0]));
 		MongodConfigBuilder builder = new MongodConfigBuilder()
-				.version(featureAwareVersion);
+				.version(determineVersion());
 		EmbeddedMongoProperties.Storage storage = this.embeddedProperties.getStorage();
 		if (storage != null) {
 			String databaseDir = storage.getDatabaseDir();
@@ -150,6 +148,20 @@ public IMongodConfig embeddedMongoConfiguration() throws IOException {
 		return builder.build();
 	}
 
+	private IFeatureAwareVersion determineVersion() {
+		if (this.embeddedProperties.getFeatures() == null) {
+			for (Version version : Version.values()) {
+				if (version.asInDownloadPath()
+						.equals(this.embeddedProperties.getVersion())) {
+					return version;
+				}
+			}
+		}
+		return Versions.withFeatures(
+				new GenericVersion(this.embeddedProperties.getVersion()),
+				this.embeddedProperties.getFeatures().toArray(new Feature[0]));
+	}
+
 	private InetAddress getHost() throws UnknownHostException {
 		if (this.properties.getHost() == null) {
 			return InetAddress.getByAddress(Network.localhostIsIPv6()
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoProperties.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoProperties.java
index 96a450db305..bc40d48ef9b 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoProperties.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoProperties.java
@@ -16,8 +16,6 @@
 
 package org.springframework.boot.autoconfigure.mongo.embedded;
 
-import java.util.Collections;
-import java.util.HashSet;
 import java.util.Set;
 
 import de.flapdoodle.embed.mongo.distribution.Feature;
@@ -40,15 +38,15 @@
 	/**
 	 * Version of Mongo to use.
 	 */
-	private String version = "3.2.2";
+	private String version = "3.6.5";
 
 	private final Storage storage = new Storage();
 
 	/**
-	 * Comma-separated list of features to enable.
+	 * Comma-separated list of features to enable. Uses the defaults of the configured
+	 * version by default.
 	 */
-	private Set<Feature> features = new HashSet<>(
-			Collections.singletonList(Feature.SYNC_DELAY));
+	private Set<Feature> features = null;
 
 	public String getVersion() {
 		return this.version;
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfigurationTests.java
index 99248a4022a..36354ebd4c3 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/mongo/embedded/EmbeddedMongoAutoConfigurationTests.java
@@ -60,19 +60,20 @@ public void close() {
 
 	@Test
 	public void defaultVersion() {
-		assertVersionConfiguration(null, "3.2.2");
+		assertVersionConfiguration(null, "3.6.5");
 	}
 
 	@Test
 	public void customVersion() {
-		assertVersionConfiguration("2.7.1", "2.7.1");
+		assertVersionConfiguration("3.6.3", "3.6.3");
 	}
 
 	@Test
 	public void customFeatures() {
-		load("spring.mongodb.embedded.features=TEXT_SEARCH, SYNC_DELAY");
+		load("spring.mongodb.embedded.features=TEXT_SEARCH, SYNC_DELAY, ONLY_WITH_SSL, NO_HTTP_INTERFACE_ARG");
 		assertThat(this.context.getBean(EmbeddedMongoProperties.class).getFeatures())
-				.contains(Feature.TEXT_SEARCH, Feature.SYNC_DELAY);
+				.containsExactly(Feature.TEXT_SEARCH, Feature.SYNC_DELAY,
+						Feature.ONLY_WITH_SSL, Feature.NO_HTTP_INTERFACE_ARG);
 	}
 
 	@Test
