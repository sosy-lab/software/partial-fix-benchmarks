diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfiguration.java
index 9e93303be9c..c9915953fd3 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfiguration.java
@@ -31,6 +31,7 @@
 import org.flywaydb.core.api.MigrationVersion;
 import org.flywaydb.core.api.callback.Callback;
 import org.flywaydb.core.api.configuration.FluentConfiguration;
+import org.flywaydb.core.api.migration.JavaMigration;
 
 import org.springframework.beans.factory.ObjectProvider;
 import org.springframework.boot.autoconfigure.AutoConfigureAfter;
@@ -113,7 +114,7 @@ public Flyway flyway(FlywayProperties properties, DataSourceProperties dataSourc
 				ResourceLoader resourceLoader, ObjectProvider<DataSource> dataSource,
 				@FlywayDataSource ObjectProvider<DataSource> flywayDataSource,
 				ObjectProvider<FlywayConfigurationCustomizer> fluentConfigurationCustomizers,
-				ObjectProvider<Callback> callbacks) {
+				ObjectProvider<JavaMigration> javaMigrations, ObjectProvider<Callback> callbacks) {
 			FluentConfiguration configuration = new FluentConfiguration(resourceLoader.getClassLoader());
 			DataSource dataSourceToMigrate = configureDataSource(configuration, properties, dataSourceProperties,
 					flywayDataSource.getIfAvailable(), dataSource.getIfAvailable());
@@ -123,6 +124,8 @@ public Flyway flyway(FlywayProperties properties, DataSourceProperties dataSourc
 			configureCallbacks(configuration, orderedCallbacks);
 			fluentConfigurationCustomizers.orderedStream().forEach((customizer) -> customizer.customize(configuration));
 			configureFlywayCallbacks(configuration, orderedCallbacks);
+			JavaMigration[] migrations = javaMigrations.stream().toArray(JavaMigration[]::new);
+			configuration.javaMigrations(migrations);
 			return configuration.load();
 		}
 
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfigurationTests.java
index fdb8ed92b59..97b2f9dd4fd 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/flyway/FlywayAutoConfigurationTests.java
@@ -29,6 +29,7 @@
 import org.flywaydb.core.api.callback.Callback;
 import org.flywaydb.core.api.callback.Context;
 import org.flywaydb.core.api.callback.Event;
+import org.flywaydb.core.api.migration.JavaMigration;
 import org.flywaydb.core.internal.license.FlywayProUpgradeRequiredException;
 import org.hibernate.engine.transaction.jta.platform.internal.NoJtaPlatform;
 import org.junit.jupiter.api.Test;
@@ -265,6 +266,16 @@ void customFlywayMigrationStrategy() {
 				});
 	}
 
+	@Test
+	void flywayJavaMigrations() {
+		this.contextRunner
+				.withUserConfiguration(EmbeddedDataSourceConfiguration.class, FlywayJavaMigrationsConfiguration.class)
+				.run((context) -> {
+					Flyway flyway = context.getBean(Flyway.class);
+					assertThat(flyway.getConfiguration().getJavaMigrations().length).isEqualTo(2);
+				});
+	}
+
 	@Test
 	void customFlywayMigrationInitializer() {
 		this.contextRunner
@@ -461,6 +472,81 @@ DataSource flywayDataSource() {
 
 	}
 
+	@Configuration
+	protected static class FlywayJavaMigrationsConfiguration {
+
+		@Component
+		private static class Migration1 implements JavaMigration {
+
+			@Override
+			public MigrationVersion getVersion() {
+				return MigrationVersion.fromVersion("2");
+			}
+
+			@Override
+			public String getDescription() {
+				return "M1";
+			}
+
+			@Override
+			public Integer getChecksum() {
+				return 1;
+			}
+
+			@Override
+			public boolean isUndo() {
+				return false;
+			}
+
+			@Override
+			public boolean canExecuteInTransaction() {
+				return true;
+			}
+
+			@Override
+			public void migrate(org.flywaydb.core.api.migration.Context context) throws Exception {
+
+			}
+
+		}
+
+		@Component
+		private static class Migration2 implements JavaMigration {
+
+			@Override
+			public MigrationVersion getVersion() {
+				return MigrationVersion.fromVersion("3");
+			}
+
+			@Override
+			public String getDescription() {
+				return "M2";
+			}
+
+			@Override
+			public Integer getChecksum() {
+				return 2;
+			}
+
+			@Override
+			public boolean isUndo() {
+				return false;
+			}
+
+			@Override
+			public boolean canExecuteInTransaction() {
+				return false;
+			}
+
+			@Override
+			public void migrate(org.flywaydb.core.api.migration.Context context) throws Exception {
+
+			}
+
+		}
+
+	}
+
 	@Configuration(proxyBeanMethods = false)
 	static class ResourceLoaderConfiguration {
 
