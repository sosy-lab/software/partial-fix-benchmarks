diff --git a/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunner.java b/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunner.java
index a07ace33875..5c193b663ea 100644
--- a/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunner.java
+++ b/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunner.java
@@ -32,6 +32,7 @@
 import org.springframework.context.ApplicationContextInitializer;
 import org.springframework.context.ConfigurableApplicationContext;
 import org.springframework.context.annotation.AnnotationConfigRegistry;
+import org.springframework.context.support.GenericApplicationContext;
 import org.springframework.core.ResolvableType;
 import org.springframework.core.env.Environment;
 import org.springframework.core.io.DefaultResourceLoader;
@@ -221,6 +222,47 @@ public SELF withParent(ApplicationContext parent) {
 				parent, this.configurations);
 	}
 
+	/**
+	 * Register the specified user bean with the {@link ApplicationContext}. The bean name
+	 * is inferred from the {@link Class#getName() name of the type}.
+	 * <p>
+	 * Such beans are registered after regular {@link #withUserConfiguration(Class[]) user
+	 * configurations} in the order of registration.
+	 * @param beanType the type of the bean
+	 * @param beanDefinition a supplier for the bean
+	 * @param <T> the type of the bean
+	 * @return a new instance with the updated bean
+	 */
+	public <T> SELF withBean(Class<T> beanType, Supplier<T> beanDefinition) {
+		return withBean(null, beanType, beanDefinition);
+	}
+
+	/**
+	 * Register the specified user bean with the {@link ApplicationContext}.
+	 * <p>
+	 * Such beans are registered after regular {@link #withUserConfiguration(Class[]) user
+	 * configurations} in the order of registration.
+	 * @param beanName the name of the bean
+	 * @param beanType the type of the bean
+	 * @param beanDefinition a supplier for the bean
+	 * @param <T> the type of the bean
+	 * @return a new instance with the updated bean
+	 */
+	public <T> SELF withBean(String beanName, Class<T> beanType,
+			Supplier<T> beanDefinition) {
+		return withInitializer(
+				beanDefinitionRegistrar(beanName, beanType, beanDefinition));
+	}
+
+	private <T> ApplicationContextInitializer<? super ConfigurableApplicationContext> beanDefinitionRegistrar(
+			String beanName, Class<T> beanType, Supplier<T> beanDefinition) {
+		return (context) -> {
+			Assert.isInstanceOf(GenericApplicationContext.class, context);
+			((GenericApplicationContext) context).registerBean(beanName, beanType,
+					beanDefinition);
+		};
+	}
+
 	/**
 	 * Register the specified user configuration classes with the
 	 * {@link ApplicationContext}.
diff --git a/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunnerTests.java b/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunnerTests.java
index b6391d265c3..13e0add7d1d 100644
--- a/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunnerTests.java
+++ b/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/runner/AbstractApplicationContextRunnerTests.java
@@ -136,6 +136,36 @@ public void runWithConfigurationsShouldRegisterConfigurations() {
 				.run((context) -> assertThat(context).hasBean("foo"));
 	}
 
+	@Test
+	public void runWithUserNamedBeanShouldRegisterBean() {
+		get().withBean("foo", String.class, () -> "foo")
+				.run((context) -> assertThat(context).hasBean("foo"));
+	}
+
+	@Test
+	public void runWithUserBeanShouldRegisterBeanWithDefaultName() {
+		get().withBean(String.class, () -> "foo")
+				.run((context) -> assertThat(context).hasBean("string"));
+	}
+
+	@Test
+	public void runWithUserBeanShouldBeRegisteredInOrder() {
+		get().withBean(String.class, () -> "one").withBean(String.class, () -> "two")
+				.withBean(String.class, () -> "three").run((context) -> {
+					assertThat(context).hasBean("string");
+					assertThat(context.getBean("string")).isEqualTo("three");
+				});
+	}
+
+	@Test
+	public void runWithConfigurationsAndUserBeanShouldRegisterUserBeanLast() {
+		get().withUserConfiguration(FooConfig.class)
+				.withBean("foo", String.class, () -> "overridden").run((context) -> {
+					assertThat(context).hasBean("foo");
+					assertThat(context.getBean("foo")).isEqualTo("overridden");
+				});
+	}
+
 	@Test
 	public void runWithMultipleConfigurationsShouldRegisterAllConfigurations() {
 		get().withUserConfiguration(FooConfig.class)
