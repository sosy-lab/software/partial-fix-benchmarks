diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java
index abf721cd97b..04e164ed532 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java
@@ -18,6 +18,7 @@
 
 import org.springframework.core.io.ClassPathResource;
 import org.springframework.util.ClassUtils;
+import org.springframework.util.StringUtils;
 
 /**
  * Abstract base class for {@link LoggingSystem} implementations.
@@ -31,12 +32,23 @@
 
 	private final String[] paths;
 
-	public AbstractLoggingSystem(ClassLoader classLoader, boolean fileOutput, boolean consoleOutput) {
-		this.classLoader = classLoader;
-		this.paths = getLogFileName(fileOutput, consoleOutput);
+	private boolean fileOutput;
+
+	private boolean consoleOutput;
+
+	public AbstractLoggingSystem(ClassLoader classLoader) {
+		this(classLoader, false, true);
 	}
 	
-	protected abstract String[] getLogFileName(boolean fileOutput, boolean consoleOutput);
+	public AbstractLoggingSystem(ClassLoader classLoader, boolean fileOutput,
+			boolean consoleOutput) {
+		this.classLoader = classLoader;
+		this.fileOutput = fileOutput;
+		this.consoleOutput = consoleOutput;
+		this.paths = getLogFileNames();
+	}
+
+	protected abstract String[] getLogFileNames();
 
 	protected final ClassLoader getClassLoader() {
 		return this.classLoader;
@@ -56,14 +68,12 @@ public void initialize() {
 				return;
 			}
 		}
-		// Fallback to the non-prefixed value
-		initialize(getPackagedConfigFile(this.paths[this.paths.length - 1]));
+		// Fallback to the non-prefixed value taking into account file and console preferences
+		initialize(getPackagedConfigFile(addChannels(this.paths[this.paths.length - 1])));
 	}
 
 	protected void initializeWithSensibleDefaults() {
-		String path = this.paths[this.paths.length - 1];
-		path = path.replaceAll("-console", "").replaceAll("-file", "");
-		initialize(getPackagedConfigFile("basic-" + path));
+		initialize(getPackagedConfigFile("basic-" + this.paths[this.paths.length - 1]));
 	}
 
 	protected final String getPackagedConfigFile(String fileName) {
@@ -74,4 +84,14 @@ protected final String getPackagedConfigFile(String fileName) {
 		return defaultPath;
 	}
 
+	private String addChannels(String fileName) {
+		String extension = "." + StringUtils.getFilenameExtension(fileName);
+		return fileName.replace(extension, getChannel() + extension);
+	}
+
+	private String getChannel() {
+		return (fileOutput && consoleOutput) ? "-file-console" : (fileOutput ? "-file"
+				: (consoleOutput ? "" : "-none"));
+	}
+
 }
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java
index a43ed55f8a1..edb36db470b 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java
@@ -51,27 +51,19 @@
 		LEVELS = Collections.unmodifiableMap(levels);
 	}
 
-	public JavaLoggingSystem(ClassLoader classLoader, boolean fileOutput, boolean consoleOutput) {
-		super(classLoader, fileOutput, consoleOutput);
+	public JavaLoggingSystem(ClassLoader classLoader) {
+		this(classLoader, false, true);
 	}
 	
-	@Override
-	protected String[] getLogFileName(boolean fileOutput, boolean consoleOutput) {
-		if (fileOutput && consoleOutput) {
-			return new String[] { "logging-file-console.properties" };
-		}
-		else if (fileOutput) {
-			return new String[] { "logging-file.properties" };
-		}
-		else if (consoleOutput) {
-			return new String[] { "logging-console.properties" };
-		}
-		else {
-			return new String[] { "logging.properties" };
-		}
+	public JavaLoggingSystem(ClassLoader classLoader, boolean fileOutput,
+			boolean consoleOutput) {
+		super(classLoader, fileOutput, consoleOutput);
 	}
 
-
+	@Override
+	protected String[] getLogFileNames() {
+		return new String[] { "logging.properties" };
+	}
 
 	@Override
 	public void initialize(String configLocation) {
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/log4j/Log4JLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/log4j/Log4JLoggingSystem.java
index ed57f47df9c..43a76ed8204 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/log4j/Log4JLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/log4j/Log4JLoggingSystem.java
@@ -51,24 +51,18 @@
 		LEVELS = Collections.unmodifiableMap(levels);
 	}
 
-	public Log4JLoggingSystem(ClassLoader classLoader, boolean fileOutput, boolean consoleOutput) {
-		super(classLoader, fileOutput, consoleOutput);
+	public Log4JLoggingSystem(ClassLoader classLoader) {
+		this(classLoader, false, true);
 	}
 	
+	public Log4JLoggingSystem(ClassLoader classLoader, boolean fileOutput,
+			boolean consoleOutput) {
+		super(classLoader, fileOutput, consoleOutput);
+	}
+
 	@Override
-	protected String[] getLogFileName(boolean fileOutput, boolean consoleOutput) {
-		if (fileOutput && consoleOutput) {
-			return new String[] { "log4j-file-console.xml", "log4j-file-console.properties" };
-		}
-		else if (fileOutput) {
-			return new String[] { "log4j-file.xml", "log4j-file.properties" };
-		}
-		else if (consoleOutput) {
-			return new String[] { "log4j-console.xml", "log4j-console.properties" };
-		}
-		else {
-			return new String[] { "log4j.xml", "log4j.properties" };
-		}
+	protected String[] getLogFileNames() {
+		return new String[] { "log4j.xml", "log4j.properties" };
 	}
 
 	@Override
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java
index 792f0293de5..8142f8f8cf8 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java
@@ -27,7 +27,6 @@
 import org.apache.logging.log4j.core.config.Configuration;
 import org.apache.logging.log4j.core.config.ConfigurationFactory;
 import org.apache.logging.log4j.core.config.ConfigurationSource;
-
 import org.springframework.boot.logging.LogLevel;
 import org.springframework.boot.logging.LoggingSystem;
 import org.springframework.boot.logging.Slf4JLoggingSystem;
@@ -57,24 +56,17 @@
 		LEVELS = Collections.unmodifiableMap(levels);
 	}
 
+	public Log4J2LoggingSystem(ClassLoader classLoader) {
+		this(classLoader, false, true);
+	}
+	
 	public Log4J2LoggingSystem(ClassLoader classLoader, boolean fileOutput, boolean consoleOutput) {
 		super(classLoader, fileOutput, consoleOutput);
 	}
 	
 	@Override
-	protected String[] getLogFileName(boolean fileOutput, boolean consoleOutput) {
-		if (fileOutput && consoleOutput) {
-			return new String[] { "log4j2-file-console.json", "log4j2-file-console.jsn", "log4j2-file-console.xml" };
-		}
-		else if (fileOutput) {
-			return new String[] { "log4j2-file.json", "log4j2-file.jsn", "log4j2-file.xml" };
-		}
-		else if (consoleOutput) {
-			return new String[] { "log4j2-console.json", "log4j2-console.jsn", "log4j2-console.xml" };
-		}
-		else {
-			return new String[] { "log4j2.json", "log4j2.jsn", "log4j2.xml" };
-		}
+	protected String[] getLogFileNames() {
+		return new String[] { "log4j2.json", "log4j2.jsn", "log4j2.xml" };
 	}
 
 	@Override
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java
index 51991d07d7d..a0daefef408 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java
@@ -58,27 +58,19 @@
 		LEVELS = Collections.unmodifiableMap(levels);
 	}
 
-	public LogbackLoggingSystem(ClassLoader classLoader, boolean fileOutput, boolean consoleOutput) {
-		super(classLoader, fileOutput, consoleOutput);
+	public LogbackLoggingSystem(ClassLoader classLoader) {
+		this(classLoader, false, true);
 	}
 	
+	public LogbackLoggingSystem(ClassLoader classLoader, boolean fileOutput,
+			boolean consoleOutput) {
+		super(classLoader, fileOutput, consoleOutput);
+	}
+
 	@Override
-	protected String[] getLogFileName(boolean fileOutput, boolean consoleOutput) {
-		if (fileOutput && consoleOutput) {
-			return new String[] { "logback-test-file-console.groovy", "logback-test-file-console.xml",
-					"logback-file-console.groovy", "logback-file-console.xml" };
-		}
-		else if (fileOutput) {
-			return new String[] { "logback-test-file.groovy", "logback-test-file.xml", "logback-file.groovy",
-					"logback-file.xml" };
-		}
-		else if (consoleOutput) {
-			return new String[] { "logback-test-console.groovy", "logback-test-console.xml", "logback-console.groovy",
-					"logback-console.xml" };
-		}
-		else {
-			return new String[] { "logback-test.groovy", "logback-test.xml", "logback.groovy", "logback.xml" };
-		}
+	protected String[] getLogFileNames() {
+		return new String[] { "logback-test.groovy", "logback-test.xml",
+				"logback.groovy", "logback.xml" };
 	}
 
 	@Override
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging-console.properties b/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging-none.properties
similarity index 68%
rename from spring-boot/src/main/resources/org/springframework/boot/logging/java/logging-console.properties
rename to spring-boot/src/main/resources/org/springframework/boot/logging/java/logging-none.properties
index 96fe6485034..fa027f53ab8 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging-console.properties
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging-none.properties
@@ -1,9 +1,6 @@
-handlers =java.util.logging.ConsoleHandler
+handlers =
 .level = INFO
 
-java.util.logging.ConsoleHandler.formatter = org.springframework.boot.logging.java.SimpleFormatter
-java.util.logging.ConsoleHandler.level = ALL
-
 org.hibernate.validator.internal.util.Version.level = WARNING
 org.apache.coyote.http11.Http11NioProtocol.level = WARNING
 org.crsh.plugin.level = WARNING
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging.properties b/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging.properties
index fa027f53ab8..96fe6485034 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging.properties
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/java/logging.properties
@@ -1,6 +1,9 @@
-handlers =
+handlers =java.util.logging.ConsoleHandler
 .level = INFO
 
+java.util.logging.ConsoleHandler.formatter = org.springframework.boot.logging.java.SimpleFormatter
+java.util.logging.ConsoleHandler.level = ALL
+
 org.hibernate.validator.internal.util.Version.level = WARNING
 org.apache.coyote.http11.Http11NioProtocol.level = WARNING
 org.crsh.plugin.level = WARNING
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j-console.properties b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j-none.properties
similarity index 67%
rename from spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j-console.properties
rename to spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j-none.properties
index e0885761df1..f8a630ca8de 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j-console.properties
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j-none.properties
@@ -1,15 +1,10 @@
-log4j.rootCategory=INFO, CONSOLE
+log4j.rootCategory=INFO
 
 PID=????
 LOG_PATH=${java.io.tmpdir}
 LOG_FILE=${LOG_PATH}/spring.log
 LOG_PATTERN=[%d{yyyy-MM-dd HH:mm:ss.SSS}] boot%X{context} - ${PID} %5p [%t] --- %c{1}: %m%n
 
-# CONSOLE is set to be a ConsoleAppender using a PatternLayout.
-log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
-log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
-log4j.appender.CONSOLE.layout.ConversionPattern=${LOG_PATTERN}
-
 
 log4j.category.org.hibernate.validator.internal.util.Version=WARN
 log4j.category.org.apache.coyote.http11.Http11NioProtocol=WARN
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j.properties b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j.properties
index f8a630ca8de..e0885761df1 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j.properties
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j/log4j.properties
@@ -1,10 +1,15 @@
-log4j.rootCategory=INFO
+log4j.rootCategory=INFO, CONSOLE
 
 PID=????
 LOG_PATH=${java.io.tmpdir}
 LOG_FILE=${LOG_PATH}/spring.log
 LOG_PATTERN=[%d{yyyy-MM-dd HH:mm:ss.SSS}] boot%X{context} - ${PID} %5p [%t] --- %c{1}: %m%n
 
+# CONSOLE is set to be a ConsoleAppender using a PatternLayout.
+log4j.appender.CONSOLE=org.apache.log4j.ConsoleAppender
+log4j.appender.CONSOLE.layout=org.apache.log4j.PatternLayout
+log4j.appender.CONSOLE.layout.ConversionPattern=${LOG_PATTERN}
+
 
 log4j.category.org.hibernate.validator.internal.util.Version=WARN
 log4j.category.org.apache.coyote.http11.Http11NioProtocol=WARN
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2-console.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2-none.xml
similarity index 83%
rename from spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2-console.xml
rename to spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2-none.xml
index 2374d828efd..eed5a2c784b 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2-console.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2-none.xml
@@ -6,11 +6,6 @@
         <Property name="LOG_FILE">${sys:LOG_PATH}/spring.log</Property>
         <Property name="LOG_PATTERN">[%d{yyyy-MM-dd HH:mm:ss.SSS}] boot%X{context} - ${sys:PID} %5p [%t] --- %c{1}: %m%n</Property>
     </Properties>
-    <Appenders>
-        <Console name="Console" target="SYSTEM_OUT" follow="true">
-            <PatternLayout pattern="${LOG_PATTERN}"/>
-        </Console>
-    </Appenders>
     <Loggers>
         <Logger name="org.hibernate.validator.internal.util.Version" level="warn" />
         <Logger name="org.apache.coyote.http11.Http11NioProtocol" level="warn" />
@@ -21,7 +16,6 @@
         <Logger name="org.eclipse.jetty.util.component.AbstractLifeCycle" level="error" />
 
         <Root level="info">
-            <AppenderRef ref="Console"/>
         </Root>
     </Loggers>
 </Configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2.xml
index eed5a2c784b..2374d828efd 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/log4j2/log4j2.xml
@@ -6,6 +6,11 @@
         <Property name="LOG_FILE">${sys:LOG_PATH}/spring.log</Property>
         <Property name="LOG_PATTERN">[%d{yyyy-MM-dd HH:mm:ss.SSS}] boot%X{context} - ${sys:PID} %5p [%t] --- %c{1}: %m%n</Property>
     </Properties>
+    <Appenders>
+        <Console name="Console" target="SYSTEM_OUT" follow="true">
+            <PatternLayout pattern="${LOG_PATTERN}"/>
+        </Console>
+    </Appenders>
     <Loggers>
         <Logger name="org.hibernate.validator.internal.util.Version" level="warn" />
         <Logger name="org.apache.coyote.http11.Http11NioProtocol" level="warn" />
@@ -16,6 +21,7 @@
         <Logger name="org.eclipse.jetty.util.component.AbstractLifeCycle" level="error" />
 
         <Root level="info">
+            <AppenderRef ref="Console"/>
         </Root>
     </Loggers>
 </Configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-file.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/base-file.xml
similarity index 96%
rename from spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-file.xml
rename to spring-boot/src/main/resources/org/springframework/boot/logging/logback/base-file.xml
index b05d3c740ef..a7ed15fc0df 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-file.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/base-file.xml
@@ -1,4 +1,4 @@
-<?xml version="1.0" encoding="UTF8"?>
+<?xml version="1.0" encoding="UTF-8"?>
 <included>
 	<conversionRule conversionWord="wex" converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />
 	<property name="LOG_FILE" value="${LOG_FILE:-${LOG_PATH:-${LOG_TEMP:-${java.io.tmpdir:-/tmp}}/}spring.log}"/>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/base.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/base.xml
index 04eb8955ebb..dc572d95830 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/base.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/base.xml
@@ -1,10 +1,17 @@
 <?xml version="1.0" encoding="UTF-8"?>
 <included>
-
-	<include resource="org/springframework/boot/logging/logback/basic.xml"/>
-
-
+	<conversionRule conversionWord="clr" converterClass="org.springframework.boot.logging.logback.ColorConverter" />
+	<conversionRule conversionWord="wex" converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />
+	
+	<property name="CONSOLE_LOG_PATTERN" value="%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(%5p) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t{14}]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n%wex"/>
+	<appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
+		<encoder>
+			<pattern>${CONSOLE_LOG_PATTERN}</pattern>
+			<charset>utf8</charset>
+		</encoder>
+	</appender>
+	
 	<root level="INFO">
-	</root>
-
+		<appender-ref ref="CONSOLE" />
+ 	</root>
 </included>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-console.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-console.xml
deleted file mode 100644
index 30dfc9be4f5..00000000000
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-console.xml
+++ /dev/null
@@ -1,18 +0,0 @@
-<?xml version="1.0" encoding="UTF8"?>
-<included>
-	<conversionRule conversionWord="clr" converterClass="org.springframework.boot.logging.logback.ColorConverter" />
-	<conversionRule conversionWord="wex" converterClass="org.springframework.boot.logging.logback.WhitespaceThrowableProxyConverter" />
-	
-	<property name="CONSOLE_LOG_PATTERN" value="%clr(%d{yyyy-MM-dd HH:mm:ss.SSS}){faint} %clr(%5p) %clr(${PID:- }){magenta} %clr(---){faint} %clr([%15.15t{14}]){faint} %clr(%-40.40logger{39}){cyan} %clr(:){faint} %m%n%wex"/>
-	<include resource="org/springframework/boot/logging/logback/base.xml"/>
-	<appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
-		<encoder>
-			<pattern>${CONSOLE_LOG_PATTERN}</pattern>
-			<charset>utf8</charset>
-		</encoder>
-	</appender>
-	
-	<root level="INFO">
-		<appender-ref ref="CONSOLE" />
- 	</root>
-</included>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-logback.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-logback.xml
index 266ac62b2ee..a5c5515b84b 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-logback.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic-logback.xml
@@ -1,4 +1,4 @@
 <?xml version="1.0" encoding="UTF-8"?>
 <configuration>
-	<include resource="org/springframework/boot/logging/logback/basic.xml" />
+	<include resource="org/springframework/boot/logging/logback/base.xml" />
 </configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic.xml
deleted file mode 100644
index f2823ffac66..00000000000
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/basic.xml
+++ /dev/null
@@ -1,20 +0,0 @@
-<?xml version="1.0" encoding="UTF-8"?>
-<included>
-
-	<appender name="DEBUG_LEVEL_REMAPPER" class="org.springframework.boot.logging.logback.LevelRemappingAppender">
-	   <destinationLogger>org.springframework.boot</destinationLogger>
-	</appender>
-	
-
-	<logger name="org.hibernate.validator.internal.util.Version" level="WARN"/>
-	<logger name="org.apache.coyote.http11.Http11NioProtocol" level="WARN"/>
-	<logger name="org.crsh.plugin" level="WARN"/>
-	<logger name="org.apache.tomcat.util.net.NioSelectorPool" level="WARN"/>
-	<logger name="org.apache.catalina.startup.DigesterFactory" level="ERROR"/>
-	<logger name="org.apache.catalina.util.LifecycleBase" level="ERROR"/>
-	<logger name="org.eclipse.jetty.util.component.AbstractLifeCycle" level="ERROR"/>
-	<logger name="org.thymeleaf" additivity="false">
-		<appender-ref ref="DEBUG_LEVEL_REMAPPER"/>
-	</logger>
-
-</included>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-console.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-console.xml
deleted file mode 100644
index 69a57c719fa..00000000000
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-console.xml
+++ /dev/null
@@ -1,5 +0,0 @@
-<?xml version="1.0" encoding="UTF8"?>
-<configuration>
-	<include resource="org/springframework/boot/logging/logback/base.xml"/>
- 	<include resource="org/springframework/boot/logging/logback/basic-console.xml"/>
-</configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file-console.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file-console.xml
index a97dd6e69e5..38e7fe99c46 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file-console.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file-console.xml
@@ -1,6 +1,5 @@
-<?xml version="1.0" encoding="UTF8"?>
+<?xml version="1.0" encoding="UTF-8"?>
 <configuration>
-	<include resource="org/springframework/boot/logging/logback/base.xml"/>
- 	<include resource="org/springframework/boot/logging/logback/basic-file.xml"/>
- 	<include resource="org/springframework/boot/logging/logback/basic-console.xml"/>
+ 	<include resource="org/springframework/boot/logging/logback/base-file.xml"/>
+ 	<include resource="org/springframework/boot/logging/logback/base.xml"/>
 </configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file.xml
index a58db2ef99d..6d0ccd35373 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-file.xml
@@ -1,5 +1,4 @@
-<?xml version="1.0" encoding="UTF8"?>
+<?xml version="1.0" encoding="UTF-8"?>
 <configuration>
-	<include resource="org/springframework/boot/logging/logback/base.xml"/>
- 	<include resource="org/springframework/boot/logging/logback/basic-file.xml"/>
+ 	<include resource="org/springframework/boot/logging/logback/base-file.xml"/>
 </configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-none.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-none.xml
new file mode 100644
index 00000000000..eb91e909b9b
--- /dev/null
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback-none.xml
@@ -0,0 +1,5 @@
+<?xml version="1.0" encoding="UTF-8"?>
+<configuration>
+	<root level="INFO">
+ 	</root>
+</configuration>
diff --git a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback.xml b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback.xml
index c4cce79ddc9..b8a41480d7d 100644
--- a/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback.xml
+++ b/spring-boot/src/main/resources/org/springframework/boot/logging/logback/logback.xml
@@ -1,4 +1,4 @@
-<?xml version="1.0" encoding="UTF8"?>
+<?xml version="1.0" encoding="UTF-8"?>
 <configuration>
 	<include resource="org/springframework/boot/logging/logback/base.xml"/>
 </configuration>
diff --git a/spring-boot/src/test/java/org/springframework/boot/logging/LoggingApplicationListenerTests.java b/spring-boot/src/test/java/org/springframework/boot/logging/LoggingApplicationListenerTests.java
index eb871fa1dce..a0d9de3ae05 100644
--- a/spring-boot/src/test/java/org/springframework/boot/logging/LoggingApplicationListenerTests.java
+++ b/spring-boot/src/test/java/org/springframework/boot/logging/LoggingApplicationListenerTests.java
@@ -103,7 +103,18 @@ public void baseConfigLocation() {
 		String output = this.outputCapture.toString().trim();
 		assertTrue("Wrong output:\n" + output, output.contains("Hello world"));
 		assertFalse("Wrong output:\n" + output, output.contains("???"));
-		assertTrue(new File(tmpDir() + "/spring.log").exists());
+		assertFalse(new File(tmpDir() + "/spring.log").exists());
+	}
+
+	@Test
+	public void noConsole() {
+		EnvironmentTestUtils.addEnvironment(this.context, "logging.console: false");
+		this.initializer.initialize(this.context.getEnvironment(),
+				this.context.getClassLoader());
+		this.logger.info("Hello world");
+		String output = this.outputCapture.toString().trim();
+		assertFalse("Wrong output:\n" + output, output.contains("Hello world"));
+		assertFalse(new File(tmpDir() + "/spring.log").exists());
 	}
 
 	@Test
@@ -130,7 +141,7 @@ public void overrideConfigDoesNotExist() throws Exception {
 		String output = this.outputCapture.toString().trim();
 		assertTrue("Wrong output:\n" + output, output.contains("Hello world"));
 		assertFalse("Wrong output:\n" + output, output.contains("???"));
-		assertTrue(new File(tmpDir() + "/spring.log").exists());
+		assertFalse(new File(tmpDir() + "/spring.log").exists());
 	}
 
 	@Test
