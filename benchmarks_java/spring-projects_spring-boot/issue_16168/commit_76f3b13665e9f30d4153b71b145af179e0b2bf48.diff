diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpoint.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpoint.java
index 61d3a872208..449542c5dcb 100644
--- a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpoint.java
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpoint.java
@@ -25,7 +25,6 @@
 
 import javax.sql.DataSource;
 
-import liquibase.changelog.ChangeLogHistoryService;
 import liquibase.changelog.ChangeSet.ExecType;
 import liquibase.changelog.RanChangeSet;
 import liquibase.changelog.StandardChangeLogHistoryService;
@@ -63,9 +62,8 @@ public ApplicationLiquibaseBeans liquibaseBeans() {
 		while (target != null) {
 			Map<String, LiquibaseBean> liquibaseBeans = new HashMap<>();
 			DatabaseFactory factory = DatabaseFactory.getInstance();
-			StandardChangeLogHistoryService service = new StandardChangeLogHistoryService();
 			this.context.getBeansOfType(SpringLiquibase.class)
-					.forEach((name, liquibase) -> liquibaseBeans.put(name, createReport(liquibase, service, factory)));
+					.forEach((name, liquibase) -> liquibaseBeans.put(name, createReport(liquibase, factory)));
 			ApplicationContext parent = target.getParent();
 			contextBeans.put(target.getId(),
 					new ContextLiquibaseBeans(liquibaseBeans, (parent != null) ? parent.getId() : null));
@@ -74,8 +72,7 @@ public ApplicationLiquibaseBeans liquibaseBeans() {
 		return new ApplicationLiquibaseBeans(contextBeans);
 	}
 
-	private LiquibaseBean createReport(SpringLiquibase liquibase, ChangeLogHistoryService service,
-			DatabaseFactory factory) {
+	private LiquibaseBean createReport(SpringLiquibase liquibase, DatabaseFactory factory) {
 		try {
 			DataSource dataSource = liquibase.getDataSource();
 			JdbcConnection connection = new JdbcConnection(dataSource.getConnection());
@@ -88,6 +85,7 @@ private LiquibaseBean createReport(SpringLiquibase liquibase, ChangeLogHistorySe
 				}
 				database.setDatabaseChangeLogTableName(liquibase.getDatabaseChangeLogTable());
 				database.setDatabaseChangeLogLockTableName(liquibase.getDatabaseChangeLogLockTable());
+				StandardChangeLogHistoryService service = new StandardChangeLogHistoryService();
 				service.setDatabase(database);
 				return new LiquibaseBean(
 						service.getRanChangeSets().stream().map(ChangeSet::new).collect(Collectors.toList()));
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpointTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpointTests.java
index f345c485a1b..8e4b16f0866 100644
--- a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpointTests.java
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/liquibase/LiquibaseEndpointTests.java
@@ -22,16 +22,19 @@
 
 import javax.sql.DataSource;
 
+import liquibase.integration.spring.SpringLiquibase;
 import org.junit.Test;
 
 import org.springframework.boot.actuate.liquibase.LiquibaseEndpoint.LiquibaseBean;
 import org.springframework.boot.autoconfigure.AutoConfigurations;
 import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
 import org.springframework.boot.autoconfigure.liquibase.LiquibaseAutoConfiguration;
+import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
 import org.springframework.boot.test.context.runner.ApplicationContextRunner;
 import org.springframework.context.ApplicationContext;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
+import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
 
 import static org.assertj.core.api.Assertions.assertThat;
 
@@ -41,6 +44,7 @@
  * @author Eddú Meléndez
  * @author Andy Wilkinson
  * @author Stephane Nicoll
+ * @author Leo Li
  */
 public class LiquibaseEndpointTests {
 
@@ -92,6 +96,21 @@ public void connectionAutoCommitPropertyIsReset() {
 		});
 	}
 
+	@Test
+	public void whenMultipleLiquibaseBeansArePresentChangeSetsAreCorrectlyReportedForEachBean() {
+		this.contextRunner.withUserConfiguration(Config.class, MultipleDataSourceLiquibaseConfiguration.class)
+				.run((context) -> {
+					Map<String, LiquibaseBean> liquibaseBeans = context.getBean(LiquibaseEndpoint.class)
+							.liquibaseBeans().getContexts().get(context.getId()).getLiquibaseBeans();
+					assertThat(liquibaseBeans.get("liquibase").getChangeSets()).hasSize(1);
+					assertThat(liquibaseBeans.get("liquibase").getChangeSets().get(0).getChangeLog())
+							.isEqualTo("classpath:/db/changelog/db.changelog-master.yaml");
+					assertThat(liquibaseBeans.get("liquibaseBackup").getChangeSets()).hasSize(1);
+					assertThat(liquibaseBeans.get("liquibaseBackup").getChangeSets().get(0).getChangeLog())
+							.isEqualTo("classpath:/db/changelog/db.changelog-master-backup.yaml");
+				});
+	}
+
 	private boolean getAutoCommit(DataSource dataSource) throws SQLException {
 		try (Connection connection = dataSource.getConnection()) {
 			return connection.getAutoCommit();
@@ -108,4 +127,42 @@ public LiquibaseEndpoint endpoint(ApplicationContext context) {
 
 	}
 
+	@Configuration
+	static class MultipleDataSourceLiquibaseConfiguration {
+
+		@Bean
+		DataSource dataSource() {
+			return createEmbeddedDatabase();
+		}
+
+		@Bean
+		DataSource dataSourceBackup() {
+			return createEmbeddedDatabase();
+		}
+
+		@Bean
+		SpringLiquibase liquibase(DataSource dataSource) {
+			return createSpringLiquibase("db.changelog-master.yaml", dataSource);
+		}
+
+		@Bean
+		SpringLiquibase liquibaseBackup(DataSource dataSourceBackup) {
+			return createSpringLiquibase("db.changelog-master-backup.yaml", dataSourceBackup);
+		}
+
+		private DataSource createEmbeddedDatabase() {
+			return new EmbeddedDatabaseBuilder().generateUniqueName(true)
+					.setType(EmbeddedDatabaseConnection.HSQL.getType()).build();
+		}
+
+		private SpringLiquibase createSpringLiquibase(String changeLog, DataSource dataSource) {
+			SpringLiquibase liquibase = new SpringLiquibase();
+			liquibase.setChangeLog("classpath:/db/changelog/" + changeLog);
+			liquibase.setShouldRun(true);
+			liquibase.setDataSource(dataSource);
+			return liquibase;
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-actuator/src/test/resources/db/changelog/db.changelog-master-backup.yaml b/spring-boot-project/spring-boot-actuator/src/test/resources/db/changelog/db.changelog-master-backup.yaml
new file mode 100644
index 00000000000..a3a19b43af3
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/resources/db/changelog/db.changelog-master-backup.yaml
@@ -0,0 +1,20 @@
+databaseChangeLog:
+  - changeSet:
+      id: 1
+      author: leoli
+      changes:
+        - createTable:
+            tableName: customerbackup
+            columns:
+              - column:
+                  name: id
+                  type: int
+                  autoIncrement: true
+                  constraints:
+                    primaryKey: true
+                    nullable: false
+              - column:
+                  name: name
+                  type: varchar(50)
+                  constraints:
+                    nullable: false
