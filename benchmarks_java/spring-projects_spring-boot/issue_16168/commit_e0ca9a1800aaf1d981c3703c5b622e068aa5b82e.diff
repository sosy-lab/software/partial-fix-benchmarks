diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/jetty/SslServerCustomizer.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/jetty/SslServerCustomizer.java
index b1ae3d50386..51b415ec634 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/jetty/SslServerCustomizer.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/jetty/SslServerCustomizer.java
@@ -24,6 +24,7 @@
 import org.eclipse.jetty.http.HttpVersion;
 import org.eclipse.jetty.http2.HTTP2Cipher;
 import org.eclipse.jetty.http2.server.HTTP2ServerConnectionFactory;
+import org.eclipse.jetty.server.ConnectionFactory;
 import org.eclipse.jetty.server.Connector;
 import org.eclipse.jetty.server.HttpConfiguration;
 import org.eclipse.jetty.server.HttpConnectionFactory;
@@ -37,6 +38,7 @@
 import org.springframework.boot.web.server.Http2;
 import org.springframework.boot.web.server.Ssl;
 import org.springframework.boot.web.server.SslStoreProvider;
+import org.springframework.boot.web.server.SslUtils;
 import org.springframework.boot.web.server.WebServerException;
 import org.springframework.util.Assert;
 import org.springframework.util.ClassUtils;
@@ -105,7 +107,8 @@ private ServerConnector createHttp11ServerConnector(Server server, HttpConfigura
 		HttpConnectionFactory connectionFactory = new HttpConnectionFactory(config);
 		SslConnectionFactory sslConnectionFactory = new SslConnectionFactory(sslContextFactory,
 				HttpVersion.HTTP_1_1.asString());
-		return new ServerConnector(server, sslConnectionFactory, connectionFactory);
+		return new SslValidatingServerConnector(server, sslContextFactory, this.ssl.getKeyAlias(), sslConnectionFactory,
+				connectionFactory);
 	}
 
 	private boolean isAlpnPresent() {
@@ -123,7 +126,8 @@ private ServerConnector createHttp2ServerConnector(Server server, HttpConfigurat
 		sslContextFactory.setCipherComparator(HTTP2Cipher.COMPARATOR);
 		sslContextFactory.setProvider("Conscrypt");
 		SslConnectionFactory ssl = new SslConnectionFactory(sslContextFactory, alpn.getProtocol());
-		return new ServerConnector(server, ssl, alpn, h2, new HttpConnectionFactory(config));
+		return new SslValidatingServerConnector(server, sslContextFactory, this.ssl.getKeyAlias(), ssl, alpn, h2,
+				new HttpConnectionFactory(config));
 	}
 
 	/**
@@ -215,4 +219,32 @@ private void configureSslTrustStore(SslContextFactory factory, Ssl ssl) {
 		}
 	}
 
+	static class SslValidatingServerConnector extends ServerConnector {
+
+		private SslContextFactory sslContextFactory;
+
+		private String keyAlias;
+
+		SslValidatingServerConnector(Server server, SslContextFactory sslContextFactory, String keyAlias,
+				SslConnectionFactory sslConnectionFactory, HttpConnectionFactory connectionFactory) {
+			super(server, sslConnectionFactory, connectionFactory);
+			this.sslContextFactory = sslContextFactory;
+			this.keyAlias = keyAlias;
+		}
+
+		SslValidatingServerConnector(Server server, SslContextFactory sslContextFactory, String keyAlias,
+				ConnectionFactory... factories) {
+			super(server, factories);
+			this.sslContextFactory = sslContextFactory;
+			this.keyAlias = keyAlias;
+		}
+
+		@Override
+		protected void doStart() throws Exception {
+			super.doStart();
+			SslUtils.assertStoreContainsAlias(this.sslContextFactory.getKeyStore(), this.keyAlias);
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/netty/SslServerCustomizer.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/netty/SslServerCustomizer.java
index 137a0d2cf97..5a57c188aca 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/netty/SslServerCustomizer.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/netty/SslServerCustomizer.java
@@ -31,6 +31,7 @@
 import org.springframework.boot.web.server.Http2;
 import org.springframework.boot.web.server.Ssl;
 import org.springframework.boot.web.server.SslStoreProvider;
+import org.springframework.boot.web.server.SslUtils;
 import org.springframework.boot.web.server.WebServerException;
 import org.springframework.util.ResourceUtils;
 
@@ -92,6 +93,8 @@ else if (this.ssl.getClientAuth() == Ssl.ClientAuth.WANT) {
 	protected KeyManagerFactory getKeyManagerFactory(Ssl ssl, SslStoreProvider sslStoreProvider) {
 		try {
 			KeyStore keyStore = getKeyStore(ssl, sslStoreProvider);
+			SslUtils.assertStoreContainsAlias(keyStore, ssl.getKeyAlias());
+
 			KeyManagerFactory keyManagerFactory = KeyManagerFactory
 					.getInstance(KeyManagerFactory.getDefaultAlgorithm());
 			char[] keyPassword = (ssl.getKeyPassword() != null) ? ssl.getKeyPassword().toCharArray() : null;
diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/undertow/SslBuilderCustomizer.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/undertow/SslBuilderCustomizer.java
index 95a7e418482..1bde5d121ba 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/undertow/SslBuilderCustomizer.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/undertow/SslBuilderCustomizer.java
@@ -41,6 +41,7 @@
 
 import org.springframework.boot.web.server.Ssl;
 import org.springframework.boot.web.server.SslStoreProvider;
+import org.springframework.boot.web.server.SslUtils;
 import org.springframework.boot.web.server.WebServerException;
 import org.springframework.util.ResourceUtils;
 
@@ -107,6 +108,8 @@ private SslClientAuthMode getSslClientAuthMode(Ssl ssl) {
 	private KeyManager[] getKeyManagers(Ssl ssl, SslStoreProvider sslStoreProvider) {
 		try {
 			KeyStore keyStore = getKeyStore(ssl, sslStoreProvider);
+			SslUtils.assertStoreContainsAlias(keyStore, ssl.getKeyAlias());
+
 			KeyManagerFactory keyManagerFactory = KeyManagerFactory
 					.getInstance(KeyManagerFactory.getDefaultAlgorithm());
 			char[] keyPassword = (ssl.getKeyPassword() != null) ? ssl.getKeyPassword().toCharArray() : null;
diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/server/SslUtils.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/server/SslUtils.java
new file mode 100644
index 00000000000..4f51a3d6b9c
--- /dev/null
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/server/SslUtils.java
@@ -0,0 +1,49 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.web.server;
+
+import java.security.KeyStore;
+import java.security.KeyStoreException;
+
+import org.springframework.util.Assert;
+import org.springframework.util.StringUtils;
+
+/**
+ * Provides utilities around SSL.
+ *
+ * @author Chris Bono
+ * @since 2.1.x
+ */
+public final class SslUtils {
+
+	private SslUtils() {
+	}
+
+	public static void assertStoreContainsAlias(KeyStore keyStore, String keyAlias) {
+		if (!StringUtils.isEmpty(keyAlias)) {
+			try {
+				Assert.state(keyStore.containsAlias(keyAlias),
+						() -> String.format("Keystore does not contain specified alias '%s'", keyAlias));
+			}
+			catch (KeyStoreException ex) {
+				throw new IllegalStateException(
+						String.format("Could not determine if keystore contains alias '%s'", keyAlias), ex);
+			}
+		}
+	}
+
+}
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java
index 14b501fc99b..0b9e0d50696 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java
@@ -30,11 +30,14 @@
 import org.mockito.ArgumentCaptor;
 import org.mockito.InOrder;
 
+import org.springframework.boot.web.reactive.server.AbstractReactiveWebServerFactory;
 import org.springframework.boot.web.reactive.server.AbstractReactiveWebServerFactoryTests;
+import org.springframework.boot.web.server.Ssl;
 import org.springframework.http.server.reactive.HttpHandler;
 
 import static org.assertj.core.api.Assertions.assertThat;
 import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
+import static org.assertj.core.api.Assertions.assertThatThrownBy;
 import static org.mockito.ArgumentMatchers.any;
 import static org.mockito.Mockito.inOrder;
 import static org.mockito.Mockito.mock;
@@ -155,4 +158,19 @@ public void referenceClearingIsDisabled() {
 		assertThat(context.getClearReferencesThreadLocals()).isFalse();
 	}
 
+	@Test
+	@Override
+	public void sslWithInvalidAliasFailsDuringStartup() {
+		String keyStore = "classpath:test.jks";
+		String keyPassword = "password";
+		AbstractReactiveWebServerFactory factory = getFactory();
+		Ssl ssl = new Ssl();
+		ssl.setKeyStore(keyStore);
+		ssl.setKeyPassword(keyPassword);
+		ssl.setKeyAlias("test-alias-404");
+		factory.setSsl(ssl);
+		assertThatThrownBy(() -> factory.getWebServer(new EchoHandler()).start())
+				.isInstanceOf(ConnectorStartFailedException.class);
+	}
+
 }
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatServletWebServerFactoryTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatServletWebServerFactoryTests.java
index abf4bacc06d..20e25a31dc9 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatServletWebServerFactoryTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatServletWebServerFactoryTests.java
@@ -65,8 +65,11 @@
 import org.mockito.InOrder;
 
 import org.springframework.boot.testsupport.rule.OutputCapture;
+import org.springframework.boot.testsupport.web.servlet.ExampleServlet;
+import org.springframework.boot.web.server.Ssl;
 import org.springframework.boot.web.server.WebServerException;
 import org.springframework.boot.web.servlet.ServletContextInitializer;
+import org.springframework.boot.web.servlet.ServletRegistrationBean;
 import org.springframework.boot.web.servlet.server.AbstractServletWebServerFactory;
 import org.springframework.boot.web.servlet.server.AbstractServletWebServerFactoryTests;
 import org.springframework.core.io.ByteArrayResource;
@@ -84,6 +87,7 @@
 import static org.assertj.core.api.Assertions.assertThat;
 import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
 import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
+import static org.assertj.core.api.Assertions.assertThatThrownBy;
 import static org.mockito.ArgumentMatchers.any;
 import static org.mockito.Mockito.inOrder;
 import static org.mockito.Mockito.mock;
@@ -534,6 +538,18 @@ public void onStartup(ServletContext servletContext) throws ServletException {
 		this.webServer.start();
 	}
 
+	@Test
+	@Override
+	public void sslWithInvalidAliasFailsDuringStartup() {
+		AbstractServletWebServerFactory factory = getFactory();
+		Ssl ssl = getSsl(null, "password", "test-alias-404", "src/test/resources/test.jks");
+		factory.setSsl(ssl);
+		ServletRegistrationBean<ExampleServlet> registration = new ServletRegistrationBean<>(
+				new ExampleServlet(true, false), "/hello");
+		assertThatThrownBy(() -> factory.getWebServer(registration).start())
+				.isInstanceOf(ConnectorStartFailedException.class);
+	}
+
 	@Override
 	protected JspServlet getJspServlet() throws ServletException {
 		Tomcat tomcat = ((TomcatWebServer) this.webServer).getTomcat();
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/reactive/server/AbstractReactiveWebServerFactoryTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/reactive/server/AbstractReactiveWebServerFactoryTests.java
index d23e9a3168d..3e888b51ab0 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/reactive/server/AbstractReactiveWebServerFactoryTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/reactive/server/AbstractReactiveWebServerFactoryTests.java
@@ -132,6 +132,44 @@ protected final void testBasicSslWithKeyStore(String keyStore, String keyPasswor
 		assertThat(result.block(Duration.ofSeconds(30))).isEqualTo("Hello World");
 	}
 
+	@Test
+	public void sslWithValidAlias() {
+		String keyStore = "classpath:test.jks";
+		String keyPassword = "password";
+		AbstractReactiveWebServerFactory factory = getFactory();
+		Ssl ssl = new Ssl();
+		ssl.setKeyStore(keyStore);
+		ssl.setKeyPassword(keyPassword);
+		ssl.setKeyAlias("test-alias");
+		factory.setSsl(ssl);
+		this.webServer = factory.getWebServer(new EchoHandler());
+		this.webServer.start();
+		ReactorClientHttpConnector connector = buildTrustAllSslConnector();
+		WebClient client = WebClient.builder().baseUrl("https://localhost:" + this.webServer.getPort())
+				.clientConnector(connector).build();
+
+		Mono<String> result = client.post().uri("/test").contentType(MediaType.TEXT_PLAIN)
+				.body(BodyInserters.fromObject("Hello World")).exchange()
+				.flatMap((response) -> response.bodyToMono(String.class));
+
+		StepVerifier.setDefaultTimeout(Duration.ofSeconds(30));
+		StepVerifier.create(result).expectNext("Hello World").verifyComplete();
+	}
+
+	@Test
+	public void sslWithInvalidAliasFailsDuringStartup() {
+		String keyStore = "classpath:test.jks";
+		String keyPassword = "password";
+		AbstractReactiveWebServerFactory factory = getFactory();
+		Ssl ssl = new Ssl();
+		ssl.setKeyStore(keyStore);
+		ssl.setKeyPassword(keyPassword);
+		ssl.setKeyAlias("test-alias-404");
+		factory.setSsl(ssl);
+		assertThatThrownBy(() -> factory.getWebServer(new EchoHandler()).start())
+				.hasStackTraceContaining("Keystore does not contain specified alias 'test-alias-404'");
+	}
+
 	protected ReactorClientHttpConnector buildTrustAllSslConnector() {
 		SslContextBuilder builder = SslContextBuilder.forClient().sslProvider(SslProvider.JDK)
 				.trustManager(InsecureTrustManagerFactory.INSTANCE);
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/server/SslUtilsTest.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/server/SslUtilsTest.java
new file mode 100644
index 00000000000..9d7e0acc327
--- /dev/null
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/server/SslUtilsTest.java
@@ -0,0 +1,83 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.web.server;
+
+import java.security.KeyStore;
+import java.security.KeyStoreException;
+
+import org.junit.Test;
+
+import static org.assertj.core.api.Assertions.assertThatThrownBy;
+import static org.mockito.BDDMockito.when;
+import static org.mockito.Mockito.mock;
+import static org.mockito.Mockito.never;
+import static org.mockito.Mockito.verify;
+
+/**
+ * Tests for {@link SslUtils}.
+ *
+ * @author Chris Bono
+ */
+
+public class SslUtilsTest {
+
+	@Test
+	public void assertStoreContainsAliasPassesWhenAliasFound() throws KeyStoreException {
+		KeyStore keyStore = mock(KeyStore.class);
+		when(keyStore.containsAlias("alias")).thenReturn(true);
+		SslUtils.assertStoreContainsAlias(keyStore, "alias");
+		verify(keyStore).containsAlias("alias");
+	}
+
+	@Test
+	public void assertStoreContainsAliasPassesWhenNullAlias() throws KeyStoreException {
+		KeyStore keyStore = mock(KeyStore.class);
+		when(keyStore.containsAlias("alias")).thenReturn(true);
+		SslUtils.assertStoreContainsAlias(keyStore, null);
+		verify(keyStore, never()).containsAlias("alias");
+	}
+
+	@Test
+	public void assertStoreContainsAliasPassesWhenEmptyAlias() throws KeyStoreException {
+		KeyStore keyStore = mock(KeyStore.class);
+		when(keyStore.containsAlias("alias")).thenReturn(true);
+		SslUtils.assertStoreContainsAlias(keyStore, "");
+		verify(keyStore, never()).containsAlias("alias");
+	}
+
+	@Test
+	public void assertStoreContainsAliasFailsWhenAliasNotFound() throws KeyStoreException {
+		KeyStore keyStore = mock(KeyStore.class);
+		when(keyStore.containsAlias("alias")).thenReturn(false);
+		assertThatThrownBy(() -> SslUtils.assertStoreContainsAlias(keyStore, "alias"))
+				.isInstanceOf(IllegalStateException.class)
+				.hasMessage("Keystore does not contain specified alias 'alias'");
+		verify(keyStore).containsAlias("alias");
+	}
+
+	@Test
+	public void assertStoreContainsAliasFailsWhenKeyStoreThrowsExceptionOnContains() throws KeyStoreException {
+		KeyStore keyStore = mock(KeyStore.class);
+		KeyStoreException keyStoreEx = new KeyStoreException("5150");
+		when(keyStore.containsAlias("alias")).thenThrow(keyStoreEx);
+		assertThatThrownBy(() -> SslUtils.assertStoreContainsAlias(keyStore, "alias"))
+				.isInstanceOf(IllegalStateException.class)
+				.hasMessage("Could not determine if keystore contains alias 'alias'").hasCause(keyStoreEx);
+		verify(keyStore).containsAlias("alias");
+	}
+
+}
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/servlet/server/AbstractServletWebServerFactoryTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/servlet/server/AbstractServletWebServerFactoryTests.java
index 18fbb798903..14bc0a838f0 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/servlet/server/AbstractServletWebServerFactoryTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/servlet/server/AbstractServletWebServerFactoryTests.java
@@ -119,6 +119,7 @@
 import static org.assertj.core.api.Assertions.assertThatIOException;
 import static org.assertj.core.api.Assertions.assertThatIllegalArgumentException;
 import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
+import static org.assertj.core.api.Assertions.assertThatThrownBy;
 import static org.hamcrest.CoreMatchers.notNullValue;
 import static org.junit.Assert.fail;
 import static org.mockito.ArgumentMatchers.any;
@@ -408,6 +409,17 @@ public void sslKeyAlias() throws Exception {
 		assertThat(response).contains("scheme=https");
 	}
 
+	@Test
+	public void sslWithInvalidAliasFailsDuringStartup() {
+		AbstractServletWebServerFactory factory = getFactory();
+		Ssl ssl = getSsl(null, "password", "test-alias-404", "src/test/resources/test.jks");
+		factory.setSsl(ssl);
+		ServletRegistrationBean<ExampleServlet> registration = new ServletRegistrationBean<>(
+				new ExampleServlet(true, false), "/hello");
+		assertThatThrownBy(() -> factory.getWebServer(registration).start())
+				.hasStackTraceContaining("Keystore does not contain specified alias 'test-alias-404'");
+	}
+
 	@Test
 	public void serverHeaderIsDisabledByDefaultWhenUsingSsl() throws Exception {
 		AbstractServletWebServerFactory factory = getFactory();
@@ -579,7 +591,7 @@ protected Ssl getSsl(ClientAuth clientAuth, String keyPassword, String keyStore)
 		return getSsl(clientAuth, keyPassword, keyStore, null, null, null);
 	}
 
-	private Ssl getSsl(ClientAuth clientAuth, String keyPassword, String keyAlias, String keyStore) {
+	protected Ssl getSsl(ClientAuth clientAuth, String keyPassword, String keyAlias, String keyStore) {
 		return getSsl(clientAuth, keyPassword, keyAlias, keyStore, null, null, null);
 	}
 
diff --git a/spring-boot-project/spring-boot/src/test/resources/mockito-extensions/org.mockito.plugins.MockMaker b/spring-boot-project/spring-boot/src/test/resources/mockito-extensions/org.mockito.plugins.MockMaker
new file mode 100644
index 00000000000..ca6ee9cea8e
--- /dev/null
+++ b/spring-boot-project/spring-boot/src/test/resources/mockito-extensions/org.mockito.plugins.MockMaker
@@ -0,0 +1 @@
+mock-maker-inline
\ No newline at end of file
