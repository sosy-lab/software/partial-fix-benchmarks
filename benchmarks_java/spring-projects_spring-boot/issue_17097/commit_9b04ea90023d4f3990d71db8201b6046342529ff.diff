diff --git a/spring-boot-project/spring-boot-tools/spring-boot-test-support/src/main/java/org/springframework/boot/testsupport/system/LocaleExtension.java b/spring-boot-project/spring-boot-tools/spring-boot-test-support/src/main/java/org/springframework/boot/testsupport/system/LocaleExtension.java
new file mode 100644
index 00000000000..48e476f04e8
--- /dev/null
+++ b/spring-boot-project/spring-boot-tools/spring-boot-test-support/src/main/java/org/springframework/boot/testsupport/system/LocaleExtension.java
@@ -0,0 +1,51 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.testsupport.system;
+
+import java.util.Locale;
+
+import org.junit.jupiter.api.extension.AfterEachCallback;
+import org.junit.jupiter.api.extension.BeforeEachCallback;
+import org.junit.jupiter.api.extension.ExtensionContext;
+
+/**
+ * Internal JUnit 5 {@code @Extension} to override system locale for test execution.
+ *
+ * @author Ilya Lukyanovich
+ */
+public class LocaleExtension implements BeforeEachCallback, AfterEachCallback {
+
+	private final Locale locale;
+
+	private Locale defaultLocale;
+
+	public LocaleExtension(Locale locale) {
+		this.locale = locale;
+	}
+
+	@Override
+	public void beforeEach(ExtensionContext context) {
+		this.defaultLocale = Locale.getDefault();
+		Locale.setDefault(this.locale);
+	}
+
+	@Override
+	public void afterEach(ExtensionContext context) {
+		Locale.setDefault(this.defaultLocale);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemJUnit5Tests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemJUnit5Tests.java
new file mode 100644
index 00000000000..6e1b2fc26af
--- /dev/null
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemJUnit5Tests.java
@@ -0,0 +1,81 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.logging;
+
+import java.nio.file.Path;
+
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.io.TempDir;
+
+import org.springframework.util.StringUtils;
+
+/**
+ * Base for {@link LoggingSystem} junit 5 tests.
+ *
+ * @author Ilya Lukyanovich
+ * @author Phillip Webb
+ * @author Andy Wilkinson
+ */
+public abstract class AbstractLoggingSystemJUnit5Tests {
+
+	private static final String JAVA_IO_TMPDIR = "java.io.tmpdir";
+
+	private String originalTempFolder;
+
+	@BeforeEach
+	public void configureTempDir(@TempDir Path temp) {
+		this.originalTempFolder = System.getProperty(JAVA_IO_TMPDIR);
+		System.setProperty(JAVA_IO_TMPDIR, temp.toAbsolutePath().toString());
+	}
+
+	@AfterEach
+	public void reinstateTempDir() {
+		System.setProperty(JAVA_IO_TMPDIR, this.originalTempFolder);
+	}
+
+	@AfterEach
+	public void clear() {
+		System.clearProperty(LoggingSystemProperties.LOG_FILE);
+		System.clearProperty(LoggingSystemProperties.PID_KEY);
+	}
+
+	protected final String[] getSpringConfigLocations(AbstractLoggingSystem system) {
+		return system.getSpringConfigLocations();
+	}
+
+	protected final LogFile getLogFile(String file, String path) {
+		return getLogFile(file, path, true);
+	}
+
+	protected final LogFile getLogFile(String file, String path, boolean applyToSystemProperties) {
+		LogFile logFile = new LogFile(file, path);
+		if (applyToSystemProperties) {
+			logFile.applyToSystemProperties();
+		}
+		return logFile;
+	}
+
+	protected final String tmpDir() {
+		String path = StringUtils.cleanPath(System.getProperty(JAVA_IO_TMPDIR));
+		if (path.endsWith("/")) {
+			path = path.substring(0, path.length() - 1);
+		}
+		return path;
+	}
+
+}
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemTests.java
index 00afb27c062..83212041f96 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/AbstractLoggingSystemTests.java
@@ -26,7 +26,7 @@
 import org.springframework.util.StringUtils;
 
 /**
- * Base for {@link LoggingSystem} tests.
+ * Base for {@link LoggingSystem} junit 4 tests.
  *
  * @author Phillip Webb
  * @author Andy Wilkinson
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/java/JavaLoggingSystemTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/java/JavaLoggingSystemTests.java
index a9cea892271..d6c995763af 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/java/JavaLoggingSystemTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/java/JavaLoggingSystemTests.java
@@ -24,17 +24,20 @@
 import java.util.logging.Level;
 import java.util.logging.Logger;
 
-import org.junit.After;
-import org.junit.Before;
-import org.junit.Rule;
-import org.junit.Test;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+import org.junit.jupiter.api.extension.ExtendWith;
+import org.junit.jupiter.api.extension.RegisterExtension;
 
-import org.springframework.boot.logging.AbstractLoggingSystemTests;
+import org.springframework.boot.logging.AbstractLoggingSystemJUnit5Tests;
 import org.springframework.boot.logging.LogLevel;
 import org.springframework.boot.logging.LoggerConfiguration;
 import org.springframework.boot.logging.LoggingSystem;
 import org.springframework.boot.logging.LoggingSystemProperties;
-import org.springframework.boot.testsupport.system.OutputCaptureRule;
+import org.springframework.boot.testsupport.system.CapturedOutput;
+import org.springframework.boot.testsupport.system.LocaleExtension;
+import org.springframework.boot.testsupport.system.OutputCaptureExtension;
 import org.springframework.util.ClassUtils;
 import org.springframework.util.StringUtils;
 
@@ -48,49 +51,40 @@
  * @author Phillip Webb
  * @author Ben Hale
  */
-public class JavaLoggingSystemTests extends AbstractLoggingSystemTests {
+@ExtendWith(OutputCaptureExtension.class)
+class JavaLoggingSystemTests extends AbstractLoggingSystemJUnit5Tests {
+
+	@RegisterExtension
+	private static final LocaleExtension LOCALE = new LocaleExtension(Locale.ENGLISH);
 
 	private static final FileFilter SPRING_LOG_FILTER = (pathname) -> pathname.getName().startsWith("spring.log");
 
 	private final JavaLoggingSystem loggingSystem = new JavaLoggingSystem(getClass().getClassLoader());
 
-	@Rule
-	public OutputCaptureRule output = new OutputCaptureRule();
-
 	private Logger logger;
 
-	private Locale defaultLocale;
-
-	@Before
-	public void init() throws SecurityException {
-		this.defaultLocale = Locale.getDefault();
-		Locale.setDefault(Locale.ENGLISH);
+	@BeforeEach
+	void init() throws SecurityException {
 		this.logger = Logger.getLogger(getClass().getName());
 	}
 
-	@After
-	public void clearLocale() {
-		Locale.setDefault(this.defaultLocale);
-	}
-
-	@After
-	public void resetLogger() {
+	@AfterEach
+	void resetLogger() {
 		this.logger.setLevel(Level.OFF);
 	}
 
 	@Test
-	public void noFile() {
+	void noFile(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.logger.info("Hidden");
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		assertThat(output).contains("Hello world").doesNotContain("Hidden");
 		assertThat(new File(tmpDir() + "/spring.log").exists()).isFalse();
 	}
 
 	@Test
-	public void withFile() {
+	void withFile(CapturedOutput output) {
 		File temp = new File(tmpDir());
 		File[] logFiles = temp.listFiles(SPRING_LOG_FILTER);
 		for (File file : logFiles) {
@@ -100,66 +94,62 @@ public void withFile() {
 		this.logger.info("Hidden");
 		this.loggingSystem.initialize(null, null, getLogFile(null, tmpDir()));
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		assertThat(output).contains("Hello world").doesNotContain("Hidden");
-		assertThat(temp.listFiles(SPRING_LOG_FILTER).length).isGreaterThan(0);
+		assertThat(temp.listFiles(SPRING_LOG_FILTER)).hasSizeGreaterThan(0);
 	}
 
 	@Test
-	public void testCustomFormatter() {
+	void testCustomFormatter(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		assertThat(output).contains("Hello world").contains("???? INFO [");
 	}
 
 	@Test
-	public void testSystemPropertyInitializesFormat() {
+	void testSystemPropertyInitializesFormat(CapturedOutput output) {
 		System.setProperty(LoggingSystemProperties.PID_KEY, "1234");
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null,
 				"classpath:" + ClassUtils.addResourcePathToPackagePath(getClass(), "logging.properties"), null);
 		this.logger.info("Hello world");
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		assertThat(output).contains("Hello world").contains("1234 INFO [");
 	}
 
 	@Test
-	public void testNonDefaultConfigLocation() {
+	void testNonDefaultConfigLocation(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, "classpath:logging-nondefault.properties", null);
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		assertThat(output).contains("INFO: Hello");
 	}
 
 	@Test
-	public void testNonexistentConfigLocation() {
+	void testNonexistentConfigLocation() {
 		this.loggingSystem.beforeInitialize();
 		assertThatIllegalStateException().isThrownBy(
 				() -> this.loggingSystem.initialize(null, "classpath:logging-nonexistent.properties", null));
 	}
 
 	@Test
-	public void getSupportedLevels() {
+	void getSupportedLevels() {
 		assertThat(this.loggingSystem.getSupportedLogLevels()).isEqualTo(
 				EnumSet.of(LogLevel.TRACE, LogLevel.DEBUG, LogLevel.INFO, LogLevel.WARN, LogLevel.ERROR, LogLevel.OFF));
 	}
 
 	@Test
-	public void setLevel() {
+	void setLevel(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.fine("Hello");
 		this.loggingSystem.setLogLevel("org.springframework.boot", LogLevel.DEBUG);
 		this.logger.fine("Hello");
-		assertThat(StringUtils.countOccurrencesOf(this.output.toString(), "Hello")).isEqualTo(1);
+		assertThat(StringUtils.countOccurrencesOf(output.toString(), "Hello")).isEqualTo(1);
 	}
 
 	@Test
-	public void setLevelToNull() {
+	void setLevelToNull(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.fine("Hello");
@@ -167,11 +157,11 @@ public void setLevelToNull() {
 		this.logger.fine("Hello");
 		this.loggingSystem.setLogLevel("org.springframework.boot", null);
 		this.logger.fine("Hello");
-		assertThat(StringUtils.countOccurrencesOf(this.output.toString(), "Hello")).isEqualTo(1);
+		assertThat(StringUtils.countOccurrencesOf(output.toString(), "Hello")).isEqualTo(1);
 	}
 
 	@Test
-	public void getLoggingConfigurations() {
+	void getLoggingConfigurations() {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.loggingSystem.setLogLevel(getClass().getName(), LogLevel.DEBUG);
@@ -181,7 +171,7 @@ public void getLoggingConfigurations() {
 	}
 
 	@Test
-	public void getLoggingConfiguration() {
+	void getLoggingConfiguration() {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.loggingSystem.setLogLevel(getClass().getName(), LogLevel.DEBUG);
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystemTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystemTests.java
index df6d0866180..79a839fb9e0 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystemTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystemTests.java
@@ -29,27 +29,23 @@
 import org.apache.logging.log4j.Logger;
 import org.apache.logging.log4j.core.LoggerContext;
 import org.apache.logging.log4j.core.config.Configuration;
-import org.hamcrest.Matcher;
-import org.hamcrest.Matchers;
-import org.junit.After;
-import org.junit.Before;
-import org.junit.Rule;
-import org.junit.Test;
-
-import org.springframework.boot.logging.AbstractLoggingSystemTests;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+import org.junit.jupiter.api.extension.ExtendWith;
+
+import org.springframework.boot.logging.AbstractLoggingSystemJUnit5Tests;
 import org.springframework.boot.logging.LogLevel;
 import org.springframework.boot.logging.LoggerConfiguration;
 import org.springframework.boot.logging.LoggingSystem;
 import org.springframework.boot.logging.LoggingSystemProperties;
-import org.springframework.boot.testsupport.assertj.Matched;
-import org.springframework.boot.testsupport.system.OutputCaptureRule;
+import org.springframework.boot.testsupport.system.CapturedOutput;
+import org.springframework.boot.testsupport.system.OutputCaptureExtension;
 import org.springframework.util.StringUtils;
 
 import static org.assertj.core.api.Assertions.assertThat;
 import static org.assertj.core.api.Assertions.assertThatIllegalStateException;
 import static org.assertj.core.api.Assertions.contentOf;
-import static org.hamcrest.Matchers.containsString;
-import static org.hamcrest.Matchers.not;
 import static org.mockito.ArgumentMatchers.any;
 import static org.mockito.Mockito.mock;
 import static org.mockito.Mockito.times;
@@ -63,35 +59,30 @@
  * @author Andy Wilkinson
  * @author Ben Hale
  */
-public class Log4J2LoggingSystemTests extends AbstractLoggingSystemTests {
-
-	@Rule
-	public OutputCaptureRule output = new OutputCaptureRule();
+@ExtendWith(OutputCaptureExtension.class)
+class Log4J2LoggingSystemTests extends AbstractLoggingSystemJUnit5Tests {
 
 	private final TestLog4J2LoggingSystem loggingSystem = new TestLog4J2LoggingSystem();
 
 	private Logger logger;
 
-	@Before
-	public void setup() {
+	@BeforeEach
+	void setup() {
 		this.loggingSystem.cleanUp();
 		this.logger = LogManager.getLogger(getClass());
 	}
 
-	@Override
-	@After
-	public void clear() {
-		super.clear();
+	@AfterEach
+	void cleanUp() {
 		this.loggingSystem.cleanUp();
 	}
 
 	@Test
-	public void noFile() {
+	void noFile(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.logger.info("Hidden");
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		Configuration configuration = this.loggingSystem.getConfiguration();
 		assertThat(output).contains("Hello world").doesNotContain("Hidden");
 		assertThat(new File(tmpDir() + "/spring.log").exists()).isFalse();
@@ -99,12 +90,11 @@ public void noFile() {
 	}
 
 	@Test
-	public void withFile() {
+	void withFile(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.logger.info("Hidden");
 		this.loggingSystem.initialize(null, null, getLogFile(null, tmpDir()));
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		Configuration configuration = this.loggingSystem.getConfiguration();
 		assertThat(output).contains("Hello world").doesNotContain("Hidden");
 		assertThat(new File(tmpDir() + "/spring.log").exists()).isTrue();
@@ -112,11 +102,10 @@ public void withFile() {
 	}
 
 	@Test
-	public void testNonDefaultConfigLocation() {
+	void testNonDefaultConfigLocation(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, "classpath:log4j2-nondefault.xml", getLogFile(tmpDir() + "/tmp.log", null));
 		this.logger.info("Hello world");
-		String output = this.output.toString().trim();
 		Configuration configuration = this.loggingSystem.getConfiguration();
 		assertThat(output).contains("Hello world").contains(tmpDir() + "/tmp.log");
 		assertThat(new File(tmpDir() + "/tmp.log").exists()).isFalse();
@@ -126,29 +115,29 @@ public void testNonDefaultConfigLocation() {
 	}
 
 	@Test
-	public void testNonexistentConfigLocation() {
+	void testNonexistentConfigLocation() {
 		this.loggingSystem.beforeInitialize();
 		assertThatIllegalStateException()
 				.isThrownBy(() -> this.loggingSystem.initialize(null, "classpath:log4j2-nonexistent.xml", null));
 	}
 
 	@Test
-	public void getSupportedLevels() {
+	void getSupportedLevels() {
 		assertThat(this.loggingSystem.getSupportedLogLevels()).isEqualTo(EnumSet.allOf(LogLevel.class));
 	}
 
 	@Test
-	public void setLevel() {
+	void setLevel(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.debug("Hello");
 		this.loggingSystem.setLogLevel("org.springframework.boot", LogLevel.DEBUG);
 		this.logger.debug("Hello");
-		assertThat(StringUtils.countOccurrencesOf(this.output.toString(), "Hello")).isEqualTo(1);
+		assertThat(StringUtils.countOccurrencesOf(output.toString(), "Hello")).isEqualTo(1);
 	}
 
 	@Test
-	public void setLevelToNull() {
+	void setLevelToNull(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.logger.debug("Hello");
@@ -156,11 +145,11 @@ public void setLevelToNull() {
 		this.logger.debug("Hello");
 		this.loggingSystem.setLogLevel("org.springframework.boot", null);
 		this.logger.debug("Hello");
-		assertThat(StringUtils.countOccurrencesOf(this.output.toString(), "Hello")).isEqualTo(1);
+		assertThat(StringUtils.countOccurrencesOf(output.toString(), "Hello")).isEqualTo(1);
 	}
 
 	@Test
-	public void getLoggingConfigurations() {
+	void getLoggingConfigurations() {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.loggingSystem.setLogLevel(getClass().getName(), LogLevel.DEBUG);
@@ -170,7 +159,7 @@ public void getLoggingConfigurations() {
 	}
 
 	@Test
-	public void getLoggingConfiguration() {
+	void getLoggingConfiguration() {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		this.loggingSystem.setLogLevel(getClass().getName(), LogLevel.DEBUG);
@@ -180,44 +169,43 @@ public void getLoggingConfiguration() {
 	}
 
 	@Test
-	public void setLevelOfUnconfiguredLoggerDoesNotAffectRootConfiguration() {
+	void setLevelOfUnconfiguredLoggerDoesNotAffectRootConfiguration(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		LogManager.getRootLogger().debug("Hello");
 		this.loggingSystem.setLogLevel("foo.bar.baz", LogLevel.DEBUG);
 		LogManager.getRootLogger().debug("Hello");
-		assertThat(this.output.toString()).doesNotContain("Hello");
+		assertThat(output.toString()).doesNotContain("Hello");
 	}
 
 	@Test
-	public void loggingThatUsesJulIsCaptured() {
+	void loggingThatUsesJulIsCaptured(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, null);
 		java.util.logging.Logger julLogger = java.util.logging.Logger.getLogger(getClass().getName());
 		julLogger.severe("Hello world");
-		String output = this.output.toString().trim();
 		assertThat(output).contains("Hello world");
 	}
 
 	@Test
-	public void configLocationsWithNoExtraDependencies() {
+	void configLocationsWithNoExtraDependencies() {
 		assertThat(this.loggingSystem.getStandardConfigLocations()).contains("log4j2.properties", "log4j2.xml");
 	}
 
 	@Test
-	public void configLocationsWithJacksonDatabind() {
+	void configLocationsWithJacksonDatabind() {
 		this.loggingSystem.availableClasses(ObjectMapper.class.getName());
 		assertThat(this.loggingSystem.getStandardConfigLocations()).contains("log4j2.json", "log4j2.jsn", "log4j2.xml");
 	}
 
 	@Test
-	public void configLocationsWithJacksonDataformatYaml() {
+	void configLocationsWithJacksonDataformatYaml() {
 		this.loggingSystem.availableClasses("com.fasterxml.jackson.dataformat.yaml.YAMLParser");
 		assertThat(this.loggingSystem.getStandardConfigLocations()).contains("log4j2.yaml", "log4j2.yml", "log4j2.xml");
 	}
 
 	@Test
-	public void configLocationsWithJacksonDatabindAndDataformatYaml() {
+	void configLocationsWithJacksonDatabindAndDataformatYaml() {
 		this.loggingSystem.availableClasses("com.fasterxml.jackson.dataformat.yaml.YAMLParser",
 				ObjectMapper.class.getName());
 		assertThat(this.loggingSystem.getStandardConfigLocations()).contains("log4j2.yaml", "log4j2.yml", "log4j2.json",
@@ -225,42 +213,39 @@ public void configLocationsWithJacksonDatabindAndDataformatYaml() {
 	}
 
 	@Test
-	public void springConfigLocations() {
+	void springConfigLocations() {
 		String[] locations = getSpringConfigLocations(this.loggingSystem);
 		assertThat(locations).containsExactly("log4j2-spring.properties", "log4j2-spring.xml");
 	}
 
 	@Test
-	public void exceptionsIncludeClassPackaging() {
+	void exceptionsIncludeClassPackaging(CapturedOutput output) {
 		this.loggingSystem.beforeInitialize();
 		this.loggingSystem.initialize(null, null, getLogFile(null, tmpDir()));
-		Matcher<String> expectedOutput = containsString("[junit-");
-		this.output.expect(expectedOutput);
 		this.logger.warn("Expected exception", new RuntimeException("Expected"));
 		String fileContents = contentOf(new File(tmpDir() + "/spring.log"));
-		assertThat(fileContents).is(Matched.by(expectedOutput));
+		assertThat(fileContents).contains("[junit-");
+		assertThat(output).contains("[junit-");
 	}
 
 	@Test
-	public void beforeInitializeFilterDisablesErrorLogging() {
+	void beforeInitializeFilterDisablesErrorLogging() {
 		this.loggingSystem.beforeInitialize();
 		assertThat(this.logger.isErrorEnabled()).isFalse();
 		this.loggingSystem.initialize(null, null, getLogFile(null, tmpDir()));
 	}
 
 	@Test
-	public void customExceptionConversionWord() {
+	void customExceptionConversionWord(CapturedOutput output) {
 		System.setProperty(LoggingSystemProperties.EXCEPTION_CONVERSION_WORD, "%ex");
 		try {
 			this.loggingSystem.beforeInitialize();
 			this.logger.info("Hidden");
 			this.loggingSystem.initialize(null, null, getLogFile(null, tmpDir()));
-			Matcher<String> expectedOutput = Matchers.allOf(containsString("java.lang.RuntimeException: Expected"),
-					not(containsString("Wrapped by:")));
-			this.output.expect(expectedOutput);
 			this.logger.warn("Expected exception", new RuntimeException("Expected", new RuntimeException("Cause")));
 			String fileContents = contentOf(new File(tmpDir() + "/spring.log"));
-			assertThat(fileContents).is(Matched.by(expectedOutput));
+			assertThat(fileContents).contains("java.lang.RuntimeException: Expected").doesNotContain("Wrapped by:");
+			assertThat(output).contains("java.lang.RuntimeException: Expected").doesNotContain("Wrapped by:");
 		}
 		finally {
 			System.clearProperty(LoggingSystemProperties.EXCEPTION_CONVERSION_WORD);
@@ -268,7 +253,7 @@ public void customExceptionConversionWord() {
 	}
 
 	@Test
-	public void initializationIsOnlyPerformedOnceUntilCleanedUp() {
+	void initializationIsOnlyPerformedOnceUntilCleanedUp() {
 		LoggerContext loggerContext = (LoggerContext) LogManager.getContext(false);
 		PropertyChangeListener listener = mock(PropertyChangeListener.class);
 		loggerContext.addPropertyChangeListener(listener);
diff --git a/src/checkstyle/checkstyle-suppressions.xml b/src/checkstyle/checkstyle-suppressions.xml
index b09c33e376a..a1c43a2181d 100644
--- a/src/checkstyle/checkstyle-suppressions.xml
+++ b/src/checkstyle/checkstyle-suppressions.xml
@@ -42,7 +42,5 @@
 	<suppress files="ModifiedClassPathRunnerExclusionsTests" checks="SpringJUnit5" />
 	<suppress files="[\\/]src[\\/]test[\\/]java[\\/]org[\\/]springframework[\\/]boot[\\/]test[\\/]rule[\\/]" checks="SpringJUnit5" />
 	<suppress files="OutputCaptureRuleTests" checks="SpringJUnit5" />
-	<suppress files="JavaLoggingSystemTests" checks="SpringJUnit5" />
-	<suppress files="Log4J2LoggingSystemTests" checks="SpringJUnit5" />
 	<suppress files="RestClientTestWithComponentIntegrationTests" checks="SpringJUnit5" />
 </suppressions>
