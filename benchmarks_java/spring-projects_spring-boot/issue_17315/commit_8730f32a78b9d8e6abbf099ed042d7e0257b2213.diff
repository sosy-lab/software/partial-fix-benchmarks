diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactory.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactory.java
index db6ac05e244..2932474b528 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactory.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactory.java
@@ -78,6 +78,8 @@
 
 	private Collection<TomcatProtocolHandlerCustomizer<?>> tomcatProtocolHandlerCustomizers = new LinkedHashSet<>();
 
+	private final List<Connector> additionalTomcatConnectors = new ArrayList<>();
+
 	private String protocol = DEFAULT_PROTOCOL;
 
 	private Charset uriEncoding = DEFAULT_CHARSET;
@@ -122,6 +124,9 @@ public WebServer getWebServer(HttpHandler httpHandler) {
 		tomcat.setConnector(connector);
 		tomcat.getHost().setAutoDeploy(false);
 		configureEngine(tomcat.getEngine());
+		for (Connector additionalConnector : this.additionalTomcatConnectors) {
+			tomcat.getService().addConnector(additionalConnector);
+		}
 		TomcatHttpHandlerAdapter servlet = new TomcatHttpHandlerAdapter(httpHandler);
 		prepareContext(tomcat.getHost(), servlet);
 		return new TomcatWebServer(tomcat, getPort() >= 0);
@@ -317,6 +322,24 @@ public void addProtocolHandlerCustomizers(TomcatProtocolHandlerCustomizer<?>...
 		return this.tomcatProtocolHandlerCustomizers;
 	}
 
+	/**
+	 * Add {@link Connector}s in addition to the default connector, e.g. for SSL or AJP
+	 * @param connectors the connectors to add
+	 */
+	public void addAdditionalTomcatConnectors(Connector... connectors) {
+		Assert.notNull(connectors, "Connectors must not be null");
+		this.additionalTomcatConnectors.addAll(Arrays.asList(connectors));
+	}
+
+	/**
+	 * Returns a mutable collection of the {@link Connector}s that will be added to the
+	 * Tomcat.
+	 * @return the additionalTomcatConnectors
+	 */
+	public List<Connector> getAdditionalTomcatConnectors() {
+		return this.additionalTomcatConnectors;
+	}
+
 	@Override
 	public void addEngineValves(Valve... engineValves) {
 		Assert.notNull(engineValves, "Valves must not be null");
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java
index 348580b685a..84a04166bec 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/web/embedded/tomcat/TomcatReactiveWebServerFactoryTests.java
@@ -20,10 +20,12 @@
 import java.net.InetSocketAddress;
 import java.net.ServerSocket;
 import java.util.Arrays;
+import java.util.Map;
 
 import org.apache.catalina.Context;
 import org.apache.catalina.LifecycleEvent;
 import org.apache.catalina.LifecycleListener;
+import org.apache.catalina.Service;
 import org.apache.catalina.connector.Connector;
 import org.apache.catalina.core.AprLifecycleListener;
 import org.apache.catalina.core.StandardContext;
@@ -175,6 +177,24 @@ void tomcatProtocolHandlerCustomizersShouldBeInvoked() {
 		}
 	}
 
+	@Test
+	void tomcatAdditionalConnectors() {
+		TomcatReactiveWebServerFactory factory = getFactory();
+		Connector[] connectors = new Connector[4];
+		Arrays.setAll(connectors, (i) -> new Connector());
+		factory.addAdditionalTomcatConnectors(connectors);
+		this.webServer = factory.getWebServer(mock(HttpHandler.class));
+		Map<Service, Connector[]> connectorsByService = ((TomcatWebServer) this.webServer).getServiceConnectors();
+		assertThat(connectorsByService.values().iterator().next().length).isEqualTo(connectors.length + 1);
+	}
+
+	@Test
+	void addNullAdditionalConnectorsThrows() {
+		TomcatReactiveWebServerFactory factory = getFactory();
+		assertThatIllegalArgumentException().isThrownBy(() -> factory.addAdditionalTomcatConnectors((Connector[]) null))
+				.withMessageContaining("Connectors must not be null");
+	}
+
 	@Test
 	void useForwardedHeaders() {
 		TomcatReactiveWebServerFactory factory = getFactory();
