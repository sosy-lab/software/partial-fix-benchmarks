diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfiguration.java
index 2c2ba9db1cf..7d265c8ef38 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfiguration.java
@@ -30,6 +30,7 @@
 
 import org.springframework.beans.factory.ObjectProvider;
 import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
 import org.springframework.boot.context.properties.EnableConfigurationProperties;
@@ -92,4 +93,28 @@ public RestHighLevelClient restHighLevelClient(RestClientBuilder restClientBuild
 
 	}
 
+	/**
+	 * Configuration to configure a {@link RestClient} bean from a
+	 * {@link RestHighLevelClient} if such a bean has been registered by the application.
+	 * If {@link RestHighLevelClient} is not unique or does not exist then
+	 * {@link RestClientBuilder#build()} will be used.
+	 */
+	@Configuration(proxyBeanMethods = false)
+	@ConditionalOnClass(RestHighLevelClient.class)
+	@ConditionalOnBean(RestHighLevelClient.class)
+	public static class RestClientConfiguration {
+
+		@Bean
+		@ConditionalOnMissingBean
+		public RestClient restClient(ObjectProvider<RestHighLevelClient> restHighLevelClient,
+				RestClientBuilder builder) {
+			RestHighLevelClient client = restHighLevelClient.getIfUnique();
+			if (client != null) {
+				return client.getLowLevelClient();
+			}
+			return builder.build();
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationTests.java
index 953f94b4b3b..eb9438b2f1f 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationTests.java
@@ -56,8 +56,11 @@
 
 	@Test
 	void configureShouldCreateBothRestClientVariants() {
-		this.contextRunner.run((context) -> assertThat(context).hasSingleBean(RestClient.class)
-				.hasSingleBean(RestHighLevelClient.class));
+		this.contextRunner.run((context) -> {
+			assertThat(context).hasSingleBean(RestClient.class).hasSingleBean(RestHighLevelClient.class);
+			RestHighLevelClient restHighLevelClient = context.getBean(RestHighLevelClient.class);
+			assertThat(restHighLevelClient.getLowLevelClient()).isNotSameAs(context.getBean(RestClient.class));
+		});
 	}
 
 	@Test
@@ -66,6 +69,27 @@ void configureWhenCustomClientShouldBackOff() {
 				.run((context) -> assertThat(context).hasSingleBean(RestClient.class).hasBean("customRestClient"));
 	}
 
+	@Test
+	void configureWhenCustomRestHighLevelClientShouldBackOff() {
+		this.contextRunner.withUserConfiguration(CustomRestHighLevelClientConfiguration.class).run((context) -> {
+			assertThat(context).hasSingleBean(RestClient.class).hasSingleBean(RestHighLevelClient.class);
+			RestHighLevelClient restHighLevelClient = context.getBean(RestHighLevelClient.class);
+			assertThat(restHighLevelClient.getLowLevelClient()).isSameAs(context.getBean(RestClient.class));
+		});
+	}
+
+	@Test
+	void configureWhenDefaultRestClientShouldCreateWhenNoUniqueRestHighLevelClient() {
+		this.contextRunner.withUserConfiguration(TwoCustomRestHighLevelClientConfiguration.class).run((context) -> {
+			assertThat(context).hasSingleBean(RestClient.class);
+			Map<String, RestHighLevelClient> restHighLevelClients = context.getBeansOfType(RestHighLevelClient.class);
+			assertThat(restHighLevelClients).isNotEmpty();
+			for (RestHighLevelClient restHighLevelClient : restHighLevelClients.values()) {
+				assertThat(restHighLevelClient.getLowLevelClient()).isNotSameAs(context.getBean(RestClient.class));
+			}
+		});
+	}
+
 	@Test
 	void configureWhenBuilderCustomizerShouldApply() {
 		this.contextRunner.withUserConfiguration(BuilderCustomizerConfiguration.class).run((context) -> {
@@ -138,4 +162,29 @@ RestClientBuilderCustomizer myCustomizer() {
 
 	}
 
+	@Configuration(proxyBeanMethods = false)
+	static class CustomRestHighLevelClientConfiguration {
+
+		@Bean
+		RestHighLevelClient customRestHighLevelClient(RestClientBuilder builder) {
+			return new RestHighLevelClient(builder);
+		}
+
+	}
+
+	@Configuration(proxyBeanMethods = false)
+	static class TwoCustomRestHighLevelClientConfiguration {
+
+		@Bean
+		RestHighLevelClient customRestHighLevelClient(RestClientBuilder builder) {
+			return new RestHighLevelClient(builder);
+		}
+
+		@Bean
+		RestHighLevelClient customRestHighLevelClient1(RestClientBuilder builder) {
+			return new RestHighLevelClient(builder);
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationWithoutRestHighLevelClientTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationWithoutRestHighLevelClientTests.java
new file mode 100644
index 00000000000..92d119e096c
--- /dev/null
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/rest/RestClientAutoConfigurationWithoutRestHighLevelClientTests.java
@@ -0,0 +1,47 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.elasticsearch.rest;
+
+import org.elasticsearch.client.RestClient;
+import org.elasticsearch.client.RestHighLevelClient;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.test.context.FilteredClassLoader;
+import org.springframework.boot.test.context.runner.ApplicationContextRunner;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link RestClientAutoConfiguration} when {@link RestHighLevelClient} is not
+ * on the classpath.
+ *
+ * @author Dmytro Nosan
+ */
+class RestClientAutoConfigurationWithoutRestHighLevelClientTests {
+
+	private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
+			.withConfiguration(AutoConfigurations.of(RestClientAutoConfiguration.class))
+			.withClassLoader(new FilteredClassLoader(RestHighLevelClient.class));
+
+	@Test
+	void shouldCreateRestClientOnly() {
+		this.contextRunner.run((context) -> assertThat(context).hasSingleBean(RestClient.class)
+				.doesNotHaveBean(RestHighLevelClient.class));
+	}
+
+}
