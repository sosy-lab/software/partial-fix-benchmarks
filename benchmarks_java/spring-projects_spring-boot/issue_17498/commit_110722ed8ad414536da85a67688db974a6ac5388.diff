diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfiguration.java
new file mode 100644
index 00000000000..097b7f3bdfa
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfiguration.java
@@ -0,0 +1,60 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.hazelcast;
+
+import java.util.Map;
+
+import com.hazelcast.core.HazelcastInstance;
+
+import org.springframework.boot.actuate.autoconfigure.health.CompositeHealthIndicatorConfiguration;
+import org.springframework.boot.actuate.autoconfigure.health.ConditionalOnEnabledHealthIndicator;
+import org.springframework.boot.actuate.autoconfigure.health.HealthIndicatorAutoConfiguration;
+import org.springframework.boot.actuate.hazelcast.HazelcastHealthIndicator;
+import org.springframework.boot.actuate.health.HealthIndicator;
+import org.springframework.boot.autoconfigure.AutoConfigureAfter;
+import org.springframework.boot.autoconfigure.AutoConfigureBefore;
+import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+
+/**
+ * {@link EnableAutoConfiguration Auto-configuration} for
+ * {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ * @since 2.2.0
+ */
+@Configuration(proxyBeanMethods = false)
+@ConditionalOnClass(HazelcastInstance.class)
+@ConditionalOnBean(HazelcastInstance.class)
+@ConditionalOnEnabledHealthIndicator("hazelcast")
+@AutoConfigureBefore(HealthIndicatorAutoConfiguration.class)
+@AutoConfigureAfter(HazelcastAutoConfiguration.class)
+public class HazelcastHealthIndicatorAutoConfiguration
+		extends CompositeHealthIndicatorConfiguration<HazelcastHealthIndicator, HazelcastInstance> {
+
+	@Bean
+	@ConditionalOnMissingBean(name = "hazelcastHealthIndicator")
+	public HealthIndicator hazelcastHealthIndicator(Map<String, HazelcastInstance> hazelcastInstances) {
+		return createHealthIndicator(hazelcastInstances);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/package-info.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/package-info.java
new file mode 100644
index 00000000000..58d63614e04
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/package-info.java
@@ -0,0 +1,20 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+/**
+ * Auto-configuration for Hazelcast's actuator.
+ */
+package org.springframework.boot.actuate.autoconfigure.hazelcast;
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfigurationTests.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfigurationTests.java
new file mode 100644
index 00000000000..a980c6db548
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfigurationTests.java
@@ -0,0 +1,55 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.hazelcast;
+
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.autoconfigure.health.HealthIndicatorAutoConfiguration;
+import org.springframework.boot.actuate.hazelcast.HazelcastHealthIndicator;
+import org.springframework.boot.actuate.health.ApplicationHealthIndicator;
+import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
+import org.springframework.boot.test.context.runner.ApplicationContextRunner;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link HazelcastHealthIndicatorAutoConfiguration}.
+ *
+ * @author Dmytro Nosan
+ */
+class HazelcastHealthIndicatorAutoConfigurationTests {
+
+	private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
+			.withConfiguration(AutoConfigurations.of(HazelcastAutoConfiguration.class,
+					HazelcastHealthIndicatorAutoConfiguration.class, HealthIndicatorAutoConfiguration.class));
+
+	@Test
+	void runShouldCreateIndicator() {
+		this.contextRunner.run((context) -> assertThat(context).hasSingleBean(HazelcastHealthIndicator.class)
+				.doesNotHaveBean(ApplicationHealthIndicator.class));
+	}
+
+	@Test
+	void runWhenDisabledShouldNotCreateIndicator() {
+		this.contextRunner.withPropertyValues("management.health.hazelcast.enabled:false")
+				.run((context) -> assertThat(context).doesNotHaveBean(HazelcastHealthIndicator.class)
+						.doesNotHaveBean(HazelcastHealthIndicator.class)
+						.hasSingleBean(ApplicationHealthIndicator.class));
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/pom.xml b/spring-boot-project/spring-boot-actuator/pom.xml
index e667322d927..f218b09b859 100644
--- a/spring-boot-project/spring-boot-actuator/pom.xml
+++ b/spring-boot-project/spring-boot-actuator/pom.xml
@@ -41,6 +41,11 @@
 			<artifactId>hazelcast-spring</artifactId>
 			<optional>true</optional>
 		</dependency>
+		<dependency>
+			<groupId>com.hazelcast</groupId>
+			<artifactId>hazelcast-client</artifactId>
+			<scope>test</scope>
+		</dependency>
 		<dependency>
 			<groupId>com.sun.mail</groupId>
 			<artifactId>jakarta.mail</artifactId>
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicator.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicator.java
new file mode 100644
index 00000000000..f913089c884
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicator.java
@@ -0,0 +1,92 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import java.util.LinkedHashMap;
+import java.util.Map;
+
+import com.hazelcast.core.Cluster;
+import com.hazelcast.core.Endpoint;
+import com.hazelcast.core.HazelcastInstance;
+
+import org.springframework.boot.actuate.health.AbstractHealthIndicator;
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.HealthIndicator;
+import org.springframework.util.Assert;
+import org.springframework.util.CollectionUtils;
+
+/**
+ * {@link HealthIndicator} for a Hazelcast.
+ *
+ * @author Dmytro Nosan
+ * @since 2.2.0
+ */
+public class HazelcastHealthIndicator extends AbstractHealthIndicator {
+
+	private static final String EMPTY = "";
+
+	private final HazelcastInstance hazelcast;
+
+	public HazelcastHealthIndicator(HazelcastInstance hazelcast) {
+		super("Hazelcast health check failed");
+		Assert.notNull(hazelcast, "HazelcastInstance must not be null");
+		this.hazelcast = hazelcast;
+	}
+
+	@Override
+	protected void doHealthCheck(Health.Builder builder) {
+		this.hazelcast.executeTransaction((context) -> EMPTY);
+		Map<String, Object> details = new LinkedHashMap<>();
+		put("name", this.hazelcast.getName(), details);
+		put("endpoint", getEndpoint(this.hazelcast.getLocalEndpoint()), details);
+		put("cluster", getCluster(this.hazelcast.getCluster()), details);
+		builder.up().withDetails(details);
+	}
+
+	private static Map<String, Object> getEndpoint(Endpoint endpoint) {
+		Map<String, Object> details = new LinkedHashMap<>();
+		put("uuid", endpoint.getUuid(), details);
+		put("address", endpoint.getSocketAddress(), details);
+		return details;
+	}
+
+	private static Map<String, Object> getCluster(Cluster cluster) {
+		try {
+			Map<String, Object> details = new LinkedHashMap<>();
+			put("time", cluster.getClusterTime(), details);
+			put("state", cluster.getClusterState(), details);
+			put("version", cluster.getClusterVersion(), details);
+			return details;
+		}
+		catch (UnsupportedOperationException ex) {
+			return null;
+		}
+	}
+
+	private static void put(String name, Object value, Map<String, Object> details) {
+		if (value != null) {
+			details.put(name, value.toString());
+		}
+	}
+
+	private static void put(String name, Map<?, ?> value, Map<String, Object> details) {
+		if (!CollectionUtils.isEmpty(value)) {
+			details.put(name, value);
+		}
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/package-info.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/package-info.java
new file mode 100644
index 00000000000..44e785578dc
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/package-info.java
@@ -0,0 +1,20 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+/**
+ * Actuator support for Hazelcast.
+ */
+package org.springframework.boot.actuate.hazelcast;
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorClientTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorClientTests.java
new file mode 100644
index 00000000000..d425d3c316a
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorClientTests.java
@@ -0,0 +1,77 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import com.hazelcast.client.HazelcastClient;
+import com.hazelcast.client.config.ClientConfig;
+import com.hazelcast.config.Config;
+import com.hazelcast.core.Hazelcast;
+import com.hazelcast.core.HazelcastInstance;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.Status;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ */
+class HazelcastHealthIndicatorClientTests {
+
+	private HazelcastInstance hazelcastServer;
+
+	private HazelcastInstance hazelcastClient;
+
+	private HazelcastHealthIndicator healthIndicator;
+
+	@BeforeEach
+	void start() {
+		this.hazelcastServer = Hazelcast.newHazelcastInstance(new Config());
+		this.hazelcastClient = HazelcastClient.newHazelcastClient(new ClientConfig());
+		this.healthIndicator = new HazelcastHealthIndicator(this.hazelcastClient);
+	}
+
+	@AfterEach
+	void stop() {
+		if (this.hazelcastServer != null) {
+			this.hazelcastServer.shutdown();
+		}
+		if (this.hazelcastClient != null) {
+			this.hazelcastClient.shutdown();
+		}
+	}
+
+	@Test
+	void hazelcastIsUp() {
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+		assertThat(health.getDetails()).containsKeys("name", "endpoint");
+	}
+
+	@Test
+	void hazelcastIsDown() {
+		this.hazelcastServer.shutdown();
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.DOWN);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorMockTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorMockTests.java
new file mode 100644
index 00000000000..8822eb6dee0
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorMockTests.java
@@ -0,0 +1,106 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import java.net.InetSocketAddress;
+import java.util.Map;
+import java.util.Objects;
+
+import com.hazelcast.cluster.ClusterState;
+import com.hazelcast.core.Cluster;
+import com.hazelcast.core.Endpoint;
+import com.hazelcast.core.HazelcastInstance;
+import com.hazelcast.version.Version;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.Status;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.mockito.BDDMockito.when;
+import static org.mockito.Mockito.mock;
+
+/**
+ * Tests for {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ */
+@SuppressWarnings("unchecked")
+class HazelcastHealthIndicatorMockTests {
+
+	private final HazelcastInstance hazelcast = mock(HazelcastInstance.class);
+
+	private final Endpoint endpoint = mock(Endpoint.class);
+
+	private final Cluster cluster = mock(Cluster.class);
+
+	private final HazelcastHealthIndicator healthIndicator = new HazelcastHealthIndicator(this.hazelcast);
+
+	@BeforeEach
+	void setUp() {
+		when(this.hazelcast.getLocalEndpoint()).thenReturn(this.endpoint);
+		when(this.hazelcast.getCluster()).thenReturn(this.cluster);
+	}
+
+	@Test
+	void hazelcastClientDetails() {
+		when(this.cluster.getClusterState()).thenThrow(UnsupportedOperationException.class);
+		when(this.endpoint.getUuid()).thenReturn("UUID");
+		when(this.endpoint.getSocketAddress()).thenReturn(new InetSocketAddress("localhost", 80));
+		when(this.hazelcast.getName()).thenReturn("hz.client");
+
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+
+		Map<String, Object> details = health.getDetails();
+		assertThat(details).containsKeys("name", "endpoint").doesNotContainKeys("cluster").containsEntry("name",
+				"hz.client");
+
+		Map<String, Object> endpoint = (Map<String, Object>) details.get("endpoint");
+		assertThat(endpoint).containsEntry("uuid", "UUID").containsEntry("address",
+				Objects.toString(new InetSocketAddress("localhost", 80)));
+
+	}
+
+	@Test
+	void hazelcastServerDetails() {
+		when(this.cluster.getClusterState()).thenReturn(ClusterState.ACTIVE);
+		when(this.cluster.getClusterTime()).thenReturn(1L);
+		when(this.cluster.getClusterVersion()).thenReturn(Version.UNKNOWN);
+		when(this.endpoint.getUuid()).thenReturn("UUID");
+		when(this.endpoint.getSocketAddress()).thenReturn(new InetSocketAddress("localhost", 80));
+		when(this.hazelcast.getName()).thenReturn("hz.server");
+
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+
+		Map<String, Object> details = health.getDetails();
+		assertThat(details).containsKeys("name", "endpoint", "cluster").containsEntry("name", "hz.server");
+
+		Map<String, Object> endpoint = (Map<String, Object>) details.get("endpoint");
+		assertThat(endpoint).containsEntry("uuid", "UUID").containsEntry("address",
+				Objects.toString(new InetSocketAddress("localhost", 80)));
+
+		Map<String, Object> cluster = (Map<String, Object>) details.get("cluster");
+		assertThat(cluster).containsEntry("state", Objects.toString(ClusterState.ACTIVE))
+				.containsEntry("time", Objects.toString(1L))
+				.containsEntry("version", Objects.toString(Version.UNKNOWN));
+
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorServerTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorServerTests.java
new file mode 100644
index 00000000000..33d1fc4cea2
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorServerTests.java
@@ -0,0 +1,69 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import com.hazelcast.config.Config;
+import com.hazelcast.core.Hazelcast;
+import com.hazelcast.core.HazelcastInstance;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.Status;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ */
+class HazelcastHealthIndicatorServerTests {
+
+	private HazelcastInstance hazelcastServer;
+
+	private HazelcastHealthIndicator healthIndicator;
+
+	@BeforeEach
+	void start() {
+		this.hazelcastServer = Hazelcast.newHazelcastInstance(new Config());
+		this.healthIndicator = new HazelcastHealthIndicator(this.hazelcastServer);
+	}
+
+	@AfterEach
+	void stop() {
+		if (this.hazelcastServer != null) {
+			this.hazelcastServer.shutdown();
+		}
+	}
+
+	@Test
+	void hazelcastIsUp() {
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+		assertThat(health.getDetails()).containsKeys("name", "endpoint", "cluster");
+	}
+
+	@Test
+	void hazelcastIsDown() {
+		this.hazelcastServer.shutdown();
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.DOWN);
+	}
+
+}
