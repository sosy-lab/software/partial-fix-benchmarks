diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfiguration.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfiguration.java
new file mode 100644
index 00000000000..097b7f3bdfa
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfiguration.java
@@ -0,0 +1,60 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.hazelcast;
+
+import java.util.Map;
+
+import com.hazelcast.core.HazelcastInstance;
+
+import org.springframework.boot.actuate.autoconfigure.health.CompositeHealthIndicatorConfiguration;
+import org.springframework.boot.actuate.autoconfigure.health.ConditionalOnEnabledHealthIndicator;
+import org.springframework.boot.actuate.autoconfigure.health.HealthIndicatorAutoConfiguration;
+import org.springframework.boot.actuate.hazelcast.HazelcastHealthIndicator;
+import org.springframework.boot.actuate.health.HealthIndicator;
+import org.springframework.boot.autoconfigure.AutoConfigureAfter;
+import org.springframework.boot.autoconfigure.AutoConfigureBefore;
+import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+
+/**
+ * {@link EnableAutoConfiguration Auto-configuration} for
+ * {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ * @since 2.2.0
+ */
+@Configuration(proxyBeanMethods = false)
+@ConditionalOnClass(HazelcastInstance.class)
+@ConditionalOnBean(HazelcastInstance.class)
+@ConditionalOnEnabledHealthIndicator("hazelcast")
+@AutoConfigureBefore(HealthIndicatorAutoConfiguration.class)
+@AutoConfigureAfter(HazelcastAutoConfiguration.class)
+public class HazelcastHealthIndicatorAutoConfiguration
+		extends CompositeHealthIndicatorConfiguration<HazelcastHealthIndicator, HazelcastInstance> {
+
+	@Bean
+	@ConditionalOnMissingBean(name = "hazelcastHealthIndicator")
+	public HealthIndicator hazelcastHealthIndicator(Map<String, HazelcastInstance> hazelcastInstances) {
+		return createHealthIndicator(hazelcastInstances);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/package-info.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/package-info.java
new file mode 100644
index 00000000000..58d63614e04
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/main/java/org/springframework/boot/actuate/autoconfigure/hazelcast/package-info.java
@@ -0,0 +1,20 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+/**
+ * Auto-configuration for Hazelcast's actuator.
+ */
+package org.springframework.boot.actuate.autoconfigure.hazelcast;
diff --git a/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfigurationTests.java b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfigurationTests.java
new file mode 100644
index 00000000000..a980c6db548
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator-autoconfigure/src/test/java/org/springframework/boot/actuate/autoconfigure/hazelcast/HazelcastHealthIndicatorAutoConfigurationTests.java
@@ -0,0 +1,55 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.autoconfigure.hazelcast;
+
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.autoconfigure.health.HealthIndicatorAutoConfiguration;
+import org.springframework.boot.actuate.hazelcast.HazelcastHealthIndicator;
+import org.springframework.boot.actuate.health.ApplicationHealthIndicator;
+import org.springframework.boot.autoconfigure.AutoConfigurations;
+import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
+import org.springframework.boot.test.context.runner.ApplicationContextRunner;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link HazelcastHealthIndicatorAutoConfiguration}.
+ *
+ * @author Dmytro Nosan
+ */
+class HazelcastHealthIndicatorAutoConfigurationTests {
+
+	private ApplicationContextRunner contextRunner = new ApplicationContextRunner()
+			.withConfiguration(AutoConfigurations.of(HazelcastAutoConfiguration.class,
+					HazelcastHealthIndicatorAutoConfiguration.class, HealthIndicatorAutoConfiguration.class));
+
+	@Test
+	void runShouldCreateIndicator() {
+		this.contextRunner.run((context) -> assertThat(context).hasSingleBean(HazelcastHealthIndicator.class)
+				.doesNotHaveBean(ApplicationHealthIndicator.class));
+	}
+
+	@Test
+	void runWhenDisabledShouldNotCreateIndicator() {
+		this.contextRunner.withPropertyValues("management.health.hazelcast.enabled:false")
+				.run((context) -> assertThat(context).doesNotHaveBean(HazelcastHealthIndicator.class)
+						.doesNotHaveBean(HazelcastHealthIndicator.class)
+						.hasSingleBean(ApplicationHealthIndicator.class));
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/pom.xml b/spring-boot-project/spring-boot-actuator/pom.xml
index e667322d927..f218b09b859 100644
--- a/spring-boot-project/spring-boot-actuator/pom.xml
+++ b/spring-boot-project/spring-boot-actuator/pom.xml
@@ -41,6 +41,11 @@
 			<artifactId>hazelcast-spring</artifactId>
 			<optional>true</optional>
 		</dependency>
+		<dependency>
+			<groupId>com.hazelcast</groupId>
+			<artifactId>hazelcast-client</artifactId>
+			<scope>test</scope>
+		</dependency>
 		<dependency>
 			<groupId>com.sun.mail</groupId>
 			<artifactId>jakarta.mail</artifactId>
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicator.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicator.java
new file mode 100644
index 00000000000..2b51af85e14
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicator.java
@@ -0,0 +1,129 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import java.net.InetSocketAddress;
+import java.net.UnknownHostException;
+import java.util.Collection;
+import java.util.LinkedHashMap;
+import java.util.Map;
+
+import com.hazelcast.core.Client;
+import com.hazelcast.core.Cluster;
+import com.hazelcast.core.Endpoint;
+import com.hazelcast.core.HazelcastInstance;
+import com.hazelcast.core.Member;
+import com.hazelcast.nio.Address;
+
+import org.springframework.boot.actuate.health.AbstractHealthIndicator;
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.HealthIndicator;
+import org.springframework.util.Assert;
+import org.springframework.util.CollectionUtils;
+
+/**
+ * {@link HealthIndicator} for a Hazelcast.
+ *
+ * @author Dmytro Nosan
+ * @since 2.2.0
+ */
+public class HazelcastHealthIndicator extends AbstractHealthIndicator {
+
+	private static final String EMPTY = "";
+
+	private final HazelcastInstance hazelcast;
+
+	public HazelcastHealthIndicator(HazelcastInstance hazelcast) {
+		super("Hazelcast health check failed");
+		Assert.notNull(hazelcast, "HazelcastInstance must not be null");
+		this.hazelcast = hazelcast;
+	}
+
+	@Override
+	protected void doHealthCheck(Health.Builder builder) {
+		this.hazelcast.executeTransaction((context) -> EMPTY);
+		Map<String, Object> details = new LinkedHashMap<>();
+		put("name", this.hazelcast.getName(), details);
+		Endpoint endpoint = this.hazelcast.getLocalEndpoint();
+		if (endpoint instanceof Member) {
+			put("member", getMember(((Member) endpoint)), details);
+			put("cluster", getCluster(this.hazelcast.getCluster()), details);
+		}
+		else if (endpoint instanceof Client) {
+			put("client", getClient(((Client) endpoint)), details);
+		}
+		builder.up().withDetails(details);
+	}
+
+	private Map<String, Object> getClient(Client client) {
+		Map<String, Object> details = new LinkedHashMap<>();
+		put("uuid", client.getUuid(), details);
+		put("address", client.getSocketAddress(), details);
+		put("labels", client.getLabels(), details);
+		put("type", client.getClientType(), details);
+		put("name", client.getName(), details);
+		return details;
+	}
+
+	private static Map<String, Object> getMember(Member member) {
+		Map<String, Object> details = new LinkedHashMap<>();
+		put("uuid", member.getUuid(), details);
+		put("address", getSocketAddress(member), details);
+		put("version", member.getVersion(), details);
+		return details;
+	}
+
+	private static Map<String, Object> getCluster(Cluster cluster) {
+		Map<String, Object> details = new LinkedHashMap<>();
+		put("time", cluster.getClusterTime(), details);
+		put("state", cluster.getClusterState(), details);
+		put("version", cluster.getClusterVersion(), details);
+		return details;
+	}
+
+	private static void put(String name, Object value, Map<String, Object> details) {
+		if (value != null) {
+			details.put(name, value.toString());
+		}
+	}
+
+	private static void put(String name, Collection<?> value, Map<String, Object> details) {
+		if (!CollectionUtils.isEmpty(value)) {
+			details.put(name, value);
+		}
+	}
+
+	private static void put(String name, Map<?, ?> value, Map<String, Object> details) {
+		if (!CollectionUtils.isEmpty(value)) {
+			details.put(name, value);
+		}
+	}
+
+	private static InetSocketAddress getSocketAddress(Member member) {
+		try {
+			Address address = member.getAddress();
+			if (address != null) {
+				return address.getInetSocketAddress();
+			}
+		}
+		catch (UnknownHostException ex) {
+			// ignore
+		}
+		return null;
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/package-info.java b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/package-info.java
new file mode 100644
index 00000000000..44e785578dc
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/hazelcast/package-info.java
@@ -0,0 +1,20 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+/**
+ * Actuator support for Hazelcast.
+ */
+package org.springframework.boot.actuate.hazelcast;
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorClientTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorClientTests.java
new file mode 100644
index 00000000000..92ec0e71e72
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorClientTests.java
@@ -0,0 +1,77 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import com.hazelcast.client.HazelcastClient;
+import com.hazelcast.client.config.ClientConfig;
+import com.hazelcast.config.Config;
+import com.hazelcast.core.Hazelcast;
+import com.hazelcast.core.HazelcastInstance;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.Status;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ */
+class HazelcastHealthIndicatorClientTests {
+
+	private HazelcastInstance hazelcastServer;
+
+	private HazelcastInstance hazelcastClient;
+
+	private HazelcastHealthIndicator healthIndicator;
+
+	@BeforeEach
+	void start() {
+		this.hazelcastServer = Hazelcast.newHazelcastInstance(new Config());
+		this.hazelcastClient = HazelcastClient.newHazelcastClient(new ClientConfig());
+		this.healthIndicator = new HazelcastHealthIndicator(this.hazelcastClient);
+	}
+
+	@AfterEach
+	void stop() {
+		if (this.hazelcastServer != null) {
+			this.hazelcastServer.shutdown();
+		}
+		if (this.hazelcastClient != null) {
+			this.hazelcastClient.shutdown();
+		}
+	}
+
+	@Test
+	void hazelcastIsUp() {
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+		assertThat(health.getDetails()).containsKeys("name", "client").doesNotContainKeys("member", "cluster");
+	}
+
+	@Test
+	void hazelcastIsDown() {
+		this.hazelcastServer.shutdown();
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.DOWN);
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorMockTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorMockTests.java
new file mode 100644
index 00000000000..1b2151cce8d
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorMockTests.java
@@ -0,0 +1,136 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import java.net.InetSocketAddress;
+import java.net.UnknownHostException;
+import java.util.Collections;
+import java.util.Map;
+
+import com.hazelcast.cluster.ClusterState;
+import com.hazelcast.core.Client;
+import com.hazelcast.core.ClientType;
+import com.hazelcast.core.Cluster;
+import com.hazelcast.core.HazelcastInstance;
+import com.hazelcast.core.Member;
+import com.hazelcast.nio.Address;
+import com.hazelcast.version.MemberVersion;
+import com.hazelcast.version.Version;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+import org.mockito.InjectMocks;
+import org.mockito.Mock;
+import org.mockito.MockitoAnnotations;
+
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.Status;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.mockito.ArgumentMatchers.any;
+import static org.mockito.BDDMockito.when;
+import static org.mockito.Mockito.verify;
+
+/**
+ * Tests for {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ */
+@SuppressWarnings("unchecked")
+class HazelcastHealthIndicatorMockTests {
+
+	@Mock
+	private HazelcastInstance hazelcast;
+
+	@Mock
+	private Client client;
+
+	@Mock
+	private Member member;
+
+	@Mock
+	private Cluster cluster;
+
+	@InjectMocks
+	private HazelcastHealthIndicator healthIndicator;
+
+	@BeforeEach
+	void initMocks() {
+		MockitoAnnotations.initMocks(this);
+	}
+
+	@AfterEach
+	void verifyInvocations() {
+		verify(this.hazelcast).executeTransaction(any());
+	}
+
+	@Test
+	void hazelcastClientDetails() {
+		when(this.hazelcast.getLocalEndpoint()).thenReturn(this.client);
+		when(this.client.getUuid()).thenReturn("UUID");
+		when(this.client.getName()).thenReturn("hz.client");
+		when(this.client.getClientType()).thenReturn(ClientType.JAVA);
+		when(this.client.getLabels()).thenReturn(Collections.singleton("label"));
+		when(this.client.getSocketAddress()).thenReturn(new InetSocketAddress("localhost", 80));
+		when(this.hazelcast.getName()).thenReturn("hz.client");
+
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+
+		Map<String, Object> details = health.getDetails();
+		assertThat(details).containsKeys("name", "client").doesNotContainKeys("cluster", "member").containsEntry("name",
+				"hz.client");
+
+		Map<String, Object> client = (Map<String, Object>) details.get("client");
+		assertThat(client).containsEntry("uuid", "UUID")
+				.containsEntry("address", new InetSocketAddress("localhost", 80).toString())
+				.containsEntry("labels", Collections.singleton("label")).containsEntry("name", "hz.client")
+				.containsEntry("type", ClientType.JAVA.toString());
+
+	}
+
+	@Test
+	void hazelcastServerDetails() throws UnknownHostException {
+		when(this.hazelcast.getLocalEndpoint()).thenReturn(this.member);
+		when(this.hazelcast.getCluster()).thenReturn(this.cluster);
+		when(this.cluster.getClusterState()).thenReturn(ClusterState.ACTIVE);
+		when(this.cluster.getClusterTime()).thenReturn(1L);
+		when(this.cluster.getClusterVersion()).thenReturn(Version.UNKNOWN);
+		when(this.member.getUuid()).thenReturn("UUID");
+		when(this.member.getAddress()).thenReturn(new Address("localhost", 80));
+		when(this.member.getVersion()).thenReturn(MemberVersion.UNKNOWN);
+		when(this.hazelcast.getName()).thenReturn("hz.server");
+
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+
+		Map<String, Object> details = health.getDetails();
+		assertThat(details).containsKeys("name", "member", "cluster").doesNotContainKeys("client").containsEntry("name",
+				"hz.server");
+
+		Map<String, Object> member = (Map<String, Object>) details.get("member");
+		assertThat(member).containsEntry("uuid", "UUID")
+				.containsEntry("address", new InetSocketAddress("localhost", 80).toString())
+				.containsEntry("version", MemberVersion.UNKNOWN.toString());
+
+		Map<String, Object> cluster = (Map<String, Object>) details.get("cluster");
+		assertThat(cluster).containsEntry("state", ClusterState.ACTIVE.toString()).containsEntry("time", "1")
+				.containsEntry("version", Version.UNKNOWN.toString());
+
+	}
+
+}
diff --git a/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorServerTests.java b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorServerTests.java
new file mode 100644
index 00000000000..ed53ccca3f5
--- /dev/null
+++ b/spring-boot-project/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/hazelcast/HazelcastHealthIndicatorServerTests.java
@@ -0,0 +1,69 @@
+/*
+ * Copyright 2012-2019 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.actuate.hazelcast;
+
+import com.hazelcast.config.Config;
+import com.hazelcast.core.Hazelcast;
+import com.hazelcast.core.HazelcastInstance;
+import org.junit.jupiter.api.AfterEach;
+import org.junit.jupiter.api.BeforeEach;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.boot.actuate.health.Health;
+import org.springframework.boot.actuate.health.Status;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link HazelcastHealthIndicator}.
+ *
+ * @author Dmytro Nosan
+ */
+class HazelcastHealthIndicatorServerTests {
+
+	private HazelcastInstance hazelcastServer;
+
+	private HazelcastHealthIndicator healthIndicator;
+
+	@BeforeEach
+	void start() {
+		this.hazelcastServer = Hazelcast.newHazelcastInstance(new Config());
+		this.healthIndicator = new HazelcastHealthIndicator(this.hazelcastServer);
+	}
+
+	@AfterEach
+	void stop() {
+		if (this.hazelcastServer != null) {
+			this.hazelcastServer.shutdown();
+		}
+	}
+
+	@Test
+	void hazelcastIsUp() {
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.UP);
+		assertThat(health.getDetails()).containsKeys("name", "member", "cluster").doesNotContainKeys("client");
+	}
+
+	@Test
+	void hazelcastIsDown() {
+		this.hazelcastServer.shutdown();
+		Health health = this.healthIndicator.health();
+		assertThat(health.getStatus()).isEqualTo(Status.DOWN);
+	}
+
+}
