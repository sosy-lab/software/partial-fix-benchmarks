diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfiguration.java
index 3ecdab3d241..5613f191503 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfiguration.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2012-2019 the original author or authors.
+ * Copyright 2012-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -74,7 +74,8 @@
 
 	/**
 	 * {@link AnyNestedCondition} that checks that either {@code spring.datasource.type}
-	 * is set or {@link PooledDataSourceAvailableCondition} applies.
+	 * or {@code spring.datasource.url} is set or
+	 * {@link PooledDataSourceAvailableCondition} applies.
 	 */
 	static class PooledDataSourceCondition extends AnyNestedCondition {
 
@@ -87,6 +88,11 @@
 
 		}
 
+		@ConditionalOnProperty(prefix = "spring.datasource", name = "url")
+		static class ExplicitUrl {
+
+		}
+
 		@Conditional(PooledDataSourceAvailableCondition.class)
 		static class PooledDataSourceAvailable {
 
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceConfiguration.java
index d94e9d3ec1a..10fe9b7e89c 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceConfiguration.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2012-2019 the original author or authors.
+ * Copyright 2012-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -20,13 +20,17 @@
 
 import com.zaxxer.hikari.HikariDataSource;
 
+import org.springframework.boot.autoconfigure.condition.AnyNestedCondition;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
 import org.springframework.boot.context.properties.ConfigurationProperties;
+import org.springframework.boot.jdbc.DataSourceBuilder;
 import org.springframework.boot.jdbc.DatabaseDriver;
 import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Conditional;
 import org.springframework.context.annotation.Configuration;
+import org.springframework.jdbc.datasource.SimpleDriverDataSource;
 import org.springframework.util.StringUtils;
 
 /**
@@ -114,12 +118,34 @@ HikariDataSource dataSource(DataSourceProperties properties) {
 	 */
 	@Configuration(proxyBeanMethods = false)
 	@ConditionalOnMissingBean(DataSource.class)
-	@ConditionalOnProperty(name = "spring.datasource.type")
+	@Conditional(GenericCondition.class)
 	static class Generic {
 
 		@Bean
 		DataSource dataSource(DataSourceProperties properties) {
-			return properties.initializeDataSourceBuilder().build();
+			DataSourceBuilder<?> dataSourceBuilder = properties.initializeDataSourceBuilder();
+			if (properties.getType() == null && DataSourceBuilder.findType(properties.getClassLoader()) == null) {
+				dataSourceBuilder.type(SimpleDriverDataSource.class);
+			}
+			return dataSourceBuilder.build();
+		}
+
+	}
+
+	static class GenericCondition extends AnyNestedCondition {
+
+		GenericCondition() {
+			super(ConfigurationPhase.PARSE_CONFIGURATION);
+		}
+
+		@ConditionalOnProperty(prefix = "spring.datasource", name = "type")
+		static class ExplicitType {
+
+		}
+
+		@ConditionalOnProperty(prefix = "spring.datasource", name = "url")
+		static class ExplicitUrl {
+
 		}
 
 	}
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfigurationTests.java
index 109c05b296d..68319edc911 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourceAutoConfigurationTests.java
@@ -33,6 +33,7 @@
 
 import com.zaxxer.hikari.HikariDataSource;
 import org.apache.commons.dbcp2.BasicDataSource;
+import org.hsqldb.jdbcDriver;
 import org.junit.jupiter.api.Test;
 
 import org.springframework.beans.factory.BeanCreationException;
@@ -158,6 +159,20 @@ void explicitTypeNoSupportedDataSource() {
 				.run(this::containsOnlySimpleDriverDataSource);
 	}
 
+	/**
+	 * This test makes sure that if no supported pool data source is present, a datasource
+	 * is still created if "spring.datasource.url" is present.
+	 */
+	@Test
+	void explicitUrlSupportedDataSource() {
+		this.contextRunner
+				.withClassLoader(new FilteredClassLoader("org.apache.tomcat", "com.zaxxer.hikari",
+						"org.apache.commons.dbcp", "org.apache.commons.dbcp2"))
+				.withPropertyValues("spring.datasource.driverClassName:org.hsqldb.jdbcDriver",
+						"spring.datasource.url:jdbc:hsqldb:mem:testdb")
+				.run(this::containsOnlySimpleDriverDataSource);
+	}
+
 	@Test
 	void explicitTypeSupportedDataSource() {
 		this.contextRunner
@@ -169,7 +184,8 @@ void explicitTypeSupportedDataSource() {
 
 	private void containsOnlySimpleDriverDataSource(AssertableApplicationContext context) {
 		assertThat(context).hasSingleBean(DataSource.class);
-		assertThat(context).getBean(DataSource.class).isExactlyInstanceOf(SimpleDriverDataSource.class);
+		assertThat(context).getBean(DataSource.class).isExactlyInstanceOf(SimpleDriverDataSource.class)
+				.extracting("driver").isExactlyInstanceOf(jdbcDriver.class);
 	}
 
 	@Test
diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/DataSourceBuilder.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/DataSourceBuilder.java
index f21da930eff..68d759d20f1 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/DataSourceBuilder.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/DataSourceBuilder.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2012-2019 the original author or authors.
+ * Copyright 2012-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -89,6 +89,7 @@ private void bind(DataSource result) {
 		ConfigurationPropertyNameAliases aliases = new ConfigurationPropertyNameAliases();
 		aliases.addAliases("url", "jdbc-url");
 		aliases.addAliases("username", "user");
+		aliases.addAliases("driver-class-name", "driver-class");
 		Binder binder = new Binder(source.withAliases(aliases));
 		binder.bind(ConfigurationPropertyName.EMPTY, Bindable.ofInstance(result));
 	}
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/jdbc/DataSourceBuilderTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/jdbc/DataSourceBuilderTests.java
index 2fe86678688..cd98aba08dc 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/jdbc/DataSourceBuilderTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/jdbc/DataSourceBuilderTests.java
@@ -29,6 +29,8 @@
 import org.junit.jupiter.api.AfterEach;
 import org.junit.jupiter.api.Test;
 
+import org.springframework.jdbc.datasource.SimpleDriverDataSource;
+
 import static org.assertj.core.api.Assertions.assertThat;
 
 /**
@@ -68,6 +70,13 @@ void defaultToCommonsDbcp2AsLastResort() {
 		assertThat(this.dataSource).isInstanceOf(BasicDataSource.class);
 	}
 
+	@Test
+	void simpleDriverDataSource() {
+		this.dataSource = DataSourceBuilder.create().url("jdbc:h2:test").type(SimpleDriverDataSource.class).build();
+		assertThat(this.dataSource).isInstanceOf(SimpleDriverDataSource.class);
+		assertThat(this.dataSource).extracting("driver").isNotNull();
+	}
+
 	@Test
 	void specificTypeOfDataSource() {
 		HikariDataSource hikariDataSource = DataSourceBuilder.create().type(HikariDataSource.class).build();
