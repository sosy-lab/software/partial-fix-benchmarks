diff --git a/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/SpringBootContextLoader.java b/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/SpringBootContextLoader.java
index a65678e25a8..7bdfbc08037 100644
--- a/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/SpringBootContextLoader.java
+++ b/spring-boot-project/spring-boot-test/src/main/java/org/springframework/boot/test/context/SpringBootContextLoader.java
@@ -28,7 +28,6 @@
 import org.springframework.boot.context.properties.source.ConfigurationPropertySource;
 import org.springframework.boot.context.properties.source.MapConfigurationPropertySource;
 import org.springframework.boot.test.mock.web.SpringBootMockServletContext;
-import org.springframework.boot.test.util.TestPropertyValues;
 import org.springframework.boot.web.reactive.context.GenericReactiveWebApplicationContext;
 import org.springframework.boot.web.servlet.support.ServletContextApplicationContextInitializer;
 import org.springframework.context.ApplicationContext;
@@ -73,6 +72,7 @@
  * @author Andy Wilkinson
  * @author Stephane Nicoll
  * @author Madhura Bhave
+ * @author Scott Frederick
  * @since 1.4.0
  * @see SpringBootTest
  */
@@ -92,7 +92,7 @@ public ApplicationContext loadContext(MergedContextConfiguration config) throws
 		application.getSources().addAll(Arrays.asList(configLocations));
 		ConfigurableEnvironment environment = getEnvironment();
 		if (!ObjectUtils.isEmpty(config.getActiveProfiles())) {
-			setActiveProfiles(environment, config.getActiveProfiles());
+			environment.setActiveProfiles(config.getActiveProfiles());
 		}
 		ResourceLoader resourceLoader = (application.getResourceLoader() != null) ? application.getResourceLoader()
 				: new DefaultResourceLoader(getClass().getClassLoader());
@@ -138,11 +138,6 @@ protected ConfigurableEnvironment getEnvironment() {
 		return new StandardEnvironment();
 	}
 
-	private void setActiveProfiles(ConfigurableEnvironment environment, String[] profiles) {
-		TestPropertyValues.of("spring.profiles.active=" + StringUtils.arrayToCommaDelimitedString(profiles))
-				.applyTo(environment);
-	}
-
 	protected String[] getInlinedProperties(MergedContextConfiguration config) {
 		ArrayList<String> properties = new ArrayList<>();
 		// JMX bean names will clash if the same bean is used in multiple contexts
diff --git a/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/SpringBootContextLoaderTests.java b/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/SpringBootContextLoaderTests.java
index 61cf76ca66d..8dddcb97cbb 100644
--- a/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/SpringBootContextLoaderTests.java
+++ b/spring-boot-project/spring-boot-test/src/test/java/org/springframework/boot/test/context/SpringBootContextLoaderTests.java
@@ -22,6 +22,8 @@
 import org.junit.Test;
 
 import org.springframework.context.annotation.Configuration;
+import org.springframework.core.env.Environment;
+import org.springframework.test.context.ActiveProfiles;
 import org.springframework.test.context.ContextConfiguration;
 import org.springframework.test.context.MergedContextConfiguration;
 import org.springframework.test.context.TestContext;
@@ -35,6 +37,7 @@
  * Tests for {@link SpringBootContextLoader}
  *
  * @author Stephane Nicoll
+ * @author Scott Frederick
  */
 public class SpringBootContextLoaderTests {
 
@@ -88,18 +91,49 @@ public void environmentPropertiesNewLineInValue() {
 		assertKey(config, "variables", "foo=FOO\n bar=BAR");
 	}
 
+	@Test
+	public void noActiveProfiles() {
+		Environment environment = getApplicationEnvironment(SimpleConfig.class);
+		assertThat(environment.getActiveProfiles()).isEmpty();
+	}
+
+	@Test
+	public void multipleActiveProfiles() {
+		Environment environment = getApplicationEnvironment(MultipleActiveProfiles.class);
+		assertProfilesContains(environment, "profile1", "profile2");
+	}
+
+	@Test
+	public void activeProfileWithComma() {
+		Environment environment = getApplicationEnvironment(ActiveProfileWithComma.class);
+		assertProfilesContains(environment, "profile1,2");
+	}
+
 	private Map<String, Object> getEnvironmentProperties(Class<?> testClass) {
-		TestContext context = new ExposedTestContextManager(testClass).getExposedTestContext();
+		TestContext context = getTestContext(testClass);
 		MergedContextConfiguration config = (MergedContextConfiguration) ReflectionTestUtils.getField(context,
 				"mergedContextConfiguration");
 		return TestPropertySourceUtils.convertInlinedPropertiesToMap(config.getPropertySourceProperties());
 	}
 
+	private Environment getApplicationEnvironment(Class<?> testClass) {
+		TestContext context = getTestContext(testClass);
+		return context.getApplicationContext().getEnvironment();
+	}
+
+	private TestContext getTestContext(Class<?> testClass) {
+		return new ExposedTestContextManager(testClass).getExposedTestContext();
+	}
+
 	private void assertKey(Map<String, Object> actual, String key, Object value) {
 		assertThat(actual.containsKey(key)).as("Key '" + key + "' not found").isTrue();
 		assertThat(actual.get(key)).isEqualTo(value);
 	}
 
+	private void assertProfilesContains(Environment environment, String... profiles) {
+		assertThat(environment.getActiveProfiles()).containsExactly(profiles);
+	}
+
 	@SpringBootTest({ "key=myValue", "anotherKey:anotherValue" })
 	@ContextConfiguration(classes = Config.class)
 	static class SimpleConfig {
@@ -142,6 +176,20 @@ private void assertKey(Map<String, Object> actual, String key, Object value) {
 
 	}
 
+	@SpringBootTest
+	@ActiveProfiles({ "profile1", "profile2" })
+	@ContextConfiguration(classes = Config.class)
+	static class MultipleActiveProfiles {
+
+	}
+
+	@SpringBootTest
+	@ActiveProfiles({ "profile1,2" })
+	@ContextConfiguration(classes = Config.class)
+	static class ActiveProfileWithComma {
+
+	}
+
 	@Configuration
 	static class Config {
 
