diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/cloud/CloudPlatform.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/cloud/CloudPlatform.java
index c7f124bfaef..0ca7a0f82e5 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/cloud/CloudPlatform.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/cloud/CloudPlatform.java
@@ -38,7 +38,7 @@
 	CLOUD_FOUNDRY {
 
 		@Override
-		public boolean isActive(Environment environment) {
+		public boolean isAutoDetected(Environment environment) {
 			return environment.containsProperty("VCAP_APPLICATION") || environment.containsProperty("VCAP_SERVICES");
 		}
 
@@ -50,7 +50,7 @@ public boolean isActive(Environment environment) {
 	HEROKU {
 
 		@Override
-		public boolean isActive(Environment environment) {
+		public boolean isAutoDetected(Environment environment) {
 			return environment.containsProperty("DYNO");
 		}
 
@@ -62,7 +62,7 @@ public boolean isActive(Environment environment) {
 	SAP {
 
 		@Override
-		public boolean isActive(Environment environment) {
+		public boolean isAutoDetected(Environment environment) {
 			return environment.containsProperty("HC_LANDSCAPE");
 		}
 
@@ -82,14 +82,14 @@ public boolean isActive(Environment environment) {
 		private static final String SERVICE_PORT_SUFFIX = "_SERVICE_PORT";
 
 		@Override
-		public boolean isActive(Environment environment) {
+		public boolean isAutoDetected(Environment environment) {
 			if (environment instanceof ConfigurableEnvironment) {
-				return isActive((ConfigurableEnvironment) environment);
+				return isAutoDetected((ConfigurableEnvironment) environment);
 			}
 			return false;
 		}
 
-		private boolean isActive(ConfigurableEnvironment environment) {
+		private boolean isAutoDetected(ConfigurableEnvironment environment) {
 			PropertySource<?> environmentPropertySource = environment.getPropertySources()
 					.get(StandardEnvironment.SYSTEM_ENVIRONMENT_PROPERTY_SOURCE_NAME);
 			if (environmentPropertySource != null) {
@@ -98,13 +98,13 @@ private boolean isActive(ConfigurableEnvironment environment) {
 					return true;
 				}
 				if (environmentPropertySource instanceof EnumerablePropertySource) {
-					return isActive((EnumerablePropertySource<?>) environmentPropertySource);
+					return isAutoDetected((EnumerablePropertySource<?>) environmentPropertySource);
 				}
 			}
 			return false;
 		}
 
-		private boolean isActive(EnumerablePropertySource<?> environmentPropertySource) {
+		private boolean isAutoDetected(EnumerablePropertySource<?> environmentPropertySource) {
 			for (String propertyName : environmentPropertySource.getPropertyNames()) {
 				if (propertyName.endsWith(SERVICE_HOST_SUFFIX)) {
 					String serviceName = propertyName.substring(0,
@@ -124,7 +124,31 @@ private boolean isActive(EnumerablePropertySource<?> environmentPropertySource)
 	 * @param environment the environment
 	 * @return if the platform is active.
 	 */
-	public abstract boolean isActive(Environment environment);
+	public boolean isActive(Environment environment) {
+		return isEnforced(environment) || isAutoDetected(environment);
+	}
+
+	/**
+	 * Detemines if the platform is enforced by looking at the
+	 * {@code "spring.main.cloud-platform"} configuration property.
+	 * @param environment the environment
+	 * @return if the platform is enforced
+	 */
+	public boolean isEnforced(Environment environment) {
+		String platform = environment.getProperty("spring.main.cloud-platform");
+		if (platform != null) {
+			return this.name().equalsIgnoreCase(platform);
+		}
+		return false;
+	}
+
+	/**
+	 * Determines is the platform is auto-detected by looking ofr platform-specific
+	 * environment variables.
+	 * @param environment the environment
+	 * @return if the platform is auto-detected.
+	 */
+	public abstract boolean isAutoDetected(Environment environment);
 
 	/**
 	 * Returns if the platform is behind a load balancer and uses
diff --git a/spring-boot-project/spring-boot/src/main/resources/META-INF/additional-spring-configuration-metadata.json b/spring-boot-project/spring-boot/src/main/resources/META-INF/additional-spring-configuration-metadata.json
index 06d811582ae..603d0ceb80e 100644
--- a/spring-boot-project/spring-boot/src/main/resources/META-INF/additional-spring-configuration-metadata.json
+++ b/spring-boot-project/spring-boot/src/main/resources/META-INF/additional-spring-configuration-metadata.json
@@ -640,6 +640,11 @@
       "description": "Mode used to display the banner when the application runs.",
       "defaultValue": "console"
     },
+    {
+      "name": "spring.main.cloud-platform",
+      "type": "org.springframework.boot.cloud.CloudPlatform",
+      "description": "Override the Cloud Platform auto-detection."
+    },
     {
       "name": "spring.main.lazy-initialization",
       "type": "java.lang.Boolean",
diff --git a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/cloud/CloudPlatformTests.java b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/cloud/CloudPlatformTests.java
index 9787259337c..c308c057d2f 100644
--- a/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/cloud/CloudPlatformTests.java
+++ b/spring-boot-project/spring-boot/src/test/java/org/springframework/boot/cloud/CloudPlatformTests.java
@@ -129,6 +129,14 @@ void getActiveWhenHasServiceHostAndNoServicePortShouldNotReturnKubernetes() {
 		assertThat(platform).isNull();
 	}
 
+	@Test
+	void getActiveWhenHasEnforcedCloudPlatform() {
+		Environment environment = getEnvironmentWithEnvVariables(
+				Collections.singletonMap("spring.main.cloud-platform", "kubernetes"));
+		CloudPlatform platform = CloudPlatform.getActive(environment);
+		assertThat(platform).isEqualTo(CloudPlatform.KUBERNETES);
+	}
+
 	private Environment getEnvironmentWithEnvVariables(Map<String, Object> environmentVariables) {
 		MockEnvironment environment = new MockEnvironment();
 		PropertySource<?> propertySource = new SystemEnvironmentPropertySource(
