diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java
index b387ff0e2b9..ec6f40f3a0d 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java
@@ -16,6 +16,7 @@
 
 package org.springframework.boot.autoconfigure.elasticsearch;
 
+import java.net.URI;
 import java.time.Duration;
 
 import org.apache.http.HttpHost;
@@ -31,6 +32,7 @@
 import org.elasticsearch.client.RestHighLevelClient;
 
 import org.springframework.beans.factory.ObjectProvider;
+import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
 import org.springframework.boot.context.properties.PropertyMapper;
@@ -43,6 +45,7 @@
  * @author Brian Clozel
  * @author Stephane Nicoll
  * @author Vedran Pavic
+ * @author Evgeniy Cheban
  */
 class ElasticsearchRestClientConfigurations {
 
@@ -58,7 +61,7 @@ RestClientBuilderCustomizer defaultRestClientBuilderCustomizer(ElasticsearchRest
 		@Bean
 		RestClientBuilder elasticsearchRestClientBuilder(ElasticsearchRestClientProperties properties,
 				ObjectProvider<RestClientBuilderCustomizer> builderCustomizers) {
-			HttpHost[] hosts = properties.getUris().stream().map(HttpHost::create).toArray(HttpHost[]::new);
+			HttpHost[] hosts = properties.getUris().stream().map(this::createHttpHost).toArray(HttpHost[]::new);
 			RestClientBuilder builder = RestClient.builder(hosts);
 			builder.setHttpClientConfigCallback((httpClientBuilder) -> {
 				builderCustomizers.orderedStream().forEach((customizer) -> customizer.customize(httpClientBuilder));
@@ -72,6 +75,12 @@ RestClientBuilder elasticsearchRestClientBuilder(ElasticsearchRestClientProperti
 			return builder;
 		}
 
+		private HttpHost createHttpHost(String uri) {
+			URI parsedUri = URI.create(uri);
+			String userInfo = parsedUri.getUserInfo();
+			return HttpHost.create((userInfo != null) ? uri.replace(userInfo + "@", "") : uri);
+		}
+
 	}
 
 	@Configuration(proxyBeanMethods = false)
@@ -114,25 +123,48 @@ RestClient elasticsearchRestClient(RestClientBuilder builder) {
 
 		private final ElasticsearchRestClientProperties properties;
 
+		private CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
+
 		DefaultRestClientBuilderCustomizer(ElasticsearchRestClientProperties properties) {
 			this.properties = properties;
 		}
 
+		@Autowired(required = false)
+		void setCredentialsProvider(CredentialsProvider credentialsProvider) {
+			this.credentialsProvider = credentialsProvider;
+		}
+
 		@Override
 		public void customize(RestClientBuilder builder) {
 		}
 
 		@Override
 		public void customize(HttpAsyncClientBuilder builder) {
+			builder.setDefaultCredentialsProvider(this.credentialsProvider);
+			this.properties.getUris().stream().map(URI::create).filter((uri) -> uri.getUserInfo() != null)
+					.forEach((uri) -> {
+						AuthScope authScope = new AuthScope(uri.getHost(), uri.getPort());
+						Credentials credentials = createCredentials(uri.getUserInfo());
+						this.credentialsProvider.setCredentials(authScope, credentials);
+					});
 			map.from(this.properties::getUsername).whenHasText().to((username) -> {
-				CredentialsProvider credentialsProvider = new BasicCredentialsProvider();
 				Credentials credentials = new UsernamePasswordCredentials(this.properties.getUsername(),
 						this.properties.getPassword());
-				credentialsProvider.setCredentials(AuthScope.ANY, credentials);
-				builder.setDefaultCredentialsProvider(credentialsProvider);
+				this.credentialsProvider.setCredentials(AuthScope.ANY, credentials);
 			});
 		}
 
+		private Credentials createCredentials(String usernameAndPassword) {
+			int delimiter = usernameAndPassword.indexOf(":");
+			if (delimiter == -1) {
+				return new UsernamePasswordCredentials(usernameAndPassword, null);
+			}
+
+			String username = usernameAndPassword.substring(0, delimiter);
+			String password = usernameAndPassword.substring(delimiter + 1);
+			return new UsernamePasswordCredentials(username, password);
+		}
+
 		@Override
 		public void customize(RequestConfig.Builder builder) {
 			map.from(this.properties::getConnectionTimeout).whenNonNull().asInt(Duration::toMillis)
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java
index f576423f6c1..b7246cc49ac 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java
@@ -20,10 +20,16 @@
 import java.util.HashMap;
 import java.util.Map;
 
+import org.apache.http.HttpHost;
+import org.apache.http.auth.AuthScope;
+import org.apache.http.auth.Credentials;
+import org.apache.http.client.CredentialsProvider;
 import org.apache.http.client.config.RequestConfig;
+import org.apache.http.impl.client.BasicCredentialsProvider;
 import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
 import org.elasticsearch.action.get.GetRequest;
 import org.elasticsearch.action.index.IndexRequest;
+import org.elasticsearch.client.Node;
 import org.elasticsearch.client.RequestOptions;
 import org.elasticsearch.client.RestClient;
 import org.elasticsearch.client.RestClientBuilder;
@@ -47,6 +53,7 @@
  *
  * @author Brian Clozel
  * @author Vedran Pavic
+ * @author Evgeniy Cheban
  */
 @Testcontainers(disabledWithoutDocker = true)
 class ElasticsearchRestClientAutoConfigurationTests {
@@ -156,6 +163,69 @@ void restClientCanQueryElasticsearchNode() {
 				});
 	}
 
+	@Test
+	void configureUriWithUsernameOnly() {
+		this.contextRunner.withUserConfiguration(CredentialsProviderConfiguration.class)
+				.withPropertyValues("spring.elasticsearch.rest.uris=http://user@localhost:9200").run((context) -> {
+					RestClient client = context.getBean(RestClient.class);
+					assertThat(client.getNodes().stream().map(Node::getHost).map(HttpHost::toString))
+							.containsExactly("http://localhost:9200");
+
+					CredentialsProvider credentialsProvider = context.getBean(CredentialsProvider.class);
+					Credentials credentials = credentialsProvider.getCredentials(new AuthScope("localhost", 9200));
+					assertThat(credentials.getUserPrincipal().getName()).isEqualTo("user");
+					assertThat(credentials.getPassword()).isNull();
+				});
+	}
+
+	@Test
+	void configureUriWithUsernameAndEmptyPassword() {
+		this.contextRunner.withUserConfiguration(CredentialsProviderConfiguration.class)
+				.withPropertyValues("spring.elasticsearch.rest.uris=http://user:@localhost:9200").run((context) -> {
+					RestClient client = context.getBean(RestClient.class);
+					assertThat(client.getNodes().stream().map(Node::getHost).map(HttpHost::toString))
+							.containsExactly("http://localhost:9200");
+
+					CredentialsProvider credentialsProvider = context.getBean(CredentialsProvider.class);
+					Credentials credentials = credentialsProvider.getCredentials(new AuthScope("localhost", 9200));
+					assertThat(credentials.getUserPrincipal().getName()).isEqualTo("user");
+					assertThat(credentials.getPassword()).isEmpty();
+				});
+	}
+
+	@Test
+	void configureUriWithUsernameAndPasswordWhenUsernameAndPasswordPropertiesSet() {
+		this.contextRunner.withUserConfiguration(CredentialsProviderConfiguration.class)
+				.withPropertyValues("spring.elasticsearch.rest.uris=http://user:password@localhost:9200,localhost:9201",
+						"spring.elasticsearch.rest.username=admin", "spring.elasticsearch.rest.password=admin")
+				.run((context) -> {
+					RestClient client = context.getBean(RestClient.class);
+					assertThat(client.getNodes().stream().map(Node::getHost).map(HttpHost::toString))
+							.containsExactly("http://localhost:9200", "http://localhost:9201");
+
+					CredentialsProvider credentialsProvider = context.getBean(CredentialsProvider.class);
+
+					Credentials uriCredentials = credentialsProvider.getCredentials(new AuthScope("localhost", 9200));
+					assertThat(uriCredentials.getUserPrincipal().getName()).isEqualTo("user");
+					assertThat(uriCredentials.getPassword()).isEqualTo("password");
+
+					Credentials defaultCredentials = credentialsProvider
+							.getCredentials(new AuthScope("localhost", 9201));
+					assertThat(defaultCredentials.getUserPrincipal().getName()).isEqualTo("admin");
+					assertThat(defaultCredentials.getPassword()).isEqualTo("admin");
+				});
+	}
+
+	@Configuration(proxyBeanMethods = false)
+	static class CredentialsProviderConfiguration {
+
+		@Bean
+		CredentialsProvider credentialsProvider() {
+			return new BasicCredentialsProvider();
+		}
+
+	}
+
 	@Configuration(proxyBeanMethods = false)
 	static class CustomRestClientConfiguration {
 
