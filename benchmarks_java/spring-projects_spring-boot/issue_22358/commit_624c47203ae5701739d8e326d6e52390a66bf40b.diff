diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfiguration.java
index 69d03e23e42..f2d518ef93f 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfiguration.java
@@ -36,6 +36,7 @@
 @EnableConfigurationProperties(ElasticsearchRestClientProperties.class)
 @Import({ ElasticsearchRestClientConfigurations.RestClientBuilderConfiguration.class,
 		ElasticsearchRestClientConfigurations.RestHighLevelClientConfiguration.class,
+		ElasticsearchRestClientConfigurations.RestClientConfiguration.class,
 		ElasticsearchRestClientConfigurations.RestClientFallbackConfiguration.class })
 public class ElasticsearchRestClientAutoConfiguration {
 
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java
index b7ab5e9c0b2..89a331fc016 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientConfigurations.java
@@ -19,6 +19,7 @@
 import java.net.URI;
 import java.net.URISyntaxException;
 import java.time.Duration;
+import java.util.Collections;
 
 import org.apache.http.HttpHost;
 import org.apache.http.auth.AuthScope;
@@ -34,6 +35,7 @@
 import org.springframework.beans.factory.ObjectProvider;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnSingleCandidate;
 import org.springframework.boot.context.properties.PropertyMapper;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
@@ -105,12 +107,24 @@ private HttpHost createHttpHost(URI uri) {
 
 		@Bean
 		@ConditionalOnMissingBean
-		RestHighLevelClient elasticsearchRestHighLevelClient(RestClientBuilder restClientBuilder) {
+		RestHighLevelClient elasticsearchRestHighLevelClient(RestClientBuilder restClientBuilder,
+				ObjectProvider<RestClient> restClient) {
+			RestClient lowLevelClient = restClient.getIfUnique();
+			if (lowLevelClient != null) {
+				return new SpringBootRestHighLevelClient(lowLevelClient);
+			}
 			return new RestHighLevelClient(restClientBuilder);
 		}
 
+	}
+
+	@Configuration(proxyBeanMethods = false)
+	@ConditionalOnClass(RestHighLevelClient.class)
+	@ConditionalOnSingleCandidate(RestHighLevelClient.class)
+	@ConditionalOnMissingBean(RestClient.class)
+	static class RestClientConfiguration {
+
 		@Bean
-		@ConditionalOnMissingBean
 		RestClient elasticsearchRestClient(RestClientBuilder builder,
 				ObjectProvider<RestHighLevelClient> restHighLevelClient) {
 			RestHighLevelClient client = restHighLevelClient.getIfUnique();
@@ -205,4 +219,12 @@ private Credentials createUserInfoCredentials(String userInfo) {
 
 	}
 
+	private static class SpringBootRestHighLevelClient extends RestHighLevelClient {
+
+		SpringBootRestHighLevelClient(RestClient restClient) {
+			super(restClient, RestClient::close, Collections.emptyList());
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java
index 09648bf9bc5..567bdd46cee 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/elasticsearch/ElasticsearchRestClientAutoConfigurationTests.java
@@ -54,6 +54,7 @@
  * @author Brian Clozel
  * @author Vedran Pavic
  * @author Evgeniy Cheban
+ * @author Stephane Nicoll
  */
 @Testcontainers(disabledWithoutDocker = true)
 class ElasticsearchRestClientAutoConfigurationTests {
@@ -75,20 +76,33 @@ void configureShouldCreateBothRestClientVariants() {
 	}
 
 	@Test
-	void configureWhenCustomClientShouldBackOff() {
-		this.contextRunner.withUserConfiguration(CustomRestClientConfiguration.class)
-				.run((context) -> assertThat(context).getBeanNames(RestClient.class).containsOnly("customRestClient"));
+	void configureWhenCustomRestClientShouldCreateHighLevelClientThatUsesIt() {
+		this.contextRunner.withUserConfiguration(CustomRestClientConfiguration.class).run((context) -> {
+			assertThat(context).hasSingleBean(RestClient.class).hasSingleBean(RestHighLevelClient.class)
+					.hasBean("customRestClient");
+			assertThat(context.getBean(RestHighLevelClient.class).getLowLevelClient())
+					.isSameAs(context.getBean("customRestClient"));
+		});
 	}
 
 	@Test
-	void configureWhenCustomRestHighLevelClientShouldBackOff() {
+	void configureWhenCustomRestHighLevelClientShouldExposeUnderlyingRestClient() {
 		this.contextRunner.withUserConfiguration(CustomRestHighLevelClientConfiguration.class).run((context) -> {
 			assertThat(context).hasSingleBean(RestClient.class).hasSingleBean(RestHighLevelClient.class);
-			assertThat(context.getBean(RestClient.class))
-					.isSameAs(context.getBean(RestHighLevelClient.class).getLowLevelClient());
+			assertThat(context.getBean(RestHighLevelClient.class).getLowLevelClient())
+					.isSameAs(context.getBean(RestClient.class));
 		});
 	}
 
+	@Test
+	void configureWhenCustomRestHighLevelClientAndRestClientShouldBackOff() {
+		this.contextRunner
+				.withUserConfiguration(CustomRestClientConfiguration.class,
+						CustomRestHighLevelClientConfiguration.class)
+				.run((context) -> assertThat(context).hasSingleBean(RestClient.class).hasBean("customRestClient")
+						.hasSingleBean(RestHighLevelClient.class).hasBean("customRestHighLevelClient"));
+	}
+
 	@Test
 	void configureWhenDefaultRestClientShouldCreateWhenNoUniqueRestHighLevelClient() {
 		this.contextRunner.withUserConfiguration(TwoCustomRestHighLevelClientConfiguration.class).run((context) -> {
