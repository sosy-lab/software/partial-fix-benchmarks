diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfiguration.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfiguration.java
index 9f6cc4ebc60..9f01df8a376 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfiguration.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfiguration.java
@@ -18,12 +18,16 @@
 
 import java.net.URL;
 import java.security.KeyStore;
+import java.util.Optional;
+import java.util.Set;
+import java.util.stream.Collectors;
 
 import javax.net.ssl.KeyManagerFactory;
 import javax.net.ssl.TrustManagerFactory;
 
 import com.couchbase.client.core.env.IoConfig;
 import com.couchbase.client.core.env.SecurityConfig;
+import com.couchbase.client.core.env.SeedNode;
 import com.couchbase.client.core.env.TimeoutConfig;
 import com.couchbase.client.java.Cluster;
 import com.couchbase.client.java.ClusterOptions;
@@ -35,6 +39,7 @@
 import org.springframework.beans.factory.ObjectProvider;
 import org.springframework.boot.autoconfigure.AutoConfigureAfter;
 import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.AnyNestedCondition;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
@@ -43,6 +48,7 @@
 import org.springframework.boot.autoconfigure.jackson.JacksonAutoConfiguration;
 import org.springframework.boot.context.properties.EnableConfigurationProperties;
 import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Conditional;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.core.Ordered;
 import org.springframework.util.ResourceUtils;
@@ -53,12 +59,13 @@
  * @author Eddú Meléndez
  * @author Stephane Nicoll
  * @author Yulin Qin
+ * @author Aaron Whiteside
  * @since 1.4.0
  */
 @Configuration(proxyBeanMethods = false)
 @AutoConfigureAfter(JacksonAutoConfiguration.class)
 @ConditionalOnClass(Cluster.class)
-@ConditionalOnProperty("spring.couchbase.connection-string")
+@Conditional(CouchbaseAutoConfiguration.CouchbasePropertyCondition.class)
 @EnableConfigurationProperties(CouchbaseProperties.class)
 public class CouchbaseAutoConfiguration {
 
@@ -76,7 +83,17 @@ public ClusterEnvironment couchbaseClusterEnvironment(CouchbaseProperties proper
 	public Cluster couchbaseCluster(CouchbaseProperties properties, ClusterEnvironment couchbaseClusterEnvironment) {
 		ClusterOptions options = ClusterOptions.clusterOptions(properties.getUsername(), properties.getPassword())
 				.environment(couchbaseClusterEnvironment);
-		return Cluster.connect(properties.getConnectionString(), options);
+
+		if (properties.getSeedNodes().isEmpty()) {
+			return Cluster.connect(properties.getConnectionString(), options);
+		}
+		else {
+			Set<SeedNode> seedNodes = properties
+					.getSeedNodes().stream().map(s -> SeedNode.create(s.getAddress(),
+							Optional.ofNullable(s.getKeyValuePort()), Optional.ofNullable(s.getClusterManagerPort())))
+					.collect(Collectors.toSet());
+			return Cluster.connect(seedNodes, options);
+		}
 	}
 
 	private ClusterEnvironment.Builder initializeEnvironmentBuilder(CouchbaseProperties properties) {
@@ -151,4 +168,22 @@ public int getOrder() {
 
 	}
 
+	static final class CouchbasePropertyCondition extends AnyNestedCondition {
+
+		public CouchbasePropertyCondition() {
+			super(ConfigurationPhase.REGISTER_BEAN);
+		}
+
+		@ConditionalOnProperty("spring.couchbase.connection-string")
+		static class OnConnectionString {
+
+		}
+
+		@ConditionalOnProperty("spring.couchbase.seed-nodes[0].address")
+		static class OnSeedNode {
+
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseProperties.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseProperties.java
index 4dc0e252ffb..e2ff3b941a8 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseProperties.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseProperties.java
@@ -17,6 +17,8 @@
 package org.springframework.boot.autoconfigure.couchbase;
 
 import java.time.Duration;
+import java.util.ArrayList;
+import java.util.List;
 
 import org.springframework.boot.context.properties.ConfigurationProperties;
 import org.springframework.util.StringUtils;
@@ -29,11 +31,17 @@
  * @author Yulin Qin
  * @author Brian Clozel
  * @author Michael Nitschinger
+ * @author Aaron Whiteside
  * @since 1.4.0
  */
 @ConfigurationProperties(prefix = "spring.couchbase")
 public class CouchbaseProperties {
 
+	/**
+	 * Alternate list of SeedNodes used to locate the Couchbase cluster.
+	 */
+	private List<SeedNode> seedNodes = new ArrayList<>();
+
 	/**
 	 * Connection string used to locate the Couchbase cluster.
 	 */
@@ -51,6 +59,14 @@
 
 	private final Env env = new Env();
 
+	public List<SeedNode> getSeedNodes() {
+		return this.seedNodes;
+	}
+
+	public void setSeedNodes(List<SeedNode> seedNodes) {
+		this.seedNodes = seedNodes;
+	}
+
 	public String getConnectionString() {
 		return this.connectionString;
 	}
@@ -310,4 +326,47 @@ public void setManagement(Duration management) {
 
 	}
 
+	public static class SeedNode {
+
+		/**
+		 * Alternate address to use when connecting to the Couchbase cluster.
+		 */
+		private String address;
+
+		/**
+		 * Alternate Key Value port to use when connecting to the Couchbase cluster.
+		 */
+		private Integer keyValuePort;
+
+		/**
+		 * Alternate Cluster Manager port to use when connecting to the Couchbase cluster.
+		 */
+		private Integer clusterManagerPort;
+
+		public String getAddress() {
+			return this.address;
+		}
+
+		public void setAddress(String address) {
+			this.address = address;
+		}
+
+		public Integer getKeyValuePort() {
+			return this.keyValuePort;
+		}
+
+		public void setKeyValuePort(Integer keyValuePort) {
+			this.keyValuePort = keyValuePort;
+		}
+
+		public Integer getClusterManagerPort() {
+			return this.clusterManagerPort;
+		}
+
+		public void setClusterManagerPort(Integer clusterManagerPort) {
+			this.clusterManagerPort = clusterManagerPort;
+		}
+
+	}
+
 }
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationIntegrationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationIntegrationTests.java
index ac8c9a00543..326e482a40b 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationIntegrationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationIntegrationTests.java
@@ -40,6 +40,7 @@
  *
  * @author Stephane Nicoll
  * @author Brian Clozel
+ * @author Aaron Whiteside
  */
 @Testcontainers(disabledWithoutDocker = true)
 class CouchbaseAutoConfigurationIntegrationTests {
@@ -51,15 +52,35 @@
 			.withCredentials("spring", "password").withStartupAttempts(5).withStartupTimeout(Duration.ofMinutes(10))
 			.withBucket(new BucketDefinition(BUCKET_NAME).withPrimaryIndex(false));
 
-	private final ApplicationContextRunner contextRunner = new ApplicationContextRunner()
+	private final ApplicationContextRunner connectionStringContextRunner = new ApplicationContextRunner()
 			.withConfiguration(AutoConfigurations.of(CouchbaseAutoConfiguration.class))
 			.withPropertyValues("spring.couchbase.connection-string: " + couchbase.getConnectionString(),
 					"spring.couchbase.username:spring", "spring.couchbase.password:password",
 					"spring.couchbase.bucket.name:" + BUCKET_NAME);
 
+	private final ApplicationContextRunner seedNodeContextRunner = new ApplicationContextRunner()
+			.withConfiguration(AutoConfigurations.of(CouchbaseAutoConfiguration.class))
+			.withPropertyValues("spring.couchbase.seed-nodes[0].address: " + couchbase.getHost(),
+					"spring.couchbase.seed-nodes[0].key-value-port: " + couchbase.getMappedPort(11210),
+					"spring.couchbase.seed-nodes[0].cluster-manager-port: " + couchbase.getMappedPort(8091),
+					"spring.couchbase.username:spring", "spring.couchbase.password:password",
+					"spring.couchbase.bucket.name:" + BUCKET_NAME);
+
 	@Test
 	void defaultConfiguration() {
-		this.contextRunner.run((context) -> {
+		this.connectionStringContextRunner.run((context) -> {
+			assertThat(context).hasSingleBean(Cluster.class).hasSingleBean(ClusterEnvironment.class);
+			Cluster cluster = context.getBean(Cluster.class);
+			Bucket bucket = cluster.bucket(BUCKET_NAME);
+			bucket.waitUntilReady(Duration.ofMinutes(5));
+			DiagnosticsResult diagnostics = cluster.diagnostics();
+			assertThat(diagnostics.state()).isEqualTo(ClusterState.ONLINE);
+		});
+	}
+
+	@Test
+	void seedNodeConfiguration() {
+		this.seedNodeContextRunner.run((context) -> {
 			assertThat(context).hasSingleBean(Cluster.class).hasSingleBean(ClusterEnvironment.class);
 			Cluster cluster = context.getBean(Cluster.class);
 			Bucket bucket = cluster.bucket(BUCKET_NAME);
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationTests.java
index a7f0914f097..e0670397f83 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/couchbase/CouchbaseAutoConfigurationTests.java
@@ -43,6 +43,7 @@
  *
  * @author Eddú Meléndez
  * @author Stephane Nicoll
+ * @author Aaron Whiteside
  */
 class CouchbaseAutoConfigurationTests {
 
@@ -50,7 +51,7 @@
 			.withConfiguration(AutoConfigurations.of(CouchbaseAutoConfiguration.class));
 
 	@Test
-	void connectionStringIsRequired() {
+	void connectionStringOrSeedNodeIsRequired() {
 		this.contextRunner.run((context) -> assertThat(context).doesNotHaveBean(ClusterEnvironment.class)
 				.doesNotHaveBean(Cluster.class));
 	}
@@ -65,6 +66,16 @@ void connectionStringCreateEnvironmentAndCluster() {
 				});
 	}
 
+	@Test
+	void seedNodeCreateEnvironmentAndCluster() {
+		this.contextRunner.withUserConfiguration(CouchbaseTestConfiguration.class)
+				.withPropertyValues("spring.couchbase.seed-nodes[0].address=localhost").run((context) -> {
+					assertThat(context).hasSingleBean(ClusterEnvironment.class).hasSingleBean(Cluster.class);
+					assertThat(context.getBean(Cluster.class))
+							.isSameAs(context.getBean(CouchbaseTestConfiguration.class).couchbaseCluster());
+				});
+	}
+
 	@Test
 	void environmentUseObjectMapperByDefault() {
 		this.contextRunner.withUserConfiguration(CouchbaseTestConfiguration.class)
