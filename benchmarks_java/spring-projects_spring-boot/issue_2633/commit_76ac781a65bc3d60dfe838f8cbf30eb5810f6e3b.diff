diff --git a/spring-boot-autoconfigure/pom.xml b/spring-boot-autoconfigure/pom.xml
index 2ce061f45fd..b849689b3a9 100644
--- a/spring-boot-autoconfigure/pom.xml
+++ b/spring-boot-autoconfigure/pom.xml
@@ -235,6 +235,11 @@
 			<artifactId>hornetq-jms-server</artifactId>
 			<optional>true</optional>
 		</dependency>
+		<dependency>
+			<groupId>org.infinispan</groupId>
+			<artifactId>infinispan-spring4</artifactId>
+			<optional>true</optional>
+		</dependency>
 		<dependency>
 			<groupId>org.springframework</groupId>
 			<artifactId>spring-jdbc</artifactId>
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/CacheType.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/CacheType.java
index 3fbe3d54c10..8ac65ae4cd5 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/CacheType.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/CacheType.java
@@ -41,6 +41,11 @@
 	 */
 	HAZELCAST(HazelcastCacheConfiguration.class),
 
+	/**
+	 * Infinispan backed caching.
+	 */
+	INFINISPAN(InfinispanCacheConfiguration.class),
+
 	/**
 	 * JCache (JSR-107) backed caching.
 	 */
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/InfinispanCacheConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/InfinispanCacheConfiguration.java
new file mode 100644
index 00000000000..7b5ec6456d0
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/cache/InfinispanCacheConfiguration.java
@@ -0,0 +1,69 @@
+/*
+ * Copyright 2012-2015 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.cache;
+
+import org.infinispan.manager.DefaultCacheManager;
+import org.infinispan.spring.provider.SpringEmbeddedCacheManager;
+import org.springframework.beans.factory.annotation.Autowired;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.cache.CacheManager;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Conditional;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.core.io.Resource;
+import org.springframework.util.CollectionUtils;
+
+import java.io.IOException;
+import java.util.List;
+
+/**
+ * Infinispan cache configuration.
+ *
+ * @author Eddú Meléndez
+ * @since 1.3.0
+ */
+@Configuration
+@ConditionalOnClass(SpringEmbeddedCacheManager.class)
+@ConditionalOnMissingBean(CacheManager.class)
+@Conditional(CacheCondition.class)
+public class InfinispanCacheConfiguration {
+
+	@Autowired
+	private CacheProperties cacheProperties;
+
+	@Bean
+	public CacheManager cacheManager() throws IOException {
+		DefaultCacheManager defaultCacheManager = createCacheManager();
+		List<String> cacheNames = this.cacheProperties.getCacheNames();
+		if (!CollectionUtils.isEmpty(cacheNames)) {
+			for (String cacheName : cacheNames) {
+				defaultCacheManager.startCache(cacheName);
+			}
+		}
+		return new SpringEmbeddedCacheManager(defaultCacheManager);
+	}
+
+	private DefaultCacheManager createCacheManager() throws IOException {
+		Resource location = this.cacheProperties.resolveConfigLocation();
+		if (location != null) {
+			return new DefaultCacheManager(this.cacheProperties.getConfig().getInputStream());
+		}
+		return new DefaultCacheManager();
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/cache/CacheAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/cache/CacheAutoConfigurationTests.java
index 46785d356ad..87737878444 100644
--- a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/cache/CacheAutoConfigurationTests.java
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/cache/CacheAutoConfigurationTests.java
@@ -380,6 +380,32 @@ public void hazelcastAsJCacheWithConfig() throws IOException {
 				is(configResource.getURI()));
 	}
 
+	@Test
+	public void infinispanCacheWithCaches() {
+		SpringEmbeddedCacheManager cacheManager = null;
+		try {
+			load(DefaultCacheConfiguration.class, "spring.cache.type=infinispan",
+					"spring.cache.cacheNames[0]=foo", "spring.cache.cacheNames[1]=bar");
+			cacheManager = validateCacheManager(SpringEmbeddedCacheManager.class);
+			assertThat(cacheManager.getCacheNames(), containsInAnyOrder("foo", "bar"));
+		} finally {
+			cacheManager.stop();
+		}
+	}
+
+	@Test
+	public void infinispanCacheWithConfig() {
+		SpringEmbeddedCacheManager cacheManager = null;
+		try {
+			load(DefaultCacheConfiguration.class, "spring.cache.type=infinispan",
+					"spring.cache.config=infinispan.xml");
+			cacheManager = validateCacheManager(SpringEmbeddedCacheManager.class);
+			assertThat(cacheManager.getCacheNames(), containsInAnyOrder("foo", "bar"));
+		} finally {
+			cacheManager.stop();
+		}
+	}
+
 	@Test
 	public void jCacheCacheWithCachesAndCustomizer() {
 		String cachingProviderFqn = HazelcastCachingProvider.class.getName();
diff --git a/spring-boot-autoconfigure/src/test/resources/infinispan.xml b/spring-boot-autoconfigure/src/test/resources/infinispan.xml
new file mode 100644
index 00000000000..88d844365ef
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/resources/infinispan.xml
@@ -0,0 +1,14 @@
+<?xml version="1.0" encoding="UTF-8"?>
+<infinispan xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
+            xsi:schemaLocation="urn:infinispan:config:7.1 http://www.infinispan.org/schemas/infinispan-config-7.1.xsd"
+            xmlns="urn:infinispan:config:7.1">
+
+	<!-- ************************************** -->
+	<!-- Corresponds to @Cacheable("cache-name") -->
+	<!-- ************************************** -->
+	<cache-container default-cache="default">
+		<local-cache name="foo"/>
+		<local-cache name="bar" />
+	</cache-container>
+
+</infinispan>
diff --git a/spring-boot-dependencies/pom.xml b/spring-boot-dependencies/pom.xml
index 2ea37532368..a7a5ef66ec5 100644
--- a/spring-boot-dependencies/pom.xml
+++ b/spring-boot-dependencies/pom.xml
@@ -82,6 +82,7 @@
 		<hsqldb.version>2.3.2</hsqldb.version>
 		<httpasyncclient.version>4.0.2</httpasyncclient.version>
 		<httpclient.version>4.4.1</httpclient.version>
+		<infinispan.version>7.2.1.Final</infinispan.version>
 		<jackson.version>2.5.1</jackson.version>
 		<janino.version>2.6.1</janino.version>
 		<javassist.version>3.18.1-GA</javassist.version> <!-- Same as Hibernate -->
@@ -1276,6 +1277,11 @@
 				<artifactId>hsqldb</artifactId>
 				<version>${hsqldb.version}</version>
 			</dependency>
+			<dependency>
+				<groupId>org.infinispan</groupId>
+				<artifactId>infinispan-spring4</artifactId>
+				<version>${infinispan.version}</version>
+			</dependency>
 			<dependency>
 				<groupId>org.javassist</groupId>
 				<artifactId>javassist</artifactId>
@@ -1849,4 +1855,4 @@
 			<id>integration-test</id>
 		</profile>
 	</profiles>
-</project>
\ No newline at end of file
+</project>
