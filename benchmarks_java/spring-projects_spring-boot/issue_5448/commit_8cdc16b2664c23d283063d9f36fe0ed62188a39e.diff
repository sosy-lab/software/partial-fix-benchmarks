diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfiguration.java
new file mode 100644
index 00000000000..f6be07d3fb3
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfiguration.java
@@ -0,0 +1,44 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.redis;
+
+import redis.clients.jedis.Jedis;
+
+import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.context.annotation.Import;
+import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
+import org.springframework.data.redis.repository.support.RedisRepositoryFactoryBean;
+
+/**
+ * {@link EnableAutoConfiguration Auto-configuration} for Spring Data's Redis
+ * Repositories.
+ *
+ * @author Eddú Meléndez
+ * @see EnableRedisRepositories
+ * @since 1.4.0
+ */
+@Configuration
+@ConditionalOnClass({ Jedis.class, EnableRedisRepositories.class })
+@ConditionalOnProperty(prefix = "spring.data.redis.repositories", name = "enabled", havingValue = "true", matchIfMissing = true)
+@ConditionalOnMissingBean(RedisRepositoryFactoryBean.class)
+@Import(RedisRepositoriesAutoConfigureRegistrar.class)
+public class RedisRepositoriesAutoConfiguration {
+}
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfigureRegistrar.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfigureRegistrar.java
new file mode 100644
index 00000000000..324d5ed340d
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfigureRegistrar.java
@@ -0,0 +1,54 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.redis;
+
+import java.lang.annotation.Annotation;
+
+import org.springframework.boot.autoconfigure.data.AbstractRepositoryConfigurationSourceSupport;
+import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
+import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
+import org.springframework.data.redis.repository.configuration.RedisRepositoryConfigurationExtension;
+import org.springframework.data.repository.config.RepositoryConfigurationExtension;
+
+/**
+ * {@link ImportBeanDefinitionRegistrar} used to auto-configure Spring Data Redis
+ * Repositories.
+ * @author Eddú Meléndez
+ * @since 1.4.0
+ */
+public class RedisRepositoriesAutoConfigureRegistrar extends
+		AbstractRepositoryConfigurationSourceSupport {
+	@Override
+	protected Class<? extends Annotation> getAnnotation() {
+		return EnableRedisRepositories.class;
+	}
+
+	@Override
+	protected Class<?> getConfiguration() {
+		return EnableRedisRepositoriesConfiguration.class;
+	}
+
+	@Override
+	protected RepositoryConfigurationExtension getRepositoryConfigurationExtension() {
+		return new RedisRepositoryConfigurationExtension();
+	}
+
+	@EnableRedisRepositories
+	private static class EnableRedisRepositoriesConfiguration {
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories b/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
index 5426fe47a96..811f7d78f39 100644
--- a/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
+++ b/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
@@ -33,6 +33,7 @@ org.springframework.boot.autoconfigure.data.mongo.MongoDataAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.mongo.MongoRepositoriesAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.solr.SolrRepositoriesAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.redis.RedisAutoConfiguration,\
+org.springframework.boot.autoconfigure.data.redis.RedisRepositoriesAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.rest.RepositoryRestMvcAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.web.SpringDataWebAutoConfiguration,\
 org.springframework.boot.autoconfigure.freemarker.FreeMarkerAutoConfiguration,\
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/alt/redis/CityRedisRepository.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/alt/redis/CityRedisRepository.java
new file mode 100644
index 00000000000..8b0a692fd03
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/alt/redis/CityRedisRepository.java
@@ -0,0 +1,24 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.alt.redis;
+
+import org.springframework.boot.autoconfigure.data.redis.city.City;
+import org.springframework.data.repository.Repository;
+
+public interface CityRedisRepository extends Repository<City, Long> {
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfigurationTests.java
new file mode 100644
index 00000000000..7f55fce7a8e
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/RedisRepositoriesAutoConfigurationTests.java
@@ -0,0 +1,104 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.redis;
+
+import org.junit.After;
+import org.junit.Rule;
+import org.junit.Test;
+
+import org.springframework.boot.autoconfigure.PropertyPlaceholderAutoConfiguration;
+import org.springframework.boot.autoconfigure.TestAutoConfigurationPackage;
+import org.springframework.boot.autoconfigure.data.alt.redis.CityRedisRepository;
+import org.springframework.boot.autoconfigure.data.empty.EmptyDataPackage;
+import org.springframework.boot.autoconfigure.data.redis.city.City;
+import org.springframework.boot.autoconfigure.data.redis.city.CityRepository;
+import org.springframework.boot.redis.RedisTestServer;
+import org.springframework.context.annotation.AnnotationConfigApplicationContext;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.data.redis.repository.configuration.EnableRedisRepositories;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link RedisRepositoriesAutoConfiguration}.
+ *
+ * @author Eddú Meléndez
+ */
+public class RedisRepositoriesAutoConfigurationTests {
+
+	@Rule
+	public RedisTestServer redis = new RedisTestServer();
+
+	private AnnotationConfigApplicationContext context;
+
+	@After
+	public void close() {
+		this.context.close();
+	}
+
+	@Test
+	public void testDefaultRepositoryConfiguration() {
+		this.context = new AnnotationConfigApplicationContext();
+		this.context.register(TestConfiguration.class,
+				RedisAutoConfiguration.class, RedisRepositoriesAutoConfiguration.class,
+				PropertyPlaceholderAutoConfiguration.class);
+		this.context.refresh();
+		assertThat(this.context.getBean(CityRepository.class)).isNotNull();
+	}
+
+	@Test
+	public void testNoRepositoryConfiguration() {
+		this.context = new AnnotationConfigApplicationContext();
+		this.context.register(EmptyConfiguration.class,
+				RedisAutoConfiguration.class,
+				RedisRepositoriesAutoConfiguration.class,
+				PropertyPlaceholderAutoConfiguration.class);
+		this.context.refresh();
+		assertThat(this.context.getBean("redisTemplate")).isNotNull();
+	}
+
+	@Test
+	public void doesNotTriggerDefaultRepositoryDetectionIfCustomized() {
+		this.context = new AnnotationConfigApplicationContext();
+		this.context.register(CustomizedConfiguration.class,
+				RedisAutoConfiguration.class,
+				RedisRepositoriesAutoConfiguration.class,
+				PropertyPlaceholderAutoConfiguration.class);
+		this.context.refresh();
+		assertThat(this.context.getBean(CityRedisRepository.class)).isNotNull();
+	}
+
+	@Configuration
+	@TestAutoConfigurationPackage(City.class)
+	protected static class TestConfiguration {
+
+	}
+
+	@Configuration
+	@TestAutoConfigurationPackage(EmptyDataPackage.class)
+	protected static class EmptyConfiguration {
+
+	}
+
+	@Configuration
+	@TestAutoConfigurationPackage(RedisRepositoriesAutoConfigurationTests.class)
+	@EnableRedisRepositories(basePackageClasses = CityRedisRepository.class)
+	static class CustomizedConfiguration {
+
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/city/City.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/city/City.java
new file mode 100644
index 00000000000..eb6e215ab26
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/city/City.java
@@ -0,0 +1,70 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.redis.city;
+
+import java.io.Serializable;
+
+import org.springframework.data.annotation.Id;
+import org.springframework.data.redis.core.RedisHash;
+
+@RedisHash("cities")
+public class City implements Serializable {
+
+	private static final long serialVersionUID = 1L;
+
+	@Id
+	private Long id;
+
+	private String name;
+
+	private String state;
+
+	private String country;
+
+	private String map;
+
+	protected City() {
+	}
+
+	public City(String name, String country) {
+		super();
+		this.name = name;
+		this.country = country;
+	}
+
+	public String getName() {
+		return this.name;
+	}
+
+	public String getState() {
+		return this.state;
+	}
+
+	public String getCountry() {
+		return this.country;
+	}
+
+	public String getMap() {
+		return this.map;
+	}
+
+	@Override
+	public String toString() {
+		return getName() + "," + getState() + "," + getCountry();
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/city/CityRepository.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/city/CityRepository.java
new file mode 100644
index 00000000000..53c753da4c1
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/redis/city/CityRepository.java
@@ -0,0 +1,32 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.redis.city;
+
+import org.springframework.data.domain.Page;
+import org.springframework.data.domain.Pageable;
+import org.springframework.data.repository.Repository;
+
+public interface CityRepository extends Repository<City, Long> {
+
+	Page<City> findAll(Pageable pageable);
+
+	Page<City> findByNameLikeAndCountryLikeAllIgnoringCase(String name, String country,
+			Pageable pageable);
+
+	City findByNameAndCountryAllIgnoringCase(String name, String country);
+
+}
