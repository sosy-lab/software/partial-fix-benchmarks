diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/RestTemplateBuilder.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/RestTemplateBuilder.java
new file mode 100644
index 00000000000..135f27a35b4
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/RestTemplateBuilder.java
@@ -0,0 +1,101 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.web;
+
+import java.util.Arrays;
+import java.util.List;
+
+import org.springframework.http.client.ClientHttpRequestFactory;
+import org.springframework.http.converter.HttpMessageConverter;
+import org.springframework.util.Assert;
+import org.springframework.web.client.RestTemplate;
+
+/**
+ * A builder used to create {@link RestTemplate} with sensible defaults.
+ *
+ * @author Stephane Nicoll
+ * @since 1.4.0
+ */
+public class RestTemplateBuilder {
+
+	private ClientHttpRequestFactory requestFactory;
+
+	private List<HttpMessageConverter<?>> httpMessageConverters;
+
+	/**
+	 * Use the specified {@link ClientHttpRequestFactory}.
+	 * @param requestFactory the request factory to use
+	 * @return this builder instance
+	 * @see RestTemplate#setRequestFactory(ClientHttpRequestFactory)
+	 */
+	protected RestTemplateBuilder requestFactory(ClientHttpRequestFactory requestFactory) {
+		this.requestFactory = requestFactory;
+		return this;
+	}
+
+	/**
+	 * Use the {@link HttpMessageConverter} instances defined by the specified
+	 * {@link HttpMessageConverters}.
+	 * @param httpMessageConverters the http message converters to use
+	 * @return this builder instance
+	 * @see #httpMessageConverters(HttpMessageConverter[])
+	 */
+	protected RestTemplateBuilder httpMessageConverters(HttpMessageConverters httpMessageConverters) {
+		List<HttpMessageConverter<?>> converters = httpMessageConverters.getConverters();
+		return httpMessageConverters(converters.toArray(new HttpMessageConverter[converters.size()]));
+	}
+
+	/**
+	 * Use the specified {@link HttpMessageConverter} instances.
+	 * @param httpMessageConverters the http message converters to use
+	 * @return this builder instance
+	 * @see RestTemplate#setMessageConverters(List)
+	 */
+	protected RestTemplateBuilder httpMessageConverters(HttpMessageConverter<?>... httpMessageConverters) {
+		this.httpMessageConverters = Arrays.asList(httpMessageConverters);
+		return this;
+	}
+
+	/**
+	 * Build a new {@link RestTemplate} with the state of this instance.
+	 * @return a rest template with this builder's settings
+	 */
+	public RestTemplate build() {
+		RestTemplate restTemplate = createRestTemplate();
+		configure(restTemplate);
+		return restTemplate;
+	}
+
+	/**
+	 * Configure the specified {@link RestTemplate} with the state of this instance.
+	 * @param restTemplate the rest template to configure
+	 */
+	public void configure(RestTemplate restTemplate) {
+		Assert.notNull(restTemplate, "RestTemplate must not be null");
+		if (this.requestFactory != null) {
+			restTemplate.setRequestFactory(this.requestFactory);
+		}
+		if (this.httpMessageConverters != null) {
+			restTemplate.setMessageConverters(this.httpMessageConverters);
+		}
+	}
+
+	protected RestTemplate createRestTemplate() {
+		return new RestTemplate();
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/RestTemplateConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/RestTemplateConfiguration.java
new file mode 100644
index 00000000000..c2dc76d5c31
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/RestTemplateConfiguration.java
@@ -0,0 +1,82 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.web;
+
+import org.apache.http.client.HttpClient;
+
+import org.springframework.beans.factory.ObjectProvider;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.context.annotation.Bean;
+import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
+
+/**
+ * Actual RestTemplate configurations imported by {@link WebClientAutoConfiguration}.
+
+ * @author Stephane Nicoll
+ */
+abstract class RestTemplateConfiguration {
+
+	private final ObjectProvider<HttpMessageConverters> messageConvertersProvider;
+
+	protected RestTemplateConfiguration(ObjectProvider<HttpMessageConverters> messageConvertersProvider) {
+		this.messageConvertersProvider = messageConvertersProvider;
+	}
+
+	protected RestTemplateBuilder createRestTemplateBuilder() {
+		RestTemplateBuilder builder = new RestTemplateBuilder();
+		HttpMessageConverters messageConverters = this.messageConvertersProvider.getIfUnique();
+		if (messageConverters != null) {
+			builder.httpMessageConverters(messageConverters);
+		}
+		return builder;
+	}
+
+	@ConditionalOnClass(HttpClient.class)
+	public static class HttpComponents extends RestTemplateConfiguration {
+
+		HttpComponents(
+				ObjectProvider<HttpMessageConverters> messageConvertersProvider) {
+			super(messageConvertersProvider);
+		}
+
+		@Bean
+		@ConditionalOnMissingBean
+		public RestTemplateBuilder restTemplateBuilder() {
+			return createRestTemplateBuilder()
+					.requestFactory(new HttpComponentsClientHttpRequestFactory());
+		}
+
+	}
+
+	public static class DefaultRequestFactory extends RestTemplateConfiguration {
+
+		DefaultRequestFactory(
+				ObjectProvider<HttpMessageConverters> messageConvertersProvider) {
+			super(messageConvertersProvider);
+		}
+
+		@Bean
+		@ConditionalOnMissingBean
+		public RestTemplateBuilder restTemplateBuilder() {
+			return createRestTemplateBuilder()
+					.requestFactory(new HttpComponentsClientHttpRequestFactory());
+		}
+
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/WebClientAutoConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/WebClientAutoConfiguration.java
new file mode 100644
index 00000000000..55a72c9d16a
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/WebClientAutoConfiguration.java
@@ -0,0 +1,44 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.web;
+
+import org.springframework.boot.autoconfigure.AutoConfigureAfter;
+import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.context.annotation.Import;
+import org.springframework.web.client.RestTemplate;
+
+/**
+ * {@link EnableAutoConfiguration Auto-configuration} for web client.
+ *
+ * @author Stephane Nicoll
+ * @since 1.4.0
+ */
+@Configuration
+@AutoConfigureAfter(HttpMessageConvertersAutoConfiguration.class)
+public class WebClientAutoConfiguration {
+
+	@Configuration
+	@ConditionalOnClass(RestTemplate.class)
+	@Import({RestTemplateConfiguration.HttpComponents.class,
+			RestTemplateConfiguration.DefaultRequestFactory.class})
+	public static class RestTemplateConfigurations {
+
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories b/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
index b589ab2402f..189675065e9 100644
--- a/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
+++ b/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
@@ -94,6 +94,7 @@ org.springframework.boot.autoconfigure.web.HttpEncodingAutoConfiguration,\
 org.springframework.boot.autoconfigure.web.HttpMessageConvertersAutoConfiguration,\
 org.springframework.boot.autoconfigure.web.MultipartAutoConfiguration,\
 org.springframework.boot.autoconfigure.web.ServerPropertiesAutoConfiguration,\
+org.springframework.boot.autoconfigure.web.WebClientAutoConfiguration,\
 org.springframework.boot.autoconfigure.web.WebMvcAutoConfiguration,\
 org.springframework.boot.autoconfigure.websocket.WebSocketAutoConfiguration,\
 org.springframework.boot.autoconfigure.websocket.WebSocketMessagingAutoConfiguration,\
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/WebClientAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/WebClientAutoConfigurationTests.java
new file mode 100644
index 00000000000..fcb96ce20a7
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/WebClientAutoConfigurationTests.java
@@ -0,0 +1,136 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.web;
+
+import java.util.List;
+
+import org.assertj.core.api.Condition;
+import org.junit.After;
+import org.junit.Test;
+
+import org.springframework.context.annotation.AnnotationConfigApplicationContext;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
+import org.springframework.http.converter.HttpMessageConverter;
+import org.springframework.http.converter.StringHttpMessageConverter;
+import org.springframework.web.client.RestTemplate;
+
+import static org.assertj.core.api.Assertions.assertThat;
+
+/**
+ * Tests for {@link WebClientAutoConfiguration}
+ *
+ * @author Stephane Nicoll
+ */
+public class WebClientAutoConfigurationTests {
+
+	private AnnotationConfigApplicationContext context;
+
+	@After
+	public void close() {
+		if (this.context != null) {
+			this.context.close();
+		}
+	}
+
+	@Test
+	public void buildDefaultRestTemplate() {
+		load(HttpMessageConvertersAutoConfiguration.class, TestConfig.class);
+		assertThat(this.context.getBeansOfType(RestTemplate.class)).hasSize(1);
+		RestTemplate restTemplate = this.context.getBean(RestTemplate.class);
+		HttpMessageConverters messageConverters = this.context.getBean(HttpMessageConverters.class);
+		List<HttpMessageConverter<?>> converters = messageConverters.getConverters();
+		assertThat(restTemplate.getMessageConverters()).containsExactly(
+				converters.toArray(new HttpMessageConverter[converters.size()]));
+		assertThat(restTemplate.getRequestFactory())
+				.isInstanceOf(HttpComponentsClientHttpRequestFactory.class);
+	}
+
+	@Test
+	public void buildNoMessageConverters() {
+		load(TestConfig.class);
+		assertThat(this.context.getBeansOfType(RestTemplate.class)).hasSize(1);
+		RestTemplate restTemplate = this.context.getBean(RestTemplate.class);
+		RestTemplate defaultRestTemplate = new RestTemplate();
+		assertThat(restTemplate.getMessageConverters().size()).isEqualTo(
+				defaultRestTemplate.getMessageConverters().size());
+	}
+
+	@Test
+	public void buildCustomMessageConverters() {
+		load(CustomHttpMessageConverter.class,
+				HttpMessageConvertersAutoConfiguration.class, TestConfig.class);
+		assertThat(this.context.getBeansOfType(RestTemplate.class)).hasSize(1);
+		RestTemplate restTemplate = this.context.getBean(RestTemplate.class);
+		assertThat(restTemplate.getMessageConverters()).has(
+				new Condition<List<? extends HttpMessageConverter<?>>>() {
+			@Override
+			public boolean matches(List<? extends HttpMessageConverter<?>> value) {
+				for (HttpMessageConverter<?> httpMessageConverter : value) {
+					if (httpMessageConverter.getClass() == CustomHttpMessageConverter.class) {
+						return true;
+					}
+				}
+				return false;
+			}
+		});
+	}
+
+	@Test
+	public void buildCustomBuilder() {
+		load(TestConfig.class, CustomBuilderConfig.class);
+		assertThat(this.context.getBeansOfType(RestTemplate.class)).hasSize(1);
+		RestTemplate restTemplate = this.context.getBean(RestTemplate.class);
+		assertThat(restTemplate.getMessageConverters()).hasSize(1);
+		assertThat(restTemplate.getMessageConverters().get(0)).isInstanceOf(CustomHttpMessageConverter.class);
+	}
+
+
+	public void load(Class<?>... config) {
+		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
+		ctx.register(config);
+		ctx.register(WebClientAutoConfiguration.class);
+		ctx.refresh();
+		this.context = ctx;
+	}
+
+	@Configuration
+	static class TestConfig {
+
+		@Bean
+		public RestTemplate restTemplate(RestTemplateBuilder builder) {
+			return builder.build();
+		}
+
+	}
+
+	@Configuration
+	static class CustomBuilderConfig {
+
+		@Bean
+		public RestTemplateBuilder restTemplateBuilder() {
+			return new RestTemplateBuilder().httpMessageConverters(new CustomHttpMessageConverter());
+		}
+
+	}
+
+	static class CustomHttpMessageConverter extends StringHttpMessageConverter {
+
+	}
+
+}
