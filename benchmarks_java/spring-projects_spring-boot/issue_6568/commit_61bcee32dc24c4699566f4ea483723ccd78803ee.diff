diff --git a/spring-boot/src/main/java/org/springframework/boot/diagnostics/analyzer/HikariAmbiguousDriverFailureAnalyzer.java b/spring-boot/src/main/java/org/springframework/boot/diagnostics/analyzer/HikariAmbiguousDriverFailureAnalyzer.java
new file mode 100644
index 00000000000..62c30dbb79e
--- /dev/null
+++ b/spring-boot/src/main/java/org/springframework/boot/diagnostics/analyzer/HikariAmbiguousDriverFailureAnalyzer.java
@@ -0,0 +1,43 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.diagnostics.analyzer;
+
+import org.springframework.boot.diagnostics.AbstractFailureAnalyzer;
+import org.springframework.boot.diagnostics.FailureAnalysis;
+
+/**
+ * An {@link AbstractFailureAnalyzer} that performs analysis of failures caused by
+ * ambiguous configuration of HikariCP connection semantics (Driver or DataSource?).
+ *
+ * @author Brett Wooldridge
+ */
+public class HikariAmbiguousDriverFailureAnalyzer extends AbstractFailureAnalyzer<IllegalArgumentException> {
+
+	@Override
+	protected FailureAnalysis analyze(Throwable rootFailure, IllegalArgumentException cause) {
+		if (!cause.getMessage().contains("cannot use driverClassName and dataSourceClassName")) {
+			return null;
+		}
+
+		String description = "Configuration of the HikariCP connection pool failed. The\n"
+				+ "'dataSourceClassName' property is not supported by Spring Boot.";
+
+		return new FailureAnalysis(description,
+				"Update your application's configuration to specify only the JDBC 'url' and/or 'driverClassName'",
+				cause);
+	}
+}
diff --git a/spring-boot/src/main/resources/META-INF/spring.factories b/spring-boot/src/main/resources/META-INF/spring.factories
index fe6431b8886..5735eba2d76 100644
--- a/spring-boot/src/main/resources/META-INF/spring.factories
+++ b/spring-boot/src/main/resources/META-INF/spring.factories
@@ -38,7 +38,8 @@ org.springframework.boot.diagnostics.analyzer.BeanNotOfRequiredTypeFailureAnalyz
 org.springframework.boot.diagnostics.analyzer.BindFailureAnalyzer,\
 org.springframework.boot.diagnostics.analyzer.NoUniqueBeanDefinitionFailureAnalyzer,\
 org.springframework.boot.diagnostics.analyzer.PortInUseFailureAnalyzer,\
-org.springframework.boot.diagnostics.analyzer.ValidationExceptionFailureAnalyzer
+org.springframework.boot.diagnostics.analyzer.ValidationExceptionFailureAnalyzer,\
+org.springframework.boot.diagnostics.analyzer.HikariAmbiguousDriverFailureAnalyzer
 
 # FailureAnalysisReporters
 org.springframework.boot.diagnostics.FailureAnalysisReporter=\
