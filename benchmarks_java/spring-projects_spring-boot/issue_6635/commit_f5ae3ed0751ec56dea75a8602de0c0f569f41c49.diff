diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaBaseConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaBaseConfiguration.java
index 67d1ccbd962..5191e412965 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaBaseConfiguration.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaBaseConfiguration.java
@@ -27,6 +27,7 @@
 import org.springframework.beans.factory.BeanFactoryAware;
 import org.springframework.beans.factory.ObjectProvider;
 import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
+import org.springframework.boot.ApplicationInfo;
 import org.springframework.boot.autoconfigure.AutoConfigurationPackages;
 import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
@@ -105,6 +106,9 @@ public EntityManagerFactoryBuilder entityManagerFactoryBuilder(
 				jpaVendorAdapter, this.properties.getProperties(),
 				persistenceUnitManagerProvider.getIfAvailable());
 		builder.setCallback(getVendorCallback());
+		if (this.beanFactory.containsBean("springApplicationInfo")) {
+			builder.setApplicationInfo(this.beanFactory.getBean(ApplicationInfo.class));
+		}
 		return builder;
 	}
 
diff --git a/spring-boot/src/main/java/org/springframework/boot/ApplicationInfo.java b/spring-boot/src/main/java/org/springframework/boot/ApplicationInfo.java
new file mode 100644
index 00000000000..8f5d2ddc0f3
--- /dev/null
+++ b/spring-boot/src/main/java/org/springframework/boot/ApplicationInfo.java
@@ -0,0 +1,63 @@
+/*
+ * Copyright 2012-2016 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot;
+
+/**
+ * Provides various information about a Spring Boot application instance.
+ *
+ * @author Stephane Nicoll
+ * @since 1.4.1
+ */
+public final class ApplicationInfo {
+
+	private final Class<?> mainApplicationClass;
+
+	private final ApplicationArguments applicationArguments;
+
+	private final Banner banner;
+
+	protected ApplicationInfo(SpringApplication application, ApplicationArguments applicationArguments, Banner banner) {
+		this.mainApplicationClass = application.getMainApplicationClass();
+		this.applicationArguments = applicationArguments;
+		this.banner = banner;
+	}
+
+	/**
+	 * Returns the main application class that has been deduced or explicitly configured.
+	 * @return the main application class or {@code null}
+	 */
+	public Class<?> getMainApplicationClass() {
+		return this.mainApplicationClass;
+	}
+
+	/**
+	 * Returns the {@link ApplicationArguments} used to start this instance.
+	 * @return the application arguments
+	 */
+	public ApplicationArguments getApplicationArguments() {
+		return this.applicationArguments;
+	}
+
+	/**
+	 * Returns the {@link Banner} used by the instance.
+	 * @return the banner or {@code null}
+	 */
+	public Banner getBanner() {
+		return this.banner;
+	}
+
+}
diff --git a/spring-boot/src/main/java/org/springframework/boot/SpringApplication.java b/spring-boot/src/main/java/org/springframework/boot/SpringApplication.java
index 1d63af9cb4b..3c313f8e32b 100644
--- a/spring-boot/src/main/java/org/springframework/boot/SpringApplication.java
+++ b/spring-boot/src/main/java/org/springframework/boot/SpringApplication.java
@@ -357,6 +357,10 @@ private void prepareContext(ConfigurableApplicationContext context,
 		if (printedBanner != null) {
 			context.getBeanFactory().registerSingleton("springBootBanner", printedBanner);
 		}
+		ApplicationInfo applicationInfo = new ApplicationInfo(this, applicationArguments,
+				printedBanner);
+		context.getBeanFactory().registerSingleton("springApplicationInfo",
+				applicationInfo);
 
 		// Load the sources
 		Set<Object> sources = getSources();
diff --git a/spring-boot/src/main/java/org/springframework/boot/orm/jpa/EntityManagerFactoryBuilder.java b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/EntityManagerFactoryBuilder.java
index 1542ead3276..15d39139f93 100644
--- a/spring-boot/src/main/java/org/springframework/boot/orm/jpa/EntityManagerFactoryBuilder.java
+++ b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/EntityManagerFactoryBuilder.java
@@ -16,6 +16,7 @@
 
 package org.springframework.boot.orm.jpa;
 
+import java.net.URL;
 import java.util.HashMap;
 import java.util.HashSet;
 import java.util.LinkedHashMap;
@@ -24,10 +25,15 @@
 
 import javax.sql.DataSource;
 
+import org.apache.commons.logging.Log;
+import org.apache.commons.logging.LogFactory;
+
+import org.springframework.boot.ApplicationInfo;
 import org.springframework.orm.jpa.JpaVendorAdapter;
 import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
 import org.springframework.orm.jpa.persistenceunit.PersistenceUnitManager;
 import org.springframework.util.ClassUtils;
+import org.springframework.util.ResourceUtils;
 
 /**
  * Convenient builder for JPA EntityManagerFactory instances. Collects common
@@ -43,6 +49,8 @@
  */
 public class EntityManagerFactoryBuilder {
 
+	private static final Log logger = LogFactory.getLog(EntityManagerFactoryBuilder.class);
+
 	private JpaVendorAdapter jpaVendorAdapter;
 
 	private PersistenceUnitManager persistenceUnitManager;
@@ -51,6 +59,8 @@
 
 	private EntityManagerFactoryBeanCallback callback;
 
+	private Class<?> applicationClass;
+
 	/**
 	 * Create a new instance passing in the common pieces that will be shared if multiple
 	 * EntityManagerFactory instances are created.
@@ -78,6 +88,34 @@ public void setCallback(EntityManagerFactoryBeanCallback callback) {
 		this.callback = callback;
 	}
 
+	/**
+	 * An optional {@link ApplicationInfo} used to further tune the entity manager.
+	 * @param applicationInfo the application info
+	 */
+	public void setApplicationInfo(ApplicationInfo applicationInfo) {
+		this.applicationClass = applicationInfo.getMainApplicationClass();
+	}
+
+	/**
+	 * Determine a persistence unit root location to use if no {@code persistence.xml} or
+	 * {@code orm.xml} are present in the project.
+	 * @return the persistence unit root location or {@code null}
+	 */
+	protected String determinePersistenceUnitRootLocation() {
+		if (this.applicationClass != null) {
+			try {
+				URL mainLocation = this.applicationClass.getProtectionDomain().
+						getCodeSource().getLocation();
+				return ResourceUtils.extractJarFileURL(mainLocation).toString();
+			}
+			catch (Exception ex) {
+				logger.info("Could not determine persistence unit root location: " + ex);
+			}
+		}
+		return null;
+	}
+
+
 	/**
 	 * A fluent builder for a LocalContainerEntityManagerFactoryBean.
 	 */
@@ -181,6 +219,10 @@ public LocalContainerEntityManagerFactoryBean build() {
 			entityManagerFactoryBean.getJpaPropertyMap()
 					.putAll(EntityManagerFactoryBuilder.this.jpaProperties);
 			entityManagerFactoryBean.getJpaPropertyMap().putAll(this.properties);
+			String rootLocation = determinePersistenceUnitRootLocation();
+			if (rootLocation != null) {
+				entityManagerFactoryBean.setPersistenceUnitRootLocation(rootLocation);
+			}
 			if (EntityManagerFactoryBuilder.this.callback != null) {
 				EntityManagerFactoryBuilder.this.callback
 						.execute(entityManagerFactoryBean);
diff --git a/spring-boot/src/test/java/org/springframework/boot/SpringApplicationTests.java b/spring-boot/src/test/java/org/springframework/boot/SpringApplicationTests.java
index 10aafc820ef..5c1cd44b3f0 100644
--- a/spring-boot/src/test/java/org/springframework/boot/SpringApplicationTests.java
+++ b/spring-boot/src/test/java/org/springframework/boot/SpringApplicationTests.java
@@ -778,6 +778,21 @@ public void headlessSystemPropertyTakesPrecedence() throws Exception {
 		assertThat(System.getProperty("java.awt.headless")).isEqualTo("false");
 	}
 
+	@Test
+	public void getApplicationInfo() {
+		TestSpringApplication application = new TestSpringApplication(
+				ExampleConfig.class);
+		application.setWebEnvironment(false);
+		this.context = application.run("foo");
+		ApplicationInfo applicationInfo = this.context.getBean(ApplicationInfo.class);
+		assertThat(application.getMainApplicationClass()).isEqualTo(application
+				.getMainApplicationClass());
+		assertThat(applicationInfo.getApplicationArguments()).isNotNull();
+		assertThat(applicationInfo.getApplicationArguments().getNonOptionArgs())
+				.containsExactly("foo");
+		assertThat(applicationInfo.getBanner()).isNotNull();
+	}
+
 	@Test
 	public void getApplicationArgumentsBean() throws Exception {
 		TestSpringApplication application = new TestSpringApplication(
