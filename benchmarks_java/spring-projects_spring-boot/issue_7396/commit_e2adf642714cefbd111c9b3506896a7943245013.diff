diff --git a/spring-boot-actuator/pom.xml b/spring-boot-actuator/pom.xml
index 7434a07c059..170ba0f5b08 100644
--- a/spring-boot-actuator/pom.xml
+++ b/spring-boot-actuator/pom.xml
@@ -307,6 +307,10 @@
 			<artifactId>json-path</artifactId>
 			<scope>test</scope>
 		</dependency>
+		<dependency>
+			<groupId>org.skyscreamer</groupId>
+			<artifactId>jsonassert</artifactId>
+		</dependency>
 		<dependency>
 			<groupId>io.undertow</groupId>
 			<artifactId>undertow-core</artifactId>
diff --git a/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/endpoint/LoggersEndpoint.java b/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/endpoint/LoggersEndpoint.java
index 334d5782957..1c8a2f98853 100644
--- a/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/endpoint/LoggersEndpoint.java
+++ b/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/endpoint/LoggersEndpoint.java
@@ -20,6 +20,8 @@
 import java.util.Collections;
 import java.util.LinkedHashMap;
 import java.util.Map;
+import java.util.Set;
+import java.util.TreeSet;
 
 import org.springframework.boot.context.properties.ConfigurationProperties;
 import org.springframework.boot.logging.LogLevel;
@@ -36,7 +38,7 @@
  */
 @ConfigurationProperties(prefix = "endpoints.loggers")
 public class LoggersEndpoint
-		extends AbstractEndpoint<Map<String, LoggersEndpoint.LoggerLevels>> {
+		extends AbstractEndpoint<Map<String, Object>> {
 
 	private final LoggingSystem loggingSystem;
 
@@ -51,20 +53,27 @@ public LoggersEndpoint(LoggingSystem loggingSystem) {
 	}
 
 	@Override
-	public Map<String, LoggerLevels> invoke() {
+	public Map<String, Object> invoke() {
 		Collection<LoggerConfiguration> configurations = this.loggingSystem
 				.getLoggerConfigurations();
 		if (configurations == null) {
 			return Collections.emptyMap();
 		}
-		Map<String, LoggerLevels> result = new LinkedHashMap<String, LoggerLevels>(
+		Map<String, LoggerLevels> loggers = new LinkedHashMap<String, LoggerLevels>(
 				configurations.size());
 		for (LoggerConfiguration configuration : configurations) {
-			result.put(configuration.getName(), new LoggerLevels(configuration));
+			loggers.put(configuration.getName(), new LoggerLevels(configuration));
 		}
+		Map<String, Object> result = new LinkedHashMap<String, Object>();
+		result.put("levels", sortLevels(loggingSystem.getSupportedLogLevels()));
+		result.put("loggers", loggers);
 		return result;
 	}
 
+	private Set<LogLevel> sortLevels(Set<LogLevel> levels) {
+		return new TreeSet<LogLevel>(levels).descendingSet();
+	}
+
 	public LoggerLevels invoke(String name) {
 		Assert.notNull(name, "Name must not be null");
 		LoggerConfiguration configuration = this.loggingSystem
diff --git a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/EndpointAutoConfigurationTests.java b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/EndpointAutoConfigurationTests.java
index e41fc55a8c7..89e81f3f59d 100644
--- a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/EndpointAutoConfigurationTests.java
+++ b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/EndpointAutoConfigurationTests.java
@@ -130,7 +130,8 @@ public void healthEndpointWithDefaultHealthIndicator() {
 	public void loggersEndpointHasLoggers() throws Exception {
 		load(CustomLoggingConfig.class, EndpointAutoConfiguration.class);
 		LoggersEndpoint endpoint = this.context.getBean(LoggersEndpoint.class);
-		Map<String, LoggerLevels> loggers = endpoint.invoke();
+		Map<String, Object> result = endpoint.invoke();
+		Map<String, LoggerLevels> loggers = (Map<String, LoggerLevels>) result.get("loggers");
 		assertThat(loggers.size()).isGreaterThan(0);
 	}
 
diff --git a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/LoggersEndpointTests.java b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/LoggersEndpointTests.java
index 21e27f7b8c7..360c04145d9 100644
--- a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/LoggersEndpointTests.java
+++ b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/LoggersEndpointTests.java
@@ -17,6 +17,11 @@
 package org.springframework.boot.actuate.endpoint;
 
 import java.util.Collections;
+import java.util.EnumSet;
+import java.util.List;
+import java.util.Map;
+import java.util.Set;
+import java.util.TreeSet;
 
 import org.junit.Test;
 
@@ -48,9 +53,13 @@ public LoggersEndpointTests() {
 	public void invokeShouldReturnConfigurations() throws Exception {
 		given(getLoggingSystem().getLoggerConfigurations()).willReturn(Collections
 				.singletonList(new LoggerConfiguration("ROOT", null, LogLevel.DEBUG)));
-		LoggerLevels levels = getEndpointBean().invoke().get("ROOT");
-		assertThat(levels.getConfiguredLevel()).isNull();
-		assertThat(levels.getEffectiveLevel()).isEqualTo("DEBUG");
+		given(getLoggingSystem().getSupportedLogLevels()).willReturn(EnumSet.allOf(LogLevel.class));
+		Map<String, LoggerLevels> loggers = (Map<String, LoggerLevels>) getEndpointBean().invoke().get("loggers");
+		Set<LogLevel> levels = (Set<LogLevel>) getEndpointBean().invoke().get("levels");
+		LoggerLevels rootLevels = loggers.get("ROOT");
+		assertThat(rootLevels.getConfiguredLevel()).isNull();
+		assertThat(rootLevels.getEffectiveLevel()).isEqualTo("DEBUG");
+		assertThat(levels).containsExactly(LogLevel.OFF, LogLevel.FATAL, LogLevel.ERROR, LogLevel.WARN, LogLevel.INFO, LogLevel.DEBUG, LogLevel.TRACE);
 	}
 
 	public void invokeWhenNameSpecifiedShouldReturnLevels() throws Exception {
diff --git a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/mvc/LoggersMvcEndpointTests.java b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/mvc/LoggersMvcEndpointTests.java
index ab769ee67ff..eb60d463fe5 100644
--- a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/mvc/LoggersMvcEndpointTests.java
+++ b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/endpoint/mvc/LoggersMvcEndpointTests.java
@@ -17,6 +17,9 @@
 package org.springframework.boot.actuate.endpoint.mvc;
 
 import java.util.Collections;
+import java.util.EnumSet;
+import java.util.LinkedHashSet;
+import java.util.Set;
 
 import org.junit.After;
 import org.junit.Before;
@@ -80,18 +83,22 @@ public void setUp() {
 				.alwaysDo(MockMvcResultHandlers.print()).build();
 	}
 
+	@Before
 	@After
-	public void reset() {
+	public void resetMocks() {
 		Mockito.reset(this.loggingSystem);
+		given(this.loggingSystem.getSupportedLogLevels()).willReturn(EnumSet.allOf(LogLevel.class));
 	}
 
 	@Test
 	public void getLoggerShouldReturnAllLoggerConfigurations() throws Exception {
 		given(this.loggingSystem.getLoggerConfigurations()).willReturn(Collections
 				.singletonList(new LoggerConfiguration("ROOT", null, LogLevel.DEBUG)));
+		String expected = "{\"levels\":[\"OFF\",\"FATAL\",\"ERROR\",\"WARN\",\"INFO\",\"DEBUG\",\"TRACE\"]," +
+				"\"loggers\":{\"ROOT\":{\"configuredLevel\":null,\"effectiveLevel\":\"DEBUG\"}}}";
+		System.out.println(expected);
 		this.mvc.perform(get("/loggers")).andExpect(status().isOk())
-				.andExpect(content().string(equalTo("{\"ROOT\":{\"configuredLevel\":"
-						+ "null,\"effectiveLevel\":\"DEBUG\"}}")));
+				.andExpect(content().json(expected));
 	}
 
 	@Test
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java
index d3521e2cedf..ac366984d76 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/AbstractLoggingSystem.java
@@ -17,7 +17,9 @@
 package org.springframework.boot.logging;
 
 import java.util.HashMap;
+import java.util.LinkedHashSet;
 import java.util.Map;
+import java.util.Set;
 
 import org.springframework.core.env.Environment;
 import org.springframework.core.io.ClassPathResource;
@@ -193,7 +195,9 @@ public LogLevels() {
 
 		public void map(LogLevel system, T nativeLevel) {
 			this.systemToNative.put(system, nativeLevel);
-			this.nativeToSystem.put(nativeLevel, system);
+			if(!this.nativeToSystem.containsKey(nativeLevel)) {
+				this.nativeToSystem.put(nativeLevel, system);
+			}
 		}
 
 		public LogLevel convertNativeToSystem(T level) {
@@ -204,6 +208,10 @@ public T convertSystemToNative(LogLevel level) {
 			return this.systemToNative.get(level);
 		}
 
+		public Set<LogLevel> getSupported() {
+			return new LinkedHashSet<LogLevel>(nativeToSystem.values());
+		}
+
 	}
 
 }
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/LoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/LoggingSystem.java
index a2e1758037e..ce5ea6db1e6 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/LoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/LoggingSystem.java
@@ -17,9 +17,13 @@
 package org.springframework.boot.logging;
 
 import java.util.Collections;
+import java.util.EnumSet;
 import java.util.LinkedHashMap;
 import java.util.List;
 import java.util.Map;
+import java.util.Set;
+
+import org.apache.commons.logging.Log;
 
 import org.springframework.util.ClassUtils;
 import org.springframework.util.StringUtils;
@@ -94,6 +98,14 @@ public Runnable getShutdownHandler() {
 		return null;
 	}
 
+	/**
+	 * Returns a set of the {@link LogLevel LogLevels} that are actually supported by the logging system.
+	 * @return
+	 */
+	public Set<LogLevel> getSupportedLogLevels() {
+		return EnumSet.allOf(LogLevel.class);
+	}
+
 	/**
 	 * Sets the logging level for a given logger.
 	 * @param loggerName the name of the logger to set
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java
index d23a99d337b..da05c83f7f3 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/java/JavaLoggingSystem.java
@@ -22,6 +22,7 @@
 import java.util.Collections;
 import java.util.Enumeration;
 import java.util.List;
+import java.util.Set;
 import java.util.logging.Level;
 import java.util.logging.LogManager;
 import java.util.logging.Logger;
@@ -113,6 +114,11 @@ protected void loadConfiguration(String location, LogFile logFile) {
 		}
 	}
 
+	@Override
+	public Set<LogLevel> getSupportedLogLevels() {
+		return LEVELS.getSupported();
+	}
+
 	@Override
 	public void setLogLevel(String loggerName, LogLevel level) {
 		Assert.notNull(level, "Level must not be null");
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java
index bfca2884d1a..3ccc3c62fe9 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/log4j2/Log4J2LoggingSystem.java
@@ -22,6 +22,7 @@
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.List;
+import java.util.Set;
 
 import org.apache.logging.log4j.Level;
 import org.apache.logging.log4j.LogManager;
@@ -197,6 +198,11 @@ protected void reinitialize(LoggingInitializationContext initializationContext)
 		getLoggerContext().reconfigure();
 	}
 
+	@Override
+	public Set<LogLevel> getSupportedLogLevels() {
+		return LEVELS.getSupported();
+	}
+
 	@Override
 	public void setLogLevel(String loggerName, LogLevel logLevel) {
 		Level level = LEVELS.convertSystemToNative(logLevel);
diff --git a/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java b/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java
index f58ba8f57e7..c71971466e5 100644
--- a/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java
+++ b/spring-boot/src/main/java/org/springframework/boot/logging/logback/LogbackLoggingSystem.java
@@ -22,6 +22,7 @@
 import java.util.ArrayList;
 import java.util.Collections;
 import java.util.List;
+import java.util.Set;
 
 import ch.qos.logback.classic.Level;
 import ch.qos.logback.classic.LoggerContext;
@@ -238,6 +239,11 @@ private LoggerConfiguration getLoggerConfiguration(
 		return new LoggerConfiguration(logger.getName(), level, effectiveLevel);
 	}
 
+	@Override
+	public Set<LogLevel> getSupportedLogLevels() {
+		return LEVELS.getSupported();
+	}
+
 	@Override
 	public void setLogLevel(String loggerName, LogLevel level) {
 		ch.qos.logback.classic.Logger logger = getLogger(loggerName);
diff --git a/spring-boot/src/test/java/org/springframework/boot/logging/logback/LogbackLoggingSystemTests.java b/spring-boot/src/test/java/org/springframework/boot/logging/logback/LogbackLoggingSystemTests.java
index 072b9b82108..42a95da9d7f 100644
--- a/spring-boot/src/test/java/org/springframework/boot/logging/logback/LogbackLoggingSystemTests.java
+++ b/spring-boot/src/test/java/org/springframework/boot/logging/logback/LogbackLoggingSystemTests.java
@@ -18,6 +18,7 @@
 
 import java.io.File;
 import java.io.FileReader;
+import java.util.EnumSet;
 import java.util.List;
 import java.util.logging.Handler;
 import java.util.logging.LogManager;
@@ -162,6 +163,11 @@ public void testNonexistentConfigLocation() throws Exception {
 				"classpath:logback-nonexistent.xml", null);
 	}
 
+	@Test
+	public getSupportedLevels() {
+		assertThat(this.loggingSystem.getSupportedLevels).isEqualTo(EnumSet.of(LogLevel.DEBUG)));
+	}
+
 	@Test
 	public void setLevel() throws Exception {
 		this.loggingSystem.beforeInitialize();
