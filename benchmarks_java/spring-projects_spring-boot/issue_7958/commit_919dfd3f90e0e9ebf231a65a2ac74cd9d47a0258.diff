diff --git a/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerProperties.java b/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerProperties.java
index 7a19af56a14..7ffe28d930a 100644
--- a/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerProperties.java
+++ b/spring-boot-actuator/src/main/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerProperties.java
@@ -17,14 +17,8 @@
 package org.springframework.boot.actuate.autoconfigure.web;
 
 import java.net.InetAddress;
-import java.util.ArrayList;
-import java.util.Collections;
-import java.util.List;
-
-import javax.servlet.http.HttpSession;
 
 import org.springframework.boot.autoconfigure.security.SecurityPrerequisite;
-import org.springframework.boot.autoconfigure.security.SecurityProperties;
 import org.springframework.boot.autoconfigure.web.ServerProperties;
 import org.springframework.boot.context.properties.ConfigurationProperties;
 import org.springframework.boot.context.properties.NestedConfigurationProperty;
@@ -43,26 +37,6 @@
 @ConfigurationProperties(prefix = "management", ignoreUnknownFields = true)
 public class ManagementServerProperties implements SecurityPrerequisite {
 
-	/**
-	 * Order applied to the WebSecurityConfigurerAdapter that is used to configure basic
-	 * authentication for management endpoints. If you want to add your own authentication
-	 * for all or some of those endpoints the best thing to do is to add your own
-	 * WebSecurityConfigurerAdapter with lower order, for instance by using
-	 * {@code ACCESS_OVERRIDE_ORDER}.
-	 */
-	public static final int BASIC_AUTH_ORDER = SecurityProperties.BASIC_AUTH_ORDER - 5;
-
-	/**
-	 * Order before the basic authentication access control provided automatically for the
-	 * management endpoints. This is a useful place to put user-defined access rules if
-	 * you want to override the default access rules for the management endpoints. If you
-	 * want to keep the default rules for management endpoints but want to override the
-	 * security for the rest of the application, use
-	 * {@code SecurityProperties.ACCESS_OVERRIDE_ORDER} instead.
-	 */
-	public static final int ACCESS_OVERRIDE_ORDER = ManagementServerProperties.BASIC_AUTH_ORDER
-			- 1;
-
 	/**
 	 * Management endpoint HTTP port. Use the same port as the application by default.
 	 */
@@ -86,8 +60,6 @@
 	 */
 	private boolean addApplicationContextHeader = false;
 
-	private final Security security = new Security();
-
 	/**
 	 * Returns the management port or {@code null} if the
 	 * {@link ServerProperties#getPort() server port} should be used.
@@ -144,10 +116,6 @@ private String cleanContextPath(String contextPath) {
 		return contextPath;
 	}
 
-	public Security getSecurity() {
-		return this.security;
-	}
-
 	public boolean getAddApplicationContextHeader() {
 		return this.addApplicationContextHeader;
 	}
@@ -156,77 +124,4 @@ public void setAddApplicationContextHeader(boolean addApplicationContextHeader)
 		this.addApplicationContextHeader = addApplicationContextHeader;
 	}
 
-	/**
-	 * Security configuration.
-	 */
-	public static class Security {
-
-		/**
-		 * Enable security.
-		 */
-		private boolean enabled = true;
-
-		/**
-		 * Comma-separated list of roles that can access the management endpoint.
-		 */
-		private List<String> roles = new ArrayList<>(
-				Collections.singletonList("ACTUATOR"));
-
-		/**
-		 * Session creating policy for security use (always, never, if_required,
-		 * stateless).
-		 */
-		private SessionCreationPolicy sessions = SessionCreationPolicy.STATELESS;
-
-		public SessionCreationPolicy getSessions() {
-			return this.sessions;
-		}
-
-		public void setSessions(SessionCreationPolicy sessions) {
-			this.sessions = sessions;
-		}
-
-		public void setRoles(List<String> roles) {
-			this.roles = roles;
-		}
-
-		public List<String> getRoles() {
-			return this.roles;
-		}
-
-		public boolean isEnabled() {
-			return this.enabled;
-		}
-
-		public void setEnabled(boolean enabled) {
-			this.enabled = enabled;
-		}
-
-	}
-
-	public enum SessionCreationPolicy {
-
-		/**
-		 * Always create an {@link HttpSession}.
-		 */
-		ALWAYS,
-
-		/**
-		 * Never create an {@link HttpSession}, but use any {@link HttpSession} that
-		 * already exists.
-		 */
-		NEVER,
-
-		/**
-		 * Only create an {@link HttpSession} if required.
-		 */
-		IF_REQUIRED,
-
-		/**
-		 * Never create an {@link HttpSession}.
-		 */
-		STATELESS
-
-	}
-
 }
diff --git a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesNoSecurityTests.java b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesNoSecurityTests.java
deleted file mode 100644
index 3a621ca2cb3..00000000000
--- a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesNoSecurityTests.java
+++ /dev/null
@@ -1,69 +0,0 @@
-/*
- * Copyright 2012-2017 the original author or authors.
- *
- * Licensed under the Apache License, Version 2.0 (the "License");
- * you may not use this file except in compliance with the License.
- * You may obtain a copy of the License at
- *
- *      http://www.apache.org/licenses/LICENSE-2.0
- *
- * Unless required by applicable law or agreed to in writing, software
- * distributed under the License is distributed on an "AS IS" BASIS,
- * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
- * See the License for the specific language governing permissions and
- * limitations under the License.
- */
-
-package org.springframework.boot.actuate.autoconfigure.web;
-
-import org.junit.After;
-import org.junit.Test;
-import org.junit.runner.RunWith;
-
-import org.springframework.boot.context.properties.EnableConfigurationProperties;
-import org.springframework.boot.test.util.TestPropertyValues;
-import org.springframework.boot.testsupport.runner.classpath.ClassPathExclusions;
-import org.springframework.boot.testsupport.runner.classpath.ModifiedClassPathRunner;
-import org.springframework.context.annotation.AnnotationConfigApplicationContext;
-
-import static org.assertj.core.api.Assertions.assertThat;
-
-/**
- * Tests for {@link ManagementServerProperties} when Spring Security is not present.
- *
- * @author Stephane Nicoll
- */
-@RunWith(ModifiedClassPathRunner.class)
-@ClassPathExclusions("spring-security-*.jar")
-public class ManagementServerPropertiesNoSecurityTests {
-
-	private AnnotationConfigApplicationContext context;
-
-	@After
-	public void close() {
-		if (this.context != null) {
-			this.context.close();
-		}
-	}
-
-	@Test
-	public void securitySettingsIgnoredWithoutSpringSecurity() {
-		ManagementServerProperties properties = load("management.security.enabled=false");
-		assertThat(properties.getSecurity().isEnabled()).isFalse();
-	}
-
-	public ManagementServerProperties load(String... environment) {
-		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext();
-		TestPropertyValues.of(environment).applyTo(context);
-		context.register(Config.class);
-		context.refresh();
-		this.context = context;
-		return this.context.getBean(ManagementServerProperties.class);
-	}
-
-	@EnableConfigurationProperties(ManagementServerProperties.class)
-	protected static class Config {
-
-	}
-
-}
diff --git a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesTests.java b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesTests.java
index 7eb5964a382..88f326cecb0 100644
--- a/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesTests.java
+++ b/spring-boot-actuator/src/test/java/org/springframework/boot/actuate/autoconfigure/web/ManagementServerPropertiesTests.java
@@ -73,19 +73,6 @@ public void slashOfContextPathIsDefaultValue() {
 		assertThat(properties.getContextPath()).isEqualTo("");
 	}
 
-	@Test
-	public void managementRolesSetMultipleRoles() {
-		ManagementServerProperties properties = load(
-				"management.security.roles=FOO,BAR,BIZ");
-		assertThat(properties.getSecurity().getRoles()).containsOnly("FOO", "BAR", "BIZ");
-	}
-
-	@Test
-	public void managementRolesAllowsIndexedAccess() {
-		ManagementServerProperties properties = load("management.security.roles[0]=FOO");
-		assertThat(properties.getSecurity().getRoles()).containsOnly("FOO");
-	}
-
 	public ManagementServerProperties load(String... environment) {
 		AnnotationConfigApplicationContext ctx = new AnnotationConfigApplicationContext();
 		TestPropertyValues.of(environment).applyTo(ctx);
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityAuthorizeMode.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityAuthorizeMode.java
deleted file mode 100644
index 33a66eef5a2..00000000000
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityAuthorizeMode.java
+++ /dev/null
@@ -1,42 +0,0 @@
-/*
- * Copyright 2012-2015 the original author or authors.
- *
- * Licensed under the Apache License, Version 2.0 (the "License");
- * you may not use this file except in compliance with the License.
- * You may obtain a copy of the License at
- *
- *      http://www.apache.org/licenses/LICENSE-2.0
- *
- * Unless required by applicable law or agreed to in writing, software
- * distributed under the License is distributed on an "AS IS" BASIS,
- * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
- * See the License for the specific language governing permissions and
- * limitations under the License.
- */
-
-package org.springframework.boot.autoconfigure.security;
-
-/**
- * Security authorization modes as specified in {@link SecurityProperties}.
- *
- * @author Phillip Webb
- * @since 1.2.2
- */
-public enum SecurityAuthorizeMode {
-
-	/**
-	 * Must be a member of one of the security roles.
-	 */
-	ROLE,
-
-	/**
-	 * Must be an authenticated user.
-	 */
-	AUTHENTICATED,
-
-	/**
-	 * No security authorization is setup.
-	 */
-	NONE
-
-}
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityProperties.java
index b748f11d348..5683843c39e 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/SecurityProperties.java
@@ -34,14 +34,6 @@
 @ConfigurationProperties(prefix = "security")
 public class SecurityProperties implements SecurityPrerequisite {
 
-	/**
-	 * Order before the basic authentication access control provided by Boot. This is a
-	 * useful place to put user-defined access rules if you want to override the default
-	 * access rules.
-	 */
-	public static final int ACCESS_OVERRIDE_ORDER = SecurityProperties.BASIC_AUTH_ORDER
-			- 2;
-
 	/**
 	 * Order applied to the WebSecurityConfigurerAdapter that is used to configure basic
 	 * authentication for application endpoints. If you want to add your own
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/EnableOAuth2Sso.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/EnableOAuth2Sso.java
index f06d3c61768..80b63b29f29 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/EnableOAuth2Sso.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/EnableOAuth2Sso.java
@@ -33,8 +33,7 @@
  * {@link WebSecurityConfigurerAdapter} provided by the user and annotated with
  * {@code @EnableOAuth2Sso}, it is enhanced by adding an authentication filter and an
  * authentication entry point. If the user only has {@code @EnableOAuth2Sso} but not on a
- * WebSecurityConfigurerAdapter then one is added with all paths secured and with an order
- * that puts it ahead of the default HTTP Basic security chain in Spring Boot.
+ * WebSecurityConfigurerAdapter then one is added with all paths secured.
  *
  * @author Dave Syer
  * @since 1.3.0
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoDefaultConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoDefaultConfiguration.java
index 6943d70f71c..194bbc00a96 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoDefaultConfiguration.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoDefaultConfiguration.java
@@ -17,31 +17,26 @@
 package org.springframework.boot.autoconfigure.security.oauth2.client;
 
 import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
-import org.springframework.boot.autoconfigure.security.SecurityProperties;
 import org.springframework.boot.autoconfigure.security.oauth2.client.OAuth2SsoDefaultConfiguration.NeedsWebSecurityCondition;
 import org.springframework.context.ApplicationContext;
 import org.springframework.context.annotation.ConditionContext;
 import org.springframework.context.annotation.Conditional;
 import org.springframework.context.annotation.Configuration;
-import org.springframework.core.Ordered;
 import org.springframework.core.type.AnnotatedTypeMetadata;
 import org.springframework.security.config.annotation.web.builders.HttpSecurity;
 import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
-import org.springframework.util.ClassUtils;
 
 /**
  * Configuration for OAuth2 Single Sign On (SSO). If the user only has
  * {@code @EnableOAuth2Sso} but not on a {@code WebSecurityConfigurerAdapter} then one is
- * added with all paths secured and with an order that puts it ahead of the default HTTP
- * Basic security chain in Spring Boot.
+ * added with all paths secured.
  *
  * @author Dave Syer
  * @since 1.3.0
  */
 @Configuration
 @Conditional(NeedsWebSecurityCondition.class)
-public class OAuth2SsoDefaultConfiguration extends WebSecurityConfigurerAdapter
-		implements Ordered {
+public class OAuth2SsoDefaultConfiguration extends WebSecurityConfigurerAdapter {
 
 	private final ApplicationContext applicationContext;
 
@@ -59,21 +54,6 @@ protected void configure(HttpSecurity http) throws Exception {
 		new SsoSecurityConfigurer(this.applicationContext).configure(http);
 	}
 
-	@Override
-	public int getOrder() {
-		if (this.sso.getFilterOrder() != null) {
-			return this.sso.getFilterOrder();
-		}
-		if (ClassUtils.isPresent(
-				"org.springframework.boot.actuate.autoconfigure.ManagementServerProperties",
-				null)) {
-			// If > BASIC_AUTH_ORDER then the existing rules for the actuator
-			// endpoints will take precedence. This value is < BASIC_AUTH_ORDER.
-			return SecurityProperties.ACCESS_OVERRIDE_ORDER - 5;
-		}
-		return SecurityProperties.ACCESS_OVERRIDE_ORDER;
-	}
-
 	protected static class NeedsWebSecurityCondition extends EnableOAuth2SsoCondition {
 
 		@Override
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoProperties.java
index 528115e6871..37a290dddec 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/client/OAuth2SsoProperties.java
@@ -35,12 +35,6 @@
 	 */
 	private String loginPath = DEFAULT_LOGIN_PATH;
 
-	/**
-	 * Filter order to apply if not providing an explicit WebSecurityConfigurerAdapter (in
-	 * which case the order can be provided there instead).
-	 */
-	private Integer filterOrder;
-
 	public String getLoginPath() {
 		return this.loginPath;
 	}
@@ -49,12 +43,4 @@ public void setLoginPath(String loginPath) {
 		this.loginPath = loginPath;
 	}
 
-	public Integer getFilterOrder() {
-		return this.filterOrder;
-	}
-
-	public void setFilterOrder(Integer filterOrder) {
-		this.filterOrder = filterOrder;
-	}
-
 }
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/OAuth2ResourceServerConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/OAuth2ResourceServerConfiguration.java
index 58c68874df1..092bf46f0d6 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/OAuth2ResourceServerConfiguration.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/OAuth2ResourceServerConfiguration.java
@@ -19,8 +19,6 @@
 import java.util.Map;
 
 import org.springframework.beans.BeanUtils;
-import org.springframework.beans.BeansException;
-import org.springframework.beans.factory.config.BeanPostProcessor;
 import org.springframework.boot.autoconfigure.condition.ConditionMessage;
 import org.springframework.boot.autoconfigure.condition.ConditionOutcome;
 import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
@@ -32,8 +30,6 @@
 import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerConfiguration.ResourceServerCondition;
 import org.springframework.boot.context.properties.bind.Bindable;
 import org.springframework.boot.context.properties.bind.Binder;
-import org.springframework.context.ApplicationContext;
-import org.springframework.context.ApplicationContextAware;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Condition;
 import org.springframework.context.annotation.ConditionContext;
@@ -85,12 +81,6 @@ public ResourceServerConfigurer resourceServer() {
 		return new ResourceSecurityConfigurer(this.resource);
 	}
 
-	@Bean
-	public static ResourceServerFilterChainOrderProcessor resourceServerFilterChainOrderProcessor(
-			ResourceServerProperties properties) {
-		return new ResourceServerFilterChainOrderProcessor(properties);
-	}
-
 	protected static class ResourceSecurityConfigurer
 			extends ResourceServerConfigurerAdapter {
 
@@ -113,45 +103,6 @@ public void configure(HttpSecurity http) throws Exception {
 
 	}
 
-	private static final class ResourceServerFilterChainOrderProcessor
-			implements BeanPostProcessor, ApplicationContextAware {
-
-		private final ResourceServerProperties properties;
-
-		private ApplicationContext context;
-
-		private ResourceServerFilterChainOrderProcessor(
-				ResourceServerProperties properties) {
-			this.properties = properties;
-		}
-
-		@Override
-		public void setApplicationContext(ApplicationContext context)
-				throws BeansException {
-			this.context = context;
-		}
-
-		@Override
-		public Object postProcessBeforeInitialization(Object bean, String beanName)
-				throws BeansException {
-			return bean;
-		}
-
-		@Override
-		public Object postProcessAfterInitialization(Object bean, String beanName)
-				throws BeansException {
-			if (bean instanceof ResourceServerConfiguration) {
-				if (this.context.getBeanNamesForType(ResourceServerConfiguration.class,
-						false, false).length == 1) {
-					ResourceServerConfiguration config = (ResourceServerConfiguration) bean;
-					config.setOrder(this.properties.getFilterOrder());
-				}
-			}
-			return bean;
-		}
-
-	}
-
 	protected static class ResourceServerCondition extends SpringBootCondition
 			implements ConfigurationCondition {
 
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/ResourceServerProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/ResourceServerProperties.java
index 081bc74791c..e589168f9fe 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/ResourceServerProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/security/oauth2/resource/ResourceServerProperties.java
@@ -25,7 +25,6 @@
 import org.springframework.beans.factory.BeanFactoryAware;
 import org.springframework.beans.factory.BeanFactoryUtils;
 import org.springframework.beans.factory.ListableBeanFactory;
-import org.springframework.boot.autoconfigure.security.SecurityProperties;
 import org.springframework.boot.context.properties.ConfigurationProperties;
 import org.springframework.security.oauth2.common.DefaultOAuth2AccessToken;
 import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerEndpointsConfiguration;
@@ -84,12 +83,6 @@
 
 	private Jwk jwk = new Jwk();
 
-	/**
-	 * The order of the filter chain used to authenticate tokens. Default puts it after
-	 * the actuator endpoints and before the default HTTP basic filter chain (catchall).
-	 */
-	private int filterOrder = SecurityProperties.ACCESS_OVERRIDE_ORDER - 1;
-
 	public ResourceServerProperties() {
 		this(null, null);
 	}
@@ -180,14 +173,6 @@ public String getClientSecret() {
 		return this.clientSecret;
 	}
 
-	public int getFilterOrder() {
-		return this.filterOrder;
-	}
-
-	public void setFilterOrder(int filterOrder) {
-		this.filterOrder = filterOrder;
-	}
-
 	@PostConstruct
 	public void validate() {
 		if (countBeans(AuthorizationServerEndpointsConfiguration.class) > 0) {
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/security/oauth2/OAuth2AutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/security/oauth2/OAuth2AutoConfigurationTests.java
index 723b8a86f4a..99cb5ace2c4 100644
--- a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/security/oauth2/OAuth2AutoConfigurationTests.java
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/security/oauth2/OAuth2AutoConfigurationTests.java
@@ -28,7 +28,6 @@
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.boot.autoconfigure.http.HttpMessageConvertersAutoConfiguration;
 import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
-import org.springframework.boot.autoconfigure.security.SecurityProperties;
 import org.springframework.boot.autoconfigure.security.oauth2.authserver.OAuth2AuthorizationServerConfiguration;
 import org.springframework.boot.autoconfigure.security.oauth2.method.OAuth2MethodSecurityConfiguration;
 import org.springframework.boot.autoconfigure.security.oauth2.resource.OAuth2ResourceServerConfiguration;
@@ -45,7 +44,6 @@
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.context.annotation.Import;
-import org.springframework.core.annotation.Order;
 import org.springframework.http.HttpEntity;
 import org.springframework.http.HttpHeaders;
 import org.springframework.http.HttpMethod;
@@ -492,7 +490,6 @@ private int countBeans(ApplicationContext context, Class<?> type) {
 	}
 
 	@Configuration
-	@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
 	protected static class TestSecurityConfiguration
 			extends WebSecurityConfigurerAdapter {
 
diff --git a/spring-boot-samples/spring-boot-sample-secure-oauth2-actuator/src/main/java/sample/secure/oauth2/actuator/ActuatorSecurityConfiguration.java b/spring-boot-samples/spring-boot-sample-secure-oauth2-actuator/src/main/java/sample/secure/oauth2/actuator/ActuatorSecurityConfiguration.java
index 3cda2c153cc..5405df6556d 100644
--- a/spring-boot-samples/spring-boot-sample-secure-oauth2-actuator/src/main/java/sample/secure/oauth2/actuator/ActuatorSecurityConfiguration.java
+++ b/spring-boot-samples/spring-boot-sample-secure-oauth2-actuator/src/main/java/sample/secure/oauth2/actuator/ActuatorSecurityConfiguration.java
@@ -2,6 +2,7 @@
 
 import org.springframework.boot.autoconfigure.security.SpringBootSecurity;
 import org.springframework.context.annotation.Configuration;
+import org.springframework.core.annotation.Order;
 import org.springframework.security.config.annotation.web.builders.HttpSecurity;
 import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
 
@@ -11,6 +12,7 @@
  * @author Madhura Bhave
  */
 @Configuration
+@Order(2) //before the resource server configuration
 public class ActuatorSecurityConfiguration extends WebSecurityConfigurerAdapter {
 
 	private final SpringBootSecurity springBootSecurity;
