diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraDataAutoConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraDataAutoConfiguration.java
new file mode 100644
index 00000000000..f84c754418d
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraDataAutoConfiguration.java
@@ -0,0 +1,71 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.cassandra;
+
+import com.datastax.driver.core.Cluster;
+import com.datastax.driver.core.Session;
+import reactor.core.scheduler.Schedulers;
+
+import org.springframework.boot.autoconfigure.AutoConfigureAfter;
+import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.cassandra.core.session.DefaultBridgedReactiveSession;
+import org.springframework.cassandra.core.session.DefaultReactiveSessionFactory;
+import org.springframework.cassandra.core.session.ReactiveSession;
+import org.springframework.cassandra.core.session.ReactiveSessionFactory;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.data.cassandra.convert.CassandraConverter;
+import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
+
+/**
+ * {@link EnableAutoConfiguration Auto-configuration} for Spring Data's reactive Cassandra
+ * support.
+ *
+ * @author Eddú Meléndez
+ * @since 2.0.0
+ */
+@Configuration
+@ConditionalOnClass({ Cluster.class, ReactiveCassandraTemplate.class })
+@ConditionalOnBean(Session.class)
+@AutoConfigureAfter(CassandraAutoConfiguration.class)
+public class ReactiveCassandraDataAutoConfiguration {
+
+	@Bean
+	@ConditionalOnMissingBean(ReactiveSession.class)
+	public ReactiveSession rectiveSession(Session session)
+			throws Exception {
+		return new DefaultBridgedReactiveSession(session, Schedulers.elastic());
+	}
+
+	@Bean
+	public ReactiveSessionFactory reactiveSessionFactory(ReactiveSession reactiveSession)
+			throws Exception {
+		return new DefaultReactiveSessionFactory(reactiveSession);
+	}
+
+	@Bean
+	@ConditionalOnMissingBean
+	public ReactiveCassandraTemplate reactiveCassandraTemplate(ReactiveSession session,
+	                                           CassandraConverter converter) throws Exception {
+		return new ReactiveCassandraTemplate(session, converter);
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfiguration.java
new file mode 100644
index 00000000000..d879275da45
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfiguration.java
@@ -0,0 +1,45 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.cassandra;
+
+import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
+import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
+import org.springframework.cassandra.core.session.ReactiveSession;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.context.annotation.Import;
+import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
+import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories;
+import org.springframework.data.cassandra.repository.support.ReactiveCassandraRepositoryFactoryBean;
+
+/**
+ * {@link EnableAutoConfiguration Auto-configuration} for Spring Data's Cassandra
+ * Reactive Repositories.
+ *
+ * @author Eddú Meléndez
+ * @see EnableReactiveCassandraRepositories
+ * @since 2.0.0
+ */
+@Configuration
+@ConditionalOnClass({ ReactiveSession.class, ReactiveCassandraRepository.class })
+@ConditionalOnProperty(prefix = "spring.data.cassandra.reactive-repositories", name = "enabled", havingValue = "true", matchIfMissing = true)
+@ConditionalOnMissingBean(ReactiveCassandraRepositoryFactoryBean.class)
+@Import(ReactiveCassandraRepositoriesAutoConfigureRegistrar.class)
+public class ReactiveCassandraRepositoriesAutoConfiguration {
+
+}
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfigureRegistrar.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfigureRegistrar.java
new file mode 100644
index 00000000000..9924b80c043
--- /dev/null
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfigureRegistrar.java
@@ -0,0 +1,57 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.cassandra;
+
+import java.lang.annotation.Annotation;
+
+import org.springframework.boot.autoconfigure.data.AbstractRepositoryConfigurationSourceSupport;
+import org.springframework.context.annotation.ImportBeanDefinitionRegistrar;
+import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories;
+import org.springframework.data.cassandra.repository.config.ReactiveCassandraRepositoryConfigurationExtension;
+import org.springframework.data.repository.config.RepositoryConfigurationExtension;
+
+/**
+ * {@link ImportBeanDefinitionRegistrar} used to auto-configure Spring Data Cassandra
+ * Reactive Repositories.
+ *
+ * @author Eddú Meléndez
+ * @since 2.0.0
+ */
+public class ReactiveCassandraRepositoriesAutoConfigureRegistrar
+		extends AbstractRepositoryConfigurationSourceSupport {
+
+	@Override
+	protected Class<? extends Annotation> getAnnotation() {
+		return EnableReactiveCassandraRepositories.class;
+	}
+
+	@Override
+	protected Class<?> getConfiguration() {
+		return EnableReactiveCassandraRepositoriesConfiguration.class;
+	}
+
+	@Override
+	protected RepositoryConfigurationExtension getRepositoryConfigurationExtension() {
+		return new ReactiveCassandraRepositoryConfigurationExtension();
+	}
+
+	@EnableReactiveCassandraRepositories
+	private static class EnableReactiveCassandraRepositoriesConfiguration {
+
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/main/resources/META-INF/additional-spring-configuration-metadata.json b/spring-boot-autoconfigure/src/main/resources/META-INF/additional-spring-configuration-metadata.json
index 20dd0ed54ff..6fa91294de3 100644
--- a/spring-boot-autoconfigure/src/main/resources/META-INF/additional-spring-configuration-metadata.json
+++ b/spring-boot-autoconfigure/src/main/resources/META-INF/additional-spring-configuration-metadata.json
@@ -84,6 +84,12 @@
     "name": "spring.data.cassandra.compression",
     "defaultValue": "none"
   },
+  {
+    "name": "spring.data.cassandra.reactive-repositories.enabled",
+    "type": "java.lang.Boolean",
+    "description": "Enable Cassandra reactive repositories.",
+    "defaultValue": true
+  },
   {
     "name": "spring.data.couchbase.consistency",
     "defaultValue": "read-your-own-writes"
diff --git a/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories b/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
index 0a3781fe3d2..099ca3d6f7c 100644
--- a/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
+++ b/spring-boot-autoconfigure/src/main/resources/META-INF/spring.factories
@@ -31,6 +31,8 @@ org.springframework.boot.autoconfigure.couchbase.CouchbaseAutoConfiguration,\
 org.springframework.boot.autoconfigure.dao.PersistenceExceptionTranslationAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.cassandra.CassandraDataAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.cassandra.CassandraRepositoriesAutoConfiguration,\
+org.springframework.boot.autoconfigure.data.cassandra.ReactiveCassandraDataAutoConfiguration,\
+org.springframework.boot.autoconfigure.data.cassandra.ReactiveCassandraRepositoriesAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.couchbase.CouchbaseDataAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.couchbase.CouchbaseRepositoriesAutoConfiguration,\
 org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration,\
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/alt/cassandra/ReactiveCityCassandraRepository.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/alt/cassandra/ReactiveCityCassandraRepository.java
new file mode 100644
index 00000000000..62f9b797019
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/alt/cassandra/ReactiveCityCassandraRepository.java
@@ -0,0 +1,24 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.alt.cassandra;
+
+import org.springframework.boot.autoconfigure.data.cassandra.city.City;
+import org.springframework.data.repository.reactive.ReactiveCrudRepository;
+
+public interface ReactiveCityCassandraRepository extends ReactiveCrudRepository<City, Long> {
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraDataAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraDataAutoConfigurationTests.java
new file mode 100644
index 00000000000..cd642a389ee
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraDataAutoConfigurationTests.java
@@ -0,0 +1,135 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.cassandra;
+
+import java.util.Set;
+
+import com.datastax.driver.core.Session;
+import org.junit.After;
+import org.junit.Before;
+import org.junit.Test;
+
+import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration;
+import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
+import org.springframework.boot.autoconfigure.data.cassandra.city.City;
+import org.springframework.boot.autoconfigure.domain.EntityScan;
+import org.springframework.boot.test.util.EnvironmentTestUtils;
+import org.springframework.cassandra.core.session.ReactiveSession;
+import org.springframework.context.annotation.AnnotationConfigApplicationContext;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.ComponentScan;
+import org.springframework.context.annotation.ComponentScan.Filter;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.context.annotation.FilterType;
+import org.springframework.data.cassandra.core.ReactiveCassandraTemplate;
+import org.springframework.data.cassandra.mapping.CassandraMappingContext;
+import org.springframework.data.cassandra.mapping.SimpleUserTypeResolver;
+import org.springframework.test.util.ReflectionTestUtils;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.mockito.Mockito.mock;
+
+/**
+ * Tests for {@link ReactiveCassandraDataAutoConfiguration} that require a Cassandra instance.
+ *
+ * @author Eddú Meléndez
+ */
+public class ReactiveCassandraDataAutoConfigurationTests {
+
+	private AnnotationConfigApplicationContext context;
+
+	@Before
+	public void setup() {
+		this.context = new AnnotationConfigApplicationContext();
+	}
+
+	@After
+	public void close() {
+		if (this.context != null) {
+			this.context.close();
+		}
+	}
+
+	@Test
+	public void templateExists() {
+		EnvironmentTestUtils.addEnvironment(this.context,
+				"spring.data.cassandra.keyspaceName:boot_test");
+		this.context.register(TestExcludeConfiguration.class, TestConfiguration.class,
+				PropertyPlaceholderAutoConfiguration.class,
+				CassandraAutoConfiguration.class, CassandraDataAutoConfiguration.class,
+				ReactiveCassandraDataAutoConfiguration.class);
+		this.context.refresh();
+		assertThat(this.context.getBeanNamesForType(ReactiveCassandraTemplate.class).length)
+				.isEqualTo(1);
+	}
+
+	@Test
+	@SuppressWarnings("unchecked")
+	public void entityScanShouldSetInitialEntitySet() throws Exception {
+		EnvironmentTestUtils.addEnvironment(this.context,
+				"spring.data.cassandra.keyspaceName:boot_test");
+		this.context.register(TestConfiguration.class, EntityScanConfig.class,
+				PropertyPlaceholderAutoConfiguration.class,
+				CassandraAutoConfiguration.class, CassandraDataAutoConfiguration.class,
+				ReactiveCassandraDataAutoConfiguration.class);
+		this.context.refresh();
+		CassandraMappingContext mappingContext = this.context
+				.getBean(CassandraMappingContext.class);
+		Set<Class<?>> initialEntitySet = (Set<Class<?>>) ReflectionTestUtils
+				.getField(mappingContext, "initialEntitySet");
+		assertThat(initialEntitySet).containsOnly(City.class);
+	}
+
+	@Test
+	public void userTypeResolverShouldBeSet() throws Exception {
+		EnvironmentTestUtils.addEnvironment(this.context,
+				"spring.data.cassandra.keyspaceName:boot_test");
+		this.context.register(TestConfiguration.class,
+				PropertyPlaceholderAutoConfiguration.class,
+				CassandraAutoConfiguration.class, CassandraDataAutoConfiguration.class,
+				ReactiveCassandraDataAutoConfiguration.class);
+		this.context.refresh();
+		CassandraMappingContext mappingContext = this.context
+				.getBean(CassandraMappingContext.class);
+		assertThat(ReflectionTestUtils.getField(mappingContext, "userTypeResolver"))
+				.isInstanceOf(SimpleUserTypeResolver.class);
+	}
+
+	@Configuration
+	@ComponentScan(excludeFilters = @Filter(classes = {
+			ReactiveSession.class }, type = FilterType.ASSIGNABLE_TYPE))
+	static class TestExcludeConfiguration {
+
+	}
+
+	@Configuration
+	static class TestConfiguration {
+
+		@Bean
+		public Session session() {
+			return mock(Session.class);
+		}
+
+	}
+
+	@Configuration
+	@EntityScan("org.springframework.boot.autoconfigure.data.cassandra.city")
+	static class EntityScanConfig {
+
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfigurationTests.java
new file mode 100644
index 00000000000..6074395fb41
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/ReactiveCassandraRepositoriesAutoConfigurationTests.java
@@ -0,0 +1,141 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.cassandra;
+
+import java.util.Set;
+
+import com.datastax.driver.core.Cluster;
+import com.datastax.driver.core.Session;
+import org.junit.After;
+import org.junit.Before;
+import org.junit.Test;
+
+import org.springframework.boot.autoconfigure.TestAutoConfigurationPackage;
+import org.springframework.boot.autoconfigure.cassandra.CassandraAutoConfiguration;
+import org.springframework.boot.autoconfigure.context.PropertyPlaceholderAutoConfiguration;
+import org.springframework.boot.autoconfigure.data.alt.cassandra.ReactiveCityCassandraRepository;
+import org.springframework.boot.autoconfigure.data.cassandra.city.City;
+import org.springframework.boot.autoconfigure.data.cassandra.city.ReactiveCityRepository;
+import org.springframework.boot.autoconfigure.data.empty.EmptyDataPackage;
+import org.springframework.cassandra.core.session.ReactiveSession;
+import org.springframework.context.annotation.AnnotationConfigApplicationContext;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.ComponentScan;
+import org.springframework.context.annotation.ComponentScan.Filter;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.context.annotation.FilterType;
+import org.springframework.data.cassandra.mapping.BasicCassandraMappingContext;
+import org.springframework.data.cassandra.repository.config.EnableReactiveCassandraRepositories;
+import org.springframework.test.util.ReflectionTestUtils;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.mockito.Mockito.mock;
+
+/**
+ * Tests for {@link ReactiveCassandraRepositoriesAutoConfiguration}.
+ *
+ * @author Eddú Meléndez
+ */
+public class ReactiveCassandraRepositoriesAutoConfigurationTests {
+
+	private AnnotationConfigApplicationContext context;
+
+	@Before
+	public void setUp() {
+		this.context = new AnnotationConfigApplicationContext();
+	}
+
+	@After
+	public void close() {
+		if (this.context != null) {
+			this.context.close();
+		}
+	}
+
+	@Test
+	public void testDefaultRepositoryConfiguration() {
+		addConfigurations(TestConfiguration.class);
+		assertThat(this.context.getBean(ReactiveCityRepository.class)).isNotNull();
+		assertThat(this.context.getBean(Cluster.class)).isNotNull();
+		assertThat(getInitialEntitySet()).hasSize(1);
+	}
+
+	@Test
+	public void testNoRepositoryConfiguration() {
+		addConfigurations(TestExcludeConfiguration.class, EmptyConfiguration.class);
+		assertThat(this.context.getBean(Cluster.class)).isNotNull();
+		assertThat(getInitialEntitySet()).hasSize(1).containsOnly(City.class);
+	}
+
+	@Test
+	public void doesNotTriggerDefaultRepositoryDetectionIfCustomized() {
+		addConfigurations(TestExcludeConfiguration.class, CustomizedConfiguration.class);
+		assertThat(this.context.getBean(ReactiveCityCassandraRepository.class)).isNotNull();
+		assertThat(getInitialEntitySet()).hasSize(1).containsOnly(City.class);
+	}
+
+	@SuppressWarnings("unchecked")
+	private Set<Class<?>> getInitialEntitySet() {
+		BasicCassandraMappingContext mappingContext = this.context
+				.getBean(BasicCassandraMappingContext.class);
+		return (Set<Class<?>>) ReflectionTestUtils.getField(mappingContext,
+				"initialEntitySet");
+	}
+
+	private void addConfigurations(Class<?>... configurations) {
+		this.context.register(configurations);
+		this.context.register(CassandraAutoConfiguration.class,
+				CassandraRepositoriesAutoConfiguration.class,
+				CassandraDataAutoConfiguration.class,
+				ReactiveCassandraDataAutoConfiguration.class,
+				ReactiveCassandraRepositoriesAutoConfiguration.class,
+				PropertyPlaceholderAutoConfiguration.class);
+		this.context.refresh();
+	}
+
+	@Configuration
+	@TestAutoConfigurationPackage(City.class)
+	static class TestConfiguration {
+
+		@Bean
+		public Session Session() {
+			return mock(Session.class);
+		}
+
+	}
+
+	@Configuration
+	@TestAutoConfigurationPackage(EmptyDataPackage.class)
+	static class EmptyConfiguration {
+
+	}
+
+	@Configuration
+	@TestAutoConfigurationPackage(ReactiveCassandraRepositoriesAutoConfigurationTests.class)
+	@EnableReactiveCassandraRepositories(basePackageClasses = ReactiveCityCassandraRepository.class)
+	static class CustomizedConfiguration {
+
+	}
+
+	@Configuration
+	@ComponentScan(excludeFilters = @Filter(classes = {
+			ReactiveSession.class }, type = FilterType.ASSIGNABLE_TYPE))
+	static class TestExcludeConfiguration {
+
+	}
+
+}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/city/ReactiveCityRepository.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/city/ReactiveCityRepository.java
new file mode 100644
index 00000000000..9f0794c4dbc
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/data/cassandra/city/ReactiveCityRepository.java
@@ -0,0 +1,23 @@
+/*
+ * Copyright 2012-2017 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.autoconfigure.data.cassandra.city;
+
+import org.springframework.data.repository.Repository;
+
+public interface ReactiveCityRepository extends Repository<City, Long> {
+
+}
diff --git a/spring-boot-dependencies/pom.xml b/spring-boot-dependencies/pom.xml
index 51bacee08d5..af0c68d7243 100644
--- a/spring-boot-dependencies/pom.xml
+++ b/spring-boot-dependencies/pom.xml
@@ -353,6 +353,11 @@
 				<artifactId>spring-boot-starter-data-cassandra</artifactId>
 				<version>2.0.0.BUILD-SNAPSHOT</version>
 			</dependency>
+			<dependency>
+				<groupId>org.springframework.boot</groupId>
+				<artifactId>spring-boot-starter-data-cassandra-reactive</artifactId>
+				<version>2.0.0.BUILD-SNAPSHOT</version>
+			</dependency>
 			<dependency>
 				<groupId>org.springframework.boot</groupId>
 				<artifactId>spring-boot-starter-data-couchbase</artifactId>
diff --git a/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc b/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
index cf40ee6efd0..2e5fd02e1b9 100644
--- a/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
+++ b/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
@@ -558,6 +558,7 @@ content into your application; rather pick only the properties that you need.
 	spring.data.cassandra.load-balancing-policy= # Class name of the load balancing policy.
 	spring.data.cassandra.port= # Port of the Cassandra server.
 	spring.data.cassandra.password= # Login password of the server.
+	spring.data.cassandra.reactive-repositories.enabled= # Enable Cassandra reactive repositories.
 	spring.data.cassandra.read-timeout-millis= # Socket option: read time out.
 	spring.data.cassandra.reconnection-policy= # Reconnection policy class.
 	spring.data.cassandra.repositories.enabled= # Enable Cassandra repositories.
diff --git a/spring-boot-starters/pom.xml b/spring-boot-starters/pom.xml
index b49990d3b46..21aa81a558d 100644
--- a/spring-boot-starters/pom.xml
+++ b/spring-boot-starters/pom.xml
@@ -29,6 +29,7 @@
 		<module>spring-boot-starter-cache</module>
 		<module>spring-boot-starter-cloud-connectors</module>
 		<module>spring-boot-starter-data-cassandra</module>
+		<module>spring-boot-starter-data-cassandra-reactive</module>
 		<module>spring-boot-starter-data-couchbase</module>
 		<module>spring-boot-starter-data-elasticsearch</module>
 		<module>spring-boot-starter-data-jpa</module>
diff --git a/spring-boot-starters/spring-boot-starter-data-cassandra-reactive/pom.xml b/spring-boot-starters/spring-boot-starter-data-cassandra-reactive/pom.xml
new file mode 100644
index 00000000000..e49c4ff4d78
--- /dev/null
+++ b/spring-boot-starters/spring-boot-starter-data-cassandra-reactive/pom.xml
@@ -0,0 +1,59 @@
+<?xml version="1.0" encoding="UTF-8"?>
+<!--
+  ~ Copyright 2012-2017 the original author or authors.
+  ~
+  ~ Licensed under the Apache License, Version 2.0 (the "License");
+  ~ you may not use this file except in compliance with the License.
+  ~ You may obtain a copy of the License at
+  ~
+  ~      http://www.apache.org/licenses/LICENSE-2.0
+  ~
+  ~ Unless required by applicable law or agreed to in writing, software
+  ~ distributed under the License is distributed on an "AS IS" BASIS,
+  ~ WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+  ~ See the License for the specific language governing permissions and
+  ~ limitations under the License.
+  -->
+
+<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
+	<modelVersion>4.0.0</modelVersion>
+	<parent>
+		<groupId>org.springframework.boot</groupId>
+		<artifactId>spring-boot-starters</artifactId>
+		<version>2.0.0.BUILD-SNAPSHOT</version>
+	</parent>
+	<artifactId>spring-boot-starter-data-cassandra-reactive</artifactId>
+	<name>Spring Boot Data Cassandra Reactive Starter</name>
+	<description>Starter for using Cassandra distributed database and Spring Data
+		Cassandra Reactive</description>
+	<url>http://projects.spring.io/spring-boot/</url>
+	<organization>
+		<name>Pivotal Software, Inc.</name>
+		<url>http://www.spring.io</url>
+	</organization>
+	<properties>
+		<main.basedir>${basedir}/../..</main.basedir>
+	</properties>
+	<dependencies>
+		<dependency>
+			<groupId>org.springframework.boot</groupId>
+			<artifactId>spring-boot-starter</artifactId>
+		</dependency>
+		<dependency>
+			<groupId>org.springframework</groupId>
+			<artifactId>spring-tx</artifactId>
+		</dependency>
+		<dependency>
+			<groupId>org.springframework.data</groupId>
+			<artifactId>spring-data-cassandra</artifactId>
+		</dependency>
+		<dependency>
+			<groupId>org.springframework.data</groupId>
+			<artifactId>spring-cql</artifactId>
+		</dependency>
+		<dependency>
+			<groupId>io.projectreactor</groupId>
+			<artifactId>reactor-core</artifactId>
+		</dependency>
+	</dependencies>
+</project>
diff --git a/spring-boot-starters/spring-boot-starter-data-cassandra-reactive/src/main/resources/META-INF/spring.provides b/spring-boot-starters/spring-boot-starter-data-cassandra-reactive/src/main/resources/META-INF/spring.provides
new file mode 100644
index 00000000000..e914aa78940
--- /dev/null
+++ b/spring-boot-starters/spring-boot-starter-data-cassandra-reactive/src/main/resources/META-INF/spring.provides
@@ -0,0 +1 @@
+provides: spring-data-cassandra, spring-cql
\ No newline at end of file
