diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfiguration.java
index 6cac5a2225e..f8e252a11bf 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfiguration.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfiguration.java
@@ -17,7 +17,6 @@
 package org.springframework.boot.autoconfigure.thymeleaf;
 
 import java.util.Collection;
-import java.util.Collections;
 import java.util.LinkedHashMap;
 
 import javax.annotation.PostConstruct;
@@ -55,6 +54,7 @@
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
 import org.springframework.core.Ordered;
+import org.springframework.util.CollectionUtils;
 import org.springframework.util.MimeType;
 import org.springframework.web.servlet.resource.ResourceUrlEncodingFilter;
 
@@ -71,7 +71,7 @@
 @Configuration
 @EnableConfigurationProperties(ThymeleafProperties.class)
 @ConditionalOnClass(TemplateMode.class)
-@AutoConfigureAfter({ WebMvcAutoConfiguration.class, WebFluxAutoConfiguration.class })
+@AutoConfigureAfter({WebMvcAutoConfiguration.class, WebFluxAutoConfiguration.class})
 public class ThymeleafAutoConfiguration {
 
 	@Configuration
@@ -137,16 +137,21 @@ public ThymeleafDefaultConfiguration(
 				Collection<ITemplateResolver> templateResolvers,
 				ObjectProvider<Collection<IDialect>> dialectsProvider) {
 			this.templateResolvers = templateResolvers;
-			this.dialects = dialectsProvider
-					.getIfAvailable(() -> Collections.emptyList());
+			this.dialects = dialectsProvider.getIfAvailable();
 		}
 
 		@Bean
 		@ConditionalOnMissingBean(SpringTemplateEngine.class)
 		public SpringTemplateEngine templateEngine() {
 			SpringTemplateEngine engine = new SpringTemplateEngine();
-			this.templateResolvers.forEach(engine::addTemplateResolver);
-			this.dialects.forEach(engine::addDialect);
+			for (ITemplateResolver templateResolver : this.templateResolvers) {
+				engine.addTemplateResolver(templateResolver);
+			}
+			if (!CollectionUtils.isEmpty(this.dialects)) {
+				for (IDialect dialect : this.dialects) {
+					engine.addDialect(dialect);
+				}
+			}
 			return engine;
 		}
 
@@ -183,9 +188,8 @@ public ThymeleafViewResolver thymeleafViewResolver() {
 				ThymeleafViewResolver resolver = new ThymeleafViewResolver();
 				resolver.setTemplateEngine(this.templateEngine);
 				resolver.setCharacterEncoding(this.properties.getEncoding().name());
-				resolver.setContentType(
-						appendCharset(this.properties.getServlet().getContentType(),
-								resolver.getCharacterEncoding()));
+				resolver.setContentType(appendCharset(this.properties.getServlet().getContentType(),
+						resolver.getCharacterEncoding()));
 				resolver.setExcludedViewNames(this.properties.getExcludedViewNames());
 				resolver.setViewNames(this.properties.getViewNames());
 				// This resolver acts as a fallback resolver (e.g. like a
@@ -218,22 +222,27 @@ private String appendCharset(MimeType type, String charset) {
 
 		private final Collection<IDialect> dialects;
 
+
 		ThymeleafReactiveConfiguration(Collection<ITemplateResolver> templateResolvers,
 				ObjectProvider<Collection<IDialect>> dialectsProvider) {
 			this.templateResolvers = templateResolvers;
-			this.dialects = dialectsProvider
-					.getIfAvailable(() -> Collections.emptyList());
+			this.dialects = dialectsProvider.getIfAvailable();
 		}
 
 		@Bean
 		@ConditionalOnMissingBean(ISpringWebFluxTemplateEngine.class)
 		public SpringWebFluxTemplateEngine templateEngine() {
 			SpringWebFluxTemplateEngine engine = new SpringWebFluxTemplateEngine();
-			this.templateResolvers.forEach(engine::addTemplateResolver);
-			this.dialects.forEach(engine::addDialect);
+			for (ITemplateResolver templateResolver : this.templateResolvers) {
+				engine.addTemplateResolver(templateResolver);
+			}
+			if (!CollectionUtils.isEmpty(this.dialects)) {
+				for (IDialect dialect : this.dialects) {
+					engine.addDialect(dialect);
+				}
+			}
 			return engine;
 		}
-
 	}
 
 	@Configuration
@@ -243,24 +252,23 @@ public SpringWebFluxTemplateEngine templateEngine() {
 
 		private final ThymeleafProperties properties;
 
+
 		ThymeleafWebFluxConfiguration(ThymeleafProperties properties) {
 			this.properties = properties;
 		}
 
 		@Bean
 		@ConditionalOnMissingBean(name = "thymeleafReactiveViewResolver")
-		public ThymeleafReactiveViewResolver thymeleafViewResolver(
-				ISpringWebFluxTemplateEngine templateEngine) {
+		public ThymeleafReactiveViewResolver thymeleafViewResolver(ISpringWebFluxTemplateEngine templateEngine) {
+
 			ThymeleafReactiveViewResolver resolver = new ThymeleafReactiveViewResolver();
 			resolver.setTemplateEngine(templateEngine);
 			resolver.setDefaultCharset(this.properties.getEncoding());
-			resolver.setSupportedMediaTypes(
-					this.properties.getReactive().getMediaTypes());
+			resolver.setSupportedMediaTypes(this.properties.getReactive().getMediaTypes());
 			resolver.setExcludedViewNames(this.properties.getExcludedViewNames());
 			resolver.setViewNames(this.properties.getViewNames());
 			if (this.properties.getReactive().getMaxChunkSize() > 0) {
-				resolver.setResponseMaxChunkSizeBytes(
-						this.properties.getReactive().getMaxChunkSize());
+				resolver.setResponseMaxChunkSizeBytes(this.properties.getReactive().getMaxChunkSize());
 			}
 			// This resolver acts as a fallback resolver (e.g. like a
 			// InternalResourceViewResolver) so it needs to have low precedence
@@ -295,7 +303,7 @@ public DataAttributeDialect dialect() {
 	}
 
 	@Configuration
-	@ConditionalOnClass({ SpringSecurityDialect.class })
+	@ConditionalOnClass({SpringSecurityDialect.class})
 	protected static class ThymeleafSecurityDialectConfiguration {
 
 		@Bean
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafProperties.java
index daa5386a724..0afe50609d0 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafProperties.java
@@ -227,8 +227,8 @@ public void setContentType(MimeType contentType) {
 		/**
 		 * Media types supported by the view technology.
 		 */
-		private List<MediaType> mediaTypes = new ArrayList<>(
-				Collections.singletonList(MediaType.TEXT_HTML));
+		private List<MediaType> mediaTypes =
+				new ArrayList(Collections.singletonList(MediaType.TEXT_HTML));
 
 		public List<MediaType> getMediaTypes() {
 			return this.mediaTypes;
@@ -245,7 +245,5 @@ public int getMaxChunkSize() {
 		public void setMaxChunkSize(int maxChunkSize) {
 			this.maxChunkSize = maxChunkSize;
 		}
-
 	}
-
 }
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafReactiveAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafReactiveAutoConfigurationTests.java
index b9fa1451ea7..aba1ef49f68 100644
--- a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafReactiveAutoConfigurationTests.java
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafReactiveAutoConfigurationTests.java
@@ -82,8 +82,7 @@ public void overrideCharacterEncoding() throws Exception {
 		assertThat(resolver instanceof SpringResourceTemplateResolver).isTrue();
 		assertThat(((SpringResourceTemplateResolver) resolver).getCharacterEncoding())
 				.isEqualTo("UTF-16");
-		ThymeleafReactiveViewResolver views = this.context
-				.getBean(ThymeleafReactiveViewResolver.class);
+		ThymeleafReactiveViewResolver views = this.context.getBean(ThymeleafReactiveViewResolver.class);
 		assertThat(views.getDefaultCharset().name()).isEqualTo("UTF-16");
 	}
 
@@ -91,10 +90,8 @@ public void overrideCharacterEncoding() throws Exception {
 	public void overrideMediaTypes() throws Exception {
 		load(BaseConfiguration.class,
 				"spring.thymeleaf.reactive.media-types:text/html,text/plain");
-		ThymeleafReactiveViewResolver views = this.context
-				.getBean(ThymeleafReactiveViewResolver.class);
-		assertThat(views.getSupportedMediaTypes()).contains(MediaType.TEXT_HTML,
-				MediaType.TEXT_PLAIN);
+		ThymeleafReactiveViewResolver views = this.context.getBean(ThymeleafReactiveViewResolver.class);
+		assertThat(views.getSupportedMediaTypes()).contains(MediaType.TEXT_HTML, MediaType.TEXT_PLAIN);
 	}
 
 	@Test
@@ -107,31 +104,27 @@ public void overrideTemplateResolverOrder() throws Exception {
 	@Test
 	public void overrideViewNames() throws Exception {
 		load(BaseConfiguration.class, "spring.thymeleaf.viewNames:foo,bar");
-		ThymeleafReactiveViewResolver views = this.context
-				.getBean(ThymeleafReactiveViewResolver.class);
-		assertThat(views.getViewNames()).isEqualTo(new String[] { "foo", "bar" });
+		ThymeleafReactiveViewResolver views = this.context.getBean(ThymeleafReactiveViewResolver.class);
+		assertThat(views.getViewNames()).isEqualTo(new String[] {"foo", "bar"});
 	}
 
 	@Test
 	public void templateLocationDoesNotExist() throws Exception {
-		load(BaseConfiguration.class,
-				"spring.thymeleaf.prefix:classpath:/no-such-directory/");
+		load(BaseConfiguration.class, "spring.thymeleaf.prefix:classpath:/no-such-directory/");
 		this.output.expect(containsString("Cannot find template location"));
 	}
 
 	@Test
 	public void templateLocationEmpty() throws Exception {
 		new File("target/test-classes/templates/empty-directory").mkdir();
-		load(BaseConfiguration.class,
-				"spring.thymeleaf.prefix:classpath:/templates/empty-directory/");
+		load(BaseConfiguration.class, "spring.thymeleaf.prefix:classpath:/templates/empty-directory/");
 		this.output.expect(not(containsString("Cannot find template location")));
 	}
 
 	@Test
 	public void useDataDialect() throws Exception {
 		load(BaseConfiguration.class);
-		ISpringWebFluxTemplateEngine engine = this.context
-				.getBean(ISpringWebFluxTemplateEngine.class);
+		ISpringWebFluxTemplateEngine engine = this.context.getBean(ISpringWebFluxTemplateEngine.class);
 		Context attrs = new Context(Locale.UK, Collections.singletonMap("foo", "bar"));
 		String result = engine.process("data-dialect", attrs);
 		assertThat(result).isEqualTo("<html><body data-foo=\"bar\"></body></html>");
@@ -140,8 +133,7 @@ public void useDataDialect() throws Exception {
 	@Test
 	public void useJava8TimeDialect() throws Exception {
 		load(BaseConfiguration.class);
-		ISpringWebFluxTemplateEngine engine = this.context
-				.getBean(ISpringWebFluxTemplateEngine.class);
+		ISpringWebFluxTemplateEngine engine = this.context.getBean(ISpringWebFluxTemplateEngine.class);
 		Context attrs = new Context(Locale.UK);
 		String result = engine.process("java8time-dialect", attrs);
 		assertThat(result).isEqualTo("<html><body>2015-11-24</body></html>");
@@ -150,8 +142,7 @@ public void useJava8TimeDialect() throws Exception {
 	@Test
 	public void renderTemplate() throws Exception {
 		load(BaseConfiguration.class);
-		ISpringWebFluxTemplateEngine engine = this.context
-				.getBean(ISpringWebFluxTemplateEngine.class);
+		ISpringWebFluxTemplateEngine engine = this.context.getBean(ISpringWebFluxTemplateEngine.class);
 		Context attrs = new Context(Locale.UK, Collections.singletonMap("foo", "bar"));
 		String result = engine.process("home", attrs);
 		assertThat(result).isEqualTo("<html><body>bar</body></html>");
@@ -176,8 +167,7 @@ private void load(Class<?> config, String... envVariables) {
 	}
 
 	@Configuration
-	@ImportAutoConfiguration({ ThymeleafAutoConfiguration.class,
-			PropertyPlaceholderAutoConfiguration.class })
+	@ImportAutoConfiguration({ThymeleafAutoConfiguration.class, PropertyPlaceholderAutoConfiguration.class})
 	protected static class BaseConfiguration {
 
 	}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafServletAutoConfigurationTests.java
similarity index 97%
rename from spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfigurationTests.java
rename to spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafServletAutoConfigurationTests.java
index 5710c93007d..8a0702817b1 100644
--- a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafAutoConfigurationTests.java
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/thymeleaf/ThymeleafServletAutoConfigurationTests.java
@@ -107,13 +107,12 @@ public void overrideTemplateResolverOrder() throws Exception {
 	public void overrideViewNames() throws Exception {
 		load(BaseConfiguration.class, "spring.thymeleaf.viewNames:foo,bar");
 		ThymeleafViewResolver views = this.context.getBean(ThymeleafViewResolver.class);
-		assertThat(views.getViewNames()).isEqualTo(new String[] { "foo", "bar" });
+		assertThat(views.getViewNames()).isEqualTo(new String[] {"foo", "bar"});
 	}
 
 	@Test
 	public void templateLocationDoesNotExist() throws Exception {
-		load(BaseConfiguration.class,
-				"spring.thymeleaf.prefix:classpath:/no-such-directory/");
+		load(BaseConfiguration.class, "spring.thymeleaf.prefix:classpath:/no-such-directory/");
 		this.output.expect(containsString("Cannot find template location"));
 	}
 
@@ -231,8 +230,8 @@ private void load(Class<?> config, String... envVariables) {
 	}
 
 	@Configuration
-	@ImportAutoConfiguration({ ThymeleafAutoConfiguration.class,
-			PropertyPlaceholderAutoConfiguration.class })
+	@ImportAutoConfiguration({ThymeleafAutoConfiguration.class,
+			PropertyPlaceholderAutoConfiguration.class})
 	static class BaseConfiguration {
 
 	}
diff --git a/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc b/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
index aaece8b7a75..658f787f655 100644
--- a/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
+++ b/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
@@ -437,12 +437,14 @@ content into your application; rather pick only the properties that you need.
 	spring.thymeleaf.cache=true # Enable template caching.
 	spring.thymeleaf.check-template=true # Check that the template exists before rendering it.
 	spring.thymeleaf.check-template-location=true # Check that the templates location exists.
-	spring.thymeleaf.content-type=text/html # Content-Type value.
-	spring.thymeleaf.enabled=true # Enable MVC Thymeleaf view resolution.
-	spring.thymeleaf.encoding=UTF-8 # Template encoding.
+	spring.thymeleaf.enabled=true # Enable Thymeleaf view resolution for Web frameworks.
+	spring.thymeleaf.encoding=UTF-8 # Template files encoding.
 	spring.thymeleaf.excluded-view-names= # Comma-separated list of view names that should be excluded from resolution.
 	spring.thymeleaf.mode=HTML5 # Template mode to be applied to templates. See also StandardTemplateModeHandlers.
 	spring.thymeleaf.prefix=classpath:/templates/ # Prefix that gets prepended to view names when building a URL.
+	spring.thymeleaf.reactive.max-chunk-size= # Maximum size of data buffers used for writing to the response, in bytes.
+	spring.thymeleaf.reactive.media-types=text/html # Media types supported by the view technology.
+	spring.thymeleaf.servlet.content-type=text/html # Content-Type value written to HTTP responses.
 	spring.thymeleaf.suffix=.html # Suffix that gets appended to view names when building a URL.
 	spring.thymeleaf.template-resolver-order= # Order of the template resolver in the chain.
 	spring.thymeleaf.view-names= # Comma-separated list of view names that can be resolved.
diff --git a/spring-boot-samples/spring-boot-sample-web-method-security/pom.xml b/spring-boot-samples/spring-boot-sample-web-method-security/pom.xml
index d1349ad2639..4050412bb26 100644
--- a/spring-boot-samples/spring-boot-sample-web-method-security/pom.xml
+++ b/spring-boot-samples/spring-boot-sample-web-method-security/pom.xml
@@ -28,6 +28,10 @@
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-security</artifactId>
 		</dependency>
+		<dependency>
+			<groupId>org.springframework.boot</groupId>
+			<artifactId>spring-boot-starter-web</artifactId>
+		</dependency>
 		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-thymeleaf</artifactId>
diff --git a/spring-boot-samples/spring-boot-sample-web-secure-custom/pom.xml b/spring-boot-samples/spring-boot-sample-web-secure-custom/pom.xml
index 2c55f2ed043..789cc856342 100644
--- a/spring-boot-samples/spring-boot-sample-web-secure-custom/pom.xml
+++ b/spring-boot-samples/spring-boot-sample-web-secure-custom/pom.xml
@@ -24,6 +24,10 @@
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-security</artifactId>
 		</dependency>
+		<dependency>
+			<groupId>org.springframework.boot</groupId>
+			<artifactId>spring-boot-starter-web</artifactId>
+		</dependency>
 		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-thymeleaf</artifactId>
diff --git a/spring-boot-samples/spring-boot-sample-web-secure-jdbc/pom.xml b/spring-boot-samples/spring-boot-sample-web-secure-jdbc/pom.xml
index bc9200c013c..0d02baf7b2b 100644
--- a/spring-boot-samples/spring-boot-sample-web-secure-jdbc/pom.xml
+++ b/spring-boot-samples/spring-boot-sample-web-secure-jdbc/pom.xml
@@ -24,6 +24,10 @@
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-security</artifactId>
 		</dependency>
+		<dependency>
+			<groupId>org.springframework.boot</groupId>
+			<artifactId>spring-boot-starter-web</artifactId>
+		</dependency>
 		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-thymeleaf</artifactId>
diff --git a/spring-boot-samples/spring-boot-sample-web-secure/pom.xml b/spring-boot-samples/spring-boot-sample-web-secure/pom.xml
index 09ca8418a95..8d9f6aa0211 100644
--- a/spring-boot-samples/spring-boot-sample-web-secure/pom.xml
+++ b/spring-boot-samples/spring-boot-sample-web-secure/pom.xml
@@ -28,6 +28,10 @@
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-security</artifactId>
 		</dependency>
+		<dependency>
+			<groupId>org.springframework.boot</groupId>
+			<artifactId>spring-boot-starter-web</artifactId>
+		</dependency>
 		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-thymeleaf</artifactId>
diff --git a/spring-boot-samples/spring-boot-sample-web-ui/build.gradle b/spring-boot-samples/spring-boot-sample-web-ui/build.gradle
index 67602012a14..b700bf65e49 100644
--- a/spring-boot-samples/spring-boot-sample-web-ui/build.gradle
+++ b/spring-boot-samples/spring-boot-sample-web-ui/build.gradle
@@ -33,6 +33,7 @@ repositories {
 }
 
 dependencies {
+	compile("org.springframework.boot:spring-boot-starter-web")
 	compile("org.springframework.boot:spring-boot-starter-thymeleaf")
 	compile("org.hibernate:hibernate-validator")
 	testCompile("org.springframework.boot:spring-boot-starter-test")
diff --git a/spring-boot-samples/spring-boot-sample-web-ui/pom.xml b/spring-boot-samples/spring-boot-sample-web-ui/pom.xml
index acd2573d31b..d5a3e8ef7a9 100755
--- a/spring-boot-samples/spring-boot-sample-web-ui/pom.xml
+++ b/spring-boot-samples/spring-boot-sample-web-ui/pom.xml
@@ -20,6 +20,10 @@
 	</properties>
 	<dependencies>
 		<!-- Compile -->
+		<dependency>
+			<groupId>org.springframework.boot</groupId>
+			<artifactId>spring-boot-starter-web</artifactId>
+		</dependency>
 		<dependency>
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter-thymeleaf</artifactId>
diff --git a/spring-boot-starters/spring-boot-starter-thymeleaf/pom.xml b/spring-boot-starters/spring-boot-starter-thymeleaf/pom.xml
index f851bc98c99..c8faeeab1c6 100644
--- a/spring-boot-starters/spring-boot-starter-thymeleaf/pom.xml
+++ b/spring-boot-starters/spring-boot-starter-thymeleaf/pom.xml
@@ -22,10 +22,6 @@
 			<groupId>org.springframework.boot</groupId>
 			<artifactId>spring-boot-starter</artifactId>
 		</dependency>
-		<dependency>
-			<groupId>org.springframework.boot</groupId>
-			<artifactId>spring-boot-starter-web</artifactId>
-		</dependency>
 		<dependency>
 			<groupId>org.thymeleaf</groupId>
 			<artifactId>thymeleaf-spring5</artifactId>
