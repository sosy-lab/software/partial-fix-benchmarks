diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfiguration.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfiguration.java
index 3d0670be3cc..7b2f3dc4063 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfiguration.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfiguration.java
@@ -26,6 +26,7 @@
 import com.unboundid.ldap.listener.InMemoryDirectoryServerConfig;
 import com.unboundid.ldap.listener.InMemoryListenerConfig;
 import com.unboundid.ldap.sdk.LDAPException;
+import com.unboundid.ldap.sdk.schema.Schema;
 import com.unboundid.ldif.LDIFReader;
 
 import org.springframework.boot.autoconfigure.AutoConfigureBefore;
@@ -55,6 +56,7 @@
  * {@link EnableAutoConfiguration Auto-configuration} for Embedded LDAP.
  *
  * @author Eddú Meléndez
+ * @author Mathieu Ouellet
  * @since 1.5.0
  */
 @Configuration
@@ -107,6 +109,28 @@ public InMemoryDirectoryServer directoryServer() throws LDAPException {
 					this.embeddedProperties.getCredential().getUsername(),
 					this.embeddedProperties.getCredential().getPassword());
 		}
+
+		Schema schema = null;
+		if (this.embeddedProperties.getValidation().isEnabled()) {
+			schema = Schema.getDefaultStandardSchema();
+			String schemaLocation = this.embeddedProperties.getValidation().getSchema();
+			if (StringUtils.hasText(schemaLocation)) {
+				try {
+					Resource resource = this.applicationContext
+							.getResource(schemaLocation);
+					if (resource.exists()) {
+						schema = Schema.mergeSchemas(schema,
+								Schema.getSchema(resource.getFile()));
+					}
+				}
+				catch (Exception ex) {
+					throw new IllegalStateException(
+							"Unable to load schema " + schemaLocation, ex);
+				}
+			}
+		}
+		config.setSchema(schema);
+
 		InMemoryListenerConfig listenerConfig = InMemoryListenerConfig
 				.createLDAPConfig("LDAP", this.embeddedProperties.getPort());
 		config.setListenerConfigs(listenerConfig);
diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapProperties.java
index 1a4ac083edd..715ba0159fc 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapProperties.java
@@ -22,6 +22,7 @@
  * Configuration properties for Embedded LDAP.
  *
  * @author Eddú Meléndez
+ * @author Mathieu Ouellet
  * @since 1.5.0
  */
 @ConfigurationProperties(prefix = "spring.ldap.embedded")
@@ -47,6 +48,11 @@
 	 */
 	private String ldif = "classpath:schema.ldif";
 
+	/**
+	 * Schema validation
+	 */
+	private Validation validation = new Validation();
+
 	public int getPort() {
 		return this.port;
 	}
@@ -79,6 +85,14 @@ public void setLdif(String ldif) {
 		this.ldif = ldif;
 	}
 
+	public Validation getValidation() {
+		return this.validation;
+	}
+
+	public void setValidation(Validation validation) {
+		this.validation = validation;
+	}
+
 	static class Credential {
 
 		/**
@@ -109,4 +123,34 @@ public void setPassword(String password) {
 
 	}
 
+	public static class Validation {
+
+		/**
+		 * Enable LDAP schema validation
+		 */
+		private boolean enabled = true;
+
+		/**
+		 * Path to the custom schema file
+		 */
+		private String schema;
+
+		public boolean isEnabled() {
+			return this.enabled;
+		}
+
+		public void setEnabled(boolean enabled) {
+			this.enabled = enabled;
+		}
+
+		public String getSchema() {
+			return this.schema;
+		}
+
+		public void setSchema(String schema) {
+			this.schema = schema;
+		}
+
+	}
+
 }
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfigurationTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfigurationTests.java
index d5d5e54daee..5643eda9460 100644
--- a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfigurationTests.java
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/ldap/embedded/EmbeddedLdapAutoConfigurationTests.java
@@ -131,6 +131,29 @@ public void testQueryEmbeddedLdap() throws LDAPException {
 		assertThat(ldapTemplate.list("ou=company1,c=Sweden,dc=spring,dc=org")).hasSize(4);
 	}
 
+	@Test
+	public void testDisableSchemaValidation() throws LDAPException {
+		load("spring.ldap.embedded.validation.enabled:false",
+				"spring.ldap.embedded.base-dn:dc=spring,dc=org");
+		InMemoryDirectoryServer server = this.context
+				.getBean(InMemoryDirectoryServer.class);
+		assertThat(server.getSchema()).isNull();
+	}
+
+	@Test
+	public void testCustomSchemaValidation() throws LDAPException {
+		load("spring.ldap.embedded.validation.schema:classpath:custom-schema.ldif",
+				"spring.ldap.embedded.ldif:classpath:custom-schema-sample.ldif",
+				"spring.ldap.embedded.base-dn:dc=spring,dc=org");
+		InMemoryDirectoryServer server = this.context
+				.getBean(InMemoryDirectoryServer.class);
+
+		assertThat(server.getSchema().getObjectClass("exampleAuxiliaryClass"))
+				.isNotNull();
+		assertThat(server.getSchema().getAttributeType("exampleAttributeName"))
+				.isNotNull();
+	}
+
 	private void load(String... properties) {
 		EnvironmentTestUtils.addEnvironment(this.context, properties);
 		this.context.register(EmbeddedLdapAutoConfiguration.class,
diff --git a/spring-boot-autoconfigure/src/test/resources/custom-schema-sample.ldif b/spring-boot-autoconfigure/src/test/resources/custom-schema-sample.ldif
new file mode 100644
index 00000000000..c5b81e84cee
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/resources/custom-schema-sample.ldif
@@ -0,0 +1,7 @@
+dn: dc=spring,dc=org
+objectclass: top
+objectclass: domain
+objectclass: extensibleObject
+objectClass: exampleAuxiliaryClass
+dc: spring
+exampleAttributeName: exampleAttributeName
diff --git a/spring-boot-autoconfigure/src/test/resources/custom-schema.ldif b/spring-boot-autoconfigure/src/test/resources/custom-schema.ldif
new file mode 100644
index 00000000000..a561a201cb1
--- /dev/null
+++ b/spring-boot-autoconfigure/src/test/resources/custom-schema.ldif
@@ -0,0 +1,17 @@
+dn: cn=schema
+attributeTypes: ( 1.3.6.1.4.1.32473.1.1.1
+  NAME 'exampleAttributeName'
+  DESC 'An example attribute type definition'
+  EQUALITY caseIgnoreMatch
+  ORDERING caseIgnoreOrderingMatch
+  SUBSTR caseIgnoreSubstringsMatch
+  SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
+  SINGLE-VALUE
+  X-ORIGIN 'Managing Schema Document' )
+objectClasses: ( 1.3.6.1.4.1.32473.1.2.2
+  NAME 'exampleAuxiliaryClass'
+  DESC 'An example auxiliary object class definition'
+  SUP top
+  AUXILIARY
+  MAY exampleAttributeName
+  X-ORIGIN 'Managing Schema Document' )
\ No newline at end of file
