diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/ServerProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/ServerProperties.java
index 9830d0e87db..b4cd03e821f 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/ServerProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/web/ServerProperties.java
@@ -1006,6 +1006,7 @@ private void customizeAccessLog(TomcatEmbeddedServletContainerFactory factory) {
 					this.accesslog.isRequestAttributesEnabled());
 			valve.setRotatable(this.accesslog.isRotate());
 			valve.setBuffered(this.accesslog.isBuffered());
+			valve.setFileDateFormat(this.accesslog.getFileDateFormat());
 			factory.addEngineValves(valve);
 		}
 
@@ -1071,6 +1072,13 @@ public void customize(Context context) {
 			 */
 			private boolean buffered = true;
 
+			/**
+			 * Customized date format in the access log file name. <a href=
+			 * "http://tomcat.apache.org/tomcat-7.0-doc/config/valve.html#Access_Log_Valve">Tomcat
+			 * Documentation</a>
+			 */
+			private String fileDateFormat = "yyyy-MM-dd";
+
 			public boolean isEnabled() {
 				return this.enabled;
 			}
@@ -1143,6 +1151,13 @@ public void setBuffered(boolean buffered) {
 				this.buffered = buffered;
 			}
 
+			public String getFileDateFormat() {
+				return fileDateFormat;
+			}
+
+			public void setFileDateFormat(String fileDateFormat) {
+				this.fileDateFormat = fileDateFormat;
+			}
 		}
 
 	}
diff --git a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/ServerPropertiesTests.java b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/ServerPropertiesTests.java
index f9df7a8314c..20617b7e72c 100644
--- a/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/ServerPropertiesTests.java
+++ b/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/web/ServerPropertiesTests.java
@@ -160,6 +160,40 @@ public void tomcatAccessLogCanBeEnabled() {
 				.isInstanceOf(AccessLogValve.class);
 	}
 
+	@Test
+	public void tomcatAccessLogFileDateFormatByDefault() {
+		TomcatEmbeddedServletContainerFactory tomcatContainer = new TomcatEmbeddedServletContainerFactory();
+		Map<String, String> map = new HashMap<String, String>();
+		map.put("server.tomcat.accesslog.enabled", "true");
+		bindProperties(map);
+		this.properties.customize(tomcatContainer);
+		assertThat(((AccessLogValve) tomcatContainer.getEngineValves().iterator().next())
+				.getFileDateFormat()).isEqualTo("yyyy-MM-dd");
+	}
+
+	@Test
+	public void tomcatAccessLogFileDateFormatCanBeRedefined() {
+		TomcatEmbeddedServletContainerFactory tomcatContainer = new TomcatEmbeddedServletContainerFactory();
+		Map<String, String> map = new HashMap<String, String>();
+		map.put("server.tomcat.accesslog.enabled", "true");
+		map.put("server.tomcat.accesslog.fileDateFormat", "yyyy-MM-dd.HH");
+		bindProperties(map);
+		this.properties.customize(tomcatContainer);
+		assertThat(((AccessLogValve) tomcatContainer.getEngineValves().iterator().next())
+				.getFileDateFormat()).isEqualTo("yyyy-MM-dd.HH");
+	}
+
+	@Test(expected = IllegalArgumentException.class)
+	public void tomcatAccessLogFileDateFormatWrongFormat() {
+		TomcatEmbeddedServletContainerFactory tomcatContainer = new TomcatEmbeddedServletContainerFactory();
+		Map<String, String> map = new HashMap<String, String>();
+		map.put("server.tomcat.accesslog.enabled", "true");
+		map.put("server.tomcat.accesslog.fileDateFormat",
+				"this-is-obviously-a-wrong-format");
+		bindProperties(map);
+		this.properties.customize(tomcatContainer);
+	}
+
 	@Test
 	public void tomcatAccessLogIsBufferedByDefault() {
 		TomcatEmbeddedServletContainerFactory tomcatContainer = new TomcatEmbeddedServletContainerFactory();
diff --git a/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc b/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
index 9adba7f7219..11e192fe9ae 100644
--- a/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
+++ b/spring-boot-docs/src/main/asciidoc/appendix-application-properties.adoc
@@ -194,6 +194,7 @@ content into your application; rather pick only the properties that you need.
 	server.ssl.trust-store-provider= # Provider for the trust store.
 	server.ssl.trust-store-type= # Type of the trust store.
 	server.tomcat.accept-count= # Maximum queue length for incoming connection requests when all possible request processing threads are in use.
+	server.tomcat.accesslog.fileDateFormat=yyyy-MM-dd # Customized date format in the access log file name.
 	server.tomcat.accesslog.buffered=true # Buffer output such that it is only flushed periodically.
 	server.tomcat.accesslog.directory=logs # Directory in which log files are created. Can be relative to the tomcat base dir or absolute.
 	server.tomcat.accesslog.enabled=false # Enable access log.
