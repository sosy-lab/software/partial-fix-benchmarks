diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java
index 40264375ac8..860e8324938 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java
@@ -24,6 +24,9 @@
 
 import javax.sql.DataSource;
 
+import org.apache.commons.logging.Log;
+import org.apache.commons.logging.LogFactory;
+
 import org.springframework.beans.factory.BeanClassLoaderAware;
 import org.springframework.beans.factory.BeanCreationException;
 import org.springframework.beans.factory.InitializingBean;
@@ -46,11 +49,14 @@
  * @author Stephane Nicoll
  * @author Benedikt Ritter
  * @author Eddú Meléndez
+ * @author Kristine Jetzke
  * @since 1.1.0
  */
 @ConfigurationProperties(prefix = "spring.datasource")
 public class DataSourceProperties
-		implements BeanClassLoaderAware, EnvironmentAware, InitializingBean {
+	implements BeanClassLoaderAware, EnvironmentAware, InitializingBean {
+
+	private static final Log logger = LogFactory.getLog(DataSourceProperties.class);
 
 	private ClassLoader classLoader;
 
@@ -288,6 +294,8 @@ public String determineUrl() {
 		if (StringUtils.hasText(this.url)) {
 			return this.url;
 		}
+
+		logger.debug("No url defined, looking for embedded database.");
 		String url = this.embeddedDatabaseConnection.getUrl(determineDatabaseName());
 		if (!StringUtils.hasText(url)) {
 			throw new DataSourceBeanCreationException(this.embeddedDatabaseConnection,
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java
index 92a64ccb617..fed7a9f7c19 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java
@@ -21,6 +21,7 @@
 import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
 
 import static org.assertj.core.api.Assertions.assertThat;
+import static org.assertj.core.api.Assertions.assertThatThrownBy;
 
 /**
  * Tests for {@link DataSourceProperties}.
@@ -28,6 +29,7 @@
  * @author Maciej Walkowiak
  * @author Stephane Nicoll
  * @author Eddú Meléndez
+ * @author Kristine Jetzke
  */
 public class DataSourcePropertiesTests {
 
@@ -59,6 +61,14 @@ public void determineUrl() throws Exception {
 				.isEqualTo(EmbeddedDatabaseConnection.H2.getUrl());
 	}
 
+	@Test
+	public void determineUrlUrlNull() throws Exception {
+		DataSourceProperties properties = new DataSourceProperties();
+		assertThatThrownBy(() -> properties.determineUrl())
+				.isInstanceOf(DataSourceProperties.DataSourceBeanCreationException.class)
+				.hasMessageContaining("Cannot determine embedded database url");
+	}
+
 	@Test
 	public void determineUrlWithExplicitConfig() throws Exception {
 		DataSourceProperties properties = new DataSourceProperties();
diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java
index eccfafac17e..f5edcd424db 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java
@@ -34,6 +34,7 @@
  * @author Phillip Webb
  * @author Dave Syer
  * @author Stephane Nicoll
+ * @author Kristine Jetzke
  * @see #get(ClassLoader)
  */
 public enum EmbeddedDatabaseConnection {
@@ -106,7 +107,7 @@ public String getUrl() {
 	 */
 	public String getUrl(String databaseName) {
 		Assert.hasText(databaseName, "DatabaseName must not be null.");
-		return String.format(this.url, databaseName);
+		return this.url != null ? String.format(this.url, databaseName) : null;
 	}
 
 	/**
