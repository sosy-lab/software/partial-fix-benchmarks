diff --git a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java
index 40264375ac8..b5d9108ced0 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/jdbc/DataSourceProperties.java
@@ -24,6 +24,8 @@
 
 import javax.sql.DataSource;
 
+import org.apache.commons.logging.Log;
+import org.apache.commons.logging.LogFactory;
 import org.springframework.beans.factory.BeanClassLoaderAware;
 import org.springframework.beans.factory.BeanCreationException;
 import org.springframework.beans.factory.InitializingBean;
@@ -51,6 +53,8 @@
 @ConfigurationProperties(prefix = "spring.datasource")
 public class DataSourceProperties
 		implements BeanClassLoaderAware, EnvironmentAware, InitializingBean {
+	
+	private static final Log logger = LogFactory.getLog(DataSourceProperties.class);
 
 	private ClassLoader classLoader;
 
@@ -288,6 +292,8 @@ public String determineUrl() {
 		if (StringUtils.hasText(this.url)) {
 			return this.url;
 		}
+		
+		logger.debug("No url defined, looking for embedded database.");
 		String url = this.embeddedDatabaseConnection.getUrl(determineDatabaseName());
 		if (!StringUtils.hasText(url)) {
 			throw new DataSourceBeanCreationException(this.embeddedDatabaseConnection,
diff --git a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java
index 92a64ccb617..814ee74295e 100644
--- a/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java
+++ b/spring-boot-project/spring-boot-autoconfigure/src/test/java/org/springframework/boot/autoconfigure/jdbc/DataSourcePropertiesTests.java
@@ -21,6 +21,7 @@
 import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
 
 import static org.assertj.core.api.Assertions.assertThat;
+import static org.assertj.core.api.Assertions.assertThatThrownBy;;
 
 /**
  * Tests for {@link DataSourceProperties}.
@@ -59,6 +60,14 @@ public void determineUrl() throws Exception {
 				.isEqualTo(EmbeddedDatabaseConnection.H2.getUrl());
 	}
 
+	@Test
+	public void determineUrlUrlNull() throws Exception {
+		DataSourceProperties properties = new DataSourceProperties();
+		assertThatThrownBy(() -> properties.determineUrl())
+				.isInstanceOf(DataSourceProperties.DataSourceBeanCreationException.class)
+				.hasMessageContaining("Cannot determine embedded database url");
+	}
+
 	@Test
 	public void determineUrlWithExplicitConfig() throws Exception {
 		DataSourceProperties properties = new DataSourceProperties();
diff --git a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java
index eccfafac17e..eeaa971cc4e 100644
--- a/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java
+++ b/spring-boot-project/spring-boot/src/main/java/org/springframework/boot/jdbc/EmbeddedDatabaseConnection.java
@@ -106,7 +106,7 @@ public String getUrl() {
 	 */
 	public String getUrl(String databaseName) {
 		Assert.hasText(databaseName, "DatabaseName must not be null.");
-		return String.format(this.url, databaseName);
+		return this.url != null ? String.format(this.url, databaseName) : null;
 	}
 
 	/**
