diff --git a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaProperties.java b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaProperties.java
index 84437aba29f..fa481f4b5f4 100644
--- a/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaProperties.java
+++ b/spring-boot-autoconfigure/src/main/java/org/springframework/boot/autoconfigure/orm/jpa/JpaProperties.java
@@ -25,7 +25,7 @@
 import org.apache.commons.logging.LogFactory;
 import org.springframework.boot.autoconfigure.jdbc.EmbeddedDatabaseConnection;
 import org.springframework.boot.context.properties.ConfigurationProperties;
-import org.springframework.boot.orm.jpa.SpringNamingStrategy;
+import org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy;
 import org.springframework.orm.jpa.vendor.Database;
 import org.springframework.util.StringUtils;
 
diff --git a/spring-boot/src/main/java/org/springframework/boot/orm/jpa/SpringNamingStrategy.java b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/SpringNamingStrategy.java
index c0f133e720f..a6917f94ced 100644
--- a/spring-boot/src/main/java/org/springframework/boot/orm/jpa/SpringNamingStrategy.java
+++ b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/SpringNamingStrategy.java
@@ -16,32 +16,17 @@
 
 package org.springframework.boot.orm.jpa;
 
-import org.hibernate.cfg.ImprovedNamingStrategy;
 import org.hibernate.cfg.NamingStrategy;
-import org.hibernate.internal.util.StringHelper;
-import org.springframework.util.Assert;
-import org.springframework.util.StringUtils;
 
 /**
  * Hibernate {@link NamingStrategy} that follows Spring recommended naming conventions.
- * Naming conventions implemented here are identical to {@link ImprovedNamingStrategy}
- * with the exception that foreign key columns include the referenced column name.
  *
  * @author Phillip Webb
- * @see "http://stackoverflow.com/questions/7689206/ejb3namingstrategy-vs-improvednamingstrategy-foreign-key-naming"
+ * @deprecated Since 1.2.0 in favor of
+ * {@link org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy}
  */
-public class SpringNamingStrategy extends ImprovedNamingStrategy {
-
-	@Override
-	public String foreignKeyColumnName(String propertyName, String propertyEntityName,
-			String propertyTableName, String referencedColumnName) {
-		String name = propertyTableName;
-		if (propertyName != null) {
-			name = StringHelper.unqualify(propertyName);
-		}
-		Assert.state(StringUtils.hasLength(name),
-				"Unable to generate foreignKeyColumnName");
-		return columnName(name) + "_" + referencedColumnName;
-	}
+@Deprecated
+public class SpringNamingStrategy extends
+		org.springframework.boot.orm.jpa.hibernate.SpringNamingStrategy {
 
 }
diff --git a/spring-boot/src/main/java/org/springframework/boot/orm/jpa/hibernate/SpringJtaPlatform.java b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/hibernate/SpringJtaPlatform.java
new file mode 100644
index 00000000000..c8b1b665458
--- /dev/null
+++ b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/hibernate/SpringJtaPlatform.java
@@ -0,0 +1,44 @@
+package org.springframework.boot.orm.jpa.hibernate;
+
+import javax.transaction.TransactionManager;
+import javax.transaction.UserTransaction;
+
+import org.hibernate.engine.transaction.jta.platform.internal.AbstractJtaPlatform;
+import org.springframework.transaction.jta.JtaTransactionManager;
+import org.springframework.util.Assert;
+
+/**
+ * Generic Hibernate {@link AbstractJtaPlatform} implementation that simply resolves the
+ * JTA {@link UserTransaction} and {@link TransactionManager} from the Spring-configured
+ * {@link JtaTransactionManager} implementation.
+ *
+ * @author Josh Long
+ * @author Phillip Webb
+ * @since 1.2.0
+ */
+public class SpringJtaPlatform extends AbstractJtaPlatform {
+
+	private static final long serialVersionUID = 1L;
+
+	private final JtaTransactionManager transactionManager;
+
+	public SpringJtaPlatform(JtaTransactionManager transactionManager) {
+		Assert.notNull(transactionManager, "TransactionManager must not be null");
+		this.transactionManager = transactionManager;
+	}
+
+	protected boolean hasTransactionManager() {
+		return true;
+	}
+
+	@Override
+	protected TransactionManager locateTransactionManager() {
+		return this.transactionManager.getTransactionManager();
+	}
+
+	@Override
+	protected UserTransaction locateUserTransaction() {
+		return this.transactionManager.getUserTransaction();
+	}
+
+}
diff --git a/spring-boot/src/main/java/org/springframework/boot/orm/jpa/hibernate/SpringNamingStrategy.java b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/hibernate/SpringNamingStrategy.java
new file mode 100644
index 00000000000..92239e48000
--- /dev/null
+++ b/spring-boot/src/main/java/org/springframework/boot/orm/jpa/hibernate/SpringNamingStrategy.java
@@ -0,0 +1,48 @@
+/*
+ * Copyright 2012-2014 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.boot.orm.jpa.hibernate;
+
+import org.hibernate.cfg.ImprovedNamingStrategy;
+import org.hibernate.cfg.NamingStrategy;
+import org.hibernate.internal.util.StringHelper;
+import org.springframework.util.Assert;
+import org.springframework.util.StringUtils;
+
+/**
+ * Hibernate {@link NamingStrategy} that follows Spring recommended naming conventions.
+ * Naming conventions implemented here are identical to {@link ImprovedNamingStrategy}
+ * with the exception that foreign key columns include the referenced column name.
+ *
+ * @author Phillip Webb
+ * @see "http://stackoverflow.com/questions/7689206/ejb3namingstrategy-vs-improvednamingstrategy-foreign-key-naming"
+ * @since 1.2.0
+ */
+public class SpringNamingStrategy extends ImprovedNamingStrategy {
+
+	@Override
+	public String foreignKeyColumnName(String propertyName, String propertyEntityName,
+			String propertyTableName, String referencedColumnName) {
+		String name = propertyTableName;
+		if (propertyName != null) {
+			name = StringHelper.unqualify(propertyName);
+		}
+		Assert.state(StringUtils.hasLength(name),
+				"Unable to generate foreignKeyColumnName");
+		return columnName(name) + "_" + referencedColumnName;
+	}
+
+}
