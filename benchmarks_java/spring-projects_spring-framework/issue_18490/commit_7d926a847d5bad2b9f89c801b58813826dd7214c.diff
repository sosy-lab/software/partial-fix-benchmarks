diff --git a/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerIntegrationTests.java b/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerIntegrationTests.java
index 46ed5efa0c3..1175eedeaaa 100644
--- a/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerIntegrationTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerIntegrationTests.java
@@ -35,7 +35,6 @@
 import org.springframework.test.context.event.annotation.BeforeTestExecution;
 import org.springframework.test.context.event.annotation.BeforeTestMethod;
 import org.springframework.test.context.event.annotation.PrepareTestInstance;
-import org.springframework.util.ReflectionUtils;
 
 import static org.mockito.Mockito.mock;
 import static org.mockito.Mockito.only;
@@ -47,27 +46,23 @@
  * accompanying {@link TestContextEvent} annotations.
  *
  * @author Frank Scheffler
+ * @author Sam Brannen
  * @since 5.2
  */
 public class EventPublishingTestExecutionListenerIntegrationTests {
 
-	private TestContextManager testContextManager;
-	private TestContext testContext;
-	private TestExecutionListener listener;
-	private Object testInstance;
-	private Method testMethod;
+	private final TestContextManager testContextManager = new TestContextManager(TestCase.class);
+	private final TestContext testContext = testContextManager.getTestContext();
+	private final TestExecutionListener listener = testContext.getApplicationContext().getBean(EventCaptureConfiguration.class).listener();
+	private final Object testInstance = new TestCase();
+	private final Method testMethod = null;
 
 
 	@Before
-	public void initialize() {
-		TestContextExposingTestContextManager tcm = new TestContextExposingTestContextManager();
-		testContextManager = tcm;
-		testContext = tcm.getProtectedTestContext();
-		listener = testContext.getApplicationContext().getBean(EventCaptureConfiguration.class).trigger();
-		// reset because mock is a cached context bean
+	public void resetMock() {
+		// The mocked listener is a bean in the ApplicationContext that is stored
+		// in a static cache by the Spring TestContext Framework.
 		reset(listener);
-		testInstance = new EmptyTestCase();
-		testMethod = ReflectionUtils.findMethod(EmptyTestCase.class, "dummyMethod");
 	}
 
 	@Test
@@ -117,50 +112,50 @@ public void afterTestClassAnnotation() throws Exception {
 	static class EventCaptureConfiguration {
 
 		@Bean
-		public TestExecutionListener trigger() {
+		public TestExecutionListener listener() {
 			return mock(TestExecutionListener.class);
 		}
 
 		@BeforeTestClass
 		public void beforeTestClass(BeforeTestClassEvent e) throws Exception {
-			trigger().beforeTestClass(e.getSource());
+			listener().beforeTestClass(e.getSource());
 		}
 
 		@PrepareTestInstance
 		public void prepareTestInstance(PrepareTestInstanceEvent e) throws Exception {
-			trigger().prepareTestInstance(e.getSource());
+			listener().prepareTestInstance(e.getSource());
 		}
 
 		@BeforeTestMethod
 		public void beforeTestMethod(BeforeTestMethodEvent e) throws Exception {
-			trigger().beforeTestMethod(e.getSource());
+			listener().beforeTestMethod(e.getSource());
 		}
 
 		@BeforeTestExecution
-		public void beforeTestExecutiob(BeforeTestExecutionEvent e) throws Exception {
-			trigger().beforeTestExecution(e.getSource());
+		public void beforeTestExecution(BeforeTestExecutionEvent e) throws Exception {
+			listener().beforeTestExecution(e.getSource());
 		}
 
 		@AfterTestExecution
 		public void afterTestExecution(AfterTestExecutionEvent e) throws Exception {
-			trigger().afterTestExecution(e.getSource());
+			listener().afterTestExecution(e.getSource());
 		}
 
 		@AfterTestMethod
 		public void afterTestMethod(AfterTestMethodEvent e) throws Exception {
-			trigger().afterTestMethod(e.getSource());
+			listener().afterTestMethod(e.getSource());
 		}
 
 		@AfterTestClass
 		public void afterTestClass(AfterTestClassEvent e) throws Exception {
-			trigger().afterTestClass(e.getSource());
+			listener().afterTestClass(e.getSource());
 		}
 
 	}
 
 	@ContextConfiguration(classes = EventCaptureConfiguration.class)
 	@TestExecutionListeners(EventPublishingTestExecutionListener.class)
-	static class EmptyTestCase {
+	static class TestCase {
 
 		/**
 		 * Serves as dummy test method.
@@ -170,15 +165,4 @@ public void dummyTestMethod() {
 		}
 	}
 
-	static class TestContextExposingTestContextManager extends TestContextManager {
-
-		public TestContextExposingTestContextManager() {
-			super(EmptyTestCase.class);
-		}
-
-		public TestContext getProtectedTestContext() {
-			return getTestContext();
-		}
-	}
-
 }
diff --git a/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerTests.java b/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerTests.java
index 7eeb2ab9f72..5c3cf3304d1 100644
--- a/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/event/EventPublishingTestExecutionListenerTests.java
@@ -23,13 +23,14 @@
 import org.mockito.Captor;
 import org.mockito.Mock;
 import org.mockito.junit.MockitoJUnitRunner;
+
 import org.springframework.test.context.TestContext;
 
 import static org.hamcrest.Matchers.equalTo;
 import static org.hamcrest.Matchers.instanceOf;
 import static org.junit.Assert.assertThat;
-import static org.mockito.BDDMockito.only;
-import static org.mockito.BDDMockito.verify;
+import static org.mockito.Mockito.only;
+import static org.mockito.Mockito.verify;
 
 /**
  * Unit tests for {@link EventPublishingTestExecutionListener}.
@@ -40,58 +41,58 @@
 @RunWith(MockitoJUnitRunner.class)
 public class EventPublishingTestExecutionListenerTests {
 
+	private final EventPublishingTestExecutionListener listener = new EventPublishingTestExecutionListener();
+
 	@Mock(answer = Answers.RETURNS_DEEP_STUBS)
 	private TestContext testContext;
 
 	@Captor
 	private ArgumentCaptor<TestContextEvent> testExecutionEvent;
 
-	private final EventPublishingTestExecutionListener listener = new EventPublishingTestExecutionListener();
-
 
 	@Test
-	public void publishBeforeClassTestExecutionEvent() {
+	public void publishBeforeTestClassEvent() {
 		listener.beforeTestClass(testContext);
 		assertEvent(BeforeTestClassEvent.class);
 	}
 
 	@Test
-	public void publishPrepareInstanceTestExecutionEvent() {
+	public void publishPrepareTestInstanceEvent() {
 		listener.prepareTestInstance(testContext);
 		assertEvent(PrepareTestInstanceEvent.class);
 	}
 
 	@Test
-	public void publishBeforeMethodTestExecutionEvent() {
+	public void publishBeforeTestMethodEvent() {
 		listener.beforeTestMethod(testContext);
 		assertEvent(BeforeTestMethodEvent.class);
 	}
 
 	@Test
-	public void publishBeforeExecutionTestExecutionEvent() {
+	public void publishBeforeTestExecutionEvent() {
 		listener.beforeTestExecution(testContext);
 		assertEvent(BeforeTestExecutionEvent.class);
 	}
 
 	@Test
-	public void publishAfterExecutionTestExecutionEvent() {
+	public void publishAfterTestExecutionEvent() {
 		listener.afterTestExecution(testContext);
 		assertEvent(AfterTestExecutionEvent.class);
 	}
 
 	@Test
-	public void publishAfterMethodTestExecutionEvent() {
+	public void publishAfterTestMethodEvent() {
 		listener.afterTestMethod(testContext);
 		assertEvent(AfterTestMethodEvent.class);
 	}
 
 	@Test
-	public void publishAfterClassTestExecutionEvent() {
+	public void publishAfterTestClassEvent() {
 		listener.afterTestClass(testContext);
 		assertEvent(AfterTestClassEvent.class);
 	}
 
-	private <T extends TestContextEvent> void assertEvent(Class<T> eventClass) {
+	private void assertEvent(Class<? extends TestContextEvent> eventClass) {
 		verify(testContext.getApplicationContext(), only()).publishEvent(testExecutionEvent.capture());
 		assertThat(testExecutionEvent.getValue(), instanceOf(eventClass));
 		assertThat(testExecutionEvent.getValue().getSource(), equalTo(testContext));
