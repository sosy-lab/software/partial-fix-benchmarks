diff --git a/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java b/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java
index e1317816b89..fadf69b1502 100644
--- a/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java
+++ b/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java
@@ -26,6 +26,7 @@
 import org.springframework.core.style.ToStringCreator;
 import org.springframework.lang.Nullable;
 import org.springframework.util.Assert;
+import org.springframework.util.ClassUtils;
 import org.springframework.util.ObjectUtils;
 
 /**
@@ -123,7 +124,7 @@
 			}
 		}
 
-		// Declared on interface?
+		// Declared on an interface?
 		for (Class<?> ifc : clazz.getInterfaces()) {
 			AnnotationDescriptor<T> descriptor = findAnnotationDescriptor(ifc, visited, annotationType);
 			if (descriptor != null) {
@@ -133,7 +134,20 @@
 		}
 
 		// Declared on a superclass?
-		return findAnnotationDescriptor(clazz.getSuperclass(), visited, annotationType);
+		AnnotationDescriptor<T> descriptor = findAnnotationDescriptor(clazz.getSuperclass(), visited, annotationType);
+		if (descriptor != null) {
+			return descriptor;
+		}
+
+		// Declared on an enclosing class of an inner class?
+		if (ClassUtils.isInnerClass(clazz)) {
+			descriptor = findAnnotationDescriptor(clazz.getDeclaringClass(), visited, annotationType);
+			if (descriptor != null) {
+				return descriptor;
+			}
+		}
+
+		return null;
 	}
 
 	/**
@@ -212,7 +226,7 @@ private static UntypedAnnotationDescriptor findAnnotationDescriptorForTypes(@Nul
 			}
 		}
 
-		// Declared on interface?
+		// Declared on an interface?
 		for (Class<?> ifc : clazz.getInterfaces()) {
 			UntypedAnnotationDescriptor descriptor = findAnnotationDescriptorForTypes(ifc, visited, annotationTypes);
 			if (descriptor != null) {
@@ -222,7 +236,20 @@ private static UntypedAnnotationDescriptor findAnnotationDescriptorForTypes(@Nul
 		}
 
 		// Declared on a superclass?
-		return findAnnotationDescriptorForTypes(clazz.getSuperclass(), visited, annotationTypes);
+		UntypedAnnotationDescriptor descriptor = findAnnotationDescriptorForTypes(clazz.getSuperclass(), visited, annotationTypes);
+		if (descriptor != null) {
+			return descriptor;
+		}
+
+		// Declared on an enclosing class of an inner class?
+		if (ClassUtils.isInnerClass(clazz)) {
+			descriptor = findAnnotationDescriptorForTypes(clazz.getDeclaringClass(), visited, annotationTypes);
+			if (descriptor != null) {
+				return descriptor;
+			}
+		}
+
+		return null;
 	}
 
 	private static void assertNonEmptyAnnotationTypeArray(Class<?>[] annotationTypes, String message) {
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
index b6a8c554fd3..9003caaf461 100644
--- a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
@@ -44,13 +44,16 @@
 @SpringJUnitConfig(TopLevelConfig.class)
 class NestedTestsWithSpringAndJUnitJupiterTests {
 
+	private static final String FOO = "foo";
+	private static final String BAR = "bar";
+
 	@Autowired
 	String foo;
 
 
 	@Test
 	void topLevelTest() {
-		assertThat(foo).isEqualTo("foo");
+		assertThat(foo).isEqualTo(FOO);
 	}
 
 
@@ -67,8 +70,26 @@ void nestedTest() throws Exception {
 			// In contrast to nested test classes running in JUnit 4, the foo
 			// field in the outer instance should have been injected from the
 			// test ApplicationContext for the outer instance.
-			assertThat(foo).isEqualTo("foo");
-			assertThat(bar).isEqualTo("bar");
+			assertThat(foo).isEqualTo(FOO);
+			assertThat(bar).isEqualTo(BAR);
+		}
+	}
+
+	@Nested
+	// @SpringJUnitConfig(NestedConfig.class)
+	class NestedTestCaseWithInheritedConfigTests {
+
+		@Autowired
+		String bar;
+
+
+		@Test
+		void nestedTest() throws Exception {
+			// Since the configuration is inherited, the foo field in the outer instance
+			// and the bar field in the inner instance should both have been injected
+			// from the test ApplicationContext for the outer instance.
+			assertThat(foo).isEqualTo(FOO);
+			assertThat(bar).isEqualTo(FOO);
 		}
 	}
 
@@ -79,7 +100,7 @@ void nestedTest() throws Exception {
 
 		@Bean
 		String foo() {
-			return "foo";
+			return FOO;
 		}
 	}
 
@@ -88,7 +109,7 @@ String foo() {
 
 		@Bean
 		String bar() {
-			return "bar";
+			return BAR;
 		}
 	}
 
diff --git a/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java b/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
index 27f00f35114..e7f7d9ccc2b 100644
--- a/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
@@ -94,10 +94,11 @@ void assertMergedConfig(
 		assertThat(mergedConfig).isNotNull();
 		assertThat(mergedConfig.getTestClass()).isEqualTo(expectedTestClass);
 		assertThat(mergedConfig.getLocations()).isNotNull();
-		assertThat(mergedConfig.getLocations()).isEqualTo(expectedLocations);
+		assertThat(mergedConfig.getLocations()).containsExactly(expectedLocations);
 		assertThat(mergedConfig.getClasses()).isNotNull();
-		assertThat(mergedConfig.getClasses()).isEqualTo(expectedClasses);
+		assertThat(mergedConfig.getClasses()).containsExactly(expectedClasses);
 		assertThat(mergedConfig.getActiveProfiles()).isNotNull();
+
 		if (expectedContextLoaderClass == null) {
 			assertThat(mergedConfig.getContextLoader()).isNull();
 		}
@@ -217,4 +218,23 @@ void assertMergedConfig(
 	static class PropertiesClassesFoo {
 	}
 
+	@ContextConfiguration(classes = FooConfig.class, loader = AnnotationConfigContextLoader.class)
+	@ActiveProfiles("foo")
+	static class OuterTestCase {
+
+		class NestedTestCaseWithInheritedConfig {
+		}
+
+		@ContextConfiguration(classes = BarConfig.class)
+		@ActiveProfiles("bar")
+		class NestedTestCaseWithMergedInheritedConfig {
+		}
+
+		@ContextConfiguration(classes = BarConfig.class, inheritLocations = false)
+		@ActiveProfiles(profiles = "bar", inheritProfiles = false)
+		class NestedTestCaseWithOverriddenConfig {
+		}
+
+	}
+
 }
diff --git a/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java b/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java
index 6d6d0652ddb..e1279f8c43e 100644
--- a/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java
@@ -21,6 +21,7 @@
 import java.lang.annotation.RetentionPolicy;
 import java.lang.annotation.Target;
 
+import org.junit.Ignore;
 import org.junit.Test;
 
 import org.springframework.test.context.BootstrapTestUtils;
@@ -194,6 +195,46 @@ public void buildMergedConfigWithAnnotationsAndOverriddenClasses() {
 			AnnotationConfigContextLoader.class);
 	}
 
+	/**
+	 * @since 5.2
+	 */
+	@Test
+	public void buildMergedConfigForNestedTestClassWithInheritedConfig() {
+		Class<?> testClass = OuterTestCase.NestedTestCaseWithInheritedConfig.class;
+		Class<?>[] expectedClasses = array(FooConfig.class);
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.2
+	 */
+	@Ignore("WIP: not yet working")
+	@Test
+	public void buildMergedConfigForNestedTestClassWithMergedInheritedConfig() {
+		Class<?> testClass = OuterTestCase.NestedTestCaseWithMergedInheritedConfig.class;
+		Class<?>[] expectedClasses = array(FooConfig.class, BarConfig.class);
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.2
+	 */
+	@Test
+	public void buildMergedConfigForNestedTestClassWithOverriddenConfig() {
+		Class<?> testClass = OuterTestCase.NestedTestCaseWithOverriddenConfig.class;
+		Class<?>[] expectedClasses = array(BarConfig.class);
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			DelegatingSmartContextLoader.class);
+	}
+
 
 	@ContextConfiguration
 	@Retention(RetentionPolicy.RUNTIME)
