diff --git a/spring-test/src/main/java/org/springframework/test/context/support/ActiveProfilesUtils.java b/spring-test/src/main/java/org/springframework/test/context/support/ActiveProfilesUtils.java
index f56d935f16a..06ca7d84739 100644
--- a/spring-test/src/main/java/org/springframework/test/context/support/ActiveProfilesUtils.java
+++ b/spring-test/src/main/java/org/springframework/test/context/support/ActiveProfilesUtils.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2017 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -28,12 +28,14 @@
 import org.springframework.beans.BeanUtils;
 import org.springframework.test.context.ActiveProfiles;
 import org.springframework.test.context.ActiveProfilesResolver;
-import org.springframework.test.util.MetaAnnotationUtils;
 import org.springframework.test.util.MetaAnnotationUtils.AnnotationDescriptor;
 import org.springframework.util.Assert;
 import org.springframework.util.ObjectUtils;
 import org.springframework.util.StringUtils;
 
+import static org.springframework.test.util.MetaAnnotationUtils.findAnnotationDescriptor;
+import static org.springframework.test.util.MetaAnnotationUtils.searchEnclosingClass;
+
 /**
  * Utility methods for working with {@link ActiveProfiles @ActiveProfiles} and
  * {@link ActiveProfilesResolver ActiveProfilesResolvers}.
@@ -73,8 +75,7 @@
 		final List<String[]> profileArrays = new ArrayList<>();
 
 		Class<ActiveProfiles> annotationType = ActiveProfiles.class;
-		AnnotationDescriptor<ActiveProfiles> descriptor =
-				MetaAnnotationUtils.findAnnotationDescriptor(testClass, annotationType);
+		AnnotationDescriptor<ActiveProfiles> descriptor = findAnnotationDescriptor(testClass, annotationType);
 		if (descriptor == null && logger.isDebugEnabled()) {
 			logger.debug(String.format(
 					"Could not find an 'annotation declaring class' for annotation type [%s] and class [%s]",
@@ -112,8 +113,18 @@
 				profileArrays.add(profiles);
 			}
 
-			descriptor = (annotation.inheritProfiles() ? MetaAnnotationUtils.findAnnotationDescriptor(
-					rootDeclaringClass.getSuperclass(), annotationType) : null);
+			if (annotation.inheritProfiles()) {
+				// Declared on a superclass?
+				descriptor = findAnnotationDescriptor(rootDeclaringClass.getSuperclass(), annotationType);
+
+				// Declared on an enclosing class of an inner class?
+				if (descriptor == null && searchEnclosingClass(rootDeclaringClass)) {
+					descriptor = findAnnotationDescriptor(rootDeclaringClass.getEnclosingClass(), annotationType);
+				}
+			}
+			else {
+				descriptor = null;
+			}
 		}
 
 		// Reverse the list so that we can traverse "down" the hierarchy.
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ActiveProfilesNestedTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ActiveProfilesNestedTests.java
new file mode 100644
index 00000000000..463e3eb1645
--- /dev/null
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ActiveProfilesNestedTests.java
@@ -0,0 +1,195 @@
+/*
+ * Copyright 2002-2020 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.test.context.junit.jupiter.nested;
+
+import java.util.List;
+
+import org.junit.jupiter.api.Disabled;
+import org.junit.jupiter.api.Nested;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.beans.factory.annotation.Autowired;
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.context.annotation.Profile;
+import org.springframework.test.context.ActiveProfiles;
+import org.springframework.test.context.ContextConfiguration;
+import org.springframework.test.context.NestedTestConfiguration;
+import org.springframework.test.context.junit.jupiter.SpringExtension;
+import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
+import org.springframework.test.context.junit.jupiter.nested.ActiveProfilesNestedTests.Config1;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.INHERIT;
+import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.OVERRIDE;
+
+/**
+ * Integration tests that verify support for {@code @Nested} test classes using
+ * {@link ActiveProfiles @ActiveProfiles} in conjunction with the
+ * {@link SpringExtension} in a JUnit Jupiter environment.
+ *
+ * @author Sam Brannen
+ * @since 5.3
+ */
+@SpringJUnitConfig(Config1.class)
+@ActiveProfiles("1")
+class ActiveProfilesNestedTests {
+
+	@Autowired
+	List<String> strings;
+
+
+	@Test
+	void test() {
+		assertThat(this.strings).containsExactly("X", "A1");
+	}
+
+
+	@Nested
+	@NestedTestConfiguration(INHERIT)
+	class InheritedConfigTests {
+
+		@Autowired
+		List<String> localStrings;
+
+
+		@Test
+		void test() {
+			assertThat(strings).containsExactly("X", "A1");
+			assertThat(this.localStrings).containsExactly("X", "A1");
+		}
+	}
+
+	@Nested
+	@SpringJUnitConfig(Config2.class)
+	@ActiveProfiles("2")
+	class ConfigOverriddenByDefaultTests {
+
+		@Autowired
+		List<String> localStrings;
+
+
+		@Test
+		void test() {
+			assertThat(strings).containsExactly("X", "A1");
+			assertThat(this.localStrings).containsExactly("Y", "A2");
+		}
+	}
+
+	@Nested
+	@NestedTestConfiguration(INHERIT)
+	@ContextConfiguration(classes = Config2.class)
+	@ActiveProfiles("2")
+	class InheritedAndExtendedConfigTests {
+
+		@Autowired
+		List<String> localStrings;
+
+
+		@Test
+		void test() {
+			assertThat(strings).containsExactly("X", "A1");
+			assertThat(this.localStrings).containsExactly("X", "A1", "Y", "A2");
+		}
+
+
+		@Nested
+		@NestedTestConfiguration(OVERRIDE)
+		@SpringJUnitConfig({ Config2.class, Config3.class })
+		@ActiveProfiles("3")
+		class DoubleNestedWithOverriddenConfigTests {
+
+			@Autowired
+			List<String> localStrings;
+
+
+			@Test
+			void test() {
+				assertThat(strings).containsExactly("X", "A1");
+				assertThat(this.localStrings).containsExactly("Y", "Z", "A3");
+			}
+
+
+			@Nested
+			@NestedTestConfiguration(INHERIT)
+			@ActiveProfiles(profiles = "2", inheritProfiles = false)
+			class TripleNestedWithInheritedConfigButOverriddenProfilesTests {
+
+				@Autowired
+				List<String> localStrings;
+
+
+				@Test
+				@Disabled("not currently working")
+				void test() {
+					assertThat(strings).containsExactly("X", "A1");
+					// TODO Figure out how "X" from Config1 is included
+					assertThat(this.localStrings).containsExactly("Y", "A2", "Z");
+				}
+			}
+		}
+
+	}
+
+	// -------------------------------------------------------------------------
+
+	@Configuration
+	static class Config1 {
+
+		@Bean
+		String x() {
+			return "X";
+		}
+
+		@Bean
+		@Profile("1")
+		String a1() {
+			return "A1";
+		}
+	}
+
+	@Configuration
+	static class Config2 {
+
+		@Bean
+		String y() {
+			return "Y";
+		}
+
+		@Bean
+		@Profile("2")
+		String a2() {
+			return "A2";
+		}
+	}
+
+	@Configuration
+	static class Config3 {
+
+		@Bean
+		String z() {
+			return "Z";
+		}
+
+		@Bean
+		@Profile("3")
+		String a3() {
+			return "A3";
+		}
+	}
+
+}
