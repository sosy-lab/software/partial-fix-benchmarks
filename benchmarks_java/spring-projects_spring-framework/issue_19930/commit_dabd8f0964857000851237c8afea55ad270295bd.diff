diff --git a/spring-test/src/main/java/org/springframework/test/context/BootstrapUtils.java b/spring-test/src/main/java/org/springframework/test/context/BootstrapUtils.java
index 8292a3cd99a..1f6c997232e 100644
--- a/spring-test/src/main/java/org/springframework/test/context/BootstrapUtils.java
+++ b/spring-test/src/main/java/org/springframework/test/context/BootstrapUtils.java
@@ -24,8 +24,11 @@
 
 import org.springframework.beans.BeanUtils;
 import org.springframework.core.annotation.AnnotatedElementUtils;
-import org.springframework.core.annotation.AnnotationAttributes;
+import org.springframework.core.annotation.MergedAnnotations;
+import org.springframework.core.annotation.MergedAnnotations.SearchStrategy;
+import org.springframework.core.annotation.RepeatableContainers;
 import org.springframework.lang.Nullable;
+import org.springframework.test.util.MetaAnnotationUtils;
 import org.springframework.util.ClassUtils;
 
 /**
@@ -169,10 +172,11 @@ static TestContextBootstrapper resolveTestContextBootstrapper(BootstrapContext b
 	}
 
 	private static Class<?> resolveDefaultTestContextBootstrapper(Class<?> testClass) throws Exception {
+		SearchStrategy searchStrategy = MetaAnnotationUtils.lookUpSearchStrategy(testClass);
+		boolean webApp = MergedAnnotations.from(testClass, searchStrategy, RepeatableContainers.none())
+				.isPresent(WEB_APP_CONFIGURATION_ANNOTATION_CLASS_NAME);
 		ClassLoader classLoader = BootstrapUtils.class.getClassLoader();
-		AnnotationAttributes attributes = AnnotatedElementUtils.findMergedAnnotationAttributes(testClass,
-			WEB_APP_CONFIGURATION_ANNOTATION_CLASS_NAME, false, false);
-		if (attributes != null) {
+		if (webApp) {
 			return ClassUtils.forName(DEFAULT_WEB_TEST_CONTEXT_BOOTSTRAPPER_CLASS_NAME, classLoader);
 		}
 		return ClassUtils.forName(DEFAULT_TEST_CONTEXT_BOOTSTRAPPER_CLASS_NAME, classLoader);
diff --git a/spring-test/src/main/java/org/springframework/test/context/web/WebTestContextBootstrapper.java b/spring-test/src/main/java/org/springframework/test/context/web/WebTestContextBootstrapper.java
index 04917c90b7c..c898143868e 100644
--- a/spring-test/src/main/java/org/springframework/test/context/web/WebTestContextBootstrapper.java
+++ b/spring-test/src/main/java/org/springframework/test/context/web/WebTestContextBootstrapper.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2016 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -16,11 +16,15 @@
 
 package org.springframework.test.context.web;
 
-import org.springframework.core.annotation.AnnotatedElementUtils;
+import org.springframework.core.annotation.MergedAnnotation;
+import org.springframework.core.annotation.MergedAnnotations;
+import org.springframework.core.annotation.MergedAnnotations.SearchStrategy;
+import org.springframework.core.annotation.RepeatableContainers;
 import org.springframework.test.context.ContextLoader;
 import org.springframework.test.context.MergedContextConfiguration;
 import org.springframework.test.context.TestContextBootstrapper;
 import org.springframework.test.context.support.DefaultTestContextBootstrapper;
+import org.springframework.test.util.MetaAnnotationUtils;
 
 /**
  * Web-specific implementation of the {@link TestContextBootstrapper} SPI.
@@ -45,7 +49,10 @@
 	 */
 	@Override
 	protected Class<? extends ContextLoader> getDefaultContextLoaderClass(Class<?> testClass) {
-		if (AnnotatedElementUtils.hasAnnotation(testClass, WebAppConfiguration.class)) {
+		SearchStrategy searchStrategy = MetaAnnotationUtils.lookUpSearchStrategy(testClass);
+		boolean webApp = MergedAnnotations.from(testClass, searchStrategy, RepeatableContainers.none())
+				.isPresent(WebAppConfiguration.class);
+		if (webApp) {
 			return WebDelegatingSmartContextLoader.class;
 		}
 		else {
@@ -61,10 +68,12 @@
 	 */
 	@Override
 	protected MergedContextConfiguration processMergedContextConfiguration(MergedContextConfiguration mergedConfig) {
-		WebAppConfiguration webAppConfiguration =
-				AnnotatedElementUtils.findMergedAnnotation(mergedConfig.getTestClass(), WebAppConfiguration.class);
-		if (webAppConfiguration != null) {
-			return new WebMergedContextConfiguration(mergedConfig, webAppConfiguration.value());
+		Class<?> testClass = mergedConfig.getTestClass();
+		SearchStrategy searchStrategy = MetaAnnotationUtils.lookUpSearchStrategy(testClass);
+		MergedAnnotation<WebAppConfiguration> mergedAnnotation = MergedAnnotations.from(testClass, searchStrategy,
+				RepeatableContainers.none()).get(WebAppConfiguration.class);
+		if (mergedAnnotation.isPresent()) {
+			return new WebMergedContextConfiguration(mergedConfig, mergedAnnotation.getString("value"));
 		}
 		else {
 			return mergedConfig;
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ContextConfigurationNestedTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ContextConfigurationNestedTests.java
index e9de6063e77..80770ba7e40 100644
--- a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ContextConfigurationNestedTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/ContextConfigurationNestedTests.java
@@ -25,7 +25,6 @@
 import org.springframework.context.annotation.Configuration;
 import org.springframework.test.context.ContextConfiguration;
 import org.springframework.test.context.NestedTestConfiguration;
-import org.springframework.test.context.junit.SpringJUnitJupiterTestSuite;
 import org.springframework.test.context.junit.jupiter.SpringExtension;
 import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
 import org.springframework.test.context.junit.jupiter.nested.ContextConfigurationNestedTests.TopLevelConfig;
@@ -35,11 +34,9 @@
 import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.OVERRIDE;
 
 /**
- * Integration tests that verify support for {@code @Nested} test classes
- * in conjunction with the {@link SpringExtension} in a JUnit Jupiter environment.
- *
- * <p>To run these tests in an IDE that does not have built-in support for the JUnit
- * Platform, simply run {@link SpringJUnitJupiterTestSuite} as a JUnit 4 test.
+ * Integration tests that verify support for {@code @Nested} test classes using
+ * {@link ContextConfiguration @ContextConfiguration} in conjunction with the
+ * {@link SpringExtension} in a JUnit Jupiter environment.
  *
  * @author Sam Brannen
  * @since 5.0
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/WebAppConfigurationNestedTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/WebAppConfigurationNestedTests.java
new file mode 100644
index 00000000000..5073c1a60e2
--- /dev/null
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/WebAppConfigurationNestedTests.java
@@ -0,0 +1,128 @@
+/*
+ * Copyright 2002-2020 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.test.context.junit.jupiter.nested;
+
+import org.junit.jupiter.api.Nested;
+import org.junit.jupiter.api.Test;
+
+import org.springframework.context.ApplicationContext;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.test.context.NestedTestConfiguration;
+import org.springframework.test.context.junit.jupiter.SpringExtension;
+import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
+import org.springframework.test.context.junit.jupiter.nested.WebAppConfigurationNestedTests.Config;
+import org.springframework.test.context.junit.jupiter.web.SpringJUnitWebConfig;
+import org.springframework.test.context.web.WebAppConfiguration;
+import org.springframework.web.context.WebApplicationContext;
+
+import static org.assertj.core.api.Assertions.assertThat;
+import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.INHERIT;
+import static org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration.OVERRIDE;
+
+/**
+ * Integration tests that verify support for {@code @Nested} test classes using
+ * {@link WebAppConfiguration @WebAppConfiguration} in conjunction with the
+ * {@link SpringExtension} in a JUnit Jupiter environment.
+ *
+ * @author Sam Brannen
+ * @since 5.0
+ * @see ConstructorInjectionNestedTests
+ * @see org.springframework.test.context.junit4.nested.NestedTestsWithSpringRulesTests
+ */
+@SpringJUnitConfig(Config.class)
+class WebAppConfigurationNestedTests {
+
+	@Test
+	void test(ApplicationContext context) {
+		assertThat(context).isNotInstanceOf(WebApplicationContext.class);
+	}
+
+
+	@Nested
+	@SpringJUnitConfig(Config.class)
+	class ConfigOverriddenByDefaultTests {
+
+		@Test
+		void test(ApplicationContext context) {
+			assertThat(context).isNotInstanceOf(WebApplicationContext.class);
+		}
+	}
+
+	@Nested
+	@SpringJUnitWebConfig(Config.class)
+	class ConfigOverriddenByDefaultWebTests {
+
+		@Test
+		void test(ApplicationContext context) {
+			assertThat(context).isInstanceOf(WebApplicationContext.class);
+		}
+	}
+
+	@Nested
+	@NestedTestConfiguration(INHERIT)
+	class NestedWithInheritedConfigTests {
+
+		@Test
+		void test(ApplicationContext context) {
+			assertThat(context).isNotInstanceOf(WebApplicationContext.class);
+		}
+
+
+		@Nested
+		@NestedTestConfiguration(OVERRIDE)
+		@SpringJUnitWebConfig(Config.class)
+		class DoubleNestedWithOverriddenConfigWebTests {
+
+			@Test
+			void test(ApplicationContext context) {
+				assertThat(context).isInstanceOf(WebApplicationContext.class);
+			}
+
+
+			@Nested
+			@NestedTestConfiguration(INHERIT)
+			class TripleNestedWithInheritedConfigWebTests {
+
+				@Test
+				void test(ApplicationContext context) {
+					assertThat(context).isInstanceOf(WebApplicationContext.class);
+				}
+			}
+		}
+
+		@Nested
+		@NestedTestConfiguration(INHERIT)
+		class DoubleNestedWithInheritedConfigAndTestInterfaceTests implements TestInterface {
+
+			@Test
+			void test(ApplicationContext context) {
+				assertThat(context).isInstanceOf(WebApplicationContext.class);
+			}
+		}
+	}
+
+	// -------------------------------------------------------------------------
+
+	@Configuration
+	static class Config {
+	}
+
+	@WebAppConfiguration
+	interface TestInterface {
+	}
+
+}
