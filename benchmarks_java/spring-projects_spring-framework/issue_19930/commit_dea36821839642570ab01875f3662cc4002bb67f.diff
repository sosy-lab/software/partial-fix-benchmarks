diff --git a/spring-test/src/main/java/org/springframework/test/context/NestedTestConfiguration.java b/spring-test/src/main/java/org/springframework/test/context/NestedTestConfiguration.java
new file mode 100644
index 00000000000..84989015420
--- /dev/null
+++ b/spring-test/src/main/java/org/springframework/test/context/NestedTestConfiguration.java
@@ -0,0 +1,89 @@
+/*
+ * Copyright 2002-2020 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.test.context;
+
+import java.lang.annotation.Documented;
+import java.lang.annotation.ElementType;
+import java.lang.annotation.Inherited;
+import java.lang.annotation.Retention;
+import java.lang.annotation.RetentionPolicy;
+import java.lang.annotation.Target;
+
+/**
+ * {@code @NestedTestConfiguration} is a type-level annotation that is used to
+ * configure how Spring test configuration annotations are processed within
+ * enclosing class hierarchies (i.e., for <em>inner</em> test classes).
+ *
+ * <p>If {@code @NestedTestConfiguration} is not <em>present</em> or
+ * <em>meta-present</em> on a test class, configuration from the test class will
+ * not propagate to inner test classes (see {@link EnclosingConfiguration#OVERRIDE}).
+ * Consequently, inner test classes will have to declare their own Spring test
+ * configuration annotations. If you wish for an inner test class to inherit
+ * configuration from its enclosing class, annotate the enclosing class with
+ * {@code @NestedTestConfiguration(EnclosingConfiguration.INHERIT)}.
+ *
+ * <p>This annotation may be used as a <em>meta-annotation</em> to create custom
+ * <em>composed annotations</em>.
+ *
+ * <p>As of Spring Framework 5.3, the use of this annotation typically only makes
+ * sense in conjunction with {@link org.junit.jupiter.api.Nested @Nested} test
+ * classes in JUnit Jupiter.
+ *
+ * @author Sam Brannen
+ * @since 5.3
+ * @see EnclosingConfiguration#INHERIT
+ * @see EnclosingConfiguration#OVERRIDE
+ * @see ContextConfiguration @ContextConfiguration
+ * @see ContextHierarchy @ContextHierarchy
+ * @see ActiveProfiles @ActiveProfiles
+ * @see TestPropertySource @TestPropertySource
+ */
+@Target({ElementType.TYPE, ElementType.METHOD})
+@Retention(RetentionPolicy.RUNTIME)
+@Documented
+@Inherited
+public @interface NestedTestConfiguration {
+
+	/**
+	 * Configures the {@link EnclosingConfiguration} mode.
+	 */
+	EnclosingConfiguration value();
+
+
+	/**
+	 * Enumeration of <em>modes</em> that dictate how test configuration from
+	 * enclosing classes is processed for inner test classes.
+	 */
+	enum EnclosingConfiguration {
+
+		/**
+		 * Indicates that test configuration for an inner test class should be
+		 * <em>inherited</em> from its {@linkplain Class#getEnclosingClass()
+		 * enclosing class}, as if the enclosing class were a superclass.
+		 */
+		INHERIT,
+
+		/**
+		 * Indicates that test configuration for an inner test class should
+		 * <em>override</em> configuration from its
+		 * {@linkplain Class#getEnclosingClass() enclosing class}.
+		 */
+		OVERRIDE
+
+	}
+
+}
diff --git a/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java b/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java
index 6313af83baa..a84a4687369 100644
--- a/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java
+++ b/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java
@@ -16,7 +16,6 @@
 
 package org.springframework.test.context.support;
 
-import java.lang.reflect.Modifier;
 import java.util.ArrayList;
 import java.util.HashSet;
 import java.util.LinkedHashMap;
@@ -34,6 +33,8 @@
 import org.springframework.test.context.ContextConfiguration;
 import org.springframework.test.context.ContextConfigurationAttributes;
 import org.springframework.test.context.ContextHierarchy;
+import org.springframework.test.context.NestedTestConfiguration;
+import org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration;
 import org.springframework.test.context.SmartContextLoader;
 import org.springframework.test.util.MetaAnnotationUtils.UntypedAnnotationDescriptor;
 import org.springframework.util.Assert;
@@ -240,8 +241,7 @@ else if (contextHierarchyDeclaredLocally) {
 		Assert.notNull(testClass, "Class must not be null");
 
 		Class<ContextConfiguration> annotationType = ContextConfiguration.class;
-		MergedAnnotations mergedAnnotations = MergedAnnotations.from(testClass,
-			SearchStrategy.TYPE_HIERARCHY_AND_ENCLOSING_CLASSES);
+		MergedAnnotations mergedAnnotations = MergedAnnotations.from(testClass, getSearchStrategy(testClass));
 		Assert.isTrue(mergedAnnotations.isPresent(annotationType), () -> String.format(
 			"Could not find an 'annotation declaring class' for annotation type [%s] and class [%s]",
 			annotationType.getName(), testClass.getName()));
@@ -252,6 +252,18 @@ else if (contextHierarchyDeclaredLocally) {
 		return attributesList;
 	}
 
+	private static SearchStrategy getSearchStrategy(Class<?> testClass) {
+		EnclosingConfiguration enclosingConfiguration =
+			MergedAnnotations.from(testClass, SearchStrategy.TYPE_HIERARCHY_AND_ENCLOSING_CLASSES)
+				.stream(NestedTestConfiguration.class)
+				.map(mergedAnnotation -> mergedAnnotation.getEnum("value", EnclosingConfiguration.class))
+				.findFirst()
+				.orElse(EnclosingConfiguration.OVERRIDE);
+		return (enclosingConfiguration == EnclosingConfiguration.INHERIT ?
+				SearchStrategy.TYPE_HIERARCHY_AND_ENCLOSING_CLASSES :
+				SearchStrategy.TYPE_HIERARCHY);
+	}
+
 	private static void resolveContextConfigurationAttributes(List<ContextConfigurationAttributes> attributesList,
 			MergedAnnotation<ContextConfiguration> mergedAnnotation) {
 
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
index afde4d286ab..1ef7fcb7417 100644
--- a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
@@ -22,6 +22,8 @@
 import org.springframework.beans.factory.annotation.Autowired;
 import org.springframework.context.annotation.Bean;
 import org.springframework.context.annotation.Configuration;
+import org.springframework.test.context.NestedTestConfiguration;
+import org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration;
 import org.springframework.test.context.junit.SpringJUnitJupiterTestSuite;
 import org.springframework.test.context.junit.jupiter.SpringExtension;
 import org.springframework.test.context.junit.jupiter.SpringJUnitConfig;
@@ -77,6 +79,7 @@ void nestedTest() throws Exception {
 
 	@Nested
 	// @SpringJUnitConfig(NestedConfig.class)
+	@NestedTestConfiguration(EnclosingConfiguration.INHERIT)
 	class NestedTestCaseWithInheritedConfigTests {
 
 		@Autowired
diff --git a/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java b/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
index 1f4982fa5d9..ffaa764036d 100644
--- a/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
@@ -36,6 +36,8 @@
 import org.springframework.test.context.ContextHierarchy;
 import org.springframework.test.context.ContextLoader;
 import org.springframework.test.context.MergedContextConfiguration;
+import org.springframework.test.context.NestedTestConfiguration;
+import org.springframework.test.context.NestedTestConfiguration.EnclosingConfiguration;
 import org.springframework.test.context.TestContextBootstrapper;
 import org.springframework.test.context.web.WebAppConfiguration;
 
@@ -233,6 +235,7 @@ void assertMergedConfig(
 
 	@ContextConfiguration(classes = FooConfig.class, loader = AnnotationConfigContextLoader.class)
 	@ActiveProfiles("foo")
+	@NestedTestConfiguration(EnclosingConfiguration.INHERIT)
 	static class OuterTestCase {
 
 		class NestedTestCaseWithInheritedConfig {
@@ -254,6 +257,7 @@ void assertMergedConfig(
 		@ContextConfiguration(classes = FooConfig.class, loader = AnnotationConfigContextLoader.class, name = "foo"), //
 		@ContextConfiguration(classes = BarConfig.class, loader = AnnotationConfigContextLoader.class)//
 	})
+	@NestedTestConfiguration(EnclosingConfiguration.INHERIT)
 	static class ContextHierarchyOuterTestCase {
 
 		class NestedTestCaseWithInheritedConfig {
