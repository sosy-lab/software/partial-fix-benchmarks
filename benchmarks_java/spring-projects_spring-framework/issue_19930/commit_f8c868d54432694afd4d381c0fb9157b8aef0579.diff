diff --git a/spring-test/src/main/java/org/springframework/test/context/support/AbstractTestContextBootstrapper.java b/spring-test/src/main/java/org/springframework/test/context/support/AbstractTestContextBootstrapper.java
index 040578d7eb3..8027595b8a6 100644
--- a/spring-test/src/main/java/org/springframework/test/context/support/AbstractTestContextBootstrapper.java
+++ b/spring-test/src/main/java/org/springframework/test/context/support/AbstractTestContextBootstrapper.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2018 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -31,7 +31,6 @@
 import org.springframework.beans.BeanInstantiationException;
 import org.springframework.beans.BeanUtils;
 import org.springframework.core.annotation.AnnotationAwareOrderComparator;
-import org.springframework.core.annotation.AnnotationUtils;
 import org.springframework.core.io.support.SpringFactoriesLoader;
 import org.springframework.lang.Nullable;
 import org.springframework.test.context.BootstrapContext;
@@ -265,7 +264,7 @@ public final MergedContextConfiguration buildMergedContextConfiguration() {
 			return buildDefaultMergedContextConfiguration(testClass, cacheAwareContextLoaderDelegate);
 		}
 
-		if (AnnotationUtils.findAnnotation(testClass, ContextHierarchy.class) != null) {
+		if (MetaAnnotationUtils.findAnnotationDescriptor(testClass, ContextHierarchy.class) != null) {
 			Map<String, List<ContextConfigurationAttributes>> hierarchyMap =
 					ContextLoaderUtils.buildContextHierarchyMap(testClass);
 			MergedContextConfiguration parentConfig = null;
diff --git a/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java b/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java
index 70d70d9f0ca..79e7d3be4d1 100644
--- a/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java
+++ b/spring-test/src/main/java/org/springframework/test/context/support/ContextLoaderUtils.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2018 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -34,6 +34,7 @@
 import org.springframework.test.util.MetaAnnotationUtils.AnnotationDescriptor;
 import org.springframework.test.util.MetaAnnotationUtils.UntypedAnnotationDescriptor;
 import org.springframework.util.Assert;
+import org.springframework.util.ClassUtils;
 import org.springframework.util.StringUtils;
 
 import static org.springframework.core.annotation.AnnotationUtils.getAnnotation;
@@ -237,21 +238,33 @@ else if (contextHierarchyDeclaredLocally) {
 	static List<ContextConfigurationAttributes> resolveContextConfigurationAttributes(Class<?> testClass) {
 		Assert.notNull(testClass, "Class must not be null");
 
-		List<ContextConfigurationAttributes> attributesList = new ArrayList<>();
 		Class<ContextConfiguration> annotationType = ContextConfiguration.class;
-
 		AnnotationDescriptor<ContextConfiguration> descriptor = findAnnotationDescriptor(testClass, annotationType);
 		Assert.notNull(descriptor, () -> String.format(
 					"Could not find an 'annotation declaring class' for annotation type [%s] and class [%s]",
 					annotationType.getName(), testClass.getName()));
 
-		while (descriptor != null) {
+		List<ContextConfigurationAttributes> attributesList = new ArrayList<>();
+		resolveContextConfigurationAttributes(attributesList, descriptor);
+		return attributesList;
+	}
+
+	private static void resolveContextConfigurationAttributes(List<ContextConfigurationAttributes> attributesList,
+			AnnotationDescriptor<ContextConfiguration> descriptor) {
+
+		if (descriptor != null) {
+			Class<?> rootDeclaringClass = descriptor.getRootDeclaringClass();
 			convertContextConfigToConfigAttributesAndAddToList(descriptor.synthesizeAnnotation(),
-					descriptor.getRootDeclaringClass(), attributesList);
-			descriptor = findAnnotationDescriptor(descriptor.getRootDeclaringClass().getSuperclass(), annotationType);
+					rootDeclaringClass, attributesList);
+			// Search on superclass
+			resolveContextConfigurationAttributes(attributesList,
+				findAnnotationDescriptor(rootDeclaringClass.getSuperclass(), ContextConfiguration.class));
+			// Search on enclosing class for "nested test class"
+			if (ClassUtils.isInnerClass(rootDeclaringClass)) {
+				resolveContextConfigurationAttributes(attributesList,
+					findAnnotationDescriptor(rootDeclaringClass.getDeclaringClass(), ContextConfiguration.class));
+			}
 		}
-
-		return attributesList;
 	}
 
 	/**
diff --git a/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java b/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java
index e1317816b89..f341da31aa9 100644
--- a/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java
+++ b/spring-test/src/main/java/org/springframework/test/util/MetaAnnotationUtils.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2019 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -26,6 +26,7 @@
 import org.springframework.core.style.ToStringCreator;
 import org.springframework.lang.Nullable;
 import org.springframework.util.Assert;
+import org.springframework.util.ClassUtils;
 import org.springframework.util.ObjectUtils;
 
 /**
@@ -123,7 +124,7 @@
 			}
 		}
 
-		// Declared on interface?
+		// Declared on an interface?
 		for (Class<?> ifc : clazz.getInterfaces()) {
 			AnnotationDescriptor<T> descriptor = findAnnotationDescriptor(ifc, visited, annotationType);
 			if (descriptor != null) {
@@ -133,7 +134,20 @@
 		}
 
 		// Declared on a superclass?
-		return findAnnotationDescriptor(clazz.getSuperclass(), visited, annotationType);
+		AnnotationDescriptor<T> descriptor = findAnnotationDescriptor(clazz.getSuperclass(), visited, annotationType);
+		if (descriptor != null) {
+			return descriptor;
+		}
+
+		// Declared on an enclosing class of an inner class?
+		if (ClassUtils.isInnerClass(clazz)) {
+			descriptor = findAnnotationDescriptor(clazz.getDeclaringClass(), visited, annotationType);
+			if (descriptor != null) {
+				return descriptor;
+			}
+		}
+
+		return null;
 	}
 
 	/**
@@ -212,7 +226,7 @@ private static UntypedAnnotationDescriptor findAnnotationDescriptorForTypes(@Nul
 			}
 		}
 
-		// Declared on interface?
+		// Declared on an interface?
 		for (Class<?> ifc : clazz.getInterfaces()) {
 			UntypedAnnotationDescriptor descriptor = findAnnotationDescriptorForTypes(ifc, visited, annotationTypes);
 			if (descriptor != null) {
@@ -222,7 +236,20 @@ private static UntypedAnnotationDescriptor findAnnotationDescriptorForTypes(@Nul
 		}
 
 		// Declared on a superclass?
-		return findAnnotationDescriptorForTypes(clazz.getSuperclass(), visited, annotationTypes);
+		UntypedAnnotationDescriptor descriptor = findAnnotationDescriptorForTypes(clazz.getSuperclass(), visited, annotationTypes);
+		if (descriptor != null) {
+			return descriptor;
+		}
+
+		// Declared on an enclosing class of an inner class?
+		if (ClassUtils.isInnerClass(clazz)) {
+			descriptor = findAnnotationDescriptorForTypes(clazz.getDeclaringClass(), visited, annotationTypes);
+			if (descriptor != null) {
+				return descriptor;
+			}
+		}
+
+		return null;
 	}
 
 	private static void assertNonEmptyAnnotationTypeArray(Class<?>[] annotationTypes, String message) {
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
index b6a8c554fd3..afde4d286ab 100644
--- a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSpringAndJUnitJupiterTests.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2019 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -44,13 +44,16 @@
 @SpringJUnitConfig(TopLevelConfig.class)
 class NestedTestsWithSpringAndJUnitJupiterTests {
 
+	private static final String FOO = "foo";
+	private static final String BAR = "bar";
+
 	@Autowired
 	String foo;
 
 
 	@Test
 	void topLevelTest() {
-		assertThat(foo).isEqualTo("foo");
+		assertThat(foo).isEqualTo(FOO);
 	}
 
 
@@ -67,8 +70,26 @@ void nestedTest() throws Exception {
 			// In contrast to nested test classes running in JUnit 4, the foo
 			// field in the outer instance should have been injected from the
 			// test ApplicationContext for the outer instance.
-			assertThat(foo).isEqualTo("foo");
-			assertThat(bar).isEqualTo("bar");
+			assertThat(foo).isEqualTo(FOO);
+			assertThat(bar).isEqualTo(BAR);
+		}
+	}
+
+	@Nested
+	// @SpringJUnitConfig(NestedConfig.class)
+	class NestedTestCaseWithInheritedConfigTests {
+
+		@Autowired
+		String bar;
+
+
+		@Test
+		void nestedTest() throws Exception {
+			// Since the configuration is inherited, the foo field in the outer instance
+			// and the bar field in the inner instance should both have been injected
+			// from the test ApplicationContext for the outer instance.
+			assertThat(foo).isEqualTo(FOO);
+			assertThat(bar).isEqualTo(FOO);
 		}
 	}
 
@@ -79,7 +100,7 @@ void nestedTest() throws Exception {
 
 		@Bean
 		String foo() {
-			return "foo";
+			return FOO;
 		}
 	}
 
@@ -88,7 +109,7 @@ String foo() {
 
 		@Bean
 		String bar() {
-			return "bar";
+			return BAR;
 		}
 	}
 
diff --git a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSqlScriptsAndJUnitJupiterTests.java b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSqlScriptsAndJUnitJupiterTests.java
index d0174f06e54..24b6ac5c6c7 100644
--- a/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSqlScriptsAndJUnitJupiterTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/junit/jupiter/nested/NestedTestsWithSqlScriptsAndJUnitJupiterTests.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2019 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -67,6 +67,10 @@ private int countRowsInTable(String tableName) {
 	}
 
 	@Nested
+	// NOTE: the following @SpringJUnitConfig declaration must NOT be removed.
+	// This was added before the TestContext framework looked up configuration
+	// on enclosing classes for @Nested test classes. As such, this serves as a
+	// regression test and cannot be changed.
 	@SpringJUnitConfig(PopulatedSchemaDatabaseConfig.class)
 	@Transactional
 	class NestedTests {
diff --git a/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java b/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
index f4c843f90e4..1f4982fa5d9 100644
--- a/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/support/AbstractContextConfigurationUtilsTests.java
@@ -33,12 +33,13 @@
 import org.springframework.test.context.CacheAwareContextLoaderDelegate;
 import org.springframework.test.context.ContextConfiguration;
 import org.springframework.test.context.ContextConfigurationAttributes;
+import org.springframework.test.context.ContextHierarchy;
 import org.springframework.test.context.ContextLoader;
 import org.springframework.test.context.MergedContextConfiguration;
 import org.springframework.test.context.TestContextBootstrapper;
 import org.springframework.test.context.web.WebAppConfiguration;
 
-import static org.assertj.core.api.Assertions.assertThat;
+import static org.assertj.core.api.SoftAssertions.assertSoftly;
 
 /**
  * Abstract base class for tests involving {@link ContextLoaderUtils},
@@ -68,11 +69,13 @@ void assertAttributes(ContextConfigurationAttributes attributes, Class<?> expect
 			String[] expectedLocations, Class<?>[] expectedClasses,
 			Class<? extends ContextLoader> expectedContextLoaderClass, boolean expectedInheritLocations) {
 
-		assertThat(attributes.getDeclaringClass()).as("declaring class").isEqualTo(expectedDeclaringClass);
-		assertThat(attributes.getLocations()).as("locations").isEqualTo(expectedLocations);
-		assertThat(attributes.getClasses()).as("classes").isEqualTo(expectedClasses);
-		assertThat(attributes.isInheritLocations()).as("inherit locations").isEqualTo(expectedInheritLocations);
-		assertThat(attributes.getContextLoaderClass()).as("context loader").isEqualTo(expectedContextLoaderClass);
+		assertSoftly(softly -> {
+			softly.assertThat(attributes.getDeclaringClass()).as("declaring class").isEqualTo(expectedDeclaringClass);
+			softly.assertThat(attributes.getLocations()).as("locations").isEqualTo(expectedLocations);
+			softly.assertThat(attributes.getClasses()).as("classes").isEqualTo(expectedClasses);
+			softly.assertThat(attributes.isInheritLocations()).as("inherit locations").isEqualTo(expectedInheritLocations);
+			softly.assertThat(attributes.getContextLoaderClass()).as("context loader").isEqualTo(expectedContextLoaderClass);
+		});
 	}
 
 	void assertMergedConfig(MergedContextConfiguration mergedConfig, Class<?> expectedTestClass,
@@ -91,21 +94,22 @@ void assertMergedConfig(
 			Set<Class<? extends ApplicationContextInitializer<?>>> expectedInitializerClasses,
 			Class<? extends ContextLoader> expectedContextLoaderClass) {
 
-		assertThat(mergedConfig).isNotNull();
-		assertThat(mergedConfig.getTestClass()).isEqualTo(expectedTestClass);
-		assertThat(mergedConfig.getLocations()).isNotNull();
-		assertThat(mergedConfig.getLocations()).isEqualTo(expectedLocations);
-		assertThat(mergedConfig.getClasses()).isNotNull();
-		assertThat(mergedConfig.getClasses()).isEqualTo(expectedClasses);
-		assertThat(mergedConfig.getActiveProfiles()).isNotNull();
-		if (expectedContextLoaderClass == null) {
-			assertThat(mergedConfig.getContextLoader()).isNull();
-		}
-		else {
-			assertThat(mergedConfig.getContextLoader().getClass()).isEqualTo(expectedContextLoaderClass);
-		}
-		assertThat(mergedConfig.getContextInitializerClasses()).isNotNull();
-		assertThat(mergedConfig.getContextInitializerClasses()).isEqualTo(expectedInitializerClasses);
+		assertSoftly(softly -> {
+			softly.assertThat(mergedConfig).as("merged config").isNotNull();
+			softly.assertThat(mergedConfig.getTestClass()).as("test class").isEqualTo(expectedTestClass);
+			softly.assertThat(mergedConfig.getLocations()).as("locations").containsExactly(expectedLocations);
+			softly.assertThat(mergedConfig.getClasses()).as("classes").containsExactly(expectedClasses);
+			softly.assertThat(mergedConfig.getActiveProfiles()).as("active profiles").isNotNull();
+
+			if (expectedContextLoaderClass == null) {
+				softly.assertThat(mergedConfig.getContextLoader()).as("context loader").isNull();
+			}
+			else {
+				softly.assertThat(mergedConfig.getContextLoader().getClass()).as("context loader").isEqualTo(expectedContextLoaderClass);
+			}
+			softly.assertThat(mergedConfig.getContextInitializerClasses()).as("context initializers").isNotNull();
+			softly.assertThat(mergedConfig.getContextInitializerClasses()).as("context initializers").isEqualTo(expectedInitializerClasses);
+		});
 	}
 
 	@SafeVarargs
@@ -130,6 +134,14 @@ void assertMergedConfig(
 	static class BarConfig {
 	}
 
+	@Configuration
+	static class BazConfig {
+	}
+
+	@Configuration
+	static class QuuxConfig {
+	}
+
 	@ContextConfiguration("/foo.xml")
 	@ActiveProfiles(profiles = "foo")
 	@Retention(RetentionPolicy.RUNTIME)
@@ -219,4 +231,42 @@ void assertMergedConfig(
 	static class PropertiesClassesFoo {
 	}
 
+	@ContextConfiguration(classes = FooConfig.class, loader = AnnotationConfigContextLoader.class)
+	@ActiveProfiles("foo")
+	static class OuterTestCase {
+
+		class NestedTestCaseWithInheritedConfig {
+		}
+
+		@ContextConfiguration(classes = BarConfig.class)
+		@ActiveProfiles("bar")
+		class NestedTestCaseWithMergedInheritedConfig {
+		}
+
+		@ContextConfiguration(classes = BarConfig.class, inheritLocations = false)
+		@ActiveProfiles(profiles = "bar", inheritProfiles = false)
+		class NestedTestCaseWithOverriddenConfig {
+		}
+
+	}
+
+	@ContextHierarchy({ //
+		@ContextConfiguration(classes = FooConfig.class, loader = AnnotationConfigContextLoader.class, name = "foo"), //
+		@ContextConfiguration(classes = BarConfig.class, loader = AnnotationConfigContextLoader.class)//
+	})
+	static class ContextHierarchyOuterTestCase {
+
+		class NestedTestCaseWithInheritedConfig {
+		}
+
+		@ContextConfiguration(classes = BazConfig.class)
+		class NestedTestCaseWithMergedInheritedConfig {
+		}
+
+		@ContextConfiguration(classes = QuuxConfig.class, loader = AnnotationConfigContextLoader.class, name = "foo")
+		class NestedTestCaseWithOverriddenConfig {
+		}
+
+	}
+
 }
diff --git a/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java b/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java
index c235e7f2cac..07defd0c4f9 100644
--- a/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java
+++ b/spring-test/src/test/java/org/springframework/test/context/support/BootstrapTestUtilsMergedConfigTests.java
@@ -21,6 +21,8 @@
 import java.lang.annotation.RetentionPolicy;
 import java.lang.annotation.Target;
 
+import org.assertj.core.api.AssertionsForClassTypes;
+import org.junit.jupiter.api.Disabled;
 import org.junit.jupiter.api.Test;
 
 import org.springframework.test.context.BootstrapTestUtils;
@@ -220,6 +222,113 @@ private void assertMergedConfigForLocationPaths(Class<?> testClass) {
 		assertThat(mergedConfig.getPropertySourceProperties()).isEmpty();
 	}
 
+	/**
+	 * @since 5.2
+	 */
+	@Test
+	public void buildMergedConfigForNestedTestClassWithInheritedConfig() {
+		Class<?> testClass = OuterTestCase.NestedTestCaseWithInheritedConfig.class;
+		Class<?>[] expectedClasses = array(FooConfig.class);
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.3
+	 */
+	@Test
+	public void buildMergedConfigForNestedTestClassWithMergedInheritedConfig() {
+		Class<?> testClass = OuterTestCase.NestedTestCaseWithMergedInheritedConfig.class;
+		Class<?>[] expectedClasses = array(FooConfig.class, BarConfig.class);
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.3
+	 */
+	@Test
+	public void buildMergedConfigForNestedTestClassWithOverriddenConfig() {
+		Class<?> testClass = OuterTestCase.NestedTestCaseWithOverriddenConfig.class;
+		Class<?>[] expectedClasses = array(BarConfig.class);
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.3
+	 */
+	@Test
+	public void buildMergedConfigForContextHierarchy() {
+		Class<?> testClass = ContextHierarchyOuterTestCase.class;
+		Class<?>[] expectedClasses = array(BarConfig.class);
+
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+		assertThat(mergedConfig).as("merged config").isNotNull();
+
+		MergedContextConfiguration parent = mergedConfig.getParent();
+		assertThat(parent).as("parent config").isNotNull();
+		// The following does not work -- at least not in Eclipse.
+		// asssertThat(parent.getClasses())...
+		// So we use AssertionsForClassTypes directly.
+		AssertionsForClassTypes.assertThat(parent.getClasses()).containsExactly(FooConfig.class);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.3
+	 */
+	@Test
+	public void buildMergedConfigForNestedTestClassWithInheritedConfigForContextHierarchy() {
+		Class<?> enclosingTestClass = ContextHierarchyOuterTestCase.class;
+		Class<?> testClass = ContextHierarchyOuterTestCase.NestedTestCaseWithInheritedConfig.class;
+		Class<?>[] expectedClasses = array(BarConfig.class);
+
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+		assertThat(mergedConfig).as("merged config").isNotNull();
+
+		MergedContextConfiguration parent = mergedConfig.getParent();
+		assertThat(parent).as("parent config").isNotNull();
+		// The following does not work -- at least not in Eclipse.
+		// asssertThat(parent.getClasses())...
+		// So we use AssertionsForClassTypes directly.
+		AssertionsForClassTypes.assertThat(parent.getClasses()).containsExactly(FooConfig.class);
+
+		assertMergedConfig(mergedConfig, enclosingTestClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
+	/**
+	 * @since 5.2
+	 */
+	@Test
+	@Disabled("Not yet working")
+	public void buildMergedConfigForNestedTestClassWithMergedInheritedConfigForContextHierarchy() {
+		Class<?> testClass = ContextHierarchyOuterTestCase.NestedTestCaseWithMergedInheritedConfig.class;
+		Class<?>[] expectedClasses = array(BarConfig.class, BazConfig.class);
+
+		MergedContextConfiguration mergedConfig = buildMergedContextConfiguration(testClass);
+		assertThat(mergedConfig).as("merged config").isNotNull();
+
+		MergedContextConfiguration parent = mergedConfig.getParent();
+		assertThat(parent).as("parent config").isNotNull();
+		// The following does not work -- at least not in Eclipse.
+		// asssertThat(parent.getClasses())...
+		// So we use AssertionsForClassTypes directly.
+		AssertionsForClassTypes.assertThat(parent.getClasses()).containsExactly(FooConfig.class);
+
+		assertMergedConfig(mergedConfig, testClass, EMPTY_STRING_ARRAY, expectedClasses,
+			AnnotationConfigContextLoader.class);
+	}
+
 
 	@ContextConfiguration
 	@Retention(RetentionPolicy.RUNTIME)
