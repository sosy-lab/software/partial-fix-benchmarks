diff --git a/spring-context/src/main/java/org/springframework/context/annotation/ConfigurationClassBeanDefinitionReader.java b/spring-context/src/main/java/org/springframework/context/annotation/ConfigurationClassBeanDefinitionReader.java
index 5719ed3eabd..c43653dd0c7 100644
--- a/spring-context/src/main/java/org/springframework/context/annotation/ConfigurationClassBeanDefinitionReader.java
+++ b/spring-context/src/main/java/org/springframework/context/annotation/ConfigurationClassBeanDefinitionReader.java
@@ -382,7 +382,7 @@ private void loadBeanDefinitionsFromImportedResources(
 
 	private void loadBeanDefinitionsFromRegistrars(Map<ImportBeanDefinitionRegistrar, AnnotationMetadata> registrars) {
 		registrars.forEach((registrar, metadata) ->
-				registrar.registerBeanDefinitions(metadata, this.registry));
+				registrar.registerBeanDefinitions(metadata, this.registry, this.importBeanNameGenerator));
 	}
 
 
diff --git a/spring-context/src/main/java/org/springframework/context/annotation/ImportBeanDefinitionRegistrar.java b/spring-context/src/main/java/org/springframework/context/annotation/ImportBeanDefinitionRegistrar.java
index 49af978b0d5..ed5f53df694 100644
--- a/spring-context/src/main/java/org/springframework/context/annotation/ImportBeanDefinitionRegistrar.java
+++ b/spring-context/src/main/java/org/springframework/context/annotation/ImportBeanDefinitionRegistrar.java
@@ -18,6 +18,7 @@
 
 import org.springframework.beans.factory.support.BeanDefinitionRegistry;
 import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
+import org.springframework.beans.factory.support.BeanNameGenerator;
 import org.springframework.core.type.AnnotationMetadata;
 
 /**
@@ -42,6 +43,7 @@
  * <p>See implementations and associated unit tests for usage examples.
  *
  * @author Chris Beams
+ * @author Juergen Hoeller
  * @since 3.1
  * @see Import
  * @see ImportSelector
@@ -55,9 +57,31 @@
 	 * <p>Note that {@link BeanDefinitionRegistryPostProcessor} types may <em>not</em> be
 	 * registered here, due to lifecycle constraints related to {@code @Configuration}
 	 * class processing.
+	 * <p>The default implementation delegates to
+	 * {@link #registerBeanDefinitions(AnnotationMetadata, BeanDefinitionRegistry)}.
 	 * @param importingClassMetadata annotation metadata of the importing class
 	 * @param registry current bean definition registry
+	 * @param importBeanNameGenerator the configuration-level bean name generator
+	 * strategy for imported beans
+	 * @since 5.2
 	 */
-	void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry);
+	default void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry,
+			BeanNameGenerator importBeanNameGenerator) {
+
+		registerBeanDefinitions(importingClassMetadata, registry);
+	}
+
+	/**
+	 * Register bean definitions as necessary based on the given annotation metadata of
+	 * the importing {@code @Configuration} class.
+	 * <p>Note that {@link BeanDefinitionRegistryPostProcessor} types may <em>not</em> be
+	 * registered here, due to lifecycle constraints related to {@code @Configuration}
+	 * class processing.
+	 * <p>The default implementation is empty.
+	 * @param importingClassMetadata annotation metadata of the importing class
+	 * @param registry current bean definition registry
+	 */
+	default void registerBeanDefinitions(AnnotationMetadata importingClassMetadata, BeanDefinitionRegistry registry) {
+	}
 
 }
