diff --git a/spring-webflux/src/main/java/org/springframework/web/reactive/config/DefaultWebFluxConfiguration.java b/spring-webflux/src/main/java/org/springframework/web/reactive/config/DefaultWebFluxConfiguration.java
new file mode 100644
index 00000000000..dcf637f8687
--- /dev/null
+++ b/spring-webflux/src/main/java/org/springframework/web/reactive/config/DefaultWebFluxConfiguration.java
@@ -0,0 +1,49 @@
+/*
+ * Copyright 2002-2020 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.web.reactive.config;
+
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.web.server.i18n.AcceptHeaderLocaleContextResolver;
+import org.springframework.web.server.i18n.LocaleContextResolver;
+
+
+/**
+ * A subclass of {@link DelegatingWebFluxConfiguration} that registers a {@link LocaleContextResolver} bean
+ * and allows its customization. This is the class actually imported by {@link EnableWebFlux @EnableWebFlux}.
+ *
+ * <p>Import directly or extend and override protected methods to customize.
+ *
+ * @author Sebastien Deleuze
+ * @since 5.3
+ */
+@Configuration(proxyBeanMethods = false)
+public class DefaultWebFluxConfiguration extends DelegatingWebFluxConfiguration {
+
+	/**
+	 * Override to plug a sub-class of {@link LocaleContextResolver}.
+	 */
+	protected LocaleContextResolver createLocaleContextResolver() {
+		return new AcceptHeaderLocaleContextResolver();
+	}
+
+	@Bean
+	public LocaleContextResolver localeContextResolver() {
+		return createLocaleContextResolver();
+	}
+
+}
diff --git a/spring-webflux/src/main/java/org/springframework/web/reactive/config/DelegatingWebFluxConfiguration.java b/spring-webflux/src/main/java/org/springframework/web/reactive/config/DelegatingWebFluxConfiguration.java
index 9f4e147359e..b1efe1fd40b 100644
--- a/spring-webflux/src/main/java/org/springframework/web/reactive/config/DelegatingWebFluxConfiguration.java
+++ b/spring-webflux/src/main/java/org/springframework/web/reactive/config/DelegatingWebFluxConfiguration.java
@@ -31,11 +31,11 @@
 /**
  * A subclass of {@code WebFluxConfigurationSupport} that detects and delegates
  * to all beans of type {@link WebFluxConfigurer} allowing them to customize the
- * configuration provided by {@code WebFluxConfigurationSupport}. This is the
- * class actually imported by {@link EnableWebFlux @EnableWebFlux}.
+ * configuration provided by {@code WebFluxConfigurationSupport}.
  *
  * @author Brian Clozel
  * @since 5.0
+ * @see DefaultWebFluxConfiguration
  */
 @Configuration(proxyBeanMethods = false)
 public class DelegatingWebFluxConfiguration extends WebFluxConfigurationSupport {
diff --git a/spring-webflux/src/main/java/org/springframework/web/reactive/config/EnableWebFlux.java b/spring-webflux/src/main/java/org/springframework/web/reactive/config/EnableWebFlux.java
index cdd078783e2..635102fbecf 100644
--- a/spring-webflux/src/main/java/org/springframework/web/reactive/config/EnableWebFlux.java
+++ b/spring-webflux/src/main/java/org/springframework/web/reactive/config/EnableWebFlux.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2019 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -73,20 +73,23 @@
  * <p>If {@code WebFluxConfigurer} does not expose some setting that needs to be
  * configured, consider switching to an advanced mode by removing the
  * {@code @EnableWebFlux} annotation and extending directly from
- * {@link WebFluxConfigurationSupport} or {@link DelegatingWebFluxConfiguration} --
- * the latter allows detecting and delegating to one or more
- * {@code WebFluxConfigurer} configuration classes.
+ * {@link WebFluxConfigurationSupport}, {@link DelegatingWebFluxConfiguration}
+ * (allows detecting and delegating to one or more {@code WebFluxConfigurer}
+ * configuration classes) or {@link DefaultWebFluxConfiguration} (defines
+ * and allows customization of a locale resolver bean).
  *
  * @author Brian Clozel
  * @author Rossen Stoyanchev
+ * @author Sebastien Deleuze
  * @since 5.0
  * @see WebFluxConfigurer
  * @see WebFluxConfigurationSupport
  * @see DelegatingWebFluxConfiguration
+ * @see DefaultWebFluxConfiguration
  */
 @Retention(RetentionPolicy.RUNTIME)
 @Target(ElementType.TYPE)
 @Documented
-@Import(DelegatingWebFluxConfiguration.class)
+@Import(DefaultWebFluxConfiguration.class)
 public @interface EnableWebFlux {
 }
diff --git a/spring-webflux/src/main/java/org/springframework/web/reactive/config/WebFluxConfigurationSupport.java b/spring-webflux/src/main/java/org/springframework/web/reactive/config/WebFluxConfigurationSupport.java
index e7061ce4255..4d0fb337655 100644
--- a/spring-webflux/src/main/java/org/springframework/web/reactive/config/WebFluxConfigurationSupport.java
+++ b/spring-webflux/src/main/java/org/springframework/web/reactive/config/WebFluxConfigurationSupport.java
@@ -67,16 +67,15 @@
 import org.springframework.web.reactive.result.view.ViewResolver;
 import org.springframework.web.server.ServerWebExchange;
 import org.springframework.web.server.WebExceptionHandler;
-import org.springframework.web.server.i18n.AcceptHeaderLocaleContextResolver;
-import org.springframework.web.server.i18n.LocaleContextResolver;
 
 /**
- * The main class for Spring WebFlux configuration.
+ * The base class for Spring WebFlux configuration.
  *
  * <p>Import directly or extend and override protected methods to customize.
  *
  * @author Rossen Stoyanchev
  * @author Brian Clozel
+ * @author Sebastien Deleuze
  * @since 5.0
  */
 public class WebFluxConfigurationSupport implements ApplicationContextAware {
@@ -311,18 +310,6 @@ public ServerCodecConfigurer serverCodecConfigurer() {
 		return serverCodecConfigurer;
 	}
 
-	/**
-	 * Override to plug a sub-class of {@link LocaleContextResolver}.
-	 */
-	protected LocaleContextResolver createLocaleContextResolver() {
-		return new AcceptHeaderLocaleContextResolver();
-	}
-
-	@Bean
-	public LocaleContextResolver localeContextResolver() {
-		return createLocaleContextResolver();
-	}
-
 	/**
 	 * Override to configure the HTTP message readers and writers to use.
 	 */
diff --git a/spring-webflux/src/test/java/org/springframework/web/reactive/result/view/LocaleContextResolverIntegrationTests.java b/spring-webflux/src/test/java/org/springframework/web/reactive/result/view/LocaleContextResolverIntegrationTests.java
index b21b68cd5b7..d512fe7835f 100644
--- a/spring-webflux/src/test/java/org/springframework/web/reactive/result/view/LocaleContextResolverIntegrationTests.java
+++ b/spring-webflux/src/test/java/org/springframework/web/reactive/result/view/LocaleContextResolverIntegrationTests.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2019 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -33,8 +33,8 @@
 import org.springframework.lang.Nullable;
 import org.springframework.stereotype.Controller;
 import org.springframework.web.bind.annotation.GetMapping;
+import org.springframework.web.reactive.config.DefaultWebFluxConfiguration;
 import org.springframework.web.reactive.config.ViewResolverRegistry;
-import org.springframework.web.reactive.config.WebFluxConfigurationSupport;
 import org.springframework.web.reactive.function.client.ClientResponse;
 import org.springframework.web.reactive.function.client.WebClient;
 import org.springframework.web.reactive.result.method.annotation.AbstractRequestMappingIntegrationTests;
@@ -83,7 +83,7 @@ void fixedLocale(HttpServer httpServer) throws Exception {
 	@Configuration
 	@ComponentScan(resourcePattern = "**/LocaleContextResolverIntegrationTests*.class")
 	@SuppressWarnings({"unused", "WeakerAccess"})
-	static class WebConfig extends WebFluxConfigurationSupport {
+	static class WebConfig extends DefaultWebFluxConfiguration {
 
 		@Override
 		protected LocaleContextResolver createLocaleContextResolver() {
diff --git a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DefaultWebMvcConfiguration.java b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DefaultWebMvcConfiguration.java
new file mode 100644
index 00000000000..16cc8178386
--- /dev/null
+++ b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DefaultWebMvcConfiguration.java
@@ -0,0 +1,44 @@
+/*
+ * Copyright 2002-2020 the original author or authors.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      https://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+
+package org.springframework.web.servlet.config.annotation;
+
+import org.springframework.context.annotation.Bean;
+import org.springframework.context.annotation.Configuration;
+import org.springframework.web.servlet.LocaleResolver;
+import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
+
+/**
+ * A subclass of {@link DelegatingWebMvcConfiguration} that registers a {@link LocaleResolver} bean
+ * and allows its customization. This is the class actually imported by {@link EnableWebMvc @EnableWebMvc}.
+ *
+ * <p>Import directly or extend and override protected methods to customize.
+ *
+ * @author Sebastien Deleuze
+ * @since 5.3
+ */
+@Configuration(proxyBeanMethods = false)
+public class DefaultWebMvcConfiguration extends DelegatingWebMvcConfiguration {
+
+	protected LocaleResolver createLocaleResolver() {
+		return new AcceptHeaderLocaleResolver();
+	}
+
+	@Bean
+	public LocaleResolver localeResolver() {
+		return createLocaleResolver();
+	}
+}
diff --git a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DelegatingWebMvcConfiguration.java b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DelegatingWebMvcConfiguration.java
index 5970d26c756..5f54657064a 100644
--- a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DelegatingWebMvcConfiguration.java
+++ b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/DelegatingWebMvcConfiguration.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2019 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -33,11 +33,11 @@
 /**
  * A subclass of {@code WebMvcConfigurationSupport} that detects and delegates
  * to all beans of type {@link WebMvcConfigurer} allowing them to customize the
- * configuration provided by {@code WebMvcConfigurationSupport}. This is the
- * class actually imported by {@link EnableWebMvc @EnableWebMvc}.
+ * configuration provided by {@code WebMvcConfigurationSupport}.
  *
  * @author Rossen Stoyanchev
  * @since 3.1
+ * @see DefaultWebMvcConfiguration
  */
 @Configuration(proxyBeanMethods = false)
 public class DelegatingWebMvcConfiguration extends WebMvcConfigurationSupport {
diff --git a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/EnableWebMvc.java b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/EnableWebMvc.java
index acdb6e05640..650d49a31e3 100644
--- a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/EnableWebMvc.java
+++ b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/EnableWebMvc.java
@@ -1,5 +1,5 @@
 /*
- * Copyright 2002-2017 the original author or authors.
+ * Copyright 2002-2020 the original author or authors.
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
@@ -68,7 +68,7 @@
  * <p>If {@link WebMvcConfigurer} does not expose some more advanced setting that
  * needs to be configured consider removing the {@code @EnableWebMvc}
  * annotation and extending directly from {@link WebMvcConfigurationSupport}
- * or {@link DelegatingWebMvcConfiguration}, e.g.:
+ * , {@link DelegatingWebMvcConfiguration} or {@link DefaultWebMvcConfiguration}, e.g.:
  *
  * <pre class="code">
  * &#064;Configuration
@@ -90,14 +90,16 @@
  *
  * @author Dave Syer
  * @author Rossen Stoyanchev
+ * @author Sebastien Deleuze
  * @since 3.1
- * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurer
- * @see org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport
- * @see org.springframework.web.servlet.config.annotation.DelegatingWebMvcConfiguration
+ * @see WebMvcConfigurer
+ * @see WebMvcConfigurationSupport
+ * @see DelegatingWebMvcConfiguration
+ * @see DefaultWebMvcConfiguration
  */
 @Retention(RetentionPolicy.RUNTIME)
 @Target(ElementType.TYPE)
 @Documented
-@Import(DelegatingWebMvcConfiguration.class)
+@Import(DefaultWebMvcConfiguration.class)
 public @interface EnableWebMvc {
 }
diff --git a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurationSupport.java b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurationSupport.java
index b85cd5cdb3b..3f6a7fe6d81 100644
--- a/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurationSupport.java
+++ b/spring-webmvc/src/main/java/org/springframework/web/servlet/config/annotation/WebMvcConfigurationSupport.java
@@ -79,7 +79,6 @@
 import org.springframework.web.servlet.HandlerAdapter;
 import org.springframework.web.servlet.HandlerExceptionResolver;
 import org.springframework.web.servlet.HandlerMapping;
-import org.springframework.web.servlet.LocaleResolver;
 import org.springframework.web.servlet.RequestToViewNameTranslator;
 import org.springframework.web.servlet.ThemeResolver;
 import org.springframework.web.servlet.ViewResolver;
@@ -90,7 +89,6 @@
 import org.springframework.web.servlet.handler.ConversionServiceExposingInterceptor;
 import org.springframework.web.servlet.handler.HandlerExceptionResolverComposite;
 import org.springframework.web.servlet.handler.HandlerMappingIntrospector;
-import org.springframework.web.servlet.i18n.AcceptHeaderLocaleResolver;
 import org.springframework.web.servlet.mvc.Controller;
 import org.springframework.web.servlet.mvc.HttpRequestHandlerAdapter;
 import org.springframework.web.servlet.mvc.SimpleControllerHandlerAdapter;
@@ -1119,11 +1117,6 @@ public HandlerMappingIntrospector mvcHandlerMappingIntrospector() {
 		return new HandlerMappingIntrospector();
 	}
 
-	@Bean
-	public LocaleResolver localeResolver() {
-		return new AcceptHeaderLocaleResolver();
-	}
-
 	@Bean
 	public ThemeResolver themeResolver() {
 		return new FixedThemeResolver();
