diff --git a/mockwebserver/src/main/java/okhttp3/mockwebserver/MockResponse.java b/mockwebserver/src/main/java/okhttp3/mockwebserver/MockResponse.java
index 8452b84634..03e8f25a8e 100644
--- a/mockwebserver/src/main/java/okhttp3/mockwebserver/MockResponse.java
+++ b/mockwebserver/src/main/java/okhttp3/mockwebserver/MockResponse.java
@@ -43,6 +43,9 @@
   private long bodyDelayAmount = 0;
   private TimeUnit bodyDelayUnit = TimeUnit.MILLISECONDS;
 
+  private long shutdownDelay = 0;
+  private TimeUnit shutdownDelayUnit = TimeUnit.MILLISECONDS;
+
   private List<PushPromise> promises = new ArrayList<>();
   private Settings settings;
   private WebSocketListener webSocketListener;
@@ -253,6 +256,21 @@ public long getBodyDelay(TimeUnit unit) {
     return unit.convert(bodyDelayAmount, bodyDelayUnit);
   }
 
+  /**
+   * Set the time to {@code delay} prior to shutting down an HTTP/2 connection. This is only valid
+   * with {@link SocketPolicy#DISCONNECT_AT_END}. Use this to simulate a graceful HTTP/2
+   * connection shutdown.
+   */
+  public MockResponse setShutdownDelay(long delay, TimeUnit unit) {
+    shutdownDelay = delay;
+    shutdownDelayUnit = unit;
+    return this;
+  }
+
+  public long getShutdownDelay(TimeUnit unit) {
+    return unit.convert(shutdownDelay, shutdownDelayUnit);
+  }
+
   /**
    * When {@link MockWebServer#setProtocols(java.util.List) protocols} include {@linkplain
    * okhttp3.Protocol#HTTP_2}, this attaches a pushed stream to this response.
diff --git a/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java b/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java
index 59b50c70af..7fcb748142 100644
--- a/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java
+++ b/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java
@@ -369,6 +369,7 @@ private void acceptConnections() throws Exception {
             logger.info(MockWebServer.this + " done accepting connections: " + e.getMessage());
             return;
           }
+          logger.info("accepted connection on port " + socket.getPort());
           SocketPolicy socketPolicy = dispatcher.peek().getSocketPolicy();
           if (socketPolicy == DISCONNECT_AT_START) {
             dispatchBookkeepingRequest(0, socket);
@@ -877,6 +878,17 @@ private FramedSocketHandler(Socket socket, Protocol protocol) {
         logger.info(MockWebServer.this + " received request: " + request
             + " and responded: " + response + " protocol is " + protocol.toString());
       }
+
+      if (response.getSocketPolicy() == DISCONNECT_AT_END) {
+        Http2Connection connection = stream.getConnection();
+        connection.shutdown(ErrorCode.NO_ERROR);
+        try {
+          Thread.sleep(response.getShutdownDelay(TimeUnit.MILLISECONDS));
+        } catch (InterruptedException e) {
+          throw new InterruptedIOException();
+        }
+        connection.close();
+      }
     }
 
     private RecordedRequest readRequest(Http2Stream stream) throws IOException {
diff --git a/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java b/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java
index eddb7557a4..b4930e63c8 100644
--- a/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java
+++ b/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java
@@ -37,7 +37,10 @@
   KEEP_OPEN,
 
   /**
-   * Close the socket after the response. This is the default HTTP/1.0 behavior.
+   * Close the socket after the response. This is the default HTTP/1.0 behavior. For HTTP/2
+   * connections, send a <a href="https://tools.ietf.org/html/rfc7540#section-6.8">GOAWAY frame</a>
+   * immediately after the response, then close the connection. The connection will be closed after
+   * delaying for {@link MockResponse#getShutdownDelay(java.util.concurrent.TimeUnit)}.
    *
    * <p>See {@link SocketPolicy} for reasons why this can cause test flakiness and how to avoid it.
    */
diff --git a/okhttp-tests/src/test/java/okhttp3/URLConnectionTest.java b/okhttp-tests/src/test/java/okhttp3/URLConnectionTest.java
index 2e71146771..5401e1f4bb 100644
--- a/okhttp-tests/src/test/java/okhttp3/URLConnectionTest.java
+++ b/okhttp-tests/src/test/java/okhttp3/URLConnectionTest.java
@@ -3530,6 +3530,40 @@ private void zeroLengthPayload(String method)
     assertEquals(0, dispatcher.runningCallsCount());
   }
 
+  @Test public void streamedBodyIsRetriedOnHttp2Shutdown() throws Exception {
+    enableProtocol(Protocol.HTTP_2);
+    server.enqueue(new MockResponse()
+        .setSocketPolicy(SocketPolicy.DISCONNECT_AT_END)
+        .setShutdownDelay(1, TimeUnit.SECONDS)
+        .setBody("abc"));
+    server.enqueue(new MockResponse()
+        .setBody("def"));
+
+    HttpURLConnection connection1 = urlFactory.open(server.url("/").url());
+    connection1.setChunkedStreamingMode(4096);
+    connection1.setRequestMethod("POST");
+    connection1.connect(); // Establish healthy HTTP/2 connection, but don't write yet.
+
+    // Send a separate request which will trigger a GOAWAY frame on the healthy connection.
+    HttpURLConnection connection2 = urlFactory.open(server.url("/").url());
+    assertContent("abc", connection2);
+
+    // Delay to ensure the GOAWAY frame is processed.
+    Thread.sleep(250);
+
+    OutputStream os = connection1.getOutputStream();
+    os.write(new byte[] { '1', '2', '3' });
+    os.close();
+    assertContent("def", connection1);
+
+    RecordedRequest request1 = server.takeRequest();
+    assertEquals(0, request1.getSequenceNumber());
+
+    RecordedRequest request2 = server.takeRequest();
+    assertEquals("123", request2.getBody().readUtf8());
+    assertEquals(0, request2.getSequenceNumber());
+  }
+
   private void testInstanceFollowsRedirects(String spec) throws Exception {
     URL url = new URL(spec);
     HttpURLConnection urlConnection = urlFactory.open(url);
diff --git a/okhttp-tests/src/test/java/okhttp3/internal/http2/Http2ConnectionTest.java b/okhttp-tests/src/test/java/okhttp3/internal/http2/Http2ConnectionTest.java
index 610e0898ec..a782691f7d 100644
--- a/okhttp-tests/src/test/java/okhttp3/internal/http2/Http2ConnectionTest.java
+++ b/okhttp-tests/src/test/java/okhttp3/internal/http2/Http2ConnectionTest.java
@@ -245,8 +245,7 @@
     try {
       connection.newStream(headerEntries("c", "cola"), true);
       fail();
-    } catch (IOException expected) {
-      assertEquals("shutdown", expected.getMessage());
+    } catch (ConnectionShutdownException expected) {
     }
     assertTrue(stream1.isOpen());
     assertFalse(stream2.isOpen());
@@ -1009,8 +1008,7 @@
     try {
       connection.newStream(headerEntries("c", "cola"), false);
       fail();
-    } catch (IOException expected) {
-      assertEquals("shutdown", expected.getMessage());
+    } catch (ConnectionShutdownException expected) {
     }
     assertTrue(stream1.isOpen());
     assertFalse(stream2.isOpen());
@@ -1068,8 +1066,7 @@
     try {
       connection.ping();
       fail();
-    } catch (IOException expected) {
-      assertEquals("shutdown", expected.getMessage());
+    } catch (ConnectionShutdownException expected) {
     }
 
     // verify the peer received what was expected
@@ -1094,8 +1091,7 @@
     try {
       connection.newStream(headerEntries("b", "banana"), false);
       fail();
-    } catch (IOException expected) {
-      assertEquals("shutdown", expected.getMessage());
+    } catch (ConnectionShutdownException expected) {
     }
     BufferedSink sink = Okio.buffer(stream.getSink());
     try {
diff --git a/okhttp-tests/src/test/java/okhttp3/internal/http2/HttpOverHttp2Test.java b/okhttp-tests/src/test/java/okhttp3/internal/http2/HttpOverHttp2Test.java
index 0ccbf3b9f3..1c3bea6afa 100644
--- a/okhttp-tests/src/test/java/okhttp3/internal/http2/HttpOverHttp2Test.java
+++ b/okhttp-tests/src/test/java/okhttp3/internal/http2/HttpOverHttp2Test.java
@@ -24,12 +24,14 @@
 import java.util.concurrent.CountDownLatch;
 import java.util.concurrent.ExecutorService;
 import java.util.concurrent.Executors;
+import java.util.concurrent.TimeUnit;
 import javax.net.ssl.HostnameVerifier;
 import okhttp3.Cache;
 import okhttp3.Call;
 import okhttp3.Cookie;
 import okhttp3.Credentials;
 import okhttp3.Headers;
+import okhttp3.Interceptor;
 import okhttp3.MediaType;
 import okhttp3.OkHttpClient;
 import okhttp3.Protocol;
@@ -791,6 +793,80 @@ private void noRecoveryFromErrorWithRetryDisabled(ErrorCode errorCode) throws Ex
     assertEquals(0, server.takeRequest().getSequenceNumber()); // New connection!
   }
 
+  @Test public void connectionNotReusedAfterShutdown() throws Exception {
+    server.enqueue(new MockResponse()
+        .setSocketPolicy(SocketPolicy.DISCONNECT_AT_END)
+        .setShutdownDelay(1, TimeUnit.SECONDS)
+        .setBody("ABC"));
+    server.enqueue(new MockResponse()
+        .setBody("DEF"));
+
+    Call call1 = client.newCall(new Request.Builder()
+        .url(server.url("/"))
+        .build());
+    Response response1 = call1.execute();
+    assertEquals("ABC", response1.body().string());
+
+    // Allow for graceful shutdown and processing of GOAWAY frame.
+    Thread.sleep(250);
+
+    Call call2 = client.newCall(new Request.Builder()
+        .url(server.url("/"))
+        .build());
+    Response response2 = call2.execute();
+    assertEquals("DEF", response2.body().string());
+    assertEquals(0, server.takeRequest().getSequenceNumber());
+    assertEquals(0, server.takeRequest().getSequenceNumber());
+  }
+
+  /**
+   * This simulates a race condition where we receive a healthy HTTP/2 connection and just prior to
+   * writing our request, we get a GOAWAY frame from the server.
+   */
+  @Test public void connectionShutdownAfterHealthCheck() throws Exception {
+    server.enqueue(new MockResponse()
+        .setSocketPolicy(SocketPolicy.DISCONNECT_AT_END)
+        .setShutdownDelay(1, TimeUnit.SECONDS)
+        .setBody("ABC"));
+    server.enqueue(new MockResponse()
+        .setBody("DEF"));
+
+    OkHttpClient client2 = client.newBuilder()
+        .addNetworkInterceptor(new Interceptor() {
+          boolean executedCall;
+
+          @Override public Response intercept(Chain chain) throws IOException {
+            if (!executedCall) {
+              // At this point, we have a healthy HTTP/2 connection. This call will trigger the
+              // server to send a GOAWAY frame, leaving the connection in a shutdown state.
+              executedCall = true;
+              Call call = client.newCall(new Request.Builder()
+                  .url(server.url("/"))
+                  .build());
+              Response response = call.execute();
+              assertEquals("ABC", response.body().string());
+              // Allow for graceful shutdown and processing of GOAWAY frame.
+              try {
+                Thread.sleep(250);
+              } catch (InterruptedException e) {
+                throw new AssertionError();
+              }
+            }
+            return chain.proceed(chain.request());
+          }
+        })
+        .build();
+
+    Call call = client2.newCall(new Request.Builder()
+        .url(server.url("/"))
+        .build());
+    Response response = call.execute();
+    assertEquals("DEF", response.body().string());
+
+    assertEquals(0, server.takeRequest().getSequenceNumber());
+    assertEquals(0, server.takeRequest().getSequenceNumber());
+  }
+
   public Buffer gzip(String bytes) throws IOException {
     Buffer bytesOut = new Buffer();
     BufferedSink sink = Okio.buffer(new GzipSink(bytesOut));
diff --git a/okhttp-tests/src/test/java/okhttp3/internal/tls/CertificatePinnerChainValidationTest.java b/okhttp-tests/src/test/java/okhttp3/internal/tls/CertificatePinnerChainValidationTest.java
index ee8a2fe935..a3de514b6e 100644
--- a/okhttp-tests/src/test/java/okhttp3/internal/tls/CertificatePinnerChainValidationTest.java
+++ b/okhttp-tests/src/test/java/okhttp3/internal/tls/CertificatePinnerChainValidationTest.java
@@ -138,6 +138,8 @@
     Response response1 = call1.execute();
     assertEquals("abc", response1.body().string());
 
+    Thread.sleep(250);
+
     // Confirm that a second request also succeeds. This should detect caching problems.
     server.enqueue(new MockResponse()
         .setBody("def")
diff --git a/okhttp/src/main/java/okhttp3/Dns.java b/okhttp/src/main/java/okhttp3/Dns.java
index a2e6db591c..904fded088 100644
--- a/okhttp/src/main/java/okhttp3/Dns.java
+++ b/okhttp/src/main/java/okhttp3/Dns.java
@@ -19,6 +19,7 @@
 import java.net.UnknownHostException;
 import java.util.Arrays;
 import java.util.List;
+import java.util.logging.Logger;
 
 /**
  * A domain name service that resolves IP addresses for host names. Most applications will use the
@@ -36,7 +37,9 @@
   Dns SYSTEM = new Dns() {
     @Override public List<InetAddress> lookup(String hostname) throws UnknownHostException {
       if (hostname == null) throw new UnknownHostException("hostname == null");
-      return Arrays.asList(InetAddress.getAllByName(hostname));
+      InetAddress[] a = InetAddress.getAllByName(hostname);
+      Logger.getLogger(Dns.class.getName()).warning(Arrays.toString(a));
+      return Arrays.asList(a);
     }
   };
 
diff --git a/okhttp/src/main/java/okhttp3/internal/connection/RealConnection.java b/okhttp/src/main/java/okhttp3/internal/connection/RealConnection.java
index 42d91038a8..547781d26f 100644
--- a/okhttp/src/main/java/okhttp3/internal/connection/RealConnection.java
+++ b/okhttp/src/main/java/okhttp3/internal/connection/RealConnection.java
@@ -366,7 +366,7 @@ public boolean isHealthy(boolean doExtensiveChecks) {
     }
 
     if (http2Connection != null) {
-      return true; // TODO: check framedConnection.shutdown.
+      return !http2Connection.isShutdown();
     }
 
     if (doExtensiveChecks) {
diff --git a/okhttp/src/main/java/okhttp3/internal/connection/StreamAllocation.java b/okhttp/src/main/java/okhttp3/internal/connection/StreamAllocation.java
index 73b2506f62..b78e3c54b1 100644
--- a/okhttp/src/main/java/okhttp3/internal/connection/StreamAllocation.java
+++ b/okhttp/src/main/java/okhttp3/internal/connection/StreamAllocation.java
@@ -28,6 +28,7 @@
 import okhttp3.internal.http1.Http1Codec;
 import okhttp3.internal.http2.ErrorCode;
 import okhttp3.internal.http2.Http2Codec;
+import okhttp3.internal.http2.ConnectionShutdownException;
 import okhttp3.internal.http2.StreamResetException;
 
 import static java.util.concurrent.TimeUnit.MILLISECONDS;
@@ -298,7 +299,8 @@ public void streamFailed(IOException e) {
           noNewStreams = true;
           route = null;
         }
-      } else if (connection != null && !connection.isMultiplexed()) {
+      } else if (connection != null && !connection.isMultiplexed()
+          || e instanceof ConnectionShutdownException) {
         noNewStreams = true;
 
         // If this route hasn't completed a call, avoid it for new connections.
diff --git a/okhttp/src/main/java/okhttp3/internal/http/RetryAndFollowUpInterceptor.java b/okhttp/src/main/java/okhttp3/internal/http/RetryAndFollowUpInterceptor.java
index 2a7cd13822..d3c835ad1d 100644
--- a/okhttp/src/main/java/okhttp3/internal/http/RetryAndFollowUpInterceptor.java
+++ b/okhttp/src/main/java/okhttp3/internal/http/RetryAndFollowUpInterceptor.java
@@ -38,6 +38,7 @@
 import okhttp3.Route;
 import okhttp3.internal.connection.RouteException;
 import okhttp3.internal.connection.StreamAllocation;
+import okhttp3.internal.http2.ConnectionShutdownException;
 
 import static java.net.HttpURLConnection.HTTP_CLIENT_TIMEOUT;
 import static java.net.HttpURLConnection.HTTP_MOVED_PERM;
@@ -119,13 +120,18 @@ public StreamAllocation streamAllocation() {
         response = ((RealInterceptorChain) chain).proceed(request, streamAllocation, null, null);
         releaseConnection = false;
       } catch (RouteException e) {
+        e.printStackTrace();
         // The attempt to connect via a route failed. The request will not have been sent.
-        if (!recover(e.getLastConnectException(), true, request)) throw e.getLastConnectException();
+        if (!recover(e.getLastConnectException(), false, request)) {
+          throw e.getLastConnectException();
+        }
         releaseConnection = false;
         continue;
       } catch (IOException e) {
+        e.printStackTrace();
         // An attempt to communicate with a server failed. The request may have been sent.
-        if (!recover(e, false, request)) throw e;
+        boolean requestSendStarted = !(e instanceof ConnectionShutdownException);
+        if (!recover(e, requestSendStarted, request)) throw e;
         releaseConnection = false;
         continue;
       } finally {
@@ -198,19 +204,20 @@ private Address createAddress(HttpUrl url) {
   /**
    * Report and attempt to recover from a failure to communicate with a server. Returns true if
    * {@code e} is recoverable, or false if the failure is permanent. Requests with a body can only
-   * be recovered if the body is buffered.
+   * be recovered if the body is buffered or if the failure occurred before the request has been
+   * sent.
    */
-  private boolean recover(IOException e, boolean routeException, Request userRequest) {
+  private boolean recover(IOException e, boolean requestSendStarted, Request userRequest) {
     streamAllocation.streamFailed(e);
 
     // The application layer has forbidden retries.
     if (!client.retryOnConnectionFailure()) return false;
 
     // We can't send the request body again.
-    if (!routeException && userRequest.body() instanceof UnrepeatableRequestBody) return false;
+    if (requestSendStarted && userRequest.body() instanceof UnrepeatableRequestBody) return false;
 
     // This exception is fatal.
-    if (!isRecoverable(e, routeException)) return false;
+    if (!isRecoverable(e, requestSendStarted)) return false;
 
     // No more routes to attempt.
     if (!streamAllocation.hasMoreRoutes()) return false;
@@ -219,7 +226,7 @@ private boolean recover(IOException e, boolean routeException, Request userReque
     return true;
   }
 
-  private boolean isRecoverable(IOException e, boolean routeException) {
+  private boolean isRecoverable(IOException e, boolean requestSendStarted) {
     // If there was a protocol problem, don't recover.
     if (e instanceof ProtocolException) {
       return false;
@@ -228,7 +235,7 @@ private boolean isRecoverable(IOException e, boolean routeException) {
     // If there was an interruption don't recover, but if there was a timeout connecting to a route
     // we should try the next route (if there is one).
     if (e instanceof InterruptedIOException) {
-      return e instanceof SocketTimeoutException && routeException;
+      return e instanceof SocketTimeoutException && !requestSendStarted;
     }
 
     // Look for known client-side or negotiation errors that are unlikely to be fixed by trying
diff --git a/okhttp/src/main/java/okhttp3/internal/http2/ConnectionShutdownException.java b/okhttp/src/main/java/okhttp3/internal/http2/ConnectionShutdownException.java
new file mode 100644
index 0000000000..00c4f2441c
--- /dev/null
+++ b/okhttp/src/main/java/okhttp3/internal/http2/ConnectionShutdownException.java
@@ -0,0 +1,25 @@
+/*
+ * Copyright (C) 2016 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package okhttp3.internal.http2;
+
+import java.io.IOException;
+
+/**
+ * Thrown when an HTTP/2 connection is shutdown (either explicitly or if the peer has sent a GOAWAY
+ * frame) and an attempt is made to use the connection.
+ */
+public final class ConnectionShutdownException extends IOException {
+}
diff --git a/okhttp/src/main/java/okhttp3/internal/http2/Http2Connection.java b/okhttp/src/main/java/okhttp3/internal/http2/Http2Connection.java
index b367c521e5..859fbb664c 100644
--- a/okhttp/src/main/java/okhttp3/internal/http2/Http2Connection.java
+++ b/okhttp/src/main/java/okhttp3/internal/http2/Http2Connection.java
@@ -30,6 +30,7 @@
 import java.util.concurrent.SynchronousQueue;
 import java.util.concurrent.ThreadPoolExecutor;
 import java.util.concurrent.TimeUnit;
+import java.util.logging.Logger;
 import okhttp3.Protocol;
 import okhttp3.internal.NamedRunnable;
 import okhttp3.internal.Util;
@@ -216,7 +217,7 @@ private Http2Stream newStream(
     synchronized (writer) {
       synchronized (this) {
         if (shutdown) {
-          throw new IOException("shutdown");
+          throw new ConnectionShutdownException();
         }
         streamId = nextStreamId;
         nextStreamId += 2;
@@ -335,7 +336,7 @@ public Ping ping() throws IOException {
     int pingId;
     synchronized (this) {
       if (shutdown) {
-        throw new IOException("shutdown");
+        throw new ConnectionShutdownException();
       }
       pingId = nextPingId;
       nextPingId += 2;
@@ -426,6 +427,8 @@ private void close(ErrorCode connectionCode, ErrorCode streamCode) throws IOExce
     }
 
     if (streamsToClose != null) {
+      Logger.getLogger(Http2Connection.class.getName()).warning(
+          "found http/2 stream to close, client=" + client);
       for (Http2Stream stream : streamsToClose) {
         try {
           stream.close(streamCode);
@@ -482,12 +485,16 @@ void start(boolean sendConnectionPreface) throws IOException {
     new Thread(readerRunnable).start(); // Not a daemon thread.
   }
 
+  public synchronized boolean isShutdown() {
+    return shutdown;
+  }
+
   /** Merges {@code settings} into this peer's settings and sends them to the remote peer. */
   public void setSettings(Settings settings) throws IOException {
     synchronized (writer) {
       synchronized (this) {
         if (shutdown) {
-          throw new IOException("shutdown");
+          throw new ConnectionShutdownException();
         }
         okHttpSettings.merge(settings);
         writer.settings(settings);
