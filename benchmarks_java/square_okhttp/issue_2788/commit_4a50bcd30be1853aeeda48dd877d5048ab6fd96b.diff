diff --git a/okhttp-tests/src/test/java/okhttp3/WebSocketHttpTest.java b/okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketHttpTest.java
similarity index 94%
rename from okhttp-tests/src/test/java/okhttp3/WebSocketHttpTest.java
rename to okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketHttpTest.java
index afa06f034b..eaff205fd8 100644
--- a/okhttp-tests/src/test/java/okhttp3/WebSocketHttpTest.java
+++ b/okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketHttpTest.java
@@ -13,7 +13,7 @@
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
-package okhttp3;
+package okhttp3.internal.ws;
 
 import java.io.IOException;
 import java.net.ProtocolException;
@@ -22,10 +22,15 @@
 import java.util.concurrent.TimeUnit;
 import java.util.concurrent.atomic.AtomicInteger;
 import java.util.logging.Logger;
+import okhttp3.Interceptor;
+import okhttp3.OkHttpClient;
+import okhttp3.RecordingHostnameVerifier;
+import okhttp3.Request;
+import okhttp3.Response;
+import okhttp3.TestLogHandler;
+import okhttp3.WebSocket;
+import okhttp3.WebSocketListener;
 import okhttp3.internal.tls.SslClient;
-import okhttp3.internal.ws.RealWebSocket;
-import okhttp3.internal.ws.WebSocketProtocol;
-import okhttp3.internal.ws.WebSocketRecorder;
 import okhttp3.mockwebserver.Dispatcher;
 import okhttp3.mockwebserver.MockResponse;
 import okhttp3.mockwebserver.MockWebServer;
@@ -544,6 +549,29 @@
     assertEquals(0, webSocket.pingCount());
   }
 
+  /** https://github.com/square/okhttp/issues/2788 */
+  @Test public void clientCancelsIfCloseIsNotAcknowledged() throws Exception {
+    webServer.enqueue(new MockResponse().withWebSocketUpgrade(serverListener));
+    RealWebSocket webSocket = newWebSocket();
+
+    clientListener.assertOpen();
+    WebSocket server = serverListener.assertOpen();
+
+    // Initiate a close on the client, which will schedule a hard cancel in 500 ms.
+    long closeAtNanos = System.nanoTime();
+    webSocket.close(1000, "goodbye", 500);
+    serverListener.assertClosing(1000, "goodbye");
+
+    // Confirm that the hard cancel occurred after 500 ms.
+    clientListener.assertFailure();
+    long elapsedUntilFailure = System.nanoTime() - closeAtNanos;
+    assertEquals(500, TimeUnit.NANOSECONDS.toMillis(elapsedUntilFailure), 250d);
+
+    // Close the server and confirm it saw what we expected.
+    server.close(1000, null);
+    serverListener.assertClosed(1000, "goodbye");
+  }
+
   private MockResponse upgradeResponse(RecordedRequest request) {
     String key = request.getHeader("Sec-WebSocket-Key");
     return new MockResponse()
diff --git a/okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketRecorder.java b/okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketRecorder.java
index c37508e0ac..ac5626ce57 100644
--- a/okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketRecorder.java
+++ b/okhttp-tests/src/test/java/okhttp3/internal/ws/WebSocketRecorder.java
@@ -194,6 +194,13 @@ public void assertFailure(Class<? extends IOException> cls, String message) {
     assertEquals(message, failure.t.getMessage());
   }
 
+  public void assertFailure() {
+    Object event = nextEvent();
+    if (!(event instanceof Failure)) {
+      throw new AssertionError("Expected Failure but was " + event);
+    }
+  }
+
   public void assertFailure(int code, String body, Class<? extends IOException> cls, String message)
       throws IOException {
     Object event = nextEvent();
diff --git a/okhttp/src/main/java/okhttp3/internal/ws/RealWebSocket.java b/okhttp/src/main/java/okhttp3/internal/ws/RealWebSocket.java
index 64c85f7a4b..4fa547b2aa 100644
--- a/okhttp/src/main/java/okhttp3/internal/ws/RealWebSocket.java
+++ b/okhttp/src/main/java/okhttp3/internal/ws/RealWebSocket.java
@@ -23,6 +23,7 @@
 import java.util.List;
 import java.util.Random;
 import java.util.concurrent.ScheduledExecutorService;
+import java.util.concurrent.ScheduledFuture;
 import java.util.concurrent.ScheduledThreadPoolExecutor;
 import okhttp3.Call;
 import okhttp3.Callback;
@@ -57,6 +58,12 @@
    */
   private static final long MAX_QUEUE_SIZE = 16 * 1024 * 1024; // 16 MiB.
 
+  /**
+   * The maximum amount of time after the client calls {@link #close} to wait for a graceful
+   * shutdown. If the server doesn't respond the websocket will be canceled.
+   */
+  private static final long CANCEL_AFTER_CLOSE_MILLIS = 60 * 1000;
+
   /** The application's original request unadulterated by web socket headers. */
   private final Request originalRequest;
 
@@ -100,6 +107,12 @@
   /** True if we've enqueued a close frame. No further message frames will be enqueued. */
   private boolean enqueuedClose;
 
+  /**
+   * When executed this will cancel this websocket. This future itself should be canceled if that is
+   * unnecessary because the web socket is already closed or canceled.
+   */
+  private ScheduledFuture<?> cancelFuture;
+
   /** The close code from the peer, or -1 if this web socket has not yet read a close frame. */
   private int receivedCloseCode = -1;
 
@@ -263,11 +276,11 @@ boolean processNextFrame() throws IOException {
     }
   }
 
-  public synchronized int pingCount() {
+  synchronized int pingCount() {
     return pingCount;
   }
 
-  public synchronized int pongCount() {
+  synchronized int pongCount() {
     return pongCount;
   }
 
@@ -304,6 +317,7 @@ public synchronized int pongCount() {
       if (enqueuedClose && messageAndCloseQueue.isEmpty()) {
         toClose = this.streams;
         this.streams = null;
+        if (cancelFuture != null) cancelFuture.cancel(false);
         this.executor.shutdown();
       }
     }
@@ -348,7 +362,7 @@ private synchronized boolean send(ByteString data, int formatOpcode) {
     return true;
   }
 
-  public synchronized boolean pong(ByteString payload) {
+  synchronized boolean pong(ByteString payload) {
     // Don't send pongs after we've failed or sent the close frame.
     if (failed || (enqueuedClose && messageAndCloseQueue.isEmpty())) return false;
 
@@ -357,7 +371,11 @@ public synchronized boolean pong(ByteString payload) {
     return true;
   }
 
-  @Override public synchronized boolean close(int code, String reason) {
+  @Override public boolean close(int code, String reason) {
+    return close(code, reason, CANCEL_AFTER_CLOSE_MILLIS);
+  }
+
+  synchronized boolean close(int code, String reason, long cancelAfterCloseMillis) {
     validateCloseCode(code);
 
     ByteString reasonBytes = null;
@@ -374,7 +392,7 @@ public synchronized boolean pong(ByteString payload) {
     enqueuedClose = true;
 
     // Enqueue the close frame.
-    messageAndCloseQueue.add(new Close(code, reasonBytes));
+    messageAndCloseQueue.add(new Close(code, reasonBytes, cancelAfterCloseMillis));
     runWriter();
     return true;
   }
@@ -424,8 +442,11 @@ boolean writeOneFrame() throws IOException {
             streamsToClose = this.streams;
             this.streams = null;
             this.executor.shutdown();
+          } else {
+            // When we request a graceful close also schedule a cancel of the websocket.
+            cancelFuture = executor.schedule(new CancelRunnable(),
+                ((Close) messageOrClose).cancelAfterCloseMillis, MILLISECONDS);
           }
-
         } else if (messageOrClose == null) {
           return false; // The queue is exhausted.
         }
@@ -492,6 +513,7 @@ void failWebSocket(Exception e, Response response) {
       failed = true;
       streamsToClose = this.streams;
       this.streams = null;
+      if (cancelFuture != null) cancelFuture.cancel(false);
       if (executor != null) executor.shutdown();
     }
 
@@ -506,7 +528,7 @@ void failWebSocket(Exception e, Response response) {
     final int formatOpcode;
     final ByteString data;
 
-    public Message(int formatOpcode, ByteString data) {
+    Message(int formatOpcode, ByteString data) {
       this.formatOpcode = formatOpcode;
       this.data = data;
     }
@@ -515,10 +537,12 @@ public Message(int formatOpcode, ByteString data) {
   static final class Close {
     final int code;
     final ByteString reason;
+    final long cancelAfterCloseMillis;
 
-    public Close(int code, ByteString reason) {
+    Close(int code, ByteString reason, long cancelAfterCloseMillis) {
       this.code = code;
       this.reason = reason;
+      this.cancelAfterCloseMillis = cancelAfterCloseMillis;
     }
   }
 
@@ -537,7 +561,7 @@ public Streams(boolean client, BufferedSource source, BufferedSink sink) {
   static final class ClientStreams extends Streams {
     private final StreamAllocation streamAllocation;
 
-    public ClientStreams(StreamAllocation streamAllocation) {
+    ClientStreams(StreamAllocation streamAllocation) {
       super(true, streamAllocation.connection().source, streamAllocation.connection().sink);
       this.streamAllocation = streamAllocation;
     }
@@ -546,4 +570,10 @@ public ClientStreams(StreamAllocation streamAllocation) {
       streamAllocation.streamFinished(true, streamAllocation.codec());
     }
   }
+
+  final class CancelRunnable implements Runnable {
+    @Override public void run() {
+      cancel();
+    }
+  }
 }
