diff --git a/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java b/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java
index 677adbae7d..6ea07862bd 100644
--- a/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java
+++ b/mockwebserver/src/main/java/okhttp3/mockwebserver/MockWebServer.java
@@ -84,6 +84,7 @@
 import static okhttp3.mockwebserver.SocketPolicy.DISCONNECT_AT_START;
 import static okhttp3.mockwebserver.SocketPolicy.DISCONNECT_DURING_REQUEST_BODY;
 import static okhttp3.mockwebserver.SocketPolicy.DISCONNECT_DURING_RESPONSE_BODY;
+import static okhttp3.mockwebserver.SocketPolicy.CONTINUE_ALWAYS;
 import static okhttp3.mockwebserver.SocketPolicy.EXPECT_CONTINUE;
 import static okhttp3.mockwebserver.SocketPolicy.FAIL_HANDSHAKE;
 import static okhttp3.mockwebserver.SocketPolicy.NO_RESPONSE;
@@ -597,7 +598,8 @@ private RecordedRequest readRequest(Socket socket, BufferedSource source, Buffer
       }
     }
 
-    if (expectContinue && dispatcher.peek().getSocketPolicy() == EXPECT_CONTINUE) {
+    final SocketPolicy socketPolicy = dispatcher.peek().getSocketPolicy();
+    if (expectContinue && socketPolicy == EXPECT_CONTINUE || socketPolicy == CONTINUE_ALWAYS) {
       sink.writeUtf8("HTTP/1.1 100 Continue\r\n");
       sink.writeUtf8("Content-Length: 0\r\n");
       sink.writeUtf8("\r\n");
diff --git a/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java b/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java
index 521c5a0e58..ccc7939778 100644
--- a/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java
+++ b/mockwebserver/src/main/java/okhttp3/mockwebserver/SocketPolicy.java
@@ -104,5 +104,11 @@
    * Typically this response is sent when a client makes a request with the header {@code
    * Expect: 100-continue}.
    */
-  EXPECT_CONTINUE
+  EXPECT_CONTINUE,
+
+  /**
+   * Transmit a {@code HTTP/1.1 100 Continue} response before reading the HTTP request body even
+   * if the client does not send the header {@code Expect: 100-continue} in its request.
+   */
+  CONTINUE_ALWAYS
 }
diff --git a/okhttp-tests/src/test/java/okhttp3/CallTest.java b/okhttp-tests/src/test/java/okhttp3/CallTest.java
index d2e48e62f5..1b53ce87f4 100644
--- a/okhttp-tests/src/test/java/okhttp3/CallTest.java
+++ b/okhttp-tests/src/test/java/okhttp3/CallTest.java
@@ -2393,17 +2393,16 @@ private void cancelDuringConnect(String scheme) throws Exception {
 
   @Test public void serverRespondsWithUnsolicited100Continue() throws Exception {
     server.enqueue(new MockResponse()
-        .setStatus("HTTP/1.1 100 Continue"));
+            .setSocketPolicy(SocketPolicy.CONTINUE_ALWAYS));
 
     Request request = new Request.Builder()
-        .url(server.url("/"))
-        .post(RequestBody.create(MediaType.parse("text/plain"), "abc"))
-        .build();
+            .url(server.url("/"))
+            .post(RequestBody.create(MediaType.parse("text/plain"), "abc"))
+            .build();
 
-    Call call = client.newCall(request);
-    Response response = call.execute();
-    assertEquals(100, response.code());
-    assertEquals("", response.body().string());
+    executeSynchronously(request)
+            .assertCode(200)
+            .assertSuccessful();
 
     RecordedRequest recordedRequest = server.takeRequest();
     assertEquals("abc", recordedRequest.getBody().readUtf8());
@@ -2414,6 +2413,31 @@ private void cancelDuringConnect(String scheme) throws Exception {
     serverRespondsWithUnsolicited100Continue();
   }
 
+  @Test public void serverRespondsWith100ContinueOnly() throws Exception {
+    server.enqueue(new MockResponse()
+            .setStatus("HTTP/1.1 100 Continue"));
+
+    Request request = new Request.Builder()
+            .url(server.url("/"))
+            .post(RequestBody.create(MediaType.parse("text/plain"), "abc"))
+            .build();
+
+    Call call = client.newCall(request);
+    try {
+      call.execute();
+      fail();
+    } catch (SocketTimeoutException expected) {
+    }
+
+    RecordedRequest recordedRequest = server.takeRequest();
+    assertEquals("abc", recordedRequest.getBody().readUtf8());
+  }
+
+  @Test public void serverRespondsWith100ContinueOnly_HTTP2() throws Exception {
+    enableProtocol(Protocol.HTTP_2);
+    serverRespondsWith100ContinueOnly();
+  }
+
   @Test public void successfulExpectContinuePermitsConnectionReuse() throws Exception {
     server.enqueue(new MockResponse()
         .setSocketPolicy(SocketPolicy.EXPECT_CONTINUE));
