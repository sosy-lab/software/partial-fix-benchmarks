diff --git a/okhttp/src/main/java/com/squareup/okhttp/Route.java b/okhttp/src/main/java/com/squareup/okhttp/Route.java
index 08963ad864..a08a4699c5 100644
--- a/okhttp/src/main/java/com/squareup/okhttp/Route.java
+++ b/okhttp/src/main/java/com/squareup/okhttp/Route.java
@@ -75,11 +75,6 @@ public boolean isModernTls() {
     return modernTls;
   }
 
-  /** Returns a copy of this route with flipped TLS mode. */
-  Route flipTlsMode() {
-    return new Route(address, proxy, inetSocketAddress, !modernTls);
-  }
-
   @Override public boolean equals(Object obj) {
     if (obj instanceof Route) {
       Route other = (Route) obj;
diff --git a/okhttp/src/main/java/com/squareup/okhttp/RouteDatabase.java b/okhttp/src/main/java/com/squareup/okhttp/RouteDatabase.java
index 9cbeaa73f1..086339f5e6 100644
--- a/okhttp/src/main/java/com/squareup/okhttp/RouteDatabase.java
+++ b/okhttp/src/main/java/com/squareup/okhttp/RouteDatabase.java
@@ -31,14 +31,8 @@
   private final Set<Route> failedRoutes = new LinkedHashSet<Route>();
 
   /** Records a failure connecting to {@code failedRoute}. */
-  public synchronized void failed(Route failedRoute, IOException failure) {
+  public synchronized void failed(Route failedRoute) {
     failedRoutes.add(failedRoute);
-
-    if (!(failure instanceof SSLHandshakeException)) {
-      // If the problem was not related to SSL then it will also fail with
-      // a different TLS mode therefore we can be proactive about it.
-      failedRoutes.add(failedRoute.flipTlsMode());
-    }
   }
 
   /** Records success connecting to {@code failedRoute}. */
diff --git a/okhttp/src/main/java/com/squareup/okhttp/internal/http/RouteSelector.java b/okhttp/src/main/java/com/squareup/okhttp/internal/http/RouteSelector.java
index 1055e4f09f..3f1dc9a2dc 100644
--- a/okhttp/src/main/java/com/squareup/okhttp/internal/http/RouteSelector.java
+++ b/okhttp/src/main/java/com/squareup/okhttp/internal/http/RouteSelector.java
@@ -33,6 +33,7 @@
 import java.util.LinkedList;
 import java.util.List;
 import java.util.NoSuchElementException;
+import javax.net.ssl.SSLHandshakeException;
 
 import static com.squareup.okhttp.internal.Util.getEffectivePort;
 
@@ -148,7 +149,16 @@ public void connectFailed(Connection connection, IOException failure) {
       proxySelector.connectFailed(uri, failedRoute.getProxy().address(), failure);
     }
 
-    routeDatabase.failed(failedRoute, failure);
+    routeDatabase.failed(failedRoute);
+
+    // If the previously returned route's problem was not related to TLS, and
+    // the next route only changes the TLS mode, we shouldn't even attempt it.
+    // This suppresses it in both this selector and also in the route database.
+    if (hasNextTlsMode() && !(failure instanceof SSLHandshakeException)) {
+      boolean modernTls = nextTlsMode() == TLS_MODE_MODERN;
+      Route routeToSuppress = new Route(address, lastProxy, lastInetSocketAddress, modernTls);
+      routeDatabase.failed(routeToSuppress);
+    }
   }
 
   /** Resets {@link #nextProxy} to the first option. */
diff --git a/okhttp/src/test/java/com/squareup/okhttp/internal/http/RouteSelectorTest.java b/okhttp/src/test/java/com/squareup/okhttp/internal/http/RouteSelectorTest.java
index 27812a910a..bbb36d42ab 100644
--- a/okhttp/src/test/java/com/squareup/okhttp/internal/http/RouteSelectorTest.java
+++ b/okhttp/src/test/java/com/squareup/okhttp/internal/http/RouteSelectorTest.java
@@ -107,7 +107,7 @@
     dns.inetAddresses = makeFakeAddresses(255, 1);
     Connection connection = routeSelector.next("GET");
     RouteDatabase routeDatabase = new RouteDatabase();
-    routeDatabase.failed(connection.getRoute(), new IOException());
+    routeDatabase.failed(connection.getRoute());
     routeSelector = new RouteSelector(address, uri, proxySelector, pool, dns, routeDatabase);
     assertConnection(routeSelector.next("GET"), address, NO_PROXY, dns.inetAddresses[0], uriPort,
         false);
@@ -281,6 +281,7 @@
     assertFalse(routeSelector.hasNext());
   }
 
+  // https://github.com/square/okhttp/issues/442
   @Test public void nonSslErrorAddsAllTlsModesToFailedRoute() throws Exception {
     Address address = new Address(uriHost, uriPort, socketFactory, hostnameVerifier, authenticator,
         Proxy.NO_PROXY, transports);
@@ -292,6 +293,7 @@
     Connection connection = routeSelector.next("GET");
     routeSelector.connectFailed(connection, new IOException("Non SSL exception"));
     assertTrue(routeDatabase.failedRoutesCount() == 2);
+    assertFalse(routeSelector.hasNext());
   }
 
   @Test public void sslErrorAddsOnlyFailedTlsModeToFailedRoute() throws Exception {
@@ -305,6 +307,7 @@
     Connection connection = routeSelector.next("GET");
     routeSelector.connectFailed(connection, new SSLHandshakeException("SSL exception"));
     assertTrue(routeDatabase.failedRoutesCount() == 1);
+    assertTrue(routeSelector.hasNext());
   }
 
   @Test public void multipleProxiesMultipleInetAddressesMultipleTlsModes() throws Exception {
@@ -372,7 +375,7 @@
     // Check that we do indeed have more than one route.
     assertTrue(regularRoutes.size() > 1);
     // Add first regular route as failed.
-    routeDatabase.failed(regularRoutes.get(0).getRoute(), new SSLHandshakeException("none"));
+    routeDatabase.failed(regularRoutes.get(0).getRoute());
     // Reset selector
     routeSelector = new RouteSelector(address, uri, proxySelector, pool, dns, routeDatabase);
 
