diff --git a/okhttp/src/main/java/okhttp3/internal/connection/ExchangeFinder.java b/okhttp/src/main/java/okhttp3/internal/connection/ExchangeFinder.java
index 08c804d707..ef741bd1eb 100644
--- a/okhttp/src/main/java/okhttp3/internal/connection/ExchangeFinder.java
+++ b/okhttp/src/main/java/okhttp3/internal/connection/ExchangeFinder.java
@@ -258,11 +258,16 @@ void trackFailure() {
     }
   }
 
-  boolean canRetry() {
+  /** Returns true if there is a failure that retrying might fix. */
+  boolean hasStreamFailure() {
     synchronized (connectionPool) {
-      // Don't try if the failure wasn't our fault!
-      if (!hasStreamFailure) return false;
+      return hasStreamFailure;
+    }
+  }
 
+  /** Returns true if a current route is still good or if there are routes we haven't tried yet. */
+  boolean hasRouteToTry() {
+    synchronized (connectionPool) {
       return retryCurrentRoute()
           || (routeSelection != null && routeSelection.hasNext())
           || routeSelector.hasNext();
diff --git a/okhttp/src/main/java/okhttp3/internal/connection/Transmitter.java b/okhttp/src/main/java/okhttp3/internal/connection/Transmitter.java
index bc1cc1f87b..f596a41d72 100644
--- a/okhttp/src/main/java/okhttp3/internal/connection/Transmitter.java
+++ b/okhttp/src/main/java/okhttp3/internal/connection/Transmitter.java
@@ -123,7 +123,9 @@ public void callStart() {
    */
   public void prepareToConnect(Request request) {
     if (this.request != null) {
-      if (sameConnection(this.request.url(), request.url())) return; // Already ready.
+      if (sameConnection(this.request.url(), request.url()) && exchangeFinder.hasRouteToTry()) {
+        return; // Already ready.
+      }
       if (exchange != null) throw new IllegalStateException();
 
       if (exchangeFinder != null) {
@@ -303,7 +305,7 @@ public void exchangeDoneDueToException() {
   }
 
   public boolean canRetry() {
-    return exchangeFinder.canRetry();
+    return exchangeFinder.hasStreamFailure() && exchangeFinder.hasRouteToTry();
   }
 
   public boolean hasExchange() {
diff --git a/okhttp/src/test/java/okhttp3/CallTest.java b/okhttp/src/test/java/okhttp3/CallTest.java
index 194a0163ac..5ee7ff24c8 100644
--- a/okhttp/src/test/java/okhttp3/CallTest.java
+++ b/okhttp/src/test/java/okhttp3/CallTest.java
@@ -910,6 +910,32 @@ private void postBodyRetransmittedAfterAuthorizationFail(String body) throws Exc
         .assertBody("success!");
   }
 
+  /** https://github.com/square/okhttp/issues/4875 */
+  @Test public void interceptorRecoversWhenRoutesExhausted() throws Exception {
+    server.enqueue(new MockResponse()
+        .setSocketPolicy(SocketPolicy.DISCONNECT_AT_START));
+    server.enqueue(new MockResponse());
+
+    client = client.newBuilder()
+        .addInterceptor(new Interceptor() {
+          @Override public Response intercept(Chain chain) throws IOException {
+            try {
+              chain.proceed(chain.request());
+              throw new AssertionError();
+            } catch (IOException expected) {
+              return chain.proceed(chain.request());
+            }
+          }
+        })
+        .build();
+
+    Request request = new Request.Builder()
+        .url(server.url("/"))
+        .build();
+    executeSynchronously(request)
+        .assertCode(200);
+  }
+
   /**
    * Make a request with two routes. The first route will fail because the null server connects but
    * never responds. The manual retry will succeed.
