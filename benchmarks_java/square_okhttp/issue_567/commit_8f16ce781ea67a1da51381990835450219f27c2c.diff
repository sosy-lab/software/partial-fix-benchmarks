diff --git a/okhttp-tests/src/test/java/com/squareup/okhttp/internal/http/HttpOverSpdyTest.java b/okhttp-tests/src/test/java/com/squareup/okhttp/internal/http/HttpOverSpdyTest.java
index 0109729389..d9c717b175 100644
--- a/okhttp-tests/src/test/java/com/squareup/okhttp/internal/http/HttpOverSpdyTest.java
+++ b/okhttp-tests/src/test/java/com/squareup/okhttp/internal/http/HttpOverSpdyTest.java
@@ -52,6 +52,7 @@
 import org.junit.Ignore;
 import org.junit.Test;
 
+import static java.util.concurrent.TimeUnit.SECONDS;
 import static org.junit.Assert.assertArrayEquals;
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertNull;
@@ -286,6 +287,52 @@ public boolean verify(String hostname, SSLSession session) {
     assertContent("A", connection, Integer.MAX_VALUE);
   }
 
+  /**
+   * Test to ensure we don't throw a read timeout on responses that are
+   * progressing slowly.  For this case, we take a large frame, ~16Kib, and
+   * throttle it to 1KiB/second.  We set the read timeout to 5 seconds.  If
+   * our implementation is acting correctly, it will not throw, as eventhough
+   * the frame isn't completely read in 5 seconds, it is progressing.
+   */
+  @Test public void readTimeoutMoreGranularThanFrameSize() throws Exception {
+    char[] body = new char[16383]; // Largest http/2 frame = 16KiB - 1
+    Arrays.fill(body, 'y');
+    server.enqueue(new MockResponse()
+        .setBody(new String(body))
+        .throttleBody(1024, 1, SECONDS)); // slow connection 1KiB/second
+    server.play();
+
+    connection = client.open(server.getUrl("/"));
+    connection.setReadTimeout(5000); // 5 seconds to read something.
+    assertContent(new String(body), connection, Integer.MAX_VALUE);
+  }
+
+  /**
+   * Test to ensure we throw a read timeout on responses that are progressing
+   * too slowly.  For this case, we take a 2Kib frame and throttle it to
+   * 1KiB/second.  We set the read timeout to half a second.  If our
+   * implementation is acting correctly, it will throw, as a byte doesn't
+   * arrive in time.
+   */
+  @Test public void readTimeoutOnSlowConnection() throws Exception {
+    char[] body = new char[2048]; // 2KiB to read
+    Arrays.fill(body, 'y');
+    server.enqueue(new MockResponse()
+        .setBody(new String(body))
+        .throttleBody(1024, 1, SECONDS)); // slow connection 1KiB/second
+    server.play();
+
+    connection = client.open(server.getUrl("/"));
+    connection.setReadTimeout(500); // half a second to read something
+    connection.connect();
+    try {
+      readAscii(connection.getInputStream(), Integer.MAX_VALUE);
+      fail("Should have timed out!");
+    } catch (IOException e){
+      assertEquals("Read timed out", e.getMessage());
+    }
+  }
+
   @Test public void spdyConnectionTimeout() throws Exception {
     MockResponse response = new MockResponse().setBody("A");
     response.setBodyDelayTimeMs(1000);
