diff --git a/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpURLConnectionImpl.java b/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpURLConnectionImpl.java
index 5337e74941..b51bbc6c60 100644
--- a/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpURLConnectionImpl.java
+++ b/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpURLConnectionImpl.java
@@ -18,6 +18,7 @@
 package com.squareup.okhttp.internal.http;
 
 import com.squareup.okhttp.Connection;
+import com.squareup.okhttp.Handshake;
 import com.squareup.okhttp.Headers;
 import com.squareup.okhttp.OkHttpClient;
 import com.squareup.okhttp.Protocol;
@@ -87,6 +88,12 @@
    */
   private Route route;
 
+  /**
+   * The most recently received TLS handshake. This will be null if we haven't
+   * connected yet, or if the most recent connection was HTTP (and not HTTPS).
+   */
+  Handshake handshake;
+
   public HttpURLConnectionImpl(URL url, OkHttpClient client) {
     super(url);
     this.client = client;
@@ -364,6 +371,9 @@ private boolean execute(boolean readResponse) throws IOException {
     try {
       httpEngine.sendRequest();
       route = httpEngine.getRoute();
+      handshake = httpEngine.getConnection() != null
+          ? httpEngine.getConnection().getHandshake()
+          : null;
       if (readResponse) {
         httpEngine.readResponse();
       }
diff --git a/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpsURLConnectionImpl.java b/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpsURLConnectionImpl.java
index a4fbeb558f..827ebd7c5e 100644
--- a/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpsURLConnectionImpl.java
+++ b/okhttp/src/main/java/com/squareup/okhttp/internal/http/HttpsURLConnectionImpl.java
@@ -76,7 +76,13 @@ private Handshake handshake() {
     if (delegate.httpEngine == null) {
       throw new IllegalStateException("Connection has not yet been established");
     }
-    return delegate.httpEngine.getResponse().handshake();
+
+    // If there's a response, get the handshake from there so that caching
+    // works. Otherwise get the handshake from the connection because we might
+    // have not connected yet.
+    return delegate.httpEngine.hasResponse()
+        ? delegate.httpEngine.getResponse().handshake()
+        : delegate.handshake;
   }
 
   @Override public void disconnect() {
diff --git a/okhttp/src/test/java/com/squareup/okhttp/internal/http/URLConnectionTest.java b/okhttp/src/test/java/com/squareup/okhttp/internal/http/URLConnectionTest.java
index 173b9edd6e..1cb3d29f60 100644
--- a/okhttp/src/test/java/com/squareup/okhttp/internal/http/URLConnectionTest.java
+++ b/okhttp/src/test/java/com/squareup/okhttp/internal/http/URLConnectionTest.java
@@ -87,6 +87,7 @@
 import static java.util.concurrent.TimeUnit.NANOSECONDS;
 import static org.junit.Assert.assertEquals;
 import static org.junit.Assert.assertFalse;
+import static org.junit.Assert.assertNotNull;
 import static org.junit.Assert.assertNull;
 import static org.junit.Assert.assertTrue;
 import static org.junit.Assert.fail;
@@ -530,6 +531,36 @@ private void doUpload(TransferKind uploadKind, WriteKind writeKind) throws Excep
     assertEquals("GET /foo HTTP/1.1", request.getRequestLine());
   }
 
+  @Test public void inspectHandshakeThroughoutRequestLifecycle() throws Exception {
+    server.useHttps(sslContext.getSocketFactory(), false);
+    server.enqueue(new MockResponse());
+    server.play();
+
+    client.setSslSocketFactory(sslContext.getSocketFactory());
+    client.setHostnameVerifier(new RecordingHostnameVerifier());
+
+    HttpsURLConnection httpsConnection = (HttpsURLConnection) client.open(server.getUrl("/foo"));
+
+    // Prior to calling connect(), getting the cipher suite is forbidden.
+    try {
+      httpsConnection.getCipherSuite();
+      fail();
+    } catch (IllegalStateException expected) {
+    }
+
+    // Calling connect establishes a handshake...
+    httpsConnection.connect();
+    assertNotNull(httpsConnection.getCipherSuite());
+
+    // ...which remains after we read the response body...
+    assertContent("", httpsConnection);
+    assertNotNull(httpsConnection.getCipherSuite());
+
+    // ...and after we disconnect.
+    httpsConnection.disconnect();
+    assertNotNull(httpsConnection.getCipherSuite());
+  }
+
   @Test public void connectViaHttpsReusingConnections() throws IOException, InterruptedException {
     server.useHttps(sslContext.getSocketFactory(), false);
     server.enqueue(new MockResponse().setBody("this response comes via HTTPS"));
