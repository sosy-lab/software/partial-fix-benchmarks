diff --git a/pom.xml b/pom.xml
index 9c9272b32..7d054e72a 100644
--- a/pom.xml
+++ b/pom.xml
@@ -65,6 +65,7 @@
     <wire.version>2.2.0</wire.version>
     <simplexml.version>2.7.1</simplexml.version>
     <moshi.version>1.5.0</moshi.version>
+    <jaxb.version>2.2.12</jaxb.version><!-- 2.3.0 breaks due to https://github.com/mojohaus/animal-sniffer/issues/29 -->
 
     <!-- Sample Dependencies -->
     <jsoup.version>1.7.3</jsoup.version>
@@ -157,6 +158,11 @@
         <artifactId>moshi</artifactId>
         <version>${moshi.version}</version>
       </dependency>
+      <dependency>
+        <groupId>javax.xml.bind</groupId>
+        <artifactId>jaxb-api</artifactId>
+        <version>${jaxb.version}</version>
+      </dependency>
       <dependency>
         <groupId>org.scala-lang</groupId>
         <artifactId>scala-library</artifactId>
diff --git a/retrofit-converters/jaxb/README.md b/retrofit-converters/jaxb/README.md
new file mode 100644
index 000000000..e35949857
--- /dev/null
+++ b/retrofit-converters/jaxb/README.md
@@ -0,0 +1,33 @@
+JAXB Converter
+==============
+
+A `Converter` which uses [JAXB][1] for serialization to and from XML.
+
+A default `JAXBContext` instance will be created or one can be configured and passed
+to `JaxbConverterFactory.create()` to further control the serialization.
+
+
+Download
+--------
+
+Download [the latest JAR][2] or grab via [Maven][3]:
+```xml
+<dependency>
+  <groupId>com.squareup.retrofit2</groupId>
+  <artifactId>converter-jaxb</artifactId>
+  <version>latest.version</version>
+</dependency>
+```
+or [Gradle][3]:
+```groovy
+compile 'com.squareup.retrofit2:converter-jaxb:latest.version'
+```
+
+Snapshots of the development version are available in [Sonatype's `snapshots` repository][snap].
+
+
+
+ [1]: https://github.com/javaee/jaxb-v2
+ [2]: https://search.maven.org/remote_content?g=com.squareup.retrofit2&a=converter-jaxb&v=LATEST
+ [3]: http://search.maven.org/#search%7Cga%7C1%7Cg%3A%22com.squareup.retrofit2%22%20a%3A%22converter-jaxb%22
+ [snap]: https://oss.sonatype.org/content/repositories/snapshots/
diff --git a/retrofit-converters/jaxb/pom.xml b/retrofit-converters/jaxb/pom.xml
new file mode 100644
index 000000000..ba99b93e0
--- /dev/null
+++ b/retrofit-converters/jaxb/pom.xml
@@ -0,0 +1,48 @@
+<?xml version="1.0" encoding="UTF-8"?>
+
+<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/maven-v4_0_0.xsd">
+  <modelVersion>4.0.0</modelVersion>
+
+  <parent>
+    <groupId>com.squareup.retrofit2</groupId>
+    <artifactId>retrofit-converters</artifactId>
+    <version>2.4.0-SNAPSHOT</version>
+    <relativePath>../pom.xml</relativePath>
+  </parent>
+
+  <artifactId>converter-jaxb</artifactId>
+  <name>Converter: JAXB</name>
+
+  <dependencies>
+    <dependency>
+      <groupId>${project.groupId}</groupId>
+      <artifactId>retrofit</artifactId>
+      <version>${project.version}</version>
+    </dependency>
+    <dependency>
+      <groupId>javax.xml.bind</groupId>
+      <artifactId>jaxb-api</artifactId>
+    </dependency>
+    <dependency>
+      <groupId>com.google.code.findbugs</groupId>
+      <artifactId>jsr305</artifactId>
+      <scope>provided</scope>
+    </dependency>
+
+    <dependency>
+      <groupId>junit</groupId>
+      <artifactId>junit</artifactId>
+      <scope>test</scope>
+    </dependency>
+    <dependency>
+      <groupId>com.squareup.okhttp3</groupId>
+      <artifactId>mockwebserver</artifactId>
+      <scope>test</scope>
+    </dependency>
+    <dependency>
+      <groupId>org.assertj</groupId>
+      <artifactId>assertj-core</artifactId>
+      <scope>test</scope>
+    </dependency>
+  </dependencies>
+</project>
diff --git a/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbConverterFactory.java b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbConverterFactory.java
new file mode 100644
index 000000000..5537aeb4c
--- /dev/null
+++ b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbConverterFactory.java
@@ -0,0 +1,87 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+import java.lang.annotation.Annotation;
+import java.lang.reflect.Type;
+import javax.annotation.Nullable;
+import javax.xml.bind.JAXBContext;
+import javax.xml.bind.JAXBException;
+import javax.xml.bind.ValidationEvent;
+import javax.xml.bind.ValidationEventHandler;
+import javax.xml.bind.annotation.XmlRootElement;
+import okhttp3.MediaType;
+import okhttp3.RequestBody;
+import okhttp3.ResponseBody;
+import retrofit2.Converter;
+import retrofit2.Retrofit;
+
+/**
+ * A {@linkplain Converter.Factory converter} which uses JAXB for XML. All validation events are
+ * ignored.
+ */
+public final class JaxbConverterFactory extends Converter.Factory {
+  static final ValidationEventHandler IGNORE_UNRECOGNIZED_FIELDS = new ValidationEventHandler() {
+    @Override public boolean handleEvent(ValidationEvent event) {
+      return true;
+    }
+  };
+
+  static final MediaType XML = MediaType.parse("application/xml; charset=utf-8");
+
+  /** Create an instance using a default {@link JAXBContext} instance for conversion. */
+  public static JaxbConverterFactory create() {
+    return new JaxbConverterFactory(null);
+  }
+
+  /** Create an instance using {@code context} for conversion. */
+  @SuppressWarnings("ConstantConditions") // Guarding public API nullability.
+  public static JaxbConverterFactory create(JAXBContext context) {
+    if (context == null) throw new NullPointerException("context == null");
+    return new JaxbConverterFactory(context);
+  }
+
+  /** If null, a new JAXB context will be created for each type to be converted. */
+  private final @Nullable JAXBContext context;
+
+  private JaxbConverterFactory(@Nullable JAXBContext context) {
+    this.context = context;
+  }
+
+  @Override public Converter<?, RequestBody> requestBodyConverter(Type type,
+      Annotation[] parameterAnnotations, Annotation[] methodAnnotations, Retrofit retrofit) {
+    if (type instanceof Class && ((Class<?>) type).isAnnotationPresent(XmlRootElement.class)) {
+      return new JaxbRequestConverter<>(contextForType((Class<?>) type), (Class<?>) type);
+    }
+    return null;
+  }
+
+  @Override public Converter<ResponseBody, ?> responseBodyConverter(
+      Type type, Annotation[] annotations, Retrofit retrofit) {
+    if (type instanceof Class && ((Class<?>) type).isAnnotationPresent(XmlRootElement.class)) {
+      return new JaxbResponseConverter<>(contextForType((Class<?>) type), (Class<?>) type);
+    }
+    return null;
+  }
+
+  private JAXBContext contextForType(Class<?> type) {
+    try {
+      return context != null ? context : JAXBContext.newInstance(type);
+    } catch (JAXBException e) {
+      throw new IllegalArgumentException(e);
+    }
+  }
+}
diff --git a/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbRequestConverter.java b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbRequestConverter.java
new file mode 100644
index 000000000..3e88c2f84
--- /dev/null
+++ b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbRequestConverter.java
@@ -0,0 +1,62 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+import java.io.IOException;
+import javax.xml.bind.JAXBContext;
+import javax.xml.bind.JAXBException;
+import javax.xml.bind.Marshaller;
+import javax.xml.stream.XMLOutputFactory;
+import javax.xml.stream.XMLStreamException;
+import javax.xml.stream.XMLStreamWriter;
+import okhttp3.MediaType;
+import okhttp3.RequestBody;
+import okio.BufferedSink;
+import retrofit2.Converter;
+
+import static retrofit2.converter.jaxb.JaxbConverterFactory.IGNORE_UNRECOGNIZED_FIELDS;
+
+final class JaxbRequestConverter<T> implements Converter<T, RequestBody> {
+  final XMLOutputFactory xmlOutputFactory = XMLOutputFactory.newInstance();
+  final JAXBContext context;
+  final Class<T> type;
+
+  JaxbRequestConverter(JAXBContext context, Class<T> type) {
+    this.context = context;
+    this.type = type;
+  }
+
+  @Override public RequestBody convert(final T value) throws IOException {
+    return new RequestBody() {
+      @Override public MediaType contentType() {
+        return JaxbConverterFactory.XML;
+      }
+
+      @Override public void writeTo(BufferedSink sink) throws IOException {
+        try {
+          Marshaller marshaller = context.createMarshaller();
+          marshaller.setEventHandler(IGNORE_UNRECOGNIZED_FIELDS);
+
+          XMLStreamWriter xmlWriter = xmlOutputFactory.createXMLStreamWriter(
+              sink.outputStream(), JaxbConverterFactory.XML.charset().name());
+          marshaller.marshal(value, xmlWriter);
+        } catch (JAXBException | XMLStreamException e) {
+          throw new XmlDataException(e);
+        }
+      }
+    };
+  }
+}
diff --git a/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbResponseConverter.java b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbResponseConverter.java
new file mode 100644
index 000000000..ce43f2c2f
--- /dev/null
+++ b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/JaxbResponseConverter.java
@@ -0,0 +1,50 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+import java.io.IOException;
+import javax.xml.bind.JAXBContext;
+import javax.xml.bind.JAXBException;
+import javax.xml.bind.Unmarshaller;
+import javax.xml.stream.XMLInputFactory;
+import javax.xml.stream.XMLStreamException;
+import javax.xml.stream.XMLStreamReader;
+import okhttp3.ResponseBody;
+import retrofit2.Converter;
+
+import static retrofit2.converter.jaxb.JaxbConverterFactory.IGNORE_UNRECOGNIZED_FIELDS;
+
+final class JaxbResponseConverter<T> implements Converter<ResponseBody, T> {
+  final XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
+  final JAXBContext context;
+  final Class<T> type;
+
+  JaxbResponseConverter(JAXBContext context, Class<T> type) {
+    this.context = context;
+    this.type = type;
+  }
+
+  @Override public T convert(ResponseBody value) throws IOException {
+    try {
+      Unmarshaller unmarshaller = context.createUnmarshaller();
+      unmarshaller.setEventHandler(IGNORE_UNRECOGNIZED_FIELDS);
+      XMLStreamReader streamReader = xmlInputFactory.createXMLStreamReader(value.charStream());
+      return unmarshaller.unmarshal(streamReader, type).getValue();
+    } catch (JAXBException | XMLStreamException e) {
+      throw new XmlDataException(e);
+    }
+  }
+}
diff --git a/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/XmlDataException.java b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/XmlDataException.java
new file mode 100644
index 000000000..bc1c99f80
--- /dev/null
+++ b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/XmlDataException.java
@@ -0,0 +1,22 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+final class XmlDataException extends RuntimeException {
+  XmlDataException(Throwable cause) {
+    super(cause);
+  }
+}
diff --git a/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/package-info.java b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/package-info.java
new file mode 100644
index 000000000..7aebe7e7b
--- /dev/null
+++ b/retrofit-converters/jaxb/src/main/java/retrofit2/converter/jaxb/package-info.java
@@ -0,0 +1,4 @@
+@ParametersAreNonnullByDefault
+package retrofit2.converter.jaxb;
+
+import javax.annotation.ParametersAreNonnullByDefault;
diff --git a/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/Contact.java b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/Contact.java
new file mode 100644
index 000000000..78e523456
--- /dev/null
+++ b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/Contact.java
@@ -0,0 +1,51 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+import java.util.ArrayList;
+import java.util.Arrays;
+import java.util.List;
+import javax.xml.bind.annotation.XmlElement;
+import javax.xml.bind.annotation.XmlRootElement;
+
+@XmlRootElement(name = "contact")
+final class Contact {
+  @XmlElement(required = true)
+  public final String name;
+
+  @XmlElement(name = "phone_number")
+  public final List<PhoneNumber> phone_numbers;
+
+  @SuppressWarnings("unused") // Used by JAXB.
+  private Contact() {
+    this("", new ArrayList<PhoneNumber>());
+  }
+
+  public Contact(String name, List<PhoneNumber> phoneNumbers) {
+    this.name = name;
+    this.phone_numbers = phoneNumbers;
+  }
+
+  @Override public boolean equals(Object o) {
+    return o instanceof Contact
+        && ((Contact) o).name.equals(name)
+        && ((Contact) o).phone_numbers.equals(phone_numbers);
+  }
+
+  @Override public int hashCode() {
+    return Arrays.asList(name, phone_numbers).hashCode();
+  }
+}
diff --git a/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/JaxbConverterFactoryTest.java b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/JaxbConverterFactoryTest.java
new file mode 100644
index 000000000..b74f21fb6
--- /dev/null
+++ b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/JaxbConverterFactoryTest.java
@@ -0,0 +1,132 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+import java.util.Collections;
+import javax.xml.bind.JAXBContext;
+import okhttp3.mockwebserver.MockResponse;
+import okhttp3.mockwebserver.MockWebServer;
+import okhttp3.mockwebserver.RecordedRequest;
+import org.junit.Before;
+import org.junit.Rule;
+import org.junit.Test;
+import retrofit2.Call;
+import retrofit2.Response;
+import retrofit2.Retrofit;
+import retrofit2.http.Body;
+import retrofit2.http.GET;
+import retrofit2.http.POST;
+
+import static junit.framework.TestCase.fail;
+import static org.assertj.core.api.Assertions.assertThat;
+
+public final class JaxbConverterFactoryTest {
+  static final Contact SAMPLE_CONTACT = new Contact("Jenny",
+      Collections.singletonList(new PhoneNumber("867-5309", Type.MOBILE)));
+
+  static final String SAMPLE_CONTACT_XML = ""
+      + "<?xml version=\"1.0\" ?>"
+      + "<contact>"
+      + "<name>Jenny</name>"
+      + "<phone_number type=\"MOBILE\">"
+      + "<number>867-5309</number>"
+      + "</phone_number>"
+      + "</contact>";
+
+  interface Service {
+    @POST("/") Call<Void> postXml(@Body Contact contact);
+    @GET("/") Call<Contact> getXml();
+  }
+
+  @Rule public final MockWebServer server = new MockWebServer();
+
+  private Service service;
+
+  @Before public void setUp() {
+    JaxbConverterFactory factory = JaxbConverterFactory.create();
+    Retrofit retrofit = new Retrofit.Builder()
+        .baseUrl(server.url("/"))
+        .addConverterFactory(factory)
+        .build();
+    service = retrofit.create(Service.class);
+  }
+
+  @Test public void xmlRequestBody() throws Exception {
+    server.enqueue(new MockResponse());
+
+    Call<Void> call = service.postXml(SAMPLE_CONTACT);
+    call.execute();
+
+    RecordedRequest request = server.takeRequest();
+    assertThat(request.getHeader("Content-Type")).isEqualTo("application/xml; charset=utf-8");
+    assertThat(request.getBody().readUtf8()).isEqualTo(SAMPLE_CONTACT_XML);
+  }
+
+  @Test public void xmlResponseBody() throws Exception {
+    server.enqueue(new MockResponse()
+        .setBody(SAMPLE_CONTACT_XML));
+
+    Call<Contact> call = service.getXml();
+    Response<Contact> response = call.execute();
+    assertThat(response.body()).isEqualTo(SAMPLE_CONTACT);
+  }
+
+  @Test public void characterEncoding() throws Exception {
+    server.enqueue(new MockResponse()
+        .setBody(""
+            + "<?xml version=\"1.0\" ?>"
+            + "<contact>"
+            + "<name>Бронтозавр \uD83E\uDD95 ティラノサウルス・レックス &#129430;</name>"
+            + "</contact>"));
+
+    Call<Contact> call = service.getXml();
+    Response<Contact> response = call.execute();
+    assertThat(response.body().name)
+        .isEqualTo("Бронтозавр \uD83E\uDD95 ティラノサウルス・レックス \uD83E\uDD96");
+  }
+
+  @Test public void userSuppliedJaxbContext() throws Exception {
+    JAXBContext context = JAXBContext.newInstance(Contact.class);
+    JaxbConverterFactory factory = JaxbConverterFactory.create(context);
+    Retrofit retrofit = new Retrofit.Builder()
+        .baseUrl(server.url("/"))
+        .addConverterFactory(factory)
+        .build();
+    service = retrofit.create(Service.class);
+
+    server.enqueue(new MockResponse());
+
+    Call<Void> call = service.postXml(SAMPLE_CONTACT);
+    call.execute();
+
+    RecordedRequest request = server.takeRequest();
+    assertThat(request.getHeader("Content-Type")).isEqualTo("application/xml; charset=utf-8");
+    assertThat(request.getBody().readUtf8()).isEqualTo(SAMPLE_CONTACT_XML);
+  }
+
+  @Test public void malformedXml() throws Exception {
+    server.enqueue(new MockResponse()
+        .setBody("This is not XML"));
+
+    Call<Contact> call = service.getXml();
+    try {
+      call.execute();
+      fail();
+    } catch (XmlDataException expected) {
+      assertThat(expected).hasMessageContaining("ParseError");
+    }
+  }
+}
diff --git a/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/PhoneNumber.java b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/PhoneNumber.java
new file mode 100644
index 000000000..927f0e8f1
--- /dev/null
+++ b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/PhoneNumber.java
@@ -0,0 +1,49 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+import java.util.Arrays;
+import javax.annotation.Nullable;
+import javax.xml.bind.annotation.XmlAttribute;
+import javax.xml.bind.annotation.XmlElement;
+
+final class PhoneNumber {
+  @XmlElement(required = true)
+  public final String number;
+
+  @XmlAttribute
+  public final Type type;
+
+  @SuppressWarnings("unused") // Used by JAXB.
+  private PhoneNumber() {
+    this("", Type.OTHER);
+  }
+
+  PhoneNumber(String number, @Nullable Type type) {
+    this.number = number;
+    this.type = type;
+  }
+
+  @Override public boolean equals(Object o) {
+    return o instanceof PhoneNumber
+        && ((PhoneNumber) o).number.equals(number)
+        && ((PhoneNumber) o).type.equals(type);
+  }
+
+  @Override public int hashCode() {
+    return Arrays.asList(number, type).hashCode();
+  }
+}
diff --git a/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/Type.java b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/Type.java
new file mode 100644
index 000000000..e8b93d2e6
--- /dev/null
+++ b/retrofit-converters/jaxb/src/test/java/retrofit2/converter/jaxb/Type.java
@@ -0,0 +1,20 @@
+/*
+ * Copyright (C) 2018 Square, Inc.
+ *
+ * Licensed under the Apache License, Version 2.0 (the "License");
+ * you may not use this file except in compliance with the License.
+ * You may obtain a copy of the License at
+ *
+ *      http://www.apache.org/licenses/LICENSE-2.0
+ *
+ * Unless required by applicable law or agreed to in writing, software
+ * distributed under the License is distributed on an "AS IS" BASIS,
+ * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
+ * See the License for the specific language governing permissions and
+ * limitations under the License.
+ */
+package retrofit2.converter.jaxb;
+
+enum Type {
+  OTHER, MOBILE
+}
diff --git a/retrofit-converters/pom.xml b/retrofit-converters/pom.xml
index c6258b38c..875802b69 100644
--- a/retrofit-converters/pom.xml
+++ b/retrofit-converters/pom.xml
@@ -24,5 +24,6 @@
     <module>simplexml</module>
     <module>scalars</module>
     <module>moshi</module>
+    <module>jaxb</module>
   </modules>
 </project>
