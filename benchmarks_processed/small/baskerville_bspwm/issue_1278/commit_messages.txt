DESKTOP_SEL: discard hashes within `MONITOR_SEL:`

Fixes #1267.
Fix #1278: find colon after truncating desc
Discard colons within refs in `desktop_from_desc`

Fixes #1278.
Closes #1282.
