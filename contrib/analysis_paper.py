#!/usr/bin/env python3

import argparse
from functools import reduce
from pathlib import Path
from typing import Tuple, List

import pandas as pd

keywords = [
    "build problem",
    "null pointer",
    "control-flow",
    "preprocessing directive",
    "hardware and OS related",
    "wrong API usage",
    "race condition",
    "performance",
    "memory leak",
    "new feature",
    "unrelated bug",
    "reverted commit",
    "regression",
    "introduces bug",
    "same fix different location",
    "code quality",
    "data types",
    "segmentation fault",
    "work in progress",
    "license issues",
    "library issues",
    "termination",
    "crashes",
]


class TexMacro:
    def __init__(self, name: str, value: str):
        self.name = name
        self.value = value

    def __str__(self):
        return f"\\newcommand{{\\{self.name}}}{{{self.value}}}"


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("tsv_files", nargs="+", help="tsv files to be merged")
    parser.add_argument("--labeled-file", help="file with labeled data")
    parser.add_argument("--output", help="output file")
    return parser.parse_args()


def analyze_labeled_data(merged_dataframe, file_path: Path) -> List[TexMacro]:
    labeled_dataframe = pd.read_csv(file_path, sep="\t")
    macros = []
    merged_dataframe = merged_dataframe[merged_dataframe["partial"]]

    labeled_in_merged = labeled_dataframe.merge(
        merged_dataframe[["task", "partial"]], on=["task", "partial"], how="inner"
    )

    keywords_to_count = {k.lower(): 0 for k in keywords}

    classes = list(labeled_in_merged[labeled_in_merged["partial"]]["reason"])
    classes = [c.lower().replace("'", "") for c in classes]
    for c in classes:
        if len(c) == 0:
            continue

        for s in c.split(","):
            found = False
            for k in keywords:
                k = k.lower()
                if k in s:
                    keywords_to_count[k] += 1
                    found = True
            if not found:
                print(f"Could not find keyword in: {c}")

    for k, v in keywords_to_count.items():
        macros.append(TexMacro(k.replace(" ", "").replace("-", "") + "Amount", str(v)))

    return macros


def analyze_merged_data(files: list[Path]) -> Tuple[pd.DataFrame, List[TexMacro]]:
    merged_dataset = reduce(
        lambda left, right: pd.merge(left, right, on=["task", "partial"], how="inner"),
        map(lambda file: pd.read_csv(file, sep="\t"), files),
    )
    macros = [
        TexMacro("totalLabeled", str(len(merged_dataset))),
        TexMacro("truePartialFixes", str(sum(merged_dataset["partial"]))),
        TexMacro("falsePartialFixes", str(sum(~merged_dataset["partial"]))),
    ]

    return merged_dataset, macros


def main():
    args = parse_args()
    merged_dataframe, macros = analyze_merged_data(args.tsv_files)
    macros_labeled_data = []
    if args.labeled_file:
        macros_labeled_data = analyze_labeled_data(merged_dataframe, args.labeled_file)

    Path(args.output).write_text("\n".join(map(str, macros + macros_labeled_data)))


if __name__ == "__main__":
    main()
