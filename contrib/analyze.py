import argparse
import json
import sys
import click
import subprocess
import pandas as pd

# ATTENTION: contrary to the IDE's warning, the following import is actually used with input() on linux machines
import readline

from rich.console import Console
from rich.markdown import Markdown

from pathlib import Path
from unidiff import PatchSet
from unidiff.errors import UnidiffParseError

from pygments import highlight
from pygments.lexers import get_lexer_by_name
from pygments.formatters import TerminalFormatter

from colorama import init, Fore, Style

# Initialize colorama for colored text
init(autoreset=True)


def print_congratulations():
    print(
        Fore.CYAN
        + Style.BRIGHT
        + """
  _  __                  _____       _             _ 
 | |/ /                 / ____|     (_)           | |
 | ' / ___  ___ _ __   | |  __  ___  _ _ __   __ _| |
 |  < / _ \/ _ \ '_ \  | | |_ |/ _ \| | '_ \ / _` | |
 | . \  __/  __/ |_) | | |__| | (_) | | | | | (_| |_|
 |_|\_\___|\___| .__/   \_____|\___/|_|_| |_|\__, (_)
               | |                            __/ |  
               |_|                           |___/   
"""
        + Fore.YELLOW
        + Style.BRIGHT
        + "\nCongratulations! You managed to classify another 100 partial fixes!\n"
        + Style.RESET_ALL
    )


def print_markdown(code, beautiful=False):
    console = Console()
    md = Markdown(code)
    if beautiful:
        console.print(md)
    else:
        with console.pager():
            console.print(md)


def print_with_syntax_highlighting(code, style="diff"):
    lexer = get_lexer_by_name(style, stripall=True)
    formatter = TerminalFormatter()

    highlighted_code = highlight(code, lexer, formatter)
    click.echo_via_pager(highlighted_code)


def parse_arguments():
    parser = argparse.ArgumentParser(
        description="Analyze the output of the diff scanner."
    )
    parser.add_argument("path", type=Path, help="Path to list of info jsons.")
    parser.add_argument("out", type=Path, help="Path to output file.")
    parser.add_argument(
        "--validation",
        type=Path,
        help="Path to validation file.",
        default=None,
    )
    return parser.parse_args()


def find_jsons(path: Path):
    with path.open() as fp:
        path = Path(__file__).parent.parent.absolute()
        # get rid of newline character
        yield from map(
            lambda x: path / x[:-1] if x[-1] == "\n" else path / x, fp.readlines()
        )


def find_diffs(info_json: Path):
    commits = json.load(info_json.open())["commits"]
    for commit in commits:
        path_to_diff = info_json.parent / f"commit_{commit['sha']}.diff"
        if path_to_diff.exists():
            yield path_to_diff


def merge_tuples(tuples):
    result = []
    merged = set()

    for tup in tuples:
        current_set = set(tup)
        overlap = current_set.intersection(merged)

        if overlap:
            # Merge with existing sets
            merged.update(current_set)
            for existing_set in result:
                if existing_set.intersection(current_set):
                    existing_set.update(current_set)
                    break
        else:
            # Add a new set
            merged.update(current_set)
            result.append(current_set)

    return result


def check_files(info_json: dict, silent: bool = False) -> bool:
    suffixes = set()
    for commit in info_json["commits"]:
        for touched in commit["files"]:
            suffixes.add(touched["file_name"].split(".")[-1])
    allowed = {"c", "h"}
    if suffixes - allowed:
        if not silent:
            print(
                Fore.YELLOW
                + Style.BRIGHT
                + f"Warning: Commits contain files ending in {suffixes - allowed} with suffixes other than {allowed}."
                + Style.RESET_ALL
            )
    forbidden = {"cpp", "hpp", "cc", "hh", "java", "py", "go", "js", "ts", "rs"}
    if forbidden & suffixes:
        if not silent:
            print(
                Fore.YELLOW
                + Style.BRIGHT
                + f"Warning: Commits contain forbidden files ending in {forbidden & suffixes}. Type 'a' to [a]utomatically reject partial fix."
                + Style.RESET_ALL
            )
        return False
    return True


def print_help():
    return (
        Fore.LIGHTCYAN_EX
        + Style.BRIGHT
        + """
Available Commands:
---------------------------------------------------------------------------------------------------------------------------------------
h	show help (this message)
q	quit
s	show statistics
m	show issue comments and git commit messages (with pager)
mm	show issue comments and git commit messages formatted as markdown (without pager)
o	open folder of issue
op	open previous diff with text editor
oi	open current diff with text editor
on	open next diff with text editor
oa	execute `op`, `oi` and `on` in sequence
ol	open issue in browser
i	show current diff in terminal (with pager)
i{0-9}+	show diff at given index in terminal (with pager, every number after i works, no space between i and number)
p	show previous diff in terminal (with pager)
n	show next diff in terminal (with pager)
n+	show next n diffs in sequence (e.g., `nnn` to show the next three diffs)
na	show all diffs in sequence
c	categorize a partial fix and proceed to the next. Use `c t <comment>` to classify as partial fix and `c f <comment>` to discard
a	automatically reject as partial fix (iff at least one of the commits modifies a C++, Java, ... file) and proceed to the next
{0-9}+	jump to any diff by index (e.g., `1` to jump to the first diff)
,	Separate multiple commands with a comma (e.g., `s,i1` to show statistics and jump to the first diff)
f	Execute 'na', 'mm' and set index to 0 beforehand (guarantees that first commit is shown first)
_______________________________________________________________________________________________________________________________________
"""
        + Style.RESET_ALL
    )


def process_diffs(
    diffs: [Path], info_json: dict, index=0, initial_actions=[], validation: Path = None
):
    if initial_actions:
        action = initial_actions.pop(0)
    else:
        print(
            Fore.GREEN
            + Style.BRIGHT
            + f"Currently at diff {index + 1}/{len(diffs)}."
            + Style.RESET_ALL
        )
        action = input(
            Fore.LIGHTBLUE_EX
            + Style.BRIGHT
            + f"Show [h]elp, [p]revious, [i]ndex, [n]ext, [s]tatistics, commit [m]essages or [c]lassify, [q]uit, [o]pen: "
            + Style.RESET_ALL
        )
        if "," in action:
            action = action.split(",")
            initial_actions = action[1:]
            action = action[0]
    if action == "f":
        action = f"{len(diffs)}"
        initial_actions = ["na", "ol", "mm"]
    if action.isnumeric():
        if int(action) < 1 or int(action) > len(diffs):
            print(
                Fore.YELLOW
                + Style.BRIGHT
                + f"Index must be between 1 and {len(diffs)}."
                + Style.RESET_ALL
            )
            return process_diffs(
                diffs,
                info_json,
                index=index,
                initial_actions=initial_actions,
                validation=validation,
            )
        return process_diffs(
            diffs,
            info_json,
            index=int(action) - 1,
            initial_actions=initial_actions,
            validation=validation,
        )
    if action == "na":
        action = "n" * len(diffs)
    if len(set(action)) == 1 and "n" in action:
        initial_actions = ["n"] * (len(action) - 1) + initial_actions
        action = "n"
    if action == "h":
        print(print_help())
    if action == "q":
        sys.exit(0)
    if action == "a" and not check_files(info_json, silent=True):
        return process_diffs(
            diffs,
            info_json,
            index=index,
            initial_actions=["c f Contains files with forbidden suffixes."],
            validation=validation,
        )
    if action == "s":
        print()
        header = "===== Statistics for %s (%s) =====" % (
            diffs[index].parent.parent.name,
            diffs[index].parent.name,
        )
        print(header)
        data = []
        functions = set()
        functions_current = set()
        for i, diff in enumerate(diffs):
            try:
                patch_set = PatchSet.from_filename(diff)
            except UnidiffParseError as e:
                print(
                    Fore.YELLOW
                    + Style.BRIGHT
                    + f"Warning: Parse error {e=}."
                    + Style.RESET_ALL
                )
                continue
            data.append((i + 1, patch_set.removed, patch_set.added, "-"))
            diff_text = diff.read_text().splitlines()
            for l in diff_text:
                if "@@" in l:
                    function = l.split("@@")[-1].strip()
                    if function:
                        functions.add(function)
                        if i == index:
                            functions_current.add(function)
        duplicates = []
        for i, diff in enumerate(diffs):
            for j, comp in enumerate(diffs):
                if i > j and diff.read_text() == comp.read_text():
                    duplicates.append((i + 1, j + 1))
                    duplicates.append((j + 1, i + 1))
        if duplicates:
            chains = merge_tuples(duplicates)
            for i in range(len(diffs)):
                for chain in chains:
                    if i + 1 in chain:
                        remaining = set(chain)
                        remaining.remove(i + 1)
                        data[i] = (
                            data[i][0],
                            data[i][1],
                            data[i][2],
                            ", ".join(map(str, sorted(remaining))),
                        )
                        break
        print(
            pd.DataFrame(
                data, columns=["Diff", "Removed", "Added", "Duplicates"]
            ).to_string(index=False)
        )
        try:
            file_names = set()
            for commit in info_json["commits"]:
                for touched in commit["files"]:
                    file_names.add(touched["file_name"])
            print("Number of changed files:", len(file_names))
            print("Number of changed functions (overall):", len(functions))
            print(
                f"Number of changed functions (diff #{index + 1}):",
                len(functions_current),
            )
            check_files(info_json)
        except KeyError as e:
            print(
                Fore.YELLOW
                + Style.BRIGHT
                + f"Warning: Key Error {e=}."
                + Style.RESET_ALL
            )
        print("=" * len(header))
        print()
    if action.startswith("m"):
        print_markdown(
            "# ISSUE \n"
            + diffs[index].parent.joinpath("issue_comments.txt").read_text()
            + "\n\n# COMMITS\n"
            + diffs[index].parent.joinpath("commit_messages.txt").read_text(),
            beautiful=len(action) > 1,
        )
    if action.startswith("o"):
        if len(action) > 1:
            command = action[1:]
            if command == "p":
                subprocess.run(
                    ["open", diffs[index - 1].absolute()],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )
            if command == "n":
                subprocess.run(
                    ["open", diffs[(index + 1) % len(diffs)].absolute()],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )
            if command == "i":
                subprocess.run(
                    ["open", diffs[index].absolute()],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )
            if command == "l":
                repository_url = json.load(
                    diffs[index].parent.parent.joinpath("repository.json").open()
                )["url"]
                if "number" in info_json:
                    issue = info_json["number"]
                else:
                    issue = int(diffs[0].parent.name.split("issue_")[1])
                subprocess.run(
                    ["open", f"{repository_url}/issues/{issue}"],
                    stdout=subprocess.DEVNULL,
                    stderr=subprocess.DEVNULL,
                )
            if command == "a":
                return process_diffs(
                    diffs,
                    info_json,
                    index=index,
                    initial_actions=initial_actions + ["op", "oi", "on"],
                    validation=validation,
                )
            if command == "m" and validation is not None and validation.is_file():
                df = pd.read_csv(validation, sep="\t")
                directory = str(diffs[index].parent / "info.json")
                directory = (
                    "benchmarks_2023/"
                    + directory.split("benchmarks_2023/", maxsplit=1)[1]
                )
                df = df[df["task"].str.contains(directory)]
                pf = list(df["partial"])[0]
                col = (
                    Fore.LIGHTGREEN_EX + Style.BRIGHT
                    if pf
                    else Fore.LIGHTRED_EX + Style.BRIGHT
                )
                print(
                    col
                    + "Classification of peer: '"
                    + str(list(df["reason"])[0])
                    + "'"
                    + Style.RESET_ALL
                )
        else:
            subprocess.run(
                ["open", diffs[index].parent.absolute()],
                stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL,
            )
    if action.startswith("i"):
        try:
            if len(action) > 1:
                next_index = int(action[1:]) - 1
                if 0 <= next_index < len(diffs):
                    index = next_index
                    print_with_syntax_highlighting(diffs[index].read_text())
                else:
                    print(
                        Fore.YELLOW
                        + Style.BRIGHT
                        + f"Index must be between 1 and {len(diffs)}."
                        + Style.RESET_ALL
                    )
            else:
                print_with_syntax_highlighting(diffs[index].read_text())
        except ValueError:
            print(
                Fore.YELLOW
                + Style.BRIGHT
                + "Action 'i' must be followed by an integer (e.g., 'i1' to jump to the first index)."
                + Style.RESET_ALL
            )
    if action == "p":
        index -= 1
        if index < 0:
            index = len(diffs) - 1
        print_with_syntax_highlighting(diffs[index].read_text())
    if action == "n":
        index += 1
        if index >= len(diffs):
            index = 0
        print_with_syntax_highlighting(diffs[index].read_text())
    if action.startswith("c"):
        if len(action) > 2:
            classified = action.split(maxsplit=2)
            if len(classified) < 3:
                print(
                    Fore.YELLOW
                    + Style.BRIGHT
                    + "Please provide a reason for your classification."
                    + Style.RESET_ALL
                )
                return process_diffs(
                    diffs, info_json, index=index, validation=validation
                )
            if classified[1] == "t":
                return (True, classified[2])
            elif classified[1] == "f":
                return (False, classified[2])
        print(
            "Invalid classification. Classify with 'c t <reason>' or 'c f <reason>' to accept or reject as partial fix, respectively."
        )
    return process_diffs(
        diffs,
        info_json,
        index=index,
        initial_actions=initial_actions,
        validation=validation,
    )


def process(path: Path, out: Path, validation: Path = None):
    if out.exists():
        classifications = pd.read_csv(out, sep="\t")
        already_processed = set(classifications["task"])
    else:
        already_processed = set()
        classifications = pd.DataFrame(columns=["task", "partial", "reason"])
    skipped = 0
    for info_json in find_jsons(path):
        if str(info_json) in already_processed:
            skipped += 1
            continue
        if skipped > 0:
            print("Skipped", skipped, "(already processed) issues.")
            skipped = 0
        diffs = list(find_diffs(info_json))
        print("Processing", info_json, f"(Issue #{len(classifications.index)})")
        partial, reason = process_diffs(
            diffs,
            json.load(info_json.open()),
            initial_actions=["s"],
            validation=validation,
        )
        row = {"task": info_json, "partial": partial, "reason": reason}
        classifications = pd.concat([classifications, pd.DataFrame([row])])
        classifications.to_csv(out, sep="\t", index=False)
        if len(classifications.index) % 100 == 0:
            print_congratulations()


if __name__ == "__main__":
    args = parse_arguments()
    process(args.path, args.out, args.validation)
