import argparse
import json
from pathlib import Path
from unidiff import PatchSet


def parse_arguments():
    parser = argparse.ArgumentParser(description="Check overlap of diffs")
    parser.add_argument("info", type=Path, help="Path to info.json")
    parser.add_argument(
        "--grace", "-g", type=int, default=0, help="Grace range for diff lines"
    )
    return parser.parse_args()


def diffs_from_json(info_path: Path):
    with info_path.open() as fp:
        info = json.load(fp)
    for commit in info["commits"]:
        path_to_diff = info_path.parent / f"commit_{commit['sha']}.diff"
        if path_to_diff.exists():
            try:
                yield PatchSet.from_filename(
                    info_path.parent / f"commit_{commit['sha']}.diff"
                )
            except:
                continue


def compute_overlap(patches: [PatchSet], grace=0):
    previous_range = None
    for change in patches:
        sources = set()
        for p in change:
            for hunk in p:
                for line in hunk:
                    if line.is_context:
                        continue
                    sources.add(line.target_line_no)
                    sources.add(line.source_line_no)
        if None in sources:
            sources.remove(None)
        if not sources:
            return False
        first_line = min(sources)
        last_line = max(sources)
        current_range = range(first_line - grace, last_line + 1 + grace)
        if previous_range is not None:
            overlap = set(previous_range).intersection(set(current_range))
            if not overlap:
                return False
        previous_range = current_range
    return True


def compute_overlap_strict(patches: [PatchSet]):
    previous = None
    for change in patches:
        sources = set()
        for p in change:
            for hunk in p:
                for line in hunk:
                    if line.is_context:
                        continue
                    sources.add(line.target_line_no)
                    sources.add(line.source_line_no)
        if None in sources:
            sources.remove(None)
        if not sources:
            return False
        if previous is not None:
            overlap = previous.intersection(sources)
            if not overlap:
                return False
        previous = sources
    return True


def main():
    args = parse_arguments()
    if args.info.is_dir():
        jsons = list(args.info.glob("**/**/info.json"))
        jsons = list(filter(lambda x: "linux-kernel/unknown" not in str(x), jsons))
        for info in jsons:
            patches = list(diffs_from_json(info))
            if compute_overlap_strict(patches):
                print(info)
    else:
        patches = list(diffs_from_json(args.info))
        print(
            "The patches have an intersection "
            if compute_overlap(patches, args.grace)
            else "The patches have no intersection"
        )


if __name__ == "__main__":
    main()
