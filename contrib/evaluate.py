from pathlib import Path
import json
import pandas as pd


def path_to_dict(json_file: Path) -> dict:
    return json.load(json_file.open())


def count_starts(repository: Path):
    return path_to_dict(repository)["stars"]


def count_repositories(root: Path):
    return len(root.glob("**/repository.json"))


def count_infos(root: Path):
    return len(root.glob("**/**/info.json"))


def count_commits(root: Path):
    return len(root.glob("**/**/info.diff"))


def count_commits_per_info(info: Path):
    return len(path_to_dict(info)["commits"])


def count_commits_per_info(info: Path):
    return len(path_to_dict(info)["commits"])


def count_commit_changes(info: Path):
    return path_to_dict(info)["score"]["total_changes"]


def count_commit_additions(info: Path):
    return sum(commit["diff_additions"] for commit in path_to_dict(info)["commits"])


def count_commit_deletions(info: Path):
    return sum(commit["diff_deletions"] for commit in path_to_dict(info)["commits"])


def count_commit_files(info: Path):
    return sum(len(commit["files"]) for commit in path_to_dict(info)["commits"])


def count_file_size(info: Path):
    return sum(
        file_entry["file_size"]
        for commit in path_to_dict(info)["commits"]
        for file_entry in commit["files"]
    )


def was_reopened(info: Path):
    return path_to_dict(info)["score"]["reopen"]


def was_ci(info: Path):
    return path_to_dict(info)["score"]["status"]


def count_reopen_events(info: Path):
    return sum(
        evt[1] == "reopened" for evt in path_to_dict(info)["score"]["reopen_events"]
    )


def to_tex_constant(name: str, value: any, print_new_line: bool = True):
    return (
        "\\newcommand{\\"
        + name
        + "}{\\SI{"
        + str(value)
        + "}}"
        + ("\n" if print_new_line else "")
    )


def has_makefile(info: Path):
    return path_to_dict(info)["score"]["is_makefile_based"]


def candidate_issues_for_repository(repository: Path):
    return path_to_dict(repository)["interesting_issues"]


def total_issues_for_repository(repository: Path):
    return path_to_dict(repository)["all_issues"]


def size_of_repository(repository: Path):
    return path_to_dict(repository)["size"]


def total_repositories_scanned(root: Path):
    return len(list(root.joinpath("history").glob("*.json"))) - 1


def create_table(root: Path):
    rows = []
    for repository in root.glob("**/repository.json"):
        repository_name = path_to_dict(repository)["url"][len("https://github.com/") :]
        candidates = candidate_issues_for_repository(repository)
        total = total_issues_for_repository(repository)
        size = size_of_repository(repository)
        stars = count_starts(repository)
        for info in repository.parent.glob("**/info.json"):
            issue_number = path_to_dict(info)["number"]
            commits = count_commits_per_info(info)
            changes = count_commit_changes(info)
            additions = count_commit_additions(info)
            deletions = count_commit_deletions(info)
            files = count_commit_files(info)
            file_size = count_file_size(info)
            reopen = was_reopened(info)
            ci = was_ci(info)
            reopen_events = count_reopen_events(info)
            makefile = has_makefile(info)
            rows.append(
                {
                    "repository": repository_name,
                    "issue_number": issue_number,
                    "repository_issue_candidates": candidates,
                    "repository_issue_total": total,
                    "repository_size": size,
                    "repository_stars": stars,
                    "issue_commits": commits,
                    "issue_changes": changes,
                    "issue_additions": additions,
                    "issue_deletions": deletions,
                    "issue_number_files": files,
                    "issue_total_file_size": file_size,
                    "issue_was_reopened": reopen,
                    "issue_was_ci": ci,
                    "issue_number_reopen_events": reopen_events,
                    "issue_has_makefile": makefile,
                }
            )
    df = pd.DataFrame(rows)
    df.to_csv("table.csv", index=False)
    return df


def extract_repo_stats(df: pd.DataFrame):
    return df[
        [
            "repository",
            "repository_issue_candidates",
            "repository_issue_total",
            "repository_stars",
            "repository_size",
        ]
    ].drop_duplicates()


def export_latex_constants(df: pd.DataFrame):
    df_repo = extract_repo_stats(df)
    with open("constants.tex", "a") as file:
        file.write(
            to_tex_constant(
                "totalRepositoriesScanned",
                total_repositories_scanned(Path("benchmarks_2023")),
            )
        )
        file.write(to_tex_constant("totalRepositories", df["repository"].nunique()))
        file.write(
            to_tex_constant(
                "totalCandidates", df_repo["repository_issue_candidates"].sum()
            )
        )
        file.write(
            to_tex_constant("totalIssues", df_repo["repository_issue_total"].sum())
        )
        file.write(to_tex_constant("totalSize", df_repo["repository_size"].sum()))
        file.write(
            to_tex_constant("averageStars", round(df_repo["repository_stars"].mean()))
        )
        file.write(to_tex_constant("totalCommits", df["issue_commits"].sum()))
        file.write(to_tex_constant("totalChanges", df["issue_changes"].sum()))
        file.write(to_tex_constant("totalAdditions", df["issue_additions"].sum()))
        file.write(to_tex_constant("totalDeletions", df["issue_deletions"].sum()))
        file.write(to_tex_constant("totalFiles", df["issue_number_files"].sum()))
        file.write(to_tex_constant("totalFileSize", df["issue_total_file_size"].sum()))
        file.write(to_tex_constant("totalReopen", df["issue_was_reopened"].sum()))
        file.write(to_tex_constant("totalCI", df["issue_was_ci"].sum()))
        file.write(
            to_tex_constant("totalReopenEvents", df["issue_number_reopen_events"].sum())
        )
        file.write(to_tex_constant("totalMakefile", df["issue_has_makefile"].sum()))


export_latex_constants(create_table(Path("benchmarks_2023")))
