#!/usr/bin/env python3

import argparse
import json
import os
import subprocess
from collections import defaultdict
from pathlib import Path

import git


def get_full_commit_hash(repo, short_commit_hash):
    try:
        full_sha = repo.commit(short_commit_hash).hexsha
    except Exception:
        print("Could not find commit hash for", short_commit_hash)
        full_sha = None

    return full_sha


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--linux-kernel", help="path to the linux kernel git repository"
    )
    parser.add_argument("--output", help="output directory")
    return parser.parse_args()


def main():
    args = parse_args()
    linux_kernel_path = Path(args.linux_kernel)
    output_path = Path(args.output)

    repo = git.Repo(linux_kernel_path)

    cwd = os.getcwd()

    os.chdir(linux_kernel_path)
    subprocess.run(
        'git log --grep="Fixes: [0-9a-zA-Z]*" --all | grep -E "^commit|Fixes:(.*)\(" > commits-with-fixes.txt',
        shell=True,
    )
    commits_with_fixes = Path("commits-with-fixes.txt").read_text().splitlines()
    os.remove("commits-with-fixes.txt")
    os.chdir(cwd)

    commits_to_fixes = defaultdict(list)
    fixes_to_commits = defaultdict(list)

    current_commit = None
    for i, line in enumerate(commits_with_fixes):
        if line.startswith("commit"):
            current_commit = line.split(" ")[1]
        elif line.lstrip().startswith("Fixes:"):
            fixes = (
                line.split("Fixes:")[1]
                .split("(")[0]
                .strip()
                .split('"')[0]
                .split("'")[0]
                .split("eventfs")[0]
                .split("hwmon")[0]
                .split("init")[0]
                .split("drm")[0]
                .split("ipmi")[0]
                .split("MIPS")[0]
                .split("lightnvm")[0]
                .split("qeth")[0]
                .split("slip")[0]
                .split("smc")[0]
                .split("treewide")[0]
                .split("can")[0]
                .split("smc")[0]
                .split("x86")[0]
                .split("bpf")[0]
                .split("firmware")[0]
                .split("powerpc")[0]
                .split("of")[0]
                .split("ACPI")[0]
                .split("hrtimer")[0]
                .split("scsi_dh_rdac")[0]
                .split("ASoC")[0]
                .split("qeth")[0]
                .split("slip")[0]
                .split("smc")[0]
                .split("-")[0]
                .split(">")[0]
                .split(":")[0]
                .strip()
            )

            if "commit" in fixes:
                fixes = fixes.split("commit")[1].strip()
            if "tag" in fixes:
                fixes = fixes.split("tag")[1].strip()
            if "Commit" in fixes:
                fixes = fixes.split("Commit")[1].strip()
            if "'" in fixes:
                fixes = fixes.split("'")[1].strip()
            if "^" in fixes:
                fixes = fixes.split("^")[0].strip()
            if "<" in fixes:
                fixes = fixes.split(" ")[0].strip()

            full_hash = get_full_commit_hash(repo, fixes)
            if full_hash is not None:
                commits_to_fixes[current_commit].append(full_hash)
                fixes_to_commits[full_hash].append(current_commit)

        if i % 1000 == 0:
            print("After line:", i)
            print("Commits to fixes:", len(commits_to_fixes))
            print("Fixes to commits:", len(fixes_to_commits))

    issue_number = 0
    for commit, fixes in commits_to_fixes.items():
        if len(fixes) != 1:
            continue

        fix = fixes[0]

        sorted_commits = [
            commit,
            fix,
        ]

        current_hash = fix
        while current_hash in commits_to_fixes.keys():
            current_hash = commits_to_fixes[current_hash][0]
            sorted_commits.append(current_hash)

        sorted_commits = sorted_commits[::-1]

        if len(sorted_commits) > 2:
            info_json = []
            issue_path = output_path / f"issue_{issue_number}"
            os.makedirs(issue_path, exist_ok=True)

            path_info_json = issue_path / "info.json"

            path_issue_messages = issue_path / "issue_comments.txt"
            path_issue_messages.write_text("Nothing to see here, please continue.")

            path_commit_messages = issue_path / "commit_messages.txt"
            all_commit_messages = []

            for current_commit in sorted_commits:
                diff_path = (
                    path_info_json.parent / f"commit_{current_commit}.diff"
                ).absolute()

                cwd = os.getcwd()
                os.chdir(linux_kernel_path)
                command = f"git diff {current_commit}~1 {current_commit} -p --minimal  > {diff_path}"
                subprocess.run(
                    command,
                    shell=True,
                )
                os.chdir(cwd)

                all_commit_messages.append(repo.commit(current_commit).message)

                info_json.append(
                    {
                        "diff_url": "https://github.com/torvalds/linux/commit/"
                        + current_commit
                        + ".diff",
                        "diff": str(diff_path.absolute()),
                        "sha": current_commit,
                    }
                )

            path_commit_messages.write_text(
                "\n\n-----------------------------------------------------------------------------------------------------------------\n\n".join(
                    all_commit_messages
                )
            )

            info_json = {"commits": info_json}

            os.makedirs(path_info_json.parent, exist_ok=True)
            path_info_json.write_text(json.dumps(info_json))
            print("Length chain:", len(sorted_commits))
            issue_number += 1


if __name__ == "__main__":
    main()
