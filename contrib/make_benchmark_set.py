#!/usr/bin/env python3

# %%
import argparse
import json
import logging
import os
from pathlib import Path
import shutil
import sys
from typing import Iterable, NamedTuple, List
import subprocess
from tqdm import tqdm


class CreationError(Exception):
    pass


class Fix(NamedTuple):
    input_file: Path
    commit_sha1: str


class ChangeDir:
    def __init__(self, dir_name):
        self._working_dir = Path(".").resolve()
        os.chdir(dir_name)

    def __enter__(self):
        return None

    def __exit__(self, type, value, traceback):
        os.chdir(self._working_dir)


def get_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--output-dir", default="output", help="Output directory for benchmark set"
    )
    parser.add_argument(
        "--no-git",
        dest="git",
        default=True,
        action="store_false",
        help="Do not initialize a new git repository for the benchmark directory",
    )
    parser.add_argument(
        "--selective",
        nargs="+",
        default=[],
    )
    parser.add_argument("issues_directory", help="Directory of issues")
    return parser


def parse_args(argv):
    args = get_parser().parse_args(argv)
    args.issues_directory = Path(args.issues_directory)
    args.output_dir = Path(args.output_dir)

    if not args.issues_directory.exists():
        raise ValueError(f"Unknown directory: {args.issues_directory}")
    return args


def get_metadata_repo(repo: Path) -> dict:
    return _load_json(repo / "repository.json")


def _load_json(p: Path) -> dict:
    with open(p) as inp:
        return json.load(inp)


def get_repo_url(repo_dir: Path) -> str:
    return get_metadata_repo(repo_dir)["url"]


def get_issue_url(repo_url: str, issue_number: str) -> str:
    assert repo_url.startswith("http")
    if repo_url.endswith(".git"):
        repo_url = repo_url[: -len(".git")]
    return f"{repo_url}/issues/{issue_number}"


def get_issue_number_from_issue_dir(issue_dir: Path) -> str:
    issue_name = issue_dir.name
    assert issue_name.startswith("issue_")
    return issue_name[len("issue_") :]


def get_issues(repo_dir: Path) -> Iterable[Path]:
    return repo_dir.glob("issue_*")


def get_metadata_issue(issue_dir: Path) -> dict:
    return _load_json(issue_dir / "info.json")


def get_diff_file(commit_sha: str) -> Path:
    return Path(f"commit_{commit_sha}.diff")


def _get_base_commit_sha(issue_metadata) -> str:
    base_commit = issue_metadata["commits"][0]
    # assert base_commit["is_parent"], f"Base commit is no parent for {issue_metadata}"
    return base_commit["sha"]


def _get_partial_fixes(issue_metadata):
    return [c["sha"] for c in issue_metadata["commits"][1:-1]]


def _get_partial_fix_diffs(issue_metadata, issue_dir: Path) -> Iterable[Fix]:
    final_fix = _get_final_fix_diff(issue_metadata, issue_dir)
    with open(final_fix.input_file) as inp:
        changes_of_fix = list(get_fixes_of_diff(inp))
    for commit_sha in _get_partial_fixes(issue_metadata):
        diff_file = issue_dir / get_diff_file(commit_sha)
        with open(diff_file) as inp:
            changes = list(get_fixes_of_diff(inp))
        if changes_of_fix != changes:
            yield Fix(diff_file, commit_sha)


def _get_files_modified(diff_line):
    start_file_b = diff_line.index(" b/")
    prefix = "diff --git a/"
    file_a = diff_line[len(prefix) : start_file_b].strip()
    file_b = diff_line[start_file_b + 3 :].strip()
    return file_a, file_b


def get_fixes_of_diff(diff_lines):
    is_current_relevant = None
    for line in diff_lines:
        if line.startswith("diff --git"):
            file_a, file_b = _get_files_modified(line)
            is_current_relevant = is_relevant(file_a) or is_relevant(file_b)
        if not is_current_relevant:
            continue
        if (line.startswith("+") and not line.startswith("+++ ")) or (
            line.startswith("-") and not line.startswith("--- ")
        ):
            yield line


def is_relevant(file_name) -> bool:
    return any(
        file_name.endswith(relevant_suffix)
        for relevant_suffix in (".c", ".i", ".cpp", ".cc")
    )


def _get_final_fix_diff(issue_metadata, issue_dir: Path) -> Fix:
    final_fix_sha = issue_metadata["commits"][-1]["sha"]
    return Fix(issue_dir / get_diff_file(final_fix_sha), final_fix_sha)


def get_task_dir_for_issue(output_dir: Path, index: int) -> Path:
    return output_dir / f"partial_{index}"


def get_final_name_for_diff(diff_file: Path) -> str:
    name = diff_file.name
    assert name.startswith("commit_")
    name = name[len("commit_") :]
    if not name.endswith(".diff"):
        name = name + ".diff"
    return name


def copy_diff(diff_file: Path, target: Path):
    target.parent.mkdir(parents=True, exist_ok=True)
    shutil.copyfile(diff_file, target)
    return target


def archive_diffs(
    repo_url: str, commit_shas: [str], task_dir: Path
) -> (Fix, [Fix], Fix):
    target_dir = task_dir / "a-base"
    command_to_clone = ["git", "clone", repo_url, str(target_dir)]
    try:
        subprocess.check_call(command_to_clone)
    except subprocess.CalledProcessError as e:
        logging.warning("Error initializing repository %s: %s", repo_url, e)
        raise CreationError() from e

    commands_to_archive = []
    command_for_parent = ["git", "rev-parse", f"{commit_shas[0]}^"]
    with ChangeDir(target_dir):
        proc = subprocess.run(command_for_parent, capture_output=True, text=True)
        base_sha = proc.stdout.strip()
        result = proc.returncode
        if result != 0 or len(base_sha) == 0:
            logging.warning("Error finding parent of %s: %s", commit_shas[0], base_sha)
            logging.warning(
                "Command: %s caused %s", " ".join(command_for_parent), proc.stderr
            )
            raise CreationError()
    logging.debug("Found parent '%s' of '%s'", commit_shas[0], base_sha)
    archive_target = (task_dir / f"{short_sha(base_sha)}.zip").absolute()
    base_target = archive_target
    commands_to_archive.append(
        [
            "git",
            "archive",
            "--format=zip",
            "--prefix=a-base/",
            "-o",
            str(archive_target),
            base_sha,
        ]
    )
    targets = []
    current_commit = base_sha
    for commit in commit_shas:
        archive_target = (task_dir / f"{short_sha(commit)}.diff").absolute()
        targets.append(archive_target)
        commands_to_archive.append(
            [
                "git",
                "diff",
                "-p",
                "--minimal",
                "--output",
                str(archive_target),
                current_commit,
                commit,
            ]
        )
        current_commit = commit
    with ChangeDir(target_dir):
        try:
            for command_to_archive in commands_to_archive:
                subprocess.check_call(command_to_archive, stdout=subprocess.DEVNULL)
        except subprocess.CalledProcessError as e:
            logging.warning(
                "Error archiving base version %s for %s: %s",
                # we insert a new sha hash but for debugging first existing one is better.
                commit_shas[1],
                repo_url,
                e,
            )
            raise CreationError() from e
    shutil.rmtree(target_dir)
    if not archive_target.exists():
        raise CreationError(
            f"Expected git archive does not exist: {str(archive_target)}"
        )
    return (
        Fix(base_target, base_sha),
        [Fix(t, c) for t, c in zip(targets[:-1], commit_shas[:-1])],
        Fix(targets[-1], commit_shas[-1]),
    )


def archive_commits(repo_url: str, commits: [str], task_dir: Path) -> Fix:
    if not repo_url.endswith(".git"):
        repo_url += ".git"
    return archive_diffs(repo_url, commits, task_dir)


def get_repo_name(repo_url: str) -> str:
    return "_".join(repo_url.split("/")[-2:])


def create_task_definition(
    repo_url: str,
    task_dir: Path,
    base_commit: Fix,
    partial_fixes: List[Fix],
    expected_fix: Fix,
    issue_dir: dict,
    classification="unknown",
) -> Path:
    task_def = task_dir / f"{get_repo_name(repo_url)}_{task_dir.name}.yml"
    assert not task_def.exists()
    metadata = get_metadata_issue(issue_dir)
    if "number" in metadata:
        issue_number = metadata["number"]
        issue_url = get_issue_url(repo_url, issue_number)
    else:
        issue_url = "null"
    if "score" in metadata:
        strategy = "reopen" if metadata["score"]["reopen"] else "status"
        build_system = ["make"] if metadata["score"]["is_makefile_based"] else []
    else:
        strategy = "linux-convention"
        build_system = ["kbuild"]

    with open(task_def, "w") as outp:
        outp.write(
            f"""format_version: '1.0'

repository_url: {repo_url}

sequence:
  base_version:
    input_file: '{base_commit.input_file.name}'
    commit_sha1: {base_commit.commit_sha1}
  fix_attempt:\n"""
        )
        for p in partial_fixes:
            outp.write(" " * 4 + f"- input_file: '{p.input_file.name}'\n")
            outp.write(" " * 4 + f"  commit_sha1: {p.commit_sha1}\n")
        outp.write(
            f"""    
  expected_fix:
    input_file: '{expected_fix.input_file.name}'
    commit_sha1: {expected_fix.commit_sha1}

  
classification: {classification}
categories:
  - arithmetic and control-flow

  
metadata:
  language: C
  strategy: {strategy}
  build_system: {build_system}
  related_issue: {issue_url}\n"""
        )


def short_sha(sha: str, length=8) -> str:
    if len(sha) < length:
        return sha
    return sha[:length]


def create_task(
    issue_dir: Path,
    output_dir: Path,
    index: int,
    repo_url: str,
    classification="unknown",
):
    try:
        task_dir = get_task_dir_for_issue(output_dir, index)
        if task_dir.exists():
            logging.info("Directory %s exists, skipping this task.", task_dir)
            return
        metadata = get_metadata_issue(issue_dir)
        base_commit = _get_base_commit_sha(metadata)
        partial_fixes = [
            f.commit_sha1 for f in _get_partial_fix_diffs(metadata, issue_dir)
        ]
        expected_fix = _get_final_fix_diff(metadata, issue_dir).commit_sha1
        logging.debug("Creating new task with the below information")
        logging.debug(f"Directory: {issue_dir}")
        logging.debug(f"Base: {base_commit}")
        logging.debug(f"Partial fixes: {partial_fixes}")
        logging.debug(f"Expected fix: {expected_fix}")
        task_dir.mkdir(parents=True, exist_ok=True)
        base_commit, partial_fixes, expected_fix = archive_commits(
            repo_url, [base_commit] + partial_fixes + [expected_fix], task_dir
        )
        create_task_definition(
            repo_url,
            task_dir,
            base_commit,
            partial_fixes,
            expected_fix,
            issue_dir,
            classification=classification,
        )
    except CreationError as e:
        if task_dir.exists():
            # remove incomplete task
            shutil.rmtree(task_dir)
        raise e


def init_output_dir(output_dir: Path):
    output_dir.mkdir(parents=True, exist_ok=True)
    if (output_dir / ".git").exists():
        raise CreationError(
            "Output directory is existing git repository. Output directory must be an empty, non-git directory"
        )
    if list(output_dir.iterdir()):
        raise CreationError(
            "Output directory is non-emtpy directory. Output directory must be an empty, non-git directory"
        )

    logging.info("Initializing new git repository at %s", output_dir)
    command_to_create_repo = ["git", "init"]
    with ChangeDir(output_dir):
        try:
            subprocess.check_call(command_to_create_repo)
        except subprocess.CalledProcessError as e:
            logging.warning(
                "Error initializing git repository at %s: %s", output_dir, e
            )
            raise CreationError() from e


def path_to_id(issue_path: str) -> str:
    if Path(issue_path).is_dir():
        return " ".join(issue_path.split(os.path.sep)[-2:])
    if Path(issue_path).is_file():
        return " ".join(issue_path.split(os.path.sep)[-3:-1])
    raise ValueError(f"Unknown path: {issue_path}")


# %%


def main(argv=None):
    if argv is None:
        argv = sys.argv[1:]
    args = parse_args(argv)
    logging.basicConfig(level=logging.INFO)
    output_dir = args.output_dir
    if args.git:
        init_output_dir(output_dir)
    repositories = list(args.issues_directory.resolve().iterdir())
    logging.info("Number of repositories: %s", len(repositories))

    classification = {}
    if args.selective:
        import pandas as pd
        from collections import Counter

        partial_fixes = []
        no_partial_fixes = []
        for s in args.selective:
            df = pd.read_csv(s, sep="\t")
            df["partial"] = df["partial"].astype(bool)
            partial = df[df["partial"]]
            partial_fixes.extend(partial["task"].to_list())
            non_partial = df[~df["partial"]]
            no_partial_fixes.extend(non_partial["task"].to_list())
        count_partial = Counter(partial_fixes)
        count_non_partial = Counter(no_partial_fixes)
        count_all = count_partial + count_non_partial
        for t, c in count_partial.items():
            classification[path_to_id(t)] = (
                "partial fix" if c == count_all[t] else "unknown"
            )
        for t, c in count_non_partial.items():
            classification[path_to_id(t)] = (
                "no partial fix" if c == count_all[t] else "unknown"
            )

    # %%

    # %%
    # necessary to change dir so that git operations operate on the output git repository
    with ChangeDir(output_dir):
        for repo in tqdm(repositories):
            try:
                repo_url = get_repo_url(repo)
                issues = get_issues(repo)
            except FileNotFoundError as e:
                logging.warning(
                    "repository.json is missing for %s, no task created for this repository. Detailed message: %s",
                    repo,
                    e,
                )
            repository_index = 1
            for issue in issues:
                try:
                    create_task(
                        issue,
                        Path(repo.name),
                        repository_index,
                        repo_url=repo_url,
                        classification=classification.get(
                            path_to_id(str(issue)), "unknown"
                        ),
                    )
                    repository_index += 1
                except FileNotFoundError as e:
                    logging.warning(
                        "A file is missing for %s, no task created for this issue. Detailed message: %s",
                        issue,
                        e,
                    )
                except CreationError as e:
                    logging.warning(
                        "Task for %s/%s could not be created: %s",
                        repo_url,
                        issue,
                        e,
                    )


if __name__ == "__main__":
    sys.exit(main())
# %%


def test_get_files_modified():
    test_line = "diff --git a/benchmarks/karelzak_util-linux/issue_422/test file b/benchmarks/karelzak_util-linux/issue_422/test file"

    file_a, file_b = _get_files_modified(test_line)

    assert file_a == "benchmarks/karelzak_util-linux/issue_422/test file"
    assert file_b == "benchmarks/karelzak_util-linux/issue_422/test file"

    test_line = "diff --git a/benchmarks/karelzak_util-linux/issue_42b/test_file b/benchmarks/karelzak_util-linux/issue_422a/test_file"

    file_a, file_b = _get_files_modified(test_line)

    assert file_a == "benchmarks/karelzak_util-linux/issue_42b/test_file"
    assert file_b == "benchmarks/karelzak_util-linux/issue_422a/test_file"
