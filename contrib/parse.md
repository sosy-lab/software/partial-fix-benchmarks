# Running the processor

The script takes two inputs:
  - the JSON file containing at least a list of commits
  - an output path to store the downloaded and preprocessed files 
```sh
 ./contrib/parse.py JSON OUTPUT    
```

An example that works without further configuration (execute from root directory of the repository):
```sh
 ./contrib/parse.py benchmarks_filtered/small/baskerville_bspwm/issue_1278/info.json output/
```

## Parser

### How does it work?

For a given url in the `info.json`, we download the modified C program (after the patch).
For the first commit in the list, we also download the original C program (the parent).
Therefore, we obtain `n + 1` C files for a list with `n` commits.
The first file, is the buggy initial version `base.c`.
Files `2...n` are the partial fixes `partial_fix_<index>.c`.
The last file (`n + 1`) is the final fix `fix.c`.
From this list, we generate exactly three diffs between `base.c`, `partial_fix_n.c`, `fix.c`.
Afterwards, we insert the first diff to `base.c` under the paradigm of program equality:
If the first fix was a partial fix, at least one variable needs to change.
If not, the task is not considered.  

### Example

```diff
diff --git a/home/matthias/SosyLab/Software/partial-fix-benchmarks/contrib/testt/base.c b/home/matthias/SosyLab/Software/partial-fix-benchmarks/contrib/testt/partial_fix_2.c
index aa9ca929d..df535ed1b 100644
--- a/home/matthias/SosyLab/Software/partial-fix-benchmarks/contrib/testt/base.c
+++ b/home/matthias/SosyLab/Software/partial-fix-benchmarks/contrib/testt/partial_fix_2.c
@@ -698,6 +698,12 @@ int desktop_from_desc(char *desc, coordinates_t *ref, coordinates_t *dst)
 	desc = desc_copy;
 
 	char *hash = strrchr(desc, '#');
+	char *colon = strrchr(desc, ':');
+
+	                                                                      
+	if (hash != NULL && colon != NULL && hash < colon) {
+		hash = NULL;
+	}
 
 	if (hash != NULL) {
 		*hash = '\0';
@@ -705,6 +711,7 @@ int desktop_from_desc(char *desc, coordinates_t *ref, coordinates_t *dst)
 		coordinates_t tmp = {mon, mon->desk, NULL};
 		if ((ret = desktop_from_desc(desc, &tmp, ref)) == SELECTOR_OK) {
 			desc = hash + 1;
+			colon = strrchr(desc, ':');
 		} else {
 			free(desc_copy);
 			return ret;
@@ -712,7 +719,6 @@ int desktop_from_desc(char *desc, coordinates_t *ref, coordinates_t *dst)
 	}
 
 	desktop_select_t sel = make_desktop_select();
-	char *colon = strrchr(desc, ':');
 
 	if (!parse_desktop_modifiers(colon != NULL ? colon : desc, &sel)) {
 		free(desc_copy);

```

From this diff, we extract all code of function `int desktop_from_desc(...)` and continue by 
inserting the least number of statements with replace variables into the `base.c` file.

```c
char __IDEFIX_preprocessing_desc_copy = desc_copy;
char __IDEFIX_preprocessing_colon = colon;
char __IDEFIX_preprocessing_desc = desc;
char __IDEFIX_preprocessing_hash = hash;
int __IDEFIX_preprocessing_ret = ret;
coordinates_t __IDEFIX_preprocessing_tmp = tmp;
coordinates_t __IDEFIX_preprocessing_ref = ref;
char *colon = strrchr(desc, ':');
if (((hash != NULL) && (colon != NULL)) && (hash < __IDEFIX_preprocessing_colon))
{
    hash = NULL;
}
if (hash != NULL)
{
    *hash = '\0';
    int ret;
    coordinates_t tmp = {mon, mon->desk, NULL};
    if ((ret = desktop_from_desc(desc, &__IDEFIX_preprocessing_tmp, __IDEFIX_preprocessing_ref)) == SELECTOR_OK)
    {
        __IDEFIX_preprocessing_desc = __IDEFIX_preprocessing_hash + 1;
        colon = strrchr(desc, ':');
    }
    else
    {
        free(__IDEFIX_preprocessing_desc_copy);
        return __IDEFIX_preprocessing_ret;
    }
}
__VERIFIER_assert((desc_copy != __IDEFIX_preprocessing_desc_copy) || ((colon != __IDEFIX_preprocessing_colon) || ((desc != __IDEFIX_preprocessing_desc) || ((hash != __IDEFIX_preprocessing_hash) || ((ret != __IDEFIX_preprocessing_ret) || ((tmp != __IDEFIX_preprocessing_tmp) || (ref != __IDEFIX_preprocessing_ref)))))));

```
The example above will insert this.
Please make sure to check the output (it's not perfect yet).

## Preprocessor

To preprocess the files you will need to be able to compile them. Currently we assume
that each project contains a makefile with which it will be compiled. This will fail more
often than not, because some dependencies are missing. This will result in an error:

```text
src/pointer.c:25:10: fatal error: xcb/xcb_keysyms.h: No such file or directory
   25 | #include <xcb/xcb_keysyms.h>
      |          ^~~~~~~~~~~~~~~~~~~
```

To find the file and install it you can follow the steps describe in [this stack overflow
answer](https://askubuntu.com/a/1359397). Once everything is installed and the project can
be compiled, everything should continue working out of the box.

Sadly this part could be automatic, but would require root permissions on the system
in order to install stuff.

### How does it work?

The preprocessor uses the tool [One line scan](https://github.com/awslabs/one-line-scan) to
add the parameter `-save-temps` to the compiler. This saves all temporary files
generated by the compiler. In particular, it generates the `.i` files, where all includes have
already been resolved. 

Once the preprocessed files have been generated, the tool looks for the one where the 
function lies which is being modified and replace the function in the file by it.

## How to get results?

All results will be in `project/issue_num/results/preprocessing`.