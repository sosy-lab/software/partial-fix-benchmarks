#!/usr/bin/env python3
import argparse
from dataclasses import dataclass
import logging
from pathlib import Path
from unidiff import PatchSet
import re
import string
import subprocess
import requests
import json
from urllib.parse import urlparse
import os
import time

import pycparser
from pycparser.c_ast import PtrDecl, ArrayDecl
from pycparser.c_generator import CGenerator

from preprocessing import preprocess_files

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
    handlers=[logging.StreamHandler()],
)


class IdefixException(Exception):
    def __init__(self, message: str) -> None:
        super().__init__(message)


# =============== Git & Download =============== #


def git_diff(file1: Path, file2: Path) -> str:
    # Run the 'git diff' command and capture its output
    command = [
        "git",
        "diff",
        "--no-index",
        str(file1.absolute()),
        str(file2.absolute()),
    ]
    stdout, stderr = subprocess.Popen(
        command, universal_newlines=True, stdout=subprocess.PIPE
    ).communicate()
    if stderr:
        raise IdefixException(stderr)
    # Return the Git diff output
    return stdout


def download_github_diff_files(
    url: str, output: Path, modified_files_path: Path, parent_path: Path = None
) -> None:
    os.makedirs(output.parent, exist_ok=True)
    # Parse the URL
    parsed_url = urlparse(url)
    path_parts = parsed_url.path.split("/")
    owner = path_parts[1]
    repo = path_parts[2]
    pr_or_commit_id = path_parts[4]

    # Construct the API URLs
    api_base_url = "https://api.github.com"
    api_repo_url = f"{api_base_url}/repos/{owner}/{repo}"
    api_pr_or_commit_url = f"{api_repo_url}/commits/{pr_or_commit_id}"
    api_files_url = f'{api_pr_or_commit_url[:-len(".diff")]}'

    # Download the files
    headers = {"Accept": "application/vnd.github.v3+json"}
    response = requests.get(api_files_url, headers=headers)
    response.raise_for_status()
    commit = response.json()
    files = commit["files"]
    files = list(filter(lambda f: f["filename"].endswith(".c"), files))
    if len(files) != 1:
        raise IdefixException("Expected exactly one file in the diff")
    with open(modified_files_path, mode="a+") as f:
        f.write("\n".join(map(lambda f: f["filename"], files)) + "\n")
    for f in files:
        program = sanitize_programs(requests.get(f["raw_url"], headers=headers).text)
        output.write_text(program)
        if parent_path is not None:
            if len(commit["parents"]) == 0:
                raise IdefixException("Expected at least one parent commit")
            p = commit["parents"][0]
            parent_file_url = f'https://raw.githubusercontent.com/{owner}/{repo}/{p["sha"]}/{f["filename"]}'
            parent_path.write_text(
                sanitize_programs(requests.get(parent_file_url, headers=headers).text)
            )


def diff_list_from_info(info_json: Path) -> list:
    commits = json.loads(info_json.read_text())["commits"]
    yield from map(lambda c: c["diff_url"], commits)


# =============== Parsing =============== #


@dataclass
class Line:
    linenumber: int
    code: str


@dataclass
class BenchmarkTask:
    base_task: Path
    modified_tasks: [Path]
    fixed_task: Path
    modified_files: Path

    def eliminate_duplicates_at_end(self):
        if (
            len(self.modified_tasks) > 1
            and self.modified_tasks[-1].read_text() == self.fixed_task.read_text()
        ):
            logging.debug("Eliminating duplicate diff.")
            return BenchmarkTask(
                self.base_task,
                self.modified_tasks[:-1],
                self.fixed_task,
                self.modified_files,
            )
        return self

    def compute_diffs(self, write_to: Path, prefix="") -> list:
        diffs = []
        previous_task = self.base_task
        for modified_task in [*self.modified_tasks, self.fixed_task]:
            diff = git_diff(previous_task, modified_task)
            diff_path = write_to.joinpath(f"{prefix}{modified_task.name}.diff")
            diff_path.write_text(diff)
            diffs.append(diff_path)
            previous_task = modified_task
        return diffs

    def __repr__(self) -> str:
        return f"Base: {self.base_task}\nModified: {self.modified_tasks}\nFixed: {self.fixed_task}"

    def squash_diffs(self):
        return BenchmarkTask(
            self.base_task,
            [self.modified_tasks[-1]],
            self.fixed_task,
            self.modified_files,
        )

    @staticmethod
    def from_directory(folder: Path):
        base_task = None
        modified_tasks = []
        fixed_task = None
        for task in folder.iterdir():
            if str(task).endswith("base.c") or str(task).endswith("base.i"):
                base_task = task
            elif str(task).endswith("fix.c") or str(task).endswith("fix.i"):
                fixed_task = task
            elif str(task).endswith(".c") or str(task).endswith(".i"):
                modified_tasks.append(task)
        modified_files = folder.joinpath("modified_files.txt")
        modified_files.touch()
        return BenchmarkTask(base_task, modified_tasks, fixed_task, modified_files)

    @staticmethod
    def from_diffs(diffs: list, output: Path):
        os.makedirs(output, exist_ok=True)
        modified_files = output.joinpath("modified_files.txt")
        modified_files.touch()
        base_file = output.joinpath("base.c")
        partial_fixes = []
        for index, diff in enumerate(diffs):
            fix_name = (
                f"partial_fix_{index + 1}.c" if index < len(diffs) - 1 else "fix.c"
            )
            full_path = output.joinpath(fix_name)
            partial_fixes.append(full_path)
            if not os.path.exists(full_path):
                download_github_diff_files(
                    diff,
                    full_path,
                    modified_files,
                    parent_path=base_file if index == 0 else None,
                )
        return BenchmarkTask(
            base_file, partial_fixes[:-1], partial_fixes[-1], modified_files
        )


class CollectNodesVisitor(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.successors = set()

    def visit(self, node):
        self.successors.add(node)
        self.generic_visit(node)


class LinenumberVisitor(pycparser.c_ast.NodeVisitor):
    def __init__(self, valid_linenumbers: set):
        self.valid_numbers = valid_linenumbers
        self.nodes = set()

    def visit(self, node):
        # ToDo if linenumber in valid_numbers
        if node.coord and node.coord.line in self.valid_numbers:
            self.nodes.add(node)
        self.generic_visit(node)

    def summarize(self):
        for node in set(self.nodes):
            collector = CollectNodesVisitor()
            collector.visit(node)
            visited = collector.successors
            visited.remove(node)
            for v in visited:
                if v in self.nodes:
                    self.nodes.remove(v)


class VariableTypeVisitor(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.variable_data_pairs = {}

    def visit_Decl(self, node):
        if isinstance(node.type, pycparser.c_ast.TypeDecl):
            var_name = node.name
            data_type = node.type
            self.variable_data_pairs[var_name] = (data_type, node)
        self.generic_visit(node)

    def visit_ArrayDecl(self, node):
        if isinstance(node.type, pycparser.c_ast.TypeDecl):
            var_name = node.type.declname
            data_type = node.type
            self.variable_data_pairs[var_name] = (data_type, node)
        self.generic_visit(node)

    def visit_PtrDecl(self, node):
        if isinstance(node.type, pycparser.c_ast.TypeDecl):
            var_name = node.type.declname
            data_type = node.type
            self.variable_data_pairs[var_name] = (data_type, node)
        self.generic_visit(node)

    def visit_DeclList(self, node):
        for decl in node.decls:
            if isinstance(decl.type, pycparser.c_ast.TypeDecl):
                var_name = decl.type.declname
                data_type = decl.type
                self.variable_data_pairs[var_name] = (data_type, node)
        self.generic_visit(node)


class PredecessorVisitor(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.predecessors = {}
        self.stack = []

    def visit(self, node):
        if self.stack:
            self.predecessors[node] = [self.stack[-1]]
            self.predecessors[node] += self.predecessors.get(self.stack[-1], [])
        else:
            self.predecessors[node] = []
        self.stack.append(node)
        self.generic_visit(node)
        self.stack.pop()


class VariableVisitor(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.variables = set()

    def visit_ID(self, node):
        self.variables.add(node)
        self.generic_visit(node)


class FunctionNameCollector(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.function_names = set()

    def visit_FuncDecl(self, node):
        # Collect function names from FuncDecl nodes
        self.function_names.add(node.decl.name.name)

    def visit_FuncDef(self, node):
        # Collect function names from FuncDef nodes
        self.function_names.add(node.decl.name.name)

    def visit_FuncCall(self, node):
        # Collect function names from FuncCall nodes
        self.function_names.add(node.name.name)


def extract_function_names(ast):
    collector = FunctionNameCollector()
    collector.visit(ast)
    return collector.function_names


class RenameVariableVisitor(pycparser.c_ast.NodeVisitor):
    def __init__(self):
        self.variables = set()

    def visit_ID(self, node):
        node.name = f"__IDEFIX_preprocessing_{node.name}"
        self.generic_visit(node)


def find_data_types_of_variables(ast: pycparser.c_parser.c_ast) -> dict:
    visitor = VariableTypeVisitor()
    visitor.visit(ast)
    return visitor.variable_data_pairs


def find_variables(ast: pycparser.c_parser.c_ast) -> set:
    visitor = VariableVisitor()
    visitor.visit(ast)
    return visitor.variables


def find_predecessors(ast: pycparser.c_parser.c_ast) -> dict:
    visitor = PredecessorVisitor()
    visitor.visit(ast)
    return visitor.predecessors


def find_successors(ast: pycparser.c_parser.c_ast) -> set:
    visitor = CollectNodesVisitor()
    visitor.visit(ast)
    return visitor.successors


def find_source_lines(
    patch: PatchSet, reversed_patch: PatchSet
) -> (int, int, int, int):
    def get_additions(selected: PatchSet):
        source_lines = set()
        target_lines = set()
        for p in selected:
            for hunk in p:
                for line in hunk:
                    if line.is_context:
                        continue
                    source_lines.add(line.source_line_no)
                    target_lines.add(line.target_line_no)
        if None in source_lines:
            source_lines.remove(None)
        if None in target_lines:
            target_lines.remove(None)
        return source_lines, target_lines

    source, target = get_additions(patch)
    target2, source2 = get_additions(reversed_patch)
    source |= target2
    target |= source2
    return min(source), max(source), min(target), max(target)


def find_function(
    diff: Path, reversed_diff: Path, original_file: Path, use_source=True
) -> [Line]:
    source_min, source_max, target_min, target_max = find_source_lines(
        PatchSet.from_filename(diff), PatchSet.from_filename(reversed_diff)
    )
    if not use_source:
        source_min = target_min
        source_max = target_max
    logging.debug(
        "Searching function in %s between lines %s and %s."
        % (original_file, source_min, source_max)
    )
    original_file_lines = original_file.read_text().splitlines()
    for index, line in reversed(list(enumerate(original_file_lines[:source_max]))):
        # if function call, find all closing brackets
        if re.match(r"(static )?\w+\s+(\w|\*)+\s*\([^)]*\)\s*\{?", line.strip()):
            logging.debug("Matched function call %s." % line)
            brackets = None
            for upcoming_index, upcoming_line in enumerate(original_file_lines[index:]):
                for character in upcoming_line:
                    if character == "{":
                        if brackets is None:
                            brackets = 1
                        else:
                            brackets += 1
                    elif character == "}":
                        if brackets is None:
                            raise IdefixException(
                                "Found closing bracket before opening bracket."
                            )
                        brackets -= 1
                    if brackets == 0:
                        if index > source_min:
                            raise IdefixException(
                                "Modified area not within one function."
                            )
                        lines = []
                        for linenumber, code in enumerate(
                            original_file_lines[index : index + upcoming_index + 1]
                        ):
                            lines.append(Line(linenumber + index, code))
                        return lines
    raise IdefixException(
        "Did not find a function definition/brackets or some brackets never close."
    )


def extract_token_at_index(code, index, before=None) -> str:
    def clean(w: str):
        forbidden = ["(", ")", ",", ";", "=", ".", "[", "]" "{", "}", ":"]
        for f in forbidden:
            w = w.replace(f, "")
        return w

    if index < 0 or index >= len(code):
        raise IdefixException(f"Invalid index {index} for {code} (out of bound).")

    left = index
    right = index

    while left >= 0 and code[left] not in string.whitespace:
        left -= 1

    while right < len(code) and code[right] not in string.whitespace:
        right += 1

    token = clean(code[left + 1 : right])
    if token == before:
        logging.debug(
            "Conversion: Extracted token matches '%s', trying to find another one."
            % before
        )
        for i in range(left):
            if code[left - i] not in string.whitespace:
                token = clean(extract_token_at_index(code, left - i, None))
                logging.debug(
                    "Conversion: Found token '%s' before '%s'." % (token, before)
                )
                break
    logging.debug(
        "Conversion: Extracted token '%s' from '%s'."
        % (token, (code[:index] + "[" + code[index] + "]" + code[index + 1 :]).strip())
    )
    return token


def function_to_ast(function: [Line]) -> pycparser.c_ast.FileAST:
    # immutability
    function = list(function)
    parsed = None
    inserted = 0
    while True:
        try:
            parsed = pycparser.CParser().parse(
                text="\n".join(map(lambda l: l.code, function))
            )
            break
        except Exception as e:
            # self-healing from error message
            # e=ParseError(:line:col: before: var/decl) used to add the declaration of
            # var or decl itself as type int to the beginning of the program.
            # if required variables do not have a type in the function, they are most likely
            # global and therefore not supported.
            msg = f"{e=}"
            logging.debug(f"Conversion: Start self-healing with {msg}")
            matched = re.match(
                r"e\=ParseError\(\'\:([0-9]+)\:([0-9]+)\: before\: (.*)\'\)", msg
            )
            with open("program.log", "w") as fp:
                fp.write("\n".join(list(map(lambda l: l.code, function))))
            if matched:
                l, c, var = matched.groups()
                l = int(l) - 1
                c = int(c) - 1
                logging.debug(f"Conversion: Matched {function[l]}, {c=}, {var=}")
                var = extract_token_at_index(function[l].code, c, before=var)
                words = function[l].code.strip().split()
                decl = var
                for i, w in enumerate(words):
                    if w == var:
                        if i == 0:
                            decl = var
                            break
                        if len(words) > i + 1:
                            if "=" in words[i + 1]:
                                decl = words[i - 1]
                                break
                            if (
                                ")" in words[i + 1]
                                or "," in words[i + 1]
                                or ";" in words[i + 1]
                            ):
                                decl = var
                                break
                        if ")" in words[i] or "," in words[i] or ";" in words[i]:
                            decl = words[i - 1]
                            break

                inserted += 1
                logging.debug(f"Conversion: INSERT DUMMY 'typedef int {decl};'")
                function.insert(0, Line(-1, f"typedef int {decl};"))
                continue
            else:
                raise IdefixException(
                    f"Self healing failed, please annotate the program manually. {e=}"
                )
    if parsed:
        return parsed, inserted
    raise IdefixException("Program contains unsupported features.")


# pycparser doesn't accept comments
def sanitize_programs(program: str):
    # does not work if something like this appears in the program:
    # ... a = '// comment';

    # replace each character with "" but keeps newlines (linenumbers still match)
    program = re.sub(r"//.*", "", program)
    stack = []
    last_character = None
    for i, c in enumerate(program):
        if c == "*" and last_character == "/":
            stack.append(i - 1)
        elif c == "/" and last_character == "*":
            popped = stack.pop()
            if len(stack) == 0:
                program = (
                    program[:popped]
                    + re.sub(r".", " ", program[popped : i + 1])
                    + program[i + 1 :]
                )
        last_character = c
    return program


def find_modified_nodes(function: list, start: int, end: int):
    function_start = function[0].linenumber
    ast, inserted = function_to_ast(function)
    predecessors_visitor = PredecessorVisitor()
    predecessors_visitor.visit(ast)
    area = set(
        range(
            start + inserted - function_start,
            end + inserted + 1 - function_start,
        )
    )
    linenumber_visitor = LinenumberVisitor(area)
    linenumber_visitor.visit(ast)
    before = len(linenumber_visitor.nodes)
    linenumber_visitor.summarize()
    logging.debug(
        f"Extraction: Removed {before - len(linenumber_visitor.nodes)} covered nodes."
    )
    touched_nodes = linenumber_visitor.nodes
    if not len(touched_nodes) > 0:
        raise IdefixException("No nodes found in modified area.")
    return touched_nodes, predecessors_visitor.predecessors, ast


def find_sequence(modified_nodes: set, predecessors: dict):
    """
    Finds the sequence of nodes that connect the modified nodes to their least common predecessor.

    Args:
        modified_nodes (set): A set of modified nodes.
        predecessors (dict): A dictionary mapping nodes to their predecessors.

    Returns:
        tuple: A tuple containing the sequence of nodes that connect the modified nodes to their least common predecessor, and the least common predecessor itself.
    """
    # debug output
    for n in modified_nodes:
        logging.debug(
            f"Extraction: Found node '{n.__class__.__name__} with predecessors {[type(p).__name__ for p in predecessors[n]]}"
        )

    common_predecessors = set.intersection(
        *[set(predecessors[n]) for n in modified_nodes]
    )
    logging.debug(
        f"Extraction: Found common predecessors: {[type(p).__name__ for p in common_predecessors]}"
    )
    least_common_predecessor = None
    for p in predecessors[list(modified_nodes)[0]]:
        if p in common_predecessors:
            least_common_predecessor = p
            break
    if least_common_predecessor is None:
        raise IdefixException("No common predecessor found.")
    logging.debug(
        f"Extraction: Found least common predecessor: {type(least_common_predecessor).__name__}"
    )

    sequence = []
    # must have children as is the common predecessor
    all_successors = set()
    for n in least_common_predecessor:
        successor_visitor = CollectNodesVisitor()
        successor_visitor.visit(n)
        # n is either a common predecessor or a modified node
        # if sequence is not empty, we add all nodes
        # (all consecutive statements covered by the sequence need to be inserted)
        if (
            n in modified_nodes
            or modified_nodes.intersection(successor_visitor.successors)
            or sequence
        ):
            sequence.append(n)
            all_successors |= successor_visitor.successors
        if modified_nodes.issubset(all_successors):
            break
    if not modified_nodes.issubset(all_successors):
        for a in all_successors:
            for b in modified_nodes:
                logging.debug(
                    f"Extraction: {type(a).__name__} == {type(b).__name__}: {a == b or a is b}"
                )
        raise IdefixException(
            f"Extraction: Sequence not contained in successors (sequence={list(map(lambda s: type(s).__name__, sequence))}; successors={list(map(lambda s: type(s).__name__, all_successors))})."
        )  # must hold always as is the common predecessor
    return sequence, least_common_predecessor


def process_task(task: BenchmarkTask, result_output: Path, tmp_output: Path) -> None:
    logging.info("Processing task...")
    os.makedirs(tmp_output, exist_ok=True)
    os.makedirs(result_output, exist_ok=True)
    diffs = task.compute_diffs(tmp_output)
    reversed_task = BenchmarkTask(
        task.fixed_task, task.modified_tasks, task.base_task, task.modified_tasks
    )
    reversed_diffs = list(
        reversed(reversed_task.compute_diffs(tmp_output, prefix="reversed_"))
    )
    if len(reversed_diffs) != len(diffs):
        raise IdefixException("Number of diffs must be equal.")
    partial_fixes = diffs[:-1]
    previous = task.base_task
    # should always be only 1 in our case
    for index, partial_fix in enumerate(partial_fixes):
        logging.info(f"Processing partial fix {index + 1}...")
        logging.info("Extraction: START")
        source_min, source_max, target_min, target_max = find_source_lines(
            PatchSet.from_filename(partial_fix),
            PatchSet.from_filename(reversed_diffs[index]),
        )
        modified_function = find_function(
            partial_fix,
            reversed_diffs[index],
            task.modified_tasks[index],
            use_source=False,
        )
        modified_nodes, predecessors, _ = find_modified_nodes(
            modified_function, target_min, target_max
        )
        sequence, _ = find_sequence(modified_nodes, predecessors)

        logging.debug(
            f"Extraction: Found sequence: {[type(n).__name__ for n in sequence]}"
        )
        compound = pycparser.c_ast.Compound(sequence)
        logging.info("Extraction: DONE")
        logging.info("Preprocessing: START")
        variables = find_variables(compound)
        function_variables = extract_function_names(compound)

        original_function = find_function(partial_fix, reversed_diffs[index], previous)
        original_ast, _ = function_to_ast(original_function)
        original_data_types = find_data_types_of_variables(original_ast)
        for var_name, (var_type, _) in original_data_types.items():
            logging.debug(
                f"Preprocessing: ADD {' '.join(var_type.type.names)} {var_name}"
            )

        # modified_data_types = find_data_types_of_variables(modified_ast)
        assertions = []
        declarations = []
        already_declared = set()
        for v in variables:
            original_name = v.name
            modified_name = f"__IDEFIX_preprocessing_{original_name}"
            if (
                original_name in original_data_types
                and original_name not in already_declared
                and original_name not in function_variables
            ):
                v.name = modified_name
                already_declared.add(original_name)

                declaration_type, original_node = original_data_types[original_name]

                new_type = pycparser.c_ast.TypeDecl(
                    declname=modified_name,
                    type=declaration_type.type,
                    align=declaration_type.align,
                    quals=declaration_type.quals,
                )

                if isinstance(original_node, ArrayDecl):
                    new_type = ArrayDecl(
                        type=new_type,
                        dim=original_node.dim,
                        dim_quals=original_node.dim_quals,
                    )
                elif isinstance(original_node, PtrDecl):
                    new_type = PtrDecl(
                        type=new_type,
                        quals=original_node.quals,
                    )

                new_declaration = pycparser.c_ast.Decl(
                    name=modified_name,
                    type=new_type,
                    init=pycparser.c_ast.ID(name=original_name),
                    bitsize=None,
                    quals=None,
                    align=None,
                    storage=None,
                    funcspec=None,
                )

                declarations.append(new_declaration)
                assertions.append(
                    pycparser.c_ast.BinaryOp(
                        op="!=",
                        left=pycparser.c_ast.ID(name=original_name),
                        right=pycparser.c_ast.ID(name=modified_name),
                    )
                )
        if not assertions:
            raise IdefixException(
                "No assertions found, therefore no partial fix occurred."
            )
        verifier_assert = pycparser.c_ast.FuncCall(
            name=pycparser.c_ast.ID(name="__VERIFIER_assert"),
            args=disjunction_of_assertions(assertions),
        )
        logging.debug(f"Preprocessing: ADD assertions (omitted)")
        compound.block_items = declarations + compound.block_items + [verifier_assert]
        logging.info("Preprocessing: DONE")
        logging.info("Addition: START")
        original_function = find_function(partial_fix, reversed_diffs[index], previous)
        modified_nodes, predecessors, original_ast = find_modified_nodes(
            original_function, source_min, source_max
        )
        sequence, least_common_predecessors = find_sequence(
            modified_nodes, predecessors
        )
        updated_list = []
        successor_index = 0
        for successor in least_common_predecessors:
            if successor not in sequence:
                if successor_index == len(sequence):
                    for c in compound:
                        updated_list.append(c)
                    successor_index += 1
                else:
                    updated_list.append(successor)
            else:
                successor_index += 1
                updated_list.append(successor)

        # TODO: non-exhaustive list
        if hasattr(least_common_predecessors, "block_items"):
            least_common_predecessors.block_items = updated_list
        elif hasattr(least_common_predecessors, "exprs"):
            least_common_predecessors.exprs = updated_list
        elif hasattr(least_common_predecessors, "stmts"):
            least_common_predecessors.stmts = updated_list
        generator = CGenerator()
        code = generator.visit(pycparser.c_ast.Compound(original_ast))
        if code[0] == "{" and code[-1] == "}":
            code = code[1:-1]
        result_output.joinpath(f"preprocessed_partial_fix_{index + 1}.c").write_text(
            code
        )
        logging.info("Addition: DONE")
        logging.info(f"Done processing partial fix {index + 1}.")
        previous = task.modified_tasks[index]
    logging.info("Done processing task.")


def disjunction_of_assertions(
    assertions: [pycparser.c_ast.BinaryOp],
) -> pycparser.c_ast.BinaryOp:
    if len(assertions) == 1:
        return assertions[0]
    if len(assertions) == 2:
        return pycparser.c_ast.BinaryOp(
            op="||", left=assertions[0], right=assertions[1]
        )
    return pycparser.c_ast.BinaryOp(
        op="||", left=assertions[0], right=disjunction_of_assertions(assertions[1:])
    )


def parse_args() -> argparse.Namespace:
    parser = argparse.ArgumentParser(
        description="Converts a folder of a single candidate to a benchmark for Idefix"
    )
    parser.add_argument("JSON", type=Path, help="Path to candidate")
    parser.add_argument("OUTPUT", type=Path, help="Path to candidate")
    parser.add_argument(
        "--task", action="store_true", help="Treat directory as path", default=False
    )
    parser.add_argument(
        "--preprocess",
        action="store_true",
        help="Whether to preprocess the files",
        default=False,
    )
    return parser.parse_args()


def process_info_json(arguments, info_json: Path, preprocess=False, from_task=False):
    path = os.path.normpath(str(info_json)).split(os.sep)[:-1]
    owner = path[-2]
    repo = path[-1]
    target_path = arguments.OUTPUT / owner / repo
    target_path.mkdir(parents=True, exist_ok=True)
    tmp_path_processing = target_path / "tmp" / "instrumentation"
    output_path_processing = target_path / "results" / "instrumentation"
    if from_task:
        task = (
            BenchmarkTask.from_directory(info_json)
            .eliminate_duplicates_at_end()
            .squash_diffs()
        )
    else:
        diff_list = list(diff_list_from_info(info_json))
        task = (
            BenchmarkTask.from_diffs(
                diffs=diff_list,
                output=tmp_path_processing,
            )
            .eliminate_duplicates_at_end()
            .squash_diffs()
        )
    logging.debug(f"Processing {info_json} with {task}")
    process_task(task, output_path_processing, tmp_path_processing)
    if preprocess:
        preprocess_files(
            task,
            diff_list,
            target_path / "results" / "preprocessing",
            target_path / "tmp" / "preprocessing",
            output_path_processing,
        )
    logging.debug(f"Finished {info_json}.")
    with arguments.OUTPUT.joinpath("success.txt").open(mode="a") as success_file:
        success_file.write(f"{info_json}\n")


def main():
    arguments = parse_args()
    arguments.OUTPUT.mkdir(parents=True, exist_ok=True)
    if arguments.JSON.is_dir():
        jsons = list(arguments.JSON.glob("**/**/info.json"))
    else:
        jsons = [arguments.JSON]
    if arguments.task:
        jsons = [arguments.JSON]

    arguments.OUTPUT.joinpath("skipped.txt").touch()
    arguments.OUTPUT.joinpath("success.txt").touch()
    processed = set(
        arguments.OUTPUT.joinpath("skipped.txt").read_text().splitlines()
    ) | set(arguments.OUTPUT.joinpath("success.txt").read_text().splitlines())

    i = 0
    while i != len(jsons):
        info_json = jsons[i]
        i += 1
        if str(info_json) in processed:
            logging.debug(f"Skip already processed file {info_json}.")
            continue
        try:
            process_info_json(
                arguments,
                info_json,
                preprocess=arguments.preprocess,
                from_task=arguments.task,
            )
        except IdefixException as e:
            logging.warning(f"Skip {info_json}: {e=}")
            with arguments.OUTPUT.joinpath("skipped.txt").open(
                mode="a"
            ) as skipped_file:
                skipped_file.write(f"{info_json}\n")
        except requests.exceptions.HTTPError as e:
            logging.warning(f"Exceeded rate limit.")
            time.sleep(100)
            logging.warning("Retrying...")
            # Retry
            i -= 1


if __name__ == "__main__":
    main()


# redo extract function with ast
