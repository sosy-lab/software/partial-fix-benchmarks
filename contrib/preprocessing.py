import logging
import os
import re
import shutil
import subprocess
from pathlib import Path
from typing import List

import regex
from git import Repo


def clone_repository(repository_url, tmp_path):
    logging.info("Cloning repository")
    if os.path.exists(tmp_path):
        return

    os.makedirs(tmp_path, exist_ok=True)
    Repo.clone_from(repository_url, tmp_path)
    return


def setup_one_line_scan(repository_path):
    logging.info("Setting up one line scan")
    if os.path.exists(repository_path):
        return
    os.makedirs(repository_path, exist_ok=True)
    Repo.clone_from("https://github.com/awslabs/one-line-scan", repository_path)
    return


def get_function_definition(contents):
    for line in contents.splitlines():
        matches = re.findall("([a-zA-Z]+ [a-zA-Z][a-zA-Z0-9_]*\()", line)
        if len(matches) not in [0, 1]:
            logging.error(f"Found multiple function declaration in line: {line}")
            return None
        if len(matches) == 1:
            return matches[0]
    return None


def get_function_declaration(function_definition: str, partial_fix_contents: str):
    start_line = None
    end_line = None
    opening_brackets = 0
    closing_brackets = 0
    for i, line in enumerate(partial_fix_contents.splitlines()):
        if function_definition in line and not ";" in line:
            start_line = i
        if start_line is not None:
            opening_brackets += line.count("{")
            closing_brackets += line.count("}")
        if opening_brackets > 0 and opening_brackets == closing_brackets:
            end_line = i
            break

    return "\n".join(partial_fix_contents.splitlines()[start_line : end_line + 1])


def replace_function_declaration(
    function_definition, function_declaration, modified_file_text
):
    function_declaration_in_modified_file = get_function_declaration(
        function_definition, modified_file_text
    )
    return modified_file_text.replace(
        function_declaration_in_modified_file, function_declaration
    )


def preprocess_files(
    task, diff_list, result_path: Path, tmp_path: Path, instrumentation_results: Path
):
    logging.info("Preprocessing files")
    os.makedirs(tmp_path, exist_ok=True)
    os.makedirs(result_path, exist_ok=True)
    one_line_scan_executable = tmp_path / "one-line-scan" / "one-line-scan"
    setup_one_line_scan(one_line_scan_executable.parent)

    project_url = diff_list[0].split("commit")[0]
    repository_path = os.path.join(
        tmp_path, diff_list[0].split("commit")[0].split("/")[-2]
    )
    clone_repository(project_url, repository_path)

    one_line_scan_executable = str(one_line_scan_executable.absolute())

    preprocessed_files_output = (tmp_path / "preprocessed-files").absolute()
    os.makedirs(preprocessed_files_output, exist_ok=True)

    cwd = os.getcwd()
    os.chdir(repository_path)
    output_dir_one_line_scan = str(Path("../output-one-line-scan").absolute())
    if os.path.exists(output_dir_one_line_scan):
        shutil.rmtree(output_dir_one_line_scan)

    command = f'{one_line_scan_executable} --plain -o {output_dir_one_line_scan} --extra-cflag-set "-save-temps" -- make'
    process = subprocess.run(
        command,
        shell=True,
        capture_output=True,
    )

    stdout = process.stdout.decode("utf-8")
    stderr = process.stderr.decode("utf-8")
    if "fatal error:" in stdout or "exists already" in stdout:
        logging.error("Stdout:\n" + stdout)
        logging.error("Stderr:\n" + stderr)
        exit(1)

    os.chdir(cwd)

    logging.info("Replacing instrumented function in original program")
    for file in os.scandir(repository_path):
        if file.name.endswith(".i"):
            shutil.copy(file.path, preprocessed_files_output / file.name)

    modified_files: List[str] = list(
        set(filter(lambda x: len(x) != 0, task.modified_files.read_text().splitlines()))
    )
    if not len(modified_files) == 1:
        logging.error("Currently we can only handle a single modified file!")
    preprocessed_modified_file = modified_files[0].split("/")[-1].replace(".c", ".i")

    # TODO: Make more efficient
    for partial_fix in os.scandir(instrumentation_results):
        partial_fix_contents = Path(partial_fix.path).read_text()

        # TODO: This way of getting the commit should be checked
        commit_hash = diff_list[0].split("commit/")[1].split(".diff")[0].strip()
        repo = Repo(repository_path)
        repo.git.checkout(commit_hash)

        # This should be the part from the beginning of the return type till the first opening bracket
        function_definition = get_function_definition(partial_fix_contents)
        if function_definition is None:
            return

        modified_file_text = (
            preprocessed_files_output / preprocessed_modified_file
        ).read_text()
        if function_definition not in modified_file_text:
            logging.error(
                f"The function declaration {function_definition} is not in the preprocessed file {preprocessed_modified_file}"
            )

        function_declaration = get_function_declaration(
            function_definition, partial_fix_contents
        )

        instrumented_preprocessed_file_contents = replace_function_declaration(
            function_definition, function_declaration, modified_file_text
        )

        (result_path / partial_fix.name).write_text(
            instrumented_preprocessed_file_contents
        )

    logging.info("Finished preprocessing")

    return
