import glob
import json
import os
from multiprocessing import Pool
import sys
from pathlib import Path
import shutil

infos = glob.glob(f"../benchmarks_{sys.argv[1]}/**/**/info.json")


def select(file_path: str):
    file_names = set()
    changed = 0
    with open(file_path, "r") as fp:
        entries = json.load(fp)
        for c in entries["commits"]:
            changed += c["diff_additions"] + c["diff_deletions"]
            for f in c["files"]:
                file_names.add(f["file_name"])
    return file_path if len(file_names) == 1 and changed < 20 else None


with Pool(os.cpu_count()) as p:
    results = set(p.map(select, infos))
    if None in results:
        results.remove(None)
    target_path = Path("../benchmarks_filtered/small/")
    target_path.mkdir()
    for r in results:
        parent = Path(r).parent
        shutil.copytree(r, target_path)
        exit(1)

print("\n".join(results))
print("=====", len(results), "=====")
