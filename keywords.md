* build problems
* null pointers
* arithmetic and control-flow (add, remove, check, fix, avoid, miss, refactor, arithmetic, division) 
* preprocessing directives (CI, build, warning, treat, stop)
* hardware and OS related (windows, linux, os, mac, distribution, android, hardware, driver, controller, framework)
* wrong API usage (reorder, rearrange, change, suspect, move)
* race conditions (sequential, mutual, exclusion, semaphore, thread, process, finish, access, deadlocks) 
* performance (perform, run, time)
* memory leaks (memory, segmentation, fault, leak)
* new features
* unrelated bugs
* reverted commits
* regressions
* introduces bug
* same fix different location (same fix, backport, reverted and applied again)
* code quality (dead code, refactoring)
* data types
* segmentation fault
* work in progress (accidentally closed issue)
* license issues
* library issues
* termination
* crashes