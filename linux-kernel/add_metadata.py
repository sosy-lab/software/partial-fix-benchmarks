import json
import os
import urllib.request
from pathlib import Path
from urllib.error import HTTPError

import git

if __name__ == "__main__":
    linux_kernel_path = Path("../benchmarks_2023/linux-kernel/")

    repository = git.Repo("linux")

    for issue in os.scandir(linux_kernel_path):
        if not issue.is_dir():
            continue

        issue_path = Path(issue.path)
        issue_contents = json.loads((issue_path / "info.json").read_text())
        path_commit_messages = issue_path / "commit_messages.txt"
        path_info_json = issue_path / "info.json"

        print(issue.name)

        if "commits" in issue_contents.keys() and isinstance(
            issue_contents["commits"], list
        ):
            continue

        print(issue_contents)

        sorted_commits = list(
            map(
                lambda x: x["diff_url"].split("/")[-1].split(".")[0],
                issue_contents["commits"]["commits"],
            )
        )

        commits_json = []
        path_issue_messages = issue_path / "issue_comments.txt"
        path_issue_messages.write_text("Nothing to see here, please continue.")

        all_commit_messages = []

        for current_commit in sorted_commits:
            # Get the commit diffs from github
            diff_url = (
                "https://github.com/torvalds/linux/commit/" + current_commit + ".diff"
            )

            diff_path = path_info_json.parent / f"commit_{current_commit}.diff"

            try:
                page = urllib.request.urlopen(diff_url)
                diff_contents = page.read().decode("utf-8")
                diff_path.write_text(diff_contents)
            except HTTPError:
                diff_path.write_text(
                    "HTTP Error when getting diff. One problem could be that the diff is too big."
                )

            commits_json.append(
                {
                    "diff_url": diff_url,
                    "diff": str(diff_path).replace("../benchmarks_2023/", ""),
                    "sha": current_commit,
                }
            )

            all_commit_messages.append(repository.commit(current_commit).message)

        info_json = {"commits": commits_json}

        path_commit_messages.write_text(
            "\n\n-----------------------------------------------------------------------------------------------------------------\n\n".join(
                all_commit_messages
            )
        )
        os.makedirs(path_info_json.parent, exist_ok=True)
        path_info_json.write_text(json.dumps(info_json))
