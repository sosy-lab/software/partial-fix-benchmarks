import json
import os
import pickle
from collections import defaultdict
from pathlib import Path

import git


def get_full_commit_hash(short_commit_hash):
    repo = git.Repo("linux")
    try:
        full_sha = repo.commit(short_commit_hash).hexsha
    except Exception:
        print("Could not find commit hash for", short_commit_hash)
        full_sha = None

    return full_sha


if __name__ == "__main__":
    # Generated from the linux kernel repository using: git log --grep="Fixes: [0-9a-zA-Z]*" --all | grep -E "^commit|Fixes:(.*)\(" > ../commits-with-fixes.txt
    file = Path("./commits-with-fixes.txt")
    # Delete the last couple of lines of the file which are not in the expected format

    path_output = Path("../benchmarks_2023/linux-kernel/")

    os.makedirs(path_output, exist_ok=True)

    commits_to_fixes = defaultdict(list)
    fixes_to_commits = defaultdict(list)

    if os.path.exists("commits_to_fixes.pkl"):
        commits_to_fixes = pickle.load(open("commits_to_fixes.pkl", "rb"))
        fixes_to_commits = pickle.load(open("fixes_to_commits.pkl", "rb"))
    else:
        current_commit = None
        for i, line in enumerate(file.read_text().splitlines()):
            if line.startswith("commit"):
                current_commit = line.split(" ")[1]
            elif line.lstrip().startswith("Fixes:"):
                fixes = (
                    line.split("Fixes:")[1]
                    .split("(")[0]
                    .strip()
                    .split('"')[0]
                    .split("'")[0]
                    .split("eventfs")[0]
                    .split("hwmon")[0]
                    .split("init")[0]
                    .split("drm")[0]
                    .split("ipmi")[0]
                    .split("MIPS")[0]
                    .split("lightnvm")[0]
                    .split("qeth")[0]
                    .split("slip")[0]
                    .split("smc")[0]
                    .split("treewide")[0]
                    .split("can")[0]
                    .split("smc")[0]
                    .split("x86")[0]
                    .split("bpf")[0]
                    .split("firmware")[0]
                    .split("powerpc")[0]
                    .split("of")[0]
                    .split("ACPI")[0]
                    .split("hrtimer")[0]
                    .split("scsi_dh_rdac")[0]
                    .split("ASoC")[0]
                    .split("qeth")[0]
                    .split("slip")[0]
                    .split("smc")[0]
                    .split("-")[0]
                    .split(">")[0]
                    .split(":")[0]
                    .strip()
                )

                if "commit" in fixes:
                    fixes = fixes.split("commit")[1].strip()
                if "tag" in fixes:
                    fixes = fixes.split("tag")[1].strip()
                if "Commit" in fixes:
                    fixes = fixes.split("Commit")[1].strip()
                if "'" in fixes:
                    fixes = fixes.split("'")[1].strip()
                if "^" in fixes:
                    fixes = fixes.split("^")[0].strip()
                if "<" in fixes:
                    fixes = fixes.split(" ")[0].strip()

                full_hash = get_full_commit_hash(fixes)
                if full_hash is not None:
                    commits_to_fixes[current_commit].append(full_hash)
                    fixes_to_commits[full_hash].append(current_commit)

            if i % 1000 == 0:
                print("After line:", i)
                print("Amount partial fixes:", len(commits_to_fixes.keys()))
                print(
                    "Commits fixing the same bug: "
                    + str(len([v for k, v in fixes_to_commits.items() if len(v) > 1]))
                )

    pickle.dump(commits_to_fixes, open("commits_to_fixes.pkl", "wb"))
    pickle.dump(fixes_to_commits, open("fixes_to_commits.pkl", "wb"))

    issue_number = 0
    for commit, fixes in fixes_to_commits.items():
        if len(fixes) != 1:
            continue

        fix = fixes[0]

        commit_info = []
        sorted_commits = [
            commit,
            fix,
        ]

        current_hash = fix
        while current_hash in commits_to_fixes.keys():
            current_hash = commits_to_fixes[current_hash][0]
            sorted_commits.append(current_hash)

        sorted_commits = sorted_commits[::-1]

        info_json = []
        for current_commit in sorted_commits:
            info_json.append(
                {
                    "diff_url": "https://github.com/torvalds/linux/commit/"
                    + current_commit
                    + ".diff"
                }
            )

        info_json = {"commits": info_json}

        if len(sorted_commits) != 2:
            path_info_json = path_output / f"unknown_issue_{issue_number}" / "info.json"
            os.makedirs(path_info_json.parent, exist_ok=True)
            path_info_json.write_text(json.dumps({"commits": info_json}))
            print("Length chain:", len(sorted_commits))
            issue_number += 1
