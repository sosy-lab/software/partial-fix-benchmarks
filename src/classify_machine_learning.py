import json
from collections import Counter

import numpy as np
import pandas as pd

from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer, TfidfTransformer
from sklearn.feature_selection import chi2
from sklearn.naive_bayes import MultinomialNB

from src.classify_mockus_votta import dataframe_from_excel

from sklearn.linear_model import LogisticRegression, SGDClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.svm import LinearSVC
from src.offline_updates import offline_update

import re
import src.file_manager as fm
from nltk.tokenize import word_tokenize

from sklearn.model_selection import cross_val_score

import src.offline_updates as benchmark_set
from nltk.stem.snowball import EnglishStemmer


def get_data(path, engine="odf"):
    df = pd.read_excel(path, engine=engine)

    texts = list()
    categories = list()
    categories_id = list()
    label_count = dict()

    for _, row in df.iterrows():
        issue_directory = "../benchmarks/" + fm.valid_file_name_of(row["repository"]) + "/issue_" + str(
            int(row["issue_number"])) + "/"
        comments = open(issue_directory + "issue_comments.txt").read()
        messages = open(issue_directory + "commit_messages.txt").read()
        labels = " ".join(json.load(open(issue_directory + "info.json"))["score"]["all_labels"])
        texts.append(comments + " " + messages + " " + labels)
        categories.append(row["category"])
        categories_id.append(int(row["category_id"]))
        if row["category"] not in label_count:
            label_count[row["category"]] = Counter(
                json.load(open(issue_directory + "info.json"))["score"]["all_labels"])
        else:
            label_count[row["category"]] += Counter(json.load(open(issue_directory + "info.json"))["score"]["all_labels"])

    print(label_count)
    return texts, categories, categories_id


def run_tests_and_find_unigrams():
    def repo_names_to_stop_word():
        repo_stop_words = set()
        offline_update(lambda repo, issue: [repo_stop_words.add(x) for x in
                                            repo["url"].replace("https://github.com", "").split("/")])
        repo_stop_words.remove("")
        return repo_stop_words

    def stopwords_to_frozenset():
        return frozenset([word.replace("\n", "") for word in open("../training_data/stop-words.txt").readlines()] +
                         list(repo_names_to_stop_word()))

    stop_words = stopwords_to_frozenset()
    stemmer = EnglishStemmer()

    def preprocessing(line):
        line = re.sub(r"[^a-zA-Z]", " ", line.lower())
        all_words = word_tokenize(line)
        words_lemmed = [stemmer.stem(w) for w in all_words if w not in stop_words]
        return words_lemmed

    texts, categories, labels = get_data("../training_data/TrainingData-Test.ods")
    df = dataframe_from_excel("../training_data/TrainingData-Test.ods")
    category_to_id = dict(zip(categories, labels))
    labels = df["category_id"]
    tfidf = TfidfVectorizer(tokenizer=preprocessing, sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1',
                            ngram_range=(1, 2), analyzer='word')
    features = tfidf.fit_transform(texts).toarray()

    number_grams = 5
    correlated = dict()
    for cat_text, cat_id in category_to_id.items():
        features_chi2 = chi2(features, labels == cat_id)
        indices = np.argsort(features_chi2[0])
        feature_names = np.array(tfidf.get_feature_names())[indices]
        unigrams = [v for v in feature_names if len(v.split(' ')) == 1]
        bigrams = [v for v in feature_names if len(v.split(' ')) == 2]
        print("# '{}':".format(cat_text))
        print("  - Most correlated unigrams:\n      . {}".format('\n      . '.join(unigrams[-number_grams:])))
        print("  - Most correlated bigrams:\n      . {}".format('\n      . '.join(bigrams[-number_grams:])))
        words = dict()
        words["unigram"] = unigrams[-number_grams:]
        words["bigrams"] = bigrams[-number_grams:]
        correlated[cat_text] = words

    models = [
        RandomForestClassifier(),
        LinearSVC(),
        MultinomialNB(),
        LogisticRegression(random_state=0),
        SGDClassifier(loss='hinge', penalty='l2',
                      alpha=0.01, random_state=42,
                      max_iter=20, tol=None),
    ]

    cross_validation_size = 20
    entries = []
    for model in models:
        model_name = model.__class__.__name__
        accuracies = cross_val_score(model, features, labels, scoring='accuracy', cv=cross_validation_size)
        for fold_idx, accuracy in enumerate(accuracies):
            entries.append((model_name, fold_idx, accuracy))

    cv_df = pd.DataFrame(entries, columns=['model_name', 'fold_idx', 'accuracy'])
    print(cv_df.groupby('model_name').accuracy.mean())


def categorize_benchmarks_do_not_modify_jsons():

    def repo_names_to_stop_word():
        repo_stop_words = set()
        offline_update(lambda repo, issue: [repo_stop_words.add(x) for x in
                                            repo["url"].replace("https://github.com", "").split("/")])
        repo_stop_words.remove("")
        return repo_stop_words

    def stopwords_to_frozenset():
        return frozenset([word.replace("\n", "") for word in open("../training_data/stop-words.txt").readlines()] +
                         list(repo_names_to_stop_word()))

    stop_words = stopwords_to_frozenset()
    stemmer = EnglishStemmer()

    def preprocessing(line):
        line = re.sub(r"[^a-zA-Z]", " ", line.lower())
        all_words = word_tokenize(line)
        words_lemmed = [stemmer.stem(w) for w in all_words if w not in stop_words]
        return words_lemmed

    label_class = {
        "network/protocol": {"network"},
        "feature request": {"feature", "enhancement"},
        "build/refactor/preprocessing": {"build", "refactor", "cleanup"}
    }

    text_class = {
        "network/protocol": {"network", "tcp", "udp", "protocol", "ethernet", "packet"},
        "feature request": {"feature", "enhance", "discussion", "extend", "nice to have", "request"},
        "build/refactor/preprocessing": {"build", "refactor", "preprocess", "clean", "warn", "ci"},
        "GUI": {"window", "focus", "mouse", "layout", "window", "fullscreen"},
        "wrong API usage": {"arrange", "change", "move", "api", "library", "assumption"},
        "control flow": {"arithmetic", "operation", "avoid", "miss", "check", "add"},
        "multithreading": {"syscal", "race", "thread", "process", "mutual", "semaphore"},
        "hardware/OS": {"windows", "linux", "os", "hardware", "driver", "distribution"},
        "memory": {"valgrind", "segmentation", "segfault", "leak", "memory", "buffer"}
    }

    texts, categories, labels = get_data("../training_data/TrainingData-Test.ods")
    df = dataframe_from_excel("../training_data/TrainingData-Test.ods")
    category_to_id = dict(zip(labels, categories))
    tfidf = TfidfVectorizer(tokenizer=preprocessing, sublinear_tf=True, min_df=5, norm='l2', encoding='latin-1', analyzer='word')
    features = tfidf.fit_transform(texts).toarray()
    classifier = SGDClassifier(loss='hinge', penalty='l2',
                      alpha=0.01, random_state=42,
                      max_iter=20, tol=None).fit(features, labels)

    def update_function(repo, issue):
        issue_json = issue["root"] + "info.json"
        data = json.load(open(issue_json))

        text = " ".join(data["score"]["all_labels"]) + " " + open(issue["root"] + "issue_comments.txt").read() \
               + " " + open(issue["root"] + "commit_messages.txt").read()
        text = text.lower()

        data["category"] = set()

        for label in data["score"]["all_labels"]:
            for key in label_class:
                for identifier_label in label_class[key]:
                    if label.lower() in identifier_label.lower():
                        data["category"].add(key)
                        break

        score = dict()
        count = 0
        category = ""
        for word in text.split():
            for key in text_class:
                score[key] = 0
                for identifier in text_class[key]:
                    if identifier in word or word in identifier:
                        score[key] += 1
                        if count < score[key]:
                            category = key

        data["category"].add(category)
        data["category"].add(category_to_id[classifier.predict(tfidf.transform([text]))[0]])

        data["category"] = list(data["category"])
        fm.dump_json(fm.valid_file_name_of(repo["url"].replace("https://github.com/", "")) + "/issue_" + str(issue["number"]) + "/info.json", data)

        print(repo["url"] + "/issues/" + str(issue["number"]), data["category"])

        # print(data["category"], "->", url)
        # if data["category"] == "hardware/OS":
        #    print(repo["url"] + "/issues/" + str(issue["number"]))

    benchmark_set.offline_update(lambda repo, issue: update_function(repo, issue))


categorize_benchmarks_do_not_modify_jsons()
# run_tests_and_find_unigrams()
