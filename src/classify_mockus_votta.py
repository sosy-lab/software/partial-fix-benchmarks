import src.offline_updates as data
import pandas as pd
import numpy as np

from collections import Counter
from stemming.porter2 import stem
from math import e
from nltk.corpus import stopwords

import src.file_manager as fm
from sklearn.feature_extraction.text import CountVectorizer


def collect_data(query=None):
    """
    Collect a dictionary of every found repository mapping to repository data and issue data
    :return: dictionary which maps repository urls to repository data and issue data
    """

    def append_issue_to(repo, issue, data_dict):
        if repo["url"] in data_dict:
            data_dict[repo["url"]]["issues"].append(issue)
        else:
            data_dict[repo["url"]] = {"repo": repo, "issues": [issue]}

    collected_data = dict()
    data.offline_update(lambda r, issue: append_issue_to(r, issue, collected_data), query=query)
    return collected_data


def tokenize(text, stems_only=False):
    """
    Tokenize a text and normalize the tokens according to
    Mockus & Votta: Identifying Reasons for Software Changes using Historic Databases (Step 1).
    Most special characters are replaced with whitespace.
    :param text: the text to tokenize
    :param stems_only: whether to consider the stems of a word only (cats -> cat)
    :return: list of lowercase tokens in the text
    """
    for char in "'\\\n.,?!#()[]{}-~+;:&|<>`=/@_*$^\"":
        text = text.replace(char, " ")
    if stems_only:
        return [stem(word).lower() for word in text.split()]
    else:
        return [word.lower() for word in text.split()]


def count_words(token_list):
    """
    Returns a Counter which maps words to their occurrence count.
    (Step 2 according to Mockus and Votta)
    :param token_list: list of tokens
    :return: tokens mapping to the number of occurrences
    """
    return Counter(token_list)


def has_number(word):
    """
    Check if a token has numbers in it. This most certainly indicates that the token is either a hex number or a
    filename. Both are not worth to be considered as text token as they are issue specific.
    :param word: check if this word contains a number
    :return: whether the given word contains at least one digit
    """
    for char in "0123456789":
        if char in word:
            return True
    return False


def normalize_and_count_words_in_all_texts():
    """
    Return a dictionary mapping every stem of a token to its occurrence count
    :return: Counter that counts overall occurrence of a word and Counter that counts occurrences in number of files
    """
    global_counter = Counter()
    distribution = Counter()
    repo_counter = Counter()
    all_data = collect_data()
    for repo in all_data:
        repo_tokens = set()
        for issue in all_data[repo]["issues"]:
            comments = open(issue["issue_comments"]).read()
            commit_msg = open(issue["commit_messages"]).read()
            tokens = [w for w in tokenize(comments + " " + commit_msg, stems_only=True) if not has_number(w) and len(w) > 1]
            repo_tokens = repo_tokens.union(set(tokens))
            distribution += Counter(set(tokens))
            global_counter += Counter(tokens)
        repo_counter += Counter(repo_tokens)
    return global_counter, distribution, repo_counter


def word_count_to_dataframe():
    occurrence = pd.DataFrame(columns=["word", "total_occ", "distribution", "repo_distribution"])
    every, dist, repo = normalize_and_count_words_in_all_texts()
    every = dict(every)
    dist = dict(dist)
    repo = dict(repo)
    counter = 0
    for key in every:
        occurrence.loc[counter] = [key, every[key], dist[key], repo[key]]
        counter += 1
    return occurrence


def dataframe_to_excel(dataframe, relative_file_path, engine="xlsxwriter"):
    # use xlsx as on default any characters are supported
    dataframe.to_excel(relative_file_path, engine=engine, index=False)


def dataframe_from_excel(relative_path, engine="odf"):
    return pd.read_excel(relative_path, engine=engine)


def remove_anomalies_and_prepare_data(count_dataframe):
    # filter keywords that do not occur often
    count_dataframe = count_dataframe[count_dataframe["total_occ"] + count_dataframe["distribution"] >= 20]
    count_dataframe = count_dataframe[count_dataframe["total_occ"] > 5]
    count_dataframe = count_dataframe[count_dataframe["distribution"] > 5]
    count_dataframe = count_dataframe[count_dataframe["repo_distribution"] > 3]

    # sort descending by common words
    count_dataframe = count_dataframe.sort_values("total_occ", ascending=False)
    return count_dataframe


def get_clean_frame():
    df = remove_anomalies_and_prepare_data(word_count_to_dataframe())
    sw = stopwords.words("english")
    flat_list = [item for sublist in list(map(lambda word: tokenize(word, True), sw)) for item in sublist]
    return df[~df['word'].isin(flat_list)]


def write_clean_sheet():
    dataframe_to_excel(get_clean_frame(), "../training_data/clean_data.xlsx")


def niche_classification(data_frame, eta=10.0):
    # niche words are words that have a high occurrence in a few issues. These words may indicate the category
    # (=> perfect indication) or are simply overused in one repository (=> outlier)
    # sigmoid function (1 / (1 + e ^ (-x))) applied to 10 * ln(total_occ) / distribution simplifies to this equation:
    data_frame["niche"] = (data_frame["total_occ"] ** (eta / data_frame["distribution"]) - 1) / \
                          (data_frame["total_occ"] ** (eta / data_frame["distribution"]) + 1)


def keyword_classification(data_frame):
    data_frame["keyword_score"] = np.multiply(data_frame["distribution"], np.log10(data_frame["total_occ"].astype('float')))
    average = data_frame["keyword_score"].mean()
    # sigmoid function (1 / (1 + e ^ (-x))) applied to (distribution * log10(total_occ) / average)
    # words with a score in [.6, 1] are common keywords (i.e. the word has many occurrences in many issues)
    data_frame["keyword"] = 2 / (1 + e**(-data_frame["keyword_score"]/average)) - 1


def run_collection():
    clean_frame = get_clean_frame()
    niche_classification(clean_frame)
    keyword_classification(clean_frame)
    dataframe_to_excel(clean_frame, "distribution.xlsx")


def train_on_labeled_data(path="../training_data/TrainingData-Test.ods"):
    df = dataframe_from_excel(path)
    category_to_text = dict()
    for index, row in df.iterrows():
        if row["category"] not in category_to_text:
            category_to_text[row["category"]] = list()
        issue_directory = "benchmarks/" + fm.valid_file_name_of(row["repository"]) + "/issue_" + str(
            row["issue_number"]) + "/"
        category_to_text[row["category"]].append(issue_directory + "issue_comments.txt")
        category_to_text[row["category"]].append(issue_directory + "commit_messages.txt")
    token_set = set()
    for category in category_to_text:
        docs = list()
        for link in category_to_text[category]:
            docs += tokenize(open("../" + link).read(), True)
        token_set = token_set.union(docs)
        count_vect = CountVectorizer()
        x = count_vect.fit_transform(docs)
        occurrence = pd.DataFrame(x.toarray(), columns=count_vect.get_feature_names())
        word_list = count_vect.get_feature_names();
        count_list = x.toarray().sum(axis=0)
        freq_q = dict(zip(word_list, count_list))

        category_to_text[category] = {
            "freq": freq_q,
            "features": len(set(docs))
        }
    return category_to_text, len(token_set)


def classify(text, category_to_text, total_features):
    new_word_list = tokenize(text, True)
    print(new_word_list)

    score = dict()

    for category in category_to_text:
        prob_q_with_ls = []
        for word in new_word_list:
            if word in category_to_text[category]["freq"].keys():
                count = category_to_text[category]["freq"][word]
            else:
                count = 0
            prob_q_with_ls.append((count + 1) / (category_to_text[category]["features"] + total_features))
        prob_q_with_ls = list(map(lambda x: x*1000, prob_q_with_ls))
        score[category] = np.prod(prob_q_with_ls)

    print(score)


# classify(open("../benchmarks/i3_i3/issue_555/issue_comments.txt").read(), *train_on_labeled_data("../training_data/TrainingData-Test.ods"))
