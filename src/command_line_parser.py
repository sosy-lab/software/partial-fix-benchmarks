import argparse
import os
from pathlib import Path


def parse():
    """
    Parse the command line arguments:
    use "-f" to queue repositories contained in this file, use -r to queue repositories by hand and use -b to set the
    output folder. Use -s to select preferred strategy. Use -u to update existing benchmarks.
    :return: dictionary that maps the arguments to the given inputs
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('-f', '--files', type=argparse.FileType('r'), nargs='+', help='add file(s) with repositories')
    parser.add_argument('-r', '--repositories', nargs='+', help='add repositories')
    parser.add_argument('-b', '--benchmark', help='set default output folder')
    parser.add_argument('-s', '--strategy',
                        help='which strategy to use, possible values: "reopen" for reopen strategy, '
                             '"status" for status strategy or "score" for score strategy')
    parser.add_argument('-u', '--update', help='Update found benchmarks after code changes that did not influence the '
                        'the main selection criteria', action='store_true')
    args = parser.parse_args()

    return {
        "benchmark": args.benchmark if args.benchmark is not None else Path(__file__).parent.resolve() / "/../benchmarks/",
        "repos": [] if not args.repositories else args.repositories,
        "files": [] if not args.files else [wrapper.name for wrapper in args.files],
        "strategy": "score" if not args.strategy else args.strategy,
        "update": args.update
    }


arguments = parse()
