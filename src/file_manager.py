import json
import re
import shutil
import os
import logging as logger

from pathlib import Path
from os import walk
from datetime import datetime
from unidiff import PatchSet, UnidiffParseError

import src.command_line_parser as cmd
from src.statistics import RepositoryStatistics, IssueStatistics

benchmark_folder = cmd.arguments["benchmark"] if cmd.arguments["benchmark"].endswith("/") else cmd.arguments["benchmark"] + "/"
history_directory = "history/"
history_file = "history.json"


def find_already_processed_benchmarks():
    """
    Synthesize a list of all covered issues and repositories form the specified benchmarks folder
    :return: dictionary that maps repository names to their issues.
    """
    repo_to_issue = dict()
    dirs = [name for name in os.listdir(benchmark_folder) if os.path.isdir(os.path.join(benchmark_folder, name))]
    for repo in dirs:
        name = None
        issues = list()
        for file in os.listdir(benchmark_folder + repo):
            if file == "repository.json":
                repo_json = benchmark_folder + repo + "/repository.json"
                name = json.load(open(repo_json))["url"].replace("https://github.com/", "")
            elif file.startswith("issue_"):
                issues.append(int(file.replace("issue_", "")))
        if name is not None and issues:
            repo_to_issue[name] = issues
    return repo_to_issue


def read_command_line():
    """
    Read the command line and prepare a list of repositories to process.
    :return: list of repositories for further analysis
    """
    repos = set()
    for file in cmd.arguments["files"]:
        with open(file) as input_file:
            for line in input_file.readlines():
                if "github.com/" in line:
                    repos.add(line.split("github.com/")[1])
                else:
                    repos.add(line)
    for repo in cmd.arguments["repos"]:
        if "github.com/" in repo:
            repos.add(repo.split("github.com/")[1])
        else:
            repos.add(repo)
    return repos, cmd.arguments["strategy"], cmd.arguments["update"]


def write_issue_statistics(repo_name: str, issue_stats: IssueStatistics):
    """
    Write statistics of an issue to the info.json in its folder
    :param repo_name: name of the parent repository
    :param issue_stats: collected data of this issue
    """
    issue_number = issue_stats.number
    issue_folder = valid_file_name_of(repo_name) + "/issue_" + str(issue_number) + "/"
    issue_info = issue_folder + "info.json"
    log("Add info for interesting issue to benchmark set:", repo_name, issue_number)
    dump_json(issue_info, issue_stats.export())


def write_issue_diff(repo_name: str, issue_id: int, diff: str, sha: str) -> str:
    """
    Write git-diff of a commit made for an issue in the issues folder.
    :param repo_name: name of parent repository
    :param issue_id:  id of issue for which the commit was made
    :param diff: git-diff of the commit
    :param sha: sha of the commit
    :return: path to the created file
    """
    issue_folder = valid_file_name_of(repo_name) + "/issue_" + str(issue_id) + "/"
    issue_info = issue_folder + "commit_" + sha + ".diff"
    dump_text(issue_info, diff)
    return issue_info


def add_repo_template_if_not_exist(repo_name):
    """
    Create a history file for this repository.
    This enables editing the repo in latter steps.
    :param repo_name: name of repository
    """
    filename = valid_file_name_of(repo_name) + ".json"
    if not Path(benchmark_folder + history_directory + filename).is_file():
        entry = {
            "finished": False,
            "covered_issues": []
        }
        dump_json(history_directory + filename, entry)


def valid_file_name_of(name: str):
    """
    Convert a string into a valid folder name
    :param name: expected folder name
    :return: folder name without invalid characters
    """
    # do not ignore such characters otherwise test/test and tes/ttest will go to the same folder
    return ''.join(i if i not in r'\/:*?<>|' else '_' for i in name).lower()


def finish_repository(repo_statistic: RepositoryStatistics):
    """
    Update the history.json file. Finished repositories are skipped on restart.
    :param repo_statistic: statistics of this repository
    """
    repo_link = repo_statistic.url
    name = repo_statistic.name

    filename = valid_file_name_of(name) + ".json"

    # finish entry in history/repo.json
    with open(benchmark_folder + history_directory + filename) as json_file:
        history = json.load(json_file)
        history["finished"] = True
        dump_json(history_directory + filename, history)

    # add repo to list of finished repos and decide whether the repo is interesting
    with open(benchmark_folder + history_directory + history_file) as history_json:
        finished_repos = json.load(history_json)
        if repo_statistic.interesting_issues:
            finished_repos["interesting"].append(repo_link)
            finished_repos["interesting"] = list(dict.fromkeys(finished_repos["interesting"]))
        else:
            finished_repos["skipped"].append(repo_link)
            finished_repos["skipped"] = list(dict.fromkeys(finished_repos["skipped"]))
        dump_json(history_directory + history_file, finished_repos)

    folder = valid_file_name_of(name) + "/"
    if Path(benchmark_folder + folder).exists():
        if os.listdir(benchmark_folder + folder):
            dump_json(folder + "repository.json", repo_statistic.export())
        else:
            shutil.rmtree(benchmark_folder + folder, ignore_errors=True)


def delete_issue(repo_name, issue_number):
    """
    Deletes an issue from the hard disk
    :param repo_name: name of the repository
    :param issue_number: number of the issue
    :return:
    """
    folder = benchmark_folder + valid_file_name_of(repo_name) + "/issue_" + str(issue_number)
    shutil.rmtree(folder, ignore_errors=True)


def cover_issue(repo_name, issue_id):
    """
    Cover an issue. Covered issues are skipped on retries or restarts.
    :param repo_name: name of repository
    :param issue_id: id of issue
    """
    filename = valid_file_name_of(repo_name) + ".json"
    with open(benchmark_folder + history_directory + filename) as json_file:
        history = json.load(json_file)
        history["covered_issues"].append(issue_id)
        dump_json(history_directory + filename, history)


def is_finished(repo_link) -> bool:
    """
    States whether a repository is finished or not
    :param repo_link: url to repository
    :return: True if repository has already been processed
    """
    with open(benchmark_folder + history_directory + history_file) as json_file:
        curr_json = json.load(json_file)
        return repo_link in curr_json["interesting"] or repo_link in curr_json["skipped"]


def is_covered(repo_name, issue_id) -> bool:
    """
    States if a issue has already been processed previously
    :param repo_name: name of repository
    :param issue_id: id of the current issue
    :return: True if issue with :issue_id: is covered already
    """
    filename = history_directory + valid_file_name_of(repo_name) + ".json"
    if Path(benchmark_folder + filename).is_file():
        with open(benchmark_folder + filename) as json_file:
            return issue_id in json.load(json_file)["covered_issues"]
    return False


def dump_json(filename: str, dictionary: dict):
    """
    Create a json file that represents the dictionary.
    Requires the dictionary to be convertible to json format
    :param filename: name of the file
    :param dictionary: dictionary that can be converted to json format
    """
    filename = benchmark_folder + filename
    Path(filename).parent.mkdir(parents=True, exist_ok=True)
    with open(filename, 'w+') as fp:
        json.dump(dictionary, fp, indent=4, sort_keys=True)


def dump_text(filename: str, content: str):
    """
    Write a text to a file. Create the file if necessary.
    :param filename: name of the file
    :param content: content of the file
    """
    filename = benchmark_folder + filename
    Path(filename).parent.mkdir(parents=True, exist_ok=True)
    with open(filename, 'w+') as fp:
        fp.write(content)


def count_interesting_issues(repo_name: str):
    """
    Count reopened issues of a repository in local file structure.
    This method works even if a repository runs into an error which causes a retry.
    :param repo_name: name of the current repository
    :return: number of found and accepted reopened issues in this repository
    """
    folder = benchmark_folder + valid_file_name_of(repo_name)
    try:
        path, dirs, files = next(walk(folder))
        return len(dirs)
    except StopIteration:
        # empty directory
        return 0


def create_log():
    """
    Create a log file
    """
    Path(benchmark_folder + "logs/").mkdir(parents=True, exist_ok=True)
    file_name = benchmark_folder + "logs/log_" + datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + ".log"
    file_handler = logger.FileHandler(file_name)
    logger.getLogger().addHandler(file_handler)
    logger.getLogger().setLevel(logger.INFO)


def log(*args, level=logger.INFO, sep=' ', skip_in_file=False):
    """
    Log to console and log file
    :param args: all objects to print
    :param level: log level
    :param sep: how to concat args
    :param skip_in_file: whether the current message should be found in the log file
    """
    print(*args)
    if not skip_in_file:
        logger.getLogger().log(level, "[" + logger.getLevelName(level) + "]" +
                               "[" + datetime.now().strftime("%Y-%m-%d %H:%M:%S") + "] " +
                               sep.join(str(a) for a in args))


def read_denylist():
    """
    Exclude all banned repository from exploration
    :return: banned repositories
    """
    banned = set()
    with open(Path(__file__).parent.resolve() / "../config/denylist.txt", "r") as blacklist:
        for line in blacklist.readlines():
            # readlines() adds \n to the line
            line = line[:-1]
            if line.startswith("# ") or line == '':
                continue
            if "github.com/" in line:
                m = re.search(r'.*github\.com/(.*)', line)
                banned.add(m.group(1).replace("\n", ""))
            else:
                banned.add(line)
    return banned


def get_patch(diff_file: str):
    """
    Parse a .diff file to a patch
    :param diff_file: relative path to diff file
    :return: PathSet of the given diff file
    """
    try:
        return PatchSet.from_filename(benchmark_folder + diff_file, encoding='utf-8')
    except UnidiffParseError as e:
        print(e)
        return None


def create_basic_folder_structure():
    """
    Create basic directories and mandatory .json files
    """
    Path(benchmark_folder + history_directory).parent.mkdir(parents=True, exist_ok=True)
    if not Path(benchmark_folder + history_directory + history_file).is_file():
        dump_json(history_directory + history_file, {"interesting": [], "skipped": []})
