from pathlib import Path
import github
import configparser
import os
import time
import datetime
import dateutil.relativedelta

import src.file_manager as fm


def now():
    """
    Current time in seconds (from 1970)
    :return: time in seconds
    """
    return int(time.time_ns() / 1000000000)


class GitHubHandler:

    def __init__(self, config_path=Path(__file__).parent.resolve() / '../config/partial-fix.cfg') -> None:
        """
        GitHubHandler gives access to the github api and handles the api-rate limit
        :param config_path: path to the config file
        """
        try:
            fm.log("Load config from", config_path)
            config = configparser.ConfigParser()
            config.read(config_path)
            access_token = config.get('GITHUB', 'access_token')
            self.config = config
            self.github = github.Github(access_token)
            self.github.get_user().login
            self.started_at = datetime.datetime.now()
            self.remaining, _, self.end = self.rate_limit()
            fm.log("Remaining calls", self.remaining, "Reset:", datetime.datetime.fromtimestamp(self.end))
        except github.BadCredentialsException:
            print("Please add a valid Github Access Token to config/partial-fix.cfg")
            exit(1)

    def wait_until_reset(self):
        """
        If the API-rate limit exceeded, wait until new requests are allowed.
        """
        wait = self.end - now() + 10
        limit = "API-rate-limit"

        search_limit = self.github.get_rate_limit().search
        if search_limit.remaining == 0 and self.remaining != 0:
            wait = 60
            limit = "API-search-limit"

        fm.log(f"Reached {limit}. Sleep for {wait} seconds. Awake at",
               datetime.datetime.fromtimestamp(now() + wait))
        if wait > 0:
            time.sleep(wait)
        self.update_limit()

    def update_limit(self):
        """
        Update remaining requests and the end of the current period
        """
        self.remaining, _, self.end = self.rate_limit()
        fm.log("API-requests remaining:", self.remaining, "API-limit reset:",
               datetime.datetime.fromtimestamp(self.end),
               skip_in_file=self.remaining < 50 or self.remaining % 1000 >= 25)

    def rate_limit(self):
        """
        Return the rate limit.
        :return:
        """
        return *self.github.rate_limiting, self.github.rate_limiting_resettime

    def keywords(self):
        return set(self.config.get('PATTERNS', 'keywords').split(","))

    def labels(self):
        return set(self.config.get('PATTERNS', 'labels').split(","))

    def in_time_range(self, issue):
        time_span = self.config.get('PATTERNS', 'time_span')
        time_step = time_span[-1]
        time_number = 0
        if time_step != '0':
            time_number = int(time_span[:-1])
        issue_created = issue.created_at
        if time_step == 0:
            return True
        elif time_step == "d":
            delta = self.started_at - dateutil.relativedelta.relativedelta(days=time_number)
            return delta >= issue_created
        elif time_step == "m":
            delta = self.started_at - dateutil.relativedelta.relativedelta(months=time_number)
            return delta >= issue_created
        elif time_step == "y":
            delta = self.started_at - dateutil.relativedelta.relativedelta(years=time_number)
            return delta >= issue_created
        return True

    def get_update_limit(self):
        return int(self.config.get('PATTERNS', 'update_recent'))

    def valid_file_extensions(self):
        return set(self.config.get('GENERAL', 'file_extensions').split(","))

    def ignore_file_extensions(self):
        return set(self.config.get('GENERAL', 'ignore_file_extensions').split(","))
