#!/bin/python3

# make this script executable from src/ directory
import datetime
import json
import os.path
import sys
import github
import requests
import time

from urllib.request import urlopen

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

import src.file_manager as fm

from src.github_handler import GitHubHandler
from src.github_search_strategy import ScoreStrategy, StatusStrategy, ReopenStrategy
from src.statistics import RepositoryStatistics
from src.github_search_generator import RepoGenerator


def wait_for_internet_connection():
    """
    If internet connection is lost, wait until connection is established again.
    Important to proceed where the script stopped.
    Prevents duplicate requests for checking if an issue was already processed.
    """
    fm.log("Lost internet connection. Wait for active connection...")
    while True:
        try:
            urlopen('https://google.com', timeout=10)
            break
        except Exception as e:
            fm.log(e)
            time.sleep(5)
    fm.log("Connection established. Continue...")


def retry(function, github_handler, repeat=5):
    """
    Retry a function at most #repeat times if it fails.
    This only counts for exceptions caused by pygithub or inactive internet connection.
    Retries after reaching the API-limit will not be counted as actual retries.
    :param github_handler: github connector
    :param function: retry this function at most #repeat times
    :param repeat: number of retries before we skip calculating the given function
    """
    while repeat != 0:
        try:
            function()
            return
        except github.RateLimitExceededException as e:
            print("Retry because of", e)
            github_handler.wait_until_reset()
            # in cases a single issues exceeds the limit of 5000 requests
            repeat = repeat - 1
        except requests.exceptions.RequestException as e:
            fm.log("Request failed", e, )
            wait_for_internet_connection()
            repeat = repeat - 1
        except github.GithubException as e:
            print("Retry because of", e)
            repeat = repeat - 1
    fm.log("Out of retries. Continue...")


def handle_repository(repository, selected_strategy, github_handler, issues=None):
    """
    Process a repository and search for partial fixes
    :param issues: pre-selected issues (for rerun)
    :param github_handler: github connection
    :param selected_strategy: strategy for issues
    :param repository: the currently processed repository
    """
    ignore_finished = True
    if issues is None:
        ignore_finished = False
        issues = repository.get_issues(state='closed')
    link = repository.html_url
    issue_count = repository.get_issues(state='all').totalCount

    try:
        repo_license = {"name": repository.get_license().license.name,
                        "download": repository.get_license().download_url}
    except github.GithubException as e:
        if e.status == 404:
            repo_license = {"name": "unknown", "download": "undefined"}
        else:
            raise e

    repo_statistic = RepositoryStatistics(
        url=link,
        name=repository.full_name,
        all_issues=issue_count,
        size=repository.size,
        stars=repository.stargazers_count,
        license=repo_license
    )
    fm.add_repo_template_if_not_exist(repository.full_name)
    # skip finished repositories
    if fm.is_finished(link) and not ignore_finished:
        fm.log("Skip already processed repository:", repository, skip_in_file=True)
        return
    for closed_issue in issues:
        # choose a strategy
        # issue urls: repo/issues/number, pull urls: repo/pull/number
        if "/pull/" not in closed_issue.html_url:
            retry(lambda: selected_strategy.handle_issue(repository, closed_issue, ignore_finished=ignore_finished),
                  github_handler)
    repo_statistic.interesting_issues = fm.count_interesting_issues(repo_statistic.name)
    fm.finish_repository(repo_statistic)


def run_configured_search(github_handler, selected_strategy, repository_names):
    """
    Runs the configured search
    :param github_handler: the github handler for handling request and api rate limit
    :param selected_strategy: configured strategy (reopen, status or score)
    :param repository_names: collection of repository names, leave empty to search top rated repositories instead
    :return:
    """
    gen = RepoGenerator(github_handler)
    if len(repository_names) == 0:
        fm.log("Search partial fixes in top rated C repositories")
    else:
        fm.log("Search for partial fixes in given repositories:", repository_names)
        gen.repositories = repository_names

    gen.find_first()
    blocklist = fm.read_denylist()
    fm.log("Running", selected_strategy, "...")
    for repository in gen:
        if repository is None:
            break
        fm.log("Current repository:", repository.full_name)
        if repository.full_name not in blocklist:
            retry(lambda: handle_repository(repository, selected_strategy, github_handler), github_handler)
        else:
            fm.log("Skip repository on denylist:", repository)
    fm.log("Done.")


def rerun_and_update_benchmarks(github_handler, selected_strategy, current_time_stamp):
    """
    If configured, run the current script again on the already discovered issues
    :param github_handler: the github handler for handling request and api rate limit
    :param selected_strategy: specified strategy (reopen, status or score)
    """
    repositories = fm.find_already_processed_benchmarks()
    fm.log("Found", repositories)
    for repository_name in repositories:
        updated_string = \
            json.load(open(fm.benchmark_folder + fm.valid_file_name_of(repository_name) + "/repository.json"))[
                "last_update"]
        updated_time = datetime.datetime.strptime(updated_string, '%Y-%m-%d %H:%M:%S.%f')
        if (current_time_stamp - updated_time).days < github_handler.get_update_limit():
            continue
        repository = github_handler.github.get_repo(repository_name)
        issues = [repository.get_issue(num) for num in repositories[repository_name]]
        fm.log("Current repository:", repository.full_name)
        retry(lambda: handle_repository(repository, selected_strategy, github_handler, issues=issues), github_handler)


def main():
    github_handler = GitHubHandler()
    fm.create_basic_folder_structure()
    fm.create_log()
    repository_names, strategy, update = fm.read_command_line()
    selected_strategy = {
        "score": ScoreStrategy(github_handler),
        "status": StatusStrategy(github_handler),
        "reopen": ReopenStrategy(github_handler)
    }[strategy]
    fm.log("Run configured search." if not update else "Update already processed benchmarks.")
    if update:
        # As the script may run more than 1 day, store the current time stamp once.
        # Otherwise, files are updated although they are in the time range when the script is started.
        current_time = datetime.datetime.now()
        retry(lambda: rerun_and_update_benchmarks(github_handler, selected_strategy, current_time), github_handler)
    else:
        retry(lambda: run_configured_search(github_handler, selected_strategy, repository_names), github_handler)


if __name__ == '__main__':
    sys.exit(main())
