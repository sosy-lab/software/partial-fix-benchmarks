from enum import Enum

import github

from src.github_handler import GitHubHandler
from datetime import datetime
import src.file_manager as fm


class SearchPhase(Enum):
    INIT = 0
    DATE = 1
    CUSTOM = 2


class RepoGenerator(object):

    def __init__(self, github_handler: GitHubHandler):
        self.finished = False
        self.repo_list = []
        self.current_index = 0
        self.list_size = 0
        self.handler = github_handler
        self.keywords = []
        self.counter = 1
        self.date = datetime(2008, 1, 1, 0, 0, 0)

        self.curr_star_count = 0
        self.repositories = []
        self.phase = SearchPhase.INIT

    def __iter__(self):
        return self

    def __next__(self):
        try:
            return self.next()
        except github.RateLimitExceededException:
            self.handler.wait_until_reset()
            return self.__next__()

    def find_first(self):
        """
        If a custom search is configured, return the repository from the custom search.
        Otherwise, find the 1000 top-rated repositories.
        :return: Configured repositories or top-rated repositories.
        """
        self.phase = SearchPhase.CUSTOM if self.repositories else SearchPhase.INIT
        if self.phase == SearchPhase.INIT:
            # find top rated repositories
            language_query = 'language:' + self.handler.config.get('GENERAL', 'language')
            query = '.'.join(self.keywords) + '+' + language_query if self.keywords else language_query
            result = self.handler.github.search_repositories(query, 'stars', 'desc')
            self.list_size = result.totalCount
            self.repo_list = result
            self.current_index = 0
        elif self.phase == SearchPhase.CUSTOM:
            # Collect all repositories from the command line input
            transformed = list()
            for full_name in self.repositories:
                try:
                    transformed.append(self.handler.github.get_repo(full_name))
                except github.GithubException:
                    fm.log("Repository", full_name, "does not exist")
            if not transformed:
                fm.log("Did not find any valid repositories in the given file.")
            self.repo_list = transformed
            self.list_size = len(transformed)

    def find_star_date(self, date):
        query = 'language:' + self.handler.config.get('GENERAL', 'language') + \
                ' stars:' + str(self.curr_star_count) + ' pushed:>=' + date.strftime("%Y-%m-%dT%H:%M:%S+00:00")
        query = '.'.join(self.keywords) + '+' + query if self.keywords else query
        result = self.handler.github.search_repositories(query, 'updated', 'asc')
        self.list_size = result.totalCount
        self.repo_list = result
        self.current_index = 0

    def next(self):
        """
        Get the next repository in the iterator or None if no repository is left.
        None is returned infinitely. To avoid endless loops, make sure to break if None is returned.
        :return: Next repository or None if finished.
        """
        if self.finished:
            return None
        # print("Total Count:",
        #     self.repo_list.totalCount, "| Stored size:", self.list_size,
        #     "| Current index:", self.current_index, "| Phase:", self.phase)
        if self.current_index < self.list_size:
            try:
                fm.log(f"Search {self.counter}. Repository")
                self.counter += 1
                repo = self.repo_list[self.current_index]
                if self.phase == SearchPhase.INIT:
                    # prevent possible endless loop
                    self.curr_star_count = repo.stargazers_count
                self.date = repo.pushed_at
                self.current_index += 1
                return repo
            except IndexError as e:
                # the pygithub module has inconsistencies with totalCount and list sizes
                # currently, there is no elegant way to solve the issue
                fm.log("IndexError:", e, "| Total Count:",
                       self.repo_list.totalCount, "| Stored size:", self.list_size,
                       "| Current index:", self.current_index, "| Phase:", self.phase)
                return self.phase_adaption()
        else:
            return self.phase_adaption()

    def phase_adaption(self):
        """Decide which phase should be entered and which repositories should be searched next"""
        if self.phase == SearchPhase.CUSTOM:
            self.finished = True
            return None
        # Github exists since 2008
        next_date = datetime(2008, 1, 1, 0, 0, 0)
        if self.phase == SearchPhase.INIT:
            self.phase = SearchPhase.DATE
        elif self.phase == SearchPhase.DATE:
            if self.list_size == 1000:
                next_date = self.date
            else:
                self.curr_star_count -= 1
        self.find_star_date(next_date)
        if self.curr_star_count == 0 and self.list_size == 0:
            self.finished = True
            return None
        return self.next()
