from abc import abstractmethod
from typing import Dict, List
from unidiff import PatchSet

import src.file_manager as fm
import github
import github.Repository
import github.Issue
import datetime

from src.statistics import IssueStatistics, CommitStatistics

MANDATORY_PROPERTY_STRATEGY = "strategy"
PROPERTY_INTERSECTING_LINES = "intersecting"
PROPERTY_NO_NEW_FILES = "no_new_files"
PROPERTY_NO_OTHER_FILES = "no_other_files"
PROPERTY_PROHIBITED_FILES = "prohibited_files"
PROPERTY_PROHIBITED_FILES_COUNT = "num_prohibited_files"
PROPERTY_HAS_MAKEFILE = "is_makefile_based"
PROPERTY_CONTAINS_KEYWORD = "contains_keywords"
PROPERTY_REOPEN = "reopen"
PROPERTY_STATUS = "status"
PROPERTY_HAS_LABEL = "has_labels"
PROPERTY_LABELS = "all_labels"
PROPERTY_MAX_TOTAL_CHANGES = "changes"
PROPERTY_FIX_SIZE = "fix_size"
PROPERTY_REOPEN_EVENTS = "reopen_events"
PROPERTY_ACCIDENTAL_REOPEN = "accidental_reopen_likely"


def parse_patch(patch: PatchSet, allowed_endings):
    """
    Parse a diff file
    :param allowed_endings: allowed file endings
    :param patch: parsed diff file from unidiff
    :return: dictionary with important tuples as value
    """
    parsed_dict = dict()
    if not patch:
        return parsed_dict
    for file in patch:
        correct_file = False
        for ending in allowed_endings:
            correct_file = file.source_file.endswith(ending)
            if correct_file:
                break
        if not correct_file:
            continue
        file_name = file.source_file
        parsed_dict[file_name] = []
        for hunk in file:
            for line in hunk:
                modify = 1 if line.is_added else -1 if line.is_removed else 0
                if modify:
                    parsed_dict[file_name].append({"modify": modify,
                                                   "source": line.source_line_no,
                                                   "target": line.target_line_no,
                                                   "content": str(line)})
        if not parsed_dict[file_name]:
            del parsed_dict[file_name]
    return parsed_dict


def virtually_equal_patches(strategy_patch1: Dict[str, List[Dict]],
                            strategy_patch2: Dict[str, List[Dict]]) -> bool:
    """
    Virtually equal patches make changes on the exact same positions in all files
    :param strategy_patch1: parsed patch 1
    :param strategy_patch2: parsed patch 2
    :return: whether the two patches are virtually equal
    """
    if len(strategy_patch1) != len(strategy_patch2):
        return False
    for key, value in strategy_patch1.items():
        if key in strategy_patch2:
            if value != strategy_patch2[key]:
                return False
        else:
            return False
    return True


def delete_duplicate_commits(commits, parents, allowed_endings):
    """
    Delete identical consecutive commits
    :param commits: list of all found commits
    :param parents: number of parents (leading commits)
    :param allowed_endings: allowed file endings
    """
    global PROPERTY_STATUS, PROPERTY_REOPEN
    if len(commits) < 3:
        return

    # remove duplicate commits and check if enough commits for partial fix triple are left
    print("Found", len(commits), "Commits:", commits)
    print("Partial Fixes start at index", parents)
    prev_commit = commits[parents]
    prev_patch = parse_patch(fm.get_patch(prev_commit.diff), allowed_endings)
    to_be_removed = []
    for i in range(parents + 1, len(commits)):
        commit = commits[i]
        curr_patch = parse_patch(fm.get_patch(commit.diff), allowed_endings)
        if virtually_equal_patches(curr_patch, prev_patch):
            # do not remove commits that are interesting even if the are duplicates
            if commit.interesting[PROPERTY_STATUS] and \
                    commit.interesting[PROPERTY_STATUS] != prev_commit.interesting[PROPERTY_STATUS] or \
                    commit.interesting[PROPERTY_REOPEN] and \
                    commit.interesting[PROPERTY_REOPEN] != prev_commit.interesting[PROPERTY_REOPEN]:
                to_be_removed.append(i - 1)
                prev_patch = curr_patch
                prev_commit = commit
            else:
                to_be_removed.append(i)

    to_be_removed.sort(reverse=True)
    for index in to_be_removed:
        commits.pop(index)


def filter_and_score_commits(issue_stats: IssueStatistics, parents: int, allowed_endings):
    """
    Filter all duplicate commits (same commit on different branches) and
    score different patches based on a line distance function.
    :param allowed_endings: allowed file endings in patches
    :param issue_stats: current issues statistics
    :param parents: number of leading parent commits in issue statistics
    :return: obtained points
    """

    def patch_to_filename_line_tuple(patch):
        """
        Make tuples from dict (file name, line number)
        :param patch: diff patch
        :return: set of tuples (file name, line number), all file names
        """
        result_set = set()
        curr_files = set()
        for file in patch:
            filename = file[2::] if file.startswith("a/") or file.startswith("b/") else file
            curr_files.add(filename)
            for entry in patch[file]:
                result_set.add((filename, entry["target"]))
                result_set.add((filename, entry["source"]))
        return result_set, curr_files

    global PROPERTY_INTERSECTING_LINES, PROPERTY_NO_NEW_FILES, PROPERTY_FIX_SIZE

    score_dict = dict()
    commits = issue_stats.commits
    delete_duplicate_commits(commits, parents, allowed_endings)
    if len(commits) < 3:
        return score_dict

    c1 = set()
    filenames = set()
    for i in range(parents, len(commits) - 1):
        diff_patch = fm.get_patch(commits[i].diff)
        parsed_patch = parse_patch(diff_patch, allowed_endings)
        filename_line_tuples, files = patch_to_filename_line_tuple(parsed_patch)
        filenames = filenames.union(files)
        c1 = c1.union(filename_line_tuples)

    parsed_patch = parse_patch(fm.get_patch(commits[-1].diff), allowed_endings)
    c2, c2_files = patch_to_filename_line_tuple(parsed_patch)
    score_dict[PROPERTY_NO_NEW_FILES] = list(sorted(c2_files.difference(filenames)))

    intersection = c1.intersection(c2)

    score_dict[PROPERTY_FIX_SIZE] = len(c1.union(c2))
    score_dict[PROPERTY_INTERSECTING_LINES] = {"intersection_size": len(intersection),
                                               "final_commit_size": len(c2),
                                               "previous_commits_size": len(c1),
                                               "computed_score":
                                                   float(len(intersection)) / len(c2) if len(c2) != 0 else 0}
    return score_dict


class SearchStrategy:
    """
    A search strategy refers to a single issue.
    This class implements the common parts for every possible strategy.
    """

    def __init__(self, github_handler):
        """
        Create a new search strategy
        :param github_handler: a github handler for access to API-rate limit and the config
        """
        self.github_handler = github_handler

    def handle_issue(self, repository, issue, ignore_finished=False):
        """
        Process an issue and store it in the output folder
        :param ignore_finished: set to true to not skip already processed issues
        :param issue: current issue
        :param repository: parent repository
        """
        global PROPERTY_MAX_TOTAL_CHANGES, PROPERTY_NO_OTHER_FILES, \
            PROPERTY_PROHIBITED_FILES, PROPERTY_PROHIBITED_FILES_COUNT

        fm.log("Current Issue:", repository, issue)

        current_issue_number = issue.number
        repo_name = repository.full_name

        # skip processed issues
        if not ignore_finished and \
                (fm.is_covered(repo_name, current_issue_number) or not self.github_handler.in_time_range(issue)):
            fm.log("Skip", repository, issue, skip_in_file=True)
            return

        # find all commits for this issue and cover them
        # each strategy returns a list of commits and a success flag (0 = fail)
        score, issue_commits, parents = self.find_pattern(repository, issue)

        if MANDATORY_PROPERTY_STRATEGY not in score:
            raise ScoreException("Every strategy must at least have the attribute",
                                 MANDATORY_PROPERTY_STRATEGY, "(bool) in its score. Given score_dict:", score)

        if not score[MANDATORY_PROPERTY_STRATEGY] or not issue_commits:
            self.finish_issue(repo_name, current_issue_number, issue)
            return

        last_commit = datetime.datetime(1970, 1, 1, 0, 0, 0)
        for commit in reversed(issue_commits):
            if commit[2]:
                last_commit = commit[0].commit.committer.date
                break

        first_commit = datetime.datetime(1970, 1, 1, 0, 0, 0)
        for commit in issue_commits:
            if commit[2]:
                first_commit = commit[0].commit.committer.date
                break

        # store the results as json
        issue_stats = IssueStatistics(
            number=current_issue_number,
            first_commit=first_commit,
            last_commit=last_commit,
            associated_pull_request=issue.pull_request)

        # add all commits changing a .c file
        allowed_file_extensions_only = True
        prohibited_files = []
        # NOTE: commit[0] has the type str if commit[2] = True (commit exists)
        for i in range(0, len(issue_commits)):
            commit = issue_commits[i]
            associated_pull_request = list()
            if commit[2]:
                for request in commit[0].get_pulls():
                    associated_pull_request.append(request.html_url)
                    if len(associated_pull_request) == 10:
                        break
            commit_stats = CommitStatistics.of(repo=repository,
                                               issue_id=current_issue_number,
                                               extensions=self.github_handler.valid_file_extensions(),
                                               ignore=self.github_handler.ignore_file_extensions(),
                                               date=str(commit[0].commit.author.date) if commit[2] else "N/A",
                                               sha=commit[0].sha if commit[2] else commit[0],
                                               files=commit[0].files if commit[2] else [],
                                               interesting=commit[1],
                                               exists=commit[2],
                                               is_parent=i < parents,
                                               associated_pull_request=associated_pull_request)
            if commit_stats.has_valid_files:
                issue_stats.append_commit(commit_stats)
                prohibited_files += commit_stats.prohibited_files
                allowed_file_extensions_only = \
                    allowed_file_extensions_only and commit_stats.allowed_file_extensions_only
            elif commit_stats.is_parent:
                issue_stats.append_commit(commit_stats)

        # if we score issues we do not consider the parents
        # here we make sure that at least 2 commits (that are not parents) are available
        if len(issue_stats.commits) <= parents + 1:
            self.finish_issue(repo_name, current_issue_number, issue)
            return

        score = {**score,
                 **filter_and_score_commits(issue_stats, parents, self.github_handler.valid_file_extensions()),
                 PROPERTY_NO_OTHER_FILES: allowed_file_extensions_only,
                 PROPERTY_PROHIBITED_FILES: prohibited_files[:10],
                 PROPERTY_PROHIBITED_FILES_COUNT: len(prohibited_files)}

        # if none of the commits edited a .c file skip this issue
        # we need at least three commits (including parents) to ensure the triple (init, partial-fix, fix)
        if len(issue_stats.commits) < 3:
            self.finish_issue(repo_name, current_issue_number, issue)
            return

        total_changes = sum(commit.diff_additions + commit.diff_deletions for commit in issue_stats.commits)

        score["total_changes"] = total_changes
        issue_stats.score = score

        fm.write_issue_statistics(repo_name, issue_stats)
        fm.cover_issue(repo_name, current_issue_number)
        self.github_handler.update_limit()
        fm.log("Found interesting Issue:", repo_name, issue)

    @abstractmethod
    def find_pattern(self, repository: github.Repository, issue: github.Issue, events=None):
        """
        Find a pattern in an issue
        :param repository: repository where this issue was created
        :param issue: current issue
        :param events: optionally provide all issue events to reduce requests on multiple executions
        :return: score, all issue commits and the number of leading parent commits
        """
        pass

    def finish_issue(self, repo_name, issue_number, issue):
        """
        Finish an issue and store it to the file
        :param repo_name: Name of the repository with the current issue
        :param issue_number: number of current issue
        :param issue: current issue
        """
        fm.cover_issue(repo_name, issue_number)
        fm.log("Finished", issue, "Repository(full_name=\"" + repo_name + "\")", skip_in_file=True)
        fm.delete_issue(repo_name, issue_number)
        self.github_handler.update_limit()


class ReopenStrategy(SearchStrategy):
    """
    Searches the following pattern: (Commit 1, ..., Commit n) where at least one commit was made after a reopen event.
    """

    def __repr__(self):
        return "ReopenStrategy"

    def find_pattern(self, repository, issue, events=None):
        global MANDATORY_PROPERTY_STRATEGY, PROPERTY_REOPEN, PROPERTY_REOPEN_EVENTS, PROPERTY_ACCIDENTAL_REOPEN

        issue_commits = list(list())
        event_log = list()
        commit_before_reopen = False
        commit_after_reopen = False
        events = events if events else issue.get_events()
        for event in events:
            event_description = event.event
            if event_description == "reopened":
                event_log.append((event.created_at, "reopened"))
                if issue_commits:
                    fm.log("Found reopened issue:", issue.html_url)
                    commit_before_reopen = True
                    issue_commits[-1][1] = {PROPERTY_REOPEN: True}
                else:
                    fm.log("Skip reopened event because no commits were made yet:", issue)
            if event_description == "closed":
                event_log.append((event.created_at, "closed"))
            if event.commit_id is not None:
                event_log.append((event.created_at, "commit"))
                try:
                    current_commit = repository.get_commit(event.commit_id)
                    issue_commits.append([current_commit, {PROPERTY_REOPEN: False}, True])
                    if commit_before_reopen:
                        commit_after_reopen = True
                except github.GithubException:
                    issue_commits.append([event.commit_id, {PROPERTY_REOPEN: False}, False])
                    if commit_before_reopen:
                        commit_after_reopen = True
                    fm.log("Do not store commit because sha hash cannot be found:", repository, issue, event.commit_id)

        # process obtained commits, do not skip on failed because of ScoreStrategy (mismatched commits on merge)
        if len(issue_commits) < 2:
            return {MANDATORY_PROPERTY_STRATEGY: False}, issue_commits, 0

        score = dict()
        score[MANDATORY_PROPERTY_STRATEGY] = True

        last_reopen = None
        accidental_reopen = False
        for (timestamp, event_type) in event_log:
            if event_type == "reopened":
                last_reopen = timestamp
            elif event_type == "closed":
                if last_reopen:
                    # less than 5 minutes
                    if (timestamp - last_reopen).total_seconds() < 300:
                        accidental_reopen = True
                        break
            elif event_type == "commit":
                last_reopen = None

        score[PROPERTY_REOPEN_EVENTS] = [(str(time), evt) for (time, evt) in event_log]
        score[PROPERTY_ACCIDENTAL_REOPEN] = accidental_reopen
        if not commit_after_reopen or not commit_before_reopen:
            score[MANDATORY_PROPERTY_STRATEGY] = False

        parents = 0
        if issue_commits[0][2]:
            first_commit = issue_commits[0][0]
            parents = len(first_commit.parents)
            issue_commits = [[parent, {PROPERTY_REOPEN: False}, True] for parent in
                             first_commit.parents] + issue_commits

        return score, issue_commits, parents


class StatusStrategy(SearchStrategy):
    """
    Searches the following pattern: (Commit 1, ... Commit n) where Commit n needs a succeeding CI and at least one of
    Commit 1 to Commit n-1 has a failing CI.
    """

    def __repr__(self):
        return "StatusStrategy"

    def find_pattern(self, repository, issue, events=None):
        global MANDATORY_PROPERTY_STRATEGY, PROPERTY_STATUS

        issue_commits = list(list())
        failed = False
        events = events if events else issue.get_events()
        unknown_commits = 0
        for event in events:
            if event.commit_id is not None:
                try:
                    current_commit = repository.get_commit(event.commit_id)
                    if current_commit.get_combined_status().state == "failure":
                        failed = True
                        issue_commits.append([current_commit, {PROPERTY_STATUS: True}, True])
                    else:
                        issue_commits.append([current_commit, {PROPERTY_STATUS: False}, True])
                except github.GithubException:
                    issue_commits.append([event.commit_id, {PROPERTY_STATUS: False}, False])
                    unknown_commits += 1
                    fm.log("Do not store commit because sha hash cannot be found:", repository, issue, event.commit_id)

        # process obtained commits, do not skip on failed because of ScoreStrategy (mismatched commits on merge)
        if len(issue_commits) < 2 or unknown_commits == len(issue_commits):
            return {MANDATORY_PROPERTY_STRATEGY: False}, issue_commits, 0

        score = dict()
        score[MANDATORY_PROPERTY_STRATEGY] = True
        if isinstance(issue_commits[-1][0], str) \
                or issue_commits[-1][0].get_combined_status().state != "success" or not failed:
            score[MANDATORY_PROPERTY_STRATEGY] = False

        parents = 0
        if issue_commits[0][2]:
            first_commit = issue_commits[0][0]
            parents = len(first_commit.parents)
            issue_commits = [[parent, {PROPERTY_STATUS: parent.get_combined_status().state == "failure"}, True]
                             for parent in first_commit.parents] + issue_commits

        return score, issue_commits, parents


def has_makefile(repository) -> bool:
    """
    States if a given repository has a makefile
    :param repository: the current repository
    :return: True if a makefile exists, False else
    """
    # we search for makefiles in the root directory only (rate limit savings)
    for file in repository.get_contents(""):
        if "makefile" in file.name.lower() or "cmakelists" in file.name.lower():
            return True
    return False


class ScoreStrategy(SearchStrategy):
    """
    Score strategy executes all other strategies and values the results respectively.
    Additionally it increases the score if labels and keywords are part of an issue.
    """

    def __init__(self, github_handler):
        super().__init__(github_handler)

    def __repr__(self):
        return "ScoreStrategy"

    def find_pattern(self, repository, issue, events=None):
        global PROPERTY_HAS_LABEL, PROPERTY_CONTAINS_KEYWORD, \
            PROPERTY_HAS_MAKEFILE, MANDATORY_PROPERTY_STRATEGY, PROPERTY_REOPEN, PROPERTY_STATUS, PROPERTY_LABELS

        events = events if events else issue.get_events()
        reopen_score, issue_commits_reopen, parents = \
            ReopenStrategy(self.github_handler).find_pattern(repository, issue, events)
        status_score, issue_commits_status, _ = \
            StatusStrategy(self.github_handler).find_pattern(repository, issue, events)

        combined_issues = []

        for [commit1, status1, exists], [commit2, status2, exists2] in zip(issue_commits_reopen, issue_commits_status):
            if commit1 == commit2:
                combined_issues.append([commit1, {**status1, **status2}, exists and exists2])
            else:
                # should be impossible
                fm.log("ERROR: ", commit1, "and", commit2, "are different but shouldn't!")
                fm.log("  - reopen list:", issue_commits_reopen, "\n  - status list:", issue_commits_status)
                return {MANDATORY_PROPERTY_STRATEGY: False}, [], 0

        if not issue_commits_reopen or not issue_commits_status:
            return {MANDATORY_PROPERTY_STRATEGY: False}, combined_issues, 0

        if not reopen_score[MANDATORY_PROPERTY_STRATEGY] and not status_score[MANDATORY_PROPERTY_STRATEGY]:
            return {MANDATORY_PROPERTY_STRATEGY: False}, combined_issues, 0

        total_score = {**reopen_score,
                       **status_score,
                       MANDATORY_PROPERTY_STRATEGY: True,
                       PROPERTY_REOPEN: reopen_score[MANDATORY_PROPERTY_STRATEGY],
                       PROPERTY_STATUS: status_score[MANDATORY_PROPERTY_STRATEGY]}

        commit_messages = ""
        for commit in combined_issues:
            # if commit exists
            if commit[2]:
                commit_messages += commit[0].commit.message + "\n"

        issue_comments = issue.title + "\n" + "\n".join(comment.body for comment in issue.get_comments())

        folder_name = fm.valid_file_name_of(repository.full_name) + "/issue_" + str(issue.number) + "/"

        fm.dump_text(folder_name + "commit_messages.txt", commit_messages)
        fm.dump_text(folder_name + "issue_comments.txt", issue_comments)

        comments = commit_messages + "\n\n" + issue_comments
        total_score[PROPERTY_CONTAINS_KEYWORD] = []
        for keyword in self.github_handler.keywords():
            if keyword in comments:
                total_score[PROPERTY_CONTAINS_KEYWORD].append(keyword)

        all_label_names = [label.name for label in issue.get_labels()]
        total_score[PROPERTY_LABELS] = all_label_names
        total_score[PROPERTY_HAS_LABEL] = []
        for label in self.github_handler.labels():
            for found in all_label_names:
                if found in label:
                    total_score[PROPERTY_HAS_LABEL].append(label)

        total_score[PROPERTY_HAS_MAKEFILE] = has_makefile(repository)

        return total_score, combined_issues, parents


class ScoreException(Exception):
    pass
