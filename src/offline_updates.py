import src.file_manager as fm
import json


def get_repository(full_name):
    path = fm.benchmark_folder + fm.valid_file_name_of(full_name) + "/"
    data = json.load(open(path + "repository.json"))
    data["root"] = path
    return data


def get_issue(repository, number):
    path = repository["root"] + "issue_" + str(number) + "/"
    data = json.load(open(path + "info.json"))
    data["root"] = path
    data["issue_comments"] = path + "issue_comments.txt"
    data["commit_messages"] = path + "commit_messages.txt"
    return data


def offline_update(update_function, query=None):
    """
    Select issues to run the update function on them.
    The query has to have the form: {"user/repo-name": [list of issue numbers], ...}
    An empty list of issue numbers means that all issues are taken into account where
    negative numbers mean that the issue will be excluded.
    You cannot mix up positive and negative numbers in a list.
    :param update_function: a function that takes repository and issue as arguments
    :param query: Dictionary that maps repositories to their issue numbers
    """
    to_search = dict()
    all_issues = fm.find_already_processed_benchmarks()
    if query:
        interesting = set(all_issues.keys()).intersection(set(query.keys()))
        for repo in interesting:
            if not query[repo]:
                to_search[repo] = all_issues[repo]
            elif all([x < 0 for x in query[repo]]):
                to_search[repo] = set(all_issues[repo]).difference({-x for x in query[repo]})
            elif all([x > 0 for x in query[repo]]):
                to_search[repo] = set(query[repo]).intersection(all_issues[repo])
            else:
                print("Skip", repo, "since the provided list contains both, positive and negative integers!")
    else:
        to_search = all_issues

    for repo in to_search:
        # if the user aborts the script, maybe the issue.json is not written yet. Skip these cases
        try:
            parsed_repo = get_repository(repo)
            for issue in to_search[repo]:
                update_function(parsed_repo, get_issue(parsed_repo, issue))
        except FileNotFoundError:
            continue
