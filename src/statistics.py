import datetime
import urllib.request
from http.client import IncompleteRead

import github
import logging as logger
from abc import abstractmethod
from dataclasses import dataclass, field
from typing import List, Dict

import src.file_manager as fm


class Statistics:

    @abstractmethod
    def export(self) -> dict:
        """
        Convert an statistics object to a dictionary.
        :return: dictionary representing this class
        """
        pass


def get_diff(repo_url, sha) -> str:
    """
    Returns git-diff for a specified commit
    :param repo_url: current repository
    :param sha: commit for git-diff as sha
    :return: git-diff of commit.
    """
    commit_url = repo_url + '/commit/' + sha + '.diff'
    try:
        page = urllib.request.urlopen(commit_url)
        return page.read().decode('utf-8')
    except UnicodeDecodeError as e:
        fm.log("Cannot decode", repo_url, e)
        return "Could not decode file. (" + str(repo_url) + ", " + sha + ")"
    except IncompleteRead as e:
        fm.log("Incomplete Read", repo_url, e)
        return "Incomplete Read. (" + str(repo_url) + ", " + sha + ")"
    except Exception as e:
        fm.log("urllib exception", repo_url, e)
        return "urllib exception. (" + str(repo_url) + ", " + sha + ")"


@dataclass
class FileStatistics(Statistics):
    file_deletions: int = 0
    file_additions: int = 0
    file_name: str = ""
    file_size: int = 0
    file_exists: bool = True

    @staticmethod
    def of(repository, file):
        """
        Create file statistics for a given file in a repository
        :param repository: parent repository
        :param file: this object contains the statistics for this file
        :return: a FileStatistics object with data about the given file
        """
        file_stats = FileStatistics()
        try:
            content_file = repository.get_contents(file.filename)
            file_stats.file_size = content_file.size
        except github.UnknownObjectException:
            file_stats.file_size = 0
            file_stats.file_exists = False
            fm.log("File", file, "does not exist anymore", level=logger.ERROR)
        file_stats.file_deletions = file.deletions
        file_stats.file_additions = file.additions
        file_stats.file_name = file.filename
        return file_stats

    def export(self) -> dict:
        return {
            "file_deletions": self.file_deletions,
            "file_additions": self.file_additions,
            "file_name": self.file_name,
            "file_size": self.file_size,
            "file_exists": self.file_exists
        }


@dataclass
class CommitStatistics(Statistics):
    sha: str
    interesting: Dict[str, bool]
    exists: bool
    date: str
    associated_pull_request: List[str]
    is_parent: bool = False
    diff: str = ""
    has_valid_files: bool = False
    diff_additions: int = 0
    diff_deletions: int = 0
    diff_file_size: int = 0
    diff_files: List = field(default_factory=lambda: [])
    diff_url: str = "unknown"
    allowed_file_extensions_only = True
    prohibited_files = []

    def export(self) -> dict:
        return {
            "sha": self.sha,
            "date": self.date,
            "interesting": self.interesting,
            "diff": self.diff,
            "diff_additions": self.diff_additions,
            "diff_deletions": self.diff_deletions,
            "diff_file_size": self.diff_file_size,
            "diff_url": self.diff_url,
            "files": [exp.export() for exp in self.diff_files],
            "exists": self.exists,
            "is_parent": self.is_parent,
            "associated_pull_request": self.associated_pull_request
        }

    def append_file(self, file_stats: FileStatistics):
        self.diff_files.append(file_stats)

    @staticmethod
    def of(repo, issue_id, extensions, ignore, sha, files, interesting: Dict[str, bool], is_parent, exists, date,
           associated_pull_request: List[str]):
        """
        Create statistics for a given commit in a repository
        :param date: creation date as string
        :param files: files affected by this commit
        :param sha: sha hash of commit
        :param exists: whether the file could be downloaded
        :param is_parent: whether the commit is a parent or part of the fix
        :param ignore: file extensions that cannot be part of fixes (e.g. txt or md files)
        :param interesting: whether a commit fulfills a certain criteria
        :param repo: parent repository
        :param issue_id: issue, where this commit was made
        :param extensions: file extensions to track
        :param associated_pull_request: list of urls to associated pull requests
        :return: a CommitStatistics object with data for given commit
        """
        commit_stats = CommitStatistics(sha=sha, interesting=interesting, exists=exists, is_parent=is_parent, date=date,
                                        associated_pull_request=associated_pull_request)
        has_valid_files = False
        if exists:
            for file in files:
                filename = file.filename
                if not any([filename.endswith(ext) for ext in extensions]):
                    if not any([filename.endswith(ext) for ext in ignore]):
                        commit_stats.prohibited_files.append(filename)
                        commit_stats.allowed_file_extensions_only = False
                    continue
                has_valid_files = True
                file_stats = FileStatistics.of(repo, file)
                commit_stats.append_file(file_stats)
                commit_stats.diff_deletions = commit_stats.diff_deletions + file.deletions
                commit_stats.diff_additions = commit_stats.diff_additions + file.additions
                commit_stats.diff_file_size = commit_stats.diff_file_size + file_stats.file_size
            if has_valid_files:
                diff = get_diff(repo.html_url, sha)
                commit_stats.diff = fm.write_issue_diff(repo.full_name, issue_id, diff, sha)
        commit_stats.has_valid_files = has_valid_files
        commit_stats.diff_url = repo.html_url + '/commit/' + sha + '.diff'
        return commit_stats


@dataclass
class IssueStatistics(Statistics):
    number: int
    first_commit: datetime.datetime
    last_commit: datetime.datetime
    associated_pull_request: str
    commits: List[CommitStatistics] = field(default_factory=lambda: [])
    score: int = 0

    def export(self) -> dict:
        return {
            "number": self.number,
            "pull_request": self.associated_pull_request if self.associated_pull_request else "None",
            "score": self.score,
            "first_commit": str(self.first_commit),
            "last_commit": str(self.last_commit),
            "commits": [exp.export() for exp in self.commits]
        }

    def append_commit(self, commit: CommitStatistics):
        self.commits.append(commit)


@dataclass
class RepositoryStatistics(Statistics):
    url: str
    name: str
    all_issues: int
    size: int
    stars: int
    license: dict
    interesting_issues: int = 0

    def export(self) -> dict:
        return {
            "url": self.url,
            "size": self.size,
            "interesting_issues": self.interesting_issues,
            "all_issues": self.all_issues,
            "stars": self.stars,
            "ratio": self.interesting_issues / self.all_issues,
            "license": self.license,
            "last_update": str(datetime.datetime.now())
        }
